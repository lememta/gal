SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

ORIGINAL_DIR=`pwd`

echo "-------------------------------------"
echo "Launching Simulink 2 GAESM conversion"
echo "-------------------------------------"

cd ${DIR}/1-Simulink2GAESM/

./simulink2gaesm.sh ${ORIGINAL_DIR}/$1 $2 $3 $4 $5 $6 $7 $8 $9

if [ $? -eq 0 ]; then

FILE_NAME="${1%.*}"

echo "-----------------------------------"
echo "Launching GAESM 2 Lustre conversion"
echo "-----------------------------------"

cd ${DIR}/2-GAESM2Lustre/

./gaesm2lustre.sh ${ORIGINAL_DIR}/${FILE_NAME}.gaesm

cd ${ORIGINAL_DIR}

fi
