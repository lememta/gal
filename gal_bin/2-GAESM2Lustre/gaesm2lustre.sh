#!/bin/sh

SCRIPT=`readlink -f $0`
export GENEAUTO_HOME=`dirname $SCRIPT`"/"

java -Xmx1024m -cp "$GENEAUTO_HOME/geneauto.gaesm2lustre.launcher-2.4.10.jar" geneauto.gaesm2lustre.launcher.GAESM2Lustre $1 $2
