This folder contains the binary version of the Simulink to Lustre model
converter. It is in an early stage of development and highly prototypical.

In order to launch the generation, simply use the Simulink2Lustre.sh script as
if you wanted to use it to launch GeneAuto.

-- Generate the Lustre code for the PriceInduction model:

./Simulink2Lustre.sh MODEL_PATH [-m FULL_CST_PATH]

If we set:
- MODEL_PATH: [relative] path to the mdl file to generate lustre code for
- FULL_CST_PATH: [absolute] path to one m file containing the constants for the model file

Rq: it is really important to give the full path to the constant file.
Rq2: it is not possible to provide multiple .m files

Further informations on how to launch GeneAuto from command line can be found on
the geneauto.org website.

Have fun !
