package geneauto.xtext.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalLustreLexer extends Lexer {
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int RULE_ID=5;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__29=29;
    public static final int T__64=64;
    public static final int T__28=28;
    public static final int T__65=65;
    public static final int T__27=27;
    public static final int T__62=62;
    public static final int T__26=26;
    public static final int T__63=63;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int T__61=61;
    public static final int EOF=-1;
    public static final int T__60=60;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__19=19;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__16=16;
    public static final int T__51=51;
    public static final int T__15=15;
    public static final int T__52=52;
    public static final int T__18=18;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int T__59=59;
    public static final int RULE_INT=4;
    public static final int T__50=50;
    public static final int RULE_LUS_REAL=8;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=10;
    public static final int RULE_ML_COMMENT=11;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_STRING=6;
    public static final int T__33=33;
    public static final int T__71=71;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__70=70;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_LUS_BOOLEAN=9;
    public static final int RULE_WS=12;
    public static final int RULE_LUS_DOUBLE=7;

    // delegates
    // delegators

    public InternalLustreLexer() {;} 
    public InternalLustreLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalLustreLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g"; }

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:11:7: ( '->' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:11:9: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12:7: ( 'and' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:13:7: ( '=>' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:13:9: '=>'
            {
            match("=>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:14:7: ( 'not' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:14:9: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:15:7: ( 'when' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:15:9: 'when'
            {
            match("when"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:16:7: ( 'nor' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:16:9: 'nor'
            {
            match("nor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:17:7: ( ';' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:17:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:18:7: ( '.' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:18:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:19:7: ( 'BLOCK' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:19:9: 'BLOCK'
            {
            match("BLOCK"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:20:7: ( 'SIGNAL' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:20:9: 'SIGNAL'
            {
            match("SIGNAL"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:21:7: ( 'PARAMETER' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:21:9: 'PARAMETER'
            {
            match("PARAMETER"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:22:7: ( 'PORT' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:22:9: 'PORT'
            {
            match("PORT"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:23:7: ( 'MAIN' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:23:9: 'MAIN'
            {
            match("MAIN"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:24:7: ( 'requires' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:24:9: 'requires'
            {
            match("requires"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:25:7: ( 'ensures' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:25:9: 'ensures'
            {
            match("ensures"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:26:7: ( 'observer' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:26:9: 'observer'
            {
            match("observer"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:27:7: ( 'int' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:27:9: 'int'
            {
            match("int"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:28:7: ( 'real' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:28:9: 'real'
            {
            match("real"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:29:7: ( 'bool' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:29:9: 'bool'
            {
            match("bool"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:30:7: ( 'double' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:30:9: 'double'
            {
            match("double"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:31:7: ( 'or' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:31:9: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:32:7: ( 'xor' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:32:9: 'xor'
            {
            match("xor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:33:7: ( '=' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:33:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:34:7: ( '<>' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:34:9: '<>'
            {
            match("<>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:35:7: ( '<=' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:35:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:36:7: ( '<' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:36:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:37:7: ( '>=' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:37:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:38:7: ( '>' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:38:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:39:7: ( '+' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:39:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:40:7: ( '-' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:40:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:41:7: ( '*' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:41:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:42:7: ( '/' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:42:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:43:7: ( 'div' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:43:9: 'div'
            {
            match("div"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:44:7: ( 'mod' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:44:9: 'mod'
            {
            match("mod"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:45:7: ( 'pre' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:45:9: 'pre'
            {
            match("pre"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:46:7: ( 'current' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:46:9: 'current'
            {
            match("current"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:47:7: ( '#open' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:47:9: '#open'
            {
            match("#open"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:48:7: ( 'node' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:48:9: 'node'
            {
            match("node"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:49:7: ( '(' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:49:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:50:7: ( ')' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:50:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:51:7: ( 'returns' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:51:9: 'returns'
            {
            match("returns"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:52:7: ( 'let' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:52:9: 'let'
            {
            match("let"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:53:7: ( 'tel' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:53:9: 'tel'
            {
            match("tel"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:54:7: ( 'var' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:54:9: 'var'
            {
            match("var"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:55:7: ( 'function' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:55:9: 'function'
            {
            match("function"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:56:7: ( 'const' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:56:9: 'const'
            {
            match("const"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "T__60"
    public final void mT__60() throws RecognitionException {
        try {
            int _type = T__60;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:57:7: ( '--@' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:57:9: '--@'
            {
            match("--@"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__60"

    // $ANTLR start "T__61"
    public final void mT__61() throws RecognitionException {
        try {
            int _type = T__61;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:58:7: ( '--!' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:58:9: '--!'
            {
            match("--!"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__61"

    // $ANTLR start "T__62"
    public final void mT__62() throws RecognitionException {
        try {
            int _type = T__62;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:59:7: ( ':' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:59:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__62"

    // $ANTLR start "T__63"
    public final void mT__63() throws RecognitionException {
        try {
            int _type = T__63;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:60:7: ( '--Trace' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:60:9: '--Trace'
            {
            match("--Trace"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__63"

    // $ANTLR start "T__64"
    public final void mT__64() throws RecognitionException {
        try {
            int _type = T__64;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:61:7: ( 'assert' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:61:9: 'assert'
            {
            match("assert"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__64"

    // $ANTLR start "T__65"
    public final void mT__65() throws RecognitionException {
        try {
            int _type = T__65;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:62:7: ( ',' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:62:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__65"

    // $ANTLR start "T__66"
    public final void mT__66() throws RecognitionException {
        try {
            int _type = T__66;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:63:7: ( '[' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:63:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__66"

    // $ANTLR start "T__67"
    public final void mT__67() throws RecognitionException {
        try {
            int _type = T__67;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:64:7: ( ']' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:64:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__67"

    // $ANTLR start "T__68"
    public final void mT__68() throws RecognitionException {
        try {
            int _type = T__68;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:65:7: ( '^' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:65:9: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__68"

    // $ANTLR start "T__69"
    public final void mT__69() throws RecognitionException {
        try {
            int _type = T__69;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:66:7: ( 'if' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:66:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__69"

    // $ANTLR start "T__70"
    public final void mT__70() throws RecognitionException {
        try {
            int _type = T__70;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:67:7: ( 'then' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:67:9: 'then'
            {
            match("then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__70"

    // $ANTLR start "T__71"
    public final void mT__71() throws RecognitionException {
        try {
            int _type = T__71;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:68:7: ( 'else' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:68:9: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__71"

    // $ANTLR start "RULE_LUS_REAL"
    public final void mRULE_LUS_REAL() throws RecognitionException {
        try {
            int _type = RULE_LUS_REAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:15: ( ( '-' )? ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT ) ( ( 'E' | 'e' ) ( '-' )? RULE_INT )? )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:17: ( '-' )? ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT ) ( ( 'E' | 'e' ) ( '-' )? RULE_INT )?
            {
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:17: ( '-' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='-') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:17: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:22: ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT )
            int alt2=3;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:23: RULE_INT '.'
                    {
                    mRULE_INT(); 
                    match('.'); 

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:36: '.' RULE_INT
                    {
                    match('.'); 
                    mRULE_INT(); 

                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:49: RULE_INT '.' RULE_INT
                    {
                    mRULE_INT(); 
                    match('.'); 
                    mRULE_INT(); 

                    }
                    break;

            }

            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:72: ( ( 'E' | 'e' ) ( '-' )? RULE_INT )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='E'||LA4_0=='e') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:73: ( 'E' | 'e' ) ( '-' )? RULE_INT
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}

                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:83: ( '-' )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0=='-') ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12046:83: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }

                    mRULE_INT(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LUS_REAL"

    // $ANTLR start "RULE_LUS_DOUBLE"
    public final void mRULE_LUS_DOUBLE() throws RecognitionException {
        try {
            int _type = RULE_LUS_DOUBLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:17: ( ( '-' )? ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT ) 'd' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:19: ( '-' )? ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT ) 'd'
            {
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:19: ( '-' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='-') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:19: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:24: ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT )
            int alt6=3;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:25: RULE_INT '.'
                    {
                    mRULE_INT(); 
                    match('.'); 

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:38: '.' RULE_INT
                    {
                    match('.'); 
                    mRULE_INT(); 

                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12048:51: RULE_INT '.' RULE_INT
                    {
                    mRULE_INT(); 
                    match('.'); 
                    mRULE_INT(); 

                    }
                    break;

            }

            match('d'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LUS_DOUBLE"

    // $ANTLR start "RULE_LUS_BOOLEAN"
    public final void mRULE_LUS_BOOLEAN() throws RecognitionException {
        try {
            int _type = RULE_LUS_BOOLEAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12050:18: ( ( 'true' | 'false' ) )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12050:20: ( 'true' | 'false' )
            {
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12050:20: ( 'true' | 'false' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='t') ) {
                alt7=1;
            }
            else if ( (LA7_0=='f') ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12050:21: 'true'
                    {
                    match("true"); 


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12050:28: 'false'
                    {
                    match("false"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LUS_BOOLEAN"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12052:17: ( '--' ~ ( ( '@' | '!' | 'T' ) ) (~ ( ( '\\r' | '\\n' ) ) )* ( '\\r' )? '\\n' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12052:19: '--' ~ ( ( '@' | '!' | 'T' ) ) (~ ( ( '\\r' | '\\n' ) ) )* ( '\\r' )? '\\n'
            {
            match("--"); 

            if ( (input.LA(1)>='\u0000' && input.LA(1)<=' ')||(input.LA(1)>='\"' && input.LA(1)<='?')||(input.LA(1)>='A' && input.LA(1)<='S')||(input.LA(1)>='U' && input.LA(1)<='\uFFFF') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12052:41: (~ ( ( '\\r' | '\\n' ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='\u0000' && LA8_0<='\t')||(LA8_0>='\u000B' && LA8_0<='\f')||(LA8_0>='\u000E' && LA8_0<='\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12052:41: ~ ( ( '\\r' | '\\n' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12052:57: ( '\\r' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='\r') ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12052:57: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12054:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12054:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12054:11: ( '^' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='^') ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12054:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12054:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='0' && LA11_0<='9')||(LA11_0>='A' && LA11_0<='Z')||LA11_0=='_'||(LA11_0>='a' && LA11_0<='z')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12056:10: ( ( '0' .. '9' )+ )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12056:12: ( '0' .. '9' )+
            {
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12056:12: ( '0' .. '9' )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='0' && LA12_0<='9')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12056:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:13: ( ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='\"') ) {
                alt15=1;
            }
            else if ( (LA15_0=='\'') ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:16: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:20: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop13:
                    do {
                        int alt13=3;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0=='\\') ) {
                            alt13=1;
                        }
                        else if ( ((LA13_0>='\u0000' && LA13_0<='!')||(LA13_0>='#' && LA13_0<='[')||(LA13_0>=']' && LA13_0<='\uFFFF')) ) {
                            alt13=2;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:21: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:66: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:86: '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:91: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop14:
                    do {
                        int alt14=3;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0=='\\') ) {
                            alt14=1;
                        }
                        else if ( ((LA14_0>='\u0000' && LA14_0<='&')||(LA14_0>='(' && LA14_0<='[')||(LA14_0>=']' && LA14_0<='\uFFFF')) ) {
                            alt14=2;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:92: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12058:137: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12060:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12060:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12060:24: ( options {greedy=false; } : . )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0=='*') ) {
                    int LA16_1 = input.LA(2);

                    if ( (LA16_1=='/') ) {
                        alt16=2;
                    }
                    else if ( ((LA16_1>='\u0000' && LA16_1<='.')||(LA16_1>='0' && LA16_1<='\uFFFF')) ) {
                        alt16=1;
                    }


                }
                else if ( ((LA16_0>='\u0000' && LA16_0<=')')||(LA16_0>='+' && LA16_0<='\uFFFF')) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12060:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12062:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12062:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12062:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt17=0;
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>='\t' && LA17_0<='\n')||LA17_0=='\r'||LA17_0==' ') ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt17 >= 1 ) break loop17;
                        EarlyExitException eee =
                            new EarlyExitException(17, input);
                        throw eee;
                }
                cnt17++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12064:16: ( . )
            // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:12064:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:8: ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | RULE_LUS_REAL | RULE_LUS_DOUBLE | RULE_LUS_BOOLEAN | RULE_SL_COMMENT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt18=68;
        alt18 = dfa18.predict(input);
        switch (alt18) {
            case 1 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:10: T__14
                {
                mT__14(); 

                }
                break;
            case 2 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:16: T__15
                {
                mT__15(); 

                }
                break;
            case 3 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:22: T__16
                {
                mT__16(); 

                }
                break;
            case 4 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:28: T__17
                {
                mT__17(); 

                }
                break;
            case 5 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:34: T__18
                {
                mT__18(); 

                }
                break;
            case 6 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:40: T__19
                {
                mT__19(); 

                }
                break;
            case 7 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:46: T__20
                {
                mT__20(); 

                }
                break;
            case 8 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:52: T__21
                {
                mT__21(); 

                }
                break;
            case 9 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:58: T__22
                {
                mT__22(); 

                }
                break;
            case 10 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:64: T__23
                {
                mT__23(); 

                }
                break;
            case 11 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:70: T__24
                {
                mT__24(); 

                }
                break;
            case 12 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:76: T__25
                {
                mT__25(); 

                }
                break;
            case 13 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:82: T__26
                {
                mT__26(); 

                }
                break;
            case 14 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:88: T__27
                {
                mT__27(); 

                }
                break;
            case 15 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:94: T__28
                {
                mT__28(); 

                }
                break;
            case 16 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:100: T__29
                {
                mT__29(); 

                }
                break;
            case 17 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:106: T__30
                {
                mT__30(); 

                }
                break;
            case 18 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:112: T__31
                {
                mT__31(); 

                }
                break;
            case 19 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:118: T__32
                {
                mT__32(); 

                }
                break;
            case 20 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:124: T__33
                {
                mT__33(); 

                }
                break;
            case 21 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:130: T__34
                {
                mT__34(); 

                }
                break;
            case 22 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:136: T__35
                {
                mT__35(); 

                }
                break;
            case 23 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:142: T__36
                {
                mT__36(); 

                }
                break;
            case 24 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:148: T__37
                {
                mT__37(); 

                }
                break;
            case 25 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:154: T__38
                {
                mT__38(); 

                }
                break;
            case 26 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:160: T__39
                {
                mT__39(); 

                }
                break;
            case 27 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:166: T__40
                {
                mT__40(); 

                }
                break;
            case 28 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:172: T__41
                {
                mT__41(); 

                }
                break;
            case 29 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:178: T__42
                {
                mT__42(); 

                }
                break;
            case 30 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:184: T__43
                {
                mT__43(); 

                }
                break;
            case 31 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:190: T__44
                {
                mT__44(); 

                }
                break;
            case 32 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:196: T__45
                {
                mT__45(); 

                }
                break;
            case 33 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:202: T__46
                {
                mT__46(); 

                }
                break;
            case 34 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:208: T__47
                {
                mT__47(); 

                }
                break;
            case 35 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:214: T__48
                {
                mT__48(); 

                }
                break;
            case 36 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:220: T__49
                {
                mT__49(); 

                }
                break;
            case 37 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:226: T__50
                {
                mT__50(); 

                }
                break;
            case 38 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:232: T__51
                {
                mT__51(); 

                }
                break;
            case 39 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:238: T__52
                {
                mT__52(); 

                }
                break;
            case 40 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:244: T__53
                {
                mT__53(); 

                }
                break;
            case 41 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:250: T__54
                {
                mT__54(); 

                }
                break;
            case 42 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:256: T__55
                {
                mT__55(); 

                }
                break;
            case 43 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:262: T__56
                {
                mT__56(); 

                }
                break;
            case 44 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:268: T__57
                {
                mT__57(); 

                }
                break;
            case 45 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:274: T__58
                {
                mT__58(); 

                }
                break;
            case 46 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:280: T__59
                {
                mT__59(); 

                }
                break;
            case 47 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:286: T__60
                {
                mT__60(); 

                }
                break;
            case 48 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:292: T__61
                {
                mT__61(); 

                }
                break;
            case 49 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:298: T__62
                {
                mT__62(); 

                }
                break;
            case 50 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:304: T__63
                {
                mT__63(); 

                }
                break;
            case 51 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:310: T__64
                {
                mT__64(); 

                }
                break;
            case 52 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:316: T__65
                {
                mT__65(); 

                }
                break;
            case 53 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:322: T__66
                {
                mT__66(); 

                }
                break;
            case 54 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:328: T__67
                {
                mT__67(); 

                }
                break;
            case 55 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:334: T__68
                {
                mT__68(); 

                }
                break;
            case 56 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:340: T__69
                {
                mT__69(); 

                }
                break;
            case 57 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:346: T__70
                {
                mT__70(); 

                }
                break;
            case 58 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:352: T__71
                {
                mT__71(); 

                }
                break;
            case 59 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:358: RULE_LUS_REAL
                {
                mRULE_LUS_REAL(); 

                }
                break;
            case 60 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:372: RULE_LUS_DOUBLE
                {
                mRULE_LUS_DOUBLE(); 

                }
                break;
            case 61 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:388: RULE_LUS_BOOLEAN
                {
                mRULE_LUS_BOOLEAN(); 

                }
                break;
            case 62 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:405: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 63 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:421: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 64 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:429: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 65 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:438: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 66 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:450: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 67 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:466: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 68 :
                // ../geneauto.xtext.lustre.ui/src-gen/geneauto/xtext/ui/contentassist/antlr/internal/InternalLustre.g:1:474: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA6 dfa6 = new DFA6(this);
    protected DFA18 dfa18 = new DFA18(this);
    static final String DFA2_eotS =
        "\3\uffff\1\4\2\uffff";
    static final String DFA2_eofS =
        "\6\uffff";
    static final String DFA2_minS =
        "\2\56\1\uffff\1\60\2\uffff";
    static final String DFA2_maxS =
        "\2\71\1\uffff\1\71\2\uffff";
    static final String DFA2_acceptS =
        "\2\uffff\1\2\1\uffff\1\1\1\3";
    static final String DFA2_specialS =
        "\6\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\2\1\uffff\12\1",
            "\1\3\1\uffff\12\1",
            "",
            "\12\5",
            "",
            ""
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "12046:22: ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT )";
        }
    }
    static final String DFA6_eotS =
        "\6\uffff";
    static final String DFA6_eofS =
        "\6\uffff";
    static final String DFA6_minS =
        "\2\56\1\uffff\1\60\2\uffff";
    static final String DFA6_maxS =
        "\2\71\1\uffff\1\144\2\uffff";
    static final String DFA6_acceptS =
        "\2\uffff\1\2\1\uffff\1\1\1\3";
    static final String DFA6_specialS =
        "\6\uffff}>";
    static final String[] DFA6_transitionS = {
            "\1\2\1\uffff\12\1",
            "\1\3\1\uffff\12\1",
            "",
            "\12\5\52\uffff\1\4",
            "",
            ""
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "12048:24: ( RULE_INT '.' | '.' RULE_INT | RULE_INT '.' RULE_INT )";
        }
    }
    static final String DFA18_eotS =
        "\1\uffff\1\57\1\64\1\66\2\64\1\uffff\1\72\13\64\1\116\1\120\2\uffff"+
        "\1\124\3\64\1\54\2\uffff\4\64\4\uffff\1\147\1\150\1\uffff\2\54\7"+
        "\uffff\2\64\3\uffff\2\64\2\uffff\1\167\11\64\1\u0084\1\64\1\u0086"+
        "\4\64\11\uffff\4\64\3\uffff\7\64\6\uffff\1\167\1\150\6\uffff\1\u0097"+
        "\1\64\1\u0099\1\u009a\2\64\2\uffff\13\64\1\uffff\1\u00a8\1\uffff"+
        "\2\64\1\u00ab\1\u00ac\1\u00ad\1\u00ae\2\64\1\u00b1\1\u00b2\2\64"+
        "\1\u00b5\2\64\1\167\1\uffff\1\64\2\uffff\1\u00b9\1\u00ba\3\64\1"+
        "\u00be\1\u00bf\1\64\1\u00c1\2\64\1\u00c4\1\64\1\uffff\1\u00c6\1"+
        "\64\4\uffff\2\64\2\uffff\1\u00ca\1\u00cb\1\uffff\3\64\2\uffff\1"+
        "\u00cf\2\64\2\uffff\1\64\1\uffff\2\64\1\uffff\1\64\1\uffff\2\64"+
        "\1\u00d8\2\uffff\1\64\1\u00cb\1\u00da\1\uffff\1\u00db\5\64\1\u00e1"+
        "\1\64\1\uffff\1\64\2\uffff\2\64\1\u00e6\1\u00e7\1\64\1\uffff\1\u00e9"+
        "\2\64\1\u00ec\2\uffff\1\u00ed\1\uffff\1\u00ee\1\u00ef\4\uffff";
    static final String DFA18_eofS =
        "\u00f0\uffff";
    static final String DFA18_minS =
        "\1\0\1\55\1\156\1\76\1\157\1\150\1\uffff\1\60\1\114\1\111\2\101"+
        "\1\145\1\154\1\142\1\146\1\157\1\151\1\157\2\75\2\uffff\1\52\1\157"+
        "\1\162\2\157\2\uffff\2\145\2\141\4\uffff\1\101\1\56\1\uffff\2\0"+
        "\3\uffff\1\0\1\uffff\1\56\1\60\1\144\1\163\3\uffff\1\144\1\145\2"+
        "\uffff\1\60\1\117\1\107\2\122\1\111\1\141\3\163\1\60\1\164\1\60"+
        "\1\157\1\165\1\166\1\162\11\uffff\1\144\1\145\1\162\1\156\3\uffff"+
        "\1\164\1\154\1\145\1\165\1\162\1\156\1\154\6\uffff\1\60\1\56\6\uffff"+
        "\1\60\1\145\2\60\1\145\1\156\2\uffff\1\103\1\116\1\101\1\124\1\116"+
        "\1\165\1\154\2\165\2\145\1\uffff\1\60\1\uffff\1\154\1\142\4\60\1"+
        "\162\1\163\2\60\1\156\1\145\1\60\1\143\1\163\1\60\1\uffff\1\162"+
        "\2\uffff\2\60\1\113\1\101\1\115\2\60\1\151\1\60\2\162\1\60\1\162"+
        "\1\uffff\1\60\1\154\4\uffff\1\145\1\164\2\uffff\2\60\1\uffff\1\164"+
        "\1\145\1\164\2\uffff\1\60\1\114\1\105\2\uffff\1\162\1\uffff\1\156"+
        "\1\145\1\uffff\1\166\1\uffff\1\145\1\156\1\60\2\uffff\1\151\2\60"+
        "\1\uffff\1\60\1\124\1\145\2\163\1\145\1\60\1\164\1\uffff\1\157\2"+
        "\uffff\1\105\1\163\2\60\1\162\1\uffff\1\60\1\156\1\122\1\60\2\uffff"+
        "\1\60\1\uffff\2\60\4\uffff";
    static final String DFA18_maxS =
        "\1\uffff\1\76\1\163\1\76\1\157\1\150\1\uffff\1\71\1\114\1\111\1"+
        "\117\1\101\1\145\1\156\1\162\1\156\3\157\1\76\1\75\2\uffff\1\52"+
        "\1\157\1\162\1\165\1\157\2\uffff\1\145\1\162\1\141\1\165\4\uffff"+
        "\1\172\1\71\1\uffff\2\uffff\3\uffff\1\uffff\1\uffff\2\71\1\144\1"+
        "\163\3\uffff\1\164\1\145\2\uffff\1\144\1\117\1\107\2\122\1\111\1"+
        "\164\3\163\1\172\1\164\1\172\1\157\1\165\1\166\1\162\11\uffff\1"+
        "\144\1\145\1\162\1\156\3\uffff\1\164\1\154\1\145\1\165\1\162\1\156"+
        "\1\154\6\uffff\1\144\1\71\6\uffff\1\172\1\145\2\172\1\145\1\156"+
        "\2\uffff\1\103\1\116\1\101\1\124\1\116\1\165\1\154\2\165\2\145\1"+
        "\uffff\1\172\1\uffff\1\154\1\142\4\172\1\162\1\163\2\172\1\156\1"+
        "\145\1\172\1\143\1\163\1\144\1\uffff\1\162\2\uffff\2\172\1\113\1"+
        "\101\1\115\2\172\1\151\1\172\2\162\1\172\1\162\1\uffff\1\172\1\154"+
        "\4\uffff\1\145\1\164\2\uffff\2\172\1\uffff\1\164\1\145\1\164\2\uffff"+
        "\1\172\1\114\1\105\2\uffff\1\162\1\uffff\1\156\1\145\1\uffff\1\166"+
        "\1\uffff\1\145\1\156\1\172\2\uffff\1\151\2\172\1\uffff\1\172\1\124"+
        "\1\145\2\163\1\145\1\172\1\164\1\uffff\1\157\2\uffff\1\105\1\163"+
        "\2\172\1\162\1\uffff\1\172\1\156\1\122\1\172\2\uffff\1\172\1\uffff"+
        "\2\172\4\uffff";
    static final String DFA18_acceptS =
        "\6\uffff\1\7\16\uffff\1\35\1\37\5\uffff\1\47\1\50\4\uffff\1\61\1"+
        "\64\1\65\1\66\2\uffff\1\77\2\uffff\1\103\1\104\1\1\1\uffff\1\36"+
        "\4\uffff\1\77\1\3\1\27\2\uffff\1\7\1\10\21\uffff\1\30\1\31\1\32"+
        "\1\33\1\34\1\35\1\37\1\102\1\40\4\uffff\1\45\1\47\1\50\7\uffff\1"+
        "\61\1\64\1\65\1\66\1\67\1\100\2\uffff\1\101\1\103\1\57\1\60\1\62"+
        "\1\76\6\uffff\1\73\1\74\13\uffff\1\25\1\uffff\1\70\20\uffff\1\2"+
        "\1\uffff\1\4\1\6\15\uffff\1\21\2\uffff\1\41\1\26\1\42\1\43\2\uffff"+
        "\1\52\1\53\2\uffff\1\54\3\uffff\1\46\1\5\3\uffff\1\14\1\15\1\uffff"+
        "\1\22\2\uffff\1\72\1\uffff\1\23\3\uffff\1\71\1\75\3\uffff\1\11\10"+
        "\uffff\1\56\1\uffff\1\63\1\12\5\uffff\1\24\4\uffff\1\51\1\17\1\uffff"+
        "\1\44\2\uffff\1\16\1\20\1\55\1\13";
    static final String DFA18_specialS =
        "\1\2\50\uffff\1\1\1\0\3\uffff\1\3\u00c1\uffff}>";
    static final String[] DFA18_transitionS = {
            "\11\54\2\53\2\54\1\53\22\54\1\53\1\54\1\51\1\33\3\54\1\52\1"+
            "\34\1\35\1\26\1\25\1\43\1\1\1\7\1\27\12\47\1\42\1\6\1\23\1\3"+
            "\1\24\2\54\1\50\1\10\12\50\1\13\2\50\1\12\2\50\1\11\7\50\1\44"+
            "\1\54\1\45\1\46\1\50\1\54\1\2\1\20\1\32\1\21\1\15\1\41\2\50"+
            "\1\17\2\50\1\36\1\30\1\4\1\16\1\31\1\50\1\14\1\50\1\37\1\50"+
            "\1\40\1\5\1\22\2\50\uff85\54",
            "\1\56\1\61\1\uffff\12\60\4\uffff\1\55",
            "\1\62\4\uffff\1\63",
            "\1\65",
            "\1\67",
            "\1\70",
            "",
            "\12\73",
            "\1\74",
            "\1\75",
            "\1\76\15\uffff\1\77",
            "\1\100",
            "\1\101",
            "\1\103\1\uffff\1\102",
            "\1\104\17\uffff\1\105",
            "\1\107\7\uffff\1\106",
            "\1\110",
            "\1\112\5\uffff\1\111",
            "\1\113",
            "\1\115\1\114",
            "\1\117",
            "",
            "",
            "\1\123",
            "\1\125",
            "\1\126",
            "\1\130\5\uffff\1\127",
            "\1\131",
            "",
            "",
            "\1\134",
            "\1\135\2\uffff\1\136\11\uffff\1\137",
            "\1\140",
            "\1\142\23\uffff\1\141",
            "",
            "",
            "",
            "",
            "\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\151\1\uffff\12\152",
            "",
            "\0\153",
            "\0\153",
            "",
            "",
            "",
            "\41\160\1\156\36\160\1\155\23\160\1\157\uffab\160",
            "",
            "\1\151\1\uffff\12\60",
            "\12\73",
            "\1\161",
            "\1\162",
            "",
            "",
            "",
            "\1\165\15\uffff\1\164\1\uffff\1\163",
            "\1\166",
            "",
            "",
            "\12\73\52\uffff\1\170",
            "\1\171",
            "\1\172",
            "\1\173",
            "\1\174",
            "\1\175",
            "\1\177\17\uffff\1\176\2\uffff\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u0085",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u008b",
            "\1\u008c",
            "\1\u008d",
            "\1\u008e",
            "",
            "",
            "",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\u0096\52\uffff\1\170",
            "\1\151\1\uffff\12\152",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u0098",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u009b",
            "\1\u009c",
            "",
            "",
            "\1\u009d",
            "\1\u009e",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "\1\u00a9",
            "\1\u00aa",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00af",
            "\1\u00b0",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00b3",
            "\1\u00b4",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00b6",
            "\1\u00b7",
            "\12\u0096\52\uffff\1\170",
            "",
            "\1\u00b8",
            "",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00c0",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00c2",
            "\1\u00c3",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00c5",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00c7",
            "",
            "",
            "",
            "",
            "\1\u00c8",
            "\1\u00c9",
            "",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce",
            "",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00d0",
            "\1\u00d1",
            "",
            "",
            "\1\u00d2",
            "",
            "\1\u00d3",
            "\1\u00d4",
            "",
            "\1\u00d5",
            "",
            "\1\u00d6",
            "\1\u00d7",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "",
            "\1\u00d9",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00dc",
            "\1\u00dd",
            "\1\u00de",
            "\1\u00df",
            "\1\u00e0",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00e2",
            "",
            "\1\u00e3",
            "",
            "",
            "\1\u00e4",
            "\1\u00e5",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00e8",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\1\u00ea",
            "\1\u00eb",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "\12\64\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA18_eot = DFA.unpackEncodedString(DFA18_eotS);
    static final short[] DFA18_eof = DFA.unpackEncodedString(DFA18_eofS);
    static final char[] DFA18_min = DFA.unpackEncodedStringToUnsignedChars(DFA18_minS);
    static final char[] DFA18_max = DFA.unpackEncodedStringToUnsignedChars(DFA18_maxS);
    static final short[] DFA18_accept = DFA.unpackEncodedString(DFA18_acceptS);
    static final short[] DFA18_special = DFA.unpackEncodedString(DFA18_specialS);
    static final short[][] DFA18_transition;

    static {
        int numStates = DFA18_transitionS.length;
        DFA18_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA18_transition[i] = DFA.unpackEncodedString(DFA18_transitionS[i]);
        }
    }

    class DFA18 extends DFA {

        public DFA18(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = DFA18_eot;
            this.eof = DFA18_eof;
            this.min = DFA18_min;
            this.max = DFA18_max;
            this.accept = DFA18_accept;
            this.special = DFA18_special;
            this.transition = DFA18_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | RULE_LUS_REAL | RULE_LUS_DOUBLE | RULE_LUS_BOOLEAN | RULE_SL_COMMENT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA18_42 = input.LA(1);

                        s = -1;
                        if ( ((LA18_42>='\u0000' && LA18_42<='\uFFFF')) ) {s = 107;}

                        else s = 44;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA18_41 = input.LA(1);

                        s = -1;
                        if ( ((LA18_41>='\u0000' && LA18_41<='\uFFFF')) ) {s = 107;}

                        else s = 44;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA18_0 = input.LA(1);

                        s = -1;
                        if ( (LA18_0=='-') ) {s = 1;}

                        else if ( (LA18_0=='a') ) {s = 2;}

                        else if ( (LA18_0=='=') ) {s = 3;}

                        else if ( (LA18_0=='n') ) {s = 4;}

                        else if ( (LA18_0=='w') ) {s = 5;}

                        else if ( (LA18_0==';') ) {s = 6;}

                        else if ( (LA18_0=='.') ) {s = 7;}

                        else if ( (LA18_0=='B') ) {s = 8;}

                        else if ( (LA18_0=='S') ) {s = 9;}

                        else if ( (LA18_0=='P') ) {s = 10;}

                        else if ( (LA18_0=='M') ) {s = 11;}

                        else if ( (LA18_0=='r') ) {s = 12;}

                        else if ( (LA18_0=='e') ) {s = 13;}

                        else if ( (LA18_0=='o') ) {s = 14;}

                        else if ( (LA18_0=='i') ) {s = 15;}

                        else if ( (LA18_0=='b') ) {s = 16;}

                        else if ( (LA18_0=='d') ) {s = 17;}

                        else if ( (LA18_0=='x') ) {s = 18;}

                        else if ( (LA18_0=='<') ) {s = 19;}

                        else if ( (LA18_0=='>') ) {s = 20;}

                        else if ( (LA18_0=='+') ) {s = 21;}

                        else if ( (LA18_0=='*') ) {s = 22;}

                        else if ( (LA18_0=='/') ) {s = 23;}

                        else if ( (LA18_0=='m') ) {s = 24;}

                        else if ( (LA18_0=='p') ) {s = 25;}

                        else if ( (LA18_0=='c') ) {s = 26;}

                        else if ( (LA18_0=='#') ) {s = 27;}

                        else if ( (LA18_0=='(') ) {s = 28;}

                        else if ( (LA18_0==')') ) {s = 29;}

                        else if ( (LA18_0=='l') ) {s = 30;}

                        else if ( (LA18_0=='t') ) {s = 31;}

                        else if ( (LA18_0=='v') ) {s = 32;}

                        else if ( (LA18_0=='f') ) {s = 33;}

                        else if ( (LA18_0==':') ) {s = 34;}

                        else if ( (LA18_0==',') ) {s = 35;}

                        else if ( (LA18_0=='[') ) {s = 36;}

                        else if ( (LA18_0==']') ) {s = 37;}

                        else if ( (LA18_0=='^') ) {s = 38;}

                        else if ( ((LA18_0>='0' && LA18_0<='9')) ) {s = 39;}

                        else if ( (LA18_0=='A'||(LA18_0>='C' && LA18_0<='L')||(LA18_0>='N' && LA18_0<='O')||(LA18_0>='Q' && LA18_0<='R')||(LA18_0>='T' && LA18_0<='Z')||LA18_0=='_'||(LA18_0>='g' && LA18_0<='h')||(LA18_0>='j' && LA18_0<='k')||LA18_0=='q'||LA18_0=='s'||LA18_0=='u'||(LA18_0>='y' && LA18_0<='z')) ) {s = 40;}

                        else if ( (LA18_0=='\"') ) {s = 41;}

                        else if ( (LA18_0=='\'') ) {s = 42;}

                        else if ( ((LA18_0>='\t' && LA18_0<='\n')||LA18_0=='\r'||LA18_0==' ') ) {s = 43;}

                        else if ( ((LA18_0>='\u0000' && LA18_0<='\b')||(LA18_0>='\u000B' && LA18_0<='\f')||(LA18_0>='\u000E' && LA18_0<='\u001F')||LA18_0=='!'||(LA18_0>='$' && LA18_0<='&')||(LA18_0>='?' && LA18_0<='@')||LA18_0=='\\'||LA18_0=='`'||(LA18_0>='{' && LA18_0<='\uFFFF')) ) {s = 44;}

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA18_46 = input.LA(1);

                        s = -1;
                        if ( (LA18_46=='@') ) {s = 109;}

                        else if ( (LA18_46=='!') ) {s = 110;}

                        else if ( (LA18_46=='T') ) {s = 111;}

                        else if ( ((LA18_46>='\u0000' && LA18_46<=' ')||(LA18_46>='\"' && LA18_46<='?')||(LA18_46>='A' && LA18_46<='S')||(LA18_46>='U' && LA18_46<='\uFFFF')) ) {s = 112;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 18, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}