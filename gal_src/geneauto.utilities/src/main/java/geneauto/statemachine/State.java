/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/statemachine/State.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2011-07-07 12:23:09 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.statemachine;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;


/**
 * Abstract base class for all states used in elementary tool state machines
 * Each elementary tool will implement:
 * 	- one tool specific subclass of this base class implementing getMachine method
 * 		typically named as <ToolName>State
 *  - one static subclass of <ToolName>State for each elementary tool state
 *  	implementing stateEntry, stateExecute and stateExit (and optionally
 *  	any state-specific functionality as private functions)
 *  
 *
 */
public abstract class State {

	/**
	 * name of the state to be used in messages
	 */
	protected String stateName = "";
	
	/**
	 * This method is called each time the state machine changes its current
	 * state. It initialises the current state, if necessary and automatically
	 * launches the method stateExecute.
	 */
	public void stateEntry(){
		EventHandler.handle(EventLevel.INFO, "", 
				"GI0005",
				"Entering state " + stateName, "");
		
		// start timer for current state
        EventHandler.startTimer("state");
	}

	/**
	 * This method is the processing core of the current state. 
	 */
	public abstract void stateExecute();

	/**
	 * This method is called each time the current state ends.
	 *
	 */
	public void stateExit(){
		EventHandler.handle(EventLevel.INFO, "", 
				"GI0007",
				"Time in state: " 
				+ EventHandler.getDuration("state", false)
				+ "(state " + stateName+ ")",
				"");
	}

    /**
     * returns reference to the instance of stateMachine
     * this abstract method shall be hidden by each elementary tool
     * by implementing a method with return type as concrete subclass
     * of StateMachine
     * 
     * @return StateMachine stateMachine
     */
    public abstract StateMachine getMachine();

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String p_stateName) {
		this.stateName = p_stateName;
	}

	public State(String stateName) {
		super();
		this.stateName = stateName;
	}
}