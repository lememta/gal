/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/StringUtils.java,v $
 *  @version	$Revision: 1.20 $
 *	@date		$Date: 2011-09-07 12:09:28 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class contains static methods for String handling.
 * 
 * 
 */
public class StringUtils {

    /**
     * Remove all string formatting if string is composed of several lines
     * 
     * NOTE (AnTo): This method removes also escaped quotes (\") from inside the
     * string. Escaped should actually be left in place. See cleanMultilineString
     * that is more appropriate for complex strings. 
     * 
     * @param s
     *            - String to transform
     * @return transformed String
     */
    public final static String baseClean(String s) {
        // If the input String is null or not a string do nothing and return
        if (s == null || !s.contains("\"")) {
            return s;
        } else {
            // Remove everything before the first quotes
            s = s.substring(s.indexOf('\"') + 1);
            // Remove everything after the last quotes
            s = s.substring(0, s.lastIndexOf('\"'));
            // Remove everything between the strings
            // While the string contains '"'
            while (s.contains("\"")) {
                // Remember the index of the first '"' character
                int i = s.indexOf('\"');
                // Removes this character from the String
                s = s.substring(0, i) + s.substring(i + 1);
                // Remove everything between the marked position and next quotes
                s = s.substring(0, i) + s.substring(s.indexOf('\"') + 1);
            }
            // Returns the transformed String
            return s;
        }
    }

	/**
	 * Perform string cleanup that is not done by processing the grammar:  
	 * 		- removes starting and ending whitespace
	 *      - removes starting and ending quotes
	 *      - unescapes escaped quotes
	 *      - removes newlines and inter-line section whitespace.
	 * 
	 * @param s
	 *            - String to transform
	 * @return transformed String
	 */
    public final static String clean(String s) {
    	if (s == null) {
    		return null;
    	}
    	
    	// remove spaces from start and end 
    	s = s.trim();

    	// remove newlines and inter-line section whitespace
        String[] sections = s.split("(\\r|\\n|\\r\\n)\\s*");
        String strPattern = "\"(.*)\"";
        Pattern p = Pattern.compile(strPattern);        
        String result = "";
        for (String line : sections) {
            line = line.trim();
            if (!line.isEmpty()) {
                Matcher m = p.matcher(line);
                if (m.matches()) {
                    result += m.group(1);
                } else {
                    result += line;
                }                
            }
        }
        
        // removes starting and ending quotes
        if (result != null && result.startsWith("\"") && result.endsWith("\"")) {
        	result = result.substring(1, result.length()-1);
        }            
        
        // Unescape quotes
        result = result.replaceAll("\\\\\"", "\"");
        
        return result;

    }

    /**
     * Cleans a multi-line string. Multi-line strings are strings of form
     * "..."\n "...."\n etc. Where newlines are not newline characters, but
     * backslash, with newline. Removes all quotes that mark string sections and
     * unescapes escaped quotes.
     * 
     * @param s
     *            - String to transform
     * @return transformed String
     */
    public static String cleanMultilineString(String s) {
    	if (s == null) {
    		return null;
    	}
    	
    	// remove spaces (the grammar leaves trailing spaces in string)
    	s = s.trim();
    	
        String[] lines = s.split("\\n");
        String strPattern = "\"(.*)\"";
        Pattern p = Pattern.compile(strPattern);        
        String result = "";
        for (String line : lines) {
            line = line.trim();
            if (!line.isEmpty()) {
                Matcher m = p.matcher(line);
                if (m.matches()) {
                    result += m.group(1);
                    
                } else {
                    EventHandler.handle(EventLevel.ERROR, "cleanMultilineString", "",
                            "String doesn't match expected pattern: " + s);
                    return null;
                }                
            }
        }
        result = result.replaceAll("\\\\\"", "\"");
        return result;
    }
     
    /**
     * Split the input String into a String list, using the input char as
     * separator for the split
     * 
     * @param input,
     *            the input String
     * @param separator,
     *            the char used for the splitting
     * @return a String list containing the result of the splitting
     */
    public static List<String> splitString(String input, char separator) {
        // Declares a String list
        List<String> outputs = new ArrayList<String>();
        // Gets the index of the first separator occurrence
        int index = input.indexOf(separator);
        // While there is still one separator occurrence at least
        while (index != -1) {
            // Add the sub-String before the separator to the String list
            outputs.add(input.substring(0, index));
            // Updates the input String
            input = input.substring(index + 1, input.length());
            // Gets the index of the next separator occurrence
            index = input.indexOf(separator);
        }
        // Adds the remaining String to the String list
        outputs.add(input);
        return outputs;
    }

}
