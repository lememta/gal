/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/union/Either.java,v $
 *  @version	$Revision: 1.3 $
 *	@date		$Date: 2011-07-07 12:23:09 $
 *
 *  Copyright (c) 2006-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils.union;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

/**
 * A disjoint union. Contains an element of two possible types (same or
 * different), but it is always known which one it belongs to: the left type or
 * the right type. Provides a type-safe way to deal simultaneously with elements
 * of different types or elements of same type, but having some different
 * meaning. Null values are not allowed.
 * 
 * @param <T1> First possible type (left type)
 * @param <T2> Second possible type (right type)
 */
public class Either<T1, T2> {
    
    private T1 left;
    private T2 right;

	/**
	 * Constructor of a disjoint union. Exactly one value allowed: either the
	 * left or right one must be non-null and the other one null.
	 * 
	 * @param left
	 * @param right
	 */
    public Either(T1 left, T2 right) {
    	if (left == null) {
    		if (right != null) {
    			this.right = right;
    		} else {
    			// No value
    			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getCanonicalName(), "",
    				"\nCannot construct an element with null value. " +
    				"Either the left or right argument must be non-null.");
    			return;
    		}    		
    	} else if (right == null) {
    		this.left = left;
    	} else {
    		// Two values
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getCanonicalName(), "",
				"\nCannot construct an element with two values. " +
				"Either the left or right argument must be null.");
			return;
    	}
    }

    /**
	 * @return true, if the element is of the left type
     */
	public boolean isLeft() {
		return left != null;
	}
	
    /**
	 * @return true, if the element is of the right type
     */
	public boolean isRight() {
		return right != null;
	}
	
    /**
	 * @return if the element is of the left type, then return the left value.
	 *         Otherwise raise an error.
     */
	public T1 getLeft() {
		if (left == null) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getCanonicalName() + ".getLeft()", "",
				"\nWrong type accessed! The element is of the \"right\" type: " +
				"\n " + right.getClass().getCanonicalName());
		}
		return left;
	}

	/**
	 * @return if the element is of the right type, then return the right value.
	 *         Otherwise raise an error.
	 */
	public T2 getRight() {
		if (right == null) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getCanonicalName() + ".getRight()", "",
				"\nWrong type accessed! The element is of the \"left\" type: " +
				"\n " + left.getClass().getCanonicalName());
		}
		return right;
	}

    @Override
    public String toString() {
    	if (left != null) {
    		return "<Left: " + left + ">";
    	} else {
    		return "<Right: " + right + ">";
    	}
    }	

}
