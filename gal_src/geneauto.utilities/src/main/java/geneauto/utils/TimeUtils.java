package geneauto.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility methods for time/date manipulation
 */
public class TimeUtils {

    /**
     * Method getFormattedDate.
     * 
     * @return The current time according to the template :<br>
     *         <p>
     *         yyyy-mm-dd hh:mm:ss
     */
	public static String getFormattedDate() {
        return getFormattedDate(new Date());
    }

    /**
     * Method getFormattedDate(Date dt).
     * 
     * @param dt Date
     * @return The supplied date time according to the template :<br>
     *         <p>
     *         yyyy-mm-dd hh:mm:ss
     */
	public static String getFormattedDate(Date dt) {
        SimpleDateFormat format =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String parsed = format.format(dt);
        return parsed;
    }

	/**
	 * Returns a String containing the current date in respect with the format:
	 * yyyy-mm-dd - hh:mm:ss:msmsms
	 */
	public static String getFormattedDate(String precision) {
		SimpleDateFormat format = new SimpleDateFormat(precision);
		String parsed = format.format(new Date());
		return parsed;
	}	

}
