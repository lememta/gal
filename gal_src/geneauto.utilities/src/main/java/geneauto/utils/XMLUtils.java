package geneauto.utils;

import org.w3c.dom.Node;

public class XMLUtils {
    
    public static String getPathString(Node node) {
        if (node == null) {
            return null;
        } else {
            String s = "";
            Node parent = node.getParentNode();
            if (parent != null) {
               s += getPathString(parent) + "."; 
            }
            s += node.getNodeName();
            return s;
        }
    }

}
