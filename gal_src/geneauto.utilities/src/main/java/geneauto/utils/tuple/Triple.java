/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/tuple/Triple.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2010-04-01 05:24:37 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils.tuple;

/**
 * 
 * A structure containing three elements
 * 
 *
 * @param <T1> Type of the left element
 * @param <T2> Type of the middle element
 * @param <T3> Type of the right element
 */
public class Triple<T1, T2, T3> {
    
    private T1 left;
    private T2 middle;
    private T3 right;
    
    public T1 getLeft() {
		return left;
	}

	public void setLeft(T1 left) {
		this.left = left;
	}

	public T2 getMiddle() {
		return middle;
	}

	public void setMiddle(T2 middle) {
		this.middle = middle;
	}

	public T3 getRight() {
		return right;
	}

	public void setRight(T3 right) {
		this.right = right;
	}

	public Triple(T1 left, T2 middle, T3 right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

}
