/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/general/NameHandler.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:09 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils.general;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.util.Collection;
import java.util.HashSet;

/**
 * A class that contains a collection of names and allows creating new names
 * that are guaranteed to be unique.
 */
public class NameHandler implements NameCollection {
    
    public NameHandler() {
        
    }

    public NameHandler(Collection<String> names) {
        this.names.addAll(names);
    }

    public NameHandler(NameCollection nc) {
        this.names.addAll(nc.getAllNames());
    }

    private HashSet<String> names = new HashSet<String>();

    public Collection<String> getAllNames() {
        return names;
    }

    /**
     * Receives some name and returns the same name, if it does not exist in the
     * internal name collection. Otherwise, creates a new name based on this
     * name, by adding a suffix _<i> (i is in interval 1 to MAX_VALUE). The
     * supplied name is added to the internal name collection.
     * 
     * @param baseName
     *            requested name, e.g. "i"
     * @return a guaranteed unique name, e.g. "i_2"
     */
    public String getUniqueName(String baseName) {
        String newName = null;
        if (!names.contains(baseName)) {
            newName = baseName;
        } else {
            // baseName is not unique name
            for (int i = 1; i < Integer.MAX_VALUE; i++) {
                newName = baseName + "_" + i;
                if (!names.contains(newName)) {
                    break;
                }
                if (i == Integer.MAX_VALUE) {
                    EventHandler.handle(EventLevel.ERROR, getClass()
                            .getSimpleName()
                            + ".getUniqueName", "",
                            "Cannot compute an unique name for base name: "
                                    + baseName
                                    + ". Maximum iteration count exceeded.");
                    return null;
                }
            }
        }
        names.add(newName);
        return newName;
    }

    /**
     * Resets the internal name collection
     */
    public void reset() {
        names.clear();
    }

}
