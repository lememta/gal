/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/GASysInfo.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2011-05-04 13:45:15 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils;
import java.io.File;

/**
 * Utility methods related to the program itself.
 */
public class GASysInfo {

	/**
	 * After initialisation (e.g. by the launcher) contains the absolute path of
	 * the binary installation directory of the current program or null, when
	 * the program is run from source.
	 */
	private static String binInstallPath;

	/**
	 * Initialises the binInstallPath field
	 * 
	 * @param launcherPackage
	 *            Package of the launcher. This must be part of the name of the
	 *            jar (generally, also of the the source folder) in the
	 *            ClassPath. It is Required to determine the location of the
	 *            binary.
	 */
	public static void initBinInstallPath(String launcherPackage) {
		
        // handle for temporary file objects
        File tmpF;
        // location of the project or .jar file from ClassPath
        String lClassPath = new File(System.getProperty("java.class.path"))
                .getAbsolutePath();

        // locate the path containing the launcher jar
        String[] splittedClassPath = lClassPath.split(File.pathSeparator);
        // if we will not find the launcher in the ClassPath, 
        // then the location must be the active directory
        lClassPath = ".";
        
		// Create pattern for launcherPackage
        // Make the pattern case-insensitive and escape the dots in the name
        String launcherPat = "(?i).*" + launcherPackage.replaceAll("\\.", "\\\\.") + ".*";

        for (int i = 0; i < splittedClassPath.length; i++) {
            if (splittedClassPath[i].matches(launcherPat)) {
                lClassPath = splittedClassPath[i];
                break;
            }
        }

        // binary distribution
        if (lClassPath.endsWith(".jar")) {
            tmpF = new File(lClassPath).getParentFile();
        }
		// source distribution
        else {
        	// the binInstallPath remains unchanged
        	return;
        }

        if (tmpF == null) {
        	binInstallPath = new File("").getAbsolutePath();
        } else {
        	binInstallPath = tmpF.getAbsolutePath();
        }
	}
	
	/**
	 * @return the absolute path of the binary installation directory of the
	 *         current program or null, when the program is run from source.
	 */
    public static String getBinInstallPath() {
        return binInstallPath;
    }
    
	/**
	 * Set the absolute path of the binary installation directory of the current
	 * program. Set path to null, when the program is run from source.
     * @param path
     */
    public static void setBinInstallPath(String path) {
    	binInstallPath = path;
    }
    
    /** @return the directory, where the default BlockLibrary resides */
    public static String getDefBlockLibraryPath() {
    	
        // binary distribution - the file is in the parent directory of the jar file
        if (binInstallPath != null) {
        	return binInstallPath;
        }
        // source distribution -- the file is located relative to the active directory
        else {
            return (new File(GAConst.DEFAULT_BLOCKLIB_DIR)).getAbsolutePath();
        }
    }
}
