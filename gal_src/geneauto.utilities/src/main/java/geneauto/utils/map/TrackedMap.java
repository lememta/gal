/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/map/TrackedMap.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2010-04-01 05:24:37 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils.map;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * A Tracked map. Allows creating and subsequent modifications, but when 
 * a map is modified after creation, then the flag "changed" becomes true.
 * 
 *
 * @param <K> Key 
 * @param <V> Value
 */
public class TrackedMap<K,V> {	
	
	/**
	 * A map that is kept internally
	 */
	private Map<K,V> map;

	/**
	 * Default constructor
	 */
	public TrackedMap() {
		map = new HashMap<K, V>();
	}
	
	/**
	 * True, when the map has been modified after creation.
	 */
	private boolean changed = false;

    /**
     * Constructor with a Map argument
     * All elements from the supplied map are copied to the newly created map.
     * @param map
     */
    public TrackedMap(Map<K, V> map) {
        this(map, true);
    }    
	
    /**
     * Constructor with a Map argument and a copy flag
     * 
     * @param map
     * @param copy
     *            When true, creates a new map object and all the elements from
     *            the supplied map are copied to the newly created map.
     * 
     *            When false, the original map is embedded in the ConstantMap.
     *            The map is not modifiable through the ConstantMap, but it
     *            remains modifiable via the original reference to the supplied
     *            map.
     */
    public TrackedMap(Map<K, V> map, boolean copy) {
        if (copy) {
            this.map = new HashMap<K, V>();
            this.map.putAll(map);            
        }
        else {
            this.map = map;            
        }
    }
    
	/**
	 * Constructor with a ConstantMap argument
	 * @param map
	 */
	public TrackedMap(ConstantMap<K, V> map) {		
		this.map = map.toHashMap();
	}
	
	public V get(K key) {
		return map.get(key);
	}

	public boolean containsKey(K key) {
		return map.containsKey(key);
	}

	public boolean containsValue(V value) {
		return map.containsValue(value);
	}

	public int size() {
		return map.size();
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	public V put(K key, V value) {
		changed = true;
		return map.put(key, value);
	}

	public V remove(K key) {
		changed = true;
		return map.remove(key);
	}

	public void clear() {
		changed = true;
		map.clear();
	}

	public void putAll(Map<? extends K,? extends V> m) {
		changed = true;
		map.putAll(m);
	}

	public boolean isChanged() {
		return changed;
	}

	public HashMap<K,V> toHashMap() {
		return new HashMap<K,V>(map);
	}
	
	public ConstantMap<K,V> toConstantMap() {
		return new ConstantMap<K,V>(map);
	}
	
	/**
	 * @return a copy of the original map, with the "changed" flag false.
	 */
	public TrackedMap<K,V> getCopy() {
		return new TrackedMap<K,V>(map);
	}

	/**
	 * @param map2 map to extend the original map with.
	 * @return an extended map that contains the second map on 
	 * top of the original map
	 */
	public TrackedMap<K,V> getExtendedCopy(TrackedMap<K, V> map2) {
		HashMap<K,V> newMap = toHashMap();
		newMap.putAll(map2.toHashMap());
        /*
         * We don't need to make a copy here, because we know, that there is no
         * usable reference to the map beyond this point
         */
		return new TrackedMap<K,V>(newMap, false);
	}
}
