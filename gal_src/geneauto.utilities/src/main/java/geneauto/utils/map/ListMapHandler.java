/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/map/ListMapHandler.java,v $
 *  @version	$Revision: 1.3 $
 *	@date		$Date: 2011-07-19 12:42:51 $
 *
 *  Copyright (c) 2006-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * Utility class for manipulating maps of kind <Key, List>.
 * 
 * The class can be tuned to use Linked or Array lists in the map entries.
 * LinkedLists are advisable, if the list contents are likely to be changed or
 * large. Otherwise, ArrayLists are simpler and more efficient.
 * 
 * @param <KeyType>
 *            Key type
 * @param <ValueElemType>
 *            Value type of the List (e.g. Integer)
 */
public class ListMapHandler<KeyType, ValueElemType> {	

	/** Reference to the handled map */
	private Map<KeyType,List<ValueElemType>> map;

	public Map<KeyType, List<ValueElemType>> getMap() {
		return map;
	}

	/** Determines whether to use Linked or Array lists */
	private boolean linkedLists = true;

    /**
     * Constructor with a default map (HashMap) and Linked or Array list option
     */
    public ListMapHandler(boolean linkedLists) {
        this.map = new HashMap<KeyType, List<ValueElemType>>();
        this.linkedLists = linkedLists;
    }

    /**
     * Constructor with a map and Linked or Array list option
     */
    public ListMapHandler(Map<KeyType, List<ValueElemType>> map, boolean linkedLists) {
        this.map = map;
        this.linkedLists = linkedLists;
    }

    /**
     * Inserts an entry to the map
     * 
     * @param key
     * @param elem
     */
    public void put(KeyType key, ValueElemType elem) {
		List<ValueElemType> lst = map.get(key);
		if (lst == null) {
			if (linkedLists) {
				lst = new LinkedList<ValueElemType>();
			} else {
				lst = new ArrayList<ValueElemType>();
			}			
			map.put(key, lst);
		}
		lst.add(elem);
    }

    /**
     * Inserts multiple entries to the map
     * 
     * @param key
     * @param elems
     */
    public void putAll(KeyType key, List<ValueElemType> elems) {
		List<ValueElemType> lst = map.get(key);
		if (lst == null) {
			if (linkedLists) {
				lst = new LinkedList<ValueElemType>();
			} else {
				lst = new ArrayList<ValueElemType>();
			}			
			map.put(key, lst);
		}
		lst.addAll(elems);
    }

    /**
     * Returns the entry with the given key
     * 
     * @param key
     * @return
     */
	public List<ValueElemType> get(KeyType key) {
		return map.get(key);
	}

    /**
     * Retrieves the list that corresponds to the given key and 
     * scans the list. Returns the first element that is an instance
     * of the specified class. Returns null, when there is no entry
     * for the given key or the key doesn't contain instances of
     * the supplied class.
     * 
     * @param key
     * @param class
     * @return
     */
    @SuppressWarnings("unchecked")
    public ValueElemType get(KeyType key, Class cls) {
    	List<ValueElemType> list = map.get(key);
    	if (list == null) {
    		return null;
    	}
    	for (ValueElemType v : list) {
            if (cls.isInstance(v)) {
                return v;
            }
        }
        return null;
	}

    /**
     * Remove the entry with the given key
     * 
     * @return the removed element, if any
     */
	public List<ValueElemType> removeKey(KeyType key) {
		return map.remove(key);
	}

    /**
     * Remove the given element from the entry with the given key
     * If the entry has no more values after this, then the whole entry
     * is removed.
     * 
     * @return true, if there was such element
     */
	public boolean removeElement(KeyType key, ValueElemType value) {
		List<ValueElemType> list = map.get(key);
		if (list == null) {
			return false;
		} else {
			boolean existed = list.remove(value);
			if (list.isEmpty()) {
				map.remove(key);
			}
			return existed;
		}
	}

    /**
     * Remove the given elements from the entry with the given key
     * If the entry has no more values after this, then the whole entry
     * is removed.
     * 
     * @return true, if any elements were actually found and removed
     */
	public boolean removeElements(KeyType key, List<ValueElemType> values) {
		List<ValueElemType> list = map.get(key);
		if (list == null) {
			return false;
		} else {
			boolean existed = list.removeAll(values);
			if (list.isEmpty()) {
				map.remove(key);
			}
			return existed;
		}
	}

}
