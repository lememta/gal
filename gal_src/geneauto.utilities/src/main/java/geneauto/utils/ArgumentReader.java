/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/ArgumentReader.java,v $
 *  @version	$Revision: 1.49 $
 *	@date		$Date: 2011-09-27 14:30:35 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Class for parsing arguments of the geneauto tools. In addition to parsing the
 * command line, the ArgumentReader is responsible for resolving the relative
 * paths and assigning default values for files and folders
 */

public class ArgumentReader {

    /**
     * Arguments which are passed to the geneauto tools.
     */
    private static String[] arguments;

    /**
     * Arguments mapped to parameters. The map contains pairs of parameter name
     * and value
     */
    private static Map<String, String> parameters = new HashMap<String, String>();

    /**
     * Unique instance of this class. This class is a singleton in order to
     * avoid concurrent accesses to the arguments.
     */
    private static ArgumentReader instance;

    /**
     * Map for accepted command line switches and corresponding parameter names
     * 
     * first element is command line switch name (including "-" or "--") second
     * element is parameter name
     */
    private static Map<String, String> acceptedArguments = new HashMap<String, String>();

	/**
	 * list of mandatory arguments 
	 * TODO AnTo 030910 It would be better to make this list by parameters instead 
	 * of arguments (switches). Currently this list does not contain the mandatory 
	 * input file name, for which the switch is empty
	 */
    private static List<String> requiredArguments = new ArrayList<String>();

    /**
     * (Sub)Set of parameters that require a value.
     */
    private static Set<String> valuedParameters = new HashSet<String>();
    
    /**
     * map of file path parameter. key of the map is the filename, value is
     * name root (<root>) for each filename the arguments reader does following
     * calculations: - separates filename from the path and writes it to
     * parameter <root>Name - separates directory path from the full path and
     * writes it to parameter <root>Directory - writes full path to <root>Path
     */
    private static Map<String, String> fileParameters = new HashMap<String, String>();
    
    /**
     * Private constructor of this class.
     * 
     * @param args
     */
    private ArgumentReader() {
    }

    /**
     * Cleans up argumentsreader static attributes
     * 
     */
    public static void clear() {
        arguments = null;
        parameters.clear();
        acceptedArguments.clear();
        fileParameters.clear();
        valuedParameters.clear();
        requiredArguments.clear();
    }

    /**
     * Method getInstance.
     * 
     * @param args
     *            Arguments which are passed to the geneauto tools.
     * @return The unique instance of this class.
     * 
     */
    public static ArgumentReader getInstance() {
        if (instance == null) {
            instance = new ArgumentReader();
        }
        return instance;
    }

    /**
     * Method readArguments
     * 
     * Parses command line arguments and initialises defaults. NB! this is here
     * for legacy reasons, normally we should use getArgument to retrieve one
     * value
     * 
     * @return The arguments sorted in a Map.
     */
    public static Map<String, String> readArguments() {
        return parameters;
    }

	/**
	 * Returns value of given parameter
	 * 
	 * @param paramName
	 *         name of the parameter
	 * @return value of the parameter (String) or null, if it is undefined
	 * 		   NOTE: Empty string is a defined value for non-valued arguments!
	 */
    public static String getParameter(String paramName) {
        return parameters.get(paramName);
    }

    /**
     * Sets the value of given parameter
     * 
     * @param paramName
     *            -- name of the parameter
     * @return value of the parameter (String)
     */
    public static void setParameter(String paramName, String value) {
        parameters.put(paramName, value);
    }

    /**
     * Initialises ArgumentReader -- sets new argument list and cleans up all
     * previously parsed and stored maps/lists
     * 
     * parses arguments and stores the resulting parameter map
     * 
     * TODO (AnTo) Check, if the following still holds:  
     *      We should also check for illegal double entries in the command line. 
     *      Non-valued switches must not have a value. Currently this
     * is not rejected. 
     *      All valued switches must have a value. Currently, if they are followed 
     * by another switch, then this is considered a value.
     * 
     * @param arguments
     *            the arguments to set
     */
    public static void setArguments(String[] p_arguments) {
        parameters.clear();
        arguments = p_arguments;
        
        // Clean surrounding quotation marks from the argument, if given.
		// NOTE: This makes sense only, when the arguments are file or directory
		// paths. Currently, in our case is is always the case. AnTo 100713.
        for (int i = 0; i < arguments.length; i++) {
        	if (arguments[i].length() > 2 &&
        			arguments[i].startsWith("\"") && arguments[i].endsWith("\"")) {
        		arguments[i] = arguments[i].substring(1, arguments[i].length()-1);
        	}
        }

        // set default list of arguments if the initialising tool has not given
        // any
        setDefaults();

        // add arguments that are common to all tools
        addCommonArguments();

        // Scan arguments
        if (arguments == null || arguments.length == 0) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ArgumentReader",
                    "GE0001", "Incorrect/missing command line parameters! "
                            + "Expected command line: \n\t"
                            + getExpectedArgString()
                            + "\n Supplied command line: \n\t"
                            + getArgumentString(), "");
        } else {
            String previous = "";
            String current = "";
            if (arguments.length == 1) {
                // Only one argument
                addParameter("", arguments[0]);
            } else {
                /*
                 * Several arguments given. Note: this loop checks two entries:
                 * previous and current. Hence, we need to make one loop more
                 * than the number of arguments, since we don't know if the last
                 * switch requires a value or not.
                 */
                for (int i = 0; i <= arguments.length; i++) {
                    if (i < arguments.length) {
                        current = arguments[i];
                    } else {
                        current = null;
                    }

                    if (previous.startsWith("-")) {
                        // Previous element is a switch (key)
                    	if (current == null || current.startsWith("-")) {
                    		// Previous element was a key without a value -  
                    		// we reached the next key or end of the list.
							// Store the previous element (key) with an explicit
							// empty string (the only case, where empty string
							// is allowed).
                            addParameter(previous, "");
                    	} else {
                    		// Current element is the value for the previous element (key)
                            addParameter(previous, current);                    		
                    	}
                    } else {

                        // Previous element is not a switch (key)
                    	
                    	// Handling for unnamed arguments (without key) - There can be only one.
                        if (!(current == null || current.startsWith("-"))) {                        	
                            // Current element is not a switch (key) and we are
                            // not done with all arguments yet.
                            addParameter("", arguments[i]);
                        }
                    }
                    previous = current;
                }
            }
        }

        // Check that all required arguments have been given
        String paramName;
        for (String key : requiredArguments) {
            paramName = acceptedArguments.get(key);
            if (!parameters.containsKey(paramName)) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "ArgumentReader", "GE0001",
                        "Missing command line parameter \""
                                + paramName
                                + "\" (command line switch \"" + key
                                + "\")! "
                                + "Expected command line: \n\t"
                                + getExpectedArgString()
                                + "\n Supplied command line: \n\t"
                                + getArgumentString());

            }
        }
        
        // Initialise default parameters
        if (!parameters.containsKey("logLevel"))
            parameters.put("logLevel", "INFO");
        
        // Check parameter values
        checkParameterValues();
    }

    /** Scans the collected parameters and performs additional validity checks */
    private static void checkParameterValues() {
		for (Entry<String, String> entry : parameters.entrySet()) {
			String key   = entry.getKey();
			String value = entry.getValue();
			if (valuedParameters.contains(key)) {
				// Valued parameter
				if ("".equals(value)) {
	            	// Value must be non-empty
        			String switchName = getSwitchForParameter(key);
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            "ArgumentReader", "",
                            "Parameter \"" + switchName 
                            		+ "\" requires a value. Empty string supplied.\n\t"
                                    + "Expected command line: \n\t"
                                    + getExpectedArgString()
                                    + "\n Supplied command line: \n\t"
                                    + getArgumentString());
				}
			} else {
            	// Non-valued parameter
				if (!"".equals(value)) {
	            	// Value must be empty string
					String switchName = getSwitchForParameter(key);
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            "ArgumentReader", "",
                            "Parameter \"" + switchName 
                            		+ "\" does not take any value.\n\t"
                                    + "Expected command line: \n\t"
                                    + getExpectedArgString()
                                    + "\n Supplied command line: \n\t"
                                    + getArgumentString());
				}
			}
		}
	}

    /** Returns the switch name that is related to a parameter */
    private static String getSwitchForParameter(String paramName) {
    	// Search accepted arguments
		for (Entry<String, String> entry : acceptedArguments.entrySet()) {
			if (entry.getValue().equals(paramName)) {
				return entry.getKey();
			}
		}
		// Not found
		// We do not throw a critical error here, 
		// because this method is called from another error call.
        EventHandler.handle(EventLevel.ERROR,
            "ArgumentReader", "getSwitchForParameter()",
            "Parameter \"" + paramName + "\" not defined!");
        return null;
	}

	public static String getExpectedArgString() {
        String expectedArgString = "";

        // if there is an argument without key, then add this first
        if (acceptedArguments.containsKey("")) {
            expectedArgString += "<" + acceptedArguments.get("") + ">";
            if (!requiredArguments.contains("")) {
                expectedArgString = "[" + expectedArgString + "]";
            }
        }

        // add mandatory arguments first
        for (String key : requiredArguments) {
            if (key != "") {
                expectedArgString += " " + key + " " + "<"
                        + acceptedArguments.get(key) + ">";
            }
        }

        // add optional arguments
        for (String key : acceptedArguments.keySet()) {
            if (key != "" && !requiredArguments.contains(key)) {
                expectedArgString += " [" + key + " " + "<"
                        + acceptedArguments.get(key) + ">]";
            }
        }

        return expectedArgString;
    }

    /**
     * Adds pair of command-line switch and corresponding parameter to the list
     * of accepted arguments. if Required = true, the parameter name is also
     * added to requiredArguments list
     * 
     * @param switchName
     *            -- name of the command-line switch
     * @param paramName
     *            -- name of the related parameter
     * @param isRequired
     *            -- the parameter is mandatory
     * @param isValued
     *            -- the parameter requires an associated value
     * @param isFile
     *            -- the parameter is filename
     * @param nameRoot
     *            -- base for deriving parameter names of file parameters
     * 
     */
    public static void addArgument(String switchName, String paramName,
            boolean isRequired, boolean isValued, boolean isFile, String nameRoot) {

        acceptedArguments.put(switchName, paramName);

        if (isRequired) {
            requiredArguments.add(switchName);
        }

        if (isValued) {
            valuedParameters.add(paramName);
        }

        if (isFile)
            fileParameters.put(paramName, nameRoot);

    }

    /**
     * Adds parameter (key + value)to the parameter map if it is found in 
     * allowedParameters
     * 
     * Throws error otherwise
     * 
     * @requirement GR-TG-I010
     * 
     * @param switchName
     * @param switchValue
     */
    private static void addParameter(String switchName, String switchValue) {
        String paramName;

        if (acceptedArguments.containsKey(switchName)) {
            paramName = acceptedArguments.get(switchName);

            /*
             * lets assume for now, that we can have only one instance of each
             * parameter
             */
            if (!parameters.containsKey(paramName)) {
                parameters.put(paramName, switchValue);
                
                /*
                 * check if the parameter is file parameter. If it is, resolve
                 * the name and separate name, directory and full path. NB! here
                 * we want to preserve the relative paths -- they will be
                 * resolved later.
                 */
                resolveFileParameter(paramName, false);

                return;
            } else {
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "ArgumentReader", "GE0001",
                        "Incorrect/missing command line parameters! Duplicated parameter \""
                                + switchName + "\". "
                                + "Expected command line: \n\t"
                                + getExpectedArgString()
                                + "\n Supplied command line: \n\t"
                                + getArgumentString(), "");
            }
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ArgumentReader",
                    "GE0001",
                    "Incorrect/missing command line parameters! Command line switch \""
                            + switchName + "\" not accepted. "
                            + "Expected command line: \n\t"
                            + getExpectedArgString()
                            + "\n Supplied command line: \n\t"
                            + getArgumentString(), "");
        }

    }

    /**
     * Checks if parameter is a filename and if yes, separates directory and
     * name part.
     * 
     * TODO: extend it to support default start of relative path. currently it
     * resolves all paths against the working directory. Normally we want to
     * find the input model first and then resolve paths against its location
     * 
     * @param paramName
     *            -- name of the parameter
     * @param forceAbsolutePath
     *            -- if true, the path is update to absolute path
     */
    private static void resolveFileParameter(String paramName,
            boolean forceAbsolutePath) {

        if (fileParameters.containsKey(paramName)) {
            String nameRoot;
            String filePath;

            nameRoot = fileParameters.get(paramName);
            filePath = parameters.get(paramName);

            if (filePath != null) {
                File f = new File(filePath);
                // Name only
                String newKey;
                newKey = nameRoot + "Name";
                parameters.put(newKey, f.getName());
                valuedParameters.add(newKey);
                if (forceAbsolutePath) {
                    // Full input path
                    newKey = nameRoot + "Path";
                    parameters.put(newKey, f.getAbsolutePath());
                    valuedParameters.add(newKey);
                    /*
                     * Directory Note! We use the FileUtil version of getParent
                     * here, because the java one returns null when no directory
                     * information has been given by the user.
                     */
                    newKey = nameRoot + "Directory";
                    parameters.put(newKey, FileUtil.getParent(f.getAbsolutePath()));
                    valuedParameters.add(newKey);
                } else {
                    // Full input path
                    newKey = nameRoot + "Path";
                    parameters.put(newKey, f.getPath());
                    valuedParameters.add(newKey);
                    // Directory
                    newKey = nameRoot + "Directory";
                    parameters.put(newKey, FileUtil.getParent(f.getPath()));
                    valuedParameters.add(newKey);
                }
            } else {
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "ArgumentReader", "GE0001",
                        "Incorrect/missing command line parameters! "
                                + "Expected command line: \n\t"
                                + getExpectedArgString()
                                + "\n Supplied command line: \n\t"
                                + getArgumentString(), "");
            }
        }
    }

	/**
	 * Determines the output and tmp directories, depending on the input file
	 * names and parameters
	 * 
	 * TODO AnTo 03.05.2011 This method contains a bit awkward management of 
	 * the toolset architecture that would be better managed in the launcher
	 * 
	 * @param sameInOutDirByDefault
	 *            If this is true and the output directory has not been set
	 *            otherwise, then assumes that the input and output directories
	 *            are equal
	 * @param toolsetStart
	 *            Tells whether it is the toolset starting up or some of its
	 *            components (an elementary tool)
	 */
    public static void initFilePathsAndLog(boolean sameInOutDirByDefault,
            boolean toolsetStart) {
        // location for temporary files
        String tempFolder = "";
        // location for input files
        String inputFolder = "";
        // location for output files
        String outputFolder = "";
        // location of the default block library
        String defBlockLibPath = "";

        // DETERMINE THE INPUT DIRECTORY
        // check that the input file folder can be located
        // if it can, make sure, it is an absolute path
        inputFolder = getParameter(GAConst.ARG_INPUTFILE_DIR);
        if (inputFolder == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ArgumentReader",
                    "", "Can not determine the input file folder");
            return;
        } else {
            // make sure we have absolute path from this point on
            resolveFileParameter(GAConst.ARG_INPUTFILE, true);
            // read again just in case it was relative path before
            inputFolder = getParameter(GAConst.ARG_INPUTFILE_DIR);
        }

        // DETERMINE THE OUTPUT DIRECTORY
        outputFolder = getParameter(GAConst.ARG_OUTPUTFOLDER);
        boolean automaticOutputFolder = outputFolder == null;
        if (toolsetStart) {
        	setParameter(GAConst.ARG_OUTPUTFOLDER_MODE, automaticOutputFolder ? "AUTO" : "USER");
        }
        
        // if not found derive from the output file name
        if (outputFolder == null) {
        	if (toolsetStart) {
        		String modelName = FileUtil.getFileNameNoExt(parameters
                        .get(GAConst.ARG_INPUTFILE_NAME));
                outputFolder = FileUtil.appendPath(inputFolder, modelName
                        + GAConst.OUTPUTFOLDER_SUFFIX);
        	} else {
        		String outputFile = getParameter(GAConst.ARG_OUTPUTFILE);
	        	if (outputFile != null && !outputFile.isEmpty()) {
					outputFolder = FileUtil.getParent(outputFile);
	        	} else if (sameInOutDirByDefault) {
	        		outputFolder = inputFolder;
	        	} else { 
	        		EventHandler.handle(EventLevel.CRITICAL_ERROR, "ArgumentReader",
	                        "", "Can not determine the output folder");
	                return;
	        	}
        	}
	        // make sure the output folder is full absolute path
	        File tmpF = new File(outputFolder);
	        outputFolder = tmpF.getAbsolutePath();
	        // add output folder to parameters
	        setParameter(GAConst.ARG_OUTPUTFOLDER, outputFolder);
        }

        // DETERMINE THE LOCATION OF TEMP FOLDER
        // derive temp folder from input file location
        if (toolsetStart) {
        	tempFolder = FileUtil.appendPath(outputFolder, GAConst.TMP_DIR);
        } else if (automaticOutputFolder) {
        	tempFolder = outputFolder;
        } else {
        	tempFolder = inputFolder;
        }
        
        // make sure the temp folder is full absolute path
        File tmpF = new File(tempFolder);
        tempFolder = tmpF.getAbsolutePath();
        // add temp folder to parameters
        setParameter(GAConst.ARG_TMPFOLDER, tempFolder);

        String tempModel = ArgumentReader.getParameter(GAConst.ARG_TEMP_MODEL);
        
        // set default value for temp model if it is not set
        if (ArgumentReader.getParameter(GAConst.ARG_TEMP_MODEL) == null) {
        	ArgumentReader.setParameter(GAConst.ARG_TEMP_MODEL, GAConst.TEMP_MODEL_EXCHANGE); 
        } else if ((!tempModel.equals(GAConst.TEMP_MODEL_EXCHANGE)) 
        		&& (!tempModel.equals(GAConst.TEMP_MODEL_ALL))) {
        	
        	// unsupported temp model value -> print error
        	EventHandler.handle(EventLevel.ERROR, "GALauncherImpl.parseCmdLn", 
        			"", "Unsupported value for tempModel switch! \nValue: "
        			+ tempModel
        			+ "\nAccepted values: "
        			+ GAConst.TEMP_MODEL_EXCHANGE
        			+ ", " + GAConst.TEMP_MODEL_ALL);
        }
        
        // DETERMINE THE LOCATION OF DEFAULT BLOCK LIBRARY FILE
        // location of the project or .jar file from classpath
        // README : MaPa, 20080804, re-factoring in order to share with
        // BlockSequencer
        // Introduce method getInstallationPath

        defBlockLibPath = GASysInfo.getDefBlockLibraryPath();

        // TODO: (to NiMa) change handling of block library so that the defLibraryFilePath
        // is always used and libraryFilePath is added to it when existing
        // when the latter is done, we can remove setting blockLibraryFilePath
        // from here
        defBlockLibPath = FileUtil.appendPath(defBlockLibPath,
                GAConst.DEFAULT_BLOCKLIB);

        if(new File(defBlockLibPath).exists()) {
            setParameter(GAConst.ARG_DEFLIBFILE, defBlockLibPath);
        }
        if (!parameters.containsKey(GAConst.ARG_LIBFILE)) {
            // setParameter(GAConst.ARG_LIBFILE, defBlockLibPath);
        } else {
            // Force libraryFilePath to be absolute path
            resolveFileParameter(GAConst.ARG_LIBFILE, true);
        }        

        /**
         * Set log file that is common for all tools in tool-set.
         * 
         * @requirement GR-TG-O011
         */
        String logFile = FileUtil.appendPath(tempFolder, GAConst.LOG_FILE_NAME);

        // CHECK PERMISSIONS
        FileUtil.assertCanWrite(logFile);
        FileUtil.assertCanWrite(tempFolder);
        FileUtil.assertCanRead(getParameter(GAConst.ARG_INPUTFILE));

        // SET THE LOG LEVEL
        // TODO: this shall be removed in future
        String strDebugLevel = ArgumentReader.getParameter("logLevel");
        boolean defaultLog = false;
        EventLevel level = EventLevel.INFO;
        try {
            level = EventLevel.valueOf(strDebugLevel);
        } catch (java.lang.IllegalArgumentException e) {
            defaultLog = true;
            level = EventLevel.INFO;
        }

        if (defaultLog) {
            EventHandler.handle(
                EventLevel.INFO,
                "ArgumentReader.initFilePathsAndLog", "",
                "Accepted values for log leval are NO_LOG, INFO, DEBUG, WARNING, ERROR, CRITICAL_ERROR. Using INFO by default.");
        }

        // initialise the event handler.
        EventHandler.init(logFile, level, toolsetStart);
    }

    /**
     * Fills acceptedArguments array with default values if it is empty
     * 
     * included for legacy reasons -- tobe removed after all tools are updated
     * to use proper command-line handling.
     * 
     */
    public static void setDefaults() {
        if (acceptedArguments.isEmpty()) {
            addArgument("", GAConst.ARG_INPUTFILE, true, true,
                    true, GAConst.ARG_INPUTFILE_NAME_ROOT);
            addArgument("-b", "libraryFilePath", false, true, false, "");
            addArgument("-O", GAConst.ARG_OUTPUTFOLDER, false, true, false, "");
            addArgument("-T", GAConst.ARG_TMPFOLDER, false, true,false, "");
            addArgument("--mLib", GAConst.ARG_MODELLIBFOLDER, false, true,false, "");
        }
    }

    /**
     * sets argument types, that are common to all tools
     * 
     */
    private static void addCommonArguments() {
        // deprecated, will be removed in the future
        addArgument("-l", "logLevel", false, true, false, "");
    }

    /**
     * Compiles a string of command line arguments for displaying in messages
     * 
     * @return argStr -- command line as one string
     */
    public static String getArgumentString() {
        String argStr = "";

        for (int i = 0; i < arguments.length; i++) {
            argStr += arguments[i] + " ";
        }

        return argStr;
    }

	/**
	 * Helper function for composing a command line array for an elementary
	 * tool.
	 * 
	 * The function works with *valued arguments*. It either adds both key and
	 * value to the supplied buffer of command line tokens or not. If the key is
	 * null or value is (null or empty), the buffer stays unmodified.
	 * 
	 * See also addFlagToCmdLine
	 * 
	 * @param pArray
	 * @param pKey
	 * @param pValue
	 * @return
	 */
    public static List<String> addArgToCmdLine(List<String> pArray,
            String pKey, String pValue) {
        // do not add key when value is empty
        if (pValue == null || pValue == "") {
            return pArray;
        }

        // if key exists add the key to the list
        if (pKey != null && pKey != "") {
            pArray.add(pKey);
        }

        // add parameter value
        pArray.add(pValue);

        return pArray;
    }

	/**
	 * Helper function for composing a command line array for an elementary
	 * tool.
	 * 
	 * The function works with *non-valued flags*. It either adds the key to 
	 * the supplied buffer of command line tokens or not. If the value is
	 * false, the buffer stays unmodified.
	 * 
	 * See also addArgToCmdLine
	 * 
	 * @param pArray
	 * @param pKey
	 * @param pValue
	 * @return
	 */
    public static List<String> addFlagToCmdLine(List<String> pArray,
            String pKey, boolean pValue) {
    	
        // do not add key when value is false
        if (pValue == false) {
            return pArray;
        }

        // add key
        pArray.add(pKey);

        return pArray;
    }

    /**
     * Tests, whether a given command line flag has been set.
     * 
     * @param flagName
     * @return true, if the flag has been set
     */
    public static boolean isFlagSet(String flagName) {
        String paramSt = ArgumentReader.getParameter(flagName);
        return paramSt != null && paramSt.equals("");
    }

}
