/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utilities/parser/FunctionSignatureParser.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:09 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utilities.parser;

/**
 * A class that performs parsing of simple function signatures Matches strings
 * like: myFun myFun() myFun(x, y) z = myFun z = myFun() z = myFun(x, y) etc.
 */

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FunctionSignatureParser {

    public String outputVar;
    public String functionName;
    public String[] arguments;

    // Matches the patterns described above
    protected String getPattern() {
        return "(?:(\\w+)\\s*=\\s*)?(\\w+)(?:\\((.*)\\))?\\s*";
    }

    /**
     * @param s
     *            String to be parsed
     */
    public FunctionSignatureParser(String s) {

        Pattern p = Pattern.compile(getPattern());
        
        s = s.trim();
        
        Matcher m = p.matcher(s);
        if (m.matches()) {
            outputVar = m.group(1);
            functionName = m.group(2);
            String argStr = m.group(3);
            if (argStr != null) {
                // Split arguments
                arguments = argStr.split("\\s*,\\s*");
                if (arguments != null) {
                    if (arguments.length == 1 && "".equals(arguments[0].trim())) {
                        // The argument part contained only whitespace
                        arguments = null;
                    } else {
                        for (int i = 0; i < arguments.length; i++) {
                            arguments[i] = arguments[i].trim();
                        }
                    }
                }
            }
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    getClass().getCanonicalName(), "",
                    "Pattern match failed on input: " + s, "");
        }
    }

}
