/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utilities/parser/AntLRUtils.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2010-04-01 05:24:37 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utilities.parser;

import geneauto.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import org.antlr.runtime.tree.Tree;

public class AntLRUtils {

    /**
     * Retrieves the list of String couples (attribute name, attribute value)
     * from the given tree
     * 
     * @param t - the tree
     * @return the map of attribute names and values
     */
    public final static Map<String, String> getAttMap(Tree t) {
        // Declares a map
        Map<String, String> map = new HashMap<String, String>();
        // For each child in the input tree
        for (int i = 0; i < t.getChildCount(); i++) {
            // If child has only one child
            if (t.getChild(i).getChildCount() == 1)
                // Puts the child name and the child's child name in the map
                map.put(t.getChild(i).getText(),
                		StringUtils.clean(
                				t.getChild(i).getChild(0).getText()));
        }
        return map;
    }

    /**
     * Locate a field by the given child name and return it.
     * We call field an element (AST child node) with non-structural value 
     * (single grand child node).
     * 
     * @param section - section from the stateflow parse tree
     * @param name - name of the element to be located
     * @return the corresponding Tree node
     */
    public static Tree getFieldByName(Tree section, String name) {
    	for (int i=0; i<section.getChildCount(); i++) {
    		if (section.getChild(i).getText().equals(name)) {
        		return section.getChild(i);
    		}
    	}
    	return null;
    }
    
    /**
     * Same as getFieldValue(section, name, true)
     * 
     * @param section - section from the AST parse tree
     * @param name - name of the element to be located using "dot-notation": "a", "a.b"
     * @return
     */
    public static String getFieldValue(Tree section, String name) {
        return getFieldValue(section, name, true);
    }
    
    /**
     * Locate a field by the given child name and return the value of the field.
     * We call field an element (AST child node) with non-structural value 
     * (grand child node).
     * 
     * @param section - section from the AST parse tree
     * @param name - name of the element to be located using "dot-notation": "a", "a.b"
     * @param clean - If true, then all newlines and quotes are removed from the string; 
     * @return
     */
    public static String getFieldValue(Tree section, String name, boolean clean) {
    	// Locate the sub-field
    	while (name.indexOf(".")>0) {
    		section = getFieldByName(section, name.substring(0, name.indexOf(".")));
    		if (section==null) {
    			return "";
    		}
    		name = name.substring(name.indexOf(".")+1);
    	}
    	
    	// Locate the field, get and clean the value
    	for (int i=0; i<section.getChildCount(); i++) {
    		if (section.getChild(i).getText().equals(name)) {
    			// We assume that fields first child contains its value
    		    String value = section.getChild(i).getChild(0).getText();
    		    if (clean) {
    		        value = StringUtils.clean(value);
    		    }
        		return value; 
    		}
    	}

    	return "";
    }
}
