package geneauto.eventhandler;

/**
 * enum gathering the event levels: 
 * 	- NO_LOG - No info messages sent to console. TODO: this event class is 
 * 				not used and should be removed (ToNa 20/07/09)
 *  - INFO - info on steps/states the program is entering/exiting 
 *  - DEBUG - info about decisions made during the processing (the information 
 *  that is not necessary for the user when following normal operation of 
 *  the tool-set, however, it may help to find the cause of the problem in
 *  case an error occurs in processing. Debug info is sent to log file, but 
 *  not to the console output.   
 *  - WARNING - an event informing the user that dangerous modelling 
 *  construct was discovered (e.g. missing parameter value was filled 
 *  in with default value). Warning is type of information event, that 
 *  highlights actions which may lead to error or undesired code. 
 *  - ERROR - program continues working but produces no output
 *  - CRITICAL_ERROR - program stops immediately and produces no output
 */
public enum EventLevel {
	NO_LOG, DEBUG, INFO, WARNING, ERROR, CRITICAL_ERROR
}