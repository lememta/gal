/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/factories/CodeModelFactory.java,v $
 *  @version	$Revision: 1.27 $
 *	@date		$Date: 2012-02-05 20:24:55 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.factories;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.components.ModelFactoryConfig;
import geneauto.models.gacodemodel.GACodeModel;

import java.util.HashMap;
import java.util.Map;

/**
 * Factory which is used to read or write a CodeModel into a XML file.
 * 
 */
public class CodeModelFactory extends ModelFactory {

	@Override
	// Do not move this method to the super class. We want to identify the subclass 
	public String getReferenceString() {
		return getClass().getCanonicalName();
	}
	
	/**
	 * Constructor of this class.
	 * 
	 * @param inputModel
	 *            Reference to the model object that is to be completed
	 * @param toolName
	 *            The name of the tool which instantiated the factory.
	 * @param p_inputFiles
	 *            Input file of the factory (CodeModelFactory never handles
	 *            several files)
	 * @param p_outputFile
	 *            The XML output file
	 * @param modelFactoryConfig
	 *            Configuration of the model factory. If null, the default
	 *            factory is used
	 * @param systemModelFactory
	 *            If this is set, then it means that the ModelFactory (model)
	 *            belongs to a dependent pair of ModelFactories (models)
	 */
	public CodeModelFactory(GACodeModel inputModel, String toolName,
			String p_inputFile, String p_outputFile,
			ModelFactoryConfig p_config, SystemModelFactory systemModelFactory) {
		
        super(p_config);
        
        primaryModelFactory = systemModelFactory;
        
        if (inputModel == null) {
        	inputModel = new GACodeModel();
        }
        
		init(inputModel, toolName, p_inputFile, p_outputFile, null);
    }

	/**
	 * Method getModel. Overrides the inherited method to return a more precise
	 * type.
	 * 
	 * @return the model of type GACodeModel.
	 * @Override
	 */
    public GACodeModel getModel() {
		if (model instanceof GACodeModel) {
			return (GACodeModel) model;
		}
		else {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					"CodeModelFactory", "", 
					"Wrong type of the model. Expecting: " 
						+ "GACodeModel" 
						+ ", got: "
						+ model.getClass().getSimpleName(),
						"");
			return null;
		}
    }
    
	/**
	 * Overrides the inherited method to return a more precise type.
	 * 
	 * @return the model of type GACodeModel.
	 * @Override
	 */
    public GACodeModel readModel() {
    	return (GACodeModel) super.readModel();
    }
    
    
    
	public Map<String, String> getExcludedXmlAttributes() {
		/*
		 * List of attributes which shall be excluded from the xml file while
		 * saving a model. 
		 * 
		 * TODO AnTo 141209 Made this list specific to a factory.
		 * Check, if it contains elements that are not needed in this factory. 
		 * Also see the task specified for this method in the super class.
		 */
		Map<String, String> excludedXmlAttributes = new HashMap<String, String>();

		if (excludedXmlAttributes.keySet().size() <= 0) {
			// - A 
			excludedXmlAttributes.put("autoInit", "false");

			// - E
			excludedXmlAttributes.put("emptyLinesBefore", "0");
			excludedXmlAttributes.put("executionOrder", "0");
			excludedXmlAttributes.put("elementMap", null);
			
			// - F
			excludedXmlAttributes.put("fromInputModel", "false");
			
			// - I
			excludedXmlAttributes.put("id", "0");
			excludedXmlAttributes.put("isAtomic", "true");
			excludedXmlAttributes.put("isBlockDescription", "false");
			excludedXmlAttributes.put("isConst", "false");
			excludedXmlAttributes.put("isFixedName", "false");
            excludedXmlAttributes.put("isGANative", "false");
            excludedXmlAttributes.put("isImplicit", "false");
            excludedXmlAttributes.put("isPrefix", null);
            excludedXmlAttributes.put("isRedundant", "false");
            excludedXmlAttributes.put("isStatic", "false");
            excludedXmlAttributes.put("isSystem", "false");
            excludedXmlAttributes.put("isVolatile", "false");
			
			// - M
            excludedXmlAttributes.put("mainStatement", null);  // Only in printer
			excludedXmlAttributes.put("model", null);
			excludedXmlAttributes.put("module", null);
			excludedXmlAttributes.put("moduleInfo", null);  // Only in printer 
			
			// - N
            excludedXmlAttributes.put("nameResolver", null);
            excludedXmlAttributes.put("nBits", "0");
            excludedXmlAttributes.put("nonStandardExtension", "false");
			excludedXmlAttributes.put("nameResolver", null);  // Only in printer 
			
            // - O
            excludedXmlAttributes.put("observabilityRequired", "false");
            excludedXmlAttributes.put("ordinal", null);
            excludedXmlAttributes.put("originalFullName", "");
			
			// - P
			excludedXmlAttributes.put("parent", null);
            excludedXmlAttributes.put("precedence", null);
            
            // - R
            excludedXmlAttributes.put("resetOutput", "false");
			
			// - S
			excludedXmlAttributes.put("scope", "0");
			// This attribute is currently used only internally in the CodeModelOptimiser
			excludedXmlAttributes.put("sequenceNumber", null);
			excludedXmlAttributes.put("signed", "false");
			excludedXmlAttributes.put("sourceElementId", "0");
            
			// - T
			excludedXmlAttributes.put("types", null);  // temporary attribute in 
													   // CodeModelDependency
			excludedXmlAttributes.put("tempVariableFactory", null);  // Only in printer 
			
			// - U
			excludedXmlAttributes.put("util", null);  // Only in printer 
		
        }
		return excludedXmlAttributes;
	}	

}
