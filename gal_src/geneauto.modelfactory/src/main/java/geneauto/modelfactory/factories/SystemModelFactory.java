/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/factories/SystemModelFactory.java,v $
 *  @version	$Revision: 1.35 $
 *	@date		$Date: 2012-02-05 20:24:54 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.factories;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.components.ModelFactoryConfig;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gablocklibrary.ParameterType;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.GenericBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Port;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.NodeTypeMap;
import geneauto.utils.PrivilegedAccessor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Model Factory for GASystemModels.
 * 
 */
public class SystemModelFactory extends ModelFactory {

	/** Set a flag notifying that there exists a dependent CodeModelFactory */
	public void setIsPrimaryModelFactory() {
		this.isPrimaryModelFactory = true;
	}

	@Override
	// Do not move this method to the super class. We want to identify the subclass 
	public String getReferenceString() {
		return getClass().getCanonicalName();
	}
	
    /**
     * @param inputModel
     * 			  Reference to the model object that is to be completed
     * @param toolName
	 *            The name of the tool which instantiated the factory.
     * @param p_inputFile
	 *            Input file for the system model factory 
	 *            (this factory never handles more than one input)
     * @param p_outputFile
	 *            The XML output file.
     * @param p_blockLibrary
	 *            Block library factory used for building nodeNameMap of the
	 *            types stored in block library.
     * @param modelFactoryConfig
     * 			  configuration of the model factory. If null, the default factory is used
	 */
	@SuppressWarnings("unchecked")
	public SystemModelFactory(GASystemModel inputModel, String toolName, String p_inputFile,
			String p_outputFile, BlockLibraryFactory p_blockLibraryFactory,
			ModelFactoryConfig p_config) {

		super(p_config);
		
        if (inputModel == null) {
        	inputModel = new GASystemModel();
        }
        
		GABlockLibrary blockLibrary = null; 
	
		if (p_blockLibraryFactory != null) {
			p_blockLibraryFactory.readModel();
			// ReadNodeTypeMap from p_blockLibrary
	        blockLibrary = p_blockLibraryFactory.getModel();
			editorMapping = (Map<String, String>) PrivilegedAccessor.getValue(
					blockLibrary, "attributeMapping");
		} 	
                
		
		/*
		 * Conditional jump over some code that is used in the Gene-Auto main
		 * transformation, but not required and compatible with the GA-Ecore
		 * translation. 
		 * 
		 * TODO (to AnTo) 9 Try to find a better solution.
		 */
		//if (!config.isIgnoreExtraMappings()) {
		if (blockLibrary != null) {
	        
	        // for each parameterType from the GABlockLibrary, 
	        // we store them into the editorMapping as well 
	        // so it eases the map browsing.
	        for (GAModelElement element : blockLibrary.getAllElements()) {
	            if (element instanceof BlockType) {
	                BlockType blockType = (BlockType) element;
	                for (ParameterType paramType : blockType.getParameterTypes()) {
	                    if (paramType.getEditorMapping() != null
	                            && (!"".equals(paramType.getEditorMapping()))) {
	                        editorMapping.put(
	                            blockType.getName() + "." + paramType.getName(), 
	                                "Parameter." + paramType.getEditorMapping());
	                    } else {
	                        editorMapping.put(
	                                blockType.getName() + "." + paramType.getName(), 
	                                    "Parameter." + paramType.getName());
	                    }
	                }
	            }
	        }
	        
	        // Browse the Block library model in order to build the
			// nodeNameMap.
			NodeTypeMap blocks = new NodeTypeMap();
			nodeNameMap = new HashMap<String, NodeTypeMap>();
			classNameMap = new HashMap<String, String>();
			idMap = new HashMap<Integer, GAModelElement>();
	
			String modelType = blockLibrary.getClass().getSimpleName();
			if (!("GABlockLibrary".equals(modelType))) {
				EventHandler.handle(
						EventLevel.ERROR, "SystemModelFactory", "",
						"SystemModelFactory : the input block library is not of correct type.", 
						"It should be of type GABlockLibrary but is actually " 
						+ modelType);
			} else {
				for (GAModelElement element : blockLibrary.getAllElements()) {
					BlockType blockType = null;
					if (element instanceof BlockType) {
						blockType = (BlockType) element;
						blockType.computeBlockClass(blockType.getType());
						blocks.getMap().put(blockType.getName(),
								blockType.getType());
	
					} else {
					}
				}
				this.nodeNameMap.put("Block", blocks);
			}
		}

		// initialise the system model
        init(inputModel, toolName, p_inputFile, p_outputFile, null);

        // attach block library to the retrieved/created model
        if (blockLibrary != null) { 
	        GASystemModel sm = getModel();
	        PrivilegedAccessor.setValue(sm, "blockLibrary", blockLibrary, true);
        }
	}
	
    /**
	 * Method getModel. Overrides the inherited method to return a more precise
	 * type.
     * 
     * @return the model of type GASystemModel.
     * @Override
     */
    public GASystemModel getModel() {
		if (model instanceof GASystemModel) {
			return (GASystemModel) model;
		}
		else {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					"SystemModelFactory", 
					"", 
					"Wrong type of the model. Expecting: " 
						+ "GASystemModel" 
						+ ", got: "
						+ model.getClass().getSimpleName(),
						"");
			return null;
		}
    }

    /*
	 * TODO AnTo 141209 This method is used only by the TSimulinkImporter. It
	 * uses the model factory to build a system model from scratch, instead of
	 * using the model elements own methods. This is overly complicated and hard
	 * to manage (duplicate code to indirectly access model elements
	 * attributes). Revise in the future
	 */
    @SuppressWarnings("unchecked")
	protected GAModelElement createModelElementImpl(String type, String name,
			GAModelElement parent, Map<String, String> attributes,
			Map<String, String> allowedParameters) {
    	
		GAModelElement result = super.createModelElementImpl(type, name,
				parent, attributes, allowedParameters);
		
		// if the element is a block, we set its attribute and,
		// if its parent is not null, we add it to its parent's
		// children
		if (result instanceof Block) {
		    Block tmpBlock = (Block) result;
		    boolean addToSystem = result.getParent() != null
		            && result.getParent() instanceof SystemBlock;

		    // if attributes are present, we set the object with the
		    // correct values.
		    if (attributes != null) {
		        attributes.put("type", name);
		        // get DiagramInfo class
		        DiagramInfo diagramInfo = tmpBlock.getDiagramInfo();


		        for (String attribute : attributes.keySet()) {
		        	// parameter is attribute of class
		            if (PrivilegedAccessor.isAttributeOfClass(
		                    attribute, tmpBlock.getClass())) {
		                PrivilegedAccessor.setValue(tmpBlock,
		                        attribute, attributes.get(attribute), true);
		            // parameter is attribute of DiagramInfo class
		            } else if (PrivilegedAccessor.isAttributeOfClass(
		                    attribute, diagramInfo.getClass())) {
		                PrivilegedAccessor.setValue(
		                        diagramInfo, attribute,
		                        attributes.get(attribute), true);
		            // block is GenericBlock or 
		            // the parameter is listed in the block library
		            } else if (tmpBlock instanceof GenericBlock 
		            		|| allowedParameters != null
		                        && allowedParameters.keySet().contains(
		                                name + "." + attribute)) {
		                    // we create an object of type
		                    // Parameter.
		            		BlockParameter param = new BlockParameter();
		                    StringExpression value = new StringExpression();
		                    value.setLitValue(attributes.get(attribute));
		                    param.setName(attribute);
		                    param.setValue(value);
		                    tmpBlock.addParameter(param);
		            }
		        }
		    }

		    if (addToSystem) {
		        SystemBlock tmpSystem = (SystemBlock) result
		                .getParent();
		        tmpSystem.addBlock((Block) result);
		    }
		}

		// if a port is met in simulink model file,
		// the simulink parser must call this method using
		// createModelElement("InDataPort", "inDataPorts", block,
		// attrs)
		// where InDataPort is the real type of the port
		// inDataPorts is the name of the corresponding attribute of
		// object block,
		// block is the Block which contains the port.
		if (result instanceof Port) {
		    for (String attribute : attributes.keySet()) {
		        PrivilegedAccessor.setValue(result, attribute,
		                attributes.get(attribute), true);
		    }
		    if (parent instanceof Block) {
		        Object portField = PrivilegedAccessor.getValue(parent,
		                name);
		        if (portField instanceof List) {
		            List<Object> ports = (List<Object>) portField;
		            ports.add(result);
		        } else {
		            PrivilegedAccessor.setValue(parent, name, result, true);
		        }

		    }
		}

		// if a signal is met in the model, we assign to its ports
		// the values of the
		// modelElements with the corresponding element id.
		if (result instanceof Signal) {
		    for (String attribute : attributes.keySet()) {
		        if (attribute.equals("srcPort")
		                || attribute.equals("dstPort")) {
		            // search for the corresponding port in the
		            // model
		            if (attributes.get(attribute) != null) {
		                    PrivilegedAccessor.setValue(result,
		                            attribute, model.getElementById(new Integer(
		                                    attributes.get(attribute)) .intValue()), true);
		            }
		        }
		        if (attribute.equals("name")) {
		            result.setName(attributes.get("name"));
		        }
		    }

		    if (parent instanceof SystemBlock) {
		        List<Object> signals = (List<Object>) PrivilegedAccessor
		                .getValue(parent, name);
		        signals.add(result);

		    }
		}
		return result;
	}

	public Map<String, String> getExcludedXmlAttributes() {
		/*
		 * List of attributes which shall be excluded from the xml file while
		 * saving a model. 
		 * 
		 * TODO AnTo 141209 Made this list specific to a factory.
		 * Check, if it contains elements that are not needed in this factory. 
		 * Also see the task specified for this method in the super class.
		 */
		Map<String, String> excludedXmlAttributes = new HashMap<String, String>();

		if (excludedXmlAttributes.keySet().size() <= 0) {
			// - A 
			excludedXmlAttributes.put("assignedPriority", "0");
			excludedXmlAttributes.put("autoInit", "false");
			
			// - D
			
			// - E
			excludedXmlAttributes.put("executionOrder", "0");
			excludedXmlAttributes.put("elementMap", null);
			
			// - F
			excludedXmlAttributes.put("fromInputModel", "false");
			
			// - I			
			excludedXmlAttributes.put("id", "0");
			excludedXmlAttributes.put("initialOutput", "0");
            excludedXmlAttributes.put("index", "-1"); // Parameter.index
			excludedXmlAttributes.put("isConst", "false");
			excludedXmlAttributes.put("isBlockDescription", "false");
			excludedXmlAttributes.put("isFixedName", "false");
            excludedXmlAttributes.put("isGANative", "false");
            excludedXmlAttributes.put("isImplicit", "false");
            excludedXmlAttributes.put("isPrefix", null);
            excludedXmlAttributes.put("isRedundant", "false");
            excludedXmlAttributes.put("isStatic", "false");
            excludedXmlAttributes.put("isSystem", "false");
            excludedXmlAttributes.put("isVolatile", "false");
			
			// - M
			excludedXmlAttributes.put("model", null);
			
			// - N
            excludedXmlAttributes.put("nameResolver", null);
            excludedXmlAttributes.put("nBits", "0");
            excludedXmlAttributes.put("nonStandardExtension", "false");
            
            // - O
            excludedXmlAttributes.put("observabilityRequired", "false");
            excludedXmlAttributes.put("ordinal", null);
            excludedXmlAttributes.put("originalFullName", "");
			
			// - P
			excludedXmlAttributes.put("parent", null);
			excludedXmlAttributes.put("posX", "0");
			excludedXmlAttributes.put("posY", "0");
            excludedXmlAttributes.put("precedence", null);
            
            // - R
            excludedXmlAttributes.put("resetOutput", "false");
			
			// - S
			excludedXmlAttributes.put("sampleTime", "0");
			// TODO This is actually a CodeModel element attribute.
			// However currently Ecore2GAXML is always using this factory instead.
		    excludedXmlAttributes.put("sequenceNumber", "0");
		    excludedXmlAttributes.put("scope", "0");
			excludedXmlAttributes.put("signed", "false");
            excludedXmlAttributes.put("sizeX", "0");
            excludedXmlAttributes.put("sizeY", "0");
            // Note: The following is actually a CodeModelElement feature,
            // but some Code Model Elements also occur in the System Model
			excludedXmlAttributes.put("sourceElementId", "0");
            
			// - U
            excludedXmlAttributes.put("userDefinedPriority", "0");
            excludedXmlAttributes.put("userPriority", "0");
		
        }
		return excludedXmlAttributes;
	}

}

