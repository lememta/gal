/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/factories/ModelFactory.java,v $
 *  @version	$Revision: 1.52 $
 *	@date		$Date: 2012-02-05 20:46:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.factories;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.components.ModelFactoryConfig;
import geneauto.modelfactory.components.ModelReader;
import geneauto.modelfactory.components.ModelWriter;
import geneauto.modelfactory.components.PackageBrowser;
import geneauto.modelfactory.components.XmlParser;
import geneauto.modelfactory.utilities.ObjectAccessor;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;
import geneauto.models.genericmodel.NodeTypeMap;
import geneauto.models.genericmodel.Transformation;
import geneauto.utils.PrivilegedAccessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.w3c.dom.Node;

/**
 * Generic component for constructing and holding models used by geneauto
 * toolset. Main class of the tool. Its role is to : <br>
 * <dd>- read an XML file and generate a Model. <br> <dd>- read a Model and
 * generate an XML file.<br>
 * 
 * 
 * TODO: there are several methods in this class where local variable and 
 * class attribute are distinguished by case (local variable/function parameter 
 * in lower case, class variable in camel case). This is error prone and should
 * be changed (ToNa 03/12/08)  
 */
public abstract class ModelFactory {

    /**
     * This map allows to map a Simple class name with its full package name. It
     * is set when the model factory is launched. it contains :
     * class.simpleName, class.canonicalName couples.
     */
    protected Map<String, String> classLocation = new HashMap<String, String>();

    /**
     * Reference to the configuration object
     */
    protected ModelFactoryConfig config;

    /**
     * Object which is used for making the XML reading easier.
     */
    protected XmlParser parser;

    /**
     * Name of the tool which instantiated the ModelFactory.
     */
    protected String toolName;

    /**
     * Name of the input file
     */
    protected String inputFile;

    /**
     * Name of the output file. This attribute cannot be changed once the
     * factory is instantiated.
     */
    protected String outputFile;

    /**
     * Root node of the processed model.
     */
    protected Model model;

    /**
     * For each Tag Name , stores a NodeTypeMap. For instance : TagName Block
     * will be associated to a map which associates : Constant -> SourceBlock,
     * Gain -> Combinatorial ...
     */
    protected Map<String, NodeTypeMap> nodeNameMap = new HashMap<String, NodeTypeMap>();

    /**
     * Contains class names and corresponding xml node names.<br>
     * <dd>Key : className <br> <dd>Value : nodeName. <br>
     * Only the class Names that are different of corresponding XML node name
     * are included in the map.
     */
    protected Map<String, String> classNameMap;

    /**
     * A map containing Ids and corresponding ModelElements.
     * Note! We assume that these elements have a field "id",
     */
    protected Map<Integer, GAModelElement> idMap = new HashMap<Integer, GAModelElement>();

    /**
     * A map which allows to keep geneauto independent from the editor (scicos/
     * simulink). This map contains, for each simulink or scicos attribute
     * (type, name, sampleTime, isVirtual ...) the corresponding attribute in
     * GASystemModel . This class is used only by SystemModelFactory. i.e. :
     * Constant.Name -> name Constant.SampleTime -> sampleTime
     * 
     * it can only be useful to treat the mapping with geneauto parameters.
     * Gain.Value -> Parameter.Value Gain.Operator -> Parameter.Operator
     */
    protected Map<String, String> editorMapping = new HashMap<String, String>();

    /**
     * A boolean which indicates if the Model is the main model or if it is a
     * SubModel. If it is a SubModel, then the History shall not appear in its
     * XML tree.
     */
    protected boolean isHighestModel;

    /**
     * Utility class for reading and creating model objects.
     */
    private ModelReader modelReader;

    /**
     * Utility class for writing the model.
     */
    private ModelWriter modelWriter;

	/**
	 * Start time of the transformation.
	 */
	private Date startTime;

	/**
	 * Flag that indicates that this ModelFactory is a first member of a pair of
	 * dependent ModelFactories.
	 */
	protected boolean isPrimaryModelFactory;

	/**
	 * Reference to the primary modelFactory in a pair of dependent
	 * ModelFactories
	 */
	protected ModelFactory primaryModelFactory;

	/**
	 * If true, then all references to the dependent model are ignored on
	 * reading the model (set to null). 
	 * NOTE! This means that when saving the model, the references are LOST!
	 */
	private boolean ignoreDependentModelRefs = false;

    /**
     * Main constructor of this class. This constructor is called by the tool
     * which instantiates the factory.
     */
    public ModelFactory(ModelFactoryConfig p_config) {
    	setStartTime(new Date()); // Save start time
        isHighestModel = true;
        config = p_config;
        if (config == null) {
        	config = ModelFactoryConfig.getInstance();
        }

    }

    /**
     * Constructor of this class. Initialises model factory by xml subtree. No
     * input or output file names are created. This constructor is called only
     * by other ModelFactories which encounter SubModels.
     * 
     * @param xmlSubtree
     */
    public ModelFactory(ModelFactoryConfig p_config, Node xmlSubtree) {
    	this(p_config);
        isHighestModel = false;
    }

    /**
     * Adds an element to the model. If the parent of the element is empty, the
     * element is added to the elements array of the model element. Otherwise,
     * it is added as a child of the node referenced by element.parent .
     */
    public void addElement(GAModelElement element) {
        model.addElement(element);
        if (element.getId() != 0) {
            addToIdMap(element.getId(), element);
        }
    }

    /**
     * Gets the element corresponding to the id given as a parameter.
     * 
     * @param id
     *            The id of the element we want to get.
     * @return The appropriate element
     */
    public GAModelElement getElementByExternalId(String id) {
        return (GAModelElement) idMap.get(id);
    }

    /**
     * Gets the element corresponding to the id given as a parameter.
     * 
     * @param id
     *            The id of the element we want to get.
     * @return The appropriate element
     */
    public GAModelElement getElementById(int id) {
        return (GAModelElement) idMap.get(id + "");
    }

    /**
     * Method getModel.
     * 
     * @return the attribute model.
     */
    public Model getModel() {
        return model;
    }

    public String getOutputFile() {
		return outputFile;
	}

	public ModelReader getModelReader() {
		return modelReader;
	}

	public ModelWriter getModelWriter() {
		return modelWriter;
	}

	public void setIgnoreDependentModelRefs() {
		this.ignoreDependentModelRefs = true;
	}

	/**
     * Lists all of the classes of the jar package given as a parameter.
     * 
     * @param packageName
     *            The name of the package we want to browse.
     * @param jars
     *            The jar files we want to browse
     * @return The list of the classes which exist within packageName.
     */
    @SuppressWarnings("unchecked")
    public static synchronized List<Class> getClassesFromFileJarFile(
            String packageName, List<String> jars) {

        List<Class> classes = new ArrayList<Class>();

        // for each jar file given as a parameter
        for (String jar : jars) {
            try {
                String newJar = jar + "/";
                JarFile currentFile = new JarFile(newJar);

                // for each class found in the jar file, we test that it
                // belongs to the package packageName
                for (Enumeration e = currentFile.entries(); e.hasMoreElements();) {
                    JarEntry current = (JarEntry) e.nextElement();
                    if ((current.getName().endsWith(".class"))
                            && (current.getName().length() > packageName
                                    .length())) {
                        if ((current.getName().substring(0,
                                packageName.length()).replace("/", ".")
                                .equals(packageName))
                                && !current.getName().contains("$")) {
                            // if all is ok, we create the class.
                            classes.add(Class
                                    .forName(current.getName().replaceAll("/",
                                            ".").replace(".class", "")));
                        }
                    }
                }
            } catch (IOException e) {
                EventHandler.handle(EventLevel.ERROR, "ModelFactory", "",
                        "A problem was encountered as the brower was reading the package "
                                + packageName + ".", e.getMessage());

            } catch (ClassNotFoundException e) {
                EventHandler.handle(EventLevel.ERROR, "ModelFactory", "",
                        "A problem was encountered as the brower was reading the package "
                                + packageName + ".", e.getMessage());
            }
        }

        return classes;
    }

    /**
     * Method which role is to initialise the factory
     * 
     * @param toolName
     *            the tool which instantiated the ModelFactory.
     * @param modeltype
     *            class name of the model.
     * @param rootpackage
     *            Search path for java classes corresponding to model elements.
     * @param p_inputFile
     *            Input file name for reading the model.
     * @param p_outputFile
     *            Name of the output file.
     * @param p_nodeNameMap
     *            Contains the node names.
     * @param classnamemap
     *            Contains class names and corresponding xml node names.
     * @param p_config
     * 
     */
    @SuppressWarnings("unchecked")
    protected void init(Model p_inputModel, String p_toolName, 
            String p_inputFile, String p_outputFile,
            Map<String, NodeTypeMap> p_nodeNameMap) {

        EventHandler.handle(EventLevel.DEBUG, "ModelFactory", "",
                "Starting initialisation of ModelFactory.", "");

        parser = XmlParser.newInstance();

        this.toolName = p_toolName;
        this.inputFile = p_inputFile;        
        this.outputFile = p_outputFile;

        if (p_nodeNameMap != null) {
        	this.nodeNameMap.putAll(p_nodeNameMap);
        }
        
        // for each class of geneauto models, we are going to store
        // its simple name and its canonical name into the classLocation map
        // this allows to keep a clean project architecture.
        List<String> packages = config.getPackageNames();

        for (String pack : packages) {
			List<Class> classes;
            // try to read the classes from the classpath directly
			classes = PackageBrowser.getClassesForPackage(pack);				
    	
            for (Class classe : classes) {
            	String className = classe.getSimpleName();
            	className = trimImpl(className);
                classLocation.put(className, classe
                        .getCanonicalName());
            }
        }

        modelReader = new ModelReader();
        modelReader.init(p_inputModel, parser, classLocation, nodeNameMap,
                inputFile, toolName, isHighestModel, idMap, config);
        model = modelReader.getModel();

        EventHandler.handle(EventLevel.DEBUG, "ModelFactory", "",
                "Ending initialisation of ModelFactory.");
    }

	/**
	 * Reads the input model from the model file(s) given as a parameter of the
	 * constructor.
	 * @return the read model
	 */
    public synchronized Model readModel() {
        EventHandler.handle(EventLevel.DEBUG, "ModelFactory", "",
                "Model factory has started to read the " + model.getClass().getSimpleName()
                        + " model.", "");
        
        // Propagate the depenent model references disabling flag to 
        // ModelReader, if required. 
        if (ignoreDependentModelRefs) {
        	modelReader.setIgnoreDependentModelRefs();
        }

        // If double model mode and converting second model, then initialise reference maps
        if (primaryModelFactory != null) {
        	// Local objects of the primary model
        	modelReader.setIdMapDM(primaryModelFactory.getModelReader().getIdMap());
        	// Outer references from the primary model
        	modelReader.setPointers(primaryModelFactory.getModelReader().getPointersDM());
        	// Set Step2 flag in the model reader
        	modelReader.setDoubleModeStep2();
        }        
        
        model = modelReader.readModel();

        if (config.isIdHandlingByModel()) {
	        // scan the model once more and assign ID's to elements that
	        // did not have one before
	        model.setNoNewId(false);
	        model.updateLastId();
        }
        
        // If single model mode, then check that there are no dependent model references
		if (!isPrimaryModelFactory && primaryModelFactory == null 
				&& !ignoreDependentModelRefs 
				&& !modelReader.getPointersDM().isEmpty()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ModelFactory", "",
						"Read model contains unresolved pointers to a dependent model, " +
						"but the current ModelFactory does not belong to a pair of dependent models!"
						+ "\n Model: " + model.getModelName() 
						+ " of type " + model.getClass().getCanonicalName());
            return null;        	
        }

        EventHandler.handle(EventLevel.DEBUG, "ModelFactory", "",
                "Model factory has ended reading the " + model.getClass().getSimpleName() + " model.");
        
        return model;
    }

    /**
     * Writes the model into the output file given as a parameter. If more than
     * one input file was read in order to create the model, the writing must
     * not be executed and raise an error.
     */
    @SuppressWarnings("unchecked")
	public synchronized void writeModel() {
    	
    	if (outputFile == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		"ModelFactory.writeModel", "",
                    "Output file name not given");   
            return;
    	}
    	
        // if an error has occurred during the execution of the tool,
        // no file must be created.
        if (EventHandler.getAnErrorHasOccured()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		"ModelFactory.writeModel", "",
                    "Error(s) occured during execution of the tool." +
                    "\n Tool output will not be written.");    		
            return;
        } 
        
        EventHandler.handle(EventLevel.DEBUG, "ModelFactory", "",
                "Model factory has started to write the " + model.getClass().getSimpleName()
                        + " model in file " + outputFile + ".");

        modelWriter = makeWriter();
        
		// Create a new transformation with tool name and read-write times and
		// add it to the model.
        Transformation tmpTransf = new Transformation(startTime, new Date(), toolName);
    	if (config.isECoreCollections()) {
    		// Get the EList by the getter and then add
    		ObjectAccessor.getEListFromField(model, "transformations").add(tmpTransf);
    	} else {
    		// Go through the model class
        	model.getTransformations().add(tmpTransf);
    	}

		// If double model mode and converting second model, then initialise
		// reference maps or set the partial write mode
        if (primaryModelFactory != null) {
        	if (primaryModelFactory.getModelWriter() != null) {
	        	// Local objects of the primary model
	        	modelWriter.setSavedObjectsDM(primaryModelFactory.getModelWriter().getSavedObjects());
	        	// Outer references from the primary model
	        	modelWriter.setSavedPointers(primaryModelFactory.getModelWriter().getSavedPointersDM());
	        	// Set Step2 flag in the model reader
	        	modelWriter.setDoubleModeStep2();
        	} else {
	        	// Local objects of the primary model
	        	modelWriter.setReadObjectsDM(primaryModelFactory.getModelReader().getIdMap());
	        	// Set partial Step2 flag in the model reader
        		modelWriter.setDoubleModePartialStep2();
        	}
        }    
        
    	// Write model
        modelWriter.writeModel(parser, model, outputFile, nodeNameMap);

        // If single model mode, then check that there were no dependent model references
		if (!isPrimaryModelFactory && primaryModelFactory == null
				&& !modelWriter.getSavedPointersDM().isEmpty()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ModelFactory", "",
						"Written model contains unresolved pointers to a dependent model, " +
						"but the current ModelFactory does not belong to a pair of dependent models!"
						+ "\n Model: " + model.getModelName() 
						+ " of type " + model.getClass().getCanonicalName());
            return;        	
        }

        EventHandler.handle(EventLevel.DEBUG, "ModelFactory", "",
                "Model factory has ended to write the " + model.getClass().getSimpleName()
                        + " model in file " + outputFile + ".", "-");
    }

	/**
	 * @return initialised ModelWriter
	 */
	protected ModelWriter makeWriter() {
		return new ModelWriter(config, getExcludedXmlAttributes());
	}

	/**
	 * Given a modelElement name and the list of its attributes, create a
	 * modelElement with the correct type.
	 * 
	 * @param type
	 *            Type of the modelElement
	 * @param name
	 *            Name of the block (block type).
	 * @param attributes
	 *            Map <attributeName, attributeValue>
	 * @return
	 */
    public GAModelElement createModelElement(String type, String name,
            GAModelElement parent, Map<String, String> attributes) {

    	// AnTo 141209. The editorMapping seems to be currently actually 
    	// used only in the SystemModelFactory and only for handling Generic 
    	// blocks.
        return createModelElementImpl(type, name, parent, attributes,
            editorMapping);
    }

    /**
     * Given a modelElement name and the list of its attributes, create a
     * modelElement with the correct type. Implementation of the method
     * createModelElement
     * 
     * @param type
     *            Type of the modelElement, if equals "Block" ,
     * @param name
     *            Name of the block (block type).
     * @param attributes
     *            Map <attributeName, attributeValue>
     * @return
     * 
     */
    protected GAModelElement createModelElementImpl(String type, String name,
            GAModelElement parent, Map<String, String> attributes,
            Map<String, String> allowedParameters) {

        GAModelElement result = null;

        // we create a new object according to its name and its type
        if (name != null && !("".equals(name))) {
            String className = "";
            if (type != null && nodeNameMap.containsKey(type)) {
                className = nodeNameMap.get(type).getMap().get(name);
            } else {
                className = type;
            }

            /* when class was not found we assume that the input was
             * of type GenericBlock 
             */
            if (classLocation.get(className) == null
                    || "".equals(classLocation.get(className))) {
                className = "GenericBlock";
            }
            Object tmpObj = PrivilegedAccessor
                    .getObjectForClassName(classLocation.get(className));

            // if object is a GAModelElement, we add it to the model.
            if (tmpObj instanceof GAModelElement) {
                result = (GAModelElement) tmpObj;
                result.setParent(parent);
            }

        }
        return result;
    }

    /**
     * Method setModel
     * 
     * @param model
     *            the model to set
     */
    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * @return the classLocation
     */
    public Map<String, String> getClassLocation() {
        return classLocation;
    }

    /**
     * Adds an element to the idMap and update the global lastId.
     * 
     * @param Id
     *            Id of the element.
     * @param element
     *            GAModelElement.
     */
    protected void addToIdMap(int id, GAModelElement element) {
        idMap.put(id, element);
        model.updateLastId(element);
    }

	/**
	 * Map of the attributes which must not be registered in the XML file while
	 * saving the Model. The mapping contains a field name and value pair. A
	 * field is excluded, if its name and value pair have been found in the map
	 * or the name has been found in the map (current limitation is that for
	 * each field there can be only one name-value pair). The field is always
	 * skipped, if the value in the map is null. The attributes have been sorted
	 * by alphabetical order for better readability. The map can be also null.
	 * 
	 * TODO: (TF:553) change this map so that class name is also included. For
	 * instance in case of expression we have dataType that is automatically
	 * calculated and do not need to be written to xml. At the same time
	 * variable and function need the same attribute written
	 */
	public abstract Map<String, String> getExcludedXmlAttributes();

	public Date getStartTime() {
		return startTime;
	}

	private void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
    // If the interfacePattern is true, then trim "Impl" from the end of the name
	private String trimImpl(String name) {
		if (config.isInterfacePattern() && name.endsWith("Impl")) {
        	name = name.substring(0, name.length()-4);
        }
		return name;
	}
	
	/** Identification of the particular model factory */
	public abstract String getReferenceString();

}
