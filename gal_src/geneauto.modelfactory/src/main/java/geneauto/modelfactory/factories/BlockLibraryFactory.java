/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/factories/BlockLibraryFactory.java,v $
 *  @version	$Revision: 1.34 $
 *	@date		$Date: 2011-07-13 08:32:36 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.factories;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.components.ModelFactoryConfig;
import geneauto.modelfactory.utilities.ObjectAccessor;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gablocklibrary.ParameterType;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Block Library Factory. This is a factory which is specific to the Block
 * Library.
 * 
 */
public class BlockLibraryFactory extends ModelFactory {

    /**
     * A list to store secondary input files
     */
    private List<String> secondaryInputFiles = new ArrayList<String>();
    
	@Override
	// Do not move this method to the super class. We want to identify the subclass 
	public String getReferenceString() {
		return getClass().getCanonicalName();
	}
	
    /**
	 * Constructs block library factory, allowing to import several files and
	 * ensuring the attribute outputFile is never assigned.
	 * 
	 * @param inputModel
	 * 			  Reference to the model object that is to be completed
	 * @param toolName
	 *            The name of the tool which instantiated the factory.
	 * @param p_inputFiles
	 *            List of the input files of this factory.
	 * @param modelFactoryConfig
	 * 				configuration of the model factory. If null, the default factory is used
	 */
	public BlockLibraryFactory(GABlockLibrary inputModel, String toolName, List<String> p_inputFiles,
			ModelFactoryConfig p_config) {
				this(inputModel, toolName, p_inputFiles, null, p_config);
			}

	/**
     * Constructs block library factory, allowing to import several files and
     * ensuring the attribute outputFile is never assigned.
     * 
     * @param inputModel
     * 			  Reference to the model object that is to be completed
     * @param toolName
     *            The name of the tool which instantiated the factory.
     * @param p_inputFiles
     *            List of the input files of this factory.
     * @param p_outputFiles 
     *            List of the output files of this factory.
     * @param modelFactoryConfig
     * 				configuration of the model factory. If null, the default factory is used
     */
    public BlockLibraryFactory(GABlockLibrary inputModel, String toolName, List<String> p_inputFiles,
    		List<String> p_outputFiles, ModelFactoryConfig p_config) {
        // call default constructor of ModelFactory.
        super(p_config);
        
        if (inputModel == null) {
        	inputModel = new GABlockLibrary();
        }
        
        // first file in the list is primary model file
        String primaryInputFile = p_inputFiles.get(0);
        String primaryOutputFile = null;
        if (p_outputFiles != null && p_outputFiles.size() > 0) {
        	primaryOutputFile = p_outputFiles.get(0);
        }
        
        // all the rest go to the secondary input files list
        for (int i = 1; i < p_inputFiles.size(); i ++) {
        	String currentFile = p_inputFiles.get(i);
        	if (currentFile.equals(primaryInputFile) ||
        			secondaryInputFiles.contains(currentFile)) {
		                EventHandler.handle(EventLevel.CRITICAL_ERROR,
	                        "BlockLibraryFactory", "",
	                        "Block library file " 
	                        + currentFile
	                        + " is imported twice", "");
        		
        	} else {
            	secondaryInputFiles.add(p_inputFiles.get(i));        		
        	}
        }
        
        // Store reference to the input model.
        model = inputModel;
        
        init(model, toolName, primaryInputFile, primaryOutputFile, null);

    }

    /**
     * Constructs block library factory from a single library file
     * 
     * @param inputModel
     * 			  Reference to the model object that is to be completed
     * @param toolName
     *            The name of the tool which instantiated the factory.
     * @param p_inputFile
     *            Input files of this factory
     * @param modelFactoryConfig
     * 				configuration of the model factory. If null, the default factory is used
     */
   public BlockLibraryFactory(GABlockLibrary inputModel, String toolName, String p_inputFile,
		   ModelFactoryConfig p_config) {
        // call default constructor of ModelFactory.
        super(p_config);
        
        if (inputModel == null) {
        	inputModel = new GABlockLibrary();
        }
        
        // Store reference to the input model.
        model = inputModel;
        
       init(model, toolName, p_inputFile, null, null);

    }

    /**
     * WriteModel is overwritten. The BlockLibraryFactory must not be able to
     * modify its input file.
     */
    public void writeModel() {
        // do nothing
    }

    /**
     * ReadModel is overwritten.
     */
    @SuppressWarnings("unchecked")
	public GABlockLibrary readModel() {  	
    	
    	if (inputFile == null || inputFile.isEmpty()) {
    		EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getSimpleName(), "", 
    				"Block library location not determined. Please check the installation.");
    		return null;    	
    	}    	
    	
        // Read the model from file
        super.readModel();
        
        GABlockLibrary blockLibrary = getModel();

        // temporary set to check duplicates in block list
        Set<String> typeNameSet = new HashSet<String>();
        
        // check duplicate block definitions
        List<BlockType> elements;
        if (config.isECoreCollections()) {
        	elements = ObjectAccessor.getEListFromField(model, "elements");
        } else {
        	elements = ((GABlockLibrary) model).getElements();
        }
        for (GAModelElement currentElt : elements) {
            // If the current element is a BlockType instance
            if (currentElt instanceof BlockType) {
            	String name = ObjectAccessor.getNameFieldValue(currentElt);
            	if (typeNameSet.contains(name)) {
                    // Duplicated definition of the same block
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            "BlockLibraryFactory", "",
                            "Duplicated definition of block type: " 
                            + name
                            + " ( in " + secondaryInputFiles.get(0) + " )");            		
            	} else {
            		typeNameSet.add(name);
            	}
            }
        }

        // import all secondary files 
        for (String currentFile : secondaryInputFiles) {
            // Creates a new BlockLibraryFactory
        	BlockLibraryFactory currentLib = new BlockLibraryFactory(new GABlockLibrary(), toolName,
        			currentFile, config);
            // read content of the secondary block library file
            currentLib.readModel();
            
            // transfer the blocks from secondary library to primary library
            for (GAModelElement secondaryType : 
            						currentLib.getModel().getElements()) {
            	if (typeNameSet.contains(secondaryType.getName())) {
                    EventHandler.handle(EventLevel.INFO,
                            "BlockLibraryFactory", "",
                            "Redefinition of block type "
                                    + secondaryType.getName()
                                    + " ( from : "
                                    + currentFile + " )", "");
            		// remove the old instance
                    blockLibrary.removeBlockTypeByName(secondaryType.getName());
            	}
        		// add element to the model
            	blockLibrary.addElement(secondaryType);
            }
        }
        return blockLibrary;
     }

    /**
     * Returns true if the parameter parameterName of Block blockName must be
     * evaluated in the EML Parser.
     * 
     * @param blockName
     *            Name of the block. (e.g. Gain, Constant, SubSystem...)
     * @param parameterName
     *            Name of the parameter (e.g. Value, ...)
     * @return true if the parameter of the block must be evaluated with EML.
     */
    public boolean getEvaluateEml(String blockName, String parameterName) {
        BlockType blockType = this.getBlockType(blockName);
        
        if (blockType != null) {
        	ParameterType parameterType = 
        					blockType.getParameterTypeByName(parameterName);
        	if (parameterType != null) {
        		return parameterType.isEvaluateEML();
        	}
        }
        
        return false;
    }

    /**
     * Returns the BlockType which is linked to the blockName given as a
     * parameter.
     * 
     * @param blockName
     *            Name of the block from which we want to have the BlockType.
     * @return The BlockType linked to the block given as a parameter.
     */
    public BlockType getBlockType(String blockName) {
        if (blockName != null) {
        	// TODO This is inefficient. The factory should maintain a hashmap. 
        	// AnTo 13.07.2011
        	return (BlockType) model.getElementByName(blockName);
        }
        
        return null;
    }

    /**
     * Returns an object of type BlockTyper, according to the String name given
     * as a parameter.
     * 
     * @param typerType
     *            Name of the typer we want to access.
     * @return The correct BlockTyper.
     */
    public BlockTyper getTyper(String typerType) {
        BlockTyper result = null;
        Object tmpResult = PrivilegedAccessor.getObjectForClassName(typerType);

        if (tmpResult instanceof BlockTyper) {
            result = (BlockTyper) tmpResult;
        }

        return result;
    }

	/**
	 * Method getModel. Overrides the inherited method to return a more precise
	 * type.
	 * 
	 * @return the model of type GABlockLibrary.
	 * @Override
	 */
    public GABlockLibrary getModel() {
        if (model instanceof GABlockLibrary) {
            return (GABlockLibrary) model;
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "BlockLibraryFactory", "",
                    "Wrong type of the model. Expecting: " + "GABlockLibrary"
                            + ", got: " + model.getClass().getSimpleName(), "");
            return null;
        }
    }
    
	public Map<String, String> getExcludedXmlAttributes() {
		/*
		 * List of attributes which shall be excluded from the xml file while
		 * saving a model. 
		 * 
		 * TODO AnTo 141209 Made this list specific to a factory.
		 * Check, if it contains elements that are not needed in this factory. 
		 * Also see the task specified for this method in the super class.
		 */
		Map<String, String> excludedXmlAttributes = new HashMap<String, String>();

		if (excludedXmlAttributes.keySet().size() <= 0) {
			// - A 
			
			// - B
			excludedXmlAttributes.put("backend", null);
			
			// - D
			
			// - E
			excludedXmlAttributes.put("executionOrder", "0");
			excludedXmlAttributes.put("elementMap", null);
			
			// - F
			excludedXmlAttributes.put("fromInputModel", "false");
			
			// - I
			excludedXmlAttributes.put("id", "0");
			excludedXmlAttributes.put("initialOutput", "0");
			excludedXmlAttributes.put("isFixedName", "false");
            excludedXmlAttributes.put("isGANative", "false");
            excludedXmlAttributes.put("isSystem", "false");
            
			// - L
			excludedXmlAttributes.put("libraryHandler", null);
			
			// - M
			excludedXmlAttributes.put("model", null);
			
			// - N
            excludedXmlAttributes.put("nBits", "0");
            
            // - O
            excludedXmlAttributes.put("originalFullName", "");
			
			// - P
			excludedXmlAttributes.put("parent", null);
            
            // - R
			
			// - S
			excludedXmlAttributes.put("scope", "0");
			excludedXmlAttributes.put("signed", "false");
            
			// - T
			excludedXmlAttributes.put("typer", null);
            
			// - U
		
        }
		return excludedXmlAttributes;
	}	

}
