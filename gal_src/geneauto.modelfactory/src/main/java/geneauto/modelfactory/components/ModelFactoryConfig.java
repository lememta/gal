/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/components/ModelFactoryConfig.java,v $
 *  @version	$Revision: 1.33 $
 *	@date		$Date: 2011-12-13 13:46:59 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.components;

import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.List;

/**
 * A configuration class for converting models to XML.
 * The role of this class is to provide list of packages where 
 * model objects can be found.
 * 
 * It also provides list of class attributes that do not need to be stored 
 * in XML.
 * 
 * In case model objects are loaded somewhere else than the default location
 * (e.g. printer package) this class should be replaced buy an override.
 * 
 */
public class ModelFactoryConfig {

	/**
	 * Jar files in which are situated the packages which are used during the
	 * reading or writing of the XML file.
	 */
	protected List<String> jarFiles = new ArrayList<String>();

	/**
	 * Names of the packages which are used during the reading or writing of the
	 * XML file.
	 */
	protected List<String> packageNames = new ArrayList<String>();

    /**
     * Unique instance of this class
     */
	protected static ModelFactoryConfig instance;

	/* TODO (to AnTo) 9 Check, if the configuration options below are still required */
	/**
	 * Controls, whether the id-s are managed internally by the Model class on
	 * read and write or is it done entirely by the ModelFactory
	 */
	protected boolean idHandlingByModel = true;

	/**
	 * Controls, whether the parent (upward) references will be
	 * automatically inferred from the xml tree and set in the model.
	 */
	protected boolean setParentsOnRead = true;

	/**
	 * Controls, whether the GABlockLibrary LibraryHandlers (typers and
	 * backends) are created or not.
	 */
	protected boolean createLibraryHandlers = true;

	/**
	 * This is a temporary configuration option. It is used to avoid some code
	 * that is used in the Gene-Auto main transformation, but not required and
	 * compatible with the GA-Ecore translation. 
	 * TODO (to AnTo) 9 Try to find a better solution.
	 */
	protected boolean ignoreExtraMappings = false;

	/**
	 * When true, then all classes are assumed to end with a suffix "Impl". This
	 * suffix is not written to the xml file.
	 */
	protected boolean interfacePattern = false;
    
	/**
	 * If true, then ECore collections will be used by default.
	 */
	protected boolean ECoreCollections = false;

	/**
	 * If true, then the methods of the model classes are not directly accessed.
	 */
	protected boolean noDirectAccess = false;

	/**
     * Private constructor of this class, to make sure that 
     * the object can be built only from the method getIInstance.
     * TODO Review this. Currently it is not used this way. AnTo 100114 
     */
    public ModelFactoryConfig () {
    }
    
    /**
     * Returns the unique instance of this class.
     * If null, creates a new instance.
     * @return
     */
    public static ModelFactoryConfig getInstance () {
        if (instance == null) {
            instance = new ModelFactoryConfig();
        }
        return instance;
    }
    
    public boolean isIdHandlingByModel() {
		return idHandlingByModel;
	}

	public boolean isSetParentsOnRead() {
		return setParentsOnRead;
	}

	public boolean isCreateLibraryHandlers() {
		return createLibraryHandlers;
	}

	public boolean isIgnoreExtraMappings() {
		return ignoreExtraMappings;
	}

	public boolean isInterfacePattern() {
		return interfacePattern;
	}

	public boolean isECoreCollections() {
		return ECoreCollections;
	}

	public boolean isNoDirectAccess() {
		return noDirectAccess;
	}

	/**
	 * List of packages in which are situated the classes which are used during
	 * the reading or writing of the Xml files.
	 * 
	 * @return List of the packages.
	 */
	public List<String> getPackageNames() {

		if (packageNames.size() <= 0) {
			packageNames.add("geneauto.models");
			packageNames.add("geneauto.models.common");
            // block library model
			packageNames.add("geneauto.models.gablocklibrary");
            // code model
			packageNames.add("geneauto.models.gacodemodel");
            packageNames.add("geneauto.models.gacodemodel.expression");
            packageNames.add("geneauto.models.gacodemodel.expression.statemodel");
            packageNames.add("geneauto.models.gacodemodel.gaenumtypes");
            packageNames.add("geneauto.models.gacodemodel.operator");
            packageNames.add("geneauto.models.gacodemodel.statement");
            packageNames.add("geneauto.models.gacodemodel.statement.statemodel");
            packageNames.add("geneauto.models.gacodemodel.statement.statemodel.broadcast");
            // system model
            packageNames.add("geneauto.models.gasystemmodel");
            packageNames.add("geneauto.models.gasystemmodel.common");
			packageNames.add("geneauto.models.gasystemmodel.gafunctionalmodel");
			packageNames.add("geneauto.models.gasystemmodel.gafunctionalmodel.blocks");
			packageNames.add("geneauto.models.gasystemmodel.gafunctionalmodel.ports");
			packageNames.add("geneauto.models.gasystemmodel.gastatemodel");
            // generic model
			packageNames.add("geneauto.models.genericmodel");
            // block library package
            packageNames.add("geneauto.blocklibrary.typers");
            packageNames.add("geneauto.blocklibrary.backends");
            // datatypes package
            packageNames.add("geneauto.models.gadatatypes");
		}
		return packageNames;
	}

	/**
	 * Checks, if the given object "child" is a true child of the object
	 * "parent" (i.e a containment relation) or just a reference.
	 * 
	 * Note: By default the relation is determined by checking, whether the
	 * child's parent field points to the given parent. With Ecore-based models
	 * the distinction is made by looking at the eContainer field. In both of
	 * these cases it follows also, that one parent cannot have a same object
	 * simultaneously as a child (in a containment relation) and as a reference
	 * (non-containment relation)!
	 * 
	 * @param parent
	 * @param child
	 * @return
	 */
	public boolean isGAReference(Object parent, Object child) {
		if (child instanceof GAModelElement) {
			return ((GAModelElement) child).getParent() != parent;
		} else {
			return false;
		}
	}

}
