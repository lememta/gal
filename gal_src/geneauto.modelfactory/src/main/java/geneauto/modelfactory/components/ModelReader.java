/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/components/ModelReader.java,v $
 *  @version	$Revision: 1.63 $
 *	@date		$Date: 2011-09-14 08:36:38 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.components;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.utilities.ObjectAccessor;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;
import geneauto.models.genericmodel.NodeTypeMap;
import geneauto.models.genericmodel.Transformation;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.tuple.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class which role is to read the XML file or nodes and to create the model or
 * model elements.
 * 
 * TODO: there are several methods in this class where local variable and 
 * class attribute are distinguished by case (local variable/function parameter 
 * in lower case, class variable in camel case). This is error prone and should
 * be changed (ToNa 03/12/08)  
 */
public class ModelReader {

    /**
     * Object which is used for making the XML reading easier.
     */
    protected XmlParser parser;

    /**
     * This map allows to map a Simple class name with its full package name. It
     * is set when the model factory is launched. it contains :
     * class.simpleName, class.canonicalName couples.
     */
    protected Map<String, String> classLocation;

    /**
     * Reference to the configuration object
     */
    protected ModelFactoryConfig config;
    
    /**
     * For each Tag Name , stores a NodeTypeMap. For instance : TagName Block
     * will be associated to a map which associates : Constant -> SourceBlock,
     * Gain -> Combinatorial ...
     */
    protected Map<String, NodeTypeMap> nodeNameMap;

    /** Root node of the processed model. */
    protected Model model;

	/**
	 * A flag which tells that we are at the second step of writing two
	 * dependent models
	 * */
    private boolean doubleModeStep2 = false;

    /**
     * Input file name
     */
    protected String inputFile;

    /**
     * A map containing Ids and corresponding ModelElements.
     */
    protected Map<Integer, GAModelElement> idMap;

    /**
     * A map containing Ids and corresponding ModelElements in the dependent model.
     */
    protected Map<Integer, GAModelElement> idMapDM;

    /**
     * Map which associates, for each pointer found in the model, the id of the
     * object on which it points and the attribute name of the GAModelElement.
     */
    protected List<Pair<Object[], String[]>> pointers = new ArrayList<Pair<Object[], String[]>>();

    /**
     * A map of Map of pointers to the dependent model.
     */
    protected List<Pair<Object[], String[]>> pointersDM = new ArrayList<Pair<Object[], String[]>>();

	/**
	 * If true, then all references to the dependent model are ignored on
	 * reading the model (set to null). 
	 * NOTE! This means that when saving the model, the references are LOST!
	 */
	private boolean ignoreDependentModelRefs = false;

    public void setDoubleModeStep2() {
		this.doubleModeStep2 = true;
	}

	public Map<Integer, GAModelElement> getIdMap() {
		return idMap;
	}

	public void setIdMap(Map<Integer, GAModelElement> idMap) {
		this.idMap = idMap;
	}

	public Map<Integer, GAModelElement> getIdMapDM() {
		return idMapDM;
	}

	public void setIdMapDM(Map<Integer, GAModelElement> idMapDM) {
		this.idMapDM = idMapDM;
	}

	public List<Pair<Object[], String[]>> getPointers() {
		return pointers;
	}

	public void setPointers(List<Pair<Object[], String[]>> pointers) {
		this.pointers = pointers;
	}

	public List<Pair<Object[], String[]>> getPointersDM() {
		return pointersDM;
	}

	public void setPointersDM(List<Pair<Object[], String[]>> pointersDM) {
		this.pointersDM = pointersDM;
	}
	
	public void setIgnoreDependentModelRefs() {
		this.ignoreDependentModelRefs = true;
	}

	/**
     * Test if a string is xml text or xml comment.
     * 
     * @param nodeName
     *            the nodeName to check.
     * @return true is node is a text or a comment.
     */
    protected boolean isTextOrComment(String nodeName) {
        boolean result = ("#text".equals(nodeName))
                || ("#comment".equals(nodeName));
        return result;
    }

    /**
     * Method processChildren, calls the method process current element for all
     * children of the parameter NodeList.
     * 
     * @param nodeList
     *            List of the node's children
     * @param parent
     *            Parent of the children
     */
    protected void processChildren(NodeList nodeList, GAModelElement parent) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            processCurrentElement(nodeList.item(i), parent, false);
        }
    }

    /**
     * Test if the current node has attributes and if attribute with the current
     * name exist. If test is passed, return the value of the attribute, else
     * return null
     * 
     * @param currentNode
     * @param itemName
     * @return
     */
    public static String getItemTest(Node currentNode, String itemName) {
        if (currentNode.getAttributes() != null
                && currentNode.getAttributes().getNamedItem(itemName) != null) {
            return currentNode.getAttributes().getNamedItem(itemName)
                    .getNodeValue();
        }
        return null;
    }

    /**
     * Method processCurrentElement, generates the GAModelElement corresponding
     * to the current node.
     * 
     * @param currentNode
     *            Current Node which is processed.
     * @param parent
     *            GAModelElement parent.
     * @param isCollectionElement
     * 				if true, then upper-level element is collection
     * @return A appropriate GAModelElement
     * 
     */
    @SuppressWarnings("unchecked")
    protected synchronized Object processCurrentElement(Node currentNode,
            GAModelElement parent, boolean isCollectionElement) {
        Object result = null;

        String rootName = currentNode.getNodeName();
        
        /*
         * Special handling for string: return value of 
         * first attribute ("value")
         */
        if (rootName.equals("String")) {
            return currentNode.getAttributes().item(0).getNodeValue();
        }
        // String rootType =
        // parser.selectNodeAttributes(currentNode).get("type");

        String rootType = getItemTest(currentNode, "type");

        NodeList rootChildren = currentNode.getChildNodes();

        if (!isTextOrComment(rootName)) {

            // create GAModelElement for this node
            if ("gaxml:model".equals(rootType)) {

                // if current node type is gaxml:model
                // instanciate new factory
                // readInnerModel(currentNode);
                result = new ArrayList<Object>();
                for (int i = 0; i < rootChildren.getLength(); i++) {
                    Object temp = processCurrentElement(rootChildren.item(i),
                            null, false);
                    if (temp != null) {
                        ((List) result).add(temp);
                    }
                }
            } else if ("gaxml:history".equals(rootType)) {
                // if current node type is gaxml:history
                // do nothing, history must already have been treated.
            } else if ("gaxml:collection".equals(rootType)) {
                // if current node type is gaxml:collection
                // node name is the name of an attribute of the current Object
                readCollection(currentNode, parent);
            } else if ("gaxml:pointer".equals(rootType)) {
                // if current node type is gaxml:pointer
                readPointer(currentNode, parent, isCollectionElement, false);
            } else if ("gaxml:dmref".equals(rootType)) {
                // if current node type is gaxml:dmref
            	if (!ignoreDependentModelRefs) {
            		readPointer(currentNode, parent, isCollectionElement, true);
            	}
            } else if ("gaxml:object".equals(rootType)) {
                // if current node type is gaxml:object
                result = readObject(currentNode, parent);
            } else if ((nodeNameMap != null)
                    && (nodeNameMap.containsKey(rootName))) {
                // create the corresponding GAModelElement and register it in
                // the
                // Model
                result = readNode(currentNode, parent);
            } else {
                // current node is an object
                String resultClassPath = classLocation.get(rootName);
                
                if (resultClassPath == null) {
                    String msg = "Model Factory could not read the input xml file correctly." 
                        + "The node which causes the problem is : "
                        + "\n rootName : " + rootName
                        + "\n rootType : " + rootType;
                    if (parent != null) {
                        msg += "\n root's parent : " + parent.getReferenceString();                        
                    }
                    EventHandler
                            .handle(
                                    EventLevel.ERROR, "ModelReader", "", msg);
                } else {
                    result = PrivilegedAccessor
                            .getObjectForClassName(resultClassPath);
                    setAttributes(result, currentNode);

                    if (result instanceof GAModelElement) {
	                    // it has parent attribute, the attribute should be set
                        if (config.isSetParentsOnRead()) {
	                        ((GAModelElement) result)
	                                .setParent((GAModelElement) parent);
	
	                    }
                        // memorise the id in case the element has one
                        // TODO: to be removed -- there is up to date idmap in
                        // the model
                        // TODO (to AnTo) 3 Check the above task - can this code be removed?
                        addToIdMap(ObjectAccessor.getIdFieldValue(result), (GAModelElement) result);
                    }

                    if (rootChildren.getLength() > 0) {
                        processChildren(rootChildren, (GAModelElement) result);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Method which reads the transformations of the model and stores them in
     * the model object.
     * 
     * @param root
     *            Transformation root node.
     */
    @SuppressWarnings("unchecked")
    protected void readCollection(Node root, GAModelElement parent) {

        // blockAttributes Node is processed differently
        // from other collections, and only in GABlockLibrary,
        // it allows to map the editor name with geneauto attribute names.
        if ("blockAttributes".equals(root.getNodeName())) {
            if (!(model instanceof GABlockLibrary)) {
                EventHandler.handle(EventLevel.ERROR, "ModelReader", "",
                        "Node name blockAttributes cannot be read as "
                                + "the model is not a BlockLibrary.",
                        "Error occured in ModelReader.readCollection.");
            } else {
                GABlockLibrary blockLib = (GABlockLibrary) model;
                Node child = null;
                for (int i = 0; i < root.getChildNodes().getLength(); i++) {
                    child = root.getChildNodes().item(i);
                    if (!isTextOrComment(child.getNodeName())) {

                        String editorName = ((GAModelElement) parent).getName()
                                + "." + getItemTest(child, "editorName");
                        String gaName = getItemTest(child, "name");
                        blockLib.getAttributeMapping().put(editorName, gaName);
                    }
                }
            }
        } else {
            NodeList rootChildren = root.getChildNodes();

            if (parent != null) {
                Class parentClass = PrivilegedAccessor.getClassForName(parent.getClass()
                        .getCanonicalName());

                // Try to obtain the collection object from the parent class
                String fieldName = root.getNodeName();
                Object collection = PrivilegedAccessor.getValue(parent,
                        fieldName);
                
				// If the parent did not have a container present in the field,
				// then try to create it
                if (collection == null) {
    				Field field = PrivilegedAccessor.getField(
    						PrivilegedAccessor.getClassForName(parentClass
    								.getCanonicalName()), fieldName);
    				
    				collection = ObjectAccessor.initCollectionField(parent, field);
	                if (collection == null) {
            			EventHandler.handle(EventLevel.CRITICAL_ERROR, "ModelReader", 
            					"", "Failed to initialise a collection."
            						+ "\n Field: " + fieldName);
	                    return;	                
	                } 
    				
                } else {
	                if (!(collection instanceof Collection)) {
	                    EventHandler.handle(EventLevel.CRITICAL_ERROR, "ModelReader", "",
	                            "Attribute " + root.getNodeName()
	                                    + " is not a Collection",
	                            "Error occured in ModelReader.readCollection.");
	                    return;	                
	                } 
                }

                for (int i = 0; i < rootChildren.getLength(); i++) {
                    Object tmp = processCurrentElement(
                            rootChildren.item(i), parent, true);
                    if (tmp != null && collection != null) {
                        ((Collection<Object>) collection).add(tmp);
                    }                
                }
                    
            }
        }
    }

	/**
     * Initialises the ModelReader
     * @param initModel
     * @param parsr
     * @param classloc
     * @param nodenamemap
     * @param initInputFile
     * @param toolname
     * @param isHighestModel
     * @param idmap
     */
    public void init(Model initModel, XmlParser parsr,
            Map<String, String> classloc, Map<String, NodeTypeMap> nodenamemap,
            String initInputFile, String toolname, boolean isHighestModel, 
            Map<Integer, GAModelElement> idmap, ModelFactoryConfig p_config) {

        parser = parsr;
        classLocation = classloc;
        nodeNameMap = nodenamemap;
        idMap = idmap;
        inputFile = initInputFile;
        model = initModel;
        
        this.config = p_config;
    }

    /**
     * Reads the input model from the model file(s) given as a parameter of the
     * constructor.
     * 
	 * @param manageIds
	 *            Controls, whether the automatic management of Ids is on or
	 *            off during the reading of the file
	 *            
     * TODO: it seems that we assume always single file. IF so, remove the list
     * and add simple string variable!! (ToNa 18/10/08)
     */
    @SuppressWarnings("unchecked")
    public synchronized Model readModel() {

    	if (config.isIdHandlingByModel()) {
	        // make sure that the model does not issue any new id's before reading
	        // file is complete
	        model.setNoNewId(true);
    	}

        if (inputFile == null) {
        	String refStr = "";
        	if (model != null) {
        		refStr += " Model: " + model.getReferenceString();
        	}
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ModelReader", "",
                    "There is no input file to read." + refStr);
        }

        Document domDocument = parser.parseWithDom(inputFile);
        Node rootNode = null;
        NodeList rootChildren = domDocument.getChildNodes();
        for (int i = 0; i < rootChildren.getLength(); i++) {
        	if (!this.isTextOrComment(rootChildren.item(i).getNodeName())) {
        		rootNode = rootChildren.item(i);
        		break;
        	}
        }

		// Check that the name of the root node matches with the unqualified
		// name of the to be created model class
        String modelType = model.getClass().getSimpleName();
        if (config.isInterfacePattern()) {
        	// Trim "Impl" from the end of the name
        	modelType = modelType.substring(0, modelType.length()-4);
        }        
        if (!modelType.equals(rootNode.getNodeName())) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ModelReader", "",
                    "This ModelFactory is not appropriate for "
                            + "reading the input file " + inputFile,
                    "Instanciated factory is for models of type " + modelType
                            + ".\n "
                            + "The input file contains a model of type "
                            + rootNode.getNodeName() + ".");
        }

        /* TODO Check this commented code? Fix and uncomment or remove. AnTo 100223 
         * for (String file : inputFiles) { if (!file.equals(inputFiles.get(0))) {
         * Document tmpDocument = parser.parseWithDom(file); Node tmpRoot =
         * tmpDocument.getFirstChild(); NodeList tmpList =
         * tmpRoot.getChildNodes();
         * 
         * for (int i = 0; i < tmpList.getLength(); i++) { if
         * (!"#text".equals(tmpList.item(i).getNodeName()) &&
         * !"TransformationHistory".equals(tmpList.item(i) .getNodeName())) {
         * Node tmpNode = domDocument.importNode(tmpList.item(i), true);
         * root.appendChild(tmpNode); } } } }
         */

        setAttributes(model, rootNode);
        readTransformation(rootNode);

        Object rootObj = processCurrentElement(rootNode, null, false);

        if (rootObj instanceof List) {        	
        	List elemList = (List) rootObj;        	
        	
        	// Set the "model" reference of the root level elements to the current model
        	for (Object obj : elemList) {
        		PrivilegedAccessor.setValue(obj, "model", model, true);
        	}
        	
        	// Add elements to the root level collection of the Model object
        	if (config.isECoreCollections()) {
        		// Add directly to the list
                List collection = ObjectAccessor.getEListFromField(model, "elements");
                collection.addAll(elemList);
        	} else {
        		// Go through setters
        		model.addElements(elemList);
        	}
        }

        resolvePointers();

        return model;
    }

    /**
     * Processes recursively given XML tree by calling itself for each subnode
     * on the tree and creates the corresponding GAModelElements.
     * 
     * @param xmlRoot
     *            current node in the XML tree.
     * @param parent
     *            parent of the currentModelElement.
     */
    protected Object readNode(Node xmlRoot, GAModelElement parent) {
        Object result = null;

        String rootName = xmlRoot.getNodeName();
        String rootType = getItemTest(xmlRoot, "type");

        NodeList rootChildren = parser.selectNodes(xmlRoot, "child::*");
        String resultClassPath = classLocation.get(nodeNameMap.get(rootName)
                .getMap().get(rootType));

        if (resultClassPath == null) {
            EventHandler.handle(EventLevel.ERROR, "ModelReader", "",
                    "Model Factory could not read the input xml "
                            + "file correctly.",
                    "The node which causes the problem is : "
                            + "\n rootName : " + rootName + "\n rootType : "
                            + rootType + "\n root's parent : "
                            + parent.getReferenceString());
        }

        result = PrivilegedAccessor.getObjectForClassName(resultClassPath);

        if (result instanceof GAModelElement) {
            ((GAModelElement) result).setParent((GAModelElement) parent);
            addToIdMap(ObjectAccessor.getIdFieldValue(result), (GAModelElement) result);

            setAttributes(((GAModelElement) result), xmlRoot);
            if (rootChildren.getLength() > 0) {
                processChildren(rootChildren, (GAModelElement) result);
            }
        }
        return result;
    }

    /**
     * If a tag gaxml:object is found, then it is considered as an attribute of
     * the GAModelElement given as a parameter of this function. <br>
     * The correct XML structure for this part is : <br>
     * <br>
     * <GAModelElementClassName id="x" name="elementName" type="zyx"><br>
     * <parameterName type="gaxml:object"><br>
     * <parameterClassName paramAttribute1="" paramAttribute2="" .../><br>
     * </parameterName><br>
     * </GAModelElement>
     */
    protected Object readObject(Node xmlRoot, GAModelElement parent) {
        Object result = null;

        // get the information which are useful for the processing of this node
        String rootName = xmlRoot.getNodeName();
        NodeList children = xmlRoot.getChildNodes();
        Node child = null;
        String objectClass = "";

        // TODO (to AnTo) 9 Check, if there exists a better way to do this
        if (!config.isCreateLibraryHandlers()) {
            // Ignore libraryHandlers (typers and backends)
        	String stBlockLibType = "GABlockLibrary";
        	if (config.isInterfacePattern()) {
        		stBlockLibType += "Impl"; 
        	}
			if ("libraryHandler".equals(rootName)
					&& stBlockLibType.equals(model.getClass().getSimpleName())) {
				return null;
			}
        }
        
        for (int i = 0; i < children.getLength(); i++) {
            if (!isTextOrComment(children.item(i).getNodeName())) {
                objectClass = children.item(i).getNodeName();

                // child contains the type and attributes of the result
                child = children.item(i);

                String resultClassPath = classLocation.get(objectClass);

                if (resultClassPath == null) {

                    // If no match could be found in classLocation Map, then the
                    // classPath has been fully specify by the user
                    resultClassPath = objectClass;

                }

                if (resultClassPath != null && (!"".equals(resultClassPath))) {
                    // special treatment : enum types in java cannot be handled
                    // via the same
                    // mechanism as the standard classes.

                    String type = getItemTest(child, "type");

                    if ("gaxml:enum".equals(type)) {
                        Object tmpEnumValues = null;
                        tmpEnumValues = PrivilegedAccessor
                                .getObjectForClassName(resultClassPath);

                        if (tmpEnumValues instanceof Object[]) {
                            Object[] enumValues = (Object[]) tmpEnumValues;
                            for (int enumCount = 0; enumCount < enumValues.length; enumCount++) {
                                if (enumValues[enumCount].toString().equals(
                                        getItemTest(child, "name"))) {
                                    result = enumValues[enumCount];

                                    if (child.hasChildNodes()) {
                                        for (int cptr = 0; cptr < child
                                                .getChildNodes().getLength(); cptr++) {
                                            if (!isTextOrComment(child
                                                    .getChildNodes().item(cptr)
                                                    .getNodeName())) {
                                                Object childAttribute = processCurrentElement(
                                                        child.getChildNodes()
                                                                .item(cptr),
                                                        null, false);
                                                PrivilegedAccessor.setValue(
                                                        result,
                                                        child.getChildNodes()
                                                                .item(cptr)
                                                                .getNodeName(),
                                                        childAttribute, true);
                                            }
                                        }
                                    }
                                    PrivilegedAccessor.setValue(parent,
                                            rootName, result, true);
                                }
                            }
                        }
                    } else {
                        // result is the computed object
                        result = PrivilegedAccessor
                                .getObjectForClassName(resultClassPath);

                        setAttributes(result, child);

                        if (parent != null) {
                            PrivilegedAccessor.setValue(parent, rootName,
                                    result, true);
                        }

                        if (result instanceof GAModelElement
                                && parent instanceof GAModelElement
                                && (!(model instanceof GABlockLibrary))
                                && parent != null) {
                            // GAModelElement : for instance : signals, ports,
                            // parameters...
                            if (config.isSetParentsOnRead()) {
	                            ((GAModelElement) result)
	                                    .setParent((GAModelElement) parent);
                            }
	                        addToIdMap(ObjectAccessor.getIdFieldValue(result), (GAModelElement) result);
                        }

                        // if has node, then result has children
                        if (child.hasChildNodes()) {
                            for (int j = 0; j < child.getChildNodes()
                                    .getLength(); j++) {
                                if (child.getChildNodes().item(j) != null
                                        && !isTextOrComment(child
                                                .getChildNodes().item(j)
                                                .getNodeName())) {

                                    String attributeName = child
                                            .getChildNodes().item(j)
                                            .getNodeName();
                                    Object childObj = null;
                                    if (result instanceof GAModelElement) {
                                        childObj = processCurrentElement(child
                                                .getChildNodes().item(j),
                                                (GAModelElement) result, 
                                                false);
                                    } else {
                                        childObj = readObject(child.getChildNodes()
                                                .item(j), null);
                                    }
									// Assign the child object to the field,
									// unless null was returned. In that case we
									// assume that it is already correctly
									// initialised (null or non-null)
                                    if (childObj != null) {
	                                    PrivilegedAccessor.setValue(result,
	                                            attributeName, childObj, true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Reads a Pointer (reference) from a given XML Root
     */
    protected void readPointer(Node xmlRoot, 
    			GAModelElement parent, boolean isCollectionElement,
    			boolean isDependentModelRef) {
        String pointer = xmlRoot.getTextContent().toString().trim();
        Integer intPtr = new Integer(pointer);

        if (intPtr < 1) {
            EventHandler.handle(EventLevel.ERROR, "ModelReader", "",
                    "Illegal pointer value \"" + pointer + "\"",
                    "The node which causes the problem is : "
                            + "\n rootName : " + xmlRoot.getNodeName()
                            + "\n rootType : " + xmlRoot.getNodeType()
                            + "\n root's parent : "
                            + parent.getReferenceString());
            return;
        }

        String pointerValue = intPtr.toString();
        String nodeName;
        
        // in case of collection the name of a parent attribute is two levels up
        // NB! here we assume that there is never two collections inside 
        // each-other
        if (isCollectionElement) {
        	nodeName = xmlRoot.getParentNode().getNodeName();
        } else {
        	nodeName = xmlRoot.getNodeName();
        }

        String[] value = { nodeName, pointerValue };
        Object[] pointerKey = { parent, nodeName };

        if (isDependentModelRef) {
            pointersDM.add(new Pair<Object[], String[]>(pointerKey, value));
        } else {
            pointers.add(new Pair<Object[], String[]>(pointerKey, value));
        }

    }

	/**
	 * Method which reads the transformations of the model and stores them in
	 * the model object.
	 * 
	 * TODO Why isn't this handled generically? Transformation is just a child
	 * of the Model. AnTo 091227.
	 * 
	 * @param root
	 *            Transformation root node.
	 */
    @SuppressWarnings("unchecked")
	protected void readTransformation(Node root) {

        if (root != null) {
            NodeList transfos = parser.selectNodes(root, ".//Transformation");

            // store in the model the existing transformations
            for (int i = 0; i < transfos.getLength(); i++) {
                Node transTmp = transfos.item(i);
                String stRead = getItemTest(
                        transTmp, "readTime");
                String stWrite = transTmp.getAttributes()
                        .getNamedItem("writeTime").getNodeValue();
                String toolName = getItemTest(
                        transTmp, "toolName");
                Transformation tmpTransf = new Transformation(stRead, stWrite, toolName);
            	if (config.isECoreCollections()) {
            		// Get the EList by the getter and then add
            		ObjectAccessor.getEListFromField(model, "transformations").add(tmpTransf);
            	} else {
            		// Go through the model class
                	model.getTransformations().add(tmpTransf);
            	}
            }
        }
    }

    /**
     * Resolves the pointers which were read during the reading of the xml file.
     * If a node with pointer attributes is read, the pointer target object may
     * not be created yet. Such pointer values are stored in the pointers array
     * and after reading all model elements the id values are mapped to
     * corresponding object pointers
     */
    private void resolvePointers() {

        // Resolve pointers to the current model
		// Note: If we are at step 2 of reading a dependent model pair and the
		// pointers map of this model is initialised with the dependent model's 
        // references to the current model, then also those get resolved here
        resolvePointers1(pointers, idMap);
        
        // Resolve pointers to the dependent model
		// Note: This is only done on step2, because the dependent model's
		// references to the current model get checked by the code in the
		// beginning of this method
        if (doubleModeStep2) {
            resolvePointers1(pointersDM, idMapDM);
        }
    }

	/**
	 * Utility method to resolve pointers in one pointer map-object map pair
	 */
	private void resolvePointers1(
			List<Pair<Object[], String[]>> ptrs, 
			Map<Integer, GAModelElement> ids) {
		int idValue = 0;
		Object[] modelElement;
		String[] values;
		for (Pair<Object[], String[]> pair: ptrs) {
			modelElement = pair.getLeft();
			values = pair.getRight();
            idValue = Integer.parseInt(values[1]);
            if (idValue != 0 && ids.containsKey(idValue)) {
                PrivilegedAccessor.setValue(modelElement[0], values[0], 
                		ids.get(idValue), true);
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        "ModelReader.resolvePointers", "",
                        "Unresolved pointer \"" + values[1]
                                + "\" \n in file " + inputFile, "");
            }
        }
	}

    /**
     * Writes attributes from an XML node to a Java object.
     * @param currentModelObject
     *            active model element.
     * @param xmlRoot
     *            current node in the XML tree.
     */
    protected void setAttributes(Object currentModelObject, Node xmlRoot) {
        for (int i = 0; i < xmlRoot.getAttributes().getLength(); i++) {
            PrivilegedAccessor.setValue(currentModelObject, xmlRoot
                    .getAttributes().item(i).getNodeName(), xmlRoot
                    .getAttributes().item(i).getNodeValue(), false);
        }
    }

	/**
	 * Assumes that the given element has an "elements" field and sets it to a
	 * given collection
	 * 
	 * @param currentModelObject
	 *            active model element.
	 * @param elements
	 *            given collection.
	 */
    protected void setElements(Object currentModelObject, Object elements) {
        PrivilegedAccessor.setValue(currentModelObject, "elements", elements, true);
    }

    /**
     * Adds an element to the idMap and update the global lastId.
     * 
     * @param Id
     *            Id of the element.
     * @param element
     *            GAModelElement.
     */
    protected void addToIdMap(int id, GAModelElement element) {
        idMap.put(id, element);
        if (config.isIdHandlingByModel()) {
            model.updateLastId(element);
        }
    }

    /**
     * @return the model
     */
    public Model getModel() {
        return model;
    }
}
