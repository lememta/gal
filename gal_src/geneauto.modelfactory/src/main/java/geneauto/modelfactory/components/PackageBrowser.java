/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/components/PackageBrowser.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:24:08 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.components;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * This class allows to browse a package and to get the classes which are
 * inside. It is useful to allow a clean package hierarchy in the development
 * project.
 */
public class PackageBrowser {

    /**
     * Lists all of the classes of the package given as a parameter.
     * 
     * @param packageName
     *            The name of the package we want to browse.
     * @return The list of the classes which exist within this package.
     */
    @SuppressWarnings("unchecked")
	public static synchronized List<Class> getClassesForPackage(
            String packageName) {
    	
        ArrayList<File> directories = new ArrayList<File>();
        ArrayList<JarFile> jars = new ArrayList<JarFile>();

        // Initialise the class loader
        ClassLoader loader = PackageBrowser.class.getClassLoader();
        if (loader == null) {
            EventHandler.handle(EventLevel.ERROR, "PackageBrowser", "",
                    "An error occurred while trying to get the classes contained in package "
                            + packageName + ".",
                    "System ClassLoader could not be found.");
        }

		// Fetch the resource. We assume that the first resource containing the
		// given package path is the right one and we will not look for others
		// further down the classpath (classes with same names are hidden by the
		// first matches anyway)
        String path = packageName.replace('.', '/');
        URL resource;
        try {

            resource = loader.getResource(path);
            
            if (resource == null) {
                EventHandler.handle(EventLevel.ERROR, "PackageBrowser", "",
                        "Failed to load a resource for package: "
                                + packageName);
				// Return empty list to continue and try to find more errors.
				// The tool execution will be stopped later, because the Error
				// flag was set
                return new ArrayList<Class>(); 
            }
           

            String packagePath = URLDecoder.decode(resource.getPath(), "UTF-8");

            if (packagePath != null) {
                int iBegin = packagePath.indexOf(':');
                int iEnd = packagePath.indexOf('!');
                if(iBegin != -1 && iEnd != -1) {
                    jars.add(new JarFile(packagePath.substring(iBegin+1,iEnd)+File.separator));
                }
                else {
                    directories.add(new File(packagePath));
                }
            } else {
                EventHandler.handle(EventLevel.ERROR, "PackageBrowser", "",
                        "A problem was encountered as the browser was reading the package "
                                + packageName + ".", "Package path not found.");
            }            
                
        } catch (IOException e) {
            EventHandler.handle(EventLevel.ERROR, "PackageBrowser", "",
                    "A problem was encountered as the browser was reading the package "
                            + packageName + ".", e.getMessage());
        }

        ArrayList<Class> classes = new ArrayList<Class>();
        // For each directory, capture all the .class files
        for (File directory : directories) {
            if (directory.exists()) {
                // Get the list of the files contained in the package
                String[] files = directory.list();
                for (String file : files) {
                    // we are only interested in .class files
                    if (file.endsWith(".class")) {
                        // removes the .class extension
						String className = packageName + '.'
								+ file.substring(0, file.length() - 6);
                        try {
							classes.add(Class.forName(className));
							
                        } catch (ClassNotFoundException e) {
							EventHandler.handle(
											EventLevel.ERROR, "PackageBrowser", "",
											"Class "
													+ className
													+ " could not be read from the classpath.",
											e.getMessage());
                        } catch (Exception e) {
							EventHandler.handle(
									EventLevel.CRITICAL_ERROR, "PackageBrowser", "",
									"Class "
											+ className
											+ " could not be read from the classpath.",
									e.getMessage());                            
                        	return null;
                        }
                    }
                }
            }
        }
        
        for (JarFile jar : jars) {
        	classes.addAll(getClassesFromJar(packageName, jar));

        }
        return classes;
    }
        
	/**
	 * Lists all classes in a supplied jar file, where the path matches the
	 * given package name.
	 * 
	 * @param packageName
	 *            Name of the package to be listed
	 * @param jarFileName
	 *            Name of the jar archive, where the package is supposed to be
	 * @return list of classes
	 */
    @SuppressWarnings("unchecked")
	private static List<Class> getClassesFromJar(String packageName, JarFile jar) {
		List<Class> classes = new ArrayList<Class>();
		for (Enumeration e = jar.entries(); e.hasMoreElements();) {
            JarEntry current = (JarEntry) e.nextElement();
            if ((current.getName().endsWith(".class"))
                    && (current.getName().length() > packageName.length())) {
                if ((current.getName().substring(0, packageName.length())
                        .replace("/", ".").equals(packageName))) {
                    // if all is ok, we create the class.
                    try {
                        classes.add(Class.forName(current.getName().replaceAll(
                                "/", ".").replace(".class", "")));
                    } catch (ClassNotFoundException e1) {
                        EventHandler.handle(EventLevel.ERROR, "PackageBrowser", "",
                            "Class " + current.getName() 
                            + " could not be read from the classpath.", e1.getMessage());
                    }
                }
            }
        }
		return classes;
	}
}
