/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/components/ModelWriter.java,v $
 *  @version	$Revision: 1.57 $
 *	@date		$Date: 2011-07-07 12:24:07 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.components;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.utilities.ObjectAccessor;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;
import geneauto.models.genericmodel.NodeTypeMap;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.TimeUtils;
import geneauto.utils.tuple.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Writes GAModelElement tree to XML file.
 */
public class ModelWriter {

    /** Root node of the processed model */
    protected Model model;

	/**
	 * A flag which tells that we are at the second step of writing two
	 * dependent models
	 */
    private boolean doubleModeStep2 = false;

	/**
	 * A flag which tells that we are at the second step of writing two
	 * dependent models and only the second model is saved. The first one is
	 * assumed to have not changed
	 */
	private boolean doubleModePartialStep2;

	/**
     * Useful to define if a GAModelElement must be written according to its
     * real type or to its super class. Used mainly for elements of type BLOCK.
     */
    protected Map<String, NodeTypeMap> nodeNameMap;

    private Map<String, String> exclusions = new HashMap<String, String>();

    /**
     * A map of all GAModelElement type elements that were saved as objects
     */
    private Map<Integer, GAModelElement> savedObjects = new HashMap<Integer, GAModelElement>();

    /**
     * A map for saved objects in the dependent model
     */
    private Map<Integer, GAModelElement> savedObjectsDM = new HashMap<Integer, GAModelElement>();

    /**
     * A map of all GAModelElement type elements that were saved as pointers The
     * key is element id Map value is a pair consisting of - element saved as
     * pointer - an element in XML tree that last referred by the pointer to the
     * given element
     */
    private Map<Integer, Pair<GAModelElement, Element>> savedPointers = new HashMap<Integer, Pair<GAModelElement, Element>>();

    /**
     * A map for saved pointers to the dependent model
     */
    private Map<Integer, Pair<GAModelElement, Element>> savedPointersDM = new HashMap<Integer, Pair<GAModelElement, Element>>();

    /**
     * A map of all GAModelElement type elements that were read during reading the dependent model
     */
    private Map<Integer, GAModelElement> readObjectsDM;

    /**
     * Reference to the configuration object
     */
    protected ModelFactoryConfig config;

    public void setDoubleModeStep2() {
		this.doubleModeStep2 = true;
	}

	public void setDoubleModePartialStep2() {
		this.doubleModePartialStep2 = true;
	}

	public Map<Integer, GAModelElement> getSavedObjects() {
		return savedObjects;
	}

	public void setSavedObjects(Map<Integer, GAModelElement> savedObjects) {
		this.savedObjects = savedObjects;
	}

	public Map<Integer, GAModelElement> getSavedObjectsDM() {
		return savedObjectsDM;
	}

	public void setSavedObjectsDM(Map<Integer, GAModelElement> savedObjectsDM) {
		this.savedObjectsDM = savedObjectsDM;
	}

	public Map<Integer, Pair<GAModelElement, Element>> getSavedPointers() {
		return savedPointers;
	}

	public void setSavedPointers(
			Map<Integer, Pair<GAModelElement, Element>> savedPointers) {
		this.savedPointers = savedPointers;
	}

	public Map<Integer, Pair<GAModelElement, Element>> getSavedPointersDM() {
		return savedPointersDM;
	}

	public void setSavedPointersDM(
			Map<Integer, Pair<GAModelElement, Element>> savedPointersDM) {
		this.savedPointersDM = savedPointersDM;
	}

	public void setReadObjectsDM(Map<Integer, GAModelElement> readObjectsDM) {
		this.readObjectsDM = readObjectsDM;
	}

	/**
	 * Constructor with a ModelFactoryConfig object and an exclusions list
	 * parameters
	 * 
	 * @param config
	 * @param exclusions
	 */
    public ModelWriter(ModelFactoryConfig config, Map<String, String> exclusions) {
		super();
		this.config = config;
		this.exclusions = exclusions;
	}

	/**
     * Main method of this class.
     * 
     * @param parser
     *            XMLParser which is used to save the model into the xml file.
     * @param mod
     *            Model which must be written into the xml file.
     * @param outputFile
     *            Path and name of the file which must be written. If it does
     *            not exist, it is created. If it exists, it is overwritten.
     * @param nodenamemap
     *            Class containing the model elements which are processed
     *            differently from the others.
     */
    public synchronized void writeModel(XmlParser parser, Model mod,
            String outputFile, Map<String, NodeTypeMap> nodenamemap) {

        // we can not continue when there is no model
        if (mod == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "ModelWriter.writeModel", "",
                    "No model provided for writing!", "");
            return;
        }
        
        model = mod;
        nodeNameMap = nodenamemap;
        
        // make sure that ID-map is up to date before starting to write
        if (config.isIdHandlingByModel()) {
        	model.updateLastId();
        	
			// We assume that at this stage all ids have been correctly assigned
			// to elements. Disable automatic id management, as it can get
			// distorted, when new elements appear during writing the elements.
			// A particular issue is currently related to storing datatypes.
			// Sometimes the getDataType method computes a value to the dataType
			// field, when the parent object was already stored with no
			// dataType! A similar case can still confuse the checking of
			// written pointers, if it happens *before* the element writing.
			// However, normally all pointed elements are created before model
			// storage step.
        	model.setNoNewId(true);
        }

        // if the directory does not exist, it is necessary to create it.
        File outFile = new File(outputFile);
        String filePath = outFile.getAbsolutePath();
        String fileName = outFile.getName();
        filePath = filePath.replace(fileName, "");
        try {
            outFile = new File(filePath);
            outFile.mkdirs();
            outFile = new File(filePath + fileName);
            outFile.createNewFile();
            outFile.setWritable(true);
        } catch (FileNotFoundException e) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "File not found: ", e);
        } catch (IOException e) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "IO error: ", e);
        }

        try {
            // initialise the document builder.
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = parser.createNewDocument("GAModel");

            DOMImplementation impl = builder.getDOMImplementation();
            doc = impl.createDocument(null, null, null);

            // write the document nodes.
            Element root = writeRoot(doc);

            writeHistory(doc, root);

            List<?> elements;
            if (config.isECoreCollections()) {
            	elements = ObjectAccessor.getEListFromField(model, "elements");
            } else {
            	elements = model.getElements();
            }
            
            if (elements != null) {
                for (Object gaEl : elements) {
                    // root node must be object
                    writeModelElement(doc, root, gaEl, ObjectStorageMode.OBJECT);
                }
            } else {
                // we do not save empty model
            }

            // transform the Document into a String
            DOMSource domSource = new DOMSource(doc);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
                    "yes");

            // set transformers properties.
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            // TODO: (TF:97) the requirements demand UTF-8
            transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            FileWriter writer = new FileWriter(outputFile);
            StreamResult streamResult = new StreamResult(writer);
            transformer.transform(domSource, streamResult);
            writer.close();

        } catch (ParserConfigurationException e) {
            EventHandler.handle(EventLevel.ERROR, "ModelWriter", "",
                    "An error occurred while writing the XML file.", e
                            .getMessage());
        } catch (TransformerConfigurationException e) {
            EventHandler.handle(EventLevel.ERROR, "ModelWriter", "",
                    "An error occurred while writing the XML file.", e
                            .getMessage());
        } catch (TransformerException e) {
            EventHandler.handle(EventLevel.ERROR, "ModelWriter", "",
                    "An error occurred while writing the XML file.", e
                            .getMessage());
        } catch (IOException e) {
            EventHandler.handle(EventLevel.ERROR, "ModelWriter", "",
                    "An error occurred while writing the XML file.", e
                            .getMessage());
        }

        // check if the model was correctly written
        checkWriteResult();
    }

    /**
     * Writes the History of a model into the XML Document.
     * 
     * @param doc
     *            The document in which the History will be written.
     * @return the History Node
     */
    private synchronized Element writeHistory(Document doc, Element parent) {
        // set writing time of existing transformations, where write time is
        // empty
    	
        List<?> transformations;
        if (config.isECoreCollections()) {
        	transformations = ObjectAccessor.getEListFromField(model, "transformations");
        } else {
        	transformations = model.getTransformations();
        }
    	
        for (Object t : transformations) {
        	String writeTime = (String) PrivilegedAccessor.getValue(t, "writeTime");
            if (writeTime == null || "".equals(writeTime)) {
            	PrivilegedAccessor.setValue(t, "writeTime", TimeUtils.getFormattedDate(), true);
            }
        }

		// TODO Preferably the root node should be handled via the same
		// mechanism as others and there would not be explicit mapping
		// here. AnTo 06.01.10
        
        Element history = doc.createElement("TransformationHistory");
        history.setAttribute("type", "gaxml:history");
        parent.appendChild(history);

        for (Object t : transformations) {
            Element historyNode = doc.createElement("Transformation");
            historyNode.setAttribute("toolName", (String) PrivilegedAccessor.getValue(t, "toolName"));
            historyNode.setAttribute("readTime", (String) PrivilegedAccessor.getValue(t, "readTime"));
            historyNode.setAttribute("writeTime", (String) PrivilegedAccessor.getValue(t, "writeTime"));
            history.appendChild(historyNode);
        }

        return history;
    }

    /**
     * Writes the Root of a model into the XML Document.
     * 
     * @param doc
     *            The document in which the Root will be written.
     * @return the Root Node
     * 
     */
    private synchronized Element writeRoot(Document doc) {
        String modelType = model.getClass().getSimpleName();
        
        modelType = trimImpl(modelType);        
        
        Element root = doc.createElement(modelType);
        doc.appendChild(root);
        
		// TODO Preferably the root node should be handled via the same
		// mechanism as others and there would not be explicit attribute mapping
		// here. AnTo 06.01.10

        root.setAttribute("type", "gaxml:model");

        root.setAttribute("lastSavedBy", (String) PrivilegedAccessor.getValue(model, "lastSavedBy"));
        root.setAttribute("lastSavedOn", (String) PrivilegedAccessor.getValue(model, "lastSavedOn"));
        root.setAttribute("modelName", (String) PrivilegedAccessor.getValue(model, "modelName"));
        // AnTo 14.12.09 - It seems redundant to duplicate the node name here
		// root.setAttribute("modelType", modelType );
        root.setAttribute("modelVersion", (String) PrivilegedAccessor.getValue(model, "modelVersion"));

        root.setAttribute("xmlns", "http://www.geneauto.org/" + modelType);

        root.setAttribute("xmlns:gaxml", "http://www.geneauto.org/GAXML");
        root.setAttribute("xmlns:gadt", "http://www.geneauto.org/GADataType");

        return root;
    }

    /**
     * Sets the attributes of XML element with values from a GAModelElement.
     * 
     * @param doc
     *            The document in which the content will be written.
     * @param parent
     *            The GAModelElement providing the data to store in the XML
     *            file.
     * @param element
     *            The GAModelElement providing the data to store in the XML
     *            file.
     * @param ObjectStorageMode
     */
    @SuppressWarnings("unchecked")
    private synchronized Element writeModelElement(Document doc,
            Element parent, Object element, ObjectStorageMode mode) {
        Element elementNode = null;

        // tag name of the newly created node
        String tagName = element.getClass().getSimpleName();
        
        tagName = trimImpl(tagName);

        if (mode == ObjectStorageMode.POINTER) {
            elementNode = makePointerNode(doc, parent, tagName, element);
        } else if (mode == ObjectStorageMode.OBJECT) {
            elementNode = makeObjectNode(doc, parent, tagName, element);

            
            /*
             * There is a special handling for String objects: 
             * single attribute with name "value" and value = object 
             * itself is created
             */
            if (element instanceof String) {
                elementNode.setAttribute("value", (String) element);
                return elementNode;
            }
            // TODO: revisit this and analyse complexity (ToNa 12/11/08)
            for (String tag : nodeNameMap.keySet()) {
                NodeTypeMap typeMapEntry = nodeNameMap.get(tag);
                if (typeMapEntry.getMap().values().contains(
                        element.getClass().getSimpleName())) {
                    tagName = tag;
                }
            }

            // get the attributes of the object
            List<Field> fields = PrivilegedAccessor.getClassFields(element
                    .getClass());

            for (Field field : fields) {
            	// Ignore static fields that are not enums
            	// Note: It is assumed that such fields can be always ignored. 
            	// If this is not the case then further analysis needs to be done
            	// For instance, currently this check avoids writing Ecore fields 
				// like "ZZZ_EDEFAULT". Otherwise, we need a different approach
				// there (AnTo 100107).
            	if (Modifier.isStatic(field.getModifiers()) && !field.getType().isEnum()) {
            		continue; // Skip over
            	}
            	
            	// Read value
                Object value = PrivilegedAccessor.getValue(element, field
                        .getName());

                if (value != null) {
                    // if the attribute is a collection, we create a collection
                    // node
                    if (value instanceof Collection) {
                        if (((Collection) value).size() > 0) {
                            writeCollection(doc, elementNode, field.getName(),
                                    ((Collection) value), element);
                        }
                    } else {
                        // else, we set all of its attributes, if they must not
                        // be excluded.
                    	
                    	String fieldName = field.getName();
                        
                        if (!((exclusions.containsKey(fieldName)) 
                        		&& ((exclusions.get(fieldName) == null) 
                        				|| (exclusions.get(fieldName).equals(value.toString()))))) {
                            // if the attribute must be stored into particular
                            // node

                            // 2 options :
                            // - field.getType contains geneauto (the class is
                            // from geneauto package)
                            // - other cases : simple type :String, int, boolean
                            // etc.
                        	
                            if (field.getType().toString().contains("geneauto")) {

                            	// Class is from a geneauto package                            	
                            	if (config.isGAReference(element, value)) {
                                    // Store as pointer
                                    writeObject(doc, ObjectStorageMode.POINTER,
                                            elementNode, element,
                                            element.getClass()
                                                    .getSimpleName() + "." + fieldName);
                            	} else {
                                    // Store as object
                                    writeObject(doc, ObjectStorageMode.OBJECT,
                                            elementNode, element,
                                            element.getClass()
                                                    .getSimpleName() + "." + fieldName);
                            	}

                            } else {
                            	
                            	// Class is not from a geneauto package                            	
                                String fieldValue = value.toString();
                                try {                                	
                                    elementNode.setAttribute(fieldName, fieldValue);
                                } catch (DOMException e) {
                                    EventHandler.handle(
                                            EventLevel.CRITICAL_ERROR,
                                            "ModelWriter.writeModelElement",
                                            "", "DOMException" 
                                            	+ "\n Field: " + fieldName
                                            	+ "\n Value: " + fieldValue
                                            		, e);
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "ModelWriter.writeModelElement", "", "Incorrect mode \""
                            + mode + "\", expected OBJECT or POINTER"
                            + "\n Model " + model.getModelName(), "");
            return null;

        }
        return elementNode;
    }

	/**
	 * Returns an Xml Node containing a collection. This node can have several
	 * children.
	 * 
	 * @param doc
	 *            Document instance which will allow us to create a node.
	 * @param parent
	 *            Parent node in XML tree
	 * @param fieldName
	 *            Name of the node which must be returned.
	 * @param value
	 *            List of all of the elements to put in the list.
	 * @param parentObject
	 *            Parent object of the collection. In non-Ecore models we decide
	 *            based on the parent object we can decide if we should store
	 *            elements as pointers or as objects
	 * 
	 * @return A node named fieldName, whose type is gaxml:collection,
	 *         containing several nodes.
	 */
    @SuppressWarnings("unchecked")
    private synchronized Element writeCollection(Document doc, Element parent,
            String fieldName, Collection value, Object parentObject) {
        Element result = null; // resulting tree node (collection)

        if (value != null && value.size() > 0) {
            result = doc.createElement(fieldName);
            result.setAttribute("type", "gaxml:collection");
            parent.appendChild(result);

            for (Object currElem : value) {
            	if (config.isGAReference(parentObject, currElem)) {
                    // Store as pointer
                    writeModelElement(doc, result, currElem,
                            ObjectStorageMode.POINTER);
            	} else {
                    // Store as object
                    writeModelElement(doc, result, currElem,
                            ObjectStorageMode.OBJECT);
            	}
            }
        }

        return result;
    }

	/**
	 * Creates a XML Node or a node attribute according to the mode. This mode
	 * can be : <br>
	 * <dd>- gaxml:object - in this case a node is created, containing a unique
	 * child.<br> <dd>- gaxml:pointer - in this case, a node is created,
	 * containing the id of a ModelElement.<br> <dd>- attribute - in this case,
	 * we store the className of the attribute value in the attribute
	 * attributeName.<br>
	 * 
	 * @param doc
	 *            Document instance which will allow us to create a node.
	 * @param mode
	 *            Mode to indicate how the object should be written
	 * @param parentElem
	 *            XML node in which the attribute must be written.
	 * @param parentObj
	 *            Object from which the field value is to be read. 
	 * @param fieldName
	 *            Name of the field of the parent which is written.
	 * @return
	 */
    @SuppressWarnings("unchecked")
    private synchronized Element writeObject(Document doc,
            ObjectStorageMode mode, Element parentElem, Object parentObj,
            String fieldName) {

        // Handle enums
        if (parentObj.getClass().isEnum()) {
			// Next check seems to effectively make sure that we are
			// on the enum constant level not one level above (field
			// level that contains the value) (AnTo 100123)
            if (fieldName.contains(parentObj.toString())) {
                parentElem.setAttribute("type", "gaxml:enum");
            }
            return null;
        }

        // The element is an instance of a normal class. 
		String fieldNameClean = trimQualifier(fieldName);
		Object childValue = PrivilegedAccessor.getValue(parentObj,
				fieldNameClean);
		
		// Store as an object or a pointer
        if (mode == ObjectStorageMode.OBJECT) {
            // The requested mode is "object"

            String childTag = childValue.getClass().getSimpleName();
            
            Element result = makeObjectContainer(doc, parentElem, fieldNameClean);

            childTag = trimImpl(childTag);

            Object childObj = PrivilegedAccessor.getValue(parentObj,
            		fieldNameClean);

            Element childElem = makeObjectNode(doc, result, childTag,
                    childObj);

            // Write fields of the given object
            List<Field> childFields = PrivilegedAccessor
                    .getClassFields(childObj.getClass());
            
            for (Field childField : childFields) {
            	String childFieldName = childField.getName();
            	
				// Check, if the child's field is an enum constant. In
				// that case we need to set the "gaxml:enum"
				// attribute in the child XML element and not go deeper.
            	
				// Maybe we can do the isEnum() check one level
				// above and avoid coming here? (AnTo 100123)                	
                if (childField.getType().isEnum()) {
					// Next check seems to effectively make sure that we are
					// on the enum constant level not one level above (field
					// level that contains the value). Is calling toString 
                	// the best way to do it?? (AnTo 100123)
                	
                	String childObjStr;
                	try {
                		childObjStr = childObj.toString();
                	} catch (Exception e) {
                		EventHandler.handle(EventLevel.ERROR, getClass().getSimpleName(), "", 
                				"Error when calling toString() on object of class:\n " 
                				+ childObj.getClass().getSimpleName(), e);
                		return null;
                	}
                    if (childFieldName.contains(childObjStr)) {
                        childElem.setAttribute("type", "gaxml:enum");
                        continue; // Skip the rest
                    }
                }
            	// Ignore static fields (enums have been dealt with above)
            	// Note: It is assumed that such fields can be always ignored. 
            	// If this is not the case then further analysis needs to be done
            	// For instance, currently this check avoids writing Ecore fields 
				// like "ZZZ_EDEFAULT". Otherwise, we need a different approach
				// (AnTo 100107).
            	if (Modifier.isStatic(childField.getModifiers())) {
            		continue; // Skip over
            	}
            	
                if (!"parent".equals(childFieldName)) {
                    Object value = PrivilegedAccessor.getValue(
                            childObj, childFieldName);
                    
                    if (value != null) {

                        if (!((exclusions.containsKey(childFieldName)) 
                        		&& ((exclusions.get(childFieldName) == null) 
                        			|| (exclusions.get(childFieldName)
                        					.equals(value.toString()))))) {

                            if (PrivilegedAccessor.hasSimpleType(value)) {
                            	String fieldValue = value.toString();
                                try {
                                	childElem.setAttribute(childFieldName, fieldValue);
                                } catch (DOMException e) {
                                    EventHandler.handle(
                                            EventLevel.CRITICAL_ERROR,
                                            "ModelWriter.writeModelElement",
                                            "", "DOMException" 
                                            	+ "\n Field: " + childFieldName
                                            	+ "\n Value: " + fieldValue
                                            		, e);
                                    return null;
                                }

                            } else {
                                // if the attribute is a collection, we
                                // create a collection node
                                if (value instanceof List) {
                                    writeCollection(doc, childElem, childFieldName, (List) value,
                                            childObj);
                                } else {
                                	if (config.isGAReference(childObj, value)) {
                                        // Store as pointer
                                        writeObject(doc,
                                                ObjectStorageMode.POINTER,
                                                childElem, childObj,
                                                (childTag + "." + childFieldName));
                                	} else {
                                        // Store as object
                                        writeObject(doc,
                                                ObjectStorageMode.OBJECT,
                                                childElem, childObj,
                                                (childTag + "." + childFieldName));
                                	}
                                }
                            }
                        }
                    }
                }
            }
            return result;

        } else if (mode == ObjectStorageMode.POINTER) {
            // The requested mode is "pointer"
			return makePointerNode(doc, parentElem, fieldNameClean,
					(GAModelElement) childValue);
			
        } else {
            // Error
        	EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getSimpleName(), "", 
    				"Unexpected storage mode:\n " 
    				+ mode.getClass().getName());
    		return null;
        }
    }

    /**
     * Checks the save result. Gives error message and breaks execution when -
     * not all objects were saved to file - a pointer was saved to file, but no
     * corresponding object
     * 
     * @return
     */
    private boolean checkWriteResult() {
    	
    	boolean success;

        // check for broken links in the current model 
		// Note: If we are at step 2 of writing a dependent model pair and the
		// pointers and objects maps of this model are initialised with the
		// dependent model's references to the current model, then also the
		// those get checked here
        success = checkPonters1(savedPointers, savedObjects);
        
        // Do not try further, if already failed
        if (!success) {
        	return false;
        }

		// check if save was complete
        Collection<GAModelElement> allElements = model.getAllElementByIdMap();
        if (allElements.size() != savedObjects.size()) {
            for (GAModelElement e : savedObjects.values()) {
                if (model.getElementById(e.getId()) == null) {
                    EventHandler.handle(EventLevel.ERROR,
                            "ModelWriter.checkWriteResult", "", "Object "
                                    + e.getShortReferenceString()
                                    + " saved to file, but does not seem "
                                    + "to exist in the model!" + "\n Model "
                                    + model.getReferenceString());
                }
            }
            for (GAModelElement e : model.getAllElementByIdMap()) {
                if (!savedObjects.containsKey((e.getId()))) {
                    EventHandler.handle(EventLevel.ERROR,
                            "ModelWriter.checkWriteResult", "", "Object "
                                    + e.getShortReferenceString()
                                    + " is not saved!" + "\n Model "
                                    + model.getReferenceString());
                }
            }
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "ModelWriter.checkWriteResult", "",
                    "Saved object count does not match that in the model! "
                            + "\n Objects in model: " + allElements.size()
                            + ", objects saved: " + savedObjects.size()
                            + "\n Model " + model.getReferenceString());
            success = false;
        }

        // Do not try further, if already failed
        if (!success) {
        	return false;
        }

        // Check for broken links to the dependent model        
		// Note: This is only done on step2, because the dependent model's
		// references to the current model get checked by the code in the
		// beginning of this method
        
        // Complete double model mode - both models were saved
        if (doubleModeStep2) {
            success = checkPonters1(savedPointersDM, savedObjectsDM);
        }

        // Partial double model mode - the first model is assumed not to have changed
        // we only need to check that the second model points to valid objects in the 
        // first model
        if (doubleModePartialStep2) {
            success = checkPonters1(savedPointersDM, readObjectsDM);
        }

        return success;
    }

	/**
	 * Check one pair of pointer - object maps
	 * @param ptrs
	 * @param objs
	 * @return true on success
	 */
	private boolean checkPonters1(
			Map<Integer, Pair<GAModelElement, Element>> ptrs,
			Map<Integer, GAModelElement> objs) {
		boolean success = true;
		for (Pair<GAModelElement, Element> ptr : ptrs.values()) {
            if (!objs.containsKey(ObjectAccessor.getIdFieldValue(ptr.getLeft()))) {
                String msg = "Broken pointer! \n Object "
                        + ptr.getLeft().getReferenceString()
                        + "\n was saved as a pointer, however, there is no "
                        + "object stored with corresponding ID."
                        + "\n Last referenced from "
                        + printElementPath(ptr.getRight());

                EventHandler.handle(EventLevel.ERROR,
                        "ModelWriter.checkWriteResult", "", msg);
                success = false;
            }
        }
		return success;
	}

    /**
     * Takes XML document as an input, creates new element with name nodeName
     * and makes the element of pointer type. The id of GAModelElement element
     * is added as node value and added to savedPointers map
     */
    private Element makePointerNode(Document doc, Element parentNode,
            String nodeName, Object obj) {

        if (obj instanceof GAModelElement) {
        	GAModelElement gaElem = (GAModelElement) obj;

        	// Check for orphans
			// NOTE: Parent in the GAModelElement sense is different from the parent in the XML sense!
            if (gaElem.getParent() == null && 
            		(gaElem.getModel() == null || !gaElem.isRootLevelElement())) {
                String parentsName = "";
                if (parentNode != null) {
                    parentsName = parentNode.getNodeName();
                }
                EventHandler.handle(EventLevel.ERROR,
                        "ModelWriter.makePointerNode", "",
                        "Orphan element: \"" + gaElem.getReferenceString() + "\""
                            + "\n Model: " + getModelRefStr(model)
                            + "\n Node: " + parentsName
                            + "\n Field: " + nodeName
                            + "\n XML full path: "
                            + geneauto.utils.XMLUtils.getPathString(parentNode) + "." + nodeName);
                return null;
            }
            
        	// Check for bad ids
            if (gaElem.getId() < 1) {
                String parentsName = "";
                if (parentNode != null) {
                    parentsName = parentNode.getNodeName();
                }
                EventHandler.handle(EventLevel.ERROR,
                        "ModelWriter.makePointerNode", "",
                        "Illegal pointer value: \"" + gaElem.getId() + "\""
                            + "\n Element: " + getElemRefStr(gaElem)
                            + "\n Model: " + getModelRefStr(model)
                            + "\n Node: " + parentsName
                            + "\n Field: " + nodeName
                            + "\n XML full path: "
                            + geneauto.utils.XMLUtils.getPathString(parentNode) + "." + nodeName);
                return null;
            }

            // Create element
            Element node = doc.createElement(nodeName);
            
            // Check, if it is a dependent model reference or not       
        	if (gaElem.getModel() == model) {
        		node.setAttribute("type", "gaxml:pointer");
                // memorise the pointer to check existence of a corresponding
                // object later
                savedPointers.put(gaElem.getId(),
                        new Pair<GAModelElement, Element>(gaElem, parentNode));
        	} else {
        		node.setAttribute("type", "gaxml:dmref");
                // memorise the pointer to check existence of a corresponding
                // object later
                savedPointersDM.put(gaElem.getId(),
                        new Pair<GAModelElement, Element>(gaElem, parentNode));
        	}
            
            node.setTextContent(gaElem.getId() + "");
            parentNode.appendChild(node);
            return node;
            
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "ModelWriter.writeModelElement", "",
                    "Only elements of type GAModelElement can be stored as pointers"
                            + "\n Model " + getModelRefStr(model));
            return null;
        }

    }

    /**
     * Takes XML document as an input, creates new element with name nodeName
     * and makes the element of object type.
     * 
     * The id of GAModelElement element is added to savedObjects map
     */
    private Element makeObjectContainer(Document doc, Element parent,
            String nodeName) {
        try {
            Element node = doc.createElement(nodeName);
            node.setAttribute("type", "gaxml:object");
            parent.appendChild(node);
            return node;
        } catch (DOMException e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "ModelWriter", "",
                    "Error creating node: " + nodeName, e);
            return null;
        }

    }

    /**
     * Takes XML document as an input, creates new element with name nodeName
     * and sets it under the parent
     * 
     * The id of GAModelElement element is added to savedObjects map
     */
    private Element makeObjectNode(Document doc, Element parentElem,
            String nodeName, Object obj) {
    	
    	Element node;
    	
        try {
        	node = doc.createElement(nodeName);
        } catch (DOMException e) {
            EventHandler.handle(
                    EventLevel.CRITICAL_ERROR,
                    "ModelWriter.makeObjectNode",
                    "", "DOMException" 
	                	+ "\n Element: " + nodeName
	                	+ "\n Parent: " + parentElem.getNodeName()
                    		, e);
            return null;
        }
        
        parentElem.appendChild(node);
        if (obj instanceof GAModelElement) {
        	int id = ObjectAccessor.getIdFieldValue(obj);
            if (savedObjects.containsKey(id)) {
                GAModelElement savedObj = savedObjects.get(id);
                if (savedObj == obj) {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            "ModelWriter.makeObjectNode", "",
                            "An attempt to save object "
                                    + getElemRefStr(savedObj)
                                    + " twice" + "\n Model "
                                    + getModelRefStr(model));
                } else {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            "ModelWriter.makeObjectNode", "",
                            "An attempt to save objecs with duplcate id"
                                    + "\n saved object: "
                                    + getElemRefStr(savedObj)
                                    + "\n saved object: "
                                    + getElemRefStr((GAModelElement) obj)                                            
                                    + "\n Model " + getModelRefStr(model));
                }

            }
            
            savedObjects.put(ObjectAccessor.getIdFieldValue(obj),
                    (GAModelElement) obj);        	
        }

        return node;
    }

    /**
     * @param e GAModelElement
     * @return string identifying the object for error reporting
     */
	private String getElemRefStr(GAModelElement e) {
    	if (config.isNoDirectAccess()) {
			return "<" + e.getClass().getSimpleName()
			+ ": " + "name="
			+ ObjectAccessor.getNameFieldValue(e)
			+ ", " + "id=" + ObjectAccessor.getIdFieldValue(e) + ">";

    	} else {
    		return e.getShortReferenceString();
    	}
	}
    
    /**
     * @param m model
     * @return string identifying the model object for error reporting
     */
	private String getModelRefStr(Model m) {
    	if (config.isNoDirectAccess()) {
			return "name="
					+ PrivilegedAccessor.getValue(m,
							"modelName") + ", type="
					+ m.getClass().getSimpleName();
    	} else {
    		return m.getReferenceString();
    	}
	}

    /**
     * Takes DOM Element as an input and prints tag names of all elements up to
     * the root of the XML tree together with element id and name attributes
     * 
     * @param el
     * @return
     */
    private String printElementPath(Element el) {
        String result;

        result = "<" + el.getNodeName() + " id=" + el.getAttribute("id")
                + " name=" + el.getAttribute("name") + ">";

        for (Node parent = el.getParentNode(); parent != null; parent = parent
                .getParentNode()) {

            if (parent instanceof Element
                    && (!((Element) parent).getAttribute("id").isEmpty() 
                    || !((Element) parent).getAttribute("name").isEmpty())) {
                result = "<" + parent.getNodeName() + " id="
                        + ((Element) parent).getAttribute("id") + " name="
                        + ((Element) parent).getAttribute("name") + ">"
                        + result;
            } else if (parent instanceof Document) {
                return result;
            } else {
                result = "<" + parent.getNodeName() + ">" + result;
            }

        }

        return result;
    }

    /** If the interfacePattern is true, then trim "Impl" from the end of the name */
	private String trimImpl(String name) {
		if (config.isInterfacePattern() && name.endsWith("Impl")) {
        	name = name.substring(0, name.length()-4);
        }
		return name;
	}

    /**
     * @param qualifiedName
     * @return the name without the prefixing qualifier
     */
	private String trimQualifier(String qualifiedName) {
		return qualifiedName.substring(
		        qualifiedName.indexOf(".") + 1, qualifiedName.length());
	}

}
