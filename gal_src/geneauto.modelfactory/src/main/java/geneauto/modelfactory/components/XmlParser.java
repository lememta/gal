/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.modelfactory/src/main/java/geneauto/modelfactory/components/XmlParser.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:24:08 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.modelfactory.components;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Utility designed to manipulate xml documents.
 * <p>
 * Provides the following services :<br>
 * <dd> - parse a document with DOM API. <br>
 * <dd> - apply an xpath query on an XML Node giving only the node and the query
 * text.<br>
 * By default, this parser relays on Xerces 2.9.0 which provides all necessary
 * capabilities. If a different xml parser implementation is used, check what
 * level of compliance it as with latest W3C recommendations.
 * <p>
 */
public class XmlParser {

	/**
	 * Document factory used for document builder instanciation it remains
	 * unchanged for a DocumentBuilderFactory instance.
	 */
	private DocumentBuilderFactory factory = DocumentBuilderFactory
			.newInstance();

	/**
	 * xpath rsolver used to apply all xpath queries.
	 */
	private XPath pathSolver = XPathFactory.newInstance().newXPath();

	/**
	 * Transformer factory that has to provide the ability to save xml data to a
	 * file.
	 */
	private TransformerFactory transformerFactory = null;

	/**
	 * Parser builder static method path schema.
	 * <p>
	 * SchemaFileName is the path of the schema that will be used for
	 * validation. documentBuilderFactoryClassName allows to set the
	 * domDocumentbuilderFactory implementation by default Xerces implementation
	 * is used.
	 * 
	 * @return XmpParser
	 */
	public static final XmlParser newInstance() {
		String className = "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl";
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
				className);
		return new XmlParser();
	}

	/**
	 * Default constructor, note that the constructor is private. newInstance
	 * method must be used to obtain a new instance of XmlParser.
	 */
	private XmlParser() {
		factory = DocumentBuilderFactory.newInstance();
	}

	/**
	 * Parses a xml document file with DOM API.
	 * <p>
	 * 
	 * @param file
	 *            file path of the file to be parsed (can't be null)
	 * @return parsed xml Document main node
	 */
	public final Document parseWithDom(final String file) {
		Document doc = null;
		try {
			DocumentBuilder docBuilder = factory.newDocumentBuilder();
			doc = docBuilder.parse(new File(file));
		} catch (ParserConfigurationException e) {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, "XMLParser", "",
					"An error occured while parsing the input file " + file,
					e.getMessage());
		} catch (SAXException e) {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, "XMLParser", "",
					"An error occured while parsing the input file " + file,
					e.getMessage());
		} catch (IOException e) {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, "XMLParser", "",
					"An error occured while parsing the input file " + file,
					e.getMessage());
		}

		return doc;
	}

	/**
	 * Gets a node list from a Node context and a valid xpath query (passed as
	 * String).
	 * 
	 * @param source
	 *            xml Node on which the request is to be applied (can not be
	 *            null)
	 * @param path
	 *            text of query to apply (can not be null)
	 * @return Null if an error occured, the list of found nodes if query
	 *         succeeded
	 */
	public final NodeList selectNodes(final Node source, final String path) {
		NodeList set = null;
		try {
			Object obj = this.pathSolver.evaluate(path, source,
					XPathConstants.NODESET);
			set = (NodeList) obj;
		} catch (XPathExpressionException e) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, "XMLParser", "",
					"Error in Xpath Expression " + path + ".",
					e.getMessage());
		}
		return set;
	}

	/**
	 * Gets a node list from a Node context and a valid xpath query (passed as
	 * String).
	 * 
	 * @param source
	 *            xml Node on which the request is to be applied (can not be
	 *            null)
	 * @param path
	 *            text of query to apply (can not be null)
	 * @return Null if an error occured, the first xml Node found otherwise
	 */
	public final Node selectFirstNode(final Node source, final String path) {
		Node lResult = null;
		NodeList set = this.selectNodes(source, path);
		if (set != null && set.getLength() != 0) {
			lResult = set.item(0);
		}
		return lResult;

	}

	/**
	 * Saves a dom document to a file given as a path.
	 * 
	 * @param doc
	 *            dom document to save to a file (can not be null)
	 * @param filename
	 *            filename to save the xml document in (can not be null)
	 * @return false if an error occured, true otherwise
	 */
	public final boolean saveDocument(final Document doc, final String filename) {
		boolean returnvalue = false;
		try {
			if (transformerFactory == null) {
				transformerFactory = TransformerFactory.newInstance();
			}
			Transformer trans = transformerFactory.newTransformer();
			FileWriter writer = new FileWriter(filename);
			trans.transform(new DOMSource(doc), new StreamResult(
					writer));
			
			
		} catch (Exception e) {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, "XMLParser", "",
					"Error while saving document in file " + filename + ".",
					e.getMessage());
		}
		return returnvalue;
	}

	/**
	 * Creates a new document.
	 * 
	 * @param docElementName
	 *            The document's name.
	 * @return a new document
	 */
	public final Document createNewDocument(final String docElementName) {
		Document doc = null;
		try {
			DocumentBuilder docBuilder = factory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			doc.appendChild(doc.createElement(docElementName));
		} catch (ParserConfigurationException e) {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, "XMLParser", "",
					"Error while creating a new document.",
					e.getMessage());
		}
		return doc;
	}

	/**
	 * Returns the String value of the attribute given as a parameter.
	 * 
	 * @param currentNode
	 *            Node from which we want to get te attribute.
	 * @return A map which associates, for each attribute, its name and its
	 *         value.
	 */
	public final Map<String, String> selectNodeAttributes(Node currentNode) {
		Map<String, String> result = new HashMap<String, String>();
		
		NodeList attributeList = selectNodes(currentNode, "@*");
				
		if (attributeList != null && attributeList.getLength() != 0) {
			for (int i = 0; i < attributeList.getLength(); i++) {
				result.put(attributeList.item(i).getNodeName(), attributeList
						.item(i).getNodeValue());
			}
		}
		return result;
	}

	/**
	 * Returns the text value of a node without the begin void characters.
	 * 
	 * @param currentNode
	 *            Node from which we want to get tthe text value.
	 * @return The wished text.
	 */
	public final String selectNodeTextValue(Node currentNode) {
		String result = "";
		if (1 == currentNode.getChildNodes().getLength()) {
			result = currentNode.getTextContent();
			result = result.replaceAll("  ", "");
			result = result.replaceAll("\n", "");
			result = result.replaceAll("\t", "");
		}
		return result;
	}
}
