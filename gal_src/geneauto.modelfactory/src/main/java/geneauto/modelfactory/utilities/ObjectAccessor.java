package geneauto.modelfactory.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.PrivilegedAccessor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

/**
 * Utility class for accessing model objects indirectly via reflection
 */
public class ObjectAccessor {

	/**
	 * 
	 * For serialising models of EMF generated classes, it is required for the
	 * concrete collection to implement the InternalEList, interface. Such lists
	 * do not have a nullary constructor. Instead, a corresponding getter method
	 * should be used to obtain the handle to the collection.
	 * 
	 * @param parent
	 * @param fieldName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List getEListFromField(Object parent, String fieldName) {
		Class parentClass = PrivilegedAccessor.getClassForName(parent
				.getClass().getCanonicalName());
		Method getter = PrivilegedAccessor.getFieldGetter(parentClass,
				fieldName);
		if (getter == null) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR,
					"geneauto.modelfactory.utilities.ObjectAccessor", "",
					"Cannot obtain a getter method for a field of type EList<?>."
							+ "\n Class " + parentClass.getCanonicalName()
							+ "\n Field " + fieldName);
			return null;
		} else {
			try {
				getter.setAccessible(true);
				List collection = (List) getter.invoke(parent);
				return collection;
			} catch (Exception e) {
				EventHandler.handle(EventLevel.CRITICAL_ERROR,
						"geneauto.modelfactory.utilities.ObjectAccessor", "",
						"Cannot invoke getter method for a field of type EList<?>."
								+ "\n Class " + parentClass.getCanonicalName()
								+ "\n Field " + fieldName + "\n Method "
								+ getter.getName(), e);
				return null;
			}
		}
	}

	/**
	 * Same as initCollectionField(Object parent, Field field), but requires
	 * only the field's name instead of a Field object.
	 * 
	 * @see initCollectionField(Object parent, Field field)
	 * 
	 * @param parent
	 * @param field
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Collection initCollectionField(Object parent, String fieldName) {
		Field targetField = PrivilegedAccessor.getField(parent.getClass(), fieldName);
		return initCollectionField(parent, targetField);
	}
	
	/**
	 * Initialises a field that is of type collection. Certain types of
	 * collections are supported. Normally just a new collection is created and
	 * assigned to the field via reflection. In the case of EList (that has no
	 * nullary constructor) the parent class is expected to contain a getter
	 * method with a standard name for this field. This getter is invoked to 
	 * obtain a reference to the collection stored in the field.
	 * 
	 * @param parent
	 * @param field
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Collection initCollectionField(Object parent, Field field) {
		if (field == null) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, "initCollectionField()", "", 
					"Field is null!" +
					"\nParent's type: " + parent.getClass().getName());
			return null;
		}
		
		Collection collection;
		// determine the type of the field
		Class fieldType = PrivilegedAccessor
		        .getFieldType(field);
		field.setAccessible(true);

		if ("org.eclipse.emf.common.util.EList".equals(fieldType.getCanonicalName())) {
			// This will create the collection object and assign it to the field
			collection = ObjectAccessor.getEListFromField(parent, field.getName());
		} else if ("java.util.List".equals(fieldType.getCanonicalName())) {
			collection = (Collection) PrivilegedAccessor.getObjectForClassName("java.util.ArrayList");
		    
		} else if ("java.util.Set".equals(fieldType.getCanonicalName())) {
			collection = (Collection) PrivilegedAccessor.getObjectForClassName("java.util.HashSet");
		
		} else {
			try {
				collection = (Collection) PrivilegedAccessor
						.getObjectForClassName(fieldType.getCanonicalName());
			} catch (ClassCastException e) {
				EventHandler.handle(EventLevel.ERROR, "ObjectAccessor.initCollectionField()", "", 
						"\nThe field " + field.getName() + " of class " + parent.getClass().getCanonicalName() + " is " + fieldType.getCanonicalName() 
					  + "\nThis type is not a sub-type of java.util.Collection");
				return null;
			}
		}

		try {
			PrivilegedAccessor.setValue(parent, field.getName(), collection, true);
		} catch (Exception e) {
			EventHandler.handle(EventLevel.ERROR, "ObjectAccessor.initCollectionField()", 
					"", "Cannot instantiate a collection of class: " + collection.getClass().getName(), e);
			return null;
		}
		return collection;
	}
	
	/**
	 * 
	 * Return the value of field "id". The field is assumed to exist and to be
	 * of type int
	 * 
	 * @param obj
	 * @return
	 */
	public static int getIdFieldValue(Object obj) {
		return (Integer) PrivilegedAccessor.getValue(obj, "id");
	}

	/**
	 * 
	 * Return the value of field "name". The field is assumed to exist and to be
	 * of type String
	 * 
	 * @param obj
	 * @return
	 */
	public static String getNameFieldValue(Object obj) {
		return (String) PrivilegedAccessor.getValue(obj, "name");
	}
}
