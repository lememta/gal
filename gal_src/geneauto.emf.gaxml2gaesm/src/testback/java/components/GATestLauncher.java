package components;

import geneauto.emf.utilities.GAEMFConst;
import geneauto.launcher.GALauncher;

public class GATestLauncher extends GALauncher {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launcher = new GATestLauncherImpl();
		
		System.out.println(GAEMFConst.LICENSE_MESSAGE);
		
		launcher.gaMain(args, "GATestLauncher");
	}
	
}
