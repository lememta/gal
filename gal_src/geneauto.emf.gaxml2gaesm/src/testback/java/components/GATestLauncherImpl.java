package components;

import geneauto.launcher.GALauncherImpl;
import geneauto.utils.GASysInfo;

/**
 * This class has mostly the same behaviour as GALauncherImpl, except that it
 * explicitly tells the GASysInfo that the entire toolset is identified as a 
 * source build (i.e. not binary-jar). This allows to locate external resources 
 * like the block library, etc wrt. the source hierarchy. This is needed, if 
 * we want to launch Gene-Auto during the build process for testing. If the 
 * standard GALauncher(Impl) is called during the build process, then 
 * it will be identified as a binary build (it was already built as a 
 * prerequisite). But, as the entire build is not deployed yet and components
 * are executed from the local Maven repository, paths to fixed resources 
 * like block library are inconsistent.
 * 
 * Secondly, the toolset runs all the main toolset steps, except the printing
 * step. This does also not work due to inconsistent toolset hierarchy during 
 * the build process - the printer tool searches for library files. Ideally,
 * there should be a way to specify this location externally.
 */
public class GATestLauncherImpl extends GALauncherImpl {

	@Override
	protected void parseCmdLn(String[] args) {
		super.parseCmdLn(args);
		GASysInfo.setBinInstallPath(null);
	}
	
}
