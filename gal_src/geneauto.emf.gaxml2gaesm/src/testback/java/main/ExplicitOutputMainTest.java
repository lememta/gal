/**
 * 
 */
package main;

import geneauto.emf.utilities.GAEMFConst;
import geneauto.tsimulinkimporter.main.TSimulinkImporter;
import geneauto.utils.GAConst;

import java.io.File;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * @author andres
 *
 */
public abstract class ExplicitOutputMainTest extends TestCase {
	
	protected String inputRoot   = "src/test/resources/";
	
	protected String outputRoot  = "src/test/output/";
	
	protected String modelExt = "mdl";
	
	protected String modelName;
	
	protected String gaOutputDir;

	protected String gaOutputFileName;
	
	private File testOutputFile;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		gaOutputDir = outputRoot + modelName + "_ga/";
		
		String mdlDir = inputRoot;
		String[] args = new String[3];
		
		gaOutputFileName = 
				gaOutputDir + modelName + "_" + GAConst.SUFFIX_SLIM + "." + GAConst.EXT_GSM;
		
		// Generate Gene-Auto System Model file from a Simulink model
		args[0] = mdlDir + modelName + "." + modelExt;
		args[1] = "-o";
		args[2] = gaOutputFileName;
		TSimulinkImporter.main(args);
		
		// Test output file
		String testOutputFileName = 
				gaOutputDir + modelName + "_" + GAConst.SUFFIX_SLIM + "." + GAEMFConst.EXT_GAESM;
		testOutputFile = new File(testOutputFileName);
		// Delete the test output file, if existing
		if (testOutputFile.exists()) {
			testOutputFile.delete();
		}
	}

	/**
	 * Test method for {@link geneauto.emf.gaxml2ecore.main.GAXML2Ecore#main(java.lang.String[])}.
	 * @Test
	 */
	public void testMain() {
		String[] args = new String[3];
		
		args[0] = gaOutputFileName;
		args[1] = "-o";
		args[2] = testOutputFile.getPath();
		
		//GAXML2Ecore.main(args);
		
		// Check if the file exists
		Assert.assertTrue(testOutputFile.exists());
	}
}
