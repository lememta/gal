/**
 * 
 */
package main;

import geneauto.emf.gaxml2gaesm.main.GAXML2Ecore;
import geneauto.test.utilities.FileUtil;
import geneauto.utils.GAConst;
import junit.framework.TestCase;

import components.GATestLauncher;

/**
 */
public abstract class BasicMainTest extends TestCase {
	
	protected String inputRoot   = "src/test/resources/";
	
	protected String outputRoot  = "src/test/output/";
	
	protected String modelExt = "mdl";
	
	protected String modelName;
	
	protected String gaOutputDir;

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		gaOutputDir = outputRoot + modelName + "_ga/";
		
		String mdlDir = inputRoot;
		String[] args = new String[3];
		
		// Generate C code
		args[0] = mdlDir + modelName + "." + modelExt;
		args[1] = "-O";
		args[2] = gaOutputDir;
		GATestLauncher.main(args);		
	}

	/**
	 * Test method for {@link geneauto.emf.gaxml2ecore.main.GAXML2Ecore#main(java.lang.String[])}.
	 * @Test
	 */
	public void testMain() {
		String inputDir;
		String[] args = new String[1];
		
		inputDir  = gaOutputDir + "tmp/";
		
		int i = 1;
		String extPattern = GAConst.EXT_GSM.replaceAll("\\.", "\\\\.");
		String sfimPattern = ".+" + GAConst.SUFFIX_SFIM + "\\." + extPattern;		
		boolean stateflowExists = 
				FileUtil.findFileByPattern(sfimPattern, inputDir) != null;
		
		args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_SLIM + "." + GAConst.EXT_GSM;
		GAXML2Ecore.main(args);
		
		args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_FMPR + "." + GAConst.EXT_GSM;
		GAXML2Ecore.main(args);

		if (stateflowExists) {
			args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_SFIM + "." + GAConst.EXT_GSM;
			GAXML2Ecore.main(args);
		}
		
		args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_FMBS + "." + GAConst.EXT_GSM;
		GAXML2Ecore.main(args);
		
		args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_FMTY + "." + GAConst.EXT_GSM;
		GAXML2Ecore.main(args);
		
		// TODO Solve handling interconnected GASM-GACM models 
		// Because handling Stateflow models introduces dependencies between the 
		// system and code model that are currently not processable by the tool
		// we need to skip these
		if (stateflowExists) return;
		
		if (stateflowExists) {
			args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_SMPR + "." + GAConst.EXT_GSM;
			GAXML2Ecore.main(args);
		}
		
		args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_FMCG + "." + GAConst.EXT_GSM;
		GAXML2Ecore.main(args);

		if (stateflowExists) {
			args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_SMCG + "." + GAConst.EXT_GSM;
			GAXML2Ecore.main(args);
			
			args[0] = inputDir + (i++) + "-" + modelName + "_" + GAConst.SUFFIX_SMPO + "." + GAConst.EXT_GSM;
			GAXML2Ecore.main(args);
		}
		
	}
}
