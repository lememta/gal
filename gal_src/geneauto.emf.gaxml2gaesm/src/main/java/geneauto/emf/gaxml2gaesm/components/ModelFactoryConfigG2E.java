/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.gaxml2gaesm.components;

import geneauto.modelfactory.components.ModelFactoryConfig;


/**
 * A configuration class for converting models to XML.
 * The role of this class is to provide list of packages where 
 * model objects can be found.
 * 
 * It also provides list of class attributes that do not need to be stored 
 * in XML.
 * 
 * In case model objects are loaded somewhere else than the default location
 * (e.g. printer package) this class should be replaced buy an override.
 * 
 */
public class ModelFactoryConfigG2E extends ModelFactoryConfig {

	/**
     * Unique instance of this class
     */
	protected static ModelFactoryConfigG2E instance;
        
    /**
     * Returns the unique instance of this class.
     * If null, creates a new instance.
     * @return
     */
    public static ModelFactoryConfigG2E getInstance () {
        if (instance == null) {
            instance = new ModelFactoryConfigG2E();
        }
        return instance;
    }
    	
}
