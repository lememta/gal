/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.gaxml2gaesm.states;

import geneauto.emf.gaxml2gaesm.main.GAXML2EcoreMachine;
import geneauto.statemachine.State;


/**
 * Abstract base class for all states in the current elementary tool.
 *
 */
public abstract class ModelConverterState extends State {
	
    public ModelConverterState(String stateName) {
		super(stateName);
	}

	/**
     * returns reference to the instance of stateMachine
     * perfoms typecast to the correct machine type
     * 
     * @return SMPreProcessorMachine stateMachine
     */
    public GAXML2EcoreMachine getMachine(){
    	return GAXML2EcoreMachine.getInstance();
    }
  
}
