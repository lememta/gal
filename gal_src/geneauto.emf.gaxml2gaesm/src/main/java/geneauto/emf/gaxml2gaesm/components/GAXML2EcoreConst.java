/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.gaxml2gaesm.components;

public interface GAXML2EcoreConst {

    // Tool name
    public final static String TOOL_NAME = "GAXML2Ecore";

    // version number and date
    // NB! These must be updated each time the release is compiled
    public final static String TOOL_VER = "1.0.1 (SNAPSHOT)";
    public final static String TOOL_DATE = "09/01/2012";

	public enum SerializationMode {
		XML, XMI_INTRINSIC, XMI_EXTRINSIC
	}
	
	/** Specify serialisation mode */
	public final SerializationMode SERIALIZATION_MODE = 
		SerializationMode.XMI_INTRINSIC;
	
	/** Specify serialisation mode */
	public String[] EXCLUDED_EMF_FIELDS = {
			"eContainer",
			"eSettings",
			"EDELIVER",
			"EDYNAMIC_CLASS",
			"ELAST_NOTIFIER_FLAG",
			"ELAST_EOBJECT_FLAG",
			"EPROXY",
			"ESCAPE",
			"EVIRTUAL_SET",
			"EVIRTUAL_UNSET",
			"EVIRTUAL_GET",
			"EVIRTUAL_IS_SET",
			"EVIRTUAL_NO_VALUE",
			"$assertionsDisabled"
		};
	
}
