/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.gaxml2gaesm.states;

import geneauto.emf.gaxml2gaesm.components.GAXML2EcoreConst;
import geneauto.emf.models.gablocklibrary.impl.GablocklibraryFactoryImpl;
import geneauto.emf.models.gacodemodel.impl.GacodemodelFactoryImpl;
import geneauto.emf.models.gasystemmodel.impl.GasystemmodelFactoryImpl;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.utilities.ObjectAccessor;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gacodemodel.GACodeModel;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.tuple.Pair;
import geneauto.utils.union.Either;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;


/**
 * Converts the model from the Gene-Auto native format to an EMF-based format.
 */
// NOTE! This class is a twin version of:
//    geneauto.emf.ecore2gaxml.states.ConvertingModelState
// with only minor differences. If one of the classes, is modified, then the 
// other one should be updated also.
public class ConvertingModelState extends ModelConverterState {

    /** Map of converted objects, id->object */
    private Map<Integer, EObject> objects = new HashMap<Integer, EObject>();
    
	/**
	 * Map of references to objects as the target objects might not be created
	 * yet before the references are converted. The structure of the map entry
	 * is: 
	 * 		(object, field name)->Either<id, list of ids> 
	 * In case the field is not a collection, then only the id will be stored 
	 * (Left), otherwise a list of id-s (Right). The order of elements in the 
	 * list is the same one, as they appear in the collection.
	 */
    private Map<Pair<EObject, String>, Either<Integer, List<Integer>>> references = 
    	new HashMap<Pair<EObject, String>, Either<Integer, List<Integer>>>();
    
    /** Local reference to the ExcludedXMLAttributes map */
    private Map<String, String> defaultValuesMap = null;

    /** List of excluded field names
     * TODO Add class name also. E.g. The key could be className.fieldName 
     */
    private Set<String> excludedFieldsSet = null;

    public ConvertingModelState() {
        // set state name for messages
        super("Converting model");
        
        // initialise defalt values map
        defaultValuesMap = getMachine().getModelFactory().getExcludedXmlAttributes();
        
        // initialise excluded values set
        excludedFieldsSet = new HashSet<String>();
        
        // Gene-Auto transient fields
        excludedFieldsSet.add("parent");
        excludedFieldsSet.add("model");
		
        // EMF internal transient fields
        excludedFieldsSet.addAll(Arrays.asList(
        		GAXML2EcoreConst.EXCLUDED_EMF_FIELDS));

    }

    /**
     * Reads the Model.
     */
    public void stateExecute() {
    	
		geneauto.models.genericmodel.Model gaModel = 
			getMachine().getModelFactory().getModel();
		
		// Store the converted model's reference to the machine
		getMachine().setEModel(convertModel(gaModel));
    	
        // Go to next state 
        getMachine().setState(new SavingModelState());
    }

    /**
     * Converts the given GA native model to an Ecore based model node by node
     * @param gaModel
     * @return converted model
     */
	@SuppressWarnings("unchecked")
	private geneauto.emf.models.genericmodel.Model convertModel(
			geneauto.models.genericmodel.Model gaModel) {
		
		geneauto.emf.models.genericmodel.Model eModel;
		
		// Convert model
		Object newObj = convertNode(gaModel);
		if (gaModel instanceof GASystemModel) {
			eModel = (geneauto.emf.models.gasystemmodel.GASystemModel) newObj;
		} else if (gaModel instanceof GACodeModel) {
			eModel = (geneauto.emf.models.gacodemodel.GACodeModel) newObj;
		} else if (gaModel instanceof GABlockLibrary) {
			eModel = (geneauto.emf.models.gablocklibrary.GABlockLibrary) newObj;
		} else {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "",
					"Root node of the converted model has unsupported type: "
							+ gaModel.getClass().getCanonicalName());
			return null;
		}
		
		// Handle references
		for (Pair<EObject, String> key : references.keySet()) {
			EObject srcObj = key.getLeft();
			String fieldName = key.getRight();
			Either<Integer, List<Integer>> ref = references.get(key);
			
			// Single value
			if (ref.isLeft()) {
				int refId = ref.getLeft();
				Object targetObj = objects.get(refId);
				if (targetObj == null) {
					int srcId = ObjectAccessor.getIdFieldValue(srcObj);
					EventHandler.handle(EventLevel.ERROR, "", "", 
							"\nReferenced object with id: " + refId 
							+ " not found in the model. Referring object:" 
							+ "\n Id   : " + srcId
							+ "\n Class: " + srcObj.getClass().getCanonicalName()
							+ "\n Field: " + fieldName);
					continue;
				} else {
					setField(srcObj, fieldName, targetObj, null);					
				}
			
			// Collection
			} else {
				List<Integer> refIds = ref.getRight();
				//@SuppressWarnings("rawtypes")
				Collection collection = ObjectAccessor.initCollectionField(srcObj, fieldName);
				int i = -1;
				for (int refId : refIds) {
					i++;
					Object targetObj = objects.get(refId);
					if (targetObj == null) {
						int srcId = ObjectAccessor.getIdFieldValue(srcObj);
						EventHandler.handle(EventLevel.ERROR, "", "", 
								"\nReferenced object with id: " + refId 
								+ " not found in the model. Referring object:" 
								+ "\n Id   : " + srcId
								+ "\n Class: " + srcObj.getClass().getCanonicalName()
								+ "\n Field: " + fieldName
								+ "\n Collection position: " + i);
						continue;
					} else {
						collection.add(targetObj);	
					}
				}
				setField(srcObj, fieldName, collection, null);
			}
		}
		
		return eModel;
	}

	/**
	 * Convert a node in a GA native model to a node conforming to the Ecore metamodel.
	 * 
	 * @param srcObj
	 *            - source object
	 * @return converted object
	 */
	private Object convertNode(Object srcObj) {
		final String methodName = "convertNode";
		if (srcObj == null) {
			return null;
		}
		
		Object newObj;
		
		// Convert the object (root)
		
		// GAModels
		if (srcObj instanceof geneauto.models.genericmodel.Model) {
			
			geneauto.models.genericmodel.Model gaModel = (geneauto.models.genericmodel.Model) srcObj;
			
			if (gaModel instanceof geneauto.models.gasystemmodel.GASystemModel) {
				newObj = GasystemmodelFactoryImpl.eINSTANCE.createGASystemModel();
			} else if (gaModel instanceof geneauto.models.gacodemodel.GACodeModel) {
				newObj = GacodemodelFactoryImpl.eINSTANCE.createGACodeModel();
			} else if (gaModel instanceof geneauto.models.gablocklibrary.GABlockLibrary) {
				newObj = GablocklibraryFactoryImpl.eINSTANCE.createGABlockLibrary();
			} else {
				EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
						"Unupported type of model: " + gaModel.getClass().getCanonicalName());
				return null;
			}

		// All other objects	
		} else {

			// Check for enums
			if (srcObj.getClass().isEnum()) {
				EventHandler.handle(EventLevel.CRITICAL_ERROR, methodName, "", 
						"Enums are not expected here. They should be dealt with before this point." +
						"\n Class: " + srcObj.getClass().getCanonicalName());
				return null;	            	
	        
	        // Normal objects
			} else {
				String convertedClassName = convertClassName(srcObj.getClass().getCanonicalName(), true);
				if ("java.lang.String".equals(convertedClassName)) {
					// For strings it is safe to just copy reference
					newObj = srcObj;
				} else {
					// Some other kind of object. Try to create new object
					newObj = PrivilegedAccessor.getObjectForClassName(convertedClassName);
					
					boolean test = true;
				}
			}
		}
		
		if (newObj instanceof EObject) {
			EObject eObj = (EObject) newObj;
			// Convert the object's fields		
			if (!convertFields(srcObj, eObj)) {
				EventHandler.handle(EventLevel.ERROR, methodName, "", 
						"Failed to convert fields of object of type: " +
						"\n " + srcObj.getClass().getCanonicalName());
				return null;		
			}
		}
		
		return newObj;
	}

	/**
	 * Convert all fields of the given source object to a given target object of
	 * type EObject.
	 * 
	 * @param srcObj
	 * @param eObj
	 * @return success
	 */
	@SuppressWarnings({ "unchecked" /*, "rawtypes"*/ })
	private boolean convertFields(Object srcObj, EObject eObj) {
		final String methodName = "convertFields";
		for (Field srcField : PrivilegedAccessor.getClassFields(srcObj.getClass())) {
			String fieldName = srcField.getName();
			
			// Check, if this an excluded field 
			if (excludedFieldsSet.contains(fieldName)) {
				continue; // Skip field
			}
			
			Object value = PrivilegedAccessor.getValue(srcObj, fieldName);
			if (value == null) {
				continue; // Assume the default value will be correct
			}
			
			// Simple types
			if (PrivilegedAccessor.hasSimpleType(srcField)) {
				setField(eObj, fieldName, value, srcObj);
				
			// GAModelElements
			} else if (value instanceof geneauto.models.genericmodel.GAModelElement) {
				geneauto.models.genericmodel.GAModelElement valueElem = 
						(geneauto.models.genericmodel.GAModelElement) value;
				Object parent = valueElem.getParent();
				if (parent == srcObj || parent == null) {
					// Convert value of the node
					Object newChildObj = convertNode(value);				
					// Set the field's value
					setField(eObj, fieldName, newChildObj, srcObj);
					// Store objects for later reference resolving
					if (newChildObj instanceof EObject) {
						objects.put(valueElem.getId(), (EObject) newChildObj);
					}
				} else {
					// Store the reference for later resolving (the target object might not exist yet)
					Pair<EObject, String> key = new Pair<EObject, String>(eObj, fieldName);
					Either<Integer, List<Integer>> refVal = 
						new Either<Integer, List<Integer>>(valueElem.getId(), null);
					references.put(key, refVal);
				}
			
			// Collections
			} else if (value instanceof Collection) {
				
				Field targetField = PrivilegedAccessor.getField(eObj.getClass(), fieldName);				
				if (targetField == null) {
					EventHandler.handle(EventLevel.ERROR, "", "convertFields()", 
							"Field in the target model's meta-model not existing."
							+ "\n Source class: " + srcObj.getClass().getCanonicalName()
							+ "\n Field: " + fieldName);
					return false;
				}
								
				Collection collection = ObjectAccessor.initCollectionField(eObj, targetField);
				if (collection == null) {
					EventHandler.handle(EventLevel.ERROR, methodName, "",
							"Unable to obtain the target collection."
							+ "\n Source class: " + srcObj.getClass().getCanonicalName()
							+ "\n Target class: " + eObj.getClass().getCanonicalName()
							+ "\n Field: " + fieldName
							+ "\n Make sure that the respective field in the target meta-model " +
									"is indeed a collection!");
					return false;
				}
				
				List refIds = new ArrayList();
				for (Object srcChildObj : (Collection) value) {
					// Convert element of a collection (we assume that collections contain only children and not references)
					Object newChildObj = convertNode(srcChildObj);
					// If it was an instance of GAModelElement, then special reference handling follows
					if (srcChildObj instanceof geneauto.models.genericmodel.GAModelElement) {
						geneauto.models.genericmodel.GAModelElement childElem = 
								(geneauto.models.genericmodel.GAModelElement) srcChildObj;
						Object parent = childElem.getParent();
						if (parent == srcObj || parent == null) {		
							if (newChildObj instanceof EObject) {
								// Store object for later reference resolving
								objects.put(childElem.getId(), (EObject) newChildObj);
							}
							collection.add(newChildObj);
						} else {
							// Store the reference for later resolving (the target object might not exist yet)
							// The collection is not filled here
							refIds.add(childElem.getId());
						}
					} else {
						collection.add(newChildObj);
					}
				}				
				if (refIds.size() > 0) {
					Pair<EObject, String> key = new Pair<EObject, String>(eObj, fieldName);
					references.put(key, new Either<Integer, List<Integer>>(null, refIds));
				}
				
			// Enums	
			} else if (value.getClass().isEnum()) {
				String convertedClassName = convertClassName(value.getClass().getCanonicalName(), false);
	            Object tmpEnumValues;
	            tmpEnumValues = PrivilegedAccessor
	                    .getObjectForClassName(convertedClassName);
	            String name = ObjectAccessor.getNameFieldValue(value);
	            Object enumObj = null;
	            if (tmpEnumValues instanceof Object[]) {
	                for (Object enumVal : (Object[]) tmpEnumValues) {
	                    if (enumVal.toString().equals(name)) {
	                        enumObj = enumVal;
	                    }
	                }
	            }
	            if (enumObj == null) {
					EventHandler.handle(EventLevel.ERROR, "", "", 
							"Failed to match enum value." +
							"\n Class: " + srcObj.getClass().getCanonicalName() +
							"\n Value: " + name);
					return false;	            	
	            }
	            setField(eObj, fieldName, enumObj, srcObj);
	            
			// Maps
			} else if (value instanceof Map){
				continue; // Ignore all maps
				
			// All other objects
			} else {
				Object newObj = convertNode(value);
				// We know at this point that value isn't null.
				// If the converted value is null, it significates an error
				if (newObj == null) {
					EventHandler.handle(EventLevel.ERROR, methodName, "", 
							"Unable to convert the value of field" + 
							"\n Name :  " + fieldName +
							"\n Value type: " + value.getClass().getCanonicalName()
							);
					return false;
				}
				setField(eObj, fieldName, newObj, srcObj);
			}			
		}
		return true;
	}

	/**
	 * If the class name starts with prefix "geneauto.models", then converts it 
	 * according to the following pattern:
	 * 	  isImpl = true:
	 * 		geneauto.models.xxx.yyy.zzz -> geneauto.emf.models.xxx.yyy.impl.zzzImpl 
	 * 	  isImpl = false:
	 * 		geneauto.models.xxx.yyy.zzz -> geneauto.emf.models.xxx.yyy.zzz
	 * 
	 * Otherwise, returns the unchanged class name
	 *  
	 * @param srcClassName a canonical class name
	 * @param isImpl 	   
	 * @return converted class name
	 */
	private String convertClassName(String srcClassName, boolean isImpl) {
		String projPrefix = "geneauto";
		String modelsPrefix = projPrefix + ".models";
		
		if (!srcClassName.startsWith(modelsPrefix )) {
			return srcClassName;
		}
		
		// geneauto.models specific conversion
		String path1 = srcClassName.substring(projPrefix.length());
		int    pos   = path1.lastIndexOf(".");
		String classNameShort = path1.substring(pos);
		String classPathRel   = path1.substring(0, pos);
		if (isImpl) {
			return projPrefix + ".emf" + classPathRel + ".impl" + classNameShort + "Impl";
		} else {
			return projPrefix + ".emf" + classPathRel + classNameShort;
		}
	}

	/**
	 * Set generically (via reflection) a given field's value in a given target
	 * object to the given value
	 * 
	 * @param eObj
	 * @param fieldName
	 * @param value
	 * @param srcObj
	 *            Reference to the source object, from which the value is taken.
	 *            Can be null. It is used in error messages, if provided
	 */
	private void setField(EObject eObj, String fieldName, Object value, Object srcObj) {
		if (eObj == null) {
			String srcRef = srcObj == null ? "<null>" : srcObj.getClass().getCanonicalName();
			EventHandler.handle(EventLevel.ERROR, "setField()", "", "Target object is null."
					+ "\n Source class: " + srcRef
					+ "\n Field: " + fieldName
					+ "\n Value: " + value.toString());
		}
		
		// Check, if this field or field-value pair (a default value) is to be ignored.
		// Alternatively we could ask from the target metamodel, but this is more complicated
		// 		- no entry in the excludedFieldsMap means that we must copy this field,
		// 		- entry with "null" as value means that the field is to be ignored always
		//      - entry with non-null value means that the field is to be ingored, if the 
		//        supplied value matches with the one in the table (default value)
		if (value != null && defaultValuesMap != null) {
			if (defaultValuesMap.containsKey(fieldName)) {
				String defaultVal = defaultValuesMap.get(fieldName);
				if (defaultVal == null) {
					// Ignore this field
					return;
				} else {
					// Check value
					if (defaultVal.equals(value.toString())) {
						return; 
					}
				}
			}
		}
		
		// TODO (to AnTo) Implement setter caching
		Method setter = PrivilegedAccessor.getFieldSetter(eObj.getClass(), fieldName);
		if (setter != null) {
			try {
				setter.invoke(eObj, value);
			} catch (Exception e) {
				String srcRef = srcObj == null ? "<null>" : srcObj.getClass().getCanonicalName();
				EventHandler.handle(EventLevel.ERROR, "setField()", "", 
						"Failed to copy a field's value through setter " + setter.getName()
						+ "\n Source class: " + srcRef
						+ "\n Target class: " + eObj.getClass().getCanonicalName()
						+ "\n Field: " + fieldName
						+ "\n Value: " + value.toString(), e);
			}
		} else if (!PrivilegedAccessor.setValue(eObj, fieldName, value, true)) {
			String srcRef = srcObj == null ? "<null>" : srcObj.getClass().getCanonicalName();
			EventHandler.handle(EventLevel.ERROR, "setField()", "", "Failed to directly copy a field's value."
					+ "\n Source class: " + srcRef
					+ "\n Target class: " + eObj.getClass().getCanonicalName()
					+ "\n Field: " + fieldName
					+ "\n Value: " + value.toString());
		}
	}

}
