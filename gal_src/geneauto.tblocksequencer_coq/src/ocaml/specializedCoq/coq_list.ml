(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/ocaml/specializedCoq/coq_list.ml,v $
 *  @version	$Revision: 1.3 $
 *  @date	$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was extracted from Coq standard library and adapted by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

open Datatypes
open Specif

(** val list_rect : 'a2 -> ('a1 -> 'a1 list -> 'a2 -> 'a2) -> 'a1 list -> 'a2 **)

let rec list_rect f f0 = function
  | [] -> f
  | (a:: l0) -> f0 a l0 (list_rect f f0 l0)

(** val list_rec : 'a2 -> ('a1 -> 'a1 list -> 'a2 -> 'a2) -> 'a1 list -> 'a2 **)

let rec list_rec f f0 = function
  | [] -> f
  | (a:: l0) -> f0 a l0 (list_rec f f0 l0)

(** val head : 'a1 list -> 'a1 option **)

let head = function
  | [] -> error
  | (x:: l0) -> value x

(** val hd : 'a1 -> 'a1 list -> 'a1 **)

let hd default = function
  | [] -> default
  | (x:: l0) -> x

(** val tail : 'a1 list -> 'a1 list **)

let tail = function
  | [] -> []
  | (a:: m) -> m

(** val length : 'a1 list -> int **)

let rec length = function
  | [] -> 0
  | (a:: m) -> succ (length m)

(** val app : 'a1 list -> 'a1 list -> 'a1 list **)

let rec app l m =
  match l with
    | [] -> m
    | (a:: l1) -> (a:: (app l1 m))

(** val destruct_list : 'a1 list -> ('a1, 'a1 list) sigT option **)

let rec destruct_list = function
  | [] -> None
  | (a:: l0) -> Some (Coq_existT (a, l0))

(** val coq_In_dec : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 list -> bool **)

let rec coq_In_dec h a = function
  | [] -> false
  | (a0:: l0) ->
      (match h a0 a with
         | true -> true
         | false -> coq_In_dec h a l0)

(** val nth : int -> 'a1 list -> 'a1 -> 'a1 **)

let rec nth n l default =
  assert (n >= 0);
  if (n = 0)
  then
    (match l with
       | [] -> default
       | (x:: l') -> x)
  else
    let m = n - 1 in
      (match l with
         | [] -> default
         | (x:: t) -> nth m t default)
(*  match n with
    | 0 -> (match l with
              | [] -> default
              | (x:: l') -> x)
    | succ m ->
        (match l with
           | [] -> default
           | (x:: t) -> nth m t default) *)

(** val nth_ok : int -> 'a1 list -> 'a1 -> bool **)

let rec nth_ok n l default =
  assert (n >= 0);
  if (n = 0)
  then
    (match l with
       | [] -> false
       | (x:: l') -> true)
  else
    let m = n - 1 in
      (match l with
        | [] -> false
        | (x:: t) -> nth_ok m t default)
(*  match n with
    | 0 -> (match l with
              | [] -> false
              | (x:: l') -> true)
    | succ m ->
        (match l with
           | [] -> false
           | (x:: t) -> nth_ok m t default) *)

(** val nth_in_or_default : int -> 'a1 list -> 'a1 -> bool **)

let rec nth_in_or_default n l d =
  match l with
    | [] -> false
    | (a:: l0) ->
        (assert (n >= 0);
          if (n = 0)
          then true
          else
            let n1 = n - 1 in
              nth_in_or_default n1 l0 d)

(*        (match n with
           | 0 -> true
           | succ n1 -> nth_in_or_default n1 l0 d) *)

(** val nth_error : 'a1 list -> int -> 'a1 coq_Exc **)

let rec nth_error l n =
  assert (n >= 0);
  if (n = 0)
  then
    (match l with
       | [] -> error
       | (x:: l0) -> value x)
  else
    let n0 = n - 1 in
      (match l with
         | [] -> error
         | (a:: l0) -> nth_error l0 n0)
(*  | 0 ->
      (match l with
         | [] -> error
         | (x:: l0) -> value x)
  | succ n0 ->
      (match l with
         | [] -> error
         | (a:: l0) -> nth_error l0 n0) *)

(** val nth_default : 'a1 -> 'a1 list -> int -> 'a1 **)

let nth_default default l n =
  match nth_error l n with
    | Some x -> x
    | None -> default

(** val remove : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 list -> 'a1 list **)

let rec remove eq_dec x = function
  | [] -> []
  | (y:: tl) ->
      (match eq_dec x y with
         | true -> remove eq_dec x tl
         | false -> (y:: (remove eq_dec x tl)))

(** val last : 'a1 list -> 'a1 -> 'a1 **)

let rec last l d =
  match l with
    | [] -> d
    | (a:: l0) ->
        (match l0 with
           | [] -> a
           | (a0:: l1) -> last l0 d)

(** val removelast : 'a1 list -> 'a1 list **)

let rec removelast = function
  | [] -> []
  | (a:: l0) ->
      (match l0 with
         | [] -> []
         | (a0:: l1) -> (a:: (removelast l0)))

(** val exists_last : 'a1 list -> ('a1 list, 'a1) sigT **)

let rec exists_last = function
  | [] -> assert false (* absurd case *)
  | (a:: l0) ->
      (match l0 with
         | [] -> Coq_existT ([], a)
         | (a0:: l1) ->
             let Coq_existT (l', s) = exists_last l0 in
             Coq_existT (((a:: l')), s))

(** val count_occ : ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 -> int **)

let rec count_occ eqA_dec l x =
  match l with
    | [] -> 0
    | (y:: tl) ->
        (match eqA_dec y x with
           | true -> succ (count_occ eqA_dec tl x)
           | false -> count_occ eqA_dec tl x)

(** val rev : 'a1 list -> 'a1 list **)

let rec rev = function
  | [] -> []
  | (x:: l') -> app (rev l') ((x:: []))

(** val rev_append : 'a1 list -> 'a1 list -> 'a1 list **)

let rec rev_append l l' =
  match l with
    | [] -> l'
    | (a:: l0) -> rev_append l0 ((a:: l'))

(** val rev' : 'a1 list -> 'a1 list **)

let rev' l =
  rev_append l []

(** val list_eq_dec : ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list -> bool **)

let rec list_eq_dec eqA_dec l l' =
  match l with
    | [] ->
        (match l' with
           | [] -> true
           | (y:: l'0) -> false)
    | (a:: l0) ->
        (match l' with
           | [] -> false
           | (y:: l'0) ->
               (match eqA_dec a y with
                  | true -> list_eq_dec eqA_dec l0 l'0
                  | false -> false))

(** val map : ('a1 -> 'a2) -> 'a1 list -> 'a2 list **)

let rec map f = function
  | [] -> []
  | (a:: t) -> ((f a):: (map f t))

(** val flat_map : ('a1 -> 'a2 list) -> 'a1 list -> 'a2 list **)

let rec flat_map f = function
  | [] -> []
  | (x:: t) -> app (f x) (flat_map f t)

(** val fold_left : ('a1 -> 'a2 -> 'a1) -> 'a2 list -> 'a1 -> 'a1 **)

let rec fold_left f l a0 =
  match l with
    | [] -> a0
    | (b:: t) -> fold_left f t (f a0 b)

(** val fold_right : ('a2 -> 'a1 -> 'a1) -> 'a1 -> 'a2 list -> 'a1 **)

let rec fold_right f a0 = function
  | [] -> a0
  | (b:: t) -> f b (fold_right f a0 t)

(** val list_power : 'a1 list -> 'a2 list -> ('a1, 'a2) ocaml_prod list list **)

let rec list_power l l' =
  match l with
    | [] -> ([]::[])
    | (x:: t) ->
        flat_map (fun f -> map (fun y -> (( (x, y)):: f)) l')
          (list_power t l')

(** val existsb : ('a1 -> bool) -> 'a1 list -> bool **)

let rec existsb f = function
  | [] -> false
  | (a:: l0) ->
      (match f a with
         | true -> true
         | false -> existsb f l0)

(** val forallb : ('a1 -> bool) -> 'a1 list -> bool **)

let rec forallb f = function
  | [] -> true
  | (a:: l0) ->
      (match f a with
         | true -> forallb f l0
         | false -> false)

(** val filter : ('a1 -> bool) -> 'a1 list -> 'a1 list **)

let rec filter f = function
  | [] -> []
  | (x:: l0) ->
      (match f x with
         | true -> (x:: (filter f l0))
         | false -> filter f l0)

(** val find : ('a1 -> bool) -> 'a1 list -> 'a1 option **)

let rec find f = function
  | [] -> None
  | (x:: tl) ->
      (match f x with
         | true -> Some x
         | false -> find f tl)

(** val partition : ('a1 -> bool) -> 'a1 list -> ('a1 list, 'a1 list)
                    ocaml_prod **)

let rec partition f = function
  | [] ->  ([], [])
  | (x:: tl) ->
      let  (g, d) = partition f tl in
      (match f x with
         | true ->  (((x:: g)), d)
         | false ->  (g, ((x:: d))))

(** val split : ('a1, 'a2) ocaml_prod list -> ('a1 list, 'a2 list) ocaml_prod **)

let rec split = function
  | [] ->  ([], [])
  | (p:: tl) ->
      let  (x, y) = p in
      let  (g, d) = split tl in  (((x:: g)), ((y:: d)))

(** val combine : 'a1 list -> 'a2 list -> ('a1, 'a2) ocaml_prod list **)

let rec combine l l' =
  match l with
    | [] -> []
    | (x:: tl) ->
        (match l' with
           | [] -> []
           | (y:: tl') -> (( (x, y)):: (combine tl tl')))

(** val list_prod : 'a1 list -> 'a2 list -> ('a1, 'a2) ocaml_prod list **)

let rec list_prod l l' =
  match l with
    | [] -> []
    | (x:: t) -> app (map (fun y ->  (x, y)) l') (list_prod t l')

(** val firstn : int -> 'a1 list -> 'a1 list **)

let rec firstn n l =
  assert (n >= 0);
  if (n = 0)
  then []
  else 
    let n0 = (n - 1) in
  (* match n with
    | 0 -> []
    | succ n0 -> *)
        (match l with
           | [] -> []
           | (a:: l0) -> (a:: (firstn n0 l0)))

(** val skipn : int -> 'a1 list -> 'a1 list **)

let rec skipn n l =
  assert (n >= 0);
  if (n = 0)
  then []
  else 
    let n0 = (n - 1) in
(*  match n with
    | 0 -> l
    | succ n0 -> *)
        (match l with
           | [] -> []
           | (a:: l0) -> skipn n0 l0)

(** val seq : int -> int -> int list **)

let rec seq start n =
  (assert (n >= 0));
  if (n = 0)
  then []
  else
    let len0 = n - 1 in
      (start:: (seq (succ start) len0))
(*  | 0 -> []
  | succ len0 -> (start:: (seq (succ start) len0)) *)

