(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/ocaml/specializedCoq/datatypes.mli,v $
 *  @version	$Revision: 1.3 $
 *  @date	$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was extracted from Coq standard library and adapted by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

val unit_rect : 'a1 -> unit -> 'a1

val unit_rec : 'a1 -> unit -> 'a1

val bool_rect : 'a1 -> 'a1 -> bool -> 'a1

val bool_rec : 'a1 -> 'a1 -> bool -> 'a1

type nat = int

val nat_rect : 'a1 -> (nat -> 'a1 -> 'a1) -> nat -> 'a1

val nat_rec : 'a1 -> (nat -> 'a1 -> 'a1) -> nat -> 'a1

type coq_Empty_set = unit (* empty inductive *)

val coq_Empty_set_rect : coq_Empty_set -> 'a1

val coq_Empty_set_rec : coq_Empty_set -> 'a1

type 'a identity =
  | Coq_refl_identity

val identity_rect : 'a1 -> 'a2 -> 'a1 -> 'a2

val identity_rec : 'a1 -> 'a2 -> 'a1 -> 'a2

val option_rect : ('a1 -> 'a2) -> 'a2 -> 'a1 option -> 'a2

val option_rec : ('a1 -> 'a2) -> 'a2 -> 'a1 option -> 'a2

val option_map : ('a1 -> 'a2) -> 'a1 option -> 'a2 option

type ('a, 'b) sum =
  | Coq_inl of 'a
  | Coq_inr of 'b

val sum_rect : ('a1 -> 'a3) -> ('a2 -> 'a3) -> ('a1, 'a2) sum -> 'a3

val sum_rec : ('a1 -> 'a3) -> ('a2 -> 'a3) -> ('a1, 'a2) sum -> 'a3

type ('a, 'b) ocaml_prod = 'a * 'b

val prod_rect : ('a1 -> 'a2 -> 'a3) -> ('a1, 'a2) ocaml_prod -> 'a3

val prod_rec : ('a1 -> 'a2 -> 'a3) -> ('a1, 'a2) ocaml_prod -> 'a3

val fst : ('a1, 'a2) ocaml_prod -> 'a1

val snd : ('a1, 'a2) ocaml_prod -> 'a2

val prod_uncurry : (('a1, 'a2) ocaml_prod -> 'a3) -> 'a1 -> 'a2 -> 'a3

val prod_curry : ('a1 -> 'a2 -> 'a3) -> ('a1, 'a2) ocaml_prod -> 'a3

type comparison =
  | Eq
  | Lt
  | Gt

val comparison_rect : 'a1 -> 'a1 -> 'a1 -> comparison -> 'a1

val comparison_rec : 'a1 -> 'a1 -> 'a1 -> comparison -> 'a1

val coq_CompOpp : comparison -> comparison

