open Datatypes
open Specif

(** val eq_nat_decide : int -> int -> bool **)

let eq_nat_decide n m =
  (assert ((n >= 0) && (m >= 0)));
  (nat_rec 
    (fun m0 -> (m0 = 0))
    (fun n0 iHn m0 ->
       if (m0 = 0) then false else (let n1 = m0 - 1 in iHn n1))
    n m)

(** val beq_nat : int -> int -> bool **)

let rec beq_nat n m =
  (assert ((n >= 0) && (m >= 0)));
  (if (n = 0) 
   then 
     (if (m = 0) then true else false)
   else
     (let n1 = n - 1 in
	if (m = 0) 
	then 
	  false 
	else
	  let m1 = m - 1 in
	    beq_nat n1 m1))

