(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/ocaml/specializedCoq/compare_dec.ml,v $
 *  @version	$Revision: 1.3 $
 *  @date	$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was extracted from Coq standard library and adapted by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

open Datatypes
open Specif

(** val zerop : nat -> bool **)

let zerop n = (assert (n >=0); n = 0)

(** val lt_eq_lt_dec : nat -> nat -> bool option **)

let lt_eq_lt_dec n m = (assert (n >=0 && m >= 0); if n <= m then Some (n < m) else None)

(** val gt_eq_gt_dec : nat -> nat -> bool option **)

let gt_eq_gt_dec n m =
  lt_eq_lt_dec n m

(** val le_lt_dec : nat -> nat -> bool **)

let le_lt_dec n m = (assert (n >=0 && m >= 0); n <= m)

(** val le_le_S_dec : nat -> nat -> bool **)

let le_le_S_dec n m =
  le_lt_dec n m

(** val le_ge_dec : nat -> nat -> bool **)

let le_ge_dec n m =
  le_lt_dec n m

(** val le_gt_dec : nat -> nat -> bool **)

let le_gt_dec n m =
  le_lt_dec n m

(** val le_lt_eq_dec : nat -> nat -> bool **)

let le_lt_eq_dec n m =
  match lt_eq_lt_dec n m with
    | Some x -> x
    | None -> assert false (* absurd case *)

(** val nat_compare : nat -> nat -> comparison **)

let nat_compare n m =
  match lt_eq_lt_dec n m with
    | Some s -> (match s with
                   | true -> Lt
                   | false -> Eq)
    | None -> Gt

(** val leb : nat -> nat -> bool **)

let leb n m = (assert (n >=0 && m >= 0); n <= m)

