(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/ocaml/specializedCoq/datatypes.ml,v $
 *  @version	$Revision: 1.3 $
 *  @date	$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was extracted from Coq standard library and adapted by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

(** val unit_rect : 'a1 -> unit -> 'a1 **)

let unit_rect f u =
  f

(** val unit_rec : 'a1 -> unit -> 'a1 **)

let unit_rec f u =
  f

(** val bool_rect : 'a1 -> 'a1 -> bool -> 'a1 **)

let bool_rect f f0 = function
  | true -> f
  | false -> f0

(** val bool_rec : 'a1 -> 'a1 -> bool -> 'a1 **)

let bool_rec f f0 = function
  | true -> f
  | false -> f0

type nat = int

(** val nat_rect : 'a1 -> (nat -> 'a1 -> 'a1) -> nat -> 'a1 **)

let rec nat_rect f f0 n = 
 let rec nat_rect_aux n =
  if n = 0 then f else let n0 = n - 1 in f0 n0 (nat_rect_aux n0)
 in (assert (n >= 0);
     nat_rect_aux n)

(** val nat_rec : 'a1 -> (nat -> 'a1 -> 'a1) -> nat -> 'a1 **)

let nat_rec f f0 n =
  nat_rect f f0 n

type coq_Empty_set = unit (* empty inductive *)

(** val coq_Empty_set_rect : coq_Empty_set -> 'a1 **)

let coq_Empty_set_rect e =
  assert false (* absurd case *)

(** val coq_Empty_set_rec : coq_Empty_set -> 'a1 **)

let coq_Empty_set_rec e =
  assert false (* absurd case *)

type 'a identity =
  | Coq_refl_identity

(** val identity_rect : 'a1 -> 'a2 -> 'a1 -> 'a2 **)

let identity_rect a f y =
  f

(** val identity_rec : 'a1 -> 'a2 -> 'a1 -> 'a2 **)

let identity_rec a f y =
  f

(** val option_rect : ('a1 -> 'a2) -> 'a2 -> 'a1 option -> 'a2 **)

let option_rect f f0 = function
  | Some x -> f x
  | None -> f0

(** val option_rec : ('a1 -> 'a2) -> 'a2 -> 'a1 option -> 'a2 **)

let option_rec f f0 = function
  | Some x -> f x
  | None -> f0

(** val option_map : ('a1 -> 'a2) -> 'a1 option -> 'a2 option **)

let option_map f = function
  | Some a -> Some (f a)
  | None -> None

type ('a, 'b) sum =
  | Coq_inl of 'a
  | Coq_inr of 'b

(** val sum_rect : ('a1 -> 'a3) -> ('a2 -> 'a3) -> ('a1, 'a2) sum -> 'a3 **)

let sum_rect f f0 = function
  | Coq_inl x -> f x
  | Coq_inr x -> f0 x

(** val sum_rec : ('a1 -> 'a3) -> ('a2 -> 'a3) -> ('a1, 'a2) sum -> 'a3 **)

let sum_rec f f0 = function
  | Coq_inl x -> f x
  | Coq_inr x -> f0 x

type ('a, 'b) ocaml_prod = 'a * 'b

(** val prod_rect : ('a1 -> 'a2 -> 'a3) -> ('a1, 'a2) ocaml_prod -> 'a3 **)

let prod_rect f = function (x, x0) -> f x x0

(** val prod_rec : ('a1 -> 'a2 -> 'a3) -> ('a1, 'a2) ocaml_prod -> 'a3 **)

let prod_rec f = function (x, x0) -> f x x0

(** val fst : ('a1, 'a2) ocaml_prod -> 'a1 **)

let fst = function (x, y) -> x

(** val snd : ('a1, 'a2) ocaml_prod -> 'a2 **)

let snd = function (x, y) -> y

(** val prod_uncurry : (('a1, 'a2) ocaml_prod -> 'a3) -> 'a1 -> 'a2 -> 'a3 **)

let prod_uncurry f x y =
  f (x, y)

(** val prod_curry : ('a1 -> 'a2 -> 'a3) -> ('a1, 'a2) ocaml_prod -> 'a3 **)

let prod_curry f = function (x, y) -> f x y

type comparison =
  | Eq
  | Lt
  | Gt

(** val comparison_rect : 'a1 -> 'a1 -> 'a1 -> comparison -> 'a1 **)

let comparison_rect f f0 f1 = function
  | Eq -> f
  | Lt -> f0
  | Gt -> f1

(** val comparison_rec : 'a1 -> 'a1 -> 'a1 -> comparison -> 'a1 **)

let comparison_rec f f0 f1 = function
  | Eq -> f
  | Lt -> f0
  | Gt -> f1

(** val coq_CompOpp : comparison -> comparison **)

let coq_CompOpp = function
  | Eq -> Eq
  | Lt -> Gt
  | Gt -> Lt

