(*---------- BEGIN FIRST PART ----------*)
(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/ocaml/WrapperReader.ml,v $
 *  @version	$Revision: 1.4 $
 *  @date	$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

(* First part: to replace model, userDefinedPriority, assignedPriority *)

(* Access to the argument parsing library *)
open Arg;;

(* Access to the file name parsing library *)
open Filename;;

(* Access to the system function library *)
open Sys;;

(* Access to operating system level functions *)
(* Used for access to the date/time of tool execution *)
open Unix;;

open Scanf;;

open Datatypes;;

open Coq_list;;

open GeneAutoLibrary;;

open GeneAutoLanguage;;

open Peano_dec;;

open Specif;;

(* define and initialize the source file name *)
let sourceFile = ref "";;

(* define and initialize the target file name *)
let targetFile = ref "";;

(* define and initialize the log file name *)
let logFile = ref "";;

(* define the possible command line options, the associated action *)
(* and the help message which is printed in case of error and *)
(* request for help option *)
(* tool requirement GR-BS-I010 *)

let commandLineOptions = [
  ("-i", (Set_string sourceFile), "input file name");
  ("-o", (Set_string targetFile), "output file name");
  ("-l", (Set_string logFile), "log file name")
];;

parse commandLineOptions (function _ -> ()) "This is the BlockSequencer help message";;

(* Coq integer representation to ML integer representation *)
(* let rec int_of_nat n =
  match n with
  | O -> 0
  | S p -> 1 + (int_of_nat p);; *)

(* ML integer representation to Coq integer representation *)
(* let rec nat_of_int i =
  if i = 0 then O else S (nat_of_int (pred i));; *)

(* Produce a Coq list of natural numbers from 0 to (n-1) *)
(* This should be from 1 to n *)
(* let generate n =
  let rec aux n l =
  if (n < 0) then l else (aux (n - 1)  (Coq_cons ((nat_of_int n),l))) in
  (aux (n-1) Coq_nil);; *)
let generate n =
  let rec aux n l =
  if (n < 0) then l else (aux (n - 1)  (n::l)) in
  (aux (n-1) []);;

(* ML list representation to Coq list representation *)
(*let rec coq_of_ml l =
  match l with
    | [] -> Coq_nil
    | t :: q -> Coq_cons ((nat_of_int t), (coq_of_ml q));; *)

(* Coq list representation to ML list representation *)
(* let rec ml_of_coq l =
  match l with
    | Coq_nil -> []
    | Coq_cons(t,q) -> ((int_of_nat t)::(ml_of_coq q));; *)

let addBloc =
  function line -> 
  function ( blocks, userProvidedPriority, assignedPriority) ->
    sscanf line "block %i %s %i %i %i %i" 
    (function idx -> 
    function kind -> 
    function inputs -> 
    function outputs -> 
    function user -> 
    function assigned ->
      (* The block number is from 0 to (n-1) *)
      (* This should be from 1 to n *)
      let idxCoq = (idx - 1) in
      let block = (Block { 
        blockKind = 
          (match kind with
            | "sequential" -> (Sequential (inputs,outputs))
            | "combinatorial" -> (Combinatorial (inputs,outputs))
            | _ -> raise (Scan_failure ("Ill formed kind " ^ kind ^ " in the line " ^ line)));
(*          order = None;
 *)
	  blockDataInputs = None;
	  blockDataOutputs = None;
	  blockControlInputs = None;
	  blockControlOutputs = None;
          blockUserDefinedPriority = 
            (if (user = 0) 
              then None 
              else Some (user));
          blockAssignedPriority = assigned;
        blockIndex = idxCoq }) in
      let a = block::blocks in
      let b = 
        (if (user = 0) 
        then userProvidedPriority 
        else 
          (function n -> 
            (if (n = idxCoq) 
            then (Some user) 
            else (userProvidedPriority n))))
      in
      let c = 
        (function n -> 
          (if (n = idxCoq) 
          then assigned 
          else (assignedPriority n)))
      in 
      (a,b,c));;

let parseBlocks input blocksNumber =
  let rec aux n =
    if (n = 0)
    then ([],(function _ -> None), (function _ -> 0))
    else (addBloc (input_line input) (aux (n - 1))) in
  aux blocksNumber;;

let addSignal line (dataSignals,controlSignals) =
  sscanf line "signal %i %s %i %i" 
  (function idx ->
  function kind ->
  function src ->
  function dst ->
    let src' = (src - 1) in
    let dst' = (dst - 1) in
      match kind with
        | "data" -> (((DataSignal (src',dst'))::dataSignals),controlSignals)
        | "control" -> (dataSignals,((ControlSignal (src',dst'))::controlSignals))
        | _ -> raise (Scan_failure ("Ill formed kind " ^ kind ^ " in the line " ^ line)));;

let parseSignals input signalsNumber =
  let rec aux n =
    if (n = 0)
    then ([],[])
    else addSignal (input_line input) (aux (n - 1)) in
  aux signalsNumber;;

let parseModel input =
  (* Number of blocks in a model *)
  let blockNumber = (sscanf (input_line input) "blocks %i" (function nbr -> nbr)) in
  let (blocks,userDefinedPriorityFunction,assignedPriorityFunction) = (parseBlocks input blockNumber) in
  (* Number of signals in a model *)
  let signalNumber = (sscanf (input_line input) "signals %i" (function nbr -> nbr)) in
  let (dataSignals,controlSignals) = (parseSignals input signalNumber) in
    ( blockNumber , 
      { blocks = blocks; 
        dataSignals = dataSignals; 
        controlSignals = controlSignals; 
        blocksNumber = blockNumber }, 
      userDefinedPriorityFunction, 
      assignedPriorityFunction );;

let logChannel = 
  try
    open_out (!logFile)
  with Sys_error(msg) -> Pervasives.stderr;;

let sourceChannel = 
  try
    open_in (!sourceFile)
  with Sys_error(msg) -> Pervasives.stdin;;

let writeTime channel = 
  let current = Unix.gettimeofday() in
  let delta = (int_of_float (floor (1000.0 *. (current -. (floor current))))) in
  let formated = Unix.localtime (floor current) in
    (Printf.fprintf channel "%02u-%02u-%02u %02u:%02u:%02u.%03u"
      (formated.tm_year mod 100)
      (formated.tm_mon + 1)
      formated.tm_mday
      formated.tm_hour
      formated.tm_min
      formated.tm_sec
      delta);;

let writeLog kind msg =
  ((output_string logChannel kind);
   (output_string logChannel "%");
   (writeTime logChannel);
   (output_string logChannel "%");
   (output_string logChannel msg);
   (* TODO : MaPa, should be done in a platform independent way *)
   (output_string logChannel "\n"));;

let writeCritical msg = writeLog "critical" msg;;

let writeError msg = writeLog "error" msg;;

let writeWarning msg = writeLog "warning" msg;;

let writeDebug msg = writeLog "debug" msg;;

let writeInformation msg = writeLog "information" msg;;

let writeNone msg = writeLog "none" msg;;

let (blockNumber, model, userDefinedPriorityFunction, assignedPriorityFunction) = 
  try (parseModel sourceChannel)
  with (Scan_failure msg) -> ((writeCritical msg);(exit 0));;

(close_in sourceChannel);;

(*---------- END FIRST PART ----------*)


