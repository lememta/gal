(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/ocaml/GenerateRectangle.ml,v $
 *  @version	$Revision: 1.3 $
 *  @date	$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

open Arg;;

open Printf;;

open Sys;;

let depth = ref 0;;

let width = ref 0;;

let targetFile = ref "";;

let logFile = ref "";;

let commandLineOptions = [
  ("-d", (Set_int depth), "depth of the rectangle");
  ("-w", (Set_int width), "width of the rectangle");
  ("-o", (Set_string targetFile), "output file name");
  ("-l", (Set_string logFile), "log file name")
];;

parse commandLineOptions (function _ -> ()) "This is the rectangle test model generator help message";;

let targetChannel = 
  open_out (!targetFile);;

(fprintf targetChannel "blocks %i\n" (!depth * !width));;

for index = 1 to !width do
  (fprintf targetChannel "block %i combinatorial 0 1 %i %i\n" index index index)
done;;

for index = !width + 1 to !width * (!depth - 1) do
  fprintf targetChannel "block %i combinatorial 1 1 %i %i\n" index index index
done;;

for index = 1 to !width do
  let val = index + (!depth - 1) * !width in
    (fprintf targetChannel "block %i combinatorial 1 0 %i %i\n" val val val)
done;;

(fprintf targetChannel "signals %i\n" (!depth - 1) * (2 * !width - 1));;

for index = 1 to (!depth - 1) * (2 * !width - 1) do
  fprintf targetChannel "signal %i data %i %i\n" index index (index + 1)
done;;

close_out targetChannel;;
