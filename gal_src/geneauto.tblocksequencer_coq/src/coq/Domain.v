(* This code was written by: *)
(*      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7 *)
(*      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7 *)
(* It is released under GPL v2 license *)
(* This file defines several modules used in the algorithms *)
  
Require Import Relation_Definitions.
Require Import Arith.
Require Import OrderedType.

(* The Arrow module defines a function from type source to type target *)
(* apply is the value representing the function *)
(* This module will be refined in order to define given functions *)

Module Type ArrowType.

  (* The type of the parameter of the function *)
  Variable source : Set.

  (* The type of the result of the function *)
  Variable target : Set.

  (* The computation of the function *)
  Variable apply : source -> target.

End ArrowType.  

(* The DataType module type defines a data of type type that can be *)
(* compared with eq *)
Module Type DataType.

  (* The type of the data *)
  Parameter type : Set.

  (* Two data must be either equals or differents *)
  (* The standard equal operator = must be complete *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom eq : forall x y : type, { (x = y) } + { ~ (x = y) }.

End DataType.

(* The Unit module refines the Data module *)
(* It represents a single value which is the absence of value *)
Module Unit <: DataType.

  (* The type of the Unit data is the unit type *)
  Definition type := unit.

  (* The proof that the standard equal operator = is complete *)
  (* Satisfies DR TBC *)
  Lemma eq : forall x y : type, { (x = y) } + { ~ (x = y) }.
    Proof.
      decide equality.		 
    Qed.

End Unit.
  
(* The PreOrderType module type defines a preorder structure *)
(* on a data set of a given type *)
Module Type PreOrderType.

  (* The data in the set upon which the preorder is defined *)
  (* TODO: MaPa, 2008/08/17, is it really that ? it is not the case *)
  (*   in all of its refinements *)
  Declare Module Data : DataType.

  (* The type of the data support of the preorder *)
  (* TODO : MaPa, 2008/08/15, why is it different from the type of the Data *)
  Variable type : Set.

  (* The lesser than or equal operation 'le' on the data *)
  (* it takes as parameters : *)
  (* - the type of the data *)
  (* - the type of the preorder for each parameter *)
  (* TODO: MaPa, 2008/08/16, why do we need to take Data.type as parameter ? *)
  Variable le : Data.type -> type -> type -> Prop.

  (* The lesser or equal operation 'le' must be reflexive *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom refl : forall d x, le d x x.

  (* The lesser or equal operation 'le' must be transitive *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom trans : forall d x y z, (le d x y) -> (le d y z) -> (le d x z).

  (* The lesser or equal operation 'le' must be decidable *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom dec : forall d x y, { le d x y } + { ~ (le d x y) }.

  (* The equal operation on the data *)
  (* it takes as parameters : *)
  (* - the type of the data *)
  (* - the type of the preorder for each parameter *)
  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

  (* The equal operation on the data can be computed *)
  (* it is decidable *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom eq_dec : forall d x y, { (eq d x y) } + { ~ (eq d x y) }.

End PreOrderType.  

(* The TotalPreOrderType module type defines a total preorder relation *)
(* on a data set of a given type *)
(* TODO : MaPa 20080731, should refine module PreOrder *)
Module Type TotalPreOrderType.

  (* The type of the data *)
  Declare Module Data : DataType.

  (* The type of the total preorder on the data *)
  (* TODO : MaPa, 2008/08/15, why is it different from the type of the Data *)
  Variable type : Set.

  (* The lesser than or equal operation on the data *)
  (* it takes as parameters : *)
  (* - the type of the data *)
  (* - the type of the preorder for each parameter *)
  Variable le : Data.type -> type -> type -> Prop.

  (** lesser or equal must be reflexive *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom refl : forall d x, le d x x.

  (** lesser or equal must be transitive *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom trans : forall d x y z, le d x y -> le d y z -> le d x z.

  (** lesser or equal must decidable *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom dec : forall d x y, { le d x y } + { ~ (le d x y) }.

  (** lesser or equal must be total *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom total : forall d x y, { le d x y } + { le d y x }.

  (* The equal operation on the data *)
  (* it takes as parameters : *)
  (* - the type of the data *)
  (* - the type of the preorder for each parameter *)
  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

  (* The equal operation on the data can be computed *)
  (* it is decidable *)
  (* TODO: MaPa, 2008/08/16, define a design requirement *)
  (* DR *)
  Axiom eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.

End TotalPreOrderType.

(* The module NaturalTotalPreOrder defines the TotalPreOrder *)
(* on natural numbers *)
(* It refines the TotalPreOrder module *)
(* and specifies the type of the data as natural *)
(* TODO: MaPa, 2008/08/17, what is the purpose of D ? *)
Module NaturalTotalPreOrder : TotalPreOrderType
        with Module Data := Unit
	with Definition type := nat.

  (* The data set *)
  Module Data := Unit.

  (* The type of the data is natural *)		     
  Definition type := nat.

  (* The lesser than or equal operation is <= on natural numbers *)	     
  Definition le : Data.type -> type -> type -> Prop :=
    fun d x y => x <= y.
	    
  (* The equal operation is = on natural numbers *)
  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.
    
  Definition eq_opt : Data.type -> type -> type -> Prop :=
    fun d x y => x = y.

(*
  Lemma eq_opt_is_eq : forall d x y, eq_opt d x y -> eq d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *.
      intros.
      subst;
        auto.
    Qed.
*)

  Lemma eq_opt_is_eq : forall d x y, eq_opt d x y -> eq d x y.
    Proof.
      unfold eq_opt in |- *.
        unfold eq in |- *.
        unfold le in |- *.
      intros.
      subst.
        auto.
    Qed.

  Lemma eq_is_eq_opt : forall d x y, eq d x y -> eq_opt d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *.
      intros.
      apply le_antisym;
        intuition.
    Qed.

  (* The proof that equal is decidable *)
  (* Satisfies DR TBC *)	    
  Lemma eq_opt_dec : forall d x y, { eq_opt d x y } + { ~ (eq_opt d x y) }.
    Proof.
      intros.
      unfold eq_opt in |- *.
      apply eq_nat_dec.	    
    Qed.

  Lemma eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.
    Proof.
      intros.
      elim (eq_opt_dec d x y);
        intros Heqo.
      left;
        apply eq_opt_is_eq;
        exact Heqo.
      right;
        intro Heq;
        apply Heqo;
        apply eq_is_eq_opt;
        exact Heq.    
    Qed.

  (* The proof that lesser or equal is reflexive *)	    
  (* Satisfies DR TBC *)	    
  Lemma refl : forall d x , le d x x. 
    Proof.
      intros.
      unfold le in |- *.
      auto with arith.
    Qed.

  (* The proof that lesser or equal is transitive *)
  (* Satisfies DR TBC *)	    
  Lemma trans : forall d x y z, (le d x y) -> (le d y z) -> (le d x z).
    Proof.
      intros.
      unfold le in |- *.
      eapply le_trans.
      apply H.
      exact H0.
    Qed.

  (* The proof that lesser or equal is decidable *)
  (* Satisfies DR TBC *)	    
  Lemma dec : forall d x y, { le d x y } + { ~ (le d x y) }.	    
    Proof.
      intros.
      unfold le in |- *.
      elim (le_gt_dec x y);
      [ auto with arith | intro; right; apply gt_not_le; assumption ].
    Qed.  

  (* The proof that lesser or equal is total *)
  (* Satisfies DR TBC *)	    
  Lemma total : forall d x y, { le d x y } + { le d y x }. 
    Proof.
      intros; unfold le in |- *.
      elim (le_ge_dec x y).
      intro; left;  intuition.
      intro; right;  intuition.
    Qed.
		     
End NaturalTotalPreOrder. 
    
(* The Priority module defines a TotalPreOrder on priorities *)
(* It refines the TotalPreOrder module and specifies the type *)
(* of the data as an optional natural number *)
(* TODO: MaPa, 2008/08/17, what is the purpose of D ? *)
Module PriorityPreOrder : TotalPreOrderType
  with Module Data := Unit
  with Definition type := option nat.

  (* The data is given as generic parameter to the module *)
  Module Data := Unit.

  (* The type of the data is an optional natural number *)
  Definition type := option nat.				   

  (* Defines the various operations on the optional natural number *)
  Section OptionPreOrder.

    (* Two priority are equals: *)
    (*   either is no priority is provided *)
    (*   or if both provided priority are equals *)
    Definition eq_option (x y : type) :=
      match x , y with 
        | None , None    => True
        | Some n, Some m => n=m
        | _, _           => False
      end.

    (* TODO: MaPa, 2008/08/15, I think it is false *)
    (* the strongest priority is 1, not n *)
    Definition le_option (x y : type)  :=
      match x, y with 
        | None, Some _   => True
        | Some n, Some m => n <= m
(* This is the right definition *)
(* TODO: MaPa, 2008/08/16, should replace it *)
(*      | Some n, Some m => n >= m *)
        | None, None     => True
        | Some_, None    => False
      end.

    (* Proof that the lesser or equal operation on optional priorities *)
    (* is reflexive *)
    Lemma le_option_refl : forall x , le_option x x.
      Proof.
        intros.
        destruct x.
        unfold le_option in |- *.
        auto.
        unfold le_option in |- *.
        auto. 
      Qed.

    (* Proof that the lesser or equal operation on optional priorities *)
    (* is decidable *)
    Lemma le_option_dec : forall x y, { (le_option x y) } + { ~ (le_option x y) }.
      Proof.
        intros.
        destruct x; 
          destruct y.
        unfold le_option in |- *.
        elim (le_lt_dec n n0).
        intro.
        left; 
          auto.
        intro.
        right.
        apply (lt_not_le n0 n).
        auto with arith.
        right.
        intuition.
        left.
        unfold le_option in |- *; 
          trivial.
        left; 
          unfold le_option in |- *; 
          trivial.
      Qed.

    (* Proof that the lesser or equal operation on optional priorities *)
    (* is transitive *)
    Lemma le_option_trans: forall x y z, (le_option x y) -> (le_option y z) -> (le_option x z).
      Proof.
        intros.
        destruct x; 
          destruct y; 
          destruct z; 
          intuition.
        unfold le_option in |- *.
        unfold le_option in H.
        unfold le_option in H0.
        apply (le_trans n n0 n1); 
          assumption.
        unfold le_option in |- *.
        unfold le_option in H.
        unfold le_option in H0.
        intuition.
      Qed.

    (* Proof that the negation of the lesser or equal operation on optional *)
    (* priorities x and y implies the lesser or equal operation on y and x *)
    Lemma le_option_not : forall x y, (~ (le_option x y)) -> (le_option y x).
      Proof.
        intros.
        destruct x; 
          destruct y.
        unfold le_option in |- *.
        unfold le_option in H.
        elim (not_gt n0 n).
        auto.
        intros.
        auto.
        elim (not_le n n0).
        auto with arith.
        intros.
        auto with arith.
        auto.
        unfold le_option in |- *.
        auto.
        elim H.
        unfold le_option in |- *;
          auto.
        unfold le_option in |- *;
          auto.
      Qed. 

    (* Proof that the lesser or equal operation on optional priorities *)
    (* is total *)
    Lemma le_option_total : forall x y, { (le_option x y) } + { (le_option y x) }.
      Proof.
        intros.
        destruct x;
          destruct y;
          intuition.
        elim (le_option_dec (Some n) (Some n0)).
        intro.
        left;
          intuition.
        intro.
        right.
        apply (le_option_not (Some n) (Some n0)).
        auto.
        right; 
          unfold le_option in |- *;
          intuition.
        left; 
          unfold le_option in |- *;  
          intuition.
        left; 
          unfold le_option in |- *;  
          intuition.  
      Qed.

  End OptionPreOrder. 

  (* The lesser or equal operation is the one defined previously *)    
  (* Satisfies DR TBC *)	    
  Definition le : Data.type -> type -> type -> Prop :=
    fun d x y => le_option x y.
					
  (* The equal operation is the one defined previously *)    
  (* Satisfies DR TBC *)
  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

  Lemma eq_option_is_eq : forall d x y, eq_option x y -> eq d x y.
    Proof.
      unfold eq_option in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold le_option in |- *.
      destruct x;
        destruct y.
      intro;
        subst;
        auto.
      auto.
      auto.
      auto.
    Qed.

  Lemma eq_is_eq_option : forall d x y, eq d x y -> eq_option x y.
    Proof.
      unfold eq_option in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold le_option in |- *.
      destruct x;
        destruct y.
      intro;
        apply le_antisym;
        intuition.
      intuition.
      intuition.
      auto.
    Qed.

  (* The proof that equal is decidable *)
  (* Satisfies DR TBC *)	    
  Lemma eq_option_dec : forall x y, { (eq_option x y) } + { ~(eq_option x y) }.
    Proof.
      intros.
      unfold eq_option in |- *.
      destruct x;
        destruct y.
      apply eq_nat_dec. 
      right;
        auto.
      right;
        auto.
      left;
        auto.
    Qed.

  Lemma eq_dec : forall d x y, { (eq d x y) } + { ~(eq d x y) }.
    Proof.
      intros.
      elim (eq_option_dec x y);
        intros Heqo.
      left;
        apply eq_option_is_eq;
        exact Heqo.
      right;
        intro Heq;
        apply Heqo;
        apply (eq_is_eq_option d);
        exact Heq.
    Qed.	
				
  (* The proof that lesser or equal is reflexive *)
  (*  is the one defined previously *)    
  (* Satisfies DR TBC *)	    
  Lemma refl : forall d x , le d x x.
    Proof.
      intros.
      unfold le in |- *.
      apply le_option_refl.
    Qed.

  (* The proof that lesser or equal is transitive *)
  (*  is the one defined previously *)    
  (* Satisfies DR TBC *)	    
  Lemma trans : forall d x y z, (le d x y) -> (le d y z) -> (le d x z).
    Proof.
      intros.
      unfold le in |- *; 
        unfold le in H; 
        unfold le in H0.
      apply (le_option_trans x y z);  
        intuition.
    Qed.

  (* The proof that lesser or equal is decidable *)
  (*  is the one defined previously *)    
  (* Satisfies DR TBC *)	    
  Lemma dec : forall d x y, { (le d x y) } +{ ~ (le d x y) }.
    Proof.
      intros.
      unfold le in |- *.
      apply (le_option_dec x y).
    Qed.

  (* The proof that lesser or equal is total *)
  (*  is the one defined previously *)    
  (* Satisfies DR TBC *)	    
  Lemma total : forall d x y, { (le d x y) } + { (le d y x) }.
    Proof.
      intros.
      unfold le in |- *.
      apply (le_option_total x y).
    Qed.
  
End PriorityPreOrder.

(* The module PreOrderCartesianProduct combines two preorder on the same data *)
(* it takes as parameter both preorders ord1 and ord2 *)
(* ord2 and ord1 must share the same data *)
(* TODO: MaPa, 2008/08/15, why do we introduce this constraint *)
(* it refines the PreOrder specifying the managed data and the type *)
(* of the preorder *)
Module PreOrderCartesianProduct 

  (* The first provided preorder *)
  (O1 : PreOrderType) 

  (* The second provided preorder on the same data *)
  (O2 : PreOrderType with Module Data := O1.Data)

  <: PreOrderType 

    (* The data is the one provided *)
    with Module Data := O1.Data

    (* The type of the lexicographic order is a cartesian product *)
    (* of the types of both preorder *)
    (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
    (* of the data *)
    with Definition type := (O1.type * O2.type)%type.

  (* The data is the one provided *)
  Module Data := O1.Data.

  (* The type of the lexicographic order is a cartesian product *)
  (* of the types of both preorder *)
  (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
  (* of the data *)
  Definition type := (O1.type * O2.type)%type.
	    
  (** The lesser or equal operation compares both elements of the pair *)
  Definition le : Data.type -> type -> type -> Prop :=
    fun d x y =>
      match x, y with
        | (x1, x2), (y1, y2) => (O1.le d x1 y1) /\ (O2.le d x2 y2)
      end.

  (** The equal operation compares both elements of the pair *)
  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

  Definition eq_opt : Data.type -> type -> type -> Prop :=
    fun d x y => 
      match x , y with 
        | (x1, x2) , (y1, y2) => (O1.eq d x1 y1) /\ (O2.eq d x2 y2)
      end.

  Lemma eq_opt_is_eq : forall d x y, eq_opt d x y -> eq d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold O1.eq in |- *;
        unfold O2.eq in |- *.
      destruct x;
        destruct y;
        intros.
      intuition.
    Qed.

  Lemma eq_is_eq_opt : forall d x y, eq d x y -> eq_opt d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold O1.eq in |- *;
        unfold O2.eq in |- *.
      destruct x;
        destruct y;
        intros.
      intuition.
    Qed.

  (** Proof that the lesser or equal operation is reflexive *)
  (* relying on the fact that it is reflexive on both elements *)
  (* Satisfies DR TBC *)	    
  Theorem refl : forall d e, le d e e.
    Proof.
      destruct e; 
        simpl in |- *; 
        split.
      apply O1.refl.
      apply O2.refl.
    Qed.

  (** Proof that the lesser or equal operation is transitive *)
  (* relying on the fact that it is transitive on both elements *)
  (* Satisfies DR TBC *)	    
  Theorem trans : forall d e1 e2 e3, (le d e1 e2) -> (le d e2 e3) -> (le d e1 e3).
    Proof.
      destruct e1 as (t0,t1); 
        destruct e2 as (t2,t3); 
        destruct e3 as (t4,t5); 
        simpl in |- *;  
        intuition.
      apply (O1.trans d t0 t2 t4); 
        assumption.
      apply (O2.trans d t1 t3 t5); 
        assumption.
    Qed.

  (** Proof that the lesser or equal operation is decidable *)
  (* relying on the fact that it is decidable on both elements *)
  (* Satisfies DR TBC *)	    
  Theorem dec :  forall d e1 e2 , { (le d e1 e2) } + { ~ (le d e1 e2) }.
    Proof.
      destruct e1 as (t0,t1); 
        destruct e2 as (t2,t3); 
        simpl in |- *.
      elim (O1.dec d t0 t2); 
        elim (O2.dec d t1 t3); 
        intros.
      left;  
        intuition.
      right;  
        intuition.
      right;  
        intuition.
      right;  
        intuition.
    Defined.

  (** Proof that the equal operation is decidable *)
  (* Satisfies DR TBC *)
   
  Lemma eq_opt_dec : forall d x y, { (eq_opt d x y) } + { ~ (eq_opt d x y) }.
    Proof.
      unfold eq_opt in |- *; 
        destruct x as (t0,t1); 
        destruct y as (t2,t3).
      elim (O1.eq_dec d t0 t2); 
        intros.
      elim (O2.eq_dec d t1 t3); 
        intros.
      left; 
        auto.
      right;
        intuition.
      right;
        intuition.
    Qed.

  Lemma eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.
    Proof.
      intros.
      elim (eq_opt_dec d x y);
        intros Heqo.
      left;
        apply eq_opt_is_eq;
        exact Heqo.
      right;
        intro Heq;
        apply Heqo;
        apply eq_is_eq_opt;
        exact Heq.    
    Qed.

End PreOrderCartesianProduct.			   

(* The module PreOrderLexicographicProduct combines two preorder *)
(* on the same data *)
(* it takes as parameter both preorders ord1 and ord2 *)
(* ord2 and ord1 must share the same data *)
(* TODO: MaPa, 2008/08/15, why do we introduce this constraint *)
(* it refines the PreOrder specifying the managed data and the type *)
(* of the preorder *)
Module PreOrderLexicographicProduct 

  (* The first provided preorder *)
  (ord1 : PreOrderType)

  (* The second provided preorder on the same data *)
  (ord2 : PreOrderType with Module Data := ord1.Data)

  <: PreOrderType 

    (* The data is the one provided *)
    with Module Data := ord1.Data

    (* The type of the lexicographic order is a cartesian product *)
    (* of the types of both preorder *)
    (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
    (* of the data *)
    with Definition type := (ord1.type * ord2.type)%type.

  (* The data is the one provided *)
  Module Data := ord1.Data.

  (* The type of the lexicographic order is a cartesian product *)
  (* of the types of both preorder *)
  (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
  (* of the data *)
  Definition type := (ord1.type * ord2.type)%type.

  (** The lesser or equal operation: *)
  (*   first compare the first elements of the pair *)
  (*   then, if the first are equals, *)
  (*         compare the second elements of the pairs *)
  (* this is the classical lexicographic ordering *)
  Definition le : Data.type -> type -> type -> Prop :=
    fun d x y => 
      match x , y with
        | (x1, x2), (y1,y2) => (ord1.le d x1 y1) /\ 
                               ((ord1.le d y1 x1) -> (ord2.le d x2 y2))
      end.

  (** The equal operation, both first and second elements must be equals *)
  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

  Definition eq_opt : Data.type -> type -> type -> Prop :=
    fun d x y => 
      match x , y with 
        | (x1,y1) , (x2, y2) => (ord1.eq d x1 x2) /\ (ord2.eq d y1 y2)
      end.

  Lemma eq_opt_is_eq : forall d x y, eq_opt d x y -> eq d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold ord1.eq in |- *;
        unfold ord2.eq in |- *.
      destruct x;
        destruct y;
        intros.
      intuition.
    Qed.

  Lemma eq_is_eq_opt : forall d x y, eq d x y -> eq_opt d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold ord1.eq in |- *;
        unfold ord2.eq in |- *.
      destruct x;
        destruct y;
        intros.
      intuition.
    Qed.

  (** Proof that the equal operation is decidable *)
  Lemma eq_opt_dec : forall d x y, { (eq_opt d x y) } + { ~ (eq_opt d x y) }.
    Proof.
      intros.
      destruct x as (t0,t1); 
        destruct y as (t2,t3).
      unfold eq_opt in |- *.
      elim (ord1.eq_dec d t0 t2); 
        elim (ord2.eq_dec d t1 t3).
      intros.
      left.
      intuition.
      intros.
      right.
      intuition.
      intros.
      right.
      intuition.
      intros.
      right.
      intuition.
    Qed.

  Lemma eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.
    Proof.
      intros.
      elim (eq_opt_dec d x y);
        intros Heqo.
      left;
        apply eq_opt_is_eq;
        exact Heqo.
      right;
        intro Heq;
        apply Heqo;
        apply eq_is_eq_opt;
        exact Heq.    
    Qed.

  (** Proof that the lesser or equal operation is reflexive *)
  (* relying on the fact that it is reflexive on both elements *)
  (* Satisfies DR TBC *)	    
  Lemma refl : forall d x, le d x x.
    Proof.
      intros.
      destruct x.
      simpl in |- *.
      intuition.
      apply ord1.refl.
      apply ord2.refl.
    Qed.

  (** Proof that the lesser or equal operation is transitive *)
  (* relying on the fact that it is transitive on both elements *)
  (* Satisfies DR TBC *)	    
  Lemma trans : forall d x y z, (le d x y) -> (le d y z) -> (le d x z).
    Proof.
      destruct x as (t0,t1); 
        destruct y as (t2,t3); 
        destruct z as (t4,t5).
      simpl in |- *;  intuition.
      apply (ord1.trans d t0 t2 t4); 
        assumption.
      apply (ord2.trans d t1 t3 t5).
      apply H2.
      apply (ord1.trans d t2 t4 t0).
      apply H.
      apply H0.
      apply H3.
      apply (ord1.trans d t4 t0 t2).
      assumption.
      assumption.
    Qed.

  (** Proof that the lesser or equal operation is decidable *)
  (* relying on the fact that it is decidable on both elements *)
  (* Satisfies DR TBC *)	    
  Lemma dec : forall d x y, { (le d x y) } + { ~ (le d x y) }.
    Proof.
      intros.
      destruct x as (t0,t1); 
        destruct y as (t2,t3); 
        simpl in |- *.
      elim (ord1.dec d t0 t2); 
        intros.
      elim (ord2.dec d t1 t3); 
        intros.
      left;  
        intuition.
      elim (ord1.dec d t2 t0); 
        intros.
      right.
      simpl in |- *;
        intuition.
      left;  
        intuition.
      right;  
        intuition.
    Qed.

End PreOrderLexicographicProduct.

(* The module TotalPreOrderLexicographicProduct combines two preorder *)
(* on the same data *)
(* it takes as parameter both total preorders ord1 and ord2 *)
(* ord2 and ord1 must share the same data *)
(* TODO: MaPa, 2008/08/15, why do we introduce this constraint *)
(* it refines the PreOrder specifying the managed data and the type *)
(* of the preorder *)
Module TotalPreOrderLexicographicProduct 

  (* The first provided preorder *)
  (ord1 : TotalPreOrderType) 

  (* The second provided preorder on the same data *)
  (ord2: TotalPreOrderType with Module Data := ord1.Data)

  <: TotalPreOrderType  

    (* The data is the one provided *)
    with Module Data := ord1.Data

    (* The type of the lexicographic order is a cartesian product *)
    (* of the types of both preorder *)
    (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
    (* of the data *)
    with Definition type := (ord1.type * ord2.type)%type.

  (* The TotalPreOrderLexicographicProduct module is a refinement of *)
  (* the PreOrderLexicographicProduct module *)
  Module L := PreOrderLexicographicProduct (ord1) (ord2).

  (* TODO: MaPa, 2008/08/15, why not use the import construct ? *)

  Module Data := L.Data.

  Definition type := L.type.

  Definition le := L.le.

  Definition refl := L.refl.

  Definition trans := L.trans.

  Definition dec := L.dec.

  Definition eq := fun d x y => le d x y /\ le d y x.

  Definition eq_dec := L.eq_dec.

  (* Satisfies DR TBC *)	    
  Lemma total: forall d x y, { (le d x y) } + { (le d y x) } .
    Proof.
      destruct x as (t0,t1); 
        destruct y as (t2,t3); 
        simpl in |- *.
      elim (ord1.total d t0 t2); 
        elim (ord2.total d t1 t3).
      intros.
      left; 
        split; 
        trivial.
      intros.
      elim (ord1.dec d t2 t0).
      intro.
      right; 
        split; 
        trivial.
      intro.
      left; 
        split; 
        trivial.
      contradiction.
      intros.
      elim (ord1.dec d t0 t2).
      intro.
      left; 
        split;  
        trivial.
      intro.
      right; 
        split; 
        trivial.
      contradiction.
      intros.
      right; 
        split; 
        trivial.   
    Qed.

End TotalPreOrderLexicographicProduct.

(** The module PreOrderLexicographicSequence defines a preorder *)
(* on a list of elements of a given data and type upon which *)
(* a preorder is already defined *)
Module PreOrderLexicographicSequence 
  (ord : PreOrderType)
  <: PreOrderType 
    (* The data is the one provided *)
    with Module Data := ord.Data
    (* The type of the lexicographic order is a list *)
    (* of the type of the preorder *)
    (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
    (* of the data *)
    with Definition type := list ord.type.

  (* The data is the one provided *)
  Module Data := ord.Data.

  (* The type of the lexicographic order is a list *)
  (* of the type of the preorder *)
  (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
  (* of the data *)
  Definition type := list ord.type.

  (** The lesser or equal operation on lists *)
  (* we rely on the classical lexicographic ordering *)
  Fixpoint le (d : Data.type) (e1 e2 : type) { struct e1 } : Prop :=
    match e1 with
      (* an empty list is lesser or equal than all lists *)
      | nil        => True
      | cons t1 q1 => 
        match e2 with
          (* a non-empty list is not lesser or equal than an empty list *)
          | nil        => False
          (* a non-empty list is lesser or equal than a non-empty list: *)
          (*   - if the head of the first is less or equal than the head *)
          (*     of the second *)
          (*   - if their head are equals and the queue of the first one *)
          (*     is lesser or equel than the queue of the second one *)
          | cons t2 q2 => (ord.le d t1 t2) /\ 
                          ((ord.le d t2 t1) -> (le d q1 q2))
        end
    end.

  (** The equal operation on list is defined using *)
  (* the lesser or equal operation *)
  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

  Fixpoint eq_opt (d : Data.type) (e1 e2 : type) { struct e1 } : Prop :=
    match e1, e2 with
      (* empty lists are equal *)
      | nil       , nil        => True
      (* a non-empty list is equal than a non-empty list:  *)
      (*   - if the head of the first is equal to the head *)
      (*     of the second *)
      (*   - if the queue of the first one is equal to     *)
      (*     the queue of the second one                   *)
      | cons t1 q1, cons t2 q2 => (ord.eq d t1 t2) /\ (eq_opt d q1 q2)
      (* a non-empty list is not equal to an empty list *)
      |  _        , _          => False
    end.

  Lemma eq_opt_is_eq : forall d x y, eq_opt d x y -> eq d x y.
    Proof.
      unfold eq in |- *.
      induction x;
        destruct y;
        simpl in |- *.
      auto.
      auto.
      auto.
      unfold ord.eq in |- *.
      intro H;
        elim H;
        clear H;
        intros H H1.
      elim (IHx y H1).
        intuition.
    Qed.

  Lemma eq_is_eq_opt : forall d x y, eq d x y -> eq_opt d x y.
    Proof.
      unfold eq in |- *.
      induction x;
        destruct y;
        simpl in |- *.
      auto.
      intuition.
      intuition.
      unfold ord.eq in |- *.
      intuition.
    Qed.

  (** Proof that the lesser or equal operation is decidable *)
  (* Satisfies DR TBC *)	    
  Lemma dec : forall d x y, { (le d x y) } + { ~ (le d x y) }.
    Proof.
      induction x;
        simpl in |- *.
      intuition.
      destruct y;
        simpl in |- *.
      intuition.
      elim (ord.dec d a t);
        intro.
      elim (ord.dec d t a);
        intro.
      elim (IHx y);
        intro.
      left;
        intuition.
      right;
        intuition.
      left;
        intuition.
      right;
        intuition.
    Qed.

  (** Proof that the equal operation is decidable *)
  (* Satisfies DR TBC *)	    
  Lemma eq_opt_dec : forall d x y, { (eq_opt d x y) } + { ~ (eq_opt d x y) }.
    Proof.
      induction x;
        destruct y;
        simpl in |- *.
      intuition.
      intuition.
      intuition.
      elim (ord.eq_dec d a t);
        intro.
      elim (IHx y);
        intro.
      intuition.
      intuition.
      intuition.
    Qed.

  Lemma eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.
    Proof.
      intros.
      elim (eq_opt_dec d x y);
        intros Heqo.
      left;
        apply eq_opt_is_eq;
        exact Heqo.
      right;
        intro Heq;
        apply Heqo;
        apply eq_is_eq_opt;
        exact Heq.    
    Qed.
				
  (** Proof that the lesser or equal operation is reflexive *)
  (* relying on the fact that it is reflexive on the elements in the list *)
  (* Satisfies DR TBC *)	    
  Lemma refl : forall d x, le d x x.
    Proof.
      induction x; 
        simpl in |- *;  
        intuition.
      apply ord.refl.
    Qed.

  (** Proof that the lesser or equal operation is transitive *)
  (* relying on the fact that it is transitive on the elements in the list *)
  (* Satisfies DR TBC *)	    
  Lemma trans : forall d x y z, (le d x y) -> (le d y z) -> (le d x z).
    Proof.
      induction x; 
        simpl in |- *.
      trivial.
      destruct y; 
        simpl in |- *.
      intuition.
      destruct z; 
        simpl in |- *.
      trivial.
      intuition.
      eapply ord.trans.
      apply H1.
      exact H.
      eapply IHx.
      apply H2.
      eapply ord.trans.
      apply H.
      exact H0.
      apply H3.
      eapply ord.trans.
      apply H0.
      exact H1.
    Qed.

End PreOrderLexicographicSequence.

(** The module TotalPreOrderLexicographicSequence defines a total preorder *)
(* on a list of elements of a given data and type upon which *)
(* a total preorder is already defined *)
Module TotalPreOrderLexicographicSequence 
  (ord : TotalPreOrderType)
  <: PreOrderType
    (* The data is the one provided *)
    with Module Data := ord.Data
    (* The type of the lexicographic order is a list *)
    (* of the type of the preorder *)
    (* TODO: MaPa, 2008/08/15, why is the type different than the one *)
    (* of the data *)
    with Definition type := list ord.type.

  (* The TotalPreOrderLexicographicSequence module is a refinement of *)
  (* the PreOrderLexicographicSequence module *)
  Module L := PreOrderLexicographicSequence (ord).

  (* TODO: MaPa, 2008/08/15, why not use the import construct ? *)

  Module Data := L.Data.

  Definition type := L.type.

  Definition le := L.le.

  Definition refl := L.refl.

  Definition trans := L.trans.

  Definition dec := L.dec.

  Definition eq := L.eq.

  Definition eq_dec := L.eq_dec.

  (** Proof that the lesser or equal operation is total *)
  (* relying on the fact that it is total on the elements in the list *)
  (* Satisfies DR TBC *)	    
  Lemma total : forall d x y, { (le d x y) } + { (le d y x) }.
    Proof.
      induction x; 
        simpl in |- *.
      intuition.
      destruct y;
        simpl in |- *.
      intuition.
      elim (ord.dec d a t); 
        elim (ord.dec d t a); 
        intros.
      elim (IHx y);  
        intuition.
      elim (IHx y);  
        intuition.
      elim (IHx y);  
        intuition.
      elim (ord.total d a t);  
        intuition.
    Qed.

End TotalPreOrderLexicographicSequence.

(* The reverse order of a preorder is a preorder *)
(* a is lesser than b in the reverse order *)
(* is b is lesser than a in the initial order *)
Module ReversePreOrder (preOrder : PreOrderType)
  <: PreOrderType  
    with Module Data := preOrder.Data
    with Definition type := preOrder.type.

  Module Data := preOrder.Data.

  Definition type := preOrder.type.

  Definition le d x y := preOrder.le d y x.

  Definition eq := fun d x y => le d x y /\ le d y x.

  (* Satisfies DR TBC *)	    
  Lemma dec : forall d x y, { (le d x y) } + { ~ (le d x y) }.
    Proof.
      unfold le in |- *.
      intros.
      apply (preOrder.dec d y x).
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma eq_dec : forall d x y, { (eq d x y) } + { ~ (eq d x y) }.
    Proof.
      unfold eq in |- *.
      intros.
      elim (preOrder.eq_dec d y x);
        intuition.
    Qed.
					
  (* Satisfies DR TBC *)	    
  Lemma refl : forall d x, le d x x.
    Proof.
      unfold le in |- *.
      apply preOrder.refl.
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma trans : forall d x y z, (le d x y) -> (le d y z) -> (le d x z).
    Proof.
      unfold le in |- *.
      intros.
      eapply preOrder.trans.
      apply H0.
      exact H.
    Qed.

End ReversePreOrder.

(* The reverse order of a total preorder is a total preorder *)
(* a is lesser than b in the reverse order *)
(* is b is lesser than a in the initial order *)
Module ReverseTotalPreOrder (totalOrder : TotalPreOrderType)
  <: TotalPreOrderType  
    with Module Data := totalOrder.Data
    with Definition type := totalOrder.type.

  Module L := ReversePreOrder (totalOrder).
  
  Module Data := L.Data.

  Definition type := L.type.

  Definition le := L.le.

  Definition refl := L.refl.

  Definition trans := L.trans.

  Definition dec := L.dec.

  Definition eq := L.eq.

  Definition eq_dec := L.eq_dec.

  (* Satisfies DR TBC *)	    
  Lemma total : forall d x y, { (le d x y) } + { (le d y x) }.
    Proof.
      unfold le in |- *.
      unfold L.le in |- *.
      intros.
      apply totalOrder.total.
    Qed.

End ReverseTotalPreOrder.

(* Build a preorder using a function A and a preorder Ord *)
(* the function is applied to the elements before comparing the results *)
(* with the preorder *)
Module InverseImagePreOrder
  (A : ArrowType) 
  (preOrder : PreOrderType with Definition type := A.target)
  <: PreOrderType.

  Module Data := preOrder.Data.

  Definition type := A.source.

  Definition le : Data.type -> type -> type -> Prop :=
    fun d x y => preOrder.le d (A.apply x) (A.apply y).	    

  Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

  Definition eq_opt : Data.type -> type -> type -> Prop :=
    fun d x y => preOrder.eq d (A.apply x) (A.apply y).

  Lemma eq_opt_is_eq : forall d x y, eq_opt d x y -> eq d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *.
      auto.
    Qed.

  Lemma eq_is_eq_opt : forall d x y, eq d x y -> eq_opt d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *.
      auto.
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma eq_opt_dec  : forall d x y , { (eq_opt d x y) } + { ~ (eq_opt d x y) }.
    Proof.
      intros.
      unfold eq_opt in |- *.
      generalize (A.apply x); 
      generalize (A.apply y).
      intros t0 t1.
      elim (preOrder.eq_dec d t1 t0);  
        intuition.	    
    Qed.

  Lemma eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.
    Proof.
      intros.
      elim (eq_opt_dec d x y);
        intros Heqo.
      left;
        apply eq_opt_is_eq;
        exact Heqo.
      right;
        intro Heq;
        apply Heqo;
        apply eq_is_eq_opt;
        exact Heq.    
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma refl : forall d x , le d x x.
    Proof.
      intros.
      unfold le in |- *.
      generalize (A.apply x).
      intros.
      apply (preOrder.refl d t).
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma trans : forall d x y z, (le d x y) -> (le d y z) -> (le d x z).
    Proof.
      intros.
      unfold le in |- *.
      unfold le in H; unfold le in H0.
      apply (preOrder.trans d (A.apply x) (A.apply y) (A.apply z));
        assumption.
    Qed.
	    
  (* Satisfies DR TBC *)	    
  Lemma dec : forall d x y, { (le d x y) } + { ~ (le d x y) }.
    Proof.
      intros.
      unfold le in |- *.
      intros.
      apply (preOrder.dec d (A.apply x) (A.apply y)).
   Qed.  

End InverseImagePreOrder.

(* Build a total preorder using a function A and a total preorder Ord *)
(* the function is applied to the elements before comparing the results *)
(* with the preorder *)
Module InverseImageTotalPreOrder 
  (A : ArrowType) 
  (totalPreOrder : TotalPreOrderType with Definition type := A.target)
  <: TotalPreOrderType.

  Module I := InverseImagePreOrder (A) (totalPreOrder).

  Module Data := I.Data.

  Definition type := I.type.

  Definition le := I.le.

  Definition eq := I.eq.

  Definition eq_dec := I.eq_dec.

  Definition refl := I.refl.

  Definition trans := I.trans.	      

  Definition dec := I.dec.

  (* Satisfies DR TBC *)	    
  Lemma total : forall d x y, { (le d x y) } + { (le d y x) }.
    Proof.
      intros; 
        unfold le in |- *.
      unfold I.le in |- *.
      apply (totalPreOrder.total d (A.apply x) (A.apply y)).
    Qed.
	    
End InverseImageTotalPreOrder.

(* The LatticeType module type defines a lattice structure *)
(* on a data set of a given type *)
Module Type LatticeType.

  (* A lattice is a special kind of preorder *)
  (* it is thus a refinement of the PreOrderType *)    
  Declare Module PreOrder : PreOrderType.

  (** The greatest and least elements in the lattice *)
  Variable bot top : PreOrder.type.

  (* bot is the least element *)
  Axiom bot_least : forall d x, PreOrder.le d bot x.

  (* top is the greatest element *)
  Axiom top_greatest : forall d x, PreOrder.le d x top.

  (** The meet (greatest lower bound) and join (least upper bound) *)
  (* operation in the lattice *)
  Variable meet join : PreOrder.Data.type -> PreOrder.type -> PreOrder.type -> PreOrder.type.

  Definition upper d a b x := (PreOrder.le d a x) /\ (PreOrder.le d b x).

  Axiom join_upper : forall d x y, upper d x y (join d x y) .

  Axiom join_supremum : forall d x y m, upper d x y m -> PreOrder.le d (join d x y) m.

  Definition lower d a b x := (PreOrder.le d x a) /\ (PreOrder.le d x b).

  Axiom meet_lower : forall d x y, lower d x y (meet d x y).			    

  Axiom meet_infimum : forall d x y m, lower d x y m -> (PreOrder.le d m (meet d x y)).
						     
  (** The lesser than operation is defined using the lesser or equal *)
  (* operation when x is lesser or equal than y and y is not lesser or *)
  (* equal than x *)
  Definition lt d x y := (PreOrder.le d x y) /\ ~ (PreOrder.le d y x).

  (** The greater than operation is defined using the lesser than operation *)
  Definition gt d x y := lt d y x.

  (** The equal operation is defined using the lesser or equal operation *)
  Definition eq d x y := (PreOrder.le d x y) /\ (PreOrder.le d y x).

  (** The greater than operation must be well founded *)
  Axiom gt_well_founded : forall d, well_founded (gt d).

End LatticeType.  

(** Cartesian product of two lattices *)

Module LatticeCartesianProduct 
  (* First lattice *)
  (L1 : LatticeType) 
  (* Second lattice with the same data as the first *)
  (L2 : LatticeType with Module PreOrder.Data := L1.PreOrder.Data) 
  <: LatticeType.

  (* The preorder for the lattice product is the cartesion product *)
  (* of the preorder of both lattices *)
  Module PreOrder := PreOrderCartesianProduct (L1.PreOrder) (L2.PreOrder).

  (** The lesser than operation is defined using the lesser or equal *)
  (* operation when x is lesser or equal than y and y is not lesser or *)
  (* equal than x *)
  Definition lt d e1 e2 := (PreOrder.le d e1 e2) /\ ~ (PreOrder.le d e2 e1).

  (** The greater than operation is defined using the lesser than operation *)
  Definition gt d e1 e2 := lt d e2 e1.

  (** The equal operation is defined using the lesser or equal operation *)
  Definition eq d e1 e2 := (PreOrder.le d e1 e2) /\ (PreOrder.le d e2 e1).

  (** The greater than operation must be well founded *)
  Axiom gt_well_founded : forall d, well_founded (gt d).

  (* The least element is the pair of both least elements *)
  Definition bot := (L1.bot, L2.bot).

  (** Proof that bot is the least element according to lesser or equal *)
  (* Satisfies DR TBC *)	    
  Theorem bot_least : forall d x, PreOrder.le d bot x.
    Proof.
      destruct x; 
        simpl in |- *; 
        split.
      apply L1.bot_least.
      apply L2.bot_least.
    Qed.

  (* The greatest element is the pair of both least elements *)
  Definition top := (L1.top, L2.top).

  (** Proof that top is the greatest element according to lesser or equal *)
  (* Satisfies DR TBC *)	    
  Theorem top_greatest : forall d x, PreOrder.le d x top.
    Proof.
      destruct x; 
        simpl in |- *; 
        split.
      apply L1.top_greatest.
      apply L2.top_greatest.
    Qed.

  (** x is upper than a and than b *) 
  Definition upper d a b x := (PreOrder.le d a x) /\ (PreOrder.le d b x).

  (* The join of two pair of elements is the pair of the joins of the *)
  (* sub-elements *)
  Definition join : PreOrder.Data.type -> PreOrder.type -> PreOrder.type -> PreOrder.type :=
    fun d x y =>
      match x, y with
        | (x1, x2), (y1, y2) => (L1.join d x1 y1, L2.join d x2 y2)
      end.

  (** Proof that the join of x and y is greater than x and y *)
  (* Satisfies DR TBC *)	    
  Theorem join_upper : forall d x y, upper d x y (join d x y).
    Proof.
      destruct x; 
        destruct y; 
        simpl in |- *; 
        generalize (L1.join_upper d t t1); 
        unfold L1.upper in |- *; 
        generalize (L2.join_upper d t0 t2); 
        unfold L2.upper in |- *; 
        split; 
        split; 
        intuition.
    Qed.

  (** Proof that the join of x and y is the least element greater than x and y *)
  (* also called the least upper bound or LUB *)
  (* Satisfies DR TBC *)	    
  Theorem join_supremum : forall d x y m, (upper d x y m) -> (PreOrder.le d (join d x y) m).
    Proof.
      destruct x; 
        destruct y; 
        destruct m; 
        simpl in |- *; 
        unfold upper in |- *;
        unfold PreOrder.le in |- *.
      intuition.
      apply L1.join_supremum.
      split; 
        assumption.
      apply L2.join_supremum.
      split; 
        assumption.
    Qed.

  (** x is lower than a and than b *) 
  Definition lower d a b x := (PreOrder.le d x a) /\ (PreOrder.le d x b).

  (* The meet of two pair of elements is the pair of the meets of the *)
  (* sub-elements *)
  Definition meet : PreOrder.Data.type -> PreOrder.type -> PreOrder.type -> PreOrder.type :=
    fun d x y =>
      match x, y with
        | (x1, x2), (y1, y2) => (L1.meet d x1 y1, L2.meet d x2 y2)
      end.

  (** Proof that the meet of x and y is lower than x and y *)
  (* Satisfies DR TBC *)	    
  Theorem meet_lower : forall d x y, lower d x y (meet d x y).
    Proof.
      destruct x; 
        destruct y; 
        simpl in |- *; 
        generalize (L1.meet_lower d t t1); 
        unfold L1.lower in |- *; 
        generalize (L2.meet_lower d t0 t2); 
        unfold L2.lower in |- *; 
        split; 
        split; 
        intuition.
    Qed.

  (** Proof that the meet of x and y is the greatest element lower than x and y *)
  (* also called the greatest lower bound or GLB *)
  (* Satisfies DR TBC *)	    
  Theorem meet_infimum : forall d x y m, (lower d x y m) -> (PreOrder.le d m (meet d x y)).
    Proof.
      destruct x; 
        destruct y; 
        destruct m; 
        simpl in |- *; 
        unfold lower in |- *;
      unfold PreOrder.le in |- *.
      intuition.
      apply L1.meet_infimum.
      split; 
        assumption.
      apply L2.meet_infimum.
      split; 
        assumption.
    Qed.

End LatticeCartesianProduct.

Module TaggedType.

  Inductive taggedNumbers : Set :=
  |Call : nat -> taggedNumbers
  |Return : nat -> taggedNumbers
  .
  Definition t := taggedNumbers.

End TaggedType.
(* The OrderedMarkedNumbers module maps marked numbers to an  OrdredType *)
Module OrderedTaggedNumbers : OrderedType with Definition t := TaggedType.t.

  (** The type of the data is markednumbers *)
  (*Inductive taggedNumbers : Set :=
    |Call : nat -> taggedNumbers
    |Return : nat -> taggedNumbers.*)

Definition t := TaggedType.t.
  (*Definition t := taggedNumbers.*)

  (** *)
  Definition eq : t -> t -> Prop := 
    fun x y => 
      match x, y  with 
        |TaggedType.Call n , TaggedType.Call m     => n = m
        |TaggedType.Return n , TaggedType.Return m => n = m
        |_ , _               => False
       end.
  
  (**  *)
  Definition lt : t -> t -> Prop := 
    fun x y =>
      match x , y with 
        |TaggedType.Call n, TaggedType.Call m      => n < m
        |TaggedType.Return n , TaggedType.Return m => n < m
        |TaggedType.Call n , TaggedType.Return m   => True
        |TaggedType.Return n , TaggedType.Call m   => False
      end.    

  (** Proof that equal is reflexive *)
  (* Satisfies DR TBC *)	    
  Lemma eq_refl : forall x : t, (eq x x).
    Proof.
      unfold eq in |- *.
      intros.
      case x.
      intro; auto with arith.
      intro; auto with arith.
    Qed.

  (** Proof that equal is symetric *)
  (* Satisfies DR TBC *)	    
  Lemma eq_sym : forall x y : t, (eq x y) -> (eq y x).
    Proof.
      destruct x; destruct y; simpl in |- *.
      auto with arith.
      auto.
      auto.
      auto with arith.
    Qed.

      
  (** Proof that equal is transitive *)
  (* Satisfies DR TBC *)	    
  Lemma eq_trans : forall x y z : t, (eq x y) -> (eq y z) -> (eq x z).
    Proof.
      destruct x; destruct y; destruct z; unfold eq in |- *;  intuition.
      transitivity n0.
      exact H.
      exact H0.
      transitivity n0;  intuition.
    Qed.

  (** Proof that lesser than is transitive *)
  (* Satisfies DR TBC *)	    
  Lemma lt_trans : forall x y z : t, (lt x y) -> (lt y z) -> (lt x z).
    Proof.
      destruct x; destruct y; destruct z; unfold lt in |- *;  intuition;
      apply (lt_trans n n0 n1); auto with arith.
      Qed.
      (*
      exact H.
      exact H0.
      elim (lt_le_trans n n0 n1).
      auto with arith.
      intros; auto with arith.
      elim (lt_le_trans n n0 n1); auto with arith.
      auto.
      elim (le_lt_trans n n0 n1); auto with arith;  intuition.
      elim (le_lt_trans n n0 n1);  intuition; auto with arith.
      apply (lt_trans n n0 n1);  intuition.
      apply (lt_le_trans n n0 n1);  intuition.
      apply (lt_trans n n0 n1);  intuition.
      apply (lt_trans n n0 n1);  intuition.*)

  (** Proof that if x is lesser than y then it is not equal to y *)
  (* Satisfies DR TBC *)	    
  Lemma lt_not_eq: forall x y : t, (lt x y) -> ~ (eq x y).
    Proof.
      intros; destruct x; destruct y; unfold lt in |- *; unfold eq in |- *;
      intuition.
      unfold lt in H.
      rewrite H0 in H.
      elim (lt_irrefl n0).
      exact H.
      unfold lt in H.
      rewrite H0 in H.
      elim (lt_irrefl n0).
      exact H.
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma compare : forall x y : t, Compare lt eq x y.
    Proof.
      destruct x; destruct y.
      case (lt_eq_lt_dec n n0).
      intro.
      elim s.
      intro.
      apply LT;  intuition.
      intro.
      apply EQ;  intuition.
      intro.
      apply GT;  intuition.
      apply LT.
      unfold lt in |- *.
      auto.
      apply GT.
      unfold lt in |- *.
      auto.
      case (lt_eq_lt_dec n n0).
      intro.
      elim s.
      intro.
      apply LT;  intuition.
      intro.
      apply EQ;  intuition.
      intro.
      apply GT;  intuition.
    Qed.

End OrderedTaggedNumbers.

(** The OrderedNatural module maps natural numbers to an OrderedType *)
Module OrderedNatural <: OrderedType.

  (** The type of the data is nat *)
  Definition t :=  nat.

  (** The equal operation is the standard one on natural numbers *)
  Definition eq : t -> t -> Prop := 
    fun x y => (x = y).

  (** The lesser than operation is the standard one on natural numbers *)
  Definition lt : t -> t -> Prop := 
    fun x y => (x < y).

  (** Proof that equal is reflexive *)
  (* Satisfies DR TBC *)	    
  Lemma eq_refl : forall x : t, (eq x x).
    Proof.
      unfold eq in |- *.
      intro; 
        auto with arith.
    Qed.

  (** Proof that equal is symetric *)
  (* Satisfies DR TBC *)	    
  Lemma eq_sym : forall x y : t, (eq x y) -> (eq y x).
    Proof.
      intros.
      unfold eq in |- *.
      unfold eq in H.
      symmetry  in |- *.
      exact H.
    Qed.

  (** Proof that equal is transitive *)
  (* Satisfies DR TBC *)	    
  Lemma eq_trans : forall x y z : t, (eq x y) -> (eq y z) -> (eq x z).
    Proof.
      unfold eq in |- *.
      intros.
      transitivity y.
      exact H.
      exact H0.
    Qed.

  (** Proof that lesser than is transitive *)
  (* Satisfies DR TBC *)	    
  Lemma lt_trans : forall x y z : t, (lt x y) -> (lt y z) -> (lt x z).
    Proof.
      unfold lt in |- *.
      intros.
      apply (lt_trans x y z).
      exact H.
      exact H0.
    Qed.

  (** Proof that if x is lesser than y then it is not equal to y *)
  (* Satisfies DR TBC *)	    
  Lemma lt_not_eq: forall x y : t, (lt x y) -> ~ (eq x y).
    Proof.
      intros; 
        unfold lt in |- *; 
        unfold eq in |- *.
      intro.
      rewrite H0 in H.
      elim (lt_irrefl y).
      intuition.
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma compare : forall x y : t, Compare lt eq x y.
    Proof.
      intros.
      case (lt_eq_lt_dec x y).
      intro.
      elim s.
      intro.
      apply LT.
      auto.
      intro.
      apply EQ.
      auto.
      intro.
      apply GT.
      auto.
    Qed.

End OrderedNatural.

(** Pair of Ordered Types*)
(*The lexicographic product of 2 Ordered Types is ordered*)

Module OrderedTypeLexicographicProduct (A B : OrderedType) <: OrderedType.

  (** The type of the data is the cartesian product of both types *)
  Definition t := (A.t * B.t)%type.

  (** The equal operation is the standard one on natural numbers *)
  Definition eq : t -> t -> Prop := 
    fun x y => 
      (A.eq (fst x) (fst y)) /\ (B.eq (snd x) (snd y)).

  (** The lesser than operation is the standard one on natural numbers *)
  Definition lt : t -> t -> Prop := 
    fun x y =>
      (A.lt (fst x) (fst y)) 
      \/ ((A.eq (fst x) (fst y)) /\ (B.lt (snd x) (snd y))).

  (** Proof that equal is reflexive *)
  (* Satisfies DR TBC *)	    
  Lemma eq_refl : forall x : t, (eq x x).
    Proof.
      intro; 
        split;  
        intuition.
    Qed.

  (** Proof that equal is symetric *)
  (* Satisfies DR TBC *)	    
  Lemma eq_sym : forall x y : t, (eq x y) -> (eq y x).
    Proof.
      unfold eq in |- *.
      intros.
      elim H.
      intros.
      split.
      apply A.eq_sym.
      exact H0.
      apply B.eq_sym.
      exact H1.
    Qed.

  (** Proof that equal is transitive *)
  (* Satisfies DR TBC *)	    
  Lemma eq_trans : forall x y z : t , (eq x y) -> (eq y z) -> (eq x z).
    Proof.
      unfold eq in |- *.
      intros.
      split.
      elim H.
      intros.
      elim H0.
      intros.
      apply A.eq_trans with (fst y).
      exact H1.
      exact H3.
      elim H; 
        elim H0.
      intros.
      apply B.eq_trans with (snd y).
      exact H4.
      exact H2.
    Qed.

  (** Proof that lesser than is transitive *)
  (* Satisfies DR TBC *)	    
  Lemma lt_trans : forall x y z : t, (lt x y) -> (lt y z) -> (lt x z).
    Proof.
      unfold lt; 
        intros.
      elim H; 
        elim H0; 
        intros.
      left. 
      apply A.lt_trans with (fst y); 
        auto.
      left.
      elim H1; 
        intros.
      case (A.compare (fst x) (fst z)); 
        intro.
      assumption.
      generalize (A.lt_not_eq H2); 
        intro. 
      elim H5.
      apply A.eq_trans with (fst z). 
      auto. 
      auto.
      generalize (@A.lt_not_eq (fst z) (fst y)); 
        intro.
      elim H5. 
      apply A.lt_trans with (fst x); 
        auto.
      apply A.eq_sym; 
        auto.
      left. 
      elim H2; 
        intros.
      case (A.compare (fst x) (fst z)); 
        intro.
      assumption.
      generalize (A.lt_not_eq H1); 
        intro. 
      elim H5.
      apply A.eq_trans with (fst x). 
      apply A.eq_sym. 
      auto. 
      auto.
      generalize (@A.lt_not_eq (fst y) (fst x)); 
        intro.
      elim H5. 
      apply A.lt_trans with (fst z); 
        auto.
      apply A.eq_sym; 
        auto.
      right. 
      elim H1; 
        elim H2; 
        intros.
      split. 
      apply A.eq_trans with (fst y); 
        auto.
      apply B.lt_trans with (snd y); 
        auto.
    Qed.

  (** Proof that if x is lesser than y then it is not equal to y *)
  (* Satisfies DR TBC *)	    
  Lemma lt_not_eq : forall x y: t, (lt x y) -> ~ (eq x y).
    Proof.
      unfold lt in |- *; 
        unfold eq in |- *; 
        intros.
      unfold not in |- *; 
        intro.
      elim H0; 
        intros.
      elim H; 
        intros.
      apply (A.lt_not_eq H3 H1).
      elim H3; 
        intro.
      intro.
      apply (B.lt_not_eq H5 H2).
    Qed.

  (* Satisfies DR TBC *)	    
  Lemma compare : forall x y : t, Compare lt eq x y.
    Proof.
      intros.
      case (A.compare (fst x) (fst y)); 
        intro.
      apply LT.
      red in |- *.
      left.
      assumption.
      case (B.compare (snd x) (snd y)); 
        intro.
      apply LT.
      red in |- *.
      right; 
        split; 
        auto.
      apply EQ.
      red in |- *.
      split.
      auto.
      auto.
      apply GT.
      red in |- *.
      right; 
        auto.
      apply GT.
      red in |- *.
      left; 
        auto.
    Qed.

End OrderedTypeLexicographicProduct.




