(* This code was written by: *)
(*      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7 *)
(*      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7 *)
(* It is released under GPL v2 license *)
(* This file defines the block sequencer elementary tool *)

Require Import List.
Require Import Arith.
Require Import FSetInterface.
Require FSetList.
Require Import Heap.
Require Import Recdef.
Require Import Wf.
Require Import Compare_dec.

Require Import Util.
Require Import GeneAutoLibrary.
Require Import Domain.
Require Import DependenceLattice.
Require Import GeneAutoLanguage.
Require Import EnvironmentLattice. 
Require Import EqNat.

Open Scope nat_scope.
Open Scope list_scope.

Set Printing Projections.

(* Definition of the variables that will be initialized by the CaML *)
(* wrapper *)

(* TODO: MaPa, 2008/08/17, should be a parameter, not a kind of constant *)
Variable model : ModelType.

(* Set of natural numbers used for block indexes *)
(* TODO: MaPa, 2008/08/17, should be the real set of valid block indexes *)
(*Module NaturalFiniteSet := FSetList.Make( OrderedNatural ). *)

Module NaturalFiniteSet := FSetList.Make(OrderedNatural).
Module DependencySet <: FiniteSetType.
  Module FiniteSet := NaturalFiniteSet.
  Fixpoint makeSetFromList (l : list nat) {struct l} : FiniteSet.t :=
    match l with 
      |nil         => FiniteSet.empty
      |head::queue => (FiniteSet.add head (makeSetFromList queue))
    end.
  Axiom makeSetFromList_correctness : forall (e : nat) (l : list nat), List.In e l -> FiniteSet.In e (makeSetFromList l).
  Definition carrier :=
    makeSetFromList (blockIndexes (blocks ModelElementType model)).
End DependencySet.

Module TaggedFiniteSet := FSetList.Make( OrderedTaggedNumbers).
(* The Dependency set *)
Module DependencySetTagged <: FiniteSetTaggedType.
  (* The set of natural numbers used as block indexes  *)
  Module FiniteSet := TaggedFiniteSet. 

  Fixpoint makeSetFromListTagged (l : list TaggedType.t ) {struct l} : FiniteSet.t :=
    match l with 
      |nil           => FiniteSet.empty
      |head::queue   => (FiniteSet.add head (makeSetFromListTagged queue))
    end.

  Fixpoint callList (l : list nat) {struct l} : list TaggedType.t:=
    match l with 
      |nil => nil
      |head::queue => (TaggedType.Call head)::(callList queue)
    end.

  Fixpoint returnList (l : list nat) {struct l} : list TaggedType.t :=
    match l with 
      |nil         => nil
      |head::queue => (TaggedType.Return head)::(returnList queue)
    end.
  
 
  (** Speficication of the makeSetFromList operation *)
  (* Proof that the makeSetFromList is correct *)
  (* TODO: MaPa, 2008/08/17, proof to be done *)

(*Axiom  makeSetFromListTagged_correctness: forall (e : OrderedTaggedNumbers.t) (l : list OrderedTaggedNumbers.t), List.In e l <-> FiniteSet.In e (makeSetFromListTagged l). *)
  Axiom  makeSetFromListTagged_correctness: forall (e : TaggedType.t) (l : list TaggedType.t), List.In e l <-> FiniteSet.In e (makeSetFromListTagged l).

  Definition carrier := 
    let cIndex := callList (blockIndexes (blocks ModelElementType model))
    in 
    let rIndex := returnList (blockIndexes (blocks ModelElementType model))
    in
    makeSetFromListTagged (cIndex++rIndex).

End DependencySetTagged.

(* DependencyLattice.PreOrder= 
      { value : S.FiniteSet.t | S.FiniteSet.Subset value S.carrier }. *)
Module DependencyLattice := FinitePowerSetLattice (DependencySetTagged).

(*DependencyEnvironment.PreOrder.type := nat -> Lattice.PreOrder.type. *)
Module DependencyEnvironment := MakeEnvironment(DependencyLattice).

(* *)
Module EnvironmentPair := LatticeCartesianProduct (DependencyEnvironment) (DependencyEnvironment).

(* *)
Definition dec_eq (A : Set) := forall x y : A, { (x = y) } + { ~ (x = y) }.
  
(* mergeDependencies *)
(* combines with the dependency lattice join operator the dependencies of the *)
(* blocks from the lists *)

Definition mergeDependencies
  (dependencies : DependencyEnvironment.PreOrder.type ) (* nat -> DependencyLattice.PreOrder.type.*)
  (diagram : ModelType) 
  (blockIndexSequence : list nat ) :=
    fold_right 
      (fun blockIndex queue => 
      DependencyLattice.join tt (dependencies blockIndex) queue) 
      DependencyLattice.bot blockIndexSequence.

Definition makeCallListNat (l : list nat) :=
 fold_right (fun t queue => (TaggedType.Call t)::queue) nil l.

Definition makeReturnListNat (l : list nat) :=
  fold_right (fun t queue => (TaggedType.Return t)::queue) nil l.

Fixpoint makeSetFromList (l : list TaggedType.t) {struct l}  : DependencyLattice.PreOrder.type :=
match l with 
  |nil => DependencyLattice.bot
  |t::queue => DependencyLattice.join tt (DependencyLattice.singleton t) (makeSetFromList queue)
end.

(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
(*Forward Input Dependencies with Mraked blocks*)
(*return the set of blocks must be read before excuting the current block*)
(* Din(A) = Dout(A.In) + Din(A.Callers) + call (A.Callers) + return (A.In)*)
Definition forwardInputDependencies
  (diagram : ModelType) 
  (currentInputDependencies : DependencyEnvironment.PreOrder.type) 
  (currentOutputDependencies : DependencyEnvironment.PreOrder.type)
  : DependencyEnvironment.PreOrder.type := 
   fun blockIndex => 
     (* the block number must be between 0 and nbrBlocks - 1 *)
     (* as it is a natural number, it cannot be less than 0 *)
     if (leb (size diagram) blockIndex)
     then DependencyLattice.bot
     else 
       (* Get the block with this index from the diagram *)
       match (lookForBlock blockIndex diagram) with
         (* There exists a block with this index *)
         | (Some blockKind) =>
           (* Get the list of the direct controled blocks : Callers *)
           let controlSources :=
             controlSourceFor (controlSignals ModelElementType diagram) blockIndex
           in
           (* get the list of the direct data providing blocks : In *)
           let dataSources := 
             dataSourceFor (dataSignals ModelElementType diagram) blockIndex 
           in
           (* get the list of input data sources as returned blocks*)
           let returnIn := DependencySetTagged.returnList dataSources 
           in
           (* get the list of callers of the current block *)
           let callCallers := DependencySetTagged.callList controlSources
           in 
             (* Merge the various dependencies *)
             DependencyLattice.join 
               tt 
               (match blockKind with
                 | (Sequential _ _) => DependencyLattice.singleton (TaggedType.Return blockIndex)
                 | _ => DependencyLattice.bot
               end)
               (DependencyLattice.join
                 tt
                 (* Din(callers)*)
                 (mergeDependencies currentInputDependencies diagram controlSources)
                 (* Data provider ouput dependencies *) 
                 (DependencyLattice.join 
                   tt
                  (mergeDependencies
                   currentOutputDependencies
                   diagram 
                   dataSources) (DependencyLattice.join tt (makeSetFromList returnIn)(makeSetFromList callCallers) )))
         | _    => DependencyLattice.bot
       end.

Theorem forwardInputDependencies_default_bot : 
  forall (diagram : ModelType) 
  (currentInputDependencies : DependencyEnvironment.PreOrder.type) 
  (currentOutputDependencies : DependencyEnvironment.PreOrder.type) 
  (n : nat), 
  (size diagram) <= n 
  -> (forwardInputDependencies diagram currentInputDependencies currentOutputDependencies n) = DependencyLattice.bot.
  Proof.
    intros.
    unfold forwardInputDependencies in |- *.
    generalize (leb_complete_conv n (size diagram)).
    case (leb (size diagram) n).
    auto.
    intro.
    absurd (size diagram <= n).
    apply lt_not_le.
    apply H0.
    reflexivity.
    exact H.
  Qed.

(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
(*return the set of blocks must be executed inorder to evaluate the output of the current block*)
(*Dout(A) = Din(A) + Dout(A.Callees) + return(A.Callees) + call (A) *)
Definition forwardOutputDependencies 
  (diagram : ModelType)
  (inputEnv : DependencyEnvironment.PreOrder.type)
  (outputEnv : DependencyEnvironment.PreOrder.type)
  : DependencyEnvironment.PreOrder.type := 
    fun blockIndex => 
      if (leb (size diagram) blockIndex)
      then DependencyLattice.bot
      else
        match (lookForBlock blockIndex diagram) with 
          | Some( blockKind ) => 
            (* get the list of blocks controlled by the current block*)
            let controlTargets := controlTargetFor (controlSignals ModelElementType diagram) blockIndex 
            in              
              DependencyLattice.join 
                tt 
                 (DependencyLattice.singleton (TaggedType.Call blockIndex))
                 (DependencyLattice.join tt 
                   (makeSetFromList (DependencySetTagged.returnList controlTargets))
                   (DependencyLattice.join tt 
                (* the output dependencies of the controling blocks *)
                (* TODO : MaPa, 2008/09/02, must check the meaning of *)
                (* controled sequential blocks *)
                 (mergeDependencies outputEnv diagram controlTargets)
                 (* The input dependencies of the block itself *) 
                 (match blockKind with
                  | Sequential _ _ => (DependencyLattice.singleton (TaggedType.Call blockIndex)) 
                  | _ => (inputEnv blockIndex)
                end)))
          | error => DependencyLattice.bot
        end.


(*Tr-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
(*return the set of blocks must be executed inorder to evaluate the output of the current block*)
(*Dout(A) = Din(A) + Dout(A.Callees) + return(A.Callees) + call (A) *)

Theorem forwardOutputDependencies_default_bot : 
  forall (diagram : ModelType) 
  (currentInputDependencies : DependencyEnvironment.PreOrder.type) 
  (currentOutputDependencies : DependencyEnvironment.PreOrder.type) 
  (n : nat), 
  (size diagram) <= n 
  -> (forwardOutputDependencies diagram currentInputDependencies currentOutputDependencies n) = DependencyLattice.bot.
  Proof.
    intros.
    unfold forwardOutputDependencies in |- *.
    generalize (leb_complete_conv n (size diagram)).
    case (leb (size diagram) n).
    auto.
    intro.
    absurd (size diagram <= n).
    apply lt_not_le.
    apply H0.
    reflexivity.
    exact H.
  Qed.


(* TR-BS-C019 *)
(* TR-BS-C021 *)
(* TR-BS-C022 *)
(* TR-BS-C023 *)
(* TR-BS-C024 *)
(* TR-BS-C025 *)
(** function forwardDependencies *)
(*  Compute the next step in the iterative fixpoint computation *)
(*  of the dependencies *)
Definition forwardDependencies 
  (diagram : ModelType) 
  (currentDependencies : EnvironmentPair.PreOrder.type) 
  : EnvironmentPair.PreOrder.type :=
    match currentDependencies with
      | (currentInputDependencies , currentOutputDependencies) => 
        (
          match (cache _ (size diagram) DependencyLattice.bot (forwardInputDependencies diagram currentInputDependencies currentOutputDependencies)) with
            | Cache f => f
          end
        , 
          match (cache _ (size diagram) DependencyLattice.bot (forwardOutputDependencies diagram currentInputDependencies currentOutputDependencies)) with
            | Cache f => f
          end
        )
    end.

(* A property is complete and decidable if it is either true or false *)
(* and the truth value can be computed in a finite number of steps *)
Definition decidable (p: Prop) := { p } + { ~ p }.

 (* Axiom out_dep_mono: forall d, forall E E': DependencyEnvironment.PreOrder.type, DependencyEnvironment.PreOrder.le (tt, (size d)) E E' -> DependencyEnvironment.PreOrder.le (tt, (size d)) (outputDependencies E d) (outputDependencies E' d). *)


(*TR-BS-C015*)
Lemma mergeDependencies_monotonic : 
   forall E E' : DependencyEnvironment.PreOrder.type, 
   forall diagram : ModelType, 
   forall blockIndexSequence : list nat, 
     (validBlockIndexesPredicate diagram blockIndexSequence) 
     -> (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E E') 
     -> (DependencyLattice.PreOrder.le 
          tt 
          (mergeDependencies E diagram blockIndexSequence) 
          (mergeDependencies E' diagram blockIndexSequence)).
Proof.
  unfold DependencyEnvironment.PreOrder.le in |- *.
  intros E E' diagram blockIndexSequence.
  elim blockIndexSequence; 
    clear blockIndexSequence;
    simpl in |- *.
  intros;
    firstorder.
  intros.
  apply DependencyLattice.join_monotonic.
  intuition.
  firstorder.
Qed.

(*TR-BS-C015*)
Lemma forwardInputDependencies_monotonic : 
  forall diagram : ModelType, 
  forall E1 E2 E3 E4 : DependencyEnvironment.PreOrder.type,
    (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E1 E3)
    -> (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E2 E4)
    -> (DependencyEnvironment.PreOrder.le 
         (tt, (size diagram)) 
         (forwardInputDependencies diagram E1 E2) 
         (forwardInputDependencies diagram E3 E4)).
   Proof.
     unfold forwardInputDependencies in |- *.
     intros.
     unfold DependencyEnvironment.PreOrder.le in |- *.
     intros.
     generalize (leb_complete (size diagram) n).
     generalize (leb_complete_conv n (size diagram)).
     case (leb (size diagram) n).
     simpl in |- *.
     intros.
     unfold DependencySetTagged.FiniteSet.Subset in |- *.
     intros; auto.
     intros.
     case (lookForBlock n diagram).
     intro.
     apply DependencyLattice.join_monotonic.
     case b.
     intros.
     apply DependencyLattice.PreOrder.refl.
     intros.
     apply DependencyLattice.PreOrder.refl.
     apply DependencyLattice.join_monotonic.
     apply mergeDependencies_monotonic.
     apply validControlSourceBlocks.
     exact H.
     apply DependencyLattice.join_monotonic.
     apply mergeDependencies_monotonic.
     apply validDataSourceBlocks.
     exact H0.
     apply DependencyLattice.join_monotonic.
     apply DependencyLattice.PreOrder.refl.
     apply DependencyLattice.PreOrder.refl.
     apply DependencyLattice.PreOrder.refl.
   Qed. 

(*TR-BS-C015*)
Lemma forwardOutputDependencies_monotonic : 
  forall diagram : ModelType, 
  forall E1 E2 E3 E4 : DependencyEnvironment.PreOrder.type, 
    (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E1 E3)
    -> (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E2 E4)
    -> (DependencyEnvironment.PreOrder.le 
         (tt, (size diagram)) 
         (forwardOutputDependencies diagram E1 E2) 
         (forwardOutputDependencies diagram E3 E4)).
  Proof.
    unfold forwardOutputDependencies in |- *.
    intros.
    unfold DependencyEnvironment.PreOrder.le in |- *.
    intros.
    generalize (leb_complete (size diagram) n).
    generalize (leb_complete_conv n (size diagram)).
    case (leb (size diagram) n).
    intros; apply DependencyLattice.PreOrder.refl.
    intros.
    case (lookForBlock n diagram).
    intros.
    apply DependencyLattice.join_monotonic.
    (*apply DependencyLattice.join_monotonic.*)
    apply DependencyLattice.PreOrder.refl.
    (*apply DependencyLattice.PreOrder.refl.*)
    apply DependencyLattice.join_monotonic.
   (* apply mergeDependencies_monotonic.*)
    apply DependencyLattice.PreOrder.refl.
    apply DependencyLattice.join_monotonic.
    apply mergeDependencies_monotonic.
    apply validControlTargetBlocks.
    exact H0.
    case b.
    intros.
    apply DependencyLattice.PreOrder.refl.
    intros.
    intuition.
    apply DependencyLattice.PreOrder.refl.
  Qed.     

(*TR-BS-C015*)
(** Proof that the forward operation is monotonic *)
(* This is a derived requirement to prove the termination of the algorithm *)
Lemma forwardDependencies_monotonic : 
  forall diagram : ModelType, 
  forall dependenciesLeft dependenciesRight : EnvironmentPair.PreOrder.type,
    (EnvironmentPair.PreOrder.le 
      (tt, (size diagram)) 
      dependenciesLeft 
      dependenciesRight) 
    -> (EnvironmentPair.PreOrder.le 
         (tt, (size diagram)) 
         (forwardDependencies diagram dependenciesLeft) 
         (forwardDependencies diagram dependenciesRight)).
  Proof.
    unfold forwardDependencies in |- *.
    intros.
    destruct dependenciesLeft; 
      destruct dependenciesRight.
    rewrite (cache_is_identity _ _ _ _ (forwardInputDependencies_default_bot  diagram t  t0)).
    rewrite (cache_is_identity _ _ _ _ (forwardOutputDependencies_default_bot diagram t  t0)).
    rewrite (cache_is_identity _ _ _ _ (forwardInputDependencies_default_bot  diagram t1 t2)).
    rewrite (cache_is_identity _ _ _ _ (forwardOutputDependencies_default_bot diagram t1 t2)).
    simpl in |- *;
      intuition.
    apply forwardInputDependencies_monotonic.
    elim H.
    intro.
    intro.
    exact H0.
    elim H.
    intros.
    exact H1.
    apply forwardOutputDependencies_monotonic.
    elim H.
    intros.
    exact H0.
    elim H.
    intros.
    exact H1.
  Qed.

(* *)
Inductive isIterate (A : Set) (function : A -> A) (initial : A) : A -> Prop :=
  | O_iter : isIterate A function initial initial
  | S_iter : forall currentStep, 
    isIterate A function initial currentStep 
    -> isIterate A function initial (function currentStep).

 (* *)
 Definition isIterateBot diagram dependencies := 
   isIterate 
     EnvironmentPair.PreOrder.type 
     (forwardDependencies diagram) 
     (fun _ => DependencyLattice.bot , fun _ => DependencyLattice.bot) 
     dependencies.

Lemma post_fixpoint_forwardDependencies : 
  forall 
    (diagram : ModelType)
    (dependencies : EnvironmentPair.PreOrder.type),
  let nextDependencies := forwardDependencies diagram dependencies in
    (isIterate 
      _ 
      (forwardDependencies diagram) 
      (fun _ => DependencyLattice.bot, fun _ => DependencyLattice.bot) 
      dependencies) 
    -> EnvironmentPair.PreOrder.le (tt, (size diagram)) dependencies nextDependencies.
  Proof.
    intros diagram dependencies.
    induction dependencies.
    intros.
    unfold nextDependencies in |- *.
    induction H.
    simpl in |- *.
    split.
    unfold DependencyEnvironment.PreOrder.le in |- *.
    intros.
    simpl in H.
    simpl in |- *.
    unfold DependencySetTagged.FiniteSet.Subset in |- *.
    rewrite (cache_is_identity _ _ _ _ (forwardInputDependencies_default_bot diagram (fun _ : nat => DependencyLattice.bot) (fun _ : nat => DependencyLattice.bot))).
    destruct forwardInputDependencies.
    intros.
    absurd (DependencySetTagged.FiniteSet.In a0 DependencySetTagged.FiniteSet.empty).
    apply DependencySetTagged.FiniteSet.empty_1.
    exact H0.
    unfold DependencyEnvironment.PreOrder.le in |- *.
    intros.
    simpl in H.
    simpl in |- *.
    unfold DependencySetTagged.FiniteSet.Subset in |- *.
    rewrite (cache_is_identity _ _ _ _ (forwardOutputDependencies_default_bot diagram (fun _ : nat => DependencyLattice.bot) (fun _ : nat => DependencyLattice.bot))).
    destruct forwardOutputDependencies.
    intros.
    absurd (DependencySetTagged.FiniteSet.In a0 DependencySetTagged.FiniteSet.empty).
    apply DependencySetTagged.FiniteSet.empty_1.
    exact H0.
    apply forwardDependencies_monotonic.
    auto.
  Qed.

(*Compute the least fixed point of a recursive definition *)
(*Proof the TBC *)
(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
Function Scheduler_rec
  (diagram : ModelType) 
  (dependencies nextDependencies : EnvironmentPair.PreOrder.type) 
  (iterator : isIterateBot diagram dependencies)
  (nextIsForward : (forwardDependencies diagram dependencies) = nextDependencies)
  { wf (EnvironmentPair.gt (tt, (size diagram))) dependencies } 
  : EnvironmentPair.PreOrder.type :=
    if (EnvironmentPair.PreOrder.dec (tt, (size diagram)) 
      nextDependencies 
      dependencies) 
    then dependencies
    else Scheduler_rec diagram nextDependencies (forwardDependencies diagram nextDependencies) (eq_ind _ _ (S_iter _ _ _ _ iterator) nextDependencies nextIsForward) (refl_equal (forwardDependencies diagram nextDependencies)).
  Proof.
    intros.
    unfold EnvironmentPair.gt in |- *.
    subst.
    cut (EnvironmentPair.PreOrder.le 
          (tt, size diagram) 
          dependencies 
          (forwardDependencies diagram dependencies)).
    intro.
    unfold EnvironmentPair.lt in |- *.
    split.
    exact H.
    intro H'.
    apply anonymous.
    exact H'.
    apply post_fixpoint_forwardDependencies.
    exact iterator.
    intro d'.
    apply (EnvironmentPair.gt_well_founded (tt, size d')).
  Defined.

(** Correction Order for data signals *)

Definition isConnected (b1 b2 : nat) (d : ModelType) :=
  In b1 (dataSourceFor (dataSignals ModelElementType d) b2 ).

(*B1 is called by B2*)
Definition isCalled (b1 b2 : nat) (d : ModelType) :=
  In b2 (controlTargetFor (controlSignals ModelElementType d) b1 ).

(*Definition isSequentialProp (d : ModelType) (b  : nat) := 
match (isSequential b d) with 
 | true => True
 | false => False
end.
*)

(* prove that mergeDependencies of a block includes the block itself *)
Lemma mergeDependencies_le : 
  forall d b E l, (In b l) 
    -> DependencyLattice.PreOrder.le tt (E b) (mergeDependencies E d l).
  Proof.
    intros d b E l0.
    elim l0;
      clear l0;
      simpl in |- *.
    intro.
    intuition.
    intros.
    elim H0.
    intros.
    rewrite H1 in |- *.
    generalize 
      (DependencyLattice.join_upper tt (E b) (mergeDependencies E d l)).
    unfold DependencyLattice.upper in |- *.
    intros.
    intuition.
    intro.
    destruct l.
    absurd (In b nil).
    apply in_nil.
    intuition.
    eapply DependencyLattice.PreOrder.trans.
    apply H.
    exact H1.
    generalize
      (DependencyLattice.join_upper tt (E a) (mergeDependencies E d (n :: l))).
    unfold DependencyLattice.upper in |- *.
    intuition.
  Qed.

(* *)
(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
Definition computeDependencies
  (diagram : ModelType) : EnvironmentPair.PreOrder.type :=
    let initialDependencies := 
      (fun _ => DependencyLattice.bot , fun _ => DependencyLattice.bot)
    in 
      Scheduler_rec 
        diagram 
        initialDependencies 
        (forwardDependencies diagram initialDependencies)
        (O_iter 
          EnvironmentPair.PreOrder.type 
          (forwardDependencies diagram) 
          initialDependencies)
        (refl_equal (forwardDependencies diagram initialDependencies)).

Definition modelDependencies := (computeDependencies model).

Definition modelInputDependencies
  (diagram : ModelType) : DependencyEnvironment.PreOrder.type :=
    fst (computeDependencies diagram).

Definition modelOutputDependencies
  (diagram : ModelType) : DependencyEnvironment.PreOrder.type :=
    snd (computeDependencies (diagram)).

(*Data Loop Detection*)

(*
Axiom inputDependency_correctness :
  forall (d : ModelType) (b : nat), (In b d.blockIndexes) ->
    let block := lookForBlock d b in
    let input := inputDependency(d)(b) in
      ((block.kind = Sequential) -> (input = (singleton b))).
*)
(** Loops detection
   -E : is the forwardOutputDependencies Environment*)
(*TR-BS-C010*)
(****TODO*****)
(*Definition blocksInAlgebraicLoop
  (dependencies : EnvironmentPair.PreOrder.type) 
  (blocks : list nat) : list nat :=
    filter 
      (fun blockIndex => isSequential blockIndex model) 
      (filter 
        (fun blockIndex => 
          DependencySet.FiniteSet.mem blockIndex  
          (proj1_sig (fst dependencies blockIndex)))
        blocks).
*)


(* Total ordering based on the assigned priority natural number property. *)
(* TR-BS-C027 *)

Module AssignedPriorityTotalPreOrder := NaturalTotalPreOrder.

(* Ordering based on the optional user defined priority natural number *)
(* property *)
(*TR-BS-C026*)

Module UserDefinedPriorityTotalPreOrder := PriorityPreOrder.

(*TR-BS-C026*)
(*TR-BS-C027*)
(* Lexicographic combined ordering between user defined priority *)
(* and assigned priority *)
(* The lexicographic product ensure that the user defined priority is *)
(* handled before the assigned priority *)
Module UserDefinedThenAssignedTotalPreOrder := 
  TotalPreOrderLexicographicProduct 
    (UserDefinedPriorityTotalPreOrder)
    (AssignedPriorityTotalPreOrder).

(** *Order Tuple Module*)

(* Types of the functions that will provide the assigned priority and *)
(* user defined priority *)

(* These are functions from the block index to the priority value *)

(* TODO: MaPa, 2008/08/17, It should not be nat *)
(* but a range between min and max *)

Definition AssignedPriorityFunctionType := 
  nat -> AssignedPriorityTotalPreOrder.type.

Definition UserDefinedPriorityFunctionType := 
  nat -> UserDefinedPriorityTotalPreOrder.type.

(* TODO: MaPa, 2008/08/17, should use the value inside the block not *)
(* an external function *)
Variable userDefinedPriorityFunction : UserDefinedPriorityFunctionType.

(* TODO: MaPa, 2008/08/17, should use the value inside the block not *)
(* an external function *)
Variable assignedPriorityFunction : AssignedPriorityFunctionType.

 (* TR-BS-C020 *)
(* Return the pair of priorities: user defined and assigned *)
 Module UserDefinedThenAssignedFunction <: ArrowType.

   (* *)
   Definition source := nat.

   (* *)
   Definition target := UserDefinedThenAssignedTotalPreOrder.type.

   (* *)
   Definition apply b := (userDefinedPriorityFunction b, assignedPriorityFunction b).

 End UserDefinedThenAssignedFunction.

(* Order on block indexes based on lexicographic product of user defined *)
(* priority and assigned priority *)
(* Combines a function that associate the pair of priorities to each index *)
(* and the ordering on the pair or priorities *)
(*gives the set of blocks ordered by lexicographic(priority, position) *)
(* TR-BS-C020 *)
Module BlockUserDefinedThenAssignedTotalPreOrder := InverseImageTotalPreOrder 
  (UserDefinedThenAssignedFunction) 
  (UserDefinedThenAssignedTotalPreOrder).

(* Reverse order on block indexes based on priorities *)
(* The greatest becomes the least and vice versa *)
(* TR-BS-C020 *)
Module ReverseBlockUserDefinedThenAssignedTotalPreOrder := 
  ReverseTotalPreOrder (BlockUserDefinedThenAssignedTotalPreOrder).

(* Order on sequence of blocks defined lexicographicaly using *)
(* the priorities order *)
(* list of blocks ordered by lexicographic(userpriority,assigned priority)*)
(* TR-BS-C020 *)
Module DependencyUserDefinedThenAssignedTotalPreOrder := 
  TotalPreOrderLexicographicSequence 
    (BlockUserDefinedThenAssignedTotalPreOrder).

(* Associate to each block index the reverse sorted list of dependencies *)
(* according to the reverse priorities *)
(* TR-BS-C020 *)
Module SortedDependencyFunction <: ArrowType.

  (* *)
  Definition source := nat.

  (* *)
  Definition target := DependencyUserDefinedThenAssignedTotalPreOrder.type.
 
(*Data Loops existance*)
(* return A in Din(A)  \/  return A in Dout(A) *)
Definition existsDataLoop (S : DependencyLattice.PreOrder.type) :=
  fun b => existsb  (fun t => match t with 
                                |TaggedType.Return n => if (beq_nat n b) then true else false 
                                |_ => false
                              end) (DependencySetTagged.FiniteSet.elements (proj1_sig S)).

(*Data Loop Removing*)
(* remove blocks where return A in Din(A) *)
Definition removeDataLoops (diagram: ModelType) (S: DependencyLattice.PreOrder.type) :=
 fun b => filter (fun t => match t with
                     |TaggedType.Return n => if (beq_nat n b) then (if(negb (isSequential b model)) then true else false ) else false
                     |_ => false
                   end) (DependencySetTagged.FiniteSet.elements (proj1_sig S)).

Definition removeControlLoops (diagram: ModelType) (S: DependencyLattice.PreOrder.type) :=
 fun b => filter (fun t => match t with
                     |TaggedType.Call n => if (beq_nat n b) then (if negb(existsDataLoop S b) then true else false) else false
                     |_ => false
                   end) (DependencySetTagged.FiniteSet.elements (proj1_sig S)).

Definition DependencySetWithoutReturn (S : DependencyLattice.PreOrder.type) :=
  filter (fun t => match t with 
                     |TaggedType.Call n => true
                     |_                 => false
                   end) (DependencySetTagged.FiniteSet.elements (proj1_sig S)).

(*new version*)
Definition DependencyListBlocks (l : list TaggedType.taggedNumbers) :=
 fold_right (fun t queue => (match t with 
                              |TaggedType.Call n   => n   
                              |TaggedType.Return n => n 
                            end)::queue) nil l.

Definition removeCalleesBlocks (l : list nat) :=
  filter (fun t => 
    (match (isControled (controlSignals ModelElementType model) t) with
       |false => true
       |true => false
     end))
  l.

(* Sort the dependencies using the reverse priorities order *)
Definition sortDependency (blockIndexSequence : DependencyLattice.PreOrder.type) :=
    match 
      treesort 
        ReverseBlockUserDefinedThenAssignedTotalPreOrder.type 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.le tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.eq tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.total tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.eq_dec tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.trans tt)
        (removeCalleesBlocks (DependencyListBlocks (DependencySetWithoutReturn blockIndexSequence)))
      (*  (DependencySetTaggedNatToNat (DependencySetWithoutReturn blockIndexSequence))  *)
       (* (DependencySet.FiniteSet.elements (proj1_sig blockIndexSequence)) *)  
    with
      | exist2 sortedBlockIndexSequence _ _ => sortedBlockIndexSequence
    end.
  (* Avoid recomputing several time the sorting by using a cache *)
  Definition cacheSortDependency := 
    cache _ 
      (* Size of the cache *)
      (size model)
      (* Default value outside the cache *)
      (removeCalleesBlocks (DependencyListBlocks (DependencySetWithoutReturn DependencyLattice.bot)))
      (*(DependencySet.FiniteSet.elements (proj1_sig DependencyLattice.bot))*)
      (* Initialize the cache with the results of the sorting *) 
      (fun blockIndex => sortDependency (snd modelDependencies blockIndex)).

  (* TODO : remove the model reference but manipulating blocks instead of indexes *)
  Definition apply blockIndex := 
    match cacheSortDependency with
      | Cache function => (function blockIndex)
    end.

End SortedDependencyFunction.

(* The GeneAuto specific pre order defined on blocks based on the dependency *)
(* to ensure correctness and the user defined and assigned priorities to *)
(* ensure deterministic unique sorting of blocks *)
(* TR-BS-C020 *)
Module GeneAutoBlockTotalPreOrder := InverseImageTotalPreOrder 
  (SortedDependencyFunction) 
  (DependencyUserDefinedThenAssignedTotalPreOrder).

(** Sorting blocks according to :
     -Output Dependencies
     -User Defined priority
     -Assigned priority *)
(* TR-BS-C020 *)
Definition sortBlocks ( blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :=
 (treesort 
   GeneAutoBlockTotalPreOrder.type  
   (GeneAutoBlockTotalPreOrder.le tt) 
   (GeneAutoBlockTotalPreOrder.eq tt) 
   (GeneAutoBlockTotalPreOrder.total tt) 
   (GeneAutoBlockTotalPreOrder.eq_dec tt) 
   (GeneAutoBlockTotalPreOrder.trans tt) 
   blockIndexes).

Definition splitIsolatedBlocks (blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :
  (list GeneAutoBlockTotalPreOrder.type) * (list GeneAutoBlockTotalPreOrder.type) :=
  partition 
(* TODO : remove the model reference but manipulating blocks instead of indexes *)
  (fun blockIndex => 
    (isIsolated 
      (dataSignals ModelElementType model) 
      (controlSignals ModelElementType model) 
      blockIndex)) 
  blockIndexes.

Definition splitLoopingBlocks (blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :
  (list GeneAutoBlockTotalPreOrder.type) * (list GeneAutoBlockTotalPreOrder.type) :=

  partition
    (fun blockIndex =>
      (negb (isSequential blockIndex model)) && (*  (List.In blockIndex (SortedDependencyFunction.DependencyListBlocks (SortedDependencyFunction.DependencySetWithoutReturn blockIndexes)))*)  
 
      (match (List.In blockIndex   
                  (SortedDependencyFunction.DependencyListBlocks 
                    (SortedDependencyFunction.DependencySetWithoutReturn (fst modelDependencies blockIndex))) ) 
         with
           |True => true
           
                     end)
   (*  (DependencySet.FiniteSet.mem blockIndex (proj1_sig (fst modelDependencies blockIndex))     )*)     )
    blockIndexes.

Definition splitControledBlocks ( blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :
  (list GeneAutoBlockTotalPreOrder.type) * (list GeneAutoBlockTotalPreOrder.type) :=
  partition
(* TODO : remove the model reference but manipulating blocks instead of indexes *)
  (fun blockIndex => 
      (isControled (controlSignals ModelElementType model) blockIndex))
    blockIndexes.


(*isolated blocks are first executed *)
(* TR-BS-C020 *)
(* TR-BS-C021 *)
Definition sequenceBlocks (blocks : list GeneAutoBlockTotalPreOrder.type) :=
  let (alone,linked) := splitIsolatedBlocks blocks in
    let (controled,free) := splitControledBlocks linked in
(*  let (inLoop,outLoop) := splitLoopingBlocks free in *)
      let inLoop : list GeneAutoBlockTotalPreOrder.type := nil in
        let outLoop : list GeneAutoBlockTotalPreOrder.type := free in
          let sortedAloneBlocks := (sortBlocks alone) in
            let sortedControledBlocks := (sortBlocks controled) in
              let sortedOutLoopBlocks := (sortBlocks outLoop) in
                match sortedAloneBlocks with
                  exist2 dataSortedAloneBlocks _ _ => 
                  match sortedControledBlocks with
                    exist2 dataSortedControledBlocks _ _ => 
                    match sortedOutLoopBlocks with
                      exist2 dataSortedOutLoopBlocks _ _ =>
                      ((inLoop, dataSortedControledBlocks),(app dataSortedAloneBlocks dataSortedOutLoopBlocks))
                    end
                  end
                end.


Definition sequenceModel (model : ModelType) := model.

(* TODO: MaPa, 2008/08/15, new architecture for enhanced performance *)
(* step 1 : read model -> importedBlocks (FiniteMap) + signals *)
(* step 2 : signals -> internalBlocks (FiniteMap) (add input/output) *)
(*          input/output = FiniteSet importedBlocks *)
(* step 3 : internalBlocks -> annotatedBlocks (add dependencies) *)
(*          dependencies = (sorted) FiniteSet of importedBlocks *)
(* step 4 : detect algebraic loops *)
(* step 5 : extract uncontrolled blocks *)
(* step 6 : sort these annotatedBlocks *)
(* step 7 : compute position in list *)
(* step 8 : merge loops, controlled, uncontrolled blocks *)

(*Correctness Properties*)
(*prove that for a non sequential block the InputDependencies is included in its OutputDependencies*)
Lemma OutInBlock : 
  forall c B E1 E2 , ((isSequential B c) = false)
    -> (B < (size c)) 
    -> (forwardOutputDependencies c E1 E2) = E2
    -> (forwardInputDependencies c E1 E2) = E1
    -> DependencyLattice.PreOrder.le tt (E1 B) (E2 B).
  Proof.
intros.
rewrite <- H1 in |- *.
unfold forwardOutputDependencies in |- *.
case (le_lt_dec B (size c)).
 intro.
   generalize (lookForBlocksValid c B).
   generalize H.
   unfold isSequential in |- *.
   case (lookForBlock B c).
  intro.
    destruct b.
   intro;  discriminate.
  intros.
    generalize
     (DependencyLattice.join_upper tt
        (DependencyLattice.singleton (TaggedType.Call B))
        (DependencyLattice.join tt
           (makeSetFromList
              (DependencySetTagged.returnList
                 (controlTargetFor c.(controlSignals ModelElementType) B)))
           (DependencyLattice.join tt
              (mergeDependencies E2 c
                 (controlTargetFor c.(controlSignals ModelElementType) B))
              (E1 B)))).
    unfold DependencyLattice.upper in |- *;  intuition.
    generalize
     (DependencyLattice.join_upper tt
        (makeSetFromList
           (DependencySetTagged.returnList
              (controlTargetFor c.(controlSignals ModelElementType) B)))
        (DependencyLattice.join tt
           (mergeDependencies E2 c
              (controlTargetFor c.(controlSignals ModelElementType) B))
           (E1 B))).
    unfold DependencyLattice.upper in |- *;  intuition.
    generalize
     (DependencyLattice.join_upper tt
        (mergeDependencies E2 c
           (controlTargetFor c.(controlSignals ModelElementType) B)) 
        (E1 B)).
    unfold DependencyLattice.upper in |- *;  intuition.
     eapply DependencyLattice.PreOrder.trans.   
     apply H11.  
    generalize (leb_correct_conv B (size c)).
    generalize (leb_correct (size c) B).
    case (leb (size c) B).
   intros; simpl in |- *;  intuition.
      discriminate.
  intros.
    generalize
     (DependencyLattice.join tt
        (mergeDependencies E2 c
           (controlTargetFor c.(controlSignals ModelElementType) B)) 
        (E1 B)).
    intros.
    elim
     (DependencyLattice.join_upper tt t
        (makeSetFromList
           (DependencySetTagged.returnList
              (controlTargetFor c.(controlSignals ModelElementType) B)))).
    intros.
     eapply DependencyLattice.PreOrder.trans.   
     apply H13.   
    generalize
     (makeSetFromList
        (DependencySetTagged.returnList
           (controlTargetFor c.(controlSignals ModelElementType) B))).
    intros.
    generalize
     (DependencyLattice.join_upper tt (DependencyLattice.join tt t t0)
        (DependencyLattice.singleton (TaggedType.Call B))).
    intros.
    generalize (DependencyLattice.join_commutative tt t0 t).
    intros.
    elim H16.
    intros.
    elim
     (DependencyLattice.join_upper tt (DependencyLattice.join tt t0 t)
        (DependencyLattice.singleton (TaggedType.Call B))).
    intros.
     eapply DependencyLattice.PreOrder.trans.   
     apply H18.    
    generalize
     (DependencyLattice.join_upper tt (DependencyLattice.join tt t0 t)
        (DependencyLattice.singleton (TaggedType.Call B))).
    unfold DependencyLattice.upper in |- *;  intuition.
     eapply DependencyLattice.PreOrder.trans.   
     apply H19.   
    elim
     (DependencyLattice.join_commutative tt (DependencyLattice.join tt t0 t)
        (DependencyLattice.singleton (TaggedType.Call B))).
    intros.
     intuition.  
   intros.
    absurd (@ None BlockKindType = None).
   intuition.
 auto. 
  intro.
   absurd (B < B).
 auto with arith.
elim (lt_trans B (size c) B);  intuition.
Qed.

Lemma OutInConnexion : 
  forall c B1 B2 E1 E2 ,
    ((isSequential B2 c) = false)
    -> isConnected B1 B2 c
    -> (B1 < (size c))
    -> (B2 < (size c))
    -> (forwardInputDependencies c E1 E2) = E1
    -> (forwardOutputDependencies c E1 E2) = E2
    -> DependencyLattice.PreOrder.le tt (E2 B1) (E1 B2).
Proof.
intros.
rewrite <- H3 in |- *.
unfold forwardInputDependencies in |- *.
case (le_lt_dec B2 (size c)).
 intro.
   generalize (lookForBlocksValid c B2).
   generalize H1.
   unfold isSequential in |- *.
   case (lookForBlock B2 c).
  destruct b.
   intros.
     unfold isConnected in H2.
     generalize
      (DependencyLattice.join_upper tt
         (mergeDependencies E1 c
            (controlTargetFor c.(controlSignals ModelElementType) B2))
         (mergeDependencies E2 c
            (dataSourceFor c.(dataSignals ModelElementType) B2))).
     unfold DependencyLattice.upper in |- *;  intuition.
      eapply DependencyLattice.PreOrder.trans.
    
      apply mergeDependencies_le.
      
      apply H0.
   
     
     
     generalize
      (DependencyLattice.join_upper tt
         (DependencyLattice.singleton (TaggedType.Return B2))
         (DependencyLattice.join tt
            (mergeDependencies E1 c
               (controlSourceFor c.(controlSignals ModelElementType) B2))
            (DependencyLattice.join tt
               (mergeDependencies E2 c
                  (dataSourceFor c.(dataSignals ModelElementType) B2))
               (DependencyLattice.join tt
                  (makeSetFromList
                     (DependencySetTagged.returnList
                        (dataSourceFor c.(dataSignals ModelElementType) B2)))
                  (makeSetFromList
                     (DependencySetTagged.callList
                        (controlSourceFor c.(controlSignals ModelElementType)
                           B2))))))).
     unfold DependencyLattice.upper in |- *;  intuition.
     
     generalize
      (DependencyLattice.join_upper tt
         (mergeDependencies E1 c
            (controlSourceFor c.(controlSignals ModelElementType) B2))
         (DependencyLattice.join tt
            (mergeDependencies E2 c
               (dataSourceFor c.(dataSignals ModelElementType) B2))
            (DependencyLattice.join tt
               (makeSetFromList
                  (DependencySetTagged.returnList
                     (dataSourceFor c.(dataSignals ModelElementType) B2)))
               (makeSetFromList
                  (DependencySetTagged.returnList
                     (controlSourceFor c.(controlSignals ModelElementType) B2)))))).
     unfold DependencyLattice.upper in |- *;  intuition.
     
     generalize
      (DependencyLattice.join_upper tt
         (mergeDependencies E2 c
            (dataSourceFor c.(dataSignals ModelElementType) B2))
         (DependencyLattice.join tt
            (makeSetFromList
               (DependencySetTagged.returnList
                  (dataSourceFor c.(dataSignals ModelElementType) B2)))
            (makeSetFromList
               (DependencySetTagged.callList
                  (controlSourceFor c.(controlSignals ModelElementType) B2))))).
     unfold DependencyLattice.upper in |- *;  intuition.
     
     generalize
      (DependencyLattice.join_upper tt
         (makeSetFromList
            (DependencySetTagged.returnList
               (dataSourceFor c.(dataSignals ModelElementType) B2)))
         (makeSetFromList
            (DependencySetTagged.callList
               (controlSourceFor c.(controlSignals ModelElementType) B2)))).
     unfold DependencyLattice.upper in |- *;  intuition.
     
      eapply DependencyLattice.PreOrder.trans.
    
      apply H14.
   
     
     generalize
      (DependencyLattice.join tt
         (mergeDependencies E2 c
            (dataSourceFor c.(dataSignals ModelElementType) B2))
         (DependencyLattice.join tt
            (makeSetFromList
               (DependencySetTagged.returnList
                  (dataSourceFor c.(dataSignals ModelElementType) B2)))
            (makeSetFromList
               (DependencySetTagged.callList
                  (controlSourceFor c.(controlSignals ModelElementType) B2))))).
     intros.
     elim
      (DependencyLattice.join_upper tt t
         (mergeDependencies E1 c
            (controlSourceFor c.(controlSignals ModelElementType) B2))).
     intros.
      eapply DependencyLattice.PreOrder.trans.
    
      apply H6.
   
     
     generalize
      (mergeDependencies E1 c
         (controlSourceFor c.(controlSignals ModelElementType) B2)).
     intro.
     generalize (leb_correct_conv B2 (size c)).
     generalize (leb_correct (size c) B2).
     case (leb (size c) B2).
    intros; simpl in |- *;  intuition.
       discriminate.
   intros.
     generalize
      (DependencyLattice.join_upper tt (DependencyLattice.join tt t t0)
         (DependencyLattice.singleton (TaggedType.Return B2))).
     intros.
     elim
      (DependencyLattice.join_upper tt (DependencyLattice.join tt t0 t)
         (DependencyLattice.singleton (TaggedType.Return B2))).
     intros.
     intros.
     generalize (DependencyLattice.join_commutative tt t0 t).
     intros.
     elim H24.
     intros.
     intros.
      eapply DependencyLattice.PreOrder.trans.
    
      apply H26.
   
     
     generalize
      (DependencyLattice.join_upper tt (DependencyLattice.join tt t0 t)
         (DependencyLattice.singleton (TaggedType.Return B2))).
     unfold DependencyLattice.upper in |- *;  intuition.
      eapply DependencyLattice.PreOrder.trans.
    
      apply H28.
   
     
     elim
      (DependencyLattice.join_commutative tt (DependencyLattice.join tt t0 t)
         (DependencyLattice.singleton (TaggedType.Return B2))).
     intros.
      intuition.     
    intros.
     generalize
     (DependencyLattice.join_upper tt
        (mergeDependencies E1 c
           (controlTargetFor c.(controlSignals ModelElementType) B2))
        (mergeDependencies E2 c
           (dataSourceFor c.(dataSignals ModelElementType) B2))).
    unfold DependencyLattice.upper in |- *;  intuition.
     eapply DependencyLattice.PreOrder.trans.
   
     apply mergeDependencies_le.
     
     apply H0.
  
    
    
    generalize
     (DependencyLattice.join_upper tt
        (DependencyLattice.singleton (TaggedType.Return B2))
        (DependencyLattice.join tt
           (mergeDependencies E1 c
              (controlSourceFor c.(controlSignals ModelElementType) B2))
           (DependencyLattice.join tt
              (mergeDependencies E2 c
                 (dataSourceFor c.(dataSignals ModelElementType) B2))
              (DependencyLattice.join tt
                 (makeSetFromList
                    (DependencySetTagged.returnList
                       (dataSourceFor c.(dataSignals ModelElementType) B2)))
                 (makeSetFromList
                    (DependencySetTagged.callList
                       (controlSourceFor c.(controlSignals ModelElementType)
                          B2))))))).
    unfold DependencyLattice.upper in |- *;  intuition.
    
    generalize
     (DependencyLattice.join_upper tt
        (mergeDependencies E1 c
           (controlSourceFor c.(controlSignals ModelElementType) B2))
        (DependencyLattice.join tt
           (mergeDependencies E2 c
              (dataSourceFor c.(dataSignals ModelElementType) B2))
           (DependencyLattice.join tt
              (makeSetFromList
                 (DependencySetTagged.returnList
                    (dataSourceFor c.(dataSignals ModelElementType) B2)))
              (makeSetFromList
                 (DependencySetTagged.callList
                    (controlSourceFor c.(controlSignals ModelElementType) B2)))))).
    unfold DependencyLattice.upper in |- *;  intuition.
    
    generalize
     (DependencyLattice.join_upper tt
        (mergeDependencies E2 c
           (dataSourceFor c.(dataSignals ModelElementType) B2))
        (DependencyLattice.join tt
           (makeSetFromList
              (DependencySetTagged.returnList
                 (dataSourceFor c.(dataSignals ModelElementType) B2)))
           (makeSetFromList
              (DependencySetTagged.callList
                 (controlSourceFor c.(controlSignals ModelElementType) B2))))).
    unfold DependencyLattice.upper in |- *;  intuition.
    
    generalize
     (DependencyLattice.join_upper tt
        (makeSetFromList
           (DependencySetTagged.returnList
              (dataSourceFor c.(dataSignals ModelElementType) B2)))
        (makeSetFromList
           (DependencySetTagged.callList
              (controlSourceFor c.(controlSignals ModelElementType) B2)))).
    unfold DependencyLattice.upper in |- *;  intuition.
    
     eapply DependencyLattice.PreOrder.trans.
   
     apply H14.
  
    
    generalize
     (DependencyLattice.join tt
        (mergeDependencies E2 c
           (dataSourceFor c.(dataSignals ModelElementType) B2))
        (DependencyLattice.join tt
           (makeSetFromList
              (DependencySetTagged.returnList
                 (dataSourceFor c.(dataSignals ModelElementType) B2)))
           (makeSetFromList
              (DependencySetTagged.callList
                 (controlSourceFor c.(controlSignals ModelElementType) B2))))).
    intros.
    elim
     (DependencyLattice.join_upper tt t
        (mergeDependencies E1 c
           (controlSourceFor c.(controlSignals ModelElementType) B2))).
    intros.
     eapply DependencyLattice.PreOrder.trans.
   
     apply H6.
  
    
    generalize
     (mergeDependencies E1 c
        (controlSourceFor c.(controlSignals ModelElementType) B2)).
    intro.
    generalize (leb_correct_conv B2 (size c)).
    generalize (leb_correct (size c) B2).
    case (leb (size c) B2).
   intros; simpl in |- *;  intuition.
      discriminate.
  intros.
    generalize
     (DependencyLattice.join_upper tt (DependencyLattice.join tt t t0)
        DependencyLattice.bot).
    intros.
    generalize (DependencyLattice.join_commutative tt t0 t).
    intros.
    elim H22.
    intros.
    elim
     (DependencyLattice.join_upper tt (DependencyLattice.join tt t0 t)
        DependencyLattice.bot).
    intros.
     eapply DependencyLattice.PreOrder.trans.
   
     apply H24.
  
    
    generalize
     (DependencyLattice.join_upper tt (DependencyLattice.join tt t0 t)
        DependencyLattice.bot).
    unfold DependencyLattice.upper in |- *;  intuition.
     eapply DependencyLattice.PreOrder.trans.
   
     apply H28.
  
    
    elim
     (DependencyLattice.join_commutative tt (DependencyLattice.join tt t0 t)
        DependencyLattice.bot).
    intros.
     intuition.
   intros.
    absurd (@ None BlockKindType = None).
   intuition.
 reflexivity.
  intros.
   absurd (B2 < B2).
 auto.
apply (lt_trans B2 (size c) B2);  intuition.
Qed.

Lemma correctnessDataSignal : 
 forall c B1 B2 E1 E2, 
   isSequential B2 c = false 
    -> isConnected B1 B2 c
    -> (B1 < (size c))
    -> (B2 < (size c))
   -> forwardInputDependencies c E1 E2 = E1
   -> forwardOutputDependencies c E1 E2 = E2 
   -> DependencyLattice.PreOrder.le tt (E2 B1) (E2 B2).
Proof.
intros.
generalize (OutInBlock c B2 E1 E2).
generalize (OutInConnexion c B1 B2 E1 E2).
intros.
generalize (DependencyLattice.PreOrder.trans tt (E2 B1) (E1 B2) (E2 B2)).
intros.
 intuition.
Qed.


(*Lemma CorrectnessControlSignal :
  forall c B1 B2 E1 E2, 
    isCalled B1 B2 c
     -> (B1 < (size c))
     -> (B2 < (size c))
     -> forwardInputDependencies c E1 E2 = E1
     -> forwardOutputDependencies c E1 E2 = E2 
     -> DependencyLattice.PreOrder.le tt (E2 B2) (E2 B1).
Proof.
intros.

Qed.*)


