(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/coq/oldies/EnvironmentLattice.v,v $
 *  @version	$Revision: 1.1 $
 *  @date	$Date: 2009-05-20 20:55:58 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

Require Import Lt.
Require Import Relation_Definitions.
Require Import Wf_nat.
Require Import Wellfounded.
Require Import Domain.
Require Import Peano_dec.

(* TODO: MaPa, 2008/08/17, make it replace the current EnvironmentLattice *)
(*
Module FiniteMapLattice (Element : LatticeType) <: LatticeType.

End FiniteMapLattice.
*)

(* Builds an Environnement assigning values of a lattice to nat indexes *)
(* given the lattice of the values *)
Module MakeEnvironment (Lattice : LatticeType) <: LatticeType.

  Module PreOrder <: PreOrderType.

    Module Data <: DataType.

      (* The first part is the type of the elements in the environment *)
      (* The second part is the size of the environment *)
      Definition type := (Lattice.PreOrder.Data.type * nat)%type.

      (* The equal operation exists, is complete and decidable *)
      (* It corresponds to equal on the Lattice elements *)
      (* and on the the lattice size *)
      Theorem eq : forall x y : type, { (x = y) } + { ~ (x = y) }.
        Proof.
          destruct x as (eltX,sizeX);
            destruct y as (eltY,sizeY).
          elim (Lattice.PreOrder.Data.eq eltX eltY).
          intro;
            subst.
          elim (eq_nat_dec sizeX sizeY).
          intro;
            subst.
          left;
            auto.
          intro;
            right;
            intro;
            injection H.
            apply b.
          intro;
            right;
            intro;
            injection H.
          auto.
        Defined.

    End Data.

    (* An environment associate a value to an index *)
    (* This is infinite environment encoded with a function *)
    (* The size of a given one is in the type of the Data *)
    (* TODO: MaPa, 2008/08/16, really need to clarify the role *)
    (* of the Data (its type) and of the type *)
    (* TODO: MaPa, 2008/08/15, implement environment with a FiniteMap *)
    (* required to improve the runtime cost *)
    (* TODO: MaPa, 2008/08/16, Why PreOrder.type and not PreOrder.Data.type ? *)
    Definition type := nat -> Lattice.PreOrder.type.

    (** The lesser or equal operation on an environment: *)
    (*   all the elements must be lesser or equals *)
    (*   it relies on the size of the environment *)
    Definition le (d : Data.type) (e1 e2 : type) :=
      forall n, n < (snd d) -> Lattice.PreOrder.le (fst d) (e1 n) (e2 n).

    (** The equal operation on an environment: *)
    (*   all the elements must be equals *)
    (*   it relies on the size of the environment *)
    Definition eq : Data.type -> type -> type -> Prop := fun d x y => (le d x y) /\ (le d y x).

    Definition eq_opt (d : Data.type) (e1 e2 : type) :=
      forall n, n < (snd d) -> Lattice.PreOrder.eq (fst d) (e1 n) (e2 n). 

(*    Fixpoint eq_opt_rec (s : nat) (e1 e2 : type) :=
      match s with *)
      
  Lemma eq_opt_is_eq : forall d x y, (eq_opt d x y) -> (eq d x y).
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold Lattice.PreOrder.eq in |- *.
      firstorder.
    Qed.

  Lemma eq_is_eq_opt : forall d x y, eq d x y -> eq_opt d x y.
    Proof.
      unfold eq_opt in |- *;
        unfold eq in |- *;
        unfold le in |- *;
        unfold Lattice.PreOrder.eq in |- *.
      firstorder.
    Qed.

    (** Proof that equal is decidable *)
    (* relying on the fact that equal on the lattice preorder is decidable *)
    Lemma eq_opt_dec : forall d x y, { (eq_opt d x y) } + { ~ (eq_opt d x y) }.
      Proof.
        unfold eq_opt in |- *;
          intros d x y; 
          elim d.
        intros a b; 
          elim b; 
          simpl in |- *;  
          intuition.
        left; 
          intros n Hn;  
          absurd (n < 0); 
          auto with arith.
        destruct (Lattice.PreOrder.eq_dec a (x n) (y n)).
        left;
          intros n0 Hn0;
          inversion Hn0;
          intuition.
        right;
          intuition.		
      Qed.

    Lemma eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.
      Proof.
        intros.
        elim (eq_opt_dec d x y);
          intros Heqo.
        left;
          apply eq_opt_is_eq;
          exact Heqo.
        right;
          intro Heq;
          apply Heqo;
          apply eq_is_eq_opt;
          exact Heq.    
      Qed.

    (** Proof that lesser or equal is reflexive *)
    (* relying on the fact that lesser or equal on the lattice preorder *)
    (* is reflexive *)
    Theorem refl : forall d e, le d e e.
      Proof.
        intros d e n H.
        apply Lattice.PreOrder.refl.
      Qed.

    (** Proof that lesser or equal is transitive *)
    (* relying on the fact that lesser or equal on the lattice preorder *)
    (* is transitive *)
    Theorem trans : forall d e1 e2 e3, le d e1 e2 -> le d e2 e3 -> le d e1 e3.
      Proof.
        intros d e1 e2 e3 H1 H2 n Hn.
        apply (Lattice.PreOrder.trans (fst d) (e1 n) (e2 n) (e3 n)).
        apply H1; 
          assumption.
        apply H2;
          assumption.
      Qed.

    (** Proof that lesser or equal is decidable *)
    (* relying on the fact that lesser or equal on the lattice preorder *)
    (* is decidable *)
    Theorem dec :  forall d e1 e2, { (le d e1 e2) } + { ~ (le d e1 e2) }.
      Proof.
        unfold le in |- *; 
          intros d e1 e2; 
          elim d; 
          clear d; 
          intros d s;
          elim s;
          clear s;
          simpl in |- *;
          intuition.
        left; 
          intros n Hn;
          absurd (n < 0);
          auto with arith.
        destruct (Lattice.PreOrder.dec d (e1 n) (e2 n)).
        left;
          intros n0 Hn0;
          inversion Hn0;
          intuition.
        right;
          intuition.
      Defined.

  End PreOrder.

  (* bot is the function which associate the least element of the lattice *)
  (* to any indexes *)
  Definition bot : PreOrder.type := fun _ => Lattice.bot.

  (** Proof that bot is the least element according to lesser or equal *)
  Theorem bot_least : forall d x, PreOrder.le d bot x.
    Proof.
      intros d x n Hn.
      apply Lattice.bot_least.
    Qed.

  (* bot is the function which associate the greatest element of the lattice *)
  (* to any indexes *)
  Definition top : PreOrder.type := fun _ => Lattice.top.

  (** Proof that top is the greatest element according to lesser or equal *)
  Theorem top_greatest : forall d x, PreOrder.le d x top.
    Proof.
      intros d x n Hn.
      apply Lattice.top_greatest.
    Qed.

  (** The lesser than operation is defined using the lesser or equal *)
  (* operation when x is lesser or equal than y and y is not lesser or *)
  (* equal than x *)
  Definition lt d e1 e2 := (PreOrder.le d e1 e2) /\ ~ (PreOrder.le d e2 e1).

  (** The greater than operation is defined using the lesser than operation *)
  Definition gt d e1 e2 := lt d e2 e1.

  (** The equal operation is defined using the lesser or equal operation *)
  Definition eq d e1 e2 := (PreOrder.le d e1 e2) /\ (PreOrder.le d e2 e1).

  (** x is upper than a and than b *) 
  Definition upper d a b x := PreOrder.le d a x /\ PreOrder.le d b x.

  (** For each index, the join of the functions is the join of both *)
  (* images of the indexed elements *)
  Definition join (d : PreOrder.Data.type) (e1 e2 : PreOrder.type) := 
    fun n => Lattice.join (fst d) (e1 n) (e2 n).

  (** Proof that the join of x and y is greater than x and y *)
  Theorem join_upper : forall d x y, upper d x y (join d x y).
    Proof.
      intros d x y.
      split; 
        intros n Hn; 
        elim (Lattice.join_upper (fst d) (x n) (y n));
        auto.
    Qed.

  (** Proof that the join of x and y is the least element greater than x and y *)
  (* also called the least upper bound or LUB *)
  Theorem join_supremum : forall d x y m, upper d x y m -> PreOrder.le d (join d x y) m.
    Proof.
      intros d x y m.
      induction 1.
      intros n Hn.
      unfold join in |- *.
      apply Lattice.join_supremum;
        intuition.
      unfold Lattice.upper in |- *;
        auto.
    Qed.

  (** x is lower than a and than b *) 
  Definition lower d a b x := PreOrder.le d x a /\ PreOrder.le d x b.

  (** For each index, the meet of the functions is the meet of both *)
  (* images of the indexed elements *)
  Definition meet (d : PreOrder.Data.type) (e1 e2 : PreOrder.type) := 
    fun n => Lattice.meet (fst d) (e1 n) (e2 n).

  (** Proof that the meet of x and y is lower than x and y *)
  Theorem meet_lower : forall d x y, lower d x y (meet d x y).
    Proof.
      intros d x y.
      split; 
        intros n Hn; 
        elim (Lattice.meet_lower (fst d) (x n) (y n));
        auto.
    Qed.

  (** Proof that the meet of x and y is the greatest element lower than x and y *)
  (* also called the greatest lower bound or GLB *)
  Theorem meet_infimum : forall d x y m, lower d x y m -> PreOrder.le d m (meet d x y).
    Proof.
      intros d x y m.
      induction 1.
      intros n Hn.
      unfold meet in |- *.
      apply Lattice.meet_infimum;
        intuition.
      unfold Lattice.lower in |- *;
        auto.
    Qed.

  (* TODO: MaPa, 2008/08/15, explain this proof principle *)
  (* it seems to be recursion on the size of the environment *)
  Inductive gt_lex (e1 e2 : PreOrder.type) : PreOrder.Data.type -> Prop :=
    (* if the last element of the environment e1 is greater than the last *)
    (* element of the environment e2, then this is also true for the next *)
    (* last element *)  
    | gt_lex_s : 
      forall d, Lattice.gt (fst d) (e1 (snd d)) (e2 (snd d)) 
                -> gt_lex e1 e2 (fst d, (S (snd d)))
    | gt_lex_pred : 
      forall d, (Lattice.eq (fst d) (e1 (snd d)) (e2 (snd d)) 
                 /\ (gt_lex e1 e2 d))
                 -> (gt_lex e1 e2 (fst d, (S (snd d)))).

  Theorem gt_incl_gt_lex : 
    forall d, inclusion PreOrder.type (gt d) (fun e1 e2 => gt_lex e1 e2 d).
    Proof.
      unfold inclusion in |- *;
        unfold gt in |- *;
        intro d;
        elim d;
        clear d;
      intros d s;
        induction s;
        intuition.
      unfold lt in H;
        intuition.
      elim H1;
        unfold PreOrder.le in |- *;
        intros n Hn;
        inversion Hn.
      unfold lt in H;
        intuition.
      destruct (Lattice.PreOrder.dec d (x s) (y s)).
      apply (gt_lex_pred x y (d, s)).
      unfold Lattice.eq in |- *;
        intuition.
      apply IHs.
      unfold lt in |- *;
        intuition.
      intros n Hn.
      simpl in |- *.
      apply H0.
      simpl in Hn;
        simpl in |- *;
        auto with arith.
      apply H1.
      unfold PreOrder.le in |- *;
        simpl in |- *;
        intros n Hn.
      inversion_clear Hn;
        intuition;
        auto with arith.
      apply (gt_lex_s x y (d, s)).
      unfold Lattice.gt in |- *;
        unfold Lattice.lt in |- *;
        simpl in |- *;
        intuition.
    Qed.

  (** The greater than operator is defined on the last element: *)
  (* the one associated to the size of the lattice *)
  Definition gt_s (d : PreOrder.Data.type) e1 e2 := 
    Lattice.gt (fst d) (e1 (snd d)) (e2 (snd d)).

  (** The equal operator is defined on the last element: *)
  (* the one associated to the size of the lattice *)
  Definition eq_s (d : PreOrder.Data.type) e1 e2 := 
    Lattice.eq (fst d) (e1 (snd d)) (e2 (snd d)).

  (** Proof that the greater than operator is well founded *)
  Theorem gt_lex_s_well_founded : 
    forall (d : PreOrder.Data.type) e, Acc (gt_s d) e.
    Proof.
      unfold gt_s.
      intro d;
        elim d;
        clear d;
        intros d s e.
      simpl.
      apply Acc_intro.
      elim (Lattice.gt_well_founded d (e s)).
      intros.
      apply Acc_intro.
      intros.
      eapply H0.
      apply H1.
      exact H2.
    Qed.

  Theorem gt_lex_pred_well_founded : 
    forall d e, Acc (fun e1 e2 => gt_lex e1 e2 d) e 
                -> Acc (fun e1 e2 => eq_s d e1 e2 /\ (gt_lex e1 e2 d)) e.
    Proof.
      destruct d.
      apply Acc_incl.
      unfold inclusion in |- *;
        intuition.
    Qed.

  Theorem gt_Sn_gt_n_well_founded : 
    forall d, well_founded (fun e1 e2 => gt_lex e1 e2 d) 
              -> well_founded (fun e1 e2 => gt_lex e1 e2 (fst d, (S (snd d)))).
    Proof.
      intro d; 
        elim d; 
        clear d; 
        intros d s; 
        simpl in |- *.
      intros H2 e.
      generalize (H2 e).
      elim (gt_lex_s_well_founded (d, s) e).
      unfold gt_s in |- *; 
        simpl in |- *.
      intros.
      generalize H0; 
        clear H H0.
      elim H1; 
        clear H1.
      clear e x.
      intros.
      apply Acc_intro; 
        intros.
      inversion H3.
      rewrite H4 in H5; 
        rewrite H4 in |- *.
      rewrite H6 in H5; 
        rewrite H6 in |- *.
      intuition.
      rewrite H4 in H5; 
        rewrite H4 in |- *.
      rewrite H6 in H5; 
        rewrite H6 in |- *.
      intuition.
      generalize H8; 
        clear H8.
      cut (d0 = (d, s)).
      intro H8; 
        rewrite H8 in |- *; 
        clear H8.
      intro H8.
      apply H0;  
        intuition.
      apply H1.
      unfold Lattice.eq in H7;  
        intuition.
      unfold Lattice.gt in |- *; 
        unfold Lattice.gt in H5.
      unfold Lattice.lt in H5; 
        unfold Lattice.lt in |- *;  
        intuition.
      eapply Lattice.PreOrder.trans.
      apply H11.
      exact H7.
      apply H12.
      eapply Lattice.PreOrder.trans.
      apply H5.
      exact H11.
      assumption.
      generalize H4; 
        generalize H6; 
        elim d0.
      simpl in |- *;
        intros;
        subst;
        auto.
    Qed.

  Theorem gt_well_founded : forall d, well_founded (gt d).
    Proof.
      intros d.
      cut (well_founded (fun e1 e2 => gt_lex e1 e2 d)).
      intro H.
      intro e.
      eapply Acc_incl.
      apply gt_incl_gt_lex.
      apply H.
      elim d; clear d; intros d s.
      elim s; clear s;  intuition.
      intro e; apply Acc_intro;  intuition.
      inversion H.
      apply (gt_Sn_gt_n_well_founded (d, n)).
      assumption.
    Qed.

End MakeEnvironment.

