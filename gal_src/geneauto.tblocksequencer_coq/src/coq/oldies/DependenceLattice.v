(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/coq/oldies/DependenceLattice.v,v $
 *  @version	$Revision: 1.1 $
 *  @date	$Date: 2009-05-20 20:55:58 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)


(* TODO : MaPa, 2008/08/15, add some coherence between the names *)
(* greater/upper, lesser/lower *)

Require Import OrderedType.
Require Import FSets.
Require Import Peano_dec.
Require Import Compare_dec.
Require Import Compare.
Require Import Decidable.
Require Import Relation_Definitions.
Require Import Wf_nat.
Require Import Wellfounded.
Require Import Max.
Require Import Arith.
Require Import Constructive_sets.
Require Import FSetList.
Require Import Domain.

(* TODO: MaPa, 2008/08/17, make it to replace the current DependencyLattice *)
(*
Module FiniteSetLattice (Element : LatticeType) <: LatticeType.

End FiniteSetLattice.
*)

(** A finite set of values. *)
(* TODO: MaPa, 2008/08/17, why do we introduce a type ? *)
Module Type FiniteSetType.

  (** The is implemented using a finite set from standart library *)
  Declare Module FiniteSet : FSetInterface.S 
    (** where the elements are block indexes, i.e. natural numbers *)
    (* No purpose at all as it is done in Sequencer.v *)
    with Module E := OrderedNatural.

  (* top is the set of all the possible values *)
  (* it is therefore specific for a given model *)
  Variable carrier : FiniteSet.t.

End FiniteSetType.

(** Functor implementing the lattice over Block finite set *)
(* it will build a lattice specific to each model *)
(* TODO : find a better solution than modules in order to be able *)
(* to write a function taking the model as parameter and then building *)
(* the lattice *)
Module FinitePowerSetLattice (S : FiniteSetType)  <: LatticeType.

  (* The preorder used for defining the lattice *)
  Module PreOrder <: PreOrderType.

    (* The data manipulated by the preorder *)
    (* In fact, we might use data for storing the carrier *)
    (* and avoid the dependent product *)
    Module Data := Unit.

    (* The type of the data manipulated by the preorder, i.e. the set *)
    (* of possible values: subsets of top, the lattice is the *)
    (* powerset of top *)
    (* TODO: MaPa, 2008/08/15, what is the exact relation between *)
    (* Data and type *)

    Definition predicate (value : S.FiniteSet.t) := S.FiniteSet.Subset value S.carrier. 

    Definition type := 
      { value : S.FiniteSet.t | S.FiniteSet.Subset value S.carrier }.

    (** Lesser or equal relation in the preorder *)
    Definition le : Data.type -> type -> type -> Prop := 
      fun d x y =>
        match x, y with
          | exist sx _, exist sy _ => S.FiniteSet.Subset sx sy
        end.

    (** Equal relation in the preorder *)
    Definition eq : Data.type -> type -> type -> Prop := fun d x y => le d x y /\ le d y x.

    Definition eq_opt : Data.type -> type -> type -> Prop := 
      fun d x y =>
        match x, y with
          | exist sx _, exist sy _ => S.FiniteSet.Equal sx sy
        end.

    Lemma eq_opt_is_eq : forall d x y, eq_opt d x y -> eq d x y.
      Proof.
        unfold eq_opt in |- *;
          unfold eq in |- *;
          unfold le in |- *.
        destruct x;
          destruct y;
          simpl in |- *.
        unfold S.FiniteSet.Equal in |- *;
          unfold S.FiniteSet.Subset in |- *.
        firstorder.
      Qed.

    Lemma eq_is_eq_opt : forall d x y, eq d x y -> eq_opt d x y.
      Proof.
        unfold eq_opt in |- *;
          unfold eq in |- *;
          unfold le in |- *.
        destruct x;
          destruct y;
          simpl in |- *.
        unfold S.FiniteSet.Equal in |- *;
          unfold S.FiniteSet.Subset in |- *.
        firstorder.
      Qed.

    (** The equal relation is decidable *)
    Lemma eq_opt_dec : forall d x y, {eq_opt d x y}+{~eq_opt d x y}.
      Proof.
        induction x; 
          induction y; 
          unfold eq_opt in |- *.
        generalize (S.FiniteSet.equal_1 (s:=x) (s':=x0)).
        generalize (S.FiniteSet.equal_2 (s:=x) (s':=x0)).
        case (S.FiniteSet.equal x x0).
        left;  
          intuition.
        right;  
          intuition.
      Qed.

    Lemma eq_dec : forall d x y, { eq d x y } + { ~ (eq d x y) }.
      Proof.
        intros.
        elim (eq_opt_dec d x y);
          intros Heqo.
        left;
          apply eq_opt_is_eq;
          exact Heqo.
        right;
          intro Heq;
          apply Heqo;
          apply eq_is_eq_opt;
          exact Heq.    
      Qed.

    (** The lesser or equal relation is reflexive *)
    Lemma refl : forall d x, le d x x.
      Proof.
        induction x; 
          unfold le in |- *.
        unfold S.FiniteSet.Subset in |- *; 
          auto.
      Qed.

    (** The lesser or equal relation is transitive *)
    Lemma trans : forall d x y z, le d x y -> le d y z -> le d x z.
      Proof.
        destruct x; 
          destruct y; 
          destruct z; 
          unfold le in |- *.
        unfold S.FiniteSet.Subset in |- *.
        firstorder.
      Qed.

    (** The lesser or equal relation is decidable *)
    Lemma dec : forall d x y, {le d x y} + {~le d x y}.
      Proof.
        induction x; 
          induction y; 
          unfold le in |- *.
        generalize (S.FiniteSet.subset_1 (s:=x) (s':=x0)).
        generalize (S.FiniteSet.subset_2 (s:=x) (s':=x0)).
        case (S.FiniteSet.subset x x0).
        left;  
          intuition.
        right;  
          intuition.
      Qed.

   End PreOrder.

  (** The least element in the lattice *)
  (* TODO: MaPa, 2008/08/17, why not assign it the empty set *)
  (* we need to prove that empty is a subset of carrier *)
  (* which is trivial *)
  Definition bot : PreOrder.type (* := S.FiniteSet.empty *).
    Proof.
      exists S.FiniteSet.empty. (* exactly that, no ? *)
      unfold S.FiniteSet.Subset in |- *.
      intros.
      absurd (S.FiniteSet.In a S.FiniteSet.empty).
      firstorder.
      exact H.
    Defined.

  (** Proof that bot is the least element according to lesser or equal *)
  Lemma bot_least : forall d x, PreOrder.le d bot x.
    Proof.
      destruct x.
      unfold bot in |- *.
      simpl in |- *.
      unfold S.FiniteSet.Subset in |- *.
      intros.
      absurd (S.FiniteSet.In a S.FiniteSet.empty).
      apply S.FiniteSet.empty_1.
      auto.
    Defined.

  (** The greatest element in the lattice *)
  (* TODO: MaPa, 2008/08/17, why not assign it the carrier set *)
  (* we need to prove that carrier is a subset of carrier *)
  (* which is trivial *)
  Definition top : PreOrder.type (* := S.carrier *).
    Proof.
      exists S.carrier. (* exactly that, no ? *)
      unfold S.FiniteSet.Subset in |- *.
      intros.
      exact H.
    Defined.

  (** Proof that top is the greatest element according to lesser or equal *)
  Lemma top_greatest : forall d x, PreOrder.le d x top.
    Proof.
      unfold PreOrder.le in |- *; 
        destruct x; 
        unfold top in |- *.
      exact s.
    Qed.

  (** x is upper than a and than b *) 
  Definition upper d a b x := PreOrder.le d a x /\ PreOrder.le d b x.

  (** The join operation in the lattice *)
  Definition join : PreOrder.Data.type -> PreOrder.type -> PreOrder.type -> PreOrder.type.
    Proof.
      intros.
      destruct H0 as (x0);
        destruct H1 as (x1).
      exists (S.FiniteSet.union x0 x1).
      unfold S.FiniteSet.Subset in |- *.
      intros.
      elim (S.FiniteSet.union_1 H0).
      auto.
      auto.
    Defined.

  (** Proof that the join operator is commutative *)
  Lemma join_commutative : forall d x y, PreOrder.eq d (join d x y) (join d y x).
    Proof.
      intros.
      apply PreOrder.eq_opt_is_eq.
      unfold join, PreOrder.eq, S.FiniteSet.Equal in |- *.
      destruct x;
        destruct y.
      intros.
      split; 
        intros.
      destruct (S.FiniteSet.union_1 H).
      apply S.FiniteSet.union_3.
      exact H0.
      apply S.FiniteSet.union_2.
      exact H0.
      destruct (S.FiniteSet.union_1 H).
      apply S.FiniteSet.union_3.
      exact H0.
      apply S.FiniteSet.union_2.
      exact H0.
    Qed.

  (** Proof that the join operator is monotonic *)
  Lemma join_monotonic : forall d x1 x2 y1 y2, PreOrder.le d x1 x2 -> PreOrder.le d y1 y2 -> PreOrder.le d (join d x1 y1) (join d x2 y2).
    Proof.
      unfold join, PreOrder.le, S.FiniteSet.Subset in |- *.
      intros.
      destruct x1; 
        destruct x2; 
        destruct y1; 
        destruct y2.
      intros.
      destruct (S.FiniteSet.union_1 H1).
      intuition.
      firstorder.
    Qed.

  (** Proof that the join of x and y is greater than x and y *)
  Lemma join_upper : forall d x y, upper d x y (join d x y).
    Proof.
      intros.
      unfold upper in |- *.
      unfold PreOrder.le in |- *.
      destruct x; 
        destruct y.
      simpl in |- *.
      split.
      unfold S.FiniteSet.Subset in |- *.
      intros.
      eapply S.FiniteSet.union_2.
      auto.
      unfold S.FiniteSet.Subset in |- *.
      intros.
      eapply S.FiniteSet.union_3.
      auto.
    Defined.

  (** Proof that the join of x and y is the least element greater than x and y *)
  (* also called the least upper bound or LUB *)
  Lemma join_supremum : forall d x y m, upper d x y m -> PreOrder.le d (join d x y) m.
    Proof.
      unfold upper, PreOrder.le, join in |- *; 
        destruct x; 
        destruct y; 
        destruct m.
      intros.
      unfold S.FiniteSet.Subset in |- *.
      intros.
      elim H.
      intros.
      elim (S.FiniteSet.union_1 H0).
      firstorder.
      firstorder.
    Defined.

  (** x is lower than a and than b *) 
  Definition lower d a b x := PreOrder.le d x a /\ PreOrder.le d x b.

  (** The meet operation in the lattice *)
  Definition meet : PreOrder.Data.type -> PreOrder.type -> PreOrder.type -> PreOrder.type.
    Proof.
      intros.
      destruct H0 as (x0);
        destruct H1 as (x1).
      exists (S.FiniteSet.inter x0 x1).
      unfold S.FiniteSet.Subset in |- *.
      intros.
      apply s.
      eapply S.FiniteSet.inter_1.
      apply H0.
    Defined.

  (** Proof that the meet of x and y is lower than x and y *)
  Lemma meet_lower : forall d x y, lower d x y (meet d x y).
    Proof.
      unfold lower, meet in |- *; 
        destruct x; 
        destruct y.
      simpl in |- *.
      split.
      unfold S.FiniteSet.Subset in |- *.
      intros.
      eapply S.FiniteSet.inter_1.
      eapply H.
      unfold S.FiniteSet.Subset in |- *.
      intros.
      eapply S.FiniteSet.inter_2.
      eapply H.
    Qed.

  (** Proof that the meet of x and y is the greatest element lower than x and y *)
  (* also called the greatest lower bound or GLB *)
  Lemma meet_infimum : forall d x y m, lower d x y m -> PreOrder.le d m (meet d x y).
    Proof.
      unfold lower, PreOrder.le, meet in |- *; 
        destruct x; 
        destruct y; 
        destruct m.
      intro.
      destruct H.
      unfold S.FiniteSet.Subset in |- *.
      intros.
      firstorder.
    Qed.

  (** The lesser than operation on the lattice *)
  Definition lt d x y := PreOrder.le d x y /\ ~PreOrder.le d y x.

  (** The greater than operation on the lattice *)
  Definition gt d x y := lt d y x.

  (** The equal operation on the lattice *)
  (* TODO : MaPa, 2008/08/15, why is it not PreOrder.eq ? *)
  Definition eq d x y := PreOrder.le d x y /\ PreOrder.le d y x.

  (** Proof that greater than is well founded *)
  (* TODO : MaPa, 2008/08/15, provide the proof *)
  Axiom gt_well_founded : forall d, well_founded (gt d).

  (*definition of a singleton*)
  (*Definition singleton := {s : S.FiniteSet.singleton | S.FiniteSet.Subset s top}.*)

  (* TODO : this axiom is false as S.carrier is not equal to nat *)
  (* the lattice is built given a finite set *)
  (* singleton should not apply to any natural number but only to a *)
  (* valid index *)
  (* it is merely a typing problem *)
  Axiom singleton_top : forall n : nat (* S.FiniteSet.elt *) , S.FiniteSet.In n S.carrier.

  (* TODO : MaPa, 2008/08/11 This is definitively incorrect *)
  (* the index must be a valid index *)
  Definition singleton : nat (* S.FiniteSet.elt *) (* Should be S.carrier *) -> PreOrder.type.
    Proof.
      intro.
      exists (S.FiniteSet.singleton H).
      unfold S.FiniteSet.Subset in |- *.
      intros.
      eapply singleton_top.
    Defined.

End FinitePowerSetLattice.

