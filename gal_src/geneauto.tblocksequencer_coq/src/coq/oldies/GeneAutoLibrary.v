(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/coq/oldies/GeneAutoLibrary.v,v $
 *  @version	$Revision: 1.1 $
 *  @date	$Date: 2009-05-20 20:55:58 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

(* For the sequencing of block it is only required to consider two kind of *)
(* blocks : Sequentiel (directFeedThrough = false) *)
(*          and Combinatorial (directFeedThrough = true) *)
(* Each block is parameterized by the number of input and output ports *)
(* Each block has a trigger and enable port *)
(* Currently, the port are not distinguished. *)

Require Import Peano_dec.

Inductive BlockKindType : Set :=
  | Sequential : nat -> nat -> BlockKindType
  | Combinatorial : nat -> nat -> BlockKindType.

(** Proof that the equal operation is complete and decidable *)
Theorem dec_block_op : forall x y : BlockKindType, { (x = y) } + { ~ (x = y) }.
  Proof.
    destruct x; 
      destruct y.
    elim (eq_nat_dec n n1).
    intro;
      subst.
    elim (eq_nat_dec n0 n2).
    intro;
      subst;
      left;
      auto.
    intro;
      right;
      intro.
    apply b.
      injection H.
    auto.
    intro;
      right;
      intro;
      injection H.
    auto.
    right;
      discriminate.
    right;
      discriminate.
    elim (eq_nat_dec n n1).
    intro;
      subst.
    elim (eq_nat_dec n0 n2).
    intro;
      subst;
      left;
      auto.
    intro;
      right;
      intro;
      injection H;
      auto.
    intro;
      right;
      intro;
      injection H;
      auto.
  Defined.
