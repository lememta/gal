Definition predicate (n : nat) := (n > 0).

Definition positive := { n : nat | predicate n }.

Lemma successor_nat_positive : forall n : nat, predicate (S(n)).
Proof
  intro.
  unfold predicate.
  induction n.
  auto.
  auto.
Qed.

Definition next ( v : positive ) : positive :=
  match v with
    exist n p => exist predicate (S( n )) (successor_nat_positive( n ))
  end.

Definition y (x : nat) : positive :=
  exist (predicate) (S(x)) (successor_nat_positive x).


