(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/coq/oldies/Sequencer.v,v $
 *  @version	$Revision: 1.1 $
 *  @date	$Date: 2009-05-20 20:55:58 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

(* This file defines the block sequencer elementary tool *)

Require Import List.
Require Import Arith.
Require Import FSetInterface.
Require FSetList.
Require Import Heap.
Require Import Recdef.
Require Import Wf.
Require Import Compare_dec.

Require Import Util.
Require Import GeneAutoLibrary.
Require Import Domain.
Require Import DependenceLattice.
Require Import GeneAutoLanguage.
Require Import EnvironmentLattice. 

Open Scope nat_scope.
Open Scope list_scope.

Set Printing Projections.
(*
(** Specification for the block sequencer *)
Module Type SequencerType.

(* Provided services *)

  (** The sequence model transformation takes as parameter a model and *)
  (* produces a modified model *)
  Definition sequence_model( model : GeneAutoModelType ) : GeneAutoModelType.

  Definition compute_dependencies( model : GeneAutoModelType ) : DependencyType.

  Definition sort_blocks( model : GeneAutoModelType, dependencies : DependencyType ).

(* Required properties *)

  (** The result of the application of sequenceModel on a valid model *)
  (* is a valid model *)
  Axiom sequenced_model_valid :
    forall model : ModelType, 
      validModelPredicate( model ) 
      -> validModelPredicate( sequenceModel( model ) ).  

  (** The result of the application of sequenceModel on a valid model *)
  (* is a sequenced model *)
  Axiom sequenced_model_correctness : 
    forall model : ModelType, 
      validModelPredicate( model ) 
      -> sequencedModelPredicate( sequenceModel( model ) ).

  (** The result of the application of sequenceModel on a valid model *)
  (* is a sequenced model *)
  Axiom sequenceRespectDataSignal_correctness :
    forall model : ModelType,
      validSequencedModelPredicate( sequenceModel( model ) )
      -> (forall signal : nat,
        (In signal model.signalIndexes)
        -> (model.signalMap( signal ))

  Axiom sequenceModel_preservation :
    forall model : ModelType
    let result := sequenceModel( model ) in
      (* The block index sets are the same *) 
      ((Equal model.blockIndexes result.blockIndexes)
      /\
      (* The base part of the blocks are the same *)
      (forall block : nat,
        (In block model.blockIndexes) 
        -> (rawBlockEqual model.blockMap( block ) result.blockMap( block ))))
      /\
      (* The signal index sets are the same *) 
      ((Equal model.signalIndexes result.signalIndexes)
      /\
      (* The signals are the same *)
      (forall signal : nat,
        (signalEqual model.signalMap( signal ) result.signalMap( signal )))).

End SequencerType.
*)


(* Definition of the variables that will be initialized by the CaML *)
(* wrapper *)

(* TODO: MaPa, 2008/08/17, should be a parameter, not a kind of constant *)
Variable model : ModelType.

(* Set of natural numbers used for block indexes *)
(* TODO: MaPa, 2008/08/17, should be the real set of valid block indexes *)
Module NaturalFiniteSet := FSetList.Make( OrderedNatural ). 

(* The Dependency set *)
Module DependencySet : FiniteSetType.

  (* The set of natural numbers used as block indexes  *)
  Module FiniteSet := NaturalFiniteSet.

  (*  *)
  Fixpoint makeSetFromList (list : list nat) {struct list} : FiniteSet.t :=
    match list with 
      | nil    => FiniteSet.empty
      | head::queue   => (FiniteSet.add head (makeSetFromList queue))
    end.

  (** Speficication of the makeSetFromList operation *)
  (* Proof that the makeSetFromList is correct *)
  (* TODO: MaPa, 2008/08/17, proof to be done *)
  Axiom makeSetFromList_correctness: forall (e : nat) (l : list nat), List.In e l <-> FiniteSet.In e (makeSetFromList l). 

 (* TODO : remove the model reference but manipulating blocks instead of indexes *)
(* seems a hard problem as the Lattice is built from the blocks *)
(* module should not be used as they can only be statically instanciated *)
(* should try to do it with a functor building the Lattice from a Set *)

  Definition carrier := 
    makeSetFromList (blockIndexes (blocks ModelElementType model)).

End DependencySet.

(* The *)
Module DependencyLattice := FinitePowerSetLattice (DependencySet).

(* *)
Module DependencyEnvironment := MakeEnvironment(DependencyLattice).

(* *)
Module EnvironmentPair := LatticeCartesianProduct (DependencyEnvironment) (DependencyEnvironment).

(* *)
Definition dec_eq (A : Set) := forall x y : A, { (x = y) } + { ~ (x = y) }.

(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
(** Function outputDependencies *)
(* Compute the output dependency of a block taking into account. *)
(* Definition outputDependencies 
  (outputEnv : DependencyEnvironment.PreOrder.type) 
  (diagram : ModelType) 
  (blockIndex : DependencySet.FiniteSet.elt) :=
   match (lookForBlock blockIndex diagram) with 
     | Some blockKind => 
       match blockKind with 
         | (Sequential _ _) => (DependencyLattice.singleton blockIndex)
         | _ => (outputEnv blockIndex) 
       end
     | _ => DependencyLattice.bot
   end. *)

(* Proof that the outputDependencies operator is monotonic *)
(* Lemma outputDependencies_monotonic : forall diagram diag E E', 
  DependencyEnvironment.PreOrder.le diagram E E' 
  -> DependencyEnvironment.PreOrder.le 
       diagram 
       (outputDependencies E diag) 
       (outputDependencies E' diag).
Proof.
  unfold DependencyEnvironment.PreOrder.le, outputDependencies in |- *;
    simpl in |- *;
    intuition.
  case (lookForBlock n diag).
  intros.
  case b.
  intros.
  apply DependencyLattice.PreOrder.refl.
  intros;  intuition.
  apply DependencyLattice.PreOrder.refl.
Qed. *)

(* Currently not used *)
 (*TR-BS-C019*)
 (*TR-BS-C021*)
 (*TR-BS-C022*)
 (*TR-BS-C023*)
 (*TR-BS-C024*)
 (*TR-BS-C025*)
(* Definition inputDependencies 
  (inputEnv : DependencyEnvironment.PreOrder.type) 
  (diagram : ModelType) 
  (blockIndex : nat) :=
    match (lookForBlock blockIndex diagram) with
      | Some b => (inputEnv blockIndex)
      | _ => DependencyLattice.bot
    end. *)

(* mergeDependencies *)
(* combines with the dependency lattice join operator the dependencies of the *)
(* blocks from the lists *)
Definition mergeDependencies 
  (dependencies : DependencyEnvironment.PreOrder.type) 
  (diagram : ModelType) 
  (blockIndexSequence : list nat) :=
    fold_right 
      (fun blockIndex queue => 
        DependencyLattice.join tt (dependencies blockIndex) queue) 
      DependencyLattice.bot 
      blockIndexSequence.

(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
Definition forwardInputDependencies 
  (diagram : ModelType) 
  (currentInputDependencies : DependencyEnvironment.PreOrder.type) 
  (currentOutputDependencies : DependencyEnvironment.PreOrder.type)
  : DependencyEnvironment.PreOrder.type := 
   fun blockIndex => 
     (* the block number must be between 0 and nbrBlocks - 1 *)
     (* as it is a natural number, it cannot be less than 0 *)
     if (leb (size diagram) blockIndex)
     then DependencyLattice.bot
     else 
       (* Get the block with this index from the diagram *)
       match (lookForBlock blockIndex diagram) with
         (* There exists a block with this index *)
         | (Some blockKind) =>
           (* Get the list of the direct controled blocks *)
           let controlTargets := 
             controlTargetFor (controlSignals ModelElementType diagram) blockIndex 
           in
           (* get the list of the direct data providing blocks *)
           let dataSources := 
             dataSourceFor (dataSignals ModelElementType diagram) blockIndex 
           in
             (* Merge the various dependencies *) 
             DependencyLattice.join 
               tt 
               (match blockKind with
                 | (Sequential _ _) => DependencyLattice.singleton blockIndex
                 | _ => DependencyLattice.bot
               end)
               (DependencyLattice.join
                 tt
                 (* Controled block input dependencies *)
                 (mergeDependencies currentInputDependencies diagram controlTargets)
                 (* Data provider ouput dependencies *) 
                 (mergeDependencies
                   currentOutputDependencies
                   diagram 
                   dataSources))
         | _    => DependencyLattice.bot
       end.

Theorem forwardInputDependencies_default_bot : 
  forall (diagram : ModelType) 
  (currentInputDependencies : DependencyEnvironment.PreOrder.type) 
  (currentOutputDependencies : DependencyEnvironment.PreOrder.type) 
  (n : nat), 
  (size diagram) <= n 
  -> (forwardInputDependencies diagram currentInputDependencies currentOutputDependencies n) = DependencyLattice.bot.
  Proof.
    intros.
    unfold forwardInputDependencies in |- *.
    generalize (leb_complete_conv n (size diagram)).
    case (leb (size diagram) n).
    auto.
    intro.
    absurd (size diagram <= n).
    apply lt_not_le.
    apply H0.
    reflexivity.
    exact H.
  Qed.


(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
Definition forwardOutputDependencies 
  (diagram : ModelType)
  (inputEnv : DependencyEnvironment.PreOrder.type)
  (outputEnv : DependencyEnvironment.PreOrder.type)
  : DependencyEnvironment.PreOrder.type := 
    fun blockIndex => 
      if (leb (size diagram) blockIndex)
      then DependencyLattice.bot
      else
        match (lookForBlock blockIndex diagram) with 
          | Some( blockKind ) => 
            let controlSources := controlSourceFor (controlSignals ModelElementType diagram) blockIndex in 
              DependencyLattice.join 
                tt 
                (* the current block itself *)
                (DependencyLattice.singleton blockIndex) 
                (DependencyLattice.join tt 
                (* the output dependencies of the controling blocks *)
                (* TODO : MaPa, 2008/09/02, must check the meaning of *)
                (* controled sequential blocks *)
                (mergeDependencies outputEnv diagram controlSources)
                (* the input dependencies of the block itself *) 
                (match blockKind with
                  | Sequential _ _ => DependencyLattice.bot
                  | _ => (inputEnv blockIndex)
                end)) 
          | error => DependencyLattice.bot
        end.

Theorem forwardOutputDependencies_default_bot : 
  forall (diagram : ModelType) 
  (currentInputDependencies : DependencyEnvironment.PreOrder.type) 
  (currentOutputDependencies : DependencyEnvironment.PreOrder.type) 
  (n : nat), 
  (size diagram) <= n 
  -> (forwardOutputDependencies diagram currentInputDependencies currentOutputDependencies n) = DependencyLattice.bot.
  Proof.
    intros.
    unfold forwardOutputDependencies in |- *.
    generalize (leb_complete_conv n (size diagram)).
    case (leb (size diagram) n).
    auto.
    intro.
    absurd (size diagram <= n).
    apply lt_not_le.
    apply H0.
    reflexivity.
    exact H.
  Qed.


(* TR-BS-C019 *)
(* TR-BS-C021 *)
(* TR-BS-C022 *)
(* TR-BS-C023 *)
(* TR-BS-C024 *)
(* TR-BS-C025 *)
(** function forwardDependencies *)
(*  Compute the next step in the iterative fixpoint computation *)
(*  of the dependencies *)
Definition forwardDependencies 
  (diagram : ModelType) 
  (currentDependencies : EnvironmentPair.PreOrder.type) 
  : EnvironmentPair.PreOrder.type :=
    match currentDependencies with
      | (currentInputDependencies , currentOutputDependencies) => 
        (
          match (cache _ (size diagram) DependencyLattice.bot (forwardInputDependencies diagram currentInputDependencies currentOutputDependencies)) with
            | Cache f => f
          end
        , 
          match (cache _ (size diagram) DependencyLattice.bot (forwardOutputDependencies diagram currentInputDependencies currentOutputDependencies)) with
            | Cache f => f
          end
        )
    end.

(* A property is complete and decidable if it is either true or false *)
(* and the truth value can be computed in a finite number of steps *)
Definition decidable (p: Prop) := { p } + { ~ p }.

 (* Axiom out_dep_mono: forall d, forall E E': DependencyEnvironment.PreOrder.type, DependencyEnvironment.PreOrder.le (tt, (size d)) E E' -> DependencyEnvironment.PreOrder.le (tt, (size d)) (outputDependencies E d) (outputDependencies E' d). *)

(*TR-BS-C015*)
Lemma mergeDependencies_monotonic : 
   forall E E' : DependencyEnvironment.PreOrder.type, 
   forall diagram : ModelType, 
   forall blockIndexSequence : list nat, 
     (validBlockIndexesPredicate diagram blockIndexSequence) 
     -> (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E E') 
     -> (DependencyLattice.PreOrder.le 
          tt 
          (mergeDependencies E diagram blockIndexSequence) 
          (mergeDependencies E' diagram blockIndexSequence)).
Proof.
  unfold DependencyEnvironment.PreOrder.le in |- *.
  intros E E' diagram blockIndexSequence.
  elim blockIndexSequence; 
    clear blockIndexSequence;
      simpl in |- *.
  intros;
    firstorder.
  intros.
  apply DependencyLattice.join_monotonic.
  intuition.
  firstorder.
Qed.

(*TR-BS-C015*)
Lemma forwardInputDependencies_monotonic : 
  forall diagram : ModelType, 
  forall E1 E2 E3 E4 : DependencyEnvironment.PreOrder.type,
    (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E1 E3)
    -> (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E2 E4)
    -> (DependencyEnvironment.PreOrder.le 
         (tt, (size diagram)) 
         (forwardInputDependencies diagram E1 E2) 
         (forwardInputDependencies diagram E3 E4)).
   Proof.
     unfold forwardInputDependencies in |- *;
       intros.
     unfold DependencyEnvironment.PreOrder.le in |- *.
     intros.
     generalize (leb_complete (size diagram) n).
     generalize (leb_complete_conv n (size diagram)).
     case (leb (size diagram) n);
       simpl in |- *.
     intros.
     unfold DependencySet.FiniteSet.Subset in |- *;
       auto.
     intros.
     case (lookForBlock n diagram).
     intro.
     apply DependencyLattice.join_monotonic.
     apply DependencyLattice.PreOrder.refl.
     apply DependencyLattice.join_monotonic.
     apply mergeDependencies_monotonic.
     apply validControlTargetBlocks.
     exact H.
     apply mergeDependencies_monotonic.
     apply validDataSourceBlocks.
     exact H0.
     apply DependencyLattice.PreOrder.refl.
   Qed. 

(*TR-BS-C015*)
Lemma forwardOutputDependencies_monotonic : 
  forall diagram : ModelType, 
  forall E1 E2 E3 E4 : DependencyEnvironment.PreOrder.type, 
    (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E1 E3)
    -> (DependencyEnvironment.PreOrder.le (tt, (size diagram)) E2 E4)
    -> (DependencyEnvironment.PreOrder.le 
         (tt, (size diagram)) 
         (forwardOutputDependencies diagram E1 E2) 
         (forwardOutputDependencies diagram E3 E4)).
  Proof.
    unfold forwardOutputDependencies in |- *.
    intros.
    unfold DependencyEnvironment.PreOrder.le in |- *.
    intros.
    generalize (leb_complete (size diagram) n).
    generalize (leb_complete_conv n (size diagram)).
    case (leb (size diagram) n).
    intros.
    apply DependencyLattice.PreOrder.refl.
    intros.
    case (lookForBlock n diagram).
    destruct b.
    apply DependencyLattice.join_monotonic.
    apply DependencyLattice.PreOrder.refl.
    apply DependencyLattice.join_monotonic.
    apply mergeDependencies_monotonic.
    apply validControlSourceBlocks.
    exact H0.
    apply DependencyLattice.PreOrder.refl.
    apply DependencyLattice.join_monotonic.
    apply DependencyLattice.PreOrder.refl.
    apply DependencyLattice.join_monotonic.
    apply mergeDependencies_monotonic.
    apply validControlSourceBlocks.
    exact H0.
    apply H.
    exact H1.
    apply DependencyLattice.PreOrder.refl.
  Qed.     

(*TR-BS-C015*)
(** Proof that the forward operation is monotonic *)
(* This is a derived requirement to prove the termination of the algorithm *)
Lemma forwardDependencies_monotonic : 
  forall diagram : ModelType, 
  forall dependenciesLeft dependenciesRight : EnvironmentPair.PreOrder.type,
    (EnvironmentPair.PreOrder.le 
      (tt, (size diagram)) 
      dependenciesLeft 
      dependenciesRight) 
    -> (EnvironmentPair.PreOrder.le 
         (tt, (size diagram)) 
         (forwardDependencies diagram dependenciesLeft) 
         (forwardDependencies diagram dependenciesRight)).
  Proof.
    unfold forwardDependencies in |- *.
    intros.
    destruct dependenciesLeft; 
      destruct dependenciesRight.
    rewrite (cache_is_identity _ _ _ _ (forwardInputDependencies_default_bot  diagram t  t0)).
    rewrite (cache_is_identity _ _ _ _ (forwardOutputDependencies_default_bot diagram t  t0)).
    rewrite (cache_is_identity _ _ _ _ (forwardInputDependencies_default_bot  diagram t1 t2)).
    rewrite (cache_is_identity _ _ _ _ (forwardOutputDependencies_default_bot diagram t1 t2)).
    simpl in |- *;
      intuition.
    apply forwardInputDependencies_monotonic.
    elim H.
    intro.
    intro.
    exact H0.
    elim H.
    intros.
    exact H1.
    apply forwardOutputDependencies_monotonic.
    elim H.
    intros.
    exact H0.
    elim H.
    intros.
    exact H1.
  Qed.

(* *)
Inductive isIterate (A : Set) (function : A -> A) (initial : A) : A -> Prop :=
  | O_iter : isIterate A function initial initial
  | S_iter : forall currentStep, 
    isIterate A function initial currentStep 
    -> isIterate A function initial (function currentStep).

 (* *)
 Definition isIterateBot diagram dependencies := 
   isIterate 
     EnvironmentPair.PreOrder.type 
     (forwardDependencies diagram) 
     (fun _ => DependencyLattice.bot , fun _ => DependencyLattice.bot) 
     dependencies.

Lemma post_fixpoint_forwardDependencies : 
  forall 
    (diagram : ModelType)
    (dependencies : EnvironmentPair.PreOrder.type),
  let nextDependencies := forwardDependencies diagram dependencies in
    (isIterate 
      _ 
      (forwardDependencies diagram) 
      (fun _ => DependencyLattice.bot, fun _ => DependencyLattice.bot) 
      dependencies) 
    -> EnvironmentPair.PreOrder.le (tt, (size diagram)) dependencies nextDependencies.
  Proof.
    intros diagram dependencies.
    induction dependencies.
    intros.
    unfold nextDependencies in |- *.
    induction H.
    simpl in |- *.
    split.
    unfold DependencyEnvironment.PreOrder.le in |- *.
    intros.
    simpl in H.
    simpl in |- *.
    unfold DependencySet.FiniteSet.Subset in |- *.
    rewrite (cache_is_identity _ _ _ _ (forwardInputDependencies_default_bot diagram (fun _ : nat => DependencyLattice.bot) (fun _ : nat => DependencyLattice.bot))).
    destruct forwardInputDependencies.
    intros.
    absurd (DependencySet.FiniteSet.In a0 DependencySet.FiniteSet.empty).
    apply DependencySet.FiniteSet.empty_1.
    exact H0.
    unfold DependencyEnvironment.PreOrder.le in |- *.
    intros.
    simpl in H.
    simpl in |- *.
    unfold DependencySet.FiniteSet.Subset in |- *.
    rewrite (cache_is_identity _ _ _ _ (forwardOutputDependencies_default_bot diagram (fun _ : nat => DependencyLattice.bot) (fun _ : nat => DependencyLattice.bot))).
    destruct forwardOutputDependencies.
    intros.
    absurd (DependencySet.FiniteSet.In a0 DependencySet.FiniteSet.empty).
    apply DependencySet.FiniteSet.empty_1.
    exact H0.
    apply forwardDependencies_monotonic.
    auto.
  Qed.

(* Compute the least fixed point of a recursive definition *)
(* Proof the TBC *)
(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
Function Scheduler_rec
  (diagram : ModelType) 
  (dependencies nextDependencies : EnvironmentPair.PreOrder.type) 
  (iterator : isIterateBot diagram dependencies)
  (nextIsForward : (forwardDependencies diagram dependencies) = nextDependencies)
  { wf (EnvironmentPair.gt (tt, (size diagram))) dependencies } 
  : EnvironmentPair.PreOrder.type :=
    if (EnvironmentPair.PreOrder.dec (tt, (size diagram)) 
      nextDependencies 
      dependencies) 
    then dependencies
    else Scheduler_rec diagram nextDependencies (forwardDependencies diagram nextDependencies) (eq_ind _ _ (S_iter _ _ _ _ iterator) nextDependencies nextIsForward) (refl_equal (forwardDependencies diagram nextDependencies)).
  Proof.
    intros.
    unfold EnvironmentPair.gt in |- *.
    subst.
    cut (EnvironmentPair.PreOrder.le 
          (tt, size diagram) 
          dependencies 
          (forwardDependencies diagram dependencies)).
    intro.
    unfold EnvironmentPair.lt in |- *.
    split.
    exact H.
    intro H'.
    apply anonymous.
    exact H'.
    apply post_fixpoint_forwardDependencies.
    exact iterator.
    intro d'.
    apply (EnvironmentPair.gt_well_founded (tt, size d')).
  Defined.

(** Correction Order for data signals *)

Definition isConnected (b1 b2 : nat) (d : ModelType) :=
  In b1 (dataSourceFor (dataSignals ModelElementType d) b2 ).

(*B1 is called by B2*)
Definition isCalled (b1 b2 : nat) (d : ModelType) :=
  In b2 (controlTargetFor (controlSignals ModelElementType d) b1 ).

(*Definition isSequentialProp (d : ModelType) (b  : nat) := 
match (isSequential b d) with 
 | true => True
 | false => False
end.
*)

(* prove that mergeDependencies of a block includes the block itself *)
Lemma mergeDependencies_le : 
  forall d b E l, (In b l) 
    -> DependencyLattice.PreOrder.le tt (E b) (mergeDependencies E d l).
  Proof.
    intros d b E l0.
    elim l0;
      clear l0;
      simpl in |- *.
    intro.
    intuition.
    intros.
    elim H0.
    intros.
    rewrite H1 in |- *.
    generalize 
      (DependencyLattice.join_upper tt (E b) (mergeDependencies E d l)).
    unfold DependencyLattice.upper in |- *.
    intros.
    intuition.
    intro.
    destruct l.
    absurd (In b nil).
    apply in_nil.
    intuition.
    eapply DependencyLattice.PreOrder.trans.
    apply H.
    exact H1.
    generalize
      (DependencyLattice.join_upper tt (E a) (mergeDependencies E d (n :: l))).
    unfold DependencyLattice.upper in |- *.
    intuition.
  Qed.

(*prove that for a non sequential block the InputDependencies is included in its OutputDependencies*)
(* Lemma OutInBlock : 
  forall c B E1 E2 , ((isSequential B c) = false)
    -> (B < (size c)) 
    -> (forwardOutputDependencies c E1 E2) = E2 
    -> DependencyLattice.PreOrder.le tt (E1 B) (E2 B).
  Proof.
    intros.
    rewrite <- H1 in |- *.
    unfold forwardOutputDependencies in |- *.
    case (le_lt_dec B (size c - 1)).
    intro.
    generalize (lookForBlocksValid c B).
    generalize H.
    unfold isSequential in |- *.
    case (lookForBlock B c).
    destruct b.
    intro;
      discriminate.
    intros.
    generalize
      (DependencyLattice.join_upper tt (DependencyLattice.singleton B)
        (DependencyLattice.join tt
          (mergeDependencies E2 c
            (controlSourceFor c.(controlSignals ModelElementType) B)) 
          (E1 B))).
    unfold DependencyLattice.upper in |- *;
      intuition.
    clear H5.
    unfold DependencyLattice.upper in |- *;
      intuition.
    generalize
      (DependencyLattice.join_upper tt
        (mergeDependencies E2 c
          (controlSourceFor c.(controlSignals ModelElementType) B)) 
        (E1 B)).
    unfold DependencyLattice.upper in |- *;
      intuition.
    eapply DependencyLattice.PreOrder.trans.
    apply H7.
    exact H6.
    intros.
    absurd (@ None BlockKindType  = None).
    intuition.
    reflexivity.
    intro.
    absurd (B < B).
    auto with arith.
    elim (lt_trans (size c - 1) B (size c)).
    intuition.
    intros.
    auto.
    assumption.
    auto.
  Qed. *)
 
(*Prove for 2 blocks B1 connected to B2, the OutputDependencies of the source block is included in the inputDependencies of the target block*)
(* Lemma OutInConnection : forall c B1 B2 E1 E2, size c > 0 -> B2 < size c -> isSequential B2 c = false -> isConnected B1 B2 c -> forwardInputDependencies c E1 E2 = E1 -> DependencyLattice.PreOrder.le tt (E2 B1) (E1 B2).
  Proof.
    intros.
    rewrite <- H3 in |- *.
    unfold forwardInputDependencies in |- *.
    case (le_lt_dec B2 (size c - 1)).
     intro.
     generalize (lookForBlocksValid c B2).
     generalize H1.
     unfold isSequential in |- *.
     case (lookForBlock B2 c).
     destruct b.
     intro;  
       discriminate.
    intros.
    unfold isConnected in H2.
    generalize
      (DependencyLattice.join_upper tt
        (mergeDependencies E1 c
          (controlTargetFor c.(controlSignals ModelElementType) B2))
        (mergeDependencies E2 c (dataSourceFor c.(dataSignals ModelElementType) B2))).
    unfold DependencyLattice.upper in |- *;
      intuition.
    eapply DependencyLattice.PreOrder.trans.
    apply mergeDependencies_le.
    apply H2.
    eapply H8.   
    intros.
    absurd (@ None blockKindType = None).
    intuition.
    reflexivity.
    intros.
    absurd (B2 < B2).
    auto with arith.
    elim (lt_trans (size c - 1) B2 (size c)).
    intuition.
    intros.
    auto.
    assumption.
    apply H0.
  Qed.
*)
(*
Lemma CorrectnessDataSignal : forall d c B1 B2 E1 E2, isSequential B2 c = false -> isConnected B1 B2 c -> forwardInputDependencies c E1 E2 = E1 -> forwardOutputDependencies c E1 E2 = E2 -> DependencyLattice.PreOrder.le d (forwardOutputDependencies c  E1 E2 B1) (forwardOutputDependencies c  E1 E2 B2).
Proof.
  intros.
  unfold DependencyLattice.PreOrder.le in |- *.
  destruct (forwardOutputDependencies c E1 E2 B1).
  destruct (forwardOutputDependencies c E1 E2 B2).
  unfold isConnected in H0.
  simpl in H0.
Qed.

Lemma CorrectnessControlSignal : forall d b1 b2, isCalledby b1 b2 -> PreOrder.le (forwardOutputDependencies d b1 E) (forwardOutputDependencies d b2 E).
Proof.
intros.
Qed.
*)

(* *)
(*TR-BS-C019*)
(*TR-BS-C021*)
(*TR-BS-C022*)
(*TR-BS-C023*)
(*TR-BS-C024*)
(*TR-BS-C025*)
Definition computeDependencies
  (diagram : ModelType) : EnvironmentPair.PreOrder.type :=
    let initialDependencies := 
      (fun _ => DependencyLattice.bot , fun _ => DependencyLattice.bot)
    in 
      Scheduler_rec 
        diagram 
        initialDependencies 
        (forwardDependencies diagram initialDependencies)
        (O_iter 
          EnvironmentPair.PreOrder.type 
          (forwardDependencies diagram) 
          initialDependencies)
        (refl_equal (forwardDependencies diagram initialDependencies)).

Definition modelDependencies := (computeDependencies model).

Definition modelInputDependencies
  (diagram : ModelType) : DependencyEnvironment.PreOrder.type :=
    fst (computeDependencies diagram).

Definition modelOutputDependency
  (diagram : ModelType) : DependencyEnvironment.PreOrder.type :=
    snd (computeDependencies (diagram)).

(*
Axiom inputDependency_correctness :
  forall (d : ModelType) (b : nat), (In b d.blockIndexes) ->
    let block := lookForBlock d b in
    let input := inputDependency(d)(b) in
      ((block.kind = Sequential) -> (input = (singleton b))).
*)
(** Loops detection
   -E : is the forwardOutputDependencies Environment*)
(*TR-BS-C010*)
Definition blocksInAlgebraicLoop
  (dependencies : EnvironmentPair.PreOrder.type) 
  (blocks : list nat) : list nat :=
    filter 
      (fun blockIndex => isSequential blockIndex model) 
      (filter 
        (fun blockIndex => 
          DependencySet.FiniteSet.mem blockIndex  
          (proj1_sig (fst dependencies blockIndex)))
        blocks).

(* Total ordering based on the assigned priority natural number property. *)
(* TR-BS-C027 *)

Module AssignedPriorityTotalPreOrder := NaturalTotalPreOrder.

(* Ordering based on the optional user defined priority natural number *)
(* property *)
(*TR-BS-C026*)

Module UserDefinedPriorityTotalPreOrder := PriorityPreOrder.

(*TR-BS-C026*)
(*TR-BS-C027*)
(* Lexicographic combined ordering between user defined priority *)
(* and assigned priority *)
(* The lexicographic product ensure that the user defined priority is *)
(* handled before the assigned priority *)
Module UserDefinedThenAssignedTotalPreOrder := 
  TotalPreOrderLexicographicProduct 
    (UserDefinedPriorityTotalPreOrder)
    (AssignedPriorityTotalPreOrder).

(** *Order Tuple Module*)

(* Types of the functions that will provide the assigned priority and *)
(* user defined priority *)

(* These are functions from the block index to the priority value *)

(* TODO: MaPa, 2008/08/17, It should not be nat *)
(* but a range between min and max *)

Definition AssignedPriorityFunctionType := 
  nat -> AssignedPriorityTotalPreOrder.type.

Definition UserDefinedPriorityFunctionType := 
  nat -> UserDefinedPriorityTotalPreOrder.type.

(* TODO: MaPa, 2008/08/17, should use the value inside the block not *)
(* an external function *)
Variable userDefinedPriorityFunction : UserDefinedPriorityFunctionType.

(* TODO: MaPa, 2008/08/17, should use the value inside the block not *)
(* an external function *)
Variable assignedPriorityFunction : AssignedPriorityFunctionType.

 (* TR-BS-C020 *)
(* Return the pair of priorities: user defined and assigned *)
 Module UserDefinedThenAssignedFunction <: ArrowType.

   (* *)
   Definition source := nat.

   (* *)
   Definition target := UserDefinedThenAssignedTotalPreOrder.type.

   (* *)
   Definition apply b := (userDefinedPriorityFunction b, assignedPriorityFunction b).

 End UserDefinedThenAssignedFunction.

(* Order on block indexes based on lexicographic product of user defined *)
(* priority and assigned priority *)
(* Combines a function that associate the pair of priorities to each index *)
(* and the ordering on the pair or priorities *)
(* TR-BS-C020 *)
Module BlockUserDefinedThenAssignedTotalPreOrder := InverseImageTotalPreOrder 
  (UserDefinedThenAssignedFunction) 
  (UserDefinedThenAssignedTotalPreOrder).

(* Reverse order on block indexes based on priorities *)
(* The greatest becomes the least and vice versa *)
(* TR-BS-C020 *)
Module ReverseBlockUserDefinedThenAssignedTotalPreOrder := 
  ReverseTotalPreOrder (BlockUserDefinedThenAssignedTotalPreOrder).

(* Order on sequence of blocks defined lexicographicaly using *)
(* the priorities order *)
(* TR-BS-C020 *)
Module DependencyUserDefinedThenAssignedTotalPreOrder := 
  TotalPreOrderLexicographicSequence 
    (BlockUserDefinedThenAssignedTotalPreOrder).

(* Associate to each block index the reverse sorted list of dependencies *)
(* according to the reverse priorities *)
(* TR-BS-C020 *)
Module SortedDependencyFunction <: ArrowType.

  (* *)
  Definition source := nat.

  (* *)
  Definition target := DependencyUserDefinedThenAssignedTotalPreOrder.type.

  (* Sort the dependencies using the reverse priorities order *)
  Definition sortDependency (blockIndexSequence : DependencyLattice.PreOrder.type) :=
    match 
      treesort 
        ReverseBlockUserDefinedThenAssignedTotalPreOrder.type 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.le tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.eq tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.total tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.eq_dec tt) 
        (ReverseBlockUserDefinedThenAssignedTotalPreOrder.trans tt)
        (DependencySet.FiniteSet.elements (proj1_sig blockIndexSequence)) 
    with
      | exist2 sortedBlockIndexSequence _ _ => sortedBlockIndexSequence
    end.

  (* Avoid recomputing several time the sorting by using a cache *)
  Definition cacheSortDependency := 
    cache _ 
      (* Size of the cache *)
      (size model)
      (* Default value outside the cache *)
      (DependencySet.FiniteSet.elements (proj1_sig DependencyLattice.bot))
      (* Initialize the cache with the results of the sorting *) 
      (fun blockIndex => sortDependency (snd modelDependencies blockIndex)).

  (* TODO : remove the model reference but manipulating blocks instead of indexes *)
  Definition apply blockIndex := 
    match cacheSortDependency with
      | Cache function => (function blockIndex)
    end.

End SortedDependencyFunction.

(* The GeneAuto specific pre order defined on blocks based on the dependency *)
(* to ensure correctness and the user defined and assigned priorities to *)
(* ensure deterministic unique sorting of blocks *)
(* TR-BS-C020 *)
Module GeneAutoBlockTotalPreOrder := InverseImageTotalPreOrder 
  (SortedDependencyFunction) 
  (DependencyUserDefinedThenAssignedTotalPreOrder).

(** sorting blocks according to :
     -Output Dependencies
     -User Defined priority
     -Assigned priority *)
(* TR-BS-C020 *)
Definition sortBlocks ( blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :=
 (treesort 
   GeneAutoBlockTotalPreOrder.type  
   (GeneAutoBlockTotalPreOrder.le tt) 
   (GeneAutoBlockTotalPreOrder.eq tt) 
   (GeneAutoBlockTotalPreOrder.total tt) 
   (GeneAutoBlockTotalPreOrder.eq_dec tt) 
   (GeneAutoBlockTotalPreOrder.trans tt) 
   blockIndexes).

Definition splitIsolatedBlocks (blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :
  (list GeneAutoBlockTotalPreOrder.type) * (list GeneAutoBlockTotalPreOrder.type) :=
  partition 
 (* TODO : remove the model reference but manipulating blocks instead of indexes *)
    (fun blockIndex => 
      (isIsolated 
        (dataSignals ModelElementType model) 
        (controlSignals ModelElementType model) 
        blockIndex)) 
      blockIndexes.

Definition splitLoopingBlocks ( blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :
  (list GeneAutoBlockTotalPreOrder.type) * (list GeneAutoBlockTotalPreOrder.type) :=
  partition
    (fun blockIndex =>
      (negb (isSequential blockIndex model)) && (DependencySet.FiniteSet.mem blockIndex (proj1_sig (fst modelDependencies blockIndex))))
    blockIndexes.

Definition splitControledBlocks ( blockIndexes : list GeneAutoBlockTotalPreOrder.type ) :
  (list GeneAutoBlockTotalPreOrder.type) * (list GeneAutoBlockTotalPreOrder.type) :=
  partition
 (* TODO : remove the model reference but manipulating blocks instead of indexes *)
    (fun blockIndex => 
      (isControled (controlSignals ModelElementType model) blockIndex))
      blockIndexes.
		     	     
(*isolated blocks are first executed *)
(* TR-BS-C020 *)
(* TR-BS-C021 *)
Definition sequenceBlocks (blocks : list GeneAutoBlockTotalPreOrder.type) :=
  let (alone,linked) := splitIsolatedBlocks blocks in
    let (controled,free) := splitControledBlocks linked in
(*  let (inLoop,outLoop) := splitLoopingBlocks free in *)
      let inLoop : list GeneAutoBlockTotalPreOrder.type := nil in
        let outLoop : list GeneAutoBlockTotalPreOrder.type := free in
          let sortedAloneBlocks := (sortBlocks alone) in
            let sortedControledBlocks := (sortBlocks controled) in
              let sortedOutLoopBlocks := (sortBlocks outLoop) in
                match sortedAloneBlocks with
                  exist2 dataSortedAloneBlocks _ _ => 
                  match sortedControledBlocks with
                    exist2 dataSortedControledBlocks _ _ => 
                    match sortedOutLoopBlocks with
                      exist2 dataSortedOutLoopBlocks _ _ =>
                      ((inLoop, dataSortedControledBlocks),(app dataSortedAloneBlocks dataSortedOutLoopBlocks))
                    end
                  end
                end.

Definition sequenceModel (model : ModelType) := model.

(* TODO: MaPa, 2008/08/15, new architecture for enhanced performance *)
(* step 1 : read model -> importedBlocks (FiniteMap) + signals *)
(* step 2 : signals -> internalBlocks (FiniteMap) (add input/output) *)
(*          input/output = FiniteSet importedBlocks *)
(* step 3 : internalBlocks -> annotatedBlocks (add dependencies) *)
(*          dependencies = (sorted) FiniteSet of importedBlocks *)
(* step 4 : detect algebraic loops *)
(* step 5 : extract uncontrolled blocks *)
(* step 6 : sort these annotatedBlocks *)
(* step 7 : compute position in list *)
(* step 8 : merge loops, controlled, uncontrolled blocks *)
