(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/coq/oldies/Util.v,v $
 *  @version	$Revision: 1.1 $
 *  @date	$Date: 2009-05-20 20:55:58 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

Require Import Arith.
Require Import List.

Axiom option_hd : forall (A : Set) (l : list A), { a : A | forall e, hd e l = a } + { l = nil }.

(* Axiom ocaml_append : forall (A : Type), list A -> list A -> list A. *)

(* Axiom ocaml_exists : forall (A : Type), (A -> bool) -> list A -> bool. *)

(* Axiom ocaml_fold_right : forall (A B : Type), (B -> A -> A) -> (list B) -> A -> A. *)

(* Axiom ocaml_append_is_app : ocaml_append = app. *)

(* Axiom ocaml_exists_is_existsb : ocaml_exists = existsb. *)

(* Axiom ocaml_fold_right_is_fold_right : forall (A B : Type) (f : B -> A -> A) (e : A) (l : list B),
  ocaml_fold_right A B f l e = fold_right f e l. *)
(*
Axiom cache (A : Type) : Set.

Axiom build : *)

Inductive CacheType (A : Type) : Type :=
 | Cache : (nat -> A) -> CacheType A.

Axiom cache : forall (A : Type), nat -> A -> (nat -> A) -> CacheType A.

Axiom cache_is_identity : 
  forall (A : Type) (size : nat) (default : A) (function : nat -> A),
    (forall n, size <= n -> function n = default) 
    -> (cache A size default function) = (Cache A function).

