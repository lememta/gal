(* This code was written by: *)
(*      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7 *)
(*      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7 *)
(* It is released under GPL v2 license *)

(* This files represents a GeneAuto model in the GeneAuto language. *)

(* Coq standard libraries for managing simple lists *)
Require Import List.

(* Coq standard libraries for managing simple natural numbers *)
Require Import Arith.

(* Coq standard libraries for managing simple sets *)
Require Import Ensembles.

(* Coq standard libraries for textual concrete representation of *)
(* operations in the standard library *)
Require Import Notations.

(* GeneAuto definition of the basic blocks in the library *)
Require Import GeneAutoLibrary.

(* GeneAuto definition of the lattice for representing the dependencies *)
Require Import DependenceLattice.

(* Data type representing the various possible value *)
(* for a block execution order *)
Inductive ExecutionOrderType : Type :=
  (* The block cannot be sequenced because it is part of an *)
  (* algebraic loop *)
  | Loop : ExecutionOrderType
  (* The block is controled by another block and thus cannot *)
  (* be assigned a static execution order *)
  | Controled : ExecutionOrderType
  (* The block has been sequenced *)
  | Position : nat -> ExecutionOrderType.

(* Data type represeting the various possible types for signals *)
Inductive SignalType : Type  :=
(* This is the types for a data signal *)
 | Integer : SignalType		  
 | Boolean : SignalType
 | Real : SignalType
 | Vector : nat -> SignalType -> SignalType
 | Matrix : nat -> nat -> SignalType -> SignalType
(* This is the type for a control signal *)
 | Event : SignalType
(* All the possible values *)
 | Top : SignalType
(* No value at all *)
 | Bottom : SignalType.

(*
Record ImportedBlockType : Set := {
  blockKind : BlockKind ;
  userDefinedPriority : option positive ;
  assignedPriority : positive ;
  blockIndex : positive
}.

Record ImportedModelType : Set := {
  blocks : ImportedBlockMap ;
  signals : SignalMap
}.

Record InternalBlockType : Set := {
  importedBlock : ImportedBlock ;
  dataInputs : list ImportedBlock ;
  dataOutputs : list ImportedBlock ;
  controlInputs : list ImportedBlock ;
  controlOutputs : list ImportedBlock
}.

Record DecoratedBlock : Set := {
  internalBlock : InternalBlock ;
  inputDependencies : list ImportedBlock ;
  outputDependencies : list ImportedBlock
}.
*)

(* TODO: MaPa, 2008/08/17, we should introduce a set of indexes *)
(* and type everything based on this set instead of nat *)
(** Data type representing a block in a functional model *)
Record BlockBaseType : Set := makeBlockBase { 
  (** kind of the block from the standard library *)
  blockKind : BlockKindType ;
  (** list of indexes for block producing data taken as input *) 
  blockDataInputs : option (list nat) ;
  (** list of indexes for block consuming data taken as output *) 
  blockDataOutputs : option (list nat) ;
  (** list of blocks controling the current block *)
  blockControlInputs : option (list nat) ;
  (** list of blocks controled by the current block *)
  blockControlOutputs : option (list nat) ;
  (** User defined priority, this is an optional property *)
  blockUserDefinedPriority : option nat ;
  (** Assigned priority *)
  blockAssignedPriority : nat ;
  (** index of the block in the model *)
  blockIndex : nat
}.

Record BlockExtensionType : Set := {
  base : BlockBaseType ;
  blockExecutionOrder : option ExecutionOrderType
  (* inputDependency : *)
  (* outputDependency : *)
  (* output : *)
}.

(*
Inductive SignalType : Set :=
  | DataSignal : nat -> nat -> SignalType
  | ControlSignal : nat -> nat -> SignalType.
*)

(** Data type represeting a data signal *)
Inductive DataConnexionType : Set :=
  DataSignal : nat -> nat -> DataConnexionType.

(** Data type represeting a control signal *)
Inductive ControlConnexionType : Set :=
  ControlSignal : nat -> nat -> ControlConnexionType.

(* Definition BlocksType := list BlockBaseType. (* nat -> BlockBaseType. *) *)

(* Definition SignalsType := list SignalType. (* nat -> SignalType. *) *)

(** Data type representing a given diagram in a model *)
Record DiagramType (ModelElementType : Set) : Set := makeDiagram {
  (* blockIndexes : list nat ;
  signalIndexes : list nat ; *)
  (** list of the blocks or sub-diagram in the model *)
  (* TODO: MaPa, 2008/08/29 use blockFun: nat -> BlockType *)
  blocks : (* BlocksType *) list ModelElementType ;
  (** list of the data signals *)
  (* TODO: MaPa, 2008/08/29 use signalsFun: nat -> SignalType *)
  dataSignals : list DataConnexionType ;
  (** list of the control signals *)
  (* TODO: MaPa, 2008/08/29 use signalsFun: nat -> SignalType *)
  controlSignals : list ControlConnexionType ;
  (** number of blocks in the diagram *)
  blocksNumber : nat 
}.

(** A model is either a block or another diagram *)
(* TODO: MaPa, 2008/08/18, this is inappropriate as: *)
(*   an atomic sub-system is a combinatorial block that must be sequenced *)
(*   a virtual subsystem must be expanded before sequencement *)
(* We should have two kind of model, raw and expanded *)
(* This can be done using properties ensured by some expansion function *)
(* Or by having two different data structures *)

(* Definition expandedModel (model : ModelType) *)

(** Data type representing a model element either a Block or a sub-system *)
Inductive ModelElementType  : Set :=
  | Block : BlockBaseType -> ModelElementType 
  | Diagram : DiagramType ModelElementType -> ModelElementType.

(** Combine the parametric DiagramType and the ModelElementType *)
Definition ModelType := DiagramType ModelElementType.

(** Get the index of the model element, either a block or a sub-system *)
Definition modelElementIndex (model : ModelElementType) :=
  match model with
    | Block(block) => blockIndex block 
    | Diagram(diagram) => blocksNumber ModelElementType diagram
  end.

(*
Module Type FunctionalModelType.

  Variable blockIndexRange : nat.

  Variable signalIndexRange : nat.

  Variable blocks : nat -> BlockType.

  Variable signals : nat -> SignalType.

End FunctionalModelType.

Module FunctionalModel : FunctionalModelType.

  Variable blockIndexes : list nat.

  Variable signalIndexes : list nat.

  Variable blockSet : list BlockBaseType.

End FunctionalModel.
*)

(** function blockIndexes: *)
(*  Build the list of block indexes for a given list of blocks *)
Definition blockIndexes (modelElements : list ModelElementType) := 
  map (fun element => modelElementIndex element) modelElements.

(* Compute the number of blocks in a diagram *)
Definition size (model : ModelType) := length (blocks ModelElementType model).

(** Predicate that states that the index in a sequence are valid *)
(*  i.e. in the 0 .. (size of model - 1) range *)
Definition validBlockIndexesPredicate 
  (model : ModelType) 
  (indexSequence : list nat) := 
  (forall index, List.In index indexSequence -> index < (size model)).

(** Hypothesis that the block indexes in a given model are correct *)
Axiom validBlockIndexes : 
  forall model : ModelType, 
    validBlockIndexesPredicate 
      model 
      (map (modelElementIndex) (blocks ModelElementType model)).

(* TODO: MaPa, 2008/08/15, replace for a FiniteMap indexed by a binary number *)
(* in order to improve the access to a block *)
(* TODO : MaPa, 2008/08/17, we should replace nat, with the type of *)
(* valid block indexes *)
Definition lookForBlock (index : nat) (model : ModelType) : option BlockKindType :=
  match 
    (List.find 
      (fun c => 
        if eq_nat_dec (modelElementIndex c) index
        then true 
        else false) 
      (blocks ModelElementType model)) 
  with
    | Some (Block k) => Some (blockKind k)
    | _              => None
end.

Axiom lookForBlocksValid :
  forall model : ModelType, 
  forall index : nat,  index < size model -> (lookForBlock index model) <> None.

(** Test if a given block designated by its index in a model *)
(* is sequential or not *)
Definition isSequential (blockIndex : nat) (model : ModelType) := 
  match lookForBlock blockIndex model with 
    | Some (blockKind) => 
      (match blockKind with 
        | (Sequential _ _) => true 
        | _ => false 
      end)
    | _ => false
  end.

(* build the list of all the block numbers that are connected through *)
(* data signals to the block as source *)
Fixpoint dataSourceFor (dataSignals : list DataConnexionType) (block : nat) { struct dataSignals } :=
  match dataSignals with
    | nil => nil
    | (DataSignal src dst) :: queue => 
      if (eq_nat_dec block dst)
      then src :: (dataSourceFor queue block)
      else (dataSourceFor queue block)
  end.

(** build the list of all the block numbers that are connected through *)
(* data signals to the block as destination *)
Fixpoint dataTargetFor ( dataSignals : list DataConnexionType) (block : nat) { struct dataSignals } :=
  match dataSignals with 
    | nil => nil
    | (DataSignal src dst)::queue => 
      if (eq_nat_dec block src)
      then dst::(dataTargetFor queue block)
      else (dataTargetFor queue block)
  end.

(*
Fixpoint adjacentBlocks ( signals : list SignalType) (block : nat) { struct signals } :=
  match signals with 
    | nil => (nil,nil,nil,nil)
    | head::queue =>
      let (dataSources,dataTargets,controlSources,controlTargets) := (adajacentBlocks block queue) in
        match head with
          | (DataSignal src dst) =>
            if (eq_nat_dec block src)
            then 
              if (eq_nat_dec block dst)
              then (src::dataSources,dst::dataTargets,controlSources,controlTargets)
              else (dataSources,dst::dataTargets,controlSources,controlTargets)
            else (dataTargetFor queue block)
              if (eq_nat_dec block dst)
              then (src::dataSources,dataTargets,controlSources,controlTargets)
              else (dataSources,dataTargets,controlSources,controlTargets)
          | (ControlSignal src dst) =>
            if (eq_nat_dec block src)
            then 
              if (eq_nat_dec block dst)
              then (dataSources,dataTargets,src::controlSources,dst::controlTargets)
              else (dataSources,dataTargets,controlSources,dst::controlTargets)
            else (dataTargetFor queue block)
              if (eq_nat_dec block dst)
              then (dataSources,dataTargets,src::controlSources,controlTargets)
              else (dataSources,dataTargets,controlSources,controlTargets)
        end.
  end.
*)

(** build the list of all the block numbers that are connected through *)
(* control signals to the block as destination *)
Fixpoint controlTargetFor (controlSignals : list ControlConnexionType) (block : nat) {struct controlSignals} :=
  match controlSignals with 
    | nil => nil 
    | (ControlSignal src dst)::queue => 
      if (eq_nat_dec block src) 
      then dst::(controlTargetFor queue block)
      else (controlTargetFor queue block) 
end.

(** build the list of all the block numbers that are connected through *)
(* control signals to the block as source *)
Fixpoint controlSourceFor (controlSignals : list ControlConnexionType) (block : nat) {struct controlSignals} :=
  match controlSignals with 
    | nil => nil
    | (ControlSignal src dst)::queue => 
      if (eq_nat_dec block dst)
      then src::(controlSourceFor queue block)
      else (controlSourceFor queue block)
  end.

(** Hypothesis that the data source block indexes in a given model are correct *)
Axiom validDataSourceBlocks :
 forall model : ModelType, 
    forall index : nat, validBlockIndexesPredicate model (dataSourceFor model.(dataSignals ModelElementType) index).

(** Hypothesis that the data target block indexes in a given model are correct *)
Axiom validDataTargetBlocks :
 forall model : ModelType, 
    forall index : nat, validBlockIndexesPredicate model (dataTargetFor model.(dataSignals ModelElementType) index).

(** Hypothesis that the control source block indexes in a given model are correct *)
Axiom validControlSourceBlocks :
  forall model : ModelType, 
    forall index : nat, validBlockIndexesPredicate model (controlSourceFor model.(controlSignals ModelElementType) index).

(** Hypothesis that the control target block indexes in a given model are correct *)
Axiom validControlTargetBlocks :
  forall model : ModelType, 
    forall index : nat, validBlockIndexesPredicate model (controlTargetFor model.(controlSignals ModelElementType) index).

(** compute the set of data sources, data target, control sources and *)
(*  control targets for a given block according to the signals in the model *)
Definition annotateBlock (diagram : ModelType) (block : BlockBaseType) : BlockBaseType :=
  makeBlockBase
    (blockKind block)
    (Some (dataSourceFor (dataSignals ModelElementType diagram) (blockIndex block)))
    (Some (dataTargetFor (dataSignals ModelElementType diagram) (blockIndex block)))
    (Some (controlSourceFor (controlSignals ModelElementType diagram) (blockIndex block)))
    (Some (controlTargetFor (controlSignals ModelElementType diagram) (blockIndex block)))
    (blockUserDefinedPriority block)
    (blockAssignedPriority block)
    (blockIndex block).

(** compute the set of data sources, data target, control sources and *)
(*  control targets for al the blocks according to the signals in the model *)
Definition annotateDiagram (model : ModelType) : ModelType :=
  makeDiagram ModelElementType
    (List.map 
      (fun modelElement =>
        match modelElement with
          | Block( block ) => Block(annotateBlock model block)
          | Diagram( diagram ) => Diagram( diagram )
        end)
      (blocks ModelElementType model))
    (dataSignals ModelElementType model)
    (controlSignals ModelElementType model)
    (blocksNumber ModelElementType model).
                      
(*Set of block dependencies *)
Fixpoint blockDependencies (model : ModelType) (n: list nat) (l : list nat) (s: Ensemble nat) {struct l} : Ensemble nat :=
  match l with 
    | nil  => Empty_set nat
    | p::q => blockDependencies model n q (Add nat s p)
  end.

(** Check whether a given block is isolated *)
Definition isIsolated (dataSignals : list DataConnexionType) (controlSignals : list ControlConnexionType) (block : nat) :=
  match (app (app (dataSourceFor dataSignals block) (dataTargetFor dataSignals block)) (app (controlSourceFor controlSignals block) (controlTargetFor controlSignals block))) with 
    | nil => true
    | _  => false
end.

(*
Axiom isIsolated_correctness : 
*)

(** Check whether a given block is controled *)
Definition isControled
  (controlSignals : list ControlConnexionType) 
  (blockIndex : nat) :=
  match (controlSourceFor controlSignals blockIndex) with 
    | nil => false
    | _  => true
end.

(*
Axiom isControled_correctness :
  forall model : ModelType,
  forall blockIndex : nat,
  forall signalIndex : nat,
  (validModel model) ->
  (validBlockIndex blockIndex model) ->
  (validSignalIndex signalIndex model) ->
  (isControlSignal signalIndex) ->
  (blockSignal = (targetBlock signalIndex))
*)
