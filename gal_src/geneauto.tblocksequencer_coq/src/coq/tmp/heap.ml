open Datatypes
open Coq_list
open Multiset
open Peano
open Sorting
open Specif

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

type 'a coq_Tree =
  | Tree_Leaf
  | Tree_Node of 'a * 'a coq_Tree * 'a coq_Tree

(** val coq_Tree_rect : 'a2 -> ('a1 -> 'a1 coq_Tree -> 'a2 -> 'a1 coq_Tree ->
                        'a2 -> 'a2) -> 'a1 coq_Tree -> 'a2 **)

let rec coq_Tree_rect f f0 = function
  | Tree_Leaf -> f
  | Tree_Node (a, t0, t1) ->
      f0 a t0 (coq_Tree_rect f f0 t0) t1 (coq_Tree_rect f f0 t1)

(** val coq_Tree_rec : 'a2 -> ('a1 -> 'a1 coq_Tree -> 'a2 -> 'a1 coq_Tree ->
                       'a2 -> 'a2) -> 'a1 coq_Tree -> 'a2 **)

let rec coq_Tree_rec f f0 = function
  | Tree_Leaf -> f
  | Tree_Node (a, t0, t1) ->
      f0 a t0 (coq_Tree_rec f f0 t0) t1 (coq_Tree_rec f f0 t1)

(** val is_heap_rec : 'a2 -> ('a1 -> 'a1 coq_Tree -> 'a1 coq_Tree -> __ -> __
                      -> __ -> 'a2 -> __ -> 'a2 -> 'a2) -> 'a1 coq_Tree ->
                      'a2 **)

let rec is_heap_rec h h0 = function
  | Tree_Leaf -> h
  | Tree_Node (a, t0, t1) ->
      h0 a t0 t1 __ __ __ (is_heap_rec h h0 t0) __ (is_heap_rec h h0 t1)

(** val contents : ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> 'a1 multiset **)

let rec contents eqA_dec t a =
  match t with
    | Tree_Leaf -> 0
    | Tree_Node (a0, t1, t2) ->
        plus (contents eqA_dec t1 a)
          (plus (contents eqA_dec t2 a)
            (match eqA_dec a0 a with
               | true -> succ 0
               | false -> 0))

type 'a insert_spec =
  'a coq_Tree
  (* singleton inductive, whose constructor was insert_exist *)

(** val insert_spec_rect : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 coq_Tree ->
                           ('a1 coq_Tree -> __ -> __ -> __ -> 'a2) -> 'a1
                           insert_spec -> 'a2 **)

let insert_spec_rect eqA_dec a t f i =
  f i __ __ __

(** val insert_spec_rec : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 coq_Tree -> ('a1
                          coq_Tree -> __ -> __ -> __ -> 'a2) -> 'a1
                          insert_spec -> 'a2 **)

let insert_spec_rec eqA_dec a t f i =
  f i __ __ __

(** val insert : ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree
                 -> 'a1 -> 'a1 insert_spec **)

let rec insert leA_dec eqA_dec t a =
  match t with
    | Tree_Leaf -> Tree_Node (a, Tree_Leaf, Tree_Leaf)
    | Tree_Node (a0, t0, t1) ->
        (match leA_dec a0 a with
           | true -> Tree_Node (a0, t1, (insert leA_dec eqA_dec t0 a))
           | false -> Tree_Node (a, t1, (insert leA_dec eqA_dec t0 a0)))

type 'a build_heap =
  'a coq_Tree
  (* singleton inductive, whose constructor was heap_exist *)

(** val build_heap_rect : ('a1 -> 'a1 -> bool) -> 'a1 list -> ('a1 coq_Tree
                          -> __ -> __ -> 'a2) -> 'a1 build_heap -> 'a2 **)

let build_heap_rect eqA_dec l f b =
  f b __ __

(** val build_heap_rec : ('a1 -> 'a1 -> bool) -> 'a1 list -> ('a1 coq_Tree ->
                         __ -> __ -> 'a2) -> 'a1 build_heap -> 'a2 **)

let build_heap_rec eqA_dec l f b =
  f b __ __

(** val list_to_heap : ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1
                       list -> 'a1 build_heap **)

let rec list_to_heap leA_dec eqA_dec = function
  | [] -> Tree_Leaf
  | (a:: l0) ->
      insert leA_dec eqA_dec (list_to_heap leA_dec eqA_dec l0) a

type 'a flat_spec =
  'a list
  (* singleton inductive, whose constructor was flat_exist *)

(** val flat_spec_rect : ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> ('a1 list ->
                         __ -> __ -> __ -> 'a2) -> 'a1 flat_spec -> 'a2 **)

let flat_spec_rect eqA_dec t f f0 =
  f f0 __ __ __

(** val flat_spec_rec : ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> ('a1 list ->
                        __ -> __ -> __ -> 'a2) -> 'a1 flat_spec -> 'a2 **)

let flat_spec_rec eqA_dec t f f0 =
  f f0 __ __ __

(** val heap_to_list : ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1
                       coq_Tree -> 'a1 flat_spec **)

let rec heap_to_list leA_dec eqA_dec = function
  | Tree_Leaf -> []
  | Tree_Node (a, t0, t1) -> (a::
      (merge leA_dec eqA_dec (heap_to_list leA_dec eqA_dec t0)
        (heap_to_list leA_dec eqA_dec t1)))

(** val treesort : ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 list
                   -> 'a1 list sig2 **)

let treesort leA_dec eqA_dec l =
  heap_to_list leA_dec eqA_dec
    (let rec f = function
       | [] -> Tree_Leaf
       | (a:: l1) -> insert leA_dec eqA_dec (f l1) a
     in f l)


