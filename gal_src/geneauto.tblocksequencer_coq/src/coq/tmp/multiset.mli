open Datatypes
open Peano
open Specif

type 'a multiset =
  'a -> int
  (* singleton inductive, whose constructor was Bag *)

val multiset_rect : (('a1 -> int) -> 'a2) -> 'a1 multiset -> 'a2

val multiset_rec : (('a1 -> int) -> 'a2) -> 'a1 multiset -> 'a2

val coq_EmptyBag : 'a1 multiset

val coq_SingletonBag : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 multiset

val multiplicity : 'a1 multiset -> 'a1 -> int

val munion : 'a1 multiset -> 'a1 multiset -> 'a1 multiset

