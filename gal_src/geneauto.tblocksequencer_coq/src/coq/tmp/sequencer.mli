open Bool
open Compare_dec
open Datatypes
open Domain
open EqNat
open GeneAutoLanguage
open GeneAutoLibrary
open Heap
open Coq_list
open OrderedType
open Specif
open Util

type __ = Obj.t

val model : coq_ModelType

module NaturalFiniteSet : 
 sig 
  module Raw : 
   sig 
    module E : 
     sig 
      type t = Domain.OrderedNatural.t
      
      val compare : t -> t -> t coq_Compare
     end
    
    module MX : 
     sig 
      val eq_dec : Domain.OrderedNatural.t -> Domain.OrderedNatural.t -> bool
      
      val lt_dec : Domain.OrderedNatural.t -> Domain.OrderedNatural.t -> bool
      
      val eqb : Domain.OrderedNatural.t -> Domain.OrderedNatural.t -> bool
     end
    
    type elt = Domain.OrderedNatural.t
    
    type t = elt list
    
    val empty : t
    
    val is_empty : t -> bool
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val singleton : elt -> t
    
    val remove : elt -> t -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val filter : (elt -> bool) -> t -> t
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val cardinal : t -> int
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
    
    val compare : t -> t -> t coq_Compare
   end
  
  module E : 
   sig 
    type t = Domain.OrderedNatural.t
    
    val compare : t -> t -> t coq_Compare
   end
  
  type slist =
    Raw.t
    (* singleton inductive, whose constructor was Build_slist *)
  
  val slist_rect : (Raw.t -> __ -> 'a1) -> slist -> 'a1
  
  val slist_rec : (Raw.t -> __ -> 'a1) -> slist -> 'a1
  
  val this : slist -> Raw.t
  
  type t = slist
  
  type elt = E.t
  
  val mem : elt -> t -> bool
  
  val add : elt -> t -> t
  
  val remove : elt -> t -> t
  
  val singleton : elt -> t
  
  val union : t -> t -> t
  
  val inter : t -> t -> t
  
  val diff : t -> t -> t
  
  val equal : t -> t -> bool
  
  val subset : t -> t -> bool
  
  val empty : t
  
  val is_empty : t -> bool
  
  val elements : t -> elt list
  
  val min_elt : t -> elt option
  
  val max_elt : t -> elt option
  
  val choose : t -> elt option
  
  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
  
  val cardinal : t -> int
  
  val filter : (elt -> bool) -> t -> t
  
  val for_all : (elt -> bool) -> t -> bool
  
  val exists_ : (elt -> bool) -> t -> bool
  
  val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
  
  val compare : t -> t -> t coq_Compare
 end

module DependencySet : 
 sig 
  module FiniteSet : 
   sig 
    module Raw : 
     sig 
      module E : 
       sig 
        type t = Domain.OrderedNatural.t
        
        val compare : t -> t -> t coq_Compare
       end
      
      module MX : 
       sig 
        val eq_dec :
          Domain.OrderedNatural.t -> Domain.OrderedNatural.t -> bool
        
        val lt_dec :
          Domain.OrderedNatural.t -> Domain.OrderedNatural.t -> bool
        
        val eqb : Domain.OrderedNatural.t -> Domain.OrderedNatural.t -> bool
       end
      
      type elt = Domain.OrderedNatural.t
      
      type t = elt list
      
      val empty : t
      
      val is_empty : t -> bool
      
      val mem : elt -> t -> bool
      
      val add : elt -> t -> t
      
      val singleton : elt -> t
      
      val remove : elt -> t -> t
      
      val union : t -> t -> t
      
      val inter : t -> t -> t
      
      val diff : t -> t -> t
      
      val equal : t -> t -> bool
      
      val subset : t -> t -> bool
      
      val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
      
      val filter : (elt -> bool) -> t -> t
      
      val for_all : (elt -> bool) -> t -> bool
      
      val exists_ : (elt -> bool) -> t -> bool
      
      val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
      
      val cardinal : t -> int
      
      val elements : t -> elt list
      
      val min_elt : t -> elt option
      
      val max_elt : t -> elt option
      
      val choose : t -> elt option
      
      val compare : t -> t -> t coq_Compare
     end
    
    module E : 
     sig 
      type t = Domain.OrderedNatural.t
      
      val compare : t -> t -> t coq_Compare
     end
    
    type slist =
      Raw.t
      (* singleton inductive, whose constructor was Build_slist *)
    
    val slist_rect : (Raw.t -> __ -> 'a1) -> slist -> 'a1
    
    val slist_rec : (Raw.t -> __ -> 'a1) -> slist -> 'a1
    
    val this : slist -> Raw.t
    
    type t = slist
    
    type elt = E.t
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val remove : elt -> t -> t
    
    val singleton : elt -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val empty : t
    
    val is_empty : t -> bool
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val cardinal : t -> int
    
    val filter : (elt -> bool) -> t -> t
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val compare : t -> t -> t coq_Compare
   end
  
  val makeSetFromList : int list -> FiniteSet.t
  
  val carrier : FiniteSet.t
 end

module TaggedFiniteSet : 
 sig 
  module Raw : 
   sig 
    module E : 
     sig 
      type t = Domain.OrderedTaggedNumbers.t
      
      val compare : t -> t -> t coq_Compare
     end
    
    module MX : 
     sig 
      val eq_dec :
        Domain.OrderedTaggedNumbers.t -> Domain.OrderedTaggedNumbers.t ->
        bool
      
      val lt_dec :
        Domain.OrderedTaggedNumbers.t -> Domain.OrderedTaggedNumbers.t ->
        bool
      
      val eqb :
        Domain.OrderedTaggedNumbers.t -> Domain.OrderedTaggedNumbers.t ->
        bool
     end
    
    type elt = Domain.OrderedTaggedNumbers.t
    
    type t = elt list
    
    val empty : t
    
    val is_empty : t -> bool
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val singleton : elt -> t
    
    val remove : elt -> t -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val filter : (elt -> bool) -> t -> t
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val cardinal : t -> int
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
    
    val compare : t -> t -> t coq_Compare
   end
  
  module E : 
   sig 
    type t = Domain.OrderedTaggedNumbers.t
    
    val compare : t -> t -> t coq_Compare
   end
  
  type slist =
    Raw.t
    (* singleton inductive, whose constructor was Build_slist *)
  
  val slist_rect : (Raw.t -> __ -> 'a1) -> slist -> 'a1
  
  val slist_rec : (Raw.t -> __ -> 'a1) -> slist -> 'a1
  
  val this : slist -> Raw.t
  
  type t = slist
  
  type elt = E.t
  
  val mem : elt -> t -> bool
  
  val add : elt -> t -> t
  
  val remove : elt -> t -> t
  
  val singleton : elt -> t
  
  val union : t -> t -> t
  
  val inter : t -> t -> t
  
  val diff : t -> t -> t
  
  val equal : t -> t -> bool
  
  val subset : t -> t -> bool
  
  val empty : t
  
  val is_empty : t -> bool
  
  val elements : t -> elt list
  
  val min_elt : t -> elt option
  
  val max_elt : t -> elt option
  
  val choose : t -> elt option
  
  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
  
  val cardinal : t -> int
  
  val filter : (elt -> bool) -> t -> t
  
  val for_all : (elt -> bool) -> t -> bool
  
  val exists_ : (elt -> bool) -> t -> bool
  
  val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
  
  val compare : t -> t -> t coq_Compare
 end

module DependencySetTagged : 
 sig 
  module FiniteSet : 
   sig 
    module Raw : 
     sig 
      module E : 
       sig 
        type t = Domain.OrderedTaggedNumbers.t
        
        val compare : t -> t -> t coq_Compare
       end
      
      module MX : 
       sig 
        val eq_dec :
          Domain.OrderedTaggedNumbers.t -> Domain.OrderedTaggedNumbers.t ->
          bool
        
        val lt_dec :
          Domain.OrderedTaggedNumbers.t -> Domain.OrderedTaggedNumbers.t ->
          bool
        
        val eqb :
          Domain.OrderedTaggedNumbers.t -> Domain.OrderedTaggedNumbers.t ->
          bool
       end
      
      type elt = Domain.OrderedTaggedNumbers.t
      
      type t = elt list
      
      val empty : t
      
      val is_empty : t -> bool
      
      val mem : elt -> t -> bool
      
      val add : elt -> t -> t
      
      val singleton : elt -> t
      
      val remove : elt -> t -> t
      
      val union : t -> t -> t
      
      val inter : t -> t -> t
      
      val diff : t -> t -> t
      
      val equal : t -> t -> bool
      
      val subset : t -> t -> bool
      
      val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
      
      val filter : (elt -> bool) -> t -> t
      
      val for_all : (elt -> bool) -> t -> bool
      
      val exists_ : (elt -> bool) -> t -> bool
      
      val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
      
      val cardinal : t -> int
      
      val elements : t -> elt list
      
      val min_elt : t -> elt option
      
      val max_elt : t -> elt option
      
      val choose : t -> elt option
      
      val compare : t -> t -> t coq_Compare
     end
    
    module E : 
     sig 
      type t = Domain.OrderedTaggedNumbers.t
      
      val compare : t -> t -> t coq_Compare
     end
    
    type slist =
      Raw.t
      (* singleton inductive, whose constructor was Build_slist *)
    
    val slist_rect : (Raw.t -> __ -> 'a1) -> slist -> 'a1
    
    val slist_rec : (Raw.t -> __ -> 'a1) -> slist -> 'a1
    
    val this : slist -> Raw.t
    
    type t = slist
    
    type elt = E.t
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val remove : elt -> t -> t
    
    val singleton : elt -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val empty : t
    
    val is_empty : t -> bool
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val cardinal : t -> int
    
    val filter : (elt -> bool) -> t -> t
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val compare : t -> t -> t coq_Compare
   end
  
  val makeSetFromListTagged : Domain.TaggedType.t list -> FiniteSet.t
  
  val callList : int list -> Domain.TaggedType.t list
  
  val returnList : int list -> Domain.TaggedType.t list
  
  val carrier : FiniteSet.t
 end

module DependencyLattice : 
 sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type = unit
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = DependencySetTagged.FiniteSet.t
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val singleton : DependencySetTagged.FiniteSet.elt -> PreOrder.coq_type
 end

module DependencyEnvironment : 
 sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type =
        (DependencyLattice.PreOrder.Data.coq_type, int) ocaml_prod
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = int -> DependencyLattice.PreOrder.coq_type
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type -> int
    -> DependencyLattice.PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type -> int
    -> DependencyLattice.PreOrder.coq_type
 end

module EnvironmentPair : 
 sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type = DependencyEnvironment.PreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type =
      (DependencyEnvironment.PreOrder.coq_type,
      DependencyEnvironment.PreOrder.coq_type) ocaml_prod
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot :
    (DependencyEnvironment.PreOrder.coq_type,
    DependencyEnvironment.PreOrder.coq_type) ocaml_prod
  
  val top :
    (DependencyEnvironment.PreOrder.coq_type,
    DependencyEnvironment.PreOrder.coq_type) ocaml_prod
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
 end

type 'a dec_eq = 'a -> 'a -> bool

val mergeDependencies :
  DependencyEnvironment.PreOrder.coq_type -> coq_ModelType -> int list ->
  DependencyLattice.PreOrder.coq_type

val makeCallListNat : int list -> Domain.TaggedType.taggedNumbers list

val makeReturnListNat : int list -> Domain.TaggedType.taggedNumbers list

val makeSetFromList :
  Domain.TaggedType.t list -> DependencyLattice.PreOrder.coq_type

val forwardInputDependencies :
  coq_ModelType -> DependencyEnvironment.PreOrder.coq_type ->
  DependencyEnvironment.PreOrder.coq_type ->
  DependencyEnvironment.PreOrder.coq_type

val forwardOutputDependencies :
  coq_ModelType -> DependencyEnvironment.PreOrder.coq_type ->
  DependencyEnvironment.PreOrder.coq_type ->
  DependencyEnvironment.PreOrder.coq_type

val forwardDependencies :
  coq_ModelType -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type

type decidable = bool

val coq_Scheduler_rec_F :
  (coq_ModelType -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> __ -> __ ->
  EnvironmentPair.PreOrder.coq_type) -> coq_ModelType ->
  EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type

val coq_Scheduler_rec_terminate :
  coq_ModelType -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type

val coq_Scheduler_rec :
  coq_ModelType -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type

type coq_R_Scheduler_rec =
  | R_Scheduler_rec_0 of EnvironmentPair.PreOrder.coq_type
  * EnvironmentPair.PreOrder.coq_type
  | R_Scheduler_rec_1 of EnvironmentPair.PreOrder.coq_type
  * EnvironmentPair.PreOrder.coq_type * EnvironmentPair.PreOrder.coq_type
  * coq_R_Scheduler_rec

val coq_R_Scheduler_rec_rect :
  coq_ModelType -> (EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> __ -> __ -> __ -> __ -> 'a1) ->
  (EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  __ -> __ -> __ -> __ -> EnvironmentPair.PreOrder.coq_type ->
  coq_R_Scheduler_rec -> 'a1 -> 'a1) -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  coq_R_Scheduler_rec -> 'a1

val coq_R_Scheduler_rec_rec :
  coq_ModelType -> (EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> __ -> __ -> __ -> __ -> 'a1) ->
  (EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  __ -> __ -> __ -> __ -> EnvironmentPair.PreOrder.coq_type ->
  coq_R_Scheduler_rec -> 'a1 -> 'a1) -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  coq_R_Scheduler_rec -> 'a1

val coq_Scheduler_rec_rect :
  coq_ModelType -> (EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> __ -> __ -> __ -> __ -> 'a1) ->
  (EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  __ -> __ -> __ -> __ -> 'a1 -> 'a1) -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> 'a1

val coq_Scheduler_rec_rec :
  coq_ModelType -> (EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> __ -> __ -> __ -> __ -> 'a1) ->
  (EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  __ -> __ -> __ -> __ -> 'a1 -> 'a1) -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> 'a1

val coq_R_Scheduler_rec_correct :
  coq_ModelType -> EnvironmentPair.PreOrder.coq_type ->
  EnvironmentPair.PreOrder.coq_type -> EnvironmentPair.PreOrder.coq_type ->
  coq_R_Scheduler_rec

val computeDependencies : coq_ModelType -> EnvironmentPair.PreOrder.coq_type

val modelDependencies : EnvironmentPair.PreOrder.coq_type

val modelInputDependencies :
  coq_ModelType -> DependencyEnvironment.PreOrder.coq_type

val modelOutputDependencies :
  coq_ModelType -> DependencyEnvironment.PreOrder.coq_type

module AssignedPriorityTotalPreOrder : 
 sig 
  module Data : 
   sig 
    type coq_type = unit
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = int
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module UserDefinedPriorityTotalPreOrder : 
 sig 
  module Data : 
   sig 
    type coq_type = unit
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = int option
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module UserDefinedThenAssignedTotalPreOrder : 
 sig 
  module L : 
   sig 
    module Data : 
     sig 
      type coq_type = UserDefinedPriorityTotalPreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type =
      (UserDefinedPriorityTotalPreOrder.coq_type,
      AssignedPriorityTotalPreOrder.coq_type) ocaml_prod
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = UserDefinedPriorityTotalPreOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = L.coq_type
  
  val dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val eq_dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val total : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
 end

type coq_AssignedPriorityFunctionType =
  int -> AssignedPriorityTotalPreOrder.coq_type

type coq_UserDefinedPriorityFunctionType =
  int -> UserDefinedPriorityTotalPreOrder.coq_type

val userDefinedPriorityFunction : coq_UserDefinedPriorityFunctionType

val assignedPriorityFunction : coq_AssignedPriorityFunctionType

module UserDefinedThenAssignedFunction : 
 sig 
  type source = int
  
  type target = UserDefinedThenAssignedTotalPreOrder.coq_type
  
  val apply :
    int -> (UserDefinedPriorityTotalPreOrder.coq_type,
    AssignedPriorityTotalPreOrder.coq_type) ocaml_prod
 end

module BlockUserDefinedThenAssignedTotalPreOrder : 
 sig 
  module I : 
   sig 
    module Data : 
     sig 
      type coq_type = UserDefinedThenAssignedTotalPreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = UserDefinedThenAssignedFunction.source
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = UserDefinedThenAssignedTotalPreOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = I.coq_type
  
  val eq_dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
  
  val dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
  
  val total : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
 end

module ReverseBlockUserDefinedThenAssignedTotalPreOrder : 
 sig 
  module L : 
   sig 
    module Data : 
     sig 
      type coq_type = BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = BlockUserDefinedThenAssignedTotalPreOrder.coq_type
    
    val dec :
      BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type ->
      BlockUserDefinedThenAssignedTotalPreOrder.coq_type ->
      BlockUserDefinedThenAssignedTotalPreOrder.coq_type -> bool
    
    val eq_dec :
      BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type ->
      BlockUserDefinedThenAssignedTotalPreOrder.coq_type ->
      BlockUserDefinedThenAssignedTotalPreOrder.coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = L.coq_type
  
  val dec :
    BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type ->
    BlockUserDefinedThenAssignedTotalPreOrder.coq_type ->
    BlockUserDefinedThenAssignedTotalPreOrder.coq_type -> bool
  
  val eq_dec :
    BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type ->
    BlockUserDefinedThenAssignedTotalPreOrder.coq_type ->
    BlockUserDefinedThenAssignedTotalPreOrder.coq_type -> bool
  
  val total :
    BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type ->
    BlockUserDefinedThenAssignedTotalPreOrder.coq_type ->
    BlockUserDefinedThenAssignedTotalPreOrder.coq_type -> bool
 end

module DependencyUserDefinedThenAssignedTotalPreOrder : 
 sig 
  module L : 
   sig 
    module Data : 
     sig 
      type coq_type = BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = BlockUserDefinedThenAssignedTotalPreOrder.coq_type list
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = BlockUserDefinedThenAssignedTotalPreOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = L.coq_type
  
  val dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val eq_dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val total : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
 end

module SortedDependencyFunction : 
 sig 
  type source = int
  
  type target = DependencyUserDefinedThenAssignedTotalPreOrder.coq_type
  
  val existsDataLoop : DependencyLattice.PreOrder.coq_type -> int -> bool
  
  val removeDataLoops :
    coq_ModelType -> DependencyLattice.PreOrder.coq_type -> int ->
    Domain.TaggedType.taggedNumbers list
  
  val removeControlLoops :
    coq_ModelType -> DependencyLattice.PreOrder.coq_type -> int ->
    Domain.TaggedType.taggedNumbers list
  
  val coq_DependencySetWithoutReturn :
    DependencyLattice.PreOrder.coq_type -> Domain.TaggedType.taggedNumbers
    list
  
  val coq_DependencyListBlocks :
    Domain.TaggedType.taggedNumbers list -> int list
  
  val removeCalleesBlocks : int list -> int list
  
  val sortDependency :
    DependencyLattice.PreOrder.coq_type ->
    ReverseBlockUserDefinedThenAssignedTotalPreOrder.coq_type list
  
  val cacheSortDependency : int list coq_CacheType
  
  val apply : int -> int list
 end

module GeneAutoBlockTotalPreOrder : 
 sig 
  module I : 
   sig 
    module Data : 
     sig 
      type coq_type =
        DependencyUserDefinedThenAssignedTotalPreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = SortedDependencyFunction.source
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type =
      DependencyUserDefinedThenAssignedTotalPreOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = I.coq_type
  
  val eq_dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
  
  val dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
  
  val total : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
 end

val sortBlocks :
  GeneAutoBlockTotalPreOrder.coq_type list ->
  GeneAutoBlockTotalPreOrder.coq_type list sig2

val splitIsolatedBlocks :
  GeneAutoBlockTotalPreOrder.coq_type list ->
  (GeneAutoBlockTotalPreOrder.coq_type list,
  GeneAutoBlockTotalPreOrder.coq_type list) ocaml_prod

val splitLoopingBlocks :
  GeneAutoBlockTotalPreOrder.coq_type list ->
  (GeneAutoBlockTotalPreOrder.coq_type list,
  GeneAutoBlockTotalPreOrder.coq_type list) ocaml_prod

val splitControledBlocks :
  GeneAutoBlockTotalPreOrder.coq_type list ->
  (GeneAutoBlockTotalPreOrder.coq_type list,
  GeneAutoBlockTotalPreOrder.coq_type list) ocaml_prod

val sequenceBlocks :
  GeneAutoBlockTotalPreOrder.coq_type list ->
  ((GeneAutoBlockTotalPreOrder.coq_type list,
  GeneAutoBlockTotalPreOrder.coq_type list) ocaml_prod,
  GeneAutoBlockTotalPreOrder.coq_type list) ocaml_prod

val sequenceModel : coq_ModelType -> coq_ModelType

