open Datatypes
open Peano_dec
open Specif

module MakeEnvironment : 
 functor (Lattice:Domain.LatticeType) ->
 sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type = (Lattice.PreOrder.Data.coq_type, int) ocaml_prod
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = int -> Lattice.PreOrder.coq_type
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type -> int
    -> Lattice.PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type -> int
    -> Lattice.PreOrder.coq_type
 end

