open Datatypes
open Coq_list
open OrderedType
open Specif

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

module Raw = 
 functor (X:OrderedType.OrderedType) ->
 struct 
  module E = X
  
  module MX = OrderedType.OrderedTypeFacts(X)
  
  type elt = X.t
  
  type t = elt list
  
  (** val empty : t **)
  
  let empty =
    []
  
  (** val is_empty : t -> bool **)
  
  let is_empty = function
    | [] -> true
    | (x:: x0) -> false
  
  (** val mem : elt -> t -> bool **)
  
  let rec mem x = function
    | [] -> false
    | (y:: l) ->
        (match X.compare x y with
           | LT -> false
           | EQ -> true
           | GT -> mem x l)
  
  (** val add : elt -> t -> t **)
  
  let rec add x s = match s with
    | [] -> (x:: [])
    | (y:: l) ->
        (match X.compare x y with
           | LT -> (x:: s)
           | EQ -> s
           | GT -> (y:: (add x l)))
  
  (** val singleton : elt -> t **)
  
  let singleton x =
    (x:: [])
  
  (** val remove : elt -> t -> t **)
  
  let rec remove x s = match s with
    | [] -> []
    | (y:: l) ->
        (match X.compare x y with
           | LT -> s
           | EQ -> l
           | GT -> (y:: (remove x l)))
  
  (** val union : t -> t -> t **)
  
  let rec union s x =
    match s with
      | [] -> x
      | (x0:: l) ->
          let rec union_aux s' = match s' with
            | [] -> s
            | (x':: l') ->
                (match X.compare x0 x' with
                   | LT -> (x0:: (union l s'))
                   | EQ -> (x0:: (union l l'))
                   | GT -> (x':: (union_aux l')))
          in union_aux x
  
  (** val inter : t -> t -> t **)
  
  let rec inter s x =
    match s with
      | [] -> []
      | (x0:: l) ->
          let rec inter_aux s' = match s' with
            | [] -> []
            | (x':: l') ->
                (match X.compare x0 x' with
                   | LT -> inter l s'
                   | EQ -> (x0:: (inter l l'))
                   | GT -> inter_aux l')
          in inter_aux x
  
  (** val diff : t -> t -> t **)
  
  let rec diff s x =
    match s with
      | [] -> []
      | (x0:: l) ->
          let rec diff_aux s' = match s' with
            | [] -> s
            | (x':: l') ->
                (match X.compare x0 x' with
                   | LT -> (x0:: (diff l s'))
                   | EQ -> diff l l'
                   | GT -> diff_aux l')
          in diff_aux x
  
  (** val equal : t -> t -> bool **)
  
  let rec equal s s' =
    match s with
      | [] ->
          (match s' with
             | [] -> true
             | (e:: l) -> false)
      | (x:: l) ->
          (match s' with
             | [] -> false
             | (x':: l') ->
                 (match X.compare x x' with
                    | LT -> false
                    | EQ -> equal l l'
                    | GT -> false))
  
  (** val subset : t -> t -> bool **)
  
  let rec subset s s' =
    match s with
      | [] -> true
      | (x:: l) ->
          (match s' with
             | [] -> false
             | (x':: l') ->
                 (match X.compare x x' with
                    | LT -> false
                    | EQ -> subset l l'
                    | GT -> subset s l'))
  
  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)
  
  let rec fold f s i =
    match s with
      | [] -> i
      | (x:: l) -> fold f l (f x i)
  
  (** val filter : (elt -> bool) -> t -> t **)
  
  let rec filter f = function
    | [] -> []
    | (x:: l) ->
        (match f x with
           | true -> (x:: (filter f l))
           | false -> filter f l)
  
  (** val for_all : (elt -> bool) -> t -> bool **)
  
  let rec for_all f = function
    | [] -> true
    | (x:: l) ->
        (match f x with
           | true -> for_all f l
           | false -> false)
  
  (** val exists_ : (elt -> bool) -> t -> bool **)
  
  let rec exists_ f = function
    | [] -> false
    | (x:: l) ->
        (match f x with
           | true -> true
           | false -> exists_ f l)
  
  (** val partition : (elt -> bool) -> t -> (t, t) ocaml_prod **)
  
  let rec partition f = function
    | [] ->  ([], [])
    | (x:: l) ->
        let  (s1, s2) = partition f l in
        (match f x with
           | true ->  (((x:: s1)), s2)
           | false ->  (s1, ((x:: s2))))
  
  (** val cardinal : t -> int **)
  
  let cardinal s =
    length s
  
  (** val elements : t -> elt list **)
  
  let elements x =
    x
  
  (** val min_elt : t -> elt option **)
  
  let min_elt = function
    | [] -> None
    | (x:: l) -> Some x
  
  (** val max_elt : t -> elt option **)
  
  let rec max_elt = function
    | [] -> None
    | (x:: l) ->
        (match l with
           | [] -> Some x
           | (e:: l0) -> max_elt l)
  
  (** val choose : t -> elt option **)
  
  let choose s =
    min_elt s
  
  (** val compare : t -> t -> t coq_Compare **)
  
  let rec compare l s' =
    match l with
      | [] ->
          (match s' with
             | [] -> EQ
             | (e:: l0) -> LT)
      | (a:: l0) ->
          (match s' with
             | [] -> GT
             | (a':: l') ->
                 (match X.compare a a' with
                    | LT -> LT
                    | EQ ->
                        (match compare l0 l' with
                           | LT -> LT
                           | EQ -> EQ
                           | GT -> GT)
                    | GT -> GT))
 end

module Make = 
 functor (X:OrderedType.OrderedType) ->
 struct 
  module Raw = Raw(X)
  
  module E = X
  
  type slist =
    Raw.t
    (* singleton inductive, whose constructor was Build_slist *)
  
  (** val slist_rect : (Raw.t -> __ -> 'a1) -> slist -> 'a1 **)
  
  let slist_rect f s =
    f s __
  
  (** val slist_rec : (Raw.t -> __ -> 'a1) -> slist -> 'a1 **)
  
  let slist_rec f s =
    f s __
  
  (** val this : slist -> Raw.t **)
  
  let this s =
    s
  
  type t = slist
  
  type elt = E.t
  
  (** val mem : elt -> t -> bool **)
  
  let mem x s =
    Raw.mem x (this s)
  
  (** val add : elt -> t -> t **)
  
  let add x s =
    Raw.add x (this s)
  
  (** val remove : elt -> t -> t **)
  
  let remove x s =
    Raw.remove x (this s)
  
  (** val singleton : elt -> t **)
  
  let singleton x =
    Raw.singleton x
  
  (** val union : t -> t -> t **)
  
  let union s s' =
    Raw.union (this s) (this s')
  
  (** val inter : t -> t -> t **)
  
  let inter s s' =
    Raw.inter (this s) (this s')
  
  (** val diff : t -> t -> t **)
  
  let diff s s' =
    Raw.diff (this s) (this s')
  
  (** val equal : t -> t -> bool **)
  
  let equal s s' =
    Raw.equal (this s) (this s')
  
  (** val subset : t -> t -> bool **)
  
  let subset s s' =
    Raw.subset (this s) (this s')
  
  (** val empty : t **)
  
  let empty =
    Raw.empty
  
  (** val is_empty : t -> bool **)
  
  let is_empty s =
    Raw.is_empty (this s)
  
  (** val elements : t -> elt list **)
  
  let elements s =
    Raw.elements (this s)
  
  (** val min_elt : t -> elt option **)
  
  let min_elt s =
    Raw.min_elt (this s)
  
  (** val max_elt : t -> elt option **)
  
  let max_elt s =
    Raw.max_elt (this s)
  
  (** val choose : t -> elt option **)
  
  let choose s =
    Raw.choose (this s)
  
  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)
  
  let fold f s x =
    Raw.fold f (this s) x
  
  (** val cardinal : t -> int **)
  
  let cardinal s =
    Raw.cardinal (this s)
  
  (** val filter : (elt -> bool) -> t -> t **)
  
  let filter f s =
    Raw.filter f (this s)
  
  (** val for_all : (elt -> bool) -> t -> bool **)
  
  let for_all f s =
    Raw.for_all f (this s)
  
  (** val exists_ : (elt -> bool) -> t -> bool **)
  
  let exists_ f s =
    Raw.exists_ f (this s)
  
  (** val partition : (elt -> bool) -> t -> (t, t) ocaml_prod **)
  
  let partition f s =
    let p = Raw.partition f (this s) in  ((fst p), (snd p))
  
  (** val compare : t -> t -> t coq_Compare **)
  
  let compare s s' =
    match Raw.compare (this s) (this s') with
      | LT -> LT
      | EQ -> EQ
      | GT -> GT
 end


