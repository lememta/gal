open Compare_dec
open Datatypes
open Coq_list
open OrderedType
open Peano_dec
open Specif

module type ArrowType = 
 sig 
  type source 
  
  type target 
  
  val apply : source -> target
 end

module type DataType = 
 sig 
  type coq_type 
  
  val eq : coq_type -> coq_type -> bool
 end

module Unit = 
 struct 
  type coq_type = unit
  
  (** val eq : coq_type -> coq_type -> bool **)
  
  let eq x y =
    true
 end

module type PreOrderType = 
 sig 
  module Data : 
   DataType
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module type TotalPreOrderType = 
 sig 
  module Data : 
   DataType
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module NaturalTotalPreOrder = 
 struct 
  module Data = Unit
  
  type coq_type = int
  
  (** val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_opt_dec d x y =
    eq_nat_dec x y
  
  (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_dec d x y =
    eq_nat_dec x y
  
  (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let dec d x y =
    le_gt_dec x y
  
  (** val total : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let total d x y =
    le_ge_dec x y
 end

module PriorityPreOrder = 
 struct 
  module Data = Unit
  
  type coq_type = int option
  
  (** val le_option_dec : coq_type -> coq_type -> bool **)
  
  let le_option_dec x y =
    match x with
      | Some n ->
          (match y with
             | Some n0 -> le_lt_dec n n0
             | None -> false)
      | None -> true
  
  (** val le_option_total : coq_type -> coq_type -> bool **)
  
  let le_option_total x y =
    match x with
      | Some n ->
          (match y with
             | Some n0 -> le_lt_dec n n0
             | None -> false)
      | None -> true
  
  (** val eq_option_dec : coq_type -> coq_type -> bool **)
  
  let eq_option_dec x y =
    match x with
      | Some n ->
          (match y with
             | Some n0 -> eq_nat_dec n n0
             | None -> false)
      | None -> (match y with
                   | Some n -> false
                   | None -> true)
  
  (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_dec d x y =
    eq_option_dec x y
  
  (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let dec d x y =
    match x with
      | Some n ->
          (match y with
             | Some n0 -> le_lt_dec n n0
             | None -> false)
      | None -> true
  
  (** val total : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let total d x y =
    match x with
      | Some n ->
          (match y with
             | Some n0 -> le_lt_dec n n0
             | None -> false)
      | None -> true
 end

module PreOrderCartesianProduct = 
 functor (O1:PreOrderType) ->
 functor (O2:sig 
  module Data : 
   sig 
    type coq_type = O1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 struct 
  module Data = O1.Data
  
  type coq_type = (O1.coq_type, O2.coq_type) ocaml_prod
  
  (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let dec d e1 e2 =
    let  (t0, t1) = e1 in
    let  (t2, t3) = e2 in
    (match O1.dec d t0 t2 with
       | true -> O2.dec d t1 t3
       | false -> false)
  
  (** val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_opt_dec d x y =
    let  (t0, t1) = x in
    let  (t2, t3) = y in
    (match O1.eq_dec d t0 t2 with
       | true -> O2.eq_dec d t1 t3
       | false -> false)
  
  (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_dec d x y =
    eq_opt_dec d x y
 end

module PreOrderLexicographicProduct = 
 functor (Coq_ord1:PreOrderType) ->
 functor (Coq_ord2:sig 
  module Data : 
   sig 
    type coq_type = Coq_ord1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 struct 
  module Data = Coq_ord1.Data
  
  type coq_type = (Coq_ord1.coq_type, Coq_ord2.coq_type) ocaml_prod
  
  (** val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_opt_dec d x y =
    let  (t0, t1) = x in
    let  (t2, t3) = y in
    (match Coq_ord1.eq_dec d t0 t2 with
       | true -> Coq_ord2.eq_dec d t1 t3
       | false -> false)
  
  (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_dec d x y =
    eq_opt_dec d x y
  
  (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let dec d x y =
    let  (t0, t1) = x in
    let  (t2, t3) = y in
    (match Coq_ord1.dec d t0 t2 with
       | true ->
           (match Coq_ord2.dec d t1 t3 with
              | true -> true
              | false ->
                  (match Coq_ord1.dec d t2 t0 with
                     | true -> false
                     | false -> true))
       | false -> false)
 end

module TotalPreOrderLexicographicProduct = 
 functor (Coq_ord1:TotalPreOrderType) ->
 functor (Coq_ord2:sig 
  module Data : 
   sig 
    type coq_type = Coq_ord1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 struct 
  module L = PreOrderLexicographicProduct(Coq_ord1)(Coq_ord2)
  
  module Data = L.Data
  
  type coq_type = L.coq_type
  
  (** val dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool **)
  
  let dec d x y =
    L.dec d x y
  
  (** val eq_dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool **)
  
  let eq_dec d x y =
    L.eq_dec d x y
  
  (** val total : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool **)
  
  let total d x y =
    let  (t0, t1) = x in
    let  (t2, t3) = y in
    (match Coq_ord1.total d t0 t2 with
       | true ->
           (match Coq_ord2.total d t1 t3 with
              | true -> true
              | false ->
                  (match Coq_ord1.dec d t2 t0 with
                     | true -> false
                     | false -> true))
       | false ->
           (match Coq_ord2.total d t1 t3 with
              | true -> Coq_ord1.dec d t0 t2
              | false -> false))
 end

module PreOrderLexicographicSequence = 
 functor (Coq_ord:PreOrderType) ->
 struct 
  module Data = Coq_ord.Data
  
  type coq_type = Coq_ord.coq_type list
  
  (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let rec dec d x y =
    match x with
      | [] -> true
      | (a:: l) ->
          (match y with
             | [] -> false
             | (t0:: y0) ->
                 (match Coq_ord.dec d a t0 with
                    | true ->
                        (match Coq_ord.dec d t0 a with
                           | true -> dec d l y0
                           | false -> true)
                    | false -> false))
  
  (** val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let rec eq_opt_dec d x y =
    match x with
      | [] ->
          (match y with
             | [] -> true
             | (t0:: y0) -> false)
      | (a:: l) ->
          (match y with
             | [] -> false
             | (t0:: y0) ->
                 (match Coq_ord.eq_dec d a t0 with
                    | true -> eq_opt_dec d l y0
                    | false -> false))
  
  (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_dec d x y =
    eq_opt_dec d x y
 end

module TotalPreOrderLexicographicSequence = 
 functor (Coq_ord:TotalPreOrderType) ->
 struct 
  module L = PreOrderLexicographicSequence(Coq_ord)
  
  module Data = L.Data
  
  type coq_type = L.coq_type
  
  (** val dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool **)
  
  let dec d x y =
    L.dec d x y
  
  (** val eq_dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool **)
  
  let eq_dec d x y =
    L.eq_dec d x y
  
  (** val total : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool **)
  
  let rec total d x y =
    match x with
      | [] -> true
      | (a:: l) ->
          (match y with
             | [] -> false
             | (t0:: y0) ->
                 (match Coq_ord.dec d a t0 with
                    | true ->
                        (match Coq_ord.dec d t0 a with
                           | true -> total d l y0
                           | false -> true)
                    | false ->
                        (match Coq_ord.dec d t0 a with
                           | true -> false
                           | false -> assert false (* absurd case *))))
 end

module ReversePreOrder = 
 functor (Coq_preOrder:PreOrderType) ->
 struct 
  module Data = Coq_preOrder.Data
  
  type coq_type = Coq_preOrder.coq_type
  
  (** val dec : Coq_preOrder.Data.coq_type -> Coq_preOrder.coq_type ->
                Coq_preOrder.coq_type -> bool **)
  
  let dec d x y =
    Coq_preOrder.dec d y x
  
  (** val eq_dec : Coq_preOrder.Data.coq_type -> Coq_preOrder.coq_type ->
                   Coq_preOrder.coq_type -> bool **)
  
  let eq_dec d x y =
    Coq_preOrder.eq_dec d y x
 end

module ReverseTotalPreOrder = 
 functor (Coq_totalOrder:TotalPreOrderType) ->
 struct 
  module L = ReversePreOrder(Coq_totalOrder)
  
  module Data = L.Data
  
  type coq_type = L.coq_type
  
  (** val dec : Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
                Coq_totalOrder.coq_type -> bool **)
  
  let dec d x y =
    L.dec d x y
  
  (** val eq_dec : Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
                   Coq_totalOrder.coq_type -> bool **)
  
  let eq_dec d x y =
    L.eq_dec d x y
  
  (** val total : Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
                  Coq_totalOrder.coq_type -> bool **)
  
  let total d x y =
    Coq_totalOrder.total d y x
 end

module InverseImagePreOrder = 
 functor (A:ArrowType) ->
 functor (Coq_preOrder:sig 
  module Data : 
   DataType
  
  type coq_type = A.target
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 struct 
  module Data = Coq_preOrder.Data
  
  type coq_type = A.source
  
  (** val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_opt_dec d x y =
    Coq_preOrder.eq_dec d (A.apply x) (A.apply y)
  
  (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let eq_dec d x y =
    eq_opt_dec d x y
  
  (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
  
  let dec d x y =
    Coq_preOrder.dec d (A.apply x) (A.apply y)
 end

module InverseImageTotalPreOrder = 
 functor (A:ArrowType) ->
 functor (Coq_totalPreOrder:sig 
  module Data : 
   DataType
  
  type coq_type = A.target
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 struct 
  module I = InverseImagePreOrder(A)(Coq_totalPreOrder)
  
  module Data = I.Data
  
  type coq_type = I.coq_type
  
  (** val eq_dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool **)
  
  let eq_dec d x y =
    I.eq_dec d x y
  
  (** val dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool **)
  
  let dec d x y =
    I.dec d x y
  
  (** val total : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool **)
  
  let total d x y =
    Coq_totalPreOrder.total d (A.apply x) (A.apply y)
 end

module type LatticeType = 
 sig 
  module PreOrder : 
   PreOrderType
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
 end

module LatticeCartesianProduct = 
 functor (L1:LatticeType) ->
 functor (L2:sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type = L1.PreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type 
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
 end) ->
 struct 
  module PreOrder = PreOrderCartesianProduct(L1.PreOrder)(L2.PreOrder)
  
  (** val bot : (L1.PreOrder.coq_type, L2.PreOrder.coq_type) ocaml_prod **)
  
  let bot =
     (L1.bot, L2.bot)
  
  (** val top : (L1.PreOrder.coq_type, L2.PreOrder.coq_type) ocaml_prod **)
  
  let top =
     (L1.top, L2.top)
  
  (** val join : PreOrder.Data.coq_type -> PreOrder.coq_type ->
                 PreOrder.coq_type -> PreOrder.coq_type **)
  
  let join d x y =
    let  (x1, x2) = x in
    let  (y1, y2) = y in  ((L1.join d x1 y1), (L2.join d x2 y2))
  
  (** val meet : PreOrder.Data.coq_type -> PreOrder.coq_type ->
                 PreOrder.coq_type -> PreOrder.coq_type **)
  
  let meet d x y =
    let  (x1, x2) = x in
    let  (y1, y2) = y in  ((L1.meet d x1 y1), (L2.meet d x2 y2))
 end

module TaggedType = 
 struct 
  type taggedNumbers =
    | Call of int
    | Return of int
  
  (** val taggedNumbers_rect : (int -> 'a1) -> (int -> 'a1) -> taggedNumbers
                               -> 'a1 **)
  
  let taggedNumbers_rect f f0 = function
    | Call x -> f x
    | Return x -> f0 x
  
  (** val taggedNumbers_rec : (int -> 'a1) -> (int -> 'a1) -> taggedNumbers
                              -> 'a1 **)
  
  let taggedNumbers_rec f f0 = function
    | Call x -> f x
    | Return x -> f0 x
  
  type t = taggedNumbers
 end

module OrderedTaggedNumbers = 
 struct 
  type t = TaggedType.t
  
  (** val compare : t -> t -> t coq_Compare **)
  
  let compare x y =
    match x with
      | TaggedType.Call n ->
          (match y with
             | TaggedType.Call n0 ->
                 (match lt_eq_lt_dec n n0 with
                    | Some s -> (match s with
                                   | true -> LT
                                   | false -> EQ)
                    | None -> GT)
             | TaggedType.Return n0 -> LT)
      | TaggedType.Return n ->
          (match y with
             | TaggedType.Call n0 -> GT
             | TaggedType.Return n0 ->
                 (match lt_eq_lt_dec n n0 with
                    | Some s -> (match s with
                                   | true -> LT
                                   | false -> EQ)
                    | None -> GT))
 end

module OrderedNatural = 
 struct 
  type t = int
  
  (** val compare : t -> t -> t coq_Compare **)
  
  let compare x y =
    match lt_eq_lt_dec x y with
      | Some s -> (match s with
                     | true -> LT
                     | false -> EQ)
      | None -> GT
 end

module OrderedTypeLexicographicProduct = 
 functor (A:OrderedType.OrderedType) ->
 functor (B:OrderedType.OrderedType) ->
 struct 
  type t = (A.t, B.t) ocaml_prod
  
  (** val compare : t -> t -> t coq_Compare **)
  
  let compare x y =
    match A.compare (fst x) (fst y) with
      | LT -> LT
      | EQ ->
          (match B.compare (snd x) (snd y) with
             | LT -> LT
             | EQ -> EQ
             | GT -> GT)
      | GT -> GT
 end


