open Datatypes
open Domain
open Coq_list
open OrderedType
open Specif

module type FiniteSetType = 
 sig 
  module FiniteSet : 
   sig 
    module E : 
     sig 
      type t = int
      
      val compare : t -> t -> t coq_Compare
     end
    
    type elt = E.t
    
    type t 
    
    val empty : t
    
    val is_empty : t -> bool
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val singleton : elt -> t
    
    val remove : elt -> t -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val compare : t -> t -> t coq_Compare
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val filter : (elt -> bool) -> t -> t
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val cardinal : t -> int
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
   end
  
  val carrier : FiniteSet.t
 end

module type FiniteSetTaggedType = 
 sig 
  module FiniteSet : 
   sig 
    module E : 
     sig 
      type t = Domain.TaggedType.t
      
      val compare : t -> t -> t coq_Compare
     end
    
    type elt = E.t
    
    type t 
    
    val empty : t
    
    val is_empty : t -> bool
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val singleton : elt -> t
    
    val remove : elt -> t -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val compare : t -> t -> t coq_Compare
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val filter : (elt -> bool) -> t -> t
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val cardinal : t -> int
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
   end
  
  val carrier : FiniteSet.t
 end

module FinitePowerSetLattice = 
 functor (S:FiniteSetTaggedType) ->
 struct 
  module PreOrder = 
   struct 
    module Data = Domain.Unit
    
    type coq_type = S.FiniteSet.t
    
    (** val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
    
    let eq_opt_dec d x y =
      match S.FiniteSet.equal x y with
        | true -> true
        | false -> false
    
    (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
    
    let eq_dec d x y =
      match S.FiniteSet.equal x y with
        | true -> true
        | false -> false
    
    (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
    
    let dec d x y =
      match S.FiniteSet.subset x y with
        | true -> true
        | false -> false
   end
  
  (** val bot : PreOrder.coq_type **)
  
  let bot =
    S.FiniteSet.empty
  
  (** val top : PreOrder.coq_type **)
  
  let top =
    S.carrier
  
  (** val join : PreOrder.Data.coq_type -> PreOrder.coq_type ->
                 PreOrder.coq_type -> PreOrder.coq_type **)
  
  let join h h0 h1 =
    S.FiniteSet.union h0 h1
  
  (** val meet : PreOrder.Data.coq_type -> PreOrder.coq_type ->
                 PreOrder.coq_type -> PreOrder.coq_type **)
  
  let meet h h0 h1 =
    S.FiniteSet.inter h0 h1
  
  (** val singleton : S.FiniteSet.elt -> PreOrder.coq_type **)
  
  let singleton h =
    S.FiniteSet.singleton h
 end

