open Datatypes
open Peano_dec
open Specif

type coq_BlockKindType =
  | Sequential of int * int
  | Combinatorial of int * int

val coq_BlockKindType_rect :
  (int -> int -> 'a1) -> (int -> int -> 'a1) -> coq_BlockKindType -> 'a1

val coq_BlockKindType_rec :
  (int -> int -> 'a1) -> (int -> int -> 'a1) -> coq_BlockKindType -> 'a1

val dec_block_op : coq_BlockKindType -> coq_BlockKindType -> bool

