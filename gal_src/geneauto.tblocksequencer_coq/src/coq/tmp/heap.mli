open Datatypes
open Coq_list
open Multiset
open Peano
open Sorting
open Specif

type __ = Obj.t

type 'a coq_Tree =
  | Tree_Leaf
  | Tree_Node of 'a * 'a coq_Tree * 'a coq_Tree

val coq_Tree_rect :
  'a2 -> ('a1 -> 'a1 coq_Tree -> 'a2 -> 'a1 coq_Tree -> 'a2 -> 'a2) -> 'a1
  coq_Tree -> 'a2

val coq_Tree_rec :
  'a2 -> ('a1 -> 'a1 coq_Tree -> 'a2 -> 'a1 coq_Tree -> 'a2 -> 'a2) -> 'a1
  coq_Tree -> 'a2

val is_heap_rec :
  'a2 -> ('a1 -> 'a1 coq_Tree -> 'a1 coq_Tree -> __ -> __ -> __ -> 'a2 -> __
  -> 'a2 -> 'a2) -> 'a1 coq_Tree -> 'a2

val contents : ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> 'a1 multiset

type 'a insert_spec =
  'a coq_Tree
  (* singleton inductive, whose constructor was insert_exist *)

val insert_spec_rect :
  ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 coq_Tree -> ('a1 coq_Tree -> __ -> __ ->
  __ -> 'a2) -> 'a1 insert_spec -> 'a2

val insert_spec_rec :
  ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 coq_Tree -> ('a1 coq_Tree -> __ -> __ ->
  __ -> 'a2) -> 'a1 insert_spec -> 'a2

val insert :
  ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> 'a1 -> 'a1
  insert_spec

type 'a build_heap =
  'a coq_Tree
  (* singleton inductive, whose constructor was heap_exist *)

val build_heap_rect :
  ('a1 -> 'a1 -> bool) -> 'a1 list -> ('a1 coq_Tree -> __ -> __ -> 'a2) ->
  'a1 build_heap -> 'a2

val build_heap_rec :
  ('a1 -> 'a1 -> bool) -> 'a1 list -> ('a1 coq_Tree -> __ -> __ -> 'a2) ->
  'a1 build_heap -> 'a2

val list_to_heap :
  ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 build_heap

type 'a flat_spec =
  'a list
  (* singleton inductive, whose constructor was flat_exist *)

val flat_spec_rect :
  ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> ('a1 list -> __ -> __ -> __ -> 'a2)
  -> 'a1 flat_spec -> 'a2

val flat_spec_rec :
  ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> ('a1 list -> __ -> __ -> __ -> 'a2)
  -> 'a1 flat_spec -> 'a2

val heap_to_list :
  ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 coq_Tree -> 'a1
  flat_spec

val treesort :
  ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list sig2

