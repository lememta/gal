open Datatypes
open Peano_dec
open Specif

module MakeEnvironment = 
 functor (Lattice:Domain.LatticeType) ->
 struct 
  module PreOrder = 
   struct 
    module Data = 
     struct 
      type coq_type = (Lattice.PreOrder.Data.coq_type, int) ocaml_prod
      
      (** val eq : coq_type -> coq_type -> bool **)
      
      let eq x y =
        let  (eltX, sizeX) = x in
        let  (eltY, sizeY) = y in
        (match Lattice.PreOrder.Data.eq eltX eltY with
           | true -> eq_nat_dec sizeX sizeY
           | false -> false)
     end
    
    type coq_type = int -> Lattice.PreOrder.coq_type
    
    (** val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
    
    let eq_opt_dec d x y =
      let  (x0, x1) = d in
      nat_rec true (fun n h ->
        match h with
          | true -> Lattice.PreOrder.eq_dec x0 (x n) (y n)
          | false -> false) x1
    
    (** val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool **)
    
    let eq_dec d x y =
      eq_opt_dec d x y
    
    (** val dec : Data.coq_type -> coq_type -> coq_type -> bool **)
    
    let dec d e1 e2 =
      let  (x, x0) = d in
      nat_rec true (fun n h ->
        match h with
          | true -> Lattice.PreOrder.dec x (e1 n) (e2 n)
          | false -> false) x0
   end
  
  (** val bot : PreOrder.coq_type **)
  
  let bot x =
    Lattice.bot
  
  (** val top : PreOrder.coq_type **)
  
  let top x =
    Lattice.top
  
  (** val join : PreOrder.Data.coq_type -> PreOrder.coq_type ->
                 PreOrder.coq_type -> int -> Lattice.PreOrder.coq_type **)
  
  let join d e1 e2 n =
    Lattice.join (fst d) (e1 n) (e2 n)
  
  (** val meet : PreOrder.Data.coq_type -> PreOrder.coq_type ->
                 PreOrder.coq_type -> int -> Lattice.PreOrder.coq_type **)
  
  let meet d e1 e2 n =
    Lattice.meet (fst d) (e1 n) (e2 n)
 end

