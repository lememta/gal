open Datatypes
open GeneAutoLibrary
open Coq_list
open Peano_dec
open Specif

type coq_ExecutionOrderType =
  | Loop
  | Controled
  | Position of int

(** val coq_ExecutionOrderType_rect : 'a1 -> 'a1 -> (int -> 'a1) ->
                                      coq_ExecutionOrderType -> 'a1 **)

let coq_ExecutionOrderType_rect f f0 f1 = function
  | Loop -> f
  | Controled -> f0
  | Position x -> f1 x

(** val coq_ExecutionOrderType_rec : 'a1 -> 'a1 -> (int -> 'a1) ->
                                     coq_ExecutionOrderType -> 'a1 **)

let coq_ExecutionOrderType_rec f f0 f1 = function
  | Loop -> f
  | Controled -> f0
  | Position x -> f1 x

type coq_SignalType =
  | Integer
  | Boolean
  | Real
  | Vector of int * coq_SignalType
  | Matrix of int * int * coq_SignalType
  | Event
  | Top
  | Bottom

(** val coq_SignalType_rect : 'a1 -> 'a1 -> 'a1 -> (int -> coq_SignalType ->
                              'a1 -> 'a1) -> (int -> int -> coq_SignalType ->
                              'a1 -> 'a1) -> 'a1 -> 'a1 -> 'a1 ->
                              coq_SignalType -> 'a1 **)

let rec coq_SignalType_rect f f0 f1 f2 f3 f4 f5 f6 = function
  | Integer -> f
  | Boolean -> f0
  | Real -> f1
  | Vector (n, s0) -> f2 n s0 (coq_SignalType_rect f f0 f1 f2 f3 f4 f5 f6 s0)
  | Matrix (n, n0, s0) ->
      f3 n n0 s0 (coq_SignalType_rect f f0 f1 f2 f3 f4 f5 f6 s0)
  | Event -> f4
  | Top -> f5
  | Bottom -> f6

(** val coq_SignalType_rec : 'a1 -> 'a1 -> 'a1 -> (int -> coq_SignalType ->
                             'a1 -> 'a1) -> (int -> int -> coq_SignalType ->
                             'a1 -> 'a1) -> 'a1 -> 'a1 -> 'a1 ->
                             coq_SignalType -> 'a1 **)

let rec coq_SignalType_rec f f0 f1 f2 f3 f4 f5 f6 = function
  | Integer -> f
  | Boolean -> f0
  | Real -> f1
  | Vector (n, s0) -> f2 n s0 (coq_SignalType_rec f f0 f1 f2 f3 f4 f5 f6 s0)
  | Matrix (n, n0, s0) ->
      f3 n n0 s0 (coq_SignalType_rec f f0 f1 f2 f3 f4 f5 f6 s0)
  | Event -> f4
  | Top -> f5
  | Bottom -> f6

type coq_BlockBaseType = { blockKind : coq_BlockKindType;
                           blockDataInputs : int list option;
                           blockDataOutputs : int list option;
                           blockControlInputs : int list option;
                           blockControlOutputs : int list option;
                           blockUserDefinedPriority : 
                           int option; blockAssignedPriority : 
                           int; blockIndex : int }

(** val coq_BlockBaseType_rect : (coq_BlockKindType -> int list option -> int
                                 list option -> int list option -> int list
                                 option -> int option -> int -> int -> 'a1)
                                 -> coq_BlockBaseType -> 'a1 **)

let coq_BlockBaseType_rect f b =
  let { blockKind = x; blockDataInputs = x0; blockDataOutputs = x1;
    blockControlInputs = x2; blockControlOutputs = x3;
    blockUserDefinedPriority = x4; blockAssignedPriority = x5; blockIndex =
    x6 } = b
  in
  f x x0 x1 x2 x3 x4 x5 x6

(** val coq_BlockBaseType_rec : (coq_BlockKindType -> int list option -> int
                                list option -> int list option -> int list
                                option -> int option -> int -> int -> 'a1) ->
                                coq_BlockBaseType -> 'a1 **)

let coq_BlockBaseType_rec f b =
  let { blockKind = x; blockDataInputs = x0; blockDataOutputs = x1;
    blockControlInputs = x2; blockControlOutputs = x3;
    blockUserDefinedPriority = x4; blockAssignedPriority = x5; blockIndex =
    x6 } = b
  in
  f x x0 x1 x2 x3 x4 x5 x6

(** val blockKind : coq_BlockBaseType -> coq_BlockKindType **)

let blockKind x = x.blockKind

(** val blockDataInputs : coq_BlockBaseType -> int list option **)

let blockDataInputs x = x.blockDataInputs

(** val blockDataOutputs : coq_BlockBaseType -> int list option **)

let blockDataOutputs x = x.blockDataOutputs

(** val blockControlInputs : coq_BlockBaseType -> int list option **)

let blockControlInputs x = x.blockControlInputs

(** val blockControlOutputs : coq_BlockBaseType -> int list option **)

let blockControlOutputs x = x.blockControlOutputs

(** val blockUserDefinedPriority : coq_BlockBaseType -> int option **)

let blockUserDefinedPriority x = x.blockUserDefinedPriority

(** val blockAssignedPriority : coq_BlockBaseType -> int **)

let blockAssignedPriority x = x.blockAssignedPriority

(** val blockIndex : coq_BlockBaseType -> int **)

let blockIndex x = x.blockIndex

type coq_BlockExtensionType = { base : coq_BlockBaseType;
                                blockExecutionOrder : 
                                coq_ExecutionOrderType option }

(** val coq_BlockExtensionType_rect : (coq_BlockBaseType ->
                                      coq_ExecutionOrderType option -> 'a1)
                                      -> coq_BlockExtensionType -> 'a1 **)

let coq_BlockExtensionType_rect f b =
  let { base = x; blockExecutionOrder = x0 } = b in f x x0

(** val coq_BlockExtensionType_rec : (coq_BlockBaseType ->
                                     coq_ExecutionOrderType option -> 'a1) ->
                                     coq_BlockExtensionType -> 'a1 **)

let coq_BlockExtensionType_rec f b =
  let { base = x; blockExecutionOrder = x0 } = b in f x x0

(** val base : coq_BlockExtensionType -> coq_BlockBaseType **)

let base x = x.base

(** val blockExecutionOrder : coq_BlockExtensionType ->
                              coq_ExecutionOrderType option **)

let blockExecutionOrder x = x.blockExecutionOrder

type coq_DataConnexionType =
  | DataSignal of int * int

(** val coq_DataConnexionType_rect : (int -> int -> 'a1) ->
                                     coq_DataConnexionType -> 'a1 **)

let coq_DataConnexionType_rect f = function
  | DataSignal (x, x0) -> f x x0

(** val coq_DataConnexionType_rec : (int -> int -> 'a1) ->
                                    coq_DataConnexionType -> 'a1 **)

let coq_DataConnexionType_rec f = function
  | DataSignal (x, x0) -> f x x0

type coq_ControlConnexionType =
  | ControlSignal of int * int

(** val coq_ControlConnexionType_rect : (int -> int -> 'a1) ->
                                        coq_ControlConnexionType -> 'a1 **)

let coq_ControlConnexionType_rect f = function
  | ControlSignal (x, x0) -> f x x0

(** val coq_ControlConnexionType_rec : (int -> int -> 'a1) ->
                                       coq_ControlConnexionType -> 'a1 **)

let coq_ControlConnexionType_rec f = function
  | ControlSignal (x, x0) -> f x x0

type 'modelElementType coq_DiagramType = { blocks : 
                                           'modelElementType list;
                                           dataSignals : 
                                           coq_DataConnexionType list;
                                           controlSignals : 
                                           coq_ControlConnexionType list;
                                           blocksNumber : 
                                           int }

(** val coq_DiagramType_rect : ('a1 list -> coq_DataConnexionType list ->
                               coq_ControlConnexionType list -> int -> 'a2)
                               -> 'a1 coq_DiagramType -> 'a2 **)

let coq_DiagramType_rect f d =
  let { blocks = x; dataSignals = x0; controlSignals = x1; blocksNumber =
    x2 } = d
  in
  f x x0 x1 x2

(** val coq_DiagramType_rec : ('a1 list -> coq_DataConnexionType list ->
                              coq_ControlConnexionType list -> int -> 'a2) ->
                              'a1 coq_DiagramType -> 'a2 **)

let coq_DiagramType_rec f d =
  let { blocks = x; dataSignals = x0; controlSignals = x1; blocksNumber =
    x2 } = d
  in
  f x x0 x1 x2

(** val blocks : 'a1 coq_DiagramType -> 'a1 list **)

let blocks x = x.blocks

(** val dataSignals : 'a1 coq_DiagramType -> coq_DataConnexionType list **)

let dataSignals x = x.dataSignals

(** val controlSignals : 'a1 coq_DiagramType -> coq_ControlConnexionType list **)

let controlSignals x = x.controlSignals

(** val blocksNumber : 'a1 coq_DiagramType -> int **)

let blocksNumber x = x.blocksNumber

type coq_ModelElementType =
  | Block of coq_BlockBaseType
  | Diagram of coq_ModelElementType coq_DiagramType

(** val coq_ModelElementType_rect : (coq_BlockBaseType -> 'a1) ->
                                    (coq_ModelElementType coq_DiagramType ->
                                    'a1) -> coq_ModelElementType -> 'a1 **)

let coq_ModelElementType_rect f f0 = function
  | Block x -> f x
  | Diagram x -> f0 x

(** val coq_ModelElementType_rec : (coq_BlockBaseType -> 'a1) ->
                                   (coq_ModelElementType coq_DiagramType ->
                                   'a1) -> coq_ModelElementType -> 'a1 **)

let coq_ModelElementType_rec f f0 = function
  | Block x -> f x
  | Diagram x -> f0 x

type coq_ModelType = coq_ModelElementType coq_DiagramType

(** val modelElementIndex : coq_ModelElementType -> int **)

let modelElementIndex = function
  | Block block -> block.blockIndex
  | Diagram diagram -> diagram.blocksNumber

(** val blockIndexes : coq_ModelElementType list -> int list **)

let blockIndexes modelElements =
  map (fun element -> modelElementIndex element) modelElements

(** val size : coq_ModelType -> int **)

let size model =
  length model.blocks

(** val lookForBlock : int -> coq_ModelType -> coq_BlockKindType option **)

let lookForBlock index model =
  match let rec find = function
          | [] -> None
          | (x:: tl) ->
              (match eq_nat_dec (modelElementIndex x) index with
                 | true -> Some x
                 | false -> find tl)
        in find model.blocks with
    | Some m ->
        (match m with
           | Block k -> Some k.blockKind
           | Diagram d -> None)
    | None -> None

(** val isSequential : int -> coq_ModelType -> bool **)

let isSequential blockIndex0 model =
  match lookForBlock blockIndex0 model with
    | Some blockKind0 ->
        (match blockKind0 with
           | Sequential (n, n0) -> true
           | Combinatorial (n, n0) -> false)
    | None -> false

(** val dataSourceFor : coq_DataConnexionType list -> int -> int list **)

let rec dataSourceFor dataSignals0 block =
  match dataSignals0 with
    | [] -> []
    | (d:: queue) ->
        let DataSignal (src, dst) = d in
        (match eq_nat_dec block dst with
           | true -> (src:: (dataSourceFor queue block))
           | false -> dataSourceFor queue block)

(** val dataTargetFor : coq_DataConnexionType list -> int -> int list **)

let rec dataTargetFor dataSignals0 block =
  match dataSignals0 with
    | [] -> []
    | (d:: queue) ->
        let DataSignal (src, dst) = d in
        (match eq_nat_dec block src with
           | true -> (dst:: (dataTargetFor queue block))
           | false -> dataTargetFor queue block)

(** val controlTargetFor : coq_ControlConnexionType list -> int -> int list **)

let rec controlTargetFor controlSignals0 block =
  match controlSignals0 with
    | [] -> []
    | (c:: queue) ->
        let ControlSignal (src, dst) = c in
        (match eq_nat_dec block src with
           | true -> (dst:: (controlTargetFor queue block))
           | false -> controlTargetFor queue block)

(** val controlSourceFor : coq_ControlConnexionType list -> int -> int list **)

let rec controlSourceFor controlSignals0 block =
  match controlSignals0 with
    | [] -> []
    | (c:: queue) ->
        let ControlSignal (src, dst) = c in
        (match eq_nat_dec block dst with
           | true -> (src:: (controlSourceFor queue block))
           | false -> controlSourceFor queue block)

(** val annotateBlock : coq_ModelType -> coq_BlockBaseType ->
                        coq_BlockBaseType **)

let annotateBlock diagram block =
  { blockKind = block.blockKind; blockDataInputs = (Some
    (dataSourceFor diagram.dataSignals block.blockIndex)); blockDataOutputs =
    (Some (dataTargetFor diagram.dataSignals block.blockIndex));
    blockControlInputs = (Some
    (controlSourceFor diagram.controlSignals block.blockIndex));
    blockControlOutputs = (Some
    (controlTargetFor diagram.controlSignals block.blockIndex));
    blockUserDefinedPriority = block.blockUserDefinedPriority;
    blockAssignedPriority = block.blockAssignedPriority; blockIndex =
    block.blockIndex }

(** val annotateDiagram : coq_ModelType -> coq_ModelType **)

let annotateDiagram model =
  { blocks =
    (map (fun modelElement ->
      match modelElement with
        | Block block -> Block (annotateBlock model block)
        | Diagram diagram -> Diagram diagram) model.blocks); dataSignals =
    model.dataSignals; controlSignals = model.controlSignals; blocksNumber =
    model.blocksNumber }

(** val isIsolated : coq_DataConnexionType list -> coq_ControlConnexionType
                     list -> int -> bool **)

let isIsolated dataSignals0 controlSignals0 block =
  match app
          (app (dataSourceFor dataSignals0 block)
            (dataTargetFor dataSignals0 block))
          (app (controlSourceFor controlSignals0 block)
            (controlTargetFor controlSignals0 block)) with
    | [] -> true
    | (n:: l) -> false

(** val isControled : coq_ControlConnexionType list -> int -> bool **)

let isControled controlSignals0 blockIndex0 =
  match controlSourceFor controlSignals0 blockIndex0 with
    | [] -> false
    | (n:: l) -> true


