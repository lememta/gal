open Coq_list
open Specif

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val sort_rec : 'a2 -> ('a1 -> 'a1 list -> __ -> 'a2 -> __ -> 'a2) -> 'a1
                   list -> 'a2 **)

let rec sort_rec h h0 = function
  | [] -> h
  | (a:: l) -> h0 a l __ (sort_rec h h0 l) __

type 'a merge_lem =
  'a list
  (* singleton inductive, whose constructor was merge_exist *)

(** val merge_lem_rect : ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list -> ('a1
                         list -> __ -> __ -> __ -> 'a2) -> 'a1 merge_lem ->
                         'a2 **)

let merge_lem_rect eqA_dec l1 l2 f m =
  f m __ __ __

(** val merge_lem_rec : ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list -> ('a1
                        list -> __ -> __ -> __ -> 'a2) -> 'a1 merge_lem ->
                        'a2 **)

let merge_lem_rec eqA_dec l1 l2 f m =
  f m __ __ __

(** val merge : ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 list ->
                'a1 list -> 'a1 merge_lem **)

let rec merge leA_dec eqA_dec l1 l2 =
  match l1 with
    | [] -> l2
    | (a:: l) ->
        let rec f = function
          | [] -> (a:: l)
          | (a0:: l3) ->
              (match leA_dec a a0 with
                 | true -> (a::
                     (merge leA_dec eqA_dec l ((a0:: l3))))
                 | false -> (a0:: (f l3)))
        in f l2


