open Datatypes
open List
open Specif

(** val option_hd : 'a1 list -> 'a1 option **)

let option_hd = (fun l -> match l with [] -> None | t::_ -> Some t)

type 'a coq_CacheType =
  int -> 'a
  (* singleton inductive, whose constructor was Cache *)

(** val coq_CacheType_rect : ((int -> 'a1) -> 'a2) -> 'a1 coq_CacheType ->
                             'a2 **)

let coq_CacheType_rect f c =
  f c

(** val coq_CacheType_rec : ((int -> 'a1) -> 'a2) -> 'a1 coq_CacheType -> 'a2 **)

let coq_CacheType_rec f c =
  f c

(** val cache : int -> 'a1 -> (int -> 'a1) -> 'a1 coq_CacheType **)

let cache = (fun s d f -> let t = Array.init s f in fun x -> (assert (x >= 0); if x < s then t.(x) else d))

