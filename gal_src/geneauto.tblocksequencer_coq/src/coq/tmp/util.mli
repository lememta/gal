open Datatypes
open List
open Specif

val option_hd : 'a1 list -> 'a1 option

type 'a coq_CacheType =
  int -> 'a
  (* singleton inductive, whose constructor was Cache *)

val coq_CacheType_rect : ((int -> 'a1) -> 'a2) -> 'a1 coq_CacheType -> 'a2

val coq_CacheType_rec : ((int -> 'a1) -> 'a2) -> 'a1 coq_CacheType -> 'a2

val cache : int -> 'a1 -> (int -> 'a1) -> 'a1 coq_CacheType

