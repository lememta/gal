open Datatypes
open Peano
open Specif

type 'a multiset =
  'a -> int
  (* singleton inductive, whose constructor was Bag *)

(** val multiset_rect : (('a1 -> int) -> 'a2) -> 'a1 multiset -> 'a2 **)

let multiset_rect f m =
  f m

(** val multiset_rec : (('a1 -> int) -> 'a2) -> 'a1 multiset -> 'a2 **)

let multiset_rec f m =
  f m

(** val coq_EmptyBag : 'a1 multiset **)

let coq_EmptyBag a =
  0

(** val coq_SingletonBag : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 multiset **)

let coq_SingletonBag aeq_dec a a' =
  match aeq_dec a a' with
    | true -> succ 0
    | false -> 0

(** val multiplicity : 'a1 multiset -> 'a1 -> int **)

let multiplicity m a =
  m a

(** val munion : 'a1 multiset -> 'a1 multiset -> 'a1 multiset **)

let munion m1 m2 a =
  plus (m1 a) (m2 a)

