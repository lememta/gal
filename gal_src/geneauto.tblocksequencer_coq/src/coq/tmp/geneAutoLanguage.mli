open Datatypes
open GeneAutoLibrary
open Coq_list
open Peano_dec
open Specif

type coq_ExecutionOrderType =
  | Loop
  | Controled
  | Position of int

val coq_ExecutionOrderType_rect :
  'a1 -> 'a1 -> (int -> 'a1) -> coq_ExecutionOrderType -> 'a1

val coq_ExecutionOrderType_rec :
  'a1 -> 'a1 -> (int -> 'a1) -> coq_ExecutionOrderType -> 'a1

type coq_SignalType =
  | Integer
  | Boolean
  | Real
  | Vector of int * coq_SignalType
  | Matrix of int * int * coq_SignalType
  | Event
  | Top
  | Bottom

val coq_SignalType_rect :
  'a1 -> 'a1 -> 'a1 -> (int -> coq_SignalType -> 'a1 -> 'a1) -> (int -> int
  -> coq_SignalType -> 'a1 -> 'a1) -> 'a1 -> 'a1 -> 'a1 -> coq_SignalType ->
  'a1

val coq_SignalType_rec :
  'a1 -> 'a1 -> 'a1 -> (int -> coq_SignalType -> 'a1 -> 'a1) -> (int -> int
  -> coq_SignalType -> 'a1 -> 'a1) -> 'a1 -> 'a1 -> 'a1 -> coq_SignalType ->
  'a1

type coq_BlockBaseType = { blockKind : coq_BlockKindType;
                           blockDataInputs : int list option;
                           blockDataOutputs : int list option;
                           blockControlInputs : int list option;
                           blockControlOutputs : int list option;
                           blockUserDefinedPriority : 
                           int option; blockAssignedPriority : 
                           int; blockIndex : int }

val coq_BlockBaseType_rect :
  (coq_BlockKindType -> int list option -> int list option -> int list option
  -> int list option -> int option -> int -> int -> 'a1) -> coq_BlockBaseType
  -> 'a1

val coq_BlockBaseType_rec :
  (coq_BlockKindType -> int list option -> int list option -> int list option
  -> int list option -> int option -> int -> int -> 'a1) -> coq_BlockBaseType
  -> 'a1

val blockKind : coq_BlockBaseType -> coq_BlockKindType

val blockDataInputs : coq_BlockBaseType -> int list option

val blockDataOutputs : coq_BlockBaseType -> int list option

val blockControlInputs : coq_BlockBaseType -> int list option

val blockControlOutputs : coq_BlockBaseType -> int list option

val blockUserDefinedPriority : coq_BlockBaseType -> int option

val blockAssignedPriority : coq_BlockBaseType -> int

val blockIndex : coq_BlockBaseType -> int

type coq_BlockExtensionType = { base : coq_BlockBaseType;
                                blockExecutionOrder : 
                                coq_ExecutionOrderType option }

val coq_BlockExtensionType_rect :
  (coq_BlockBaseType -> coq_ExecutionOrderType option -> 'a1) ->
  coq_BlockExtensionType -> 'a1

val coq_BlockExtensionType_rec :
  (coq_BlockBaseType -> coq_ExecutionOrderType option -> 'a1) ->
  coq_BlockExtensionType -> 'a1

val base : coq_BlockExtensionType -> coq_BlockBaseType

val blockExecutionOrder :
  coq_BlockExtensionType -> coq_ExecutionOrderType option

type coq_DataConnexionType =
  | DataSignal of int * int

val coq_DataConnexionType_rect :
  (int -> int -> 'a1) -> coq_DataConnexionType -> 'a1

val coq_DataConnexionType_rec :
  (int -> int -> 'a1) -> coq_DataConnexionType -> 'a1

type coq_ControlConnexionType =
  | ControlSignal of int * int

val coq_ControlConnexionType_rect :
  (int -> int -> 'a1) -> coq_ControlConnexionType -> 'a1

val coq_ControlConnexionType_rec :
  (int -> int -> 'a1) -> coq_ControlConnexionType -> 'a1

type 'modelElementType coq_DiagramType = { blocks : 
                                           'modelElementType list;
                                           dataSignals : 
                                           coq_DataConnexionType list;
                                           controlSignals : 
                                           coq_ControlConnexionType list;
                                           blocksNumber : 
                                           int }

val coq_DiagramType_rect :
  ('a1 list -> coq_DataConnexionType list -> coq_ControlConnexionType list ->
  int -> 'a2) -> 'a1 coq_DiagramType -> 'a2

val coq_DiagramType_rec :
  ('a1 list -> coq_DataConnexionType list -> coq_ControlConnexionType list ->
  int -> 'a2) -> 'a1 coq_DiagramType -> 'a2

val blocks : 'a1 coq_DiagramType -> 'a1 list

val dataSignals : 'a1 coq_DiagramType -> coq_DataConnexionType list

val controlSignals : 'a1 coq_DiagramType -> coq_ControlConnexionType list

val blocksNumber : 'a1 coq_DiagramType -> int

type coq_ModelElementType =
  | Block of coq_BlockBaseType
  | Diagram of coq_ModelElementType coq_DiagramType

val coq_ModelElementType_rect :
  (coq_BlockBaseType -> 'a1) -> (coq_ModelElementType coq_DiagramType -> 'a1)
  -> coq_ModelElementType -> 'a1

val coq_ModelElementType_rec :
  (coq_BlockBaseType -> 'a1) -> (coq_ModelElementType coq_DiagramType -> 'a1)
  -> coq_ModelElementType -> 'a1

type coq_ModelType = coq_ModelElementType coq_DiagramType

val modelElementIndex : coq_ModelElementType -> int

val blockIndexes : coq_ModelElementType list -> int list

val size : coq_ModelType -> int

val lookForBlock : int -> coq_ModelType -> coq_BlockKindType option

val isSequential : int -> coq_ModelType -> bool

val dataSourceFor : coq_DataConnexionType list -> int -> int list

val dataTargetFor : coq_DataConnexionType list -> int -> int list

val controlTargetFor : coq_ControlConnexionType list -> int -> int list

val controlSourceFor : coq_ControlConnexionType list -> int -> int list

val annotateBlock : coq_ModelType -> coq_BlockBaseType -> coq_BlockBaseType

val annotateDiagram : coq_ModelType -> coq_ModelType

val isIsolated :
  coq_DataConnexionType list -> coq_ControlConnexionType list -> int -> bool

val isControled : coq_ControlConnexionType list -> int -> bool

