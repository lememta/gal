open Bool
open Compare_dec
open Datatypes
open Domain
open EqNat
open GeneAutoLanguage
open GeneAutoLibrary
open Heap
open Coq_list
open OrderedType
open Specif
open Util

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val model : coq_ModelType **)

let model = WrapperReader.model

module NaturalFiniteSet = FSetList.Make(Domain.OrderedNatural)

module DependencySet = 
 struct 
  module FiniteSet = NaturalFiniteSet
  
  (** val makeSetFromList : int list -> FiniteSet.t **)
  
  let rec makeSetFromList = function
    | [] -> FiniteSet.empty
    | (head:: queue) -> FiniteSet.add head (makeSetFromList queue)
  
  (** val carrier : FiniteSet.t **)
  
  let carrier =
    makeSetFromList (blockIndexes model.blocks)
 end

module TaggedFiniteSet = FSetList.Make(Domain.OrderedTaggedNumbers)

module DependencySetTagged = 
 struct 
  module FiniteSet = TaggedFiniteSet
  
  (** val makeSetFromListTagged : Domain.TaggedType.t list -> FiniteSet.t **)
  
  let rec makeSetFromListTagged = function
    | [] -> FiniteSet.empty
    | (head:: queue) ->
        FiniteSet.add head (makeSetFromListTagged queue)
  
  (** val callList : int list -> Domain.TaggedType.t list **)
  
  let rec callList = function
    | [] -> []
    | (head:: queue) -> ((Domain.TaggedType.Call head)::
        (callList queue))
  
  (** val returnList : int list -> Domain.TaggedType.t list **)
  
  let rec returnList = function
    | [] -> []
    | (head:: queue) -> ((Domain.TaggedType.Return
        head):: (returnList queue))
  
  (** val carrier : FiniteSet.t **)
  
  let carrier =
    makeSetFromListTagged
      (app (callList (blockIndexes model.blocks))
        (returnList (blockIndexes model.blocks)))
 end

module DependencyLattice = DependenceLattice.FinitePowerSetLattice(DependencySetTagged)

module DependencyEnvironment = EnvironmentLattice.MakeEnvironment(DependencyLattice)

module EnvironmentPair = Domain.LatticeCartesianProduct(DependencyEnvironment)(DependencyEnvironment)

type 'a dec_eq = 'a -> 'a -> bool

(** val mergeDependencies : DependencyEnvironment.PreOrder.coq_type ->
                            coq_ModelType -> int list ->
                            DependencyLattice.PreOrder.coq_type **)

let mergeDependencies dependencies diagram blockIndexSequence =
  fold_right (fun blockIndex queue ->
    DependencyLattice.join () (dependencies blockIndex) queue)
    DependencyLattice.bot blockIndexSequence

(** val makeCallListNat : int list -> Domain.TaggedType.taggedNumbers list **)

let makeCallListNat l =
  fold_right (fun t0 queue -> ((Domain.TaggedType.Call t0)::
    queue)) [] l

(** val makeReturnListNat : int list -> Domain.TaggedType.taggedNumbers list **)

let makeReturnListNat l =
  fold_right (fun t0 queue -> ((Domain.TaggedType.Return t0)::
    queue)) [] l

(** val makeSetFromList : Domain.TaggedType.t list ->
                          DependencyLattice.PreOrder.coq_type **)

let rec makeSetFromList = function
  | [] -> DependencyLattice.bot
  | (t0:: queue) ->
      DependencyLattice.join () (DependencyLattice.singleton t0)
        (makeSetFromList queue)

(** val forwardInputDependencies : coq_ModelType ->
                                   DependencyEnvironment.PreOrder.coq_type ->
                                   DependencyEnvironment.PreOrder.coq_type ->
                                   DependencyEnvironment.PreOrder.coq_type **)

let forwardInputDependencies diagram currentInputDependencies currentOutputDependencies blockIndex =
  match leb (size diagram) blockIndex with
    | true -> DependencyLattice.bot
    | false ->
        (match lookForBlock blockIndex diagram with
           | Some blockKind ->
               let controlSources =
                 controlSourceFor diagram.controlSignals blockIndex
               in
               let dataSources = dataSourceFor diagram.dataSignals blockIndex
               in
               DependencyLattice.join ()
                 (match blockKind with
                    | Sequential (n, n0) ->
                        DependencyLattice.singleton (Domain.TaggedType.Return
                          blockIndex)
                    | Combinatorial (n, n0) -> DependencyLattice.bot)
                 (DependencyLattice.join ()
                   (mergeDependencies currentInputDependencies diagram
                     controlSources)
                   (DependencyLattice.join ()
                     (mergeDependencies currentOutputDependencies diagram
                       dataSources)
                     (DependencyLattice.join ()
                       (makeSetFromList
                         (DependencySetTagged.returnList dataSources))
                       (makeSetFromList
                         (DependencySetTagged.callList controlSources)))))
           | None -> DependencyLattice.bot)

(** val forwardOutputDependencies : coq_ModelType ->
                                    DependencyEnvironment.PreOrder.coq_type
                                    ->
                                    DependencyEnvironment.PreOrder.coq_type
                                    ->
                                    DependencyEnvironment.PreOrder.coq_type **)

let forwardOutputDependencies diagram inputEnv outputEnv blockIndex =
  match leb (size diagram) blockIndex with
    | true -> DependencyLattice.bot
    | false ->
        (match lookForBlock blockIndex diagram with
           | Some blockKind ->
               let controlTargets =
                 controlTargetFor diagram.controlSignals blockIndex
               in
               DependencyLattice.join ()
                 (DependencyLattice.singleton (Domain.TaggedType.Call
                   blockIndex))
                 (DependencyLattice.join ()
                   (makeSetFromList
                     (DependencySetTagged.returnList controlTargets))
                   (DependencyLattice.join ()
                     (mergeDependencies outputEnv diagram controlTargets)
                     (match blockKind with
                        | Sequential (n, n0) ->
                            DependencyLattice.singleton
                              (Domain.TaggedType.Call blockIndex)
                        | Combinatorial (n, n0) -> inputEnv blockIndex)))
           | None -> DependencyLattice.bot)

(** val forwardDependencies : coq_ModelType ->
                              EnvironmentPair.PreOrder.coq_type ->
                              EnvironmentPair.PreOrder.coq_type **)

let forwardDependencies diagram = function
  |  (currentInputDependencies, currentOutputDependencies) -> 
      ((cache (size diagram) DependencyLattice.bot
         (forwardInputDependencies diagram currentInputDependencies
           currentOutputDependencies)),
      (cache (size diagram) DependencyLattice.bot
        (forwardOutputDependencies diagram currentInputDependencies
          currentOutputDependencies)))

type decidable = bool

(** val coq_Scheduler_rec_F : (coq_ModelType ->
                              EnvironmentPair.PreOrder.coq_type ->
                              EnvironmentPair.PreOrder.coq_type -> __ -> __
                              -> EnvironmentPair.PreOrder.coq_type) ->
                              coq_ModelType ->
                              EnvironmentPair.PreOrder.coq_type ->
                              EnvironmentPair.PreOrder.coq_type ->
                              EnvironmentPair.PreOrder.coq_type **)

let coq_Scheduler_rec_F scheduler_rec diagram dependencies nextDependencies =
  match EnvironmentPair.PreOrder.dec ( ((), (size diagram))) nextDependencies
          dependencies with
    | true -> dependencies
    | false ->
        scheduler_rec diagram nextDependencies
          (forwardDependencies diagram nextDependencies) __ __

(** val coq_Scheduler_rec_terminate : coq_ModelType ->
                                      EnvironmentPair.PreOrder.coq_type ->
                                      EnvironmentPair.PreOrder.coq_type ->
                                      EnvironmentPair.PreOrder.coq_type **)

let rec coq_Scheduler_rec_terminate diagram dependencies nextDependencies =
  match EnvironmentPair.PreOrder.dec ( ((), (size diagram))) nextDependencies
          dependencies with
    | true -> dependencies
    | false ->
        coq_Scheduler_rec_terminate diagram nextDependencies
          (forwardDependencies diagram nextDependencies)

(** val coq_Scheduler_rec : coq_ModelType ->
                            EnvironmentPair.PreOrder.coq_type ->
                            EnvironmentPair.PreOrder.coq_type ->
                            EnvironmentPair.PreOrder.coq_type **)

let coq_Scheduler_rec x x0 x1 =
  coq_Scheduler_rec_terminate x x0 x1

type coq_R_Scheduler_rec =
  | R_Scheduler_rec_0 of EnvironmentPair.PreOrder.coq_type
  * EnvironmentPair.PreOrder.coq_type
  | R_Scheduler_rec_1 of EnvironmentPair.PreOrder.coq_type
  * EnvironmentPair.PreOrder.coq_type * EnvironmentPair.PreOrder.coq_type
  * coq_R_Scheduler_rec

(** val coq_R_Scheduler_rec_rect : coq_ModelType ->
                                   (EnvironmentPair.PreOrder.coq_type ->
                                   EnvironmentPair.PreOrder.coq_type -> __ ->
                                   __ -> __ -> __ -> 'a1) ->
                                   (EnvironmentPair.PreOrder.coq_type ->
                                   EnvironmentPair.PreOrder.coq_type -> __ ->
                                   __ -> __ -> __ ->
                                   EnvironmentPair.PreOrder.coq_type ->
                                   coq_R_Scheduler_rec -> 'a1 -> 'a1) ->
                                   EnvironmentPair.PreOrder.coq_type ->
                                   EnvironmentPair.PreOrder.coq_type ->
                                   EnvironmentPair.PreOrder.coq_type ->
                                   coq_R_Scheduler_rec -> 'a1 **)

let rec coq_R_Scheduler_rec_rect diagram f f0 dependencies nextDependencies t0 = function
  | R_Scheduler_rec_0 (dependencies0, nextDependencies0) ->
      f dependencies0 nextDependencies0 __ __ __ __
  | R_Scheduler_rec_1 (dependencies0, nextDependencies0, res, r0) ->
      f0 dependencies0 nextDependencies0 __ __ __ __ res r0
        (coq_R_Scheduler_rec_rect diagram f f0 nextDependencies0
          (forwardDependencies diagram nextDependencies0) res r0)

(** val coq_R_Scheduler_rec_rec : coq_ModelType ->
                                  (EnvironmentPair.PreOrder.coq_type ->
                                  EnvironmentPair.PreOrder.coq_type -> __ ->
                                  __ -> __ -> __ -> 'a1) ->
                                  (EnvironmentPair.PreOrder.coq_type ->
                                  EnvironmentPair.PreOrder.coq_type -> __ ->
                                  __ -> __ -> __ ->
                                  EnvironmentPair.PreOrder.coq_type ->
                                  coq_R_Scheduler_rec -> 'a1 -> 'a1) ->
                                  EnvironmentPair.PreOrder.coq_type ->
                                  EnvironmentPair.PreOrder.coq_type ->
                                  EnvironmentPair.PreOrder.coq_type ->
                                  coq_R_Scheduler_rec -> 'a1 **)

let rec coq_R_Scheduler_rec_rec diagram f f0 dependencies nextDependencies t0 = function
  | R_Scheduler_rec_0 (dependencies0, nextDependencies0) ->
      f dependencies0 nextDependencies0 __ __ __ __
  | R_Scheduler_rec_1 (dependencies0, nextDependencies0, res, r0) ->
      f0 dependencies0 nextDependencies0 __ __ __ __ res r0
        (coq_R_Scheduler_rec_rec diagram f f0 nextDependencies0
          (forwardDependencies diagram nextDependencies0) res r0)

(** val coq_Scheduler_rec_rect : coq_ModelType ->
                                 (EnvironmentPair.PreOrder.coq_type ->
                                 EnvironmentPair.PreOrder.coq_type -> __ ->
                                 __ -> __ -> __ -> 'a1) ->
                                 (EnvironmentPair.PreOrder.coq_type ->
                                 EnvironmentPair.PreOrder.coq_type -> __ ->
                                 __ -> __ -> __ -> 'a1 -> 'a1) ->
                                 EnvironmentPair.PreOrder.coq_type ->
                                 EnvironmentPair.PreOrder.coq_type -> 'a1 **)

let rec coq_Scheduler_rec_rect diagram f f0 dependencies nextDependencies =
  match EnvironmentPair.PreOrder.dec ( ((), (size diagram))) nextDependencies
          dependencies with
    | true -> f dependencies nextDependencies __ __ __ __
    | false ->
        f0 dependencies nextDependencies __ __ __ __
          (coq_Scheduler_rec_rect diagram f f0 nextDependencies
            (forwardDependencies diagram nextDependencies))

(** val coq_Scheduler_rec_rec : coq_ModelType ->
                                (EnvironmentPair.PreOrder.coq_type ->
                                EnvironmentPair.PreOrder.coq_type -> __ -> __
                                -> __ -> __ -> 'a1) ->
                                (EnvironmentPair.PreOrder.coq_type ->
                                EnvironmentPair.PreOrder.coq_type -> __ -> __
                                -> __ -> __ -> 'a1 -> 'a1) ->
                                EnvironmentPair.PreOrder.coq_type ->
                                EnvironmentPair.PreOrder.coq_type -> 'a1 **)

let coq_Scheduler_rec_rec diagram f f0 dependencies nextDependencies =
  coq_Scheduler_rec_rect diagram f f0 dependencies nextDependencies

(** val coq_R_Scheduler_rec_correct : coq_ModelType ->
                                      EnvironmentPair.PreOrder.coq_type ->
                                      EnvironmentPair.PreOrder.coq_type ->
                                      EnvironmentPair.PreOrder.coq_type ->
                                      coq_R_Scheduler_rec **)

let coq_R_Scheduler_rec_correct x x0 x1 res =
  coq_Scheduler_rec_rect x (fun y y0 _ _ _ _ z _ -> R_Scheduler_rec_0 (y,
    y0)) (fun y y0 _ _ _ _ y5 z _ -> R_Scheduler_rec_1 (y, y0,
    (coq_Scheduler_rec_terminate x y0 (forwardDependencies x y0)),
    (y5 (coq_Scheduler_rec_terminate x y0 (forwardDependencies x y0)) __)))
    x0 x1 res __

(** val computeDependencies : coq_ModelType ->
                              EnvironmentPair.PreOrder.coq_type **)

let computeDependencies diagram =
  let initialDependencies =  ((fun x -> DependencyLattice.bot), (fun x ->
    DependencyLattice.bot))
  in
  coq_Scheduler_rec_terminate diagram initialDependencies
    (forwardDependencies diagram initialDependencies)

(** val modelDependencies : EnvironmentPair.PreOrder.coq_type **)

let modelDependencies =
  computeDependencies model

(** val modelInputDependencies : coq_ModelType ->
                                 DependencyEnvironment.PreOrder.coq_type **)

let modelInputDependencies diagram x =
  fst (computeDependencies diagram) x

(** val modelOutputDependencies : coq_ModelType ->
                                  DependencyEnvironment.PreOrder.coq_type **)

let modelOutputDependencies diagram x =
  snd (computeDependencies diagram) x

module AssignedPriorityTotalPreOrder = Domain.NaturalTotalPreOrder

module UserDefinedPriorityTotalPreOrder = Domain.PriorityPreOrder

module UserDefinedThenAssignedTotalPreOrder = Domain.TotalPreOrderLexicographicProduct(UserDefinedPriorityTotalPreOrder)(AssignedPriorityTotalPreOrder)

type coq_AssignedPriorityFunctionType =
  int -> AssignedPriorityTotalPreOrder.coq_type

type coq_UserDefinedPriorityFunctionType =
  int -> UserDefinedPriorityTotalPreOrder.coq_type

(** val userDefinedPriorityFunction : coq_UserDefinedPriorityFunctionType **)

let userDefinedPriorityFunction = WrapperReader.userDefinedPriorityFunction

(** val assignedPriorityFunction : coq_AssignedPriorityFunctionType **)

let assignedPriorityFunction = WrapperReader.assignedPriorityFunction

module UserDefinedThenAssignedFunction = 
 struct 
  type source = int
  
  type target = UserDefinedThenAssignedTotalPreOrder.coq_type
  
  (** val apply : int -> (UserDefinedPriorityTotalPreOrder.coq_type,
                  AssignedPriorityTotalPreOrder.coq_type) ocaml_prod **)
  
  let apply b =
     ((userDefinedPriorityFunction b), (assignedPriorityFunction b))
 end

module BlockUserDefinedThenAssignedTotalPreOrder = Domain.InverseImageTotalPreOrder(UserDefinedThenAssignedFunction)(UserDefinedThenAssignedTotalPreOrder)

module ReverseBlockUserDefinedThenAssignedTotalPreOrder = Domain.ReverseTotalPreOrder(BlockUserDefinedThenAssignedTotalPreOrder)

module DependencyUserDefinedThenAssignedTotalPreOrder = Domain.TotalPreOrderLexicographicSequence(BlockUserDefinedThenAssignedTotalPreOrder)

module SortedDependencyFunction = 
 struct 
  type source = int
  
  type target = DependencyUserDefinedThenAssignedTotalPreOrder.coq_type
  
  (** val existsDataLoop : DependencyLattice.PreOrder.coq_type -> int -> bool **)
  
  let existsDataLoop s b =
    let rec existsb = function
      | [] -> false
      | (a:: l0) ->
          (match match a with
                   | Domain.TaggedType.Call n -> false
                   | Domain.TaggedType.Return n -> beq_nat n b with
             | true -> true
             | false -> existsb l0)
    in existsb (DependencySetTagged.FiniteSet.elements (proj1_sig s))
  
  (** val removeDataLoops : coq_ModelType ->
                            DependencyLattice.PreOrder.coq_type -> int ->
                            Domain.TaggedType.taggedNumbers list **)
  
  let removeDataLoops diagram s b =
    filter (fun t0 ->
      match t0 with
        | Domain.TaggedType.Call n -> false
        | Domain.TaggedType.Return n ->
            (match beq_nat n b with
               | true -> negb (isSequential b model)
               | false -> false))
      (DependencySetTagged.FiniteSet.elements (proj1_sig s))
  
  (** val removeControlLoops : coq_ModelType ->
                               DependencyLattice.PreOrder.coq_type -> int ->
                               Domain.TaggedType.taggedNumbers list **)
  
  let removeControlLoops diagram s b =
    filter (fun t0 ->
      match t0 with
        | Domain.TaggedType.Call n ->
            (match beq_nat n b with
               | true -> negb (existsDataLoop s b)
               | false -> false)
        | Domain.TaggedType.Return n -> false)
      (DependencySetTagged.FiniteSet.elements (proj1_sig s))
  
  (** val coq_DependencySetWithoutReturn : DependencyLattice.PreOrder.coq_type
                                           -> Domain.TaggedType.taggedNumbers
                                           list **)
  
  let coq_DependencySetWithoutReturn s =
    filter (fun t0 ->
      match t0 with
        | Domain.TaggedType.Call n -> true
        | Domain.TaggedType.Return n -> false)
      (DependencySetTagged.FiniteSet.elements (proj1_sig s))
  
  (** val coq_DependencyListBlocks : Domain.TaggedType.taggedNumbers list ->
                                     int list **)
  
  let coq_DependencyListBlocks l =
    fold_right (fun t0 queue -> 
      ((match t0 with
          | Domain.TaggedType.Call n -> n
          | Domain.TaggedType.Return n -> n):: queue)) [] l
  
  (** val removeCalleesBlocks : int list -> int list **)
  
  let removeCalleesBlocks l =
    filter (fun t0 ->
      match isControled model.controlSignals t0 with
        | true -> false
        | false -> true) l
  
  (** val sortDependency : DependencyLattice.PreOrder.coq_type ->
                           ReverseBlockUserDefinedThenAssignedTotalPreOrder.coq_type
                           list **)
  
  let sortDependency blockIndexSequence =
    treesort (ReverseBlockUserDefinedThenAssignedTotalPreOrder.total ())
      (ReverseBlockUserDefinedThenAssignedTotalPreOrder.eq_dec ())
      (removeCalleesBlocks
        (coq_DependencyListBlocks
          (coq_DependencySetWithoutReturn blockIndexSequence)))
  
  (** val cacheSortDependency : int list coq_CacheType **)
  
  let cacheSortDependency =
    cache (size model)
      (removeCalleesBlocks
        (coq_DependencyListBlocks
          (coq_DependencySetWithoutReturn DependencyLattice.bot)))
      (fun blockIndex -> sortDependency (snd modelDependencies blockIndex))
  
  (** val apply : int -> int list **)
  
  let apply blockIndex =
    cacheSortDependency blockIndex
 end

module GeneAutoBlockTotalPreOrder = Domain.InverseImageTotalPreOrder(SortedDependencyFunction)(DependencyUserDefinedThenAssignedTotalPreOrder)

(** val sortBlocks : GeneAutoBlockTotalPreOrder.coq_type list ->
                     GeneAutoBlockTotalPreOrder.coq_type list sig2 **)

let sortBlocks blockIndexes0 =
  treesort (GeneAutoBlockTotalPreOrder.total ())
    (GeneAutoBlockTotalPreOrder.eq_dec ()) blockIndexes0

(** val splitIsolatedBlocks : GeneAutoBlockTotalPreOrder.coq_type list ->
                              (GeneAutoBlockTotalPreOrder.coq_type list,
                              GeneAutoBlockTotalPreOrder.coq_type list)
                              ocaml_prod **)

let splitIsolatedBlocks blockIndexes0 =
  partition (fun blockIndex ->
    isIsolated model.dataSignals model.controlSignals blockIndex)
    blockIndexes0

(** val splitLoopingBlocks : GeneAutoBlockTotalPreOrder.coq_type list ->
                             (GeneAutoBlockTotalPreOrder.coq_type list,
                             GeneAutoBlockTotalPreOrder.coq_type list)
                             ocaml_prod **)

let splitLoopingBlocks blockIndexes0 =
  partition (fun blockIndex -> negb (isSequential blockIndex model))
    blockIndexes0

(** val splitControledBlocks : GeneAutoBlockTotalPreOrder.coq_type list ->
                               (GeneAutoBlockTotalPreOrder.coq_type list,
                               GeneAutoBlockTotalPreOrder.coq_type list)
                               ocaml_prod **)

let splitControledBlocks blockIndexes0 =
  partition (fun blockIndex -> isControled model.controlSignals blockIndex)
    blockIndexes0

(** val sequenceBlocks : GeneAutoBlockTotalPreOrder.coq_type list ->
                         ((GeneAutoBlockTotalPreOrder.coq_type list,
                         GeneAutoBlockTotalPreOrder.coq_type list)
                         ocaml_prod, GeneAutoBlockTotalPreOrder.coq_type
                         list) ocaml_prod **)

let sequenceBlocks blocks0 =
  let  (alone, linked) = splitIsolatedBlocks blocks0 in
  let  (controled, free) = splitControledBlocks linked in
   (( ([], (sortBlocks controled))),
  (app (sortBlocks alone) (sortBlocks free)))

(** val sequenceModel : coq_ModelType -> coq_ModelType **)

let sequenceModel model0 =
  model0


