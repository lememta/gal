open Datatypes
open Domain
open Coq_list
open OrderedType
open Specif

module type FiniteSetType = 
 sig 
  module FiniteSet : 
   sig 
    module E : 
     sig 
      type t = int
      
      val compare : t -> t -> t coq_Compare
     end
    
    type elt = E.t
    
    type t 
    
    val empty : t
    
    val is_empty : t -> bool
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val singleton : elt -> t
    
    val remove : elt -> t -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val compare : t -> t -> t coq_Compare
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val filter : (elt -> bool) -> t -> t
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val cardinal : t -> int
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
   end
  
  val carrier : FiniteSet.t
 end

module type FiniteSetTaggedType = 
 sig 
  module FiniteSet : 
   sig 
    module E : 
     sig 
      type t = Domain.TaggedType.t
      
      val compare : t -> t -> t coq_Compare
     end
    
    type elt = E.t
    
    type t 
    
    val empty : t
    
    val is_empty : t -> bool
    
    val mem : elt -> t -> bool
    
    val add : elt -> t -> t
    
    val singleton : elt -> t
    
    val remove : elt -> t -> t
    
    val union : t -> t -> t
    
    val inter : t -> t -> t
    
    val diff : t -> t -> t
    
    val compare : t -> t -> t coq_Compare
    
    val equal : t -> t -> bool
    
    val subset : t -> t -> bool
    
    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1
    
    val for_all : (elt -> bool) -> t -> bool
    
    val exists_ : (elt -> bool) -> t -> bool
    
    val filter : (elt -> bool) -> t -> t
    
    val partition : (elt -> bool) -> t -> (t, t) ocaml_prod
    
    val cardinal : t -> int
    
    val elements : t -> elt list
    
    val min_elt : t -> elt option
    
    val max_elt : t -> elt option
    
    val choose : t -> elt option
   end
  
  val carrier : FiniteSet.t
 end

module FinitePowerSetLattice : 
 functor (S:FiniteSetTaggedType) ->
 sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type = unit
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = S.FiniteSet.t
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val singleton : S.FiniteSet.elt -> PreOrder.coq_type
 end

