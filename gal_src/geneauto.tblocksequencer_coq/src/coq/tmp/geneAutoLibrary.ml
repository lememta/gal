open Datatypes
open Peano_dec
open Specif

type coq_BlockKindType =
  | Sequential of int * int
  | Combinatorial of int * int

(** val coq_BlockKindType_rect : (int -> int -> 'a1) -> (int -> int -> 'a1)
                                 -> coq_BlockKindType -> 'a1 **)

let coq_BlockKindType_rect f f0 = function
  | Sequential (x, x0) -> f x x0
  | Combinatorial (x, x0) -> f0 x x0

(** val coq_BlockKindType_rec : (int -> int -> 'a1) -> (int -> int -> 'a1) ->
                                coq_BlockKindType -> 'a1 **)

let coq_BlockKindType_rec f f0 = function
  | Sequential (x, x0) -> f x x0
  | Combinatorial (x, x0) -> f0 x x0

(** val dec_block_op : coq_BlockKindType -> coq_BlockKindType -> bool **)

let dec_block_op x y =
  match x with
    | Sequential (n, n0) ->
        (match y with
           | Sequential (n1, n2) ->
               (match eq_nat_dec n n1 with
                  | true -> eq_nat_dec n0 n2
                  | false -> false)
           | Combinatorial (n1, n2) -> false)
    | Combinatorial (n, n0) ->
        (match y with
           | Sequential (n1, n2) -> false
           | Combinatorial (n1, n2) ->
               (match eq_nat_dec n n1 with
                  | true -> eq_nat_dec n0 n2
                  | false -> false))

