(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/coq/tmp/WrapperWriter.ml,v $
 *  @version	$Revision: 1.12 $
 *  @date	$Date: 2009-07-24 12:50:44 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was written by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

open Datatypes;;

open Domain;;

open Sequencer;;

(*---------- BEGIN SECOND PART ----------*)

(* Second part: to be inserted at the end of BlockSequencer.ml *)

let output_int writer entier = (output_string writer (string_of_int entier));;

let write_int writer value =
  (output_string writer (string_of_int value));;

let printSchedule writer n =
 ((write_int writer n);(output_string writer ":"))
;;

let targetChannel = 
  try
    open_out (!WrapperReader.targetFile)
  with Sys_error(msg) -> Pervasives.stdout;;

let writeResults writer results =
  let rec aux l n =
    match l with
      | [] -> ()
      | t :: q -> 
        ((printSchedule targetChannel (1+t));
        (output_string targetChannel (string_of_int n));
        (output_string targetChannel "\n");
        (aux q (n+1))
     ) in
  (aux results 1);;

let writeControlled writer results =
  let rec aux l n =
    match l with
      | [] -> ()
      | t :: q -> 
        ((printSchedule targetChannel (1+t));
        (output_string targetChannel (string_of_int (- n)));
        (output_string targetChannel "\n");
        (aux q (n+1))
     ) in
  (aux results 1);;

let writeLooping writer results =
  let rec aux l =
    match l with
      | [] -> ()
      | t :: q -> 
        ((printSchedule targetChannel (1+t));
        (output_string targetChannel "0");
        (output_string targetChannel "\n");
        (aux q)
     ) in
  (aux results);;

let blockSequencer blockNumber targetChannel =
  let blockIndexes = (WrapperReader.generate blockNumber) in
  let ( ( looping, controled), sequenced) = (sequenceBlocks blockIndexes) in
    (writeLooping targetChannel looping);
    (writeControlled targetChannel controled);
    (writeResults targetChannel sequenced);
    (output_string targetChannel "\n");;

blockSequencer WrapperReader.blockNumber targetChannel;;

(close_out targetChannel);;

let printEnvironment env nbr = 
  for idx = 0 to (nbr - 1) do
    (WrapperReader.writeDebug 
      ("block( "
      ^ (string_of_int (idx + 1))
      ^ " ) = {" 
      ^ (Sequencer.DependencySetTagged.FiniteSet.fold 
	   (fun v str -> 
	      str ^ " " ^ 
		(match v with 
		   | (Domain.TaggedType.Call n) -> "C" ^(string_of_int (n + 1)) 
		   | (Domain.TaggedType.Return n) -> "R" ^ (string_of_int (n + 1))))
		(env idx) "" )
      ^ " }"))
  done;;

let printDependencies = function
  | (envIn,envOut) ->
      (WrapperReader.writeDebug "Input dependencies:");
      (printEnvironment envIn WrapperReader.blockNumber);
      (WrapperReader.writeDebug "Output dependencies:");
      (printEnvironment envOut WrapperReader.blockNumber);
      (envIn,envOut) ;;

printDependencies modelDependencies;;

(close_out WrapperReader.logChannel);;

(*---------- END SECOND PART ----------*)

