open Coq_list
open Specif

type __ = Obj.t

val sort_rec :
  'a2 -> ('a1 -> 'a1 list -> __ -> 'a2 -> __ -> 'a2) -> 'a1 list -> 'a2

type 'a merge_lem =
  'a list
  (* singleton inductive, whose constructor was merge_exist *)

val merge_lem_rect :
  ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list -> ('a1 list -> __ -> __ -> __
  -> 'a2) -> 'a1 merge_lem -> 'a2

val merge_lem_rec :
  ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list -> ('a1 list -> __ -> __ -> __
  -> 'a2) -> 'a1 merge_lem -> 'a2

val merge :
  ('a1 -> 'a1 -> bool) -> ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list -> 'a1
  merge_lem

