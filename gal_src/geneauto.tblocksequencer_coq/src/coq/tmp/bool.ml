(*
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/coq/tmp/bool.ml,v $
 *  @version	$Revision: 1.9 $
 *  @date	$Date: 2009-07-24 12:50:44 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 *
 * This code was extracted from Coq standard library and adapted by:
 *      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7, Nassima.Izerrouken@enseeiht.fr
 *      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7, Marc.Pantel@enseeiht.fr
 *)

open Datatypes
open Specif

(** val bool_dec : bool -> bool -> bool **)

let bool_dec b1 b2 = b1 = b2

(** val eqb : bool -> bool -> bool **)

let eqb b1 b2 = b1 = b2

(** val ifb : bool -> bool -> bool -> bool **)

let ifb b1 b2 b3 =
  match b1 with
    | true -> b2
    | false -> b3

(** val andb : bool -> bool -> bool **)

let andb b1 b2 = b1 && b2

(** val orb : bool -> bool -> bool **)

let orb b1 b2 = b1 || b2

(** val implb : bool -> bool -> bool **)

let implb b1 b2 = b1 <= b2

(** val xorb : bool -> bool -> bool **)

let xorb b1 b2 = b1 <> b2

(** val negb : bool -> bool **)

let negb = not

(** val orb_true_elim : bool -> bool -> bool **)

let orb_true_elim b1 b2 = b1

(** val andb_false_elim : bool -> bool -> bool **)

let andb_false_elim b1 b2 = not b1


