open Compare_dec
open Datatypes
open Coq_list
open OrderedType
open Peano_dec
open Specif

module type ArrowType = 
 sig 
  type source 
  
  type target 
  
  val apply : source -> target
 end

module type DataType = 
 sig 
  type coq_type 
  
  val eq : coq_type -> coq_type -> bool
 end

module Unit : 
 sig 
  type coq_type = unit
  
  val eq : coq_type -> coq_type -> bool
 end

module type PreOrderType = 
 sig 
  module Data : 
   DataType
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module type TotalPreOrderType = 
 sig 
  module Data : 
   DataType
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module NaturalTotalPreOrder : 
 sig 
  module Data : 
   sig 
    type coq_type = unit
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = int
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module PriorityPreOrder : 
 sig 
  module Data : 
   sig 
    type coq_type = unit
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = int option
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module PreOrderCartesianProduct : 
 functor (O1:PreOrderType) ->
 functor (O2:sig 
  module Data : 
   sig 
    type coq_type = O1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 sig 
  module Data : 
   sig 
    type coq_type = O1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = (O1.coq_type, O2.coq_type) ocaml_prod
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module PreOrderLexicographicProduct : 
 functor (Coq_ord1:PreOrderType) ->
 functor (Coq_ord2:sig 
  module Data : 
   sig 
    type coq_type = Coq_ord1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 sig 
  module Data : 
   sig 
    type coq_type = Coq_ord1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = (Coq_ord1.coq_type, Coq_ord2.coq_type) ocaml_prod
  
  val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module TotalPreOrderLexicographicProduct : 
 functor (Coq_ord1:TotalPreOrderType) ->
 functor (Coq_ord2:sig 
  module Data : 
   sig 
    type coq_type = Coq_ord1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type 
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 sig 
  module L : 
   sig 
    module Data : 
     sig 
      type coq_type = Coq_ord1.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = (Coq_ord1.coq_type, Coq_ord2.coq_type) ocaml_prod
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = Coq_ord1.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = L.coq_type
  
  val dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val eq_dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val total : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
 end

module PreOrderLexicographicSequence : 
 functor (Coq_ord:PreOrderType) ->
 sig 
  module Data : 
   sig 
    type coq_type = Coq_ord.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = Coq_ord.coq_type list
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module TotalPreOrderLexicographicSequence : 
 functor (Coq_ord:TotalPreOrderType) ->
 sig 
  module L : 
   sig 
    module Data : 
     sig 
      type coq_type = Coq_ord.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = Coq_ord.coq_type list
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = Coq_ord.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = L.coq_type
  
  val dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val eq_dec : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
  
  val total : L.Data.coq_type -> L.coq_type -> L.coq_type -> bool
 end

module ReversePreOrder : 
 functor (Coq_preOrder:PreOrderType) ->
 sig 
  module Data : 
   sig 
    type coq_type = Coq_preOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = Coq_preOrder.coq_type
  
  val dec :
    Coq_preOrder.Data.coq_type -> Coq_preOrder.coq_type ->
    Coq_preOrder.coq_type -> bool
  
  val eq_dec :
    Coq_preOrder.Data.coq_type -> Coq_preOrder.coq_type ->
    Coq_preOrder.coq_type -> bool
 end

module ReverseTotalPreOrder : 
 functor (Coq_totalOrder:TotalPreOrderType) ->
 sig 
  module L : 
   sig 
    module Data : 
     sig 
      type coq_type = Coq_totalOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = Coq_totalOrder.coq_type
    
    val dec :
      Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
      Coq_totalOrder.coq_type -> bool
    
    val eq_dec :
      Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
      Coq_totalOrder.coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = Coq_totalOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = L.coq_type
  
  val dec :
    Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
    Coq_totalOrder.coq_type -> bool
  
  val eq_dec :
    Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
    Coq_totalOrder.coq_type -> bool
  
  val total :
    Coq_totalOrder.Data.coq_type -> Coq_totalOrder.coq_type ->
    Coq_totalOrder.coq_type -> bool
 end

module InverseImagePreOrder : 
 functor (A:ArrowType) ->
 functor (Coq_preOrder:sig 
  module Data : 
   DataType
  
  type coq_type = A.target
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 sig 
  module Data : 
   sig 
    type coq_type = Coq_preOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = A.source
  
  val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
 end

module InverseImageTotalPreOrder : 
 functor (A:ArrowType) ->
 functor (Coq_totalPreOrder:sig 
  module Data : 
   DataType
  
  type coq_type = A.target
  
  val dec : Data.coq_type -> coq_type -> coq_type -> bool
  
  val total : Data.coq_type -> coq_type -> coq_type -> bool
  
  val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
 end) ->
 sig 
  module I : 
   sig 
    module Data : 
     sig 
      type coq_type = Coq_totalPreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = A.source
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  module Data : 
   sig 
    type coq_type = Coq_totalPreOrder.Data.coq_type
    
    val eq : coq_type -> coq_type -> bool
   end
  
  type coq_type = I.coq_type
  
  val eq_dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
  
  val dec : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
  
  val total : I.Data.coq_type -> I.coq_type -> I.coq_type -> bool
 end

module type LatticeType = 
 sig 
  module PreOrder : 
   PreOrderType
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
 end

module LatticeCartesianProduct : 
 functor (L1:LatticeType) ->
 functor (L2:sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type = L1.PreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type 
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot : PreOrder.coq_type
  
  val top : PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
 end) ->
 sig 
  module PreOrder : 
   sig 
    module Data : 
     sig 
      type coq_type = L1.PreOrder.Data.coq_type
      
      val eq : coq_type -> coq_type -> bool
     end
    
    type coq_type = (L1.PreOrder.coq_type, L2.PreOrder.coq_type) ocaml_prod
    
    val dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_opt_dec : Data.coq_type -> coq_type -> coq_type -> bool
    
    val eq_dec : Data.coq_type -> coq_type -> coq_type -> bool
   end
  
  val bot : (L1.PreOrder.coq_type, L2.PreOrder.coq_type) ocaml_prod
  
  val top : (L1.PreOrder.coq_type, L2.PreOrder.coq_type) ocaml_prod
  
  val join :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
  
  val meet :
    PreOrder.Data.coq_type -> PreOrder.coq_type -> PreOrder.coq_type ->
    PreOrder.coq_type
 end

module TaggedType : 
 sig 
  type taggedNumbers =
    | Call of int
    | Return of int
  
  val taggedNumbers_rect :
    (int -> 'a1) -> (int -> 'a1) -> taggedNumbers -> 'a1
  
  val taggedNumbers_rec :
    (int -> 'a1) -> (int -> 'a1) -> taggedNumbers -> 'a1
  
  type t = taggedNumbers
 end

module OrderedTaggedNumbers : 
 sig 
  type t = TaggedType.t
  
  val compare : t -> t -> t coq_Compare
 end

module OrderedNatural : 
 sig 
  type t = int
  
  val compare : t -> t -> t coq_Compare
 end

module OrderedTypeLexicographicProduct : 
 functor (A:OrderedType.OrderedType) ->
 functor (B:OrderedType.OrderedType) ->
 sig 
  type t = (A.t, B.t) ocaml_prod
  
  val compare : t -> t -> t coq_Compare
 end

