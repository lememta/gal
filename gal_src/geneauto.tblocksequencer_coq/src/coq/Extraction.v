Require Import Datatypes.
Require Import OrderedType.
Require Import Specif.
Require Import Peano.
Require Import Peano_dec.
Require Import Compare_dec.
Require Bool.
Require Import FSetList.
Require Import Heap.

Require Import Util.
Require Import Domain.
Require Import DependenceLattice.
Require Import GeneAutoLibrary.
Require Import GeneAutoLanguage.
Require Import EnvironmentLattice.
Require Import Sequencer.

(* embedding of coq data structures into ocaml data structures.*)
Extract Inductive unit => "unit" [ "()" ].
Extract Inductive bool => "bool" [ "true" "false" ].
Extract Inductive sumbool => "bool" [ "true" "false" ].
Extract Inductive sumor => "option" [ "Some" "None" ].
Extract Inductive option => "option" [ "Some" "None"].
Extract Inductive list => "list" [ "Ocaml_nil" "Ocaml_cons" ].
(* Extract Inductive list => "list" [ "[]" "(::)" ]. *)
Extract Inductive prod => "ocaml_prod" [ "" ].
(* Extract Inductive prod ... voir doc Coq 8.2 *)
Extract Inductive nat  => "int" [ "0" "succ" ].

(* some coq and ocaml functions on lists disagree, so these patches are provided. *)
(* Extract Constant ocaml_append    => "List.append". *)
Extract Constant option_hd => "(fun l -> match l with [] -> None | t::_ -> Some t)".
(* Extract Constant ocaml_exists => "List.exists". *)
(* Extract Constant ocaml_fold_right => "List.fold_right". *)

(* a caching mechanism (read-only) to recover some efficiency when evaluating functions *)
Extract Constant cache => "(fun s d f -> let t = Array.init s f in fun x -> (assert (x >= 0); if x < s then t.(x) else d))".

(* inlining to work around an extraction ugliness (undesired eta-expansion) *)
Extraction Inline Scheduler_rec.

(* no inlining in order to use native ocaml functions instead of their source code specialized expansions. *)
Extraction NoInline map filter fold_right partition nat_rec nat_rect eq_nat_dec. 

(* Unset Extraction Optimize.

Unset Extraction AutoInline. *)

(* constants defining the model. A functor would have been better here. *)
Extract Constant model => "WrapperReader.model".
Extract Constant userDefinedPriorityFunction => "WrapperReader.userDefinedPriorityFunction".
Extract Constant assignedPriorityFunction => "WrapperReader.assignedPriorityFunction".

(* extraction of coq standard libraries. Some of them have been patched afterwards and so are not extracted anymore. *)
(* Extraction Library Datatypes.
Extraction Library Bool.
Extraction Library Peano.
Extraction Library Peano_dec.
Extraction Library Compare_dec. *)
Extraction Library OrderedType.
Extraction Library Specif.
Extraction Library Logic.
(* Extraction Library List. *)
Extraction Library FSetList.
Extraction Library Multiset.
Extraction Library Sorting.
Extraction Library Heap.
(* Extraction "heap"  treesort. *)

(* extraction of the scheduler user modules. *)
Extraction Library Util.
Extraction Library Domain.
Extraction Library DependenceLattice.
Extraction Library GeneAutoLibrary.
Extraction Library GeneAutoLanguage.
Extraction Library EnvironmentLattice.
Extraction Library Sequencer.
