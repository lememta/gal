(* This code was written by: *)
(*      - Nassima Izerrouken, CONTINENTAL/IRIT/INPT/N7 *)
(*      - Marc Pantel and Xavier Thirioux, IRIT/INPT/N7 *)
(* It is released under GPL v2 license *)

(* For the sequencing of block it is only required to consider two kind of *)
(* blocks : Sequentiel (directFeedThrough = false) *)
(*          and Combinatorial (directFeedThrough = true) *)
(* Each block is parameterized by the number of input and output ports *)
(* Each block has a trigger and enable port *)
(* Currently, the port are not distinguished. *)

Require Import Peano_dec.

Inductive BlockKindType : Set :=
  | Sequential : nat -> nat -> BlockKindType
  | Combinatorial : nat -> nat -> BlockKindType.

(** Proof that the equal operation is complete and decidable *)
Theorem dec_block_op : forall x y : BlockKindType, { (x = y) } + { ~ (x = y) }.
  Proof.
    destruct x; 
      destruct y.
    elim (eq_nat_dec n n1).
    intro;
      subst.
    elim (eq_nat_dec n0 n2).
    intro;
      subst;
      left;
      auto.
    intro;
      right;
      intro.
    apply b.
      injection H.
    auto.
    intro;
      right;
      intro;
      injection H.
    auto.
    right;
      discriminate.
    right;
      discriminate.
    elim (eq_nat_dec n n1).
    intro;
      subst.
    elim (eq_nat_dec n0 n2).
    intro;
      subst;
      left;
      auto.
    intro;
      right;
      intro;
      injection H;
      auto.
    intro;
      right;
      intro;
      injection H;
      auto.
  Defined.
