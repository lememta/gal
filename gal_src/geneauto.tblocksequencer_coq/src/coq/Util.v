Require Import Arith.
Require Import List.

Axiom option_hd : forall (A : Set) (l : list A), { a : A | forall e, hd e l = a } + { l = nil }.

(* Axiom ocaml_append : forall (A : Type), list A -> list A -> list A. *)

(* Axiom ocaml_exists : forall (A : Type), (A -> bool) -> list A -> bool. *)

(* Axiom ocaml_fold_right : forall (A B : Type), (B -> A -> A) -> (list B) -> A -> A. *)

(* Axiom ocaml_append_is_app : ocaml_append = app. *)

(* Axiom ocaml_exists_is_existsb : ocaml_exists = existsb. *)

(* Axiom ocaml_fold_right_is_fold_right : forall (A B : Type) (f : B -> A -> A) (e : A) (l : list B),
  ocaml_fold_right A B f l e = fold_right f e l. *)
(*
Axiom cache (A : Type) : Set.

Axiom build : *)

Inductive CacheType (A : Type) : Type :=
 | Cache : (nat -> A) -> CacheType A.

Axiom cache : forall (A : Type), nat -> A -> (nat -> A) -> CacheType A.

Axiom cache_is_identity : 
  forall (A : Type) (size : nat) (default : A) (function : nat -> A),
    (forall n, size <= n -> function n = default) 
    -> (cache A size default function) = (Cache A function).

