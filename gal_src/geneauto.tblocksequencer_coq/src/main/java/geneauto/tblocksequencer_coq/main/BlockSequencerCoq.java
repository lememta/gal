/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/main/java/geneauto/tblocksequencer_coq/main/BlockSequencerCoq.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2011-11-29 17:40:58 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301 USA
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national ITEA funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FeRIA/IRIT - INPT/University of Toulouse
 *  	INRIA 
 */
package geneauto.tblocksequencer_coq.main;

import geneauto.eventhandler.EventHandler;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

/**
 * Main class of the tool. It reads the arguments and launches the GASystemModel
 * preprocessing.
 */
public class BlockSequencerCoq {

    public static String VERSION = "0.1 02/07/2008";
    public static String TOOL_NAME = "TBlockSequencer_coq";

    /**
     * reference to the tool statemachine
     */
    private static BlockSequencerCoqMachine stateMachine;

    /**
     * Main method of the CPrinter tool. It reads the program parameters and
     * launches the GASystemModel preprocessing.
     * 
     * @param args
     *            List of the arguments of the tool.
     */
    public static void main(String[] args) {
        // set root of all location strings in messages
        EventHandler.setToolName(TOOL_NAME);

        // initialise ArgumentReader
        ArgumentReader.clear();

        ArgumentReader.addArgument("", GAConst.ARG_INPUTFILE, true, true,
                true, GAConst.ARG_INPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-o", GAConst.ARG_OUTPUTFILE, true, true,
                true, GAConst.ARG_OUTPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-b", GAConst.ARG_LIBFILE, false, true,
                true, GAConst.ARG_LIBFILE_NAME_ROOT);
        ArgumentReader.addArgument("-d", GAConst.ARG_DEBUG, false, 
        		false, false, "");

        stateMachine = BlockSequencerCoqMachine.getInstance();

        // set arguments list to ArgumentReader and parse the list
        stateMachine.initTool(args, true);

        // Run the state machine
        stateMachine.run();
    }
}
