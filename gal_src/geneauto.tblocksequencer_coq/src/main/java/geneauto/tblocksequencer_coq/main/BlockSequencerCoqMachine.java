/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/main/java/geneauto/tblocksequencer_coq/main/BlockSequencerCoqMachine.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer_coq.main;

import geneauto.modelfactory.factories.ModelFactory;
import geneauto.models.genericmodel.Model;
import geneauto.statemachine.StateMachine;
import geneauto.tblocksequencer_coq.states.InitializingState;
import geneauto.utils.FileUtil;

import java.net.URL;

/**
 */
public class BlockSequencerCoqMachine extends StateMachine {
	
	// TODO: (to MaPa) remove if not used
    private static String BlockSequenceDefaultLocation = "";

	/**
	 * Unique instance of this class.
	 */
	protected static StateMachine instance;
	
    /**
     * Model read by the SystemModelFactory. It contains all of the
     * GAModelElements which will need to be typed.
     */
    private Model systemModel;

    /**
     * Factory which is used to read the model from the xml file.
     */
    private ModelFactory systemModelFactory;
    
    private String tempPath;

    /**
     * Private constructor of this class. Initialises the state machine
     */
    private BlockSequencerCoqMachine() {
    	// set the version number of the elementary tool machine
    	super(BlockSequencerCoq.VERSION,"");
    	
    	String toolClassPath = "geneauto/tblocksequencer_coq/main/BlockSequencerCoqMachine.class";
        URL url = this.getClass().getClassLoader().getResource(
                toolClassPath);
        
        String version = FileUtil.getVersionFromManifest(url,toolClassPath);
        String releaseDate = FileUtil.getDateFromManifest(url, toolClassPath);
        
        if (version!=null) {
            setVerString(version);
        }
        if (releaseDate!=null) {
            setReleaseDate(releaseDate);
        }
    	
		// set initial state
        initialState = InitializingState.getInstance();
    }

    public static BlockSequencerCoqMachine getInstance(){
    	if (instance == null){
    		instance = new BlockSequencerCoqMachine();
    	}
    	
    	return (BlockSequencerCoqMachine) instance;
    }

	public Model getSystemModel() {
		return systemModel;
	}

	public void setSystemModel(Model systemModel) {
		this.systemModel = systemModel;
	}

	public ModelFactory getSystemModelFactory() {
		return systemModelFactory;
	}

	public void setSystemModelFactory(ModelFactory systemModelFactory) {
		this.systemModelFactory = systemModelFactory;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	} 
	
	public String getTempPath() {
		return this.tempPath;
	}
 }
