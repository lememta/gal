/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/main/java/geneauto/tblocksequencer_coq/states/TransformingState.java,v $
 *  @version	$Revision: 1.42 $
 *	@date		$Date: 2011-11-28 22:44:15 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer_coq.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.genericmodel.Model;
import geneauto.utils.FileUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


/** Core class implementing the block sequencer front end.
 *  Read the XML file
 *  Write a simple text model file
 *  Call the OCaML/Coq block sequencer
 *  Read the simple text execution order and log files
 *  Write the XML file with the computed execution order
 */
public class TransformingState extends BlockSequencerCoqState {
	
	/** Name of the elementary tool */
	private static String TOOL_NAME = "TBlockSequencer";
	
	/** Error messages */
	private final static String EVENT_CRITICAL_ILL_FORMED_MODEL = "The model is not well formed.";
	
	private final static String EVENT_CRITICAL_ALG_LOOP = "The model contains algebraic loops.";
	
	private final static String EVENT_CRITICAL_GUESS_OS = "Can't guess what operating system running GeneAuto. Won't be able to run the Block Sequencer.";
	
	private final static String EVENT_CRITICAL_EXECUTE_SEQUENCER = "Can't execute the BlockSequencer external program.";
	
	private final static String EVENT_CRITICAL_FIND_SEQUENCER = "Can't find the BlockSequencer external program.";
	
	private final static String EVENT_CRITICAL_FIND_OCAMLRUN = "Can't find the ocamlrun external program and BlockSequencer is not executable independantly.";

	private static final String EVENT_INFO_SEVERAL_SEQUENCER = "Found several external block sequencer, will use the first one found. The next one found is";

	private static final String EVENT_DEBUG_BINARY_NOT_EXECUTABLE = "Found a binary external block sequencer that is not executable.";

	private static final String EVENT_DEBUG_UNREADABLE_SEQUENCER = "Found a binary external block sequencer that is not readable.";

	private static final String EVENT_DEBUG_UNREADABLE_OCAMLRUN = "Found an external ocamlrun that is not readable or executable.";

	private static final String EVENT_INFO_SEVERAL_OCAMLRUN = "Found several external ocamlrun, will use the first one found. The next one found is";
	

	/** Detect the operating system we are running on in order to choose the right external executable */
	/* TODO : MaPa, 2008/08/15, must change this as there should be no dependence on the OS */
	private static final boolean onWindows = java.lang.System.getProperty( "os.name").startsWith( "Windows");
	private static final boolean onLinux = java.lang.System.getProperty( "os.name").startsWith( "Linux");
	private static final boolean onMacOSX = java.lang.System.getProperty( "os.name").startsWith( "Mac OS X");

	/* Initialize onWindows */
	static {
		if (((! onWindows) && (! onLinux) && (! onMacOSX) ) || (onWindows && onLinux ) || (onWindows && onMacOSX) || (onLinux && onMacOSX)) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", EVENT_CRITICAL_GUESS_OS, "");		
		} else {
			String msg = "Detected OS (" + java.lang.System.getProperty( "os.name") + ") = ";
			if (onWindows) {
				msg += "Windows";
			} else {
				if (onLinux) {
					msg += "Linux";
				} else {
					if (onMacOSX) {
						msg += "Mac OS X";
					} else {
						msg += "None";
					}
				}
			}
			EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");
		}
	}
	
	/** Name of the Coq/CaML bytecode executable */
	private final static String BYTECODE_TOOL_NAME;
	
	/* Initialize BYTECODE_TOOL_NAME */
	static {
		if (onWindows) {
			/** Prefix for executable in Windows */
			BYTECODE_TOOL_NAME = "BlockSequencer-bytecode.exe";
		} else {
			if (onLinux) {
				BYTECODE_TOOL_NAME = "BlockSequencer-bytecode.bin";
			} else {
				/** Prefix for the executable under Unix */
				/* This can be anything except .exe */
				/* The problem is derived from the two very different implementation of the system call library */
				if (onMacOSX) {
					BYTECODE_TOOL_NAME = "BlockSequencer-bytecode.app";
				} else {
					BYTECODE_TOOL_NAME = "BlockSequencer-bytecode.unknow";
				}
			}
		}
	}

	/** Name of the Coq/CaML binary executable */	
	private final static String NATIVE_TOOL_NAME;

	/* Initialize NATIVE_TOOL_NAME */
	static {
		if (onWindows) {
			/** Prefix for executable in Windows */
			NATIVE_TOOL_NAME = "BlockSequencer-native.exe";
		} else {
			/** Prefix for the executable under Unices (Linux or Mac OS X) */
			/* This can be anything except .exe */
			if (onLinux) {
				NATIVE_TOOL_NAME = "BlockSequencer-native.bin";
			} else {
				if (onMacOSX) {
					NATIVE_TOOL_NAME = "BlockSequencer-native.app";
				} else {
					NATIVE_TOOL_NAME = "BlockSequencer-native.unknow";
				}
			}
		}
	}
	
	
	/** Translation map between the tag used in the Coq/CaML log file */
	/* and the Java logging framework */
	private static Map<String,EventLevel> LOG_TAG_MAP = new HashMap<String,EventLevel>();
	
	static {
		LOG_TAG_MAP.put("critical", EventLevel.CRITICAL_ERROR);
		LOG_TAG_MAP.put("error", EventLevel.ERROR);
		LOG_TAG_MAP.put("warning", EventLevel.WARNING);
		LOG_TAG_MAP.put("debug", EventLevel.DEBUG);
		LOG_TAG_MAP.put("information", EventLevel.INFO);
		LOG_TAG_MAP.put("none", EventLevel.NO_LOG);
	};

	/** Try to locate the GeneAuto installation relying on the Java classpath which must have access to */
	/* the GeneAuto jar files */
	/* return a String containing the directory in which the GeneAuto libraries reside */
	/* Refinement level 4 */
	private static String lookForClassPath() {
		/* If nothing is found, null is returned */
		String result = null;
		/* Look for the Java classpath property
		 */
		String classPath = System.getProperty("java.class.path");
		String msg = "The java.class.path property points to " + classPath;
		EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");

		if (classPath == null) {
			/* Then for the CLASSPATH environment variable
			 */
			classPath = System.getenv("CLASSPATH");
			msg = "The CLASSPATH variable points to " + classPath;
			EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");	
		}
		if (classPath != null) {
			if (classPath.contains(File.pathSeparator)) {
				String[] splittedClassPath = classPath.split(File.pathSeparator);		
				for (String part : splittedClassPath) {
					if (part.toLowerCase().matches("(?i).*galauncher*")) {
						result = part;
					}
				}
			}
		}
		if (result != null) {
			if (result.endsWith(".jar")){
				result = (new File(result).getParent());
			}
		}
		return result;
	}
	
	/** Extract the sub-list of string corresponding to existing files or directories */
	private static List<String> filterExistingFile( List<String> paths) {
		if (paths != null) { 
			List<String> results = new ArrayList<String>(); 
			for( String path : paths) {
				if (path != null) {
					File local = new File( path );
					if (local.exists()) {
						results.add (path);
					}
				}
			}
			return results;
		} else {
			return null;
		}
	}
	
	/* List of path where the tool can look for the Coq/CaML executable */
	private final static List<String> DEFAULT_SEQUENCER_PATHS;
	
	/* Initialize DEFAULT_SEQUENCER_PATHS */
	static {
		List<String> local = new ArrayList<String>();
		String path = null;
		/* First look for the environment variable defining the installation path */
		path =  System.getenv("GENEAUTO_HOME");
		String msg = "The GENEAUTO_HOME variable points to " + path;
		EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");	
		if (path != null) {
			local.add( path + File.separator + "bin");
		}
		/* Then look for the Java property defining the installation path */
		path = System.getProperty("geneauto.home");
		msg = "The geneauto.home property points to " + path;
		EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");
		if (path != null) {
			local.add( path + File.separator + "bin" );
		}
		/* Then look for the GeneAuto Java libraries for finding the installation path */
		path = lookForClassPath();
		msg = "Using the Java class path, the GeneAuto installation seems to be in " + path;
		EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");	
		if (path != null) {
			local.add( path + File.separator + "bin" );
		}
		/* Then look for potential predefined installation path */
		if (onWindows) {
			local.add( "C:\\Program Files\\GeneAuto\\bin" );			
		} else {
			if (onLinux || onMacOSX) {
				local.add( "/usr/GeneAuto/bin" );
				local.add( "/usr/local/GeneAuto/bin");
				local.add( "/opt/GeneAuto/bin" );
				local.add( "/opt/local/GeneAuto/bin" );	
			}
		}
		/* Then look in the eclipse project hierarchy */
		local.add( ".." + File.separator + "geneauto.tblocksequencer_coq" + File.separator + "bin" );
		DEFAULT_SEQUENCER_PATHS = local;
	}

	/* List of path where the tool can look for the ocamlrun virtual machine executable */	
	private final static List<String> DEFAULT_OCAMLRUN_PATHS;

	/* Initialize DEFAULT_OCAMLRUN_PATHS */
	static {
		List<String> local = new ArrayList<String>();
		if (onWindows) {
			local.add( "C:\\Program Files\\Objective Caml\\bin\\ocamlrun.exe");
		} else {
			if (onLinux || onMacOSX) {
				local.add( "/bin/ocamlrun");
				local.add( "/usr/bin/ocamlrun");
				local.add( "/usr/ocaml/bin/ocamlrun");
				local.add( "/usr/local/bin/ocamlrun");
				local.add( "/usr/local/ocaml/bin/ocamlrun");
				local.add( "/opt/bin/ocamlrun");
				local.add( "/opt/ocaml/bin/ocamlrun");
				local.add( "/opt/local/bin/ocamlrun");
				local.add( "/opt/local/ocaml/bin/ocamlrun");
			}
		}
		DEFAULT_OCAMLRUN_PATHS = local; // filterExecutableFile( filterExistingFile( local ));
	}



	/** Tags used in the Coq/CaML <==> Java communication protocol */
	
	/* Start of the block section */
	private final static String BLOCKS_TAG = "blocks";
	
	private final static String BLOCK_TAG = "block";
	
	private final static String SEQUENTIAL_BLOCK_TAG = "sequential";
	
	private final static String COMBINATORIAL_BLOCK_TAG = "combinatorial";
	
	/* Start of the signal section */
	private final static String SIGNALS_TAG = "signals";
	
	private final static String SIGNAL_TAG = "signal";
	
	private final static String DATA_TAG = "data";
	
	private final static String CONTROL_TAG = "control";

	private static TransformingState instance;
	
	public TransformingState(){
		// set state name for messages
		super("Transforming");
	}
	
	public static TransformingState getInstance(){
		if (instance == null){
			instance = new TransformingState();
		}
				
		return (TransformingState)instance;
	}
	
	/** Write the information regarding a block of a given index in the writer */
	/* Refinement level 3 */
	private void writeBlock( PrintWriter writer, Block block, int index ) {
		final String msg = "<" + block.getName() + "> has been assigned number " + index + "(" + block.getOriginalFullName() + ")";
		EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");	
		writer.print( BLOCK_TAG + " " + index + " " );
		if (block.isDirectFeedThrough()) {
			writer.print( COMBINATORIAL_BLOCK_TAG + " " );
		} else {
			writer.print( SEQUENTIAL_BLOCK_TAG + " " );
		}
		writer.print( block.getInDataPorts().size() + " " + block.getOutDataPorts().size() + " ");
		writer.println( block.getUserDefinedPriority() + " " + block.getAssignedPriority() );
	}

	/** Write the information regarding a block of a given index in the writer */
	/* Refinement level 3 */
    private void writeSignal(PrintWriter writer, Signal signal, int index, Map<Block,Integer> blockIndexMap,
			Map<Inport, Block> inportBlockMap,
			Map<Outport, Block> outportBlockMap) {
    	Inport dst = signal.getDstPort();
    	/* Look for the block for whom dst is an input port */
    	Block dstBlock = inportBlockMap.get( dst );
    	Outport src = signal.getSrcPort();
    	/* Look for the block for whom src is an output port */
    	Block srcBlock = outportBlockMap.get( src );
		String msg = "<" + srcBlock.getName() + "> through port " + src + " is connected to <" + dstBlock.getName() + "> through port " + dst;
		EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");
		writer.print( SIGNAL_TAG + " " + index + " " );
		if ((src instanceof OutDataPort) && ((dst instanceof InDataPort) || (dst instanceof InEnablePort) || (dst instanceof InEdgeEnablePort))) {
			writer.print( DATA_TAG + " " );
		} else {
			if ((src instanceof OutControlPort) && (dst instanceof InControlPort)) {	
				writer.print( CONTROL_TAG + " " );
			} else {
				String srcPortKind = "unknown";
				if (src instanceof OutDataPort) {
					srcPortKind = "Out Data";
				} else {
					if (src instanceof OutControlPort) {
						srcPortKind = "Out Control";
					}		
				}
				String dstPortKind = "unknown";
				if (dst instanceof InDataPort) {
					dstPortKind = "In Data";
				} else {
					if (dst instanceof InEnablePort) {
						dstPortKind = "In Enable (Data)";
					} else {
						if (dst instanceof InEdgeEnablePort) {
							dstPortKind = "In Edge Enable (Data)";
						} else {
							if (dst instanceof InControlPort) {
								dstPortKind = "In Control";
							}
						}
					}
				}
				msg = EVENT_CRITICAL_ILL_FORMED_MODEL + " A data port is connected to a control port: " + src.getName() + "(" + srcPortKind + ") -> " + dst.getName() + "(" + dstPortKind + ")";
				EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", msg, "");					
			}
		}
		/* Look for the block index of the source block and target block */
		writer.println(blockIndexMap.get(srcBlock) + " " + blockIndexMap.get(dstBlock));
	}
    
	/** Write the information regarding a diagram in a model */
	/* Refinement level 2 */
	private Map<Block,Integer> writeModel( PrintWriter writer, SystemBlock system) throws IOException {
		/* Current index for blocks, starts from 1 up to the number of blocks */
		int blockIndex = 1;
		/* Maps the block index to the block object */
		Map<Integer,Block> blockMap = new HashMap<Integer,Block>();
		/* Maps the block object to the block index */
		Map<Block,Integer> blockIndexMap = new HashMap<Block,Integer>();
		/* Current index for signals, starts from 1 up to the number of signals */		
		int signalIndex = 1;
		/* Maps the signal index to the signal object */
		Map<Integer,Signal> signalMap = new HashMap<Integer,Signal>();
		/* Maps input port objects to block objects */
		Map<Inport,Block> inportBlockMap = new HashMap<Inport,Block>();
		/* Maps output port objects to block objects */
		Map<Outport,Block> outportBlockMap = new HashMap<Outport,Block>();
		/* Write the beginning of the block section and the number of blocks */
		writer.println( BLOCKS_TAG + " " + system.getBlocks().size());
		for (Block block : system.getBlocks()) {
			/* Register the block object corresponding to the block index */
			blockMap.put(blockIndex,block);
			/* Register the block index corresponding to the block object */
			blockIndexMap.put(block,blockIndex);
			/* Register the block corresponding to all its data input ports */
			/* This is required to related the input port in a signal and the source block */
			for (Inport inport : block.getInDataPorts()) {
				inportBlockMap.put(inport,block);
			}
			/* Register the block corresponding to all its control input ports */
			/* This is required to related the input port in a signal and the source block */
			for (Inport inport : block.getInControlPorts()) {
			  inportBlockMap.put(inport,block);
			}
			/* Register the block corresponding to its input enable port */
			/* This is required to related the input port in a signal and the source block */
			inportBlockMap.put(block.getInEnablePort(),block);
			/* Register the block corresponding to its input edge enable port */
			/* This is required to related the input port in a signal and the source block */
			inportBlockMap.put(block.getInEdgeEnablePort(),block);
			/* Register the block corresponding to all its data output ports */
			/* This is required to related the output port in a signal and the target block */			
			for (Outport outport : block.getOutDataPorts()) {
				outportBlockMap.put(outport,block);
			}
			/* Register the block corresponding to all its control output ports */
			/* This is required to related the output port in a signal and the target block */
			for (Outport outport : block.getOutControlPorts()) {
				outportBlockMap.put(outport,block);
			}			
			/* Write the information related to the block */
			writeBlock(writer,block,blockIndex);
			/* Next block index */
			blockIndex++;
		}
		/* At the end of the block list, the block index must be equal to the number of block + 1 */
		assert( blockIndex == (system.getBlocks().size() + 1));
		/* Write the beginning of the signal section and the number of signals */
		writer.println( SIGNALS_TAG + " " + system.getSignals().size());
		for (Signal signal : system.getSignals()) {
			/* Register the signal object corresponding to the signal index */
			signalMap.put(signalIndex,signal);
			/* Write the information related to the signal */
			writeSignal( writer, signal, signalIndex, blockIndexMap, inportBlockMap, outportBlockMap);
			/* Next signal index */
			signalIndex++;
		}
		/* At the end of the signal list, the block index must be equal to the number of signals + 1 */
		assert( signalIndex == (system.getSignals().size() + 1));
		/* Returns the map associated a block object to its block index */
		/* This will be used to assign the execution order to the block object based on its index */
		return blockIndexMap;
	}
	
	/** Look for the block sequencer external tool in the various potential paths */
	private static String lookForSequencer(List<String> paths) {
		if (paths != null) { 
			String result = null; 
			for( String path : paths) {
				if (path != null) {
					String msg = "Trying using the path " + path;
					EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");								
					String nativeName = path + File.separator + NATIVE_TOOL_NAME;
					File nativeFile = new File( nativeName );
					msg = "Trying " + nativeName;
					EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");	
					if (nativeFile.exists()) {
						if (nativeFile.canRead() && nativeFile.canExecute()) {
							EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", "Successfull.", "");	
							if (result == null) {
								result = nativeName;
							} else {
								EventHandler.handle(EventLevel.INFO, TOOL_NAME, "", EVENT_INFO_SEVERAL_SEQUENCER + " " + nativeName, "");								
							}
						} else {
							EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", EVENT_DEBUG_BINARY_NOT_EXECUTABLE + " " + nativeName, "");					
						}
					}
					String bytecodeName = path + File.separator + BYTECODE_TOOL_NAME;
					File bytecodeFile = new File( bytecodeName );
					msg = "Trying " + bytecodeName;
					EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");	
					if (bytecodeFile.exists()) {
						if (bytecodeFile.canRead()) {
							EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", "Successfull.", "");
							if (result == null) {
								result = bytecodeName;
							} else {
								EventHandler.handle(EventLevel.INFO, TOOL_NAME, "", EVENT_INFO_SEVERAL_SEQUENCER + " " + bytecodeName, "");																
							}
						} else {
							EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", EVENT_DEBUG_UNREADABLE_SEQUENCER + " " + bytecodeName, "");		
						}
					}
				}
			}
			if (result != null) {
				return (new File( result)).getAbsolutePath();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/** Look for the ocamlrun external tool in the various potential paths */
	private static String lookForOCaMLRun( List<String> paths) {
		if (paths != null) {
			String result = null;
			for (String path : paths) {
				String msg = "Trying " + path;
				EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");	
				File file = new File( path );
				if (file.exists()) {
					if (file.canRead() && file.canExecute()) {
						EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", "Successfull.", "");
						if (result == null) {
							result = path;
						} else {
							EventHandler.handle(EventLevel.INFO, TOOL_NAME, "", EVENT_INFO_SEVERAL_OCAMLRUN, "");
						}
					} else {
						EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", EVENT_DEBUG_UNREADABLE_OCAMLRUN, "");
					}
				}
			}
			if (result != null) {
				return (new File( result)).getAbsolutePath();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	private static String coqSequencer = lookForSequencer( DEFAULT_SEQUENCER_PATHS );
	
	private static String OCaMLrun = lookForOCaMLRun( DEFAULT_OCAMLRUN_PATHS );
	
	/** Launch the external OCaML/Coq BlockSequencer, retrieve the messages printed on the console and feed them */
	/* to the common GeneAuto log manager */
	/* Refinement level 2 */
	private void executeSequencer(final String prefix, final String inputPath, final String outputPath, final String logPath) throws IOException {
		if (coqSequencer == null) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", EVENT_CRITICAL_FIND_SEQUENCER, "");			
		} else {
			final File dir = new File( prefix );
			final String[] envMain = null;
			String[] argsMain = null;
			String msg = null;
			if ((new File(coqSequencer)).canExecute()) {
				final String[] argsMainDirect = { coqSequencer, "-i", inputPath, "-o", outputPath, "-l", logPath };
				argsMain = argsMainDirect;
				msg = "External tool command line : " + coqSequencer + " -i " + inputPath + " -o " + outputPath + " -l " + logPath + " in " + dir;
			} else {
				if (OCaMLrun != null) {
					msg = "External tool command line : " + OCaMLrun + " " + coqSequencer + " -i " + inputPath + " -o " + outputPath + " -l " + logPath + " in " + dir;
					if ((new File(OCaMLrun)).canExecute()) {
						final String[] argsMainIndirect = { OCaMLrun, coqSequencer, "-i", inputPath, "-o", outputPath, "-l", logPath };
						argsMain = argsMainIndirect;
					} else {
						OCaMLrun = null;
						EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", EVENT_CRITICAL_FIND_OCAMLRUN, "");
					}
				}
			}
			EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");
			if (argsMain == null) {
				EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", EVENT_CRITICAL_EXECUTE_SEQUENCER, "");				
			} else {
				Process mainProcess = java.lang.Runtime.getRuntime().exec(argsMain,envMain,dir);
				BufferedReader mainOutputs = new BufferedReader(new InputStreamReader( mainProcess.getInputStream()));
				BufferedReader mainErrors = new BufferedReader(new InputStreamReader( mainProcess.getErrorStream()));
				String line = mainOutputs.readLine();
				while (line != null) {	
					msg = "Console output from BlockSequencer: " + line;
					EventHandler.handle(EventLevel.INFO, TOOL_NAME, "", msg, "");
					line = mainOutputs.readLine();
				}
				line = mainErrors.readLine();
				while (line != null) {	
					msg = "Console errors from BlockSequencer: " + line;
					EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", msg, "");			
					line = mainErrors.readLine();
				}
			}
		}
	}	

	/** Read the log file produced by the OCaML/Coq model sequencer */
	/* Refinement level 2 */
	private void readLog( BufferedReader logger) throws IOException {
		String logLine = logger.readLine();
		while (logLine != null) {
			final StringTokenizer tokens = new StringTokenizer(logLine, "%");
			String kind = tokens.nextToken();
			String time = tokens.nextToken();
			String msg = tokens.nextToken();
			if (LOG_TAG_MAP.containsKey(kind)) {
				EventHandler.handle(LOG_TAG_MAP.get(kind), TOOL_NAME, "", time, msg, "");					
			} else {
				EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", "A message line in the log.txt file did not start with an appropriate tag: " + logLine, "");
			}
			logLine = logger.readLine();
		}
	}

	/** Read the execution order file produced by the OCaML/Coq model sequencer */
	/* Refinement level 2 */
	private Map<Integer,Integer> readExecutionOrder( BufferedReader reader) throws IOException {
		final Map<Integer, Integer> result = new HashMap<Integer, Integer>();
		String line = reader.readLine();
		StringTokenizer tokens = new StringTokenizer(line, ":");
		while ((line != null) && (tokens.hasMoreTokens())) {
			final int block = Integer.parseInt(tokens.nextToken());
			final int sequence = Integer.parseInt(tokens.nextToken());
			if (sequence == 0) {
				EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", EVENT_CRITICAL_ALG_LOOP, "");
				final String msg = "The block number " + block + "could not be sequenced.";
				EventHandler.handle(EventLevel.CRITICAL_ERROR, TOOL_NAME, "", msg, "");
			} else {
				final String msg = "The block number " + block + " is sequenced at position " + sequence;
				EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");					
			}
			result.put(block, sequence);
			line = reader.readLine();
			tokens = new StringTokenizer(line, ":");
		}
		return result;
	}
	
	/** sequence a given model, writing the intermediate files at a given place designated by a path in the file system */
	/* Refinement level 1 */
	private void sequence( SystemBlock system, String path ) {
		try {
			String msg = "Sequencing the <" + system.getName() + "> system.";
			EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");
			/** Current index of the system being sequenced. This is useful if there is several non-atomic subsystems */
			/* in the same diagram of a given model. */
			int systemIndex = 1;
			final File directory = new File(path);
			directory.mkdirs();

			for (Block block : system.getBlocks()) {
				if (block instanceof SystemBlock) {
					sequence( (SystemBlock) block, FileUtil.appendPath( path, "system" + systemIndex));
					/* Next subsystem in the model */
					systemIndex++;
				}
			}
			/* Name for the simple model text file */
			final String outputPath = FileUtil.appendPath(path, "model.txt");
			/* Name for the simple log text file */
			final String logPath = FileUtil.appendPath(path, "log.txt");
			/* Name for the simple execution order text file */
			final String inputPath = FileUtil.appendPath(path, "sequencer.txt");
			
			/* Write the model to a simple text file */
			final File output = new File(outputPath);
			PrintWriter writer = new PrintWriter( new FileWriter( output ));
			Map<Block,Integer> blockIndexMap = writeModel( writer, system);
			writer.close();

			/* Execute the OCaML/Coq model sequencer */
			executeSequencer( path, outputPath, inputPath, logPath);

			/* Process the simple log text file */
			final File log = new File(logPath);
			BufferedReader logger = new BufferedReader( new FileReader( log ));
			readLog( logger );
			logger.close();

			/* Process the simple execution order text file */
			final File input = new File(inputPath);
			final BufferedReader reader = new BufferedReader(new FileReader(input));
			final Map<Integer, Integer> result = readExecutionOrder(reader);

			/* Assign its execution order to each block */
			for (Block block : system.getBlocks()) {
				if (block.getInControlPorts().size() > 0) {
					assert( result.get(blockIndexMap.get(block)) < 0);
					msg = "The block <" + block.getName() + "> (number " + blockIndexMap.get( block ) + ") is controled";
					EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");
				} else {
					msg = "The block <" + block.getName() + "> (number " + blockIndexMap.get( block ) + ") is sequenced at position " + result.get(blockIndexMap.get(block));
					EventHandler.handle(EventLevel.DEBUG, TOOL_NAME, "", msg, "");
				}
				block.setExecutionOrder(result.get(blockIndexMap.get(block)));
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * 
     */
	public void stateExecute() {

		Model model = getMachine().getSystemModel();
		if (model instanceof GASystemModel) {
			GASystemModel systemModel = (GASystemModel) model;
			int systemIndex = 1;
			for (SystemBlock systemBlock : systemModel.getTopLevelSystemBlocks()) {
				String path = FileUtil.appendPath( getMachine().getTempPath(), "system" + systemIndex);

				sequence( systemBlock, path);
				systemIndex++;
			}
		}
		getMachine().setState(SavingModelState.getInstance());
       
	}
}
    
