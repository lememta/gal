/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/main/java/geneauto/tblocksequencer_coq/states/BlockSequencerCoqState.java,v $
 *  @version	$Revision: 1.2 $
 *	@date		$Date: 2009-02-03 10:05:49 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer_coq.states;

import geneauto.statemachine.State;
import geneauto.tblocksequencer_coq.main.BlockSequencerCoqMachine;


/**
 * Abstract base class for all CodeModelOptimizer states
 *
 */
public abstract class BlockSequencerCoqState extends State {
	
    public BlockSequencerCoqState(String stateName) {
		super(stateName);
	}

	/**
     * returns reference to the instance of stateMachine
     * perfoms typecast to the correct machine type
     * 
     * @return BlockSequencerCoqMachine stateMachine
     */
    public BlockSequencerCoqMachine getMachine(){
    	return BlockSequencerCoqMachine.getInstance();
    }
  
}
