/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer_coq/src/main/java/geneauto/tblocksequencer_coq/states/InitializingState.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2009-12-27 18:36:04 $
 *
 *  Copyright (c) 2006-2009 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2006-2009 Continental SAS
 *		http://www.continental-corporation.com, olivier.ssi-yan-kai@continental-corporation.com 

 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer_coq.states;

import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.tblocksequencer_coq.main.BlockSequencerCoq;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.List;

/**
 * First state which is executed by the tool. It checks the java parameters,
 * imports the model from the xml file, reads the block library, initializes the
 * whole tool.
 * 
 */
public class InitializingState extends BlockSequencerCoqState {

    private static InitializingState instance;

    public InitializingState() {
        // set state name for messages
        super("Initializing");
    }

    public static InitializingState getInstance() {
        if (instance == null) {
            instance = new InitializingState();
        }

        return (InitializingState) instance;
    }

    /**
     * Reads the CodeModel.
     */
    public void stateExecute() {
        // get parameters.
        String inputFile = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);
        String outputFile = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFILE);
        String libraryFile = ArgumentReader.getParameter(GAConst.ARG_LIBFILE);
        String tempPath = ArgumentReader.getParameter(GAConst.ARG_TMPFOLDER);

        getMachine().setTempPath(tempPath);

        // check for read/write permissions
        FileUtil.assertCanRead(inputFile);
        FileUtil.assertCanWrite(outputFile);
        FileUtil.assertCanWrite(tempPath);

        // init the library files
        List<String> libraryFiles = new ArrayList<String>();
        libraryFiles.add(ArgumentReader.getParameter(GAConst.ARG_DEFLIBFILE));
        if (libraryFile != null) {
            libraryFiles.add(libraryFile);
        }
        // initializes the BlockLibraryFactory
        BlockLibraryFactory blockLibraryFactory = new BlockLibraryFactory(
        		new GABlockLibrary(), BlockSequencerCoq.TOOL_NAME, libraryFiles, null);

        // initializes the systemModelfactory
        ModelFactory systemModelfactory = new SystemModelFactory(
        		new GASystemModel(), BlockSequencerCoq.TOOL_NAME, inputFile, outputFile,
                blockLibraryFactory, null);

        getMachine().setSystemModelFactory(systemModelfactory);

        // Read model
        getMachine().getSystemModelFactory().readModel();
        getMachine().setSystemModel(
                getMachine().getSystemModelFactory().getModel());

        // Go to next state - transforming
        getMachine().setState(TransformingState.getInstance());

    }
}
