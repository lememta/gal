/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.ttyper/src/main/java/geneauto/typer/main/TyperTool.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2009-11-10 09:57:22 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.typer.main;

import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.statemachine.StateMachine;
import geneauto.typer.states.InitializingState;
import geneauto.typer.states.SavingModelState;
import geneauto.typer.states.TyperState;
import geneauto.typer.states.TypingModelState;
import geneauto.typer.states.ValidatingModelState;
import geneauto.utils.FileUtil;

import java.net.URL;

/**
 * State machine of the Typer. It takes as an input a GASystemModel, and types
 * its output ports. This class is a singleton. As there is only one typer
 * allowed to run at the same time, this class is a singleton.
 * 
 */
public final class TyperTool extends StateMachine{

    /**
     * Unique instance of this class.
     */
    private static TyperTool instance;

    /**
     * Unique way to access the typer.
     * 
     * @return the unique instance of this class.
     */
    public static TyperTool getInstance() {
        if (instance == null) {
            instance = new TyperTool();
        }
        return instance;
    }

    /**
     * First state of the typer. Reads the input parameters, initialises the
     * typer with the given values.
     */
    private TyperState initializingState;

    /**
     * Third state. Types the output ports of the model's blocks.
     */
    private TyperState typingModelState;

    /**
     * Fourth state. Checks that the typing was done correctly and did not
     * insert into the model typing errors.
     */
    private TyperState validatingModelState;

    /**
     * Last state. Saves the model into a XML file using the model factory.
     */
    private TyperState savingModelState;
    
    /**
     * Model read by the SystemModelFactory. It contains all of the 
     * GAModelElements which will need to be typed.
     */
    private GASystemModel systemModel;

    /**
     * Library factory which will help to type the model.
     */
    private GABlockLibrary blockLibrary;

    /**
     * Factory which is used to read and write the model in the xml file.
     */
    private SystemModelFactory systemModelFactory;

    /**
     * Factory which is used to read and write the model in the library file.
     */
    private BlockLibraryFactory blockLibraryFactory;

    /**
     * Private constructor of this class. Instantiates all of the states of the
     * state machine. Sets the current State to initializingState.
     */
    private TyperTool() {
        super("DEV", ""); // TODO: put version number here
        
    	String toolClassPath = "geneauto/typer/main/TyperTool.class";
        URL url = this.getClass().getClassLoader().getResource(
                toolClassPath);
        
        String version = FileUtil.getVersionFromManifest(url,toolClassPath);
        String releaseDate = FileUtil.getDateFromManifest(url, toolClassPath);
        
        if (version!=null) {
            setVerString(version);
        }
        if (releaseDate!=null) {
            setReleaseDate(releaseDate);
        }
        
        initializingState = new InitializingState(this);
        typingModelState = new TypingModelState(this);
        validatingModelState = new ValidatingModelState(this);
        savingModelState = new SavingModelState(this);
    }

    /**
     * @return the blockLibrary
     */
    public GABlockLibrary getBlockLibrary() {
        return blockLibrary;
    }

    
    /**
     * @return the blockLibraryFactory
     */
    public BlockLibraryFactory getBlockLibraryFactory() {
        return blockLibraryFactory;
    }

    /**
     * @return the initializingState
     */
    public TyperState getInitializingState() {
        return initializingState;
    }

    /**
     * @return the savingModelState
     */
    public TyperState getSavingModelState() {
        return savingModelState;
    }

    /**
     * @return the systemModel
     */
    public GASystemModel getSystemModel() {
        return systemModel;
    }

    /**
     * @return the systemModelFactory
     */
    public ModelFactory getSystemModelFactory() {
        return systemModelFactory;
    }

    /**
     * @return the typingModelState
     */
    public TyperState getTypingModelState() {
        return typingModelState;
    }

    /**
     * @return the validatingModelState
     */
    public TyperState getValidatingModelState() {
        return validatingModelState;
    }

    /**
     * Initialises the typer tool.
     * 
     * @param arguments
     */
    public void init() {
        setState(initializingState);
    }

    /**
     * @param blockLibrary the blockLibrary to set.
     */
    public void setBlockLibrary(GABlockLibrary blockLibrary) {
        this.blockLibrary = blockLibrary;
    }

    /**
     * @param blockLibraryFactory
     *            the blockLibraryFactory to set
     */
    public void setBlockLibraryFactory(BlockLibraryFactory blockLibraryFactory) {
        this.blockLibraryFactory = blockLibraryFactory;
    }

    /**
     * @param systemModel the systemModel to set
     */
    public void setSystemModel(GASystemModel systemModel) {
        this.systemModel = systemModel;
    }
    
    /**
     * @param systemModelFactory
     *            the systemModelFactory to set
     */
    public void setSystemModelFactory(SystemModelFactory systemModelFactory) {
        this.systemModelFactory = systemModelFactory;
    }
}
