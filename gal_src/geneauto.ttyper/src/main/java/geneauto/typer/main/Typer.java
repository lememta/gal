/**
 *
 */
package geneauto.typer.main;

import geneauto.eventhandler.EventHandler;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

/**
 * Main class of the Typer tool. It reads the arguments and launches the typing
 * process.
 * 
 */
public class Typer {

    public static String TOOL_NAME = "TTyper";

	/**
     * Main method of the typer tool. It reads the program parameters, launches
     * the typing process, then close the program.
     * 
     * @param args
     *            List of the arguments of the typer.
     */
    public static void main(String[] args) {

        // set root of all location strings in messages
        EventHandler.setToolName("TTyper");

        // initialise ArgumentReader
        ArgumentReader.clear();

        // set expected argument list
        ArgumentReader.addArgument("", GAConst.ARG_INPUTFILE, true, true,
                true, GAConst.ARG_INPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-o", GAConst.ARG_OUTPUTFILE, false, true,
                true, GAConst.ARG_OUTPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-b", GAConst.ARG_LIBFILE, false, true,
                true, GAConst.ARG_LIBFILE_NAME_ROOT);
        ArgumentReader.addArgument("-d", GAConst.ARG_DEBUG, false, 
        		false, false, "");

        // set arguments list to ArgumentReader and parse the list
        TyperTool.getInstance().initTool(args, true);

        runTyper();
    }

    /**
     * Runs the typer according to the given parameters.
     */
    private static void runTyper() {
        TyperTool.getInstance().init();
    }
}
