/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.ttyper/src/main/java/geneauto/typer/states/TypingModelState.java,v $
 *  @version	$Revision: 1.60 $
 *	@date		$Date: 2011-07-07 12:24:15 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.typer.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.typer.main.TyperTool;
import geneauto.utils.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 */
public class TypingModelState extends TyperState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public TypingModelState(TyperTool machine) {
        super(machine, "TypingModel");
    }

    /**
     * Logs the beginning of the model typing and gets the SystemModel.
     */
    public void stateEntry() {
        super.stateEntry();

        systemModel = stateMachine.getSystemModel();
    }

    /**
     * Check each Block of the model. If this block has output ports, it calls
     * the method typeBlockPorts (block).
     */
    public void stateExecute() {

        // type blocks. Assume that in case of error, the error message
        // is thrown in either getRootSystemBlock or typeSystem ports

        List<Block> untypedBlocks = new ArrayList<Block>();
        Set<Block> backtrackChain = new HashSet<Block>();

        //type all the model, starting from top-level system block
        typeBlockPorts(stateMachine.getSystemModel().getRootSystemBlock(),
                untypedBlocks, backtrackChain);

        stateMachine.setState(stateMachine.getValidatingModelState());
    }

    /**
     * Puts the modelElements into a map, to be retrieved by execution order.
     * Specific treatment for controlled subsystems, they are put in a list with
     * the block feeds data for them.
     * 
     * Returns pair, where left element is minimum execution order value of 
     * sequential blocks and the right element is a map of execution order
     * values and corresponding blocks.
     */
    private Pair<Integer, Map<Integer, List<Block>>> createOrderedMap(
            SystemBlock system) {

        // compose map where execution order is the key
        // controlled blocks are in this map with the maximum execution order
        // of their data provider + 1
        Map<Integer, List<Block>> modelElements = 
        								new HashMap<Integer, List<Block>>();

        // maximum execution order of data-flow controlled blocks
        int currEO = 0;
        // minimum execution order of sequential blocks.
        // NB! here we assume that sequential block is never controlled
        int minSeq = -1;

        for (Block block : system.getBlocks()) {
            if (block.getExecutionOrder() < 0) {
                // if block is a controlled subsystem
                // then we put it in the map with
                // the execution order of highest
                // data input's execution order + 1
                // this is to ensure that all data inputs are typed before
                currEO = getDataSourceMaxEO(block, new HashSet<Block>()) + 1;

            } else {
                currEO = block.getExecutionOrder();
            }

            // put block to the map of all element
            if (modelElements.containsKey(currEO)) {
                insertBlockToList(block, modelElements.get(currEO));
            } else {
                modelElements.put(currEO, insertBlockToList(block, null));
            }

            // if block is sequential, put it also to specific map
            if (!block.isDirectFeedThrough()) {
            	if (minSeq == -1 || block.getExecutionOrder() < minSeq) {
            		minSeq = block.getExecutionOrder();
                }
            }
        }

        return new Pair<Integer, Map<Integer, List<Block>>>(
        											minSeq, modelElements);
    }

    /**
     * Takes block b and list targetList as an input. Places b in the
     * targetList. If b.executionOrder > -1 it will be placed in the end of the
     * list If b.executionOrder < 0 it will be placed in the list sorted by
     * execution order value as follows: prevBlock.executionOrder >
     * b.executiOnorder AND( b.executionOrder > nextBlock.executionOrder OR
     * nextBlock.executionOrder() > -1) If targetList is null new LinkedList
     * will be created
     * 
     * @param b -
     *            block to add to the list
     * @param targetList -
     *            list of blocks with zero or more controlled blocks
     *            (executionOrder < 0) and zero or one data-flow ordered block
     * @return targetList
     */
    private List<Block> insertBlockToList(Block b, List<Block> targetList) {
        if (targetList == null) {
            targetList = new LinkedList<Block>();
        }

        if (b.getExecutionOrder() < 0) {
            /*
             * controlled block. put it in the list so that
             * prevBlock.getExecutionOrder() > b.getExecutionOrder AND(
             * b.getExecutionOrder > nextBlock.getExecutionorder() OR
             * nextBlock.getExecutionorder() > -1)
             * 
             * We assume that list contains zero or more unique negative
             * execution order values and zero or one non-negative value
             */
            int currentEO = 0;
            int idx = 0;
            for (idx = 0; idx < targetList.size(); idx++) {
                currentEO = targetList.get(idx).getExecutionOrder();
                if (currentEO > -1 || currentEO < b.getExecutionOrder()) {
                    break;
                }
            }
            /*
             * add block to the given position. If list was empty, the block
             * will be at position 0. If the new block has lowest negative it
             * will be added to the end of the list
             */
            targetList.add(idx, b);
        } else {
            // data-flow-scheduled block goes to the end of the list.
            targetList.add(b);
        }

        return targetList;
    }

    /**
     * Gets the block library and types the output ports according to it.
     * 
     * @param system --
     *            the system block to resolve
     * @param backtrackChain --
     *            set for recording blocks visited while backtracking type (to
     *            avoid loops)
     */
    private synchronized void typeSystemPorts(SystemBlock system,
            Set<Block> backtrackChain) {
        // we sort the elements of the system into a map so that
        // we can browse the system in the correct execution order.
        Pair<Integer, Map<Integer, List<Block>>> blockMapPair = 
        											createOrderedMap(system);
        Map<Integer, List<Block>> modelElements = blockMapPair.getRight();
        int minSeq = blockMapPair.getLeft();

        // a list of blocks that can not be typed on first round (sequential
        // blocks, function call targets with input from another function call
        List<Block> untypedBlocks = new LinkedList<Block>();

        /* FIRST PASS
         * goes through all blocks in the subsystem
         */
        for (int execOrder = 0; execOrder <= modelElements.size(); execOrder++) {
            // type all blocks in the block list
            processBlockList(modelElements.get(execOrder), untypedBlocks,
                    backtrackChain);
        }

        /* SECOND PASS
         * goes through the model again starting from the sequential block
         * with lowest execution order.
         *  
         * second pass to type sequential blocks. This is required for
         * sequential blocks typed with both intern parameter and input type
         * (like unitDelay). Such type of block can have intern parameter giving
         * a type different than input type that will be given in the second
         * pass (for example if parameter signal type is scalar and input is
         * array with same data type)
         */
        for (int execOrder = minSeq; execOrder <= modelElements.size(); 
        													execOrder++) {

        	// type all blocks in the block list
        	processBlockList(modelElements.get(execOrder), untypedBlocks,
        													backtrackChain);
        }
        
        
        /* THIRD PASS
         * final pass to type blocks that could not be typed even after typing
         * sequential blocks. if this fails we assume that there was a loop
         */
        for (Block block : untypedBlocks) {
            if (!outputsAreTyped(block)) {
                typeBlockPorts(block, null, backtrackChain);
            }
        }
    }

    /**
     * takes set of blocks that are set to the same execution order this list
     * contains maximum one block that actually has the given execution order
     * and 0 or more controlled blocks that have derived the execution order
     * from their predecessors.
     * 
     * processes first all controlled blocks and finally the block with defined
     * execution order
     * 
     * @param blockList --
     *            list of blocks to process
     * @param untypedBlocks --
     *            list to memorise the blocks that are still after this loop
     * @param backtrackChain --
     *            list of all block processed in this chain to discover loops
     */
    private void processBlockList(List<Block> blockList,
            List<Block> untypedBlocks, Set<Block> backtrackChain) {

        // block to be processed last
        Block lastBlock = null;

        if (blockList != null) {
            // each block of the system must be typed.
            for (Block block : blockList) {
                if (block.getExecutionOrder() < 0) {
                    typeBlockPorts(block, untypedBlocks, backtrackChain);
                } else {
                    // NB! here we assume that there is never more that one
                    // block with executionOrder > -1
                    lastBlock = block;
                }
            }

            if (lastBlock != null) {
                typeBlockPorts(lastBlock, untypedBlocks, backtrackChain);
            }
        }

    }

    /**
     * Method which assigns a datatype to a block's output ports.
     * 
     * @param block
     *            the block which is to be typed.
     * @param untypedBlocks
     *            list for blocks that need to be processed again on next if
     *            this argument is null, then this is a last pass and error
     *            should be thrown when untyped block is discovered
     * @param backtrackChain
     *            set of blocks that are processed while backtracking the input
     *            type of controlled blocks. It is necessary for avoiding loops
     */
    private synchronized void typeBlockPorts(Block block,
            List<Block> untypedBlocks, Set<Block> backtrackChain) {

        // if block is a system, then we browse its content
        if (block instanceof SystemBlock) {
			/* if block inputs are not typed see if types can be found for them
			 * before continuing. This does not apply to the top-level system
             */
            if (block.getParent() != null 
            	&& !(inputsAreTyped(block, untypedBlocks))) {
                if (untypedBlocks == null) {
                    // last pass. If we still have untyped blocks there is
                    // either a loop or an unordered chain of controlled blocks
                    if (block.isControlled()) {
                        typePredecessors(block, backtrackChain);
                        // check again if inputs got typed. If if not return
                        // with error
                        if (!inputsAreTyped(block, null)) {
                            return;
                        }
                    } else {
                        // only controlled blocks can be untyped at this point
                        EventHandler.handle(EventLevel.CRITICAL_ERROR,
                                "typeBlockPorts", "",
                                "Unable to type input ports of block\n "
                                        + block.getReferenceString() + ".", "");

                    }
                } else {
                    // first pass, just add block to the list of untyped blocks
                    untypedBlocks.add(block);
                    return;
                }
            }

            // if the block is a subsystem
            // first we type the Inport blocks of this system
            typeSystemsInports((SystemBlock) block);

            // then we type its inner elements
            typeSystemPorts((SystemBlock) block, backtrackChain);
        }

        // if the block has in or out control ports, then they
        // are assigned the type boolean .
        // assign type boolean to control ports of the block.
        typeControlPorts(block);

        /*
         * unless we are not in the last loop or the block is not sequential
         * (untypedBlocks == null) check if block inputs are typed. If they are
         * not, memorise the block and return in the next loop. An exception is
         * done for the top-level system block which has by construction untyped
         * input ports
         */
        if (block.isDirectFeedThrough() && untypedBlocks != null
                && !inputsAreTyped(block, untypedBlocks)
                && block.getParent() != null) {
            return;
        }
        BlockTyper typer = block.getBlockTyper();

        // if the typing returns true, then
        // the outports have been typed and the types
        // can be propagated via the signals
        try {
            if (typer.assignTypes(block)) {
                for (OutDataPort port : block.getOutDataPorts()) {
                    // TODO: double check if having an untyped
                    // port here at this stage could be a
                    // problem
                    EventHandler.handle(EventLevel.DEBUG, "", "DataType "
                            + DataTypeAccessor.toString(port.getDataType())
                            + "\n was assigned to Port "
                            + port.getReferenceString() + ".", "");
                }

                // if typing all right, then datatype is
                // propagated.
                typeSuccessors(block);
            } else {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                        .getCanonicalName(), "", "Error while typing block: "
                        + block.getReferenceString());
            }
        } catch (Exception e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "typeBlockPorts",
                    "",
                    "Error when typing block " + block.getReferenceString(), e);
        }
    }

    /**
     * Once a block's output data ports have been typed, we type all of the
     * dstPorts of the signals coming out from this block.
     * 
     * @param The
     *            block which has just been typed.
     */
    private synchronized void typeSuccessors(Block block) {
        Inport dstPort = null;
        
        // for each outport of the typed block
        // the system is browsed, looking for signals which come out
        // from its output ports.
        for (OutDataPort outPort : block.getOutDataPorts()) {
            for (Signal signal : outPort.getOutgoingSignals()) {
                // sets dst port with same data type as src port.
                dstPort = signal.getDstPort();
                if (dstPort != null /* && dstPort.getDataType() == null */) {
                    // assign the datatype to the signal's dstPort
                    if (outPort.getDataType() != null) {
                        dstPort.setDataType(outPort.getDataType());

                        EventHandler.handle(EventLevel.DEBUG, "Typer", "",
                                "Propagating types via signals\nDataType\n"
                                        + dstPort.getDataType().toString()
                                        + "\nwas assigned to Port "
                                        + dstPort.getReferenceString() + ".",
                                "");
                    } else {
                        EventHandler.handle(EventLevel.CRITICAL_ERROR, "Typer",
                                "", "Type propagated from port "
                                        + outPort.getName() + " of block "
                                        + block.getReferenceString()
                                        + " is null", "");
                    }

                }
            }
        }
    }

    /**
     * Checks for untyped ports. Tries to type for output ports of each block
     * that does not have datatype
     * 
     * @param block
     *            block with untyped inputs
     * @param backtrackChain
     *            set of blocks that are processed while backtracking the input
     *            type of controlled blocks. It is necessary for avoiding loops
     * @return
     */
    private synchronized void typePredecessors(Block block,
            Set<Block> backtrackChain) {
        if (backtrackChain.contains(block)) {
            // there is a loop. stop here
            return;
        }

        // remember current block to detect loops
        backtrackChain.add(block);
        for (Inport port : block.getAllInDataPorts()) {
            if (port.getDataType() == null) {
                for (Signal s : port.getIncomingSignals()) {
                    // type the source block
                    typeBlockPorts((Block) s.getSrcPort().getParent(), null,
                            backtrackChain);
                }
            }
        }

        // block processed. remove it from the set
        backtrackChain.remove(block);
    }

    /**
     * Types the system's Inport blocks.
     * 
     * @param system
     *            the system which Inport blocks must be typed.
     */
    private synchronized void typeSystemsInports(SystemBlock system) {
        Block inportBlock = null;

        // we get all the system's Inport blocks.
        for (Inport inport : system.getAllInports()) {
            // the port has corresponding inport block
            if (inport.getSourceBlock() != null) {
                inportBlock = inport.getSourceBlock();

                // we assign to the inport block the same type as
                // the corresponding input port.
                for (OutDataPort outport : inportBlock.getOutDataPorts()) {
                	if (inport.getDataType() != null) {
                        outport.setDataType(inport.getDataType());
                        EventHandler.handle(EventLevel.DEBUG, "Typer", "",
                                "Typing system's ports\nDataType\n"
                                        + outport.getDataType().toString()
                                        + "\nwas assigned to Port "
                                        + outport.getReferenceString() + ".",
                                "");
                    } else {
                        // TODO: decide if we need error here
                    }
                }
            }
        }
    }

    /**
     * Method which, given a block, type its control ports.
     * 
     * @param system
     */
    private void typeControlPorts(Block block) {
        if (block.getInControlPorts().size() > 0) {
            for (InControlPort icp : block.getInControlPorts()) {
                icp.setOutputDataType(new TBoolean());
            }
        }
    }

    /**
     * Returns max execution order of all blocks providing data inputs to the
     * given block. In case of function-call activated block processes also
     * inputs of the activator block.
     * 
     * 
     * @param b -
     *            block to be processed
     * @param backtrackChain -
     *            set of blocks already processed to avoid loops
     * 
     * @return int
     */
    private int getDataSourceMaxEO(Block b, Set<Block> backTrackChain) {
        Block srcBlock = null;
        // maximum execution order. Initially execution order of a block
        // itself -- to cover the case of block with no inputs
        int maxEO = b.getExecutionOrder();

        // execution order of last processed input
        int currEO = 0;

        if (backTrackChain.contains(b)) {
            // we are in loop. break here
            return 0;
        } else {
            backTrackChain.add(b);
        }

        // process all incoming signals
        for (Signal s : b.getIncomingSignals()) {
            srcBlock = (Block) s.getSrcPort().getParent();
            if (srcBlock != null) {
                // in case of controlling block or function-call-activated
                // data block process its predecessors
                if (s.getDstPort() instanceof InControlPort
                        || srcBlock.getExecutionOrder() < 0) {
                    currEO = getDataSourceMaxEO(srcBlock, backTrackChain);
                } else {
                    currEO = srcBlock.getExecutionOrder();
                }

                if (currEO > maxEO) {
                    maxEO = currEO;
                }
            }
        }

        // the block is processed, we go back in chain so it can
        // be removed from stack
        backTrackChain.remove(b);

        if (maxEO < 1) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                    .getCanonicalName(), "",
                    "Unable to determine execution order of prdecessors for "
                            + "block\n " + b.getReferenceString(), "");
            return 0;
        }

        return maxEO;
    }

    /**
     * Check if all input data, enable and edgeEnable ports are typed.
     * 
     * If argument untypedBlocks is null, CRITICAL_ERROR is thrown in case of
     * untyped input. Otherwise the block is stored in untypedBlocks list
     * 
     * @param block --
     *            block to check
     * @param untypedBlocks --
     *            list of blocks still to be typed
     * @return true if ports are typed, false otherwise
     */
    private boolean inputsAreTyped(Block block, List<Block> untypedBlocks) {
        for (Inport port : block.getAllInDataPorts()) {
            if (port.getDataType() == null) {
				if (untypedBlocks == null) {
					EventHandler.handle(EventLevel.CRITICAL_ERROR,
							"inputsAreTyped", "",
							"The input port " + port.getPortNumber() + " \""
									+ port.getName() + "\"\n of block "
									+ port.getParent().getReferenceString()
									+ "\n is not typed", "");
				} else {
					untypedBlocks.add(block);
				}

				return false;
            }
        }

        return true;
    }

    /**
     * Check if all output data ports are typed
     * 
     * @param block --
     *            block to check
     * @return true if ports are typed, false otherwise
     */
    private boolean outputsAreTyped(Block block) {
        for (OutDataPort port : block.getOutDataPorts()) {
            if (port.getDataType() == null) {
                return false;
            }
        }

        return true;
    }

}
