/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.ttyper/src/main/java/geneauto/typer/states/InitializingState.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-07-07 12:24:15 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.typer.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.typer.main.Typer;
import geneauto.typer.main.TyperTool;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class InitializingState extends TyperState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public InitializingState(TyperTool machine) {
        super(machine, "Initializing");
    }

    /**
     * Initialises the modelFactory according to the Typer parameters.
     * Initialises also the EventsHandler. This means that no Event can be
     * raised until this method is successfully executed.
     */
    public void stateEntry() {
        super.stateEntry();

        // get the typer's parameters.
        String inputFile = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);
        String outputFile = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFILE);
        String libraryFile = ArgumentReader.getParameter(GAConst.ARG_LIBFILE);
        String tempPath = ArgumentReader.getParameter(GAConst.ARG_TMPFOLDER);

        // check for read/write permissions
        FileUtil.assertCanRead(inputFile);
        FileUtil.assertCanWrite(outputFile);
        FileUtil.assertCanWrite(tempPath);

        // init the library files
        List<String> libraryFiles = new ArrayList<String>();
        if (ArgumentReader.getParameter(GAConst.ARG_DEFLIBFILE) != null) {
            libraryFiles.add(ArgumentReader
                    .getParameter(GAConst.ARG_DEFLIBFILE));
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "",
                    "Default block library is missing");
        }
        if (libraryFile != null) {
            libraryFiles.add(libraryFile);
        }
        // initialises the BlockLibraryFactory
		BlockLibraryFactory blockLibraryFactory = new BlockLibraryFactory(
				new GABlockLibrary(), Typer.TOOL_NAME, libraryFiles, null);

        stateMachine.setBlockLibraryFactory(blockLibraryFactory);

        // initialises the systemModelfactory
		SystemModelFactory systemModelfactory = new SystemModelFactory(
				new GASystemModel(), Typer.TOOL_NAME, inputFile, outputFile,
				((BlockLibraryFactory) blockLibraryFactory), null);

        stateMachine.setSystemModelFactory(systemModelfactory);
    }

    /**
     * Reads the systemModel. Doing this, the library file is read too.
     */
    public void stateExecute() {
        /*
         * EventsHandler.handle(EventLevel.INFO, "Typer", "", "Start of typer
         * execution.", "");
         */
        stateMachine.getSystemModelFactory().readModel();
        stateMachine.setSystemModel((GASystemModel) stateMachine
        		.getSystemModelFactory()
                .getModel());
        stateMachine.setBlockLibrary(stateMachine.getBlockLibraryFactory()
                .getModel());
        stateMachine.setState(stateMachine.getTypingModelState());
    }

    /**
     * Logs the end of the initialization into the log file. public void
     * stateExit() { EventsHandler.handle(EventLevel.INFO, "Typer", "", "Ending
     * typer initialization.", ""); }
     */

}
