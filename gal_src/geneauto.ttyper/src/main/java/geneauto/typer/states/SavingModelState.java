/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.ttyper/src/main/java/geneauto/typer/states/SavingModelState.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:24:15 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.typer.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.typer.main.TyperTool;

/**
 */
public class SavingModelState extends TyperState {

	/**
	 * Constructor of this class.
	 * 
	 * @param machine
	 */
	public SavingModelState(TyperTool machine) {
		super(machine, "SavingModel");
	}

	/**
	 * Stores the systemModel into a xml file, only if the typer has not met an
	 * error during its execution.
	 */
	public void stateExecute() {
		if (!EventHandler.getAnErrorHasOccured()) {
			// write model.
			stateMachine.getSystemModelFactory().writeModel();

		} else {
			EventHandler.handle(
					EventLevel.INFO, "Typer", "",
					"At least one error occured during typer's execution."
					+ "\n The model will not be saved into the output file.",
					"");
		}

		stateMachine.stop();
	}

}
