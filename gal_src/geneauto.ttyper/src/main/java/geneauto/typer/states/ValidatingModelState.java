/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.ttyper/src/main/java/geneauto/typer/states/ValidatingModelState.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:24:14 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.typer.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.typer.main.TyperTool;

/**
 *
 */
public class ValidatingModelState extends TyperState {
	
	/**
	 * Constructor of this class.
	 * @param machine
	 */
	public ValidatingModelState(TyperTool machine) {
		super(machine, "ValidatingModel");
	}

    /**
     * Logs the beginning of the typing validation and gets the SystemModel.
     */
    public final void stateEntry() {
		super.stateEntry();	

        systemModel = stateMachine.getSystemModel();
	}

	/**
     * For each block of the model, if the the block is handled by 
     * a library handler (different from code model or Macro), 
     * calls the method validateTyping of its typer. 
	 */
    public final void stateExecute() {
        // browse the model and puts the elements in a map according
        // to their execution order.
        for (GAModelElement element : systemModel.getAllElements()) {
            if (element instanceof Block) {
                Block block = (Block) element;

                if (block.getOutDataPorts().size() > 0) {
                    if (! validateBlock(block)) {
                         EventHandler.handle(
                                 EventLevel.ERROR, 
                                 "ValidatingModelState", "", 
                                 "Block\n " + block.getReferenceString()
                                 + "\n was not correctly typed.", 
                                 "");
                    }
                }
            }
        }
		stateMachine.setState(stateMachine.getSavingModelState());
	}

  /**
     * Private method which is called by state Execute. Its role is 
     * to call the method validateBlockType of the block typer for each block.
     * @param block
     * 
     * @return
     */
    private final boolean validateBlock(Block block) {

/* TODO: (TF:405) this does not work before handling of data-store is moved to 
 * TODO: TF 556: Clarify the role of Typer.validateTypes
 * pre-processor  (ToNa 21/11/08)
    	// check that all input ports have type
    	for (Inport port : block.getAllInDataPorts()) {
    		if (port.getDataType() == null) {
	            EventsHandler.handle(
	                    EventLevel.ERROR,
	                    "validateBlock",
	                    "", 
	                    "The input port\n "
	                        + port.getReferenceString()
	                        + "\n is not typed", "");
	            return false;
    		}
    	}
    	
    	// check that all output ports have type
    	for (OutDataPort port : block.getOutDataPorts()) {
    		if (port.getDataType() == null) {
	            EventsHandler.handle(
	                    EventLevel.ERROR,
	                    "validateBlock",
	                    "", 
	                    "The output port port\n "
	                        + port.getReferenceString()
	                        + "\n is not typed", "");
	            return false;
    		}
    	}

 */
    
    	// execute checking function in corresponding typer
        BlockType blockType = 
            ((BlockLibraryFactory)stateMachine
                .getBlockLibraryFactory()).getBlockType(block.getType());
        
        if (blockType != null
             && blockType.getLibraryHandler() != null) {
            BlockTyper typer = blockType
                            .getLibraryHandler().getTyper();
            return typer.validateTypes(block);
        }
        
        return true; 
    }
}
