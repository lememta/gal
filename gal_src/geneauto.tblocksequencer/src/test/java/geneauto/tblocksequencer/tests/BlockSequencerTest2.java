/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer/src/test/java/geneauto/tblocksequencer/tests/BlockSequencerTest2.java,v $
 *  @version	$Revision: 1.20 $
 *	@date		$Date: 2009-02-03 10:04:53 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, alain.roan@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer.tests;


import static org.junit.Assert.*;


import java.util.List;

import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.tblocksequencer.BlockSequencer;
import geneauto.tblocksequencer.BlockSequencerUtilities;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests done with file : 
 * 		inputFiles/sample2.gsm
 * 		inputFiles/sample5.gsm
 * 
 *
 */
public class BlockSequencerTest2 {

	BlockSequencer sequencer;
	private String testFilesRoot = "src/test/resources/";

	@Before
	public void setUp() throws Exception {

	}


	@Test
	public void testPropagate() {

		String[] args =  new String[3];

		args[0] = testFilesRoot + "inputFiles/sample2.gsm.xml";
		args[1] = "-o";
		args[2] = testFilesRoot + "outputFiles/sample2.gsm.xml";
		

		sequencer = new BlockSequencer(args);

		sequencer.readModel();

		if(sequencer.getSystemModel() == null)
			System.exit(0);

		int lastExecutionOrder = 1;

		sequencer.initExecutionOrder();

		List<Block> first;
		first = BlockSequencerUtilities.getFirstBlock(sequencer.getHighLevelBlock());

		assertEquals(0,((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Sum")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Gain")).getExecutionOrder());

		assertEquals(0,((Block)sequencer.getElemByName("Unit Delay")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Unit Delay1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(0,((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Sign1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

		lastExecutionOrder = BlockSequencerUtilities.propagate(first.get(0),sequencer.getHighLevelBlock(),lastExecutionOrder, sequencer.getDebugLevel());

		assertEquals(1,((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Sum")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Gain")).getExecutionOrder());

		assertEquals(0,((Block)sequencer.getElemByName("Unit Delay")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Unit Delay1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(0,((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Sign1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

		lastExecutionOrder = BlockSequencerUtilities.propagate(first.get(1),sequencer.getHighLevelBlock(),lastExecutionOrder, sequencer.getDebugLevel());

		assertEquals(1,((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Sum")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Gain")).getExecutionOrder());

		assertEquals(2,((Block)sequencer.getElemByName("Unit Delay")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Unit Delay1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(0,((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Sign1")).getExecutionOrder());
		assertEquals(0,((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

		lastExecutionOrder = BlockSequencerUtilities.propagate(first.get(2),sequencer.getHighLevelBlock(),lastExecutionOrder, sequencer.getDebugLevel());

		assertEquals(1,((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(5,((Block)sequencer.getElemByName("Sum")).getExecutionOrder());
		assertEquals(6,((Block)sequencer.getElemByName("Gain")).getExecutionOrder());

		assertEquals(2,((Block)sequencer.getElemByName("Unit Delay")).getExecutionOrder());
		assertEquals(3,((Block)sequencer.getElemByName("Unit Delay1")).getExecutionOrder());
		assertEquals(7,((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(4,((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(9,((Block)sequencer.getElemByName("Sign1")).getExecutionOrder());
		assertEquals(8,((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

	}

	/**
	 * Test done with file inputFiles/sample2.gsm
	 * 
	 * Requirement : TR-SCC-003
	 * 
	 * Blocks having their attribute directFeedThrough set to "false" can be treated similarly 
	 * to source blocks and can be executed before others blocks
	 * 
	 * There isn't user priority defined in the chart
	 * 
	 * Order expected (from Simulink debugger):
	 *   0:0    'sample2/Constant' (Constant)
	 *   0:1    'sample2/Unit Delay' (UnitDelay)
	 *   0:2    'sample2/Unit Delay1' (UnitDelay)
	 *   0:3    'sample2/Product1' (Product)
	 *   0:4    'sample2/Sum' (Sum)
	 *   0:5    'sample2/Gain' (Gain)
	 *   0:6    'sample2/Product' (Product)
	 *   0:7    'sample2/Scope' (Scope)
	 *   0:8    'sample2/Sign1' (Signum)
	 *   
	 */
	
	@Test
	public void testSequenceBlock() {

		String[] args =  new String[3];

		args[0] = testFilesRoot + "inputFiles/sample2.gsm.xml";
		args[1] = "-o";
		args[2] = testFilesRoot + "outputFiles/sample2.gsm.xml";

		sequencer = new BlockSequencer(args);

		sequencer.readModel();

		if(sequencer.getSystemModel() == null)
			System.exit(0);

		BlockSequencerUtilities.sequenceBlock(sequencer.getHighLevelBlock(), sequencer.getDebugLevel());

		assertEquals(1,((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(5,((Block)sequencer.getElemByName("Sum")).getExecutionOrder());
		assertEquals(6,((Block)sequencer.getElemByName("Gain")).getExecutionOrder());

		assertEquals(2,((Block)sequencer.getElemByName("Unit Delay")).getExecutionOrder());
		assertEquals(3,((Block)sequencer.getElemByName("Unit Delay1")).getExecutionOrder());
		assertEquals(7,((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(4,((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(9,((Block)sequencer.getElemByName("Sign1")).getExecutionOrder());
		assertEquals(8,((Block)sequencer.getElemByName("Scope")).getExecutionOrder());
	}




	@Test
	public void testSortBlocks() {

		String[] args =  new String[3];

		args[0] = testFilesRoot + "inputFiles/sample5.gsm.xml";
		args[1] = "-o";
		args[2] = testFilesRoot + "outputFiles/sample5.gsm.xml";
		
		sequencer = new BlockSequencer(args);

		sequencer.readModel();

		if(sequencer.getSystemModel() == null)
			System.exit(0);

		List<Block> sorted;
		sorted = BlockSequencerUtilities.sortBlocks(sequencer.getHighLevelBlock().getBlocks());

		assertEquals(8, sorted.size());

		assertEquals("Product3", sorted.get(0).getName());
		assertEquals("Product", sorted.get(1).getName());
		assertEquals("Constant2", sorted.get(2).getName());
		assertEquals("Product2", sorted.get(3).getName());
		assertEquals("Product1", sorted.get(4).getName());
		assertEquals("Constant1", sorted.get(5).getName());
		assertEquals("Constant", sorted.get(6).getName());
		assertEquals("Scope", sorted.get(7).getName());

	}

	/**
	 * Test done with file inputFiles/sample5.gsm
	 * 
	 * Requirement : TR-SCC-006
	 * 
	 * Blocks having lower user defined priority should have lower execution order
	 * 
	 * Priority defined in simulink chart :
	 * 	'Constant2' (Constant)	:	1
	 * 	'Constant1' (Constant)	:	2
	 * 	'Product' (Product)	:	1
	 * 	'Constant' (Constant)	:	3
	 * 	'Product1' (Product)	:	2
	 * 	'Product2' (Product)	:	2
	 * 	'Product3' (Product)	:	1
	 * 	'Scope' (Scope)		:	-
	 * 
	 * Order expected (from Simulink debugger):
	 *   0:0    'untitled/Constant2' (Constant)
	 *   0:1    'untitled/Constant1' (Constant)
	 *   0:2    'untitled/Product' (Product)
	 *   0:3    'untitled/Constant' (Constant)
	 *   0:4    'untitled/Product1' (Product)
	 *   0:5    'untitled/Product2' (Product)
	 *   0:6    'untitled/Product3' (Product)
	 *   0:7    'untitled/Scope' (Scope)
	 *   
	 *   Result : execution order in SystemModel after sequencing is the same than simulink one.
	 */
	@Test
	public void testSequenceBlock2() {

		String[] args =  new String[3];

		args[0] = testFilesRoot + "inputFiles/sample5.gsm.xml";
		args[1] = "-o";
		args[2] = testFilesRoot + "outputFiles/sample5.gsm.xml";

		sequencer = new BlockSequencer(args);

		sequencer.readModel();

		if(sequencer.getSystemModel() == null)
			System.exit(0);

		BlockSequencerUtilities.sequenceBlock(sequencer.getHighLevelBlock(), sequencer.getDebugLevel());

		assertEquals(4, ((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(2, ((Block)sequencer.getElemByName("Constant1")).getExecutionOrder());
		assertEquals(1, ((Block)sequencer.getElemByName("Constant2")).getExecutionOrder());
		assertEquals(3, ((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(5, ((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(6, ((Block)sequencer.getElemByName("Product2")).getExecutionOrder());
		assertEquals(7, ((Block)sequencer.getElemByName("Product3")).getExecutionOrder());
		assertEquals(8, ((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

	}




}
