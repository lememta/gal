/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer/src/test/java/geneauto/tblocksequencer/tests/BlockSequencerTest.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2009-02-03 10:04:53 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, alain.roan@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer.tests;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.tblocksequencer.BlockSequencer;
import geneauto.tblocksequencer.BlockSequencerUtilities;



/**
 * Unit tests done with file inputFiles/sample1.gsm
 * 
 * Requirement : TR-SCC-002
 * 
 * Blocks that consume data should have their execution order higher than blocks consuming 
 * this data.
 * 
 * There isn't user priority defined in the chart
 * 
 * Order expected (from Simulink debugger):
 *   0:0    'sample1/Constant' (Constant)
 *   0:1    'sample1/Constant1' (Constant)
 *   0:2    'sample1/Product1' (Product)
 *   0:3    'sample1/Constant2' (Constant)
 *   0:4    'sample1/Product' (Product)
 *   0:5    'sample1/Product2' (Product)
 *   0:6    'sample1/Product3' (Product)
 *   0:7    'sample1/Scope' (Scope)
 * 
 * Result : execution order in SystemModel after sequencing is the same than simulink one.
 * 
 * Note : if position between blocks change (for instance moving 'Constant1' above 'Constant') 
 * execution order will change in SystemModel but not in Simulink chart, because Simulink doesn't 
 * consider position criteria in assigning execution order.
 * 
 *
 */
public class BlockSequencerTest {

	BlockSequencer sequencer;
	private String testFilesRoot = "src/test/resources/";


	@Before
	public void setUp() throws Exception {

		String[] args =  new String[3];

		args[0] = testFilesRoot + "inputFiles/sample1.gsm.xml";
		args[1] = "-o";
		args[2] = testFilesRoot + "outputFiles/tmp/sample1.gsm.xml";

		sequencer = new BlockSequencer(args);

		sequencer.readModel();
		
		if(sequencer.getSystemModel() == null)
			System.exit(0);


	}
	
	@Test
	public void testReadModel() {

		assertEquals("error reading input model","GASystemModel", sequencer.getSystemModel().getModelType());


	}
	
	@Test
	public void testGetLowerAssignPriority() {
		Block lowerAP;
		lowerAP = BlockSequencerUtilities.getLowerAssignPriority(sequencer.getHighLevelBlock().getBlocks());

		assertEquals("Constant",lowerAP.getName());		
	}

	 
	
	@Test
	public void testGetLowerUserPriority() {
		List<Block> lowerUP;
		lowerUP = BlockSequencerUtilities.getLowerUserPriority(sequencer.getHighLevelBlock().getBlocks());

		assertEquals(8, lowerUP.size());		
	}

	
	

	 
	
	@Test
	public void testGetBlocks() {

		assertEquals(8, sequencer.getHighLevelBlock().getBlocks().size());
		
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Constant")));
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Constant1")));
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Constant2")));
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Product")));
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Product1")));
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Product2")));
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Product3")));
		assertEquals(true,sequencer.getHighLevelBlock().getBlocks().contains((Block)sequencer.getElemByName("Scope")));

	}

	@Test
	public void testSortBlocks() {

		List<Block> sorted;
		sorted = BlockSequencerUtilities.sortBlocks(sequencer.getHighLevelBlock().getBlocks());

		assertEquals(8, sorted.size());

		assertEquals("Constant", sorted.get(0).getName());
		assertEquals("Product2", sorted.get(1).getName());
		assertEquals("Product1", sorted.get(2).getName());
		assertEquals("Constant1", sorted.get(3).getName());

		assertEquals("Product3", sorted.get(4).getName());
		assertEquals("Scope", sorted.get(5).getName());
		assertEquals("Product", sorted.get(6).getName());
		assertEquals("Constant2", sorted.get(7).getName());

	}

	@Test
	public void testNextBlocks() {

		List<Block> next;
		next = BlockSequencerUtilities.nextBlocks((Block)sequencer.getElemByName("Product"),sequencer.getHighLevelBlock());

		assertEquals(2, next.size());
		assertEquals("Product2", next.get(0).getName());
		assertEquals("Product3", next.get(1).getName());

	}


	@Test
	public void testPreviousBlocks() {
		List<Block> previous;
		previous = BlockSequencerUtilities.previousBlocks((Block)sequencer.getElemByName("Product1"),sequencer.getHighLevelBlock());

		assertEquals(2, previous.size());
		assertEquals("Constant", previous.get(0).getName());
		assertEquals("Constant1", previous.get(1).getName());

	}


	@Test
	public void testNextNotOrderedBlocks() {

		//initialise execution order of next block of "Product"
		Block prod2 = (Block)sequencer.getElemByName("Product2");
		prod2.setExecutionOrder(1);

		List<Block> next;
		next = BlockSequencerUtilities.nextNotOrderedBlocks((Block)sequencer.getElemByName("Product"),sequencer.getHighLevelBlock());

		assertEquals(1, next.size());
		assertEquals("Product3", next.get(0).getName());
	}


	@Test
	public void testGetFirstBlock() {

		List<Block> first;
		first = BlockSequencerUtilities.getFirstBlock(sequencer.getHighLevelBlock());

		assertEquals(3, first.size());
		assertEquals("Constant",first.get(0).getName());
		assertEquals("Constant1",first.get(1).getName());
		assertEquals("Constant2",first.get(2).getName());

	}

	@Test
	public void testPropagate() {

		int lastExecutionOrder;
		
		List<Block> first;
		first = BlockSequencerUtilities.getFirstBlock(sequencer.getHighLevelBlock());

		lastExecutionOrder = BlockSequencerUtilities.propagate(first.get(0),sequencer.getHighLevelBlock(),1, sequencer.getDebugLevel());

		assertEquals(2, lastExecutionOrder);
		
		assertEquals(1, ((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Constant1")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Constant2")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(0, ((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Product2")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Product3")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

		lastExecutionOrder = BlockSequencerUtilities.propagate(first.get(1),sequencer.getHighLevelBlock(),lastExecutionOrder, sequencer.getDebugLevel());

		assertEquals(4, lastExecutionOrder);
		
		assertEquals(1, ((Block)((Block)sequencer.getElemByName("Constant"))).getExecutionOrder());
		assertEquals(2, ((Block)sequencer.getElemByName("Constant1")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Constant2")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(3, ((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Product2")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Product3")).getExecutionOrder());
		assertEquals(0, ((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

		lastExecutionOrder = BlockSequencerUtilities.propagate(first.get(2),sequencer.getHighLevelBlock(),lastExecutionOrder, sequencer.getDebugLevel());

		assertEquals(9, lastExecutionOrder);
		
		assertEquals(1, ((Block)((Block)sequencer.getElemByName("Constant"))).getExecutionOrder());
		assertEquals(2, ((Block)sequencer.getElemByName("Constant1")).getExecutionOrder());
		assertEquals(4, ((Block)sequencer.getElemByName("Constant2")).getExecutionOrder());
		assertEquals(5, ((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(3, ((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(6, ((Block)sequencer.getElemByName("Product2")).getExecutionOrder());
		assertEquals(7, ((Block)sequencer.getElemByName("Product3")).getExecutionOrder());
		assertEquals(8, ((Block)sequencer.getElemByName("Scope")).getExecutionOrder());


	}
	 

	@Test
	public void testSequenceBlock() {

	    BlockSequencerUtilities.sequenceBlock(sequencer.getHighLevelBlock(), sequencer.getDebugLevel());

		assertEquals(1, ((Block)sequencer.getElemByName("Constant")).getExecutionOrder());
		assertEquals(2, ((Block)sequencer.getElemByName("Constant1")).getExecutionOrder());
		assertEquals(4, ((Block)sequencer.getElemByName("Constant2")).getExecutionOrder());
		assertEquals(5, ((Block)sequencer.getElemByName("Product")).getExecutionOrder());

		assertEquals(3, ((Block)sequencer.getElemByName("Product1")).getExecutionOrder());
		assertEquals(6, ((Block)sequencer.getElemByName("Product2")).getExecutionOrder());
		assertEquals(7, ((Block)sequencer.getElemByName("Product3")).getExecutionOrder());
		assertEquals(8, ((Block)sequencer.getElemByName("Scope")).getExecutionOrder());

	}

	

	@Test
	public void testWriteModel() {
		
	    BlockSequencerUtilities.sequenceBlock(sequencer.getHighLevelBlock(), sequencer.getDebugLevel());
		sequencer.writeModel();
		
		
	}









	

}
