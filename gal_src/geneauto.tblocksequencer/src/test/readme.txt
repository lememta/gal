author : Fabien Castel
date : 09/04/2008

--------------------------------------------------------------------------------------------------------------
Sample1

Requirement : TR-SCC-002
Blocks that consume data should have their execution order higher than blocks consuming this data.

There isn't user priority defined in the chart

Order expected (from Simulink debugger):
  0:0    'sample1/Constant' (Constant)
  0:1    'sample1/Constant1' (Constant)
  0:2    'sample1/Product1' (Product)
  0:3    'sample1/Constant2' (Constant)
  0:4    'sample1/Product' (Product)
  0:5    'sample1/Product2' (Product)
  0:6    'sample1/Product3' (Product)
  0:7    'sample1/Scope' (Scope)

Note : It's necessary to set manually assigned priority. This information will be set by preprocessor in the
future

Result : execution order in SystemModel after sequencing is the same than simulink one.

Note : if position between blocks change (for instance moving 'Constant1' above 'Constant') execution order 
will change in SystemModel but not in Simulink chart, because Simulink doesn't consider position criteria in
assigning execution order.

--------------------------------------------------------------------------------------------------------------
Sample2

Requirement : TR-SCC-003
Blocks having their attribute directFeedThrough set to "false" can be treated similarly to source blocks and
can be executed before others blocks

There isn't user priority defined in the chart

Order expected (from Simulink debugger):
  0:0    'sample2/Constant' (Constant)
  0:1    'sample2/Unit Delay' (UnitDelay)
  0:2    'sample2/Unit Delay1' (UnitDelay)
  0:3    'sample2/Product1' (Product)
  0:4    'sample2/Sum' (Sum)
  0:5    'sample2/Gain' (Gain)
  0:6    'sample2/Product' (Product)
  0:7    'sample2/Scope' (Scope)
  0:8    'sample2/Sign1' (Signum)

--------------------------------------------------------------------------------------------------------------
Sample3

Requirement : TR-SCC-004

TODO

--------------------------------------------------------------------------------------------------------------
Sample4

Requirement : TR-SCC-005

TODO

--------------------------------------------------------------------------------------------------------------
Sample5

Requirement : TR-SCC-006
Blocks having lower user defined priority should have lower execution order

Priority defined in simulink chart :
'Constant2' (Constant)	:	1
'Constant1' (Constant)	:	2
'Product' (Product)	:	1
'Constant' (Constant)	:	3
'Product1' (Product)	:	2
'Product2' (Product)	:	2
'Product3' (Product)	:	1
'Scope' (Scope)		:	-

Order expected (from Simulink debugger):
  0:0    'untitled/Constant2' (Constant)
  0:1    'untitled/Constant1' (Constant)
  0:2    'untitled/Product' (Product)
  0:3    'untitled/Constant' (Constant)
  0:4    'untitled/Product1' (Product)
  0:5    'untitled/Product2' (Product)
  0:6    'untitled/Product3' (Product)
  0:7    'untitled/Scope' (Scope)

Note : It's necessary to set manually user and assigned priority. This information will be set by importer
and preprocessor in the future

Result : execution order in SystemModel after sequencing is the same than simulink one.

--------------------------------------------------------------------------------------------------------------
Sample6

Requirement : TR-SCC-003
Blocks having their atribute directFeedThrough should have their execution order lower than the blocks 
consuming the data their provide.

Order from Simulink debugger :
  0:0    'sample6/Unit Delay' (UnitDelay)
  0:1    'sample6/Scope' (Scope)
  0:2    'sample6/Unit Delay1' (UnitDelay)
  0:3    'sample6/Unit Delay2' (UnitDelay)

Order expected (from requirements) :
1    'Unit Delay2' (UnitDelay)  
2    'Unit Delay1' (UnitDelay)
3    'Unit Delay' (UnitDelay)
4    'Scope' (Scope)

  
Note : It's necessary to set manually user and assigned priority. This information will be set by importer
and preprocessor in the future

Result : execution order in SystemModel conform to the requirement.

