/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer/src/main/java/geneauto/tblocksequencer/BlockSequencerUtilities.java,v $
 *  @version	$Revision: 1.28 $
 *	@date		$Date: 2009-02-03 10:04:52 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, alain.roan@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventHandler.EventLevel;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains utilities methods in order to sequence a GASystemModel.
 * 
 */
public class BlockSequencerUtilities {

	/**
	 * Main recursive algorithm which sequence all the blocks by calling
	 * propagation function on each starting blocks
	 * 
	 * @param SystemBlock
	 *            defining the global system in which sequencing process is
	 *            done.
	 * @param debugLevel
	 *            The current log level.
	 */
	public static void sequenceBlock(SystemBlock systemBlock,
			EventLevel debugLevel) {

		List<Block> startingBlock;
		startingBlock = getFirstBlock(systemBlock);

		// index of the block handled in the starting blocks array
		int index = 0;

		// variable to save last execution order given to a block
		int executionOrder = 1;

		boolean stopSequence = false;
		boolean allNonExecutable = true;

		while ((!startingBlock.isEmpty()) & !stopSequence) {

			// if all the starting blocks aren't executable, stop the process
			for (Block block : startingBlock) {
				if (isExecutable(block, systemBlock)) {
					allNonExecutable = false;
					break;
				}
			}
			if (allNonExecutable == true)
				stopSequence = true;

			// start propagation from first executable starting block
			if (isExecutable(startingBlock.get(index), systemBlock)) {
				executionOrder = propagate(startingBlock.get(index),
						systemBlock, executionOrder, debugLevel);

				// remove the block
				startingBlock.remove(index);
				if (index >= startingBlock.size())
					index = 0;

			} else {
				// if block isn't yet executable, go to the following
				// (for instance it has a control signal output and target block
				// isn't yet executable)
				if (index == startingBlock.size()) {
					index = 0;
				} else
					index++;
			}
		}
	}

	/**
	 * Return all the blocks that have no input port and block that have
	 * "directFeedThrough attribute = false". They are potential starting blocks
	 * to beginning the sequencing. Blocks in the result list are ordered
	 * referring to their assignedPriority and userPriority attributes
	 * 
	 * @param SystemBlock
	 *            defining the system in which first blocks are sought
	 */
	public static List<Block> getFirstBlock(SystemBlock systemBlock) {

		List<Block> blocks = systemBlock.getBlocks();

		List<Block> result = new ArrayList<Block>();
		List<Block> firstBlocks = new ArrayList<Block>();

		for (Block block : blocks) {

			// if block hasn't input neither output
			if ((block.getInDataPorts().size() == 0)
					&& (block.getOutDataPorts().size() == 0)) {
				result.add(block);
			}
			// if block hasn't input or has directFeedThrough set to false
			// TODO inControlPorts are generally not an input for a block
			else if (((!block.isDirectFeedThrough()) 
					|| ((block.getInDataPorts().size() == 0) 
						&& (block.getInControlPorts().size() == 0)))) {
				firstBlocks.add(block);
			}
		}

		result = sortBlocks(result);

		result.addAll(sortBlocks(firstBlocks));

		return result;
	}

	/**
	 * Check if all the blocks related to the inputs of the current block are
	 * sequenced. If true, execution order assignment propagation may continue.
	 * If false, blocks before current block must be ordered.
	 * 
	 * @param Block
	 *            that has to be checked
	 * @param SystemBlock
	 *            defining the system which block belong to
	 */
	public static boolean isExecutable(Block block, SystemBlock systemBlock) {

		// if block has directFeedThrough=false is executable
		if (!block.isDirectFeedThrough()) {

			// check if block hasn't previous unsequenced blocks with
			// directFeedThrough=false
			for (Block b : previousBlocks(block, systemBlock)) {
				if ((!b.isDirectFeedThrough()) & (b.getExecutionOrder() <= 0)) {
					return false;
				}
			}
			return true;
		}

		// if block is a control block, check target controlled blocks
		if (block.getOutControlPorts().size() > 0) {
			for (Block b : getBlocksControlled(block, systemBlock)) {
				if (!isExecutable(b, systemBlock)) {
					return false;
				}
			}
		}

		// check if block has out data ports linked to in control port
		for (OutDataPort p : block.getOutDataPorts()) {
			// browsing all the signals in the system
			for (Signal s : systemBlock.getSignals()) {
				// if signal come from current port
				if (s.getSrcPort().getId() == p.getId()) {
					// get the destination port
					Inport inp = s.getDstPort();
					if ((inp instanceof InControlPort)
							|| (inp instanceof InEnablePort)
							|| (inp instanceof InEdgeEnablePort)) {
						// get the block carrying the in control port
						Block b = (Block) inp.getParent();
						if (!isExecutable(b, systemBlock)) {
							return false;
						}
					}
				}
			}
		}

		for (Block b : previousBlocks(block, systemBlock)) {

			if ((b.getExecutionOrder() == 0) 
					&& (b.getInControlPorts().size() == 0)
					&& (b.getInEdgeEnablePort() == null)
					&& (b.getInEnablePort() == null)) {
				return false;
			} else if (
					((b.getInControlPorts().size() > 0)
						|| (b.getInEdgeEnablePort() != null) 
						|| (b.getInEnablePort() != null))
					&& (!isExecutable(b, systemBlock))) {
				return false;
			}
		}

		// if all the test are passed, block is executable
		return true;
	}

	/**
	 * Return all the next blocks of the parameter block. Blocks in the result
	 * list are ordered referring to their assignedPriority and userPriority
	 * attributes
	 * 
	 * @param Origin
	 *            block
	 * @param SystemBlock
	 *            defining the system in which next blocks are sought
	 */
	public static List<Block> nextBlocks(Block block, SystemBlock systemBlock) {

		// get all the blocks in the system
		List<Block> blocks;
		blocks = systemBlock.getBlocks();

		// get all the signals in the system
		List<Signal> signals;
		signals = systemBlock.getSignals();

		// get the output ports of the blocks
		List<OutDataPort> outPorts;
		outPorts = block.getOutDataPorts();

		// get the signals linked to the output ports
		ArrayList<Signal> linkedSignals;
		linkedSignals = new ArrayList<Signal>();

		for (Signal signal : signals) {
			if (outPorts.contains(signal.getSrcPort())) {
				linkedSignals.add(signal);
			}
		}

		// get the input ports linked to the signals
		ArrayList<InDataPort> inPorts;
		inPorts = new ArrayList<InDataPort>();

		for (Signal signal : linkedSignals) {
			if (!inPorts.contains(signal.getDstPort())) {
				if (signal.getDstPort() instanceof InDataPort) {
					inPorts.add((InDataPort) signal.getDstPort());
				}
			}
		}

		// get the blocks linked to the input ports
		ArrayList<Block> nextBlocks;
		nextBlocks = new ArrayList<Block>();

		for (Block b : blocks) {
			for (InDataPort port : inPorts) {
				if ((b.getInDataPorts().contains(port))
						& (!nextBlocks.contains(b)))
					// if the block is already in the result list, it's not
					// added
					nextBlocks.add(b);
			}
		}

		return sortBlocks(nextBlocks);
	}

	/**
	 * Return all the previous blocks of the parameter block, through data
	 * signals. Blocks in the result list are ordered referring to their
	 * assignedPriority and userPriority attributes
	 * 
	 * @param Origin
	 *            block
	 * @param SystemBlock
	 *            defining the system in which previous blocks are sought
	 */
	public static List<Block> previousBlocks(Block block,
			SystemBlock systemBlock) {

		// get all the blocks in the system
		List<Block> blocks;
		blocks = systemBlock.getBlocks();

		// get all the signals in the system
		List<Signal> signals;
		signals = systemBlock.getSignals();

		// get the input ports of the blocks
		List<InDataPort> inPorts;
		inPorts = block.getInDataPorts();

		// get the signals linked to the input ports
		ArrayList<Signal> linkedSignals;
		linkedSignals = new ArrayList<Signal>();

		for (Signal signal : signals) {
			if (inPorts.contains(signal.getDstPort())) {
				linkedSignals.add(signal);
			}
		}

		// get the output ports linked to the signals
		ArrayList<OutDataPort> outPorts;
		outPorts = new ArrayList<OutDataPort>();

		for (Signal signal : linkedSignals) {
			if (!outPorts.contains(signal.getSrcPort()))
				outPorts.add((OutDataPort) signal.getSrcPort());
		}

		// get the blocks linked to the output ports
		ArrayList<Block> prevBlocks;
		prevBlocks = new ArrayList<Block>();

		for (Block b : blocks) {
			for (OutDataPort port : outPorts) {
				// if the block is already in the result list, it's not added
				if ((b.getOutDataPorts().contains(port))
						& (!prevBlocks.contains(b)))
					prevBlocks.add(b);
			}
		}

		return sortBlocks(prevBlocks);

	}

	/**
	 * return the blocks controlled by parameter block Blocks in the result list
	 * are ordered referring to their assignedPriority and userPriority
	 * attributes
	 * 
	 * @param Control
	 *            block
	 * @param SystemBlock
	 *            defining the system in which controlled blocks are sought
	 */
	public static List<Block> getBlocksControlled(Block block,
			SystemBlock systemBlock) {

		ArrayList<Block> controlledBlocks;
		controlledBlocks = new ArrayList<Block>();

		// get all the blocks in the system
		List<Block> blocks;
		blocks = systemBlock.getBlocks();

		// get all the signals in the system
		List<Signal> signals;
		signals = systemBlock.getSignals();

		// get the output ports of the blocks
		List<Outport> outPorts = new ArrayList<Outport>();
		outPorts.addAll(block.getOutControlPorts());
		outPorts.addAll(block.getOutDataPorts());

		// if block hasn't out control port, return an empty array
		if (outPorts.isEmpty()) {
			return controlledBlocks;
		}

		// get the signals linked to the output control ports
		ArrayList<Signal> linkedSignals;
		linkedSignals = new ArrayList<Signal>();

		for (Signal signal : signals) {
			if (outPorts.contains(signal.getSrcPort())) {
				linkedSignals.add(signal);
			}
		}

		// get the input control ports linked to the signals
		ArrayList<Inport> inPorts;
		inPorts = new ArrayList<Inport>();

		for (Signal signal : linkedSignals) {
			if (!inPorts.contains(signal.getDstPort())) {
				if (signal.getDstPort() instanceof InControlPort) {
					inPorts.add((InControlPort) signal.getDstPort());
				}
			}
		}

		// get the blocks linked to the input control ports
		//TODO Refine the following code - (edge)enable ports are not control ports. (VaKo 18.08.2008) 
		for (Block b : blocks) {
			for (Inport port : inPorts) {
				if (!controlledBlocks.contains(b)) {
					if (b.getInControlPorts().size() > 0) {
						for (InControlPort icp : b.getInControlPorts()) {
							 if (icp.getId() == port.getId()) {
								controlledBlocks.add(b);
							}
						}
					} else if (b.getInEnablePort() != null
							&& b.getInEnablePort().getId() == port.getId()) {
						controlledBlocks.add(b);
					} else if (b.getInEdgeEnablePort() != null
							&& b.getInEdgeEnablePort().getId() == port.getId()) {
						controlledBlocks.add(b);
					}
				}
			}
		}

		return sortBlocks(controlledBlocks);
	}

	/**
	 * return the control blocks of the parameter block
	 * 
	 * 
	 * @param Controlled
	 *            block
	 * @param SystemBlock
	 *            defining the system in which control blocks are sought
	 */
	public static List<Block> getControlBlock(Block block,
			SystemBlock systemBlock) {

		ArrayList<Block> controlBlocks = new ArrayList<Block>();

		// get all the blocks in the system
		List<Block> blocks;
		blocks = systemBlock.getBlocks();

		// get all the signals in the system
		List<Signal> signals;
		signals = systemBlock.getSignals();

		// get the input control port of the blocks
		Inport inPort = null;

		//TODO Refine the following code - there could be simultaneously InEnable and InEdgeEnable or several InControlPorts. (edge)enable ports are not control ports. (VaKo 18.08.2008) 
		if (block.getInControlPorts().size() > 0) {
			//TODO Proper list handling to be added here
			inPort = block.getInControlPorts().get(0);
		}
		if (block.getInEnablePort() != null) {
			inPort = block.getInEnablePort();
		}
		if (block.getInEdgeEnablePort() != null) {
			inPort = block.getInEdgeEnablePort();
		}

		// if block hasn't in control port, return an empty array
		if (inPort == null) {
			return controlBlocks;
		}

		// get the signals linked to the input control ports
		ArrayList<Signal> linkedSignals;
		linkedSignals = new ArrayList<Signal>();

		for (Signal signal : signals) {
			if (inPort.getId() == signal.getDstPort().getId()) {
				linkedSignals.add(signal);
			}
		}

		// get the output control ports linked to the signals
		List<Outport> outPorts = new ArrayList<Outport>();

		for (Signal signal : linkedSignals) {
			if (!outPorts.contains(signal.getSrcPort())) {
				if (signal.getSrcPort() instanceof Outport) {
					outPorts.add((Outport) signal.getSrcPort());
				}
			}
		}

		// get the blocks linked to the output control ports

		for (Block b : blocks) {
			for (Outport port : outPorts) {
				// if the block is already in the result list, it's not added
				if (((b.getOutControlPorts().contains(port)) || (b
						.getOutDataPorts().contains(port)))
						&& (!controlBlocks.contains(b)))
					controlBlocks.add(b);
			}
		}
		return sortBlocks(controlBlocks);

	}

	/**
	 * Return all the next blocks of the parameter block that haven't been
	 * sequenced. Blocks in the result list are ordered referring to their
	 * assignedPriority and userPriority attributes.
	 * 
	 * @param Origin
	 *            block
	 * @param SystemBlock
	 *            defining the system in which next blocks are sought
	 */
	public static List<Block> nextNotOrderedBlocks(Block block,
			SystemBlock systemBlock) {

		List<Block> result;
		result = new ArrayList<Block>();

		List<Block> blocks;
		blocks = nextBlocks(block, systemBlock);

		// adding block through data signal
		for (Block b : blocks) {
			if (b.getExecutionOrder() == 0)
				result.add(b);
		}

		// adding blocks through control signal
		for (Block b : getBlocksControlled(block, systemBlock)) {
			if (b.getExecutionOrder() == 0)
				result.add(b);
		}

		return sortBlocks(result);
	}

	/**
	 * Get the blocks in the list having lower user defined priority different
	 * than 0
	 * 
	 * @param List
	 */
	public static List<Block> getLowerUserPriority(List<Block> blocks) {

		List<Block> lowerUPblocks;
		lowerUPblocks = new ArrayList<Block>();

		int lowerPriority = Integer.MAX_VALUE;

		if (blocks.isEmpty())
			return null;

		// if all the block have a 0 userPriority value, return blocks
		boolean allZero = true;

		for (Block block : blocks) {
			if (block.getUserDefinedPriority() > 0)
				allZero = false;
		}

		if (allZero)
			return blocks;

		for (Block block : blocks) {
			if (block.getUserDefinedPriority() != 0) {
				if (block.getUserDefinedPriority() == lowerPriority) {
					lowerUPblocks.add(block);
				} else if (block.getUserDefinedPriority() < lowerPriority) {
					lowerUPblocks.clear();
					lowerUPblocks.add(block);
					lowerPriority = block.getUserDefinedPriority();
				}
			}
		}

		return lowerUPblocks;
	}

	/**
	 * Get the block in the list blocks having lower assigned priority
	 * 
	 * @param List
	 */
	public static Block getLowerAssignPriority(List<Block> blocks) {

		if (blocks.isEmpty())
			return null;

		Block result;
		result = blocks.get(0);

		for (Block block : blocks) {
			if (block.getAssignedPriority() < result.getAssignedPriority()) {
				result = block;
			}
		}
		return result;
	}

	/**
	 * Sort a list of blocks referring first to their userPriority attribute and
	 * then to their assignedPriority attribute
	 * 
	 * @param List
	 *            that has to be sorted
	 */
	public static List<Block> sortBlocks(List<Block> blocks) {

		List<Block> aux;
		aux = blocks;

		List<Block> result;
		result = new ArrayList<Block>();

		List<Block> lowerUPblocks;

		Block blockAux;

		while (!aux.isEmpty()) {
			lowerUPblocks = getLowerUserPriority(aux);
			while (!lowerUPblocks.isEmpty()) {
				blockAux = getLowerAssignPriority(lowerUPblocks);

				result.add(blockAux);

				lowerUPblocks.remove(blockAux);
				aux.remove(blockAux);
			}
		}
		return result;
	}

	/**
	 * Recursive function that set executionOrder attribute of the parameter
	 * block and propagate the sequencing to his following blocks.
	 * 
	 * @param Starting
	 *            block for recursive propagation.
	 * @param SystemBlock
	 *            defining the system in which propagation is done.
	 * @param Execution
	 *            order value to give to the starting block.
	 * @param debugLevel
	 *            The current log level.
	 * @return Last execution order value given to a block.
	 */
	public static int propagate(Block block, SystemBlock systemBlock,
			int currentExecutionOrder, EventLevel debugLevel) {

		int executionOrder = currentExecutionOrder;

		// if the block have been already handled by sequencing, do nothing
		// if the block isn't executable, do nothing
		if ((block.getExecutionOrder() == 0)
				&& (isExecutable(block, systemBlock))) {

			// control subsystem case : recursive call to method sequenceBlock
			// to sequence into the system block
			//TODO This might be wrong! InEnablePort or InEdgeEnablePort's are not control ports! (VaKo 18.08.2008) 
			if ((block instanceof SystemBlock)
					&& ((block.getInControlPorts().size() > 0)
							|| (block.getInEnablePort() != null) 
							|| (block.getInEdgeEnablePort() != null))) {

				// if control blocks have not been sequenced, don't sequence yet
				// the inside sub-system
				for (Block b : getControlBlock(block, systemBlock)) {
					if (b.getExecutionOrder() == 0) {

						executionOrder = propagate(b, systemBlock,
								executionOrder, debugLevel);

					}
				}

				// execution order of a controlled block should be -1
				setExecutionOrder(block, -1, debugLevel);

				// triggered subsystem
				try {
					sequenceBlock((SystemBlock) block, debugLevel);
				} catch (java.lang.IllegalArgumentException e) {
					EventHandler
							.handle(
									EventLevel.ERROR,
									"BlockSequencerUtilities",
									"",
									Messages
											.getString("BlockSequencerUtilities.CannotHandleSubSys") + block.getExternalID(), //$NON-NLS-1$
									Messages
											.getString("BlockSequencerUtilities.CannotHandleSubSysDetailled")); //$NON-NLS-1$
				}
				// call recursively for each following block not yet sequenced
				/*
				 * for (Block next : nextNotOrderedBlocks(block, systemBlock)) {
				 * executionOrder = propagate(next, systemBlock, executionOrder,
				 * debugLevel); }
				 */
			} else if ((block instanceof SystemBlock)
					&& (block.isVirtual() == false)) {
				// subsystem non virtual

				// sequence inside system
				try {
					sequenceBlock((SystemBlock) block, debugLevel);
				} catch (java.lang.IllegalArgumentException e) {
					EventHandler
							.handle(
									EventLevel.ERROR,
									"BlockSequencerUtilities",
									"",
									Messages
											.getString("BlockSequencerUtilities.CannotHandleSubSys") + block.getExternalID(), //$NON-NLS-1$
									Messages
											.getString("BlockSequencerUtilities.CannotHandleSubSysDetailled")); //$NON-NLS-1$
				}
				// set execution order
				setExecutionOrder(block, executionOrder, debugLevel);
				executionOrder++;

			} else if ((block instanceof SystemBlock)
					&& (block.isVirtual() == true)) {
				EventHandler
						.handle(
								EventLevel.ERROR,
								"BlockSequencerUtilities",
								"BSE0003",
								Messages
										.getString("BlockSequencerUtilities.Block") + block.getName() + Messages.getString("BlockSequencerUtilities.SubSysBlock"), //$NON-NLS-1$ //$NON-NLS-2$
								Messages
										.getString("BlockSequencerUtilities.SubSysBlockDetailled")); //$NON-NLS-1$
			}
			// Simple data block case
			//TODO This might be wrong! A block cannot have simultaneously an (edge)enable and control blocks. (VaKo 18.08.2008) 
			else if ((block.getInControlPorts().size() == 0)
					&& (block.getInEnablePort() == null)
					&& (block.getInEdgeEnablePort() == null)) {
				setExecutionOrder(block, executionOrder, debugLevel);
				executionOrder++;
			}
			// Controlled block case
			else {
				// execution order of a controlled block should be -1
				setExecutionOrder(block, -1, debugLevel);

				// call recursively propagate for each control block
				for (Block control : getControlBlock(block, systemBlock)) {
					if (control.getExecutionOrder() != 0) {
						executionOrder = propagate(control, systemBlock,
								executionOrder, debugLevel);
					}
				}
			}

			// call recursively for each following block not yet sequenced
			for (Block next : nextNotOrderedBlocks(block, systemBlock)) {
				executionOrder = propagate(next, systemBlock, executionOrder,
						debugLevel);
			}

		}
		// return the execution order for the following of the current block
		return executionOrder;
	}

	/**
	 * Give to block an executionOrder value, according to oldestTime attribute,
	 * and update current value of oldestTime attribute.
	 * 
	 * @param Block
	 *            which execution order must be set.
	 * @param Value
	 *            to give to execution order of the block.
	 * @param debugLevel
	 *            the current log level.
	 */
	private static void setExecutionOrder(Block block, int value,
			EventLevel debugLevel) {

		if (block.getExecutionOrder() == 0) {
			block.setExecutionOrder(value);
		}

		EventHandler
				.handle(
						EventLevel.DEBUG,
						"BlockSequencerUtilities",
						"",
						Messages.getString("BlockSequencerUtilities.Block") + block.getName() //$NON-NLS-1$
								+ Messages
										.getString("BlockSequencerUtilities.HasBeenSequenced") //$NON-NLS-1$
								+ value,
						Messages
								.getString("BlockSequencerUtilities.HasBeenSequencedDetailled")); //$NON-NLS-1$

	}

}
