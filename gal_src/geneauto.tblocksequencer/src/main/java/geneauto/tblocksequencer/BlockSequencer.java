/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tblocksequencer/src/main/java/geneauto/tblocksequencer/BlockSequencer.java,v $
 *  @version	$Revision: 1.42 $
 *	@date		$Date: 2009-02-03 10:04:52 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, alain.roan@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tblocksequencer;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventHandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Principal class for BlockSequencer tool
 * 
 *
 */
public class BlockSequencer {

	/**
	 * Input SystemModel in which executionOrder parameters must be defined
	 */
	private Model systemModel;

	/**
	 * Input XML SystemModel file without block sequencing
	 */
	private String inputFile;

	/**
	 * Directory where output XML SystemModel file with block sequencing must be
	 * wrote
	 */
	private String outputFile;

	/**
	 * Level of debug fixed by the user
	 */
	private EventHandler.EventLevel debugLevel;

	/**
	 * Block library XML file where are stored all the description of the
	 * blocks. Useful to read the input model using ModelFactory tool.
	 */
	private String libraryFile;

	/**
	 * The model factory instance of the block sequencer elementary tool.
	 */
	private ModelFactory factory;

	/**
	 * The library factory instance of the block sequencer elementary tool.
	 */
	private BlockLibraryFactory libraryFactory;

	/**
	 * Constructor of the class
	 * 
	 * @param libraryFile
	 * @param debugLevel
	 * @param outputFile
	 * @param inputFile
	 */
	public BlockSequencer(String[] args) {

		// set root of all location strings in messages
		EventHandler.setToolName("TBlockSequencer");

		// clean argumentsreader form old values
		// this is required because static classes are shared between different
		// tools)
		ArgumentReader.clear();

		ArgumentReader.addArgument("", GAConst.ARG_INPUTFILE, true, true,
				GAConst.ARG_INPUTFILE_NAME_ROOT);
		ArgumentReader.addArgument("-o", GAConst.ARG_OUTPUTFILE, false, true,
				GAConst.ARG_OUTPUTFILE_NAME_ROOT);
		ArgumentReader.addArgument("-b", GAConst.ARG_LIBFILE, false, true,
				GAConst.ARG_LIBFILE_NAME_ROOT);

		// parse command line
		ArgumentReader.setArguments(args);

		// Initialize location of temporary folder and log writing
		ArgumentReader.initFilePathsAndLog(true, false);

		// TODO: set the version number of the elementary tool machine
		String version = "DEV";
		String releaseDate = "";

		String toolClassPath = "geneauto/tblocksequencer/BlockSequencer.class";
		URL url = this.getClass().getClassLoader().getResource(toolClassPath);

		version = FileUtil.getVersionFromManifest(url, toolClassPath);
		releaseDate = FileUtil.getDateFromManifest(url, toolClassPath);

		// message about starting the tool
		EventHandler.handle(EventLevel.INFO, "", "GI0002",
				"Elementary tool ver. " + version + " " + releaseDate
						+ " started at "
						+ EventHandler.getDateTimeForMessage()
						+ "\n\t (command line: "
						+ ArgumentReader.getArgumentString() + ")", "");

		// read required parameters from argumentreader
		this.inputFile = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);
		// to be replaced with -o option value
		this.outputFile = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFILE);
		this.libraryFile = ArgumentReader.getParameter(GAConst.ARG_LIBFILE);

		// check if the input files are readable and outputs writeable

		FileUtil.assertCanRead(inputFile);

		if (libraryFile != null) {
			FileUtil.assertCanRead(libraryFile);
		}
		FileUtil.assertCanWrite(outputFile);

		// this is actually not required -- EventsHandler is static and has
		// logLEvel attribute,
		// we do not need to feed the loglevel with each call to its members
		// TODO: refactor utilities that take debuglevel as an attribute
		this.debugLevel = EventHandler.getLogLevel();
	}

	/**
	 * Getter to the systemModel attribute
	 */
	public Model getSystemModel() {
		return systemModel;
	}

	/**
	 * Getter to the inputFile attribute
	 */
	public String getInputFile() {
		return inputFile;
	}

	/**
	 * Getter to the debugLevel attribute
	 */
	public EventHandler.EventLevel getDebugLevel() {
		return debugLevel;
	}

	/**
	 * Call the modelFactory tool to read the XML input file
	 */
	public void readModel() {

		getFactory().readModel();

		systemModel = getFactory().getModel();

	}

	/**
	 * Call the modelFactory to write model to XML file in directory defined by
	 * outputFile parameter.
	 */
	public void writeModel() {

		getFactory().setModel(systemModel);

		getFactory().writeModel();
	}

	/**
	 * Get all the blocks in the systemModel attribute
	 */
	public List<Block> getAllBlocks() {

		ArrayList<Block> blocks;
		blocks = new ArrayList<Block>();

		for (GAModelElement elem : systemModel.getAllElements()) {
			if (elem instanceof Block) {
				blocks.add((Block) elem);
			}
		}
		return blocks;
	}

	/**
	 * Get the block with highest level in the systemModel attribute (the one
	 * with no parent)
	 */
	public SystemBlock getHighLevelBlock() {
		SystemBlock parent;
		parent = new SystemBlock();

		for (GAModelElement elem : systemModel.getAllElements()) {
			if ((elem instanceof Block) & (elem.getParent() == null)) {
				parent = (SystemBlock) elem;
			}
		}
		return parent;
	}

	/**
	 * Get the element corresponding to the given id in the systemModel
	 * attribute
	 * 
	 * @param Id
	 *            of the expected element
	 */
	public GAModelElement getElemById(int id) {

		for (GAModelElement elem : systemModel.getAllElements()) {
			if (elem.getId() == id) {
				return elem;
			}
		}
		return null;
	}

	/**
	 * Get the element corresponding to the given name in the systemModel
	 * attribute
	 * 
	 * @param Name
	 *            of the expected element
	 */
	public GAModelElement getElemByName(String name) {
		for (GAModelElement elem : systemModel.getAllElements()) {
			if (elem.getName() != null) {
				if (elem.getName().equals(name)) {
					return elem;
				}
			}
		}
		return null;
	}

	/**
	 * Set executionOrder attribute of all the blocks off the system with 0.
	 */
	public void initExecutionOrder() {

		// array of blocks that have already an execution order
		ArrayList<Block> blocks;
		blocks = new ArrayList<Block>();

		for (Block block : getAllBlocks()) {
			if (block.getExecutionOrder() > 0) {
				blocks.add(block);
			}
		}

		if (!blocks.isEmpty()) {

			EventHandler
					.handle(
							EventLevel.WARNING,
							"BlockSequencer",
							"BSW0001",
							Messages
									.getString("BlockSequencer.AlreadySeqenced"), //$NON-NLS-1$
							Messages
									.getString("BlockSequencer.AreadySequencedDetailled")); //$NON-NLS-1$

			for (Block block : blocks) {
				EventHandler
						.handle(
								EventLevel.WARNING,
								"BlockSequencer",
								"BSW0002",
								Messages.getString("BlockSequencer.Block") //$NON-NLS-1$
										+ block.getName()
										+ Messages
												.getString("BlockSequencer.SequencedValue") //$NON-NLS-1$
										+ block.getExecutionOrder()
										+ Messages
												.getString("BlockSequencer.ValueWillBeErased"), Messages.getString("BlockSequencer.ValueWillBeErasedDetailled")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}

		// setting all the execution order value to 0
		for (Block block : getAllBlocks()) {
			if (block.getExecutionOrder() > 0) {
				block.setExecutionOrder(0);
			}
		}
	}

	/**
	 * command line : tblocksequencer "inputfile" "library file" "outputfile"
	 * "logdirectory" loglevel
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		BlockSequencer sequencer = null;
		sequencer = new BlockSequencer(args);
		sequencer.sequence();

	}

	public boolean sequence() {

		EventHandler
				.handle(
						EventLevel.INFO,
						"BlockSequencer",
						"",
						Messages
								.getString("BlockSequencer.StartOfSequencerExec"), Messages.getString("BlockSequencer.StartOfSequencerExecDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		// reading model from input file

		EventHandler
				.handle(
						EventLevel.DEBUG,
						"BlockSequencer",
						"",
						Messages
								.getString("BlockSequencer.StartReadingInputModel") + inputFile, Messages.getString("BlockSequencer.StartReadingInputModelDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		readModel();

		EventHandler
				.handle(
						EventLevel.DEBUG,
						"BlockSequencer",
						"",
						Messages
								.getString("BlockSequencer.EndReadingInputModel") + inputFile, Messages.getString("BlockSequencer.EndReadingInputModelDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		// initializing all the execution order attributes

		initExecutionOrder();

		// sequencing blocks
		EventHandler
				.handle(
						EventLevel.INFO,
						"BlockSequencer",
						"",
						Messages
								.getString("BlockSequencer.StartSequencingModel"), Messages.getString("BlockSequencer.StartSequencingModelDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		// find the high level block
		SystemBlock parent;
		parent = new SystemBlock();

		for (Block block : getAllBlocks()) {
			if (block.getParent() == null) {
				parent = (SystemBlock) block;
			}
		}

		BlockSequencerUtilities.sequenceBlock(parent, debugLevel);

		EventHandler
				.handle(
						EventLevel.INFO,
						"BlockSequencer",
						"",
						Messages.getString("BlockSequencer.EndSequencingModel"), Messages.getString("BlockSequencer.EndSequencingModelDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		// checking if all blocks have been sequenced
		ArrayList<Block> unsequencedBlocks;
		unsequencedBlocks = new ArrayList<Block>();

		for (Block block : getAllBlocks()) {
			// block unsequenced
			if (block.getExecutionOrder() == 0) {
				// block is not a controlled block
				if ((block.getInControlPorts().size() == 0)
						&& (block.getInEnablePort() == null)
						&& (block.getInEdgeEnablePort() == null)) {

					// block has at least one input or one output
					if ((block.getInDataPorts().size() != 0)
							&& (block.getOutDataPorts().size() != 0)) {
						unsequencedBlocks.add(block);
					}
				}
			}
		}

		// if unsequenced block list is not empty, an error is raised
		for (Block block : unsequencedBlocks) {
			EventHandler
					.handle(
							EventLevel.ERROR,
							"BlockSequencer",
							"BSE0002",
							Messages
									.getString("BlockSequencer.CannotBeSequenced") + block.getName(), //$NON-NLS-1$ //$NON-NLS-2$
							Messages
									.getString("BlockSequencer.CannotBeSequencedDetailled")); //$NON-NLS-1$

		}
		if (unsequencedBlocks.size() > 0) {
			return false;
		}

		// writing to output file
		EventHandler
				.handle(
						EventLevel.DEBUG,
						"BlockSequencer",
						"",
						Messages.getString("BlockSequencer.StartWriting") + getFactory().getOutputFile(), Messages.getString("BlockSequencer.StartWritingDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		writeModel();

		EventHandler
				.handle(
						EventLevel.DEBUG,
						"BlockSequencer",
						"",
						Messages.getString("BlockSequencer.EndWriting") + getFactory().getOutputFile(), Messages.getString("BlockSequencer.EndWritingDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		EventHandler
				.handle(
						EventLevel.INFO,
						"BlockSequencer",
						"",
						Messages.getString("BlockSequencer.EndOfExecution"), Messages.getString("BlockSequencer.EndOfExecutionDetailled")); //$NON-NLS-1$ //$NON-NLS-2$

		return true;
	}

	/**
	 * Return the model factory instance of the block sequencer. If null the
	 * factory will be initialized.
	 * 
	 * @return the factory
	 */
	public ModelFactory getFactory() {
		if (factory == null) {

			factory = new SystemModelFactory(
					BlockSequencerConstants.BLOCK_SEQUENCER_TOOL_NAME,
					inputFile, outputFile, getLibraryFactory());
		}
		return factory;
	}

	/**
	 * Return the library factory instance of the block sequencer. If null the
	 * factory will be initialized.
	 * 
	 * @return the libraryFactory
	 */
	public BlockLibraryFactory getLibraryFactory() {
		if (libraryFactory == null) {

			// initializing block library factory
			List<String> inputLibraryFiles = new ArrayList<String>();
			inputLibraryFiles.add(ArgumentReader
					.getParameter(GAConst.ARG_DEFLIBFILE));
			if (libraryFile != null) {
				inputLibraryFiles.add(libraryFile);
			}

			libraryFactory = new BlockLibraryFactory(
					BlockSequencerConstants.BLOCK_SEQUENCER_TOOL_NAME,
					inputLibraryFiles);
		}
		return libraryFactory;
	}

}
