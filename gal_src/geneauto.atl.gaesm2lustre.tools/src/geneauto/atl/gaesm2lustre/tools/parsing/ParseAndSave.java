package geneauto.atl.gaesm2lustre.tools.parsing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.engine.parser.AtlParser;

public class ParseAndSave {

	static String file = "/home/arnaud/Documents/repositories/NASA2013/nasa2013/tools/geneauto_lustre/geneauto.atl.gaesm2lustre/transformations/gaesm2luse.atl";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AtlParser parser = AtlParser.getDefault();
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			//IModel model = parser.parseToModel(fis);
			EObject obj = parser.parse(fis);
			System.out.println("Done !");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ATLCoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
