package geneauto.atl.gaesm2lustre.tools.common;

import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

public class JavaFunctions {

	public String getDate()
	{
		String actual;
		DateFormat dateFormatter = DateFormat.getDateTimeInstance();
		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT+2"));
		actual=dateFormatter.format(new Date());
		return actual;
	}

}
