#!/bin/sh

########################################################
# Utility functions
########################################################

# Checks result code. If non-zero, then exits with the same code
# Parameter 1 is the return code
# Parameter 2 is text to display on failure.
check_result()
{
  if [ "${1}" -ne "0" ]; then
    echo "ERROR # ${1} : ${2}" # output message
	echo "Press Enter to continue"
	read                       # wait for input
    exit ${1}                  # return with the right code
  fi
}

