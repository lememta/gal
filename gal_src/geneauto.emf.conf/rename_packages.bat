
SET GA_VER=2.4.10
SET EMF_VER=1.0.2
SET ECL_VER=1.0.2

:: go to folder where the bat file is
cd %~dp0
:: go to the project root
cd ..

:: MOVE target\geneauto-%GA_VER%-bin.dir target\geneauto-emf-converters-%EMF_VER%-bin
MOVE target\geneauto-%GA_VER%-bin.dir target\geneauto-%GA_VER%-bin
if ERRORLEVEL 1 goto FAIL_CMD

:: MOVE target\geneauto-%GA_VER%-src.dir target\geneauto-emf-converters-%EMF_VER%-src
MOVE target\geneauto-%GA_VER%-src.dir target\geneauto-%GA_VER%-src
if ERRORLEVEL 1 goto FAIL_CMD

MOVE target\geneauto-%GA_VER%-ecore.zip target\geneauto-emf-ecore-%EMF_VER%.zip
if ERRORLEVEL 1 goto FAIL_CMD

MOVE target\geneauto-%GA_VER%-eclipse-bin.zip target\geneauto-eclipse-ui-%ECL_VER%-bin.zip
if ERRORLEVEL 1 goto FAIL_CMD

MOVE target\geneauto-%GA_VER%-eclipse-src.zip target\geneauto-eclipse-ui-%ECL_VER%-src.zip
if ERRORLEVEL 1 goto FAIL_CMD

goto :EOF

:FAIL_CMD
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error executing batch command!
echo ---------------------------------------------------------------------------
pause
exit /b 1
