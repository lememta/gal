@echo off

echo ------------------------------------------------------
echo INSTALLING SPECIFIED PLUGINS TO THE MAVEN REPOSITORY
echo ------------------------------------------------------

:: TODO Make relative


:: The plugins directory of Eclipse that will be used. NB! END WITH BACKSLASH
SET ECLIPSE_PLUGIN_DIR=C:\Progs\eclipse-modeling-helios-RC4-incubation-win32\plugins\

:: The Gene-Auto libraries directory. NB! END WITH BACKSLASH
SET GENEAUTO_LIBRARY_DIR=C:\Dev\geneauto-dev\Eclipse\geneauto.libraries\


:: --------------------------------------------------------------------------------
:: Required EMF plugins from the Eclipse isntallation directory
:: --------------------------------------------------------------------------------

:: Directory where the jar is located (! END WITH BACKSLASH !)
SET DIR=%ECLIPSE_PLUGIN_DIR%

:: Plugin name
SET NAME=lpg.runtime.java
:: Plugin group
SET GROUP=lpg.runtime
:: Plugin version
SET VER=2.0.17.v201004271640
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.ocl
:: Plugin group
SET GROUP=org.eclipse
:: Plugin version
SET VER=3.0.1.R30x_v201008251030
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.ocl.ecore
:: Plugin group
SET GROUP=org.eclipse.ocl
:: Plugin version
SET VER=3.0.1.R30x_v201008251030
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.emf.ecore
:: Plugin group
SET GROUP=org.eclipse.emf
:: Plugin version
SET VER=2.6.0.v20100614-1136
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.emf.common
:: Plugin group
SET GROUP=org.eclipse.emf
:: Plugin version
SET VER=2.6.0.v20100614-1136
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.emf.ecore.xmi
:: Plugin group
SET GROUP=org.eclipse.emf.ecore
:: Plugin version
SET VER=2.5.0.v20100521-1846
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.core.runtime
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.6.0.v20100505
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.core.jobs
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.5.0.v20100515
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.core.contenttype
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.4.100.v20100505-1235
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.registry
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
SET VER=3.5.0.v20100503
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.preferences
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
SET VER=3.3.0.v20100503
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.app
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
SET VER=1.3.0.v20100512
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.common
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
::SET VER=3.5.0.v20090520-1800
SET VER=3.6.0.v20100503
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.persistence.jpa.equinox.weaving
:: Plugin group
SET GROUP=org.eclipse.persistence.jpa.equinox
:: Plugin version
SET VER=1.1.3.v20091002-r5404
:: Install
:: SKIPPED call :INSTALL

:: Plugin name
SET NAME=org.eclipse.osgi
:: Plugin group
SET GROUP=org.eclipse
:: Plugin version
SET VER=3.6.0.v20100517
:: Install
:: SKIPPED call :INSTALL

:: Plugin name
SET NAME=javax.transaction
:: Plugin group
SET GROUP=javax
:: Plugin version
SET VER=1.1.1.v201002111330
:: Install
:: SKIPPED call :INSTALL

:: --------------------------------------------------------------------------------
:: Specific case. This plugin does not follow the usual naming convention. 
:: Hence, we rename it in the repository to be usable with maven
:: --------------------------------------------------------------------------------

:: Directory where the jar is located (! ending with backslash !)
SET DIR=%ECLIPSE_PLUGIN_DIR%org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520\

:: Plugin name
SET NAME=runtime_registry_compatibility
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.3.0.v20100520
:: Compute file name
SET FILE=%NAME%
:: Install 
call :INSTALL_2


@echo off
echo ===========================================================================
echo :::
echo :::                      !    D   O   N   E    !
echo :::
echo ===========================================================================
pause
goto :EOF

:FAIL_INSTALL
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error installing plugin
echo ---------------------------------------------------------------------------
pause
exit

:FAIL_COPY
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error copying plugin
echo ---------------------------------------------------------------------------
pause
exit

:INSTALL
@echo off
:: Compute file name with underscore (the original jar - Eclipse style name)
SET FILE=%NAME%
IF DEFINED VER SET FILE=%FILE%_%VER%
:INSTALL_2
:: Compute file name with dash      (this is how maven renames them)
SET FILE_M=%NAME%
IF DEFINED VER SET FILE_M=%FILE_M%-%VER%
:: Full path to jar
SET JAR=%DIR%%FILE%.jar
:: Install to maven repository
call mvn install:install-file -DgroupId=%GROUP% -DartifactId=%NAME% -Dversion=%VER% -Dpackaging=jar -Dfile=%JAR%
IF ERRORLEVEL 1 GOTO FAIL_INSTALL

:COPY
:: Copy to the Gene-Auto library directory for deployment
SET TMP=%GROUP:.=\%
SET DEST_DIR=%GENEAUTO_LIBRARY_DIR%%TMP%\%NAME%\%VER%\
SET DEST_JAR=%DEST_DIR%%FILE_M%.jar

echo Making directory %DEST_DIR%
if not exist %DEST_DIR% mkdir %DEST_DIR%
IF ERRORLEVEL 1 GOTO FAIL_COPY

echo Copying %JAR% to %DEST_JAR%
copy %JAR% %DEST_JAR%
IF ERRORLEVEL 1 GOTO FAIL_COPY

goto :EOF