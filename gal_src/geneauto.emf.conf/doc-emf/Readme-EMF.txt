
*******************************************************************************

                        Gene-Auto EMF Model Converters Readme
                            
*******************************************************************************

                            www.geneauto.org
                           geneauto.krates.ee

This readme contains following sections:

    * What are Gene-Auto and Gene-Auto Eclipse tools?
    * Who develops them?
    * How are they licensed?
    * How to install the Gene-Auto EMF Model Converters?
    * Functionality and usage of the Gene-Auto EMF Model Converters
    * Release notes/Change log
    * Where to find more information?


----------------------------------------------------------------------
    What are Gene-Auto and Gene-Auto Eclipse tools?
----------------------------------------------------------------------

- Gene-Auto

Gene-Auto is an open-source toolset for converting Simulink, Stateflow 
and Scicos models to executable program code. Output to C code is currently
available, Ada output is under development. The toolset development 
conforms with the DO-178B/ED-12B standards and aims to be qualified to 
be used in the safety critical domain.

- Gene-Auto Eclipse tools

The Gene-Auto base toolset is primarily intended to be a qualifiable embedded 
code generation toolchain. It is implemented in core Java using only a minimal
set of external libraries. Gene-Auto Eclipse tools provide complementary 
functionalities to work with models using Eclipse based tooling.

-- Gene-Auto EMF Ecore

The Eclipse Modeling Framework (EMF) is a framework within Eclipse that provides
facilities for (meta)modelling, code generation, tool building etc, based on a 
structured data model. Ecore is the central metamodelling language (a dialect of 
UML) of the EMF. This package contains the Gene-Auto Ecore-based metamodel and 
related resources.

-- Gene-Auto EMF Model Converters

This package contains tools that allow importing/exporting Gene-Auto models to 
the EMF/Ecore-based format and to use standard Eclipse-based tooling to:

  * export Gene-Auto models to other languages and tools to perform 
    additional verification and validation tasks, optimisation etc
  * import models from other languages to perform code generation with
    Gene-Auto.

- Gene-Auto Eclipse UI tools

This package contains the Gene-Auto System Model editor that has been generated 
from the Gene-Auto EMF/Ecore metamodel and a separate plugin geneauto.eclipse.menus 
that allows to use the Gene-Auto code generator and the Gene-Auto EMF model 
conversion tools directly from Eclipse.


----------------------------------------------------------------------
    Who develops them?
----------------------------------------------------------------------

Gene-Auto was originally developed by the Gene-Auto consortium with 
support from ITEA (www.itea2.org) and national public funding bodies 
in France, Estonia, Belgium and Israel. It is currently developed
further within several other projects. For more information about the 
Gene-Auto toolset see the Gene-Auto ReadMe and the webpages.

The Gene-Auto Eclipse tools are developed by FeRIA/IRIT - INPT/University
of Toulouse, Institute of Cybernetics at Tallinn University of Technology 
and IB Krates OU.

The copyright of the different components belongs to their respective 
developers.


----------------------------------------------------------------------
    How are they licensed?
----------------------------------------------------------------------

The Gene-Auto EMF toolset is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published 
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>


----------------------------------------------------------------------
    How to install the Gene-Auto EMF Model Converters?
----------------------------------------------------------------------

-  Install Gene-Auto
--------------------------------

To use the Gene-Auto EMF tools you need to install Gene-Auto first. 
You can find detailed installation instructions from: 

    http://www.geneauto.org/?page=documents
    
    D2.55 Gene-Auto Installation and Usage Manual  
    
For the binary installation simply download the latest binary 
distribution (geneauto-2.x.x-bin.zip) from:
    
    http://www.geneauto.org/?page=downloads
    
and unzip the file contents to a separate folder (the location is not 
important).

For installing the source release and for other information please consult
the installation manual.

-  Install Gene-Auto EMF Model Converters
------------------------------------------

-- Binary release

Download the latest binary distribution (geneauto-emf-converters-1.x.x-bin.zip) from:
    
    http://www.geneauto.org/?page=downloads
    
and unzip the file contents to the folder, where you unzipped the Gene-Auto 
main tool.

-- Source release

The source installation follows the similar path as installing the source 
release of Gene-Auto. Please follow the installation steps as instructed in 
the Gene-Auto installation manual referred above. Make sure that you install 
the projects of Gene-Auto EMF Model Converters to the same directory where 
you installed the projects of the Gene-Auto main toolset.

--------
NOTE: Please note that Gene-Auto and Gene-Auto EMF Model Converters are built 
separately and have their own version numbering. However, Gene-Auto EMF Model 
Converters require a fixed version of Gene-Auto. The table of dependencies is
following:

                Gene-Auto       EMF Model Converters         
                    2.4.10                 1.0.1                     
                    2.4.7                 1.0.0                     


----------------------------------------------------------------------
    Functionality and usage of the Gene-Auto Model Converters
----------------------------------------------------------------------

The current package contains following primary components:

- geneauto.emf.gaxml2ecore
---------------------------
A tool that converts models from the Gene-Auto native file format to the 
Gene-Auto Ecore based format. Currently, converting Gene-Auto System Model 
files is supported. Conversion of Code Model files will be added later. 
By default the Gene-Auto native System Model files have the extension 
.gsm.xml. The converted files have by default the extension .gaesm. 

Command (using the batch file, recommended):
    gaxml2ecore
Command (using Java directly):
    java -Xmx1024m -cp "<PATH TO GENE-AUTO>geneauto.emf.gaxml2ecore-1.x.x.jar" geneauto.emf.gaxml2ecore.main.GAXML2Ecore    
Arguments:
    <inputFilePath> [-b <blockLibraryFilePath>] [-o <outputFilePath>] [-l <logLevel>]


- geneauto.emf.ecore2gaxml
---------------------------
A tool to convert models from the Gene-Auto Ecore based format to the
Gene-Auto native file format. Currently, converting Gene-Auto System Model 
files is supported. Conversion of Code Model files will be added later. 
By default the Gene-Auto Ecore System Model files have the extension 
.gaesm. The converted files have by default the extension .gsm.xml. 

Command (using the batch file, recommended):
    ecore2gaxml
Command (using Java directly):
    java -Xmx1024m -cp "<PATH TO GENE-AUTO>geneauto.emf.ecore2gaxml-1.x.x.jar" geneauto.emf.ecore2gaxml.main.Ecore2GAXML
Arguments:
    <inputFilePath> [-b <blockLibraryFilePath>] [-o <outputFilePath>] [-l <logLevel>]


- geneauto.emf.launcher
---------------------------
This package is a collection of tools to perform directly the following
transformations (using the Gene-Auto code generator): 


-- Simulink to Gene-Auto (Ecore)
  
Command (using the batch file, recommended):
    simulink2ga
Command (using Java directly):
    java -Xmx1024m -cp "<PATH TO GENE-AUTO>geneauto.emf.launcher-1.x.x.jar" geneauto.emf.launcher.Simulink2GA
Arguments:
    <inputFilePath> [--mLib <externalLibFolder>] [-b <blockLibraryFilePath>] [-O <outputPath>] [-m <constantFilePath>] [-l <logLevel>]
  

-- Gene-Auto (Ecore) to C 
  
Command (using the batch file, recommended):
    ga2c
Command (using Java directly):
    java -Xmx1024m -cp "<PATH TO GENE-AUTO>geneauto.emf.launcher-1.x.x.jar" geneauto.emf.launcher.GA2C
Arguments:
    <inputFilePath> [-b <blockLibraryFilePath>] [-O <outputPath>] [-l <logLevel>]
  

-- Gene-Auto (Ecore) to C (optimized) 

Command (using the batch file, recommended):
    ga2copt
Command (using Java directly):
    java -Xmx1024m -cp "<PATH TO GENE-AUTO>geneauto.emf.launcher-1.x.x.jar" geneauto.emf.launcher.GA2COpt
Arguments:
    <inputFilePath> [-b <blockLibraryFilePath>] [-O <outputPath>] [-l <logLevel>]


----------------------------------------------------------------------
    Release notes/Change log
----------------------------------------------------------------------

1.0.1
----------

  All projects upgraded to be compatible with Gene-Auto 2.4.10
  
  gaxml2exore and ecore2gaxml converters convert now also GACodeModel files:
  *.gcm.xml and *.gaecm  


----------------------------------------------------------------------
    Where to find more information?
----------------------------------------------------------------------

You can find more technical or background information and the latest 
toolset releases on two websites:
                www.geneauto.org and geneauto.krates.ee
                
There is a lot of useful information in the Gene-Auto FAQ:
                 http://geneauto.krates.ee/faq
    
You are also welcome to contact the Gene-Auto team through the web-sites
to share your experience, ask for new features and/or contribute to 
the development. We hope you will find Gene-Auto useful!

                        The Gene-Auto team