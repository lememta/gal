:: make sure we are on the same device where the executable file 
%~d0
:: go to folder where the bat file is
cd %~dp0

SET reportFile=buildlog.txt

@echo off

echo Output will be stored in %reportFile%

echo Compile and test in progress ...

call compile_and_test.bat b > %reportFile%

@echo off
echo ===========================================================================
echo :::                      Completed
echo :::               Output stored in %reportFile%
echo ===========================================================================

pause
