:: make sure we are on the same device where the executable file 
%~d0
:: go to folder where the bat file is
cd %~dp0

:: Run MergeTool with given specification
java -cp tools\MergeTool.jar main.MergeTool src-extra\geneauto.emf.edit\Merge.conf
if ERRORLEVEL 1 goto FAIL_CMD

pause

goto :EOF

:FAIL_CMD
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error executing batch command!
echo ---------------------------------------------------------------------------
pause
exit
