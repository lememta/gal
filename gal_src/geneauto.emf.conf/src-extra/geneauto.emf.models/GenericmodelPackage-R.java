	/**
	 * For some reason EMF does not generated this field, but generates a
	 * reference to it, if the Ecore model is referenced from generated external
	 * packages (Eclipse MDT 3.6)
	 */
	int GENERIC_LINK_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation
	 * @generated
	 */
	EClass getAnnotation();