	 * @generated
	 */
	public GAModelElement getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (GAModelElement)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GenericmodelPackage.GA_MODEL_ELEMENT__PARENT, oldParent, parent));
			}
		}
		return parent;
	}