	 * @generated NOT
	 */
	public EList<GAModelElement> getAllElements() {
		EList<GAModelElement> lst = new BasicEList<GAModelElement>();
		TreeIterator<EObject> iter = eAllContents();
		while (iter.hasNext()) {
			EObject eObj = iter.next();
			if (eObj instanceof GAModelElement) {
				lst.add((GAModelElement) eObj);
			} 
		}
		return lst;
	}
