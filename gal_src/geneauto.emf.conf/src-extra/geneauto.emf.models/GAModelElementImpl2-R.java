	 * @generated NOT
	 */
	public GAModelElement basicGetParent() {
		// Proxy resolution is not done in this method
		// It is done in the getter itself.
		EObject pObj = eContainer();
		if (pObj instanceof GAModelElement) {
			return (GAModelElement) eContainer();
		} else {
			return null;
		}
	}
