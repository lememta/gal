	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation
	 * @generated
	 */
	EClass getAnnotation();