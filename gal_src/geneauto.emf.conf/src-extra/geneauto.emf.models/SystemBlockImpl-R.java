	 * @generated NOT
	 */
	public EList<Block> flattenedChildBlocks() {
		BasicEList<Block> lst = new BasicEList<Block>();
		// TODO Dummy
		for (Block child : blocks) {
			if (child instanceof SystemBlock) {
				SystemBlock childSys = (SystemBlock) child;
				if (!childSys.isVirtual()) {
					lst.add(child);
				} else {
					lst.addAll(childSys.flattenedChildBlocks());
				}
			} else {
				lst.add(child);
			}
		}
		return lst;
	}
