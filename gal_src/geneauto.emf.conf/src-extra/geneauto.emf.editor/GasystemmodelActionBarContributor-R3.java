		super.addGlobalActions(menuManager);
		
		addGeneAutoActions(menuManager);
	}

	/**
	 * This inserts Gene-Auto actions to the main menu.
	 */
	protected void addGeneAutoActions(IMenuManager menuManager) {
		
		menuManager.insertAfter("Gene-Auto-actions", new Separator("Gene-Auto-end"));
		
		IAction act;
		
		act = new Action("Setup") {
			@Override
			public void run() {
				IWorkbench wb = PlatformUI.getWorkbench();
				IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
				PreferenceDialog pref = 
					PreferencesUtil.createPreferenceDialogOn(win.getShell(), 
							"geneauto.emf.menus.PreferencePage", null, null);
				if (pref != null)
					pref.open();
			}
		};
		menuManager.insertAfter("Gene-Auto-actions", act);

	}