
Note that the seek text in build.properties involves currently one extra newline. 
This is due to a limitation in the MergeTool. When it is fixed, then the seek text 
must be corrected also!

SEE ALSO CODE CUSTOMISATIONS IN THE VERIFICATION WORKFLOW