:: This script copies maven configuration files to the root of Eclipse 
:: workspace and updates Eclipse project files

:: make sure we are on the same device where the executable file 
%~d0
:: go to folder where the bat file is
cd %~dp0

:: the script was executed from conf folder
IF exist mvn GOTO copy

if ERRORLEVEL 1 goto FAIL_CMD

:copy
call copy_mvn_conf

if ERRORLEVEL 1 goto FAIL_CMD
    
:mvn
cd ..\
call mvn clean
::call mvn eclipse:clean
call mvn eclipse:eclipse
pause
goto :EOF

:FAIL_CMD
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error executing batch command!
echo ---------------------------------------------------------------------------
pause
exit
