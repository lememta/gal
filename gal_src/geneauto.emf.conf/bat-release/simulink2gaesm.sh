#!/bin/sh

SCRIPT=`readlink -f $0`
export GENEAUTO_HOME=`dirname $SCRIPT`"/"

java -Xmx1024m -cp "$GENEAUTO_HOME/geneauto.emf.launcher-1.0.1.jar" geneauto.emf.launcher.Simulink2GAESM $1 $2 $3 $4 $5 $6 $7 $8 $9
