@echo off

echo ------------------------------------------------------
echo  INSTALLING SPECIFIED PLUGINS:
echo        FROM    geneauto.libraries 
echo        TO        LOCAL MAVEN REPOSITORY
echo ------------------------------------------------------

:: make sure we are on the same device where the executable file 
%~d0
:: go to folder where the bat file is
cd %~dp0

:: The Gene-Auto libraries directory. NB! END WITH BACKSLASH
SET GENEAUTO_LIBRARY_DIR=..\geneauto.libraries\

:: --------------------------------------------------------------------------------
:: INSTALL PLUGINS
:: --------------------------------------------------------------------------------

SET DIR=%GENEAUTO_LIBRARY_DIR%

:: Plugin name
SET NAME=lpg.runtime.java
:: Plugin group
SET GROUP=lpg.runtime
:: Plugin version
SET VER=2.0.17.v201004271640
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.ocl
:: Plugin group
SET GROUP=org.eclipse
:: Plugin version
SET VER=3.1.0.v20110913-1213
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.ocl.ecore
:: Plugin group
SET GROUP=org.eclipse.ocl
:: Plugin version
SET VER=3.1.1.v20110823-1646
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.emf.ecore
:: Plugin group
SET GROUP=org.eclipse.emf
:: Plugin version
SET VER=2.7.0.v20110912-0920
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.emf.common
:: Plugin group
SET GROUP=org.eclipse.emf
:: Plugin version
SET VER=2.7.0.v20110912-0920
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.emf.ecore.xmi
:: Plugin group
SET GROUP=org.eclipse.emf.ecore
:: Plugin version
SET VER=2.7.0.v20110520-1406
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.core.runtime
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.7.0.v20110110
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.core.jobs
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.5.100.v20110404
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.core.contenttype
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.4.100.v20110423-0524
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.registry
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
SET VER=3.5.101.R37x_v20110810-1611
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.preferences
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
SET VER=3.4.1.R37x_v20110725
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.app
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
SET VER=1.3.100.v20110321
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.equinox.common
:: Plugin group
SET GROUP=org.eclipse.equinox
:: Plugin version
SET VER=3.6.0.v20110523
:: Install
call :INSTALL

:: Plugin name
SET NAME=org.eclipse.osgi
:: Plugin group
SET GROUP=org.eclipse
:: Plugin version
SET VER=3.7.1.R37x_v20110808-1106
:: Install
call :INSTALL

:: --------------------------------------------------------------------------------
:: Specific case. This plugin does not follow the usual naming convention. 
:: Hence, we rename it in the repository to be usable with maven
:: --------------------------------------------------------------------------------

:: Plugin name
SET NAME=runtime_registry_compatibility
:: Plugin group
SET GROUP=org.eclipse.core
:: Plugin version
SET VER=3.5.0.v20110505
:: Directory where the jar is located (! ending with backslash !)
SET DIR=%ECLIPSE_PLUGIN_DIR%org.eclipse.core.runtime.compatibility.registry_%VER%\
:: Compute file name
SET FILE=%NAME%
:: Install 
call :INSTALL_2


@echo off
echo ===========================================================================
echo :::
echo :::                      !    D   O   N   E    !
echo :::
echo ===========================================================================
pause
goto :EOF

:FAIL_INSTALL
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error installing plugin
echo ---------------------------------------------------------------------------
pause
exit


:INSTALL
@echo off
:: Compute file name with underscore (the original jar - Eclipse style name)
SET FILE=%NAME%
IF DEFINED VER SET FILE=%FILE%_%VER%
:INSTALL_2
:: Compute file name with dash      (this is how maven renames them)
SET FILE_M=%NAME%
IF DEFINED VER SET FILE_M=%FILE_M%-%VER%

:: Full path to jar
SET TMP=%GROUP:.=\%
SET DIR=%GENEAUTO_LIBRARY_DIR%%TMP%\%NAME%\%VER%\
SET JAR=%DIR%%FILE_M%.jar

:: Install to maven repository
call mvn install:install-file -DgroupId=%GROUP% -DartifactId=%NAME% -Dversion=%VER% -Dpackaging=jar -Dfile=%JAR%
IF ERRORLEVEL 1 GOTO FAIL_INSTALL


goto :EOF