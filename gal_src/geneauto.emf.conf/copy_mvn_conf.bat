@echo off

:: make sure we are on the same device where the executable file 
%~d0
:: go to folder where the bat file is
cd %~dp0

:: Copy root configuration
copy mvn\root\*.* ..\

if ERRORLEVEL 1 goto FAIL_CMD

:: These dirs are automatically generated by EMF and don't contain the pom. Copy now (or update)

SET PROJ=geneauto.emf.models
CALL :COPY_READONLY_POM

pause
goto :EOF

:FAIL_CMD
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error executing batch command!
echo ---------------------------------------------------------------------------
pause
exit

:COPY_READONLY_POM
:: Make writeable first
if exist ..\%PROJ%\pom.xml attrib ..\%PROJ%\pom.xml -R
copy mvn\%PROJ%\pom.xml ..\%PROJ%\

if ERRORLEVEL 1 goto FAIL_CMD

:: Make the copied file read-only
attrib ..\%PROJ%\pom.xml +R

if ERRORLEVEL 1 goto FAIL_CMD

:: Readme file with info
copy mvn\%PROJ%\pom.xml.README ..\%PROJ%\

if ERRORLEVEL 1 goto FAIL_CMD

goto :EOF