#!/bin/sh
# This script copies maven configuration files to the root of Eclipse 
# workspace compiles all projects and assemblies bin and src 
# packages

# output all commands executed in the script to stdo
# required for debugging only
#set -x

# go to the directory where the script is
cd `dirname $0`

# load some utlities
source ./util.sh

# copy maven configuration
./copy_mvn_conf.sh
check_result $? "Can't copy Maven configuration"

# go back to root
cd ../

# build
#mvn clean package assembly:assembly
mvn clean package assembly:directory
check_result $? "Building failed"

# turn off echoing commands
set +x

echo ===========================================================================
echo :::
echo :::                      !    D   O   N   E    !
echo :::
echo ===========================================================================

# Wait for a keystroke
echo "Press Enter to continue"
read