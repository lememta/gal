#!/bin/sh
# This script post-processes the assembly result from Maven
#    renames some generated packages etc-

# go to the directory where the script is
cd `dirname $0`

# load some utlities
source ./util.sh

cd ..

# check if the target directory exists
stat target -c%n
check_result $? "Target directory not available"

# remove archive-tmp
rm -r target/archive-tmp
check_result $? "Can't remove archive-tmp"

GA=geneauto-2.4.10

OLD=target/"$GA"-bin.dir
NEW=target/"$GA"-verif-bin
mv "$OLD" "$NEW"
check_result $? "Can't rename file $OLD"

OLD=target/"$GA"-src.dir
NEW=target/"$GA"-verif-src
mv "$OLD" "$NEW"
check_result $? "Can't rename file $OLD"

#OLD=target/"$GA"-ecore.dir
#NEW=target/"$GA".verif.ecore
#mv "$OLD" "$NEW"
#check_result $? "Can't rename file $OLD"


# turn off echoing commands
set +x

echo ===========================================================================
echo :::
echo :::                      !    D   O   N   E    !
echo :::
echo ===========================================================================

# Wait for a keystroke
echo "Press Enter to continue"
read