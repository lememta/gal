@echo off
echo ===========================================================================
echo :::
echo :::                     ! A T T E N T I O N !
echo :::
echo ::: The script will build the Gene-Auto EMF and Eclipse releases.
echo ::: Please make sure that:
echo :::
echo :::    - All the version numbers and other info in the pom files is uptodate
echo :::
echo :::    - The required Eclipse plugins have been built and exist in the 
echo :::      'plugins' directory under the project root
echo :::
echo ===========================================================================
pause

:: This script copies maven configuration files to the root of Eclipse 
:: workspace compiles all projects and executes unit tests

:: make sure we are on the same device where the executable file 
%~d0
:: go to folder where the bat file is
cd %~dp0

:: the script was executed from conf folder
IF exist mvn GOTO copy

if ERRORLEVEL 1 goto FAIL_CMD

:copy
call copy_mvn_conf

if ERRORLEVEL 1 goto FAIL_CMD

:mvn
cd ..\

if ERRORLEVEL 1 goto FAIL_CMD

call mvn clean package assembly:assembly

if ERRORLEVEL 1 goto FAIL_COMPILE

:: make sure we are on the same device where the executable file 
%~d0
:: go to folder where the bat file is
cd %~dp0

call rename_packages

if ERRORLEVEL 1 goto FAIL_RENAME

@echo off
echo ===========================================================================
echo :::
echo :::                      !    D   O   N   E    !
echo :::
echo ===========================================================================
if not "%1"=="b" pause
goto :EOF

:FAIL_COMPILE
@echo off
echo ---------------------------------------------------------------------------
echo ::: Building geneauto-emf has failed!
echo ---------------------------------------------------------------------------
pause
exit /b 1

:FAIL_RENAME
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error renaming generated packages!
echo ---------------------------------------------------------------------------
pause
exit /b 1

:FAIL_CMD
@echo off
echo ---------------------------------------------------------------------------
echo ::: Error executing batch command!
echo ---------------------------------------------------------------------------
pause
exit /b 1

:END
