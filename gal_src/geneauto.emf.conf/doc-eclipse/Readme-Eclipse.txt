
*******************************************************************************

                        Gene-Auto Eclipse tools Readme
                            
*******************************************************************************

                            www.geneauto.org
                           geneauto.krates.ee

This readme contains following sections:

    * What are Gene-Auto and Gene-Auto Eclipse tools?
    * Who develops them?
    * How are they licensed?
    * Functionality and usage of the Gene-Auto Eclipse tools
    * How to install the Gene-Auto Eclipse tools?
    * Release notes/Change log
    * Where to find more information?


----------------------------------------------------------------------
    What are Gene-Auto and Gene-Auto Eclipse tools?
----------------------------------------------------------------------

- Gene-Auto

Gene-Auto is an open-source toolset for converting Simulink, Stateflow 
and Scicos models to executable program code. Output to C code is currently
available, Ada output is under development. The toolset development 
conforms with the DO-178B/ED-12B standards and aims to be qualified to 
be used in the safety critical domain.

- Gene-Auto Eclipse tools

The Gene-Auto base toolset is primarily intended to be a qualifiable embedded 
code generation toolchain. It is implemented in core Java using only a minimal
set of external libraries. Gene-Auto Eclipse tools provide complementary 
functionalities to work with models using Eclipse based tooling.

-- Gene-Auto EMF Ecore

The Eclipse Modeling Framework (EMF) is a framework within Eclipse that provides
facilities for (meta)modelling, code generation, tool building etc, based on a 
structured data model. Ecore is the central metamodelling language (a dialect of 
UML) of the EMF. This package contains the Gene-Auto Ecore-based metamodel and 
related resources.

-- Gene-Auto EMF Model Converters

This package contains tools that allow importing/exporting Gene-Auto models to 
the EMF/Ecore-based format and to use standard Eclipse-based tooling to:

  * export Gene-Auto models to other languages and tools to perform 
    additional verification and validation tasks, optimisation etc
  * import models from other languages to perform code generation with
    Gene-Auto.

- Gene-Auto Eclipse UI tools

This package contains the Gene-Auto System Model editor that has been generated 
from the Gene-Auto EMF/Ecore metamodel and a separate plugin geneauto.eclipse.menus 
that allows to use the Gene-Auto code generator and the Gene-Auto EMF model 
conversion tools directly from Eclipse.

----------------------------------------------------------------------
    Who develops them?
----------------------------------------------------------------------

Gene-Auto was originally developed by the Gene-Auto consortium with 
support from ITEA (www.itea2.org) and national public funding bodies 
in France, Estonia, Belgium and Israel. It is currently developed
further within several other projects. For more information about the 
Gene-Auto toolset see the Gene-Auto ReadMe and the webpages.

The Gene-Auto Eclipse tools are developed by FeRIA/IRIT - INPT/University
of Toulouse, Institute of Cybernetics at Tallinn University of Technology 
and IB Krates OU.

The copyright of the different components belongs to their respective 
developers.


----------------------------------------------------------------------
    How are they licensed?
----------------------------------------------------------------------

The Gene-Auto EMF toolset is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published 
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>


------------------------------------------------------------------------------------
    Functionality and usage of the Gene-Auto Eclipse UI tools
------------------------------------------------------------------------------------

The Gene-Auto Eclipse UI tools provide some integration between the Gene-Auto code 
generator and Eclipse. They are meant to be used with the binary installation of 
Gene-Auto. First you need to configure the Eclipse workspace to be aware of its 
location. To do this, either right-click in the workspace and choose 
Gene-Auto/Setup or choose from the main menu Windows/Preferences/Gene-Auto.

Then, it is possible to launch the Gene-Auto code generator directly by right-clicking 
on a file (currently either a .mdl or .gaesm file) and selecting the appropriate action.
The output is displayed in the Eclipse console and the relevant part of the workbench 
is automatically refreshed after Gene-Auto finishes.

The package also contains the standard EMF structural (tree-view) editor for Gene-Auto 
System models (the Ecore based .gaesm files generated either directly by the Gene-Auto 
EMF tools or via the Gene-Auto menu in Eclipse). This editor allows to browse the model, 
create/remove nodes and modify their attributes. It is also possible to use the standard 
model compare utility to compare models structurally and perform some basic structural 
validations. The editor has been automatically generated from the Ecore metamodel and 
has been slightly modified to make some additions to the main menu. 

The package enables also the convenient Eclipse model compare facility for Gene-Auto 
Ecore System Model models.

Currently, the EMF structural editor provides rather limited functionality. However, the 
Gene-Auto EMF and Eclipse tools are meant to be used as a starting point for developing 
and adding more functionality that will complement the Gene-Auto code generation in 
various ways.


----------------------------------------------------------------------
    How to install the Gene-Auto Eclipse UI tools?
----------------------------------------------------------------------

To use the Gene-Auto Eclipse UI tools you need to install also the binary 
releases of Gene-Auto and Gene-Auto EMF Model Conveters. 

-  Install Gene-Auto
--------------------------------

Download the latest binary distribution (geneauto-2.x.x-bin.zip) 
from:
    
    http://www.geneauto.org/?page=downloads
    
and unzip the file contents to a separate folder (the location is not 
important).

-  Install Gene-Auto EMF Gene-Auto Model Converters
-----------------------------------------------------

Download the latest binary distribution (geneauto-emf-converters-1.x.x-bin.zip)
from:
    
    http://www.geneauto.org/?page=downloads
    
and unzip the file contents to the folder, where you unzipped the Gene-Auto 
main tool.

-  Install Eclipse
--------------------------------

The Gene-Auto Eclipse tools have been built for Eclipse 3.6 (Helios).
However, usage with several earlier versions is possible, see the note 
below.

Download the Eclipse Modeling tools 3.6 (Helios) package from:

    www.eclipse.org   

Unzip the downloaded archive to a separate folder.

NOTE: Although the Gene-Auto Eclipse tools have been built with Eclipse 3.6,
it is probably possible to use them with several older Eclipse versions. In 
this case you need to either
    - update all the plugins required by the different Gene-Auto Eclipse 
      plugins in your Eclipse installation. You can find out the dependencies
      if you examine the plugin.xml in the individual jars or
    - use the source release of the Gene-Auto Eclipse tools, change the plugin
      dependencies manually and rebuild the plugins
      

-  Install Gene-Auto Eclipse UI tools
-----------------------------------

-- Binary release

Download the latest binary distribution (geneauto-eclipse-ui-1.x.x-bin.zip) from:
    
    http://www.geneauto.org/?page=downloads
    
Remove any previous versions of the plugin jars contained in the downloaded zip file
from the dropins folder of your Eclipse installation (plugins folder in Eclipse 3.4 
and earlier - See also the note above about older Eclipse versions!).

Unzip the contents of the downloaded zip file directly (flat, no directories) to the 
dropins folder of your Eclipse installation (plugins folder in Eclipse 3.4 and earlier 
- See also the note above about older Eclipse versions!).

Start Eclipse.

-- Source release

Download the latest source distribution (geneauto-eclipse-ui-1.x.x-src.zip) from:
    
    http://www.geneauto.org/?page=downloads
    
In your Eclipse workspace, where you want to install the tools:

    Right-click in the Project Browser and choose "Import", 
    "Existing Projects into Workspace" and click Next,
    "Select archive file" and browse for the dowloaded archive,
    Click "Finish"
    
    Right click on one of the new projects (no matter which one - all will be started)
    Choose "Run As/Eclipse application"
    A new workspace will be launched that contains the new plugins installed and active

--------
NOTE 1: The binary release of the Gene-Auto Eclipse UI tools contains one more project 
than the source release: geneauto.emf.models. This must be also copied to the Eclipse
dropins (or plugins) directory. It is the same project that exists in the Gene-Auto 
EMF bundle, but to be used as an Eclipse plugin it is packaged differently.

NOTE 2: Please note that the Gene-Auto Eclipse UI tools are built separately from 
other tools and have their own version numbering. However, each build requires a 
specific version of Gene-Auto and Gene-Auto EMF Model Converters. 

Here is the table of dependencies:

              Gene-Auto       EMF Model Converters         Eclipse UI     
                2.4.10                 1.0.1                   1.0.2
                2.4.7                 1.0.0                   1.0.1
                2.4.7                 1.0.0                   1.0.0      


----------------------------------------------------------------------
    Release notes/Change log
----------------------------------------------------------------------

1.0.2
----------

  All projects upgraded to be compatible with Gene-Auto 2.4.10

1.0.1
----------

* geneauto.eclipse.menus

    Added menus for conversion between Gene-Auto native and Ecore formats
    Updated provider information in manifest.mf

* geneauto.emf.models

    Updated provider information in plugin.properties

* geneauto.emf.edit

    Updated provider information in plugin.properties

* geneauto.emf.editor

    Updated provider information in plugin.properties


----------------------------------------------------------------------
    Where to find more information?
----------------------------------------------------------------------

You can find more technical or background information and the latest 
toolset releases on two websites:
                www.geneauto.org and geneauto.krates.ee
                
There is a lot of useful information in the Gene-Auto FAQ:
                 http://geneauto.krates.ee/faq
    
You are also welcome to contact the Gene-Auto team through the web-sites
to share your experience, ask for new features and/or contribute to 
the development. We hope you will find Gene-Auto useful!

                        The Gene-Auto team