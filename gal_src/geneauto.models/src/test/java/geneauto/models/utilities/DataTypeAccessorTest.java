/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/test/java/geneauto/models/utilities/DataTypeAccessorTest.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;
import static org.junit.Assert.assertTrue;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gadatatypes.TRealSingle;

import org.junit.Ignore;
import org.junit.Test;

/**
 */
public class DataTypeAccessorTest {

    /**
     * Test of the method getDataType, we check the returned type and its
     * attributes.
     */
    @Test
    @Ignore
    public void testGetDataType() {

        // simple values;
        GADataType g1 = DataTypeAccessor.getDataType("1");
        GADataType g2 = DataTypeAccessor.getDataType("-1");
        GADataType g3 = DataTypeAccessor.getDataType("1.5");
        GADataType g4 = DataTypeAccessor.getDataType(".5");
        GADataType g5 = DataTypeAccessor.getDataType("1.");
        GADataType g6 = DataTypeAccessor.getDataType("-1.5");

        assertTrue(g1 instanceof TRealDouble);
        assertTrue(g2 instanceof TRealDouble);
        assertTrue(g3 instanceof TRealDouble);
        assertTrue(g4 instanceof TRealDouble);
        assertTrue(g5 instanceof TRealDouble);
        assertTrue(g6 instanceof TRealDouble);

        // explicitly typed values;
        g1 = DataTypeAccessor.getDataType("int8(-1)");
        g2 = DataTypeAccessor.getDataType("single(1)");
        g3 = DataTypeAccessor.getDataType("uint16(1.5)");
        g4 = DataTypeAccessor.getDataType("int16(1.2)");
        g5 = DataTypeAccessor.getDataType("int32(.5)");
        g6 = DataTypeAccessor.getDataType("double(1.)");

        assertTrue(g1 instanceof TRealInteger);
        assertTrue(((TRealInteger) g1).getNBits() == 8);
        assertTrue(((TRealInteger) g1).isSigned());
        assertTrue(g2 instanceof TRealSingle);
        assertTrue(g3 instanceof TRealInteger);
        assertTrue(((TRealInteger) g3).getNBits() == 16);
        assertTrue(!((TRealInteger) g3).isSigned());
        assertTrue(g4 instanceof TRealInteger);
        assertTrue(((TRealInteger) g4).getNBits() == 16);
        assertTrue(((TRealInteger) g4).isSigned());
        assertTrue(g5 instanceof TRealInteger);
        assertTrue(((TRealInteger) g5).getNBits() == 32);
        assertTrue(((TRealInteger) g5).isSigned());
        assertTrue(g6 instanceof TRealDouble);

        // simple vectors
        g1 = DataTypeAccessor.getDataType("[1:2:10]");
        g2 = DataTypeAccessor.getDataType("[-1:2:10]");
        g3 = DataTypeAccessor.getDataType("[1 3 5 7 9]");
        g4 = DataTypeAccessor.getDataType("[1 -3 5 -7 9]");
        g5 = DataTypeAccessor.getDataType("[1,3,5,7,9]");
        g6 = DataTypeAccessor.getDataType("[1,3 5 7,9]");
        
        assertTrue(g1.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g1) == 5);
        assertTrue(g1.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g2.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g2) == 6);
        assertTrue(g2.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g3.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g3) == 5);
        assertTrue(g3.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g4.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g4) == 5);
        assertTrue(g4.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g5.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g5) == 5);
        assertTrue(g5.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g6.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g6) == 5);
        assertTrue(g6.getPrimitiveType() instanceof TRealDouble);

        // explicitly typed vectors
        g1 = DataTypeAccessor.getDataType("int8([1:2:10])");
        g2 = DataTypeAccessor.getDataType("single([-1:2:10])");
        g3 = DataTypeAccessor.getDataType("uint16([1 3 5 7 9])");
        g4 = DataTypeAccessor.getDataType("int32([1 -3 5 -7 9])");
        g5 = DataTypeAccessor.getDataType("uint32([1,3,5,7,9])");
        g6 = DataTypeAccessor.getDataType("double([1,3 5 7,9])");
        
        assertTrue(g1.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g1) == 5);
        assertTrue(g1.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g1.getPrimitiveType()).getNBits() == 8);
        assertTrue(((TRealInteger)g1.getPrimitiveType()).isSigned());
        assertTrue(g2.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g2) == 6);
        assertTrue(g2.getPrimitiveType() instanceof TRealSingle);
        assertTrue(g3.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g3) == 5);
        assertTrue(g3.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g3.getPrimitiveType()).getNBits() == 16);
        assertTrue(!((TRealInteger)g3.getPrimitiveType()).isSigned());
        assertTrue(g4.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g4) == 5);
        assertTrue(g4.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g4.getPrimitiveType()).getNBits() == 32);
        assertTrue(((TRealInteger)g4.getPrimitiveType()).isSigned());
        assertTrue(g5.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g5) == 5);
        assertTrue(g5.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g5.getPrimitiveType()).getNBits() == 32);
        assertTrue(!((TRealInteger)g5.getPrimitiveType()).isSigned());
        assertTrue(g6.isVector());
        assertTrue(DataTypeAccessor.getXDimension(g6) == 5);
        assertTrue(g6.getPrimitiveType() instanceof TRealDouble);

        // simple matrix types
        g1 = DataTypeAccessor.getDataType("[1:2:10 ; 1:2:10]]");
        g2 = DataTypeAccessor.getDataType("[-1:2:10 ; 1 3 5 7 9]");
        g3 = DataTypeAccessor.getDataType("[1 3 5 7 9 ; 1:2:10]");
        g4 = DataTypeAccessor.getDataType("[1 -3 5 -7 9; 1,3,5,7,9]");
        g5 = DataTypeAccessor.getDataType("[1,3,5,7,9; 1,3 5 7,9]");
        g6 = DataTypeAccessor.getDataType("[1,3 5 7,9 ; 1:2:10]");
        
        assertTrue(g1.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g1) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g1) == 5);
        assertTrue(g1.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g2.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g2) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g2) == 6);
        assertTrue(g2.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g3.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g3) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g3) == 5);
        assertTrue(g3.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g4.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g4) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g4) == 5);
        assertTrue(g4.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g5.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g5) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g5) == 5);
        assertTrue(g5.getPrimitiveType() instanceof TRealDouble);
        assertTrue(g6.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g6) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g6) == 5);
        assertTrue(g6.getPrimitiveType() instanceof TRealDouble);
        


        // simple matrix types
        g1 = DataTypeAccessor.getDataType("int8([1:2:10 ; 1:2:10]])");
        g2 = DataTypeAccessor.getDataType("single([-1:2:10 ; 1 3 5 7 9])");
        g3 = DataTypeAccessor.getDataType("uint16([1 3 5 7 9 ; 1:2:10])");
        g4 = DataTypeAccessor.getDataType("int32([1 -3 5 -7 9; 1,3,5,7,9])");
        g5 = DataTypeAccessor.getDataType("uint32([1,3,5,7,9; 1,3 5 7,9])");
        g6 = DataTypeAccessor.getDataType("double([1,3 5 7,9 ; 1:2:10])");
        
        assertTrue(g1.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g1) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g1) == 5);
        assertTrue(g1.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g1.getPrimitiveType()).getNBits() == 8);
        assertTrue(((TRealInteger)g1.getPrimitiveType()).isSigned());
        assertTrue(g2.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g2) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g2) == 6);
        assertTrue(g2.getPrimitiveType() instanceof TRealSingle);
        assertTrue(g3.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g3) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g3) == 5);
        assertTrue(g3.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g3.getPrimitiveType()).getNBits() == 16);
        assertTrue(!((TRealInteger)g3.getPrimitiveType()).isSigned());
        assertTrue(g4.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g4) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g4) == 5);
        assertTrue(g4.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g4.getPrimitiveType()).getNBits() == 32);
        assertTrue(((TRealInteger)g4.getPrimitiveType()).isSigned());
        assertTrue(g5.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g5) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g5) == 5);
        assertTrue(g5.getPrimitiveType() instanceof TRealInteger);
        assertTrue(((TRealInteger)g5.getPrimitiveType()).getNBits() == 32);
        assertTrue(!((TRealInteger)g5.getPrimitiveType()).isSigned());
        assertTrue(g6.isMatrix());
        assertTrue(DataTypeAccessor.getXDimension(g6) == 2);
        assertTrue(DataTypeAccessor.getYDimension(g6) == 5);
        assertTrue(g6.getPrimitiveType() instanceof TRealDouble);
        
    }
}
