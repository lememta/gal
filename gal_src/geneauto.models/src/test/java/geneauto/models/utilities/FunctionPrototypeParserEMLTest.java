/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/test/java/geneauto/models/utilities/FunctionPrototypeParserEMLTest.java,v $
 *  @version	$Revision: 1.2 $
 *	@date		$Date: 2010-11-04 14:01:31 $
 *
 *  Copyright (c) 2006-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;

import org.junit.Assert;
import org.junit.Test;

/**
 */
public class FunctionPrototypeParserEMLTest {
	
	/** If true, then produces extra monitoring output to System.out */
	private boolean debug = false;

	/**
	 * Tests the FunctionPrototypeParserEML
	 * 
	 * NOTE: The FunctionPrototypeParserEML contains several parsing phases. We
	 * are here only testing the initial splitting of the prototype string in 
	 * the parser constructor
	 */
    @Test
    public void testParser() {
    	
		if (debug) {
			System.out.println("\nTest " + getClass().getName() + " starting.\n");
		}
		
    	Block b = null; // Dummy block 
    	
    	// Here we test parsing of different *valid* strings. 
    	// We don't do negative tests
    	// See also http://www.mathworks.com/help/toolbox/simulink/sfg/bq4g1es-1.html
    	
    	String[] ss = {
    			// Some basic tests
        		"void myFunction", 
        		"void myFunction_123_456", 
        		"void myFunction()", 
        		"void myFunction(int16 u1)", 
        		"void myFunction(int16  u1, uint16 y1)", 
        		"void myFunction(double u1, single y1, int8 p1)", 
        		"void myFunction(uint8  u1, int16 y1 , uint16 p1)", 
        		"void   \t  myFunction(   uint8  u1, \tint16 y1\t, \tuint16 p1\t\t)",
        		
        		// Probably no 'void' should work also, although didn't find any direct 
        		// reference in the documentation
        		"myFunction(uint8  u1, int16 y1 , uint16 p1)",
        		
    			// Input options from the url above
        		"function(void)",
        		"function(int16 u1)",
        		"function(int16 u1[1])",
        		"function(int16 u1[10])",
        		//"function(int16 u1[])", 		Currently not supported
        		"function(int16 u1[3][5])",
        		//"function(int16 u1[][])",		Currently not supported
        		
    			// Output options from the url above
        		"function(int16 y1[1])",
        		"function(int16 y1[10])",
        		"function(int16 y1[3][5])",
        		
        		// Parameter arguments
        		"function(int16 p1)",
        		"function(int16 p1[1])",
        		"function(int16 p1[10])",
        		//"function(int16 p1[])",		Currently not supported
        		"function(int16 p1[3][5])",
        		//"function(int16 p1[][])",		Currently not supported

        		// Calls with output variable
        		"int16  y1 = myfunction",
        		"uint16 y1 = myfunction()",
        		"int32  y2 = myFunction(uint8  u1, int16 y1 , uint16 p1)", 
        		"int32  y2[1]  = myFunction(uint8  u1, int16 y1 , uint16 p1)",
        		"int32  y2[10] = myFunction(uint8  u1, int16 y1 , uint16 p1)",
        		//"int32  y2[] = myFunction(uint8  u1, int16 y1 , uint16 p1)", Currently not supported
        		"int32  y2[3][5] = myFunction(uint8  u1, int16 y1 , uint16 p1)",
        		//"int32  y2[][] = myFunction(uint8  u1, int16 y1 , uint16 p1)", Currently not supported
        		"\t\tint32  y2 = myFunction(uint8  u1, int16 y1 , uint16 p1)\t\t", 
        		
        		// Other tests
        		// Long function name
        		"_my_function_aaaaaaaaaaaaaaa(uint32 y1[2], double u1)",
        		"_my_function_aaaaaaaaaaaaaaaaaaaaaaaaaaa(uint32 y1[2], double u1)",
        		"_my_function_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa(uint32 y1[2], double u1)",
        		"_my_function_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa(uint32 y1[2], double u1)",
        		"_my_function_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa(uint32 y1[2], double u1, double u2, double u3, double u4, double u5, double u6, double u7, double u8, double u9, double u10)",

    	};
    	
    	for (String s : ss) {
    		testString(s, b);	
    	}

		if (debug) {
			System.out.println("\nTest " + getClass().getName() + " completed.\n");
		}
    }

	/**
	 * Tests one string. If the string is bad, then either the constructor
	 * throws an error and we fail the test or the constructor stops the Java
	 * process by raising a CRITICAL_ERROR in EventHandler.
	 * 
	 * @param s String to parse
	 * @param b Dummy block
	 */
	private void testString(String s, Block b) {
		if (debug) {
			System.out.println("Testing: " + s);
		}
		try {
			new FunctionPrototypeParserEML(s, b);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
