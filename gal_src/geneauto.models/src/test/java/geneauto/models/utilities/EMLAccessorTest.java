/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/test/java/geneauto/models/utilities/EMLAccessorTest.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2012-02-20 09:16:51 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.models.gacodemodel.expression.ConstantListExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.utilities.emlconverter.EMLAccessor;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

/**
 * 
 *
 */
public class EMLAccessorTest extends TestCase {

	/**
	 * Unit test 1 for eml parsing
	 * 
	 * TODO Unimplemented test!
	 * 
	 */
	@Test
	public void testEmlToCodeModel() {
		assertEquals("Eml expression parsing : test 1", true, true);
	}

	/**
	 * Checks, if the argument is a list of lists. I.e. type is a 2D array, but
	 * we avoid invoking type checking here. Instead, just check the structure
	 */
	private boolean isListOfLists(Expression exp) {
		if (exp instanceof GeneralListExpression) {
			List<Expression> subExps = exp.getExpressions();
			if (subExps == null) {
				return false;
			}
			for (Expression subExp : subExps) {
				if (subExp instanceof GeneralListExpression) {
					List<Expression> subExps2 = subExp.getExpressions();
					if (subExps2 == null) {
						return false;
					}
					for (Expression subExp2 : subExps2) {
						if (subExp2 instanceof ListExpression) {
							return false; // must be a non-list
						}
					}
				} else {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Checks, if the argument expression has at most 2D. I.e, its type is a 2D array, 
	 * but we avoid invoking type checking here. Instead, just check the structure
	 */
	private boolean isMax2D(Expression exp) {
		if (exp instanceof GeneralListExpression) {
			List<Expression> subExps = exp.getExpressions();
			if (subExps == null) {
				return false; // malformed list expression
			}
			for (Expression subExp : subExps) {
				if (!isMax1D(subExp)) {
					return false;
				}
			}
			return true;
		}
		return true;
	}

	/**
	 * Checks, if the argument expression has at most 1D. I.e, its type is a 1D array, 
	 * but we avoid invoking type checking here. Instead, just check the structure
	 */
	private boolean isMax1D(Expression exp) {
		if (exp instanceof GeneralListExpression) {
			List<Expression> subExps = ((GeneralListExpression) exp)
					.getExpressions();
			if (subExps == null) {
				return false; // malformed list expression
			}
			for (Expression subExp : subExps) {
				if (subExp instanceof ListExpression) {
					return false;
				}
			}
			return true;
		} else if (exp instanceof ConstantListExpression) {
			Expression subExp = ((ConstantListExpression) exp)
					.getExpression();
			if (subExp == null) {
				return false; // malformed list expression
			}
			// Return true, if subExp is scalar
			return !(subExp instanceof ListExpression);
		} else {
			// Scalar
			return true;			
		}
	}

	/**
	 * Unit test for 1D Cell Expression parsing
	 */
	@Test
	public void testCellExpression1D() {
		List<String> stringsToParse = new ArrayList<String>();
		Expression exp;
		
		// Expected positive tests
		stringsToParse.add("  {e1  e2  e3} ");
		stringsToParse.add("  {e1, e2, e3} ");
		stringsToParse.add("  {[e1], [e2], [e3]} ");
		stringsToParse.add("  {[e1], [e2, e3]  } ");
		stringsToParse.add("  {e1,   [e2, e3]  } ");
		stringsToParse.add("  {e1,   [e2 e3]  } ");
		stringsToParse.add("  {e1   [e2 e3]  } ");
		
		for (String s : stringsToParse) {
			exp = EMLAccessor.parseCellExpression1D(s);
			assertTrue("Parsed expression must have at most 2 dimensions. Source: " + s, 
					isMax2D(exp));		
		}
		
		// Expected negative tests
		stringsToParse.clear();
		
		stringsToParse.add("  {e1  e2;  e3 e4} ");
		stringsToParse.add("  e1  e2  e3 e4 ");
		
		for (String s : stringsToParse) {
			exp = EMLAccessor.parseCellExpression1D(s);
			assertTrue("Incompatible string not rejected by parser. Source: " + s, 
					exp == null);
		}		
		
	}

}
