/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TRealDouble.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2012-03-08 10:01:49 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.utils.PrivilegedAccessor;

/**
 * ISO/IEC 9899 double-precision floating point
 *
 */
public class TRealDouble extends TRealFloatingPoint {

	public TRealDouble(){
		
	}
	
	public String getTypeName(){
		return "REAL";
	}

    @Override
    public Expression getDefaultValue() {
        DoubleExpression realExp = (DoubleExpression) PrivilegedAccessor
            .getObjectInPackage(DoubleExpression.class, this);
        realExp.setLitValue("0.0");
        return realExp;
    }
    
    
    public boolean isSubType(GADataType dt) {
        if (dt instanceof TRealDouble) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TRealFloatingPoint scaleFor(double value) {
        return this;
    }
    
    
    
}