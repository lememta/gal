/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TInherited.java,v $
 *  @version	$Revision: 1.14 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.gaenumtypes.TypeInheritanceRule;

/**
 * Inherited type - the element inherits its type by some fixed rule.
 * 
 * In Gene-Auto an approach has been taken to distinguish an inherited type from
 * an unknown type. In both cases the proper type of the element is unknown, but
 * in the first case there is a specific rule given, how to deduce this type,
 * while in the second case this information is missing or implicit. 
 *
 * The type of the object must be resolved during the code generation process.
 * 
 */
public class TInherited extends GADataType {
	public TypeInheritanceRule rule;


	public TInherited(){

	}

	protected String attrToString() {
		return super.attrToString() 
			+ "rule: " + rule.name();
	}

    /**
     * Sub-typing relation cannot be defined for the inherited type, 
     * since we do not know what is the eventual type.
     */
    @Override
    public boolean isSubType(GADataType dt) {
        EventHandler.handle(EventLevel.ERROR, 
                getClass().getSimpleName() + ".isSubType", "", 
                "Sub-typing relation is not defined for the inherited type."
                + "\n Element: " + this.getReferenceString());
        return false;
    }

}