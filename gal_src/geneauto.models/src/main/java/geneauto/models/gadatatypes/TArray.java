/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TArray.java,v $
 *  @version	$Revision: 1.87 $
 *	@date		$Date: 2012-03-08 10:01:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.NumericExpression;
import geneauto.models.utilities.DataTypeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Array data type
 * 
 */
public class TArray extends GADataType {

    protected TPrimitive baseType;

    /**
     * length of an array. In case the array has several dimensions each
     * dimension must appear as a separate expression in the list.
     * 
     * Dimension is assumed to be either an IntegerExpression (literal value) 
     * or a VariableExpression referring to an integer constant.  
     */
    protected List<Expression> dimensions = new ArrayList<Expression>();

    public TArray() {
        super();
    }

    /**
     * @param dims
     *            List of Expression containing dimensions
     */
    public TArray(List<Expression> dims) {
        this();
        setDimensions(dims);
    }

    /**
     * Constructs a multidimensional array type
     * 
     * @param dims
     *            List of Expressions containing dimensions
     * @param primitive
     *            base type of the array
     */
    public TArray(List<Expression> dims, TPrimitive primitive) {
        this(dims);
        setBaseType(primitive);
    }

    /**
     * Constructs a 1 dimensional array type (a vector)
     *  
     * @param length 
     *            Expression specifying the length of the vector
     * @param primitive
     *            base type of the array
     */
    public TArray(Expression length, TPrimitive primitive) {
        super();
        List<Expression> dims = new ArrayList<Expression>();
        dims.add(length);
        setDimensions(dims);
        setBaseType(primitive);
    }

    /**
     * Constructs a 2 dimensional array type (a matrix)
     *  
     * @param xLength 
     *            Expression specifying the length of the first dimension (number of rows)
     * @param xLength 
     *            Expression specifying the length of the second dimension (number of columns)
     * @param primitive
     *            base type of the array
     */
    public TArray(Expression xLength, Expression yLength, TPrimitive primitive) {
        super();
        List<Expression> dims = new ArrayList<Expression>();
        dims.add(xLength);
        dims.add(yLength);
        setDimensions(dims);
        setBaseType(primitive);
    }

    @Override
    public boolean isVector() {
        return getDimensions().size() == 1;
    }

    @Override
    public boolean isMatrix() {
        return getDimensions().size() == 2;
    }

    public TPrimitive getBaseType() {
        return baseType;
    }

    public void setBaseType(TPrimitive baseType) {
        this.baseType = (TPrimitive) baseType.getCopy();
        this.baseType.setParent(this);
    }

    @Override
    public int getDimensionality() {
        if (this.dimensions.size() == 0) {
            EventHandler.handle(EventLevel.ERROR, "models.TArray", 
                    "GED0031",
                    "No index qualifier for array \""
                            + this.getReferenceString() + "\"", "");
            return 0;
        }
        return dimensions.size();
    }

    /**
     * Takes list of expressions as argument and sets the value of dimensions
     * attribute. The old values of dimensions attribute are deleted
     * 
     * @param dimensions
     *            the "dimensions" to set.
     */
    public boolean setDimensions(List<Expression> dimensions) {
        this.dimensions.clear();

        for (Expression dimExpr : dimensions) {
            if (!addDimension(dimExpr)) {
                return false;
            }
        }

        return true;

    }

    /**
     * Takes list of expressions as argument and copies the values from this
     * list ot dimensions argument.
     * the dimensions list is cleared before starting to copy new dimensions
     * 
     * @param dimensions
     *            the "dimensions" to set.
     */
    public boolean copyDimensions(List<Expression> dimensions) {
        this.dimensions.clear();

        for (Expression dimExpr : dimensions) {
            if (!addDimension(dimExpr.getCopy())) {
                return false;
            }
        }

        return true;

    }

    /**
     * Takes single expression as an argument and sets the value of dimensions
     * attribute. The old values of dimensions attribute are deleted
     * 
     * @param dimensions
     *            the "dimensions" to set.
     */
    public boolean setDimensions(Expression dimensions) {
        this.dimensions.clear();
        return addDimension(dimensions);
    }

    /**
     * Adds one dimension to array. The argument is an expression that
     * represents array dimension. If this argument is list expression,
     * addDimension is called for each of its members. Otherwise the method
     * checks if expression can be used as dimension expression using expression
     * method canBeArrayDimensionand if yes adds new member to dimensions array
     * 
     * @param dimension
     * @return true if adding the dimension was successful, false otherwise
     */
    public boolean addDimension(Expression dimension) {

        if (dimension == null
                || (dimension.getDataType() != null && !dimension.isScalar())) {
            String msg = "Dimension must be a scalar expression. " 
                + "\nExpression: ";
            if (dimension == null) {
                msg += "null";
            } else {
                msg += dimension.getReferenceString();
                msg += ", dataType: "
                        + dimension.getDataType().getShortReferenceString();
            }
            EventHandler.handle(EventLevel.ERROR, "TArray.addDimension", 
                    "GED0030",
                    msg);
            return false;
        }

        dimension.setParent(this);
        dimensions.add(dimension);
        return true;
    }

    public boolean addDimensions(List<Expression> dims) {
        for (Expression dim : dims) {
            if (!addDimension(dim)) {
                return false;
            }
        }
        return true;
    }

    public List<Expression> getDimensions() {
        return dimensions;
    }

    /**
	 * Reduces the dimensionality of the datatype
	 * 
	 * NB! The method modifies the underlying object in place and optionally 
	 * returns a new object. Thus:
	 * 	  USE ALWAYS the returned object
	 * 	  MAKE SURE  that it is safe to modify or discard the underlying object
	 */
	public GADataType reduceDimensionality() {
	    if (dimensions == null || dimensions.isEmpty()) {
	        EventHandler.handle(EventLevel.ERROR, "reduceDimensionality()", "", 
	                "Array type has null or empty dimensions: " + toString()
	                + "\nReference: " + getReferenceString());
	        return null;
	    }
	    // Remove the highest dimension
	    dimensions.remove(0);
	    
	    if (dimensions.isEmpty()) {
	    	return baseType;
	    } else {
	    	return this;
	    }
	}

	/**
     * @return dimensions of the Array as Array
     */
    public Expression[] dimensionsToArray() {
        Object[] expressions = dimensions.toArray();
        Expression[] result = new Expression[expressions.length];
        int i = 0;
        for (Object obj : expressions) {
            result[i] = (Expression) obj;
            i++;
        }
        return result;
    }

    @Override
    public int[] getDimensionsLengths() {
        int length = dimensions.size();
        int[] result;

        result = new int[length];
        for (int i = 0; i < length; i++) {
            result[i] = (int) dimensions.get(i).evalReal().getRealValue();
        }

        return result;
    }

    @Override
	public int getDimensionsLength(int dimIdx) {
    	Expression dim = dimensions.get(dimIdx);
    	
    	if (dim == null) {
            EventHandler.handle(
            		EventLevel.ERROR,
                    this.getClass().getCanonicalName(), 
                    "",
                    "Invalid dimension index "
                    + dimIdx + " requested in data type " 
                    + this.toString());
            return -1;
    	} else {
    		return (int) dim.evalReal().getRealValue();
    	}
	}

	/**
     * Returns true when the type is scalar type (for all types except TArray
     * and its subtypes
     * 
     * @return boolean
     */
    public boolean isScalar() {
        return isScalar(true);
    }

    /**
     * At the moment we assume that all arrays are non-scalar, 
     * even if there are all dimensions are of length 1.
     */
    @Override
    public boolean isScalar(boolean isForgiving) {
        Expression firstDimension = dimensions.get(0);

        // we have only one dimension with length 1 => raise warning message
        if ((getDimensionality() == 1)
                && ((int) (firstDimension.evalReal().getRealValue()) == 1)) {
            EventHandler.handle(EventLevel.WARNING, 
                    getClass().getSimpleName() + ".isScalar", "GEM0088",
                    "An array with single dimension and length 1."
                            + "\nDataType: " + getReferenceString(), "");
        }

        /*
         * false in all other cases
         * 
         * NB! we return legal value also in case of incomplete array definition
         * assuming that error was thrown already in getArity
         */
        return false;
    }

    protected String attrToString() {
        String dimPart = "";
        String tmp     = "";
        boolean first = true;
        for (Expression e : getDimensions()) {
        	NumericExpression val = e.evalReal();
            if (val != null) {
                double d = val.getRealValue();
                if ((int) (d) == d) {
                    tmp = (int) d + "";                   
                } else {
                    tmp = d + "";                    
                }
            } else {
                tmp = e.toString();                
            }
            if (first) {
                first = false;
                dimPart = tmp;
            } else {
                dimPart += ", " + tmp;
            }            
        }
        dimPart = "[" + dimPart + "]";
        String superPart = super.attrToString();
        if (superPart != null && !superPart.isEmpty()) {
            superPart += ", ";
        }
        return  superPart + "baseType: {"
                + baseType.toString() + "}, dimensions: " + dimPart;
    }

    /**
     * @return the default value of elements of this type
     */
    public Expression getDefaultValue() {
        // NOTE: The returned value is a scalar.
        // The scalar expansion feature of GASysteModel and GACodeModel
        // languages expand it, when required.
        return getBaseType().getDefaultValue();
    }

    public boolean isSubType(TArray dt) {
        if (dt.getDimensionality() != getDimensionality()) {
            return false;
        } else if (!DataTypeUtils.checkDimensions(this, dt, false, false)) {
            return false;
        } else {
            return getBaseType().isSubType(dt.getBaseType());
        }
    }

    public boolean isSubType(GADataType dt) {
        if (!dt.isScalar()) {
            return isSubType((TArray) dt);
        } else {
            return false;
        }
    }

    /**
     * @return true, when the array is of following form: 1xN (row matrix) or
     *         Nx1 (column matrix).
     */
    public boolean isVectorMatrix() {
        int[] dimLengths = this.getDimensionsLengths();
        if (dimLengths.length == 2) {
            if (dimLengths[0] == 1) {
                return true;
            } else if (dimLengths[1] == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return true, when the array contains exactly one element (no matter of
     *         the dimensionality).
     */
    public boolean isSingletonArray() {
        for (int n : this.getDimensionsLengths()) {
            if (n != 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return true, when the array is of following form: 1xN (row matrix)
     */
    public boolean isRowMatrix() {
        int[] dimLengths = this.getDimensionsLengths();
        if (dimLengths.length == 2) {
            if (dimLengths[0] == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return true, when the array is of following form: Nx1 (column matrix)
     */
    public boolean isColMatrix() {
        int[] dimLengths = this.getDimensionsLengths();
        if (dimLengths.length == 2) {
            if (dimLengths[1] == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public GADataType normalize(boolean full) {
        GADataType resultType = this;

        if (full) {
            // Convert vector matrixes        
        	// NB! this destroys the initial type by moving the dimension 
        	// expressions to new type. 
            int[] dimLengths = resultType.getDimensionsLengths();
            if (dimLengths.length == 2) {
                if (dimLengths[0] == 1) {
                    resultType = new TArray(this.getDimensions().get(1),
                            this.getPrimitiveType());
    
                } else if (dimLengths[1] == 1) {
                    resultType = new TArray(this.getDimensions().get(0),
                            this.getPrimitiveType());
                }
            }
        }

        // Check, if the type is or was reduced to a singleton.
        if (resultType.isSingletonArray()) {
            resultType = resultType.getPrimitiveType();
        }

        return resultType;
    }

    /**
     * Convert a vector of size A in a matrix of size 1xA.
     * 
     * @return
     */
    public GADataType toRowMatrix() {
        GADataType returnType = null;
        if (this.getDimensionality() == 1) {
            List<Expression> dims = new ArrayList<Expression>();
            dims.add(new IntegerExpression(1));
            dims.add(dimensions.get(0).getCopy());

            returnType = new TArray(dims, baseType.getCopy());
        }
        return returnType;
    }

    /**
     * Convert a vector of size A in a matrix of size Ax1.
     * 
     * @return
     */
    public GADataType toColMatrix() {
        GADataType returnType = null;
        if (this.getDimensionality() == 1) {
            List<Expression> dims = new ArrayList<Expression>();
            dims.add(dimensions.get(0).getCopy());
            dims.add(new IntegerExpression(1));

            returnType = new TArray(dims, baseType.getCopy());
        }
        return returnType;
    }

    /**
     * Checks if the current data type and type given in argument are equal 
     */
	@Override
	public boolean equalsTo(GADataType other) {
	    return equalsTo(other, false);
	}
	
	/**
	 * @param other data type to compare with
	 * @param checkDimensionTypes flag, if true -> compares 
	 * data types of corresponding dimensions
	 * 
     * Checks if the current data type and type given in argument are equal 
     */
	public boolean equalsTo(GADataType other, boolean checkDimensionTypes) {
	    
        // check the metatype
        if (!equalClass(other))
            return false;
        
        TArray aOther = (TArray) other;
        // check the base type
        if (!this.getBaseType().equalsTo(aOther.getBaseType())) {
            return false;
        }
        
        // check dimensions
        return DataTypeUtils.checkDimensions(this, aOther, false, checkDimensionTypes);
    }

}