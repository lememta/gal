/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TPointer.java,v $
 *  @version	$Revision: 1.28 $
 *	@date		$Date: 2012-03-08 10:01:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;


/**
 * Pointer data type. Pointer can reference any object of any data type,
 * including another pointer.
 * 
 */
public class TPointer extends GADataType {

	protected GADataType baseType;

	public TPointer() {
		super();
	}

	public TPointer(GADataType baseType) {
		this();
		setBaseType(baseType);
	}

	public GADataType getBaseType() {
		return baseType;
	}

	public TPrimitive getPrimitiveType() {
		return this.baseType.getPrimitiveType();
	}

	/**
	 * Method setBaseType.
	 * 
	 * @param baseType
	 *            the "baseType" to set.
	 */
	public void setBaseType(GADataType baseType) {
		if (baseType.getParent() == null) {
			this.baseType = baseType;			
		} else {
			this.baseType = baseType.getCopy();
		}
		this.baseType.setParent(this);
	}

	protected String attrToString() {
		return super.attrToString() + "baseType{" + baseType.toString() + "}";
	}
	

    /**
     * A pointer can only be compared with and assigned to another pointer.
     */
    @Override
    public boolean isSubType(GADataType dt) {
    	if (!(dt instanceof TPointer)) {
    		return false;
    	} else {
    		GADataType baseDT1 = getBaseType();
    		GADataType baseDT2 = ((TPointer) dt).getBaseType();
    		if (baseDT1 == null) {
    			EventHandler.handle(EventLevel.ERROR, 
    					getClass().getSimpleName() + ".isSubType(..)", "", 
    					"Cannot determine SubType relation. BaseType undefined in the left type:" +
    					"\n  " + this.toString());
    			return false;
    		}
    		if (baseDT2 == null) {
    			EventHandler.handle(EventLevel.ERROR, 
    					getClass().getSimpleName() + ".isSubType(..)", "", 
    					"Cannot determine SubType relation. BaseType undefined in the right type:" +
    					"\n  " + dt.toString());
    			return false;
    		}
    		return baseDT1.isSubType(baseDT2);
    	}
    }

	@Override
	public boolean equalsTo(GADataType other) {
		if (!equalClass(other))
			return false;
		
		return getBaseType().equalsTo(((TPointer) other).getBaseType());
	}
	
}