/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TVoid.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2012-03-08 10:01:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

/**
 * Void data type as in C and Java.
 * 
 * More formally, it is actually a unit type - a type with exactly one element.
 * But conventionally this is called void.
 * 
 * TODO Consider removing the void type. It does not fit well in the type
 * hierarchy. AnTo 8.3.2012
 */
public class TVoid extends TPrimitive {

    public TVoid() {

    }

    public String getTypeName() {
        return "void";
    }

    /**
     * An element of void type can only be compared with and assigned to another
     * void element.
     */
    @Override
    public boolean isSubType(GADataType dt) {
        return dt instanceof TVoid;
    }

    @Override
    public boolean equalsTo(GADataType dt) {
        return dt instanceof TVoid;
    }
}