/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TRealFixedPoint.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2012-03-08 10:01:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

/**
 * @version 1.0
 * @created 07-avr.-2008 14:24:59
 */
public class TRealFixedPoint extends TRealNumeric {

    protected int gain;
    protected int nBits;
    protected int offset;

    public TRealFixedPoint() {

    }

    protected String attrToString() {
        return super.attrToString() + ", gain: " + gain + ", nBits: " + nBits
                + ", offset" + offset;
    }
    
    @Override
    public boolean isSubType(GADataType dt) {
        EventHandler.handle(EventLevel.ERROR, 
                getClass().getSimpleName() + ".isSubType()", "", 
                "Call to unimplemented method." +
                "\n Element: " + this.getReferenceString());
        return false;
    }

}