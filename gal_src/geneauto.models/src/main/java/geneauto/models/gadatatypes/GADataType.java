/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/GADataType.java,v $
 *  @version	$Revision: 1.80 $
 *	@date		$Date: 2012-03-08 10:01:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.CustomType_CM;
import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.models.gacodemodel.Module;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.genericmodel.GAModelElement;

import java.util.Collections;
import java.util.List;

/**
 * Gene-Auto datatype. More abstract than C types: primitive types + Vector,
 * Matrix, Complex numeric...
 * 
 */
public abstract class GADataType extends GAModelElement {
    /**
     * reference to the Module containing current element
     */
    protected Module module;

    /**
     * Native types are known to the code generator and defined globally, if
     * required. Custom types are defined in the module where they belong.
     */
    protected boolean isGANative() {
        return true;
    }

    public GADataType() {
        super();
    }

    /**
     * Returns the C string corresponding to the base type instance. I.e. the
     * string that is used when declaring a variable of this type, e.g: int myAr
     * myStruct
     * 
     * Size information is not returned.
     * 
     * Note! For custom data types this is not the same as the definition of
     * that type (i.e. typedef). The definition is obtained from the related
     * CodeComponent.
     * 
     */
    public String getTypeName() {
        return getClass().getSimpleName();
    }
    
    public String getInstance() {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "GADataType", "GEM0012",
                "Call to unimplemented or unsupported method: getInstance() Element: "
                        + getReferenceString(), "");
        return null;
    }

    /**
     * TODO: (to AnRo) add comment
     * @return
     */
    public CustomType_CM toCustomType() {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "GADataType", "GEM0012",
                "Call to unimplemented or unsupported method: toCustomType() Element: "
                        + getReferenceString(), "");
        return null;
    }

    /**
     * Returns the type itself for all types except arrays and pointers. For the
     * latter returns the primitive type at the deepest level. I.e. double for
     * **double and double ar[m][n] Note! This is different from the baseType
     * that is *double for the first case and double ar[n] for the second.
     */
    public TPrimitive getPrimitiveType() {
        if (this instanceof TPrimitive) {
            return (TPrimitive) this;
        } else {
            return (TPrimitive) ((TArray) this).getBaseType();
        }
    }

    /**
     * Returns true when the type is scalar type (for all types except TArray
     * and its sub-types
     * 
     * @return boolean
     */
    public boolean isScalar() {
        return isScalar(true);
    }

    public boolean isScalar(boolean isForgiving) {
        return true;
    }

    /**
     * @return true, when the dataType is an Array with 1 dimension.
     */
    public boolean isVector() {
        return false;
    }

    /**
     * @return true, when the dataType is an Array with 2 dimensions.
     */
    public boolean isMatrix() {
        return false;
    }

    /**
     * @return true, when the array is of following form: 1xN (row matrix) 
     * or Nx1 (column matrix). 
     */
    public boolean isVectorMatrix() {
        return false;
    }

    /**
     * @return true, when the array is of following form: 1xN (row matrix) 
     */
    public boolean isRowMatrix() {
        return false;
    }

    /**
     * @return true, when the array is of following form: Nx1 (column matrix) 
     */
    public boolean isColMatrix() {
        return false;
    }

    /**
     * @return true, when the array contains exactly one element 
     * (no matter of the dimensionality).
     */
    public boolean isSingletonArray() {
        return false;
    }


    /**
     * Returns prefix of a variable when composing variable instance (e.g. * in
     * case of reading value of pointer variable in C) This method shall be
     * implemented by printer package
     * 
     * @param Variable_CM
     *            var
     * @return String
     */
    protected String getVarPrefix(Variable_CM var) {
        return "";
    }

    /**
     * Returns suffix of a variable when composing variable instance (e.g. array
     * index) This method shall be implemented by printer package
     * 
     * @param Variable_CM
     *            var
     * @return String
     */
    protected String getVarPostfix(Variable_CM var) {
        return "";
    }

    /**
     * Composes variable name
     * 
     * NB! This is default configuration. The printer package shall override 
     * this method for each data type where the presentation differs from this 
     * default  
     * 
     * @param Variable_CM
     *            var
     * @return String
     */
    public String getVarInstance(Variable_CM var) {
        GADataType dt = var.getDataType();
        return dt.getVarPrefix(var) + var.getInstance() + dt.getVarPostfix(var);
    }

    /**
     * Returns the code that initialises the particular data object.
     * 
     * See also the style argument.
     * 
     * @param style
     *            = StructAssignStyle.Default - Default style =
     *            StructAssignStyle.NameFields - MembersByName
     * 
     * 
     *            Only matters for structs. E.g
     * 
     *            assuming that there exists a following type definition:
     * 
     *            typedef struct {int a, char b, double c} myStruct;
     * 
     *            Then:
     * 
     * 
     *            StructAssignStyle.Default => myStruct = {1, 'x', 0.3};
     * 
     *            StructAssignStyle.NameFields => myStruct.a = 1; myStruct.b =
     *            "x"; myStruct.c = 0.3;
     * 
     *            This argument can be optional.
     */
    public Statement getVarInitializer(Variable_CM var) {
        VariableExpression varExp = new VariableExpression(var);
        Expression init = var.getInitialValue();
        if (init == null) {
            init = getDefaultValue();
        }
        return new AssignStatement(AssignOperator.SIMPLE_ASSIGN, varExp, init);
    }

    /**
     * @return the dimensionality of the data element Dimensionality of: scalar
     *         data types is 0, array with one dimension (or vector) 1, etc.
     * 
     *         Array data types must override this method to return number of
     *         dimensions.
     * 
     */
    public int getDimensionality() {
        return 0;
    }

    /**
     * Returns list of dimension expressions for the given data element In case
     * of scalar data types the array contains one element Array data types must
     * redefine this method
     * 
     * @return
     */
    public List<Expression> getDimensions() {
        return Collections.emptyList();
    }

    /**
     * Returns string describing the data type to be used in messages
     * 
     * @return String
     */
    public String toString() {
        String attrPart = attrToString();
        if (attrPart != null && !attrPart.isEmpty()) {
            attrPart = " : " + "(" + attrPart + ")";
        }        
        return getClass().getSimpleName() + attrPart;
    }

    /**
     * Returns significant attribute values to be included in the string
     * composed by toString method. Each subclass of GADataType introducing new
     * attributes must redefine this method
     * 
     * @return String
     */
    protected String attrToString() {
        return "";
    }

    /**
     * Returns array of dimension length values for the given data element 
     * In case of scalar data types the array contains one element 
     * Array data types must redefine this method
     * 
     * (AnTo) Wouldn't it be more reasonable to return null for scalars?
     * Currently, both vectors and scalars return an array of length 1.   
     * 
     * @return
     */
    public int[] getDimensionsLengths() {
        int[] result = new int[1];
        result[0] = 0;
        return result;
    }

    /**
     * Returns value of single dimension length for the given data element 
     * In case of scalar the returned value is always 0
     * Array data types must redefine this method
     * 
     * @param dimIdx  -- index of dimension index (0..)
     * @return int
     */
    public int getDimensionsLength(int dimIdx) {
        if (dimIdx == 0) {
        	return 0;
        }
        
        EventHandler.handle(
        		EventLevel.ERROR,
                this.getClass().getCanonicalName(), 
                "GED0029",
                "Invalid dimension index "
                + dimIdx + " requested in data type " 
                + this.toString()
                + "\n Scalar datatypes can have only single dimension.");

        return 0;
    }
    
    /**
     * 
     * @return size of array if data type instance of TArray, 0 - for Data type
     *         that is not instance of TArray;
     */
    public int getSize() {
        int result = 1;
        if (getDimensions().size() == 0) {
            return 0;
        }
        for (Expression dim : this.getDimensions()) {
            if (dim.eval() != null) {
                result *= dim.evalReal().getRealValue();
            }
        }
        return result;
    }

    /**
     * Overrides the superclass method to get the right type
     * @see geneauto.models.genericmodel.GAModelElement#getCopy()
     */
    public GADataType getCopy() {
        return (GADataType) super.getCopy();
    }

    /**
     * Recursively goes through the whole Expression tree and converts
     * appropriate parts from SystemModel constructs to CodeModel constructs.
     */
    public void toCodeModel() {
        for (GAModelElement c : getChildren(Expression.class)) {
            ((Expression) c).toCodeModel();
        }
    }

    public GADataType toCodeModelElement() {
        return this.getCopy();
    }

    /**
     * @return the default value of elements of this type
     */
    public Expression getDefaultValue() {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "GADataType", "GEM0012",
                "Call to unimplemented or unsupported method: getDefaultValue() Element: "
                        + getReferenceString(), "");
        return null;
    }
    
    /**
     * this method is called only if the current element is in the CodeModel
     * @return module containing the current element
     */
    public Module getModule() {
        if (module != null) {
            return module;
        } else {
            // Go up the tree and try to get a module.
            if (getParent() instanceof GACodeModelElement) {
                module = ((GACodeModelElement) getParent()).getModule();
                return module;
            } else if (getParent() instanceof GADataType) {
                module = ((GADataType) getParent()).getModule();
                return module;    
            } else {
                // If the parent is not a CodeModelElement this is an error.
                String msg = "Cannot determine containing module. "
                    + "\nCurrent element: " + getReferenceString();
                if (getParent() != null) {
                    msg += "\nParent: " + getParent().getReferenceString();
                }
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "GACodeModelElement.getModule()", "GEM0085", msg, "");
                return null;
            }
        }
    }

    /**
     * Performs type/shape normalisation of the data type.
     * 
     * Full normalisation means that row and column matrixes (vector-matrixes)
     * are converted to vectors and all singleton arrays (vectors, matrixes,
     * arrays) with only one element are converted to scalars.
     * 
     * Partial normalisation only converts singleton arrays.
     * @param full
     *            If true, then row and column matrixes (vector-matrixes) are
     *            converted to vectors.
     * 
     * @return a converted type or null on error.
     * 
     * NOTE: The current element can be partly destroyed! Use getCopy(), if required.
     */
    public GADataType normalize(boolean full) {
        return this;
    }
    
    /**
     * Sub-type comparison.
     * 
     * @param dt data type to compare with
     * @return true if the current data type is a sub-type of the given type
     */
    public abstract boolean isSubType(GADataType dt);
    
    /**
     * 
     * @return new instance of the data type. Implemented for getDataType method to
     * return right data types.
     */
    public GADataType getNewInstance() {
        GACodeModelElement.printUnsupportedMethodError(this, "getNewInstance");
        return null;
    }
    
    /**
     * Checks if the current data type and type given in argument 
     * other are equal i.e.
     * 	- have the same metatype
     *  - have the same metatype parameters
     *  - in case of array types, have the same dimensions
     *  - in case of structure types have the same members
     *  
     *  TODO: to be decided, if this method should override the default 
     *  method "equals". In that case the hashCode method also needs to be 
     *  redefined, because equal objects musth have equal hashCodes. However,
     *  we might not want it (id-s are still different)?
     */
	public boolean equalsTo(GADataType other) {
		// The default implementation uses the mathematical definition
		// of equality based on the isSubType partial ordering of datatypes.
		// Subtypes can implement specific versions of the check for efficiency 
		// reasons or if subtyping is for some reason not defined/applicable
		return (this.isSubType(other) && other.isSubType(this));
		}
	
}