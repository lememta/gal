/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TBoolean.java,v $
 *  @version	$Revision: 1.20 $
 *	@date		$Date: 2012-03-08 10:01:49 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.utils.PrivilegedAccessor;

/**
 * Boolean data type
 */
public class TBoolean extends TPrimitive {

	public TBoolean() {

	}

	public String getTypeName() {
		return "BOOL";
	}

    /**
     * In the input languages on Gene-Auto booleans are actually implemented as
     * integers and also treated as integers.
     * 
     * @return the integer type that corresponds to the boolean type.
     * 
     */
    public TRealInteger toIntegerType() {
        TRealInteger realInt = (TRealInteger) PrivilegedAccessor
        .getObjectInPackage(TRealInteger.class, this);
        realInt.setNBits(8);
        realInt.setSigned(false);
        return realInt;
    }

    @Override
    public Expression getDefaultValue() {
        return (FalseExpression) PrivilegedAccessor
            .getObjectInPackage(FalseExpression.class, this);
    }

	@Override
    public boolean isSubType(GADataType dt) {
		// TODO Check, if this is really the intended semantics. AnTo 8.3.2012
        return (dt instanceof TPrimitive);
    }
    
	@Override
	public boolean equalsTo(GADataType other) {
        return (other instanceof TBoolean);
        }

}