/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TRealInteger.java,v $
 *  @version	$Revision: 1.43 $
 *	@date		$Date: 2012-03-08 10:01:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.tuple.Triple;

/**
 * Integer data type with length defined in "nBits" attribute. The type of
 * integer (signed/unsigned) is defined by "signed" attribute
 * 
 */
public class TRealInteger extends TRealNumeric {

    /**
     * Number of bits. -1 for undefined.
     */
    protected int nBits = -1;
    protected boolean signed;

    /*
     * Note! If any non standard defaults values of attributes are required,
     * then check that they are consistent with the defaults of xml
     * reading/writing, as well as with usage and specification! It is better to
     * set the default values in the field definitions, where possible. Current
     * defaults: sign=false, nbits=0.
     */

    public TRealInteger() {
        setDefaultParameters();
    }

    /**
     * @param nbits 	-- bit lenght of the integer
     * @param sign		-- true = signed, false = unsigned
     */
    public TRealInteger(int nbits, boolean sign) {
        nBits = nbits;
        signed = sign;
    }

    /**
     * Method getNBits.
     * 
     * @return the attribute "nBits".
     */
    public int getNBits() {
        return nBits;
    }

    public void setNBits(int bits) {
        nBits = bits;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    protected String attrToString() {
        String superPart = super.attrToString();
        if (!superPart.isEmpty()) {
            superPart += ", ";
        }        
        return superPart + "signed: " + signed + ", nBits: " + nBits;
    }

    /**
     * Method isSigned.
     * 
     * @return the attribute "signed".
     */
    public boolean isSigned() {
        return signed;
    }

    /**
     * TODO What is this method for?? (AnTo 091109)
     * @return
     */
    public boolean isIntValue() {
        return true;
    }

    public String getTypeName() {
        String type = "";

        if (nBits != 0) {
            type = "INT";
            if (signed == false) {
                type = "U" + type;
            }
            if ((nBits == 8) || (nBits == 16) || (nBits == 32)) {
                return type + nBits;
            }
        }

        // correct type is returned above. If we reach this line, the type is
        // illegal
        EventHandler.handle(EventLevel.ERROR, "TRealInteger", "GED0032",
                "Unsupported integer size \"" + nBits + "\"\n" + "Element: "
                        + getReferenceString());

        return null;
    }

    @Override
    public Expression getDefaultValue() {
        IntegerExpression intExp = (IntegerExpression) PrivilegedAccessor
            .getObjectInPackage(IntegerExpression.class, this);
        intExp.setLitValue("0");
        intExp.setDataType(this);
        return intExp;
    }

    // TODO Consider boolean as a 1-bit integer? Then the method needs to be
	// changed a bit (AnTo 010709)
    @Override
    public boolean isSubType(GADataType dt) {
        if (dt instanceof TBoolean) {
            return false;
        } else if (dt instanceof TRealInteger) {
            if (getNBits() <= ((TRealInteger) dt).getNBits()) {
                return true;
            } else {
                return false;
            }
        } else {
        	// TODO List valid options explicitly (AnTo 010709)
            return true;
        }

    }

    /**
     * parseInteger takes string with numeric value and a class 
     * indicating the package that is basis for constructing data type 
     * as input and returns: integer value parsed from the string a flag isHex, 
     * that is set "true2 when the integer in given string was in hexadecimal 
     * format a GADataType, with shortest bit length accommodating the parsed value
     * The following rules apply to parsing:
     * 		- The returned GADataType is always TRealInteger. The package for 
     * instantiating the TRealInteger is same as that for the packageTempate 
     * argument
     * 		- When parsed string starts with "0x" or "0X" the flag isHex is 
     * set to true and the value is converted to decimal format.
     * 		- When parsed string does not start with "0x" or "0X" the value is 
     * assumed to be in decimal format
     * 		- When the parsed string does not contain legal hexadecimal or 
     * decimal integer, null is returned from the function
     * 
     * @param parsedString
     * @param packageTemplate
     * @return
     */
    public static Triple<Integer, Boolean, TRealInteger> 
	    			parseInteger(String parsedString, Object packageTemplate) {
        try {
            int value;
            boolean isHex = false;
            TRealInteger dt = (TRealInteger) PrivilegedAccessor
                .getObjectInPackage(TRealInteger.class, packageTemplate);
            Triple<Integer, Boolean, TRealInteger> triple;
            if (parsedString.startsWith("0x")||parsedString.startsWith("0X")) {
                parsedString = parsedString.replace("0x", "").replace("0X", "");
                value = Integer.parseInt(parsedString, 16);
                isHex = true;
            } else {
				// Check, if it is an integer.
				/*
				 * AnTo 091109 We don't use Integer.parseInt(..), because this
				 * fails on strings like 1E3, which is actually a legal integer
				 * in Simulink and C.
				 */
            	if (parsedString.contains(".")) {
            		// Check specifically for ".", because the following check 
            		// treats "0.0" and "0" equivalently, which we don't want to do.
            		return null;
            	}
            	Double d1 = new Double(parsedString);
            	Double d2 = new Double(d1.intValue());
            	// Compare between Double objects. This excludes the 
            	// possibility of treating 0 and -0 the same.
            	if (d2.equals(d1)) {
            		value = d1.intValue();
            	} else {
            		return null;
            	}            
            }
            dt = DataTypeUtils.scaleFor(dt, value);
            triple 
                = new Triple<Integer, Boolean, TRealInteger>(value, isHex, dt);
            return triple;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public TRealInteger getDefaultDT() {
        TRealInteger result = (TRealInteger) PrivilegedAccessor
            .getObjectInPackage(TRealInteger.class, this);
        result.setDefaultParameters();
        return result;
    }
    
    public void setDefaultParameters() {
        nBits = 8;
        signed = false;
    }

    /**
     * Checks if the current data type and type given in argument are equal 
     */
	@Override
	public boolean equalsTo(GADataType other) {
		
		if (!equalClass(other)) {
			return false;
		}
		
		if (nBits != ((TRealInteger) other).getNBits()
				|| signed != ((TRealInteger) other).isSigned()) {
			return false;
		}
		
		return true;
	}

}