/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gadatatypes/TCustom.java,v $
 *  @version	$Revision: 1.40 $
 *	@date		$Date: 2012-03-08 10:01:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gadatatypes;

import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.genericmodel.Model;

import java.util.List;

/**
 * Custom (user-defined complex) data type. The data type is defined using the
 * code model language construct CustomType. CustomType enables to define name of
 * the new datatype and datatype contents either if form of literal code or list
 * of structure members.
 * 
 * 
 */
public class TCustom extends TPrimitive {

    /**
     * The struct's name and its internal structure are described in a NameSpace
     * . This is the pointer to the related CodeComponent.
     */
    protected CustomType typeReference;

    public TCustom() {

    }

    public TCustom(CustomType typeReference) {
        this.typeReference = typeReference;
    }

    @Override
    protected boolean isGANative() {
        return false;
    }

    public CustomType getCustomType() {
        return typeReference;
    }

    public void setCustomType(CustomType custType) {
        typeReference = custType;
    }

    protected String attrToString() {
        return super.attrToString() 
        + " typeReference: " 
        + ((typeReference==null)?"null":typeReference.getName());
    }

    /**
     * returns members of the variable
     * 
     * @return structure members if variable is structure
     */
    public List<StructureMember> getMembers() {
        return typeReference.getMembers();
    }
    
    /**
     * Finds structure member with given name
     * 
     * @param name name of the member
     * @return structure member with name = name
     */
    public StructureMember getMemberByName(String name) {
        return typeReference.getMemberByName(name);
    }

    /**
     * update reference to CustomType element
     */
    @Override
    public GADataType toCodeModelElement() {

        TCustom type_cm = (TCustom) this.getCopy();

        if (this.getCustomType() instanceof CustomType_SM) {
            if (((CustomType_SM) this.getCustomType()).getCodeModelElement() != null) {
                type_cm.setCustomType((CustomType) ((CustomType_SM) this
                        .getCustomType()).getCodeModelElement());
            } else {
                //error
            }
        } else {
            //error
        }

        return type_cm;

    }

	@Override
	public boolean isSubType(GADataType dt) {
        if (dt instanceof TCustom) {
            return isSubType((TCustom) dt);
        } else {
            return false;
        }
	}

	public boolean isSubType(TCustom dt) {
		/*
		 * There are currently no sub-types for custom types. To be compliant,
		 * two TCustom data types must refer to the SAME custom type. The
		 * assumption here is that we also demand the same java object in the
		 * model. Even if different object has the same name and same members it
		 * does not count!
		 * 
		 * TODO We can relax this. If there is the same number of elements, the
		 * names match (do not need to be in the same order) and the subtyping
		 * holds between the corresponding elements then it should be also ok.
		 * (AnTo 090702)
		 */	
		return typeReference.equalsTo(dt.getCustomType());
	}

	@Override
	public boolean equalsTo(GADataType other) {

		if (!equalClass(other)) {
			return false;
		}
		
		if (typeReference == null) {
			return false;
		}
		
		return typeReference.equalsTo(((TCustom) other).getCustomType());
	}

	/**
	 * in addition to changing the model, checks that the typeReference 
	 * points to data structure in the same model
	 * 
	 * TODO: (TF:614) do the same thing for other elements that can be 
	 * moved between models and have pointers (TArray, TPointer) 
     * (ToNa 04/08/09)
	 */
	@Override
	public void setModel(Model m) {
		super.setModel(m);
		if (typeReference != null 
					&& typeReference.getModel() != null 
					&& m != typeReference.getModel()) {
			if (typeReference instanceof CustomType_SM) {
				typeReference = (CustomType) ((CustomType_SM) typeReference)
													.getCodeModelElement();
			}
		}
	}

	/**
	 * If typeReference is defined, returns initial value of the 
	 * referenced datatype. null otherwise.
	 */
    @Override
    public Expression getDefaultValue() {
    	// if customtype is given, get default value from there 
        if (getCustomType() != null) {
        	return getCustomType().getInitialValue();
        } else {
        	return null;
        }
    }

	
}