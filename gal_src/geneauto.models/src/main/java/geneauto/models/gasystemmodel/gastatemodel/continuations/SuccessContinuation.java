/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/continuations/SuccessContinuation.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel.continuations;

import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.gastatemodel.Action;
import geneauto.models.gasystemmodel.gastatemodel.Location;
import geneauto.models.gasystemmodel.gastatemodel.SimpleAction;

import java.util.LinkedList;
import java.util.List;

/**
 * Success continuation. This continuation is executed, 
 * when the evaluation proceeds forward along the transitions.
 * 
 */
public abstract class SuccessContinuation extends Continuation {
	
	/**
	 * State that is the source of the chain of transitions.
	 */
	protected Location source;

	/**
	 * List of transition actions (gets modified by checking each transition segment).
	 */
	protected List<SimpleAction> actions = new LinkedList<SimpleAction>();

	/**
	 * This continuation will be carried out, when the evaluation reaches a terminal junction
	 */
	protected TerminalContinuation terminalContinuation = new TerminalContinuation();

	/**
	 * Default constructor
	 */
	public SuccessContinuation() {
		super();		
	}

	public TerminalContinuation getTerminalContinuation() {
		return terminalContinuation;
	}

	public List<SimpleAction> getActions() {
		return actions;
	}
	
	public void addAction(SimpleAction action) {
		actions.add(action);
	}	

	public void setActions(List<SimpleAction> actions) {
		this.actions.clear();
		for (SimpleAction a : actions){
			addAction(a);
		}
	}
	
	public void addActions(List<SimpleAction> actions) {
		for (SimpleAction a : actions) {
			addAction(a);
		}
	}

	public Location getSource() {
		return source;
	}

	public void setSource(Location source) {
		this.source = source;
	}

	public void setTerminalContinuation(TerminalContinuation terminalContinuation) {
		this.terminalContinuation = terminalContinuation;
	}	
	
	/**
	 * Abstract evaluation of transition actions
	 * 
	 * @return the code performing transition actions
	 */
	public List<Statement> evalAbsActions() {
		List<Statement> stmts = new LinkedList<Statement>();		
		if (!getActions().isEmpty()) {
			BlankStatement comnt = new BlankStatement("Perform transition actions");
			stmts.add(comnt);				
			for (Action act : getActions()) {
				stmts.add(act.getOperation().getCopy());
			}							
		}
		return stmts;
	}
	
	/**
	 * Copy fields from the current Continuation to the given Continuation
	 * @param c
	 */
	protected void copyFields(SuccessContinuation c) {
	    c.setSource(getSource());
        for (SimpleAction a : getActions()) {
            c.addAction(a.getCopy());            
        }
	    c.setTerminalContinuation(getTerminalContinuation().getCopy());
	}

    /**
     * @return the copy of the current object
     */
    public abstract SuccessContinuation getCopy();

}
