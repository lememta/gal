/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/OnEventAction.java,v $
 *  @version	$Revision: 1.21 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.map.ConstantMap;

/**
 * This action is only carried out, when the active event matches the condition.
 */
public class OnEventAction extends Action {

	/**
	 * The action is only carried out, when the active event matches the event for
	 * this action.
	 */
	protected Event event;

	/** Default constructor */
	public OnEventAction() {
		super();
	}

	public void setEvent(Event e) {
		this.event = e;
	}

	public Event getEvent() {
		return this.event;
	}

	/**
	 * Resolves by-name references in the current element only. See also:
	 * resolveReferences
	 * 
	 * The method checks, whether the Event object pointed to in the event field
	 * is contained in a model. If not, then it is assumed to be a dummy Event
	 * and we have to find the proper Event (having the same name as the dummy
	 * Event) by going up in the model's hierarchy. The event field is assumed
	 * not to be empty.
	 * 
	 * @param ConstantMap
	 *            <String, GAModelElement> nameMap
	 */
	 @Override
	 public GAModelElement resolveCurrentElement(
		 final ConstantMap<String, GAModelElement> nameMap) {
		 
		 // Check, if the event has been resolved already to a proper event.
		 if (event.getModel() != null) {
			 // The object itself is still the same. Hence, return null.
			 return null;
		 }
		 
		GAModelElement el = nameMap.get(event.getName());
		if (el != null) {
			if (el instanceof Event) {
				setEvent((Event) el); // Replace the event
			} else {
				EventHandler.handle(EventLevel.ERROR,
						getClass().getSimpleName() + ".resolveReferencesInCurrent", "",
						"Expecting Event: " + el.getReferenceString() + "\n"
								+ "Current element: " + getReferenceString());
			}
		} else {
			if (nameMap.containsKey(event.getName())) {
				EventHandler.handle(EventLevel.ERROR,
						getClass().getSimpleName() + ".resolveReferencesInCurrent", 
							"", 
							"Multiple definitions for event " + event.getName() 
							+ ". Can not resolve reference by name.\n"
								+ "Current element: " + getReferenceString());
			} else {
				EventHandler.handle(EventLevel.ERROR,
						getClass().getSimpleName() + ".resolveReferencesInCurrent", 
							"", "Event "
							+ event.getName() + " undefined.\n"
							+ "Current element: " + getReferenceString());
			}			
		}
		// The object itself is still the same. Hence, return null.
		return null;
	}
	 
}