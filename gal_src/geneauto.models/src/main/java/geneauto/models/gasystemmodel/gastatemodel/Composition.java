/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/Composition.java,v $
 *  @version	$Revision: 1.34 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.common.Node;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Abstract base class for common properties of state and flow graph element
 * compositions.
 */
public abstract class Composition extends GASystemModelElement {

	protected List<Box> boxes = new ArrayList<Box>();

	// Constructor
	public Composition(){
		super();
	}

	// Constructor for convenience
	public Composition(GAModelElement parent){
		this();
		setParent(parent);
	}

	/*
	 * Continuations have local scope in system model
	 * 
	 * (non-Javadoc)
	 * @see geneauto.models.genericmodel.GAModelElement#isReferable()
	 */
	public boolean isReferable() {
		return false;
	}
	
	/**
	 * Abstract evaluation of a composition entry.
	 * Opens the locations in the composition.
	 * 
	 * @return the for entering a composition
	 */
	public abstract List<Statement> evalAbsEntry(EvaluationContext ctxt);
	
	/**
	 * Abstract evaluation of executing a composition that is already open.
	 * 
	 * @return the code executing an open composition
	 */
	public abstract List<Statement> evalAbsDuring(EvaluationContext ctxt);
	
	/**
	 * Abstract evaluation of exiting a composition.
	 * Closes the locations in the composition.
	 * 
	 * @return the for closing a composition
	 */
	public abstract List<Statement> evalAbsExit(EvaluationContext ctxt);
	
	/**
	 * @return true for compositions that don't contain any substates
	 */
	public abstract boolean isStateless();

	/**
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addFunction instead
	 */
	public List<Box> getBoxes() {
		return boxes;
	}

	/**
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addState instead
	 */
	@SuppressWarnings("unchecked")
	public List<State> getStates() {
		return Collections.EMPTY_LIST;
	}	

	public void addJunction(Junction j){
		// Should be implemented only when the composition type has junctions		
		EventHandler.handle(EventLevel.CRITICAL_ERROR, "Composition",
				"GE9998",
				"Call to unsupported method: addJunction() Element: "
						+ getReferenceString(), "");		
	}
	
	/**
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addJunction instead
	 */
	@SuppressWarnings("unchecked")
	public List<Junction> getJunctions() {
		return Collections.EMPTY_LIST;
	}	
	
	public abstract List<Transition> getDefaultTransitions();
	
	public void addBox(Box b){
		b.setParent(this);
		boxes.add(b);
	}

	/**
	 * The parent Location of a Composition is either a State or ChartRoot.
	 * 
	 * @return The parent Location 
	 */
	public Location getParentLocation() {		
		GAModelElement elt = getParent();
		if (elt instanceof State) {
			return (State) elt;
			}
		else if (elt instanceof ChartBlock) {
			return ((ChartBlock) elt).getRootLocation();
		}
		else {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					"Composition.getParentLocation", 
					"", 
					"Unexpected parent. Expecting either a State or ChartBlock.\nThis: " 
						+ getReferenceString()
						+ "\nParent: "
						+ elt.getReferenceString(), 
					"");				
			return null;
		}
	}
	
    /**
     * Converts the current Composition to a FlowGraphComposition, if possible.
     * Note! All child objects of the current object are moved to the newly created 
     * object. 
     */
    public FlowGraphComposition toFlowGraphComposition() {
        if (isStateless()) {
           FlowGraphComposition fg = new FlowGraphComposition();
           // Copy general attributes
           copyCommonAttributes(fg);
           // Move attributes specific to this class
           fg.setDefaultTransitions(getDefaultTransitions());
           fg.setJunctions(getJunctions());
           return fg;
        }
        else {
            EventHandler
                    .handle(
                            EventLevel.CRITICAL_ERROR,
                            "",
                            "",
                            "Cannot convert a composition with substates into a FlowGraphComposition. Element: "
                                    + getReferenceString(), "");
            return null;
        }
    }
    
    /**
     * Converts the current Composition to an AndComposition, if possible.
     * Note! All child objects of the current object are moved to the newly created 
     * object. 
     */
    public AndComposition toAndComposition() {
        if ((getDefaultTransitions() == null || getDefaultTransitions().isEmpty())
                && (getJunctions() == null || getJunctions().isEmpty())) {
            // Check, that there are not outer Transitions originating from
            // States belonging to the Composition
           for (State s : getStates()) {
               if (!s.getOuterTransitions().isEmpty()) {
                   Transition t = s.getOuterTransitions().get(0);
                   EventHandler
                   .handle(
                           EventLevel.ERROR,
                           "toAndComposition",
                           "",
                           "Outer transitions are not allowed in states belonging to a parallel (AND) composition. "
                               + "\nState: " + s.getReferenceString() 
                               + "\nTransition: " + t.getReferenceString());
               }
           }
           AndComposition ac = new AndComposition();
           // Copy general attributes
           copyCommonAttributes(ac);
           // Move attributes specific to this class
           ac.setStates(getStates());
           return ac;
        }
        else {
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "toAndComposition",
                            "",
                            "Cannot convert a composition with default transitions "
                                + "or junctions into a parallel (AND) composition. Element: "
                                + getReferenceString());
            return null;
        }
    }
    
    /**
     * A parent Node of a Composition is either a State or the Chart
     * 
     * @return The parent Node
     */
    public Node getParentNode() {
        if (getParent() instanceof Node) {
            return (Node) getParent();
        } else {
            String msg = "Unexpected type of parent." 
                + "\nElement: " + getReferenceString();
            if (getParent() != null) {
                msg += "\nParent's type: " + getParent().getClass().getName();
            }
            return null;
        }
    }

    /**
     * Sorts the States and transitions according to their executionOrder
     */
    public void sort() {
        Collections.sort(getDefaultTransitions());
        for (Junction j : getJunctions()) {
            j.sort();
        }        
        for (State s : getStates()) {
            s.sort();
        }        
    }

}