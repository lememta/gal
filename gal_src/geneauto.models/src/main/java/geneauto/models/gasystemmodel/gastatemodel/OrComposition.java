/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/OrComposition.java,v $
 *  @version	$Revision: 1.33 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.statemodel.IsOpenExpression;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.gastatemodel.continuations.DefaultTransitionContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FlowGraphContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.TerminalContinuation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * A collection of states, junctions and default transitions. Only one state of
 * an OR composition can be active at a time.
 */
public class OrComposition extends Composition {

    /**
     * Default transitions of the OR composition
     */
    protected TransitionList defaultTransitionList = new TransitionList();

    /*
     * States contained in this composition
     */
    protected List<State> states = new ArrayList<State>();

    /*
     * Junctions contained in this composition
     */
    protected List<Junction> junctions = new ArrayList<Junction>();

    // Constructor
    public OrComposition() {
        super();
        defaultTransitionList.setParent(this);
    }

    // Constructor for convenience
    public OrComposition(GAModelElement parent) {
        this();
        setParent(parent);
    }

    public List<Transition> getDefaultTransitions() {
        return defaultTransitionList.getTransitions();
    }

    public void setDefaultTransitions(List<Transition> transitions) {
        defaultTransitionList.setTransitions(transitions);
    }

    public void addDefaultTransition(Transition transition) {
        defaultTransitionList.addTransition(transition);
    }

    public void setJunctions(List<Junction> junctions) {
        this.junctions.clear();
        for (Junction j : junctions) {
            addJunction(j);
        }
    }

    /**
     * Abstract evaluation of a composition entry. Opens the locations in the
     * composition.
     * 
     * @return the code for entering a composition
     */
    public List<Statement> evalAbsEntry(EvaluationContext in_ctxt) {
        List<Statement>   stmts = new LinkedList<Statement>();      
        EvaluationContext ctxt = new EvaluationContext(in_ctxt);
        
        if (getDefaultTransitions() == null
                || getDefaultTransitions().isEmpty()) {
            // No default Transitions
            if (getStates() == null || getStates().isEmpty()) {
                // No States either
                // Nothing to do
                return stmts;
            } else if (getStates().size() == 1) {
                // Single State
                State s = getStates().get(0);
                stmts.addAll(s.evalAbsEntry(ctxt, null));
            } else {
                // Multiple SubStates, but no default transition. Not allowed, because it is unclear, which substate to open.
            	String refStr = getReferenceString();            	
            	if (getParent() != null) {
            		getParent().getReferenceString(); // Reference to the parenting state is more meaningful to the user.
            	} 
            	EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName() + ".evalAbsEntry()", "", 
            			"Violation of Gene-Auto Stateflow modelling rule 5.2.12 - An OR decomposition must always have an unguarded default transition."
            			+ "\n Element: " + refStr);
            	return null;
           }
        } else {
            // Default Transitions exist
            if (getStates() == null || getStates().isEmpty()) {
                // No States
                // This should actually be a FlowGraphComposition?
                SuccessContinuation success = new FlowGraphContinuation();
                FailureContinuation fail = new FailureContinuation();
                stmts.addAll(getDefaultTransitionList().evalAbs(ctxt, success, fail));
            } else {
                // State(s) exist in the OrComposition
            	
            	// Check, if there exists an unguarded path to a state
				TransitionList tl = getDefaultTransitionList();
				
				if (tl == null || (tl.hasUnguardedPathToState() == false)) {
	            	String refStr = getReferenceString();            	
	            	if (getParent() != null) {
	            		getParent().getReferenceString(); // Reference to the parenting state is more meaningful to the user.
	            	} 
	            	EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName() + ".evalAbsEntry()", "", 
	            			"Violation of Gene-Auto Stateflow modelling rule 5.2.12	- An OR decomposition must always have an unguarded default transition."
	            			+ "\n Element: " + refStr);
	            	return null;					
				}            	

              // Continuation, when default transitions lead to a terminal
              // junction.
              TerminalContinuation terminal = new TerminalContinuation();
              
              // Continuation, when default transitions fail.
              FailureContinuation fail = new FailureContinuation();
				
                // Continuation, when default transitions succeed.
                SuccessContinuation success = new DefaultTransitionContinuation();
                success.setSource(getParentLocation());
                success.setTerminalContinuation(terminal);

                // Code, for checking default transitions.
                // NOTE: This cannot be omitted, when the transition list is
                // empty,
                // as there is remaining work in the failure continuation.
                stmts.addAll(getDefaultTransitionList().evalAbs(ctxt, success, fail));
            }
        }
        
        return stmts;
    }

    /**
     * Abstract evaluation of executing a composition that is already open.
     * 
     * @return the code executing an open composition
     */
    public List<Statement> evalAbsDuring(EvaluationContext in_ctxt) {
        List<Statement>   stmts = new LinkedList<Statement>();      
        EvaluationContext ctxt = new EvaluationContext(in_ctxt);

        if (states == null || states.isEmpty()) {
            // No States
            // Nothing to do
        } else {
            // State(s) exist in the OrComposition
            // Return code that check's one-by-one all States for being open
            // and executes when they are.

            // Create an iterator starting from the end of the list
            ListIterator<State> iter = states.listIterator(states.size() - 1);
            State s = states.get(states.size() - 1);
            Statement thenStmt, elseStmt = null, checkStmt;
            
            // Scan the list of States backwards
            while (true) {

            	List<Statement> thenLst = new LinkedList<Statement>();
                thenLst.addAll(s.evalAbsDuring(ctxt));
                if (thenLst != null && !thenLst.isEmpty()) {
                    thenStmt = new CompoundStatement(null, thenLst);
                } else {
                	thenStmt = null;
                }

                Expression condExp = new IsOpenExpression(s, getModel().getCodeModel());
                if (thenStmt != null || elseStmt != null) {
                	checkStmt = new IfStatement(condExp, thenStmt, elseStmt);
                } else {
                	checkStmt = null;
                }
                
				// Update pointer for the next round or quit the loop that scans
				// States, if reached the beginning
                if (iter.hasPrevious()) {
                    s = (State) iter.previous();
                    elseStmt = checkStmt; 
                } else {
                    break; // Quit loop 
                }
            }
            
            // If a statement was created, then add it to the list
            if (checkStmt != null) {
            	stmts.add(checkStmt);
            }            
        }        
        return stmts;
    }

    /**
     * Abstract evaluation of exiting a composition. Closes the locations in the
     * composition.
     * 
     * @return the for closing a composition
     */
    public List<Statement> evalAbsExit(EvaluationContext in_ctxt) {
        List<Statement>   stmts = new LinkedList<Statement>();      
        EvaluationContext ctxt = new EvaluationContext(in_ctxt);

        if (states == null || states.isEmpty()) {
            // No States
            // Nothing to do
        } else {
            // State(s) exist in the OrComposition
            // Return code that check's one-by-one all States for being open
            // and closes when they are.

            // Create an iterator starting from the end of the list
            ListIterator<State> iter = states.listIterator(states.size() - 1);
            State s = states.get(states.size() - 1);
            Statement thenStmt, elseStmt = null, checkStmt;
            // Scan the list of States backwards
            while (true) {
                List<Statement> thenLst = new LinkedList<Statement>();
                thenLst.addAll(s.evalAbsExit(ctxt, null, false));
                thenStmt = new CompoundStatement(null, thenLst);
                Expression condExp = new IsOpenExpression(s, getModel().getCodeModel());
                checkStmt = new IfStatement(condExp, thenStmt, elseStmt);
                if (iter.hasPrevious()) {
                    s = (State) iter.previous();
                    elseStmt = checkStmt;
                } else {
                    break;
                }
            }
            stmts.add(checkStmt);
        }
        return stmts;
    }

    /**
     * @return true for compositions that don't contain any substates
     */
    public boolean isStateless() {
        return states == null || states.size() == 0;
    }

    public void addState(State s) {
        s.setParent(this);
        states.add(s);
    }

    public void setStates(List<State> ss) {
    	states.clear();
        if (ss != null) {
            for (State s : ss) {
                addState(s);
            }
        }
    }
    public void addJunction(Junction j) {
    	
        j.setParent(this);
        junctions.add(j);
    }

    /*
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use setDefaultTransitions or addDefaultTransition instead
     */
    public TransitionList getDefaultTransitionList() {
        return defaultTransitionList;
    }

    /*
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use addState instead
     */
    public List<State> getStates() {
        return states;
    }

    /*
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use addJunction instead
     */
    public List<Junction> getJunctions() {
        return junctions;
    }    

}