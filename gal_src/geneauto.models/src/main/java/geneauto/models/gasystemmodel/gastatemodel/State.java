/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/State.java,v $
 *  @version	$Revision: 1.46 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.statemodel.IsEventExpression;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gacodemodel.statement.statemodel.CloseStatement;
import geneauto.models.gacodemodel.statement.statemodel.OpenStatement;
import geneauto.models.gasystemmodel.common.ContainerNode;
import geneauto.models.gasystemmodel.common.Function_SM;
import geneauto.models.gasystemmodel.common.Node;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gastatemodel.continuations.DefaultTransitionContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.InnerTransitionContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.OuterTransitionContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.TerminalContinuation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * A location in the chart that can stay persistently active during subsequent
 * activations of the chart. A state can have further internal structure,
 * contain variables, etc.
 */
public class State extends Location implements ContainerNode, Comparable<State> {

    /** Execution order - Only matters for states in an And composition. */
    protected int executionOrder;
    protected List<SimpleAction> entryActions = new ArrayList<SimpleAction>();
    protected List<Action> duringActions = new ArrayList<Action>();
    protected List<SimpleAction> exitActions = new ArrayList<SimpleAction>();
    protected TransitionList outerTransitionList = new TransitionList();
    protected TransitionList innerTransitionList = new TransitionList();
    protected List<Variable_SM> variables = new ArrayList<Variable_SM>();
    protected List<Event> events = new ArrayList<Event>();
    protected Composition composition;
    protected StateType type;
    
    /** 2006b extension - state machine type : Classic, Mealey or Moore */
    protected StateMachineType machineType;

    /**
     * All functions of the block (e.g.graphical functions, truth-tables, EML
     * etc.).
     */
    protected List<Function_SM> functions = new ArrayList<Function_SM>();

    /** Constructor */
    public State() {
        super();
        innerTransitionList.setParent(this);
        outerTransitionList.setParent(this);
    }

    /** Constructor for convenience */
    public State(GAModelElement parent) {
        this();
        setParent(parent);
    }

    public void setExecutionOrder(int executionOrder) {
        this.executionOrder = executionOrder;
    }

    public int getExecutionOrder() {
        return executionOrder;
    }

    public List<SimpleAction> getEntryActions() {
        return entryActions;
    }

    public void addEntryAction(SimpleAction a) {
        a.setParent(this);
        entryActions.add(a);
    }

    public List<Action> getDuringActions() {
        return duringActions;
    }

    public void addDuringAction(Action a) {
        a.setParent(this);
        duringActions.add(a);
    }

    public List<SimpleAction> getExitActions() {
        return exitActions;
    }

    public void addExitAction(SimpleAction a) {
        a.setParent(this);
        exitActions.add(a);
    }

    public void addUnLabelledAction(SimpleAction a) {
        a.setParent(this);
        if (StateMachineType.CLASSIC.equals(this.getMachineType())) {
        	entryActions.add(a);
        } else if (StateMachineType.MOORE.equals(this.getMachineType())) {
//        	duringActions.add(a);
//        	exitActions.add(a.getCopy());
            EventHandler.handle(EventLevel.ERROR,
                    "State.addUnLabelledAction", "",
                    "Moore state machine type detected - " +
                    "Only classical state models are supported "
                            + this.getReferenceString(), "");
        } else if (StateMachineType.MEALEY.equals(this.getMachineType())) {
            EventHandler.handle(EventLevel.ERROR,
                    "State.addUnLabelledAction", "",
                    "Mealey state machine type detected - " +
                    "Only classical state models are supported "
                            + this.getReferenceString(), "");
        } else {
            EventHandler.handle(EventLevel.ERROR,
                    "State.addUnLabelledAction", "",
                    "Problem adding unlabelled state actions "
                            + this.getReferenceString(), "");
        }
    }

    public void setInnerTransitions(List<Transition> transitions) {
        innerTransitionList.setTransitions(transitions);
    }

    public void addInnerTransition(Transition transition) {
        innerTransitionList.addTransition(transition);
    }

    public List<Transition> getOuterTransitions() {
        return outerTransitionList.getTransitions();
    }

    public List<Transition> getInnerTransitions() {
        return innerTransitionList.getTransitions();
    }

    public void setOuterTransitions(List<Transition> transitions) {
        outerTransitionList.setTransitions(transitions);
    }

    public void addOuterTransition(Transition transition) {
        outerTransitionList.addTransition(transition);
    }

    public void setComposition(Composition composition) {
        composition.setParent(this);
        this.composition = composition;
    }

    public Composition getComposition() {
        return composition;
    }

    public void setType(StateType type) {
        this.type = type;
    }

    public StateType getType() {
        return type;
    }

	/**
     * Abstract evaluation of closing a state and opening another
     * 
     * @param sourcePath
     *            Path of States starting from the lowest common ancestor of source and destination and ending with the source State
     * @param destPath
     *            Path of States starting from the lowest common ancestor of source and destination and ending with the destination State
     * @param ctxt
     *            EvaluationContext
     * @param success
     *            Success continuation (transition actions to be carried out)
     * @return the code closing a state and opening another
     */
    private List<Statement> evalChangeState(
            LinkedList<Location> sourcePath,
            LinkedList<Location> destPath, 
            EvaluationContext ctxt,
            SuccessContinuation success) {
        List<Statement> stmts = new LinkedList<Statement>();

        // Remove the common ancestor from the head of the source path
        // This will normally not be closed, except for outer loops
        Location ancestor = sourcePath.pop();
        // Pop the common ancestor from the destination path
        destPath.pop();
        
        State source;
        if (sourcePath.isEmpty()) {
            // Outer loop
            source = (State) ancestor;
        } else {
            source = (State) sourcePath.removeLast();
        }
        
        // Close source State
        stmts.addAll(source.evalAbsExit(ctxt, sourcePath, false));        

        // Perform transition actions
        stmts.addAll(success.evalAbsActions());

        // Enter destination State
        State dest;
        if (destPath == null || destPath.isEmpty()) {
            // Open the current State
            dest = this;
        } else {
            // Open the topmost State in the path and pop the path.
            dest = (State) destPath.pop();
        }
        stmts.addAll(dest.evalAbsEntry(ctxt, destPath));

        return stmts;
    }

    /**
     * Abstract evaluation of a transition destination. Looks at the current
     * location and decides, what to do next
     * 
     * @return the code evaluating the transition destination
     */
    public List<Statement> evalAbsDestination(
            EvaluationContext ctxt,
            SuccessContinuation success,
            FailureContinuation fail) {
        List<Statement> stmts = new LinkedList<Statement>();
        List<Statement> tmpStmts = new LinkedList<Statement>();

        if (success instanceof DefaultTransitionContinuation) {
            // Perform pending transition action(s)
            stmts.addAll(success.evalAbsActions());
            // Open neccessary states
            BlankStatement comnt = new BlankStatement("Enter state "
                    + getQualifiedName());
            stmts.add(comnt);
            LinkedList<Location> path = getSubPath(success.getSource(), this);
            // Remove the source of the transition from the path of States
            // to be opened, as it is open already.
            path.pop();
            // Open the next State in the path and pass the rest as arguments
            State s1 = (State) path.pop();
            stmts.addAll(s1.evalAbsEntry(ctxt, path));
            
        } else if (success instanceof OuterTransitionContinuation) {
            // This is a transition from an open state to a new state
            // Calculate the superstate to be closed, based on the starting
            // state and the current state (destination).
            // Close neccessary states
            // Perform pending transition action(s)
            // Open neccessary states
            State source = (State) success.getSource();
            Location commonLoc = getLowestCommonLocation(source);
            LinkedList<Location> sourcePath = source.getSubPath(commonLoc,
                    source);
            LinkedList<Location> destPath = this.getSubPath(commonLoc, this);
            if (sourcePath.size() == 1) {
                if (destPath.size() == 1) {
                    // An outer loop
                    stmts.addAll(evalChangeState(sourcePath, destPath, ctxt, success));
                } else {
                    // Source state is an ancestor of the destination state
                    // This is not a valid outer transition, it should be an
                    // inner transition
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            "State.evalAbsDestination", "",
                            "Invalid outer transition.\nSource: "
                                    + source.getReferenceString()
                                    + "\nDestination: "
                                    + this.getReferenceString(), "");
                }
            }
            // sourcePath contains more than the current state 
            else if (destPath.size() == 1) {
                // Destination State is an ancestor of the source State
                // That State will not be closed and reentered.
                // Instead, we reenter the Composition in the destination State.

                // Close the Composition of the destination (=current State).
                // (We know that the destination/current State has a non-empty Composition.)
                tmpStmts = getComposition().evalAbsExit(ctxt);
                if (!tmpStmts.isEmpty()) {
                    BlankStatement comnt = new BlankStatement(
                            "Close the inner composition of state "
                                    + getQualifiedName());
                    stmts.add(comnt);
                    stmts.addAll(tmpStmts);
                }
                
                // Perform transition actions
                stmts.addAll(success.evalAbsActions());

                // Reopen the Composition of the destination (=current State)
                // (We know that the destination/current State has a non-empty Composition.)
                tmpStmts = getComposition().evalAbsEntry(ctxt);
                if (!tmpStmts.isEmpty()) {
                    BlankStatement comnt = new BlankStatement(
                            "Enter the inner composition of state "
                                    + getQualifiedName());
                    stmts.add(comnt);
                    stmts.addAll(tmpStmts);
                }
            } else {
                stmts.addAll(evalChangeState(sourcePath, destPath, ctxt, success));
            }
            
        } else if (success instanceof InnerTransitionContinuation) {
            State source = (State) success.getSource();
            
            // Close the Composition of the parent State
            // Perform pending transition action(s)
            // Open the destination State and any intermediate States

            // Close the Composition of the destination (current State)
            // (We know that the current State has a non-empty Composition.)
            tmpStmts = source.getComposition().evalAbsExit(ctxt);
            if (!tmpStmts.isEmpty()) {
                BlankStatement comnt = new BlankStatement(
                        "Close the inner composition of state "
                                + source.getQualifiedName());
                stmts.add(comnt);
                stmts.addAll(tmpStmts);
            }

            // Perform transition actions
            stmts.addAll(success.evalAbsActions());

            LinkedList<Location> sourcePath = getSubPath(source, this);
            sourcePath.pop(); // Remove the source location from the head of the
            // list

            if (sourcePath.isEmpty()) {
                // An inner loop
                // (Re)open the Composition of the destination (current State)
                // (We know that the current State has a non-empty Composition.)
                tmpStmts = source.getComposition().evalAbsEntry(ctxt);
                if (!tmpStmts.isEmpty()) {
                    BlankStatement comnt = new BlankStatement(
                            "Reenter the composition of state "
                                    + source.getQualifiedName());
                    stmts.add(comnt);
                    stmts.addAll(tmpStmts);
                }
            } else {
                // sourcePath is not empty
                // Retrieve the first (Sub-)State from the path
                State nextState = (State) sourcePath.poll();
                // Open that State and give the remaining path of States that
                // must be opened as parameter.
                tmpStmts = nextState.evalAbsEntry(ctxt, sourcePath);
                if (!tmpStmts.isEmpty()) {
                    BlankStatement comnt = new BlankStatement(
                            "Reenter the inner composition of state "
                                    + source.getQualifiedName());
                    stmts.add(comnt);
                    stmts.addAll(tmpStmts);
                }
            }
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "State.evalAbsDestination", "",
                    "Unexpected success continuation type: "
                            + success.getClass().getName() + "\nElement: "
                            + getReferenceString(), "");
        }
        return stmts;
    }

    /**
     * Abstract evaluation of a State entry. Performs entry actions and proceeds
     * with entering the inner Composition.
     * 
     * @param The
     *            list of locations to be entered, before entering the current
     *            location.
     * 
     * @return the code for entering a State
     */
    public List<Statement> evalAbsEntry(EvaluationContext ctxt, 
            LinkedList<Location> path) {
        List<Statement> stmts = new LinkedList<Statement>();
        List<Statement> tmpStmts = new LinkedList<Statement>();

        // Mark State as open
        Statement openingStmt = new OpenStatement(this);
        // openingStmt.addAnnotation("Open state " + getQualifiedName());
        stmts.add(openingStmt);

        // Initialise local variables
        stmts.addAll(absInitLocalVars());
        
        // Perform entry actions
        if (!getEntryActions().isEmpty()) {
            BlankStatement comnt = new BlankStatement(
                    "Perform entry actions of state " 
                    + getQualifiedName());
            stmts.add(comnt);
            for (Action act : getEntryActions()) {
                stmts.add(act.getOperation().getCopy());
            }
        }

        // Open SubStates, if they exist
        if (path == null || path.isEmpty()) {
            // If an inner Composition is present, then open it along the
            // default path
            if (getComposition() != null) {
                
                tmpStmts = getComposition().evalAbsEntry(ctxt);
                if (!tmpStmts.isEmpty()) {
                    BlankStatement comnt = new BlankStatement(
                            "Open the inner composition of state " + getQualifiedName());
                    stmts.add(comnt);
                    stmts.addAll(tmpStmts);
                }                
            }
        } else {
            // Open the inner Composition along a specified path
            State s1 = (State) path.pop();
            stmts.addAll(s1.evalAbsEntry(ctxt, path));
        }
        return stmts;
    }

    /**
     * Abstract evaluation of executing a State that is already open.
     * 
     * 1) Checks all transitions that leave the state, if the State can be left,
     * it is left and we are done. 2) Execute the during actions. 3) Check the
     * internal transitions. 4) Continue by executing the decomposition of the
     * state.
     * 
     * @return the code executing an open State
     */
    public List<Statement> evalAbsDuring(EvaluationContext ctxt) {
        LinkedList<Statement> stmts = new LinkedList<Statement>();

        // Code, when inner transitions fail.
        List<Statement> innerFailStmts = new LinkedList<Statement>();
        if (getComposition() != null) {
            innerFailStmts = getComposition().evalAbsDuring(ctxt);
        }

        // Code, when inner transitions lead to a terminal junction.
        List<Statement> innerTermStmts = new LinkedList<Statement>();
        for (Statement s : innerFailStmts) {
            innerTermStmts.add(s.getCopy()); // Same behaviour, but different
                                             // Statement instances
        }
        
        // Continuation, when inner transitions lead to a terminal junction.
        TerminalContinuation innerTerminal = new TerminalContinuation(
                innerTermStmts);
        
        // Continuation, when inner transitions fail.
        FailureContinuation innerFail = new FailureContinuation(innerFailStmts);

        // Continuation, when inner transitions succeed.
        SuccessContinuation innerSuccess = new InnerTransitionContinuation();
        innerSuccess.setSource(this);
        innerSuccess.setTerminalContinuation(innerTerminal);

        // Code, when outer transitions fail.
        List<Statement> outerFailStmts = new LinkedList<Statement>();
                
        // Evaluate during and on-event actions
        outerFailStmts.addAll(evalAbsDuringAndOnEventActions());
        // Evaluate inner transitions
        // NOTE: This cannot be omitted, when the transition list is empty,
        // as there is remaining work in the failure continuation.
        outerFailStmts.addAll(getInnerTransitionList().evalAbs(ctxt, 
                innerSuccess, innerFail));
        
        // Code, when outer transitions lead to a terminal junction.
        List<Statement> outerTermStmts = new LinkedList<Statement>();
        for (Statement s : outerFailStmts) {
            outerTermStmts.add(s.getCopy()); // Same behaviour, but different
                                             // Statement instances
        }        

        // Continuation, when outer transitions lead to a terminal junction.
        TerminalContinuation outerTerminal = new TerminalContinuation(
                outerTermStmts);        
        
        // Continuation, when outer transitions fail.
        FailureContinuation outerFail = new FailureContinuation(outerFailStmts);

        // Continuation, when outer transitions succeed.
        SuccessContinuation outerSuccess = new OuterTransitionContinuation();
        outerSuccess.setSource(this);
        outerSuccess.setTerminalContinuation(outerTerminal);

        // Code, for checking outer transitions.
        // NOTE: This cannot be omitted, when the transition list is empty,
        // as there is remaining work in the failure continuation.
        stmts.addAll(getOuterTransitionList().evalAbs(ctxt, outerSuccess, outerFail));
        
        if (stmts != null && !stmts.isEmpty()) {
            BlankStatement comnt = new BlankStatement(
                    "Execute state " + getQualifiedName());
            stmts.addFirst(comnt);            
        }

        return stmts;
    }

    /**
     * Abstract evaluation of closing and leaving a State.
     * 
     * 1) Close Composition (SubStates) 2) Perform exit actions 3) Mark State as
     * closing
     * 
     * @param ctxt
     *            EvaluationContext
     * @param sourcePath
     *            List of parent States that will have to be closed after
     *            closing the current State
     * @param upwardOnly
     *            When true, code for closing the inner composition is not generated. 
     *            It assumed to be closed already.  
     * @return the code exiting a State
     */
    public List<Statement> evalAbsExit(
            EvaluationContext ctxt,
            LinkedList<Location> sourcePath,
            boolean upwardOnly) {
        List<Statement> stmts = new LinkedList<Statement>();

        if (!upwardOnly) {
            // If an inner Composition is present, then close it
            if (getComposition() != null && !getComposition().isStateless()) {
                BlankStatement comnt = new BlankStatement(
                        "Close the inner composition of state " + getQualifiedName());
                stmts.add(comnt);
                stmts.addAll(getComposition().evalAbsExit(ctxt));
            }
        }

        // Perform exit actions
        if (!getExitActions().isEmpty()) {
            BlankStatement comnt = new BlankStatement(
                "Perform exit actions of state " 
                + getQualifiedName());
            stmts.add(comnt);
            for (Action act : getExitActions()) {
                stmts.add(act.getOperation().getCopy());
            }
        }

        // Mark State as closed
        Statement closingStmt = new CloseStatement(this);
        // closingStmt.addAnnotation("Close state " + getQualifiedName());
        stmts.add(closingStmt);
        
        // Close any required parent States
        /* Note: we would achieve the same effect by calling 
         * evalAbsExit directly on the highest level State that
         * has to be closed, but it is somewhat more efficient
         * this way - we know the states that have to be closed. 
         */
        if (sourcePath != null && !sourcePath.isEmpty()) {
            State parent = (State) sourcePath.removeLast();
            stmts.addAll(parent.evalAbsExit(ctxt, sourcePath, true));
        }

        return stmts;
    }

    public void setEvents(List<Event> events) {
        this.events.clear();
        for (Event e : events) {
            addEvent(e);
        }
    }

    public void addEvent(Event e) {
        // e.setParent(this);
        events.add(e);
    }

    public void setFunctions(List<Function_SM> functions) {
        this.functions.clear();
        for (Function_SM f : functions) {
            addFunction(f);
        }
    }

    public void addFunction(Function_SM f) {
        f.setParent(this);
        functions.add(f);
    }

    public void setVariables(List<Variable_SM> variables) {
        this.variables.clear();
        for (Variable_SM v : variables) {
            addVariable(v);
        }
    }

    public void addVariable(Variable_SM v) {
        v.setParent(this);
        variables.add(v);
    }

    public TransitionList getOuterTransitionList() {
        return outerTransitionList;
    }

    public TransitionList getInnerTransitionList() {
        return innerTransitionList;
    }

    /*
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use addVariable instead
     */
    public List<Variable_SM> getVariables() {
        return variables;
    }

    /*
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use addEvent instead
     */

    public List<Event> getEvents() {
        return events;
    }

    /*
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use addFunction instead
     */
    public List<Function_SM> getFunctions() {
        return functions;
    }

    /**
     * @return Full name of the State containing names of parent States
     *         separated with ".". The path part does not contain the
     *         Chart's name.
     */
    public String getQualifiedName() {
        return getNameWithPath(".");
    }

    /**
     * @param separ separator
     * @return Full name of the State containing names of parent States
     *         separated with given separator. The path part does not contain the
     *         Chart's name.
     */
    public String getNameWithPath(String separator) {
        Location parentLoc = getParentLocation();
        if (parentLoc instanceof State) {
            State s = (State) parentLoc;
            return s.getNameWithPath(separator) + separator + getName();
        } else if (parentLoc instanceof ChartRoot) {
            return getName();
        } else {
            String refStr = null;
            if (parentLoc != null) {
                refStr = parentLoc.getReferenceString();
            }
            EventHandler.handle(EventLevel.ERROR, "State.getNameWithPath",
                    "", "Unexpected logical parent.\nThis: "
                            + getReferenceString() + "\nLogical parent: "
                            + refStr, "");
            return "";
        }
    }

    /**
     * Abstract evaluation of during and on-event actions
     * 
     * @return the code performing during and on-event actions
     */
    private List<Statement> evalAbsDuringAndOnEventActions() {
        List<Statement> stmts = new LinkedList<Statement>();
        if (!getDuringActions().isEmpty()) {
            BlankStatement comnt = new BlankStatement(
                    "Perform during and on-event actions of state "
                    + getQualifiedName());
            stmts.add(comnt);
            for (Action act : getDuringActions()) {
                if (act instanceof OnEventAction) {
                    OnEventAction oeAct = (OnEventAction) act;
                    Expression exp = new IsEventExpression(oeAct.getEvent());
                    IfStatement stmt = new IfStatement(exp, act.getOperation().getCopy(),
                            null);
                    stmts.add(stmt);
                } else {
                    stmts.add(act.getOperation().getCopy());
                }
            }
        }
        return stmts;
    }

    @Override
    public List<Node> getPath(PathOption pathOption) {
        List<Node> path;
        if (getParentLocation() instanceof State) {
            path = ((State) getParentLocation()).getPath(pathOption);
        } else if (getParentLocation() instanceof ChartRoot) {
            path = ((ChartRoot) getParentLocation()).getChart().getPath(pathOption);
        } else {
            path = new LinkedList<Node>();
        }            
        path.add(this);
        return path;
    }
    
    @Override
    public String getPathString(String separator, PathOption pathOption) {
        List<Node> path = getPath(pathOption);
        int n = path.size();
        int i = 0;
        String pathString = new String();
        for (Node node : path) {
            pathString += node.getName();
            if (++i < n) {
                pathString += separator;
            }
        }
        return pathString;
    }
    
   @Override
    public List<Node> getChildNodes() {
        List<Node> childNodes = new LinkedList<Node>();
        
        // Immediate leaf Nodes
        for (GAModelElement e : getChildren(Node.class)) {
            childNodes.add((Node) e);
        }
        
        // leaf Nodes in the Composition
        if (getComposition() != null) {
            for (GAModelElement e : getComposition().getChildren(Node.class)) {
                childNodes.add((Node) e);
            }
        }
        return childNodes;
    }

   @Override
   public int compareTo(State s) {        
       if (this.getExecutionOrder() > s.getExecutionOrder()) {
           return 1;
       } else if (this.getExecutionOrder() < s.getExecutionOrder()) {
           return -1;
       } else {
           EventHandler.handle(EventLevel.ERROR, getClass().getSimpleName(), "", 
                   "Elements have equal executionOrder."
                   + "\nElement 1: " + this.getReferenceString()
                   + "\nElement 2: " + s.getReferenceString()
                   );
           return 0;
       }
   }

   /**
    * Sorts the States and transitions according to their executionOrder
    */
   public void sort() {
       Collections.sort(getOuterTransitions());
       Collections.sort(getInnerTransitions());
       Collections.sort(getEvents());
       if (getComposition() != null) {
           getComposition().sort();
       }
   }

   public void setMachineType(StateMachineType machineType) {
	   this.machineType = machineType;
   }

   public StateMachineType getMachineType() {
	   return machineType;
   }
      
}