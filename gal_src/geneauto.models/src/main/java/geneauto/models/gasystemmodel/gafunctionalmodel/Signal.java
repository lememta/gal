/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/Signal.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2010-11-08 20:06:33 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel;

import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Data;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;

import java.util.ArrayList;
import java.util.List;

/**
 * Connector describing data or control flow from source block output interface
 * (outport) to destination block input interface (inport).
 */
public class Signal extends Data {

	/**
	 * Reference to source port.
	 */
	protected Outport srcPort;
	/**
	 * Reference to destination port.
	 */
	protected Inport dstPort;
	
	/**
	 * Sample time property set by the user. Currently, the only legal value is -1
	 * (inherited).
	 */
	protected int sampleTime;

	/**
	 * Collection of observation point names.
	 * 
	 * If a signal has observation points, then there is a special observation
	 * variable created for each observation point for monitoring this Signal's
	 * value.
	 */
	private List<String> observationPoints = new ArrayList<String>();
	
	public Signal() {
		super();
	}

	public Signal(Outport srcPort, Inport dstPort, String storageClass, GADataType dataType) {
		super(storageClass, dataType);
		this.srcPort = srcPort;
		this.dstPort = dstPort;
	}

	public Inport getDstPort(){
		return dstPort;
	}

	public Outport getSrcPort(){
		return srcPort;
	}

	public Block getDstBlock(){
		return dstPort.getParentBlock();
	}

	public Block getSrcBlock(){
		return srcPort.getParentBlock();
	}

	public void setDstPort(Inport pdstPort){
        dstPort = pdstPort;
	}

    public void setSrcPort(Outport psrcPort){
        srcPort = psrcPort;
    }

    public List<String> getObservationPoints() {
		return observationPoints;
	}

	public void addObservationPoint(String observationPointName) {
		this.observationPoints.add(observationPointName);
	}

	public void addObservationPoints(List<String> observationPoints) {
		this.observationPoints.addAll(observationPoints);
	}

	public boolean isObservable() {
        return observationPoints.size() > 0;
    }

}