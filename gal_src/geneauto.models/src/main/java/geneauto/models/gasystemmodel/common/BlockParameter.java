/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/BlockParameter.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2010-04-07 11:22:50 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.ParameterType;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;
import geneauto.models.utilities.NameResolvingContext;
import geneauto.utils.map.ConstantMap;

/**
 * This is a SystemModel Parameter that is used to describe Block features 
 * common to one specific block type. Block parameter can be simply a string
 * having specific semantics in the context of this specific block type, a 
 * constant or any EML expression.
 */
public class BlockParameter extends Parameter {

    public BlockParameter() {
        super();
    }

    

    public BlockParameter(Expression value, String name) {
        super(value, name);
    }

    /** 
     * Creates parameter with given string value
     * 
     * @param name		name of the parameter
     * @param value		value of the parameter
     */
    public BlockParameter(String name, String value) {
    	super();
    	setName(name);
    	setValue(value);
    }

    /**
     * When defining function prototypes in s-functions and user-defined 
     * functions in Gene-auto block library there is a special notation for 
     * referring to block inputs, outputs and other parameters:
     * 
	 * "u" and a number - denotes an input port of the block, with the given index
	 * "y" and a number - denotes an output port of the block, with the given index
	 * "p" and a number - denotes a block parameter, with the given index
	 *
	 * When resolving named references it is important to understand whether 
	 * a symbol in expression is a simple reference by name or this special
	 * reference. 
	 * 
     * @param ident
     * @return true, if the argument is of form "u#", "y#" or "p#", where # is a
     *         number. Such special treatment of argument values is valid only
     *         in case of USerDefinedFunction blocks. In case of all other 
     *         blocks non-numeric parameter values are treated as symbols 
     *         referring to variables or constants
     */
    public boolean isSpecialIdentifier(String ident) {
    	if (this.supportsSpecialReferences()) {
    		return ident.matches("[uyp]\\d+");
    	} else {
    		return false;
    	}
    }

    @Override
    public void resolveReferences(ConstantMap<String, GAModelElement> nameMap,
            Model rootModel) {
        // Set a global flag that we are now in BlockParameterContext
        NameResolvingContext.setBlockParameterContext(this);

        // Resolve references in the current object and children
        super.resolveReferences(nameMap, rootModel);

        // Remove the global flag that we are now in BlockParameterContext
        NameResolvingContext.setBlockParameterContext(null);

    }
    
    /**
     * @return parameter type from block library. To fetch the parameter type, 
     * the parameter must be associated with a block before
     */
    public ParameterType getParameterType() {
    	// retrieve the parent -- here we can assume that the parent is
    	// always a block
    	Block b = (Block) this.getParent();
    	if (b == null) {
    		return null;
    	}
    	
    	BlockType bt = b.getBlockType();
    	if (bt == null) {
    		return null;
    	}
    	
    	return bt.getParameterTypeByName(name);
    }

    /**
     * checks if references to block inputs, outputs and parameters using
     * a notation (u|y|p)<no> are allowed for this parameter
     * 
     * @return boolean
     * 
     * TODO: in case of performance problems this method can be enhanced by 
     * composing a map of parameters supporting special references before the 
     * name resolving step (ToNa 07/04/10) 
     */
    private boolean supportsSpecialReferences() {
    	ParameterType pt = getParameterType();
    	
    	if (pt == null) {
    		return false;
    	}
    	
    	return pt.isSupportSpecialReferences();
    }
}
