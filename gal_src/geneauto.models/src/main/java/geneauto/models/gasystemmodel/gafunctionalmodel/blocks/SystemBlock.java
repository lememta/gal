/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/blocks/SystemBlock.java,v $
 *  @version	$Revision: 1.50 $
 *	@date		$Date: 2011-09-13 10:43:54 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.blocks;

import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.TernaryExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.FunctionStyle;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.GASystemModelRoot;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.gasystemmodel.gastatemodel.Event;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.general.NameFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A system is a block which contains other blocks.
 * 
 */
public class SystemBlock extends Block implements GASystemModelRoot {

    /**
     * List of the blocks which are part of the System.
     */
    protected List<Block> blocks = new ArrayList<Block>();
    /**
     * List of the signals which are part of the System.
     */
    protected List<Signal> signals = new ArrayList<Signal>();

    protected Variable_CM indexVariable = null;

    /**
     * List of all variables defined in an external .m file (constants) or
     * global stateflow variables. This attribute is applicable only in case of
     * a root level system.
     */
    protected List<Variable_SM> variables = new ArrayList<Variable_SM>();

    protected List<CustomType_SM> customTypes = new ArrayList<CustomType_SM>();

    /** Parameter that determines, how the function shall be generated */
    protected FunctionStyle style = FunctionStyle.FUNCTION;
    
    public List<CustomType_SM> getCustomTypes() {
        return customTypes;
    }

    public void setCustomTypes(List<CustomType_SM> customTypes) {
        this.customTypes = customTypes;
    }

    /**
     * List of global stateflow events. This attribute is applicable only in
     * case of a root level system.
     */
    protected List<Event> events = new ArrayList<Event>();

    public SystemBlock() {
        super();
    }

    /**
     * Method getBlocks.
     * 
     * @return the attribute "blocks".
     */
    public List<Block> getBlocks() {
        return blocks;
    }

    /**
     * Method getSignals.
     * 
     * @return the attribute "signals".
     */
    public List<Signal> getSignals() {
        return signals;
    }

    /**
     * Get the signal that enters to the given port.
     * 
     * @param p
     * @return
     */
    public Signal getIncomingSignal(Inport p) {
        for (Signal s : this.getSignals()) {
            if (s.getDstPort() == p) {
                return s;
            }
        }
        return null;
    }

    /**
     * Get all the signal that starting from the given port.
     * 
     * @param p
     * @return
     */
    public List<Signal> getOutcomingSignal(Outport p) {
        List<Signal> signals = new ArrayList<Signal>();
        for (Signal s : this.getSignals()) {
            if (s.getSrcPort() == p) {
                signals.add(s);
            }
        }
        return signals;
    }

    public List<Variable_SM> getVariables() {
        return variables;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void addBlock(Block block) {
        block.setParent(this);
        blocks.add(block);
    }

    public void addBlocks(List<Block> blocks) {
        if (blocks != null) {
            for (Block b : blocks) {
                addBlock(b);
            }
        }
    }

    public void setBlocks(List<Block> blocks) {
        this.blocks.clear();
        addBlocks(blocks);
    }

    /**
     * Adds block to given index
     * 
     * @param idx
     * @param block
     */
    public void addBlock(int idx, Block block) {
        block.setParent(this);
        blocks.add(idx, block);
    }

    public void addSignal(Signal signal) {
        signal.setParent(this);
        signals.add(signal);
    }

    public void addSignals(List<Signal> signals) {
        if (signals != null) {
            for (Signal s : signals) {
                addSignal(s);
            }
        }
    }

    public void setSignals(List<Signal> signals) {
        this.signals.clear();
        addSignals(signals);
    }

    public void addVariables(List<Variable_SM> variables) {
        if (variables != null) {
            for (Variable_SM v : variables) {
                addVariable(v);
            }
        }
    }

    public void setVariables(List<Variable_SM> variables) {
        this.variables.clear();
        addVariables(variables);
    }

    public void addVariable(Variable_SM variable) {
        variable.setParent(this);
        variables.add(variable);
    }

    public void addCustomType(CustomType_SM custom) {
        custom.setParent(this);
        customTypes.add(custom);
    }

    public void addCustomType(List<CustomType_SM> custom) {
        if (customTypes != null) {
            for (CustomType_SM c : customTypes) {
                addCustomType(c);
            }
        }
    }

    public void addEvent(Event event) {
        event.setParent(this);
        events.add(event);
    }

    /* User-defined name root for functions generated from this system */ 
    protected String fcnName;

    /**
     * Returns user-defined name to be used as root for all functions 
     * corresponding to this subsystem. The required suffixes are added 
     * in places where the function name is used.
     * 
     *  The order of determining the function name is as follows:
     *  	- when system has value for attribute fcnName, this value is 
     *  	returned
     *  	- otherwise, when system has parameter RTWFcnName its value is 
     *  	returned
     *  	- otherwise normalised subsystem name is returned
     *  
     *  NB! it is intentional, that only subsystem name is normalised. All other 
     *  names must follow naming convention of expected output language.
     *  
     * When used for the first time, the derived name is stored in fcnName 
     * attribute reducing the need of processing in subsequent calls
     * @return
     */
    public String getFcnName() {

    	if (fcnName != null && !fcnName.isEmpty()) {
    		return fcnName; 
    	}
    	
    	Parameter param = this.getParameterByName("RTWFcnName");
    	if (param == null 
    			|| param.getStringValue() == null
    			|| param.getStringValue().isEmpty()) {
    		fcnName = NameFormatter.normaliseFunctionName(getName(), 
    												getReferenceString());
    	} else {
    		fcnName = param.getStringValue();
    	}

        return fcnName;
    }

    public String getHandlingModeName() {
        String result = null;
        for (Parameter crtParam : parameters) {
            if (crtParam.getName().equals("RTWSystemCode")) {
                result = ((StringExpression) crtParam.getValue()).getLitValue();
            }
        }
        return result;
    }

    public String getFileName() {
        String result = null;
        for (Parameter crtParam : parameters) {
            if (crtParam.getName().equals("RTWFileName")) {
                result = ((StringExpression) crtParam.getValue()).getLitValue();
            }
        }
        return result;
    }

    public String getSysSampleTime() {
        String result = null;
        for (Parameter crtParam : parameters) {
            if (crtParam.getName().equals("SystemSampleTime")) {
                result = ((StringExpression) crtParam.getValue()).getLitValue();
            }
        }
        return result;
    }

    public Block getBlockFromName(String bloName) {
        Block result = null;
        for (Block currentBlock : blocks) {
            if (currentBlock.getName().equals(bloName)) {
                result = currentBlock;
            }
        }
        return result;
    }

    public String getMRCoeff() {
        String result = null;
        for (Parameter crtParam : parameters) {
            if (crtParam.getName().equals("MultiRateCoeff")) {
                result = ((StringExpression) crtParam.getValue()).getLitValue();
            }
        }
        return result;
    }

    /**
     * Check if system contain a sub-system controlled by a function-call
     * generated by a chart block.
     * 
     * @return
     */
    public boolean containsFunctionCalledBlock() {
        for (Block b : this.getBlocks()) {
            if ((b.getInControlPorts() != null)
                    && (b.getInControlPorts().size() > 0)) {
                for (InControlPort p : b.getInControlPorts()) {
                    Signal relatedSignal = null;
                    for (Signal s : this.getSignals()) {
                        if (s.getDstPort() == p) {
                            relatedSignal = s;
                        }
                    }
                    if ((relatedSignal == null)
                            || (relatedSignal.getSrcPort().getParent() instanceof ChartBlock)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if the current subsystem contains implicitly condition that 
     * causes its execution in a loop during a single pass through the model.
     * 
     * 
     * @return true when the given subsystem is executed in a loop
     */
    public boolean isIterated() {
        for (Block b : this.getBlocks()) {
            if (b.getType().equals("ForIterator")) {
                return true;
            }
        }
        return false;
    }

    public Block getForIteratorBlock() {
        for (Block b : this.getBlocks()) {
            if (b.getType().equals("ForIterator")) {
                return b;
            }
        }
        return null;
    }

    public Variable_CM getIndexVariable() {
        return indexVariable;
    }

    public void setIndexVariable(Variable_CM indexVariable) {
        this.indexVariable = indexVariable;
    }

    public void setFcnName(String fcnName) {
        this.fcnName = fcnName;
    }

    /**
     * Return list of variables stored in the system's upper super system
     * 
     * @return
     */
    public List<Variable_SM> getSystemVars() {
        List<Variable_SM> listVar;
        SystemBlock upperBlock = this;
        while (upperBlock.getParent() != null) {
            upperBlock = (SystemBlock) upperBlock.getParent();
        }
        listVar = upperBlock.getVariables();
        return listVar;
    }

    /**
     * Fill variable attribute of parameter expression by browsing a list of
     * variable and finding the one with the same name as the expression.
     * 
     * @param varList
     *            list of variable
     * @param expr
     *            expression with variable attribute to be filled
     */
    public void linkVartoVarExpr(List<Variable_SM> varList, Expression expr) {
        if (expr instanceof VariableExpression) {
            String variableName = expr.getName();
            String memberName = "";

            if (variableName.contains(".")) {
                String[] tab = variableName.split("\\.");
                variableName = tab[0];
                memberName = tab[1];
            }

            for (Variable_SM v : this.getSystemVars()) {
                if (!varList.contains(v)) {
                    varList.add(v);
                }
            }
            for (Variable_SM v : varList) {
                if (!memberName.equals("")) {
                    if (v.getName().equals(variableName)) {
                        StructureMember sm = ((TCustom) v.getDataType())
                                .getCustomType().getMemberByName(memberName);

                        ((VariableExpression) expr).setVariable(sm);
                        expr.setDataType(sm.getDataType());
                        continue;
                    }
                } else if (v.getName().equals(variableName)) {
                    ((VariableExpression) expr).setVariable(v);
                    expr.setDataType(v.getDataType());
                    continue;
                }
            }
        } else if (expr instanceof GeneralListExpression) {
            for (Expression currentExpr : ((GeneralListExpression) expr)
                    .getExpressions()) {
                this.linkVartoVarExpr(varList, currentExpr);
            }
        } else if (expr instanceof BinaryExpression) {
            Expression leftExpr = ((BinaryExpression) expr).getLeftArgument();
            this.linkVartoVarExpr(varList, leftExpr);
            Expression rightExpr = ((BinaryExpression) expr).getRightArgument();
            this.linkVartoVarExpr(varList, rightExpr);
        } else if (expr instanceof UnaryExpression) {
            Expression unaExpr = ((UnaryExpression) expr).getArgument();
            this.linkVartoVarExpr(varList, unaExpr);
        } else if (expr instanceof TernaryExpression) {
            Expression firstExpr = ((TernaryExpression) expr).getFirst();
            this.linkVartoVarExpr(varList, firstExpr);
            Expression secondExpr = ((TernaryExpression) expr).getSecond();
            this.linkVartoVarExpr(varList, secondExpr);
            Expression thirdExpr = ((TernaryExpression) expr).getThird();
            this.linkVartoVarExpr(varList, thirdExpr);
        }

    }

    public GADataType getOutportBlockType(Block outport) {

        Parameter port = outport.getParameterByName("Port");
        int portNumber = Integer.valueOf(((StringExpression) port.getValue()).getLitValue());

        if(getOutDataPorts().size() > 0){
            return getOutDataPorts().get(portNumber - 1).getDataType();
        }
        else{
            return null;
        }
    }
    
    /**
     * 
     * @param blockType type of the blocks to be returned
     * @param model
     * 
     * @return list of blocks which type = blockType composed of immediate 
     * children of the given block
     */
    public List<Block> getBlocksByType(String blockType) {
        List<Block> result = new ArrayList<Block>();
        for (GAModelElement elem: getChildren(Block.class)) {
            if (((Block) elem).getType().equals(blockType)) {
                result.add((Block) elem);
            }
        }
        return result;
    }

    /**
     * 
     * @param blockType type of the blocks to be returned
     * @param model
     * 
     * @return list of blocks which type = blockType composed of all 
     * children of the given block
     */
    public List<Block> getAllBlocksByType(String blockType) {
        List<Block> result = new ArrayList<Block>();
        for (GAModelElement elem: getAllChildren(Block.class)) {
        if (((Block) elem).getType().equals(blockType)) {
            result.add((Block) elem);
        }
    }
        return result;
    }

    /**
     * 
     * @param blockTypes types of the blocks to be returned
     * @param model 
     * 
     * @return map where the key is type of the block and 
     * value is a list of blocks with that type fetched from the
     * subtree of given block 
     */
    public Map<String, List<Block>> getAllBlocksByTypes(
            							Set<String> blockTypes) {
    	
        Map<String, List<Block>> resultMap =
            new HashMap<String, List<Block>>();
        List<Block> blockList;
        
        for (GAModelElement elem: getAllChildren(Block.class)) {
            
            /*
             * Check if type of the block 
             * is the same as one of the blockTypes
             */
            if (blockTypes.contains(((Block)elem).getType())) {
            	String blockType = ((Block)elem).getType();
                blockList = resultMap.get(blockType);
                if (blockList == null) {
                    blockList = new ArrayList<Block>();
                    resultMap.put(blockType, blockList);
                }
                blockList.add((Block)elem);
            }
        }
        return resultMap;
    }

    /**
     * 
     * @param blockTypes types of the blocks to be returned
     * @param model 
     * 
     * @return map where the key is type of the block and 
     * value is a list of blocks with that type fetched from the
     * immediate children of given block 
     */
    public Map<String, List<Block>> getBlocksByTypes(
            					Set<String> blockTypes) {
    	
        Map<String, List<Block>> resultMap =
            new HashMap<String, List<Block>>();
        List<Block> blockList;
        
        for (GAModelElement elem: getChildren(Block.class)) {
            
            /*
             * Check if type of the block 
             * is the same as one of the blockTypes
             */
            if (blockTypes.contains(((Block)elem).getType())) {
            	String blockType = ((Block)elem).getType();
                blockList = resultMap.get(blockType);
                if (blockList == null) {
                    blockList = new ArrayList<Block>();
                    resultMap.put(blockType, blockList);
                }
                blockList.add((Block)elem);
            }
        }
        return resultMap;
    }

	/**
	 * @param portBlock
	 * @return all signals (!*) belonging to the current system (*!) and
	 *         originating from the given portBlock
	 */
    public List<Signal> getSignalsFromPort(Block portBlock) {
    	List<Signal> lst = new LinkedList<Signal>();
    	for (Signal s : getSignals()) {
    		if (s.getSrcBlock() == portBlock) {
    			lst.add(s);
    		}
    	}
    	return lst;
    }
    
	/**
	 * @return all contained blocks of type SystemBlock (1 level)
	 */
    public List<SystemBlock> getInnerSystems() {
    	List<SystemBlock> lst = new LinkedList<SystemBlock>();
    	for (Block b : getBlocks()) {
    		if (b instanceof SystemBlock) {
    			lst.add((SystemBlock) b);
    		}
    	}
    	return lst;
    }

    public FunctionStyle getStyle() {
        return style;
    }

    public void setStyle(FunctionStyle style) {
        this.style = style;
    }

    public boolean isTreatAsAtomic() {
        Parameter param = getParameterByName("AtomicSubSystem");
        return param != null && "on".equals(param.getStringValue());
    }
}