/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/TransitionList.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Transition list.
 * 
 * This class implements Stateflow specific transition list evaluation.
 */
public class TransitionList extends GASystemModelElement {

	private LinkedList<Transition> transitions = new LinkedList<Transition>();

	/**
	 * Constructor
	 */
	public TransitionList() {
		super();
	}

	/*
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addTransition or setTransitions instead
	 */
	public List<Transition> getTransitions() {
		return transitions;
	}

	public void setTransitions(List<Transition> transitions) {
		this.transitions.clear();
		for (Transition t : transitions){
			addTransition(t);
		}
	}

	public void addTransition(Transition t){
		t.setParent(this);
		transitions.add(t);
	}
	
	public boolean isEmpty() {
		return transitions.isEmpty();
	}

	/**
	 * Helper function for evalAbs.
	 * Recursively traverses the list of transitions.
	 * @param position - the index of the transition to start processing from
	 * @return the code evaluating a list of transitions
	 */
	private List<Statement> evalAbs(
	        int position, EvaluationContext ctxt, 
	        SuccessContinuation success, FailureContinuation fail) {
		
		List<Statement> stmts = null;

		// Check next transition, if possible
		if (position >= transitions.size()) {
			stmts = fail.getStatements();
		}
		else {
			Transition trans = transitions.get(position);
			FailureContinuation fail2 = new FailureContinuation();
			// Set the failure continuation to the result of evaluating
			// the tail of the transition list
			fail2.setStatements(evalAbs(position + 1, ctxt, success.getCopy(), fail));
			// Evaluate the transition 
			// Note we don't make a copy of success here, since we only
			// have to make sure that both branches don't get the same success.
			// We can modify the supplied success in one branch.
			stmts = trans.evalAbs(ctxt, success, fail2);
		}		
		return stmts;
	}

	/**
	 * @return true, when the list of transitions contains an unguarded path to
	 *         a state, otherwise false
	 */
	public boolean hasUnguardedPathToState() {
		for (Transition t : getTransitions()) {
			if (t.isConditional() == false) {
				if (t.getDestination() instanceof State) {
					return true;
				} else if (t.getDestination() instanceof Junction) {
					Junction j = (Junction) t.getDestination();
					TransitionList tl = j.getTransitionList();
					if ((tl != null) && tl.hasUnguardedPathToState()) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Abstract evaluation of a list of transition.
	 * 
	 * @return the code evaluating a list of transitions
	 */
	public List<Statement> evalAbs(EvaluationContext ctxt, 
	        SuccessContinuation success, FailureContinuation fail) {
		return evalAbs(0, ctxt, success, fail);
	}

    /**
     * Sorts the transitions according to their executionOrder
     */
    public void sortStates() {
        Collections.sort(transitions);        
    }
	
}