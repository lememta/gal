/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/blocks/ChartBlock.java,v $
 *  @version	$Revision: 1.69 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.blocks;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.common.NameSpaceElement;
import geneauto.models.gacodemodel.CustomType_CM;
import geneauto.models.gacodemodel.Dependency;
import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.models.gacodemodel.Module;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.statemodel.IsOpenExpression;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gacodemodel.statement.statemodel.OpenStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.common.FunctionScope;
import geneauto.models.gasystemmodel.common.Function_SM;
import geneauto.models.gasystemmodel.common.Node;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.common.VariableScope;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gastatemodel.ChartRoot;
import geneauto.models.gasystemmodel.gastatemodel.Composition;
import geneauto.models.gasystemmodel.gastatemodel.Event;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Special block for Stateflow charts.
 */
public class ChartBlock extends Block {

    /**
     * Set of state-model or graphical function chart objects.
     */
    protected Composition composition;

    /**
     * Root location of the chart.
     */
    protected ChartRoot rootLocation;

    /**
     * Events owned by this block
     */
    protected List<Event> events = new ArrayList<Event>();

    /**
     * The function that performs the main computation of the chart.
     */
    protected Function_SM computeFunction;

    /**
     * The function that performs the initialisation of the chart.
     */
    protected Function_SM initFunction;

    /**
     * CodeModel expression referring to the active event
     */
    private Expression activeEventReference;

    /**
     * CodeModel expression referring to the context expression for using from
     * INSIDE the chart.
     */
    private Expression contextReference;

    /**
     * Data type of the structure for the Chart intance's local data
     */
    protected GADataType instanceStructType;

    /**
     * An expression used to refer the chart instance's data EXTERNALLY. This is
     * DIFFERENT in all instances of one chart.
     */
    private Expression instanceOuterReference;
    
    /**
     * Reference to the init function argument containing memory variables
     */
    private Expression stateArgInternalRef;

    /**
     * Default event (tick) for this chart;
     */
    private Event defaultEvent;

    /**
     * All functions of the block (e.g.graphical functions, truth-tables, EML
     * etc.), but not the initialisation function and main computation
     * functions. However, these are also returned by getFunctions().
     */
    protected List<Function_SM> functions = new ArrayList<Function_SM>();

    /**
     * Declares whether the functions should be exported TODO To be decided
     * whether we need such attribute in the model or can add the corresponding
     * scope to the functions while importing the model
     */
    protected boolean exportFunctions;

    /**
     * List of variables defined in the state model
     */
    protected List<Variable_SM> variables = new ArrayList<Variable_SM>();

    /**
     * Segment of CodeModel created form the current block
     */
    protected List<GACodeModelElement> codeModelElements = new ArrayList<GACodeModelElement>();

    /**
     * Execute the inner Composition during the initialisation. Otherwise, the
     * composition will be initialised at the first execution of the
     * composition. This is applicable only to a root-level composition. This
     * value is set by the user (under chart options).
     */
    protected boolean executeAtInit;

    /**
     * List of external header file names to be included in the generated code
     */
    protected List<Dependency> externalDependencies = new ArrayList<Dependency>();

    public ChartBlock() {
        super();
        rootLocation = new ChartRoot();
        rootLocation.setParent(this);
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setExportFunctions(boolean exportFunctions) {
        this.exportFunctions = exportFunctions;
    }

    public boolean isExportFunctions() {
        return exportFunctions;
    }

    public List<Function_SM> getFunctions() {
        return functions;
    }

    public void setComposition(Composition composition) {
        composition.setParent(this);
        this.composition = composition;
    }

    public Composition getComposition() {
        return composition;
    }

    public List<Variable_SM> getVariables() {
        return variables;
    }

    /**
     * @return SystemModel NameSpaceElements exported by the Chart.
     * 
     *         NOTE! Currently the method does not return the created custom
     *         types, as this is not required for planned usages of the
     *         function. However, it might be changed later.
     * 
     */
    public List<NameSpaceElement> getExportedElements() {
        List<NameSpaceElement> lst = new LinkedList<NameSpaceElement>();
        for (Variable_SM v : getVariables()) {
            if (v.getScope() == VariableScope.EXPORTED_VARIABLE) {
                lst.add(v);
            }
        }
        for (Function_SM f : getFunctions()) {
            if (f.getScope() == FunctionScope.EXPORTED_FUNCTION) {
                lst.add(f);
            }
        }
        if (getInitFunction() != null
                && getInitFunction().getScope() == FunctionScope.EXPORTED_FUNCTION) {
            lst.add(getInitFunction());
        }
        if (getComputeFunction() != null
                && getComputeFunction().getScope() == FunctionScope.EXPORTED_FUNCTION) {
            lst.add(getComputeFunction());
        }
        return lst;
    }

    /**
     * Special case of evaluating a chart, when there are no states in the
     * chart.
     * 
     * In this case there is no notion of the state of the chart - the default
     * path of the composition is traversed each time the chart is activated.
     * 
     * @return the code for evaluating a chart
     */
    private List<Statement> evalAbsNoStates() {
        List<Statement> stmts = new LinkedList<Statement>();

        // Evaluate the composition entry (the composition is stateless)
        if (composition != null) {
            stmts.addAll(composition.evalAbsEntry(null));
        }

        return stmts;
    }

    /**
     * Special case of evaluating a chart, when there are (sub)states in the
     * chart. In this case, if the chart has not been activated yet, then the
     * default path is traversed. Subsequent activations start from the highest
     * level states. The default path will never be taken again.
     * 
     * @return the code for evaluating a chart
     */
    private List<Statement> evalAbsWithStates() {
        List<Statement> stmts = new LinkedList<Statement>();

        // Check, if the chart is closed (not opened yet)
        Expression chartOpenExp = new IsOpenExpression(getRootLocation(),
                getModel().getCodeModel());
        Expression condExp = new UnaryExpression(chartOpenExp,
                UnaryOperator.NOT_OPERATOR);

        // Create then branch (chart is not open)
        List<Statement> thenStmts = new LinkedList<Statement>();
        
        // Evaluate the chart entry
        thenStmts.add(new BlankStatement("Open the chart"));        
        thenStmts.addAll(evalAbsEntry());
        
        // Create else branch (chart is open)
        List<Statement> elseStmts = new LinkedList<Statement>();
        
        // Evaluate an open chart
        elseStmts.add(new BlankStatement("Execute an open chart"));
        elseStmts.addAll(evalAbsDuring());

        // Make an if statement
        CompoundStatement thenStmt = new CompoundStatement(null, thenStmts);
        CompoundStatement elseStmt = new CompoundStatement(null, elseStmts);
        IfStatement ifStmt = new IfStatement(condExp, thenStmt, elseStmt);
        stmts.add(ifStmt);

        return stmts;
    }

    /**
     * Abstract evaluation of a chart. Consists of a check, whether the chart is
     * active or not. And all following activities in the chart, based on the
     * incoming event.
     * 
     * @return the code for evaluating a chart
     */
    public List<Statement> evalAbs() {
        List<Statement> stmts;

        // Evaluate chart
        if (composition == null || composition.isStateless()) {
            stmts = evalAbsNoStates();
        } else {
            stmts = evalAbsWithStates();
        }
        
        // Handle jumps generated from Junctions
		stmts = EvaluationContext.handleJunctionRefs(stmts, this);
        
        return stmts;
    }

    /**
     * Abstract evaluation of the chart entry. Opens the locations in the chart.
     * 
     * @return the code for entering a chart
     */
    public List<Statement> evalAbsEntry() {
        List<Statement> stmts = new LinkedList<Statement>();

        Statement openingStmt = new OpenStatement(getRootLocation());
        // openingStmt.addAnnotation("Open chart");
        stmts.add(openingStmt);
        BlankStatement comnt = new BlankStatement(
                "Enter the chart's composition");
        stmts.add(comnt);
        stmts.addAll(composition.evalAbsEntry(null));

        return stmts;
    }

    /**
     * Abstract evaluation of executing a chart that is already open.
     * 
     * @return the code executing an open chart
     */
    public List<Statement> evalAbsDuring() {
        if (getComposition() != null) {
            return getComposition().evalAbsDuring(null);
        } else {
            return null;
        }
    }

    public boolean isExecuteAtInit() {
        return executeAtInit;
    }

    public void setExecuteAtInit(boolean executeAtInit) {
        this.executeAtInit = executeAtInit;
    }

    public Expression getActiveEventReference() {
        return activeEventReference;
    }

    public void setActiveEventReference(Expression activeEventReference) {
        this.activeEventReference = activeEventReference;
    }

    public Expression getContextReference() {
        return contextReference;
    }

    public void setContextReference(Expression contextReference) {
        this.contextReference = contextReference;
    }

    /**
     * Returns list with code model elements from the dependent model referenced
     * by the elements of codeModel list
     */
    public List<GACodeModelElement> getCodeModelElements() {
        List<GACodeModelElement> resultList = new ArrayList<GACodeModelElement>();

        if (codeModelElements != null) {
            for (GACodeModelElement el : codeModelElements) {
                if (el != null && el instanceof GACodeModelElement) {
                    resultList.add((GACodeModelElement) el);
                }
            }
        }

        return resultList;
    }

    /**
     * Makes object of type GAModelElementReference for each element in the
     * input GACodeModelElement array and stores the created object to codeModel
     * attribute of current element.
     * 
     * Old contents of codeModel attribute are deleted
     */
    public void setCodeModelElements(List<GACodeModelElement> cmEl) {
        this.codeModelElements = new ArrayList<GACodeModelElement>();
        // create new reference
        for (GACodeModelElement e : cmEl) {
            addCodeModelElement(e);
        }
    }

    /**
     * Makes object of type GAModelElementReference referencing in the input
     * GACodeModelElement and stores the created object to codeModel attribute
     * of current element.
     * 
     * The new element is added to the previous contents of the codeModel
     * attribute
     */
    public void addCodeModelElement(GACodeModelElement cmEl) {
        // create new reference
        this.codeModelElements.add(cmEl);
    }

    public void setEvents(List<Event> events) {
        this.events.clear();
        for (Event e : events) {
            addEvent(e);
        }
    }

    public void addEvent(Event e) {
        e.setParent(this);
        events.add(e);
    }

    public void addEvents(List<Event> ee) {
    	for (Event e : ee) {
    		addEvent(e);
    	}
    }

    public void setFunctions(List<Function_SM> functions) {
        this.functions.clear();
        for (Function_SM f : functions) {
            addFunction(f);
        }
    }

    public void addFunction(Function_SM f) {
        f.setParent(this);
        functions.add(f);
    }

    public void setVariables(List<Variable_SM> variables) {
        this.variables.clear();
        for (Variable_SM v : variables) {
            addVariable(v);
        }
    }

    public void addVariable(Variable_SM variable) {
        variable.setParent(this);
        variables.add(variable);

    }

    public Function_SM getComputeFunction() {
        return computeFunction;
    }

    public void setComputeFunction(Function_SM computeFunction) {
        computeFunction.setParent(this);
        this.computeFunction = computeFunction;
    }

    public Function_SM getInitFunction() {
        return initFunction;
    }

    public void setInitFunction(Function_SM initFunction) {
        initFunction.setParent(this);
        this.initFunction = initFunction;
    }

    public void setInstanceStructType(GADataType t) {
        this.instanceStructType = t;
    }

    /**
     * @return the data type of the chart instance structure. Note: when the
     *         structure has no members, then null is returned
     */
    public GADataType getInstanceStructType() {
        return instanceStructType;
    }

    /**
     * @param portName
     *            NB! Only for dataPorts currently.
     * @return expression referring to the given port, based on the given
     *         instance of the chartInstanceStruct
     * 
     */
    public Expression getCodeModelReferenceForPort(String portName) {
        CustomType tInstanceStruct = ((TCustom) getInstanceStructType())
                .getCustomType();
        StructureMember portMember = tInstanceStruct.getMemberByName(portName);
        Expression refExpr = getInstanceOuterReference();
        // Return instStructExpr.ports.portName
        return new MemberExpression(portMember, refExpr.getCopy());
    }

    public Expression getInstanceOuterReference() {
        return instanceOuterReference;
    }

    public void setInstanceOuterReference(Expression instanceStructExpression) {
        this.instanceOuterReference = instanceStructExpression;
    }

    /**
     * A Chart is converted to calls that execute charts + the actual code for
     * the charts. The ChartBackend returns the calls to charts + the modules
     * that contain stubs of the chart execution functions This is required in
     * order to have intact links between calls to chart and the execution code
     * itself.
     * 
     * @return List of modules generated by the chart.
     */
    public List<Module> getGeneratedModules() {
        List<Module> modules = new LinkedList<Module>();
        for (GACodeModelElement el : getCodeModelElements()) {
            if (el instanceof Module) {
                modules.add((Module) el);
            }
        }
        return modules;
    }

    public Event getDefaultEvent() {
        return defaultEvent;
    }

    /**
     * @return a CodeModel expression referring the default event for this
     *         chart.
     */
    public Expression getDefaultEventReference() {
        GACodeModelElement e = defaultEvent.getReferenceExpression();

        if (e != null) {
            if (e instanceof Expression) {
                return (Expression) e;
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        "ChartBLock.getDefaultEventReference", "",
                        "Incorrect function reference type: \"" + e.getClass()
                                + "\". Expected \"Expression\". \n"
                                + getReferenceString(), "");
            }
        }

        return null;
    }

    public void setDefaultEvent(Event defaultEvent) {
        if (defaultEvent != null) {
            defaultEvent.setParent(this);
        }
        this.defaultEvent = defaultEvent;
    }

    public ChartRoot getRootLocation() {
        return rootLocation;
    }

    /**
     * NOTE: A different implementation as in Block
     */
    @Override
    public List<Node> getPath(PathOption pathOption) {
        List<Node> path;
        if (pathOption == PathOption.STATEMODEL_NO_CHART) {
            path = new LinkedList<Node>();
        } else if (pathOption == PathOption.STATEMODEL) {
            path = new LinkedList<Node>();
            path.add(this);
        } else if (pathOption == PathOption.COMPLETE) {
            if (getParent() instanceof Node) {
                path = ((Node) getParent()).getPath(pathOption);
            } else {
                path = new LinkedList<Node>();
            }
            path.add(this);
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "ChartBlock.getPath", "", "Unsupported PathOption: "
                            + pathOption, "");
            return null;
        }
        return path;
    }

    @Override
    public List<Node> getChildNodes() {
        List<Node> childNodes = new LinkedList<Node>();

        // Immediate leaf Nodes
        for (GAModelElement e : getChildren(Node.class)) {
            childNodes.add((Node) e);
        }

        // leaf Nodes in the Composition
        if (getComposition() != null) {
            for (GAModelElement e : getComposition().getChildren(Node.class)) {
                childNodes.add((Node) e);
            }
        }
        return childNodes;
    }

	public void addExternalDependency(Dependency externalDependency) {
		if (externalDependency!=null) {
		    if (externalDependency.getParent() == null) {
                externalDependency.setParent(this);
	            this.externalDependencies.add(externalDependency);		        
		    } else {
		        Dependency dCopy = externalDependency.getCopy();
		        dCopy.setParent(this);
                this.externalDependencies.add(dCopy);		        
		    }
		}
	}

	public void setExternalDependencies(List<Dependency> externalDependencies) {
	    this.externalDependencies.clear();
		for (Dependency d : externalDependencies) {
			addExternalDependency(d);
		}
	}

	public List<Dependency> getExternalDependencies() {
		return externalDependencies;
	}

	public Expression getStateArgInternalRef() {
		return stateArgInternalRef;
	}

	public void setStateArgInternalRef(Expression stateArgInternalRef) {
		this.stateArgInternalRef = stateArgInternalRef;
	}

	/**
	 * Shorthand for retrieving data type for state variables
	 * @return
	 */
	public CustomType_CM getStateVariablesStructType() {
		return getModel().getCodeModel().getTempModel().getStateVariablesStructType();
	}
}
