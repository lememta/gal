/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/ports/OutDataPort.java,v $
 *  @version	$Revision: 1.20 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Variable_SM;

/**
 * Block interface producing data signals
 */
public class OutDataPort extends Outport {

	/** 
	 * Data type of output port 
	 */
	protected GADataType dataType;
	
	/**
	 * Initial output value of the port (available before the block is
	 * executed first time
	 */
	protected Expression initialOutput;
	
	/**
	 * ChartBlock variable corresponding to this OutDataPort
	 */
	protected Variable_SM variable;

	/**
	 * "true" if "Output when disabled" parameter is "reset", "false" if "held". When
	 * true, and the subsystem has enable port, the output value should be reset to
	 * initialOutput when subsystem is disabled. When "false" the output preserves the
	 * value produced by last calculation.
	 */
	protected boolean resetOutput;

	public OutDataPort(){
		super();
	}

	public GADataType getDataType(){
		return dataType;
	}

	/**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
	public void setDataType(GADataType dataType) {
		if (dataType != null){
		    if (dataType.getParent() == null) {
		        this.dataType = dataType;
		    } else {
		        this.dataType = dataType.getCopy();
		    }
			this.dataType.setParent(this);
		}
		else
			this.dataType = null;
	}

	public Expression getInitialOutput() {
		return initialOutput;
	}

	public void setInitialOutput(Expression initialOutput) {
		this.initialOutput = initialOutput;
		this.initialOutput.setParent(this);
	}

	public boolean isResetOutput() {
		return resetOutput;
	}

	public void setResetOutput(boolean resetOutput) {
		this.resetOutput = resetOutput;
	}
	
	public void setVariable(Variable_SM variable) {
		this.variable = variable;
	}

	public Variable_SM getVariable() {
		return variable;
	}

}