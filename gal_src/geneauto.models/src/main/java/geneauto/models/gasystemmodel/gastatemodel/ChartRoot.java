/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/ChartRoot.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.List;

/**
 * A location associated with the chart itself. Only semantical meaning is
 * during the chart initialisation - it is important, whether the chart is open
 * or not. Cannot be target to destinations.
 */
public class ChartRoot extends Location {

    // Constructor
    public ChartRoot() {
        super();
    }

    /**
     * @return the Chart's name.
     */
    public String getQualifiedName() {
        return getName();
    }

    // Not used in this class
    public List<Statement> evalAbsDestination(EvaluationContext ctxt, 
            SuccessContinuation success,
            FailureContinuation fail) {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "State", "GE9998",
                "Call to unsupported method: evalAbsDestination() Element: "
                        + getReferenceString(), "");
        return null;
    }

    @Override
    public String getName() {
        if (getChart() != null) {
            return getChart().getName();
        } else {
            return null;
        }
    }

    /**
     * Returns the parenting Chart.
     */
    public ChartBlock getChart() {
        if (getParent() != null) {
            if (getParent() instanceof ChartBlock) {
                return (ChartBlock) getParent();
            } else {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, "ChartRoot",
                        "", "Unexpected parent class: "
                                + getParent().getReferenceString(), "");
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Return the variables in the parenting Chart.
     * 
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use addVariable instead
     */
    public List<Variable_SM> getVariables() {
        return getChart().getVariables();
    }

}