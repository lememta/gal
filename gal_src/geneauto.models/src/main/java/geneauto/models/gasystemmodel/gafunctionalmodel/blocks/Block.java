/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/blocks/Block.java,v $
 *  @version	$Revision: 1.71 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.blocks;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockBackend;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.common.ContainerNode;
import geneauto.models.gasystemmodel.common.Node;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Port;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * A node from the input model describing computation. On the highest level of
 * abstraction the functional model is a graph consisting of Signals and Blocks.
 * Block represents data processing (computation) while signal shows data
 * transfer between two computations.
 * 
 * Block may be either a primitive block implementing directly a specific
 * algorithm (defined in block library) or complex that has its functionality
 * defined by lower-level system model or state model chart.
 */
public class Block extends GASystemModelElement implements ContainerNode {

    /**
     * (Simulink) Type of the block (type name that should exist in block
     * library)
     */
    protected String type;
    /**
     * Mask type of the block (if block has mask, and the mask name matches
     * block type in block library, then maskType is used instead of type.
     */
    protected String maskType;
    /**
     * List of input data ports
     */
    protected List<InDataPort> inDataPorts = new ArrayList<InDataPort>();
    /**
     * Enable port
     */
    protected InEnablePort inEnablePort;
    /**
     * Edge Enable port
     */
    protected InEdgeEnablePort inEdgeEnablePort;
    /**
     * List of input control ports
     */
    protected List<InControlPort> inControlPorts = new ArrayList<InControlPort>();
    /**
     * List of output data ports
     */
    protected List<OutDataPort> outDataPorts = new ArrayList<OutDataPort>();
    /**
     * List of output control ports
     */
    protected List<OutControlPort> outControlPorts = new ArrayList<OutControlPort>();
    /**
     * Determines, if GASystemModel is virtual object (will not be converted to
     * output code)
     */
    protected boolean isVirtual;
    /**
     * List of block parameters
     */
    protected Set<BlockParameter> parameters = new HashSet<BlockParameter>();
    /**
     * Sample time property set by the user. -1 = inherited.
     */
    protected double sampleTime;
    /**
     * This attribute is used for analysing loops (determining, whether a loop
     * is algebraic or not).
     */
    protected boolean directFeedThrough = true;
    protected int userDefinedPriority;
    protected int assignedPriority;
    protected int executionOrder;
    protected String assignedPrioritySource;

    /**
     * Block attributes related to its appearance on a diagram
     */
    protected DiagramInfo diagramInfo;

    /**
     * Reference to parent subsystem port that is attached to this block 
     * this attribute is relevant in case of Inport and Outport blocks only
     * TODO This attribute should be moved to the Inport and Outport blocks. 
     * Possibly also with a more specific type (AnTo 100309).
	 * ToNa 10/03/10: port already has this reference. Since it is required to find 
     * block from inport and port from block both attributes were added.
     */
    protected Port portReference;

    public Block() {
        super();
        diagramInfo = new DiagramInfo();
    }

    public int getAssignedPriority() {
        return assignedPriority;
    }

    public String getAssignedPrioritySource() {
        return assignedPrioritySource;
    }

    public DiagramInfo getDiagramInfo() {
        return diagramInfo;
    }

    public boolean isDirectFeedThrough() {
        return directFeedThrough;
    }

    public int getExecutionOrder() {
        return executionOrder;
    }

    public List<InControlPort> getInControlPorts() {
        return inControlPorts;
    }

    public List<InDataPort> getInDataPorts() {
        return inDataPorts;
    }

    public InEdgeEnablePort getInEdgeEnablePort() {
        return inEdgeEnablePort;
    }

    public InEnablePort getInEnablePort() {
        return inEnablePort;
    }

    /**
     * Returns in data port corresponding to given port number
     * 
     * @param no
     * @return
     */
    public InDataPort getInDataPortByNo(int no) {
    	for (InDataPort p : getInDataPorts()) {
    		if (p.getPortNumber() == no) {
    			return p;
    		}
    	}
    	
    	return null;
    }

    /**
     * Returns out data port corresponding to given port number
     * 
     * @param no
     * @return
     */
    public OutDataPort getOutDataPortByNo(int no) {
    	for (OutDataPort p : getOutDataPorts()) {
    		if (p.getPortNumber() == no) {
    			return p;
    		}
    	}
    	
    	return null;
    }

    /**
     * 
     * @return list with all input ports of the block
     */
    public List<Inport> getAllInports() {
        List<Inport> inputPorts = new LinkedList<Inport>();
        inputPorts.addAll(getInDataPorts());
        inputPorts.addAll(getInControlPorts());
        if (getInEnablePort() != null) {
            inputPorts.add(getInEnablePort());
        }
        if (getInEdgeEnablePort() != null) {
            inputPorts.add(getInEdgeEnablePort());
        }

        return inputPorts;
    }

    /**
     * 
     * @return all output ports of a block
     */
    public List<Outport> getAllOutports() {
        List<Outport> outputPorts = new LinkedList<Outport>();
        outputPorts.addAll(getOutDataPorts());
        outputPorts.addAll(getOutControlPorts());

        return outputPorts;
    }

    /**
     * 
     * @return all ports of a block
     */
    public List<Port> getAllPorts() {
        List<Port> allPorts = new LinkedList<Port>();
        allPorts.addAll(getAllInports());
        allPorts.addAll(getAllOutports());

        return allPorts;
    }

    /**
     * Return a list containing all InPorts except the InControlPorts
     * 
     * @return the list with InDataPorts, InEdgeEnablePort, InEnablePort
     */
    public List<Inport> getAllInDataPorts() {
        List<Inport> result = new ArrayList<Inport>();
        if (inEdgeEnablePort != null) {
            result.add(inEdgeEnablePort);
        }
        if (inEnablePort != null) {
            result.add(inEnablePort);
        }
        result.addAll(inDataPorts);
        return result;
    }

    public boolean isVirtual() {
        return isVirtual;
    }

    public List<OutControlPort> getOutControlPorts() {
        return outControlPorts;
    }

    public List<OutDataPort> getOutDataPorts() {
        return outDataPorts;
    }

    public Set<BlockParameter> getParameters() {
        return parameters;
    }

    public BlockParameter getParameterByName(String name) {
        for (BlockParameter p : parameters) {
            if (name.equals(p.getName()))
                return p;
        }

        return null;
    }

    /**
     * returns parameter with given index
     * 
     * @param idx -- positive integer. In case of a negative index null 
     * 				 is returned, as default value for index is -1 and there
     * 				 may be more than one index with the default value 
     * @return
     */
    public Parameter getParameterByIndex(int idx) {
    	if (idx > -1) {
	        for (Parameter p : parameters) {
	            if (p.getIndex() == idx)
	                return p;
	        }
    	}

        return null;
    }

    /**
     * Returns sample time of the block
     * The method assumes, the block has discrete sample time.
     *  
     * There are two special values:
     * 	"-1" means the sample time is inherited
     *   "0" means the sample time is continuous. 
     *   Continuous sample time is translated to inherited i.e. 
     *   the procedure returns "-1" 
     *   
     * @return -1 or positive value that is greater than zero
     */
    public double getSampleTime() {
    	if (sampleTime == 0) {
    		return -1;
    	} else {
    		return sampleTime;
    	}
    }

    /**
     * (Simulink) Type of the block (type name that should exist in block
     * library)
     */
    public String getType() {
        return type;
    }

    public int getUserDefinedPriority() {
        return userDefinedPriority;
    }

    /**
     * in case the block has userDefined priority returns this, 
     * otherwise returns assignedPriority
     *  
     * @return
     */
    public int getPriority() {
    	if (getUserDefinedPriority() == 0) {
    		return getAssignedPriority();
    	} else {
    		return getUserDefinedPriority();
    	}
    }
    
    public void setExecutionOrder(int executionOrder) {
        this.executionOrder = executionOrder;
    }

    public void setVirtual(boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

    public void setAssignedPriority(int assignedPriority) {
        this.assignedPriority = assignedPriority;
    }

    public void setAssignedPrioritySource(String assignedPrioritySource) {
        this.assignedPrioritySource = assignedPrioritySource;
    }

    public void setDiagramInfo(DiagramInfo diagramInfo) {
        this.diagramInfo = diagramInfo;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDirectFeedThrough(boolean directFeedThrough) {
        this.directFeedThrough = directFeedThrough;
    }

    public void setParameters(List<BlockParameter> parameters) {
        this.parameters.clear();
        for (BlockParameter p : parameters) {
            addParameter(p);
        }
    }

    public void addParameters(List<BlockParameter> parameters) {
        for (BlockParameter p : parameters) {
            addParameter(p);
        }
    }

    /**
     * Adds parameter to parameter set. If there is alredy a parameter 
	 * with the same name, the old object will be replaced
     * 
     * @param parameter
     */
    public void addParameter(BlockParameter parameter) {
        if (parameter != null) {
            parameter.setParent(this);
            parameters.add(parameter);
        }
    }

    /**
     * Adds block parameter as pair of strings
     * @param name		name of the parameter
     * @param value		value of the parameter
     */
    public void addParameter(String name, String value) {
    	addParameter(new BlockParameter(name, value));
    }
    /**
     * Adds parameter to parameter list if the list does not contain parameter
     * with the same name
     * 
     * @param parameter
     */
    public boolean addUniqueParameter(BlockParameter parameter) {
    	if (parameters.contains(parameter)) {
    		return false;
    	} else {
    		addParameter(parameter);
    	}
    	
    	return true;
    }

    public void setInDataPorts(List<InDataPort> inDataPorts) {
        this.inDataPorts.clear();
        for (InDataPort p : inDataPorts) {
            addInDataPort(p);
        }
    }

    public void addInDataPort(InDataPort port) {
        inDataPorts.add(port);
        if (port != null) {
            port.setParent(this);
        }
    }

    public void setInEnablePort(InEnablePort inEnablePort) {
        this.inEnablePort = inEnablePort;
        if (inEnablePort != null) {
            inEnablePort.setParent(this);
        }
    }

    public void setInEdgeEnablePort(InEdgeEnablePort inEdgeEnablePort) {
        this.inEdgeEnablePort = inEdgeEnablePort;
        if (inEdgeEnablePort != null) {
            inEdgeEnablePort.setParent(this);
        }
    }

    public void addInControlPort(InControlPort port) {
        inControlPorts.add(port);
        if (port != null) {
            port.setParent(this);
        }
    }

    public void setOutDataPorts(List<OutDataPort> outDataPorts) {
        this.outDataPorts.clear();
        for (OutDataPort p : outDataPorts) {
            addOutDataPort(p);
        }
    }

    public void addOutDataPort(OutDataPort port) {
        outDataPorts.add(port);
        if (port != null) {
            port.setParent(this);
        }
    }

    public void setOutControlPorts(List<OutControlPort> outControlPorts) {
        this.outControlPorts.clear();
        for (OutControlPort p : outControlPorts) {
            addOutControlPort(p);
        }
    }

    public void addOutControlPort(OutControlPort port) {
        outControlPorts.add(port);
        if (port != null) {
            port.setParent(this);
        }
    }

    public void setSampleTime(double sampleTime) {
        this.sampleTime = sampleTime;
    }

    public void setUserDefinedPriority(int userDefinedPriority) {
        this.userDefinedPriority = userDefinedPriority;
    }

    public Parameter getParameterByValue(String name) {

        Parameter p = null;

        for (Parameter param : this.getParameters()) {
            if (param.getName().equals(name)) {
                p = param;
            }
        }

        return p;
    }

    public String getMaskType() {
        return maskType;
    }

    public void setMaskType(String maskType) {
        this.maskType = maskType;
    }

    @Override
    public List<Node> getPath(PathOption pathOption) {
        List<Node> path;
        if (getParent() instanceof Node) {
            path = ((Node) getParent()).getPath(pathOption);
        } else {
            path = new LinkedList<Node>();
        }
        path.add(this);
        return path;
    }

    @Override
    public String getPathString(String separator, PathOption pathOption) {
        List<Node> path = getPath(pathOption);
        int n = path.size();
        int i = 0;
        String pathString = new String();
        for (Node node : path) {
            pathString += node.getName();
            if (++i < n) {
                pathString += separator;
            }
        }
        return pathString;
    }

    /**
     * Returns BlockType object corresponding to the name of block in
     * BlockLibrary attached to the system model.
     * 
     * @return BlockType
     */
    public BlockType getBlockType() {
        GABlockLibrary bl = ((GASystemModel) model).getBlockLibrary();

        if (bl == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "Block.getBlockType", "",
                    "System model does not have attached block library", "");
            return null;
        }

        return bl.getBlockTypeByName(getType());
    }

    /**
     * Returns Typer class corresponding to the current block.
     * Throws CRITICAL_ERROR in case the block type or typer can not be found
     *
     * @return BlockTyper
     */
    public BlockTyper getBlockTyper() {
    	BlockType bt = getBlockType();
    	BlockTyper typer = null;
    	
    	if (bt != null) {
    		typer = bt.getLibraryHandler().getTyper();
    	}
    	
    	if (typer == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "Block.getBlockTyper", "",
                    "\nUnable to instantiate block typer." +
                    "\n Block: " + getReferenceString());
            return null;    		
    	}
    	
    	return typer;
    }

    /**
     * Returns Backend class corresponding to the current block.
     * Throws CRITICAL_ERROR in case the block type or backend can not be found
     *
     * @return BlockBackend
     */
    public BlockBackend getBlockBackend() {
    	BlockType bt = getBlockType();
    	BlockBackend backend = null;
    	
    	if (bt != null) {
    		backend = bt.getLibraryHandler().getBackend();
    	}
    	
    	if (backend == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "Block.getBlockTyper", "",
                    "Unable to instantiate block backend", "");
            return null;    		
    	}
    	
    	return backend;
    }

    @Override
    public List<Node> getChildNodes() {
        List<Node> childNodes = new LinkedList<Node>();
        for (GAModelElement e : getChildren(Node.class)) {
            childNodes.add((Node) e);
        }
        return childNodes;
    }

    public Port getPortReference() {
        return portReference;
    }

    public void setPortReference(Port portReference) {

        // to avoid loops
        if (this.portReference == portReference) {
            return;
        }

        // make sure that there will not remain orphaned
        // block reference in port
        if (this.portReference != null) {
            Port tmpPort = this.portReference;
            this.portReference = null;
            tmpPort.setSourceBlock(null);
        }

        this.portReference = portReference;

        // the reference between block and port is always symmetrical
        if (this.portReference != null) {
            this.portReference.setSourceBlock(this);
        }
    }

    @Override
    public void breakReferences() {
        super.breakReferences();
        // when block was removed from a model make sure that
        // there is no port that refers to it
        setPortReference(null);
    }

    public void cleanPorts() {
        for (Port p : this.getAllPorts()) {
            p.setSourceBlock(null);
        }
    }

    public List<Signal> getIncomingSignals() {
        List<Signal> signalList = new ArrayList<Signal>();

        // incoming signals are always in the parent of block
        SystemBlock signalParent = (SystemBlock) getParent();

        if (signalParent == null)
            return signalList;

        // create HashSet of port for efficient searching
        Set<Inport> portMap = new HashSet<Inport>();
        portMap.addAll(getAllInports());
        for (Signal s : signalParent.getSignals()) {
            if (portMap.contains(s.getDstPort())) {
                signalList.add(s);
            }
        }

        return signalList;
    }
    
    public List<Signal> getIncomingControlSignals() {
        List<Signal> signalList = new ArrayList<Signal>();

        // when there is no control ports, there is no control signals
        // either
        if (getInControlPorts().size() == 0) {
        	return signalList;
        }
        
        // incoming signals are always in the parent of block
        SystemBlock signalParent = (SystemBlock) getParent();

        if (signalParent == null){
            return signalList;
        }

        // create HashSet of port for efficient searching
        Set<Inport> portMap = new HashSet<Inport>();
        portMap.addAll(getInControlPorts());
        for (Signal s : signalParent.getSignals()) {
            if (portMap.contains(s.getDstPort())) {
                signalList.add(s);
            }
        }

        return signalList;
    }    

    public List<Signal> getOutgoingSignals() {
        List<Signal> signalList = new ArrayList<Signal>();
        Set<Outport> portMap = new HashSet<Outport>();
        portMap.addAll(getAllOutports());
        
        // outgoing signals are always in the parent block
        // and parent can only be a system block
        SystemBlock signalParent = (SystemBlock) getParent();

        if (signalParent == null)
            return signalList;

        for (Signal s : signalParent.getSignals()) {
            if (portMap.contains(s.getSrcPort())) {
                signalList.add(s);
            }
        }

        return signalList;
    }

    public Statement getTargetCode(OutControlPort ocp) {

        if (this.getOutControlPorts().contains(ocp)) {
            Statement stmt = null;
            SystemBlock sb = (SystemBlock) this.getParent();
            for (Signal s : sb.getSignals()) {
                if (s.getSrcPort().equals(ocp)) {
                    Block targetBlock = (Block) s.getDstPort().getParent();
                    // Get computation code of the target block
                    stmt = (Statement) targetBlock.getCodeModelElement();
                }
            }
            return stmt;
        }
        return null;
    }

    /**
     * Returns set of blocks controlling this block. 
     * In case of a controlling block cascade returns the whole
     * chain (i.e. the initiator of function call
     * 
     * @return
     */
    public Set<Block> getControllingBlocks() {
    	Set<Block> ctrlBlocks = new HashSet<Block>();
    	Block srcBlock = null;
    	
    	for (Signal s : getIncomingControlSignals()) {
    		srcBlock = (Block) s.getSrcPort().getParent();
    		if (srcBlock != null) {
    			if (srcBlock.isControlled()) {
    				ctrlBlocks.addAll(srcBlock.getControllingBlocks());
    			} 
    			
    			ctrlBlocks.add(srcBlock);
    		}
    	}
    	
    	return ctrlBlocks;
    }
    
    /**
     * Returns all blocks providing data to this block
     * In case of function-call activated block returns also
     * inputs of the activator block
     * 
     * @return
     */
    public Set<Block> getDataSources() {
    	Set<Block> srcBlocks = new HashSet<Block>();
    	Block srcBlock = null;
    	
    	for (Signal s : getIncomingSignals()) {
    		if (!(s.getDstPort() instanceof InControlPort)){
	    		srcBlock = (Block) s.getSrcPort().getParent();
	    		if (srcBlock != null) {
	    			if (srcBlock.isControlled()) {
	    				srcBlocks.addAll(srcBlock.getControllingBlocks());
	    			} 
	    			
	    			srcBlocks.add(srcBlock);
	    		}
    		}
    	}
    	
    	return srcBlocks;
    }
    
    /**
     *  @return true if the block is controlled blocks (the block has 
     *  InControlPort)
     *  Currently the check relies on the number of input control ports.
     *  Another criteria would be he execution order value, but this would 
     *  work only after TBlockSequencer
     */
    public boolean isControlled() {
    	if (inControlPorts.size() > 0) {
    		return true;
    	}
    	
    	return false;
    }
    
    /**
     * @return true if block has incoming enable or edge-enable port
     */
    public boolean isEnabled() {
    	if (inEdgeEnablePort != null || inEnablePort != null) {
    		return true;
    	}
    	
    	return false;
    }
}