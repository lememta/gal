/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/ports/Inport.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2010-09-02 14:02:31 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract base class for all input ports
 */
public class Inport extends Port {

	   /**
     * Data type of the port
     */
    protected GADataType dataType;
	
    /* determines if port is connected to data consumer block inside of 
     * a subsystem
     * 
     * TODO: (TF:687) revise necessity of this attribute! It is used only to 
     * determine whether block input variable is required inside of a block 
     * or not in case of normal subsystems, the same information can be obtained 
     * by checking (sourceBlock != null). It may be still required in case of other 
	 * blocks. Consider using a method that gets the required information from
	 * model without the need for additional flag or at least rename the flag
     * The current name is misleading (ToNa 24/11/09)  
     */
	protected boolean relatedToInportBlock;
	
	public Inport(){
		super();
	}

	public boolean isRelatedToInportBlock() {
		return relatedToInportBlock;
	}

	public void setRelatedToInportBlock(boolean relatedToInportBlock) {
		this.relatedToInportBlock = relatedToInportBlock;
	}

    public GADataType getDataType() {
        return dataType;
    }

    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    public void setDataType(GADataType dataType) {
        if (dataType != null){
            if (dataType.getParent() == null) {
                this.dataType = dataType;
            } else {
                this.dataType = dataType.getCopy();
            }
            this.dataType.setParent(this);
        }
        else
            this.dataType = null;
    }
    
	public List<Signal> getIncomingSignals() {
		List<Signal> signalList = new ArrayList<Signal>();
		// incoming signals are always in the parent of ports parent
		// and the port's parent can not be anything else than SystemBlock
		SystemBlock signalParent = (SystemBlock) getParent().getParent();
			
		if (signalParent == null)
			return signalList;
		
		for (Signal s : signalParent.getSignals()) {
			if (s.getDstPort() == this){
				signalList.add(s);
			}
		}
		
		return signalList;
	}

	@Override
	public String getShortReferenceString() {
		return super.getShortReferenceString() + "(" + getPortNumber() + ")";
	}
}