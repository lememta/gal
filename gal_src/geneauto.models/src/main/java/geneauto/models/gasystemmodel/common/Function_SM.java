/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/Function_SM.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-11-29 03:13:58 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.FunctionArgument_CM;
import geneauto.models.gacodemodel.Function_CM;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.SequentialFunctionBody;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Function defined in the system model. The examples of function types include
 * graphical function and truth-table from state-flow and Embedded Matlab (EML)
 * functions that can be defined both in Stateflow or Simulink. Functions from
 * other sources (SCICOS) can be also uses, assuming that the function body in the
 * source model can be converted to one of following representations:
 * <ul>
 * 	<li>graphical function or truth table encoded using GASMLanguage </li>
 * 	<li>EML function encoded in GACodeModel language</li>
 * </ul>

 */
public abstract class Function_SM extends GASystemModelElement implements
    SystemFunction, Node {
	
	/**
	 * Collection to store local variables of this function
	 */
	protected List<Variable_SM> variables = new ArrayList<Variable_SM>();
	
	/**
	 * Determines if function is visible outside of module scope (see enumerator
	 * FunctionScope)
	 */
	protected FunctionScope scope = FunctionScope.LOCAL_FUNCTION;

	/**
	 * List of function arguments. The list may be empty in case the function does not have any arguments.
	 */
	protected List<FunctionArgument_SM> arguments = new ArrayList<FunctionArgument_SM>();

	/**
	 * return data type of the function
	 */
	protected GADataType dataType;
	
    /**
     * expression template that can be used for composing expressions referring to this variable
     * This is a CallExpression with no arguments. The caller should make a copy of this
     * expression and supply correct arguments.     
     */
    protected Expression referenceExpression;
    
	public Function_SM() {
		super();
	}
	
	public Function_SM(List<Variable_SM> variables, FunctionScope scope) {
		this();
		setVariables(variables);
		this.scope = scope;
	}

	public List<FunctionArgument_SM> getArguments() {
		return arguments;
	}

	public void setArguments(List<FunctionArgument_SM> arguments) {
	    this.arguments.clear();
	    if (arguments != null) {
	        for (FunctionArgument_SM fa : arguments) {
	            addArgument(fa);
	        }	        
	    }
	}

	public void addArgument(FunctionArgument_SM argument){
		argument.setParent(this);
		arguments.add(argument);
	}
	
	public GADataType getDataType() {
		return dataType;
	}

    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    public void setDataType(GADataType dataType) {
        if (dataType != null){
            if (dataType.getParent() == null) {
                this.dataType = dataType;
            } else {
                this.dataType = dataType.getCopy();
            }
            this.dataType.setParent(this);
        }
        else
            this.dataType = null;
    }
    
	public List<Variable_SM> getVariables() {
		return variables;
	}

	public void addVariable(Variable_SM v){
		v.setParent(this);
		variables.add(v);
	}
	
	public void setScope(FunctionScope scope) {
		this.scope = scope;
	}

	public FunctionScope getScope() {
		return scope;
	}
	
	/**
	 * @return an output variable, if defined.
	 */
	public Variable_SM getOutputVariable() {
		if (variables != null) {		
			for (Variable_SM v : variables) {
				if (v.getScope() == VariableScope.FUNCTION_OUTPUT_VARIABLE) return v;
			}
		}
		return null;
	}

	public void setVariables(List<Variable_SM> variables) {
		for(Variable_SM v : variables) {
			addVariable(v);
		}
	}
		
    public Expression getReferenceExpression() {
        return referenceExpression;
    }

    public void setReferenceExpression(Expression referenceExpression) {
        this.referenceExpression = referenceExpression;
    }

	/**
	 * Converts a SystemModel Function to a CodeModel Function
	 * The Function has a body of type SequentialFunctionBody
	 * and an empty list of Statements.
	 * 
	 * The generated function is placed in the supplied NameSpace.
	 */
	public Function_CM toCodeModel(NameSpace nameSpace) {
	    
		if (nameSpace == null){
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		"Function.toCodeModel", 
            		"",
                    "Empty module argument! Generated function can be stored only in an existing module. ("
                            + this.getReferenceString() + ")", 
                    "");
            return null;
		}
		
        Function_CM cmElt = new Function_CM();

        // Register the Function in the supplied NameSpace (assigns also the ID)
        nameSpace.addElement(cmElt);        
        
		// Copy inherited fields
        copyCommonAttributes(cmElt);
		
		// Copy fields specific to this class
		cmElt.setDataType(		getDataType());
		cmElt.getDataType().toCodeModel();
		cmElt.setScope(			getScope().toCodeModel());
		
		// Arguments
		List<FunctionArgument_CM> cmArgs = new ArrayList<FunctionArgument_CM>();
		if (getArguments() != null) {
			for (FunctionArgument_SM arg : getArguments()) {
				cmArgs.add(arg.toCodeModelVariable(null)); // Does not add the Argument to the Function
			}
		}
		cmElt.setArguments(		cmArgs);
		
        // Local NameSpace
        NameSpace nsLocal = new NameSpace();
        // Bind the NameSpace object to the CodeModel, but do not place it there yet
        nsLocal.setModel(getModel().getCodeModel());
        
		// Convert local variables
		if (getVariables() != null) {
			for (Variable_SM v : getVariables()) {
				v.toCodeModelVariable(nsLocal); // Also adds the variable to the NameSpace
			}
		}
		
		// Function body (a stub with an empty list of Statements)
		SequentialFunctionBody fb = 
			new SequentialFunctionBody(nsLocal, new ArrayList<Statement>());
		cmElt.setBody(fb);
		
        // Compile reference expression template 
		// A CallExpression with no Arguments. The caller should make a copy of this
		// Expression and supply correct Arguments.		
        CallExpression cmEltRefExpr = new CallExpression(cmElt,
                new LinkedList<Expression>());
        getModel().getCodeModel().addTempElement(cmEltRefExpr);
        setReferenceExpression(cmEltRefExpr);
		
		// Add a pointer in the SystemModel element to the CodeModel element
 		this.setCodeModelElement(cmElt); 		
		// Add a CodeModel-SystemModel pointer
		cmElt.setSourceElement(this);
		
		return cmElt;
	}
	
	public Function_CM getCodeModelElement(){
		GAModelElement f = super.getCodeModelElement();
		if (f instanceof Function_CM){
			return (Function_CM) f;
		}
		else
			return null;
	}

    @Override
    public List<Node> getPath(PathOption pathOption) {
        List<Node> path;
        if (getParent() instanceof Node) {
            path = ((Node) getParent()).getPath(pathOption);
        } else {
            path = new LinkedList<Node>();
        }            
        path.add(this);
        return path;
    }
    
    @Override
    public String getPathString(String separator, PathOption pathOption) {
        List<Node> path = getPath(pathOption);
        int n = path.size();
        int i = 0;
        String pathString = new String();
        for (Node node : path) {
            pathString += node.getName();
            if (++i < n) {
                pathString += separator;
            }
        }
        return pathString;
    }

	/**
	 * This overriding compensates the fact the Function's NameSpace is not
	 * directly in the NameSpace hierarchy.
	 */
	@Override
    public List<GAModelElement> getReferableChildren() {
        List<GAModelElement> lst = super.getReferableChildren();
        lst.addAll(getArguments());
        return lst;
    }
}