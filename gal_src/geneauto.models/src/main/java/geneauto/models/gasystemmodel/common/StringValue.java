/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/StringValue.java,v $
 *  @version	$Revision: 1.21 $
 *	@date		$Date: 2010-11-11 14:50:05 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TString;
import geneauto.models.utilities.emlconverter.EMLAccessor;

/**
 * Parameter value is given as a string
 */
public class StringValue extends DataValue {

    protected String value;

    public StringValue() {
        super();
    }

    @Override
    public GADataType getDataType() {
        return new TString();
    }

    public StringValue(String value) {
        super();
        this.value = value;
    }

    public String getStringValue() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String pvalue) {
        value = pvalue;
    }

    /**
     * Converts the StringValue to a CodeModel expression.
     * NOTE! If the parameter value is known to be of type string, then create 
     * a StringExpression using the value instead of calling this method! 
     */
    @Override
   public Expression getExpression() {
    	return EMLAccessor.convertEMLToCodeModel(
                value, false);
    }

}