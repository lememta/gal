/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/VariableScope.java,v $
 *  @version	$Revision: 1.14 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;

/**
 * Scope of a variable
 * TODO Maybe convert to a class. There is infact some hierarchy of scopes. 
 * e.g. FunctionOutputVariable and LocalVariable
 */
public enum VariableScope {

	/**
	 * The variable is local to the object that owns it.
	 */
	LOCAL_VARIABLE,
	/**
	 * Input to a block/chart.
	 */
	INPUT_VARIABLE,
	/**
	 * Output from a block/chart.
	 */
	OUTPUT_VARIABLE,
	/**
	 * Function input (argument) variable.
	 */
	FUNCTION_INPUT_VARIABLE,
	/**
	 * A special case of a local variable. 
	 * The value of this variable is returned as the function's output.
	 */
	FUNCTION_OUTPUT_VARIABLE,
	/**
	 * The variable is imported to the object and can be refered to by it.
	 */
	IMPORTED_VARIABLE,
	/**
	 * The variable is defined in the object that owns it, but must be visible also
	 * from outside.
	 */
	EXPORTED_VARIABLE,
	/**
	 * An automatically generated variable for maintaining model elements state.
	 */
	STATE_VARIABLE;
	
	
	/**
	 * Converts a SystemModel VariableScope to a CodeModel equivalent
	 */
	public DefinitionScope toCodeModel() {
		switch (this) {
		case LOCAL_VARIABLE 			: return DefinitionScope.LOCAL;
		case INPUT_VARIABLE 			: return DefinitionScope.EXPORTED;
		case OUTPUT_VARIABLE			: return DefinitionScope.EXPORTED;
		case FUNCTION_INPUT_VARIABLE 	: return DefinitionScope.LOCAL;
		case FUNCTION_OUTPUT_VARIABLE 	: return DefinitionScope.FUNCTION_OUTPUT;
		case IMPORTED_VARIABLE 			: return DefinitionScope.IMPORTED;
		case EXPORTED_VARIABLE 			: return DefinitionScope.EXPORTED;
		default : 
			EventHandler.handle(EventLevel.ERROR, 
					"VariableScope.toCodeModel", "",
					"Unexpected enum value: " + this);
			return null;
		}
	}


}