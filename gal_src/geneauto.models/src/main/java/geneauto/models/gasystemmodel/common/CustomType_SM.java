/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/CustomType_SM.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2010-09-13 12:33:45 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.CustomTypeContent;
import geneauto.models.gacodemodel.CustomType_CM;
import geneauto.models.gacodemodel.Module;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.StructureContent;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomType_SM extends GASystemModelElement implements CustomType,
        NameSpaceElement_SM {

    protected CustomTypeContent content;

    public CustomType_SM() {
        super();
    }

    /**
     * 
     * @param content
     * @param scope
     */
    public CustomType_SM(CustomTypeContent content) {
        this();
        this.content = content;
    }

    public CustomTypeContent getContent() {
        return content;
    }

    public void setContent(CustomTypeContent content) {
        content.setParent(this);
        this.content = content;
    }

    /**
     * @return members of Custom type if contents is of StructureMemberContent
     *         type null - if not
     */
    public List<StructureMember> getMembers() {
        List<StructureMember> result = new ArrayList<StructureMember>();
        result.addAll(getContent().getStructMembers());
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }

    public StructureMember getMemberByName(String p_name) {
        return getContent().getMemberByName(p_name);
    }

	/** 
	 * This is not null, when a common initial value is specified for all
	 * elements of this type
	 */
    public Expression getInitialValue() {
    	return content.getInitialValue();
    }
    
    /**
     * Transfer SystemModel custom type to equivalent CodeModel custom type,
     * adding it in the nameSpace and updating cross references
     * 
     * @param namespace
     * @param nameMap
     * @return
     */
    public CustomType_CM toCodeModelCustomType(NameSpace namespace,
            Map<String, GAModelElement> nameMap) {

        CustomType_CM custom_cm = null;

        if (this.codeModelElement == null) {
            CustomTypeContent oldContent = getContent();
            CustomTypeContent newContent = (CustomTypeContent) getContent()
            .getCopy();
            StructureMember oldMember = null;
            CustomType oldCustomType = null;
            CustomType_CM newCustomType = null;
            TCustom newType = null;
            // in case the content is StructureContent check if any 
            // members refers to CustomType too
            /* TODO: (TF:614) this handles only the case when member is of type 
             * TCustom. In fact, the member may be also TPointer or TArray 
             * with base type of TCustom. Extend the approach 
             * (ToNa 04/08/09) */
            if (oldContent instanceof StructureContent) {
                for (int i=0; 
                i < oldContent.getStructMembers().size(); 
                i++) {
                    oldMember = oldContent.getStructMembers().get(i); 
                    if (oldMember.getDataType() instanceof TCustom) {
                        oldCustomType = ((TCustom) oldMember.getDataType())
                        .getCustomType();
                        if (oldCustomType instanceof CustomType_SM) {
                            // convert the type
                            newCustomType =  ((CustomType_SM) oldCustomType)
                            .toCodeModelCustomType(namespace, nameMap);
                            newType =  (TCustom) PrivilegedAccessor
                            .getObjectInPackage(TCustom.class, this);
                            ((TCustom)newType).setCustomType(newCustomType);
                            // change the type of the new member
                            newContent.getStructMembers()
                            .get(i)
                            .setDataType(newType);
                        }
                    }
                }
            }
            custom_cm = new CustomType_CM(newContent, 
                    DefinitionScope.EXPORTED);

            copyCommonAttributes(custom_cm);

            namespace.addElement(custom_cm);
        } else {
            custom_cm = (CustomType_CM) this.getCodeModelElement();
        }

        // make sure there is reference to code model element
        if (this.codeModelElement == null) {
            // set CodeModelElement attribute of CustomType_SM
            this.setCodeModelElement(custom_cm);
        }

        // update map for name references
        GAModelElement varInst = null;

        for (GAModelElement m : namespace.getModel().getAllElements(
                Module.class)) {
            varInst = ((Module) m).getNameSpace().getVariableByType(this);
            if (varInst != null) {
                break;
            }
        }

        // FIXME: it should be checked, that structure elements are not sought 
        // from this map any more and then remove this loop. Using member 
        // name is error prone -- member name alone may easily clash with other 
        // names. Hierarchical names depend on context and can not be put in map 
        // here. 
        for (StructureMember member : custom_cm.getContent().getStructMembers()) {
            nameMap.put(member.getName(), member);
        }

        return custom_cm;
    }

    @Override
    public boolean equalsTo(CustomType other) {
        if (!(other instanceof CustomType_SM)) {
            return false;
        }

        return content.equalsTo(((CustomType_SM) other).getContent());
    }

}
