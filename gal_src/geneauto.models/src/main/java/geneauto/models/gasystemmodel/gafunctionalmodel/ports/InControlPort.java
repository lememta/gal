/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/ports/InControlPort.java,v $
 *  @version	$Revision: 1.19 $
 *	@date		$Date: 2010-08-20 11:34:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.gastatemodel.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Input port accepting control signals. Control signal represents event-based
 * block activation in the GAFMLaguage.
 */
public class InControlPort extends Inport {
	
	/**
	 * Reference to stateflow event 
	 */
	protected Event event;
	/**
	 * Used only if "outputShown" property of Simulink model is set to "true".
	 */
	protected GADataType outputDataType;
	/**
	 * "true" if "Sample time type" property of Simulink model is set to "periodic",
	 * "false" otherwise.
	 */
	protected boolean periodicSampleTime;
	/**
	 * "States when enabling" property of Simulink model tells whether the internals
	 * states of controlled system should  be initiated ("held" or "reset" or the
	 * property is "inherit"'ed). "true" if "States when enabling" property is "reset"
	 * or "reset is inherited", "false" otherwise.
	 */
	protected boolean resetStates;
	/**
	 * "Sample time" property of the Simulink model. Is significant if
	 * periodicSampleTime is "true".
	 */
	protected int sampleTime;
	
	/**
     * Statements that are to be executed from within the controlling block
     * when its OutControlPort is activated. Typically they
     * trigger the execution of a function call target system.
     * TODO separate namespace elements and statements into a Pair
     */
    private List<Statement> computeCode = new ArrayList<Statement>();
	
	public InControlPort(){
		super();
		relatedToInportBlock = false;
	}

    public GADataType getOutputDataType() {
        return outputDataType;
    }
    
    public void setOutputDataType(GADataType outputDataType) {
    	if (outputDataType != null){
    		this.outputDataType = outputDataType.getCopy();
    		this.outputDataType.setParent(this);
    	}
    	else{
    		this.outputDataType = null;
    	}
    			
    }

	public void setPeriodicSampleTime(boolean periodicSampleTime) {
		this.periodicSampleTime = periodicSampleTime;
	}

	public boolean isPeriodicSampleTime() {
		return periodicSampleTime;
	}

	public void setResetStates(boolean resetStates) {
		this.resetStates = resetStates;
	}

	public boolean isResetStates() {
		return resetStates;
	}

	public void setSampleTime(int sampleTime) {
		this.sampleTime = sampleTime;
	}

	public int getSampleTime() {
		return sampleTime;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Event getEvent() {
		return event;
	}

    public List<Statement> getComputeCode() {
        List<Statement> resultList = new ArrayList<Statement>();
        if (computeCode != null) {
            for (Statement el: computeCode) {
                resultList.add((Statement) el);
            }
        }
        return resultList;
    }

	public void setComputeCode(List<Statement> statements) {
        this.computeCode = new ArrayList<Statement>();
        for (Statement s : statements) {
        	addComputeCodeStatement(s);
        }
    }

    public void addComputeCodeStatement(Statement statement) {
        this.computeCode.add(statement);
    }

    public void addComputeCode(List<Statement> statements) {
        this.computeCode.addAll(statements);
    }

}