/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/GASystemModel.java,v $
 *  @version	$Revision: 1.21 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gacodemodel.GACodeModel;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.genericmodel.Model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * GASystemModel language is the gene-auto language for encoding system models.
 */
public class GASystemModel extends Model {

	/**
	 * List of top level model elements (those appearing under the "model" node in XML
	 * file). The elements in this list must be unique -- no two elements with the
	 * same type and name attribute value should appear in the list. ModelFactory must
	 * guarantee, that in case two elements with the same name are found in the same
	 * model file an error is thrown. In case of to overlapping elements in different
	 * models the last one read overrides previous element and warning is issued
	 * 
	 * NOTE! When the type of this collection is changed at least the following other 
	 * methods need to be updated also: 
	 * 	addElement(Object), addElement(<ElementType>), 
	 *  addElements(..), setElements(..), getElements(..) 
	 */	
    protected List<GASystemModelRoot> elements = new ArrayList<GASystemModelRoot>();
    
	/**
	 * Reference to a code model derived from this system model
	 * 
	 * When system model contains GAModelElementReference type items, then they are
	 * assumed to refer to elements in this code model
	 */
	GACodeModel codeModel;	
	
	/*
	 * Reference to block library that can be used for getting block types in 
	 * current model. 
	 */
	GABlockLibrary blockLibrary;
		
	public GASystemModel(){
		super();
	}
	
    public List<GASystemModelRoot> getElements() {
        return elements;
    }
    
    public void setElements(List<?> elements) {
    	this.elements.clear();
    	for (Object o : elements){
    		addElement((GASystemModelRoot) o);
    	}
    }
    
    /**
     * Adds model element to the elements list and sets parent of the 
     * element to null. 
     */
    public void addElement(GASystemModelRoot e) {
    	e.setParent(null);
    	e.setModel(this);
    	getElements().add(e);
    }
    
	/**
	 * Generic addElement method overridden from the Model class.
	 * NOTE! This method must call the addElement method specific to this Model.
	 * If the type in the cast is wrong it will go to endless loop, as it will
	 * match the same method!
	 */
	@Override
    public void addElement(Object o) {
    	addElement((GASystemModelRoot) o);    	
    }

	@Override
    public void addElements(List<?> p_elements) {
    	for (Object o : p_elements){
    		addElement((GASystemModelRoot) o);
    	}
    }

    /**
     * Return all top-level SystemBlocks in the SystemModel. 
     */
	public List<SystemBlock> getTopLevelSystemBlocks() {
    	List<SystemBlock> systemBlocks = new LinkedList<SystemBlock>();
    	if (getElements() != null) {
    		for (GASystemModelRoot elt : getElements()) {
    			if (elt instanceof SystemBlock) { 
    				systemBlocks.add((SystemBlock)elt);
    			}
    		}
    	}
    	return systemBlocks;		
	}	

	/**
	 * Returns root element of system model. Stops the tool if there is 
	 * no root, more than one root is detected or the root element 
	 * is not SystemBlock
	 * 
	 * @return SystemBlock
	 */
    public SystemBlock getRootSystemBlock() {
    	//Assume that he first element is always the root level System Block
    	if (elements.size() == 0){
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "getRootSystemBlock", 
                    "",
                    "Empty model, no root element",
                    "");    		
    	}
    	else if (elements.size() > 1){
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "getRootSystemBlock", 
                    "",
                    "Inconsistent model: more than one root element.",
                    "");    		    		
    	}
    	else if (elements.get(0) instanceof SystemBlock){
    		return (SystemBlock)elements.get(0);
    	}
    	else{
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "getRootSystemBlock", 
                    "",
                    "Inconsistent model: root element is of type " 
                    + elements.get(0).getClass().getName()
                    + ", expected type SystemBlock",
                    "");    		    		
    	}

    	// if we reach here there was an error
    	return null;
    }

    public GACodeModel getCodeModel() {
		return codeModel;
	}

	public void setCodeModel(GACodeModel codeModel) {
		this.codeModel = codeModel;
		
		// the reference between two models must be mutual
		if (codeModel.getSystemModel() != this){
			codeModel.setSystemModel(this);
		}
	}
	
	/*
	 * @return code model derived from this system model
	 */
	public Model getDependentModel(){
		return getCodeModel();
	}
    
    /**
     * When codeModel attribute is null creates new code model and copies 
     * transition history from system model there.
     * 
     * @return reference to code model (codeModel attribute value)
     */
	public GACodeModel initCodeModel(){
		if (codeModel == null) {
			// construct new code model
			codeModel = new GACodeModel();
			
	        codeModel.getTransformations().addAll(this.getTransformations());
	        codeModel.setModelName(this.getModelName());
	        codeModel.setModelVersion(this.getModelVersion());
	        
	        // make sure the two models have mutual references
	        codeModel.setSystemModel(this);
		}
		
		return codeModel;
	}
	
	public GABlockLibrary getBlockLibrary() {
		return blockLibrary;
	}

	public void setBlockLibrary(GABlockLibrary blockLibrary) {
		this.blockLibrary = blockLibrary;
	}
}