/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/Node.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.models.genericmodel.GANamed;

import java.util.List;


/**
 * Interface for GASystemModel elements that can be viewed as logical nodes in
 * the model. These are elements like Block, Chart, State, Variable, etc. But
 * not elements like Signal, Transition, Composition etc.
 * 
 * Currently, Nodes are used for managing path strings and resolving qualified
 * references in the StateModel. Further uses can be added.
 * 
 * TODO: Perhaps also a method getParentNode() would be useful? 
 * Decide if it should be also generalised to CodeModel (AnTo)
 * 
 */
public interface Node extends GANamed {

    /**
     * @param pathOption
     *            - option specifying the starting point of the path (root)
     * @return a list of nodes, starting from the root node specified, by the
     *         pathOption and ending with the current Node.
     */
    public List<Node> getPath(PathOption pathOption);

    /**
     * @param separator
     *            - path separator string
     * @param pathOption
     *            - option specifying the starting point of the path (root)
     * @return path string (the names of nodes in the path separated, with the
     *         given separator)
     */
    public String getPathString(String separator, PathOption pathOption);

}
