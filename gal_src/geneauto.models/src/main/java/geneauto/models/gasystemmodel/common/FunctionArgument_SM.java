/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/FunctionArgument_SM.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.FunctionArgument_CM;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.genericmodel.GAModelElement;

/**
 * Function argument object.
 */
public class FunctionArgument_SM extends Variable_SM {

    /**
     * Typically the arguments defined explicitly by the user are not
     * optimizable. Automatically generated function arguments can become
     * redundant, although this is not known, when the function signature is
     * generated.
     */
    protected boolean isRedundant;

    // Constructor
    public FunctionArgument_SM() {
        super();
        this.scope = VariableScope.FUNCTION_INPUT_VARIABLE;
    }

    /**
     * Constructor for convenience
     * 
     * @param GAModelElement
     *            parent
     */
    public FunctionArgument_SM(GAModelElement parent) {
        super();
        this.scope = VariableScope.FUNCTION_INPUT_VARIABLE;
        setParent(parent);
    }

    /**
     * Constructor for convenience
     * 
     * @param DataValue
     *            initialValue
     */
    public FunctionArgument_SM(Expression initialValue) {
        super();
        this.scope = VariableScope.FUNCTION_INPUT_VARIABLE;
        this.initialValue = initialValue;
    }

    /**
     * Constructor for convenience
     * 
     * @param Variable_SM
     *            v
     */
    public FunctionArgument_SM(Variable_SM v) {
        super();
        // Copy generic attributes
        v.copyCommonAttributes(this);
        // Copy attributes specific to variables
        setDataType(v.getDataType().getCopy());
        this.isOptimizable = v.isOptimizable();
        // Set scope
        this.scope = VariableScope.FUNCTION_INPUT_VARIABLE;
    }

    public boolean isRedundant() {
        return isRedundant;
    }

    public void setRedundant(boolean isRedundant) {
        this.isRedundant = isRedundant;
    }

    /**
     * Converts a SystemModel FunctionArgument to a CodeModel FunctionArgument
     * 
     * Note: The nameSpace argument is not meaningful for function arguments and
     * must be null!
     */
    @Override
    public FunctionArgument_CM toCodeModelVariable(NameSpace nameSpace) {

        if (nameSpace != null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "FunctionArgument_CM.toCodeModel", "",
                    "nameSpace argument must be null", "");
            return null;
        }

        FunctionArgument_CM cmElt = new FunctionArgument_CM();

        // Explicitly initialise the model reference. We know that this element
        // will be stored in the CodeModel. This assigns also the ID.
        cmElt.setModel(getModel().getCodeModel());

        // Copy inherited fields
        copyCommonAttributes(cmElt);
        cmElt.setDataType(getDataType());
        cmElt.getDataType().toCodeModel();
        cmElt.setScope(getScope().toCodeModel());
        cmElt.setConst(isConst());
        cmElt.setStatic(isStatic());
        cmElt.setVolatile(isVolatile());
        cmElt.setOptimizable(isOptimizable());
        if (getInitialValue() != null) {
            cmElt.setInitialValue(getInitialValue().getCopy()
                    .toCodeModel());
        }

        // Copy fields specific to this class
        cmElt.setRedundant(isRedundant());

        // Compile reference expression template
        VariableExpression cmEltRefExpr = new VariableExpression(cmElt);
        getModel().getCodeModel().addTempElement(cmEltRefExpr);
        setReferenceExpression(cmEltRefExpr);

        // Add a SystemModel-CodeModel pointer
        this.setCodeModelElement(cmElt);
        // Add a CodeModel-SystemModel pointer
        cmElt.setSourceElement(this);

        return cmElt;
    }

}