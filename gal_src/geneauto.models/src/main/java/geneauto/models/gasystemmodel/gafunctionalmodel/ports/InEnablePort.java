/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/ports/InEnablePort.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.models.gadatatypes.GADataType;

/**
 * Enable port is a port that determines based on the input data whether block
 * can execute on a given clock tick or not. It can be viewed as a logical
 * condition controlling block execution. From the viewpoint of data exchange,
 * the enable port has dual behaviour. First, it checks the condition and
 * determines if block can execute. In addition, the port can optionally pass
 * also the incoming data value for internal processing in the block if
 * activation condition is satisfied. The outputShown parameter is handled by
 * the existence of corresponding input block inside the system.
 */
public class InEnablePort extends Inport {
	
	/**
	 * Data type of the event value exposed to the blocks inside of subsystem
	 */
	protected GADataType outputDataType;

	/**
	 * "States when enabling" property of Simulink model tells whether the
	 * internals states of controlled system should be initiated ("held" or
	 * "reset"). "true" if "States when enabling" property is "reset", "false"
	 * otherwise.
	 */
	protected boolean resetStates;

	public InEnablePort() {
		super();
		relatedToInportBlock = false;
	}

	public GADataType getOutputDataType() {
		return outputDataType;
	}

    public void setOutputDataType(GADataType outputDataType) {
    	if (outputDataType != null){
    		this.outputDataType = outputDataType.getCopy();
    		this.outputDataType.setParent(this);
    	}
    	else{
    		this.outputDataType = null;
    	}
    			
    }

	public boolean isResetStates() {
		return resetStates;
	}

	public void setResetStates(boolean resetStates) {
		this.resetStates = resetStates;
	}

}