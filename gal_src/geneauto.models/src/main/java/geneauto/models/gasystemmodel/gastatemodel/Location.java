/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/Location.java,v $
 *  @version	$Revision: 1.27 $
 *	@date		$Date: 2011-11-28 22:44:09 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.common.VariableScope;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.LinkedList;
import java.util.List;

/**
 * A graph node in state model -- generalises all types of start and end nodes
 * for transitions.
 */
public abstract class Location extends GASystemModelElement {

    /**
     * A variable that records the open/closed status of the Location
     */
    protected Variable_SM stateVariable;

    public Location() {
        super();
    }

    /**
     * Abstract evaluation of a transition destination. Looks at the current
     * location and decides, what to do next
     * 
     * @return the code evaluating the transition destination
     */
    public abstract List<Statement> evalAbsDestination(
            EvaluationContext ctxt,
            SuccessContinuation success, 
            FailureContinuation fail);

    public Variable_SM getStateVariable() {
        return stateVariable;
    }

    public void setStateVariable(Variable_SM stateVariable) {
        this.stateVariable = stateVariable;
        if (this.stateVariable != null) {
            this.stateVariable.setParent(this);
        }
    }

    public void createStateVariable() {
        Variable_SM v = new Variable_SM();
        setStateVariable(v);
        v.setDataType(new TBoolean());
        v.setScope(VariableScope.STATE_VARIABLE);
        v.setInitialValue(new FalseExpression());
        v.setName("OPEN"); // A prefix containing parent's name will be added at
                           // a later stage.
        if (this instanceof ChartRoot) {
            // The annotations in the ChartRoot are generated from
            // the Annotation objects in the Stateflow diagram.
            // Don't add them here.
        } else {
            v.addAnnotations(getAnnotations());
        }
    }

    /**
     * A logical parent of a Location is either another Location or null, when
     * the Location is the ChartRoot.
     * 
     * @return The parent Location
     */
    public Location getParentLocation() {
        // RootLocation
        if (this instanceof ChartRoot) {
            return null;
        }
        // Some State below the RootLocation
        GAModelElement elt = getParent();
        if (elt instanceof Composition) {
            GAModelElement elt2 = elt.getParent();
            if (elt2 instanceof Location) {
                return (Location) elt2;
            } else if (elt2 instanceof ChartBlock) {
                return ((ChartBlock) elt2).getRootLocation();
            } else {
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "Location.getParentLocation", "",
                        "Unexpected grandparent. Expecting a Location.\nThis: "
                                + getReferenceString() + "\nGrandparent: "
                                + elt2.getReferenceString(), "");
                return null;
            }
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "Location.getParentLocation", "",
                    "Unexpected parent. Expecting a Composition.\nThis: "
                            + getReferenceString() + "\nParent: "
                            + elt.getReferenceString(), "");
            return null;
        }
    }

    /**
     * @return Name of the Chart, when the Location is the RootLocation, or name
     *         of the State, prefixed with names of parent States separated with
     *         ".". The path part does not contain the Chart's name.
     */
    public abstract String getQualifiedName();

    /**
     * Constructs a path from the ChartRoot to the current Location The returned
     * path contains the root location as the first element.
     * 
     * @return a list of Locations
     */
    public LinkedList<Location> getPath() {
        LinkedList<Location> lst;
        if (this instanceof ChartRoot) {
            lst = new LinkedList<Location>();
        } else {
            lst = getParentLocation().getPath();
        }
        lst.add(this);
        return lst;
    }

    /**
     * Constructs a path from a given ancestor Location to the current Location
     * that must be a descendant of the ancestor Location. The returned path
     * contains the ancestor as the first element and the starting location as 
     * the last element.
     * 
     * @param Ancestor
     *            Location
     * @param Starting
     *            Location
     * @return a list of Locations
     */
    public LinkedList<Location> getSubPath(Location ancestorLoc,
            Location startLoc) {
        // Reached the ancestor
        if (this == ancestorLoc) {
            LinkedList<Location> lst = new LinkedList<Location>();
            lst.add(this);
            return lst;
        }
        if (this instanceof ChartRoot) {
            // Reached the root, but didn't find the ancestor
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "Location.getSubPath", "", "Given location: "
                            + ancestorLoc.getReferenceString()
                            + " is not the ancestor of: "
                            + startLoc.getReferenceString(), "");
            return null;
        } else {
            // Continue upwards
            Location parentLoc = getParentLocation();
            LinkedList<Location> lst = parentLoc.getSubPath(ancestorLoc,
                    startLoc);
            lst.add(this);
            return lst;
        }
    }

    /**
     * @param Other
     *            location
     * @return The lowest common location in the paths of the current location
     *         and some given location.
     */
    public Location getLowestCommonLocation(Location otherLoc) {
        LinkedList<Location> path1 = this.getPath();
        LinkedList<Location> path2 = otherLoc.getPath();
        Location loc = path1.peek();
        while (!path1.isEmpty()) {
            Location l1, l2;
            l1 = path1.poll();
            if (path2.isEmpty()) {
                break;
            } else {
                l2 = path2.poll();
                if (l1 == l2) {
                    loc = l1;
                } else {
                    break;
                }
            }
        }
        return loc;
    }

    /**
     * Returns the variables that are local to the Location.
     * 
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use addVariable instead
     */
    public abstract List<Variable_SM> getVariables();

    /**
     * @return code for initialising local variables.
     */
    public List<Statement> absInitLocalVars() {
        LinkedList<Statement> stmts = new LinkedList<Statement>();
        if (getVariables() != null) {
            Statement s;
            for (Variable_SM v : getVariables()) {
                if (!v.isConst()) {
                    s = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, v
                            .getReferenceExpression(), v
                            .getInitialOrDefaultValue().getCopy());
                    stmts.add(s);
                }
            }
        }
        if (stmts.size() > 0) {
            stmts.addFirst(new BlankStatement("Initialize local variables"));
        }
        return stmts;
    }

}