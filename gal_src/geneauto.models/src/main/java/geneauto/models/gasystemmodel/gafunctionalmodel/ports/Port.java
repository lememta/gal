/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gafunctionalmodel/ports/Port.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2010-08-26 13:43:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;

/**
 * Abstract base class for all port types.
 */
public abstract class Port extends GASystemModelElement {
	
	/**
	 * reference to related block inside a subsystem
	 * in case of InDataPort this block is Inport, in case of
	 * OutDataPort Outport
	 * 
	 * The attribute has generic type Blocks because the source block 
	 * type may change depending on phase of transformation 
	 */
	protected Block sourceBlock;
	
	/**
	 * Port sequence number in the input model (simulink-specific)
	 */
	protected int portNumber;

    public int getPortNumber() {
        return portNumber;
    }

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	
	public Port(){
		super();
	}

	public Block getSourceBlock() {
		return sourceBlock;
	}

	public void setSourceBlock(Block sourceBlock) {
		// to avoid loops
		if (this.sourceBlock == sourceBlock){
			return;
		}
		
		// make sure there is no orphaned block port
		// reference in port
		if (this.sourceBlock != null){
			Block tmpBlock = this.sourceBlock;
			this.sourceBlock = null;
			tmpBlock.setPortReference(null);
		}
		
		this.sourceBlock = sourceBlock;
		
		// the reference between block and port is always symmetrical
		if (this.sourceBlock != null){
			this.sourceBlock.setPortReference(this);
		}
	}

	@Override
	public void breakReferences() {
		super.breakReferences();
		if (getSourceBlock() != null) {
			getSourceBlock().setPortReference(null);
			setSourceBlock(null);
		}
	}
	
	/** 
	 * returns the block that contains this port
	 * this method is a shorthand to simplify handling of 
	 * signals 
	 * 
	 * @return Block
	 */
	public Block getParentBlock() {
		return (Block) getParent();
	}
}