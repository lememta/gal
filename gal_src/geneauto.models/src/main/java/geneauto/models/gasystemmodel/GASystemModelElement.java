/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/GASystemModelElement.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2010-07-09 13:27:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel;

import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.models.genericmodel.GAModelElement;

/**
 * Abstract base class for all Gene-Auto System Model language elements
 */
public abstract class GASystemModelElement extends GAModelElement {

	/**
	 * The CodeModel element that is created from this SystemModel element 
	 * E.g. a CodeModel Variable
	 */
	protected GACodeModelElement codeModelElement;


	/**
	 * system model element can belong to system model only
	 * so we override the getter to cast the data type
	 * TODO (to AnTo) 0 Check - NOTE! This might not always be true (AnTo 100213)
	 */
	public GASystemModel getModel() {
		return (GASystemModel) model;
	}

	public void setModel(GASystemModel model) {
		super.setModel(model);
	}

	public GASystemModelElement() {
		super();
	}
	
	/**
	 * Returns pointer to an element in the dependent code model
	 * normally it is GACodeModelElement, however, as the code model
	 * also contains generic elements (Annotation, GADataType) we can not 
	 * guarantee it
	 */ 
	public GAModelElement getCodeModelElement() {
		return codeModelElement;
	}

	/**
	 * Sets reference to a code model element in the related model
	 */
	public void setCodeModelElement(GACodeModelElement sourceElement) {
		this.codeModelElement = sourceElement;
	}
	
	/*
	 * System model elements are referrable by default
	 * 
	 * (non-Javadoc)
	 * @see geneauto.models.genericmodel.GAModelElement#isReferable()
	 */
	public boolean isReferable() {
		return true;
	}

}