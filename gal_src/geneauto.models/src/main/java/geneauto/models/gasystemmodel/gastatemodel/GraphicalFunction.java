/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/GraphicalFunction.java,v $
 *  @version	$Revision: 1.28 $
 *	@date		$Date: 2010-05-04 09:54:08 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.statement.ReturnStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.common.Function_SM;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.LinkedList;
import java.util.List;

/**
 * A graphical function is a function defined graphically by a flow graph that
 * includes Stateflow action language. Contains junctions, transitions and EML
 * expressions.
 */
public class GraphicalFunction extends Function_SM {

	protected FlowGraphComposition composition;

	// Constructor
	public GraphicalFunction() {
		super();
	}

	// Constructor for convenience
	public GraphicalFunction(GAModelElement parent) {
		this();
		setParent(parent);
	}

	public void setComposition(FlowGraphComposition composition) {
		composition.setParent(this);
		this.composition = composition;
	}

	public FlowGraphComposition getComposition() {
		return composition;
	}
	
	/**
	 * Abstract evaluation of a graphical function.
	 * 
	 * The default path of the composition is traversed each time the 
	 * function is called. 
	 * 
	 * If an output variable is defined, then the value of the this 
	 * variable is returned as function output.  
	 * 
	 * @return the code for evaluating a graphical function
	 */
	public List<Statement> evalAbs() {
		List<Statement>   stmts = new LinkedList<Statement>();
		
		// Create EvaluationContext
        EvaluationContext ctxt = new EvaluationContext(null);
		
		// Evaluate the composition entry (the composition is stateless)
		if (composition != null) {
			stmts.addAll(composition.evalAbsEntry(ctxt));			
		}
		
        // Handle jumps generated from Junctions
		stmts = EvaluationContext.handleJunctionRefs(stmts, this);
        
		// Check, if an output variable is defined.
		// If it is, create a return statement.
		Variable_SM v = getOutputVariable();
		if (v != null) {
			ReturnStatement st = new ReturnStatement(
						new VariableExpression(v.getCodeModelElement()));
			stmts.add(st);			
		}
		
		return stmts;
	}

}