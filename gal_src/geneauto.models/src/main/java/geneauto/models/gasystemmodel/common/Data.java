/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/Data.java,v $
 *  @version	$Revision: 1.19 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.GASystemModelElement;

/**
 * Abstract base class for all data elements
 */
public class Data extends GASystemModelElement {

	/**
	 * Reflects how the data element should be treated: global, local etc.
	 * TODO: decide if this is string or specific enumerator!!
	 */
	protected String storageClass;

	/**
	 * Data type of the data element
	 */
	protected GADataType dataType;

	public Data() {
		super();
	}

	public Data(String storageClass, GADataType dataType) {
		this();
		this.storageClass = storageClass;
		setDataType (dataType);
	}

	public GADataType getDataType() {
		return dataType;
	}

    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    public void setDataType(GADataType dataType) {
        if (dataType != null){
            if (dataType.getParent() == null) {
                this.dataType = dataType;
            } else {
                this.dataType = dataType.getCopy();
            }
            this.dataType.setParent(this);
        }
        else
            this.dataType = null;
    }
    
}