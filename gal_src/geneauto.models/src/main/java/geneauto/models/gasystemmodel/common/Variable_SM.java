/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/Variable_SM.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.NamedConstant;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gasystemmodel.gastatemodel.State;
import geneauto.models.genericmodel.GAModelElement;

import java.util.LinkedList;
import java.util.List;

/**
 * Variable object for storing values of data elements.
 */
public class Variable_SM extends Data implements SystemVariable, Node {
	
    /**
     * Initial value of the variable
     */
    protected Expression initialValue;

    /**
     * Scope of the variable. Determines the role and visibility of the variable
     * in system.
     * 
     * TODO: Refine the scopes to be used in the GASystemModel Stateflow
     * specific scopes in the Stateflow tool and GA v.1: SFLocalData |
     * SFInputData | SFOutputData | SFParamData | SFConstData | SFFunInputData |
     * SFFunOutputData | SFTempData | SFDataStoreData | SFExportedData |
     * SFImportedData Some or all of these could be omitted, if the scope will
     * be determined by other relations (e.g. relations to the ports, function.
     * etc,) - initially needed. Keep for double-checking?
     */
    protected VariableScope scope;
    protected boolean isStatic;
    protected boolean isConst;
    protected boolean isOptimizable;
    protected boolean isVolatile;

    /**
     * Expression template that can be used for composing expressions referring
     * to this variable
     */
    protected Expression referenceExpression;

    /**
     * Some objects in the StateModel can bind variables and events. Binding
     * means that no other object except the binder or its children can modify
     * the bound object. Here we have reference to the state that binds the
     * current object
     */
    protected State binder;

    public void setBinder(State binder) {
        this.binder = binder;
    }

    // Constructor
    public Variable_SM() {
        super();
    }

    // Constructor for convenience
    public Variable_SM(GAModelElement parent) {
        this();
        setParent(parent);
    }

    public Variable_SM(String name, Expression initialValue, VariableScope scope) {
        this();
        setName(name);
        setInitialValue(initialValue);
        setScope(scope);
    }

    public Variable_SM(String name, VariableScope scope) {
        this();
        setName(name);
        setScope(scope);
    }

    /**
     * Method getBinder.
     * 
     * @return the attribute "binder".
     */

    public State getBinder() {
        return binder;
    }

    public Expression getInitialValue() {
        return initialValue;
    }

	@Override
	public Expression getInitialValueExpression() {
		return getInitialValue();
	}    
    /**
     * @return the initial value, if specified. Otherwise the default initial
     *         value of the datatype of the variable.
     */
    public Expression getInitialOrDefaultValue() {
        if (getInitialValue() != null) {
            return getInitialValue();
        } else if (getDataType() != null) {
            return getDataType().getDefaultValue();
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "Variable.getInitialOrDefaultValue()", "",
                    "Either \"initialValue\" or \"dataType\" field must be not null. Element: "
                            + getReferenceString());
            return null;
        }
    }

    /**
     * Method isConst.
     * 
     * @return the attribute "isConst".
     */
    public boolean isConst() {
        return isConst;
    }

    /**
     * Method isOptimizable.
     * 
     * @return the attribute "isOptimizable".
     */
    public boolean isOptimizable() {
        return isOptimizable;
    }

    /**
     * Method isStatic.
     * 
     * @return the attribute "isStatic".
     */
    public boolean isStatic() {
        return isStatic;
    }

    /**
     * Method isVolatile.
     * 
     * @return the attribute "isVolatile".
     */
    public boolean isVolatile() {
        return isVolatile;
    }

    public VariableScope getScope() {
        return scope;
    }

    /**
     * Makes a copy of the input argument and associates it with the current
     * element
     * 
     * @param initialValue
     */
    public void setInitialValue(Expression initialValue) {
        if (initialValue == null) {
            this.initialValue = null;
        } else {
            this.initialValue = (Expression) initialValue.getCopy();
            this.initialValue.setParent(this);
        }
    }

	@Override
    public void setInitialValueExpression(Expression exp) {
        setInitialValue(exp);
    }
    
    public void setScope(VariableScope scope) {
        this.scope = scope;
    }

    public void setStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public void setConst(boolean isConst) {
        this.isConst = isConst;
    }

    public void setOptimizable(boolean isOptimizable) {
        this.isOptimizable = isOptimizable;
    }

    public void setVolatile(boolean isVolatile) {
        this.isVolatile = isVolatile;
    }

    /**
     * Converts a SystemModel Variable to a CodeModel Variable.
     * 
     * Note: toCodeModelVariable, toStructureMember and toNamedConst are
     * mutually exclusive -- they are never called on the same object
     * 
     * @param nameSpace
     *            When this is null, we add it to TempModel, otherwise to given
     *            NameSpace
     * @param asNamedConst
     *            A named constant is created instead of a variable.
     *            NamedConstant is a sub-class of Variable. However, there exist
     *            also constant Variables in the CodeModel that are real
     *            Variables (with isConst=true) and are not named constants.
     */
    private Variable_CM toCodeModelVariable(NameSpace nameSpace,
            boolean asNamedConst) {

        Variable_CM cmElt = getCodeModelElement();
        
        // the variable is already converted to code model element
        if (cmElt != null) {
        	return cmElt;
        }
        
        if (asNamedConst) {
            cmElt = new NamedConstant();
        } else {
            cmElt = new Variable_CM();
        }

        // Initialise the model reference
        // either add the variable to the nameSpace
        // or to the TempModel
        // Both assign also the ID.
        if (nameSpace == null && getModel().getCodeModel() != null) {
            getModel().getCodeModel().addTempElement(cmElt);
        } else {
            nameSpace.addElement(cmElt);
        }

        // Copy inherited fields
        copyCommonAttributes(cmElt);

        // Copy fields specific to this class
        cmElt.setDataType(getDataType().toCodeModelElement());        
        cmElt.getDataType().toCodeModel();
        if (getScope() != null) {
            cmElt.setScope(getScope().toCodeModel());
        }
        cmElt.setConst(isConst());
        cmElt.setStatic(isStatic());
        cmElt.setVolatile(isVolatile());
        cmElt.setOptimizable(isOptimizable());

        // transfer the initial value
        if (getInitialValue() != null) {
            cmElt.setInitialValue(getInitialValue().getCopy()
                    .toCodeModel());
        }
        if (getInitialValueExpression() != null) {
        	cmElt.setInitialValueExpression(
        		getInitialValueExpression().getCopy().toCodeModel());
        }

        // Compile reference expression template
        VariableExpression cmEltRefExpr = new VariableExpression(cmElt);
        getModel().getCodeModel().addTempElement(cmEltRefExpr);
        setReferenceExpression(cmEltRefExpr);

        // Add a SystemModel-CodeModel pointer
        this.setCodeModelElement(cmElt);
        // Add a CodeModel-SystemModel pointer
        cmElt.setSourceElement(this);

        return cmElt;
    }

    /**
     * Converts a SystemModel Variable to a CodeModel Variable.
     * 
     * Note: toCodeModelVariable, toStructureMember and toNamedConst are
     * mutually exclusive -- they are never called on the same object
     * 
     * @param nameSpace
     *            When this is null, we add it to TempModel, otherwise to given
     *            NameSpace
     */
    public Variable_CM toCodeModelVariable(NameSpace nameSpace) {
        return toCodeModelVariable(nameSpace, false);
    }

    /**
     * Converts a SystemModel Variable to a CodeModel NamedConstant.
     * 
     * Note: toCodeModelVariable, toStructureMember and toNamedConst are
     * mutually exclusive -- they are never called on the same object
     * 
     * @param nameSpace
     *            When this is null, we add it to TempModel, otherwise to given
     *            NameSpace
     */
    public NamedConstant toNamedConstant(NameSpace nameSpace) {
        return (NamedConstant) toCodeModelVariable(nameSpace, true);
    }

    /**
     * Converts the parts can be converted to a CodeModel StructureMember (it
     * makes the structure member element in data type)
     * 
     * toCodeModel and toStructureMember are mutually exclusive -- they are
     * never called on the same object
     */
    public StructureMember toStructureMember(Expression structExp) {
        StructureMember cmElt = new StructureMember();

        // Copy inherited fields
        copyCommonAttributes(cmElt);

        // Copy fields specific to this class
        cmElt.setDataType(getDataType());

        // Compile reference expression template
        // structExp and cmElt must be in the same model
        // this method also assigns ID
        cmElt.setModel(structExp.getModel());
        MemberExpression cmEltRefExpr = new MemberExpression(cmElt, structExp
                .getCopy());
        getModel().getCodeModel().addTempElement(cmEltRefExpr);
        setReferenceExpression(cmEltRefExpr);

        // Add a SystemModel-CodeModel pointer
        this.setCodeModelElement(cmElt);
        // Add a CodeModel-SystemModel pointer
        cmElt.setSourceElement(this);

        return cmElt;
    }

    public Expression getReferenceExpression() {
        return referenceExpression;
    }

    public void setReferenceExpression(Expression referenceExpression) {
        this.referenceExpression = referenceExpression;
    }

    /*
     * Override to check datatype
     * 
     * (non-Javadoc)
     * 
     * @see
     * geneauto.models.gasystemmodel.GASystemModelElement#getCodeModelElement()
     */
    public Variable_CM getCodeModelElement() {
        GAModelElement el = super.getCodeModelElement();
        if (el instanceof Variable_CM) {
            return (Variable_CM) el;
        } else {
            return null;
        }
    }

    @Override
    public List<Node> getPath(PathOption pathOption) {
        List<Node> path;
        if (getParent() instanceof Node) {
            path = ((Node) getParent()).getPath(pathOption);
        } else {
            path = new LinkedList<Node>();
        }            
        path.add(this);
        return path;
    }
    
    @Override
    public String getPathString(String separator, PathOption pathOption) {
        List<Node> path = getPath(pathOption);
        int n = path.size();
        int i = 0;
        String pathString = new String();
        for (Node node : path) {
            pathString += node.getName();
            if (++i < n) {
                pathString += separator;
            }
        }
        return pathString;
    }
    
    @Override
    public void normalizeType(boolean full) {        

        // Update initial value of the variable
        Expression initValExp = getInitialValueExpression(); 
        if (initValExp != null) {
            initValExp = initValExp.normalizeShape(true);
        }        
        setInitialValueExpression(initValExp);
        
        // Update type of the variable
        setDataType(getDataType().normalize(true));

// TODO (to AnTo) Enable this, after either the preprocessor handles 
// normalisation or typer can access CodeModel
        
//        // Update CodeModelElement, if exists
//        if (getCodeModelElement() != null) {
//            ((IsVariable) getCodeModelElement()).normalizeType(full);
//        }
//
//        // Update ReferenceExpression, if exists
//        if (getReferenceExpression() != null) {
//            setReferenceExpression(getReferenceExpression().normalizeType(full));
//        }
    }

}