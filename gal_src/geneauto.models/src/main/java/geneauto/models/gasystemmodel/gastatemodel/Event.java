/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/Event.java,v $
 *  @version	$Revision: 1.44 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.NameSpaceElement;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.NamedConstant;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.common.Node;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.models.genericmodel.GAModelElement;

import java.util.LinkedList;
import java.util.List;

/**
 * Event object.
 */
public class Event extends GASystemModelElement 
    implements NameSpaceElement, Node, Comparable<Event> 
    {
	/**
	 * Input, Output, Local - It can be determined also by the related port, but if
	 * semantic checks and evaluation is done without involving the port objects, then
	 * it is needed.
	 */
	protected EventScope scope;
	
	/**
	 * pointer to output control port related to this event
	 * TODO check whether this attribute is needed (if it is add more specific description)
	 */
	protected OutControlPort outControlPort;
	
	/**
	 * Some objects in the StateModel can bind variables and events. Binding means
	 * that no other object except the binder or its children can modify the bound
	 * object. Here we have reference to the state that binds the current object
	 */
	protected State binder;

	/**
	 * An integer code that uniquely identifies the event in a given chart.
	 */
	protected int eventCode;

	/**
	 * expression template that can be used for composing expressions referring to this variable
	 */
	protected Expression referenceExpression;
	
	protected String triggerType;

	// Constructor
	public Event() {
		super();
	}

	public void setScope(EventScope scope) {
		this.scope = scope;
	}

	public EventScope getScope() {
		return scope;
	}

	public void setOutControlPort(OutControlPort outControlPort) {
		this.outControlPort = outControlPort;
	}

	public OutControlPort getOutControlPort() {
		return outControlPort;
	}

	public void setBinder(State binder) {
		this.binder = binder;
	}

	public State getBinder() {
		return binder;
	}	

    /**
	 * Converts a SystemModel Event to a CodeModel class
	 * and adds it to TempModel
	 * 
	 * If nameSpace argument is given, the constant is placed in this
	 * NameSpace. Otherwise it is put to TempModel 
	 */
	public NamedConstant toCodeModel(NameSpace nameSpace) {
		NamedConstant cmElt = new NamedConstant();
		
        // Register the CodeModelElement in the supplied NameSpace (assigns also
        // the ID)
        if (nameSpace == null || getModel().getCodeModel() == null){
            // add generated constant to TempModel
            getModel().getCodeModel().addTempElement(cmElt);
        }
        else{
            // add constant to given NameSpace
            nameSpace.addElement(cmElt);
        }
        
        // Copy inherited fields
        copyCommonAttributes(cmElt);
		
		// Copy/create fields specific to this class
		cmElt.setDataType(		new TRealInteger(16, false));
		cmElt.getDataType().toCodeModel();
		cmElt.setScope(		getScope().toCodeModel());
		cmElt.setInitialValue(	new IntegerExpression(getEventCode()).toCodeModel());
		
		// compile reference expression template
		VariableExpression cmEltRefExpr = new VariableExpression(cmElt);
		getModel().getCodeModel().addTempElement(cmEltRefExpr);
		setReferenceExpression(cmEltRefExpr);
		
		// Add a SystemModel-CodeModel pointer
		this.setCodeModelElement(cmElt);	// This might not be very meaningful
		// Add a CodeModel-SystemModel pointer
		cmElt.setSourceElement(this);
		
		return cmElt;
	}

	public int getEventCode() {
		return eventCode;
	}

	/**
	 * @return EventCode of the default event (tick): 0
	 */
	public static int getDefaultEventCode() {
		return 0;
	}

	/**
	 * @return First available user event code: 1. 
	 * 		   0 is reserved for the default event.
	 */
	public static int getFirstUserEventCode() {
		return getDefaultEventCode() + 1;
	}
	
	public void setEventCode(int eventCode) {
		this.eventCode = eventCode;
	}

	/*
	 * Override to check datatype
	 * 
	 * (non-Javadoc)
	 * @see geneauto.models.gasystemmodel.GASystemModelElement#getCodeModelElement()
	 */
	public NamedConstant getCodeModelElement(){
		GAModelElement el = super.getCodeModelElement();
		if (el instanceof NamedConstant){
			return (NamedConstant) el;
		}
		else{
			return null;
		}
	}

	public Expression getReferenceExpression() {
		return referenceExpression;
	}

	public void setReferenceExpression(Expression referenceExpression) {
		this.referenceExpression = referenceExpression;
	}
	
    public Expression getInitialValueExpression() {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "Event", "GE9998",
                "Call to an unsupported method: getInitialValueExpression. Element: "
                        + getReferenceString(), "");
        return null;
    }    
    
    public void setInitialValueExpression(Expression exp) {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "Event", "GE9998",
                "Call to an unsupported method: setInitialValueExpression. Element: "
                        + getReferenceString(), "");
    }    
    
    /**
     * @return the Location that owns this variable
     */
    public Location getParentLocation() {
        if (getParent() instanceof ChartBlock) {
            return ((ChartBlock) getParent()).getRootLocation();
        } else if (getParent() instanceof State) {
            return (State) getParent();
        } else {
            String msg = "Unexpected type of parent:" + "\nElement: "
                    + getReferenceString();
            if (getParent() != null) {
                msg += "\nParent type: " + getParent().getClass().getName();
            }
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "Event.getParentLocation()", "", msg,
                    "");
            return null;
        }
    }

    @Override
    public List<Node> getPath(PathOption pathOption) {
        List<Node> path;
        if (getParent() instanceof Node) {
            path = ((Node) getParent()).getPath(pathOption);
        } else {
            path = new LinkedList<Node>();
        }            
        path.add(this);
        return path;
    }
    
    @Override
    public String getPathString(String separator, PathOption pathOption) {
        List<Node> path = getPath(pathOption);
        int n = path.size();
        int i = 0;
        String pathString = new String();
        for (Node node : path) {
            pathString += node.getName();
            if (++i < n) {
                pathString += separator;
            }
        }
        return pathString;
    }

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	public String getTriggerType() {
		return triggerType;
	}
    
    @Override
    public int compareTo(Event e) {        
        if (this.getEventCode() > e.getEventCode()) {
            return 1;
        } else if (this.getEventCode() < e.getEventCode()) {
            return -1;
        } else {
            EventHandler.handle(EventLevel.ERROR, getClass().getSimpleName(), "", 
                    "Elements have equal eventCode."
                    + "\nElement 1: " + this.getReferenceString()
                    + "\nElement 2: " + e.getReferenceString()
                    );
            return 0;
        }
    }
    
}