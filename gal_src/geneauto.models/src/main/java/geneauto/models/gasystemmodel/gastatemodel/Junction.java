/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/Junction.java,v $
 *  @version	$Revision: 1.37 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Label;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.GotoStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * A location in the chart that is merely a connection point of transitions.
 * Additionally, a history junction has special meaning for the State that
 * contains it.
 */
public class Junction extends Location {

    /**
     * Connective - This junction is a connective junction History - This
     * junction is a history junction. Only OR compositions can have a history
     * junction. Transitions to it are ok, out not.
     */
    protected JunctionType junctionType;

    /**
     * Transitions starting from the junction
     */
    protected TransitionList transitionList = new TransitionList();

    // Constructor
    public Junction() {
        super();
        transitionList.setParent(this);
    }

    // Constructor for convenience
    public Junction(GAModelElement parent) {
        this();
        setParent(parent);
    }

    public void setJunctionType(JunctionType junctionType) {
        this.junctionType = junctionType;
    }

    public JunctionType getJunctionType() {
        return junctionType;
    }

    public void setTransitions(List<Transition> transitions) {
        transitionList.setTransitions(transitions);
    }

    public void addTransition(Transition transition) {
        transitionList.addTransition(transition);
    }

    /*
     * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING
     * ELEMENTS! use setTransitions or addTransition instead
     */
    public TransitionList getTransitionList() {
        return transitionList;
    }

    public List<Transition> getTransitions() {
        return transitionList.getTransitions();
    }

    // Not applicable to this class
    public String getQualifiedName() {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "Junction", "GE9998",
                "Call to unsupported method: getQualifiedName() Element: "
                        + getReferenceString(), "");
        return null;
    }

    /**
     * Abstract evaluation of a transition destination. Looks at the current
     * location and decides, what to do next
     * 
     * @return the code evaluating the transition destination
     */
    public List<Statement> evalAbsDestination(EvaluationContext ctxt, 
            SuccessContinuation success,
            FailureContinuation fail) {
        List<Statement> stmts = new LinkedList<Statement>();

        if (transitionList == null || transitionList.isEmpty()) {
            // Reached a terminal junction. Get continuation and execute
            stmts = success.getTerminalContinuation().getStatements();
        } else {
            /*
             * Reached an intermediate junction. Evaluate transitions.
             * 
             * To avoid replicating code and potential infinite loops in joining
             * transitions, we only generate a goto statement here. If this is
             * the first time we see such a junction, we also generate code, but
             * do not put it in the model yet. Later we replace one of the gotos
             * (that will be for sure used - some generated code sections might
             * eventually be left out) with the generated code.
             */
            Label lbl = ctxt.getJunctionLabel(this);
            String lblName;
            if (lbl == null) {                
                // Unseen location
                // Generate a label and store it == mark Junction as seen.
                lbl = ctxt.addJunctionLabel(this);
                lblName = lbl.getName();
                // Generate code
                // Code from evaluating transitions originating from the
                // junction
                List<Statement> junctStmts = transitionList.evalAbs(ctxt, success,
                        fail);
                if (junctStmts != null && !junctStmts.isEmpty()) {
                    Statement lblStmt = new BlankStatement();
                    lblStmt.setLabel(lbl);
                    List<Statement> tmpStmts = new LinkedList<Statement>();
                    tmpStmts.add(lblStmt);
                    tmpStmts.addAll(junctStmts);
                    ctxt.addJunctionCode(lblName, tmpStmts);
                } else {
                    // There is nothing to be done (no code) for this junction.
                    // Register this fact also in the Code reference 
                    //(i.e. that for this Label there is no code!)
                    ctxt.addJunctionCode(lblName, null);
                }                
            } else {
                lblName = lbl.getName();
            }

            // Make a goto statement. 
            // NOTE: We cannot make a goto with an object reference here, as
            // currently at a later stage most of the statements are copied 
            // and we cannot rely on the object reference to the label.
            // The named references are resolved later.
            GotoStatement gotoStmt = new GotoStatement(lblName);
            gotoStmt.setSourceElement(this);
            stmts.add(gotoStmt);
        }

        return stmts;
    }

    /**
     * No variables in Junctions.
     */
    public List<Variable_SM> getVariables() {
        return null;
    }

    /**
     * Sorts the transitions according to their executionOrder
     */
    public void sort() {
        Collections.sort(getTransitions());
    }

}