/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/continuations/FailureContinuation.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel.continuations;

import geneauto.models.gacodemodel.statement.Statement;

import java.util.LinkedList;
import java.util.List;

/**
 * Failure continuation. This continuation is executed, when the evaluation
 * needs to backtrack.
 * 
 */
public class FailureContinuation extends Continuation {

    /**
     * Statements carrying out the required activity
     */
    private List<Statement> statements = new LinkedList<Statement>();

    /**
     * Default constructor
     */
    public FailureContinuation() {
        super();
    }

    /**
     * Constructor with params
     * 
     * @param List
     *            <Statement> Statements
     */
    public FailureContinuation(List<Statement> statements) {
        this();
        addStatements(statements);
    }

    public void addStatement(Statement statement) {
        statements.add(statement);
    }

    public void setStatements(List<Statement> statements) {
        this.statements.clear();
        for (Statement a : statements) {
            addStatement(a);
        }
    }

    public void addStatements(List<Statement> statements) {
        for (Statement a : statements) {
            addStatement(a.getCopy());
        }
    }

    public List<Statement> getStatements() {
        return statements;
    }

    /**
     * Copy fields from the current Continuation to the given Continuation
     * @param c
     */
    protected void copyFields(FailureContinuation c) {
        for (Statement s : getStatements()) {
            c.addStatement(s.getCopy());            
        }
    }

    public FailureContinuation getCopy() {
        FailureContinuation c = new FailureContinuation();
        copyFields(c);
        return c;
    }

}
