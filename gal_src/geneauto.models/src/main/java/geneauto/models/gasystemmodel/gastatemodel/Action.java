/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/Action.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.genericmodel.GAModelElement;

/**
 * Represents either a change in data or an event broadcast that is to be carried
 * out, when the action is triggered.
 */
public abstract class Action extends GASystemModelElement {

	/**
	 * The operation that is to be carried out, when the action is triggered. Either a
	 * change of data or an event broadcast.
	 */
	protected Statement operation;

	// Constructor
	public Action(){
		super();
	}

	public void setOperation(Statement operation) {
		operation.setParent(this);
		this.operation = operation;
	}

	/**
	 * if the attribute operation has value this value
	 * otherwise seeks it codeModelElement has value and returns statement from 
	 * code model
	 * 
	 * @return
	 */
	public Statement getOperation() {
		if (operation != null) {
			return operation;
		}
		else {
			GAModelElement cmElt = getCodeModelElement();
			if (cmElt != null) {
				if (cmElt instanceof Statement){
					return (Statement) cmElt;
				}
				else{
					EventHandler.handle(
							EventLevel.CRITICAL_ERROR,
							"Action.getOperation",
							"",
							"Incorrect operation statement type \"" + cmElt.getClass() + "\"\n"
							+ "\n expected \"Statement\""
							+ "\n Current object: " + getReferenceString(),
							"");			
					return null;
				}
			}
			else {
				EventHandler.handle(
						EventLevel.CRITICAL_ERROR,
						"Action.getOperation",
						"",
						"Operation undefined in action: " 
							+ getReferenceString(),
						"");			
				return null;
			}
		}
	}

    /**
     * Converts the operation to a CodeModel Statement and cleans the operation
     * attribute. After this is done, getOperation will return the result of
     * getCodeModelElement().
     */
	public Statement toCodeModel() {
		if (operation != null){
			Statement stmt = operation.getCopy();
            operation = null;
            
            // Register the CodeModelElement in the CodeModel (assigns also the ID)
            getModel().getCodeModel().addTempElement(stmt);
            setCodeModelElement(stmt);
            
            // Convert Expressions in the operation Statement also to the CodeModel.
            for (GAModelElement el : stmt.getChildren(Expression.class)) {
                ((Expression) el).toCodeModel();
            }
			
			return stmt;
		}
		return null;
	}

    @Override
    public Action getCopy() {
        return (Action) super.getCopy();
    }	
	
}