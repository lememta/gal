/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/common/Parameter.java,v $
 *  @version	$Revision: 1.30 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.common;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.common.NameSpaceElement;
import geneauto.models.gacodemodel.CustomType_CM;
import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.genericmodel.GAModelElement;

import java.util.LinkedList;
import java.util.List;

/**
 * Parameter (block-type specific property) of a block. A parameter can be a
 * simple value or an expression encoded in GACodeModel language.
 * From the viewpoint of GASystemModel language it is simply a name-value pair. 
 * Semantics of each instance of parameter class is defined by the block type
 * of the block that owns the parameter instance. 
 */
public abstract class Parameter extends Data 
                    implements NameSpaceElement {

    /**
     * value of the parameter
     */
    protected Expression value;
    
    /**
     * index of the parameter
     * used only in case the source model explicitly supports 
     * indexed access to parameters.
     * 
     * It is allowed to have mixture of indexed and non-indexed parameters in
     * the same model, however, when index is defined (value 0 or bigger) then
     * it is assumed to be unique in the scope of parameter parent.
     * 
     * The uniqueness is not checked in the model
     */
    protected int index = -1;

    /**
	 * Method getIndex.
	 * @return the attribute "index".
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Method setIndex.
	 * @param index the "index" to set.
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	public Parameter() {
        super();
    }
    
    public Parameter(Expression value, String name) {
        this();
        setValue(value);
        setName(name);
    }

    public Expression getValue() {
        return value;
    }

    @Override
    public GADataType getDataType() {
    	// Derive the value from the value and do not buffer it
        if (value == null) {
            return null;
        } else {
            return value.getDataType();
        } 
    }

    @Override
    public void setDataType(GADataType dataType) {
        EventHandler.handle(EventLevel.ERROR,
                "Parameter.setDataType", "",
                "An attempt to set data type to the parameter"
                    + "\nParameter: " + getReferenceString()
        			+ "\nThe type should be derived from the value");
    }

    /**
     * converts the value to string and returns this string
     * 
     * @return String
     * 
     * TODO: decide what to do in case the value type is not StringExpression
     */
    public String getStringValue() {
        if (value != null && value instanceof StringExpression)
            return ((StringExpression) value).getLitValue();

        return null;
    }

    public void setValue(Expression value) {
        this.value = value;
        if (this.value != null) {
            this.value.setParent(this);
        }
    }
    
    public void setValue(String value) {
    	setValue(new StringExpression(value));
    }

    /**
     * Returns copy of the parameter value in form of an expression Replaces all
     * system model elements referenced in expression with corresponding code
     * model element
     */
    public Expression getCodeModelExpression() {
        if (value == null) {
            return null;
        }            

        // retrieve the expression value
        Expression exp = value.getCopy();

        // check that the expression would not contain system model variables
        GAModelElement cmEl;
        GAModelElement var;
        List<GAModelElement> exprList = new LinkedList<GAModelElement>();
        if (exp instanceof VariableExpression) {
            exprList.add(exp);
        } else {
            exp.getAllChildren(VariableExpression.class, exprList);
        }

        for (GAModelElement e : exprList) {
            var = (GAModelElement) ((VariableExpression) e).getVariable();
            if (var instanceof GASystemModelElement) {
                cmEl = ((Variable_SM) var).getCodeModelElement();
                if (cmEl == null) {
                    EventHandler.handle(EventLevel.ERROR,
                            "Parameter.getCodeModelExpression", "",
                            "Expression " + getReferenceString()
                                    + "\n contains system model element "
                                    + var.getShortReferenceString()
                                    + ", that does not have reference to "
                                    + "corresponding code model element", "");
                    return null;
                }

                ((VariableExpression) e).setVariable((Variable_CM) cmEl);
            } else if (var instanceof GACodeModelElement) {
                if (var instanceof StructureMember) {
                    CustomType custom = ((CustomType) var.getParent()
                            .getParent());
                    if (custom instanceof CustomType_SM) {
                        CustomType_CM customCM = (CustomType_CM) ((CustomType_SM) custom)
                                .getCodeModelElement();

                        ((VariableExpression) e).setVariable(customCM
                                .getMemberByName(var.getName()));
                    }
                }

            }
        }

        return exp;
    }

	/**
	 * Returns true if the two parameters have the same name
	 * In case at least one of the parameters has null for name,
	 * the return value is false.
	 */
    @Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Parameter)) {
			return false;
		}
		
		if(getName() == null || ((Parameter)other).getName() == null) {
			return false;
		}
		
		return getName().equals(((Parameter)other).getName());
	}

	/**
	 * Calculates hash code according to the parameter name -- the assumption 
	 * is that there should be no parameters with overlapping names
	 * 
	 * When parameter name is missing, returns 0
	 *  
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (getName() == null) {
			return 0;
		} else {
			return getName().hashCode();
		}
	}
}