/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/Box.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.common.ContainerNode;
import geneauto.models.gasystemmodel.common.Function_SM;
import geneauto.models.gasystemmodel.common.Node;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Box is used for grouping sate chart elements.
 * 
 * Box represents the Stateflow's box object with a following restriction: the
 * box and its children can only contain graphical functions, truth tables, etc.
 * but not states or flow-graph sections. I.e. it cannot be a part of the
 * chart's main part and is only a place for grouping functions.
 * 
 * Justification: Using boxes to group also states or flow-graph networks
 * creates strange and complex scoping. The logical scoping for executing the
 * chart sections and the name scopes introduced by boxes do not match. Boxes
 * are fine for grouping function objects (no interference with the main
 * chart)..
 */
public class Box extends GASystemModelElement implements ContainerNode {

	protected List<Function_SM> functions = new ArrayList<Function_SM>();
	protected List<Box> boxes = new ArrayList<Box>();

	// Constructor
	public Box() {
		super();
	}

	// Constructor for convenience
	public Box(GAModelElement parent) {
		this();
		setParent(parent);
	}

	/**
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addFunction instead
	 */
	public List<Function_SM> getFunctions() {
		return functions;
	}

	public void addFunction(Function_SM f){
		f.setParent(this);
		functions.add(f);
	}
	
	/**
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addFunction instead
	 */
	public List<Box> getBoxes() {
		return boxes;
	}

	public void addBox(Box b){
		b.setParent(this);
		boxes.add(b);
	}

    /**
     * A parent Node of a Box is either another Box, a State or the Chart
     * 
     * @return The parent Node
     */
    public Node getParentNode() {
        if (getParent() instanceof Node) {
            return (Node) getParent();
        } else if (getParent() instanceof Composition) {
            return ((Composition) getParent()).getParentNode();
        } else {
            String msg = "Unexpected type of parent." 
                + "\nElement: " + getReferenceString();
            if (getParent() != null) {
                msg += "\nParent's type: " + getParent().getClass().getName();
            }
            return null;
        }
    }

    @Override
    public List<Node> getPath(PathOption pathOption) {
        List<Node> path;
        Node parentNode = getParentNode();
        if (parentNode != null) {
            path = parentNode.getPath(pathOption);
        } else {
            path = new LinkedList<Node>();
        }            
        path.add(this);
        return path;
    }
    
    @Override
    public String getPathString(String separator, PathOption pathOption) {
        List<Node> path = getPath(pathOption);
        int n = path.size();
        int i = 0;
        String pathString = new String();
        for (Node node : path) {
            pathString += node.getName();
            if (++i < n) {
                pathString += separator;
            }
        }
        return pathString;
    }

    @Override
    public List<Node> getChildNodes() {
        List<Node> childNodes = new LinkedList<Node>();
        for (GAModelElement e : getChildren(Node.class)) {
            childNodes.add((Node) e);
        }
        return childNodes;
    }
    
}