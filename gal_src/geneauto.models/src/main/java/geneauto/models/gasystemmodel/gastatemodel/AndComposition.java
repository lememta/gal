/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/AndComposition.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * A collection of states. The states are treated in a pseudo-parallel manner. When
 * the composition is active then it means that all its component states are
 * active. The actual execution order of sub-states is determined by their
 * execution order.
 
 */
public class AndComposition extends Composition {

	private List<State> states = new ArrayList<State>();

	// Constructor
	public AndComposition() {
		super();
	}

	// Constructor for convenience
	public AndComposition(GAModelElement parent) {
		setParent(parent);
	}

	/*
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addState instead
	 */

	public List<State> getStates() {
		return states;
	}
	
    public void addState(State s) {
        s.setParent(this);
        states.add(s);
    }
    
    public void setStates(List<State> ss) {
    	states.clear();
        if (ss != null) {
            for (State s : ss) {
                addState(s);
            }
        }
    }
    
	/**
	 * Abstract evaluation of a composition entry.
	 * Opens the locations in the composition.
	 * 
	 * @return the for entering a composition
	 */
	public List<Statement> evalAbsEntry(EvaluationContext in_ctxt) {
		List<Statement>   stmts = new LinkedList<Statement>();		
        EvaluationContext ctxt = new EvaluationContext(in_ctxt);
        
		if (states == null || states.isEmpty()) {
			// No States. Nothing to do 
		} 
		else {
			// Return code opening the States one-by-one
			for (State s : states) {
				stmts.addAll(s.evalAbsEntry(ctxt, null));				
			}
		}			
		return stmts;
	}
	
	/**
	 * Abstract evaluation of executing a composition that is already open.
	 * 
	 * @return the code executing an open composition
	 */
	public List<Statement> evalAbsDuring(EvaluationContext in_ctxt) {
        List<Statement>   stmts = new LinkedList<Statement>();      
        EvaluationContext ctxt = new EvaluationContext(in_ctxt);
        
		if (states == null || states.isEmpty()) {
			// No States. Nothing to do 
		} 
		else {
			// Return code for executing the States one-by-one
			for (State s : states) {
				stmts.addAll(s.evalAbsDuring(ctxt));				
			}
		}		
		return stmts;
	}

	/**
	 * Abstract evaluation of exiting a composition.
	 * Closes the locations in the composition.
	 * 
	 * @return the for closing a composition
	 */
	public List<Statement> evalAbsExit(EvaluationContext in_ctxt) {
        List<Statement>   stmts = new LinkedList<Statement>();      
        EvaluationContext ctxt = new EvaluationContext(in_ctxt);
        
		if (states == null || states.isEmpty()) {
			// No States. Nothing to do 
		} 
		else {
			// Return code for closing the States one-by-one
			for (State s : states) {
				stmts.addAll(s.evalAbsExit(ctxt, null, false));				
			}
		}		
		return stmts;
	}

	/**
	 * @return true for compositions that don't contain any sub-states
	 */
	public boolean isStateless() {
		return states == null || states.size() == 0;
	}

	/**
	 *  A dummy for flexibility.
	 */
	public List<Transition> getDefaultTransitions() {
		return new LinkedList<Transition>();
	}
	
    /**
     * Sorts the States and transitions according to their executionOrder
     */
	@Override
    public void sort() {
	    super.sort();
        Collections.sort(getStates());
    }
	

	
}