/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/Transition.java,v $
 *  @version	$Revision: 1.46 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gacodemodel.utilities.CodeModeUtilities;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;
import geneauto.utils.map.ConstantMap;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A directed connector between States and/or Junctions (commonly named Locations).
 * Either connects two locations or enters to a location from an imaginary start
 * location (a default transition).
 */
public class Transition extends GASystemModelElement implements Comparable<Transition> {

	protected List<Event> events = new LinkedList<Event>();

	protected Expression guard;

	/**
	 * Determines the execution order in a list of transitions.
	 */
	protected int executionOrder;
	
	/**
	 * These actions are executed, when a transition is evaluated and its event
	 * condition and guard are true.
	 */
	protected List<SimpleAction> conditionActions = new ArrayList<SimpleAction>();

	/**
	 * These actions are executed, when a transition is actually taken
	 */
	protected List<SimpleAction> transitionActions = new ArrayList<SimpleAction>();

	protected Location destination;
	
	/**
	 * Type of the transition
	 */
	protected TransitionType type;

	// Constructor
	public Transition() {
		super();
	}

	// Constructor for convenience
	public Transition(GAModelElement parent) {
		super();
		setParent(parent);
	}

	public List<Event> getEvents() {
		return events;
	}
	
	public void addEvent(Event e){
		events.add(e);
	}

	public void setGuard(Expression guard) {
		guard.setParent(this);
		this.guard = guard;
	}

	public Expression getGuard() {
		if (guard != null) {
			return guard;
		}
		else {
			GAModelElement cmElt = getCodeModelElement();
			if (cmElt == null) {
				return null;
			} else {
				if (cmElt instanceof Expression){
					return (Expression) cmElt;
				} else{
					EventHandler.handle(
							EventLevel.CRITICAL_ERROR,
							"Transition.getGuard",
							"",
							"Incorrect guard type \"" + cmElt.getClass() + "\"\n"
							+ "\n expected \"Expression\""
							+ "\n Current object: " + getReferenceString(),
							"");			
					return null;
				}
			}
		}
	}

	public void setExecutionOrder(int executionOrder) {
		this.executionOrder = executionOrder;
	}

	public int getExecutionOrder() {
		return executionOrder;
	}

	/**
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addConditionAction instead
	 */
	public List<SimpleAction> getConditionActions() {
		return conditionActions;
	}

	public void addConditionAction(SimpleAction a){
		a.setParent(this);
		conditionActions.add(a);
	}
	
	/**
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addtransitionAction instead
	 */
	public List<SimpleAction> getTransitionActions() {
		return transitionActions;
	}

	public void addTransitionAction(SimpleAction a){
		a.setParent(this);
		transitionActions.add(a);
	}

	public void setDestination(Location destination) {
		this.destination = destination;
	}

	public Location getDestination() {
		return destination;
	}

	public void setType(TransitionType type) {
		this.type = type;
	}

	public TransitionType getType() {
		return type;
	}
	
	/**
	 * Moves the guard to code model and cleans the guard attribute
	 * after this is done, getGuard will return the result of getCodeModelElement() 
	 */
	public Expression toCodeModel() {
		if (guard != null){
			Expression exp = guard.getCopy();
			guard = null;	
			
	        // Register the CodeModelElement in the CodeModel (assigns also the ID)
            getModel().getCodeModel().addTempElement(exp);
            
            // Normalise element references in the Expression.
            // Note! As this may require that the entire Expression gets replaced,
            // update also the handle to the object.
			exp = exp.toCodeModel();
            setCodeModelElement(exp);
            
            // The expressions in actions are not converted to CodeModel here.
            // Actions are converted separately.
            
			return exp;
		}
		return null;
	}
	
	/**
	 * Abstract evaluation of a transition.
	 * 
	 * @return the code evaluating the transition
	 */
	public List<Statement> evalAbs(
	        EvaluationContext ctxt,
	        SuccessContinuation success, 
	        FailureContinuation fail) {
		List<Statement>   stmts 	 = new LinkedList<Statement>();
		
		// Transition condition
		Expression condExp = processCondition();
		
		// Add the transitions actions of the current transition to the continuation
		success.addActions(transitionActions);
		
		// Create then branch (condition = True)
		List<Statement>	  thenStmts = new LinkedList<Statement>();
		// Condition actions
		if (getConditionActions() != null && !getConditionActions().isEmpty()) {
			thenStmts.add(new BlankStatement("Perform condition actions"));
			for (Action ca : getConditionActions()) {
			    thenStmts.add(ca.getOperation().getCopy());			
			}
		}
		// Evaluate the destination of the transition
		if (destination != null) {
			List<Statement>	  tmpStmts = destination.evalAbsDestination(
			        ctxt, success, fail);
			thenStmts.addAll(tmpStmts);
		} else {
			EventHandler.handle(
				EventLevel.ERROR,
				"Transition.evalAbs",
				"",
				"Transition destination is null: " + getReferenceString(),
				"");
		}
		
        // Create else branch (condition = False)
		List<Statement> elseStmts  = fail.getStatements();
		
		// Make an if statement
		List<Statement> checkStmts = CodeModeUtilities.makeOptimizedIf(condExp, thenStmts, elseStmts);
		// Add the annotations of the transition to this statement
		if (annotations != null && !annotations.isEmpty()) {
			Statement comnt = new BlankStatement();
			comnt.setAnnotations(annotations);
			stmts.add(comnt);
		}
		stmts.addAll(checkStmts);
		
		return stmts;
	}

    /**
     * @return
     */
    private Expression processCondition() {
        Expression 		  actEvtExp  = EvaluationContext.getActiveEventExpression().getCopy();
		List<Expression>  evtExps 	 = new LinkedList<Expression>();
		for (Event e : getEvents()) evtExps.add(e.getReferenceExpression().getCopy());
		Expression 		  evtExp 	 = CodeModeUtilities.makeListCompare(actEvtExp, evtExps);
		Expression		  grdExp;
		if (getGuard() == null) {
			grdExp = null;
		} else {
			grdExp = getGuard().getCopy();
		}
		Expression 		  condExp 	 = CodeModeUtilities.makeOptimizedAnd(evtExp, grdExp);
        return condExp;
    }
    
    /**
     * @return true, when the Transition has an event condition or guard, otherwise false
     */
    public boolean isConditional() {
    	if (getGuard() != null) {
    		return true;
    	}
    	if (getEvents() == null || getEvents().size() == 0) {
    		return false;
    	}
    	return true;
    }

	/**
	 * Resolves by-name references in the current element only.
	 * See also: resolveReferences
	 * 
	 * The method checks, whether the Event objects pointed to in the events field
	 * are contained in a model. If not, then it is assumed to be a dummy Event
	 * and we have to find the proper Event (having the same name as the dummy
	 * Event) by going up in the model's hierarchy. The event field is assumed
	 * not to be empty.
	 * 
	 * @param  ConstantMap<String, GAModelElement> nameMap
	 */
	 @Override
	 public GAModelElement resolveCurrentElement(
		 final ConstantMap<String, GAModelElement> nameMap) {
		 
		 int i = -1;
		 for (Event event : events) {
			 i++;
			 // Check, if the event has been resolved already to a proper event.
			 if (event.getModel() != null) {
				// Skip this object and continue with the rest. Note: it should
				// normally be the case that either all are unresolved or all
				// are resolved, but it is safer to scan the whole list
				 continue;
			 }
			 
			GAModelElement el = nameMap.get(event.getName());
			if (el != null) {
				if (el instanceof Event) {
					events.set(i, (Event) el); // Replace the event
				} else {
					EventHandler.handle(
							EventLevel.ERROR,
							"Transition.resolveReferencesInCurrent",
							"",
							"Expecting Event: " + el.getReferenceString() + "\n" 
							+ "Current element: " + getReferenceString());				 
				}
			} else {
				if (nameMap.containsKey(event.getName())) {
					EventHandler.handle(EventLevel.ERROR,
							getClass().getSimpleName() + ".resolveReferencesInCurrent", 
								"", 
								"Multiple definitions for event " + event.getName() 
								+ ". Can not resolve reference by name.\n"
									+ "Current element: " + getReferenceString());
				} else {
					EventHandler.handle(
							EventLevel.ERROR,
							"Transition.resolveReferencesInCurrent",
							"",
							"Event " + event.getName() + " undefined.\n" 
							+ "Current element: " + getReferenceString());
				}			
			}
		}
        // The object itself is still the same. Hence, return null.
        return null;
	}

    @Override
    public int compareTo(Transition t) {        
        if (this.getExecutionOrder() > t.getExecutionOrder()) {
            return 1;
        } else if (this.getExecutionOrder() < t.getExecutionOrder()) {
            return -1;
        } else {
            EventHandler.handle(EventLevel.ERROR, getClass().getSimpleName(), "", 
                    "Elements have equal executionOrder."
                    + "\nElement 1: " + this.getReferenceString()
                    + "\nElement 2: " + t.getReferenceString()
                    );
            return 0;
        }
    }

}