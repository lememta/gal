/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gasystemmodel/gastatemodel/FlowGraphComposition.java,v $
 *  @version	$Revision: 1.29 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gasystemmodel.gastatemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FailureContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.FlowGraphContinuation;
import geneauto.models.gasystemmodel.gastatemodel.continuations.SuccessContinuation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.statemodel.EvaluationContext;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A collection of transitions and junctions.
 */
public class FlowGraphComposition extends Composition {

	protected List<Junction> junctions = new ArrayList<Junction>();
	protected TransitionList defaultTransitionList = new TransitionList();

	// Constructor
	public FlowGraphComposition() {
		super();
		defaultTransitionList.setParent(this);
	}

	// Constructor for convenience
	public FlowGraphComposition(GAModelElement parent) {
		this();
		setParent(parent);
	}

	/*
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use setDefaultTransitions or addDefaultTransition instead
	 */
	public List<Transition> getDefaultTransitions() {
		return defaultTransitionList.getTransitions();
	}

	public void setDefaultTransitions(List<Transition> transitions) {
		defaultTransitionList.setTransitions(transitions);
	}

	public void addDefaultTransition(Transition transition) {
		defaultTransitionList.addTransition(transition);
	}
	
	/*
	 * NB! the resulting list of this getter MUST NOT BE USED FOR ADDING ELEMENTS!
	 * use addJunction instead
	 */
	public List<Junction> getJunctions() {
		return junctions;
	}
	
	/**
	 * Abstract evaluation of a composition entry.
	 * Opens the locations in the composition.
	 * 
	 * @return the for entering a composition
	 */
	public List<Statement> evalAbsEntry(EvaluationContext in_ctxt) {
        List<Statement>   stmts = new LinkedList<Statement>();      
        EvaluationContext ctxt = new EvaluationContext(in_ctxt);
	    
		SuccessContinuation success = new FlowGraphContinuation();
		FailureContinuation fail	= new FailureContinuation();
		
		stmts = defaultTransitionList.evalAbs(ctxt, success, fail);
		
		return stmts; 
	}
	
	/**
	 * Abstract evaluation of executing a composition that is already open.
	 * 
	 * @return the code executing an open composition
	 */
	public List<Statement> evalAbsDuring(EvaluationContext ctxt) {
		// Not applicable in this class
		EventHandler.handle(EventLevel.CRITICAL_ERROR, "FlowGraphComposition",
				"GE9998",
				"Call to unsupported method: evalAbsDuring() Element: "
						+ getReferenceString(), "");
		return null;
	}

	/**
	 * Abstract evaluation of exiting a composition.
	 * Closes the locations in the composition.
	 * 
	 * @return the for closing a composition
	 */
	public List<Statement> evalAbsExit(EvaluationContext ctxt) {
		// Not applicable in this class
		EventHandler.handle(EventLevel.CRITICAL_ERROR, "FlowGraphComposition",
				"GE9998",
				"Call to unsupported method: evalAbsExit() Element: "
						+ getReferenceString(), "");
		return null;
	}

	/**
	 * @return true for compositions that don't contain any sub-states
	 */
	public boolean isStateless() {return true;}

	/**
	 * @return the events in the current node and its children.
	 */
	public List<Event> getAllEvents() {
		return new LinkedList<Event>();
	}

	/**
	 * @return the variables in the current node and its children.
	 */
	public List<Variable_SM> getAllVariables() {
		return new LinkedList<Variable_SM>();
	}
	
	public void addJunction(Junction j) {
		j.setParent(this);
		junctions.add(j);
	}
	
	public void setJunctions(List<Junction> junctions) {
		this.junctions.clear();
		for (Junction j : junctions){
			addJunction(j);
		}
	}

    @Override
    public FlowGraphComposition toFlowGraphComposition() {
        return this;
    }	
	
}