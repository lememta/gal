package geneauto.models.gavamodel.statement;

import java.util.ArrayList;
import java.util.List;

import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.statement.Statement;

/**
 * AnnotationStatement is the main class for code annotations 
 */
public class AnnotationStatement extends Statement {
	
	private List<VAQuantifier> quantifier;
	private Expression expr;
	protected String proof=null; 
	
	public String getProof() {
		return proof;
	}

	public void setProof(String proof) {
		this.proof = proof;
	}
	
	public AnnotationStatement() {
		super();
		quantifier = new ArrayList<VAQuantifier>();
	}

	public AnnotationStatement(BinaryExpression in) {
		expr=in;
	}

	public void setQuantifiers(List<VAQuantifier> quantifier) {
		this.quantifier = quantifier;
	}

	public List<VAQuantifier> getQuantifier() {
		return quantifier;
	}

	public void setExpr(Expression expr) {
		if (expr.getParent()==null){
			expr.setParent(this);
		}
		this.expr = expr;
	}

	public Expression getExpr() {
		return expr;
	}
}
