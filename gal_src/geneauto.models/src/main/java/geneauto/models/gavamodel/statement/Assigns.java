package geneauto.models.gavamodel.statement;

import geneauto.models.gacodemodel.expression.VariableExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * AssignsStatement implements assigns in the program annotations 
 * The components of Assigns statement are:
 * <ul>
 * <li>vars -- a list of code model variable to valid</li>
 * </ul>
 */
public class Assigns extends AnnotationStatement
{
	private List<VariableExpression> vars;
	
	public Assigns(){
		this.vars = new ArrayList<VariableExpression>();
	}
	
	public Assigns(VariableExpression var){
		this.vars = new ArrayList<VariableExpression>();
		this.vars.add(var);
	}
	
	public Assigns(List<VariableExpression> lstVars){
		this.vars = new ArrayList<VariableExpression>();
		this.vars.addAll(lstVars);
	}
	
	public List<VariableExpression> getVariables(){
		return vars;
	}
	
	public void setVariables(List<VariableExpression> lstVars){
		this.vars.clear();
		this.vars.addAll(lstVars);
	}
	
	public void addVariable(VariableExpression var){
		this.vars.add(var);
	}
}
