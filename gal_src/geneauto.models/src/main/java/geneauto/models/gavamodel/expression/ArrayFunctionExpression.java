package geneauto.models.gavamodel.expression;

import java.util.List;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;

public class ArrayFunctionExpression extends GenericFunctionExpression {

	private List<IntegerExpression> dims; 
	
	public ArrayFunctionExpression(String s, List<IntegerExpression> d,List<Expression> l) {
		this.rootName=s;
		this.dims=d;
		for (IntegerExpression i : d) {
			i.setParent(this); 
		}
		this.arguments=l;
		for (Expression e : l) {
			e.setParent(this);
		}
		setPrintName();
	}
	public ArrayFunctionExpression() {
		
	}
	public void setPrintName() { 
		switch (dims.size()) {
		case 1: rootName="vect_of_";
		break;
		case 2: rootName="mat_of_";
		break;
		default: 
			rootName="array_of_";
		
		}
		String tempName=rootName;
		tempName=tempName+dims.get(0).getLitValue();
		for (int i=1; i<dims.size(); i++){
			tempName=tempName+"x"+dims.get(i).getLitValue();
		}
		tempName+="_scalar"; 
		this.rootName=tempName; 
	}
	public List<IntegerExpression> getDims() {
		return dims;
	}
	public void setDims(List<IntegerExpression> dims) {
		this.dims = dims;
	}

}
