/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VA Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getVAElement()
 * @model
 * @generated
 */
public class VAElement extends Annotation {
	
	private GAModelElement parent;
	
	private int id;
	protected String proof=null; 
	
	public String getProof() {
		return proof;
	}

	public void setProof(String proof) {
		this.proof = proof;
	}

	public VAElement() {
		super();
		setVerificationAnnotation(true);
	}
	
	public void setParent(GAModelElement element) {
		this.parent=element;
	}
	
	public GAModelElement getParent() {
		return this.parent;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
} // VAElement
