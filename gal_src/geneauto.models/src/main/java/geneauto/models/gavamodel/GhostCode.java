/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;

import geneauto.models.gacodemodel.statement.Statement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ghost Code</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getGhostCode()
 * @model
 * @generated
 */
public class GhostCode extends SimpleVAElement {
	
	private Statement stmt;
	
	public GhostCode() {
		super();
	}

	public void setStatement(Statement stmt) {
		this.stmt = stmt;
	}

	public Statement getStatement() {
		return stmt;
	}

} // GhostCode
