package geneauto.models.gavamodel.statement;

import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * ForAllStatement implements \forall in the program annotations 
 * The components of ForAll statement are:
 * <ul>
 * <li>lstVars -- a list of variables used in the annotation</li>
 * <li>lstRanges -- a list of ranges for each variable</li>
 * <li>expr -- an expression: the body of the annotation</li>
 * </ul>
 */
public class ForAll extends VAQuantifier
{
	private List<VariableExpression> lstVars;
	private List<RangeExpression> lstRanges;
	
	public ForAll(){
		this.lstRanges = new ArrayList<RangeExpression>();
		this.lstVars = new ArrayList<VariableExpression>();
	}
	
	public ForAll(VariableExpression var, RangeExpression range){
		this.lstRanges = new ArrayList<RangeExpression>();
		this.lstVars = new ArrayList<VariableExpression>();
		lstRanges.add(range);
		lstVars.add(var);
	}
	
	public ForAll(List<VariableExpression> vars, List<RangeExpression> ranges){
		this.lstRanges = ranges;
		this.lstVars = vars;
	}
	
	public void setLstVars(List<VariableExpression> lstVars) {
		this.lstVars = lstVars;
	}
	
	public List<VariableExpression> getLstVars() {
		return lstVars;
	}
	
	public void setLstRanges(List<RangeExpression> lstRanges) {
		this.lstRanges = lstRanges;
	}
	
	public List<RangeExpression> getLstRanges() {
		return lstRanges;
	}
}
