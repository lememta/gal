package geneauto.models.gavamodel.statement;

/**
 * LoopInvariantStatement implements loop invariant in the program annotations 
 * The components of LoopInvariant statement are:
 * <ul>
 * <li>subStatement -- a SubAnnotationStatement as the body of the LoopInvariant statement</li>
 * </ul>
 */
public class LoopInvariant extends AnnotationStatement {

	public LoopInvariant(){
		
	}
}
