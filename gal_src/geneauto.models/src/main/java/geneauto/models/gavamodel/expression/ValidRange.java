package geneauto.models.gavamodel.expression;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;

/**
 * ValidRangeStatement implements \valid_range in the program annotations 
 * The components of ValidRange statement are:
 * <ul>
 * <li>var -- a code model variable for which we stipulate the range to valid</li>
 * <li>range -- a range stating the range to valid</li>
 * </ul>
 */
public class ValidRange extends Expression {
	
	private VariableExpression var;
	private RangeExpression range;
	
	public ValidRange(){
		
	}
	
	public ValidRange(VariableExpression var, RangeExpression range){
		this.range = range;
		this.var = var;
	}
	
	public void setVar(VariableExpression var) {
		this.var = var;
	}
	
	public VariableExpression getVar() {
		return var;
	}
	
	public void setRange(RangeExpression range) {
		this.range = range;
	}
	
	public RangeExpression getRange() {
		return range;
	}

}
