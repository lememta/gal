package geneauto.models.gavamodel.expression;

import java.util.List;

import geneauto.models.gacodemodel.expression.Expression;

public class GenericFunctionExpression extends Expression {

	public GenericFunctionExpression() {
		// TODO Auto-generated constructor stub
	}

	public List<Expression> getArguments() {
		return arguments;
	}
	public void setArguments(List<Expression> arguments) {
		this.arguments = arguments;
	}
	public String getRootName() {
		return rootName;
	}
	public void setRootName(String rootName) {
		this.rootName = rootName;
	}
	public String getPrintName() {
		return printName;
	}
	public void setPrintName(String printName) {
		this.printName = printName;
	}
	protected List<Expression> arguments;
	protected String rootName; 
	protected String printName;
	
	public GenericFunctionExpression(String s, List<Expression> l) {
		rootName=s;
		arguments=l;
		for (Expression e : l) {
			e.setParent(this); 
		}
	}

}
