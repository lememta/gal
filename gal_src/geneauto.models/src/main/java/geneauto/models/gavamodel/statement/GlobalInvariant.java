package geneauto.models.gavamodel.statement;

/**
 * GlobalInvariantStatement implements global invariant in the program annotations 
 * The components of GlobalInvariant statement are:
 * <ul>
 * <li>subStatement -- a SubAnnotationStatement as the body of the GlobalInvariant statement</li>
 * </ul>
 */
public class GlobalInvariant extends AnnotationStatement
{
	
	public GlobalInvariant(){
		
	}
}
