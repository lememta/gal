/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getLoopAnnotation()
 * @model
 * @generated
 */
public class LoopContract extends CompoundVAElement {
	
	public LoopContract() {
		super();
	}
	
} // LoopAnnotation
