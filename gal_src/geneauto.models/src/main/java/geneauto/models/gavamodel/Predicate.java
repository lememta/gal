package geneauto.models.gavamodel;

import java.util.ArrayList;
import java.util.List;

import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gavamodel.statement.AnnotationStatement;

public class Predicate extends VAElement {

	private AnnotationStatement annotStmt;
	private List<VariableExpression> parameters;
	private String name;
	
	public Predicate(String name) {
		super();
	}
	
	public void setAnnotStmt(AnnotationStatement annotStmt) {
		this.annotStmt = annotStmt;
	}

	public AnnotationStatement getAnnotStmt() {
		return annotStmt;
	}
	
	public void addParameter(VariableExpression var){
		if (parameters == null){
			parameters = new ArrayList<VariableExpression>();
		}
		parameters.add(var);
	}
	
	public List<VariableExpression> getParameters(){
		return parameters;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
}
