package geneauto.models.gavamodel.statement;

/**
 * RequiresStatement implements requires in the program annotations 
 * The components of Requires statement are:
 * <ul>
 * <li>subStatement -- a SubAnnotationStatement as the body of the require statement</li>
 * </ul>
 */
public class Requires extends AnnotationStatement
{
	
	public Requires(){
		
	}
}
