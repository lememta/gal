package geneauto.models.gavamodel.statement;

import geneauto.models.gacodemodel.expression.Expression;

/**
 * LoopVariantStatement implements loop variant in the program annotations 
 * The components of LoopVariant statement are:
 * <ul>
 * <li>expr -- an expression stating the variant of the loop annotation</li>
 * </ul>
 */
public class LoopVariant extends AnnotationStatement
{
	
	private Expression expr;
	
	public LoopVariant(){
		
	}
	
	public LoopVariant(Expression expr){
		this.setExpr(expr);
	}

	public void setExpr(Expression expr) {
		this.expr = expr;
	}

	public Expression getExpr() {
		return expr;
	}
	
	
	
}
