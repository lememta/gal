package geneauto.models.gavamodel.statement;

import geneauto.models.gacodemodel.Label;

import java.util.ArrayList;
import java.util.List;

/**
 * BehaviorStatement implements behavior in the program annotations 
 * The components of Behavior statement are:
 * <ul>
 * <li>lstAssumeStatement -- a list of AssumesStatements used as pre-conditions for this branch of computation</li>
 * <li>lstEnsuresStatements -- a list of EnsuresStatements used as post-conditions for this branch of computation</li>
 * </ul>
 */
public class Behavior extends AnnotationStatement
{
	private List<Assumes> lstAssumeStatement;
	private List<Ensures> lstEnsuresStatements;
	
	public Behavior(){
		super();
	}
	
	public Behavior(Label label){
		this.setLabel(label);
		this.lstEnsuresStatements = new ArrayList<Ensures>();
		this.lstAssumeStatement = new ArrayList<Assumes>();
	}
	
	public Behavior(Label label, List<Assumes> assumeStatement, List<Ensures> lstEnsuresStatements){
		this.setLabel(label);
		this.lstAssumeStatement = assumeStatement;
		this.lstEnsuresStatements = lstEnsuresStatements;
	}
	
	public void setLstAssumeStatement(List<Assumes> assumeStatement) {
		this.lstAssumeStatement = assumeStatement;
	}
	
	public List<Assumes> getLstAssumeStatement() {
		return lstAssumeStatement;
	}
	
	public void setLstEnsuresStatements(List<Ensures> lstEnsuresStatements) {
		this.lstEnsuresStatements = lstEnsuresStatements;
	}
	
	public List<Ensures> getLstEnsuresStatements() {
		return lstEnsuresStatements;
	}
	
	
}
