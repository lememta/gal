package geneauto.models.gavamodel.statement;

/**
 * AssumesStatement implements assumes in the program annotations 
 * The components of Assumes statement are:
 * <ul>
 * <li>subStatement -- a SubAnnotationStatement as the body of the require statement</li>
 * </ul>
 */
public class Assumes extends AnnotationStatement {

	public Assumes(){
	}
}
