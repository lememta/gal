package geneauto.models.gavamodel.statement;

/**
 * EnsuresStatement implements ensures annotation in the program annotations 
 * The components of Ensures statement are:
 * <ul>
 * <li>subStatement -- a SubAnnotationStatement as the body of the require statement</li>
 * </ul>
 */
public class Ensures extends AnnotationStatement
{
	private AnnotationStatement subStatement;

	public Ensures(){
		
	}
	
	public Ensures(AnnotationStatement subStatement){
		if (subStatement.getParent()==null){
			subStatement.setParent(this);
		}

		this.subStatement = subStatement;
	}
	
	public void setSubStatement(AnnotationStatement subStatement) {
		if (subStatement.getParent()==null){
			subStatement.setParent(this);
		}

		this.subStatement = subStatement;
	}

	public AnnotationStatement getSubStatement() {
		return subStatement;
	}
}
