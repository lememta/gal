package geneauto.models.gavamodel.statement;

import geneauto.models.gacodemodel.statement.Statement;

/**
 * GhostStatement implements ghost annotation in the program annotations 
 * The components of Ghost statement are:
 * <ul>
 * <li>expr -- an expression used as ghost code in the annotations</li>
 * </ul>
 */
public class Ghost extends AnnotationStatement {
	
	private Statement stmt;

	public Ghost(){
		
	}
	
	public Ghost(Statement stmt){
		this.stmt = stmt;
	}
	
	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}

	public Statement getStmt() {
		return stmt;
	}
	
	
	
}
