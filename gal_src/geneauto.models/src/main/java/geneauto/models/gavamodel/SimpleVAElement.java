package geneauto.models.gavamodel;

import geneauto.models.gavamodel.statement.AnnotationStatement;

public abstract class SimpleVAElement extends VAElement{

	private AnnotationStatement annotStmt;

	public SimpleVAElement() {
		super();
	}
	
	public void setAnnotStmt(AnnotationStatement annotStmt) {
		this.annotStmt = annotStmt;
	}

	public AnnotationStatement getAnnotStmt() {
		return annotStmt;
	}
	
	
}
