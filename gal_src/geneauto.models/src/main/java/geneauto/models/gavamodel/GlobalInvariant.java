/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Global Invariant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getGlobalInvariant()
 * @model
 * @generated
 */
public class GlobalInvariant extends SimpleVAElement {
} // GlobalInvariant
