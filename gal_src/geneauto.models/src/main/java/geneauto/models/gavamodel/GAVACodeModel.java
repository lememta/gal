/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GAVA Code Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getGAVACodeModel()
 * @model
 * @generated
 */
public class GAVACodeModel extends GAVAModel {
} // GAVACodeModel
