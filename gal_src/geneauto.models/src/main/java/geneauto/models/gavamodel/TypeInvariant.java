/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Invariant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getTypeInvariant()
 * @model
 * @generated
 */
public class TypeInvariant extends CompoundVAElement {
} // TypeInvariant
