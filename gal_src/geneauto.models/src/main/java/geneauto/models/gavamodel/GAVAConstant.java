package geneauto.models.gavamodel;

public interface GAVAConstant {
	
	 /** Proof Strategies * */
    public final static String PROOF_SPROCEDURE="SProcedure";
    public final static String PROOF_LINEAR="AffineEllipsoid";
    public final static String PROOF_SKIP="Identity"; 
}
