/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;

import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gavamodel.statement.AnnotationStatement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VA Code Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getVACodeElement()
 * @model
 * @generated
 */
public class VACodeElement extends VAElement {
	
	private AnnotationStatement annotStmt;
	private Statement stmt;

	public void setAnnotStmt(AnnotationStatement stmt) {
		this.annotStmt = stmt;
	}

	public AnnotationStatement getAnnotStmt() {
		return annotStmt;
	}

	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}

	public Statement getStmt() {
		return stmt;
	}
	
} // VACodeElement
