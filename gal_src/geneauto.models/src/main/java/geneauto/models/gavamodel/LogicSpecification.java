/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logic Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getLogicSpecification()
 * @model
 * @generated
 */
public class LogicSpecification extends CompoundVAElement {
} // LogicSpecification
