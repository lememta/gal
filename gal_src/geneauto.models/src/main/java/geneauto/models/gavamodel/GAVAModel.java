/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;

import java.util.List;

import geneauto.models.genericmodel.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GAVA Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getGAVAModel()
 * @model
 * @generated
 */
public class GAVAModel extends Model {

	@Override
	public List<VAElement> getElements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setElements(List<?> elements) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addElement(Object p_element) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addElements(List<?> p_elements) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Model getDependentModel() {
		// TODO Auto-generated method stub
		return null;
	}
} // GAVAModel
