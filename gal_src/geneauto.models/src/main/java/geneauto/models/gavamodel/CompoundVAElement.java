package geneauto.models.gavamodel;

import geneauto.models.gavamodel.statement.AnnotationStatement;

import java.util.ArrayList;
import java.util.List;

public abstract class CompoundVAElement extends VAElement {

	private List<AnnotationStatement> annotStmts;
	
	public CompoundVAElement() {
		super();
		annotStmts = new ArrayList<AnnotationStatement>();
	}

	public void setAnnotStmts(List<AnnotationStatement> annotStmts) {
		this.annotStmts = annotStmts;
	}

	public List<AnnotationStatement> getAnnotStmts() {
		return annotStmts;
	}
	
	
	
}
