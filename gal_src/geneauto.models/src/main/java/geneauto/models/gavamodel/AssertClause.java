/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assert Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getAssertClause()
 * @model
 * @generated
 */
public class AssertClause extends SimpleVAElement {
} // AssertClause
