package geneauto.models.gavamodel.statement;

import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * ExistsStatement implements \exists in the program annotations 
 * The components of Exists statement are:
 * <ul>
 * <li>lstVars -- a list of variables used in the annotation</li>
 * <li>lstRanges -- a list of ranges for each variable</li>
 * <li>expr -- an expression: the body of the annotation</li>
 * </ul>
 */
public class Exists extends VAQuantifier
{
	private List<VariableExpression> lstVars;
	private List<RangeExpression> lstRanges;
	
	public Exists(){
		this.lstRanges = new ArrayList<RangeExpression>();
		this.lstVars = new ArrayList<VariableExpression>();
	}
	
	public Exists(VariableExpression var, RangeExpression range){
		this.lstRanges = new ArrayList<RangeExpression>();
		this.lstVars = new ArrayList<VariableExpression>();
		lstRanges.add(range);
		lstVars.add(var);
	}
	
	public Exists(List<VariableExpression> vars, List<RangeExpression> ranges){
		this.lstRanges = ranges;
		this.lstVars = vars;
	}
	
	public void setLstVars(List<VariableExpression> lstVars) {
		this.lstVars = lstVars;
	}
	
	public List<VariableExpression> getLstVars() {
		return lstVars;
	}
	
	public void setLstRanges(List<RangeExpression> lstRanges) {
		this.lstRanges = lstRanges;
	}
	
	public List<RangeExpression> getLstRanges() {
		return lstRanges;
	}
}
