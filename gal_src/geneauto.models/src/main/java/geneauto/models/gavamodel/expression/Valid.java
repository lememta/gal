package geneauto.models.gavamodel.expression;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.VariableExpression;

/**
 * ValidStatement implements \valid in the program annotations 
 * The components of Valid statement are:
 * <ul>
 * <li>var -- a code model variable to valid</li>
 * </ul>
 */
public class Valid extends Expression
{
	
	private VariableExpression var;
	
	public Valid(){
		
	}
	
	public Valid(VariableExpression var){
		this.var = var;
	}
	
	public void setVar(VariableExpression var) {
		this.var = var;
	}
	
	public VariableExpression getVar() {
		return var;
	}
	
}
