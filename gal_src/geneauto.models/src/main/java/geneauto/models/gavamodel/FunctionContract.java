/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package geneauto.models.gavamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contract</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.emf.models.gavamodel.GavamodelPackage#getContract()
 * @model
 * @generated
 */
public class FunctionContract extends CompoundVAElement {
	
	public FunctionContract() {
		super();
	}
	
} // Contract
