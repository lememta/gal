package geneauto.models.gavamodel;

import java.util.ArrayList;
import java.util.List;

import geneauto.models.gacodemodel.NameSpace;

public class LetExpression extends VAElement {

	private NameSpace nameSpace;
	private List<VAElement> elements;
	
	public LetExpression(){
		super();
		this.nameSpace = new NameSpace();
		elements = new ArrayList<VAElement>();
	}
	
	public LetExpression(NameSpace nameSpace){
		super();
		this.nameSpace = nameSpace;
		elements = new ArrayList<VAElement>();
	}
	
	public void setNameSpace(NameSpace nameSpace) {
		this.nameSpace = nameSpace;
	}
	
	public NameSpace getNameSpace() {
		return nameSpace;
	}
	
	public void setExpression(List<VAElement> elements) {
		this.elements = elements;
	}
	
	public List<VAElement> getElements() {
		return elements;
	}
	
}
