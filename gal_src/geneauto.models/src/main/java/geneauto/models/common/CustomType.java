/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/common/CustomType.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.models.common;

import geneauto.models.gacodemodel.CustomTypeContent;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.genericmodel.Model;

import java.util.List;

/**
 * Generalises over the CustomType classes in system model and code model
 *
 */
public interface CustomType extends NameSpaceElement {
    
    /**
     * @return members of Custom type if contents is of StructureMemberContent
     *         type null - if not 
     *        
     */
    public List<StructureMember> getMembers();

    public StructureMember getMemberByName(String p_name);
    
    public boolean equalsTo(CustomType other);
    
	/** 
	 * This is not null, when a common initial value is specified for all
	 * elements of this type
	 */
    public Expression getInitialValue();
    
    public Model getModel();
    
    public CustomTypeContent getContent();
}
