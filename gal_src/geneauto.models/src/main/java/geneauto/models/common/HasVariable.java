package geneauto.models.common;

/**
 * Interface for all elements that has Variable
 * type argument
 * 
 * @author Andrei Rybinsky
 *
 */
public interface HasVariable {
	public Variable getVariable();
	public void setVariable(Variable var);
}
