/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/statemodel/broadcast/ExternalBroadcast.java,v $
 *  @version	$Revision: 1.27 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement.statemodel.broadcast;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.GACodeModel;
import geneauto.models.gacodemodel.TempModel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.models.gasystemmodel.gastatemodel.Event;
import geneauto.models.gasystemmodel.gastatemodel.EventScope;
import geneauto.models.utilities.BackendUtilities;

import java.util.LinkedList;
import java.util.List;

/**
 * This is a broadcast made outside the Stateflow chart - the event being
 * broadcast is an "output event." It is different from other kind of broadcasts
 * in that it causes no evaluation of any parts of the chart. It cannot be also
 * listened to in the chart. But, if the event's (=port's) trigger type is function-
 * call, then the external (Simulink) system attached to it gets executed and the
 * input variables of the chart have immediately new values for the rest of the
 * chart's evaluation.
 *
 */
public class ExternalBroadcast extends BroadcastStatement {

	public ExternalBroadcast() {
	    super();
	}
	
    public ExternalBroadcast(Event event) {
        super(event);
    }

    /**
     * Overrides the superclass method to get the type right
     * 
     * (non-Javadoc)
     * 
     * @see geneauto.models.genericmodel.GAModelElement#getCopy()
     */
    public ExternalBroadcast getCopy() {
        return (ExternalBroadcast) super.getCopy();
    }

    /**
     * Conversion to a Basic CodeModel construct
     * 
     * Note: The current implementation allows only one Statement to be
     * returned. Hence, it must be a CompoundStatement. This can be optimised
     * out in the Printer or elsewhere. Otherwise this implementation should 
     * be changed.
     */
    public List<Statement> toBasicCodeModel() {
        
        // Locate the related OutControlPort
        if (getEvent() == null) {
            EventHandler.handle(EventLevel.ERROR, "ExternalBroadcast", "",
                    "Event reference is null.\n Element:"
                            + getReferenceString());
            return null;            
        }
        
        Variable_CM var = getEvent().getCodeModelElement();
        if (var == null) {
            EventHandler.handle(EventLevel.ERROR, "ExternalBroadcast", "",
                    "CodeModel element missing." 
            			+ "\n Event:  " + getEvent().getName() 
                        + "\n Element:" + getReferenceString());
            return null;            
        }
        if (!(var instanceof Variable_CM)) {
            EventHandler.handle(EventLevel.ERROR, "ExternalBroadcast",
                    "",
                    "Expecting a CodeModel variable, but got: "
                            + var.getReferenceString() + "\nElement:"
                            + getReferenceString());
            return null;            
        }
        
        GASystemModelElement sel = ((Variable_CM) var).getSourceElement();
        if (sel == null) {
            EventHandler.handle(EventLevel.ERROR, "ExternalBroadcast",
                    "",
                    "Source element is null. Relation to the SystemModel is broken." 
                        + "\nElement:" + getReferenceString());
            return null;            
        }
        if (!(sel instanceof Event)) {
            EventHandler.handle(EventLevel.ERROR, "ExternalBroadcast",
                    "",
                    "Expecting an Event, but got: "
                            + sel.getReferenceString() + "\nElement:"
                            + getReferenceString());
            return null;            
        }
        Event e = (Event) sel;
        if (e.getScope() != EventScope.OUTPUT_EVENT) {
            // TODO (to AnTo) Add reference to modelling rule
            EventHandler.handle(EventLevel.ERROR, "ExternalBroadcast", "",
                    "Modelling rule violation. Event broadcasts are only allowed to be made to output ports."
                    + "\n Element: " + getReferenceString()
                    + "\n Event: "   + e.getReferenceString());
            return null;            
        }        
        OutControlPort p = e.getOutControlPort(); 
        if (p == null) {
            EventHandler.handle(EventLevel.ERROR, "ExternalBroadcast", "",
            		"OutControlPort is null." 
                        + "\nEvent: "  + sel.getReferenceString() 
                        + "\nElement:" + getReferenceString());
            return null;            
        }
        
        // Retrieve the statements associated with the port and make code
        // TODO improve the method to accept also NameSpace elements
        // TODO Maybe it would be better to manipulate with a CompoundStatement instead of the Pair? We need to make a copy of it anyway.
        List<Statement> stmts = 
        	BackendUtilities.getControlPortComputationCode(p).getRight();
        
        // If no statements, then we assume that there is nothing connected to
        // the port
        if (stmts == null || stmts.isEmpty()) {
            String comnt = "Sending event " + e.getName()
                    + " to OutControlPort " + p.getName()
                    + " skipped, because there is nothing connected to the port.";
            stmts = new LinkedList<Statement>();
            stmts.add(new BlankStatement(comnt));
            return stmts;
        }
        
        String annot = "Sending event " + e.getName() + " to OutControlPort";

        // Create copy of these statements before return
        List<Statement> StmtsCopy = new LinkedList<Statement>();
        StmtsCopy.add(new BlankStatement(annot));
        for (Statement s : stmts) {
            StmtsCopy.add(s.getCopy());
        }
        
		// The NamedConstant that was created simply for maintaining the link
		// with the original SystemModel Event can now be removed. However, we
        // cannot simply remove it entirely at this point, as the TempModel might 
        // have references to it. So, instead we put it also in the TempModel.
        // Check, before that it isn't in the TempModel already
        if (!(var.getParent() instanceof TempModel)) {
        	// Store reference to the CodeModel
        	GACodeModel cm = (GACodeModel) var.getModel(); 
        	// Delete the variable from the old NameSpace
        	var.removeMe();
        	// Move it to the TempModel
        	cm.getTempModel().addElement(var);
        }
        
        return StmtsCopy;        
    }

}