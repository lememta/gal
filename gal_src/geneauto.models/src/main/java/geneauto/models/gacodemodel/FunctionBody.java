/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/FunctionBody.java,v $
 *  @version	$Revision: 1.20 $
 *	@date		$Date: 2011-11-29 22:24:00 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.gaenumtypes.FunctionStyle;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.map.ConstantMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract representation of the function's body. The current package defines
 * two kinds of function body: SequentialFunctionBody and ExpressionFunctionBody
 */
public abstract class FunctionBody extends GACodeModelElement {

    public FunctionBody() {
        super();
    }

    /**
     * Printing method with additional function style argument
     * 
     * @param FunctionStyle
     *            style
     * @return
     */
    public String print(FunctionStyle style) {
        GACodeModelElement.printUnsupportedMethodError(this, "print");
        return null;
    }

    /**
     * Method getNameSpace. Dummy function in this class. Not applicable to all
     * subclasses
     */
    public NameSpace getNameSpace() {
        GACodeModelElement.printUnsupportedMethodError(this, "getNameSpace");
        return null;
    }

    public void setNameSpace(NameSpace nameSpace) {
        printUnsupportedMethodError(this, "setNameSpace");
    }

    /**
     * @return an output variable, if existing.
     */
    public abstract Variable_CM getOutputVariable();    

    /**
     * Method getStatements. Dummy function in this class. Not applicable to all
     * subclasses
     */
    public List<Statement> getStatements() {
        printUnsupportedMethodError(this, "getStatements");
        return null;
    }

    /**
     * Method setStatements. Dummy function in this class. Not applicable to all
     * subclasses
     */
    public void setStatements(List<Statement> statements) {
        printUnsupportedMethodError(this, "setStatemenents");
    }

    /**
     * Method addStatements. Dummy function in this class. Not applicable to all
     * subclasses
     */
    public void addStatements(List<Statement> statements) {
        printUnsupportedMethodError(this, "addStatements");
    }

    /**
     * Adds statement to function body and sets statement parent Dummy function
     * in this class. Not applicable to all subclasses
     */
    public void addStatement(Statement statement) {
        printUnsupportedMethodError(this, "addStatement");
    }
    
    /**
     * goes through the function body and resolves references to function
     * arguments
     */
    abstract public void resolveReferencesToArguments();

    /**
     * compiles a name map of arguments of the given function
     * @param f
     * @return
     */
    protected ConstantMap<String, GAModelElement> argumentsToMap(Function_CM f) {
    	
    	Map<String, GAModelElement> map = new HashMap<String, GAModelElement>();
    	for (FunctionArgument_CM fa : f.getArguments()) {
    		map.put(fa.getName(), fa);
    	}
    	
    	return new ConstantMap<String, GAModelElement>(map);
    }

//    /**
//     * Override default behaviour in this class to adjust the fact that 
//     * currently the function argument namespace is not above the function body
//     *   
//     * @return the containing NameSpace of a CodeModelElement or null, if it is missing
//     */
//    @Override
//    public NameSpace getContainingNameSpace() {
//    	Function_CM f = getContainingFunction();    	
//        if (f != null) {
//            return f.getNameSpace();
//        } else {
//            return null;
//        }
//    }

 
    /**
	 * Specific adaptation to cope that the Function.nameSpace (NameSpace of 
	 * FunctionArguments) isn't hierarchically on top of the function body.
	 */
    @Override
    public NameSpace getContainingNameSpace() {
        if (parent instanceof Function_CM) {
            return ((Function_CM) parent).getNameSpace();
        } else {
        	// Unknown - A valid containing NameSpace is that of the Function
            return null;
        }
    }

   
}