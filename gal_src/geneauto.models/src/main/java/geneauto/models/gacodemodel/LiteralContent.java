/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/LiteralContent.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.expression.Expression;

/**
 * This class represents custom type (content) that is given as a string and the
 * code generator does not analyze its structure.
 * 
 * Fields: value = <target language type>
 * 
 * C output: typedef <value> <name of the CustomType>;
 * 
 */
public class LiteralContent extends CustomTypeContent {
    /**
     * Target language equivalent of the custom type: e.g. "char"
     */
    protected String value;

    
    
    public LiteralContent(String value) {
        super();
        this.value = value;
    }

    public LiteralContent() {
        super();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Literal content never has initial value
     */
    @Override
    public Expression getInitialValue() {
        return null;
    }
    
	@Override
	public boolean equalsTo(CustomTypeContent other) {
		if (!(other instanceof LiteralContent)) {
			return false;
		}
		return value.equals(((LiteralContent) other).getValue());
	}
    
    
}