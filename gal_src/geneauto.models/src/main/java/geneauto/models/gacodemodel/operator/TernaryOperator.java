/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/operator/TernaryOperator.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.operator;


/**
 * An operator with three arguments
 *
 */
public enum TernaryOperator implements Operator{
	/**
	 * Ternary If. E.g. in C:
	 *   x ? 1 : 0
	 */
	TERNARY_IF_OPERATOR(3);
	
	/**
	 * Precedence level of the operator. Higher numeric value binds tighter.
	 */
	private int precedence;

	/**
	 * @param precedence    precedence
	 */
	private TernaryOperator(int precedence) {
		this.precedence = precedence;
	}
	
	public String getName() {
	    return getClass().getSimpleName() + ": " + name();
	}

	public int getPrecedence() {
		return this.precedence;
	}
	
	public String printElement(String firstOperandPart, String secondOperandPart,
            String thirdOperandPart) {
    String code = "";
    switch (this) {
    case TERNARY_IF_OPERATOR:
        code = firstOperandPart + " ? " + secondOperandPart + " : "
                + thirdOperandPart;
        break;
    default:
        return "UNKNOWN TERNARY OPERATOR";
    }
    return code;
	}
	
	public boolean assertTypeValidity() {
		/*
		 * TODO This check accepts any typing, which is probably too weak. 
		 * Note that if also a negative return is added, then also an error 
		 * needs to be raised!
		 */
	    return true;
	}
	
}