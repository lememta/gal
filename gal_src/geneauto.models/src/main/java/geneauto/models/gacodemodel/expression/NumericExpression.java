package geneauto.models.gacodemodel.expression;

import geneauto.models.gadatatypes.GADataType;

public abstract class NumericExpression extends LiteralExpression {

    public NumericExpression() {
        super();
    }

    public NumericExpression(String litValue, GADataType dataType) {
        super(litValue, dataType);
    }

    public NumericExpression(String litValue) {
        super(litValue);
    }
    

// FIXME Probably a redundant method. Check and remove.    
//    /**
//     * Parses litValue. Defines the value of
//     * attribute isScientific and sets the value of the 
//     * exponent if it is defined
//     */
//    protected void splitLitValue() {
//        Assert.assertNotNull(litValue, 
//                "litValue. Element: " + getReferenceString());
//        if (litValue.toLowerCase().contains("e")) {
//            scientificValue = true;
//            try {
//                int eIndex = litValue.toLowerCase().indexOf("e");
//                String exponentString = litValue.substring(
//                        eIndex + 1);
//                exponentString.replace("+", "");
//                exponent = Integer.parseInt(
//                        exponentString);
//            } catch (NumberFormatException nfe) {
//                // TODO print an error here?
//                return;
//            }
//        } else {
//            scientificValue = false;
//        }
//    }
    
	// FIXME Probably a redundant method. Check and remove.   
	//    // TODO improve this part in the future
	//    // replace try-catch blocks with regular expressions
	//    protected void splitLitValue() {
	//        super.splitLitValue();
	//        GADataType dt = parseRealType();
	//        setDataType(dt);
	//
	//        if (dt == null) {
	//            // An error has been raised already
	//            return;
	//        }
	//        
	//        // assume first that the value is positive
	//        isNegative = false;
	//        
	//        String value = litValue;
	//        if (isScientificValue()) {
	//            value = value.substring(
	//                    0, value.toLowerCase().indexOf("e"));
	//        }
	//        String[] litValueParts = value.split("\\.");
	//        if (litValueParts.length == 1) {
	//            try {
	//                setIntegerPart(
	//                        Integer.parseInt(litValueParts[0]));
	//                setFractionalPart("0");
	//            } catch (NumberFormatException nfe) {
	//                // TODO print error here?
	//                return;
	//            }
	//        } else if (litValueParts.length == 2) {
	//            try {
	//                setIntegerPart(
	//                    Integer.parseInt(litValueParts[0]));
	//                setFractionalPart(litValueParts[1]);
	//            } catch (NumberFormatException nfe) {
	//                // TODO print error here?
	//                return;
	//            }
	//            
	//        } else {
	//            // TODO print error here
	//            return;
	//        }
	//    }

    /** A 'double' representation of a given numeric value */
    public abstract double getRealValue();

	/** Toggles the sign of the expression. */
	public abstract void toggleSign();
	
	
}
