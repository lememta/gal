/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/BlankStatement.java,v $
 *  @version	$Revision: 1.10 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.models.genericmodel.Annotation;

/**
 * BlankStatement is used for encoding labels or annotations that are not
 * related to a specific code model element as an Annotation object. If such an
 * annotation exists in the input model or is created during the code generation
 * process, a BlankStatement is created and an annotation and/or a label is
 * attached to this empty statement object.
 */
public class BlankStatement extends Statement {

	/**
	 * Default constructor
	 */
	public BlankStatement() {
		super();
	}	

	/**
	 * @param String - Annotation string
	 */
	public BlankStatement(String st) {
		super(new Annotation(st));
	}

    /**
     * Number of empty lines to be printed before this Statement (i.e. before
     * any annotations or labels).
     */
	protected int emptyLinesBefore;
	
    public int getEmptyLinesBefore() {
        return emptyLinesBefore;
    }

    public void setEmptyLinesBefore(int emptyLinesBefore) {
        this.emptyLinesBefore = emptyLinesBefore;
    }
	
	@Override
    public boolean isEffectFree() {
        return true;
    }

    @Override
    public boolean isEmptyStatement() {
        return true;
    }
    
}
