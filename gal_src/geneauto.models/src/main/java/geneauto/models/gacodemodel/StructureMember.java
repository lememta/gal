/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/StructureMember.java,v $
 *  @version	$Revision: 1.43 $
 *	@date		$Date: 2010-10-01 13:52:12 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.genericmodel.Model;

/**
 * Member of a structure
 * 
 * TODO (AnTo) Not sure, if this class should extend Variable. A StructureMember
 * is more part of a type definition.
 */
public class StructureMember extends Variable_CM {

    public StructureMember() {
        super();
    }

    /* 
     * Model element should get link with model from its parent
     * not forced by constructor! (ToNa 03/08/09) 
     */
    @Deprecated 
    public StructureMember(Model m) {
        this();
        setModel(m);
    }

    /*
     * Structure members have global scope in code model
     * 
     * (non-Javadoc)
     * 
     * @see geneauto.models.genericmodel.GAModelElement#isReferable()
     */
    public boolean isReferable() {
        return true;
    }

    public void setConst(boolean isConst) {
        printFieldNotApplicableError(this, "isConst", "setConst");
    }

    public boolean isVolatile() {
        printFieldNotApplicableError(this, "isVolatile", "isVolatile");
        return false;
    }

    public void setVolatile(boolean isVolatile) {
        printFieldNotApplicableError(this, "isVolatile", "setVolatile");
    }

    public DefinitionScope getScope() {
        printFieldNotApplicableError(this, "scope", "getScope");
        return null;
    }

    public boolean isAutoInit() {
        printFieldNotApplicableError(this, "isAutoInit", "isAutoInit");
        return false;
    }

    /**
     * In case of the parent of current structure member is 
     * CustomType_CM return true, if the type is imported.
     * 
     * In all other cases simply return the value of the isFixedName flag    
     */
    @Override
    public boolean hasFixedName() {
    	CustomType type = this.getContainingCT();
    	if (type instanceof CustomType_CM) {
    		CustomType_CM type_cm = (CustomType_CM) type; 
            if (type_cm.getScope() == DefinitionScope.IMPORTED_DECLARED) {
                return true;
            } else if (type_cm.getScope() == DefinitionScope.IMPORTED) {
                return true;
            } 
    	}
    	
        return isFixedName;
    }

    public void setAutoInit(boolean autoInit) {
        printFieldNotApplicableError(this, "isAutoInit", "setAutoInit");
    }

    public void setScope(DefinitionScope scope) {
        printFieldNotApplicableError(this, "scope", "setScope");
    }

    public boolean equalsTo(StructureMember other) {
    	if (!getName().equals(other.getName())) {
    		return false;
    	}
    	
    	return getDataType().equalsTo(other.getDataType());
    }
    
    /**
     * Removes given structure member from model and from the corresponding
     * CustomType,
     * 
     * Removal is not permitted when custom type is not local 
     */
    public boolean removeFromNameSpace() {
    	/* memorise custom type content (it will not be available later as
    	 * we remove the current element from model
    	 */
    	StructureContent structCont = (StructureContent) this.getParent();
    	if (structCont == null) {
    		// already removed
    		return false;
    	}
    	DefinitionScope scope =  ((CustomType_CM) structCont.getParent()).getScope();
    	NameSpace ns = this.getParentNameSpace();
    	if (scope != DefinitionScope.LOCAL) {
    		// only local structure types can be modified
    		return false;
    	}
    	
    	/* remove element from the model */
    	removeMe();
    	
    	/* if removing from structure, check if the type still has some members */
    	if (structCont != null) {
    		if (!(structCont.hasMembers())) {
    			/* no members, remove the type */
    			structCont.getParent().removeMe();
    			/* if there is variable with that data type in current 
    			 * namespace, delete that too
    			 * 
    			 * TODO: this is a hack required for the optimiser -- when 
    			 * reference counting is correct, all not used variables 
    			 * corresponding to a data type shall be removed later 
    			 */
    			ns.deleteCustomVar((CustomType_CM) structCont.getParent());
    		}
    	}    	
    	
    	return true;
    }
 
    /**
     * 
     * @return CustomType object, that owns this StrutureMember element
     */
    public CustomType getContainingCT() {
    	return ((CustomTypeContent) getParent()).getContainingCT();
    }
    
    /**
     * Suffixes the element name with "#<ordinal>" where <ordinal>
     * is the sequence number with identical names
     * 
     * It is assumed that "#" is illegal symbol or identifiers in 
     * all output languages and name normalising in printer will remove it.
     * We use special symbol to be able to unmangle the name for comparisons   
     * 
     * @param ordinal -- the suffix to be added
     */
    public void mangleName(int ordinal) {
    	setName(name + "#" + ordinal);
    }
    
    /**
     * determines if name1 and name2 are equal after removing the suffix
     * starting with "#". 
     * 
     *  NB! in case there is several "#" symbols in the string the leftmost is 
     *  used. Thus "foo" == "foo#1" == "foo#1#2#3"
     *  
     * @param name1
     * @param name2
     * @return
     */
    public boolean equalBeforeMangling(String name) {
    	String regEx = "([^#]*)(#.*)?";
    	return this.name.replaceAll(regEx, "$1")
    					.equals(name.replaceAll(regEx, "$1"));
    }

	/* (non-Javadoc)
	 * @see geneauto.models.gacodemodel.Variable_CM#getReferenceExpression(geneauto.models.gacodemodel.GACodeModelElement)
	 */
	@Override
	public Expression getReferenceExpression(
			GACodeModelElement instanceContainer) {
	
		// try to locate the reference to a variable that contains
		// current structure member
		Expression varRef = null;
		if (instanceContainer instanceof Expression) {
			// the argument is already a reference to the container
			varRef = (Expression) instanceContainer;
		} else if (instanceContainer instanceof Variable_CM) {
			varRef = ((Variable_CM) instanceContainer).getReferenceExpression(null);
		} else if (instanceContainer instanceof Function_CM) {
            // Seek for a function argument with given type
        	CustomType memberType = this.getContainingCT();
        	for (FunctionArgument_CM arg : 
        					((Function_CM)instanceContainer).getArguments()){
        		GADataType argBaseType = arg.getDataType().getPrimitiveType();
        		
    			if (argBaseType instanceof TCustom
    				&& ((TCustom) argBaseType).getCustomType().equalsTo(memberType)) {
    				varRef = arg.getReferenceExpression(null);
    				break;
        		}
        	}
        } else if (instanceContainer instanceof NameSpace) {
            // Retrieve the structure having the given data type
            Variable_CM varCm = ((NameSpace) instanceContainer).
            						getVariableByType(this.getContainingCT());
            if (varCm != null) {
            	varRef = varCm.getReferenceExpression(null);
            }   	
		}


		if (varRef == null) {
			// variable instance not found, return null
			return null;
		} else {
			// compose member expression 
			return new MemberExpression(this, varRef);
		}
	}
    
    
}