/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/VariableExpression.java,v $
 *  @version	$Revision: 1.118 $
 *	@date		$Date: 2012-01-07 17:46:38 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.Function_CM;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.utilities.CodeModeUtilities;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.common.SystemVariable;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.NameResolvingContext;
import geneauto.utils.GAConst;
import geneauto.utils.assertions.Assert;
import geneauto.utils.map.ConstantMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Reference to a Variable object. The reference can be given either directly or
 * by giving a name in the name field. In the last case the name should match
 * the name of some existing Variable object. In a final model there should not be
 * any by-name references. If both: a direct reference and name are given, then
 * the direct reference has priority.
 */
public class VariableExpression extends Expression implements LValueExpression {

    /**
     * Direct reference to a Variable object in the NameSpace.
     */
    protected Variable variable;

    /**
     * Default constructor of the class
     */
    public VariableExpression() {
        super();
    }

    /**
     * Constructor
     * 
     * @param variable
     */
    public VariableExpression(Variable variable) {
        super();
        setVariable(variable);
    }

    public VariableExpression(String referenceString) {
        this();
        setName(referenceString);
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
        if ((variable.getDataType() != null) && (getIndexExpressions().isEmpty())) {
            dataType = variable.getDataType().getCopy();
            dataType.setParent(this);
        }
    }
    
	/**
	 * @return the name of the variable, if present, otherwise, the name of the
	 *         expression
	 */
    public String getVariableName() {
    	if (variable != null) {
    		return variable.getName();
    	} else {
    		return name;
    	}
    }

    @Override
    public Expression normalizeShape(boolean full) {        
        if (getVariable() == null) {
            EventHandler.handle(EventLevel.ERROR, 
                    getClass().getSimpleName() + ".normalizeType()", "", 
                    "Cannot perform normalisation, because the variable is unresolved." +
                    "\n Expression: " + this.getReferenceString());            
            return null;
        }
        // Normalise variable
        getVariable().normalizeType(full);
        // Reset the previously computed type of the expression itself
        setDataType(null);
        return this;        
    }    

    public boolean isConst() {
        Assert.assertNotNull(variable, "field variable is empty");
        if (getVariable() != null) {
            return getVariable().isConst();
        } else {
            return false;
        }
    }

    public Expression eval(boolean strict, boolean optimizableOnly) {
        // Validate that the variable reference is not empty
        Assert.assertNotNull(
        		getVariable(), 
        		getClass().getCanonicalName()+ ".eval(..) getVariable() returns null" 
        		 	+ "\n  Element: " + getReferenceString(), 
        		strict);
        if (getVariable() == null) {
            return null;
        }

        // If the variable is a constant, then evaluate its initial value
        // expression
        if (!getVariable().isConst()) {
            // No error is returned. The null result can be used for 
            // detecting that the value cannot be statically evaluated 
            // (it is not a constant).  
            return null;
            
        } else if (optimizableOnly && !getVariable().isOptimizable()) {
            // Do not evaluate unoptimisable (constant) data, if this isn't allowed.  
            return null;
            
        } else {
            Assert.assertNotNull(variable, "field variable is empty");
            if (getVariable().getInitialValueExpression() != null) {
                Expression result = 
                        getVariable().getInitialValueExpression().eval();
                return result;
            } else {
                return null;
            }
        }
    }

    @Override
    public NumericExpression evalReal() {
    	NumericExpression result;
    	// FIXME (to AnTo) 120912 Strange code commented out. 
    	// Is it really necessary to numerically evaluate booleans? 
    	// Cleanup after testing
    	
//        if (getDataType() instanceof TBoolean) {
//            result = getVariable()
//                .getInitialValueExpression().evalReal();
//            if (result.getRealValue() != 0.0) {
//                result.setLitValue("1.0");
//            }
//        } else {
            result =  super.evalReal();
//        }
        return result;
    }

    @Override
    public void resetDataType() {
        if (variable != null) {
            attachDataType(variable.getDataType());
            checkIndexExpressions();
            
        }
    }

    /**
     * Resolves by-name references in the current element only. See also:
     * resolveReferences
     * 
     * @param ConstantMap
     *            <String, GAModelElement> nameMap
     */
    @Override
    public GAModelElement resolveCurrentElement(
            final ConstantMap<String, GAModelElement> nameMap) {

        // Resolve only, when the variable field is empty
        if (this.getVariable() == null) {
            
            // Ignore special identifiers in parameters. These must be resolved separately
            if (NameResolvingContext.isBlockParameterContext()
                    && NameResolvingContext
                    	.getBlockParameterContext()
                    	.isSpecialIdentifier(this.getName())) {
                return null;  
            }
            
            // Try to resolve the by-name reference.
            GAModelElement el = nameMap.get(this.getName());
            if (el != null) {
                Variable sel;
                if (el instanceof Variable) {
                    sel = (Variable) el;

                    // Bind the Expression with the found object.
                    setVariable(sel);
                } else if (el instanceof Parameter) {
                    Parameter param = (Parameter) el;
                    Expression parVal = param.getValue();
                    Assert.assertNotNull(parVal, 
                            "Missing value of following parameter: " 
                            + param.getReferenceString());
                    Expression valueExp = parVal;
                    if (valueExp == null) {
                        EventHandler.handle(EventLevel.ERROR,
                                getClass().getSimpleName() + ".resolveCurrentElement", 
                                "",
                                "Unable to evaluate expression from "
                                        + "following DataValue: "
                                        + "Current element: "
                                        + parVal.getReferenceString());
                        return null;
                    }
                    
                    valueExp = valueExp.getCopy();
                    valueExp.resolveReferences(nameMap, getModel());
                    if (getParent() != null) {
                        replaceMe(valueExp);
                    }
                    return valueExp;
                    
                } else {
    				if (nameMap.containsKey(this.getName())) {
    					EventHandler.handle(EventLevel.ERROR,
    							getClass().getSimpleName() + ".resolveReferencesInCurrent", 
    								"", 
    								"Multiple definitions for variable " 
    								+ this.getName() 
    								+ ". Can not resolve reference by name.\n"
    								+ "Current element: " + getReferenceString());
    				} else {
                        EventHandler.handle(EventLevel.ERROR,
                                getClass().getSimpleName() + ".resolveCurrentElement", 
                                "GEM0079",
                                "Expecting IsSystemVariable: "
                                        + el.getReferenceString() + "\n"
                                        + "Current element: "
                                        + getReferenceString());
    				}			
                }
            } 
            
            boolean varUndiefined = false;
            if (el == null) {
            	
            	/*
            	 * Check if variable name is "_state_ and is from exported function. 
            	 * Generate specific error message in this case
            	 */
            	if (getName() != null && getName().equals(GAConst.VAR_MEM_NAME)) {
            		Function_CM fun = getContainingFunction();
            		if (fun != null && fun.getScope() == DefinitionScope.EXPORTED) {
            			EventHandler.handle(EventLevel.ERROR, "resolveCurrentElement", "", 
                				"Variable " + getName() 
                				+ " is referring to the state of a chart of an exported function."
                				+ " Exported charts cannot reference chart's state. Function: "
                				+ fun.getName()
                				+ "\n  Current element: " + getReferenceString());
            		} else {
            			varUndiefined = true;
            		}
            	} else {
            		varUndiefined = true;
            	}
            }
            if (varUndiefined) {
                EventHandler.handle(EventLevel.ERROR,
                        getClass().getSimpleName() + ".resolveCurrentElement", 
                        "GEM0080",
                        "Variable " + getName() + " undefined.\n"
                                + "Current element: " + getReferenceString());
            }
        }
        return null;
    }

    @Override
    public Expression toCodeModel() {

        // If the "variable" field points to a SystemModel Variable.
        if (getVariable() instanceof SystemVariable) {
            SystemVariable sel = (SystemVariable) getVariable();
            GAModelElement cRef = sel.getReferenceExpression();
            if (cRef != null) {
                if (cRef instanceof Expression) {
                    /*
                     * Replace the original VariableExpression in the parent
                     * with the reference expression obtained from the matched
                     * element in the map. 
                     * A copy is needed, because generally the reference 
                     * expression can be required by others too.
                     */
                    Expression newExp = ((Expression) cRef).getCopy();
                    
                    // Move index expressions to the new element
                    newExp.setIndexExpressions(this.getIndexExpressions());
					// Cleanup the old expression. Even, when the old expression
					// isn't used any more, this helps to check model integrity.
                    this.setIndexExpressions(null); 
                    
                    for (Expression e : newExp.getIndexExpressions()) {
                        e.toCodeModel();
                    }

                    // Replace the old object with the new one in the parent's
                    // fields
                    if (getParent() != null) {
                        replaceMe(newExp);
                    }
                    return newExp;
                } else {
                    EventHandler.handle(EventLevel.ERROR,
                            getClass().getSimpleName() + ".toCodeModel", "GEM0081",
                            "Expecting: Expression, but got: " + sel.getClass()
                                    + "\n" + "Current element: "
                                    + getReferenceString());
                    return null;
                }
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        getClass().getSimpleName() + ".toCodeModel", "GEM0082",
                        "ReferenceExpression undefined in: "
                                + ((GAModelElement) sel).getReferenceString()
                                + "\n" + "Current element: "
                                + getReferenceString());
                return null;
            }
        }

        else {
            return this;
        }
    }

    @Override
    public List<Variable> getReferencedVariables() {
        List<Variable> varList = new ArrayList<Variable>();
        varList.add(variable);
        return varList;
    }

    public Variable getExpressionVariable() {
        return variable;
    }

    public String printElement(int precLevel) {
        String result = "";
        Assert.assertNotNull(variable, "variable is empty");
        if (this.getVariable() != null) {
        	if (getVariable() instanceof Variable_CM) {
        		result = ((Variable_CM) this.getVariable()).printElement();
        	} else {
        		result = getVariable().getName();
        	}
        } else {
            result = "<VARIABLE UNDEFINED>";
        }
        return result + Expression.printIndexPart(this);
    }

    @Override
    public boolean isRowMtrx(Expression mtrxToCmpr, boolean isLeftExpr) {
        return CodeModeUtilities.isRowMtrx(this, mtrxToCmpr, 
                isLeftExpr);
    }

    @Override
    public String toString() {
        String s = super.toString();
        if (getVariable() != null) {
            s += " Variable: " + getVariable().getName();
        }
        return s;
    }

    @Override
    public String getReferenceString() {
        String s = super.getReferenceString();
        if (getVariable() != null) {
            s += " Variable: " + getVariable().getName();
        }
        return s;
    }

    @Override
    public boolean nameEquals(String name) {
        if (getName() != null) {
            return getName().equals(name);
        } else {
            return false;
        }
    }
 
    @Override
    /**
     * Name of the variable expression has special meaning in case the variable
     * expression is used as template for implicit argument value. In this case
     * the template expression is sought by the name of function argument it 
     * is supposed to replace.
     * 
     * Example. We have two functions 
     *    fun1 (int x) {
     *    	fun2(x);
     *    }
     *    
     *    fun2 (int y) {
     *      ...
     *    }
     * 
     * When the argument of fun2 is marked implicit, then it is assumed that 
     * each function that calls fun2 should have a template expression referring 
     * to the argument value that should be passed. This expression should be 
     * named "y"
     *
     * Another assumption is, that the implicit argument value can be only 
     * VariableExpression or MemberExpression
     * 
     * If the variable field is assigned, then the name of the variable has 
     * priority over the name of the expression.
     */
    public String getName() {
    	if (variable != null) {
    		return variable.getName();
    	} else {
        	return super.getName();
    	}
    }

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		if (!isScalar()) {
			result.add(this);
		}
		return result;
	}
    
}