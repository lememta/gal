/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/LValueExpression.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2011-10-04 11:16:57 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.models.common.HasVariable;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.genericmodel.GAModelElement;

/**
 * Expressions that can exist on the left side of an assignment.
 */
public interface LValueExpression extends HasVariable {
    
    public GAModelElement getParent();
    public GADataType getDataType();
    public void setDataType(GADataType dt);
    public void addIndexExpression(Expression exp);

    /**
     * TODO (to AnRo) Method comments are missing. Don't understand the meaning
     * of the arguments. Check, if the methods in the Expression class and
     * GADataType class (the shape can be determined by checking the shape of
     * the datatype) would be sufficient. Add method comments, if this method is
     * required.
     * 
     * @param mtrxToCmpr
     * @param isLeftExpr
     * @return
     */
    public boolean isRowMtrx(Expression mtrxToCmpr, boolean isLeftExpr);
    
    /**
     * 
     * @return statement that contains current expression
     */
    public Statement getContatiningStatement();
    
    /**
     * Performs type/shape normalisation of the Expression.
     * 
     * Full normalisation means that row and column matrixes (vector-matrixes)
     * are converted to vectors and all singleton arrays (vectors, matrixes,
     * arrays) with only one element are converted to scalars.
     * 
     * Partial normalisation only converts singleton arrays.
     * 
     * @param full
     *            If true, then row and column matrixes (vector-matrixes) are
     *            converted to vectors.
     * @return a converted expression or null on error.
     */
    public Expression normalizeShape(boolean full);
    
}


