/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/operator/UnaryOperator.java,v $
 *  @version	$Revision: 1.41 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.operator;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPointer;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.utils.PrivilegedAccessor;

/**
 * 
 */
public enum UnaryOperator implements Operator {

    /**
     * *x
     */
    DEREF_OPERATOR(15, true),

    /**
     * &x
     */
    REF_OPERATOR(15, true),

    /**
     * !x
     */
    NOT_OPERATOR(15, true),

    /**
     * ~x
     */
    COMPLEMENT_OPERATOR(15, true),

    /**
     * Type cast The UnaryExpression with CastOperator takes the expression data
     * type and converts expression argument to that type
     */
    CAST_OPERATOR(15, true),

    /**
     * Unary plus +x
     */
    UNARY_PLUS_OPERATOR(15, true),

    /**
     * Unary minus -x
     */
    UNARY_MINUS_OPERATOR(15, true)

    ;

    /**
     * Precedence level of the operator. Higher numeric value binds tighter.
     */
    private int precedence;

    /**
     * Specifies whether the unary operator is printed before or after the
     * expression.
     */
    private boolean isPrefix;

    /**
     * @param precedence
     * @param isPrefix
     * 
     * @param precedence
     * @param isPrefix
     *            isPrefix
     */
    private UnaryOperator(int precedence, boolean isPrefix) {
        this.precedence = precedence;
        this.isPrefix = isPrefix;
    }

    public int getPrecedence() {
        return this.precedence;
    }

    public String getName() {
        return getClass().getSimpleName() + ": " + name();
    }

    public boolean isPrefix() {
        return isPrefix;
    }

    /**
     * Converts an EML (Embedded Matlab Language) operator string to a Gene-Auto
     * operator.
     * 
     * Note: Here EML means the "general Matlab family action/expression
     * language". There are slight differences in the meaning and availability
     * of the operators in the Matlab scripting language, Simulink and
     * Stateflow.
     * 
     * @param operatorString
     *            string representation of the operator
     * @param bitwise
     *            determines, whether the logical operators are to be treated
     *            bitwise or logically.
     * @return Gene-Auto operator or null, when the string is not recognised.
     */
    public static UnaryOperator fromEML(String operatorString, boolean bitwise) {
        if ("+".equals(operatorString)) {
            return UNARY_PLUS_OPERATOR;
        } else if ("-".equals(operatorString)) {
            return UNARY_MINUS_OPERATOR;
        } else if ("~".equals(operatorString)) {
            if (bitwise) {
                return COMPLEMENT_OPERATOR;
            } else {
                return NOT_OPERATOR;
            }
        } else if ("!".equals(operatorString)) {
            return NOT_OPERATOR;
        } else if ("&".equals(operatorString)) {
            return REF_OPERATOR;
        } else if ("*".equals(operatorString)) {
            return DEREF_OPERATOR;
        } else {
            // No warnings or errors here as we use the same method for
            // statement type detection
            return null;
        }
    }

    /**
     * Infer datatype of an UnaryExpression (operator + argument)
     * TODO Consider moving this method to the UnaryExpression class (AnTo 100510)
     * @param argument
     * @return
     */
    public GADataType getDataType(Expression argument) {
    	GADataType argDT = argument.getDataType();
        GADataType baseType = null;
        
        if (!argDT.isScalar()) {
            if (argDT instanceof TArray) {
                baseType = ((TArray) argDT).getBaseType();
            }
        }
        
        switch (this) {
        case UNARY_PLUS_OPERATOR:
        case UNARY_MINUS_OPERATOR:
            if ((argDT instanceof TRealNumeric)
                    || (baseType instanceof TRealNumeric)) {
                return argDT;
            } else {
                return null;
            }
            
        case NOT_OPERATOR:
            TBoolean booleanDT = (TBoolean) PrivilegedAccessor.getObjectInPackage(TBoolean.class, argDT);
            GADataType resultDT = argDT.getCopy();
            if (resultDT.isScalar()) {
                return booleanDT;
            } else if (resultDT instanceof TArray) {
                TArray arrayDT = (TArray) resultDT;
                arrayDT.setBaseType(booleanDT);
                return arrayDT;
            } else {
                return null;
            }
            
        case COMPLEMENT_OPERATOR:
            if ((argDT instanceof TBoolean) || (argDT instanceof TRealInteger)
                    || (baseType instanceof TBoolean)
                    || (baseType instanceof TRealInteger)) {
                return argDT;
            } else {
                return null;
            }
            
        case CAST_OPERATOR: 
        	// Always an error - Only explicit typing is meaningful.
        	// If the parent was typed, then this method should not have been called!
        	{  // Local compound statement to encapsulate parentRef
	        	String parentRef = ""; 
	        	if (argument.getParent() != null) {
	        		parentRef = "n   Expression: " + argument.getParent().getReferenceString(); 
	        	}
	        	EventHandler.handle(EventLevel.ERROR, 
	        			getClass().getSimpleName() + ".getDataType(Expression argument)", "", 
	        			"Cannot infer data type of the cast operation. " +
	        			"Cast expression must be explicitly typed!"
	        			+ parentRef
	        			+ "\n  Operator: " + this.toString()
	        			+ "\n  Argument: " + argument.getReferenceString());
	        	return null;
        	}
        	
        case REF_OPERATOR:
        	if (argument instanceof VariableExpression || argument instanceof MemberExpression) {
        		if (argDT != null) {
        			return new TPointer(argDT);
        		} else {
        			return null; // Undefined
        		}
        	} else {
            	// Always an error - Only referencing VariableExpression and MemberExpression is legal
            	EventHandler.handle(EventLevel.ERROR, 
            			getClass().getSimpleName() + ".getDataType(Expression argument)", "", 
            			"Cannot infer data type of the reference operation. " +
            			"Only referencing VariableExpression and MemberExpression is legal!"
            			+ "\n  Operator: " + this.toString()
            			+ "\n  Argument: " + argument.getReferenceString());
            	return null;
        	}
        	
        case DEREF_OPERATOR:
    		if (argDT != null) {
    			if (argDT instanceof TPointer) {
    				return ((TPointer) argDT).getBaseType();
    			} else {
                	// Always an error - Only dereferencing pointers is legal
                	EventHandler.handle(EventLevel.ERROR, 
                			getClass().getSimpleName() + ".getDataType(Expression argument)", "", 
                			"Cannot infer data type of the dereference operation. " +
                			"Only dereferencing an expression of type TPointer is legal!"
                			+ "\n  Operator: " + this.toString()
                			+ "\n  Argument: " + argument.getReferenceString());
                	return null;
    			}
    		} else {
    			return null; // Undefined
    		}
    		
    	default: 
            // Case mismatch
        	String parentRef = ""; 
        	if (argument.getParent() != null) {
        		parentRef = "n   Expression: " + argument.getParent().getReferenceString(); 
        	}
        	EventHandler.handle(EventLevel.CRITICAL_ERROR, 
        			getClass().getSimpleName() + ".getDataType(Expression argument)", "", 
        			"Cannot infer data type of an unary operation. " +
        			"Unexpected operator!"
        			+ parentRef
        			+ "\n  Operator: " + this.toString()
        			+ "\n  Argument: " + argument.getReferenceString());
        	return null;
        }
    }
    
    /**
     * Prints the current element in pseudo code or event messages
     * 
     * @param outerPrecedence precedence of the outer expression
     * 
     * @return pseudo code of the element
     */
    public String printElement(UnaryExpression expression) {
        String code;
        switch (this) {
        case DEREF_OPERATOR:
            code = "*";
            break;

        case REF_OPERATOR:
            code = "&";
            break;

        case NOT_OPERATOR:
            code = "!";
            break;

        case COMPLEMENT_OPERATOR:
            code = "~";
            break;

        case CAST_OPERATOR:
            // Note: This is the default case. Conversion to the boolean
            // type is handled separately.
        	String dtStr;
        	try {
        		dtStr = expression.getDataType().getTypeName();
        	} catch (Exception e) {
        		dtStr = "<DATA TYPE NOT KNOWN>";
        	}
            code = "(" + dtStr + ") ";
            break;

        case UNARY_PLUS_OPERATOR:
            code = "+";
            break;

        case UNARY_MINUS_OPERATOR:
            code = "-";
            break;

        default: // @requirement TR-CP-UP10
            EventHandler.handle(EventLevel.ERROR, "", "CP0080",
                    "Operator " + getClass().getName()
                            + " does not have a printing rule.", "");
            code = "NO PRINTING RULE";

        }
        return code;
    }
    
    /**
     * Checks if argument data type is suitable
     * for given operator
     *  
     * @param expression	-- argument expression
     * @return true when data type is suitable for composing an expression
     * false otherwise
     */
   public boolean assertTypeValidity(Expression expression) {
	   boolean result = getDataType(expression) != null;
	   if (!result) {
           EventHandler.handle(EventLevel.CRITICAL_ERROR,
                   getClass().getName() + ".assertTypeValidity(Expression expression)", 
                   "",
                   "Type assertion failed" + 
                   "\n Expression: " + expression.getReferenceString());		   
	   }
       return result;
    }

}