/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/RangeIterationStatement.java,v $
 *  @version $
 *	@date $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

package geneauto.models.gacodemodel.statement;

import geneauto.models.common.HasVariable;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.HasNameSpace;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.NameSpaceElement_CM;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.PrivilegedAccessor;

/**
 * RangeIterationStatement expresses a loop over fixed number of steps. While 
 * the ForStament allows to define dynamic end condition evaluated on each step,
 * the RangeIterationStatemnt is guaranteed to go through all steps in the 
 * range.
 */
public class RangeIterationStatement extends LoopStatement
        implements HasNameSpace, HasVariable {
    /**
     * expression fixing the start and the end of the iteration
     */
    protected RangeExpression range;
    /**
     * namespace used in case the itrator uses local iterator variable
     */
    protected NameSpace nameSpace;
    /**
     * reference to the iterator variable
     */
    protected Variable_CM iteratorVariable;

    public RangeIterationStatement() {
        super();
    }

    public RangeIterationStatement(Statement bodyStatement,
            Variable_CM iteratorVariable, NameSpace nameSpace,
            RangeExpression range) {
        super();
        setBodyStatement(bodyStatement);
        setIteratorVariable(iteratorVariable);
        setNameSpace(nameSpace);
        setRange(range);
    }
    
    public RangeIterationStatement(Statement bodyStatement,
            Variable_CM iteratorVariable,
            RangeExpression range) {
        this(bodyStatement, iteratorVariable, new NameSpace(), range);
    }

    public RangeExpression getRange() {
        return range;
    }

    public void setRange(RangeExpression range) {
        if (range != null) {
            range.setParent(this);
        }
        
        // set correct data type to iterator variable if it is defined
        if (iteratorVariable != null) {
            TRealInteger defaultDT = (TRealInteger) PrivilegedAccessor.getObjectInPackage(TRealInteger.class, 
                    iteratorVariable);
            DataTypeUtils.scaleFor(defaultDT, range.getStart(), range.getEnd());
            GADataType varDT = iteratorVariable.getDataType();
            if (defaultDT != null && varDT == null) {
                iteratorVariable.setDataType(defaultDT);
            } else if (defaultDT != null && varDT != null) {
            	GADataType newVarDt = DataTypeUtils.chooseLeastCommonType(defaultDT, varDT, false);
            	iteratorVariable.setDataType(newVarDt);
            }
        }
        this.range = range;
    }

    public NameSpace getNameSpace() {
        if (nameSpace == null) {
            NameSpace newNameSpace = (NameSpace) PrivilegedAccessor
                .getObjectInPackage(NameSpace.class, 
                    this);
            setNameSpace(newNameSpace);
        }
        return nameSpace;
    }

    public void setNameSpace(NameSpace nameSpace) {
        if (nameSpace != null) {
            nameSpace.setParent(this);
        }
        this.nameSpace = nameSpace;
    }
    
    public void addNameSpaceElement(NameSpaceElement_CM element) {
        if (nameSpace == null) {
            NameSpace newNameSpace = (NameSpace) PrivilegedAccessor
                    .getObjectInPackage(NameSpace.class, 
                            element);
            setNameSpace(newNameSpace);
        }
        nameSpace.addElement(element);
    }

    public Variable_CM getIteratorVariable() {
        return iteratorVariable;
    }

    public void setIteratorVariable(Variable_CM iteratorVariable) {
        if (iteratorVariable != null) {
            getNameSpace().addElement(iteratorVariable);
        }
        this.iteratorVariable = iteratorVariable;
    }

	@Override
	public Variable getVariable() {
		return getIteratorVariable();
	}

	@Override
	public void setVariable(Variable var) {
		if (var instanceof Variable_CM) {
			setIteratorVariable((Variable_CM) var);
		}
	}
}
