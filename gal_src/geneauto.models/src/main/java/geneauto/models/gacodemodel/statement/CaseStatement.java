/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/CaseStatement.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2011-10-11 11:06:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.models.gacodemodel.expression.Expression;

import java.util.ArrayList;
import java.util.List;

/**
 * Case statement. Similar to the C switch, but a) has no fall through - all
 * cases are implicitly terminated b) "when" expressions can be arbitrary
 * expressions. Note: depending on the target language, not all expressions are
 * legal. E.g. in C only singleton expressions are legal. However, in Ada a list
 * expression is also legal.
 * 
 */
public class CaseStatement extends Statement implements StatementWithBranches{

	/** Expression to be tested. */
	protected Expression condition;
    /** List of conditional alternatives. */
	protected List<WhenCase> whenCases = new ArrayList<WhenCase>();
    /** Default alternative. */
	protected DefaultCase defaultCase;

	public CaseStatement() {
		super();
	}

	public CaseStatement(Expression condition, List<WhenCase> whenCases,
			DefaultCase defaultCase) {
		super();
		setCondition(condition);
		
		for(WhenCase whencase : whenCases){
			if(whencase != null){
				whencase.setParent(this);	
			}			
		}
		this.whenCases = whenCases;
		
		if(defaultCase != null){
			defaultCase.setParent(this);	
		}		
		this.defaultCase = defaultCase;
	}

	
	/**
	 * sets condition expression. If given expression is part of another element 
	 * (has parent) a copy of the expression is made
	 * 
	 * @param condition
	 */
	public void setCondition(Expression condition) {
		if(condition != null){
			if (condition.getParent() == null){
				this.condition = condition;
			}
			else{
				this.condition = (Expression) condition.getCopy();
			}
			this.condition.setParent(this);
		}
		else
			this.condition = null;
	}

	public void setWhenCases(List<WhenCase> whenCases) {
		for(WhenCase whencase : whenCases){
			if(whencase != null){
				whencase.setParent(this);	
			}			
		}
		this.whenCases = whenCases;
	}

	public void setDefaultCase(DefaultCase defaultCase) {
		if(defaultCase != null){
			this.defaultCase = defaultCase;	
		}		
	}

	public Expression getCondition() {
		return condition;
	}

	public DefaultCase getDefaultCase() {
		return defaultCase;
	}

	public List<WhenCase> getWhenCases() {
		return whenCases;
	}

	@Override
	public List<Statement> getAllSubStatements() {
		List<Statement> stList = new ArrayList<Statement>();
		for (WhenCase whCase: getWhenCases()) {
			stList.add(whCase);
			if (!whCase.getAllSubStatements().isEmpty()) {
				stList.addAll(whCase.getAllSubStatements());
			}
		}
		if (getDefaultCase() != null) {
			stList.add(getDefaultCase());
			if (!getDefaultCase().getAllSubStatements().isEmpty()) {
				stList.addAll(getDefaultCase().getAllSubStatements());
			}
		}
		return stList;
	}

    @Override
    public boolean containsStmts() {
        return true;
    }

    @Override
    public int setSequenceNumber(int number) {
        super.setSequenceNumber(number);
        for (WhenCase st: whenCases) {
            number = st.setSequenceNumber(++number);
        }
        if (defaultCase != null) {
            number = defaultCase.setSequenceNumber(++number);
        }
        return number;
    }

	@Override
	public List<Statement> getBranches() {
		List<Statement> result = new ArrayList<Statement>();
		for (WhenCase whenCase: whenCases) {
			result.add(whenCase);
		}
		if (defaultCase != null) {
			result.add(defaultCase);
		}
		return result;
	}
    
    
}