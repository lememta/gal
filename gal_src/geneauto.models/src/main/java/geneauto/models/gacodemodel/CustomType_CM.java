/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/CustomType_CM.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-12 13:30:23 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gadatatypes.TCustom;

import java.util.ArrayList;
import java.util.List;

/**
 * User-defined type that must be explicitly defined (typedef-d) in the
 * generated code, before it can be used. See also TCustom
 */
public class CustomType_CM extends NameSpaceElement_CM 
		implements CustomType {

    protected CustomTypeContent content;

    public CustomType_CM() {
        super();
    }

    /**
     * @param content
     * @param scope
     */
    public CustomType_CM(CustomTypeContent content, DefinitionScope scope) {
        this();
        content.setParent(this);
        this.content = content;
        this.scope = scope;
    }

    public CustomTypeContent getContent() {
        return content;
    }

    public void setContent(CustomTypeContent content) {
        content.setParent(this);
        this.content = content;
    }

    public List<StructureMember> getMembers() {
        List<StructureMember> result = new ArrayList<StructureMember>();
        result.addAll(getContent().getStructMembers());
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }

    public StructureMember getMemberByName(String p_name) {
        return getContent().getMemberByName(p_name);
    }
    
	/** 
	 * This is not null, when a common initial value is specified for all
	 * elements of this type
	 */
    public Expression getInitialValue() {
    	return content.getInitialValue();
    }    

	@Override
	public boolean equalsTo(CustomType other) {
		if (!(other instanceof CustomType_CM)) {
			return false;
		}
		if (getName() != null 
		        && other.getName() != null
		        && (!(getName().equals(other.getName())))) {
		    return false;
		}
		
		return content.equalsTo(((CustomType_CM) other).getContent());
	}

    /**
     * Function that tests whether one custom type depends on another.
     * 
	 * Types are interdependent, if one uses the other in its definition. 
	 * Normally, this should not be mutual.
	 * 
	 * return  1 when the current type depends on the other type,
	 *        -1 when the other type depends on the current type,
	 * 		   0 when the two types are independent
	 */
	public int dependsOf(CustomType_CM ct2) {
    	
    	// Check if the other type is a pre-requirement to this type
		if (getContent() instanceof ArrayContent) {
			ArrayContent arContent = (ArrayContent) getContent();
			if (arContent.getBaseType() instanceof TCustom) {
        		if (((TCustom) arContent.getBaseType()).getCustomType().equals(ct2)) {
        	    	// The other type is a pre-requirement to this type
        	    	return 1;
        		}
			}
		}
		
		// Check if this is a pre-requirement to the other type
		if (ct2.getContent() instanceof ArrayContent) {
			ArrayContent arContent = (ArrayContent) ct2.getContent();
			if (arContent.getBaseType() instanceof TCustom) {
				if (((TCustom) arContent.getBaseType()).getCustomType().equals(this)) {
	    	    	// Current type is a pre-requirement to the given type
	    	    	return -1;
	    		}
			}
		}
		
    	// Independent
		return 0;
	}
 
}