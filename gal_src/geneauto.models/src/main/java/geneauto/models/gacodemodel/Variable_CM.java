/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/Variable_CM.java,v $
 *  @version	$Revision: 1.73 $
 *	@date		$Date: 2011-07-12 13:30:35 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.utilities.DataTypeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract entity representing a variable.
 */
public class Variable_CM extends NameSpaceElement_CM 
	implements CodeModelVariable {

    /**
     * Initial value of the variable, if supplied.
     */
    protected Expression initialValue;
    /**
     * When this flag is set then the variable isn't modifiable during runtime.
     */
    protected boolean isConst;
    /**
     * Static or not (C sense).
     */
    protected boolean isStatic;
    /**
     * Volatile or not.
     */
    protected boolean isVolatile;

    // DO NOT SET ANY INITIAL TYPE HERE!!!
    protected GADataType dataType;

    /**
     * When this attribute is "true", the variable is assigned initialValue when
     * created. If initialisValue is NULL, the dafaultInitialValue of data type
     * is used.
     */
    protected boolean autoInit = true;
	/**
	 * Typically the arguments defined explicitly by the user are not
	 * optimisable. Automatically generated function arguments can become
	 * redundant, although this is not known, when the function signature is
	 * generated.
	 * 
	 * NOTE: Independently from this flag, the variable becomes also
	 * unoptimisable, when it has observation points! 
	 */
    protected boolean isOptimizable = true;
    
    /**
     * This list contains the names of external 
     * test points that expose the value of this variable
     */
    protected  List<String> observationPoints 
        = new ArrayList<String>();

    /**
     * Default constructor of the class
     */
    public Variable_CM() {
        super();
        isConst = false;
        // DO NOT ASSUME ANYTHING ABOUT THE TYPE HERE!!!
        // Only explicit typing allowed!
        // Null is allowed here also. In that case the 
        // type shall be defined later!     
    }

    /**
     * Construct for variable of type int and name varName
     * 
     * @param varName
     *            name of the variable
     */
    public Variable_CM(String varName) {
        this();
        setName(varName);
        // DO NOT ASSUME ANYTHING ABOUT THE TYPE HERE!!!
        // Only explicit typing allowed!
        // Null is allowed here also. In that case the 
        // type shall be defined later!     
    }

    /**
     * Constructor of the class
     * 
     * @param name
     * @param initialValue
     * @param isConst
     * @param isStatic
     * @param isVolatile
     * @param scope
     * @param dataType
     */
    public Variable_CM(String name, 
    		Expression initialValue, 
    		boolean isConst,
            boolean isStatic, 
            boolean isVolatile, 
            DefinitionScope scope,
            GADataType dataType) {
        this();
        setName(name);
        setInitialValue(initialValue);
        this.isConst = isConst;
        this.isStatic = isStatic;
        this.isVolatile = isVolatile;
        setScope(scope);
        setDataType(dataType);
    }

    /*
     * Variables have global scope in code model
     * 
     * (non-Javadoc)
     * 
     * @see geneauto.models.genericmodel.GAModelElement#isReferable()
     */
    public boolean isReferable() {
        return true;
    }

    /**
     * 
     * @param noDefault flag, that shows either to use default initial 
     * value of data type or not
     * 
     * @return value of initialValue attribute (noDefault = true) 
     * or default initialValue (noDefault = false)
     */
    public Expression getInitialValue(boolean noDefault) {
    	Expression exp = initialValue;
		// If the initialValue is null, then see, if the datatype is a custom
		// type that has a fixed initialValue
    	if (exp == null && !noDefault) {
    		GADataType dt = getDataType();
    		exp = dt.getDefaultValue();
    	}
        return exp;
    }
    
    public Expression getInitialValue() {
        return getInitialValue(false);
    }

    public Expression getInitialValueExpression() {
        return getInitialValue();
    }

    public void setInitialValue(Expression initialValue) {
        if (dataType != null 
                && initialValue != null
                && initialValue.getDataType() != null) {
            if (!DataTypeUtils.isValidAssignment(dataType, initialValue.getDataType())) {
                EventHandler.handle(EventLevel.ERROR, "setInitialValue", "", 
                        "Supplied value for the initial value has incompatible " +
                        "type for the current variable."
                        + "\nVariable: " + getReferenceString()
                        + "\nVariable datatype: " + dataType.toString()
                        + "\nValue: " + initialValue.toString()
                        + "\nValue datatype: " + initialValue.getDataType().toString()                        
                        );
                return;
            }
        }
        if (initialValue != null) {
            initialValue.setParent(this);
        }
        this.initialValue = initialValue;
    }

    public void setInitialValueExpression(Expression exp) {
        setInitialValue(exp);
    }

    public boolean isConst() {
        return isConst;
    }

    public void setConst(boolean isConst) {
        this.isConst = isConst;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public boolean isVolatile() {
        return isVolatile;
    }

    public void setVolatile(boolean isVolatile) {
        this.isVolatile = isVolatile;
    }

    public GADataType getDataType() {
        return dataType;
    }

    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    public void setDataType(GADataType dataType) {
        if (dataType != null){
            if (dataType.getParent() == null) {
                this.dataType = dataType;
            } else {
                this.dataType = dataType.getCopy();
            }
            this.dataType.setParent(this);
        }
        else
            this.dataType = null;
    }

    /**
     * @return the string that is used to reference the variable
     */
    public String getInstance() {
        printUnsupportedMethodError(this, "getInstance");
        return null;
    }

    public boolean isAutoInit() {
        return autoInit;
    }

    public void setAutoInit(boolean autoInit) {
        this.autoInit = autoInit;
    }

	/**
	 * @return the flag isOptimizable
	 * 
	 * NOTE: Independently from this flag, the variable becomes also
	 * unoptimisable, when it has observation points! 
	 */
    public boolean isOptimizable() {
        return isOptimizable;
    }

    public void setOptimizable(boolean isOptimizable) {
        this.isOptimizable = isOptimizable;
    }

    /**
     * Prints the current element in pseudo code or event messages
     * 
     * @param outerPrecedence precedence of the outer expression
     * 
     * @return pseudo code of the element
     */
    public String printElement() {
        return getName();
    }

    @Override
    public void normalizeType(boolean full) {
        
        // Update initial value of the variable
        Expression initValExp = getInitialValueExpression(); 
        if (initValExp != null) {
            initValExp = initValExp.normalizeShape(true);
        }        
        setInitialValueExpression(initValExp);
        
        // Update type of the variable
        setDataType(getDataType().normalize(true));
    }
 
    /**
     *  Returns expression referring to the current Variable_CM element
     *  	- in case of a StructureMember the expression is MemberExpression
     *  	- in any other case VariableExpression is composed
     *  
     *  This method here composes the default Variable expression. 
     *  StructureMember must override this methiod 
     *  
     *  @param instanceContainer ,
     *            in case of StructureMember it is the container of the variable 
     *            instantiated from the CustomType where StructureMember 
     *            belongs. When the container is Function, we assume the 
     *            sought variable is FuncitonArgument, if it is a NameSpace
     *            we assume it is a Variable_CM. 
     *            NB! It is assumed here that in the given container there is 
     *            only one instance of given custom type.

     * @param instanceContainer
     * 
     * @return Expression
     */
    public Expression getReferenceExpression(
    		GACodeModelElement instanceContainer) {
    	
    	// compose variable expression and return it. instanceContainer is 
    	// ignored
        return new VariableExpression(this);
    }

    public List<String> getObservationPoints() {
        return observationPoints;
    }

    public void addObservationPoint(String observationPoint) {
    	// In case there are multiple instances of a test point
    	// with same name, then only add 1
    	// TODO AnTo 101108 Actually it should be better to avoid duplication 
    	// of test points (due to branching signals) in the importer
    	if (!observationPoints.contains(observationPoint)) {
    		observationPoints.add(observationPoint);
    	}
    }
    
    public void addObservationPoints(List<String> observarionPoints) {
        for (String observationPoint: observarionPoints) {
            addObservationPoint(observationPoint);
        }
    }

    /**
     * When this method returns true, the variable 
     * shall be made externally observable in generated code 
     * regardless of its scope. It is assumed that the 
     * observation mechanism protects the original 
     * value from modifications. For instance in C, 
     * a separate write-only variable is created to expose
     * the value of the observed variable
     */
    public boolean isObservable() {
        if (observationPoints.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Function that tests whether one variable depends on another.
     * 
	 * Variables are interdependent, if one uses the other as initial value. 
	 * This should never be mutual.
	 * 
	 * return  1 when the current variable depends on the other variable,
	 *        -1 when the other variable depends on the current variable,
	 * 		   0 when the two variables are independent
	 */
	public int dependsOf(Variable_CM var2) {
    	
    	// Check if the current variable contains the other variable in its initial value
		Expression ivExp = this.getInitialValue(true);
		List<Variable> refVars;
		if (ivExp != null) {
    		refVars = ivExp.getReferencedVariables();
    		if (refVars != null && refVars.contains(var2)) {
    			// The current variable depends on the other variable
				return 1;
    		}
		}
		
    	// Check if the other variable contains the current variable in its initial value
		ivExp = var2.getInitialValue(true);
		if (ivExp != null) {
    		refVars = ivExp.getReferencedVariables();
    		
    		if (refVars != null && refVars.contains(this)) {
    			// The other variable depends on the current variable
				return -1;
    		}
		}
		
		// Independent
		return 0;
	}
    
}