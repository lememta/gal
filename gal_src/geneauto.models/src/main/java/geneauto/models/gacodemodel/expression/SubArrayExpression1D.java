package geneauto.models.gacodemodel.expression;

import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.List;

/**
 * SubArrayExpression for 1 dimensional expression. 
 * It is sub expression of  expression from start (inclusive)
 * to end index (inclusive) of single dimension of expression
 * 
 * @author Andrei Rybinsky
 *
 */
public class SubArrayExpression1D extends SubArrayExpression {

	/**
	 * Index of expression that is start index of sub expression
	 */
	private int startIndex;
	
	/**
	 * Index of expression that is last index of sub expression
	 */
	private int endIndex;
	
	
	public SubArrayExpression1D() {
		super();
	}
	
	public SubArrayExpression1D(LValueExpression expression,
			int startIndex, int endIndex) {
		this();
		this.endIndex = endIndex;
		setExpression(expression);
		this.startIndex = startIndex;
	}
	
	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
	
	@Override
	public int getIndexExpressionStartIndex(int indExpIndex) {
		if (indExpIndex == 0) {
			return startIndex;
		} else {
			return super.getIndexExpressionStartIndex(indExpIndex);
		}
	}

	@Override
	public void resetDataType() {
		Variable var = getVariable();
		if (var != null) {
			GADataType varDT = expression.getDataType();
			if (varDT instanceof TArray) {
				TArray varArrayDtCopy = (TArray) varDT.getCopy();
				if (varArrayDtCopy.getDimensionality() == 1) {
					Expression dimExp = varArrayDtCopy
						.getDimensions().get(0);
					Integer newDim = new Integer(endIndex - startIndex + 1);
					IntegerExpression intExp = (IntegerExpression) PrivilegedAccessor
			        	.getObjectInPackage(IntegerExpression.class, this);
					intExp.setLitValue((new Integer(newDim)).toString());
					dimExp.replaceMe(intExp);
					attachDataType(varArrayDtCopy);
		            checkIndexExpressions();
				}
			} else if (varDT.isScalar()) {
				attachDataType(varDT.getCopy());
			}
		}
	}

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		if (expression != null) {
			result.addAll(((Expression) expression).getExpendableAtoms());
		}
		return result;
	}
	
	@Override
	public void setVariable(Variable var) {
		if (var instanceof Variable_CM) {
			setVariable((Variable_CM) var);
		}
	}
}
