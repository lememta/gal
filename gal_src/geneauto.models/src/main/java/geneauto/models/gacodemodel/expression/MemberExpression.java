/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/MemberExpression.java,v $
 *  @version	$Revision: 1.97 $
 *	@date		$Date: 2012-01-07 17:20:36 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.CustomType_CM;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.utilities.CodeModeUtilities;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gadatatypes.TPointer;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;
import geneauto.utils.map.ConstantMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Reference to a member of a structure. 
 * 
 * Examples (pseudo code):
 * a)	structS.fieldF;		 // Direct reference to a variable
 * 							 //   expression = structS
 * 
 * b)	pointerP->fieldF;    // Implicit dereference via pointer
 * 							 //   expression = pointerP
 * 
 * c) 	(*pointerP).fieldF;  // Explicit dereference via pointer
 * 							 //   expression = dereferenced pointerP
 * 
 * NOTE! See the exact constraints for the "expression" field under the field description. 
 * 
 */
public class MemberExpression extends Expression implements LValueExpression {
	
    /**
     * Reference to the structure member in the CustomType defining the
     * structure.
     */
    protected StructureMember member;
    
    /**
     * Expression referring to the structure variable instance. 
     * Legal expressions are of type
     * 		Structure (examples a, c),
     * 			i.e. TCustom (TCustom must point to a CustomType defining a structure)
     * 		Pointer to a structure (example b),
     * 			i.e. TPointer, with baseType TCustom (pointing to a CustomType as above)
     * 
     */
    protected Expression expression;

    public MemberExpression() {
        super();
    }

    /** 
     * @param member 
     * @param expression 
     */
    public MemberExpression(StructureMember member, Expression expression) {
        this();

        setMember(member);
        setExpression(expression);
    }

    public Expression getExpression() {
        return expression;
    }

    public StructureMember getMember() {
        return member;
    }

    public void setMember(StructureMember member) {
        this.member = member;
        if (member.getDataType() != null) {
            dataType = member.getDataType().getCopy();
            dataType.setParent(this);
        }
    }

    public void setExpression(Expression expression) {

        if (expression != null) {
            expression.setParent(this);
        }
        this.expression = expression;
    }

    public boolean isConst() {
        return false;
    }

    public boolean isConst(double p_value) {
        return false;
    }

    @Override
    public void resetDataType() {
    	if (expression != null) {
    		expression.resetDataType();
    	}
    	
        if (member != null) {
        	GADataType memDT = member.getDataType();
			// Note: Previously here came a null pointer exception. If null
			// datatype is still required, then the code must be modified to
			// allow it.
        	if (memDT == null) {
        		EventHandler.handle(EventLevel.CRITICAL_ERROR, 
        				getClass().getName()+ ".getDataType()", "", 
            			"Cannot infer the data type of the member field" 
            			+ "\n Expression: " + this.getReferenceString());
        	}
        	attachDataType(memDT);
            checkIndexExpressions();
        }
    }

    public void setIndexExpressions(List<Expression> indexExpressions) {
        super.setIndexExpressions(indexExpressions);

    }

    public Variable getExpressionVariable() {
        return member;
    }

    /**
     * 
     * @return CustomType related with current memberExpression
     */
    public CustomType_CM getCustomType() {
        GAModelElement parent = member.getParent();
        while (parent != null) {
            if (parent instanceof CustomType_CM) {
                return (CustomType_CM) parent;
            }
            parent = parent.getParent();
        }
        return null;
    }

    @Override
    public Variable getVariable() {
        return getMember();
    }

    public String printElement(int precLevel) {
        int innerPrecedence = 16;
        String memberPart = "";
        String expressionPart = expression.printElement(precLevel);
        if (member != null) {
            memberPart = member.getName();
        } else {
            Assert.assertNotNull(member, "member field is empty");

            // In case of no stop on error
            memberPart = "<MEMBER FIELD IS EMPTY>";
        }

        // Default case - "expression" refers directly to a structure
        String operatorPart = ".";
        
        // "expression" is of type TPointer
        if (expression.getDataType() instanceof TPointer) {
            operatorPart = "->";
            
        // "expression" a dereferenced TPointer           
        } else if (expression instanceof UnaryExpression) {
            if (((UnaryExpression) expression).getOperator() == UnaryOperator.DEREF_OPERATOR) {
                operatorPart = "->";
                expressionPart = ((UnaryExpression) expression).getArgument()
                        .printElement(precLevel);
            }
        }

        String code;
        code = expressionPart + operatorPart + memberPart
                + Expression.printIndexPart(this);

        // Set parenthesis, if required
        code = Expression.parenthesize(code, precLevel, innerPrecedence);
        return code;
    }

    @Override
    public GAModelElement resolveCurrentElement(
            final ConstantMap<String, GAModelElement> nameMap) {
        if (this.expression != null && this.member != null) {
            return null;
        } else if ((this.getName() != null) && this.getName().contains(".")) {
            String[] splitRes = this.getName().split("\\.");
            String varName = splitRes[0];

            // Try to resolve the by-name reference.
            GAModelElement el = nameMap.get(varName);

            Expression expression = null;
            if (el != null) {
                if (el instanceof Variable) {
                    Variable var = (Variable) el;

                    // Bind the Expression with the found object.
                    expression = new VariableExpression(var);
                } else if (el instanceof Parameter) {
                    VariableExpression varExp = new VariableExpression();
                    varExp.setName(varName);
                    expression = varExp;
                } else {
                    EventHandler.handle(EventLevel.ERROR,
                            "MemberExpression.resolveCurrentElement", "",
                            "Expecting Expression: " + el.getReferenceString()
                                    + "\n" + "Current element: "
                                    + getReferenceString(), "");
                }
            } else {
    			if (nameMap.containsKey(varName)) {
    				EventHandler.handle(EventLevel.ERROR,
    						getClass().getSimpleName() + ".resolveReferencesInCurrent", 
    							"", 
    							"Multiple definitions for variable " 
    							+ varName 
    							+ ". Can not resolve reference by name.\n"
    							+ "Current element: " + getReferenceString());
    			} else {
    				// error handling below 
    			}            	
            }
            
            
            if (expression != null) {
                GAModelElement resolvedElt = expression.resolveCurrentElement(nameMap);
                if (resolvedElt != null) {
                    expression = (Expression) resolvedElt;
                }
                getMemberExpression(expression, getName());
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        "MemberExpression.resolveCurrentElement", "",
                        "Unable to get expression attribute for MemberExpression: " 
                            + "\nExpression: " + this.getReferenceString());
            }
        }
        return null;
    }
    
    
    /**
     * Gets member expression by given variable expression
     * and full name (with points) of the member
     * 
     * @param exp expression that corresponds to the 
     * substring to the first point. E.g. in case of memberFullName = 
     * a.b.c returns variable expression the corresponds to a
     *  
     * @param memberNameFull member's name with points. E.g. a.b.c or a.b
     * 
     * @return member expression
     */
    private MemberExpression getMemberExpression(
            Expression exp, 
            String memberNameFull) {
        Expression expression;
        
        if(memberNameFull.contains(".")) {
            String expressionString = memberNameFull.substring(0, 
                    memberNameFull.lastIndexOf("."));
            String memberName = memberNameFull.substring(memberNameFull.lastIndexOf(".") + 1); 
            /*
             * memberNameFull has 2 or more points ->
             * expression is result of
             * getMemberExpression with substring to the
             * last point in memberNameFull, otherwise
             * expression is varExp
             */
            if (expressionString.contains(".")) {
                expression = getMemberExpression(exp, 
                        expressionString);
            } else {
                expression = exp;
            }
            setExpression(expression);
            GADataType expressionDT = expression.getDataType();
            if (expressionDT instanceof TCustom) {
                setMember(((TCustom) expressionDT)
                        .getMemberByName(memberName));
            }
            return this;
        } else {
            EventHandler.handle(EventLevel.ERROR, 
                    "getMemberExpression()", 
                    "",
                    "memberNameFull must contain at least 1 point."
                            + "\nmemberNameFull: " + memberNameFull
                            + "\nExpression: " + this.getReferenceString());
            return null;
        }
        
    }

    public boolean isRowMtrx(Expression mtrxToCmpr, boolean isLeftExpr) {
        return CodeModeUtilities.isRowMtrx(this, mtrxToCmpr, isLeftExpr);
    }

    @Override
    public String toString() {
        String s = super.toString();
        if (getMember() != null) {
            s += " member:" + getMember().getName();
        }
        return s;
    }

    @Override
    public Expression normalizeShape(boolean full) {
        if (getVariable() == null) {
            EventHandler.handle(EventLevel.ERROR, getClass().getSimpleName()
                    + ".normalizeType()", "",
                    "Cannot perform normalisation, because the member is unresolved."
                            + "\n Expression: " + this.getReferenceString());
            return null;
        }
        // Normalise variable
        getVariable().normalizeType(full);
        // Reset the previously computed type of the expression itself
        setDataType(null);
        return this;
    }

    @Override
    public boolean nameEquals(String name) {
        if (expression != null) {
            return expression.nameEquals(name);
        } else {
            return false;
        }
    }

	/** 
	 * Converts MemberExpression referring to system model elements to 
	 * appropriate code model constructs
	 */
	@Override
	public Expression toCodeModel() {

		// call parent class toCodeModel method to convert indexes
		super.toCodeModel();
		
		// nothing to convert, return
		if (member == null) {
			return this;
		}
		
		CustomType memberType = member.getContainingCT();
		if (memberType instanceof CustomType_SM) {
			CustomType_CM memberType_CM = 
				(CustomType_CM) ((CustomType_SM) memberType)
											.getCodeModelElement();
			if (memberType_CM == null) {
	            EventHandler.handle(EventLevel.ERROR, 
	            		getClass().getSimpleName()+ ".toCodeModel", "",
	                    "Can not convert MemberExpression to code model because " +
	                    "the  referred datatype is not converted yet"
	                    + "\n Expression: " + this.getReferenceString()				
	            		+ "\n Datatype: " + memberType.getReferenceString());
			} else {
				member = memberType_CM.getMemberByName(member.getName());
			}
		}
		
		// check if expression needs converting too
		expression = expression.toCodeModel();
		
		return this;
	}
	
    @Override
    /**
     * Name of the variable expression has special meaning in case the variable
     * expression is used as template for implicit argument value. In this case
     * the template expression is sought by the name of function argument it 
     * is supposed to replace.
     * 
     * Example. We have two functions 
     *    fun1 (int x) {
     *    	fun2(x);
     *    }
     *    
     *    fun2 (int y) {
     *      ...
     *    }
     * 
     * When the argument of fun2 is marked implicit, then it is assumed that 
     * each function that calls fun2 should have a template expression referring 
     * to the argument value that should be passed. This expression should be 
     * named "y"
     *
     * Another assumption is, that the implicit argument value can be only 
     * VariableExpression or MemberExpression
     * 
     * The current override of getName method is made solely for documenting 
     * this special semantics. It should not alter the functionality of the 
     * original getName method. 
     */
    public String getName() {
    	return super.getName();
    }
    
    @Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		if (!isScalar()) {
			result.add(this);
		}
		return result;
	}
    
    @Override
	public void setVariable(Variable var) {
		if (var instanceof StructureMember) {
			setMember(member);
		}
	}

}