/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/UnaryExpression.java,v $
 *  @version	$Revision: 1.77 $
 *	@date		$Date: 2011-09-21 07:10:23 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TPointer;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Unary Expression
 */
public class UnaryExpression extends Expression {

    protected Expression argument;
    protected UnaryOperator operator;

    public UnaryExpression() {
        super();
    }

    public UnaryExpression(Expression argument, UnaryOperator unaryOperator) {
        this();
        setArgument(argument);
        setOperator(unaryOperator);
    }

    public UnaryExpression(Expression argument, UnaryOperator unaryOperator,
            GADataType dataType) {
        this();
        setArgument(argument);
        setOperator(unaryOperator);
        setDataType(dataType);
    }

    public Expression getArgument() {
        return argument;
    }

    public void setArgument(Expression argument) {
        if (argument != null) {
            argument.setParent(this);
        }
        this.argument = argument;
    }

    public UnaryOperator getOperator() {
        return operator;
    }

    public void setOperator(UnaryOperator unaryOperator) {
        this.operator = unaryOperator;
    }

    public boolean isConst() {
        return argument.isConst();
    }

    @Override
    public void resetDataType() {
    	if (argument == null) {
    		// argument is not assigned yet
    		return;
    	}
    	
    	// reset argument datatype
        argument.resetDataType();
        GADataType argDataType = argument.getDataType();
        if (argDataType == null) {
        	// do not reevaluate the data type when argument data type is not known
        	return;
        }
        
        GADataType resDataType = null;
        if (operator == UnaryOperator.REF_OPERATOR) {
        	// in case of a ref operator, the data type is pointer to the
        	// argument data type
        	resDataType =(TPointer) PrivilegedAccessor
            						.getObjectInPackage(TPointer.class, this);
        	((TPointer)resDataType).setBaseType(argDataType);
        } else if (operator == UnaryOperator.DEREF_OPERATOR) {
        	// in case of deref operator the data type is base type of the 
        	// argument data type
            if (argDataType instanceof TPointer) {
            	resDataType =((TPointer) argDataType).getBaseType();
            } else {
                EventHandler.handle(
                        EventLevel.ERROR,
                        "UnaryExpression.getDataType", "GEM0076",
                        "DeRef operator applied to a non pointer type "
                        + "\n Data type: " 
                        + DataTypeAccessor.toString(argDataType)
                        + "\n Current element: "
                        + getReferenceString());
            }
        } else if (operator == UnaryOperator.CAST_OPERATOR) {
            // do not reevaluate data type if operator is cast operator!
        	if (argument.isScalar() && !isScalar()) {
        		resDataType = getDataType().getPrimitiveType();
        	} else {
        		return;
        	}
        	
        } else if (operator != null) {
        	// in all other cases set data type of the argument
        	resDataType = operator.getDataType(argument);
        } else {
        	// operator not defined, leave the data type unchanged
        	return;
        }
        
        attachDataType(resDataType);
    }
    
    public String printElement(int outerPrecedence) {
        String opPart;
        String argPart;
        boolean isPrefix;
        int innerPrecedence = 0;

        // Operator part
        if (operator != null) {
            innerPrecedence = operator.getPrecedence();
            
            opPart = operator.printElement(this);            
            isPrefix = operator.isPrefix();
            
        } else {
            this.printNoRequiredFieldError("operator");
            return null;
        }

        // Operand part
        Assert.assertNotNull(argument, "field arugment is missing");
        if (argument != null) {
            argPart = this.argument.printElement(innerPrecedence);
        } else {
            return null;
        }

        // Combine expression
        String code = opPart;
        if (!isPrefix) {
            code = argPart + opPart;
        } else {
            code = opPart + argPart;
        }
        
        code += Expression.printIndexPart(this);

        // Set parenthesis, if required
        code = Expression.parenthesize(code, outerPrecedence, innerPrecedence);

        return code;
    }

    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    @Override
    public void setDataType(GADataType dataType) {
        // NOTE! Following check is specific to the current class!
        if (operator == UnaryOperator.CAST_OPERATOR) {
            this.attachDataType(dataType);
        }
    }

    
    public String toString() {
        String opPart = "";
        if (getOperator() != null) {
            opPart = " op=\"" + getOperator().printElement(this) + "\"";
        }
        return super.toString() + opPart;
    } 

    public Expression eval(boolean printError, boolean optimizableOnly) {

        if (getValue() != null) {
            return getValue();
        }

        Assert.assertNotNull(argument, "argument is null. UnaryExpression: " 
                + getReferenceString());

        double d;
        NumericExpression argumentValue = argument.evalReal(printError, optimizableOnly);


        if (argumentValue == null) {
            // Assume the error was raised already, if required.
            return null;            
        }     

        switch (operator) {
        case UNARY_MINUS_OPERATOR:
            d = - argumentValue.getRealValue();
            break;
        case UNARY_PLUS_OPERATOR:
        case CAST_OPERATOR:
            d = argumentValue.getRealValue();
            break;

            // TODO Add other operators

        default:
            // TODO refine error
            if (printError) {
                printUnsupportedOperatorException(this, operator, "eval");
            }
            setValue(null);

            return getValue();
        }
        if ((int) d == d) {
            setValue(new IntegerExpression((int) d, getDataType()));
        } else {
            setValue(new DoubleExpression(d));
        }
        return getValue();
    }

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		if (argument != null) {
			result.addAll(argument.getExpendableAtoms());
		}
		return result;
	}
    
    
}