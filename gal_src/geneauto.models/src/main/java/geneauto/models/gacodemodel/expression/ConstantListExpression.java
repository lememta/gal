package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * List expression that consist of <n> same expressions, where 
 * <n> is value of length field
 * 
 * @author Andrei Rybinsky
 *
 */
public class ConstantListExpression extends ListExpression {
	/**
	 * Repeated expression 
	 */
	protected Expression expression;
	
	/**
	 * number of equal elements 
	 */
	protected int length;
	
	
	public ConstantListExpression() {
		super();
	}
	
	

	public ConstantListExpression(Expression expression, int length) {
		this();
		setExpression(expression);
		this.length = length;
	}

	@Override
	public TPrimitive getPrimitiveType() {
		Assert.assertNotNull(expression, "Exression is null");
		return expression.getPrimitiveType();
	}

	@Override
	public boolean isLiteral() {
		Assert.assertNotNull(expression, "Exression is null");
		return expression.isLiteral();
	}

	@Override
	public boolean isConst() {
		Assert.assertNotNull(expression, "Exression is null");
		return expression.isConst();
	}

	/**
     * Assert that the expression has some sub-expressions.
     */
    public boolean assertSubExpressionsExist() {
        if (getExpressions() == null || getExpressions().isEmpty()) {
            EventHandler.handle(EventLevel.ERROR, 
                    getClass().getSimpleName(), "", 
                    "Subexpression count is 0." +
                    "\n Expression: " + getReferenceString());
            return false;
        }
        return true;
    }
    
    @Override
	protected void resetChildrenDataTypes() {
    	if (expression != null) {
    		expression.resetDataType();
    	}
	}

	@Override
	protected void setIndexExpressionsToChildren(List<Expression> tmpIdxExpr) {
		if (expression != null) {
			expression.setIndexExpressions(tmpIdxExpr);
		}
	}



	public String printElement(int precLevel) {
        String result = "";
        
        
        // the expression contains index expression
        // take sub-element by first index and assume it will return 
        // its member by index too if there is more dimensions
        if (getIndexExpressions().size() > 0){
            Integer[] indexes = evaluateIndexes();
            result = getElementByIndex(indexes).printElement(precLevel);
        }
        // plain list expression without index. Print everything in curly 
        // brackets 
        else {
            result += "{length: " + length 
            	+ ", expression: " + expression.print(precLevel)+ "}";
        }
        
        return result;
    }
	
	public List<Expression> getDimensions() {
    	if (expression == null || length < 1) {
    		return Collections.emptyList();
    	} else {
    		List<Expression> dimList = new LinkedList<Expression>();
    		// the first dimension length is the number of expressions
    		IntegerExpression intExp = (IntegerExpression) PrivilegedAccessor.getObjectInPackage(IntegerExpression.class, this);
    		intExp.setLitValue("" + length);
    		dimList.add(intExp);
    		
			if (expression instanceof ListExpression) {
				dimList.addAll(((ListExpression) expression)
														.getDimensions());
			}
			
    		return dimList;
    	}
    }

	public Expression getExpression() {
		return expression;
	}
	
	@Override
	public List<Expression> getExpressions() {
		// TODO to be compatible with the function semantics, it should create 
		// dynamically a list of expressions - repeating the single expression 
		// as many times as there are dimensions (AnTo 120210) 
		EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getSimpleName() 
				+ ".getExpressions()", "", "Call to unimplemented method");
		return null;
	}
	
	public void setExpression(Expression expression) {
		if (expression.getParent() != null) {
            expression = expression.getCopy();
        }
		expression.setParent(this);
		this.expression = expression;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public int getSize() {
		return length;
	}



	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		result.add(this);
		if (expression != null) {
			result.addAll(expression.getExpendableAtoms());
		}
		return result;
	}
	
	
}
