/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/GACodeModel.java,v $
 *  @version	$Revision: 1.29 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class GACodeModel extends Model {

	/**
	 * List of top level model elements (those appearing under the "model" node in XML
	 * file). The elements in this list must be unique -- no two elements with the
	 * same type and name attribute value should appear in the list. ModelFactory must
	 * guarantee, that in case two elements with the same name are found in the same
	 * model file an error is thrown. In case of to overlapping elements in different
	 * models the last one read overrides previous element and warning is issued
	 * 
	 * NOTE! When the type of this collection is changed at least the following other 
	 * methods need to be updated also: 
	 * 	addElement(Object), addElement(<ElementType>), 
	 *  addElements(..), setElements(..), getElements(..) 
	 */	
    protected List<GACodeModelRoot> elements = new ArrayList<GACodeModelRoot>();
    
    /**
     * map of module elements in the current model as module is the main
     * packaging unit in the code model, it is important to have up-to date list
     * of all existing modules
     * 
     * The key of the string is module name and value is pointer to module
     * object
     */
    private Map<String, Module> modules = new HashMap<String, Module>();

    /**
     * Reference to a system model that is source for elements in this code
     * model
     * 
     * When code model contains GAModelElementReference type items, then they
     * are assumed to refer to elements in this system model
     */
    GASystemModel systemModel;

    /**
     * Temporary storage for model elements, that need to be stored for other
     * transformation steps but are not part of the final model that is
     * converted to C-code
     */
    TempModel tempModel;

    public GACodeModel() {
        super();
    }

    /**
     * Replaces a module (lookup by name)
     * 
     * @param module
     * 
     *            TODO: replace elements.remove(elt) with corresponding method
     *            in Model and make sure this method is redefined in GACodeModel
     *            to remove element also from modules map
     */
    public void replaceModule(Module module) {
        for (GACodeModelRoot elt : getElements()) {
            if (elt instanceof Module) {
                if (elt.getName().equals(module.getName())) {
                    elements.remove(elt);
                    addElement(module);
                    return;
                }
            }
        }
        EventHandler.handle(EventLevel.CRITICAL_ERROR,
                "GACodeModel.replaceModule", "GEM0051", "Module with a name "
                        + module.getName() + " not found.");

    }

    /**
     * @return module having given name
     * 
     * @param moduleName -- name of the searched module
     * 
     * @param createNew -- when true creates new module when the searched module
     * is not found
     * 
     * TODO: optimise this. currently it walks through the whole model each time
     */
    public Module getModuleByName(String moduleName, boolean createNew) {

        // check for existence of a module in map
        if (modules.containsKey(moduleName)) {
            return modules.get(moduleName);
        }

        // module was not found, create new, if the caller demands so
        if (createNew) {
            return createModuleByName(moduleName);
        } else {
            return null;
        }
    }

    /**
     * creates new module, adds it to the model, to the modules map and returns
     * the pointer to new element
     */
    public Module createModuleByName(String moduleName) {
        // check for existence of a module in map
        if (modules.containsKey(moduleName)) {
            return modules.get(moduleName);
        } else {
            Module m;

            // create new module
            m = new Module(moduleName);
            
            // add the module to model
            addElement(m);
            
            // add module to module map. get the name from module obejct
            // to be sure that we get correct name even if the Module
            // class modified it
            modules.put(m.getName(), m);
            return m;
        }
    }

    /**
     * Returns all Modules in the model. It is assumed, that modules can be only
     * top-level elements.
     */
    public List<Module> getModules() {
        List<Module> lst = new LinkedList<Module>();
        for (GACodeModelRoot e : elements) {
            if (e instanceof Module) {
                lst.add((Module) e);
            }
        }
        return lst;
    }

    /**
     * Iterates through the model and stores all module elements to moduleMap
     */
    public void populateModuleMap() {
        for (Module m : getModules()) {
            modules.put(m.getName(), m);
        }
    }
    
    /**
     * Reference to System Model that is source of this Code Model
     * 
     * (non-Javadoc)
     * 
     * @see geneauto.models.genericmodel.Model#getDependentModel()
     */
    public Model getDependentModel() {
        return getSystemModel();
    }

    public List<GACodeModelRoot> getElements() {
        return elements;
    }
    
    public void setElements(List<?> elements) {
    	this.elements.clear();
    	for (Object o : elements){
    		addElement((GACodeModelRoot) o);
    	}
    }
    
    /**
     * Adds model element to the elements list and sets parent of the 
     * element to null. In addition checks if it is a module and adds 
     * it to modules map
     */
    public void addElement(GACodeModelRoot e) {
    	e.setParent(null);
    	e.setModel(this);
    	getElements().add(e);
        if (e instanceof Module) {
            modules.put(e.getName(), (Module) e);
        }
    }

	/**
	 * Generic addElement method overridden from the Model class.
	 * NOTE! This method must call the addElement method specific to this Model.
	 * If the type in the cast is wrong it will go to endless loop, as it will
	 * match the same method!
	 */
	@Override
    public void addElement(Object o) {
    	addElement((GACodeModelRoot) o);    	
    }

	@Override
    public void addElements(List<?> p_elements) {
    	for (Object o : p_elements){
    		addElement((GACodeModelRoot) o);
    	}
    }

    public GASystemModel getSystemModel() {
        return systemModel;
    }

    public void setSystemModel(GASystemModel systemModel) {
        this.systemModel = systemModel;

        // the reference between two models must be mutual
        if (systemModel.getCodeModel() != this) {
            systemModel.setCodeModel(this);
        }
    }

    public void addTempElement(GAModelElement element) {
        getTempModel().addElement(element);
    }

    public GAModelElement getTempElementById(int id) {
        return getTempModel().getElementById(id);
    }

    /**
     * Returns TempModel. If there is no such element in the model yet, creates
     * a new TempModel element
     */
    public TempModel getTempModel() {
        if (tempModel == null) {
            // check first if the loaded model has TempModel element
            tempModel = (TempModel) getElementByType(TempModel.class);

            // the element was not found in current model
            if (tempModel == null) {
                // create new and add it to elements list
                tempModel = new TempModel();
                addElement(tempModel);
            }
        }

        return tempModel;
    }

    public void deleteTempElementById(int id) {
        getTempModel().deleteElementById(id);
    }

    public void clearTempModel() {
        if (tempModel != null) {
            tempModel.clear();
        }
    }
}