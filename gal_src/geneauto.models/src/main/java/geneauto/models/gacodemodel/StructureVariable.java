/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/StructureVariable.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-09-13 10:43:54 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.TCustom;
import geneauto.utils.PrivilegedAccessor;

/**
 * (AnTo) TODO The usefulness of this class need revising.
 * 
 */
public class StructureVariable extends Variable_CM {
    public Expression getInitialValue() {
        GeneralListExpression listExp = (GeneralListExpression) PrivilegedAccessor.getObjectInPackage(
                GeneralListExpression.class, this);
        if (!(this.getDataType() instanceof TCustom)) {
            EventHandler.handle(EventLevel.ERROR, 
                    "getInitialValue", 
                    "", 
                    "data type of sctructure variable should be TCustom");
            return null;
        } else {
            TCustom custom = (TCustom) this.getDataType();
            VariableExpression varExp;
            for (StructureMember member : custom.getMembers()) {
                varExp = new VariableExpression(member);
                listExp.addIndexExpression(varExp);
            }
        }
        if (getDataType() != null) {
            listExp.setDataType(getDataType());
        }
        return listExp;
    }

    public void setInitialValue(Expression exp) {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "StructureVariable",
                "GE9997", "Call to an unimplemented function: setInitialValue",
                "");
        // (AnTo) TODO This does not work. We can't assume that the input
        // expression is a VariableExpression. The approach need revising.
        //         
        // this.initialValue = exp;
        // if (!(this.getDataType() instanceof TCustom)) {
        // // TODO add error here
        // } else {
        // TCustom custom = (TCustom) this.getDataType();
        // for (Expression member : custom.getMembers().getExpressions()) {
        // ((VariableExpression) exp).getVariable()
        // .setInitialValue(member);
        // }
        // }
    }
}
