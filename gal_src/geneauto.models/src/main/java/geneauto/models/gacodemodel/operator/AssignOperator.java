/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/operator/AssignOperator.java,v $
 *  @version	$Revision: 1.43 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.operator;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.assertions.Assert;

/**
 *
 */
public enum AssignOperator implements Operator {
    /**
     * =
     */
    SIMPLE_ASSIGN(2),
    /**
     * +=
     */
    ADD_ASSIGN(2),
    /**
     * -=
     */
    SUB_ASSIGN(2),
    /**
     * *=
     */
    MUL_ASSIGN(2),
    /**
     * /=
     */
    DIV_ASSIGN(2),
    /**
     * &=
     */
    AND_ASSIGN(2),
    /**
     * |=
     */
    OR_ASSIGN(2),
    /**
     * ^=
     */
    XOR_ASSIGN(2),
    /**
     * =, used in case of variable initialisation is significant in case of
     * constant arrays -- they can be printed differently in case of
     * initialising statement. Only used inside the printer component. Not
     * present in intermediate complete models.
     */
    INIT_ASSIGN(2);

    /**
     * Precedence level of the operator. Higher numeric value binds tighter.
     */
    private int precedence;

    /**
     * 
     * @param precedence
     */
    private AssignOperator() {
        this.precedence = 2;
    }

    /**
     * @param bIsPrefix
     * 
     * @param precedence
     *            precedence
     */
    private AssignOperator(int precedence) {
        this.precedence = precedence;
    }

    public int getPrecedence() {
        return precedence;
    }

    public String getName() {
        return getClass().getSimpleName() + ": " + name();
    }

    /**
     * Converts an EML (Embedded Matlab Language) operator string to a Gene-Auto
     * operator.
     * 
     * Note: Here EML means the "general Matlab family action/expression
     * language". There are slight differences in the meaning and availability
     * of the operators in the Matlab scripting language, Simulink and
     * Stateflow.
     * 
     * @param operatorString
     *            string representation of the operator
     * @param bitwise
     *            determines, whether the logical operators are to be treated
     *            bitwise or logically.
     * @param element
     *            GAModelElement containing the operatorString. For error
     *            reporting.
     * @return Gene-Auto operator or null, when the string is not recognised.
     */
    public static AssignOperator fromEML(String operatorString,
            boolean bitwise, GAModelElement element) {
        boolean badBitwise = false;
        if ("=".equals(operatorString)) {
            return SIMPLE_ASSIGN;
        } else if ("+=".equals(operatorString)) {
            return ADD_ASSIGN;
        } else if ("-=".equals(operatorString)) {
            return SUB_ASSIGN;
        } else if ("*=".equals(operatorString)) {
            return MUL_ASSIGN;
        } else if ("/=".equals(operatorString)) {
            return DIV_ASSIGN;
        } else if ("|=".equals(operatorString)) {
            if (bitwise) {
                return OR_ASSIGN;
            } else {
                badBitwise = true;
            }
        } else if ("&=".equals(operatorString)) {
            if (bitwise) {
                return AND_ASSIGN;
            } else {
                badBitwise = true;
            }
        } else if ("^=".equals(operatorString)) {
            if (bitwise) {
                return XOR_ASSIGN;
            } else {
                badBitwise = true;
            }
        }

        // Check, if it was a bitwise operator with no bitwise context
        if (badBitwise) {
            // This is actually only related to importing from Stateflow. Maybe, the
            // message could be more specific?
            String msg = "Bitwise operator not allowed, since bitwise operations are disabled in the context of the current element."
                    + "\nOperator: " + operatorString;
            if (element != null) {
                msg += "\nElement: " + element.getReferenceString();
            }
            EventHandler.handle(EventLevel.ERROR, "AssignOperator", "GEM0083", msg);
        }

        // Otherwise: No warnings or errors here as we use the same method for
        // statement type detection
        return null;
    }

    /**
     * Checks that data types of left and right expression
     * are compatible. Throws an error if they are not.
     *  
     * @return true if left and right argument have compatible types
     * 
     */
    public boolean checkTypeValidity(Expression leftExpression, 
    									Expression rightExpression) {
    	
    	GADataType leftDT = leftExpression.getDataType();
    	GADataType rightDT = rightExpression.getDataType();
    	
    	Assert.assertNotNull(leftDT,  "\n " + getClass().getName() + ".checkTypeValidity(), Parameter=leftExpression" 
    			+ "\n " + leftExpression.getReferenceString());
    	Assert.assertNotNull(rightDT, "\n " + getClass().getName() + ".checkTypeValidity(), Parameter=rightExpression"
    			+ "\n " + rightExpression.getReferenceString());
    	
    	// the left expression must be assignable
        if (leftExpression instanceof ListExpression) {
            return false;
        }
        
        // check validity of expression types
    	return checkTypeValidity(
    			leftDT, rightDT, rightExpression instanceof ListExpression);
    }
    
    /**
     * Checks that data types of left and right expression
     * are compatible. Throws an error if they are not.
     *  
     * @param leftDT -- data type of left expression
     * @param rightDT -- data type of right expression
     * @param rigthExprIsList -- true, if right expression is list expression
     * @return true if left and right argument have compatible types
     * 
     * TODO: (to AnRo) remove comparison with init assign in the models package, 
     * override in c printer if necessary
     * TODO: (to AnRo) There is an argument rightExprIsList. This should be same 
     * as !getDataType().isScalar() and such a method argument is unnecessary.
     * Hence, also the first part in ((!rightExprIsList) || (rightDT.isScalar()))
     * is in excess. 
     */
    public boolean checkTypeValidity(GADataType leftDT, 
    			GADataType rightDT,
    			boolean rightExprIsList){

    	Assert.assertNotNull(leftDT,  "\n " + getClass().getName() + ".checkTypeValidity(), Parameter=leftDt");
    	Assert.assertNotNull(rightDT, "\n " + getClass().getName() + ".checkTypeValidity(), Parameter=rightDt");
    	
        if (!DataTypeUtils.isValidAssignment(leftDT, rightDT)) {
            return false;
        }
        
        return true;
    }

}