/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/AssignStatement.java,v $
 *  @version	$Revision: 1.53 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.LValueExpression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * AssignStatement implements all types of assignments (=, +=, *= etc.) NB! the
 * class assumes that the operator is fixed in constructor and is not changed
 * later. That is why the setter for operator is not provided.
 */
public class AssignStatement extends Statement {

	protected AssignOperator operator;
	protected Expression leftExpression;
	protected Expression rightExpression;

	/**
	 * sets operator to SimpleAssing, leftExpression and rightExpression remain
	 * NULL
	 */
	public AssignStatement() {
		super();
		setOperator (AssignOperator.SIMPLE_ASSIGN);
	}

	/**
	 * sets operator to given argument, leftExpression and rightExpression
	 * remain NULL
	 */
	public AssignStatement(AssignOperator assignOperator) {
		super();
		setOperator (assignOperator);
	}

	/**
	 * Constructor of the class initialising all attributes
	 * 
	 * @param assignOperator Statement's operator
	 * @param leftExpression Statement's left expression
	 * @param rightExpression Statement's right expression
	 * @param checkTypeValidity Flag. If it true check type validity for left and rightExpression
	 * shall be done
	 */
	public AssignStatement(AssignOperator assignOperator,
		Expression leftExpression, Expression rightExpression, boolean checkTypeValidity) {
		this();
		setOperator (assignOperator);
		setLeftExpression(leftExpression);	
		setRightExpression(rightExpression);
		
		boolean isUndefined = false;
		VariableExpression varExp;
		
		// if assignStatement contains variable expression with 
		// null variable -> skip validation
		for (GAModelElement elt: getChildren(VariableExpression.class)) {
		    varExp = (VariableExpression) elt;
		    if (varExp.getVariable() == null ) {
		        isUndefined = true;
		        break;
		    }
		}
		if (isUndefined) {
		    checkTypeValidity = false;
		}
		// Assert type validity
		if (checkTypeValidity) {		
	        if (!operator.checkTypeValidity(leftExpression, rightExpression)) {
	            EventHandler.handle(EventLevel.ERROR,
            		this.getClass().getCanonicalName(), 
            		"", 
            		"Incompatible types in assignment"
                    + "\n Left: " 
                    + DataTypeAccessor.toString(leftExpression.getDataType())
                    + " (Expression: " + leftExpression.printElement() + ")"
                    + "\n Right: " 
                    + DataTypeAccessor.toString(rightExpression.getDataType())
                    + " (Expression: " + rightExpression.printElement() + ")"
                    + "\n Statement: " + getReferenceString());	            
	        }
		}		
	}
	
	public AssignStatement(AssignOperator assignOperator,
	        Expression leftExpression, Expression rightExpression) {
	    this(assignOperator, leftExpression, rightExpression, true);
	}

	public AssignOperator getOperator() {
		return operator;
	}

	public Expression getLeftExpression() {
		return leftExpression;
	}

	public Expression getRightExpression() {
		return rightExpression;
	}

	/**
	 * sets leftExpression. If given expression is already member 
	 * of other element, makes copy of the expression
	 */
	public void setLeftExpression(Expression expr) {
		if(expr != null){
		    if (!(expr instanceof LValueExpression || expr instanceof ListExpression)) {
		        EventHandler.handle(EventLevel.ERROR, getClass().getSimpleName() + "setLeftExpression", "", 
		                "Expression of this kind is not allowed on the left side of an assignment." +
		                "\n Expression: " + expr.getReferenceString());
		        return;
            }
			if (expr.getParent() == null) { 
				this.leftExpression = expr;
			} else {
				this.leftExpression = (Expression) expr.getCopy();
			}
			
			this.leftExpression.setParent(this);	
		}	
		else
			this.leftExpression = null;		
	}

	/**
	 * sets rightExpression. If given expression is already member 
	 * of other element, makes copy of the expression
	 */
	public void setRightExpression(Expression expr) {
		if(expr != null){
			if (expr.getParent() == null){ 
				this.rightExpression = expr;
			}
			else {
				this.rightExpression = (Expression) expr.getCopy();
			}
			this.rightExpression.setParent(this);	
		}	
		else
			this.rightExpression = null;		
	}

	public void setOperator(AssignOperator operator) {
		this.operator = operator;
	}
}