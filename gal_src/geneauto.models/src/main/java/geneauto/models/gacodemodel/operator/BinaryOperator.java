/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/operator/BinaryOperator.java,v $
 *  @version	$Revision: 1.70 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.operator;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public enum BinaryOperator implements Operator {

    /*
     * 
     * Arithmetical operators
     * 
     */

    /**
     * +
     */
    ADD_OPERATOR(12),

    /**
     * -
     */
    SUB_OPERATOR(12),

    /**
     * .* (element-wise product)
     */
    MUL_OPERATOR(13),

    /**
     * /
     */
    DIV_OPERATOR(13),

    /**
     * ^
     */
    POWER_OPERATOR(13),

    /*
     * 
     * Operator with matrix as operands only
     */

    /**
     * * (matrix product)
     */
    MUL_MAT_OPERATOR(13),

    /*
     * 
     * Operator with integers as operands only
     */

    /**
     * %
     */
    MOD_OPERATOR(13),

    /*
     * 
     * Bitwise operators
     */

    /**
     * &
     */
    BITWISE_AND_OPERATOR(8),
    /**
     * |
     */
    BITWISE_OR_OPERATOR(6),
    /**
     * ^
     */
    BITWISE_XOR_OPERATOR(7),

    /**
     * <<
     */
    SHIFT_LEFT_OPERATOR(11),
    /**
     * >>
     */
    SHIFT_RIGHT_OPERATOR(11),

    /*
     * 
     * Logical operators
     */

    /**
     * ==
     */
    EQ_OPERATOR(9),
    /**
     * >=
     */
    GE_OPERATOR(10),
    /**
     * >
     */
    GT_OPERATOR(10),
    /**
     * <=
     */
    LE_OPERATOR(10),
    /**
     * <
     */
    LT_OPERATOR(10),
    /**
     * !=
     */
    NE_OPERATOR(9),
    /**
     * &&
     */
    LOGICAL_AND_OPERATOR(5),
    /**
     * ||
     */
    LOGICAL_OR_OPERATOR(4),

    ;

    /**
     * Precedence level of the operator. Higher numeric value binds tighter.
     */
    private int precedence;

    /**
     * 
     * @return name of the operator
     */

    /**
     * 
     * @param precedence
     *            precedence
     * @param name
     *            name of the operator
     */
    private BinaryOperator(int precedence) {
        this.precedence = precedence;
    }

    public int getPrecedence() {
        return this.precedence;
    }

    public String getName() {
        return getClass().getSimpleName() + ": " + name();
    }

    /**
     * Converts an EML (Embedded Matlab Language) operator string to a Gene-Auto
     * operator.
     * 
     * Note: Here EML means the "general Matlab family action/expression
     * language". There are slight differences in the meaning and availability of
     * the operators in the Matlab scripting language, Simulink and Stateflow.
     * 
     * @param operatorString
     *            string representation of the operator
     * @param bitwise
     *            determines, whether the logical operators are to be treated
     *            bitwise or logically.
     * @param stateflow
     *            treat operator as in Stateflow
     * @return Gene-Auto operator or null, when the string is not recognised.
     */
    public static BinaryOperator fromEML(String operatorString,
            boolean bitwise, boolean stateflow) {
        if ("+".equals(operatorString)) {
            return ADD_OPERATOR;
        } else if ("-".equals(operatorString)) {
            return SUB_OPERATOR;
        } else if ("*.".equals(operatorString)) {
            return MUL_OPERATOR;
        } else if ("*".equals(operatorString)) {
            if (stateflow) {
                return MUL_OPERATOR;
            } else {
                return MUL_MAT_OPERATOR;
            }            
        } else if ("/".equals(operatorString)) {
            return DIV_OPERATOR;
        } else if ("%%".equals(operatorString)) {
            // Note! This is a different string than in C
            return MOD_OPERATOR;
        } else if ("&".equals(operatorString) && bitwise) {
            return BITWISE_AND_OPERATOR;
        } else if ("&".equals(operatorString) && !bitwise) {
            return LOGICAL_AND_OPERATOR;
        } else if ("|".equals(operatorString) && bitwise) {
            return BITWISE_OR_OPERATOR;
        } else if ("|".equals(operatorString) && !bitwise) {
            return LOGICAL_OR_OPERATOR;
        } else if ("^".equals(operatorString) && bitwise) {
            return BITWISE_XOR_OPERATOR;
        } else if ("^".equals(operatorString) && !bitwise) {
            return POWER_OPERATOR;
        } else if ("==".equals(operatorString)) {
            return EQ_OPERATOR;
        } else if ("!=".equals(operatorString)) {
            return NE_OPERATOR;
        } else if ("~=".equals(operatorString)) {
            // Note! This is a different string than in C
            return NE_OPERATOR;
        } else if ("<>".equals(operatorString)) {
            // Note! This is a different string than in C
            return NE_OPERATOR;
        } else if (">".equals(operatorString)) {
            return GT_OPERATOR;
        } else if ("<".equals(operatorString)) {
            return LT_OPERATOR;
        } else if (">=".equals(operatorString)) {
            return GE_OPERATOR;
        } else if ("<=".equals(operatorString)) {
            return LE_OPERATOR;
        } else if ("&&".equals(operatorString)) {
            return LOGICAL_AND_OPERATOR;
        } else if ("||".equals(operatorString)) {
            return LOGICAL_OR_OPERATOR;
        } else if ("<<".equals(operatorString)) {
            return SHIFT_LEFT_OPERATOR;
        } else if (">>".equals(operatorString)) {
            return SHIFT_RIGHT_OPERATOR;
        } else {
            // No warnings or errors here as we use the same method for
            // statement type detection
            return null;
        }
    }

    /**
     * Converts operator string from block parameter to a binary operator
     *
     * NB! in case of composite operators ("NAND", "NOR", "XOR") returns the 
     * binary operator to be done by two arguments. however it is assumed,
     * that there is higher level logic that combines the given binary 
     * with appropriate unary operator
     * 
     * E.g. in case of expression "x NAND y" the desired result is 
     * 	"NOT(x AND y)". This method returns "AND", the higher level
     * logic must add not 
     * 
     * 
     * @param operatorString
     * @return
     */
    public static BinaryOperator fromParameter(String operatorString) {
	    if (operatorString.equals("AND")) {
	        return BinaryOperator.LOGICAL_AND_OPERATOR;
	    } else if (operatorString.equals("NAND")) {
		        return BinaryOperator.LOGICAL_AND_OPERATOR;
	    } else if (operatorString.equals("OR")) {
	        return BinaryOperator.LOGICAL_OR_OPERATOR;
	    } else if (operatorString.equals("NOR")) {
	        return BinaryOperator.LOGICAL_OR_OPERATOR;
	    } else if (operatorString.equals("XOR")) {
	        return BinaryOperator.BITWISE_XOR_OPERATOR;
	    }
	    
	    return null;
    }
	/**
	 * Infer the resulting data type of the binary expression based on the
	 * operator and argument expressions
	 * 
	 * @param leftExpr
	 * @param rightExpr
	 * @param strict - raise error on failure
	 * @return resulting type on success null on failure
	 */
    public GADataType getDataType(Expression leftExpr,
            Expression rightExpr, boolean strict) {
    	GADataType resultDT = getDataType(leftExpr.getDataType(), rightExpr
                .getDataType(), false); // NOTE: We postpone the error message here
    	
    	// Check for error
        if (resultDT == null && strict) {
        	EventHandler.handle(EventLevel.ERROR, 
        			getClass().getName() + ".getDataType()", "", 
        			"Can't infer resulting data type of a BinaryExpression."
        			+ "\n Operator: " + this.getName()
        			+ "\n Left  expression: " + leftExpr.getReferenceString()
        			+ "\n       datatype  : " 
        			+ DataTypeAccessor.toString(leftExpr.getDataType())
        			+ "\n Right expression: " + rightExpr.getReferenceString()
        			+ "\n       datatype  : " 
        			+ DataTypeAccessor.toString(rightExpr.getDataType()));
        }
   	
        return resultDT;
    }

	/**
	 * Infer the resulting data type of the binary expression based on the
	 * operator and types of the arguments
	 * 
	 * @param leftDT
	 * @param rightDT
	 * @param strict - raise error on failure
	 * @return resulting type on success null on failure
	 */
    public GADataType getDataType(GADataType leftDT, GADataType rightDT, boolean strict) {
        
    	GADataType resultDT = null;
    	
        if (leftDT == null || rightDT == null) {
            // Cannot infer type. This could be because there is incomplete
            // information at this stage of processing. 
        	resultDT = null;
        	
        } else { 
	        switch(this) {
	        case EQ_OPERATOR:
	        case GE_OPERATOR:
	        case LE_OPERATOR:
	        case GT_OPERATOR:
	        case LT_OPERATOR:
	        case NE_OPERATOR:         
	        case LOGICAL_AND_OPERATOR:
	        case LOGICAL_OR_OPERATOR:
	            TBoolean boolDT = (TBoolean) PrivilegedAccessor
	            .getObjectInPackage(TBoolean.class, leftDT);
	            if (leftDT.isScalar() && rightDT.isScalar()) {
	                resultDT = boolDT;
	            } else {
	                GADataType dt = inferElemWise(leftDT, rightDT);
	                if (dt instanceof TArray) {
	                    ((TArray) dt).setBaseType(boolDT);
	                } else {
	                	// Error
	                    dt = null;
	                }
	                resultDT = dt;
	            }	
	            break;
	        
	        case ADD_OPERATOR:
	        case SUB_OPERATOR:
	        case MUL_OPERATOR:
	        case DIV_OPERATOR:
	        	/* NOTE Division in Simulink and Gene-Auto means floating point division by default!
	        	 * However, we don't do it here, because due to fall-through of cases it would affect
	        	 * also other cases */
	        	
	        case POWER_OPERATOR:            
	        case MOD_OPERATOR:
	        case BITWISE_AND_OPERATOR:
	        case BITWISE_OR_OPERATOR:
	        case BITWISE_XOR_OPERATOR:
	        case SHIFT_LEFT_OPERATOR:
	        case SHIFT_RIGHT_OPERATOR:
	        	// Infer element-wise
	            resultDT = inferElemWise(leftDT, rightDT);
	            
	            if (resultDT.equalsTo(new TBoolean())){
	            	resultDT = new TRealInteger();
	            }
	            // Integer-Floating point correction for division
	            // Done here, because of fall-through. See also note above.
	            if (this == DIV_OPERATOR) {
	            	if (resultDT instanceof TRealInteger) {
	            		resultDT = new TRealDouble();
	            	} else if (resultDT instanceof TArray) {
	            		TArray ar = (TArray) resultDT;
	            		if (ar.getBaseType() instanceof TRealInteger) {
	            			ar.setBaseType(new TRealDouble());
		            	}	            		
	            	}
	            }
	            break;

	        case MUL_MAT_OPERATOR:
	        	resultDT = inferMulMat(leftDT, rightDT);
	            break;
	            
	        default:
	        	resultDT = null;
	        }
        }
        if (resultDT == null && strict) {
        	EventHandler.handle(EventLevel.ERROR, getClass().getName() + ".getDataType()", "", 
        			"Can't infer resulting data type of a BinaryExpression."
        			+ "\n Operator: " + this.getName()
        			+ "\n Left datatype : " + leftDT.toString()
        			+ "\n Right datatype: " + rightDT.toString());
        }
        return resultDT;
    }

    /**
     * Infer elementwise the result type of operation 
     * 
     * @param leftDT
     * @param rightDT
     * @return
     */
    private GADataType inferElemWise(GADataType leftDT, GADataType rightDT) {
        GADataType resultDT;
        
        if (leftDT.isScalar() || rightDT.isScalar()) {
            // One is scalar
        	
        	// First compute the least common type of the operands
            resultDT = DataTypeUtils.chooseLeastCommonType(leftDT, rightDT, true);
            
			// For logical or relational operators application of the
			// operator makes the element type of the resulting array to
			// "boolean".
            if (isLogical() || isRelational()) {
                if (resultDT instanceof TArray) {
                    ((TArray) resultDT).setBaseType(new TBoolean());
                } else {
                	// Error
                	resultDT = null;
                }
            }
            
        } else {
            // Both are non-scalars
            
            // Check for a need for vector to matrix promotion
            if (leftDT.isVector() && rightDT.isMatrix()) {
                leftDT = rightDT.isRowMatrix() ? 
                            ((TArray) leftDT).toRowMatrix() : ((TArray) leftDT).toColMatrix();
            } else if (leftDT.isMatrix() && rightDT.isVector()) {
                rightDT = leftDT.isRowMatrix() ? 
                        ((TArray) rightDT).toRowMatrix() : ((TArray) rightDT).toColMatrix();
            }
            
            // Check if dimensions agree (after matrix promotion)
            if (!DataTypeUtils.checkDimensions((TArray) leftDT, (TArray) rightDT, true, false)) {
                return null;
            }                
                
            // Make the output type
            resultDT = leftDT.getCopy();
            TPrimitive primDT;
            if (isLogical() || isRelational()) {
                primDT = new TBoolean();
            } else {
                primDT = DataTypeUtils.chooseLeastCommonType(leftDT
                        .getPrimitiveType(), rightDT.getPrimitiveType());
            }
            ((TArray) resultDT).setBaseType(primDT);
            
        }
        
        // Simplify the output type according to the typing rules.
        resultDT = resultDT.normalize(false);
        
        // Perform additional constraint checks
        if (!validateElemWiseResult(rightDT, resultDT)) {
            return null;
        }
        
        return resultDT;
    }

    /**
     * Check additional constraints on the result type 
     * @param rightDT
     * @param resultDT
     * @return
     */
    private boolean validateElemWiseResult(GADataType rightDT,
            GADataType resultDT) {        
        boolean valid = true;
        
        switch (this) {
        
        case ADD_OPERATOR:
        case SUB_OPERATOR:
        case MUL_OPERATOR:
        case DIV_OPERATOR:                
            // Only numbers accepted (this includes also booleans)
            if (!(resultDT.getPrimitiveType().isSubType(TRealNumeric.getLargestPrimitiveType()))) {
                valid = false;
            }
            break;

        case POWER_OPERATOR:            
            // Only numbers accepted
            if (!(resultDT.getPrimitiveType().isSubType(TRealNumeric.getLargestPrimitiveType()))) {
                valid = false;
            }
            // Right argument of the power operator must be a scalar
            if (!rightDT.isScalar()) {
                valid = false; 
            }            
            break;
            
        case MOD_OPERATOR:
        case BITWISE_AND_OPERATOR:
        case BITWISE_OR_OPERATOR:
        case BITWISE_XOR_OPERATOR:
        case SHIFT_LEFT_OPERATOR:
        case SHIFT_RIGHT_OPERATOR:
            // Only integers accepted
            if (!(resultDT.getPrimitiveType() instanceof TRealInteger)) {
                valid = false;
            }
            break;
        case EQ_OPERATOR:
        case GE_OPERATOR:
        case LE_OPERATOR:
        case GT_OPERATOR:
        case LT_OPERATOR:
        case NE_OPERATOR:         
        case LOGICAL_AND_OPERATOR:
        case LOGICAL_OR_OPERATOR:
            // Only booleans accepted
            if (!(resultDT.getPrimitiveType() instanceof TBoolean)) {
                valid = false;
            }
            break;
        default:
            valid = false; // For robustness
        }
        
        return valid;
    }

    /**
     * Infer the result type of matrix multiplication operation.
     * 
     * @param leftDT
     * @param rightDT
     * @return
     */
    private GADataType inferMulMat(GADataType leftDT, GADataType rightDT) {
        GADataType resultDT = null;            
        // left or/and right data type 
        // is scalar -> the same data type like 
        // in MulOperator with current data types
        TPrimitive baseType = DataTypeUtils.chooseLeastCommonType(
                leftDT.getPrimitiveType(), rightDT.getPrimitiveType());
        List<Expression> dimensions = new ArrayList<Expression>();
        double rightDT1DimValue = -1;
        double rightDT2DimValue = -1;
        double leftDT1DimValue = -1;
        double leftDT2DimValue = -1;
        if (leftDT.isScalar() || rightDT.isScalar()) {
            MUL_OPERATOR.getDataType(leftDT, rightDT, true);
        } else if ((leftDT.getDimensionality() == 1) 
                && (rightDT.getDimensionality() == 1 )) {
            EventHandler.handle(
                    EventLevel.ERROR, "BinaryOperator.getDataType()", "???", 
                    "Cannot perform matrix multiplication of two vectors.");
        }
        if (leftDT.getDimensionality() > 0) {
            leftDT1DimValue = leftDT.getDimensions()
                .get(0).evalReal().getRealValue();
        };
        if (leftDT.getDimensionality() > 1) {
            leftDT2DimValue = leftDT.getDimensions()
                .get(1).evalReal().getRealValue();
        };
        if (rightDT.getDimensionality() > 0) {
            rightDT1DimValue = rightDT.getDimensions()
                .get(0).evalReal().getRealValue();
        };
        if (rightDT.getDimensionality() > 1) {
            rightDT2DimValue = rightDT.getDimensions()
                .get(1).evalReal().getRealValue();
        };
        if ((leftDT.getDimensionality() == 0)
                || (rightDT.getDimensionality() == 0)) {
            return BinaryOperator.MUL_OPERATOR.
                getDataType(leftDT, rightDT, true);
        
        // leftDT - vector, rightDT - matrix
        } else if ((leftDT.getDimensionality() == 1) && 
                (rightDT.getDimensionality() == 2)) {
            
            // vector[n] * matrix[n, m] -> matrix[1, m]
            if (leftDT1DimValue == rightDT1DimValue) {
                if (rightDT2DimValue == 1) {
                    return baseType;
                } else {
                    dimensions.add(new IntegerExpression(1));
                    dimensions.add(rightDT.getDimensions().get(1).getCopy());
                    resultDT = new TArray(dimensions, baseType);
                }                     
            
            // vector[n] * matrix[1, m] -> matrix[n, m]
            } else  if (rightDT1DimValue == 1) {
                
                dimensions.add(leftDT.getDimensions().get(0).getCopy());
                dimensions.add(rightDT.getDimensions().get(1).getCopy());
                resultDT = new TArray(dimensions, 
                        baseType);
            } else {
                EventHandler.handle(EventLevel.ERROR, "inferMulMat", 
                        "", 
                        "incompatible data types for inferMulMat. "
                        + "\n1. data type" + leftDT
                        + "\n2. data type" + rightDT);
                return null;
            }
            
        // leftDT - matrix, rightDT - vector
        } else if ((leftDT.getDimensionality() == 2) && 
                (rightDT.getDimensionality() == 1)) {
            if (leftDT2DimValue == rightDT1DimValue) {
                resultDT = new TArray(leftDT.getDimensions().get(0).getCopy(), 
                        baseType);
            } else if (leftDT2DimValue == 1) {
                dimensions.add(leftDT.getDimensions().get(0).getCopy());
                dimensions.add(rightDT.getDimensions().get(0).getCopy());
                resultDT = new TArray(dimensions, baseType);
            } else {
                EventHandler.handle(EventLevel.ERROR, "inferMulMat", 
                        "", 
                        "incompatible data types for inferMulMat. "
                        + "\n1. data type" + leftDT
                        + "\n2. data type" + rightDT);
                return null;
            }
        }  
        
        // both data types are matrixes   
        else if ((leftDT.getDimensionality() == 2)
                && (rightDT.getDimensionality() == 2)) {
            dimensions.add(leftDT.getDimensions().get(0).getCopy());
            dimensions.add(rightDT.getDimensions().get(1).getCopy());
            resultDT = new TArray(dimensions, baseType);
        } else {
            return null;
        }
        
        // convert singleton arrays (matrices, vectors) to scalar
        resultDT = resultDT.normalize(false);
        
        return resultDT;            
    }
    
    
    /**
     * Prints the current element in pseudo code or event messages
     * 
     * @param outerPrecedence precedence of the outer expression
     * 
     * @return pseudo code of the element
     */
    public String printElement() {
        String code = "";
        switch (this) {
        case ADD_OPERATOR:
            code = "+";
            break;
        case BITWISE_AND_OPERATOR:
            code = "&";
            break;
        case BITWISE_OR_OPERATOR:
            code = "|";
            break;
        case BITWISE_XOR_OPERATOR:
            code = "^";
            break;
        case DIV_OPERATOR:
            code = "/";
            break;
        case EQ_OPERATOR:
            code = "==";
            break;
        case GE_OPERATOR:
            code = ">=";
            break;
        case GT_OPERATOR:
            code = ">";
            break;
        case LOGICAL_AND_OPERATOR:
            code = "&&";
            break;
        case LOGICAL_OR_OPERATOR:
            code = "||";
            break;
        case LE_OPERATOR:
            code = "<=";
            break;
        case LT_OPERATOR:
            code = "<";
            break;
        case MOD_OPERATOR:
            code = "%";
            break;
        case MUL_OPERATOR:
            code = "*";
            break;
        case MUL_MAT_OPERATOR:
            code = "(MUL_MAT)";
            break;
        case NE_OPERATOR:
            code = "!=";
            break;
        case POWER_OPERATOR:
            code = "^";
            break;
        case SHIFT_LEFT_OPERATOR:
            code = "<<";
            break;
        case SHIFT_RIGHT_OPERATOR:
            code = ">>";
            break;
        case SUB_OPERATOR:
            code = "-";
            break;
        default: 
            EventHandler.handle(EventLevel.ERROR, "", "", "Operator \""
                    + this.getName() + "\" does not have a pseudo code printing rule.",
                    "");

        }
        return code;
    }

    /**
     * Checks if data types of left and right expression are suitable
     * for given operator
     *  
     * @param leftExpression
     * @param rightExpression
     * @return true when data types are suitable for composing a statement
     * false otherwise
     */
    public boolean assertTypeValidity(Expression leftExpression, 
            Expression rightExpression) {
 	   boolean result = getDataType(leftExpression, rightExpression, true) != null;
	   if (!result) {
           EventHandler.handle(EventLevel.CRITICAL_ERROR,
                   getClass().getName() + ".assertTypeValidity(Expression leftExpression, Expression rightExpression)", 
                   "",
                   "Type assertion failed" + 
                   "\n leftExpression:  " + leftExpression.getReferenceString() +
                   "\n rightExpression: " + rightExpression.getReferenceString()
                   );		   
	   }
       return result;
    }

    /**
     * Checks if data types of left and right expression are suitable
     * for given operator
     *  
     * @param leftDT		-- data type of left expression
     * @param rightDT		-- data type or right expression
     * @return true when data types are suitable for composing a statement
     * false otherwise
     */
    public boolean assertTypeValidity(GADataType leftDT, GADataType rightDT) {
  	   boolean result = getDataType(leftDT, rightDT, true) != null;
	   if (!result) {
           EventHandler.handle(EventLevel.CRITICAL_ERROR,
                   getClass().getName() + ".assertTypeValidity(GADataType leftDT, GADataType rightDT)", 
                   "",
                   "Type assertion failed" + 
                   "\n leftDT:  " + leftDT.getReferenceString() +
                   "\n rightDT: " + rightDT.getReferenceString()
                   );		   
	   }
       return result;
    }

    public boolean isLogical() {
        switch(this) {
        case LOGICAL_AND_OPERATOR:
        case LOGICAL_OR_OPERATOR:
            return true;
        default:
            return false;
        }
    }
    
    public boolean isRelational() {
        switch(this) {
        case EQ_OPERATOR:
        case GE_OPERATOR:
        case LE_OPERATOR:
        case GT_OPERATOR:
        case LT_OPERATOR:
        case NE_OPERATOR:     
            return true;
        default:
            return false;
        }
    }
}