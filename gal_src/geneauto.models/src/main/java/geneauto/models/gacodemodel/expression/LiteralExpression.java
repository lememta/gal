/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/LiteralExpression.java,v $
 *  @version	$Revision: 1.55 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TRealFloatingPoint;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gadatatypes.TString;

/**
 * Literal expression. Contains a literal value, e.g. a number. In order to keep
 * exactly the original representation (e.g. numeric format and precision), the
 * value field is of type String.
 * 
 * Note: Literal expression is only for values not references.
 * VariableExpression is for references/identifiers. Including named constants.
 */
public abstract class LiteralExpression extends Expression {

    protected String litValue;

    /**
     * Default constructor of the class
     */
    public LiteralExpression() {
        super();
    }

    public LiteralExpression(String litValue) {
        this();
        setLitValue(litValue);
    }

    /**
     * Constructor of the class
     * 
     * @param litValue
     *            literal value of the expression
     * @param dataType
     *            type of the expression
     */
    public LiteralExpression(String litValue, GADataType dataType) {
        this(litValue);
        // We do not call the super type method setDataType, because that
        // allows only to reset type and assumes that a type is automatically 
        // derived. For literal expressions we want the type to be set explicitly.        
        attachDataType(dataType);
    }

    public String getLitValue() {
        return litValue;
    }

    public void setLitValue(String litValue) {
        this.litValue = litValue;
    }


    public boolean isConst() {
        return true;
    }

    public Expression eval(boolean printError, boolean optimizableOnly) {
        double val = 0;
        try {
            val = Double.parseDouble(litValue);
        } catch (Exception e) {
            if (printError) {
                EventHandler.handle(EventLevel.ERROR, "models.LiteralExpression",
                        "GEM0072", "value of LiteralExpression is not of type REAL"
                        + "\nExpression: " + getReferenceString(),
                        "");
            }
            return null;
        }
        DoubleExpression re = new DoubleExpression(val);
        return re;
    }
    
    public String printElement(int precLevel) {
        if (getDataType() instanceof TString) {
            return "\"" + litValue + "\"";
        }
        return litValue;
    }

    /**
     * In case of literal expression the resetDataType takes effect only when 
     * the expression does not have the data type yet. In this case the data
     * type is derived from the default value of the corresponding expression 
     * type and the expression value.
     * 
     * When literal expression has data type defined already, resetDatatype does
     * not change it.
     * 
     * The functionality for each literalExpression type is implemented in the
     * corresponding subclass
     */
    @Override
    public void resetDataType() {
    }

    @Override
    public String toString() {
        return super.toString() + " \"" + getLitValue() + "\"";
    }    
    
    /**
     * returns the right type of LiteralExpression subclass 
     * depending on the data type of setting value
     * @param type data type
     * @param value string value
     * @return Literal expression subclass
     */
    public static LiteralExpression getInstance(String value, GADataType type) {
        LiteralExpression litExp;
        if (type instanceof TRealInteger) {
            litExp = new IntegerExpression(value, type);
        } else if (type instanceof TBoolean) {
            litExp = BooleanExpression.getBooleanInstance(value);
        } else if (type instanceof TRealFloatingPoint) {
            litExp = new DoubleExpression(value, type);
        } else if (type instanceof TString) {
            litExp = new StringExpression(value);
        } else {
            String msg = "Impossible to return Instance of LiteralExpression"
                + "because, ";
            if (type == null) {
                msg += "data type is not set";
            } else {
                msg += "data type is unnacceptable for LiteralExpression" + 
                "/ndata type " + type.toString();
            }
            EventHandler.handle(EventLevel.ERROR, 
                    "getInstance", "", msg);
            litExp = null;
        }
        return litExp;
    }

    @Override
    public boolean isLiteral() {
        return true;
    }
	
	@Override
	public String getStringValue() {
		return litValue;
	}
}