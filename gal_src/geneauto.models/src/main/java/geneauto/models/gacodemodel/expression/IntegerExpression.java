/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/IntegerExpression.java,v $
 *  @version	$Revision: 1.53 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.PrivilegedAccessor;

/**
 * Literal expression containing an integer value
 * 
 * TODO This class should not use the litValue field from the superclass. 
 * That field should be removed in the future. 
 */
public class IntegerExpression extends NumericExpression {
    
    /**
     * Default constructor of this class.
     */
    public IntegerExpression() {
        super();
    }

    /**
     * Specific constructor of this class.
     * @param value An integer value
     */
    public IntegerExpression(int value) {
        super(value + "");
        setDataType(DataTypeUtils.
        		scaleFor(getDefaultDT(), value));
    }

    public IntegerExpression(String value) {
        super(value);
        setDataType(DataTypeUtils.
        		scaleFor(getDefaultDT(), Integer.parseInt(value)));
    }

    /**
     * Specific constructor of this class.
     * @param value An integer value
     * @param dt    Its exact data type
     */
    public IntegerExpression(int value, GADataType dt) {
        super(value + "");
        setDataType(dt);
    }

    public IntegerExpression(String StringExpression, GADataType dt) {
        super(StringExpression, dt);
    }

    public IntegerExpression eval() {
    	// Return just self
    	// No need to assign the buffer in this class 
        return this;
    }

	@Override
	public double getRealValue() {
		return getIntValue();
	}

    @Override
	public Expression getValue() {
		// In this class we are not using the buffer.
		return this;
	}

	/** 
     * Returns integer value together with correct sign
     * @return int
     */
    public int getIntValue() {
    	int res = 0;
        try {
        	res = Integer.parseInt(litValue);
        } catch (NumberFormatException e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    "getIntValue", 
                    "CP0082",
                    "Unacceptable number format for IntegerExpression"
                    + " \nvalue: " + litValue
                    + " \nExpression: " + this.getReferenceString());
        }
        return res;
    }    
    
    public String printElement (int precLevel) {
        return litValue;
    }

    /**
     * Scale expression data type according to its value when the data type
     * is not defined. If the data type is defined do nothing 
     */
    @Override
    public void resetDataType() {
    	if (dataType == null) {
    		setDataType(DataTypeUtils.scaleFor(getDefaultDT(), getIntValue()));
    	}
    }


    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    @Override
    public void setDataType(GADataType dataType) {
        attachDataType(dataType);
    }
    
// FIXME Probably a redundant method. Check and remove.    
//    @Override
//    protected void splitLitValue() {
//    	super.splitLitValue();
//        String value = litValue;
//        if (isScientificValue()) {
//            value = value.substring(
//                    0, value.toLowerCase().indexOf("e"));
//        }
//        Triple<Integer, Boolean, TRealInteger> triple = TRealInteger
//            .parseInteger(value, this);
//        if (triple != null) {
//            setIntegerPart(triple.getLeft());
//            setHexValue(triple.getMiddle());
//            if (dataType == null) {
//                setDataType(triple.getRight());
//            }
//        }
//    }    
    
    public TRealInteger getDefaultDT() {
        TRealInteger result = (TRealInteger) PrivilegedAccessor
            .getObjectInPackage(TRealInteger.class, this);
        if (result == null) {
            result = new TRealInteger();
        }
        result.setDefaultParameters();
        return result;
    }

	@Override
	public void setLitValue(String value) {
        try {
    		// Check, if the value is an integer
        	Integer.parseInt(value);
    		// Use the given value string as is 
        	super.setLitValue(value); 
            setDataType(DataTypeUtils.
            		scaleFor(getDefaultDT(), Integer.parseInt(value)));
        } catch (NumberFormatException e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    "setLitValue", 
                    "CP0082",
                    "Unacceptable number format for IntegerExpression"
                    + " \nvalue: " + value
                    + " \nExpression: " + this.getReferenceString());
        }
	}

	@Override
	public Expression normalizeShape(boolean full) {
		// this is of scalar type already. do nothing
		return this;
	}

	@Override
	public void toggleSign() {
		// Negate the value, convert to integer and then to string  
		setLitValue((int) (getIntValue() * -1) + "");		
	}
}
