/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/RealExpression.java,v $
 *  @version	$Revision: 1.33 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealFloatingPoint;

/**
 * Expression that represents a 'double' value (IEEE-754 binary64).
 */
public class DoubleExpression extends NumericExpression {

    /**
     * Default constructor of the class
     */
    public DoubleExpression() {
        super();
    }

    public DoubleExpression(String value) {
        super(value);
        setDataType(getDefaultDT().scaleFor(getRealValue()));
    }
    
    public DoubleExpression(String value, GADataType dt) {
        super(value, dt);
    }
    
    public DoubleExpression(Double value, GADataType dt) {
        this(value.toString(), dt);
    }
    
    public DoubleExpression(Double value) {
        this(value.toString());
    }
    
    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    @Override
    public void setDataType(GADataType dataType) {
        attachDataType(dataType);
    }

    public Expression eval(boolean printError, boolean optimizableOnly) {
        return new DoubleExpression(getRealValue());
    }

	@Override
	public double getRealValue() {
		return getDoubleValue();
	}
    
// FIXME Probably a redundant method. Check and remove.   
//    // TODO improve this part in the future
//    // replace try-catch blocks with regular expressions
//    protected void splitLitValue() {
//        super.splitLitValue();
//        GADataType dt = parseRealType();
//        setDataType(dt);
//
//        if (dt == null) {
//            // An error has been raised already
//            return;
//        }
//        
//        // assume first that the value is positive
//        isNegative = false;
//        
//        String value = litValue;
//        if (isScientificValue()) {
//            value = value.substring(
//                    0, value.toLowerCase().indexOf("e"));
//        }
//        String[] litValueParts = value.split("\\.");
//        if (litValueParts.length == 1) {
//            try {
//                setIntegerPart(
//                        Integer.parseInt(litValueParts[0]));
//                setFractionalPart("0");
//            } catch (NumberFormatException nfe) {
//                // TODO print error here?
//                return;
//            }
//        } else if (litValueParts.length == 2) {
//            try {
//                setIntegerPart(
//                    Integer.parseInt(litValueParts[0]));
//                setFractionalPart(litValueParts[1]);
//            } catch (NumberFormatException nfe) {
//                // TODO print error here?
//                return;
//            }
//            
//        } else {
//            // TODO print error here
//            return;
//        }
//    }

    

// FIXME Probably a redundant method. Check and remove.
//    private GADataType parseRealType() {
//        try {
//            // Check, if the number is of form "1.234f"
//            if (litValue.contains("f")) {
//                try {
//                    Float.parseFloat(litValue);
//                    return (TRealSingle) PrivilegedAccessor
//                        .getObjectInPackage(TRealSingle.class, this);
//                }
//                // Not possible to parse it into Single
//                catch (NumberFormatException e) {
//                    EventHandler.handle(EventLevel.ERROR, "models.RealExpression",
//                            "", "Bad floating value : " + litValue);
//                    return null;
//                }                
//            } else {
//                // Assume it is a double. Do not check the length.
//                try {
//                    Double.parseDouble(litValue);
//                    return (TRealDouble) PrivilegedAccessor
//                        .getObjectInPackage(TRealDouble.class, this);
//                }
//                // Not possible to parse it into Double
//                catch (NumberFormatException e) {
//                    EventHandler.handle(EventLevel.ERROR, "models.RealExpression",
//                            "", "Bad double value : " + litValue);
//                    return null;
//                }       
//            }
//        }
//        catch (Exception e) {
//            EventHandler.handle(EventLevel.ERROR, "models.RealExpression",
//                    "GEM0074", "Unexpected error while parsing : " + litValue
//                    + "\nExpression: " + getReferenceString());
//            return null;
//        }
//    }

    public String printElement(int precLevel) {
        return litValue;
    }

    /**
     * When data type is not defined derives a new data type from default 
     * data type and expression value. Does nothing when the expression already
     * has data type.
     */
    @Override
    public void resetDataType() {
    	if (dataType == null) {
    		setDataType(getDefaultDT().scaleFor(getRealValue()));
    	}
    }
    
    public TRealFloatingPoint getDefaultDT() {
        return TRealFloatingPoint.getDefaultDT(this);
    }

	@Override
	public Expression normalizeShape(boolean full) {
		// this is always scalar type. do nothing
		return this;
	}
	
    public double getDoubleValue() {
        return Double.parseDouble(litValue);
    }

	public void getDoubleValue(double val) {
		litValue = val + "";
	}

	@Override
	public void toggleSign() {
		// Negate the value and convert to string  
		setLitValue((getDoubleValue() * -1) + "");		
	}
}