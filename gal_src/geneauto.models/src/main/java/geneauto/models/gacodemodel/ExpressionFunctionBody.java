/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/ExpressionFunctionBody.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2010-04-21 10:09:18 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.common.Function;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.map.ConstantMap;

/**
 * Function's body that is formed only of a single expression.
 * 
 * In C this can be useful for creating function-like macros.
 */
public class ExpressionFunctionBody extends FunctionBody {

    /**
     * expression performing necessary calculation and returning a value
     */
    protected Expression expression;

    public ExpressionFunctionBody() {
        super();
    }

    public ExpressionFunctionBody(Expression expression) {
        super();
        setExpression(expression);
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        expression.setParent(this);
        this.expression = expression;
    }

    /**
     * Expression function body does not have namespace on its own if there are
     * additional local variables required for expression function body, then
     * they have to be added to the parent namespace
     * 
     * @return parent's namespace if parent is correctly set, null otherwise
     */
    public NameSpace getNamespce() {
        if (parent != null && parent instanceof NameSpaceElement_CM) {
            return ((NameSpaceElement_CM) parent).getParentNameSpace();
        }
        return null;
    }
    
    /**
     * @return an output variable, if existing.
     */
    public Variable_CM getOutputVariable() {
        return null;
    }
 
    /**
     * goes through the function body and resolves references to function
     * arguments
     */
    @Override
    public void resolveReferencesToArguments() {
    	Function f = getContainingFunction();
    	
    	// resolving references makes sense only in case of Funciton_CM
    	if (f instanceof Function_CM) {
            // check if any of the statements has reference to function arguments
            ConstantMap<String, GAModelElement> argNames 
            								= argumentsToMap((Function_CM) f);
            
            if (expression != null) {
            	expression.resolveReferences(argNames, getModel());
            }
    	}
    	
    }
    
}