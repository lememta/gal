/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/statemodel/IsEventExpression.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2010-04-02 06:05:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression.statemodel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gasystemmodel.gastatemodel.Event;
import geneauto.models.utilities.statemodel.EvaluationContext;

/**
 * Stateflow: Predicate that checks, if the current event is a specified event.
 */
public class IsEventExpression extends StateModelExpression {	

	/**
	 * Pointer to the Event that is checked.
	 */
	protected Event event;

	/**
	 * Default constructor
	 */
	public IsEventExpression() {
		super();
	}

	/**
	 * Constructor with param(s)
	 * 
	 * @param Event
	 */
	public IsEventExpression(Event event) {
		super();
		setEvent(event);
	}

	public boolean isConst() {
		return false;
	}
	
	public boolean isConst(double p_value) {
		return false;
	}
	
	public Expression eval(boolean printError, boolean optimizableOnly) {
        // Return null to indicate that the expression can't be evaluated statically 
		return null;
	}

	public Event getEvent() {
		return event;			
	}

	public void setEvent(Event event) {
		this.event = event;
	}
	
	/**
	 * Conversion to a Basic CodeModel construct
	 */
	public Expression toBasicCodeModel() {
		Expression exp = new BinaryExpression(
				EvaluationContext.getActiveEventExpression().getCopy(),
				getEvent().getReferenceExpression().getCopy(),
				BinaryOperator.EQ_OPERATOR
				);
		return exp;
	}

}