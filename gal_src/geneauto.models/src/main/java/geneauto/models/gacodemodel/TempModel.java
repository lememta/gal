/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/TempModel.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2012-01-07 17:48:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.GAConst;
import geneauto.utils.general.NameFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Temporary storage for model elements that are created
 * during the code-generation transformations and need to be stored
 * temporarily for the next transformation steps.
 * 
 * The elements contained in TempModel are ignored when performing 
 * optimisation and printing output code
 */
public class TempModel extends GACodeModelElement implements GACodeModelRoot {
	
	/**
	 * hash map to get elements quickly
	 * the key of the map is element id and value is element pointer
	 */
	protected Map<String, GAModelElement> elementMap = new HashMap<String, GAModelElement>();	
	
	/**
	 * list of elements for storing to XML 
	 * this is required only because model factory does not support maps
	 */
	protected List<GAModelElement> elements = new ArrayList<GAModelElement>();
	
    /**
     * data-type for model memories
     */
    private CustomType_CM stateVariablesStructType = null;
    
    /**
     * Temporary namespace for NS elements that are first created in 
     * tempModel and only then moved to real model 
     */
    private NameSpace tempNameSpace = null;

	public TempModel(){
		super();		
	}
	
	/**
	 * adds element to TempModel
	 */
	public void addElement(GAModelElement element){
		element.setParent(this);
		elements.add(element);
		elementMap.put(element.getId()+"", element);
	}

	/**
	 * retrieves element from TempModel
	 */
	public GAModelElement getElementById(int p_id){
		GAModelElement element;
		
		// make sure that elementMap is up to date
		populateElementMap();
		
		element = elementMap.get(p_id + "");	
		return element;
	}
	
	/**
	 * deletes element from TempModel
	 */
	public void deleteElementById(int p_id){
		GAModelElement element;
		
		element = elementMap.get(p_id + "");
		elements.remove(element);
		elementMap.remove(p_id + "");		
	}
	
	/**
	 * populates the elementMap
	 * this method must be invoked after a TempModel object is 
	 * constructed by ModelFacotry. In this case we have members in
	 * elements array, but not in elementMap
	 * 
	 *  the method starts populating the elements only in case the size of 
	 *  the elements array is different of size of the elementMap
	 *  so it is safe to call it each time before reading elements from map 
	 */
	public void populateElementMap(){
		if (elements.size() != elementMap.size()){
			elementMap.clear();
			
			for (GAModelElement e : elements){
				elementMap.put(e.getId() + "", e);
			}
		}
	}

    /**
     * Cleans the TempModel: removes all elements that have been moved and have
     * a new parent element.
     */
    public void clean() {
        if (elements != null) {
            List<GAModelElement> oldEls = new LinkedList<GAModelElement>();
            for (GAModelElement el : elements) {
                if (el.getParent() != this) {
                    oldEls.add(el);
                }
            }
            elements.removeAll(oldEls);
        }
    }
    
    /**
     * Clears the TempModel
     */
    public void clear() {
        elementMap.clear();
        elements.clear();
    }

	public List<GAModelElement> getElements() {
		return elements;
	}

	public CustomType_CM getStateVariablesStructType() {
		return stateVariablesStructType;
	}

	public void setStateVariablesStructType(CustomType_CM stateVariablesStructType) {
		this.stateVariablesStructType = stateVariablesStructType;
	}

	public void createStateVariablesStructure(String modelName) {
        // data type for state variables
        CustomType_CM memoryStruct = new CustomType_CM();
        modelName = NameFormatter.normaliseVariableName(GAConst.STRUCT_MEM_PREFIX 
        		+ modelName
        		+ GAConst.STRUCT_MEM_SUFFIX, 
        		"StateVariablesStructure",
        		true);
        memoryStruct.setName(modelName);
        
        // place created model element to model
        getTempNameSpace().addElement(memoryStruct);
        
        // empty StructureContent to be filled in later
        memoryStruct.setContent(new StructureContent());
        // the type is global
        memoryStruct.setScope(DefinitionScope.EXPORTED);
        
        // NB! to be verified -- this can create problems as we now have data 
        // type without a namespace 
    	setStateVariablesStructType(memoryStruct);    		
	}

	public NameSpace getTempNameSpace() {
		if (tempNameSpace == null) {
			tempNameSpace = new NameSpace();
			tempNameSpace.setParent(this);
		}
		
		return tempNameSpace;
	}
}
