package geneauto.models.gacodemodel.statement;

import java.util.List;

/**
 * Interface for statements that has branches
 * for accessing its branches
 * 
 * @author Andrei Rybinsky
 *
 */
public interface StatementWithBranches {
	
	/**
	 * 
	 * @return all branches of branch statement
	 */
	public List<Statement> getBranches();
}
