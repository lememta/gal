/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/GotoStatement.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2010-05-04 12:05:05 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.models.gacodemodel.Label;

/**
 * Goto statement. Note: Gene-Auto should normally not generate code with
 * goto-s. However, goto-s are sometimes useful for intermediate code
 * transformation steps.
 * 
 * Reference to the target Label can be given either directly or by giving a
 * name in the name field. In the last case the name should match the name of
 * some existing Label object. In a final model there should not be any by-name
 * references. If both: a direct reference and name are given, then the direct
 * reference has priority.
 */
public class GotoStatement extends Statement {

    /** Direct reference to the target Label */
    protected Label target;

    public GotoStatement() {
        super();
    }

    public GotoStatement(Label targetLabel) {
        super();
        setTarget(targetLabel);
    }

    public GotoStatement(String labelString) {
        super();
        this.name = labelString;
    }

    public void setTarget(Label targetLabel) {
        this.target = targetLabel;
    }
    
    public Label getTarget() {
        return target;
    }
    
    public Statement getTargetStatement() {
        if (target == null) {
            return null;
        } else {
            return (Statement) target.getParent(); 
        }
    }

    /**
     * @return name of the target label. 
     */
    public String getTargetName() {
    	if (target != null) {
    		return target.getName();
    	} else {
    		return name;
    	}        
    }

    @Override
    public String toString() {
        String targetPart = "";
        targetPart = " -> " + "\"" + getTargetName() + "\"";
        return super.toString() + targetPart;
    }
}