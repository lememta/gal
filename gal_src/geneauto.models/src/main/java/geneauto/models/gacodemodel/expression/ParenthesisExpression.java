/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/ParenthesisExpression.java,v $
 *  @version	$Revision: 1.44 $
 *	@date		$Date: 2011-09-20 13:10:29 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import java.util.ArrayList;
import java.util.List;

/**
 * Parenthesis
 * 
 * 
 */
public class ParenthesisExpression extends Expression {

    protected Expression expression;

    public ParenthesisExpression() {
        super();
    }

    public ParenthesisExpression(Expression expression) {
        super();
        if (expression != null) {
            expression.setParent(this);
        }
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setIndexExpressions(List<Expression> indexExpressions) {
        super.setIndexExpressions(indexExpressions);
        
        expression.setIndexExpressions(indexExpressions);
    }

    public void setExpression(Expression expression) {
        if (expression != null) {
            expression.setParent(this);
        }
        this.expression = expression;
    }

    public boolean isConst() {
        return expression.isConst();
    }

    public Expression eval(boolean printError, boolean optimizableOnly) {
        return expression.eval(printError, optimizableOnly);
    }

    @Override
    public void resetDataType() {
        //expression.resetDataType();
        if (expression != null) {
        	attachDataType(expression.getDataType());
        }
    }
    
    public String printElement(int precLevel) {
        return "(" + this.expression.print(0) + ")"
        + Expression.printIndexPart(this);
    }

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		if (expression != null) {
			result.addAll(expression.getExpendableAtoms());
		}
		return result;
	}
    
    
}