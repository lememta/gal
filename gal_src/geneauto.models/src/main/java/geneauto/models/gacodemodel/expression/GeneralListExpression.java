/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/GeneralListExpression.java,v $
 *  @version	$Revision: 1.3 $
 *	@date		$Date: 2012-02-19 23:05:02 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * List expression. An expression of a style: {5, 6, 7} or {{1, 2}, {3, 4}}
 * 
 * Such expressions are used for initialising structs and arrays in C and in GA.
 * 
 * Note: An expression like myVar[1][2] is a VarExp with two indexExpressions.
 * 
 * modif FaCa : add List<Expression>
 */
public class GeneralListExpression extends ListExpression {

	protected List<Expression> expressions = new ArrayList<Expression>();

    public GeneralListExpression(List<Expression> expressions) {
        super();
        for (Expression expr : expressions) {
            if (expr != null) {
                expr.setParent(this);
            }
        }
        this.expressions = expressions;
    }

    public GeneralListExpression() {
        super();
    }

    @Override
    public List<Expression> getExpressions() {
        return expressions;
    }

    public void addExpression(Expression exp) {
        if (exp != null) {
            if (exp.getParent() != null) {
                exp = exp.getCopy();
            }
            exp.setParent(this);
        }
// AnTo 090704 Removed this check. We don't require that all sublists have the same size.
// ListExpression can be used in more general cases (e.g. for structure initialisation)
//        
//        int size = expressions.size();
//        if (size > 0) {
//            if ((expressions.get(size - 1).getDataType() != null) && 
//                (exp.getDataType() != null)&& (expressions.get(size - 1).getDataType().getDimensionality() 
//                    != exp.getDataType().getDimensionality())) {
//                EventHandler.handle(EventLevel.ERROR, "ListExpression.addDimension", 
//                        "", "dimensionality of ListExpression's members are not the same: "
//                        + "\n" + exp.getReferenceString()
//                        + "," + "\n" + expressions.get(size - 1));
//            }
//        }
        expressions.add(exp);
    }

    public void setExpressions(List<Expression> expressions) {
    	// make sure the current list is empty
    	this.expressions.clear();
    	
    	if (expressions == null) {
    		// the method was called to clear the expressions list
    		return;
    	}
    	boolean uniformCheck = !(getDataType() instanceof TCustom);
    	int count = 0;
    	GADataType firstExpDT = null;
    	TArray firstExpArDT;
    	GADataType expDT;
    	for (Expression expr : expressions) {
    	    /*
    	     * Note! The following test checks, if the list has uniform structure.
    	     * This is only applicable to lists that represent arrays. However, they
    	     * might also represent structures. Hence, this check is only to be
    	     * done, when the datatype is unknown. For structures, the type needs to
    	     * be always existing at this point!
    	     * 
    	     * This check is done only in case data type of expression is not 
    	     * of type TCustom
    	     */
    	    if (uniformCheck) {
    	        if (getDataType() != null) {
    	            expDT = expr.getDataType();
    	            if (count != 0) {
    	                // expr has scalar data type -> must be the same class of DT as firstExp
    	                if (expDT.isScalar()) {

    	                    // expr DT is not scalar -> must have the same dimensionality and equal corresponding
    	                    // dimensions length as firstExp
    	                } else {
    	                    firstExpArDT = (TArray) firstExpDT;

    	                    // if dimensionality or corresponding
    	                    // dimensions length is not equal -> print error 
    	                    if ((expDT.getDimensionality() != firstExpArDT.getDimensionality())) {
    	                        EventHandler.handle(EventLevel.ERROR, "setExpressions", "", 
    	                                "An Attempt to add expression with different number of dimensions "
    	                                + "then the 1. subexpression of ListExpression." 
    	                                + "\nListExpression 1. subexpression's dimensionality: " 
    	                                + firstExpArDT.getDimensionality()
    	                                + "\n added expression's dimensionality: " 
    	                                + expDT.getDimensionality());
    	                    }
    	                }
    	            } else {
    	                firstExpDT = expr.getDataType();
    	            }
    	        }
    	        count++;
    	    }
    	    addExpression(expr);
    	}
    }

    public boolean isConst() {
        Iterator<Expression> iterator = this.expressions.iterator();
        while (iterator.hasNext()) {
            Expression expression = iterator.next();
            if (!(expression.isConst())) {
                return false;
            }
        }
        return true;
    }

    public boolean isConst(double p_value) {
        return false;
    }
   
    public Expression[] asArray() {
        Expression[] exp = new Expression[this.expressions.size()];
        Iterator<Expression> iterator = this.expressions.iterator();
        for (int i = 0; i < this.expressions.size(); i++) {
            exp[i] = iterator.next();
        }
        return exp;
    }


    // TODO: check with Andrei what for this method is used
    public GADataType getDataType(IntegerExpression exp, TArray array) {
        if (dataType == null) {
            
            // first dimension is number of subexpressions of current 
            // list expression
            
            array.addDimension(exp);
            List<Expression> dimensionsToAdd = new ArrayList<Expression>();
            GADataType dt = null;
            List<GADataType> pdtList = new ArrayList<GADataType>();
            List<GADataType> dtList = new ArrayList<GADataType>();
            for (Expression subExp: expressions) {
                if (subExp.getDataType() != null) {
                    pdtList.add(subExp.getDataType().getPrimitiveType());
                    dtList.add(subExp.getDataType());
                }
            }
            dt = DataTypeUtils
                .chooseLeastCommonType(dtList, false);
            if (dt != null) {
                array.setBaseType((TPrimitive) DataTypeUtils
                        .chooseLeastCommonType(pdtList, false));
                dimensionsToAdd = dt.getDimensions();
            } else {
                EventHandler.handle(EventLevel.ERROR, 
                        "getDataType", "",
                        "data type of the the ListExpression can't be determined. "
                        + "\nListExpression: " + this.getReferenceString());
                return null;
            }
            
            for (Expression dim: dimensionsToAdd) {
                array.addDimension(dim.getCopy());
            }
            dataType = array;
            dataType.setParent(this);
        }
        if ((dataType instanceof TArray) || (!getIndexExpressions().isEmpty())) {
            return dataType;
        } else {
            EventHandler.handle( EventLevel.ERROR,
                    "models.gacodemodel.ListExpression", "GED0026",
                    "Illegal data type " + dataType.getClass().getSimpleName() 
                    + " for ListExpression " + this.getReferenceString() 
                    + "\nListExpression must have array type.");
            return null;
        }
    }
    
    

	protected void resetChildrenDataTypes() {
		for (Expression exp: expressions) {
		    exp.resetDataType();
		}
	}
    
	protected void setIndexExpressionsToChildren(List<Expression> tmpIdxExpr) {
		for (Expression e : getExpressions()){
			e.setIndexExpressions(tmpIdxExpr);
		}
	}
    
    public void assertConsistency(){
    	if (expressions == null || expressions.size() == 0){
            EventHandler.handle( EventLevel.CRITICAL_ERROR,
                    "models.gacodemodel.ListExpression", 
                    "GEM0067",
                    "Model consistency error: \nListExpression: " +
                    getReferenceString()
                    + " does not have any members.");
            return;
    	}
    	
    	if (this.getDataType() == null){
            EventHandler.handle( EventLevel.CRITICAL_ERROR,
                    "models.gacodemodel.ListExpression", 
                    "GEM0068",
                    "Model consistency error: \nexpression " +
                    getReferenceString()
                    + " has incomplete type information.");
            return;
    	}

    }
    
    public String printElement(int precLevel) {
        String result = "";
        
        // check that the model element is consistent before printing it
        assertConsistency();
        
        // the expression contains index expression
        // take sub-element by first index and assume it will return 
        // its member by index too if there is more dimensions
        if (getIndexExpressions().size() > 0){
            Integer[] indexes = evaluateIndexes();
            result = getElementByIndex(indexes).printElement(precLevel);
        }
        // plain list expression without index. Print everything in curly 
        // brackets 
        else {
            result += "{";
            Iterator<Expression> iterator = expressions.iterator();
            int count = 0;
            while (iterator.hasNext()) {
                Expression exp = iterator.next();
                result += exp.printElement(precLevel);
                if (count != expressions.size() - 1) {
                    count++;
                    result += ", ";
                }
            }
            result += "}";
        }
        
        return result;
    }

    /**
     * Note: this method only checks the shape. 
     * It must not check the type as it might be unknown yet!
     */
    @Override
    public Expression normalizeShape(boolean full) {        
        Expression exp;
        
        // Convert vector-matrixes, if required
        if (full && isVectorMatrix()) {
            exp = converTArrayMatrix();            
        } else {
            exp = this;
        }
        
        // Convert singleton arrays
        boolean isSingletonArray = true;
        Expression tmpExp = exp;
        
        while (tmpExp instanceof GeneralListExpression) {
            if (tmpExp.getExpressions().size() != 1) {
                isSingletonArray = false;
                break;
            }
            tmpExp = ((GeneralListExpression) tmpExp).getExpressions().get(0); 
        }            
        
        if (isSingletonArray) {
            exp = tmpExp;
        }
        
        return exp;
    }

    /**
     * Note: this method only checks the shape. 
     * It must not check the type as it might be unknown yet!
     */
    public boolean isVectorMatrix() {
        
        if (!assertSubExpressionsExist()) {
            return false;
        }
        
        Expression innerExp = getExpressions().get(0);
        GeneralListExpression innerList = null;
        if (innerExp instanceof GeneralListExpression) {
            innerList = (GeneralListExpression) innerExp;
            if (!innerList.assertSubExpressionsExist()) {
                return false;
            }
        }        
        
        if (getExpressions().size() == 1) {
            // Check, if it is a row matrix 1xN
            if (innerList != null) {
                // It is a row matrix 1xN or an array 1xNxMx...                
                return !(innerList.getExpressions().get(0) instanceof GeneralListExpression);                
            } else {
                // It is a singleton
                return false;                
            }
        } else {
            // Check, if it is a column matrix Nx1
            if (innerList != null) {
                // It is a column matrix Nx1 or a matrix/array NxMx...                
                return innerList.getExpressions().size() == 1;                
            } else {
                // It is a vector already
                return false;                
            }            
        }
    }

    /**
     * Assert that the expression has some sub-expressions.
     */
    public boolean assertSubExpressionsExist() {
        if (getExpressions() == null || getExpressions().isEmpty()) {
            EventHandler.handle(EventLevel.ERROR, 
                    getClass().getSimpleName(), "", 
                    "Subexpression count is 0." +
                    "\n Expression: " + getReferenceString());
            return false;
        }
        return true;
    }

    /**
     * @param newListExp
     * @return
     */
    private GeneralListExpression converTArrayMatrix() {
        GeneralListExpression newListExp = (GeneralListExpression) PrivilegedAccessor.getObjectInPackage(
                GeneralListExpression.class, this);
        boolean error = false;
        
        int n = getExpressions().size();
        
        if (n == 1) {
            Expression e = getExpressions().get(0);
            if (e instanceof GeneralListExpression) {
                newListExp = (GeneralListExpression) e;
                if (newListExp.getExpressions().get(0) instanceof GeneralListExpression) {
                    error = true;
                } 
                
            } else {
                error = true;
            }        
            
        } else if (n > 1) {
            
            // Scan list elements. These should be ListExpressions of length 1
            for (Expression e : getExpressions()) {
                if (e instanceof GeneralListExpression) {
                    GeneralListExpression eLst = (GeneralListExpression) e;
                    if (eLst.getExpressions().size() == 1) {
                        newListExp.addExpression(eLst.getExpressions().get(0));
                    } else {
                        error = true;
                        break;
                    }
                } else {
                    error = true;
                    break;
                }
            }
            
        } else {
            error = true;
        }
        
        if (error) {
            EventHandler.handle(EventLevel.ERROR, 
                    getClass().getSimpleName() + ".converTArrayMatrix()", "",  
                    "Cannot convert given expression to a vector expression." +
                    "\n Expression: " + this.toString() +
                    "\n Reference: " + this.getReferenceString()
                    );
            return null;
        }
        return newListExp;
    }

    @Override
    public boolean isLiteral() {
        for (Expression expression: expressions) {
            if (!expression.isLiteral()) {
                return false;
            }
        }
        return true;
    }
    
    public List<Expression> getDimensions() {
    	if (expressions.isEmpty()) {
    		return Collections.emptyList();
    	} else {
    		List<Expression> dimList = new LinkedList<Expression>();
    		// the first dimension length is the number of expressions
    		IntegerExpression intExp = (IntegerExpression) PrivilegedAccessor.getObjectInPackage(IntegerExpression.class, this);
    		intExp.setLitValue("" + expressions.size());
    		dimList.add(intExp);
    		// check that all the Expressions in the list are of the same size
    		int maxLen = -1;
    		for (Expression e : expressions) {
    			if (e instanceof GeneralListExpression) {
    				if (maxLen == -1) {
    					// first iteration -- memorise the length
    					maxLen = ((GeneralListExpression) e).getExpressions().size();
    				} else if (maxLen != ((GeneralListExpression) e).getExpressions().size()) {
    					// can not construct array data type -- subexpressions
    					// are of different size
    					return Collections.emptyList();
    				}
    			} else if (e instanceof ConstantListExpression) {
    				maxLen = ((ConstantListExpression) e).length;
    			} else {
    				if (maxLen == -1) {
    					// first iteration -- memorise that we had scalar
    					maxLen = 0;
    				} else if (maxLen != 0) {
    					// can not construct array data type -- subexpressions
    					// are of different size
    					return Collections.emptyList();
    				}
    			}    			
    		}
    		
			// is subexpressions were lists then add their dimensions
			// at this point it is safe to simply take the dimensions of
			// the first expression -- the consistency of dimension lengths 
			// was checked earlier
			if (maxLen > 0) {
				dimList.addAll(((GeneralListExpression) expressions.get(0))
														.getDimensions());
			}
			
    		return dimList;
    	}
    }
    
    public TPrimitive getPrimitiveType() {
    	if (dataType == null) {
    		List<GADataType> dtList = new LinkedList<GADataType>();
    		for (Expression e : getExpressions()) {
    			dtList.add(e.getPrimitiveType());
    		}
    		if (dtList.isEmpty()) {
    			return null;
    		} else {
    			return (TPrimitive) DataTypeUtils
    									.chooseLeastCommonType(dtList, false);
    		}
    	} else {
    		return dataType.getPrimitiveType();
    	}
    }

	
    @Override
	public int getSize() {
    	if (expressions != null) {
    		return expressions.size();
    	} else {
    		return 0;
    	}
	}

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		result.add(this);
		for (Expression exp: expressions) {
				result.addAll(exp.getExpendableAtoms());
		}
		return result;
	}
    
    
}