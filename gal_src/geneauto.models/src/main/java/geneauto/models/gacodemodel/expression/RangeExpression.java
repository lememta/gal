/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/RangeExpression.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2010-04-29 13:12:38 $
 *
 *  Copyright (c) 2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

 package geneauto.models.gacodemodel.expression;

import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.PrivilegedAccessor;

/**
 * RangeExpression represents a finite set of integer values identified by start
 * value and end value. The range expression is used for expressing iteration
 * steps.
 * 
 * The dataType of the RangeExpression is a type for a range iterator variable.
 * Any numeric type can be used, however, the value will be always integer. If the
 * type is not explicitly defined in the model, the smallest integer type suitable
 * for accommodating the iterator values is assigned.
 * 
 * If named range expression types are used (e.g. in ADA) the data type is the
 * Derived type expressing the range datatype.
 */
public class RangeExpression extends Expression {

	/**
	 * start of the iteration
	 */
	protected int start;
	/**
	 * end of the iteration
	 */
	protected int end;

    public RangeExpression() {
        super();
    }

    public RangeExpression(int start, int end) {
        super();
        this.end = end;
        this.start = start;
        TRealInteger dt = (TRealInteger) PrivilegedAccessor.getObjectInPackage(
                TRealInteger.class, this);
        DataTypeUtils.scaleFor(dt, start, end);
        setDataType(dt);
    }

    public RangeExpression(int start, int end, GADataType dt) {
        super();
        this.end = end;
        this.start = start;
        setDataType(dt);
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

	/* (non-Javadoc)
	 * @see geneauto.models.gacodemodel.expression.Expression#setDataType(geneauto.models.gadatatypes.GADataType)
	 */
	@Override
	public void setDataType(GADataType dataType) {
		attachDataType(dataType);
	}
    
	public boolean equals(Object other) {
        if (!(other instanceof RangeExpression)) {
            return false;
        }
        RangeExpression otherRange = (RangeExpression) other;
        return start == otherRange.getStart()
            && end == otherRange.getEnd()
            && getDataType().equalsTo(otherRange.getDataType());
    }
	
	@Override
	public String toString() {
		String s = super.toString();
		return s + " start=" + start + ", end=" + end;
	}
}