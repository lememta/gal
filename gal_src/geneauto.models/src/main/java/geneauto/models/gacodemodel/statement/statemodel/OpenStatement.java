/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/statemodel/OpenStatement.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement.statemodel;
import geneauto.models.gacodemodel.expression.TrueExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.gastatemodel.Location;

import java.util.LinkedList;
import java.util.List;

/**
 * StateFlow statement: Open a location (mark as active).
 *
 */
public class OpenStatement extends StateModelStatement {

	/**
	 * Pointer to the Location that is opened.
	 */
	protected Location location;

	/**
	 * Default constructor
	 */
	public OpenStatement() {
		super();
	}

	/**
	 * Constructor with param(s)
	 * 
	 * @param Location
	 */
	public OpenStatement(Location location) {
		this();
		setLocation(location);
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	/**
	 * Conversion to a Basic CodeModel construct
	 */
	public List<Statement> toBasicCodeModel() {
	    List<Statement> stmts = new LinkedList<Statement>();
		Statement stmt = new AssignStatement(
				AssignOperator.SIMPLE_ASSIGN,
				getLocation().getStateVariable().getReferenceExpression(),
				new TrueExpression());
        stmts.add(stmt);
        return stmts;
	}

}