/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/utilities/CodeModeUtilities.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.gacodemodel.expression.TrueExpression;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CodeModeUtilities {

    /**
     * Makes an expression that compares one expression against a list of
     * expressions.
     * 
     * @param left
     *            left expression
     * @param rights
     *            right expressions (expressions to compare against)
     * @return a list compare expression or null, when the list is empty
     */
    public static Expression makeListCompare(Expression left,
            List<Expression> rights) {
        if (rights == null || rights.isEmpty()) {
            return null;
        } else {
            LinkedList<Expression> compExps = new LinkedList<Expression>();
            for (Expression right : rights) {
                Expression compExp = new BinaryExpression(left.getCopy(), right,
                        BinaryOperator.EQ_OPERATOR);
                compExps.add(compExp);
            }
            return makeListBinaryExpression(compExps,
                    BinaryOperator.LOGICAL_OR_OPERATOR);
        }
    }

    /**
     * Makes a binary expression over a list of expressions with a given binary
     * operator
     * 
     * @param exps
     *            - list of expressions to be combined
     * @return a binary expression or null, when the list is empty
     */
    public static Expression makeListBinaryExpression(List<Expression> exps,
            BinaryOperator op) {
        if (exps == null || exps.isEmpty()) {
            return null;
        } else {
            LinkedList<Expression> expsL = new LinkedList<Expression>(exps);
            Expression left, right;
            right = expsL.pollLast(); // Get and remove the last element
            left = expsL.pollLast(); // Get and remove the new last element
            while (left != null) {
                right = new BinaryExpression(left, right, op);
                left = expsL.pollLast(); // Get and remove the last element
            }
            return right;
        }
    }

    /**
     * Makes an optimized AND expression of two (boolean) expressions.
     * 
     * TODO Can be improved by adding evaluation
     * 
     * @param exp1
     *            - expressions 1
     * @param exp2
     *            - expressions 2
     * @return null when both sides are null - effectively means True True when
     *         both sides evaluate to True - not implemented False when one of
     *         the sides evaluates to False - not implemented exp1 when exp2 is
     *         null (or evaluates to True - not implemented) exp2 when exp1 is
     *         null (or evaluates to True - not implemented) exp1 && exp2
     *         otherwise
     */
    public static Expression makeOptimizedAnd(Expression exp1, Expression exp2) {
        if (exp1 == null && exp2 == null) {
            return null;
        } else if (exp1 == null) {
            return exp2;
        } else if (exp2 == null) {
            return exp1;
        } else {
            return new BinaryExpression(exp1, exp2,
                    BinaryOperator.LOGICAL_AND_OPERATOR);
        }
    }

    /**
     * Makes an optimised IF statement based on a given (boolean) condition
     * expression.
     * 
     * @param condition_expression
     * @param then_statement
     * @param else_statement
     * @return then_statement when condition_expression is null (considered
     *         unconditional)
     * 
     *         then_statement when the condition expression is TrueExpression
     * 
     *         else_statement when the condition expression is FalseExpression
     * 
     *         then_statement when the condition expression evaluates to True
     * 
     *         else_statement when the condition expression evaluates to False
     *         
     *         then_statement is null or empty:
     *         if !condition_expression then 
     *             else_statement -- NOT implemented. Maybe in the printer? 
     *              
     *         Otherwise:         
     *         if condition_expression then 
     *             then_statement 
     *             else else_statement
     */
    public static List<Statement> makeOptimizedIf(Expression exp,
            List<Statement> then_statements, List<Statement> else_statements) {

        if (exp == null) {
            // Return then (as specified)
            return then_statements;

        } else {
            
            // Try to evaluate the condition expression
            Expression valExp = exp.eval(false, true);
            boolean isConst;
            boolean val = false; // Redundant initialisation to avoid false
                                 // compiler complaints
            if (valExp instanceof TrueExpression) {            	
                isConst = true;
                val = true;
            } else if (valExp instanceof FalseExpression) {            	
                isConst = true;
                val = false;
            } else {
                isConst = false;
            }
            
            // Compose if
            if (isConst && val) {
                return then_statements;
            } else if (isConst && !val) {
                return else_statements;
            } else {
                CompoundStatement thenStmt = new CompoundStatement(null,
                        then_statements);
                CompoundStatement elseStmt = null;
                if (else_statements != null && !else_statements.isEmpty()) {
                    elseStmt = new CompoundStatement(null, else_statements);
                }
                Statement s = new IfStatement(exp, thenStmt, elseStmt);
                List<Statement> stmts = new LinkedList<Statement>();
                stmts.add(s);
                return stmts;
            }
        }
    }
    
    public static boolean isRowMtrx(Expression vecToCmpr, 
        Expression mtrxToCmpr, boolean isLeftExpr) {
        
        if ((vecToCmpr.getDataType().getDimensionality() == 1) && 
                (mtrxToCmpr.getDataType().getDimensionality() == 2)) {
            
            int[] vectorLength 
                = vecToCmpr.getDataType().getDimensionsLengths();
            int[] matrixLengths = mtrxToCmpr
                .getDataType().getDimensionsLengths();
            if (isLeftExpr) {
                if (vectorLength[0] ==  matrixLengths[0]) {
                    return true;
                } else if (matrixLengths[0] == 1) {
                    return false;
                } else {
                    // TODO: refine error
                    EventHandler.handle(EventLevel.ERROR, 
                            "isRowMtrx", "", 
                            printWrongDTCombinationErrorMsg(isLeftExpr, vectorLength,
                                    matrixLengths));
                    return false;
                }
            } else {
                if (vectorLength[0] ==  matrixLengths[1]) {
                    return false;
                } else if (matrixLengths[1] == 1) {
                    return true;
                } else {
                    EventHandler.handle(EventLevel.ERROR, 
                            "isRowMtrx", "", 
                            printWrongDTCombinationErrorMsg(isLeftExpr, vectorLength,
                                    matrixLengths));
                    return false;
                }
            }
        } else {
            EventHandler.handle(EventLevel.ERROR, 
                    "isRowMtrx", "", 
                    "Illegal dataTypes of expression must be vector and matrix ");
            return false;
        }
    }

    private static String printWrongDTCombinationErrorMsg(boolean isLeftExpr,
            int[] vectorLength, int[] matrixLengths) {
        String errorMsg = "unacceptable argument combination: ";
        String vectorDims = "Vector[" + vectorLength[0] + "]";
        String mtrxDims = "Matrix[" + matrixLengths[0] 
             + "][" + matrixLengths[1] + "]";
        if (isLeftExpr) {
            errorMsg += vectorDims + " . " + mtrxDims;
        } else {
            errorMsg += mtrxDims + " . " + vectorDims; 
        }
        return errorMsg;
    }


    /**
     * Given an expression that is either a literal representing integer or 
     * a ListExpression containing integers returns a corresponding array 
     * of integers.
     * 
     * NOTE: This method could be generalised so that nested lists are flattened.
     * 
     * @param exp the value which is to be converted.
     * @return an array containing the values.
     */
    public static int[] expressionToIntArray(Expression exp) {
    	List<Expression> expLst = flattenExpression(exp);
    	int[] result = new int[expLst.size()];
		int i=0;
		for (Expression subExp : expLst) {
        	try {
				result[i++] = ((IntegerExpression) subExp).getIntValue();
        	} catch (Exception e) {
        		EventHandler.handle(EventLevel.ERROR, "listExpressionToIntArray", 
        				"", "Unexpected kind of expression: " + subExp.getReferenceString());
        		return null;
        	}
        }
        return result;
    }
    
    /**
     * If the given expression is a ListExpression, then 
     * flattens it recursively. I.e. constructs a list of elements 
     * of the supplied ListExpression. These expressions are not
     * ListExpressions any more. 
     * 
     * If the given expression is anything else than a ListExpression, 
     * then returns a singleton list with just this expression.
     * 
     * NOTE: The reurned value is constructed "by-value". I.e. the 
     * sub-expressions are copies of original expressions. This is 
     * somewhat safer.
     * 
     * @param exp the value which is to be converted.
     * @return an array containing the values.
     */
    public static List<Expression> flattenExpression(Expression exp) {
    	List<Expression> lst = new ArrayList<Expression>();
    	
    	if (exp instanceof ListExpression) {
    		for (Expression subExp : ((ListExpression) exp).getExpressions()) {
    			lst.addAll(flattenExpression(subExp));
    		}    		
    	} else {
    		lst.add(exp.getCopy());
    	}
        return lst;
    }

}
