/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/TernaryExpression.java,v $
 *  @version	$Revision: 1.38 $
 *	@date		$Date: 2011-09-20 13:10:30 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.models.gacodemodel.operator.TernaryOperator;
import geneauto.models.utilities.DataTypeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Ternary Expression - expression with a ternary operator and 3 operands
 * 
 * 
 */
public class TernaryExpression extends Expression {

    protected TernaryOperator operator;
    /**
     * first operand
     */
    protected Expression first;
    /**
     * second operand
     */
    protected Expression second;
    /**
     * third operand
     */
    protected Expression third;

    public TernaryExpression() {
        super();
    }

    public TernaryExpression(Expression first, Expression second,
            Expression third, TernaryOperator operator) {
        super();
        if (first != null) {
            first.setParent(this);
        }
        this.first = first;
        if (second != null) {
            second.setParent(this);
        }
        this.second = second;
        if (third != null) {
            third.setParent(this);
        }
        this.third = third;
        this.operator = operator;
    }

    public TernaryOperator getOperator() {
        return operator;
    }

    public void setOperator(TernaryOperator operator) {
        this.operator = operator;
    }

    public Expression getFirst() {
        return first;
    }

    public void setFirst(Expression first) {
        if (first != null) {
            first.setParent(this);
        }
        this.first = first;
    }

    public Expression getSecond() {
        return second;
    }

    public void setSecond(Expression second) {
        if (second != null) {
            second.setParent(this);
        }
        this.second = second;
    }

    public Expression getThird() {
        return third;
    }

    public void setThird(Expression third) {
        if (third != null) {
            third.setParent(this);
        }
        this.third = third;
    }

    public boolean isConst() {
        return ((first.isConst()) && second.isConst() && third.isConst());
    }

    @Override
    public Expression eval(boolean printError, boolean optimizableOnly) {
    	Expression condValExp = first.eval(); 
        if (condValExp instanceof TrueExpression) {
            return second.eval();
        } else if (condValExp instanceof FalseExpression) {
            return third.eval();
        } else {
        	// Failed to evaluate
        	return null;        	
        }
    }
    
    public String printElement(int precLevel) {
        String fstExpPart = "";
        String sndExpPart = "";
        String trdExpPart = "";
        int innerPrecedence = 0;

        // First operand part
        if (first != null) {
            fstExpPart = this.first.printElement(innerPrecedence);
        } else {
            printNoRequiredFieldError("first");
        }

        // Second operand part
        if (second != null) {
            sndExpPart = this.second.printElement(innerPrecedence);
        } else {
            printNoRequiredFieldError("second");
        }

        // Third operand part
        if (third != null) {
            trdExpPart = this.third.printElement(innerPrecedence);
        } else {
            printNoRequiredFieldError("third");
        }

        // Combine expression
        String code = "";
        if (operator != null) {
            innerPrecedence = operator.getPrecedence();
            if (operator == null) {
                printNoRequiredFieldError("operator");
                return "";
            }
            code = operator.printElement(fstExpPart,
                    sndExpPart, trdExpPart);
        } else {
            printNoRequiredFieldError("operator");
        }

        return code;
    }

    @Override
    public void resetDataType() {
    	if (first != null){
    		first.resetDataType();
    	}
    	if (second != null) {
    		second.resetDataType();
    	}
    	if (third != null) {
            third.resetDataType();    		
    	}
    	
    	attachDataType(
    			DataTypeUtils.chooseLeastCommonType(second.getDataType(), 
                third.getDataType(), true));
    }

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		if (first != null) {
			result.addAll(first.getExpendableAtoms());
		}
		if (second != null) {
			result.addAll(second.getExpendableAtoms());
		}
		if (third != null) {
			result.addAll(third.getExpendableAtoms());
		}
		return result;
	}
    
    
}
