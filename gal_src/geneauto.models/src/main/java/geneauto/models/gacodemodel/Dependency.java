/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/Dependency.java,v $
 *  @version	$Revision: 1.24 $
 *	@date		$Date: 2011-05-23 10:08:06 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

/**
 * Dependency of one module on another module. In C this will be implemented as
 * #include statement
 */
public abstract class Dependency extends GACodeModelElement
		implements Comparable<Dependency> {

    public Dependency() {
        super();
    }

    public Dependency(String name) {
        this();
        this.name = name;
    }

    /**
     * Setter for the attribute name. In case dependency contains extension ->
     * sets the string without extension
     */
    public void setName(String name) {

        // name contains "."
        int pointIndex = name.lastIndexOf(".");
        if (pointIndex != -1) {
            name = name.substring(0, pointIndex);
        }
        super.setName(name);
    }
    
    @Override
    public Dependency getCopy() {
        return (Dependency) super.getCopy();
    }

	/**
	 * Basic sorting by name to get a deterministic order. The sorting criteria
	 * can be extended by introducing inter-Dependency dependencies, if required
	 */
	@Override
	public int compareTo(Dependency dep2) {
		return getName().compareTo(dep2.getName());
	}

    /*@Override
    public int hashCode() {
        return getName().hashCode();
    }*/
    
}