/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/SequentialComposition.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.genericmodel.GAModelElement;

import java.util.List;

/**
 * Interface for CodeModel elements that contain a sequential composition (a
 * list) of statements.
 * 
 * Known implementors of this interface are: 
 * CompoundStatment, SequentiolFunctionBody, WhenCase, DefaultCase
 * 
 * 
 */
public interface SequentialComposition {
    /**
     * 
     * @return attribute of type List<Statement> elements
     */
    public List<Statement> getStatements();

    /**
     * Adds Statements to the List of Statements of argument of type
     * List<Statemetn>
     * 
     * @param st
     *            statement to be added to the List
     */
    public void setStatement(Statement st);

    /**
     * Adds Statement to the List of Statements of argument of type
     * List<Statemetn> at the position index
     * 
     * @param index
     *            position of statement being put
     * @param st
     *            to be added to the List
     */
    public void setStatement(int index, Statement st);

    public List<Statement> getAllSubStatements();

    /**
     * 
     * @return false - if it is SequntialFunctionBody, isConditional() method
     *         (in Statement) result - if it is CompoundStatement
     */
    public boolean isConditional();

    /**
     * 
     * @return parent of current element
     */
    public GAModelElement getParent();

    /**
     * 
     * @return namespace of current element
     */
    public NameSpace getNameSpace();

    /**
     * 
     * @return name space or sequential function body 
     * where current element of the current statement is located
     */
    public SequentialComposition getStatementSpace();
}
