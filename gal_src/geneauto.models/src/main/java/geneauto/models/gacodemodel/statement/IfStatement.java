/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/IfStatement.java,v $
 *  @version	$Revision: 1.33 $
 *	@date		$Date: 2011-10-11 11:06:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.models.gacodemodel.expression.Expression;

import java.util.ArrayList;
import java.util.List;

/**
 * If-Then-Else statement
 */
public class IfStatement extends Statement implements StatementWithBranches{

    /** condition expression of the if statement */
	protected Expression conditionExpression;
	/** statement to execute when condition was true */
	protected Statement thenStatement;
	/** statement to execute when condition was false */
	protected Statement elseStatement;

	/**
	 * Default constructor of the class
	 */
	public IfStatement() {
		super();
	}

	/**
	 * Constructor of the class
	 * 
	 * @param conditionalExpression
	 * @param thenStatement
	 * @param elseStatement
	 */
	public IfStatement(Expression conditionalExpression,
			Statement thenStatement, Statement elseStatement) {
		super();
		setConditionExpression (conditionalExpression);
		setElseStatement (elseStatement);
		setThenStatement (thenStatement);
	}

	public Expression getConditionExpression() {
		return conditionExpression;
	}

	public void setConditionExpression(Expression expr) {
		if(expr != null){
			if (expr.getParent() == null){ 
				this.conditionExpression = expr;
			}
			else {
				this.conditionExpression = expr.getCopy();
			}
			this.conditionExpression.setParent(this);	
		}	
		else
			this.conditionExpression = null;		
	}

	public Statement getElseStatement() {
		return elseStatement;
	}

	public void setElseStatement(Statement elseStatement) {
		if (elseStatement != null) {
			elseStatement.setParent(this);
		}
		this.elseStatement = elseStatement;
	}

	public Statement getThenStatement() {
		return thenStatement;
	}

	public void setThenStatement(Statement thenStatement) {
		if (thenStatement != null) {
			thenStatement.setParent(this);
		}
		this.thenStatement = thenStatement;
	}

	@Override
	public List<Statement> getAllSubStatements() {
		List<Statement> stList =  new ArrayList<Statement>();
		stList.add(thenStatement);
		if (!thenStatement.getAllSubStatements().isEmpty()) {
			stList.addAll(thenStatement.getAllSubStatements());
		}
		if (getElseStatement() != null) {
			stList.add(elseStatement);
			if (!getElseStatement().getAllSubStatements().isEmpty()) {
				stList.addAll(getElseStatement().getAllSubStatements());
			}
		}
		return stList;
	}
	
    @Override
    public boolean containsStmts() {
        return true;
    }

    @Override
    public int setSequenceNumber(int number) {
        super.setSequenceNumber(number);
        if (thenStatement != null) {
            number = thenStatement.setSequenceNumber(++number);
        }
        if (elseStatement != null) {
            number = elseStatement.setSequenceNumber(++number);
        }
        return number;
    }

	@Override
	public List<Statement> getBranches() {
		List<Statement> result = new ArrayList<Statement>();
		if (thenStatement != null) {
			result.add(thenStatement);
		}
		if (elseStatement != null) {
			result.add(elseStatement);
		}
		return result;
	}
    
    
}