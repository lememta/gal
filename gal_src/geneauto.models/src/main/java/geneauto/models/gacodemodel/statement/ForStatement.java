/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/ForStatement.java,v $
 *  @version	$Revision: 1.36 $
 *	@date		$Date: 2011-10-28 14:19:31 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;

import java.util.ArrayList;
import java.util.List;

/**
 * ForStatement implements For-loop in the program code The components of for
 * statement are:
 * <ul>
 * <li>preStatement -- a statement executed before entering the first loop</li>
 * <li>postStatement -- a statement executed after each loop </li>
 * <li>conditionalExpression -- an expression to determine the terminate
 * condition </li>
 * <li>bodyStatement -- a statement to execute as loop body</li>
 * </ul>
 */
public class ForStatement extends LoopStatement {

	/** condition expression of the for loop */
	protected Expression conditionExpression;
	/** a statement to execute after each loop */
	protected Statement postStatement;
	/** a statement to execute before entering the first loop */
	protected Statement preStatement;
	
	public ForStatement() {
		super();
	}

	/**
	 * Constructor of the class
	 * 
	 * @param conditionalExpression
	 * @param preStatement
	 * @param postStatement
	 * @param bodyStatement
	 */
	public ForStatement(Expression conditionalExpression,
			Statement preStatement, Statement postStatement, 
			Statement bodyStatement) {
		super();
		setConditionExpression (conditionalExpression);
		setPostStatement (postStatement);
		setPreStatement (preStatement);
		setBodyStatement (bodyStatement);
	}

    /**
     * Constructor for the default ForStatement, e.g.: for (int tempVarName = 0;
     * tempVArName < maxCondition; tempVarName++) { <bodyStatement> }
     * 
     * @param loopVar
     *            name of the loop variable inside the for-clause
     * @param maxCondition
     *            max value of the temporary variable
     * @param body
     *            body of the ForStatement       
     */
    public ForStatement(Variable_CM loopVar, int maxCondition, Statement body) {
        
        VariableExpression varExp   = new VariableExpression(loopVar);
        IntegerExpression intExp    = new IntegerExpression(0);
        IntegerExpression intExp2   = new IntegerExpression(maxCondition);
        IncStatement unSt           = new IncStatement(varExp);
        
        setPreStatement(new AssignStatement(AssignOperator.INIT_ASSIGN,
                varExp, intExp));
        
        setPostStatement(unSt); 
        setConditionExpression(new BinaryExpression(varExp, intExp2,
                BinaryOperator.LT_OPERATOR));
        setBodyStatement(body);
    }

	public Expression getConditionExpression() {
		return conditionExpression;
	}

	public void setConditionExpression(Expression expr) {
		if(expr != null){
			if (expr.getParent() == null){ 
				this.conditionExpression = expr;
			}
			else {
				this.conditionExpression = (Expression) expr.getCopy();
			}
			this.conditionExpression.setParent(this);	
		}	
		else
			this.conditionExpression = null;		
	}

	public Statement getPostStatement() {
		return postStatement;
	}

	public void setPostStatement(Statement postStatement) {
		if (postStatement != null) {
			postStatement.setParent(this);
		}
		this.postStatement = postStatement;
	}

	public Statement getPreStatement() {
		return preStatement;
	}

	public void setPreStatement(Statement preStatement) {
		if (preStatement != null) {
			preStatement.setParent(this);
		}
		this.preStatement = preStatement;
	}

	@Override
	public List<Statement> getAllSubStatements() {
		List<Statement> stList =  new ArrayList<Statement>();
		if (getPostStatement() != null) {
			stList.add(postStatement);
			if (!getPostStatement().getAllSubStatements().isEmpty()) {
				stList.addAll(getPostStatement().getAllSubStatements());
			}
		}
		if (getPreStatement() != null) {
			stList.add(preStatement);
			if (!getPreStatement().getAllSubStatements().isEmpty()) {
				stList.addAll(getPreStatement().getAllSubStatements());
			}
		}
		if (getBodyStatement() != null) {
			stList.add(bodyStatement);
			if (!getBodyStatement().getAllSubStatements().isEmpty()) {
				stList.addAll(getBodyStatement().getAllSubStatements());
			}
		}
		return stList;
	}
	
    @Override
    public boolean containsStmts() {
        return true;
    }

    @Override
    public int setSequenceNumber(int number) {
        super.setSequenceNumber(number);
        if (preStatement != null) {
            number = preStatement.setSequenceNumber(++number);
        }
        if (postStatement != null) {
            number = postStatement.setSequenceNumber(++number);
        }
        if (bodyStatement != null) {
            number = bodyStatement.setSequenceNumber(++number);
        }
        return number;
    }
    
    
}