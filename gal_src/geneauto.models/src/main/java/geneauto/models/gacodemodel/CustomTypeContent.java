/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/CustomTypeContent.java,v $
 *  @version	$Revision: 1.38 $
 *	@date		$Date: 2011-07-07 12:23:41 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.expression.Expression;

import java.util.List;

/**
 * Custom target language type's content type. In C the part between typedef
 * ...; will be generated based on this.
 */
public abstract class CustomTypeContent extends GACodeModelElement {

    public CustomTypeContent() {
        super();
    }

    public List<StructureMember> getStructMembers() {
        return null;
    }

    /**
     * Dummy in the base class
     * 
     * @param name
     * @return
     */
    public StructureMember getMemberByName(String p_name) {
        GACodeModelElement.printUnsupportedMethodError(this, 
                "getMemberByName");
        return null;
    }
    
	/** 
	 * This is not null, when a common initial value is specified for all
	 * elements of this type
	 */
    public abstract Expression getInitialValue();
    
    public String printInitialValue() {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
                "GEM0012",
                "Call to unimplemented or unsupported method: printInitialValue()"
                + "\n Element: " + getReferenceString()
                + "\n Class: " + getClass().getCanonicalName());
        return null;
    }

    /**
     * 
     * @return CustomType object, that owns this custom type content element
     */
    public CustomType getContainingCT() {
    	return (CustomType) getParent();
    }
    
    public abstract boolean equalsTo(CustomTypeContent other);
        
}