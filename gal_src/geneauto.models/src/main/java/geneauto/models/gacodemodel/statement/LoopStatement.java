/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/LoopStatement.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2010-08-20 11:32:08 $
 *
 *  Copyright (c) 2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

package geneauto.models.gacodemodel.statement;

import geneauto.models.genericmodel.Annotation;

import java.util.List;

/*
 * An abstract class representing a loop
 * The bodyStatement is executed according to loop condition
 * Loop conditions must be defined by concrete subclasses of this class
 */
public abstract class LoopStatement extends Statement {

	/** a statement to execute as loop body */
	protected Statement bodyStatement;

	public LoopStatement() {
		super();
	}

	public LoopStatement(Annotation annotation) {
		super(annotation);
	}

	public LoopStatement(List<Annotation> annotations) {
		super(annotations);
	}

	public Statement getBodyStatement() {
		return bodyStatement;
	}

	public void setBodyStatement(Statement bodyStatement) {
		if (bodyStatement != null) {
			bodyStatement.setParent(this);
		}
		this.bodyStatement = bodyStatement;
	}

	public void setBodyStatements(List<Statement> bodyStatements) {
		if (bodyStatements != null) {
			CompoundStatement cs = new CompoundStatement(bodyStatements);
			cs.setParent(this);
			this.bodyStatement = cs;
		} else {
			this.bodyStatement = null;
		}
		
	}
}