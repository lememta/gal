/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/ArrayContent.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.utilities.DataTypeUtils;

/**
 * This class represents custom type (content) that is array 
 * with defined base type and range   
 * 
 * @author Andrey Rybinski
 *
 */
public class ArrayContent extends CustomTypeContent {
    /**
     * custom type that 
     */
    protected GADataType baseType;
    
    protected RangeExpression range;
    
    public ArrayContent() {
        super();
    }

    public ArrayContent(GADataType baseType, RangeExpression range) {
        super();
        setBaseType(baseType);
        setRange(range);
    }

    @Override
    public boolean equalsTo(CustomTypeContent other) {
        if (other instanceof ArrayContent) {
            ArrayContent arrayContent = (ArrayContent) other;
            if (!(getBaseType().equalsTo(arrayContent.getBaseType()))) {
                return false;
            }
            RangeExpression otherRange = arrayContent.getRange();
            if (otherRange == null) {
                return false;
            }
            if (!(range.getDataType().
                    equalsTo(otherRange.getDataType()))) {
                return false;
            }
            return range.getStart() == otherRange.getStart()
                    && (range.getEnd() == otherRange.getEnd());
            
        } 
        return false;
    }

    @Override
    public Expression getInitialValue() {
        // TODO Auto-generated method stub
        return null;
    }

    public GADataType getBaseType() {
        return baseType;
    }

    public void setBaseType(GADataType baseType) {
        attachDataType(baseType);
    }

    /**
     * Attaches given data type to object
     *   - when given data type does not have parent, then we attach it to current object
     *   - when given data type has already a parent, we make copy and attach this to current object
     * @param dt
     */
    protected void attachDataType(GADataType dt) {
        dt = DataTypeUtils.attachDataType(dt);
        if (dt != null) {
            dt.setParent(this);
        }
        baseType = dt;
    }
    
    public RangeExpression getRange() {
        return range;
    }

    public void setRange(RangeExpression range) {
        this.range = range;
        if (range != null) {
            range.setParent(this);
        }
    }

}
