/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/BooleanExpression.java,v $
 *  @version	$Revision: 1.14 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.utils.PrivilegedAccessor;

import org.junit.Assert;

public abstract class BooleanExpression extends LiteralExpression {
    
    public BooleanExpression() {
        super();
    }
    
    public BooleanExpression(String litValue) {
        super(litValue);
    }

    public abstract boolean getBooleanValue();
    
    /**
     * If data type is not defined sets it to boolean
     */
    @Override
    public void resetDataType() {
    	if (dataType == null) {
    		dataType = (TBoolean) PrivilegedAccessor
            	.getObjectInPackage(TBoolean.class, this);
    		attachDataType(dataType);
    	}
    }
    
    public static BooleanExpression getBooleanInstance (String value) {
        Assert.assertNotNull(value, "BooleanExpression: getInstance");
        if (value.equals("0") || value.equals("false")) {
            return new FalseExpression();
        } else if (value.equals("1") || value.equals("true")) {
            return new TrueExpression();
        } else {
            EventHandler.handle(EventLevel.ERROR, "getBooleanInstance", "", 
                    "Unacceptable litValue (" + value + ") for Boolean Expression."
                    + "/n Only 'true', 'false', '1' and '0' is acceptable");
            return null;
        }
    }
    
    @Override
    public void setLitValue(String litValue) {
        // we do not allow to change value in True or False Expression
    }
    
	@Override
	public Expression normalizeShape(boolean full) {
		// this is of scalar type already. do nothing
		return this;
	}    

	@Override
	// TODO Consider usage of this function. Converting a boolean value to real 
	// might not be a good thing to do.
    public DoubleExpression evalReal() {		
		return new DoubleExpression(litValue);
    }
   
}
