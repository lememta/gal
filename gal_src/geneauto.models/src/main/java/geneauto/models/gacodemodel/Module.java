/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/Module.java,v $
 *  @version	$Revision: 1.67 $
 *	@date		$Date: 2010-10-01 13:37:49 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Abstract entity representing a software module.
 */
public class Module extends GACodeModelElement 
        implements GACodeModelRoot, HasNameSpace {

    /**
     * Dependency on another module. (Corresponds to #include in C). Information
     * that is needed can include: header file to be included exported names
     * (module level namespace)
     */
    protected List<Dependency> dependencies = new ArrayList<Dependency>();

    /**
     * Namespace for owned variables, functions and types
     */
    protected NameSpace nameSpace;

    /**
     * Description about the module to be inserted into the header file. Note!
     * The general description property is redundant for Module.
     */
    protected Annotation headerAnnotation;

    /**
     * Header file name Equal to module name by default If header file name does
     * not have extension the printer component should add one
     */
    protected String headerFileName;

    /**
     * Source file name Equal to module name by default If source file name does
     * not have extension the printer component should add one
     */
    protected String sourceFileName;
    
    /**
     * Init function (<Module>_init) of the Module
     */
    protected Function_CM initFunction;


    /*
     * Creates module element and its NameSpace
     */
    public Module() {
        super();

        // module always has NameSpace
        setNameSpace(new NameSpace());
        
        // module name is never changeable
        setFixedName(true);
    }

    /*
     * Creates module element and its NameSpace sets the module name together
     * with source and header file names
     */
    public Module(String moduleName) {
        this();
        setName(moduleName);
    }

    public NameSpace getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(NameSpace namespace) {
        namespace.setParent(this);
        this.nameSpace = namespace;
    }

    public Function_CM getInitFunction() {
        return initFunction;
    }

    public void setInitFunction(Function_CM initFunction) {
        this.initFunction = initFunction;
    }

    public String getHeaderFileName() {
        if (headerFileName == null) {
            setHeaderFileName(getName());
        }
        return headerFileName;
    }

    public String getSourceFileName() {
        if (sourceFileName == null) {
            setSourceFileName(getName());
        }
        return sourceFileName;
    }

    public List<Dependency> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<Dependency> depList) {
        this.dependencies.clear();
        for (Dependency d : depList) {
            addDependency(d);
        }
    }

    public void addDependencies(List<Dependency> dependencies) {
        for (Dependency d : dependencies) {
            addDependency(d);
        }
    }

    /**
     * Adds a dependency if it is not already in the dependencies
     * 
     * When added dependency does not have parent it is placed under 
     * the current module.
     * When added dependency has parent a copy is made and the copy is placed 
     * under the current module. The original dependency remains where it is. 
     * 
     * @param dependency
     *            dependency to add
     */
    public void addDependency(Dependency dependency) {

        // check, if a dependency with such name already exists
        //if (containsDependency(dependency)) {
        if (dependencies.contains(dependency)) {
            // dependency already exists
            return;
        } else {
        	if (dependency.getParent() == null) {
        		dependency.setParent(this);
                dependencies.add(dependency); 
        	} else {
        		Dependency newDependency = dependency.getCopy();
        		newDependency.setParent(this);
                dependencies.add(newDependency);         		
        	}
        }
    }

    public Annotation getHeaderAnnotation() {
        return headerAnnotation;
    }

    public void setHeaderAnnotation(String s) {
        this.headerAnnotation = new Annotation(s);
    }

    public void setHeaderAnnotation(Annotation headerAnnotation) {
        this.headerAnnotation = headerAnnotation;
    }

    /**
     * NB! if if the caller needs to set header and source file names
     * explicitly, then it must be made after calling setName, as setName
     * overrides the header and source file with defaults
     */
    public void setHeaderFileName(String headerFileName) {
        this.headerFileName = headerFileName;
    }

    /**
     * NB! if if the caller needs to set header and source file names
     * explicitly, then it must be made after calling setName, as setName
     * overrides the header and source file with defaults
     */
    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }

    /**
     * @return all NameSpaceElements exported by the current Module. It is
     *         assumed that such elements exist only in the top-level NameSpace
     */
    public List<NameSpaceElement_CM> getExportedElements() {
        List<NameSpaceElement_CM> lst = new LinkedList<NameSpaceElement_CM>();
        for (NameSpaceElement_CM el : getNameSpace().getNsElements()) {
            if (el.getScope() == DefinitionScope.EXPORTED) {
                lst.add(el);
            }
        }
        return lst;
    }

    /**
     * 
     * @return all Statements of Current Module. The Statements are ordered by
     *         the appearance in the CodeModel
     */
    public List<Statement> getStatements() {
        List<Statement> stList = new ArrayList<Statement>();
        for (GAModelElement element : getNameSpace().getNsElements()) {
            if (element instanceof Function_CM) {
                Function_CM fun = (Function_CM) element;
                if (fun.getBody() instanceof SequentialFunctionBody) {
                    SequentialFunctionBody funBody = (SequentialFunctionBody) fun
                            .getBody();
                    for (Statement st : funBody.getStatements()) {
                        stList.add(st);
                        if (!st.getAllSubStatements().isEmpty()) {
                            stList.addAll(st.getAllSubStatements());
                        }
                    }
                }

            }
        }
        return stList;
    }

    /*
     * Sets the module name, header and source file names (the latter are equal
     * to module name by default)
     * 
     * NB! if if the caller needs to set header and source file names
     * explicitly, then it must be made after calling setName
     * 
     * (non-Javadoc)
     * 
     * @see
     * geneauto.models.genericmodel.GAModelElement#setName(java.lang.String)
     */
    public void setName(String moduleName) {
        super.setName(moduleName);
        setHeaderFileName(moduleName);
        setSourceFileName(moduleName);
    }

    @Override
    public Module getModule() {
        return this;
    }
    
    public boolean containsDependency(Dependency dependency) {
        for (Dependency moduleDependency: dependencies) {
            if (moduleDependency.equals(dependency)) {
                return true;
            }
        }
        return false;
    }
}