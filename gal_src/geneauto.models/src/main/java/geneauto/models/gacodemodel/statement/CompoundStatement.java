/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/CompoundStatement.java,v $
 *  @version	$Revision: 1.51 $
 *	@date		$Date: 2011-11-28 22:44:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.models.gacodemodel.HasNameSpace;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.SequentialComposition;

import java.util.ArrayList;
import java.util.List;

/**
 * Compound statement is a group of statements encapsulated in common local
 * scope. The scope may contain local variables
 */
public class CompoundStatement extends Statement 
        implements SequentialComposition, HasNameSpace {

    protected NameSpace nameSpace;
    protected List<Statement> statements = new ArrayList<Statement>();
    /**
     * In order to guarantee correct semantics for the "Treat as atomic unit" 
     * flag in simulink subsystem a mean for grouping statements is required
     * 
     * This flag is used to notify the printer, that the compound statement 
     * scope is ignored and only its contents are printed. 
     */
    protected boolean isVirtual;
    
    /**
     * If true, the statements belonging to this compound statement must 
	 *	be kept together through all transformations
     */
    protected boolean isAtomic = true;

    public CompoundStatement() {
        super();
    }

    public CompoundStatement(List<Statement> statements) {
        super();
        addStatements(statements);
    }
    
    public CompoundStatement(NameSpace nameSpace, List<Statement> statements) {
        super();
        if (nameSpace != null) {
            nameSpace.setParent(this);
        }
        this.nameSpace = nameSpace;

        addStatements(statements);
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public void setStatements(List<Statement> statements) {
        for (Statement st : statements) {
            if (st != null) {
                st.setParent(this);
            }
        }
        this.statements = statements;
    }

    public void addStatement(Statement statement) {
        statement.setParent(this);
        this.statements.add(statement);
    }

    public void addStatements(List<Statement> statement) {
        for (Statement st : statement) {
            st.setParent(this);
            this.statements.add(st);
        }
    }

    public NameSpace getNameSpace() {
    	if (nameSpace == null) {
    	    setNameSpace(new NameSpace());
    	}
    	
        return nameSpace;
    }

    public void setNameSpace(NameSpace nameSpace) {
        if (nameSpace != null) {
            nameSpace.setParent(this);
        }
        this.nameSpace = nameSpace;
    }

    public int getSubStatementsCnt() {
        return statements.size();
    }

    @Override
    public List<Statement> getSubStatements() {
        List<Statement> l_statements = new ArrayList<Statement>();
        l_statements.addAll(getStatements());
        return l_statements;
    }

    @Override
    public List<Statement> getAllSubStatements() {
        List<Statement> stList = new ArrayList<Statement>();
        for (Statement st : getStatements()) {
            stList.add(st);
            if (st.getAllSubStatements() != null) {
                stList.addAll(st.getAllSubStatements());
            }
        }
        return stList;
    }

    @Override
    public void setStatement(Statement st) {
        if (st != null) {
            st.setParent(this);
            this.statements.add(st);
        }
    }

    @Override
    public void setStatement(int index, Statement st) {
        st.setParent(this);
        this.getStatements().add(index, st);
    }
    
    @Override
    public boolean containsStmts() {
        return true;
    }

    @Override
    public int setSequenceNumber(int number) {
        super.setSequenceNumber(number);
        for (Statement st: statements) {
            number = st.setSequenceNumber(++number);
        }
        return number;
    }

    @Override
    public boolean isEmptyStatement() {
        if (statements == null || statements.isEmpty()) {
            return true;
        }
        for (Statement st: statements) {
            if (!st.isEmptyStatement()) {
                return false;
            }
        }
        return true;
    }

    public boolean isVirtual() {
        return isVirtual;
    }

    public void setVirtual(boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

    public boolean isAtomic() {
        return isAtomic;
    }

    public void setAtomic(boolean isAtomic) {
        this.isAtomic = isAtomic;
    }

    
}