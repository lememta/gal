/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/CallExpression.java,v $
 *  @version	$Revision: 1.83 $
 *	@date		$Date: 2011-09-20 13:10:30 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Function;
import geneauto.models.gacodemodel.CodeModelDependency;
import geneauto.models.gacodemodel.Dependency;
import geneauto.models.gacodemodel.Function_CM;
import geneauto.models.gacodemodel.Module;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TPointer;
import geneauto.models.gasystemmodel.common.SystemFunction;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.map.ConstantMap;

import java.util.ArrayList;
import java.util.List;

/**
 * An expression representing a function call
 */
public class CallExpression extends Expression {

    /**
     * Reference to executed function
     */
    protected Function function;
    /**
     * Reference to the module that contains the called function (optional).
     */
    protected Dependency dependency = null;
    /**
     * Function arguments
     */
    protected List<Expression> arguments = new ArrayList<Expression>();

    public CallExpression() {
        super();
    }

    public CallExpression(Function function, List<Expression> arguments) {
        super();
        // it is important to call setFunction here,
        // because it sets also the data type
        setFunction(function);
        setArguments(arguments);
    }

    public CallExpression(String name, 
    		List<Expression> arguments,
    		GADataType dt) {
        super();
        setArguments(arguments);
        setName(name);
        setDataType(dt);
    }

    public Dependency getDependency() {
        return dependency;
    }

    public void setDependency(Dependency dependency) {
        dependency.setParent(this);
        this.dependency = dependency;
    }

    // sets function and data type
    public void setFunction(Function funCalled) {
        function = funCalled;
        setDataType(function.getDataType());
    }

    public List<Expression> getArguments() {
        return arguments;
    }

    public void setArguments(List<Expression> args) {
        this.arguments.clear();
        if (args != null) {
            for (Expression expr : args) {
                addArgument(expr);
            }
        }
    }

    public void addArgument(Expression arg) {
        arg.setParent(this);
        arguments.add(arg);
    }
    
    public void replaceArgument(int index, Expression newArgument) {
        newArgument.setParent(this);
        arguments.add(index, newArgument);
        arguments.remove(arguments.get(index + 1));
    }
 
    public Function getFunction() {
        return function;
    }

    public boolean isConst() {
        return false;
    }

    public boolean isConst(double p_value) {
        // we can not guarantee, that function returns constant value
        return false;
    }

    @Override
    public void resetDataType() {
        for (Expression arg: arguments) {
            arg.resetDataType();
        }
        
        if (this.dataType == null && function != null) {
            attachDataType(function.getDataType());
        }
    }
    
    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    @Override
    public void setDataType(GADataType dataType) {
        this.attachDataType(dataType);
    }

    /**
     * Resolves by-name references in the current element only. See also:
     * resolveReferences
     * 
     * @param ConstantMap
     *            <String, GAModelElement> nameMap
     */
    @Override
    public GAModelElement resolveCurrentElement(
            final ConstantMap<String, GAModelElement> nameMap) {
        // Resolve only, when the function field is empty
        if (this.getFunction() == null) {
            GAModelElement el = nameMap.get(this.getName());
            if (el != null) {
                if (el instanceof Function) {
                    Function sel = (Function) el;
                    // Bind the Expression with the found object.
                    setFunction(sel);
                } else {
                    EventHandler.handle(EventLevel.ERROR,
                            "CallExpression.resolveCurrentElement", "",
                            "Expecting IsFunction: " + el.getReferenceString());
                }
            } else {
    			if (nameMap.containsKey(this.getName())) {
    				EventHandler.handle(EventLevel.ERROR,
    						getClass().getSimpleName() + ".resolveReferencesInCurrent", 
    							"", 
    							"Multiple definitions for function " 
    							+ this.getName() 
    							+ ". Can not resolve reference by name.\n"
    							+ "Current element: " + getReferenceString());
    			} else {
                    // Do nothing. Assume it is a call to an external function.
                    // The call will be made by name.
    			}
            }
        }
        return null;
    }

    @Override
    public Expression toCodeModel() {

        // If the "function" field points to a SystemModel Function.
        if (getFunction() instanceof SystemFunction) {
            SystemFunction sel = (SystemFunction) getFunction();
            GAModelElement cRef = sel.getReferenceExpression();
            if (cRef != null) {
                if (cRef instanceof CallExpression) {
                    /*
                     * Replace the original CallExpression in the parent with
                     * the reference expression obtained from the matched
                     * element in the map
                     */
                    CallExpression newExp = (CallExpression) cRef.getCopy();
                    /*
                     * Move the arguments from the old expression and convert
                     * them to CodeModel (NOTE: This makes use of the general
                     * assumption in the toCodeModel method, that it is allowed
                     * to destroy the old expression. It should normally done 
                     * only once for each Expression instance.)
                     * We also assume, that the reference expression either 
                     * contains no arguments or the arguments here are to be 
                     * appended to the argument list.
                     * A copy is needed, because generally the reference 
                     * expression can be required by others too.
                     */
                    for (Expression argExp : this.getArguments()) {                        
                        newExp.addArgument(argExp);
                        // Call toCodeModel only after the new parent has been
                        // set, as the Expression might replace itself via the
                        // toCodeModel() method.
                        argExp.toCodeModel();
                    }
                    
                    newExp.setIndexExpressions(this.getIndexExpressions());
                    for (Expression e : newExp.getIndexExpressions()) {
                        e.toCodeModel();
                    }

                    // Replace the old object with the new one in the parent's
                    // fields
                    if (getParent() != null) {
                        replaceMe(newExp);
                    }
                    return newExp;

                } else {
                    EventHandler.handle(EventLevel.ERROR,
                            "CallExpression.toCodeModel", "GEM0060",
                            "Expecting CallExpression, but got "
                                    + cRef.getClass().getName() + "\n"
                                    + "Current element: "
                                    + getReferenceString());
                    return null;
                }
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        "CallExpression.toCodeModel", "GEM0061",
                        "ReferenceExpression undefined in: "
                                + ((GAModelElement) sel).getReferenceString()
                                + "\n" + "Current element: "
                                + getReferenceString());
                return null;
            }
        }

        else {
            return this;
        }
    }

    public List<Expression> getBaseTypeArguments() {
        List<Expression> baseTypeArguments = new ArrayList<Expression>();
        for (Expression exp : getArguments()) {
            if (exp.getDataType() instanceof TPointer) {
                Expression expCopy = (Expression) exp.getCopy();
                expCopy.setDataType(exp.getDataType().getPrimitiveType());
                baseTypeArguments.add(expCopy);
            } else {
                baseTypeArguments.add(exp);
            }
        }
        return baseTypeArguments;
    }
    
    public String printElement(int precLevel) {
        if (function != null) {
            if (function instanceof Function_CM) {
                Function_CM cmFun = (Function_CM) function;
                Module callModule = getModule();
                // Object reference to the CodeModel function is given
                if (dependency != null) {
                    // Dependency is given
                    if (callModule != null) {
                        callModule.addDependency(dependency);
                    }
                } else {
                    // Dependency is not given. If the called function is in
                    // another module, then create the required dependency.
                    Module funModule = cmFun.getModule();
                    if (funModule != callModule) {
                        callModule.addDependency(new CodeModelDependency(funModule));
                    }
                }
                return cmFun.instanceToString(arguments, this);
            } else {
                EventHandler.handle(EventLevel.ERROR, "CallExpression.print",
                        "GEM0058", "Expecting a CodeModel function. "
                        + "\nElement: " + getReferenceString());
                return "WRONG TYPE OF FUNCTION in CallExpression";
            }
        } else {
            // Reference to a function is given by name
            if (name != null) {
                if (dependency != null) {
                    if (getModule() != null) {
                        getModule().addDependency(dependency);
                    }
                } else {
                    printNoRequiredFieldError("\"dependency\" must be given, "
                            + "when the function is referred by name and "
                            + "there is no default dependency configured for this function. ");
                    return "DEPENDENCY UNDEFINED in CallExpression " + name
                            + "(" + Function_CM.compileArgumentList(arguments)
                            + ")";
                }
                return name + "(" + Function_CM.compileArgumentList(arguments)
                        + ")";
            } else {
                printNoRequiredFieldError("name");
            }
            return "FUNCTION UNDEFINED in CallExpression";
        }
    }

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		for (Expression argument: arguments) {
			result.addAll(argument.getExpendableAtoms());
		}
		return result;
	}
    
    
}