/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/StructureContent.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-09-13 10:43:54 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.ExpressionUtilities;
import geneauto.utils.ListUtil;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This class represents custom type (content) that is a structure containing a
 * list of members.
 * 
 * TODO (AnTo) There is still some unification and refinement to do. Structures exist
 * both in system and code model, but the content is of the code model. Ideally,
 * one set of classes for structures and structure members should be sufficient.
 */
public class StructureContent extends CustomTypeContent {

    protected List<StructureMember> structMembers = new ArrayList<StructureMember>();

    public StructureContent() {
        super();
    }

    public StructureContent(List<StructureMember> members) {
        this();
        setStructMembers(members);
    }

    public List<StructureMember> getStructMembers() {
        return structMembers;
    }
    
    public List<StructureMember> getStructMembersCopy() {
        
        List<StructureMember> copys = new ArrayList<StructureMember>();
        for(StructureMember elem :  structMembers){
            StructureMember copy = (StructureMember)elem.getCopy();
            copy.setParent(elem.getParent());
            copys.add(copy);
        }
        
        return copys;
    }
    
    

    public void setStructMembers(List<StructureMember> members) {
        this.structMembers.clear();
        for (StructureMember sm : members) {
            addStructureMember(sm);
        }
    }

    public void addStructMembers(List<StructureMember> members) {
        for (StructureMember sm : members) {
            addStructureMember(sm);
        }
    }

    /**
     * Adds structMember to structMembers list and sets its 
     * parent to current element
     * 
     * Assumes that each structure member has unique name. Raiser error 
     * In case the name is not unique 
     * 
     * @param structMember
     */
    public void addStructureMember(StructureMember structMember) {
        addStructureMember(structMember, false);
    }

    /**
     * Adds structMember to structMembers list and sets its 
     * parent to current element
     * 
     * Assumes that each structure member has unique name. Raiser error 
     * In case the name is not unique 
     * 
     * @param structMember -- the member to add
     * @param mangle -- if true, non/unique structure member names are 
     * 					automatically mangled to have unique name
     */
    public void addStructureMember(StructureMember structMember, boolean mangle) {
    	if (mangle) {
    		// make sure the mane is unique
    		mangleName(structMember);
    	} else {
    		// check if the name is unique and raise error if it is not
    		if (!isUniqueMemberName(structMember.getName())) {
                EventHandler.handle(EventLevel.ERROR, 
                        "addStructureMember", 
                        "", 
                        "Non-unique member: \"" 
                        + structMember.getName() + "\"!", 
                        "");
                return;
    		}
    		
    	}
    	
        structMember.setParent(this);
        this.structMembers.add(structMember);
    }

    /**
     * checks if given name is unique member name
     * two mangled names are assumed to be equal (TODO: reconsider this 
     * -- it may make sense to handle mangled names as unique ToNa)  
     *  
     * @param memberName
     * @return
     */
    private boolean isUniqueMemberName(String memberName) {
    	for (StructureMember m : structMembers) {
    		if (m.equalBeforeMangling(memberName)) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    private void mangleName(StructureMember structMember) {
    	// we need to memorise the first memeber -- if it is the only one it 
    	// is un-mangled and its name should be changed too
    	StructureMember firstMember = null;
    	int equalCnt = 0;
    	for (StructureMember m : structMembers) {
    		if (m.equalBeforeMangling(structMember.getName())) {
    			// memorise the first member as this is likely not mangled yet
    			if (equalCnt == 0) {
    				firstMember = m;
    			}
    			// advance the counter
    			equalCnt++;
    		}
    	}
    	
    	// if more than one memeber with similar name
    	if (equalCnt > 0) {
    		// mangle the name of the new member
    		structMember.mangleName(equalCnt+1);
    		// when it was the first mangled name, mangle also the name 
    		// of already existing element
        	if (equalCnt == 1) {
        		firstMember.mangleName(1);
        	}
    	}
    }
    
    
    /**
     * @param name
     * @return a member with specified name
     */
    public StructureMember getMemberByName(String p_name) {
        String memberName;
        for (StructureMember s : getStructMembers()) {
            memberName = s.getName();
            /*
             * p_name is given without points 
             * and name of the member contains points ->
             * take substring after last point
             * 
             * TODO remove this check after memberNames are
             * saved without points
             * 
             */
            if (!(p_name.contains("."))
                    && memberName.contains(".")) {
                memberName = memberName.substring(
                        memberName.lastIndexOf(".") + 1);
            }
            if (memberName.equals(p_name)) {
                return s;
            }
        }
        return null;
    }
    
    /**
     * @return 
     * 		null           - when no members or all members have null initial value
     * 		ListExpression - when all members have non-null initial values
     * 		error 		   - when some members have non-null and some have null initial values
     */
    public Expression getInitialValue() {
    	
    	if (getStructMembers() == null) {
    		return null;
    	}
    	List<Expression> exps = new LinkedList<Expression>();
    	
    	boolean allNull = true, allNotNull = true;
    	
    	for (StructureMember m : getStructMembers()) {
    	    Expression e = m.getDataType().getDefaultValue();
    	    
    	    // create ListExpression according to dimensions 
    	    // in case member's data type is TArray
    	    if (m.getDataType() instanceof TArray) {
    	        ExpressionUtilities expUtils 
    	            = new ExpressionUtilities(m.getDataType().getDefaultValue());
    	        List<Integer> dims = ListUtil.toList(
    	                m.getDataType().getDimensionsLengths());
    	        e = expUtils.toListExpression(dims);
    	    }

    		
    		if (e != null) {
    			allNull = false;
        		exps.add(e.getCopy());
    		} else {
    			allNotNull = false;
        		exps.add(null);
    		}
    	}
    	if (allNull) {
    		return null;
    	} else if (allNotNull) {
    		GeneralListExpression lst = (GeneralListExpression) PrivilegedAccessor
					.getObjectInPackage(GeneralListExpression.class, this);
			/*
			 * Try to determine and set the datatype of the expression.
			 * Otherwise, the datatype is deduced, by the list's structure,
			 * which however has no information about the data type - the custom
			 * type cannot be inferred based on the contents!
			 */
    		GAModelElement parent = getParent();
    		if (parent instanceof CustomType) {
        		TCustom dt =  (TCustom) PrivilegedAccessor
        			.getObjectInPackage(TCustom.class, this);
        		dt.setCustomType((CustomType) parent);
    			lst.setDataType(dt);
    		}    	
			lst.setExpressions(exps);
    		return lst;    		
    	} else
	        EventHandler.handle(EventLevel.ERROR, getClass().getName() + ".getInitialValue()",
	                "",
	                "Structure contains members with null and non-null initial values. "
	                + "\n Element: " + getReferenceString());
	        return null;
    }
    
	@Override
	public boolean equalsTo(CustomTypeContent other) {
		if (!(other instanceof StructureContent)) {
			return false;
		}
		
		if (getStructMembers().size() != other.getStructMembers().size()) {
			return false;
		}
		
		for (StructureMember m : getStructMembers()) {
		    StructureMember member = ((StructureContent) other)
								.getMemberByName(m.getName());
		    if (member == null) {
		        return false;
		    }
			if (!m.equalsTo(member)) {
				return false;
			}
		}
		
		return true;
	}

	/*
	 * returns true, if structure has members, false otherwise
	 */
	public boolean hasMembers() {
		return  !(structMembers.isEmpty());
	}
}