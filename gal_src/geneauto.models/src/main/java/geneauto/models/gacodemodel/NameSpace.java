/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/NameSpace.java,v $
 *  @version	$Revision: 1.47 $
 *	@date		$Date: 2012-02-19 22:55:50 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gadatatypes.TPointer;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Symbol table for code generation. Statements and expressions can refer to
 * this table. For instance a VariableExpression may contain a reference to the
 * specific section of the NameSpace The final code generation phase retrieves
 * the actual piece of code (usually an identifier, but could be also a section
 * of code) that corresponds to some part of the expression.
 * 
 * 
 */
public class NameSpace extends GACodeModelElement {

    public NameSpace() {
        super();
    }

    /**
     * Initialises NameSpace with the elements in nsElements
     */
    public NameSpace(List<NameSpaceElement_CM> nsElements) {
        this();
        addElements(nsElements);
    }

    /**
     * Elements belonging to the NameSpace
     */
    protected List<NameSpaceElement_CM> nsElements = new ArrayList<NameSpaceElement_CM>();

    /**
     * List of implicit ArgumentValues.
     * 
     * If calls to functions with implicit ArgumentValues are made within the
     * NameSpace, then these expressions are substituted for the implicit
     * arguments. Implicit arguments are matched *by name*.
     * 
     * DESIGN RULES:
     *  - Function.nameSpace contains no implicit values. Put them in 
     *    Function.body.nameSpace instead to have function-wide visibility.
     *  - If an implicit value refers to a function argument, then such 
     *    implicit value shall only be placed in Function.body.nameSpace.    
     */
    protected List<Expression> implicitArgumentValues = new ArrayList<Expression>();

	/**
	 * Returns reference to the NSElements collection 
	 * NOTE! This collection should not be used for adding elements! It should be 
	 * done by appropriate accessors (e.g. addElement(..)) instead!
	 * */
    public List<NameSpaceElement_CM> getNsElements() {
        return nsElements;
    }

    /** Add element to collection and update its parent */
    public void addElement(NameSpaceElement_CM nsElement) {
        nsElement.setParent(this);
        this.nsElements.add(nsElement);
    }

    /** Add element to a specified location in the collection and update its parent */
    public void addElement(int index, NameSpaceElement_CM nsElement) {
        nsElement.setParent(this);
        this.nsElements.add(index, nsElement);
    }

    /**
     * Adds an element to the NameSpace, if the same element is not there
     * already
     * 
     * @param element
     */
    public void addElementIfNotExisting(NameSpaceElement_CM element) {
        for (NameSpaceElement_CM existingEl : nsElements) {
            if (existingEl == element) {
                return;
            }
        }
        addElement(element);
    }

    /** Add elements in the collection and update their parent */
    public void addElements(List<NameSpaceElement_CM> p_nsElements) {
        if (p_nsElements != null) {
            for (NameSpaceElement_CM x : p_nsElements) {
                addElement(x);
            }
        }
    }

    /** Add elements to a specified location in the collection and update their parent */
    public void addElements(int index, List<NameSpaceElement_CM> p_nsElements) {
        if (p_nsElements != null) {
			// Reverse the original list, so that the inserted elements end up
			// in the right order
        	Collections.reverse(p_nsElements);        	
            for (NameSpaceElement_CM x : p_nsElements) {
                addElement(index, x);
            }
        }        
    }

    public List<Expression> getImplicitArgumentValues() {
        return implicitArgumentValues;
    }

    /**
     * Returns implicit argument value expression, where the expression name
     * equals to the given parameter
     *  
     * @param name
     * @return
     */
    public Expression getImplicitArgumentValue(String name) {
    	if (name == null) {
    		// TODO: add error handling here
    		return null;
    	}
    	
    	for (Expression e : implicitArgumentValues) {
    		if (name.equals(e.getName())) {
    			return e;
    		}
    	}
    	
    	return null;
    }
    
    /**
     * Returns implicit argument value expression, where the expression name
     * equals to the given parameter
     *  
     * @param name
     * @param recursive - recursively check upper NameSpaces, if entry not found 
     * @return
     */
    public Expression getImplicitArgumentValue(String name, boolean recursive) {
    	Expression val = getImplicitArgumentValue(name);
    	if (val != null || !recursive) { 
    		return val;
    	} else {
    		NameSpace cns = getContainingNameSpace();
    		if (cns != null) {
    			return cns.getImplicitArgumentValue(name, true);
    		} else {
    			return null;
    		}
    	}
    }
    
    public void setImplicitArgumentValues(
            List<Expression> implicitArgumentValues) {
        this.implicitArgumentValues.clear();
        if (implicitArgumentValues != null) {
            for (Expression e : implicitArgumentValues) {
                addImplicitArgumentValue(e);
            }
        }
    }

    public void addImplicitArgumentValue(Expression implicitArgumentValue) {
        implicitArgumentValue.setParent(this);
        this.implicitArgumentValues.add(implicitArgumentValue);
    }

    public void removeImplicitArgumentValue(String argName) {
    	Expression ivExp = null;
		for (Expression exp : implicitArgumentValues) {
    		if (argName.equals(exp.getName())) {
    			ivExp = exp;
    			break;
        	}
    	}
    	if (ivExp != null) {
    		implicitArgumentValues.remove(ivExp);
    	}
    }
    
    @Override
	public GAModelElement getCopy() {
		// getCopy() normally not applicable for NameSpace instances. If
		// required, then specific handling must be implemented (deep copy +
		// reassigning all (only some??) references to member elements.) AnTo
		// 100820
    	EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getSimpleName(), "", 
    			"Called getCopy(). This is probably an error. Copying entire NameSpaces " +
    			"shouldn't normally be done. If required, then it needs extra treatment " +
    			"that has not been implemented!");
		return null;
	}

    /**
     * deletes variable which dataType is TCustom with customType = type
     * 
     * @param type
     *            customType of the TCustom (dataType of the variable)
     */
    public void deleteCustomVar(CustomType_CM type) {
        Variable_CM var = null;
        for (NameSpaceElement_CM elem : getNsElements()) {
            if (elem instanceof Variable_CM) {
                Variable_CM tempVar = (Variable_CM) elem;
                if (tempVar.getDataType() instanceof TCustom) {
                    TCustom custom = (TCustom) tempVar.getDataType();
                    if (custom.getCustomType() == type) {
                        var = tempVar;
                        break;
                    }
                }
            }
        }
        if (var != null) {
            var.removeFromNameSpace();
        }
    }

    /**
     * @param name
     * @return true, when element with given name exists
     */
    public boolean containsElement(String name) {

        for (NameSpaceElement_CM n : this.getNsElements()) {
            if (n.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param name
     * @return element with given name, if exists
     */
    public NameSpaceElement_CM getElement(String name) {

        for (NameSpaceElement_CM n : this.getNsElements()) {
            if (n.getName().equals(name)) {
                return n;
            }
        }
        return null;
    }
    
    /**
	 * Implement specific behaviour regarding FunctionArgument_CM - Remove also the
	 * reference from the containing Function's arguments list	 * 
     * @param element Element to be removed
     */
    public void removeElement(FunctionArgument_CM element) {
        // Note: The exists check is needed to avoid mutual infinite recursion
    	// with the containing function.
    	if (nsElements.contains(element)) {
    		nsElements.remove(element);
    		element.getContainingFunction().removeArgument(element);
    		element.setParent(null);
    	}
    }

    /**
     * Removes element from nsElements list
     * Resets the parent of the removed element
     * 
     * @param element Element to be removed
     */
    public void removeElement(NameSpaceElement_CM element) {
        nsElements.remove(element);
        element.setParent(null);
    }

    /**
     * Retrieves the Variable_CM that instantiates a CustomType
     * 
     * @param cstType ,
     *            the customType
     * @return the Variable_CM
     */
    public Variable_CM getVariableByType(CustomType cstType) {
        Variable_CM result = null;

        CustomType correspondingType = null;

        if (cstType instanceof CustomType_SM) {
            correspondingType = (CustomType_CM) ((CustomType_SM) cstType)
                    .getCodeModelElement();
        } else if (cstType instanceof CustomType_CM) {
            correspondingType = (CustomType_SM) ((CustomType_CM) cstType)
                    .getSourceElement();
        }

        // Browses the code model, for each element...
        for (GAModelElement element : this.getNsElements()) {// (new
            // Variable_CM()).getClass().getCanonicalName(),
            // gaCodeModel.getAllElements().get(0)))
            // {
            // If is a Variable_CM instance
            if (element instanceof Variable_CM) {
            	if (isCstTypeVar(cstType, correspondingType, 
            			(Variable_CM) element)) {
            		result = (Variable_CM) element;
            	}
            }
        }
        
        /*
         * Nothing was found -> check parent's content
         */
        if (result == null) {
        	GAModelElement parent = getParent();
        	if (parent != null) {
        		
        		/*
        		 * parent is function -> check its arguments
        		 */
        		if (parent instanceof Function_CM) {
        			Function_CM fun = (Function_CM) parent;
        			for (FunctionArgument_CM arg: fun.getArguments()) {
                		if (isCstTypeVar(cstType, correspondingType, arg)) {
                			result = arg;
                			break;
                		}
                	}
        		}
        		
        		/*
        		 * parent's parent has name space -> check it
        		 */
        		if (result == null) {
        			NameSpace grandParentNS = null;
        			if (parent.getParent() != null
        					&& parent.getParent() instanceof HasNameSpace) {
        				grandParentNS = ((HasNameSpace) parent.getParent()).getNameSpace();
                		
        			} else if (parent.getParent() != null
        					&& parent.getParent() instanceof NameSpace) {
        				grandParentNS = (NameSpace) parent.getParent();
        			}
        			if (grandParentNS != null) {
        				result = grandParentNS.getVariableByType(cstType);
        			}
        		}
        	}
        }
        return result;
    }

    /**
     * Checks if var's data type is structure cstType
     * or correspondingType
     * 
     * @param cstType first custom 
     * type to be checked
     * @param anotherCstType second custom type to 
     * be checked
     * @param var Variable to be checked
     * @return true in case result of check is positive, 
     * false otherwise
     */
	private boolean isCstTypeVar(CustomType cstType,
			CustomType anotherCstType,
			Variable_CM var) {
		boolean result = false;
		
		// Retrieves the dataType
		GADataType tmpType = var.getDataType();
		
		// If it is a TCustom dataType
		if (tmpType instanceof TCustom) {
		    TCustom type = (TCustom) tmpType;
		    // Checks if the TCustom customType is the same as the
		    // argument
		    if ((type.getCustomType().equalsTo(cstType))
		            || (type.getCustomType().equalsTo(anotherCstType))) {
		        // If true result is positive
		        result = true;
		    }
		}
		
		// If it is a TPointer dataType
		else if (tmpType instanceof TPointer) {
		    TPointer type = (TPointer) tmpType;
		    // Checks if the TPointer baseType is TCustom
		    if (type.getBaseType() instanceof TCustom) {
		        if ((((TCustom) type.getBaseType()).getCustomType().equalsTo(cstType))
		                || (((TCustom) type.getBaseType())
		                        .getCustomType().equalsTo(anotherCstType))) {
		            // If true result is positive
		            result = true;
		        }
		    }
		}
		return result;
	}

	/** Clear the namespace */
	public void clear() {
		if (nsElements != null) {
			nsElements.clear();
		}		
	}

}