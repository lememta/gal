/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/BinaryExpression.java,v $
 *  @version	$Revision: 1.96 $
 *	@date		$Date: 2011-09-20 13:10:29 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */ 
package geneauto.models.gacodemodel.expression;

import static geneauto.utils.assertions.Assert.assertNotNull;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gadatatypes.GADataType;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class BinaryExpression extends Expression {

    protected Expression leftArgument;
    protected Expression rightArgument;
    protected BinaryOperator operator;

    public BinaryExpression() {
        super();
    }

    public BinaryExpression(Expression leftArgument, Expression rightArgument,
            BinaryOperator binaryOperator) {
        super();

        if (leftArgument != null) {
            leftArgument.setParent(this);
        }
        this.leftArgument = leftArgument;

        if (rightArgument != null) {
            rightArgument.setParent(this);
        }
        this.rightArgument = rightArgument;

        this.operator = binaryOperator;
    }

    public BinaryExpression(Expression leftArgument, Expression rightArgument,
            BinaryOperator binaryOperator, GADataType type) {
        super();
        if (leftArgument != null) {
            leftArgument.setParent(this);
        }
        this.leftArgument = leftArgument;

        if (rightArgument != null) {
            this.rightArgument = rightArgument;
        }
        rightArgument.setParent(this);

        this.operator = binaryOperator;
        setDataType(type);
    }

    public Expression getLeftArgument() {
        return leftArgument;
    }

    public void setLeftArgument(Expression leftArgument) {
        if (leftArgument != null) {
            leftArgument.setParent(this);
        }
        this.leftArgument = leftArgument;
    }

    public Expression getRightArgument() {
        return rightArgument;
    }

    public void setRightArgument(Expression rightArgument) {
        if (rightArgument != null) {
            rightArgument.setParent(this);
        }
        this.rightArgument = rightArgument;
    }

    public BinaryOperator getOperator() {
        return operator;
    }

    public void setOperator(BinaryOperator binaryOperator) {
        this.operator = binaryOperator;
    }

    public boolean isConst() {
        return ((leftArgument.isConst()) && rightArgument.isConst());
    }

    public boolean isConst(double l_value) {
        l_value = 0;
        return ((leftArgument.isConst()) && rightArgument.isConst());
    }

    public Expression eval(boolean printError, boolean optimizableOnly) {
        
        if (getValue() != null) {
            return getValue();
        }
        
        if (leftArgument == null) {
            if (printError) {
                EventHandler.handle(EventLevel.ERROR, "BinaryExpression.eval()", "",
                        "leftArgument is null. Element: "
                                + getReferenceString());
            }
            return null;            
        }
        
        if (rightArgument == null) {
            if (printError) {
                EventHandler.handle(EventLevel.ERROR, "BinaryExpression.eval()", "",
                        "rightArgument is null. Element: "
                                + getReferenceString());
            }
            return null;            
        }
        
        double d;
        NumericExpression leftValue = leftArgument.evalReal(printError, optimizableOnly);
        NumericExpression rightValue = rightArgument.evalReal(printError, optimizableOnly);
        
        if (leftValue == null) {
            // Assume the error was raised already, if required.
            return null;            
        }
        
        if (rightValue == null) {
            // Assume the error was raised already, if required.
            return null;            
        }       
        
        switch (operator) {
        case ADD_OPERATOR:
            d = leftValue.getRealValue() + rightValue.getRealValue();
            break;
        case DIV_OPERATOR:
            d = leftValue.getRealValue() / rightValue.getRealValue();
            break;
        case MUL_OPERATOR:
            d = leftValue.getRealValue() * rightValue.getRealValue();
            break;
        case SUB_OPERATOR:
            d = leftValue.getRealValue() - rightValue.getRealValue();
            break;
        case LT_OPERATOR:
            d = leftValue.getRealValue() < rightValue.getRealValue() ? 1 : 0;
            break;
        case LE_OPERATOR:
            d = leftValue.getRealValue() <= rightValue.getRealValue() ? 1 : 0;
            break;
        case GT_OPERATOR:
            d = leftValue.getRealValue() > rightValue.getRealValue() ? 1 : 0;
            break;
        case GE_OPERATOR:
            d = leftValue.getRealValue() >= rightValue.getRealValue() ? 1 : 0;
            break;
        case EQ_OPERATOR:
            d = leftValue.getRealValue() == rightValue.getRealValue() ? 1 : 0;
            break;
        case NE_OPERATOR:
            d = leftValue.getRealValue() != rightValue.getRealValue() ? 1 : 0;
            break;
            
        // TODO Add other operators
            
        default:
            // TODO refine error
            if (printError) {
                printUnsupportedOperatorException(this, operator, "eval");
            }
            setValue(null);

            return getValue();
        }
        if ((int) d == d) {
            setValue(new IntegerExpression((int) d, getDataType()));
        } else {
            setValue(new DoubleExpression(d));
        }
        return getValue();
    }

    @Override
    public void resetDataType() {
    	if (leftArgument != null) {
    		leftArgument.resetDataType();
    	}
    	if (rightArgument != null) {
            rightArgument.resetDataType();    		
    	}
    	
        if (operator != null) {
            // NOTE: The strictness flag is set to "true" here. In
            // principle, "false" is also possible. However, usually it is
            // an error somewhere else, if the typing does not succeed here.
        	attachDataType(operator.getDataType(leftArgument, rightArgument, true));
        }
    }

    @Override
    public Expression toCodeModel() {
        assertNotNull(leftArgument, "BinaryExpression.leftArgument. Element: "
                + getReferenceString());
        assertNotNull(rightArgument,
                "BinaryExpression.rightArgument. Element: "
                        + getReferenceString());
        
        super.toCodeModel();
        
        leftArgument.toCodeModel();
        rightArgument.toCodeModel();
        
        return this;
    }
   
   public String printElement(int outerPrecedence) {
       String opPart = "";
       String leftPart = "";
       String rightPart = "";
       int innerPrecedence = 0;

       // Operator part
       if (operator != null) {
           innerPrecedence = operator.getPrecedence();
           opPart = operator.printElement();
       } else {
           printNoRequiredFieldError("operator");
       }

       // Left part
       if (leftArgument != null) {
           leftPart = leftArgument.printElement(innerPrecedence);
       } else {
           EventHandler.handle(EventLevel.ERROR, "CPrinter.BinaryExpression",
                   "GEM0055", "Missing left operand: " + getReferenceString());
       }

       // Right part
       if (rightArgument != null) {
           
           // operator is "*" or "%" -> parenthesize anyway
           if ((operator == BinaryOperator.DIV_OPERATOR) 
                   || (operator == BinaryOperator.MOD_OPERATOR)) {
               rightPart = rightArgument.printElement(14);
           } else {
               rightPart = rightArgument.printElement(innerPrecedence);
           }
       } else {
           EventHandler.handle(EventLevel.ERROR, "CPrinter.BinaryExpression",
                   "GEM0056", "Missing right operand: " + getReferenceString(), "");
       }

       // Combine expression
       String code = "";
       if (operator != BinaryOperator.POWER_OPERATOR) {
           code = leftPart + " " + opPart + " " + rightPart;
       } else {
           code = leftPart + " " + "^" + " " + rightPart;
       }

       // Set parenthesis, if required
       code = Expression.parenthesize(code, outerPrecedence, innerPrecedence);
       return code;
   }

    @Override
    public String toString() {
        String opPart = "";
        if (getOperator() != null) {
            opPart = " op=\"" + getOperator().printElement() + "\"";
        }
        return super.toString() + opPart;
    }

	@Override
	public List<Expression> getExpendableAtoms() {
		List<Expression> result = new ArrayList<Expression>();
		if (leftArgument != null) {
			result.addAll(leftArgument.getExpendableAtoms());
		}
		if (rightArgument != null) {
			result.addAll(rightArgument.getExpendableAtoms());
		}
		return result;
	}  
    
    
}