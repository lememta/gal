/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/NamedConstant.java,v $
 *  @version	$Revision: 1.24 $
 *	@date		$Date: 2011-12-08 13:11:29 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.expression.Expression;

/**
 * A symbolic named constant for which no variable expected to be created. It 
 * is generally not the same as Variable with isConstant="true". 
 * 
 * For example, in C it might be represented as a define macro. E.g.
 *   #define PI = 3.14
 * 
 * It is assumed that in this class initalValue has always value -- this is 
 * the value of constant. The isConstant flag is fixed to value "true".
 */
public class NamedConstant extends Variable_CM {

    public NamedConstant() {
        super();
        isConst = true;
    }

    @Override
    public boolean isConst() {
        return true;
    }

    @Override
    public void setConst(boolean isConst) {
        // Ignore
    }

    /**
     * 
     * @return value of the constant. Constant value is always equal to its
     *         initial value
     */
    public Expression getValue() {
        return initialValue;
    }

}