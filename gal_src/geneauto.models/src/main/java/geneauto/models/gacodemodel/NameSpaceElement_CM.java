/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/NameSpaceElement_CM.java,v $
 *  @version	$Revision: 1.14 $
 *	@date		$Date: 2011-07-12 13:27:46 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.genericmodel.GAModelElement;

/**
 * Abstract entity representing different code components that can belong to a
 * namespace.
 */
public abstract class NameSpaceElement_CM extends GACodeModelElement {
			
    /**
     * Scope of the element
     * 
     * @see DefinitionScope
     */
    protected DefinitionScope scope = DefinitionScope.LOCAL;

    public NameSpaceElement_CM() {
        super();
    }

    /**
     * NameSpaceElements have global scope in code model
     * 
     * (non-Javadoc)
     * 
     * @see geneauto.models.genericmodel.GAModelElement#isReferable()
     */
    public boolean isReferable() {
        return true;
    }

    /**
     * @return literal string that represents elements's definition
     */
    public String getDefinition() {
        printUnsupportedMethodError(this, "getDefinition");
        return null;
    }

    /**
     * @return literal string that represents elements's declaration
     */
    public String getDeclaration() {
        printUnsupportedMethodError(this, "getDeclaration");
        return null;
    }

    /**
     * 
     * @return the NameSpace, where this element belongs.
     */
    public NameSpace getParentNameSpace() {

        GAModelElement parent = this.getParent();

        while (parent != null) {
            if (parent instanceof NameSpace) {
                return (NameSpace)parent;
            }
            else{
                parent = parent.getParent();
            }
        }
        
        return null;
    }

    /**
     * 
     * @return the StructureMemberContent for Structure Member, null - otherwise
     */
    public StructureContent getStructureMembersNameSpace() {
        if (this.getParent() instanceof StructureContent) {
            return (StructureContent) this.getParent();
        } else {
            if (getParent() instanceof StructureMember) {
                return ((StructureMember) getParent())
                        .getStructureMembersNameSpace();
            } else {
                return null;
            }
        }
    }

    /**
     * Removes given NameSpace element from model
     * By default behaves exactly the same as removeMe, a separate method
     * is created to allow specific checks in subclasses 
     * (e.g. removal of StrcutureMember is disallowed from imported data types) 
     * 
     */
    public boolean removeFromNameSpace() {
    	/* remove element from the model */
    	removeMe();
    	
    	return true;
    }

    public DefinitionScope getScope() {
        return scope;
    }

    public void setScope(DefinitionScope scope) {
        this.scope = scope;
    }
    
    /**
     * If element is imported, its name shall never be changed regardless of
     * the state of the isFixedName flag
     */
    @Override
    public boolean hasFixedName() {
        if (getScope() == DefinitionScope.IMPORTED_DECLARED) {
            return true;
        } else if (getScope() == DefinitionScope.IMPORTED) {
            return true;
        } else {
            return super.hasFixedName();
        }
    }
   
}