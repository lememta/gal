/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/SequentialFunctionBody.java,v $
 *  @version	$Revision: 1.38 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Function;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.map.ConstantMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Function's body that is formed of statements.
 */
public class SequentialFunctionBody extends FunctionBody implements
        SequentialComposition, HasNameSpace {

    /**
     * name space for local variables
     */
    protected NameSpace nameSpace;

    /**
     * statements forming the function body
     */
    protected List<Statement> statements = new ArrayList<Statement>();

    /**
     * Default constructor
     */
    public SequentialFunctionBody() {
        super();
    }

    /**
     * Constructor with specified fields
     * 
     * @param nameSpace
     * @param statements
     */
    public SequentialFunctionBody(NameSpace nameSpace,
            List<Statement> statements) {
        if (nameSpace != null) {
            nameSpace.setParent(this);
        }
        this.nameSpace = nameSpace;
        if (statements != null) {
            for (Statement x : statements) {
                if (x != null) {
                    x.setParent(this);
                }
            }
        }
        this.statements = statements;
    }

    /**
     * Constructor with specified fields
     * 
     * @param statements
     */
    public SequentialFunctionBody(List<Statement> statements) {
        this(null, statements);
    }

    /**
     * Method getStatements.
     * 
     * @return the attribute "statements".
     */
    public List<Statement> getStatements() {
        return statements;
    }

    /**
     * Method getNameSpace. Always returns a namespace -- of one did not exist
     * before creates new namespace
     * 
     * @return the attribute "nameSpace".
     */
    public NameSpace getNameSpace() {
        if (nameSpace == null) {
            setNameSpace(new NameSpace());
        }
        return nameSpace;
    }

    /**
     * Method setNameSpace.
     * 
     * @param nameSpace
     *            the "nameSpace" to set.
     */
    public void setNameSpace(NameSpace nameSpace) {
        nameSpace.setParent(this);
        this.nameSpace = nameSpace;
    }

    /**
     * Initialises statement list with a signle statement
     * 
     * @param statement
     */
    public void setStatement(Statement statement) {
        this.statements = new ArrayList<Statement>();
        if (statement != null) {
            statement.setParent(this);
            this.statements.add(statement);
        }
    }

    /**
     * Method setStatements.
     * 
     * @param statements
     *            the "statements" to set.
     */
    public void setStatements(List<Statement> statements) {
        if (statements != null) {
            for (Statement x : statements) {
                if (x != null) {
                    x.setParent(this);
                }
            }
        }
        this.statements = statements;
    }

    /**
     * Method addStatements.
     * 
     * @param statements
     *            the "statements" to add.
     */
    public void addStatements(List<Statement> p_statements) {
        if (p_statements != null) {
            for (Statement s : p_statements) {
                addStatement(s);
            }
        }
    }

    /**
     * Replaces statement replSt with the list of statements
     * in stList. Preserves statement order
     * 
     * @param replSt
     * @param stList
     */
    public void replaceStatementWithList(Statement replSt, List<Statement> stList) {
		int idx = statements.indexOf(replSt);
		for (Statement st : stList) {
			st.setParent(this);
		}
		
		if (idx > -1) {
			statements.addAll(idx, stList);
			statements.remove(idx);
			replSt.setParent(null);
    	} else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		"SequentialFunctionBody.replaceStatementWithList",
                    "GEM0053", 
                    "Unable to find statement\n "
                    + replSt.getReferenceString()
                    + "\n in " + this.getReferenceString(), 
                    "");
    	}	
    }    
    
    /**
     * Method addStatements.
     * 
     * @param statements
     *            the "statements" to add.
     */
    public void addStatement(Statement statement) {
        statement.setParent(this);
        this.statements.add(statement);
    }

    @Override
    public void setStatement(int index, Statement st) {
        st.setParent(this);
        this.getStatements().add(index, st);
    }

    /**
     * Adds statements list to the List of Statements of argument of type
     * List<Statemetn> at the position index
     * 
     * @param index
     *              position of statement being put
     * @param stmts
     *              list of statements to be added to the List
     */
    public void setStatements(int index, List<Statement> stmts) {
        for (Statement st: stmts) {
            st.setParent(this);
        }
        this.getStatements().addAll(index, stmts);
    }

    @Override
    public List<Statement> getAllSubStatements() {
        List<Statement> stList = new ArrayList<Statement>();
        for (Statement st : getStatements()) {
            stList.add(st);
            if (!st.getAllSubStatements().isEmpty()) {
                stList.addAll(st.getAllSubStatements());
            }
        }
        return stList;
    }

    /**
     * @return an output variable, if existing.
     */
    public Variable_CM getOutputVariable() {
        if (getNameSpace() == null) {
            return null;
        }
        else {
            for (NameSpaceElement_CM el : getNameSpace().getNsElements()) {
                if (el.getScope() == DefinitionScope.FUNCTION_OUTPUT) {
                    return (Variable_CM) el;
                }
            }
            return null;
        }
    }
    
    public boolean isConditional() {
        return false;
    }

    @Override
    public SequentialComposition getStatementSpace() {
        return null;
    }

    /**
     * goes through the function body and resolves references to function
     * arguments
     */
    @Override
    public void resolveReferencesToArguments() {
    	Function f = getContainingFunction();
    	
    	// resolving references makes sense only in case of Funciton_CM
    	if (f instanceof Function_CM) {
            // check if any of the statements has reference to function arguments
            ConstantMap<String, GAModelElement> argNames 
            								= argumentsToMap((Function_CM) f);
            
            for (Statement stmt : getStatements()) {
            	stmt.resolveReferences(argNames, getModel());
            }    		
    	}
    	
    }
}