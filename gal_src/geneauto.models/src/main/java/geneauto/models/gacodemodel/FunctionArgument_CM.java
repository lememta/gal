/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/FunctionArgument_CM.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-11-29 15:52:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.gaenumtypes.Direction;


/**
 * Abstract entity representing a function argument. Note: Each argument
 * requires also an entry in the CodeComponent section of the NameSpace. Its
 * scope should be Imported, so that it won't get unnecessarily declared.
 * Constructors RequiredArg Bool CCPtr - Argument variable and a flag stating,
 * whether it is optimisable or not. RedundantArg - A redundant argument that
 * shall not be printed neither in function definition nor a function call.
 * However, (abstract) function calls must still have all arguments. The
 * redundant ones simply won't be printed.
 */
public class FunctionArgument_CM extends Variable_CM {

    /**
     * Typically the arguments defined explicitly by the user are not
     * optimisable. Automatically generated function arguments can become
     * redundant, although this is not known, when the function signature is
     * generated.
     */
    protected boolean isRedundant;

    /**
     * Implicit arguments are not given in function calls. Rather, the visible
     * NameSpaces are searched for values that are to be substituted as
     * corresponding arguments. This avoids carrying around unnessecary arguments.
     * @see NameSpace
     */
    protected boolean isImplicit;
    
    /**
     * Direction of the argument
     * Possible values are IN, OUT or IN_OUT
     */
    protected Direction direction;

    public FunctionArgument_CM() {
        super();
    }

    /**
     * 
     * @param isOptimizable
     * @param isRedundant
     */
    public FunctionArgument_CM(boolean isOptimizable, boolean isRedundant) {
        this();
        this.isOptimizable = isOptimizable;
        this.isRedundant = isRedundant;
    }

    public boolean isRedundant() {
        return isRedundant;
    }

    public void setRedundant(boolean isRedundant) {
        this.isRedundant = isRedundant;
    }

    public boolean isImplicit() {
        return isImplicit;
    }

    public void setImplicit(boolean isImplicit) {
        this.isImplicit = isImplicit;
    }

    public Direction getDirection() {
    	return getDirection(false);
    }
    
    public Direction getDirection(boolean useDefaultValue) {
    	if (direction == null) {
    		if (useDefaultValue) {
    			return Direction.IN;
    		}
    	} 
    	return direction;
    	
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public FunctionArgument_CM getCopy() {
        return (FunctionArgument_CM) super.getCopy();
    }

	/* (non-Javadoc)
	 * @see geneauto.models.gacodemodel.NameSpaceElement_CM#removeFromNameSpace()
	 * 
	 * FunctionArgument does not belong to namespace and therefore can not 
	 * be removed using this method. 
	 * If it is required to remove it, use removeMe() 
	 * 
	 */
	@Override
	public boolean removeFromNameSpace() {
		return false;
	}

	/**
	 * Converts (destructively) the current object to a Variable_CM object.
	 * Field values are moved rather than copied.
	 * NB! Update this method, if fields are added or removed from any superclass.
	 * 
	 * @return
	 */
	public Variable_CM toVariable_CM() {
		Variable_CM var = new Variable_CM();
		
		// Move GAModelElement attributes
		moveCommonAttributes(var);
		
		// Move GACodeModelElement specific attributes
		var.setModule(module);
		var.setSourceElement(sourceElement);
		
		// Move NameSpaceElement_CM specific attributes
		var.setScope(scope);
		
		// Move Variable_CM specific attributes
		var.setAutoInit(autoInit);
		var.setDataType(dataType);
		var.setInitialValue(initialValue);
		var.setConst(isConst);
		var.setOptimizable(isOptimizable);
		var.setStatic(isStatic);
		var.setVolatile(isVolatile);
		
		return var;
	}

    /**
	 * Implement specific behaviour regarding FunctionArgument_CM - Remove also the
	 * reference from the containing Function's arguments list
	 */
	@Override
    public void removeMe(){
		Function_CM f = getContainingFunction();
		if (f != null) {
			// This removes the element both from nameSpace and the arguments list
			f.removeArgument(this);
		}    	
    }

}