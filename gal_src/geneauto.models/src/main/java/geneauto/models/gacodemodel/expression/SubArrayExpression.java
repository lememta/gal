package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.utilities.CodeModeUtilities;

/**
 * Sub expression of array type LValue expression
 * 
 * @author Andrei Rybinsky
 *
 */
public abstract class SubArrayExpression extends Expression implements LValueExpression{
	
	/**
	 * Expression which sub expression current expression is
	 */
	protected LValueExpression expression;

	public SubArrayExpression() {
		super();
	}

	@Override
	public Variable getVariable() {
		return expression.getVariable();
	}

	public LValueExpression getExpression() {
		return expression;
	}

	public void setExpression(LValueExpression expression) {
		if (expression instanceof SubArrayExpression) {
			EventHandler.handle(EventLevel.ERROR, 
					"SubArrayExpression.setExpression()", 
					"", 
					"SubArrayExpression is not allowed to have " 
					+ "expression of SubArrayExpression! \n" + 
					"SubExpression: " + this.getReferenceString());
			return;
		}
		if (expression instanceof Expression) {
			if (expression.getParent() != null) {
				expression = (LValueExpression) ((Expression) 
						expression).getCopy();
			}
			((Expression) expression).setParent(this);
		}
		this.expression = expression;
	}

	@Override
	public boolean isRowMtrx(Expression mtrxToCmpr, boolean isLeftExpr) {
		return CodeModeUtilities.isRowMtrx(this, mtrxToCmpr, 
                isLeftExpr);
	}

	@Override
	public abstract void resetDataType();
	
	
}
