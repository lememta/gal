package geneauto.models.gacodemodel;

/**
 * 
 * Interface for all elements that has 
 * attribute of type NameSpace
 * 
 * @author Andrey Rybinsky
 *
 */
public interface HasNameSpace {
    public NameSpace getNameSpace();
    
    public void setNameSpace(NameSpace ns);
}
