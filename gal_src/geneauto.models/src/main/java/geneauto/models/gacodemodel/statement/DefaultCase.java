/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/DefaultCase.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-11-28 22:44:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.SequentialComposition;

import java.util.ArrayList;
import java.util.List;

/**
 * A statement to be executed if any of the WhenCaseStatements of CaseStatement
 * was not true
 */
public class DefaultCase extends Statement implements SequentialComposition{

	protected List<Statement> statements = new ArrayList<Statement>();

	public DefaultCase() {
		super();
	}

	public DefaultCase(List<Statement> statements) {
		super();
		for (Statement st : statements) {
			if (st != null) {
				st.setParent(this);
			}
		}
		this.statements = statements;
	}

	public List<Statement> getStatements() {
		return statements;
	}

	public void setStatements(List<Statement> statements) {
		for (Statement st : statements) {
			if (st != null) {
				st.setParent(this);
			}
		}
		this.statements = statements;
	}

	@Override
	public List<Statement> getAllSubStatements() {
		List<Statement> stList = new ArrayList<Statement>();
		for (Statement st: getStatements()){
			stList.add(st);
			stList.addAll(st.getAllSubStatements());
		}
		return stList;
	}

	@Override
	public void setStatement(Statement st) {
		statements.add(st);
	}

	@Override
	public void setStatement(int index, Statement st) {
		statements.add(index, st);
	}

	@Override
	public NameSpace getNameSpace() {
		return null;
	}
	
    @Override
    public boolean containsStmts() {
        return true;
    }

    @Override
    public int setSequenceNumber(int number) {
        super.setSequenceNumber(number);
        for (Statement st: statements) {
            st.setSequenceNumber(++number);
        }
        return number;
    }
	
	
}