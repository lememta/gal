/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/operator/OperatorAppearance.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.operator;

/**
 * Enumeration type to determine how the opeartor should be 
 * processed in expression or statement. Each operator type will determine 
 * the processing method based on operator argument(s)  
 *
 */
public enum OperatorAppearance {
	NOTACCEPTED,			// the given combination of arguments is not accepted 
							// for this operator
	SCALAR,					// the operator is simply printed in expression
	SCALARTOARRAY,			// left side of the operator is a scalar and right side
							// and array. Elementwise operation between a scalar 
							// and array is performed. The result is an array
							// of the same size as right argument
	ARRAYTOSCALAR,			// right side of the operator is a scalar and the left side
							// and array. Elementwise operation between a scalar 
							// and array is performed. The result is an array
							// of the same size as left argument
	ELEMENTWISEARRAY,		// both sides are arrays, elementwise operation should be 
							// performed
	VECTORTOMATRIX			// left side is vector, right side is vector or matrix
							// type-specific matrix operation should be performed

}
