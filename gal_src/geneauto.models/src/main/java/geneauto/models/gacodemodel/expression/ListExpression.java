package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.List;

public abstract class ListExpression extends Expression {
	/**
	 * Override this method, to allow setting types for custom types. These
	 * cannot be automatically inferred.
	 * 
	 * NOTE: this method was overridden because 
     * by default it is not allowed to change 
     * data type of the expression except the case
     * when new data type is null
	 */
    @Override
	public void setDataType(GADataType dt) {
    	if (dt instanceof TCustom) {
    	    attachDataType(dt); 
    	} else {
    		super.setDataType(dt);
    	}
	}
    
    @Override
    public void resetDataType() {
    	if (dataType instanceof TCustom) {
    		// in case of a structure we do not try to derive the type from 
    		// elements -- we simply assume it is correct
    		return;
    	} else {
    		// if it is not explicitly said to be a structure, we assume
    		// the data type is an array
    		
    		// reset data type of array elements
	        resetChildrenDataTypes();
	        
	        // derive data type from array elements 
	        dataType = null;
	    	TPrimitive newBaseType = getPrimitiveType();
	    	if (newBaseType != null) {
	        	TArray newType = (TArray) PrivilegedAccessor.getObjectInPackage(TArray.class, this);
	        	newType.setBaseType(newBaseType);
	        	newType.setDimensions(getDimensions());
	        	attachDataType(newType);
	        	checkIndexExpressions();
	    	}
    	}
    }
    
    protected abstract void resetChildrenDataTypes();
    
    public void setIndexExpressions(List<Expression> indexExpressions) {
        
        // set the full list as index of this ListExpression
    	super.setIndexExpressions(indexExpressions);
        
    	// remove first element and set remaining list as ListExpression 
    	// of each sub-element
        if (indexExpressions.size() > 1){
	    	List<Expression> tmpIdxExpr = new ArrayList<Expression>();
	    	tmpIdxExpr.addAll(indexExpressions);
	    	tmpIdxExpr.remove(0);
        	setIndexExpressionsToChildren(tmpIdxExpr);
        }
    }
    
    protected abstract void setIndexExpressionsToChildren(List<Expression> tmpIdxExpr);

	/**
     * Constructs list of IntegerExpressions with dimension lengths 
     * of the list expression. 
     * 
     * @return	Collections.EMPTY_LIST in case the expressions list is
     * empty or contains subexpressions with different lengths. Otherwise
     * a list with IntegerExpression for each dimension.
     */
    public abstract List<Expression> getDimensions();
    
    /**
     * returns the scalar member of List Expression
     * 
     * @param index
     *            array, that shows indexes of scalar value: e.g. ListExp: {{{1,
     *            2, 3}, {4, 5, 6}}, {{11, 12, 13}, {14, 15, 16}}} {0, 1, 2} ->
     *            ListExp[0][1][2] - > 6
     * @return the scalar member of List Expression
     */
    public Expression getElementByIndex(List<Integer> indexes) {
    	return getElementByIndex((Integer[]) indexes.toArray());
    }
    
    /**
     * returns the scalar member of List Expression
     * 
     * @param index
     *            array, that shows indexes of scalar value: e.g. ListExp: {{{1,
     *            2, 3}, {4, 5, 6}}, {{11, 12, 13}, {14, 15, 16}}} {0, 1, 2} ->
     *            ListExp[0][1][2] - > 6
     * @return the scalar member of List Expression
     */
    public Expression getElementByIndex(Integer[] indexes) {    	
    	if (indexes.length == 0){
            EventHandler.handle( EventLevel.CRITICAL_ERROR,
                    "models.gacodemodel.ListExpression.getElementByIndex", 
                    "GED0027",
                    "Empty list of indexes " + getReferenceString());
            return null;
    	}
    	
    	int currentIndex = indexes[0];
    	
    	if (getSize() < currentIndex+1){
            EventHandler.handle( EventLevel.CRITICAL_ERROR,
                    "models.gacodemodel.ListExpression.getElementByIndex", 
                    "GED0028",
                    "Index out of bounds!"
                    + "\n Expression: " + getReferenceString()
                    + "\n elements: " + getSize()
                    + "\n desired index: " + currentIndex);
            return null;    		
    	}
    	
    	Expression firstIndexExpression;
    	Expression secondIndexExpression;
    	Expression currentIndexExpression;
    	if (this instanceof ConstantListExpression) {
    		firstIndexExpression = ((ConstantListExpression) this)
    				.getExpression();
    		secondIndexExpression = firstIndexExpression;
    		currentIndexExpression = firstIndexExpression;
    	} else if (this instanceof GeneralListExpression) {
    		firstIndexExpression = ((GeneralListExpression) this)
    				.getExpressions().get(0);
    		if ( (indexes.length > 1) && (indexes[1] < getSize())) {
    			secondIndexExpression = ((GeneralListExpression) this)
						.getExpressions().get(indexes[1]);
				
    		} else {
    			secondIndexExpression = null;
    		}
			currentIndexExpression = ((GeneralListExpression) this)
				.getExpressions().get(currentIndex);
    		
    	} else {
    		firstIndexExpression = null;
    		secondIndexExpression = null;
    		currentIndexExpression = null;
    	}
    	// there is more indexes
    	// remove the first index from list and retrieve a member from 
    	// subexpression
    	if (firstIndexExpression instanceof ListExpression) {
    	    
    	    if(currentIndexExpression instanceof ListExpression){
    	        Integer[] l_indexes = new Integer[indexes.length-1];
    	    	for (int i=1; i < indexes.length; i++) {
	                l_indexes[i-1] = indexes[i];
    	    	}
    	    	
                return currentIndexExpression
                							.getElementByIndex(l_indexes);    
    	    } else if(currentIndex == 0){
    	        return secondIndexExpression;
    	    }
    	    else{
                EventHandler.handle( EventLevel.CRITICAL_ERROR,
                        "models.gacodemodel.ListExpression.getElementByIndex", 
                        "???",
                        "Index out of bounds!"
                        + "\n Expression: " + getReferenceString()
                        + "\n elements: " + getSize()
                        + "\n desired index: " + currentIndex,
                        "");
                return null;    
    	    }
    	}
    	
    	// no more indexes, return subexpression
    	else {
    		return currentIndexExpression;
    	}
    }
    
    /**
     * 
     * @return number of expressions (for GeneralListExpression) or 
     * length (for GeneralListExpression)
     */
    public abstract int getSize();
}
