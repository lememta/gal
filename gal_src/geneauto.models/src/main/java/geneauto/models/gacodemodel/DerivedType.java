/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/DerivedType.java,v $
 *  @version $
 *	@date $
 *
 *  Copyright (c) 2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;

public class DerivedType extends CustomType_CM {
	
    protected GADataType parentType;

    public DerivedType() {
        super();
    }

    public DerivedType(CustomTypeContent content, DefinitionScope scope) {
        super(content, scope);
    }

    public DerivedType(CustomTypeContent content, DefinitionScope scope, 
            GADataType parentType) {
        this(content, scope);
        setParentType(parentType);
    }

    public GADataType getParentType() {
        return parentType;
    }

    public void setParentType(GADataType parentType) {
        this.parentType = parentType;
        if (parentType != null) {
            parentType.setParent(this);
        }
    }

    /**
     * Function that tests whether one derived type depends on another derived 
     * type or custom type (Note - derived type is a special case of a custom type).
     * 
	 * Types are interdependent, if one uses the other in its definition. 
	 * Normally, this should not be mutual.
	 * 
	 * return  1 when the current type depends on the other type,
	 *        -1 when the other type depends on the current type,
	 * 		   0 when the two types are independent
	 */
	public int dependsOf(CustomType_CM ct2) {
    	
    	// Note: DerivedType is an instance of CustomType!
    	
    	// Check if this is the parent of the other type
    	if (ct2 instanceof DerivedType) {
    		DerivedType dt2 = (DerivedType) ct2;
    		if (dt2.getParentType() instanceof TCustom) {
	    		if (((TCustom) dt2.getParentType()).getCustomType().equals(this)) {
	    	    	// Current type is the supertype of the other type
	    	    	return -1;
	    		}
    		}
    	}
    	
    	// Check if the other type is the parent of this
		if (parentType instanceof TCustom) {
    		if (((TCustom) parentType).getCustomType().equals(ct2)) {
    	    	// The other type is the supertype of the current type
    	    	return 1;
    		}
		}
    		
		// Independent types
    	return 0;
	}
    
}
