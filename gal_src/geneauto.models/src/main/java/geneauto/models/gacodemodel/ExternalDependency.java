/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/ExternalDependency.java,v $
 *  @version	$Revision: 1.20 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

/**
 * This is a dependency of a CodeModel Module of an external module that is not
 * represented in the CodeModel, e.g. "math" (referenced by including "math.h").
 * 
 * The external module is referenced by name attribute
 */
public class ExternalDependency extends Dependency {

    /**
     * Determines if the dependency points to system library (true) or
     * user-defined library (false)
     */
    protected boolean isSystem = false;

    /**
     * True, when the dependency has a non-standard file extension 
     * (e.g. "zzz.param", instead of "zzz.h"). In this case the 
     * whole name is stored in the "name" field. Otherwise, the 
     * name is stored in the "name" field without the file extension 
     * (e.g. "zzz"). 
     */
    protected boolean nonStandardExtension = false;

    public ExternalDependency() {
        super();
    }

    /**
     * 
     * @param name
     *            -- name of the referenced module
     */
    public ExternalDependency(String name) {
        super(name);
        this.isSystem = false;
    }

    /**
     * @param name
     *            -- name of the referenced module
     * @param isSystem
     *            -- true == the dependency points to a system library
     */
    public ExternalDependency(String name, boolean isSystem) {
        super(name);
        this.isSystem = isSystem;
    }

    /**
     * @param name
     *            -- name of the referenced module (without file extension,
     *            except, when nonStandardExtension == true)
     * @param isSystem
     *            -- true == the dependency points to a system library
     * @param nonStandardExtension
     *            -- true == the file extension is non-standard
     */
    public ExternalDependency(String name, boolean isSystem, boolean nonStandardExtension) {
        super(name);
        this.isSystem = isSystem;
        this.nonStandardExtension = nonStandardExtension;
    }

   /**
     * @return name of the referenced module
     */
    public String getReferencedModule() {
        return name;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Dependency) {
            return ((Dependency)obj).getName().equals(getName());
        } else {
            return false;
        }
    }
}