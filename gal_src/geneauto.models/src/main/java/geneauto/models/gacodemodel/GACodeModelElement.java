/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/GACodeModelElement.java,v $
 *  @version	$Revision: 1.54 $
 *	@date		$Date: 2011-11-29 22:24:00 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.operator.Operator;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.genericmodel.GAModelElement;

/**
 * Abstract base class for all Gene-Auto Code Model language elements
 */
public abstract class GACodeModelElement extends GAModelElement {

    public GACodeModelElement() {
        super();
    }

    /**
     * A GASystemModelElement that is the source (origin) of this GACodeModel
     * element.
     * 
     * Note that 'sourceElement' is a direct object reference. Another feature 
     * 'sourceElementId' provides an indirect reference through the element's id. 
     * This can be used to simplify references, when the two models are serialized 
     * in different files.
     * 
     * At most one of 'sourceElement' or 'sourceElementId' should be assigned.
     */
    protected GASystemModelElement sourceElement;

	/**
	 * Indirect reference to the GASystemModelElement that is the source (origin) 
	 * of this GACodeModel element.
	 * 
	 * Note that another feature 'sourceElement' provides a direct reference to 
	 * that element.
	 * 
	 * At most one of 'sourceElement' or 'sourceElementId' should be assigned.
	 */
	protected int sourceElementId;
	
	/**
	 * Optional qualifier that specifies the role/action of the source 
	 * GASystemModelElement that generated this GACodeModelElement.
	 */
	protected String sourceAction;
	
    /**
     * Reference to the module where the element is defined. *
     */
    protected Module module;


    /**
     * Code Model specific reference string. Code Model elements are often
     * unnamed, but they often have an id. Element's identification is
     * considered unique, when it has an id that is not 0. If the id is not
     * present or is 0, then there will be additional path information supplied,
     * until a uniquely identifiable parent is found.
     * 
     * @return a string identifying the model element uniquely
     */
/* ToNa 26/11/08 not sure if we want to hide path information here    
    public String getReferenceString() {
        String parentPart = "";
        if (getId() == 0 && parent != null) {
            parentPart = parent.getReferenceString() + ".";
        }
        return parentPart + "<" + getClass().getSimpleName() + ": " + "name="
                + getOriginalName() + ", " + "id=" + getId() + ">";
    }
*/
    
    public Module getModule() {
        if (module != null) {
            return module;
        } else {
            // Go up the tree and try to get a module.
            if (getParent() instanceof GACodeModelElement) {
                module = ((GACodeModelElement) getParent()).getModule();
                return module;
            } else if (getParent() instanceof GADataType) {
                module = ((GADataType) getParent()).getModule();
                return module;
            } else {
                // If the parent is not a CodeModelElement this is an error.
                String msg = "Cannot determine containing module. "
                        + "\nCurrent element: " + getReferenceString();
                if (getParent() != null) {
                    msg += "\nParent: " + getParent().getReferenceString();
                }
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "GACodeModelElement.getModule()", "", msg);
                return null;
            }
        }
    }

    /**
     * This method must be implemented by the printer component for all
     * printable code model elements. Exceptions are declarable elements -- they
     * are printed using three methods getDeclaration, getDefinition and
     * getInstance
     * 
     * @return generated target language code for the element
     */
    public String print() {
        printUnsupportedMethodError(this, "print()");
        return null;
    }

    /**
     * Checks if current GACodeModelElement is part of a function (Function_CM) 
     * If it is returns pointer to this function, null otherwise
     * 
     * @return Function of the current GACodeModelElement or null in case if
     *         there are no such function
     */
    public Function_CM getContainingFunction() {
        if (getParent() instanceof Function_CM) {
            return (Function_CM) getParent();
        } else if (getParent() instanceof GACodeModelElement) {
            return ((GACodeModelElement) getParent()).getContainingFunction();
        } else {
            return null;
        }
    }

    /**
     * @return the containing NameSpace of a CodeModelElement or null, if it is missing
     */
    public NameSpace getContainingNameSpace() {
    	/*
		 * NOTE: The NameSpace containment hierarchy is a bit specific! 
		 * Except for elements directly in a NameSpace, the logically containing
		 * NameSpace is located in a separate field of a containing HasNameSpace
		 * element instance (e.g. Function, CompoundStatement, ...)
		 * Also, according to the current design - the parent of a NameSpace is 
		 * and can never be a NameSpace.
		 */
    	if (this instanceof NameSpace) {
    		// We need to handle NameSpace first - this is a special case
    		if (parent instanceof GACodeModelElement) {
    			return ((GACodeModelElement) parent).getContainingNameSpace();
    		} else {
    			return null;
    		}            
        } else if (parent instanceof NameSpace) {
            return (NameSpace) parent;
        } else if (parent instanceof HasNameSpace) {
            return ((HasNameSpace) parent).getNameSpace();
        } else if (parent instanceof GACodeModelElement) {
            return ((GACodeModelElement) parent).getContainingNameSpace();
        } else {
            return null;
        }
    }

    public CompoundStatement getCompoundStatement() {
        if (this instanceof CompoundStatement) {
            return (CompoundStatement) this;
        } else {
            if (this.getParent() instanceof GACodeModelElement) {
                return ((GACodeModelElement) this.getParent())
                        .getCompoundStatement();
            } else {
                return null;
            }
        }
    }

    /**
     * prints the error informing that a required field is missing
     * 
     * @param String
     *            field_ref missing required field or reference text explaining
     *            the accepted combination of fields
     */
    public void printNoRequiredFieldError(String field) {
        printNoRequiredFieldError(field, this);
    }

    /**
     * prints the error informing that a required field is missing
     * 
     * @param String
     *            field_ref missing required field or reference text explaining
     *            the accepted combination of fields
     *@param GACodeModelElement
     *            element element where error was found
     */
    public static void printNoRequiredFieldError(String field_ref,
            GACodeModelElement element) {
        EventHandler.handle(EventLevel.ERROR, element.getClass().getName(),
                "GEM0011",
                "Model consistency error: missing value in a required field. "
                        + "\n Object: " + element.getReferenceString() + ", "
                        + "\n type: " + element.getClass().getName()
                        + ", " + "\n field: " + field_ref, "");
    }
    
    // TODO add additional argument to the function and comment
    public static void printUnsupportedMethodError(GAModelElement el, 
            String methodName) {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "GACodeModelElement",
                "GEM0012",
                "Call to unimplemented or unsupported method: " + methodName
                + "\n Element: " + el.getReferenceString()
                + "\n Class: " + el.getClass().getCanonicalName());
    }
    
    /**
     * prints Error reporting that field is not applicable for the current 
     * model element
     * @param el model element
     * @param fieldName name of the field of the model element that is not
     * suitable
     * @param methodName name of the method where error occurs
     */
    public static void printFieldNotApplicableError(GACodeModelElement el,
            String fieldName, String methodName) {
        EventHandler.handle(EventLevel.ERROR, methodName, "GEM0013",
                "Field " + fieldName + " is not applicable in " +
                el.getClass().getCanonicalName()
                        + "/n Element: " + el.getReferenceString(), "");
    }
    
    /**
     * prints error reporting that operator is unsupported
     * @param el
     * @param op operator
     * @param methodName name of the method where error occurs
     */
    public static void printUnsupportedOperatorException(GACodeModelElement el,
            Operator op, String methodName) {
        EventHandler.handle(EventLevel.ERROR,
                methodName, "GEM0055",
                "Unsupported operator: " + op + 
                "\nElement: " + el.getReferenceString());
    }
        

    public GASystemModelElement getSourceElement() {
        return sourceElement;
    }

    public void setSourceElement(GASystemModelElement sourceElement) {
        this.sourceElement = sourceElement;
    }

    public int getSourceElementId() {
		return sourceElementId;
	}

	public void setSourceElementId(int sourceElementId) {
		this.sourceElementId = sourceElementId;
	}

	public String getSourceAction() {
		return sourceAction;
	}

	public void setSourceAction(String sourceAction) {
		this.sourceAction = sourceAction;
	}

	@Override
    public void copyCommonAttributes(GAModelElement element) {
        super.copyCommonAttributes(element);
        if (element instanceof GACodeModelElement) {
            ((GACodeModelElement) element).setSourceElement(getSourceElement());
        }
    }

    /**
     * removes element from the the NameSpace in case element is
     * NameSpaceElement child
     */
    public boolean removeFromNameSpace() {
    	return false;
    }

    public void setModule(Module module) {
        this.module = module;
    }
}