/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/expression/Expression.java,v $
 *  @version	$Revision: 1.152 $
 *	@date		$Date: 2012-02-19 23:05:01 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.expression;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPointer;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.models.utilities.ExpressionUtilities;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.List;


/**
 * An expression is a sequence of operators and operands that specifies
 * computation of a value, or that designates an object or a function, or that
 * generates side effects, or that performs a combination thereof. In this
 * CodeModel expressions have a pure meaning. I.e. they always return a value
 * and cause no side-effects. E.g.: myFunction(5) + x2.
 * 
 * Absence of side effects is assumed for the expression itself and is not
 * guaranteed in case the expression contains functions.
 */
public abstract class Expression extends GACodeModelElement {

    /**
     * Datatype of the Expression.
     * 
     * This can be null, when the type is not known.
     * 
     */
    protected GADataType dataType;
    protected List<Expression> indexExpressions = new ArrayList<Expression>();
    
    /**
     * Buffered result of the eval() method that can be accessed through 
     * getValue() and setValue().
     * 
     * It is assumed that after the eval method is executed once, the
     * contents of the expression do not change any more. 
     * 
     * When eval() is called on an object that has been already evaluated, 
     * the buffered value is returned.
     * 
     * To reset use setValue(null);
     */
    private Expression value;

    public Expression() {
        super();
    }

    public Expression getValue() {
        return value;
    }

    /**
     * This method is normally called by the eval() method implementations. 
     * Use setValue(null) to explicitly reset the buffered value.
     * 
     * @param value
     */
    public void setValue(Expression value) {
        if (value != null) { 
            value.setParent(this);
        }
        this.value = value;
    }

    /**
     * Returns the target language string corresponding to the expression.
     * 
     * Note! This should be overridden by the target language specific
     * subclasses.
     * 
     * @param precLevel
     *            Precedence level of the context of the expression.
     * @return Target language string corresponding to the expression.
     */
    public String print(int precLevel) {
        printUnsupportedMethodError(this, "print");
        return null;
    }

    public GADataType getDataType() {
        if (dataType == null) {
        	// Resets the data type recursively in sub-expressions
			// TODO It is a bit awkward place for this action. If the dataType
			// field is null already, then why scan continue processing deeper?
        	// AnTo 21.04.2011 
            resetDataType();
        } 
        return dataType;
    }
    
    /**
     * Reevaluates data type of expression
     */
    public void resetDataType() { }

    /**
     * Sets the data type of the Expression.
     * 
     * NOTE! For the base class and for classes that do not override this method
     * it allows only to reset the datatype to null. The reason for that is that
     * often the datatype of the expression should be computed from bottom to
     * top starting from the atoms (implemented by getDataType). Resetting is
     * allowed only to clear the previously computed data type, in case the
     * Expression's structure or the type of some or its components has changed.
     * 
     * Was before: Same as setDataType(dataType, true);
     * TODO: decide if we should reset it to null here or call resetDataType
     * instead (ToNa 29/04/10) 
     */
    public void setDataType(GADataType dataType) {
        if (dataType == null) {
            this.dataType = null;
        }
    }

    /**
     * Sets data type. As it is assumed, that data type instance is always owned
     * by its parent then always copy the given dataType object.
     * 
     * @param dataType
     *            type to set
     * @param indexConversion
     *            when this is true, then the dimensionality of the data type is
     *            reduced, based on the amount of indexes there are in the
     *            expression.
     */
    public void setDataType(GADataType dataType, boolean indexConversion) {

        if (dataType == null) {
            this.dataType = null;

        } else {
        	attachDataType(dataType);
            
            // Reduce the dimensionality of the data type, if required.
            if (indexConversion) {
                for (int i = indexExpressions.size(); i > 0; i--) {
                    reduceDimensionality();
                }
            }
        }
    }
    
    /**
     * Attaches given data type to object
     *   - when given data type does not have parent, then we attach it to current object
     *   - when given data type has already a parent, we make copy and attach this to current object
     * @param dt
     */
    protected void attachDataType(GADataType dt) {
    	dataType = DataTypeUtils.attachDataType(dt);
        if (dataType != null) {
        	dataType.setParent(this);
        }
    }

    public List<Expression> getIndexExpressions() {
        return indexExpressions;
    }

    /**
     * Tries to evaluate index expressions and returns a list of integers on
     * success
     */
    public Integer[] evaluateIndexes(boolean raiseError) {
        Integer[] indexes = new Integer[getIndexExpressions().size()];
        int i = 0;
        for (Expression e : getIndexExpressions()) {
        	NumericExpression val = e.evalReal();
            if (val == null) {
                if (raiseError) {
                    String msg = "Cannot evaluate index expression.";
                    if (e.isConst() == false) {
                        msg += " Index is not a constant.";                    
                    }
                    msg += "\n Expression: " + this.toString()
                    + "\n Reference: "  + this.getReferenceString()
                    + "\n Index: "      + e.toString()
                    + "\n Reference: "  + e.getReferenceString();

                    EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                            "Expression.evaluateIndexes()", "", msg);
                }
                return null;                
            }
            indexes[i] = (int) val.getRealValue();    
            i++;
        }
        return indexes;
    }
    
    public Integer[] evaluateIndexes() {
        return evaluateIndexes(true);
    }

    /**
     * Same as addIndexExpression(exp, true);
     */
    public void addIndexExpression(Expression exp) {
        addIndexExpression(exp, true);
    }
    

    /**
     * Add an index expression. Each add also removes one dimension from the
     * data type of the expression.
     * 
     * @see setDataType
     * @param typeConversion
     *            when this is true, then the dimensionality of the data type is
     *            reduced by one.
     */
    public void addIndexExpression(Expression exp, boolean typeConversion) {
        exp.setParent(this);
        // NOTE: It is important to call getDataType() before adding the indexes,
        // as getDataType() might update the dataType and there will be inconsistency
        // between the inferred base type of the expression and indexes.
        GADataType dt = this.getDataType();
        /*if (((exp.getDataType()) != null)
                && (!(exp.getDataType() instanceof TRealInteger))) {
            EventsHandler.handle(EventLevel.ERROR, 
                    "Expression.addIndexExpression", "", 
                    "Data type of index Expression should be TRealInteger");
            return;
        }*/
        // TODO temporary solution for
        // NullPointerException in StateflowImporter
        if (dt instanceof TPointer) {
            dt = ((TPointer) dt).getBaseType();
        }
        if (dt == null || !dt.isScalar()) {
            indexExpressions.add(exp);
            if (typeConversion && dt != null) {            
                // Update the data type
                reduceDimensionality();
            }
        }
        
        
    }
    
    /**
     * @param aDT data type of expression if 
     * it was without indexes
     * 
     * Increases value of index expression by 1.
     * E.g. 
     * [0][1][2] -> [0][1][3] (3. dim size > 3)
     * or 
     * [0][1][2] -> [0][2][2] (3. dim size <= 3, 2. dim size > 2)
     */
    public void incIndexExpression(TArray aDT) {
        IntegerExpression index;
        Expression dim;
        for (int i = indexExpressions.size(); i > 0; i--) {
            index = (IntegerExpression) getIndexExpressions().get(i-1);
            dim = aDT.getDimensions().get(i-1);
            
            /* 
             * Calculate starting value for index expression
             * (it may be different from 0 in case of sub array 
             * expression)
             */
            Integer start = getIndexExpressionStartIndex(i-1);
            if ((index.evalReal() == null) 
                    || (!(index instanceof IntegerExpression))) {
                // add error here
                
            // the value of current index is the same as 
            // the value of dimension -> reset current dimension
            } else if (index.evalReal().getRealValue() - start
                    == (dim.evalReal().getRealValue() - 1) ) {
                ExpressionUtilities.resetIndex(index, start);
             
            // the value of current index is not the same as 
            // the value of dimension -> increase current dimension by 1
            } else {
                ExpressionUtilities.incIndex(index);
                break;
            }
        }
    }
    
    /**
     * Resets indexExpressions with their starting indexes. E.g.
     * 		ar[3][5][1] -> ar[0][0][0] 
     */
    public void resetIndexExpressions() {
        IntegerExpression dimExp = (IntegerExpression) PrivilegedAccessor.getObjectInPackage(
                IntegerExpression.class, this);
        
		// We will unattach the old dataType object and leave it existing, in
		// case there are any references to it.
        // Note: We are intentionally bypassing the getter!
        GADataType dtOld = dataType;  
        // Note: We are intentionally bypassing the setter!
        dataType = dtOld.getCopy();
        dataType.setParent(this);
        
        int startIndex;
        
        // Add starting starting index to every index expression
        for (int i = 0; i < dtOld.getDimensions().size(); i++) {
        	if (i > 0) {
        		dimExp = (IntegerExpression) dimExp.getCopy();
        	}
        	startIndex = getIndexExpressionStartIndex(i);
        	ExpressionUtilities.resetIndex(dimExp, startIndex);
            addIndexExpression(dimExp.getCopy());
        }
    }
    
    /**
     * 
     * 
     * @param indExpIndex ordinal of index expression
     * @return Starting index of index expression (0 by default, 
     * may be different in sub array expression)
     */
    public int getIndexExpressionStartIndex(int indExpIndex) {
    	return 0;
    }
    
    /**
     * Converts list of integers to IntegerExpressions and sets them as 
     * indexExpressions of current expression
     * @param indexes
     */
    public void setIndexes(Integer[] indexes) {
    	for (Integer idx : indexes) {
    	    IntegerExpression intExp = (IntegerExpression) PrivilegedAccessor.
    	        getObjectInPackage(IntegerExpression.class, this);
    	    intExp.setLitValue(idx.toString());
    		addIndexExpression(intExp, true);
    	}
    }
    
    /**
     * Converts list of integers to IntegerExpressions and sets them as 
     * indexExpressions of current expression
     * @param indexes
     */
    public void setIndexes(List<Integer> indexes){
    	setIndexes((Integer[]) indexes.toArray());
    }

    /**
     * Same as setIndexExpressions(indexExprs, true);
     */
    public void setIndexExpressions(List<Expression> indexExprs) {
        setIndexExpressions(indexExprs, true);
    }

    /**
     * sets indexExpressions to the current expression, adds current expression
     * as parent of all indexEpxressions of this expression
     * 
     * NOTE! It is assumed that the expression's type must be non-scalar to allow 
     * adding index expressions! If the expression already had index expressions, 
     * then the resulting type might have been scalar (e.g. if ar is a one-dimensional 
     * array, then ar[5] is scalar). 
     * 
     * The type is ignored, when null is supplied. Also, calling 
     * 		setIndexExpressions(null) 
     * clears the old data type and you can then assign new index expressions. 
     * 
     * @param indexExpressions
     *            index expression to be set to the current expression
     * @param typeConversion
     *            when this is true, then the dimensionality of the data type is
     *            reduced by one.
     */
    public void setIndexExpressions(List<Expression> indexExprs,
            boolean typeConversion) {
    	
    	if (indexExprs == null) {
    		if (this.indexExpressions != null) {
    			this.indexExpressions.clear();
    		}
			// The data type of the expression depends on the number of index
			// expressions. If they are removed, reset the data type. It will be
			// automatically re-derived, when needed and if possible.
    		this.dataType = null;
    		return;
    	}
    	
        if (!this.isScalar()) {
            this.getIndexExpressions().clear();
            for (Expression expr : indexExprs) {
                this.addIndexExpression(expr, typeConversion);
            }
        }
    }

    /**
     * Reduces the dimensionality of the expression datatype
     * 
     * NB! The method assumes that the datatype object belongs to the current
     * Expression and that there are no other references to it. The datatype 
     * object is modified in place or replaced by another one (when reduced to 
     * a scalar). Thus 
     * 
	 * 	  MAKE SURE that it is safe to modify or discard the attached datatype
	 * 	  object and/or UPDATE any direct references to the datatype, after 
	 *    executing this method.
     * 
     */
    protected void reduceDimensionality() {
    	GADataType dt = getDataType();
    	
    	// data type is pointer -> reduce dimensionality of base type
    	if (dt instanceof TPointer) {
    	    dt = ((TPointer) dt).getBaseType();
    	}
        if (dt instanceof TArray) {
        	GADataType dtNew = ((TArray) dt).reduceDimensionality();
        	if (dtNew != dt) {
        		setDataType(dtNew, false);
            }
        } else {
            EventHandler.handle(
                EventLevel.ERROR, "Expression", "",
                "Error infering data type of an expression. " +
                "More indexes given than there are dimensions in " +
                "the basic type of the expression."
                        + "\nExpression: " + getReferenceString());
        }
    }

	/**
	 * Specialisation of moveCommonAttributes for Expression-to-Expression move
	 * (Direction is from the current Expression to the supplied one)
	 * 
	 * NOTE: The derived attributes dataType and value are not moved, since this
	 * is not generally meaningful. If it is required in some cases, then create
	 * a special method or add a flag for tuning the behaviour.
	 */
    public void moveCommonAttributes(Expression otherExp) {
    	super.moveCommonAttributes(otherExp);
    	
    	// The derived attributes dataType and value are not moved
    	
    	// Move any other attributes    	
        if (!this.getIndexExpressions().isEmpty()) {
            otherExp.setIndexExpressions(this.getIndexExpressions());
        }        
        
    }

    /**
     * Performs type/shape normalisation of the Expression.
     * 
     * Full normalisation means that row and column matrixes (vector-matrixes)
     * are converted to vectors and all singleton arrays (vectors, matrixes,
     * arrays) with only one element are converted to scalars.
     * 
     * Partial normalisation only converts singleton arrays.
     * 
     * @param full
     *            If true, then row and column matrixes (vector-matrixes) are
     *            converted to vectors.
     * @return a converted expression or null on error.
     */
    public Expression normalizeShape(boolean full) {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                getClass().getSimpleName() + ".normalizeShape()", "", 
                "Unsupported type of expression for this operation. " +
                "\n Expression: " + getReferenceString());
        
        return null;
    }
    

    /**
     * Same as eval(printError)
     */
    public Expression eval() {
        return eval(true, false);
    }
    
    /**
     * Calculates the result of the expression, if it consists of only constant
     * elements. Otherwise returns null.
     * 
     * @param strict
     *            Raise error, when cannot evaluate
     * @param optimizableOnly
     *            Do not evaluate unoptimisable data. I.e return null, when
     *            the element to be evaluated is not optimisable. Here only
     *            meaningful for constants. For non-constant data null is
     *            returned anyway.
     */
    public Expression eval(boolean strict, boolean optimizableOnly) {
        if (strict) {
            EventHandler.handle(EventLevel.ERROR, "eval",
                    "GEM0057", "Cannot evaluate: " 
                        + "\n Element: " + getReferenceString()
                        + "\n Class: " + getClass().getCanonicalName());
        }
        return null;
    }

    
    /**
     * Same as evalReal(true)
     */
    public NumericExpression evalReal() {
        return evalReal(true, false);
    }

    /**
     * Same as eval(printError), but if the expression evaluates to a value, it is
     * expected to be of real numeric type.
     * 
     * @param printError
     *            Raise error, when cannot evaluate
     * @param optimizableOnly
     *            Do not evaluate unoptimisable data. I.e return null, when
     *            the element to be evaluated is not optimisable. Here only
     *            meaningful for constants. For non-constant data null is
     *            returned anyway.
     */
    public NumericExpression evalReal(boolean printError, boolean optimizableOnly) {
        Expression exp = eval(printError, optimizableOnly);
        if (exp == null) {
            return null;
        } else if (exp instanceof NumericExpression) {
            return (NumericExpression) exp;
        } else {
            EventHandler.handle(EventLevel.ERROR, "Expression.evalReal()",
                    "GEM0063",
                    "Expecting a real numeric value: " 
                    + "\nElement: " + exp.getReferenceString() 
                    + "\nInferred type:" + exp.getDataType().toString());
            return null;
        }
    }

    /**
     * @return true if the variable in expression is scalar, false in case it is
     *         not scalar or the type of the expression cannot be determined.
     */
    public boolean isScalar() {
        return isScalar(false);
    }

    /**
     * @param isForgiving
     *            when this argument is true and the data type cannot be
     *            determined no error is thrown. Otherwise, an error is thrown.
     * 
     * @return true if the variable in expression is scalar, false in case it is
     *         not scalar or the type of the expression cannot be determined.
     */
    public boolean isScalar(boolean isForgiving) {
        if (getDataType() != null) {
            return !(getDataType() instanceof TArray);
        } else {
            if (!isForgiving) {
                EventHandler.handle(EventLevel.ERROR, "Expression.isScalar()",
                        "GED0025", "Data type undefined. Element: "
                                + getReferenceString(), "");
            }
            return false;
        }
    }

    /**
     * Determines if the given expression is a legal value for array dimension.
     * The expression must meet following restrictions: - the value is literal
     * value or named constant - the value is integer value or in case of real
     * value, has only integer part - the value is > 0
     * 
     * @param printError flag that determines print error or not
     * @param refString parsed string from which error comes
     * @return boolean
     */
    public boolean canBeDimensionLength(boolean printError, String refString) {
        String errorMsg = "";
        if (refString != null) {
            errorMsg += "Error while parsing '" + refString + "': \n";
        }
        boolean returnType = true;
        // TODO: add common checks here (e.g. eval() > 0)
        // and type specific check to each expression type
        if (eval(false, false) == null) {
            if (printError) {
                errorMsg += "Current expresssion type";
            }
            returnType = false;
        } else {
            double l_value = this.evalReal(printError, false).getRealValue();
            if ((l_value < 1) || (l_value != (int) l_value)) {
                if (printError) {
                    errorMsg += "Value of current expression (" + l_value + ")";
                }
                returnType = false;
            } 
        }
        if (!returnType) {
            if (printError) {
                EventHandler
                .handle(
                        EventLevel.ERROR,
                        "Expression.canBeDimensionLength()",
                        "GEM0062",
                        errorMsg + " can not be the lenght of dimension: "
                        + "\nExpression: " 
                        + printElement() +
                        "\nExpression datatype: " + getDataType().toString());
            }
        }
        return returnType;
    }
    
    public boolean canBeDimensionLength() {
        return canBeDimensionLength(false, null);
    }

    public boolean isConst() {
        return false;
    }

    public Expression getCopy() {
        return (Expression) super.getCopy();
    }

    /**
     * Converts appropriate parts from SystemModel constructs to CodeModel
     * constructs.
     * 
     * NOTE 1: This method changes the Expression ITSELF (in derived classes)!
     * If a COPY is needed instead, make getCopy() first and then call
     * toCodeModel() on the copied expression. I.e. the supplied expression can 
     * be modified and returned or modified and another expression returned 
     * instead.
     * 
     * NOTE 2: Implementations of this method are NOT meant to go RECURSIVELY
     * into sub-expressions. Use getAllChildren(Expression.class) to get all
     * sub-expressions when required.
     * 
     * NOTE 3: If the result of the conversion is a new object, then it is the 
     * responsibility of the toCodeModel(..) implementation to update the required 
     * references, primarily the respective containment and parent link. This can 
     * be accomplished via a call to the replaceMe(..) method.
     * 
     * @return the generated CodeModel Expression that might or might not be the
     *         current expression itself
     */
    public Expression toCodeModel() {
    	
    	// convert the indexes
        for (Expression e : this.getIndexExpressions()) {
            e.toCodeModel();
        }
        
        // convert datatype (has effect only in case to TCustom)
        if (getDataType() != null) { 
        	setDataType(getDataType().toCodeModelElement());
        }
        
        return this;
    }

    /**
     * List with all variables referenced by an expression. By default
     * returns unfilled List;
     * 
     * 
     * @return List with expression's variables - if expression contains
     *         variable(s), unfilled List - if doesn't
     */
    public List<Variable> getReferencedVariables() {
        List<Variable> result = new ArrayList<Variable>();
        Variable var = this.getExpressionVariable();
        if (var != null) {
            result.add(var);
        }
        for (GAModelElement varExpElem : this.getAllChildren(LValueExpression.class)) {
            LValueExpression varExp = (LValueExpression) varExpElem;
            result.add(varExp.getVariable());
        }
        return result;
    }

    /**
     * Returns primary target variable of an expression In case of Variable
     * expression this is value of attribute variable and in case of Member
     * expression the value of attribute member
     * 
     * In all other cases returns null
     */
    public Variable getExpressionVariable() {
        return null;
    }

	/**
	 * @return string value corresponding to the data value
	 */
	public String getStringValue() {
        EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
                "GEM0012",
                "Call to unimplemented or unsupported method: getStringValue()"
                + "\n Element: " + getReferenceString()
                + "\n Class: " + getClass().getCanonicalName());
        return null;
	}
	
    /**
     * 
     * @return statement of the current expression
     */
    public Statement getContatiningStatement() {
        if (getParent() == null){
            return null;
        } else if (getParent() instanceof Statement) {
            return (Statement) getParent();
        } else if (getParent() instanceof Expression) {
            return ((Expression) getParent()).getContatiningStatement();
        } else {
            return null;
        }
    }

    /**
     * Replaces one expression with another.
     * 
     * @param expNew
     *            expression to be set instead of the old one
     */
    @Override
    public void replaceMe(Object element) {
        if (!(element instanceof Expression)) {
            String errMsg = "An attempt to replace expression with ";
            if (element == null) {
                errMsg += "null.";
            } else {
                errMsg += "element of another type. " 
                    + "\nElement's class: "
                    + element.getClass().getCanonicalName();
            }
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    "Expression.replaceMe()", "", errMsg
                    );
            return;
        }
        
        Expression expNew = (Expression) element;
        
        super.replaceMe(expNew);
    }

    /**
     * returns the sequential number of Expression with indexExpressions E.g.
     * myAr = new int[3][4][5] myAr[1][2][3] sequential number is: 3 + 2*5 +
     * 3*4*5
     * 
     * @return sequential number of Expression with indexExpressions, -1 - if
     *         Expressions has no indexExpression or indexExpression consists of
     *         variables
     */
    public int getSeqNum() {
        int result = 0;
        int size = this.getDataType().getDimensions().size();
        if (size == 0) {
            return -1;
        }
        int product = 1;
        for (int i = (size - 1); i >= 0; i--) {
            for (int j = (size - 1); j > i; j--) {
                if (this.getDataType().getDimensions().get(j).eval() != null) {
                    product *= this.getDataType().getDimensions().get(j)
                            .evalReal().getRealValue();
                } else {
                    return -1;
                }
            }
            int indexValue;
            if (this.getIndexExpressions().get(i).eval() != null) {
                indexValue = (int) this.getIndexExpressions().get(i).evalReal()
                        .getRealValue();
            } else {
                return -1;
            }
            result += product * indexValue;
        }
        return result;
    }

    /**
     * 
     * @return all subexpressions of current Expression
     */
    public List<Expression> getAllSubExpressions() {
        List<Expression> result = new ArrayList<Expression>();
        for (GAModelElement elem : this.getAllChildren(Expression.class)) {
            result.add((Expression) elem);
        }
        return result;
    }

    /**
     * This method is to simplify listExpression -- by default we always
     * construct a list of single element. ListExpression overrides this method
     * and returns list of its subexpressions
     * 
     * TODO This is dubious semantic overloading. Should be better only 
     * applicable to the ListExpression class (AnTo 120210) 
     */
    public List<Expression> getExpressions() {
        List<Expression> exprList = new ArrayList<Expression>();
        exprList.add(this);
        return exprList;
    }

    public Expression getElementByIndex(List<Integer> indexes) {
    	Integer[] idxArray = (Integer[]) indexes.toArray();
    	
    	return getElementByIndex(idxArray);
    }
    /**
     * makes copy of itself and sets indexes according to the given
     * list
     * 
     * ListExpression overrides this method and returns single element
     * instead of indexed Expression
     * 
     * @param indexes
     * @return
     */
    public Expression getElementByIndex(Integer[] indexes) {
    	if (indexes.length != getDataType().getDimensionality()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		"Models.Expression.getElementByIndex", 
            		"GEM0064",
                    "Incompatible dimensionality. " 
            		+ "\nDatatype has "
            		+ getDataType().getDimensionality() 
            		+ " dimensions, "
            		+ indexes.length 
            		+ " indexes provided. \n Expression: "
            		+ getReferenceString(), 
            		"");
            return null;
    	}

    	int[] dims = getDataType().getDimensionsLengths();
    	for (int i = 0; i < indexes.length; i++) {
    		if (indexes[i] >= dims[i]) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                		"Models.Expression.getElementByIndex", 
                		"GEM0065",
                        "Incompatible dimension lenghts \n Expression: "
                		+ getReferenceString(), 
                		"");
                return null;
    		}
    		
    	}
    	Expression result = getCopy();
    	result.setIndexes(indexes);
    	
        return result;
    }
    
    /**
     * Prints the current element in pseudo code or event messages
     * 
     * @param outerPrecedence precedence of the outer expression
     * 
     * @return pseudo code of the element
     */
    public String printElement() {
        return printElement(0);
    }
    
    /**
     * Prints the current element in pseudo code or event messages
     * 
     * @param outerPrecedence precedence of the outer expression
     * 
     * @return pseudo code of the element
     */
    public String printElement (int outerPrecedence) {
        return null;
    }
    
    /**
     * Prints the index part of the expression: e.g. <Exp>[0][1].
     * 
     * @param exp
     *            expression whose indexes are to be printed
     * @return the indexPart string of the expression
     * 
     */
    public static String printIndexPart(Expression exp) {
        String indexPart = "";
        for (Expression idxExp : exp.getIndexExpressions()) {
            indexPart += "[" + idxExp.printElement() + "]";
        }
        return indexPart;
    }
    
    public static String parenthesize(String exprStr, int outerPrecLevel,
            int innerPrecLevel) {
        if (outerPrecLevel > innerPrecLevel) {
            exprStr = "(" + exprStr + ")";
        }
        return exprStr;
    }

    /**
     * Browse current expression and all his children and check if there's among
     * this a binary expression with Div operator.
     * 
     * @return
     */
    public boolean containsBinaryDiv() {
        if ((this instanceof BinaryExpression)
                && ((BinaryExpression) this).getOperator().getName().equals(
                        "BinaryOperator: DivOperator")) {
            return true;
        }
        for (GAModelElement e : this.getChildren()) {
            if (e instanceof Expression) {
                if (((Expression) e).containsBinaryDiv()) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public List<BinaryExpression> getBinaryDiv() {
        List<BinaryExpression> bins = new ArrayList<BinaryExpression>();
        
        if ((this instanceof BinaryExpression)
                && ((BinaryExpression) this).getOperator().getName().equals(
                        "BinaryOperator: DivOperator")) {
            bins.add((BinaryExpression)this);
        }
        for (GAModelElement e : this.getChildren()) {
            if (e instanceof Expression) {
                bins.addAll(((Expression)e).getBinaryDiv());
            }
        }
        return bins;
    }
 
    /**
     * Returns true, if current expression contains list expression
     * or is list expression itself
     */
    public boolean containsListExpression() {
        if (this instanceof ListExpression) {
            return true;
        } else {
            return !(getAllChildren(ListExpression.class).isEmpty());
        }
    }
    
    /**
     * Checks if the name of the expression or one of its 
     * children is the same as given string name
     * 
     * @param name name for comparison
     * @return the result of the comparison
     */
    public boolean nameEquals(String name) {
        Expression exp;
        for (GAModelElement el: getChildren(Expression.class)) {
            exp = (Expression) el;
            if (exp.nameEquals(name)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Defines whether expression is literal or not
     * 
     * NOTE: literal for ListExpression means that all of its children 
     * are literal
     * 
     * @return True if expression is literal, false otherwise
     */
    public boolean isLiteral() {
        return false;
    }
    
    /** 
     * @return primitive type of the current expression. When expression has 
     * data type, it will be the primitive type of that data type.
     * 
     * If no data type is given, by default null is returned. In special cases
     * (eg in case of ListExpression) the data type is derived from 
     * subexpressions.
     */
    public TPrimitive getPrimitiveType() {
    	if (getDataType() == null) {
    		return null;
    	} else {
    		return getDataType().getPrimitiveType();
    	}
    }
    
    /**
     * Checks if expression has dimensions. 
     * If yes -> reduces dimensionality
     */
    public void checkIndexExpressions() {
        if (!(indexExpressions.isEmpty())) {
            int numOfIndexes = indexExpressions.size();
            if (dataType instanceof TArray) {
                TArray aDT = (TArray) dataType;
                List<Expression> dims = aDT.getDimensions();
                for (int i = 0; i < numOfIndexes; i++) {
                    reduceDimensionality();
                }
                if (dims.isEmpty()) {
                    
                }
            }
        }
    }
    
    /**
     * 
     * @return List where may be expression in case it may 
     * have index expressions and all children expressions 
     * that may have index expressions
     * 
     */
    public List<Expression> getExpendableAtoms() {
    	return new ArrayList<Expression>();
    }
}