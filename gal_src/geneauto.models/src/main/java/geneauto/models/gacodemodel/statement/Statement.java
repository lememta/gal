/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/statement/Statement.java,v $
 *  @version	$Revision: 1.71 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel.statement;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.FunctionBody;
import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.models.gacodemodel.Label;
import geneauto.models.gacodemodel.SequentialComposition;
import geneauto.models.gacodemodel.SequentialFunctionBody;
import geneauto.models.gacodemodel.expression.LValueExpression;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A statement specifies an action to be performed. Statements do not return a
 * value and normally have some side-effect.
 * 
 * E.g: x1 = x2 + 3
 */
public abstract class Statement extends GACodeModelElement {

    /** label to be printed before the statement body */
    private Label label;
    /** sequence number in the code. 0 if not initialised */
    private int sequenceNumber = 0;

    /**
     * Default constructor
     */
    public Statement() {
        super();
    }

    public Statement(Annotation annotation) {
        super();
        this.annotations = new LinkedList<Annotation>();
        this.annotations.add(annotation);
    }

    public Statement(List<Annotation> annotations) {
        super();
        this.annotations = annotations;
    }

    public void setLabel(Label label) {
        if (label != null) {
            label.setParent(this);
        }
        this.label = label;
    }

    public Label getLabel() {
        return label;
    }

    public int getSubStatementsCnt() {
        return 1;
    }

    /**
     * @param printBodyOnly
     *            if true -> prints statement without semicolons, labels and
     *            annotations
     * @return
     */
    public String print(boolean printBodyOnly) {
        printUnsupportedMethodError(this, "print");
        return null;
    }

    /**
     * removes current statement from the name space
     */
    public boolean removeFromNameSpace() {
        if (getStatementSpace() != null) {
            getStatementSpace().getStatements().remove(this);
            return true;
        }
        
        return false;
    }

    /**
     * @return all Variables referenced by the current Statement
     */
    public List<Variable> getReferencedVariables() {
    	List<Variable> vars = new LinkedList<Variable>();
    	for (GAModelElement el : getAllChildren(LValueExpression.class)) {
    		vars.add(((LValueExpression) el).getVariable());
    	}
        return vars;
    }

    /**
     * TODO There is a name-semantic inconsistency in this method. 
     * A "sub-" method should not contain the element itself! AnTo 101210
     * 
     * @return Sub-statements of current Statement and statement itself
     * 
     */
    public List<Statement> getSubStatements() {
        List<Statement> statements = new ArrayList<Statement>();
        statements.add(this);
        return statements;
    }

    /**
     * TODO There is a name-semantic inconsistency in this method. 
     * A "sub-" method should not contain the element itself! AnTo 101210
     * 
     * @return The Statement itself and all its sub-statements
     */
    public List<Statement> getAllSubStatements() {
    	// Default behaviour in super class
        return new ArrayList<Statement>();
    }

    /**
     * 
     * @return 1) non-virtual CompoundStatment or SequntialFuntionBody, which 
     *         attribute statements member is current statement 
     *         
     *         2) null - if current Statement is single statement 
     *         of some statement part (for example simple assign 
     *         statement in else clause of IfStatement)
     */
    public SequentialComposition getStatementSpace() {
        GAModelElement element = this.getParent();
        while (!(element instanceof SequentialComposition)) {
            if ((element instanceof CompoundStatement)
                    && (((CompoundStatement) element).isVirtual())) {
                continue;
            } else {
                element = element.getParent();
            }
        }
        return (SequentialComposition) element;
    }

    /**
     * Adds to current statement's annotation annotations of statement st
     * 
     * @param st
     *            statement from which we copy annotation to current statement
     */
    public void copyAnnotationFromStatement(Statement st) {
        for (Annotation a: st.getAnnotations()) {
            addAnnotation(a.getCopy());
        }
    }
        
    /**
     * Removes all annotations from statement st to the current
     * statement
     * @param st statement from annotations are copied
     */
    public void takeAnnotationsFrom(Statement st) {
        for (Annotation a: st.getAnnotations()) {
            addAnnotation(a);
        }
        st.deleteAllAnnotations();
    }
    
    /**
     * Deletes all annotations of the statement
     */
    public void deleteAllAnnotations() {
        this.annotations.clear();
    }

    /**
     * Overrides the superclass method to get the type right
     * 
     * (non-Javadoc)
     * 
     * @see geneauto.models.genericmodel.GAModelElement#getCopy()
     */
    public Statement getCopy() {
        return (Statement) super.getCopy();
    }

    /**
     * Recurses up in the tree and returns true, if we reach function body
     * or root element with no conditionally executed statement (IfStatement, 
     * WhenCase, DefaultCase, ForStatement
     * 
     * @return true - any of the parents of this statement is 
     * 				IfStatement or when-/defaultCase of CaseStatement, false otherwise
     */
    public boolean isConditional() {
    	
    	if (getParent() == null 
    			|| !(getParent() instanceof Statement
    					|| getParent() instanceof FunctionBody)) {
    		/* parent of a statement can be only another statement or function
    		 * body. If something else is found, return an error
    		 */
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		"Statement.isConditional",
                    "",
                    "Statement must belong to another statement, " +
                    "function or module!" 
                    + "\n Statement: " + getReferenceString());
            /* for compiler only , CRITICAL_ERROR will stop the tool before */
            return false;
    	} else if (getParent() instanceof FunctionBody) {
        	/* statement is not conditional if we have reached FunctionBody */
    		return false;
    	} else if (getParent() instanceof IfStatement
    			|| getParent() instanceof WhenCase
    			|| getParent() instanceof DefaultCase
    			|| getParent() instanceof LoopStatement) {
        	/* statement is conditional if it its parent is IfStatement,
        	 * CaseStatemnt or LoopStatement
        	 */
    		return true;
    	} else {
    		/* if we are here, the parent can be only Statement, recurse up 
    		 * and ask if it is  conditional*/
    		return ((Statement) getParent()).isConditional();
    	}
    	
    }

    /**
     * 
     * @return false for all statements except:
     *      Compound-, If-, For-, CaseStatement, DefaultCase, WhenCase
     */
    public boolean containsStmts() {
        return false;
    }

    @Override
    public String toString() {
        String lblPart = "";
        if (getLabel() != null) {
            lblPart = " lbl:" + "\"" + getLabel().getName() + "\"";
        }
        return super.toString() + lblPart;
    }

    /**
     * @return the block of Statements starting with the current one and going 
     * up to the end of a CompoundStatement or a FunctionBody. 
     * 
     * Note: Currently, the method works with Statements that are children of 
     * either a CompoundStatement or a SequentialFunctionBody. It would be nicer to 
     * make it work generically for Lists, but this cannot be efficiently done now, 
     * since the Statement has no link to the List object.   
     */
    public List<Statement> getStartingCodeBlock() {
        List<Statement> lst; 
        if (getParent() instanceof CompoundStatement) {
            lst = ((CompoundStatement) getParent()).getStatements();
        } else if (getParent() instanceof SequentialFunctionBody) {
            lst = ((SequentialFunctionBody) getParent()).getStatements();
        } else {
            String msg = "Unsupported type of parent object."
                + "\nStatement: " + this.getReferenceString(); 
            if (parent == null) {
                msg += "\nParent: " + parent;
            } else {
                msg += "\nParent: " + parent.getReferenceString();
            }
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    getClass().getSimpleName() 
                    + ".getStartingCodeBlock()", "", msg);
            return null;            
        }
        int pos = lst.indexOf(this);
        if (pos < 0) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    getClass().getSimpleName() 
                    + ".getStartingCodeBlock()", "", 
                    "Parent-child inconsistency. Element: " + this.getReferenceString());
            return null;            
        }
        return lst.subList(pos, lst.size());
    }
    
    /**
     * @param stmts - list of statements (not null, but can be empty)
     * @return true, when the supplied Statements have no computational effect.
     */
    public static boolean isEffectFree(List<Statement> stmts) {
        for (Statement s : stmts) {
            if (!s.isEffectFree()) {
                return false;
            }
        }    
        return true;
    }

    /**
     * @return true, when the current Statement has no computational effect.
     * Currently the only such case is BlankStatement.
     */
    public boolean isEffectFree() {
        return false;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }
    
    /**
     * sets sequence number to the statement and its children statement(s)
     * 
     * @param number sequence number to be set for the current statement
     * 
     * @return sequence number of current statement or current statement's children 
     * with maximum value
     */
    public int setSequenceNumber(int number) {
        this.sequenceNumber = number;
        return number;
    }

    /**
     * Replaces statement with another statement
     * or list of statements. Copies label and 
     * annotation of old statement to new one
     * (first statement in case newElement is list).
     */
    @SuppressWarnings("unchecked")
	@Override
    public void replaceMe(Object newElement) {
        super.replaceMe(newElement);
        Statement statement = null;
        if (newElement instanceof Statement) {
            statement = (Statement) newElement;
        } else if (newElement instanceof List) {
            List<Statement> list = (List<Statement>) newElement;
            if (list != null && !list.isEmpty() 
                    && list.get(0) instanceof Statement) {
                statement = (Statement) list.get(0);
            }
        }
        if (statement != null) {
            if (getLabel() != null) {
                statement.setLabel((Label) getLabel().getCopy());
            }
            if (statement != this) {
                statement.copyAnnotationFromStatement(this);
            }
        }
    }
    
    /**
     * Defines if statement contains of at least one not empty statement
     * 
     * @return true if statement contains at least one not empty statement
     */
    public boolean isEmptyStatement() {
        return false;
    }
    
    /**
     * 
     * @return All annotations of statement where 
     * isBlockDescription = true
     */
    public List<Annotation> getBlockDescriptions() {
        List<Annotation> result = new ArrayList<Annotation>();
        for(Annotation a: annotations) {
            if (a.isBlockDescription()) {
                result.add(a);
            }
        }
        return result;
    }
    
    /**
     * 
     * @return All block description annotations of 
     * nearest parent statement that has them
     */
    public List<Annotation> getBlockDescriptionsFromParent() {
        List<Annotation> result = new ArrayList<Annotation>();
        GAModelElement elt = getParent();
        while (elt instanceof Statement) {
            result = ((Statement) elt).getBlockDescriptions();
            if (!result.isEmpty()) {
                break;
            } else {
                elt = elt.getParent();
            }
        }
        return result;
    }
    
    /**
     * 
     * @return first atomic compound statement parent 
     * of current statement if it exists, null otherwise
     */
    public Statement getAtomicStParent() {
        GAModelElement parent = getParent();
        if (parent instanceof Statement) {
            if ((parent instanceof CompoundStatement) 
                    && (((CompoundStatement) parent).isAtomic())) {
                return (CompoundStatement) parent;
            } else {
                return ((Statement) parent).getAtomicStParent(); 
            }
        } else {
            return null;
        }
    }
}