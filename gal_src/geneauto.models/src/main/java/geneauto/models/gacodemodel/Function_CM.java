/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gacodemodel/Function_CM.java,v $
 *  @version	$Revision: 1.52 $
 *	@date		$Date: 2011-11-29 15:52:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gacodemodel;

import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.gaenumtypes.FunctionStyle;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract entity representing a function. Function description contains
 * information about its signature (name, return value, arguments) and body.
 * Three related code sections can be generated from the function object:
 * <ul>
 * <li><b>Function declaration</b> or function prototype to be placed either in
 * *. h file (in case of exported function) or in *.c file (in case of local
 * function).</li>
 * <li><b>Function definition</b> - full function definition including the
 * signature and body</li>
 * <li><b>Function instance </b>or function call to be used to execute the
 * function.</li>
 * </ul>
 * Function declaration and function definition can be compiled solely from the
 * information contained in the Function object. To compile a function instance,
 * a list of function arguments is required to link the function call to the
 * rest of the code.
 */
public class Function_CM extends NameSpaceElement_CM implements CodeModelFunction, HasNameSpace {

    /**
     * Top level NameSpace. Contains only arguments to the function and is 
     * logically above the function body.
     * 
     * TODO (to AnTo) Update argument setters and getParentingNameSpace for
     * FunctionArguments
     */
    protected NameSpace nameSpace;
    
    /**
     * List of function arguments. The list may be empty in case the function
     * does not have any arguments.
     */
    protected List<FunctionArgument_CM> arguments = new ArrayList<FunctionArgument_CM>();
    /**
     * Body of the function. Function body may be empty when function scope is
     * "Imported"
     */
    protected FunctionBody body;

    /**
     * data type of the function
     */
    protected GADataType dataType;

    /**
     * Specifies how the function is to be represented.
     */
    protected FunctionStyle functionStyle = FunctionStyle.FUNCTION;

    public Function_CM() {
        super();
    	scope = DefinitionScope.EXPORTED;
    }

    /**
     * @param body
     */
    public Function_CM(FunctionBody body) {
        this();
        setBody(body);
    }

    public Function_CM(List<FunctionArgument_CM> arguments, FunctionBody body,
            GADataType dataType, DefinitionScope scope,
            FunctionStyle functionStyle) {
        this();
        setArguments(arguments);
        setBody(body);
        setDataType(dataType);
        setScope(scope);
        setFunctionStyle(functionStyle);
    }

    public DefinitionScope getScope() {
        return scope;
    }

    /**
     * Getter with lazy initialisation to SequentialFunctionBody, if body is
     * null. Currently some parts of the CodeModel generator rely on that.
     */
    public FunctionBody getBody() {
        if (body == null) {
            body = new SequentialFunctionBody();
        }
        return body;
    }

    public List<FunctionArgument_CM> getArguments() {
        return arguments;
    }

    /**
     * Adds argument to the arguments list and sets parent of the argument to
     * current element 
     * 
     * @param argument
     */
    public void addArgument(FunctionArgument_CM argument) {
        arguments.add(argument);
        getNameSpace().addElement(argument);
    }

    public void removeArgument(FunctionArgument_CM funArgument) {
        arguments.remove(funArgument);
        getNameSpace().removeElement(funArgument);
    }

    /**
     * Sets the data type. If the supplied type object does not have a parent
     * yet, then the same instance is taken and made a child of the current
     * object. Otherwise, a copy of the supplied object is made.
     */
    public void setDataType(GADataType dataType) {
        if (dataType != null){
            if (dataType.getParent() == null) {
                this.dataType = dataType;
            } else {
                this.dataType = dataType.getCopy();
            }
            this.dataType.setParent(this);
        }
        else
            this.dataType = null;
    }

    public GADataType getDataType() {
        return dataType;
    }

    public void setArguments(List<FunctionArgument_CM> arguments) {
        // Update arguments
        this.arguments.clear();
        if (arguments != null) {
            for (FunctionArgument_CM fa : arguments) {
                addArgument(fa);
            }
        }
        // Update namespace
        this.getNameSpace().clear();
        if (arguments != null) {
            for (FunctionArgument_CM fa : arguments) {
            	this.getNameSpace().addElement(fa);
            }
        }
    }

    public void setBody(FunctionBody body) {
        if (body != null) {
            body.setParent(this);
        }
        this.body = body;
    }

    public void setScope(DefinitionScope scope) {
        this.scope = scope;
    }

    /**
     * @return an output variable, if existing.
     */
    public Variable_CM getOutputVariable() {
        if (body == null) {
            return null;
        } else {
            return body.getOutputVariable();
        }
    }
    
    public FunctionStyle getFunctionStyle() {
        return functionStyle;
    }

    public void setFunctionStyle(FunctionStyle functionStyle) {
        this.functionStyle = functionStyle;
    }

    public NameSpace getNameSpace() {
        // Lazy initialisation used, because we don't want to add this NameSpace
        // explicitly and we want it always existing, when required.
        if (this.nameSpace == null) {
            setNameSpace(new NameSpace());
        }
        return nameSpace;
    }

    public void setNameSpace(NameSpace nameSpace) {
        nameSpace.setParent(this);
        this.nameSpace = nameSpace;
    }
    
    public String instanceToString(List<Expression> args, CallExpression callExp) {
            String argList =compileArgumentList(args);
            return name + "(" + argList + ")";
    }

    /**
     * Takes list of expressions representing function argumetns as argument and
     * returns comma-separated list of argumetns for a function
     * 
     * @param args
     * @return
     */
    public static String compileArgumentList(List<Expression> args) {
        String argList = "";
        int counter = 0;
        for (Expression exp : args) {
            counter++;
            argList += exp.printElement();
            if (counter < args.size()) {
                argList += ", ";
            }
        }
        return argList;
    }

    /**
     * Returns a function argument with the given name
     *  
     * @param name -- name of hte sought argument
     * @return FunctionArgument_CM
     */
    public FunctionArgument_CM getArgumentByName(String name) {
    	for (FunctionArgument_CM arg : this.getArguments()) {
    		if (arg.getName().equals(name)) {
    			return arg;
    		}
    	}
    	
    	return null;
    }
    
    /**
     * Function that tests whether one function depends on another.
     * 
	 * Functions are interdependent, if one function calls another. Normally, 
	 * this should not be mutual.
	 * 
	 * return  1 when the current function calls the other function,
	 *        -1 when the other function calls the current function,
	 * 		   0 when the two functions are independent
	 * 
	 * TODO The functionality is currently not implemented, as it is not known,
	 * if this function will be really required. AnTo 12.07.2011 
	 */
	public int dependsOf(Function_CM fun2) {
    	
    	// Independent - Dummy
    	return 0;
    		
	}

	/**
	 * This overriding compensates the fact the Function's NameSpace is not
	 * directly in the NameSpace hierarchy.
	 */
	@Override
    public List<GAModelElement> getReferableChildren() {
        List<GAModelElement> lst = super.getReferableChildren();
        lst.addAll(getArguments());
        return lst;
    }
	
}