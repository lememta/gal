/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gablocklibrary/GABlockLibrary.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2010-06-04 16:58:53 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gablocklibrary;

import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * GABlockLibrary language is a simple configuration language for describing 
 * GASystemModel block types. The main term of the language is BlockType. 
 * There should be one BlockType entry for each block type that can be handled 
 * by Gene-Auto toolset. 
 * 
 * If block type is not defined in Block library, the block is considered as 
 * GenericBlokc. Models containing generic blocks can not be used in code 
 * generation. 
 * 
 * BlockType is identified by name (Gain, Const,  Product, Div ...). 
 * For code generator the block types are categorised into BlockClasses using 
 * the "type" attribute. In configuration file, it is suggested to group the 
 * classes and separate them by comment line. 
 * 
 * The semantics of a BlockType are defined by LibraryHandler that gives 
 * reference to typer and backend class that correspondingly check consistency 
 * of port types and generate code for a block of that type. 
 * In addition the BlockType can define functionSignature and headerFilename to 
 * refer to a function defined in an external library.
 */
public class GABlockLibrary extends Model {

	/**
	 * List of top level model elements (those appearing under the "model" node in XML
	 * file). The elements in this list must be unique -- no two elements with the
	 * same type and name attribute value should appear in the list. ModelFactory must
	 * guarantee, that in case two elements with the same name are found in the same
	 * model file an error is thrown. In case of to overlapping elements in different
	 * models the last one read overrides previous element and warning is issued
	 * 
	 * NOTE! When the type of this collection is changed at least the following other 
	 * methods need to be updated also: 
	 * 	addElement(Object), addElement(<ElementType>), 
	 *  addElements(..), setElements(..), getElements(..) 
	 */	
    protected List<BlockType> elements = new ArrayList<BlockType>();
    
	/**
	 * We do not use dependent models in code model
	 * 
	 * (non-Javadoc)
	 * @see geneauto.models.genericmodel.Model#getDependentModel()
	 */
    @Override
	public Model getDependentModel() {
		return null;
	}
    
    /**
     * Checks, if the id of given object is greater than lastId 
     * and the id is still free
     *  
     * If it is then updates the lastId() 
     * @param id
     */
    @Override
    public void updateLastId(GAModelElement reqObj) {
        int id = reqObj.getId();
        
        if (reqObj.getModel() != this){
            reqObj.setModel(this);
        }
        
        if (id < 1){
            // an object does not have ID yet. Assign one
            if (!noNewId){
                reqObj.setModel(this, true);
            }
            // we do not update the map in case of illegal ID
            // when new id was assigned, it is already registered in map 
            // so we can safely return
            return;
        }
        
        // check if id was in the map
        GAModelElement obj = idMap.get(id + "");
        if (obj == null){
            // given id was free
            if (id > lastId) {
                lastId = id;
            }   
            idMap.put(id + "", reqObj);
        } else if (obj != reqObj){
            // duplicate ID, raise error
            obj.forceId(this.getNewId(obj));
        }
        else {
            // the same object was registered twice. This is OK
        }
    }

	/**
	 * Elements of block library are always retrieved by name
	 * make a map to find elements effectively
	 */
	public Map <String, GAModelElement> nameMap = 
        new HashMap<String, GAModelElement> ();
	
	public Map <String, String> attributeMapping = 
        new HashMap<String, String> ();
    
	
    /**
     * Method getAttributeMapping.
     * @return the attribute "attributeMapping".
     */
    public Map<String, String> getAttributeMapping() {
        return attributeMapping;
    }

    public GABlockLibrary() {
		super();
	}

    public List<BlockType> getElements() {
        return elements;
    }
    
    @Override
    public void setElements(List<?> elements) {
    	this.elements.clear();
    	for (Object e : elements){
    		addElement((BlockType) e);
    	}
    }
    
    /**
     * Adds element to the elements list and as well to the name map
     */
	public void addElement(BlockType element) {
    	element.setParent(null);
    	element.setModel(this);
    	getElements().add(element);
		nameMap.put(element.getName(), element);
	}

	/**
	 * Generic addElement method overridden from the Model class.
	 * NOTE! This method must call the addElement method specific to this Model.
	 * If the type in the cast is wrong it will go to endless loop, as it will
	 * match the same method!
	 */
	@Override
    public void addElement(Object o) {
    	addElement((BlockType) o);    	
    }

	@Override
    public void addElements(List<?> p_elements) {
    	for (Object o : p_elements){
    		addElement((BlockType) o);
    	}
    }

	/**
	 * fetches element from name map
	 */
	@Override
	public GAModelElement getElementByName(String name) {
		return nameMap.get(name);
	}
	
	/**
	 * searches for BlockType by name and removes it from the 
	 * model. Here we use special method instead of removeMe 
	 * as the element needs to be removed both from model and from
	 * the nameMaap. Also it allows more optimised processing 
	 * 
	 * @param name		name of the block type to be removed 
	 */
	public void removeBlockTypeByName(String name) {
		BlockType blockType = getBlockTypeByName(name);
		if (blockType != null) {
			// remove the element from model
			elements.remove(blockType);
			// reset the parent pointer
			blockType.setParent(null);
			// remove the element from nameMap
			nameMap.remove(name);
		}
	}
    
	/**
	 * returns a block type from name map
	 * 
	 * @param name		name of the sought block type
	 * @return			object of type BlockType, null if object not found
	 */
    public BlockType getBlockTypeByName(String name){
    	GAModelElement el = getElementByName(name);
    	if (el instanceof BlockType){
    		return (BlockType) el;
    	}
    	
    	return null;
    }

}