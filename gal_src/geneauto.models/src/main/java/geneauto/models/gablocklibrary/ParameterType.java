/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gablocklibrary/ParameterType.java,v $
 *  @version	$Revision: 1.10 $
 *	@date		$Date: 2011-09-01 09:07:25 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gablocklibrary;

import geneauto.models.genericmodel.GAModelElement;

import java.util.List;



/**
 *
 */
public class ParameterType extends GAModelElement{

	private boolean evaluateEML;
	
    /**
     * Full normalisation or not. Only applicable, when evaluateEML == true. If
     * this is also true, then the parsed expression is also fully normalised
     * (important for matrixes and vectors). Otherwise it is only partially
     * normalised (singleton arrays are converted to scalars)
     * 
     */
    private boolean fullNormalization;
    
    private boolean parseEML;
    private String editorMapping;
    
    /**
     * Certain blocks in allow referencing to its inputs outputs or parameters
     * by a name with pattern u<no>, y<no> or p<no> respectively.
     * Setting this flag to true tells the parser that such referencing is 
     * allowed in current parameter 
     */
    private boolean supportSpecialReferences;
	
    /**
     * List of mappins of this parameter in different input languages
     */
    private List<InputLanguageMapping> inputLanguageMappings;

	public ParameterType() {
	
	}

	public List<InputLanguageMapping> getInputLanguageMappings() {
		return inputLanguageMappings;
	}

	public void setInputLanguageMappings(
			List<InputLanguageMapping> inputLanguageMappings) {
		this.inputLanguageMappings = inputLanguageMappings;
	}
	
	public void addInputLanguageMapping(InputLanguageMapping mapping) {
		this.inputLanguageMappings.add(mapping);
	}

	/**
     * @return the attribute "editorMapping".
     */
    public String getEditorMapping() {
        return editorMapping;
    }

	/**
	 * @return the attribute "evaluateEML".
	 */
	public boolean isEvaluateEML() {
		return evaluateEML;
	}

	/**
	 * @return the attribute "name".
	 */
	public String getName() {
		return name;
	}

    /**
     * @return the attribute "parseEML".
     */
    public boolean isParseEML() {
        return parseEML;
    }

    /**
     * @return the attribute "fullNormalization".
     */
    public boolean isFullNormalization() {
        return fullNormalization;
    }
    
    /**
	 * Method isSupportSpecialReferences.
	 * @return the attribute "supportSpecialReferences".
	 */
	public boolean isSupportSpecialReferences() {
		return supportSpecialReferences;
	}

	/**
	 * Method setSupportSpecialReferences.
	 * @param supportSpecialReferences the "supportSpecialReferences" to set.
	 */
	public void setSupportSpecialReferences(boolean supportSpecialReferences) {
		this.supportSpecialReferences = supportSpecialReferences;
	}

}