/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gablocklibrary/CombinatorialBackend.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gablocklibrary;

import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;

import java.util.List;
import java.util.Map;

/**
 * General class for combinatorial blocks backends
 * 
 *
 */
public interface CombinatorialBackend extends BlockBackend {

	/**
	 * Return computation code (functional handling done by the block)
	 * 
	 * @param block
	 *            block for which is called the backend
	 * @param inputs
	 *            list of the input variables of the block
	 * @param outputs
	 *            list of the output variables of the block
	 * @param controls
	 *            list of the input control variables of the block
	 * @param memory
	 *            memory variable of the block
	 * @return GACodeModel fragment describing block behaviour
	 */
	public List<GAModelElement> getComputationCode(Block block,
			List<Expression> inputs, List<Expression> outputs,
			List<Expression> controls);

	/**
	 * Return declaration code (global variable or statement that need to be
	 * declared before block computation code)
	 * 
	 * @param block
	 *            block for which is called the backend
	 * @param map 
	 * @return GACodeModel fragment describing declarations involve by block
	 */
	public List<GAModelElement> getDeclarationCode(Block block, Map<String, GAModelElement> map);
	
	   /**
     * Return declaration code (global variable or statement that need to be
     * declared before block computation code)
     * 
     * @param block
     *            block for which is called the backend
     * @return GACodeModel fragment describing declarations involve by block
     */
    public List<GAModelElement> getInitCode(Block block);

}
