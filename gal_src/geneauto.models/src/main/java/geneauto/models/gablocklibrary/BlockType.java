/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/gablocklibrary/BlockType.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-09-01 09:07:24 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.gablocklibrary;

import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a block for GASystemModel.
 * 
 * 
 */
public class BlockType extends GAModelElement {
	protected String type;

	protected List<ParameterType> parameterTypes = new ArrayList<ParameterType>();

	/**
	 * Indicates, that backend returns single-instruction expression that can be
	 * embedded into other expressions in optimisation. For instance two
	 * subsequent gain blocks (one multiplying input with 2 and the other with 3
	 * can be merged into one expression o2 = i1*2*3
	 * 
	 */
	protected boolean singleInstruction;

	/**
	 * TODO: To be decided if this is a parameter of block or port 
	 * WHY IS IT IN BLOCK LIBRARY?
	 */
	protected boolean directFeedThrough;

	/**
	 * A reference to the LibraryHandler object defining backend and typer
	 * classes 
	 */
	protected LibraryHandler libraryHandler;

	/**
	 * BlockClass determines the category of block in code generation
	 */
	protected BlockClass blockClass;

    /**
     * In case of legacy functions, allow to know the headerFile 
     * in which the function is located. 
     */
    protected String headerFileName; 

    /**
     * In case of legacy functions determines the function name,
     * type and arguments 
     */
    protected String functionSignature;
    
    /**
     * Priority of the preprocessor. 
     * If it is 0 => there is no preprocessor 
     * associated with current block type
     */
    protected int preprocessorPriority;
        
	public BlockType() {
		super();
	}

	/**
	 * Method getBlockClass.
	 * @return the attribute "blockClass".
	 */
	public BlockClass getBlockClass() {
		return blockClass;
	}

	public String getFunctionSignature() {
		return functionSignature;
	}

	/**
	 * Method isDirectFeedThrough.
	 * @return the attribute "directFeedThrough".
	 */
	public boolean isDirectFeedThrough() {
		return directFeedThrough;
	}

	/**
	 * Method getLibraryHandler.
	 * @return the attribute "libraryHandler".
	 */
	public LibraryHandler getLibraryHandler() {
		return libraryHandler;
	}

	/**
	 * Method getParameterTypes.
	 * @return the attribute "parameterTypes".
	 */
	public List<ParameterType> getParameterTypes() {
		return parameterTypes;
	}
	
	/**
     * Return the ParameterType corresponding to the given name
     * @return the attribute "parameterTypes".
     */
    public ParameterType getParameterTypeByName(String name) {
        for(ParameterType ptype : parameterTypes){
            if(ptype.getName().equals(name)){
                return ptype;
            }
        }
        return null;
    }
    
	/**
	 * Method isSingleInstruction.
	 * @return the attribute "singleInstruction".
	 */
	public boolean isSingleInstruction() {
		return singleInstruction;
	}

	/**
	 * Method getType.
	 * @return the attribute "type".
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Method which, given a type string, computes the block class. 
	 * 
	 * TODO AnTo 141209 The result of this method is actually unused - check, 
	 * if it can be removed?
	 */
	public void computeBlockClass (String p_type) {
		if (p_type.equals("SourceBlock")) {
			blockClass = BlockClass.SOURCE;
		}
		if (p_type.equals("SinkBlock")) {
			blockClass = BlockClass.SINK;
		}
		if (p_type.equals("CombinatorialBlock")) {
			blockClass = BlockClass.COMBINATORIAL;
		}
		if (p_type.equals("ControlBlock")) {
			blockClass = BlockClass.CONTROL;
		}
		if (p_type.equals("SequentialBlock")) {
			blockClass = BlockClass.SEQUENTIAL;
		}
		if (p_type.equals("RoutingBlock")) {
			blockClass = BlockClass.ROUTING;
		}
		if (p_type.equals("ObservationBlock")) {
			blockClass = BlockClass.OBSERVATION;
		}
		if (p_type.equals("GenericBlock")) {
			blockClass = BlockClass.GENERIC;
		}
		if (p_type.equals("StateFlowBlock")) {
			blockClass = BlockClass.STATEFLOW;
		}
		if (p_type.equals("System")) {
			blockClass = BlockClass.SYSTEM;
		}
		if (p_type.equals("ReferenceBlock")) {
			blockClass = BlockClass.REFERENCE;
		}
	}

    
    /**
     * Method getHeaderFileName.
     * @return the attribute "headerFileName".
     */
    public String getHeaderFileName() {
        return headerFileName;
    }

    public int getPreprocessorPriority() {
        return preprocessorPriority;
    }
}
