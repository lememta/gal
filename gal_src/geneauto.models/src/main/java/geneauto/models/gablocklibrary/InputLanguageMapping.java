package geneauto.models.gablocklibrary;

import geneauto.models.genericmodel.GAModelElement;

public class InputLanguageMapping extends GAModelElement {
	protected String language;
	
	protected String version;
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getVersionStr() {
		return version;
	}
	
	public Double getVersion() {
		try {
			return Double.parseDouble(version);
		} catch (Exception e) {
			return null;
		}
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	@Override
	public String toString() {
		return "name: " + name 
		+ "\nversion: " + version
		+ "\nlanguage: " + language;
	}
}
