/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/emlconverter/EMLAccessor.java,v $
 *  @version	$Revision: 1.74 $
 *	@date		$Date: 2012-02-20 09:16:51 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities.emlconverter;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.expression.TernaryExpression;
import geneauto.models.gacodemodel.expression.TrueExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.utilities.emlconverter.antlr.EMLExpressionsLexer;
import geneauto.models.utilities.emlconverter.antlr.EMLExpressionsParser;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;

public class EMLAccessor {

    /**
     * Parses the input file into a tree (AST)
     * 
     * @param str
     *            the String to parse
     * @throws Exception
     *             input file is missing
     */
    private final static EMLExpressionsParser getParserEML(String str) throws Exception {
        // Puts the String in a InputStream instance
        /*
         * 17.12.2008 Parser grammar modified so no embracing parenthesis is
         * needed any more StringBufferInputStream inputStream = new
         * StringBufferInputStream( '(' + str + ')');
         */
        ByteArrayInputStream inputStream = new ByteArrayInputStream(str
                .getBytes());
        // Creates ANTLRInputStream instance
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        // Instantiates the lexer
        EMLExpressionsLexer lexer = new EMLExpressionsLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        // Instantiates the parser
        EMLExpressionsParser parser = new EMLExpressionsParser(tokens);
        return parser;
    }

    /**
     * Convert an EML String into a code model Expression instance.
     * 
     * NOTE: This method assumes that the string to parse represents something 
     * else than a string, e.g. a number "3.145", list of numbers "1, 2, 3", etc.
     * If the string value is known to be of type string (instance of TString) 
     * it should be directly converted to a StringExpression. This approach is 
     * not sufficient for arrays of string or general string manipulating. 
     * TODO This method should be replaced by a proper grammar based parser. This 
     * would clarify the structure and e.g. allow handling of lists of strings 
     * (AnTo 101111)
     * 
     * @param s
     *            the String to parse
     * 
     * @param fullNormalization
     *            Specifies, if the normalisation applied to the resulting
     *            expression is full or partial. Only applicable to
     *            ListExpressions For the meaning of it see the GADataType and
     *            Expression classes.
     * @return the corresponding Expression or null, when parsing failed. NOTE:
     *         We only return null, when parsing failed. For "[]" we return an
     *         empty ListExpression.
     */
    public final static Expression convertEMLToCodeModel(String s,
            boolean fullNormalization) {
        Expression result = null;
        boolean isList = s.contains(",");
        
        try {
            // Initialises the antlr parser and retrieves the instance
            if (isList) {
                result = new GeneralListExpression();
                Expression currentExpr = null;
                
                /*
                 * If parsed string contains both commas and
                 * angle brackets - parse the whole string, 
                 * otherwise split it by commas
                 */
                if (s.contains("[") || s.contains("]")) {
                    result = convertEMLToExpression(s);
                } else {
                    String[] elements = s.split(",");
                    for (String currentElt : elements) {
                        currentExpr = convertEMLToExpression(currentElt);
                        ((GeneralListExpression) result).addExpression(currentExpr);
                    }
                }
            } else if (s.equals("[]")) {
                // [] corresponds to the simulink default value
                // Return a dummy ListExpression
                // NOTE: returning null means that there was an error in
                // parsing,
                // this is not the case here!
                return new GeneralListExpression();

            } else {
                result = convertEMLToExpression(s);
            }

        } catch (Exception e) {
            /*
             * EventsHandler.handle(EventLevel.ERROR, "EMLParser", "", "Missing
             * input stream.", "");
             */
            EventHandler.handle(EventLevel.ERROR, "convertEMLToCodeModel()",
                    "", "Error parsing eml expression : " + s, e);
            return null;
        }

        // Normalisation
        if (result instanceof GeneralListExpression) {
            if (fullNormalization) {
                result = result.normalizeShape(true);
            } else {
                result = result.normalizeShape(false);
            }
        }

        return result;
    }

    /**
     * Converts String to Expression
     * 
     * @param s String to be parsed
     * @return Expression that corresponds to given string s
     * @throws Exception
     */
	private static Expression convertEMLToExpression(String s)
			throws Exception {
		Expression result = null;
		EMLExpressionsParser emlParser;
		emlParser = getParserEML(s);

		try {
		    // Parses the input String and put the result tree
		    // in the modelTree attribute
		    CommonTree tree = ((CommonTree) emlParser.expr().getTree());
		    if (tree == null) {
		        return null;
		    }
		    
		    result = EMLConverter.transformExpression(tree, null);
		    
		    result = processExpression(result, s);		    
		} catch (Exception e) {
		    throw e;
		}
		return result;
	}

    private static Expression processExpression(Expression expr, String s) {

        Expression result = expr;

        // Go through expression tree and process variable names to create
        // corresponding structure when expected
        result = processVariableName(result);

        // replace literal expression with name "true" or "false" by
        // corresponding dedicated expressions
        if (result instanceof VariableExpression) {
            if (result.getName() != null && result.getName().equals("true")) {
                result = new TrueExpression();
            } else if (result.getName() != null
                    && result.getName().equals("false")) {
                result = new FalseExpression();
            }
        }

        processBinaryDiv(result);

        return result;
    }

    /**
     * 
     * @param expr
     */
    private static void processBinaryDiv(Expression expr) {

        if (expr.containsBinaryDiv()) {
            for (BinaryExpression bin : expr.getBinaryDiv()) {
                if (bin.getLeftArgument() instanceof IntegerExpression) {

                    bin.setLeftArgument(new DoubleExpression(
                            ((IntegerExpression) bin.getLeftArgument())
                                    .getLitValue()));

                } else if (bin.getLeftArgument().getDataType() instanceof TRealInteger) {

                    bin.getLeftArgument().setDataType(new TRealDouble());

                } else if ((bin.getLeftArgument().getDataType() instanceof TArray)
                        && ((TArray) bin.getLeftArgument().getDataType())
                                .getBaseType() instanceof TRealInteger) {

                    ((TArray) bin.getLeftArgument().getDataType())
                            .setBaseType(new TRealDouble());

                }
            }
        }

    }

    /**
     * 
     * @param expr
     */
    private static Expression processVariableName(Expression expr) {

        Expression result = expr;

        // if expr is a VariableExpression, it's transformed into a
        // MemberExpression
        if (expr instanceof VariableExpression) {

            if (expr.getName().contains(".")) {

                result = new MemberExpression();
                // setting member name (reference will be set by name resolving)
                result.setName(expr.getName());
            }
        }
        // else, recursive process has to be done if expr contains other
        // sub-expressions
        else if (expr instanceof BinaryExpression) {
            ((BinaryExpression) expr)
                    .setLeftArgument(processVariableName(((BinaryExpression) expr)
                            .getLeftArgument()));
            ((BinaryExpression) expr)
                    .setRightArgument(processVariableName(((BinaryExpression) expr)
                            .getRightArgument()));
        } else if (expr instanceof GeneralListExpression) {
            result = new GeneralListExpression();
            result.setName(expr.getName());
            result.setDataType(expr.getDataType());
            for (Expression e : ((GeneralListExpression) expr).getExpressions()) {
                ((GeneralListExpression) result).addExpression(processVariableName(e));
            }
        } else if (expr instanceof TernaryExpression) {
            ((TernaryExpression) expr)
                    .setFirst(processVariableName(((TernaryExpression) expr)
                            .getFirst()));
            ((TernaryExpression) expr)
                    .setSecond(processVariableName(((TernaryExpression) expr)
                            .getSecond()));
            ((TernaryExpression) expr)
                    .setThird(processVariableName(((TernaryExpression) expr)
                            .getThird()));
        } else if (expr instanceof UnaryExpression) {
            ((UnaryExpression) expr).setArgument(((UnaryExpression) expr)
                    .getArgument());
        } else if (expr instanceof CallExpression) {
            result = new CallExpression();
            result.setName(expr.getName());
            result.setDataType(expr.getDataType());
            for (Expression e : ((CallExpression) expr).getArguments()) {
                ((CallExpression) result).addArgument(processVariableName(e));
            }
        }

        return result;
    }

	/**
	 * A simple parser for simple one-dimensional Matlab cell expressions.
	 * 
	 * One-dimensional means that they don't contain ";". The function 
	 * can be simply extended to multidimensional case, by e.g. adding 
	 * a new top level function that splits by ";" and calls this one for 
	 * each row. 
	 * 
	 * In this function we are considering the kinds of expressions specified 
	 * below. "ei" is an elementary expression, i.e. non-list. Currently, 
	 * these can only be continous number or character sequences (variables or 
	 * literals).
	 * 
	 * The return type for such expressions is *always* a List of Lists 
	 * (i.e. 2-dimensional). Examples:
	 *  
	 * 		{e1  e2  e3} 
	 * 		{e1, e2, e3} 
	 * 		{e1,e2,e3} 
	 * 		{[e1], [e2], [e3]}
	 * 		{[e1], [e2, e3]  }
	 * 		{e1,   [e2, e3]  }
	 * 		{e1,   [e2 e3]   }
	 * 		{e1    [e2 e3]   } 
	 * 
	 * TODO Implement in a grammar based parser instead and add multi-dimension 
	 * support (AnTo 120209).
	 * 
	 * @param s string to parse
	 * @return
	 */
	public static ListExpression parseCellExpression1D(String s) {
		// Check, if the argument is one-dimensional. 
		// Terminating semicolon at the end of line is also rejected
		// This would be easy to add.
		ListExpression exp = null;
		boolean failed = false;
		if (s.contains(";")) {
			failed = true;
		}
		if (!failed) {
			exp  = parseCellExpression1DAux(s);
			failed = exp == null;
		}
		if (failed) {
			EventHandler.handle(EventLevel.ERROR, "", "", 
					"Invalid or unsupported one dimensional cell expression: " + s);
		}
		return exp;
	}

	/**
	 * Helper function for parseCellExpression1D
	 * @param s
	 * @return
	 */
	private static ListExpression parseCellExpression1DAux(String s) {
		GeneralListExpression rootExp;

		String t = s.trim();
		
		if (!(t.startsWith("{") && t.endsWith("}"))) {
			return null;
		}		
		t = t.substring(1, t.length()-1);
		t = t.trim();

		// Unify separators
		// Comma surrounded by whitespace or non-empty whitespace are treated as 
		// element separators. For general expressions this is not correct, but 
		// it is correct regarding the constraints to elementary expressions 
		// defined above.
		t = t.replaceAll("(\\s*,\\s*)|(\\s+)", ",");
		
		List<String> subExps = new ArrayList<String>();
		int i; 
		String u; // Subexpression to add
		while (t.length() > 0) {
			if (t.startsWith("[")) {
				// Current subexpression is of kind [...]. Look for closing brace.
				i = t.indexOf("]");
				if (i == -1) {
					return null;
				}
				u = t.substring(0, i+1);
				t = t.substring(i+1);
			} else {
				// Current subexpression is a continuous unbraced string. Look
				// for next comma or end of string
				i = t.indexOf(",");
				if (i == -1) {
					u = t;
					t = "";
				} else {
					u = t.substring(0, i);					
					t = t.substring(i); // For simplicity keep the comma for now 
				}				
			}
			subExps.add(u);
			// Check tail. It must start with comma or be empty.
			if (t.startsWith(",")) {
				t = t.substring(1);
			} else {
				if (t.length() != 0) {
					return null;
				}
			}
		}
		
		// Convert the subexpression strings to code model Expressions
		rootExp = new GeneralListExpression();		
		for (String seStr : subExps) {
			Expression seExp = convertEMLToCodeModel(seStr, true);
			if (seExp == null) {
				return null; // There was some error converting the expression
			}
			// We expect the rootexpression to be a list of lists
			if (!(seExp instanceof ListExpression)) {
				// Embed the subexpression expression in a new ListExpression 
				List<Expression> le = new ArrayList<Expression>();
				le.add(seExp);
				seExp = new GeneralListExpression(le);
			}
			rootExp.addExpression(seExp);
		}
		
		return rootExp;
	}

}
