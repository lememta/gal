/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/FunctionPrototypeParserEML.java,v $
 *  @version	$Revision: 1.28 $
 *	@date		$Date: 2011-09-15 10:24:47 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TString;
import geneauto.models.gadatatypes.TVoid;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.emlconverter.EMLAccessor;
import geneauto.utilities.parser.FunctionSignatureParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses the function prototype retrieved from EML expression in format
 * [<return type> <return variable>] <function name>([<arg1 type> <arg1 name>,
 * <argN type> <argN name>])
 * 
 * <return variable> and <argX name> are in form {u|y|p}<no> where u - input
 * port y - output port p - parameter <no> - ordinal of the given type of
 * variables
 * 
 * 
 */
public class FunctionPrototypeParserEML extends FunctionSignatureParser {
    
    protected String getPattern() {
    	return "(?:(?:((?:\\w+\\s+)?[\\w\\[\\]]+)\\s*=\\s*)|(?:void\\s+))?([\\w]+)(?:\\((.*)\\))?";

    }
    
    private int resultPortNum = 0;
    private List<SignatureElement> outputTypes = new ArrayList<SignatureElement>();
    private List<SignatureElement> inputTypes = new ArrayList<SignatureElement>();
    private List<SignatureElement> paramTypes = new ArrayList<SignatureElement>();
    private List<SignatureElement> argumentTypes = new ArrayList<SignatureElement>();
    private Block block;
    private HashMap<Integer, Expression> parameters = new HashMap<Integer, Expression>();

    public FunctionPrototypeParserEML(String inputString, Block block) {
        super(inputString);
        this.block = block;
        addSignature(outputVar, 'y', true);
        if (!outputTypes.isEmpty())
            resultPortNum = outputTypes.get(0).getPortNumber();
        if (arguments != null) {
            for (String str : arguments) {
                if (this.addSignature(str, 'u', false)) {
                } else if (this.addSignature(str, 'p', false)) {
                } else if (this.addSignature(str, 'y', false)) {
                } else {
                	// TODO AnTo 101103 here was a comment "Add error" 
                	// Revise this method and this case. It seems to have obsolete branches
                	// at least in some cases, since strings like "double u1" end up 
                	// here, but they are not errors
                }
            }
        }
    }

    public List<SignatureElement> getOutputTypes() {
        return outputTypes;
    }

    public List<SignatureElement> getInputTypes() {
        return inputTypes;
    }

    public List<SignatureElement> getParamTypes() {
        return paramTypes;
    }

    public List<SignatureElement> getArgumentTypes() {
        return argumentTypes;
    }

    /**
     * adds
     * 
     * @param str
     *            String with information to parse (eg. "double u1")
     * @param c
     *            character defining the type of variable ('y' - output, 'u' -
     *            input, 'p' - parameter) function signature)
     * @param elType 
     * 
     * @return true if dataType of the variable was added to the List or was
     *         assigned to function signature
     */
    private boolean addSignature(String str, char c,
            SignatureElementType elType, boolean isFunOutPut) {
        if (str == null) {
            return true;
        }
        String patternString = "\\s*([\\w]+)\\s*" + c + "(\\d)+(\\[.*\\])*\\s*";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            Integer intValue;
            try {
                intValue = Integer.parseInt(matcher.group(2));
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            SignatureElement elt = new SignatureElement(getDataType(matcher
                    .group(3), matcher.group(1), str), intValue, elType);
            switch (elType) {
            case INPUT_VARIABLE:
            case OUTPUT_VARIABLE:
            case PARAMETER:
                switch (elType) {
                case INPUT_VARIABLE:
                    inputTypes.add(elt);
                    break;
                case OUTPUT_VARIABLE:
                    outputTypes.add(elt);
                    break;
                case PARAMETER:
                    paramTypes.add(elt);
                    break;
                }
                if (isFunOutPut) {
                    resultPortNum = intValue;
                } else {
                    argumentTypes.add(elt);
                }
                break;
            }
        }
        return false;
    }

    private boolean addSignature(String str, char c, boolean isFunOutPut) {
        SignatureElementType elType;

        if (c == 'u') {
            elType = SignatureElementType.INPUT_VARIABLE;
            return addSignature(str, c, elType, isFunOutPut);
        } else if (c == 'y') {
            elType = SignatureElementType.OUTPUT_VARIABLE;
            return addSignature(str, c, elType, isFunOutPut);
        } else if (c == 'p') {
            elType = SignatureElementType.PARAMETER;
            return addSignature(str, c, elType, isFunOutPut);
        } else {
            return false;
        }
    }

    
    /**
     * 
     * @param str
     *      String representing dimensions of variable (e.g. [][] or [2][2])
     * @param baseDT
     *      String representing base data type (e.g. double for double[])
     * @param parsedString 
     *      String with information to parse (eg. "double u1")
     *
     * @return dataType of the variable (e.g. for "double[] u1" returns TArray
     *         with base type TRealDouble)
     */
    private GADataType getDataType(String str, String baseDT, String parsedString) {
        GADataType dt = null;
       
        // TODO (to AnRo): why do we parse dimensions here separately
        // it seems that there is an appropriate method in DataTypeAccessor
        // already (ToNa 03/05/10)
        if (str != null) {
            String temp = str.substring(str.indexOf("["));
            List<Expression> dimList = getDimensions(temp, parsedString);
            TPrimitive baseType = DataTypeAccessor.parsePrimitiveType(baseDT);
            switch (dimList.size()) {
            case 0:
                break;
            case 1:
                dt = new TArray(dimList.get(0), baseType);
                break;
            case 2:
                dt = new TArray(dimList, baseType);
                break;
            default:
                dt = new TArray(dimList, baseType);
            }
        } else {
        	dt = DataTypeAccessor.parsePrimitiveType(baseDT, false);
        	if (dt == null) {
                EventHandler.handle(EventLevel.ERROR, getClass().getCanonicalName(), 
                		"",
                        "Undefined data type \""
                		+ baseDT 
                		+ "\" in expression \""
                		+ parsedString
                		+ "\"\n Block: "
                		+ block.getReferenceString(),"");

        	}
        }
        
        return dt;
    }

	/**
	 * Takes the parameter index as input and returns an expression with
	 * corresponding parameter value. The parameter is first sought by pattern
	 * "SParameter<paramIdx>". If not found, then by index.
	 * 
	 * @param paramIdx
	 * @param expectedType
	 *            Expected type of the Parameter. Currently, it is only used to 
	 *            determine whether the parameter is of type string or not.
	 *            If it isn't supplied, then we assume the parameter is not of type 
	 *            string.
	 * @return
	 */
    private Expression getParameterExpression(int paramIdx, GADataType expectedType) {
    	// first check if block parameter has s-function type of 
    	// indexing
    	// TODO: reconsider this -- here we encode simulink-specific 
    	// syntax in the metamodel. Find a way to solve this in the importer 
    	// (ToNa 03/05/10)
        Parameter param = block.getParameterByName("SParameter" + paramIdx);
        
        // s-function-style parameter not found, try by index
        if (param == null) {
        	param = block.getParameterByIndex(paramIdx);
        }
        
        // parameter still not found, return with error
        if (param == null) {
            EventHandler.handle(EventLevel.ERROR, 
            		getClass().getCanonicalName() + ".getParameterExpression", 
            		"",
                    "Parameter with index " 
            		+ paramIdx 
            		+ "was not found!\n Block: " 
            		+ block.getReferenceString(),"");
            return null;        	
        }
        
        Expression exp;
        // All parameter values are encoded as StringExpression at this point. We need different 
        // handling, based on whether the value is expected to be a string or not 
        if (expectedType instanceof TString) {
        	String value = param.getStringValue();
        	
        	// In case value is in form "<value>" remove quote marks
        	if (value != null && value.startsWith("\"") && value.endsWith("\"")) {
        		value = value.substring(1, value.length() - 1);
        	}
        	exp = new StringExpression(value); 
        } else {
        	 exp = param.getValue();
        }
        
        if (exp == null) {
            EventHandler.handle(EventLevel.ERROR, 
            		getClass().getCanonicalName() + ".getParameterExpression", 
            		"",
                    "Error parsing value of parameter " 
            		+ "P" + paramIdx + ": \"" 
            		+ param.getValue().getStringValue() + "\""
            		+"\n Block: " + block.getReferenceString(),"");
            return null;            
        } else { 
            return exp;
        }
    }

    /**
     * 
     * @param indexPart
     *            index part of the value (e.g. [3][2] or [][])
     * @return List with variable dimension expressions (if index part is like
     *         "[][]" - returns LiteralValues with empty parameter)
     */
    private List<Expression> getDimensions(String indexPart, String parsedString) {
        List<Expression> result = new ArrayList<Expression>();
        String patternString = "^(\\[\\w*\\])+$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(indexPart);
        // check that index part is similar to "[]...[]" or
        // "[number]...[number]"
        if (matcher.find()) {
            int index = -1;
            int index2 = -1;
            do {
                index = indexPart.indexOf("[", index + 1);
                index2 = indexPart.indexOf("]", index2 + 1);
                if (index2 != -1) {
                    String dimValue = indexPart.substring(index + 1, index2);
                    Expression exp = EMLAccessor.convertEMLToCodeModel(
                                dimValue, false);
                    VariableExpression varExp;
                    Expression expNew = null;
                    String name = "";
                    List<GAModelElement> varExps = exp
                            .getAllChildren(VariableExpression.class);
                    if (exp instanceof VariableExpression) {
                        varExps.add(exp);
                    }
                    for (GAModelElement elem : varExps) {
                        name = elem.getName();
                        patternString = "([p])(\\d+)";
                        pattern = Pattern.compile(patternString);
                        matcher = pattern.matcher(name);
                        if (matcher.find()) {
                            String s = matcher.group(1);
                            int portNum = Integer.parseInt(matcher.group(2));
                            if (s.equals("p")) {
                                expNew = getParameterExpression(portNum, null);
                                parameters.put(portNum, expNew);
                                varExp = (VariableExpression) elem;
                                // TODO not finished yet
                                if (expNew.canBeDimensionLength(true, parsedString)) {
                                    if (exp == varExp) {
                                        exp = expNew;
                                    } else {
                                        varExp.replaceMe(expNew);
                                    }
                                }
                            }
                        }
                    }
                    result.add(exp);
                }
            } while (index != -1);
        }
        return result;
    }

    /**
     * 
     * @param portNo
     *            number of the port
     * @return data type of the input port
     */
    public GADataType getInputPortType(int portNo) {
        for (SignatureElement info : getInputTypes()) {
            if (info.getPortNumber() == portNo) {
                return info.getDataType();
            }
        }
        return null;
    }

    /**
     * 
     * @param portNo
     *            number of the port
     * @return data type of the output port
     */
    public GADataType getOutputPortType(int portNo) {
        for (SignatureElement info : getOutputTypes()) {
            if (info.getPortNumber() == portNo) {
                return info.getDataType();
            }
        }
        return null;
    }

    /**
     * 
     * @return name of the function
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * 
     * @param outputs
     *            List with output expressions
     * @return variable or member expression from the outputs list
     */
    public Expression getResultExpression(List<Expression> outputs) {
        if (resultPortNum != 0) {
            if (outputs.size() >= resultPortNum) {
                return outputs.get(resultPortNum - 1);
            }
        }
        return null;
    }

    /**
     * 
     * @param inputs
     *            list with input expressions
     * @param outputs
     *            list with output expressions
     * @param controls
     *            list with parameter expressions
     * @return List with argument expression selected from 3 list (arguments of
     *         the current function)
     */
    public List<Expression> getArgumentList(List<Expression> inputs,
            List<Expression> outputs) {
        List<Expression> resultList = new ArrayList<Expression>();
        Expression paramExp = null;
        int portNum = 0;
        int i = 0;
    	Expression exp;
    	SignatureElement signatureEl;
    	GADataType dtSignature;
        for (SignatureElement elem : getArgumentTypes()) {
            portNum = elem.getPortNumber();
            switch (elem.getElementType()) {
            case INPUT_VARIABLE:
            	// Add the expression referencing to the input port variable
            	exp = inputs.get(portNum - 1);
            	signatureEl = argumentTypes.get(i);
            	dtSignature = signatureEl.getDataType();
            	if (dtSignature.isScalar()) {
                	// AnTo 101108
                	// In case the user-defined function signature is scalar, 
            		// but the input argument is a singleton array, then add 
            		// required indices
        			if (exp.getDataType().isSingletonArray()) {
	        			int n = exp.getDataType().getDimensionality();
	        			// Add n indexExpressions to get the value of the first element
	        			for (int j=0; j<n; j++) {
	        				exp.addIndexExpression(new IntegerExpression(0));
	        			}
        			}
            		
            	} else if (dtSignature.isSingletonArray()) {
                	// AnTo 101103
                	// In case the user-defined function signature has an array of length 1, 
                	// then normally in Simulink and Gene-Auto this is treated as a scalar.
                	// However, since we have no control over the called function definition 
                	// we need to adapt the call. We turn the scalar effectively to an array 
                	// by passing its address to the called function. We don't have this problem 
                	// with outputs, since all scalar outputs are converted to pointers.
                	// TODO This solution works fine for C. Check for Ada!
            		exp = new UnaryExpression(exp, UnaryOperator.REF_OPERATOR);
            	}
                resultList.add(exp);
                break;
            case OUTPUT_VARIABLE:
            	// Add the expression referencing to the output port variable
            	exp = outputs.get(portNum - 1);
            	signatureEl = argumentTypes.get(i);
            	dtSignature = signatureEl.getDataType();
            	// AnTo 101103/101108
            	// In case the variable is scalar
				// This is a special case, since for non-scalars the passing 
            	// mechanism in the generated C code is both for Simulink and 
            	// Gene-Auto "by address" anyway. For Ada the direction of the variable 
            	// is detected and handled automatically by the printer. Special handling
            	// is required for scalars so that it is possible to modify them from the 
            	// called code.
            	if (dtSignature.isScalar()) {
            		if (exp.getDataType().isScalar()) {
            			// AND the supplied expression is SCALAR:
                    	// Make the reference "by address"
                		exp = new UnaryExpression(exp, UnaryOperator.REF_OPERATOR);
            		} else {
            			// AND the supplied expression is a NON-SCALAR:
                    	// Check that it is a singleton array. And if yes, then pass 
            			// the head address of the first element
            			if (!exp.getDataType().isSingletonArray()) {
            				EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
            						"Output expression passed to argument " + (i+1) + " to Block:\n  " 
            						+ block.getReferenceString() + "\n" + "must be a scalar or singleton array. But got: " 
            						+ exp.getDataType().toString());
            				return null;
            			}
            			int n = exp.getDataType().getDimensionality();
            			// Add n-1 indexExpressions to get the head address of the first element
            			for (int j=0; j<n-1; j++) {
            				exp.addIndexExpression(new IntegerExpression(0));
            			}
            		}
            	}
                resultList.add(exp);
                break;
            case PARAMETER:
                if (parameters.containsKey(portNum)) {
                    paramExp = parameters.get(portNum);
                } else {
                    paramExp = getParameterExpression(portNum, 
                    		argumentTypes.get(i).getDataType());
                }
                resultList.add(paramExp.getCopy());
                break;
            }
            i++;
        }
        return resultList;
    }
    
    /**
     * Returns data-type of the function. If there is no data type,
     * TVoid is returned 
     * 
     * @return GADataType
     */
    public GADataType getReturnType(){
    	GADataType dt = getOutputPortType(1);
    	
    	if (dt == null){
    		return new TVoid();
    	} else {
    		return dt;
    	}    		
    }

}
