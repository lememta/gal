/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/ModelUtilities.java,v $
 *  @version	$Revision: 1.19 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.PrivilegedAccessor;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

public class ModelUtilities {

    /**
     * Transfer all attributes and parameters from old to new GAModelElement.
     * 
     * @param oldElement
     *            The element which is to be replaced.
     * @param newElement
     *            The element which replaces the oldElement.
     */
    @SuppressWarnings("unchecked")
	public static void transferContent(GAModelElement oldElement, 
    		GAModelElement newElement) {
        List<Field> oldElementsFields = PrivilegedAccessor
                .getClassFields(oldElement.getClass());
        
        /**
         *  all fields are copied if in the newElement suitable field exists
         *  Exclusions:
         *  	id -- if element is copied it must get new id
         *  	parent -- normally parent shall be set by the parent object 
         *  				setter
         *  	model -- setting a model indicates, that object must get id. 
         *  				This must not be done before the real parent is 
         *  				known   
         */
        
        for (Field field : oldElementsFields) {
        	if (field.getName().equals("id")
        			|| field.getName().equals("parent")
        			|| field.getName().equals("model")){
        		continue;
        	}
        	
            Object fieldValue = 
            	PrivilegedAccessor.getValue(oldElement, field.getName());
            if (fieldValue != null &&
                PrivilegedAccessor.setValue(newElement, field.getName(), fieldValue, false)) {
                // Correct the fields parent pointer if the oldElement was parent 
                if (fieldValue instanceof GAModelElement 
                		&& ((GAModelElement)fieldValue).getParent()!=null
                		&& ((GAModelElement)fieldValue).getParent().equals(oldElement)) {
               		((GAModelElement)fieldValue).setParent((GAModelElement)newElement);
                } else if (fieldValue instanceof Collection) {
                    for (Object obj : (Collection)fieldValue) {
                        if (obj instanceof GAModelElement 
                        		&& ((GAModelElement)obj).getParent()
                        				.equals(oldElement)) {
                            ((GAModelElement)obj).setParent((GAModelElement)newElement);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Locates block's parameter by the given paramName and return its value
     * @param block
     * @param paramName - name of the parameter
     * @return the value if the parameter is found or null otherwise
     */
    @Deprecated //There is block method for that purpose
    public static Expression getParameterValue(Block block, String paramName) {
        for (Parameter param : block.getParameters()) {
            if (param.getName().equals(paramName)) {
            	return param.getValue();
            }
        }
        return null;
    }
    
	public static void removeElements(List<GAModelElement> removedElements) {

		for (GAModelElement elementToRemove : removedElements) {

			// getting the element parent
			GAModelElement parent = elementToRemove.getParent();

			/*
			 * Browsing all the GAModelElement type to remove the element in the
			 * right place
			 */

			if (elementToRemove instanceof Signal) {
				if (parent instanceof SystemBlock) {
					((SystemBlock) parent).getSignals().remove(elementToRemove);
				} else {
					EventHandler.handle(EventLevel.CRITICAL_ERROR, "TFMPreprocessor - ReplacingGotoFromBlockState", "",
							"Parent of a Signal must be a SystemBlock", "");
				}
			} else if (elementToRemove instanceof Block) {
				if (parent instanceof SystemBlock) {
					((SystemBlock) parent).getBlocks().remove(elementToRemove);
				} else {
					EventHandler.handle(EventLevel.CRITICAL_ERROR, "TFMPreprocessor - ReplacingGotoFromBlockState", "",
							"Parent of a Block must be a SystemBlock", "");
				}
			} else if (elementToRemove instanceof InDataPort) {
				if (parent instanceof Block) {
					((Block) parent).getInDataPorts().remove(elementToRemove);
				} else {
					EventHandler.handle(EventLevel.CRITICAL_ERROR, "TFMPreprocessor - ReplacingGotoFromBlockState", "",
							"Parent of a Port must be a Block", "");
				}
			} else if (elementToRemove instanceof OutDataPort) {
				if (parent instanceof Block) {
					((Block) parent).getOutDataPorts().remove(elementToRemove);
				} else {
					EventHandler.handle(EventLevel.CRITICAL_ERROR, "TFMPreprocessor - ReplacingGotoFromBlockState", "",
							"Parent of a Port must be a Block", "");
				}
			} else if (elementToRemove instanceof Parameter) {
                if (parent instanceof Block) {
                    ((Block) parent).getParameters().remove(elementToRemove);
                } else {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, "TFMPreprocessor - ReplacingGotoFromBlockState", "",
                            "Parent of a Port must be a Block", "");
                }
			} else {
				EventHandler.handle(EventLevel.CRITICAL_ERROR, "TFMPreprocessor - ReplacingGotoFromBlockState", "",
						"Element to remove must be Signal, Port or Block", "");
			}
		}
	}
}
