/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/BackendUtilities.java,v $
 *  @version	$Revision: 1.34 $
 *	@date		$Date: 2011-11-28 22:44:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.NameSpaceElement_CM;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.models.genericmodel.Annotation;
import geneauto.utils.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class BackendUtilities {

    /**
     * return variable expression var[i].
     * 
     * @param var
     * @param i
     * @return
     */
    public static Expression varIndex(final Expression var, Expression index) {

        // create a copy of the input expression object, in order to not modify
        // it
        Expression aux = var.getCopy();

        // index i initialization
        ArrayList<Expression> indexs = new ArrayList<Expression>();
        indexs.add(index);
        aux.setIndexExpressions(indexs);

        return aux;
    }

    public static void getMatrixProductAssignment(Expression matRes,
            Expression mat1, Expression mat2, AssignStatement asSt) {

        getMatrixProductAssignment(null, matRes.getCopy(), mat1.getCopy(), mat2
                .getCopy(), asSt);

    }

    private static void getMatrixProductAssignment(Block block,
            Expression matRes, Expression mat1, Expression mat2,
            AssignStatement asSt) {
        // determining matrix dimensions
        IntegerExpression rowExpr1 = null;
        IntegerExpression columnExpr1 = null;

        GADataType type = mat1.getDataType();

        rowExpr1 = (IntegerExpression) ((TArray) type).getDimensions().get(0);

        columnExpr1 = (IntegerExpression) ((TArray) mat1.getDataType())
                .getDimensions().get(1);

        int rowMat1 = Integer.valueOf(rowExpr1.getLitValue());
        int columnMat1 = Integer.valueOf(columnExpr1.getLitValue());

        IntegerExpression rowExpr2 = (IntegerExpression) ((TArray) mat2
                .getDataType()).getDimensions().get(0);

        IntegerExpression columnExpr2 = (IntegerExpression) ((TArray) mat2
                .getDataType()).getDimensions().get(1);

        int rowMat2 = Integer.valueOf(rowExpr2.getLitValue());
        int columnMat2 = Integer.valueOf(columnExpr2.getLitValue());

        // checking dimension validity and computing result dimension
        int row = 0, column = 0;

        if (columnMat1 == rowMat2) {
            row = rowMat1;
            column = columnMat2;
        } else {
            String msg = " Inputs have not correct dimension : ";
            if (block == null) {

            } else {
                msg += block.getReferenceString();
            }
            EventHandler.handle(EventLevel.ERROR, "ProductBackend", "", msg,
                    "");
        }

        // setting result matrix type
        Expression rowRes = new IntegerExpression(row);
        Expression columnRes = new IntegerExpression(column);

        List<Expression> listDim = new ArrayList<Expression>();
        listDim.add(rowRes);
        listDim.add(columnRes);

        matRes.setDataType(new TArray(listDim, mat1.getDataType()
                .getPrimitiveType()));

        /*
         * Fill the ListExpression that will be the right expression of the
         * assign statement
         */
        List<Expression> matrix = new ArrayList<Expression>();

        // for each matrix element
        for (int i = 0; i < row; i++) {

            List<Expression> lines = new ArrayList<Expression>();

            for (int j = 0; j < column; j++) {

                // number of sum on the expression of the result for each
                // element, in fact number of column of the first input matrix
                // (or number of line of the second)
                int sumNumber = Integer
                        .valueOf(((IntegerExpression) ((TArray) mat1
                                .getDataType()).getDimensions().get(1))
                                .getLitValue());

                Expression matrixMember = null;

                BinaryOperator prod = BinaryOperator.ADD_OPERATOR;

                List<BinaryExpression> exprs;
                exprs = new ArrayList<BinaryExpression>();

                // for each factor in the sum :
                // res[i][j] = E_k {mat1[i][k]*mat2[k][j] }
                for (int k = 0; k < sumNumber - 1; k++) {

                    // mat1[i][k] . mat2[k][j]
                    BinaryExpression prodAux = new BinaryExpression(
                            BackendUtilities.varIndex2(mat1, i, k),
                            BackendUtilities.varIndex2(mat2, k, j),
                            BinaryOperator.MUL_OPERATOR);

                    exprs.add(new BinaryExpression(prodAux, null, prod));

                    if ((k == 0) && (k == sumNumber - 2)) {
                        matrixMember = exprs.get(k);

                        exprs.get(k)
                                .setRightArgument(
                                        new BinaryExpression(BackendUtilities
                                                .varIndex2(mat1, i, k + 1),
                                                BackendUtilities.varIndex2(
                                                        mat2, k + 1, j),
                                                BinaryOperator.MUL_OPERATOR));
                    } else if (k == 0) {
                        matrixMember = exprs.get(k);

                    } else if (k == sumNumber - 2) {
                        exprs.get(k)
                                .setRightArgument(
                                        new BinaryExpression(BackendUtilities
                                                .varIndex2(mat1, i, k + 1),
                                                BackendUtilities.varIndex2(
                                                        mat2, k + 1, j),
                                                BinaryOperator.MUL_OPERATOR));
                    }
                    if (k != 0) {
                        exprs.get(k - 1).setRightArgument(exprs.get(k));
                    }
                }
                lines.add(matrixMember);
            }
            matrix.add(new GeneralListExpression(lines));
        }

        GeneralListExpression matrixExp = new GeneralListExpression(matrix);

        List<Expression> dimMatrix = new ArrayList<Expression>();
        dimMatrix.add(new IntegerExpression(row));
        dimMatrix.add(new IntegerExpression(column));

        matrixExp.setDataType(new TArray(dimMatrix));

        /*
         * // assign statement AssignStatement assign = new AssignStatement(
         * AssignOperator.SimpleAssign, matRes, matrixExp);
         */

        asSt.setOperator(AssignOperator.SIMPLE_ASSIGN);
        asSt.setLeftExpression(matRes);
        asSt.setRightExpression(matrixExp);
        if (block != null) {
            // giving a name to the assign statement
            asSt.setName(block.getName() + "Assign");

            // annotating generated code model to indicate source block of the
            // code model part
            asSt.setAnnotations(BackendUtilities.getAnnotation(block));
        }
    }

    /**
     * return variable expression var[i].
     * 
     * @param var
     * @param i
     * @return
     */
    public static Expression varIndex(final Expression var, int i) {

        // create a copy of the input expression object, in order to not modify
        // it
        Expression aux = var.getCopy();

        // index i initialization
        ArrayList<Expression> indexs = new ArrayList<Expression>();
        indexs.add(new IntegerExpression(i));
        aux.setIndexExpressions(indexs);

        return aux;
    }

    /**
     * return variable expression var[i][i].
     * 
     * @param var
     * @param i
     * @param j
     * @return
     */
    public static Expression varIndex2(final Expression var, Expression i,
            Expression j) {

        // create a copy of the input expression object, in order to not modify
        // it
        Expression aux = var.getCopy();

        // index i initialization
        ArrayList<Expression> indexs = new ArrayList<Expression>();
        indexs.add(i);
        indexs.add(j);
        aux.setIndexExpressions(indexs);

        return aux;
    }

    /**
     * return variable expression var[i][i].
     * 
     * @param var
     * @param i
     * @param j
     * @return
     */
    public static Expression varIndex2(final Expression var, int i, int j) {

        // create a copy of the input expression object, in order to not modify
        // it
        Expression aux = var.getCopy();

        // index i initialization
        ArrayList<Expression> indexs = new ArrayList<Expression>();
        indexs.add(new IntegerExpression(i));
        indexs.add(new IntegerExpression(j));
        aux.setIndexExpressions(indexs);

        return aux;
    }

    /**
     * parse a binary EML relational expression (u1 > 0, for instance)
     * 
     * @param expr
     * @return
     */
    public static Expression parseEmlExpression(String expr,
            List<Expression> inputs) {

        int inputNum = 0;
        BinaryOperator op = null;
        DoubleExpression realExpr = new DoubleExpression();

        for (int i = 0; i < expr.length(); i++) {
            if (expr.charAt(i) == 'u') {
                inputNum = Integer.valueOf(String.valueOf(expr.charAt(i + 1)));
            }
            if (expr.charAt(i) == '>') {
                if (expr.charAt(i + 1) == '=') {
                    op = BinaryOperator.GE_OPERATOR;
                    realExpr.setLitValue(expr.substring(i + 2, expr.length()));
                } else {
                    op = BinaryOperator.GT_OPERATOR;
                    realExpr.setLitValue(expr.substring(i + 1, expr.length()));
                }
            }
            if (expr.charAt(i) == '<') {
                if (expr.charAt(i + 1) == '=') {
                    op = BinaryOperator.LE_OPERATOR;
                    realExpr.setLitValue(expr.substring(i + 2, expr.length()));
                } else {
                    op = BinaryOperator.LT_OPERATOR;
                    realExpr.setLitValue(expr.substring(i + 1, expr.length()));

                }
            }
        }

        return new BinaryExpression(inputs.get(inputNum - 1), realExpr, op);
    }

    /**
     * 
     * @param stringName
     * @return
     */
    public static String cleanStringName(String stringName) {

        // replace all space character
        return stringName.replace(" ", "").replace("/", "").replace("\\", "")
                .replace("\n", "");

    }

    /**
     * parse an array give as string argument ("[1 2 3 1]" for instance) in list
     * of literal expression
     * 
     * @param array
     * @return
     */
    public static GeneralListExpression parseArray(String array, GADataType type) {

        ArrayList<Expression> exprs = new ArrayList<Expression>();
        int index;

        for (int i = 0; i < array.length(); i++) {

            if ((array.charAt(i) == ' ') || array.charAt(i) == '[') {
                index = i + 1;

                if (index < array.length()) {
                    while ((array.charAt(index) != ' ')
                            && (array.charAt(index) != ']')
                            && (array.charAt(index) != ';')) {
                        index++;
                    }

                    exprs.add(LiteralExpression.getInstance(array.substring(i + 1,
                            index), type));
                }
            }
        }
        return new GeneralListExpression(exprs);
    }

    /**
     * parse an fragment (just one line) of matrix give as string argument ("[1
     * 2 3; 1 2 3]" for instance) in list of literal expression
     * 
     * @param array
     * @return
     */
    public static int parseMatrixAux(String matrix, GADataType type,
            GeneralListExpression exprs, int index) {

        int i = index;

        while (i < matrix.length()) {

            if ((matrix.charAt(i) == ' ') || (matrix.charAt(i) == '[')
                    || (matrix.charAt(i) == ';')) {
                index = i + 1;

                if (index < matrix.length()) {
                    while ((matrix.charAt(index) != ' ')
                            && (matrix.charAt(index) != ';')
                            && (matrix.charAt(index) != ']')) {
                        index++;
                    }

                    exprs.addExpression(LiteralExpression.getInstance(matrix.substring(
                            i + 1, index), type));

                    if ((matrix.charAt(index) == ']')
                            || (matrix.charAt(index) == ';')) {
                        return index;
                    }
                }
            }
            i++;
        }
        return index;
    }

    /**
     * parse an matrix give as string argument ("[1 2 3; 1 2 3]" for instance)
     * in list of list of literal expression
     * 
     * @param matrix
     * @return
     */
    public static GeneralListExpression parseMatrix(String matrix, GADataType type) {

        GeneralListExpression exprs = new GeneralListExpression();

        int index = 0;

        while (index < matrix.length() - 1) {

            GeneralListExpression expr = new GeneralListExpression();
            index = parseMatrixAux(matrix, type, expr, index);
            exprs.addExpression(expr);
        }
        return exprs;
    }

    public static BinaryOperator parseOperator(String stringOperator) {

        if (stringOperator.equals("AND")) {
            return BinaryOperator.LOGICAL_AND_OPERATOR;
        }
        if (stringOperator.equals("OR")) {
            return BinaryOperator.LOGICAL_OR_OPERATOR;
        }
        if (stringOperator.equals("XOR")) {
            return BinaryOperator.BITWISE_XOR_OPERATOR;
        }

        return null;
    }

    /**
     * return assign statement : out[i][j] = in1[i][j] op in2[i][j] op ... op
     * inN[i][j].
     * 
     * @param inputs
     *            List of variable expressions with TArray type
     * @param output
     *            Variable expression with TArray type
     * @param i
     * @return
     */
    public static AssignStatement assignMatrix(List<Expression> inputs,
            Expression output, int i, int j, BinaryOperator operator) {

        // general assign initialization
        AssignStatement assign = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN);

        // left expression assign : output[i][j]
        assign.setLeftExpression(BackendUtilities.varIndex2(output, i, j));

        // input product expression : in1[i]* ... * inN[i]
        List<BinaryExpression> exprs;
        exprs = new ArrayList<BinaryExpression>();

        int inputNumber = inputs.size();

        // for each block input
        for (int k = 0; k < inputNumber - 1; k++) {

            // inputs_k[i]
            exprs.add(new BinaryExpression(BackendUtilities.varIndex2(inputs
                    .get(k), i, j), null, operator));

            if ((k == 0) && (k == inputNumber - 2)) {
                assign.setRightExpression(exprs.get(k));

                exprs.get(k).setRightArgument(
                        BackendUtilities.varIndex2(inputs.get(k + 1), i, j));
            } else if (k == 0) {
                assign.setRightExpression(exprs.get(k));
            } else if (k == inputNumber - 2) {
                exprs.get(k).setRightArgument(
                        BackendUtilities.varIndex2(inputs.get(k + 1), i, j));

            }
            if (k != 0) {
                exprs.get(k - 1).setRightArgument(exprs.get(k));
            }
        }
        return assign;
    }

    /**
     * return assign statement : out[i] = in1[i] op in2[i] op ... op inN[i].
     * 
     * @param inputs
     *            List of variable expressions with TArray signal type
     * @param output
     *            Variable expression with TArray signal type
     * @param i
     * @return
     */
    public static AssignStatement assignVector(List<Expression> inputs,
            Expression output, int i, BinaryOperator operator) {

        // general assign initialization
        AssignStatement assign = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN);

        // left expression assign : output[i]
        assign.setLeftExpression(BackendUtilities.varIndex(output, i));

        // input product expression : in1[i]* ... * inN[i]
        List<BinaryExpression> exprs;
        exprs = new ArrayList<BinaryExpression>();

        int inputNumber = inputs.size();

        // for each block input
        for (int k = 0; k < inputNumber - 1; k++) {

            // inputs_k[i]
            exprs.add(new BinaryExpression(BackendUtilities.varIndex(inputs
                    .get(k), i), null, operator));

            if ((k == 0) && (k == inputNumber - 2)) {
                assign.setRightExpression(exprs.get(k));

                exprs.get(k).setRightArgument(
                        BackendUtilities.varIndex(inputs.get(k + 1), i));
            } else if (k == 0) {
                assign.setRightExpression(exprs.get(k));
            } else if (k == inputNumber - 2) {
                exprs.get(k).setRightArgument(
                        BackendUtilities.varIndex(inputs.get(k + 1), i));

            }
            if (k != 0) {
                exprs.get(k - 1).setRightArgument(exprs.get(k));
            }
        }
        return assign;
    }

    /**
     * return assign statement : out = in1 op in2 op ... op inN.
     * 
     * @param inputs
     *            List of variable expressions with TArray signal type
     * @param output
     *            Variable expression with TArray signal type
     * @param i
     * @return
     */
    public static AssignStatement assignScalar(List<Expression> inputs,
            Expression output, BinaryOperator operator) {

        // general assign initialization
        AssignStatement assign = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN);

        // left expression assign : output[i]
        assign.setLeftExpression(output);

        // input product expression : in1[i]* ... * inN[i]
        List<BinaryExpression> exprs;
        exprs = new ArrayList<BinaryExpression>();

        int inputNumber = inputs.size();

        // for each block input
        for (int k = 0; k < inputNumber - 1; k++) {

            // inputs_k[i]
            exprs.add(new BinaryExpression(inputs.get(k), null, operator));

            if ((k == 0) && (k == inputNumber - 2)) {
                assign.setRightExpression(exprs.get(k));

                exprs.get(k).setRightArgument(inputs.get(k + 1));
            } else if (k == 0) {
                assign.setRightExpression(exprs.get(k));
            } else if (k == inputNumber - 2) {
                exprs.get(k).setRightArgument(inputs.get(k + 1));

            }
            if (k != 0) {
                exprs.get(k - 1).setRightArgument(exprs.get(k));
            }
        }
        return assign;
    }

    /**
     * out = in[0] op in[1] op ... op in[N].
     * 
     * @param input
     *            expression referring to the output
     *            this is assumed to be scalar
     * @param output
     *            expression referring to the input
     *            this is assumed to be vector
     * @param operator
     * 			  primary binary operator NB! in case of a composite operator 
     * 			  (NAND, NOR, XOR) the full operation must be derived from 
     * 			  opString
     * @param opString
     * 			  full operator string from list (AND, OR, NOR, NAND, XOR)
     * 
     * TODO (TF:797): consider inlining the loop in case it contains only one element
     * 		 i.e. use simple assignments in case the vector is composed of 
     * 		 one or two elements only (ToNa 27/04/10)
     * 
     * @return an iterator completing expression 
     * 				out = in[0] op in[1] op ... op in[N]. 
     */
    public static List<Statement> assignVector2Scalar(Expression input,
            Expression output, BinaryOperator operator, String opString) {

    	
    	List<Statement> statements = new ArrayList<Statement>();
    	
    	int maxIndex = input.getDataType().getDimensionsLength(0)-1;
    	RangeExpression range = new RangeExpression(1, maxIndex);
    	Variable_CM indexVar = new Variable_CM(
        		"i1",
        		null, false, false, false,
                DefinitionScope.LOCAL, 
                range.getDataType());
        
        VariableExpression index = new VariableExpression(indexVar);
        Expression inputFirstElem = input.getCopy();
        inputFirstElem.addIndexExpression(new IntegerExpression(0));        
        Expression inputWithIndex = input.getCopy();
        inputWithIndex.addIndexExpression(index);
        
    	if ("XOR".equals(opString)) {
    		// in case of an expression y_bool = x1 xor x2 xor x3 we do:
    		// y_bool = (boolean) (((x1 != 0) ^ (x2 != 0)) ^ (x3 != 0))

            AssignStatement initSt =  new AssignStatement(
                	AssignOperator.SIMPLE_ASSIGN,
                	output.getCopy(),
                	new UnaryExpression( 
        					new BinaryExpression( 
        							inputFirstElem,
        							new IntegerExpression(0),
        							BinaryOperator.NE_OPERATOR),
        					UnaryOperator.CAST_OPERATOR,
        					new TRealInteger(8, false)));
            RangeIterationStatement iteratorSt = new RangeIterationStatement(
                new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN,
                        output.getCopy(),
                        new BinaryExpression(
                        		new UnaryExpression( 
                        				output.getCopy(),
                        				UnaryOperator.CAST_OPERATOR,
                						new TRealInteger(8, false)),
                        		new UnaryExpression( 
                        				new BinaryExpression( 
                        						inputWithIndex,
                        						new IntegerExpression(0),
                        						BinaryOperator.NE_OPERATOR),
                        						UnaryOperator.CAST_OPERATOR,
                        						new TRealInteger(8, false)),
                        		operator
                        		)
                        ),
                indexVar, range
                );
                            
            iteratorSt.addNameSpaceElement(indexVar);
            statements.add(initSt);
            statements.add(iteratorSt);
            
    	} else {
        	// perform elementwise binary operation between the arguments
            AssignStatement initSt =  new AssignStatement(
                	AssignOperator.SIMPLE_ASSIGN,
                	output.getCopy(),
                	inputFirstElem);
	        RangeIterationStatement iteratorSt = new RangeIterationStatement(
	                new AssignStatement(
	                        AssignOperator.SIMPLE_ASSIGN,
	                        output.getCopy(),
	                        new BinaryExpression(
	                                output.getCopy(), 
	                                inputWithIndex,
	                                operator)),
	                indexVar,
	                (RangeExpression) range.getCopy());
	                            
	        iteratorSt.addNameSpaceElement(indexVar);
        	
            statements.add(initSt);
            statements.add(iteratorSt);
            
        	// apply NOT to the result
        	// TODO: this can be optimising by reversing the binary operator 
        	// above and integrating the not operator right into the loop
        	// (ToNa 27/04/10)
        	if ("NAND".equals(opString) 
            		|| "NOR".equals(opString)) {
                AssignStatement notSt =  new AssignStatement(
                    	AssignOperator.SIMPLE_ASSIGN,
                    	output.getCopy(),
                    	new UnaryExpression(output.getCopy(), 
        								UnaryOperator.NOT_OPERATOR));
                statements.add(notSt);
        	}
            
    	}
    	
        return statements;
    }
    
    public static List<Annotation> getAnnotation(Block block) {

        ArrayList<Annotation> annot = new ArrayList<Annotation>();
        annot.add(new Annotation(0, "Block: " + block.getOriginalFullName(),
                "geneauto.blocklibrary", true));

        return annot;
    }

    public static List<Expression> parseStringArrayValue(String array,
            GADataType type) {

        int index = 0;
        Expression expr;
        List<Expression> list;
        list = new ArrayList<Expression>();

        while (array.charAt(index) != ']') {
            if ((array.charAt(index) == '[') | (array.charAt(index) == ' ')) {
                expr = LiteralExpression.getInstance(String.valueOf(array
                        .charAt(index + 1)), type);
                list.add(expr);
            }
            index += 1;
        }

        return list;
    }

    /**
     * Parses the OutputSignals parameter
     * 
     * @param value,
     *            the parameter value
     * @return a vector of String containing the selected signal indexes
     */
    public static Vector<String> parseOutputSignals(String value) {
        Vector<String> portIndexes = new Vector<String>();
        value = value.replace("signal", "");
        while (value.indexOf(',') != -1) {
            int i = value.indexOf(',');
            portIndexes.add(value.substring(0, i));
            value = value.substring(i + 1, value.length());
        }
        portIndexes.add(value);
        return portIndexes;
    }

    /**
     * Find the control signal and its destination exiting from given port Get
     * the computation code from the destination port
     * 
     * @param p
     * @return
     */
    public static Pair<List<NameSpaceElement_CM>, List<Statement>> getControlPortComputationCode(
            OutControlPort p) {
        // Find the control signal and its destination exiting from given port
        for (Signal s : ((SystemBlock) p.getParent().getParent()).getSignals()) {
            if (s.getSrcPort().equals(p)) {
                Block b = (Block) s.getDstPort().getParent();
                if ("Outport".equals(b.getType())) {
                    // Check if we are in the main (topmost) system
                    if (p.getParent().equals(p.getTopLevelParent())) {
                        // TODO handle control signal exiting topmost system
                        EventHandler
                                .handle(
                                        EventLevel.WARNING,
                                        "BackendUtilities.getControlPortComputationCode",
                                        "",
                                        "Control signals exiting top-level subsystem",
                                        "");
                        List<Statement> stmts = new ArrayList<Statement>();
                        stmts.add(new BlankStatement(
                                "NB!! INSERT HERE THE CODE for executing "
                                        + "function-call target"));
                        return new Pair<List<NameSpaceElement_CM>, List<Statement>>(
                                null, stmts);
                    }
                    // follow control signal out of the current system
                    else {
                        // get the code from the only port that can
                        // correspond to this block
                        return getControlPortComputationCode((OutControlPort) b
                                .getPortReference());
                    }
                } else if ("Terminator".equals(b.getType())) {
                    List<Statement> stmts = new ArrayList<Statement>();
                    stmts.add(new BlankStatement("Terminator"));
                    return new Pair<List<NameSpaceElement_CM>, List<Statement>>(
                            null, stmts);
                } else {
                    // Get computation code of the target block's inControlPort
                    return new Pair<List<NameSpaceElement_CM>, List<Statement>>(
                            new ArrayList<NameSpaceElement_CM>(),
                            ((InControlPort) s.getDstPort()).getComputeCode());
                }
            }
        }
        // It seems the outControlPort is unconnected - create corresponding
        // code
        EventHandler.handle(EventLevel.WARNING,
                "BackendUtilities.getControlPortComputationCode", "",
                "Unconnected Control signal detected in "
                        + p.getReferenceString(), "");
        List<Statement> stmts = new ArrayList<Statement>();
        stmts.add(new BlankStatement("Unconnected outControl"));
        return new Pair<List<NameSpaceElement_CM>, List<Statement>>(null, stmts);
    }

}
