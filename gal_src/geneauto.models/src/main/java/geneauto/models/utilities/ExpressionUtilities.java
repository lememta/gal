/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/ExpressionUtilities.java,v $
 *  @version	$Revision: 1.10 $
 *	@date		$Date: 2011-11-28 22:44:10 $
 *
 *  Copyright (c) 2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.ConstantListExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.LValueExpression;
import geneauto.models.gacodemodel.expression.SubArrayExpression1D;
import geneauto.utils.PrivilegedAccessor;

import java.util.List;

public class ExpressionUtilities {
    private Expression expression;
    
    public ExpressionUtilities() {
        
    }
    
    public ExpressionUtilities(Expression expression) {
        super();
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    
    /**
     * Creates a list expression, where the supplied expression is replicated 
     * a requested number of times. 
     * 
     * e.g. "e" and [2, 3] => {{"e", "e", "e"}, {"e", "e", "e"}}
     * 
     * @param exp expression to convert
     * @param dims dimensions that are to be created
     * @return
     */
    public Expression toListExpression(Expression exp, List<Integer> dims) {
        if (dims.isEmpty()) {
            return exp;
        } else {
            int iLastD = dims.size()-1;
            ConstantListExpression lExp = getConstantListExpressionInstance(exp);
            lExp.setExpression(exp);
            lExp.setLength(dims.get(iLastD));
            dims.remove(iLastD);
            return toListExpression(lExp, dims);
        }
    }
    
    public Expression toListExpression(List<Integer> dims) {
        return toListExpression(expression, dims);
    }
    
    
    public void setIndexes(Integer[] indexes) {
        for (Integer idx : indexes) {
            expression.addIndexExpression(getIntegerExpression(idx), true);
        }
    }
    
    public GeneralListExpression getGeneralListExpressionInstance(Expression expression) {
        return (GeneralListExpression) PrivilegedAccessor.
            getObjectInPackage(GeneralListExpression.class, expression);
    }
    
    public ConstantListExpression getConstantListExpressionInstance(Expression expression) {
        return (ConstantListExpression) PrivilegedAccessor.
            getObjectInPackage(ConstantListExpression.class, expression);
    }
    
    public IntegerExpression getIntegerExpression(int value) {
        IntegerExpression intExp = (IntegerExpression) PrivilegedAccessor.
            getObjectInPackage(IntegerExpression.class, expression);
        intExp.setLitValue((new Integer(value)).toString());
        return intExp;
    }
    
    /**
     * Increases value of index by one
     * @param index
     */
    public static void incIndex(IntegerExpression index) {
        index.setLitValue(Integer.
                toString((int) index.evalReal().getRealValue() + 1) 
                );
    }
    
    /**
     * Sets value of integer expression to start index
     * 
     * @param index integer expression which value to be changed
     * @param start start index (0 by default but may be different 
     * in case it is index of sub array expression)
     */
    public static void resetIndex(IntegerExpression index, Integer start) {
        if (start == 0) {
        	index.setLitValue("0");
        } else {
        	index.setLitValue(start.toString());
        }
    	
    }
    
    /**
     * 
     * 
     * @param exp
     * @param startIndex
     * @param endIndex
     * 
     * @return sub array expression for given expression starting 
     * from srartIndex and ending with endIndex. In case startIndex
     * is equal to endIndex returns exp with corresponding index expression
     */
    public static Expression getSubArrayExpression(Expression exp, 
    		int startIndex, int endIndex) {
    	Expression result;
    	if (startIndex == endIndex) {
        	result = exp.getCopy();
        	result.addIndexExpression(new IntegerExpression(startIndex));
        } else {
        	if (!(exp instanceof LValueExpression)) {
        		EventHandler.handle(EventLevel.ERROR, 
        				"ExpressionUtilities.getSubArrayExpression", 
        				"", "Expression of sub array expression must be LValueExpression.\n"
        				+ "Expression" + exp.getReferenceString());
        		return null;
        	}
        	result = new SubArrayExpression1D((LValueExpression) exp, 
            		startIndex, endIndex);
        }
    	return result;
    }
}
