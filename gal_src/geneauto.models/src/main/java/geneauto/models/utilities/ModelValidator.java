package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.GACodeModel;
import geneauto.models.gacodemodel.TempModel;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.tuple.Pair;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ModelValidator {
	
	/**
	 * The current Gene-Auto metamodel implementation does not explicitly
	 * differenciate between containment and non-containment references.
	 * Containment is indirectly handled through the parent attribute. If a
	 * reference to is a containment reference, then it is required that the
	 * respective element points back to the parent element through its parent
	 * attribute.
	 * 
	 * TODO: Create a map containing "class"-"list of fields" pairs.
	 */
	private static String[] containmentFieldArray = {"indexExpressions"};
	private static Set<String> containmentFields = 
			new HashSet<String>(Arrays.asList(containmentFieldArray));

	/**
	 * Traverses the tree and checks that all GAModelElements have a 
	 * correct parent link (except root).
	 * 
	 * If all checks pass, the method returns quietly. Otherwise it raises an
	 * ERROR event for each error (up to the max allowed) and finally throws 
	 * a RunTimeException that displays the stack trace.
	 * 
	 * @param m Model
	 */
	public static void assertNoOrphans(Model m) {
		boolean errors = false;
		for (Object obj : m.getElements()) {
			if (obj instanceof GAModelElement) {
				errors = errors || assertNoOrphans(m, (GAModelElement) obj, null, null);
			}
		}
		// Check CodeModel.tempModel
		if (m instanceof GACodeModel) {
			TempModel tempModel = ((GACodeModel) m).getTempModel();
			if (tempModel != null) {
				for (Object obj : tempModel.getElements()) {
					if (obj instanceof GAModelElement) {
						errors = errors || assertNoOrphans(m, (GAModelElement) obj, null, null);
					}
				}
			}
		}
		if (errors) {
			throw new RuntimeException("Model integrity errors detected!");
		}
	}

	/**
	 * Traverses the tree starting from a given model element and checks 
	 * that all GAModelElements have a a correct parent link (except root).
	 * 
	 * If all checks pass, the method returns quietly. Otherwise it raises an
	 * ERROR event for each error (up to the max allowed) and finally throws 
	 * a RunTimeException that displays the stack trace.
	 * 
	 * @param m Model
	 * @param el GAModelElement
	 */
	public static void assertNoOrphans(Model m, GAModelElement el) {		
		boolean errors = assertNoOrphans(m, el, null, null);
		if (errors) {
			throw new RuntimeException("Model integrity errors detected!");
		}
	}
	
	/**
	 * Traverses the tree starting from a given model element and its parent 
	 * and checks that all GAModelElements have a correct parent link 
	 * (except root).
	 */
	public static boolean assertNoOrphans(Model m, GAModelElement el, GAModelElement previous, String fieldName) {
		boolean errors = false;		
		boolean currentRoot = previous == null;
		boolean modelRoot = (m != null) && m.getElements().contains(el);
		if (el.getId() == 1633 || el.getId() == 4579) {
			EventHandler.handle(
				EventLevel.INFO, "assertNoOrphans", "", "id=" + el.getId());
		}
		if (el.getParent() == null) {
			// No parent link
			if (currentRoot || modelRoot) {
				// Root elements have no parent
			} else {
				String parentPart = 
						"\n  Referring element (parent??): " + previous.getReferenceString() +
						"\n  Field : " + fieldName;
				EventHandler.handle(
						EventLevel.ERROR, "assertNoOrphans", "", "Parent link does not respect containment hierarchy"
								+ (m != null ? "\n  Model  : " + m.getReferenceString() : "")
								+ "\n  Element: name=" + el.getName()
								+ "\n           type="
								+ el.getClass().getName() + parentPart);
				errors = true;
			}
		} else {
			// Parent link exists
			// If the previous element has been given, check that the link matches it
			if (containmentFields.contains(fieldName)) {
				if (previous != null && el.getParent() != previous) {
					String parentPart = 
							"\n  Referring element (parent??): " + previous.getReferenceString() +
							"\n  Field : " + fieldName;
					EventHandler.handle(
							EventLevel.ERROR, "assertNoOrphans", "", "Orphan element"
									+ (m != null ? "\n  Model  : " + m.getReferenceString() : "")
									+ parentPart
									+ "\n  Element: " + el.getReferenceString()
									+ "\n  Claimed parent: " + el.getParent().getReferenceString()
									);
					errors = true;
				}
			}
		}
		// Recurse deeper, if it is the current root element or a proper child of the previous element. 
		// Ignore other model roots, which might have been referred.  
		if (currentRoot || ((el.getParent() == previous) && !modelRoot)) {
			for (Pair<Field, Object> pair : PrivilegedAccessor.getFieldsAndObjects(el)) {
				// Check, if value is a GAModelElement
				if (pair.getRight() instanceof GAModelElement) {
					errors = errors || assertNoOrphans(m, (GAModelElement) pair.getRight(), el, pair.getLeft().getName());
					
				// Check, if value is a List<GAModelElement> 
			    // Note: We cannot check this directly, since e.g. List<Block> is not a List<GAModelElement>
				} else if (pair.getRight() instanceof List) {
					for (Object o : (List<?>) pair.getRight()) {
						if (o instanceof GAModelElement) {
							errors = errors || assertNoOrphans(m, (GAModelElement) o, el, pair.getLeft().getName());
						}
					}					
				} else {
					// Ignore the rest
				}
			}
		}
		return errors;
	}
}
