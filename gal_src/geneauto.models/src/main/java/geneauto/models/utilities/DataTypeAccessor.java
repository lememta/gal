/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/DataTypeAccessor.java,v $
 *  @version	$Revision: 1.66 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TComplexDouble;
import geneauto.models.gadatatypes.TComplexInteger;
import geneauto.models.gadatatypes.TComplexNumeric;
import geneauto.models.gadatatypes.TComplexSingle;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gadatatypes.TRealSingle;
import geneauto.models.gadatatypes.TString;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.tuple.Triple;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which role is to get, given a String value and parameters, the
 * corresponding GADataType. This class is used mainly by the typer tool.
 * <p>
 * Datatypes shall be expressed according to the following patterns: <br>
 * <p>
 * <p>
 * FOR SIMPLE VALUES :
 * <p>
 * <value> <br>
 * examples : 1; -1; 1.5; -1.4 <br>
 * <p>
 * <type> '(' '[' <value> ' ' <value> ' ' <value> ']' ')' <br>
 * examples : uint8(1); int32(-1); double(1.5); boolean(0) <br>
 * <p>
 * <p>
 * FOR VECTORS :
 * <p>
 * '[' <value> ' ' <value> ' ' <value> ']' <br>
 * example : [1 -1 1.5 -1.4] <br>
 * <p>
 * <type> '[' <value> ' ' <value> ' ' <value> ']' <br>
 * example : double([1 -1 1.5 -1.4]) <br>
 * <p>
 * <p>
 * FOR MATRIX :
 * <p>
 * '[' <line> ' ; ' <line> ' ; ' <line> ']' <br>
 * example : [1 -1 ; 1.5 -1.4] <br>
 * <p>
 * <type> '(' '[' <line> ' ; ' <line> ' ; ' <line> ']' ')' <br>
 * example : double([1 -1 ; 1.5 -1.4]) <br>
 * <p>
 * <p>
 * where <type> can be one of {double, single, int8, int16, int32, uint8,
 * uint16, uint32} <br>
 * where <value> can be any value supported by simulink<br>
 * <p>
 * <p>
 * If the datatype is not indicated, then it is assumed that the default
 * datatype is TRealDouble.
 * 
 */
public class DataTypeAccessor {

    /**
     * Method which, given an object value, e.g. 2, true, [1 2 3]...should
     * return the corresponding GADataType. for now ... This method is useful
     * mainly when typing a block using the rule : "Inherited from Constant
     * value"
     * 
     * In case the type is not explicitly set returns TRealDouble'
     * 
     * @param typeString
     *            the object Value from which we want to get the type.
     * @return the correct DataType.
     */
    public static GADataType getDataType(String typeString) {
        return getDataType(typeString, true);
    }

    /**
     * Method which, given an object value, e.g. 2, true, [1 2 3]...should
     * return the corresponding GADataType. for now ... This method is useful
     * mainly when typing a block using the rule : "Inherited from Constant
     * value"
     * 
     * @param typeString
     *            the object Value from which we want to get the type.
     * @param useDefaultType
     *            if true, TRealDouble is returned in case the type is not
     *            explicitly set. IN case of false value, the method returns
     *            null value
     * @return the correct DataType.
     */
    public static GADataType getDataType(String typeString,
            boolean useDefaultType) {
        GADataType result = parseType(typeString, useDefaultType);

        if (result == null && useDefaultType) {
            result = new TRealDouble();
        }

        return result;
    }

    /**
     * Extract the sub-string between '(' and ')'
     * 
     * @param objectValue,
     *            the String to parse
     */
    /*
     * 17.12.2008 EMLparser grammar was modified to accept expressions with
     * typedef public static String getValue(String objectValue) { String res =
     * objectValue;
     * 
     * while(res.startsWith(" ")){ res = res.substring(1); }
     * 
     * if(!typeIsSpecified(res)){ return res; }else{ if (res.indexOf('(') != -1 &&
     * res.indexOf(')') != -1) { int firstPar = res.indexOf("(")+1; int lastPar =
     * res.length()-1;
     * 
     * res = res.substring(firstPar,lastPar); } }
     * 
     * return res; }
     * 
     * private static boolean typeIsSpecified(String objectValue) {
     * 
     * if(objectValue.startsWith("int") ||objectValue.startsWith("uint")
     * ||objectValue.startsWith("double") ||objectValue.startsWith("boolean")
     * ||objectValue.startsWith("single")){ return true; } return false; }
     */
    /**
     * If the value is a matrix or a vector, then it is parsed, else, we get the
     * primitive type.
     * 
     * @param typeString
     *            the String value
     * @param useDefaultType
     *            if true, TRealDouble is returned in case the type is not
     *            explicitly set. IN case of false value, the method returns
     *            null value
     * @return a primitive, matrix or vector gadatatype.
     */
    public static GADataType parseType(String typeString, boolean useDedaultType) {
        GADataType result = null;

        // get the primitive type
        GADataType primitive = null;

        // get the values
        if (typeString.contains("[")) {
            typeString = typeString.substring(typeString.indexOf("["));

            typeString = typeString.replace(")", "");
            String[][] parsedArray = parseArray(typeString);

            List<GADataType> tempType = new ArrayList<GADataType>();
            for(int i=0; i<parsedArray.length ; i++){
                for(int j=0; j<parsedArray[0].length ; j++){
                    tempType.add(parsePrimitiveType(parsedArray[i][j], useDedaultType));
                }
            }
            primitive = DataTypeUtils.chooseLeastCommonType(tempType, useDedaultType);
            //primitive = parsePrimitiveType(parsedArray[0][0], useDedaultType);
            if (primitive == null) {
                return null;
            }

            if (parsedArray.length == 1) {
                List<Expression> dims = new ArrayList<Expression>();
                dims.add(new IntegerExpression(parsedArray[0].length));
                result = new TArray(dims, (TPrimitive) primitive);

            } else if (parsedArray[0].length == 1) {

                List<Expression> dims = new ArrayList<Expression>();
                dims.add(new IntegerExpression(parsedArray.length));

                result = new TArray(dims, (TPrimitive) primitive);

            } else if (parsedArray.length > 1) {
                // matrix
                List<Expression> dims = new ArrayList<Expression>();
                dims.add(new IntegerExpression(parsedArray.length));
                dims.add(new IntegerExpression(parsedArray[0].length));

                result = new TArray(dims, (TPrimitive) primitive);
            } else {
                result = primitive;
            }
        } else if (typeString.contains("{")) {
            typeString = typeString.substring(typeString.indexOf("{"));

            typeString = typeString.replace(")", "");
            String[][] parsedArray = parseArray(typeString);

            primitive = parsePrimitiveType(parsedArray[0][0], useDedaultType);
            if (primitive == null) {
                return null;
            }

            if (parsedArray.length == 1) {
                // vector
                List<Expression> dims = new ArrayList<Expression>();
                dims.add(new IntegerExpression(parsedArray[0].length));
                // in this case no primitive type is defined. TRealDouble is
                // assigned by default (see TF 510)
                result = new TArray(dims, (TPrimitive) primitive);

            } else if (parsedArray[0].length == 1) {

                List<Expression> dims = new ArrayList<Expression>();
                dims.add(new IntegerExpression(parsedArray.length));

                result = new TArray(dims, (TPrimitive) primitive);
            } else if (parsedArray.length > 1) {
                // matrix
                List<Expression> dims = new ArrayList<Expression>();
                dims.add(new IntegerExpression(parsedArray.length));
                dims.add(new IntegerExpression(parsedArray[0].length));

                result = new TArray(dims, (TPrimitive) primitive);
            } else {
                result = primitive;
            }
        } else {
            return parsePrimitiveType(typeString, useDedaultType);
        }

        return result;
    }

    /**
     * Converts a Simulink type string to a GADataType. If the type is numeric,
     * then it is assumed to be a real numeric type (not complex).
     * 
     * When type is not explicitly set, assumes TRealDouble
     * 
     * @param typeString
     * @return
     */
    public static TPrimitive parsePrimitiveType(String typeString) {
        return parsePrimitiveType(typeString, true, true);
    }

    /**
     * Converts a Simulink type string to a GADataType. If the type is numeric,
     * then it is assumed to be a real numeric type (not complex).
     * 
     * @param typeString
     * @param useDefaultType
     *            if true, TRealDouble is returned in case the type is not
     *            explicitly set. IN case of false value, the method returns
     *            null value
     * @return
     */
    public static TPrimitive parsePrimitiveType(String typeString,
            boolean useDefaultType) {
        return parsePrimitiveType(typeString, true, useDefaultType);
    }

    /**
     * Converts a Simulink type string to a GADataType.
     * 
     * @param typeString
     * @param realNumeric
     *            if true and the type is numeric, then RealNumeric is assumed.
     *            If false, and the type is numeric, then ComplexNumeric is
     *            assumed.
     * @param useDefaultType
     *            if true, TRealDouble or TCompexDouble is returned in case the
     *            type string is not one of the fixed type strings if not,
     *            returns null and raises an error
     * @return
     */
    public static TPrimitive parsePrimitiveType(String typeString,
            boolean realNumeric, boolean useDefaultType) {
        TPrimitive result = null;
        int nbits = 0;
        boolean signed = true;

        typeString = typeString.trim();
        typeString = typeString.toLowerCase();
        // TODO (101110 AnTo) "true" and "false" are values, but the rest of the 
        // accepted strings are type names. Check, if it is possible to remove this
        // inconsistency
        if (typeString.equals("string")) {
            result = new TString();        
        } else if (typeString.equals("true")) {
            result = new TBoolean();
        } else if (typeString.equals("false")) {
            result = new TBoolean();
        } else {
            // (101110 AnTo) why are we checking for "contains"? 
        	// It should be "equals" at this point
            if (typeString.contains("uint8")) {
                nbits = 8;
                signed = false;
            } else if (typeString.contains("int8")) {
                nbits = 8;
            } else if (typeString.contains("uint16")) {
                nbits = 16;
                signed = false;
            } else if (typeString.contains("int16")) {
                nbits = 16;
            } else if (typeString.contains("uint32")) {
                nbits = 32;
                signed = false;
            } else if (typeString.contains("int32")) {
                nbits = 32;
            }

            if (nbits != 0) {
                if (realNumeric) {
                    result = new TRealInteger(nbits, signed);
                } else {
                    result = new TComplexInteger(nbits, signed);
                }
            } else {
                if (typeString.contains("single")) {
                    if (realNumeric) {
                        result = new TRealSingle();
                    } else {
                        result = new TComplexSingle();
                    }
                } else if (typeString.contains("double")) {
                    if (realNumeric) {
                        result = new TRealDouble();
                    } else {
                        result = new TComplexDouble();
                    }
                } else if (typeString.contains("boolean")) {
                    result = new TBoolean();
                } else if (useDefaultType) {
                    if (realNumeric) {
                        result = new TRealInteger();
                        Triple<Integer, Boolean, TRealInteger> triple = TRealInteger
                                .parseInteger(typeString, result);
                        if (triple != null) {
                        	result = triple.getRight();
                        } else {
                            result = new TRealDouble();
                        }
                    } else {
                        result = new TComplexDouble();
                    }
                } else {
                    result = null;
                }

            }
        }

        return result;
    }

    /**
     * Method which role is to parse an array of values, and to create a Matrix
     * containing the arrayString values.
     * 
     * @param arrayString
     *            the String which is to be parsed.
     * @return a matrix containing all values of the String array sorted in a
     *         matrix form.
     */
    public static String[][] parseArray(final String arrayString) {
        String[][] result = null;
        String tmpValue = arrayString;
        if (tmpValue.contains("[") || tmpValue.contains("]")
                || tmpValue.contains("{") || tmpValue.contains("}")) {

            tmpValue = tmpValue.replace("[", "");
            tmpValue = tmpValue.replace("]", "");
            tmpValue = tmpValue.replace("{", "");
            tmpValue = tmpValue.replace("}", "");

            String[] rows = tmpValue.split(";");
            String[] columns = parseMatrixColumn(rows[0]);
            result = new String[rows.length][columns.length];

            int numCols = columns.length;

            for (int rowCounter = 0; rowCounter < rows.length; rowCounter++) {
                columns = parseMatrixColumn(rows[rowCounter]);
                int tmpNumCols = (columns.length < numCols) ? columns.length
                        : numCols;
                for (int colCounter = 0; colCounter < tmpNumCols; colCounter++) {
                    result[rowCounter][colCounter] = columns[colCounter];
                }
            }
        } else {
            result = new String[1][1];
            result[0][0] = arrayString;
        }
        return result;
    }

    /**
     * 3 options : a - the separator is " " b - the separator is "," c - the
     * line has following format "init : increment : finalValue" examples
     * "1:3:10 15 20 25" will return an array containing 1 3 5 7 9 15 20 25
     * "1:3:10, 15, 20, 25" will return an array containing 1 3 5 7 9 15 20 25
     * "1:3:10, 15 20, 25" will return an array containing 1 3 5 7 9 15 20 25
     * 
     * @param column
     *            the column which is to be parsed
     * @return an array in which the different column values are stored.
     */
    private static String[] parseMatrixColumn(String column) {
        List<String> tmpResult = new ArrayList<String>();

        // first we parse according to the spaces
        String[] spaceSplit = column.split(" ");
        for (int spaceSplitCntr = 0; spaceSplitCntr < spaceSplit.length; spaceSplitCntr++) {
            String spaceSplitted = spaceSplit[spaceSplitCntr];
            String[] comaSplit = spaceSplitted.split(",");
            for (int comaSplitCntr = 0; comaSplitCntr < comaSplit.length; comaSplitCntr++) {
                String comaSplitted = comaSplit[comaSplitCntr];
                String[] twoPointsSplit = comaSplitted.split(":");
                if (twoPointsSplit.length == 3) {
                    tmpResult.addAll(getIncrementedValues(twoPointsSplit[0],
                            twoPointsSplit[1], twoPointsSplit[2]));
                } else {
                    if (!"".equals(comaSplitted)) {
                        tmpResult.add(comaSplitted);
                    }
                }
            }
        }

        String[] result = new String[tmpResult.size()];
        int index = 0;
        for (String str : tmpResult) {
            result[index] = str;
            index++;
        }
        return result;
    }

    /**
     * Method which returns an array containing all the values from an
     * incremented vector init:increment:final
     * 
     * @param init
     *            the initial value
     * @param increment
     *            the value of the increment
     * @param finalvalue
     *            the final value
     * @return example : 0:2:10 will return 0,2,4,6,8,10
     */
    private static List<String> getIncrementedValues(String init,
            String increment, String finalvalue) {
        List<String> result = new ArrayList<String>();

        // 2 possibilities : double or int in our case
        if (init.contains(".")) {
            double initDouble = new Double(init);
            double incrementDouble = new Double(increment);
            double finalvalueDouble = new Double(finalvalue);

            for (double count = initDouble; count <= finalvalueDouble; count = count
                    + incrementDouble) {
                result.add((new Double(count)).toString());
            }
        } else {
            int initInteger = new Integer(init);
            int incrementInteger = new Integer(increment);
            int finalvalueInteger = new Integer(finalvalue);

            for (int count = initInteger; count <= finalvalueInteger; count = count
                    + incrementInteger) {
                result.add((new Integer(count)).toString());
            }
        }

        return result;
    }

    /**
     * Method which indicates if the type is a TBoolean or has a TBoolean
     * primitive type.
     * 
     * @param type
     *            the type we want to chck
     * @return true if type is a TBoolean
     */
    public static boolean hasBooleanDataType(GADataType type) {
        return ((type instanceof TBoolean) || ((type.getPrimitiveType() != null) && (type
                .getPrimitiveType() instanceof TBoolean)));
    }

    /**
     * Method which returns the i-th dimension of a given GADataType.
     * 
     * @param datatype
     *            the datatype from which we want to access the X dimension
     * @return -1 if the datatype is a primitive type, or the X dimension if the
     *         datatype is a TArray or a TArray
     */
    public static int getIDimension(GADataType datatype, int i) {
        int result = -1;

        if (datatype instanceof TArray) {
            TArray array = (TArray) datatype;
            if (array.getDimensions().get(i) instanceof IntegerExpression) {
                IntegerExpression expr = ((IntegerExpression) array
                        .getDimensions().get(i));
                result = (new Integer(expr.getLitValue())).intValue();
            }

        }

        return result;
    }

    /**
     * Method which returns the X dimension (first dimension) of a given
     * GADataType.
     * 
     * TODO: check if this method is still useful. getDimensionslengt() 
     * is method of GADataType, so it can be directly called on each 
     * data type element
     * 
     * @param datatype
     *            the datatype from which we want to access the X dimension
     * @return -1 if the datatype is a primitive type, or the X dimension if the
     *         datatype is an instance of TArray
     */
    public static int getXDimension(GADataType datatype) {
        int result = -1;

        if (datatype instanceof TArray) {
            TArray array = (TArray) datatype;
            result = array.getDimensionsLength(0);
        }
        
        return result;
    }

    /**
     * Method which returns the Y dimension of a given GADataType.
     *
     * TODO: check if this method is still useful. getDimensionslengt() 
     * is method of GADataType, so it can be directly called on each 
     * data type element

     * @param datatype
     *            the datatype from which we want to access the Y dimension
     * @return -1 if the datatype is a primitive type or a TArray, or the Y
     *         dimension if the datatype is a TArray
     */
    public static int getYDimension(GADataType datatype) {
        int result = -1;

        if (datatype.isMatrix()) {
            TArray matrix = (TArray) datatype;
            result = matrix.getDimensionsLength(1);
        }

        return result;
    }

    /**
     * Method which returns the given datatype more readable.
     * 
     * @param datatype
     *            the datatype we want to display on screen.
     * @return the String corresponding to the datatype argument.
     */
    public static String toString(GADataType datatype) {
        try {
            return datatype.toString();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Method which, given a real datatype, returns the corresponding complex
     * type.
     * 
     * @param real
     *            the real type to transform
     * @return the corresponding complex type.
     */
    public static GADataType switchRealComplex(GADataType real) {
        GADataType result = null;

        Object tmp = PrivilegedAccessor.getObjectForClassName(real.getClass()
                .getName().replace("Real", "Complex"));

        if (tmp instanceof TComplexNumeric) {
            result = (GADataType) tmp;
            for (Field field : PrivilegedAccessor.getClassFields(real
                    .getClass())) {
                PrivilegedAccessor.setValue(result, field.getName(),
                        PrivilegedAccessor.getValue(real, field.getName()), true);
            }
        }

        return result;
    }

    /**
     * Method which, given a complex datatype, returns the corresponding real
     * type.
     * 
     * @param complex
     *            the complex type to transform
     * @return the corresponding real type.
     */
    public static GADataType switchComplexReal(GADataType complex) {
        GADataType result = null;

        Object tmp = PrivilegedAccessor.getObjectForClassName(complex
                .getClass().getName().replace("Complex", "Real"));

        if (tmp instanceof TRealNumeric) {
            result = (GADataType) tmp;
            for (Field field : PrivilegedAccessor.getClassFields(complex
                    .getClass())) {
                PrivilegedAccessor.setValue(result, field.getName(),
                        PrivilegedAccessor.getValue(complex, field.getName()), true);
            }
        }

        return result;
    }

    /**
     * Method which returns true if the two types given as parameter have the
     * same dimensions.
     * 
     * @param typea
     *            the first type to compare.
     * @param typeb
     *            the second type to compare.
     * @return true if both types have the same dimensions.
     */
    public static boolean checkDimensions(GADataType typea, GADataType typeb) {
        boolean result = false;
        if (typea.isScalar() && typeb.isScalar()) {
            return ((TPrimitive) typea).isSubType((TPrimitive) typeb)
                    || ((TPrimitive) typeb).isSubType((TPrimitive) typea);
        } else if (typea.getClass().isInstance(typeb)
                || typeb.getClass().isInstance(typea)) {
            if (typea.getDimensions().size() == typeb.getDimensions().size()) {
                result = true;
                for (int i = 0; i < typea.getDimensions().size(); i++) {
                    result = result
                            && (DataTypeAccessor.getIDimension(typea, i) == DataTypeAccessor
                                    .getIDimension(typeb, i));
                }
            } else {
                result = false;
            }
        }

        return result;
    }

    /**
     * Method which returns true if the two types given as parameter have the
     * same primitive types.
     * 
     * @param typea
     *            the first type to compare.
     * @param typeb
     *            the second type to compare.
     * @return true if both types have the same dimensions.
     */
    public static boolean checkPrimitiveTypes(GADataType typea, GADataType typeb) {
        boolean result = false;

        if (typea == null && typeb == null) {
            EventHandler.handle(EventLevel.ERROR,
                    "DataTypeAccessor.checkPrimitiveTypes", "",
                    "Comparison both arguments are NULL", "");
            return false;
        }
        if (typea == null && typeb != null) {
            EventHandler.handle(EventLevel.ERROR,
                    "DataTypeAccessor.checkPrimitiveTypes", "",
                    "Comparison first argument is NULL, "
                            + " second argument is "
                            + typeb.getReferenceString(), "");
            return false;
        }
        if (typea != null && typeb == null) {
            EventHandler.handle(EventLevel.ERROR,
                    "DataTypeAccessor.checkPrimitiveTypes", "",
                    "Comparison second argument is NULL, "
                            + " first argument is "
                            + typea.getReferenceString(), "");
            return false;
        }

        TPrimitive primTypeA = typea.getPrimitiveType();
        TPrimitive primTypeB = typeb.getPrimitiveType();

        /*
         * TODO replaced by AnRo result =
         * primTypeA.getClass().getSimpleName().equals(
         * primTypeB.getClass().getSimpleName());
         */
        result = primTypeA.isSubType(primTypeB)
                || primTypeB.isSubType(primTypeA);
        return result;
    }

    /**
     * Returns the primitive type of a given port.
     * 
     * @param port
     *            the port whose type we want to get.
     * @return the primitive type of port.
     */
    public static TPrimitive getPrimitiveType(InDataPort port) {
        TPrimitive primitiveType = null;

        if (port.getDataType() instanceof TPrimitive) {
            primitiveType = (TPrimitive) port.getDataType();
        }
        if (port.getDataType() instanceof TArray) {
            primitiveType = port.getDataType().getPrimitiveType();
        }

        return primitiveType;
    }

    /**
     * Returns the length of the vector data type. if the input is a scalar then
     * returns 1. If input is matrix, then return -1 => error event to be
     * raised.
     * 
     * @param port
     *            the port whose length we want to get.
     * @return the length of the type of port.
     */
    public static int getTypeLength(InDataPort port) {
        int length = 0;

        if (port.getDataType() instanceof TPrimitive) {
            length++;
        }
        if (port.getDataType().isVector()) {
            TArray vector = (TArray) port.getDataType();
            LiteralExpression lengthExpr = (LiteralExpression) vector
                    .getDimensions().get(0);
            length = length
                    + (new Integer(lengthExpr.getLitValue())).intValue();
        }
        if ((port.getDataType() instanceof TArray)
                && (port.getDataType()).isVectorMatrix()) {
            // TODO Implement getMaxDimLength() for TArray or something like
            // that to avoid unnecessary creating and discarding of the vector type.
            // Also, not sure we need the normalisation here. But, if it is used we 
            // must make a getCopy() first to avoid damaging the original type.
            // (AnTo 0901014)
            GADataType vectorDT = port.getDataType().getCopy().normalize(true);
            length = vectorDT.getDimensionsLengths()[0];
        }

        return length;
    }

    /**
     * Checks if the ports given as arguments are all typed
     * 
     * @param inPorts
     * @return, true if all ports are types
     */
    public static boolean checkTyping(List<InDataPort> inPorts) {
        boolean result = true;
        for (InDataPort currentInPort : inPorts) {
            if (currentInPort == null) {
                result = false;
            } else if (currentInPort.getDataType() == null) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Checks if the ports given as arguments have the same type
     * 
     * @param inPorts
     * @return, true if all ports have the same type
     */
    public static boolean checkSameTyping(List<InDataPort> inPorts) {
        boolean result = true;
        if (checkTyping(inPorts)) {
            GADataType firstType = inPorts.get(0).getDataType();
            for (InDataPort currentInPort : inPorts) {
                if (!currentInPort.getDataType().getTypeName().equals(
                        firstType.getTypeName())) {
                    result = false;
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Checks if the ports given as arguments have the same primitive type
     * 
     * @param inPorts
     * @return, true if all ports have the same type
     */
    public static boolean checkSamePrimitiveTyping(List<InDataPort> inPorts) {
        if (checkTyping(inPorts)) {
            GADataType firstType = null;
            for (InDataPort currentInPort : inPorts) {
                if (firstType == null) {
                    if (currentInPort.getDataType() instanceof TPrimitive) {
                        firstType = currentInPort.getDataType();
                    }
                    if (currentInPort.getDataType().isVector()) {
                        firstType = ((TArray) currentInPort.getDataType())
                                .getBaseType();
                    }
                    if (currentInPort.getDataType().isMatrix()) {
                        firstType = ((TArray) currentInPort.getDataType())
                                .getBaseType();
                    }
                } else {
                    if (currentInPort.getDataType() instanceof TPrimitive) {
                        if (!currentInPort.getDataType().getTypeName().equals(
                                firstType.getTypeName())) {
                            return false;
                        }
                    }
                    if (currentInPort.getDataType().isVector()) {
                        if (!((TArray) currentInPort.getDataType())
                                .getBaseType().getTypeName().equals(
                                        firstType.getTypeName())) {
                            return false;
                        }
                    }
                    if (currentInPort.getDataType().isMatrix()) {
                        if (!((TArray) currentInPort.getDataType())
                                .getBaseType().getTypeName().equals(
                                        firstType.getTypeName())) {
                            return false;
                        }
                    }
                }
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Checks if the block given as argument has the correct numbers of input
     * ports (nbI) and output ports (nbO)
     * 
     * @param i,
     *            the number of input ports
     * @param j,
     *            the number of output ports
     * @return, true if the Block has the right number of inputs and outputs
     */
    public static boolean checkNbIO(Block block, int nbI, int nbO) {
        boolean result = true;
        if (block.getInDataPorts() == null) {
            result = false;
        } else if (block.getOutDataPorts() == null) {
            result = false;
        } else {
            if ((block.getInDataPorts().size() != nbI)
                    || (block.getOutDataPorts().size() != nbO)) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Check if a block has at least one input port with TArray data type
     * 
     * @param block,
     *            the block to check
     * @return true if block has at least one input port with TArray data type
     */
    public static boolean hasComplexTypeInput(Block block) {
        boolean result = false;
        for (InDataPort currentInPort : block.getInDataPorts()) {
            if (currentInPort.getDataType() instanceof TArray) {
                result = true;
            }
        }
        return result;
    }

    /**
     * check if s start with a numeric value or with a minus operator (-)
     * following by a numeric value
     * 
     * @param s
     * @return
     */
    public static boolean isNumeric(String s) {
        if (s.matches("\\[.*") || s.matches("\\d.*") || s.matches("-\\d.*")) {
            return true;
        } else
            return false;

    }

    public static boolean compare(GADataType datatype_1, GADataType datatype_2) {
        boolean result = true;

        if (datatype_1 != null && datatype_2 != null) {
            if (!(datatype_1.getClass().getSimpleName().equals(datatype_2
                    .getClass().getSimpleName()))) {
                result = false;
            }

            if (datatype_1 instanceof TArray && datatype_2 instanceof TArray) {

                int[] dims1 = ((TArray) datatype_1).getDimensionsLengths();
                int[] dims2 = ((TArray) datatype_2).getDimensionsLengths();

                if (dims1.length != dims2.length) {
                    result = false;
                } else {
                    for (int i = 0; i < dims1.length; i++) {
                        if (dims1[i] != dims2[i]) {
                            result = false;
                        }

                    }
                }

                if (!compare(((TArray) datatype_1).getPrimitiveType(),
                        ((TArray) datatype_2).getPrimitiveType())) {
                    result = false;
                }
            }
        } else {
            result = false;
        }

        return result;
    }

}
