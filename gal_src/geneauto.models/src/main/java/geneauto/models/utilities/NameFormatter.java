/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/NameFormatter.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.models.gacodemodel.NameSpaceElement_CM;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NameFormatter {
    /**
     * Tests if input string is legal C identifier
     *  - name starts with a letter or underscore
     *  - name is composed of only letters, numbers and underscores
     * 
     * @param s -- string to test
     * @return boolean
     */
    public boolean isLegalIdentifier(String s){
        Pattern p = Pattern.compile("[_a-zA-Z][0-9._a-zA-Z]*");
        Matcher matcher = p.matcher(s);
        return matcher.matches();
    }
    
    /**
     * Takes identifier name as input and prefixes the name with "_"
     * in case it starts with a number
     * @param s -- string to to be checked 
     * @return udpated string
     */
    
    public String prefixLeadingNumber(String s){
        if(s.matches("[0-9][0-9._a-zA-Z]*"))
            s = "_" + s;
        
        return s;
    }

    /**
     * Normalises function name:
     *  - replaces illegal symbols with _
     *  - reports error when name starts with a number
     *  
     * @param function that needs to be normalised
     * @return String -- normalised names
     */
    public String normaliseFunctionName(GACodeModelElement f){
        String name = f.getName();

        // remove newlines
        name = name.replace("\\n", "_");
        
        // accepted symbols for function name
        Pattern p = Pattern.compile("[^0-9_a-zA-Z]");
        Matcher matcher = p.matcher(name);
        name = matcher.replaceAll("_");
        
        // if identifier starts with a number, prefix it
        name = prefixLeadingNumber(name);
        
        if (!isLegalIdentifier(name)){
            EventHandler.handle(EventLevel.ERROR, 
                    "normaliseFunctionName", 
                    "CP0084", 
                    "Illegal name for a function: \"" 
                    + name + "\" in "
                    + f.getReferenceString(), 
                    "");
            return "";
        }
        
        return name;
    }
    
    /**
     * Normalises variable name:
     *  - replaces illegal symbols with _
     *  - reports error when name starts with a number
     *  
     * @param namespaceeleemnt that needs to be normalised
     * @return String -- normalised name
     */
    public String normaliseVariableName(NameSpaceElement_CM v){
        String name = v.getName();
        
        // remove newlines
        name = name.replace("\\n", "_");
        
        // accepted symbols for variable/type name
        Pattern p = Pattern.compile("[^0-9._a-zA-Z]");
        Matcher matcher = p.matcher(name);
        name = matcher.replaceAll("_");

        // if identifier starts with a number, prefix it
        name = prefixLeadingNumber(name);

        if (!isLegalIdentifier(name)){
            EventHandler.handle(EventLevel.ERROR, 
                    "normaliseVariableName", 
                    "GEM0085", 
                    "Illegal name for a variable: \"" 
                    + name + "\" in "
                    + v.getReferenceString(), 
                    "");
            return "";
        }
        
        return name;
    }

    /**
     * Normalises file name:
     *  - replaces illegal symbols with _
     *  
     * @param string that needs to be normalised
     * @return String -- normalised name
     */
    public String normaliseFileName(String name){
        // remove newlines
        name = name.replace("\\n", "_");
        
        // accepted symbols for variable/type name
        Pattern p = Pattern.compile("[^0-9._a-zA-Z]");
        Matcher matcher = p.matcher(name);
        name = matcher.replaceAll("_");
        
        // if identifier starts with a number, prefix it
        name = prefixLeadingNumber(name);
        
        if (!isLegalIdentifier(name)){
            EventHandler.handle(EventLevel.ERROR, 
                    "normaliseVariableName", 
                    "CP0083", 
                    "Illegal name for a file: \""
                    + name + "\"", 
                    "");
            return null;
        }
        
        return name;
    }
}
