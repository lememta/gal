package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.GACodeModel;
import geneauto.models.gacodemodel.GACodeModelRoot;
import geneauto.models.gacodemodel.Module;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.NameSpaceElement_CM;
import geneauto.models.gacodemodel.TempModel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.genericmodel.GAModelElement;

import java.util.HashSet;
import java.util.Set;

public class CodeModelValidator {

	/**
	 * Checks integrity of variable references.
	 * 
	 * NOTE! THIS METHOD IS FOR DEBUGGING ONLY!
	 * The current implementation is very inefficient.
	 * 
	 * If all checks pass, the method returns quietly. Otherwise it raises an
	 * ERROR event for each error (up to the max allowed) and finally throws 
	 * a RunTimeException that displays the stack trace.
	 * 
	 * @param cm CodeModel
	 */
	public static void assertVariableReferences(GACodeModel cm) {
		boolean errors = false;
		boolean found;	
		// Collect global variables to a set
		Set<Variable_CM> globalVars = new HashSet<Variable_CM>();
		for (Module m : cm.getModules()) {
			for (NameSpaceElement_CM nsEl : m.getNameSpace().getNsElements())
				if (nsEl instanceof Variable_CM) {
					globalVars.add((Variable_CM) nsEl);
				}			
		}
		// Extra top loop to avoid looking into TempModel
		for (GACodeModelRoot rootEl : cm.getElements()) {
			if (!(rootEl instanceof TempModel)) {
				for (GAModelElement el : ((GAModelElement) rootEl)
						.getAllChildren(VariableExpression.class)) {
					VariableExpression exp = ((VariableExpression) el);
					Variable v = exp.getVariable();
					if (v == null) {
						continue;
					}
					if (!(v instanceof Variable_CM)) {
						EventHandler.handle(EventLevel.ERROR, "assertVariableReferences", 
								"", "Wrong kind of variable detected in the CodeModel." +
										"\n  Variable: " + v.getReferenceString() +
										"\n  Type: " + v.getClass().getName());
						continue;
					}
					Variable_CM cmv = (Variable_CM) v;
					// Climb up the code model tree to find if the element is visible.
					// This is inefficient to do it one-by-one, but it is for debugging only!
					EventHandler.handle(EventLevel.INFO, "assertVariableReferences", 
							"", "\n  Resolving reference: " + exp.getReferenceString()
							  + "\n  Resolved to:         " + v.getReferenceString());			
					NameSpace ns = exp.getContainingNameSpace();
					found = false;
					while (ns != null) {
						if (ns.getNsElements().contains(v)) {
							// Found referenced variable
							found = true;
							break;
						} else {
							// Climb further
							ns = ns.getContainingNameSpace();
						}
					}
					if (!found) {
						// Reached top, without finding the variable - Check global variables
						found = globalVars.contains(cmv);						
					}
					if (!found) {
						// Not found, i.e. not visible
						EventHandler.handle(EventLevel.ERROR, "assertVariableReferences", 
								"", "\n  Variable           : " + cmv.getReferenceString() + 
								    "\n  is not visible from: " + exp.getReferenceString());
						errors = true;				
					}
				}
			}
		}
		if (errors) {
			throw new RuntimeException("Code model integrity errors detected!");
		}
	}
}
