/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/AutomaticNameResolver.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */

package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.genericmodel.GAModelElement;

import java.util.HashSet;

/**
 * Class that generates unique names for temporary variables and temporary variables
 * 
 * 
 * TODO (to AnRo) This class should be separate from IteratorHandler.
 * IteratorHandler can use NameResolver, not vice versa. 
 * 
 */
public class AutomaticNameResolver {
    
    /**
     * Set of all variable names of current nameSpace
     */
    HashSet<String> varNames;
    /**
     * name space of current resolver
     */
    NameSpace nameSpace;
    VariableIteratorHandler varIter;
    
    public AutomaticNameResolver() {
    }

    public AutomaticNameResolver(NameSpace ns) {
        this();
        this.nameSpace = ns;
        varIter = new VariableIteratorHandler();
    }
    /**
     * 
     * @return list of the variables' names of the module
     */
    public HashSet<String> getVarNames() {
        
        // list is asked for the first time -> add all
        // names of variables from the module
        if (varNames == null) {
            varNames = new HashSet<String>();
            for (GAModelElement var: nameSpace.
                    getAllChildren(Variable_CM.class)) {
                varNames.add(((Variable_CM) var).getName());
            }
        }
        return varNames;
    }

    /**
     * 
     * @param baseName
     *            nameRoot of the temporary variable, e.g. "i"
     * @return unique variable (if counter value is less then 
     * size of tempVarList -> creates new Variable
     */
    public String getUniqueVarName(String baseName) {
        if (varIter != null) {
            baseName = baseName + varIter.getCounter();
        }
        String newName = null;
        int i;
        if (!getVarNames().contains(baseName)) {
            newName = baseName;
        } else {

            // if baseName is not unique name -> 
            // add suffix _<i> (i is in interval 1 to MAX_VALUE) and 
            // check uniqueness again
            for (i = 1; i < Integer.MAX_VALUE; i++) {
                newName = baseName + "_" + i;
                if (!getVarNames().contains(newName)) {
                    break;
                }
                if (i == Integer.MAX_VALUE) {
                    EventHandler.handle(EventLevel.ERROR, "getUniqueVarName", "",
                            "Cannot compute an unique name for base name: " + baseName
                            + ". Maximum iteration count exceeded.", "");
                    return null;
                }
            }
        }
        return newName;
    }
    
    public void deleteAllTempVars() {
        this.varIter = new VariableIteratorHandler();
    }

    public NameSpace getNameSpace() {
        return nameSpace;
    }

    public VariableIteratorHandler getVarIter() {
        return varIter;
    }

    
    public Variable_CM getUniqueVar() {
        return varIter.getNext();
    }
    
    public Variable_CM addVariable(Variable_CM var) {
        var.setName(getUniqueVarName(var.getName()));
        varIter.addIterVar(var);
        return var;
    }


}
