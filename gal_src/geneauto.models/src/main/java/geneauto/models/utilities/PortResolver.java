/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/PortResolver.java,v $
 *  @version	$Revision: 1.26 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.GASystemModelRoot;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ControlBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Port;
import geneauto.models.genericmodel.GAModelElement;

import java.util.List;

/**
 * Utility class for performing port transformations
 * 
 *
 */
public class PortResolver {

	/*
     * Cycles all blocks in the model for outControlPorts
     * Checks if all the ports in the signal chain starting from this
     * control port are of type InControlPort
     * If not, replaces ports in the end of signals
     */
    public static void feedForwardControlPorts(GASystemModel systemModel) {

    	Block parentBlock;
    	/**
    	 * find all OutControlPort
    	 */
    	for (GAModelElement startPort : systemModel.getAllElements(OutControlPort.class)){
    		// port can only belong to block, so it is safe to cast
    		parentBlock = (Block) startPort.getParent();
    		
			// find signals starting from this block
			for (Signal s : ((SystemBlock) parentBlock.getParent()).getSignals()){
				if (s.getSrcPort() == startPort){
					// replace destination port with control port 
					// (does nothing if output port is already control port 
					replaceInputWithControlPort(s.getDstPort());
				}
			}
    	}
    }

	/*
     * Cycles all blocks in the model for inControlPorts
     * Checks if all the ports in the signal chain ending in this
     * control port are of type OutControlPort
     * If not, replaces ports in the beginning of signals
     */
    public static void backpropagateControlPorts(GASystemModel systemModel) {

    	Block parentBlock;
    	/**
    	 * find all OutControlPort
    	 */
    	for (GAModelElement endPort : systemModel.getAllElements(InControlPort.class)){
    		// port can only belong to block, so it is safe to cast
    		parentBlock = (Block) endPort.getParent();
    		
			// find signals ending in this block
			for (Signal s : ((SystemBlock) parentBlock.getParent()).getSignals()){
				if (s.getDstPort() == endPort){
					// replace destination port with control port 
					// (does nothing if output port is already control port 
					replaceOutputWithControlPort(s.getSrcPort());
				}
			}
    	}
    }

    /**
     * Checks if top level system has data ports blocks that end in control
     * port. If there is, replaces the output port of Inport block with 
     * control port
     * 
     * @param system
     */
    public static void checkSystemInControlPorts(SystemBlock system) {
    	
    	// cycle all signals and check if any of them ends in control port
    	// but starts in data port
    	for (Signal s : system.getSignals()) {
    		if (s.getDstPort() instanceof InControlPort
    				&& !(s.getSrcPort() instanceof OutControlPort)){
    			replaceOutputWithControlPort(s.getSrcPort());
    		}
    	}
    }
    /**
     * takes a port as an input, replaces this port with InCon
     * Reconnects all signals
     * 
     * @param oldPort -- port to be replaced
     * @return newly created port (or oldPort in case it was already of correct 
     * type)
     */
    public static InControlPort replaceInputWithControlPort(Inport oldPort){
    	
    	/*
    	 * In top-level subsystem the enable and control ports are not present
    	 * Until this is fixed, the code generation with function call to 
    	 * top-level subsystem fails. 
    	 * FIXME: remove after ports are correctly imported in top-level 
    	 * subsystem (ToNa 05/07/09)
    	 */
    	if (oldPort == null) {
    		return null;
    	}
    	
    	Block parentBlock = (Block) oldPort.getParent();
    	InControlPort newPort = null;
    	if (oldPort instanceof InControlPort){
    		// everything OK, just set correct return value
    		newPort = (InControlPort) oldPort;
    	} else {
	    	newPort = new InControlPort();
	    	SystemBlock parentSystem = (SystemBlock) oldPort.getParent().getParent();
	
	    	// copy matching attribute values
	    	ModelUtilities.transferContent(oldPort, newPort);
	    	// release old port (un-sets its parent and removes from parent block)
	    	parentBlock.removeChild(oldPort);
	    	// sets new control port to block
	    	parentBlock.addInControlPort(newPort);
	    	
	    	// find, if oldPort has any signals. Connect them to new port
	    	int signalCnt = 0;
	    	for (Signal s : parentSystem.getSignals()){
	    		if (s.getDstPort() == oldPort){
	    			s.setDstPort(newPort);
	    			signalCnt++;
	    			if (signalCnt > 1){
	    	            EventHandler.handle(
	    	            		EventLevel.ERROR,
	    	                    "HandlePortsState", 
	    	                    "",
	    	                    "InControlPort " + oldPort.getReferenceString()
	    	                    + "has more than one incoming signal", 
	    	                    "");
	    			}
	    		}
	    	}	    	
    	}
    	
    	// function call goes outside of subsystem. Make sure the output port
    	// is OutControlPort
    	if (parentBlock instanceof SinkBlock &&
    			parentBlock.getType().equals("Outport")){
    		if (parentBlock.getParent().equals(parentBlock.getTopLevelParent())) {
    			EventHandler.handle(
    					EventLevel.CRITICAL_ERROR, 
    					"replaceInputWithControlPort", 
    					"", "Control signal exiting the top-level system via " +
    							parentBlock.getReferenceString(), "");
    		}
    		Outport port = replaceOutputWithControlPort(
    				(Outport)parentBlock.getPortReference());
    		parentBlock.setPortReference(port);
    	}
    	
    	return newPort;
    	
    }

    /**
     * checks input port of an Outport block and if it is a data 
     * port replaces it with control port
     */
    private static void outportBlockToControl(Block outportBl){
    	
    	if (outportBl == null){
    		return;
    	}
    	
    	List<Inport> allPorts = outportBl.getAllInports();
    	
    	if (allPorts.size() != 1) {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					"outportBlockToControl", 
					"", 
					"Unexpected number (" + allPorts.size()
					+ ") of input ports in Outport block "
					+ outportBl.getReferenceString(), "");
			return;
    	}
    	
    	Inport port = allPorts.get(0);
    	if (port instanceof InControlPort) {
    		// port is already of correct type
    		return;
    	}
    	else {
    		replaceInputWithControlPort(port);
    	}	
    }
    
    /**
     * takes a port as an input, replaces this port with OutControlPort
     * Reconnects all signals
     * 
     * @param oldPort -- port to be replaced
     * @return newly created port (or oldPort in case it was already of correct 
     * type
     */
    public static OutControlPort replaceOutputWithControlPort(Outport oldPort){

    	// the port is already of correct type. Nothing to do
    	if (oldPort instanceof OutControlPort){
    		// if the port is output of a subsystem make sure the
    		// input of a Inport block is of correct type
    		outportBlockToControl(oldPort.getSourceBlock());
    		
    		return (OutControlPort) oldPort;
    	}
    	
    	Block parentBlock = (Block) oldPort.getParent();
    	SystemBlock parentSystem = null;
    	parentSystem = (SystemBlock) oldPort.getParent().getParent();
    	
    	OutControlPort newPort = new OutControlPort();
    	// copy matching attribute values
    	ModelUtilities.transferContent(oldPort, newPort);
    	// release old port (un-sets its parent and removes from parent block)
    	oldPort.removeMe();

    	// sets new control port to block
    	parentBlock.addOutControlPort(newPort);
    	
    	// find, if oldPort has any signals. Connect them to new port
    	for (Signal s : parentSystem.getSignals()){
    		if (s.getSrcPort() == oldPort){
    			s.setSrcPort(newPort);
    		}
    	}
    	
    	// if replacing output port of a sybsystem
    	if (newPort.getSourceBlock() != null){
        	// set the corresponding block pointing to the new port as well
        	newPort.getSourceBlock().setPortReference(newPort);
        	// make sure the sourceBlock has correct input port type
        	outportBlockToControl(newPort.getSourceBlock());
    	}
    	
    	return newPort;
    }

    /*
     * goes through all control paths and removes Mux blocks
     */
    public static void cleanControlPath(GASystemModel systemModel) {

        for (GAModelElement elem : systemModel
        							.getAllElements(CombinatorialBlock.class)) {
            // for each Mux block
            if (((Block) elem).getType().equals("Mux")) {
                removeMuxFromControlPath(((Block) elem));
            }
        }
    }

    /**
     * Searches for control signals starting from Mux block and replaces them 
     * with direct connection between source of the control signal coming to mux and 
     * target of the control signal. 
     * removes mux port in the end.
     *   
     * @param blo
     * @param systemModel
     */
    private static void removeMuxFromControlPath(Block blo) {
    	// parent of a block is always a system
    	SystemBlock parentSystem = (SystemBlock) blo.getParent();
    	if (parentSystem == null) {
    		// top level block was given as an input
    		return;
    	}
    	
    	// will be evaluated true, if the mux block either has 
    	// control input or outputs, or its output signal ends in 
    	// control port
    	// NB! here we assume that mux port never has mixed data and control 
    	// signals
    	boolean muxOnControlPath = false;
    	if (blo.getInControlPorts().size() > 0 ||
    			blo.getOutControlPorts().size() > 0) {
    		muxOnControlPath = true;
    	}
    		
        // normally Mux block has one output. We use getAllOutports to 
        // be sure that the algorithm works even if the back-propagation of
        // control ports has changed the port type
    	// there is only one outgoing signal. We will find it
        Signal outSignal = null;
        Outport outp = blo.getAllOutports().get(0);
    	for (Signal s : outp.getOutgoingSignals()) {
    		// outgoing signal is control signal when it has control port 
    		// at least in one end
    		if (muxOnControlPath ||
    				s.getDstPort() instanceof InControlPort){
    			outSignal = s;
    			muxOnControlPath = true;
    			break;
    		}
    	}

        if (outSignal == null) {
        	if (muxOnControlPath){
	            EventHandler.handle(EventLevel.CRITICAL_ERROR,
	                    "portresolver.removeMuxFromControlPath", 
	                    "",
	                    "Mux block " + blo.getReferenceString()
	                    + "\n on control path with no outgoing signals!", 
	                    "");
        	}
            return;
        }

        // record target block
        Block targetBlock = (Block) outSignal.getDstPort().getParent();
        Inport targetPort = outSignal.getDstPort();
        
        // go through input signals and reconnect their target
        for (Signal s : blo.getIncomingSignals()) {
    		// create new port
            InControlPort newInPort = new InControlPort();
            // transfer date from the old port
            ModelUtilities.transferContent(targetPort, newInPort);
            // attach port to target block
            targetBlock.addInControlPort(newInPort);
            // reroute the signal
            s.setDstPort(newInPort);
            // make sure the signal starts in in controlport
            replaceOutputWithControlPort(s.getSrcPort());
        }

        // remove the target port
        targetPort.removeMe();
        // remove old outgoing signal
        outSignal.removeMe();
        // remove the mux block
        blo.removeMe();
    }

    /**
     * finds all Inport and Outport blocks in a system and sets reference 
     * to associated port object
     * 
     * @param systemModel
     */
    public static void connectPortBlocks(GASystemModel systemModel) {
    	String blockType = null;

    	// output ports
    	for (GAModelElement b : systemModel.getAllElements(SinkBlock.class)) {
    		blockType = ((SinkBlock) b).getType();
    		if (blockType.equals("Outport")) {
    			connectPortAndBlock((SinkBlock) b);
    		}
    	}
    	
    	// input data ports
    	for (GAModelElement b : systemModel.getAllElements(SourceBlock.class)) {
    		blockType = ((SourceBlock) b).getType();
    		if (blockType.equals("Inport")){
    			connectPortAndBlock((SourceBlock) b);
    		}
    	}
    	
    	for (GAModelElement b : systemModel.getAllElements(ControlBlock.class)) {
    		blockType = ((ControlBlock) b).getType();
    		
    		if (blockType.equals("ActionPort")) {
    			setActionPortAttributes((Block) b);
    		} else if (blockType.equals("TriggerPort")) {
    			setTriggerPortAttributes((Block)b);
    		} else if (blockType.equals("EnablePort")) {
    			setEnablePortAttributes((Block)b);
    		}
    	}
    }

    /**
     * Finds all Inport and Outport blocks inside of a system and sets 
     * reference to associated port object.  
     * 
     * NB! This method does not recurse into the contents of a subsystem
     * it only processes the immediate children 
     * 
     * @param systemModel
     */
    public static void connectPortBlocks(SystemBlock parentSystem) {
    	String blockType = null;

    	// output ports
    	for (GAModelElement b : parentSystem.getChildren(SinkBlock.class)) {
    		blockType = ((SinkBlock) b).getType();
    		if (blockType.equals("Outport")) {
    			connectPortAndBlock((SinkBlock) b);
    		}
    	}
    	
    	// input data ports
    	for (GAModelElement b : parentSystem.getChildren(SourceBlock.class)) {
    		blockType = ((SourceBlock) b).getType();
    		if (blockType.equals("Inport")){
    			connectPortAndBlock((SourceBlock) b);
    		}
    	}
    	
    	for (GAModelElement b : parentSystem.getChildren(ControlBlock.class)) {
    		blockType = ((ControlBlock) b).getType();
    		
    		if (blockType.equals("ActionPort")) {
    			setActionPortAttributes((Block) b);
    		} else if (blockType.equals("TriggerPort")) {
    			setTriggerPortAttributes((Block)b);
    		} else if (blockType.equals("EnablePort")) {
    			setEnablePortAttributes((Block)b);
    		}
    	}
    }

    /**
     * Retrieves block parameter "Port" and converts its value to 
     * integer. In case the parameter does not exist returns 1 assuming 
     * that is it the first port in the subsystem that is nto always 
     * explicitly numbered
     * 
     * NB! The method should be used only for Inport and Outport blocks
     * The block type is not checked in the method
     *  
     * @param b		-- block to be checked (Block)
     * @return		-- port number (int)
     */
    private static int getPortNo(Block b) {
    	BlockParameter portNoPar = b.getParameterByName("Port");
		if (portNoPar == null){
			/* simulink may leave the Port property empty for the first 
			   port in the subsystem */ 
			return 1;
		} else {
			return Integer.parseInt(portNoPar.getStringValue());
		}

    	
    }
    
    /**
     * Takes a block (Inport or Outport) and a list of ports
     * Connects the block by port number to corresponding block
     * 
     * @param b			-- port block
     * @return port connected to the given block
     */
    private static Port connectPortAndBlock(Block b){
    	
    	if (b.getPortReference() != null) {
    		// the block is already attached to the block
    		return b.getPortReference();
    	}
    	
    	SystemBlock parentSystem = (SystemBlock) b.getParent();

    	// for robustness
    	if (parentSystem == null) {
            EventHandler.handle(EventLevel.ERROR,
                    "PortResolver.connectPortaAndBlock", "",
                    "Port blcok with no parent \n " + b.getReferenceString(),
                    "");    				
    		return null;
    	}
    	
    	int portNo = getPortNo(b);
    	String portTypeStr = "";
    	
		// find output port with the given number
    	Port p = null;
    	if (b instanceof SourceBlock) {
    		p = parentSystem.getInDataPortByNo(portNo);
    		portTypeStr = "input port";
    	} else if (b instanceof SinkBlock) {
    		p = parentSystem.getOutDataPortByNo(portNo);
    		portTypeStr = "output port";
    	} else {
            EventHandler.handle(EventLevel.ERROR,
                    "PortResolver.connectPortaAndBlock", "",
                    "incorrect block type \n " + b.getReferenceString(),
                    "");    				
    		return null;    		
    	}
    	
		if (p == null) {
			// raise error when port not found
	        EventHandler.handle(EventLevel.ERROR,
	                "PortResolver.connectPortaAndBlock", "",
	                "The parent of block " + b.getReferenceString()
	                + "\n does not have corresponding "
	                + portTypeStr 
	                + " with port number " + portNo, 
	                "");
	        return null;
		} else {
			// set cross-references between the port and the block 
			b.setPortReference(p);
			p.setSourceBlock(b);
			if (p instanceof InDataPort) {
				setInportAttributes((SourceBlock) b, (InDataPort) p);
			} else {
				setOutportAttributes(((SinkBlock) b), (OutDataPort) p);
			}
			
			return p;
    	}
		
    }

    /**
     * Transfers Inport block properties to InDataPort object attributes
     * Assumes, that port already has connected port object
     * 
     * @param inportBlo
     */
    private static void setInportAttributes(SourceBlock inportBlo, InDataPort port) {
    	// set port name
    	port.setName(inportBlo.getName());
    	
        // get port parameters
        Parameter paramDataType = inportBlo.getParameterByName("DataType");

        // transfer data type parameter
        if (paramDataType != null) {
            String dataType = paramDataType.getStringValue();
            if (!"auto".equals(dataType)) {
                GADataType type = DataTypeAccessor.getDataType(dataType);
                port.setDataType(type);
            }
        }
    }

    /**
     * Transfers Outport block properties to corresponding OutDataPort 
     * object attributes.
     * Assumes, that block already has connected port object
     * 
     * @param outportBlo
     */
    private static void setOutportAttributes(SinkBlock outportBlo, OutDataPort port) {
    	// set port name
    	port.setName(outportBlo.getName());
    	
        // get port parameters
        Parameter paramInitialOutput = 
        				outportBlo.getParameterByName("InitialOutput");
        Parameter paramResetOutput = 
        				outportBlo.getParameterByName("OutputWhenDisabled");

        // transfer initial output parameter
        if (paramInitialOutput != null && paramInitialOutput.getValue() != null) {
            port.setInitialOutput(paramInitialOutput.getValue().getCopy());
        }

        // transfer resetOutput parameter
        if (paramResetOutput != null) {
            String resetOutput = paramResetOutput.getStringValue();
            if (resetOutput.equals("reset")) {
                port.setResetOutput(true);
            } else if (resetOutput.equals("held")) {
                port.setResetOutput(false);
            } else {
                EventHandler.handle(
                            EventLevel.ERROR,
                            "HandlePortsState.replaceControlBlocks", 
                            "",
                            "Parameter 'OutputWhenDisabled' has " +
                            "illegal value \"" + resetOutput + "\" "
                            + "\n expexted values: [reset|held]"
                            + "\n block: " 
                            + outportBlo.getReferenceString(), 
                            "");
            }
        }
    }

    /**
     * Transfers ActionPort attributes to InControlPort object
     * Removes the ActionPort block
     * @param b
     */
    private static void setActionPortAttributes(Block b) {
    	SystemBlock system = (SystemBlock) b.getParent();
    	InControlPort port = system.getInControlPorts().get(0);
    	
        // transfer InitializeStates field to resetStates
        Parameter paramInitState = 
        				b.getParameterByName("InitializeStates");

        if (paramInitState == null) {
            EventHandler.handle(EventLevel.ERROR,
                    "HandleState.replaceControlBlocks", 
                    "",
                    "Parameter 'InitializeStates' not defined "
                    + "\n block: " + b.getReferenceString(), 
                    "");
        }

        String initState = paramInitState.getStringValue();

        if (initState.equals("reset")) {
            ((InControlPort) port).setResetStates(true);
        } else if (initState.equals("held")) {
        	((InControlPort) port).setResetStates(false);
        } else {
            EventHandler.handle(
                            EventLevel.ERROR,
                            "HandleState.replaceControlBlocks", 
                            "",
                            "Parameter 'InitializeStates' has illegal "
                            + "value \"" + initState + "\" "
                            + "\n expexted values: [reset|held]"
                            + "\n block: " + b.getReferenceString(), 
                            "");
        }

        // remove block from model
        b.getParent().removeChild(b); 
        port.setSourceBlock(null);
    }
    
    /**
     * Transfers EnablePort attributes to InEnablePort object
     * @param b
     */
    private static void setEnablePortAttributes(Block b) {
    	SystemBlock system = (SystemBlock) b.getParent();
		InEnablePort port = system.getInEnablePort();

		// set port object name
		port.setName(b.getName());
		
	    // transfer parameter "StatesWhenEnabling" value to inEnable
	    // port of the parent system block (true if state is reset,
	    // false otherwise
	    Parameter p  = b.getParameterByName("StatesWhenEnabling");
	    if (p != null) {
	        String paramValue = p.getStringValue();
	        if (paramValue.equals("reset")) {
	            ((InEnablePort) port).setResetStates(true);
	        } else {
	        	((InEnablePort) port).setResetStates(false);
	        }
	    }
	
	    // get "Show output port" parameter
	    Parameter showOutputPort = 
						b.getParameterByName("ShowOutputPort");
	
	    String showOutputPortValue = showOutputPort.getStringValue();
	    
	    if (showOutputPortValue.equals("off")) {
	    	((InEnablePort) port).setRelatedToInportBlock(false);
	        // remove block from model
	        b.getParent().removeChild(b);
	        port.setSourceBlock(null);
	    } else if (showOutputPortValue.equals("on")) {
	        // replace EnablePort block by an Inport block
	    	port.setSourceBlock(replaceBlock(b, port));
	    } else {
	        EventHandler.handle(
	                        EventLevel.ERROR,
	                        "HandlePortsState.replaceControlBlocks", 
	                        "",
	                        "Parameter 'ShowOutputPort' has illegal "
	                        + "value \"" + showOutputPortValue + "\" "
	                        + "\n expexted values: [on|off]"
	                        + "\n block: " + b.getReferenceString(), 
	                        "");
	    }
	}
    
    /**
     * transfers trigger port parameters to either InControlPort or
     * InEdgeEnablePort
     * @param b
     */
    private static void setTriggerPortAttributes(Block b) {
    	SystemBlock system = (SystemBlock) b.getParent();
    	Inport port = null;
    	
        Parameter showOutputPort = b.getParameterByName("ShowOutputPort");
        Parameter triggerType = b.getParameterByName("TriggerType");

        String triggerTypeValue = triggerType.getStringValue();
        String showOutputPortValue = showOutputPort.getStringValue();

        /* 
         * Function call port. Importer has classified it as 
         * InEdge enable port. delete the existing port and create new
         */
        if (triggerTypeValue.equals("function-call")) {

        	port = PortResolver.replaceInputWithControlPort(
        								system.getInEdgeEnablePort());

        /*
         * Enable or EdgeEnable port
         */
        } else if (triggerTypeValue.equals("falling")
                || triggerTypeValue.equals("rising")
                || triggerTypeValue.equals("either")) {
        	port = system.getInEdgeEnablePort();

            // transfer parameter "TriggerType" value to inEdgeEnable
            // port of the parent system block
            ((InEdgeEnablePort)port).setTriggerType(triggerTypeValue);

        } else {
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "HandlePortsState.replaceControlBlocks", 
                            "",
                            "Parameter 'TriggerType' has illegal "
                            + "value \"" + triggerTypeValue + "\" "
                            + "\n expected values: " 
                            + "[functon-call|falling|rising|either]"
                            + "\n block: " + b.getReferenceString(), 
                            "");
        }
        
        if (port != null) {
        	// set port name
        	port.setName(b.getName());

            if (showOutputPortValue.equals("on")) {
                // replace TriggerPort block by an Inport block
            	port.setSourceBlock(replaceBlock(b, port));
            } else if (showOutputPortValue.equals("off")) {
            	port.setRelatedToInportBlock(false);
            	port.setSourceBlock(null);
                // remove block from model
                b.getParent().removeChild(b);
            } else {
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "HandlePortsState.replaceControlBlocks", 
                                "",
                                "Parameter 'ShowOutputPort' has illegal "
                                + "value \"" + showOutputPortValue + "\" "
                                + "\n expexted values: [on|off]"
                                + "\n block: " + b.getReferenceString(), 
                                "");
            }
        }

    }
    /**
     * Replaces block b with Inport block
     * 
     * @param oldInportBlock		-- block to be replaced (either Enable or EdgeEnable)
     * @param p		-- port obejct related to this block
     * 
     * @return new created block
     */
    private static Block replaceBlock(Block oldInportBlock, Inport p) {

    	SystemBlock system = (SystemBlock)oldInportBlock.getParent();
        Block newInportBlock = new SourceBlock();
        ModelUtilities.transferContent(oldInportBlock, newInportBlock);
        newInportBlock.setPortReference(p);
        system.removeChild(oldInportBlock);
        system.addBlock(0, newInportBlock);
        
        newInportBlock.setType("Inport");

        BlockParameter portNumber = newInportBlock.getParameterByName("Port");
        StringExpression portNumberVal = new StringExpression(
        								String.valueOf(p.getPortNumber()));
        if (portNumber == null) {
        	// port number parameter did not exist, create new
            portNumber = new BlockParameter(portNumberVal, "Port");
            newInportBlock.addParameter(portNumber);
        } else {
        	// set new value
        	portNumber.setValue(portNumberVal);
        }
        
        // setting enable or edgeEnable relatedToInportBlock parameter to have
        // link between this port and inport block for code generation
        p.setRelatedToInportBlock(true);
        
        return newInportBlock;
    }
    
    /**
     * Adds port objects to the top-level subsystem
     * Loops through the top-level elements of the system model,
     * for each subsystem finds its oprt blocks, creates corresponding
     * port object and sets cross-refences between the port and the 
     * block  
     * 
     * @param gaSystemModel -- reference to the sysmte model object
     */
    public static void addPortsToTopSystem(GASystemModel gaSystemModel) {

        for (GASystemModelRoot elem : gaSystemModel.getElements()) {
            // elements are top-level elements of the model
            SystemBlock top = null;
            if (elem instanceof SystemBlock){
                top = (SystemBlock) elem;
            } else {
                EventHandler.handle(
                        EventLevel.CRITICAL_ERROR,
                        "PortResolver.addPortToTopSystem",
                        "",
                        "Only SystemBlock is allowed as a top-level element, " +
                        "found \n " + elem.getReferenceString(),
                        "");
            }

            for (Block block : top.getBlocks()) {
                if (block instanceof SourceBlock
                		&& block.getType().equals("Inport")) {
                	// new port
                    InDataPort inport = new InDataPort();
                    
                    // set cross-references
                    inport.setSourceBlock(block);
                    
                    // transfer port number form parameters
                    inport.setPortNumber(getPortNo(block));
                    
                    // transfer custom properties
                    setInportAttributes((SourceBlock) block, inport);
                    
                    // attach port to the system
                    top.addInDataPort((InDataPort)inport);

                } else if (block instanceof SinkBlock
                		&& block.getType().equals("Outport")) {
                	// new port
                    OutDataPort outport = new OutDataPort();
                    
                    // set coross-references
                    outport.setSourceBlock(block);
                    
                    // transfer port number form parameters
                    outport.setPortNumber(getPortNo(block));
                    
                    // transfer custom properties
                    setOutportAttributes((SinkBlock) block, outport);
                    
                    // attach port to the system
                    top.addOutDataPort(outport);
                }
                
            }
        }

    }

}
