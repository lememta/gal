/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/assertions/Assert.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities.assertions;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.genericmodel.GAModelElement;

public class Assert {

    /**
     * Ensures that an object has a valid name. Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param element
     *            The model element to check
     */
    public static void assertNameNotEmpty(GAModelElement element) {
        assertNameNotEmpty(element, "");
    }

    public static void assertNameNotEmpty(GAModelElement element, String additionalMsg) {
        if (element.getName() == null || element.getName().isEmpty()) {
            String msg = "Name is null or empty.";
            if (additionalMsg != null && !additionalMsg.isEmpty()) {
                msg += " " + additionalMsg;
            }
            if (element != null) {
                msg += "\nElement: " + element.getReferenceString();
            }
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "assertNameNotEmpty",
                    "", msg, "");
        }
    }

    /**
     * Checks, if the datatype of the Expression can be calculated, based on the 
     * Gene-Auto typing rules.
     * 
     * @param e Expression
     * @param additionalMsg
     */
    public static void assertTypeable(Expression e, String additionalMsg) {
        if (e.getDataType() == null) {
            String msg = "Expression is not typeable.";
            if (additionalMsg != null && !additionalMsg.isEmpty()) {
                msg += " " + additionalMsg;
            }
            msg += "\nExpression: " + e.printElement();
            msg += "\nReference: " + e.getReferenceString();
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "assertTypeable",
                    "", msg, "");
        }
    }
}
