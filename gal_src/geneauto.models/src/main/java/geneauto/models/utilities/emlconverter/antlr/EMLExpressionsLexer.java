// $ANTLR 3.0.1 T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g 2010-03-10 16:59:08

package geneauto.models.utilities.emlconverter.antlr;

// Required for lexer::members netToken()
/* Comment out when debugging in AntLrWorks
*/
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.FailedPredicateException;
import org.antlr.runtime.IntStream;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;

@SuppressWarnings("unused")
public class EMLExpressionsLexer extends Lexer {
    public static final int DOTNUMBER=61;
    public static final int LOR=29;
    public static final int ExpressionStatement=8;
    public static final int EXPONENT=62;
    public static final int STAR=37;
    public static final int COMP_OP=35;
    public static final int EQ_OP=34;
    public static final int MOD=39;
    public static final int LETTER=58;
    public static final int SHIFT=36;
    public static final int AssignStatement=5;
    public static final int ParenthesisExpression=15;
    public static final int NOT=41;
    public static final int AND=33;
    public static final int EOF=-1;
    public static final int LPAREN=44;
    public static final int RCOMMENT=24;
    public static final int SL_DATA_TYPE=43;
    public static final int RPAREN=45;
    public static final int SLASH=38;
    public static final int COMMA=48;
    public static final int CompoundStatement=4;
    public static final int IndexExpressions=19;
    public static final int TILDE=42;
    public static final int IDENT=55;
    public static final int CallExpression=10;
    public static final int PLUS=26;
    public static final int DIGIT=60;
    public static final int NL=23;
    public static final int DOT=54;
    public static final int COMMENT=25;
    public static final int TypedExpression=20;
    public static final int UnaryExpression=17;
    public static final int SCOLON=50;
    public static final int INTEGER=47;
    public static final int TernaryExpression=16;
    public static final int XOR=32;
    public static final int SYMBOL=59;
    public static final int RealExpression=13;
    public static final int NUMBER=46;
    public static final int POWER=40;
    public static final int LSQUB=49;
    public static final int DecStatement=7;
    public static final int MINUS=27;
    public static final int IntegerExpression=11;
    public static final int Tokens=64;
    public static final int RSQUB=51;
    public static final int Annotations=21;
    public static final int VariableExpression=18;
    public static final int COLON=57;
    public static final int BinaryExpression=9;
    public static final int WS=56;
    public static final int ANY=63;
    public static final int OR=31;
    public static final int ASSIGN=28;
    public static final int DotIdent=22;
    public static final int RCURLB=53;
    public static final int ListExpression=12;
    public static final int MemberExpression=14;
    public static final int LCURLB=52;
    public static final int LAND=30;
    public static final int IncStatement=6;
    
    // Semantic predicate - indicates that the C-bitwise operations flag was set in the source language
    // TODO: check if it makes any sense in Simulink
    public boolean cBitOptsOn = false;
    
    /* Comment to run AntLRWorks debugger
       Uncomment before release
    */
    public Token nextToken() {
      while (true) {
       this.token = null;
       this.channel = Token.DEFAULT_CHANNEL;
       this.tokenStartCharIndex = input.index();
       this.tokenStartCharPositionInLine = input.getCharPositionInLine();
       this.tokenStartLine = input.getLine();
       this.text = null;
       if ( input.LA(1)==CharStream.EOF ) {
        return Token.EOF_TOKEN;
       }
       try {
        mTokens();
        if ( this.token==null ) {
         emit();
        }
        else if ( this.token==Token.SKIP_TOKEN ) {
         continue;
        }
        return this.token;
       }
       catch (RecognitionException re) {
        reportError(re);
        EventHandler.handle( EventLevel.ERROR, "", "", "Errors while parsing - unable to continue.", "");
       }
      }
     }

    public EMLExpressionsLexer() {;} 
    public EMLExpressionsLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g"; }

    // $ANTLR start COMMENT
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:242:9: ( '/*' ( . )* '*/' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:242:11: '/*' ( . )* '*/'
            {
            match("/*"); 

            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:242:17: ( . )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='*') ) {
                    int LA1_1 = input.LA(2);

                    if ( (LA1_1=='/') ) {
                        alt1=2;
                    }
                    else if ( ((LA1_1>='\u0000' && LA1_1<='.')||(LA1_1>='0' && LA1_1<='\uFFFE')) ) {
                        alt1=1;
                    }


                }
                else if ( ((LA1_0>='\u0000' && LA1_0<=')')||(LA1_0>='+' && LA1_0<='\uFFFE')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:242:17: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match("*/"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMMENT

    // $ANTLR start RCOMMENT
    public final void mRCOMMENT() throws RecognitionException {
        try {
            int _type = RCOMMENT;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:245:9: ( '//' (~ '\\\\' )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:245:11: '//' (~ '\\\\' )*
            {
            match("//"); 

            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:245:17: (~ '\\\\' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='\u0000' && LA2_0<='[')||(LA2_0>=']' && LA2_0<='\uFFFE')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:245:18: ~ '\\\\'
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RCOMMENT

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:250:4: ( ( ' ' | '\\t' | '...\\\\n' ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:250:6: ( ' ' | '\\t' | '...\\\\n' )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:250:6: ( ' ' | '\\t' | '...\\\\n' )
            int alt3=3;
            switch ( input.LA(1) ) {
            case ' ':
                {
                alt3=1;
                }
                break;
            case '\t':
                {
                alt3=2;
                }
                break;
            case '.':
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("250:6: ( ' ' | '\\t' | '...\\\\n' )", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:250:7: ' '
                    {
                    match(' '); 

                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:250:11: '\\t'
                    {
                    match('\t'); 

                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:250:16: '...\\\\n'
                    {
                    match("...\\n"); 


                    }
                    break;

            }

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    // $ANTLR start NL
    public final void mNL() throws RecognitionException {
        try {
            int _type = NL;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:251:5: ( '\\\\n' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:251:7: '\\\\n'
            {
            match("\\n"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NL

    // $ANTLR start LPAREN
    public final void mLPAREN() throws RecognitionException {
        try {
            int _type = LPAREN;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:254:8: ( '(' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:254:10: '('
            {
            match('('); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LPAREN

    // $ANTLR start RPAREN
    public final void mRPAREN() throws RecognitionException {
        try {
            int _type = RPAREN;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:255:8: ( ')' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:255:10: ')'
            {
            match(')'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RPAREN

    // $ANTLR start LCURLB
    public final void mLCURLB() throws RecognitionException {
        try {
            int _type = LCURLB;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:256:8: ( '{' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:256:10: '{'
            {
            match('{'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LCURLB

    // $ANTLR start RCURLB
    public final void mRCURLB() throws RecognitionException {
        try {
            int _type = RCURLB;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:257:8: ( '}' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:257:10: '}'
            {
            match('}'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RCURLB

    // $ANTLR start LSQUB
    public final void mLSQUB() throws RecognitionException {
        try {
            int _type = LSQUB;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:258:7: ( '[' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:258:9: '['
            {
            match('['); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LSQUB

    // $ANTLR start RSQUB
    public final void mRSQUB() throws RecognitionException {
        try {
            int _type = RSQUB;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:259:7: ( ']' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:259:9: ']'
            {
            match(']'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RSQUB

    // $ANTLR start DOT
    public final void mDOT() throws RecognitionException {
        try {
            int _type = DOT;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:260:6: ( '.' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:260:8: '.'
            {
            match('.'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DOT

    // $ANTLR start COMMA
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:261:7: ( ',' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:261:9: ','
            {
            match(','); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMMA

    // $ANTLR start COLON
    public final void mCOLON() throws RecognitionException {
        try {
            int _type = COLON;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:262:7: ( ':' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:262:9: ':'
            {
            match(':'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COLON

    // $ANTLR start SCOLON
    public final void mSCOLON() throws RecognitionException {
        try {
            int _type = SCOLON;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:263:8: ( ';' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:263:10: ';'
            {
            match(';'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SCOLON

    // $ANTLR start PLUS
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:264:6: ( '+' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:264:8: '+'
            {
            match('+'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end PLUS

    // $ANTLR start MINUS
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:265:7: ( '-' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:265:9: '-'
            {
            match('-'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end MINUS

    // $ANTLR start STAR
    public final void mSTAR() throws RecognitionException {
        try {
            int _type = STAR;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:266:6: ( '*' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:266:8: '*'
            {
            match('*'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STAR

    // $ANTLR start SLASH
    public final void mSLASH() throws RecognitionException {
        try {
            int _type = SLASH;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:267:7: ( '/' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:267:9: '/'
            {
            match('/'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SLASH

    // $ANTLR start MOD
    public final void mMOD() throws RecognitionException {
        try {
            int _type = MOD;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:268:6: ( '%%' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:268:8: '%%'
            {
            match("%%"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end MOD

    // $ANTLR start LAND
    public final void mLAND() throws RecognitionException {
        try {
            int _type = LAND;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:269:7: ( '&&' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:269:9: '&&'
            {
            match("&&"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LAND

    // $ANTLR start LOR
    public final void mLOR() throws RecognitionException {
        try {
            int _type = LOR;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:270:7: ( '||' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:270:9: '||'
            {
            match("||"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LOR

    // $ANTLR start AND
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:271:6: ( '&' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:271:8: '&'
            {
            match('&'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end AND

    // $ANTLR start OR
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:272:6: ( '|' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:272:8: '|'
            {
            match('|'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end OR

    // $ANTLR start POWER
    public final void mPOWER() throws RecognitionException {
        try {
            int _type = POWER;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:273:8: ({...}? => '^' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:273:10: {...}? => '^'
            {
            if ( !(!cBitOptsOn ) ) {
                throw new FailedPredicateException(input, "POWER", "!cBitOptsOn ");
            }
            match('^'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end POWER

    // $ANTLR start XOR
    public final void mXOR() throws RecognitionException {
        try {
            int _type = XOR;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:274:6: ({...}? => '^' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:274:8: {...}? => '^'
            {
            if ( !( cBitOptsOn ) ) {
                throw new FailedPredicateException(input, "XOR", " cBitOptsOn ");
            }
            match('^'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end XOR

    // $ANTLR start SHIFT
    public final void mSHIFT() throws RecognitionException {
        try {
            int _type = SHIFT;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:275:7: ( ( '<<' | '>>' ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:275:9: ( '<<' | '>>' )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:275:9: ( '<<' | '>>' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='<') ) {
                alt4=1;
            }
            else if ( (LA4_0=='>') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("275:9: ( '<<' | '>>' )", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:275:10: '<<'
                    {
                    match("<<"); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:275:15: '>>'
                    {
                    match(">>"); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SHIFT

    // $ANTLR start EQ_OP
    public final void mEQ_OP() throws RecognitionException {
        try {
            int _type = EQ_OP;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:276:7: ( ( '==' | '!=' | '~=' | '<>' ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:276:9: ( '==' | '!=' | '~=' | '<>' )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:276:9: ( '==' | '!=' | '~=' | '<>' )
            int alt5=4;
            switch ( input.LA(1) ) {
            case '=':
                {
                alt5=1;
                }
                break;
            case '!':
                {
                alt5=2;
                }
                break;
            case '~':
                {
                alt5=3;
                }
                break;
            case '<':
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("276:9: ( '==' | '!=' | '~=' | '<>' )", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:276:10: '=='
                    {
                    match("=="); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:276:15: '!='
                    {
                    match("!="); 


                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:276:20: '~='
                    {
                    match("~="); 


                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:276:25: '<>'
                    {
                    match("<>"); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end EQ_OP

    // $ANTLR start COMP_OP
    public final void mCOMP_OP() throws RecognitionException {
        try {
            int _type = COMP_OP;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:277:9: ( ( '>=' | '<=' | '>' | '<' ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:277:11: ( '>=' | '<=' | '>' | '<' )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:277:11: ( '>=' | '<=' | '>' | '<' )
            int alt6=4;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='>') ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1=='=') ) {
                    alt6=1;
                }
                else {
                    alt6=3;}
            }
            else if ( (LA6_0=='<') ) {
                int LA6_2 = input.LA(2);

                if ( (LA6_2=='=') ) {
                    alt6=2;
                }
                else {
                    alt6=4;}
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("277:11: ( '>=' | '<=' | '>' | '<' )", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:277:12: '>='
                    {
                    match(">="); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:277:17: '<='
                    {
                    match("<="); 


                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:277:22: '>'
                    {
                    match('>'); 

                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:277:26: '<'
                    {
                    match('<'); 

                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMP_OP

    // $ANTLR start ASSIGN
    public final void mASSIGN() throws RecognitionException {
        try {
            int _type = ASSIGN;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:9: ( ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:11: ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:11: ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' )
            int alt7=8;
            switch ( input.LA(1) ) {
            case '=':
                {
                alt7=1;
                }
                break;
            case '+':
                {
                alt7=2;
                }
                break;
            case '-':
                {
                alt7=3;
                }
                break;
            case '*':
                {
                alt7=4;
                }
                break;
            case '/':
                {
                alt7=5;
                }
                break;
            case '&':
                {
                alt7=6;
                }
                break;
            case '|':
                {
                alt7=7;
                }
                break;
            case '^':
                {
                alt7=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("278:11: ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' )", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:12: '='
                    {
                    match('='); 

                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:16: '+='
                    {
                    match("+="); 


                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:21: '-='
                    {
                    match("-="); 


                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:26: '*='
                    {
                    match("*="); 


                    }
                    break;
                case 5 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:31: '/='
                    {
                    match("/="); 


                    }
                    break;
                case 6 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:36: '&='
                    {
                    match("&="); 


                    }
                    break;
                case 7 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:41: '|='
                    {
                    match("|="); 


                    }
                    break;
                case 8 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:278:46: '^='
                    {
                    match("^="); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ASSIGN

    // $ANTLR start NOT
    public final void mNOT() throws RecognitionException {
        try {
            int _type = NOT;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:279:7: ( '!' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:279:9: '!'
            {
            match('!'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NOT

    // $ANTLR start TILDE
    public final void mTILDE() throws RecognitionException {
        try {
            int _type = TILDE;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:280:7: ( '~' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:280:9: '~'
            {
            match('~'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end TILDE

    // $ANTLR start SL_DATA_TYPE
    public final void mSL_DATA_TYPE() throws RecognitionException {
        try {
            int _type = SL_DATA_TYPE;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:14: ( ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )
            int alt8=9;
            switch ( input.LA(1) ) {
            case 'd':
                {
                alt8=1;
                }
                break;
            case 's':
                {
                alt8=2;
                }
                break;
            case 'i':
                {
                int LA8_3 = input.LA(2);

                if ( (LA8_3=='n') ) {
                    int LA8_6 = input.LA(3);

                    if ( (LA8_6=='t') ) {
                        switch ( input.LA(4) ) {
                        case '1':
                            {
                            alt8=4;
                            }
                            break;
                        case '3':
                            {
                            alt8=5;
                            }
                            break;
                        case '8':
                            {
                            alt8=3;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 8, input);

                            throw nvae;
                        }

                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 6, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 3, input);

                    throw nvae;
                }
                }
                break;
            case 'u':
                {
                int LA8_4 = input.LA(2);

                if ( (LA8_4=='i') ) {
                    int LA8_7 = input.LA(3);

                    if ( (LA8_7=='n') ) {
                        int LA8_9 = input.LA(4);

                        if ( (LA8_9=='t') ) {
                            switch ( input.LA(5) ) {
                            case '1':
                                {
                                alt8=7;
                                }
                                break;
                            case '3':
                                {
                                alt8=8;
                                }
                                break;
                            case '8':
                                {
                                alt8=6;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 13, input);

                                throw nvae;
                            }

                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 9, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 7, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 4, input);

                    throw nvae;
                }
                }
                break;
            case 'b':
                {
                alt8=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("281:16: ( 'double' | 'single' | 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' | 'boolean' )", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:17: 'double'
                    {
                    match("double"); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:26: 'single'
                    {
                    match("single"); 


                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:35: 'int8'
                    {
                    match("int8"); 


                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:42: 'int16'
                    {
                    match("int16"); 


                    }
                    break;
                case 5 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:50: 'int32'
                    {
                    match("int32"); 


                    }
                    break;
                case 6 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:58: 'uint8'
                    {
                    match("uint8"); 


                    }
                    break;
                case 7 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:66: 'uint16'
                    {
                    match("uint16"); 


                    }
                    break;
                case 8 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:75: 'uint32'
                    {
                    match("uint32"); 


                    }
                    break;
                case 9 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:281:84: 'boolean'
                    {
                    match("boolean"); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SL_DATA_TYPE

    // $ANTLR start IDENT
    public final void mIDENT() throws RecognitionException {
        try {
            int _type = IDENT;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:285:7: ( ( LETTER | SYMBOL ) ( LETTER | SYMBOL | DIGIT )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:285:9: ( LETTER | SYMBOL ) ( LETTER | SYMBOL | DIGIT )*
            {
            if ( input.LA(1)=='$'||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:285:25: ( LETTER | SYMBOL | DIGIT )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='$'||(LA9_0>='0' && LA9_0<='9')||(LA9_0>='A' && LA9_0<='Z')||LA9_0=='_'||(LA9_0>='a' && LA9_0<='z')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:
            	    {
            	    if ( input.LA(1)=='$'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end IDENT

    // $ANTLR start NUMBER
    public final void mNUMBER() throws RecognitionException {
        try {
            int _type = NUMBER;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:8: ( ( INTEGER ( DOT | DOTNUMBER | EXPONENT ) ) | DOTNUMBER )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( ((LA11_0>='0' && LA11_0<='9')) ) {
                alt11=1;
            }
            else if ( (LA11_0=='.') ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("287:1: NUMBER : ( ( INTEGER ( DOT | DOTNUMBER | EXPONENT ) ) | DOTNUMBER );", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:10: ( INTEGER ( DOT | DOTNUMBER | EXPONENT ) )
                    {
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:10: ( INTEGER ( DOT | DOTNUMBER | EXPONENT ) )
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:11: INTEGER ( DOT | DOTNUMBER | EXPONENT )
                    {
                    mINTEGER(); 
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:19: ( DOT | DOTNUMBER | EXPONENT )
                    int alt10=3;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0=='.') ) {
                        int LA10_1 = input.LA(2);

                        if ( ((LA10_1>='0' && LA10_1<='9')) ) {
                            alt10=2;
                        }
                        else {
                            alt10=1;}
                    }
                    else if ( (LA10_0=='E'||LA10_0=='e') ) {
                        alt10=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("287:19: ( DOT | DOTNUMBER | EXPONENT )", 10, 0, input);

                        throw nvae;
                    }
                    switch (alt10) {
                        case 1 :
                            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:21: DOT
                            {
                            mDOT(); 

                            }
                            break;
                        case 2 :
                            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:27: DOTNUMBER
                            {
                            mDOTNUMBER(); 

                            }
                            break;
                        case 3 :
                            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:287:39: EXPONENT
                            {
                            mEXPONENT(); 

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:288:5: DOTNUMBER
                    {
                    mDOTNUMBER(); 

                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NUMBER

    // $ANTLR start DOTNUMBER
    public final void mDOTNUMBER() throws RecognitionException {
        try {
            int _type = DOTNUMBER;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:290:11: ( DOT INTEGER ( EXPONENT )? )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:290:13: DOT INTEGER ( EXPONENT )?
            {
            mDOT(); 
            mINTEGER(); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:290:25: ( EXPONENT )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='E'||LA12_0=='e') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:290:26: EXPONENT
                    {
                    mEXPONENT(); 

                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DOTNUMBER

    // $ANTLR start INTEGER
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:9: ( ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:12: ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:12: ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='0') ) {
                int LA15_1 = input.LA(2);

                if ( (LA15_1=='X'||LA15_1=='x') ) {
                    alt15=2;
                }
                else {
                    alt15=1;}
            }
            else if ( ((LA15_0>='1' && LA15_0<='9')) ) {
                alt15=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("292:12: ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ )", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:13: ( DIGIT )+
                    {
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:13: ( DIGIT )+
                    int cnt13=0;
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( ((LA13_0>='0' && LA13_0<='9')) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:13: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt13 >= 1 ) break loop13;
                                EarlyExitException eee =
                                    new EarlyExitException(13, input);
                                throw eee;
                        }
                        cnt13++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:22: '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+
                    {
                    match('0'); 
                    if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse =
                            new MismatchedSetException(null,input);
                        recover(mse);    throw mse;
                    }

                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:292:36: ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+
                    int cnt14=0;
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( ((LA14_0>='0' && LA14_0<='9')||(LA14_0>='A' && LA14_0<='F')||(LA14_0>='a' && LA14_0<='f')) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:
                    	    {
                    	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse =
                    	            new MismatchedSetException(null,input);
                    	        recover(mse);    throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt14 >= 1 ) break loop14;
                                EarlyExitException eee =
                                    new EarlyExitException(14, input);
                                throw eee;
                        }
                        cnt14++;
                    } while (true);


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end INTEGER

    // $ANTLR start EXPONENT
    public final void mEXPONENT() throws RecognitionException {
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:297:11: ( ( 'e' | 'E' ) ( PLUS )? INTEGER )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:297:13: ( 'e' | 'E' ) ( PLUS )? INTEGER
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:297:23: ( PLUS )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0=='+') ) {
                alt16=1;
            } else if ((LA16_0=='-')) {
            	alt16=3;
            }
   
            switch (alt16) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:297:23: PLUS
                    {
                    mPLUS(); 

                    }
                    break;
                case 3 :
                	{
                	mMINUS();
                	}
                	break;
            }

            mINTEGER(); 
            }

        }
        finally {
        }
    }
    // $ANTLR end EXPONENT

    // $ANTLR start LETTER
    public final void mLETTER() throws RecognitionException {
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:299:8: ( 'a' .. 'z' | 'A' .. 'Z' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

        }
        finally {
        }
    }
    // $ANTLR end LETTER

    // $ANTLR start DIGIT
    public final void mDIGIT() throws RecognitionException {
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:301:7: ( '0' .. '9' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:301:9: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end DIGIT

    // $ANTLR start SYMBOL
    public final void mSYMBOL() throws RecognitionException {
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:303:8: ( '_' | '$' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:
            {
            if ( input.LA(1)=='$'||input.LA(1)=='_' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

        }
        finally {
        }
    }
    // $ANTLR end SYMBOL

    // $ANTLR start ANY
    public final void mANY() throws RecognitionException {
        try {
            int _type = ANY;
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:310:5: ( . )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:310:8: .
            {
            matchAny(); 
            
            EventHandler.handle( EventLevel.ERROR, "labelParser", "",
            	"Unsupported character \'" + getText() + "\' - skipped ", "" );
            skip();


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ANY

    public void mTokens() throws RecognitionException {
        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:8: ( COMMENT | RCOMMENT | WS | NL | LPAREN | RPAREN | LCURLB | RCURLB | LSQUB | RSQUB | DOT | COMMA | COLON | SCOLON | PLUS | MINUS | STAR | SLASH | MOD | LAND | LOR | AND | OR | POWER | XOR | SHIFT | EQ_OP | COMP_OP | ASSIGN | NOT | TILDE | SL_DATA_TYPE | IDENT | NUMBER | DOTNUMBER | INTEGER | ANY )
        int alt17=37;
        alt17 = dfa17.predict(input);
        switch (alt17) {
            case 1 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:10: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 2 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:18: RCOMMENT
                {
                mRCOMMENT(); 

                }
                break;
            case 3 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:27: WS
                {
                mWS(); 

                }
                break;
            case 4 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:30: NL
                {
                mNL(); 

                }
                break;
            case 5 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:33: LPAREN
                {
                mLPAREN(); 

                }
                break;
            case 6 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:40: RPAREN
                {
                mRPAREN(); 

                }
                break;
            case 7 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:47: LCURLB
                {
                mLCURLB(); 

                }
                break;
            case 8 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:54: RCURLB
                {
                mRCURLB(); 

                }
                break;
            case 9 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:61: LSQUB
                {
                mLSQUB(); 

                }
                break;
            case 10 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:67: RSQUB
                {
                mRSQUB(); 

                }
                break;
            case 11 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:73: DOT
                {
                mDOT(); 

                }
                break;
            case 12 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:77: COMMA
                {
                mCOMMA(); 

                }
                break;
            case 13 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:83: COLON
                {
                mCOLON(); 

                }
                break;
            case 14 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:89: SCOLON
                {
                mSCOLON(); 

                }
                break;
            case 15 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:96: PLUS
                {
                mPLUS(); 

                }
                break;
            case 16 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:101: MINUS
                {
                mMINUS(); 

                }
                break;
            case 17 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:107: STAR
                {
                mSTAR(); 

                }
                break;
            case 18 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:112: SLASH
                {
                mSLASH(); 

                }
                break;
            case 19 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:118: MOD
                {
                mMOD(); 

                }
                break;
            case 20 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:122: LAND
                {
                mLAND(); 

                }
                break;
            case 21 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:127: LOR
                {
                mLOR(); 

                }
                break;
            case 22 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:131: AND
                {
                mAND(); 

                }
                break;
            case 23 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:135: OR
                {
                mOR(); 

                }
                break;
            case 24 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:138: POWER
                {
                mPOWER(); 

                }
                break;
            case 25 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:144: XOR
                {
                mXOR(); 

                }
                break;
            case 26 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:148: SHIFT
                {
                mSHIFT(); 

                }
                break;
            case 27 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:154: EQ_OP
                {
                mEQ_OP(); 

                }
                break;
            case 28 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:160: COMP_OP
                {
                mCOMP_OP(); 

                }
                break;
            case 29 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:168: ASSIGN
                {
                mASSIGN(); 

                }
                break;
            case 30 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:175: NOT
                {
                mNOT(); 

                }
                break;
            case 31 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:179: TILDE
                {
                mTILDE(); 

                }
                break;
            case 32 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:185: SL_DATA_TYPE
                {
                mSL_DATA_TYPE(); 

                }
                break;
            case 33 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:198: IDENT
                {
                mIDENT(); 

                }
                break;
            case 34 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:204: NUMBER
                {
                mNUMBER(); 

                }
                break;
            case 35 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:211: DOTNUMBER
                {
                mDOTNUMBER(); 

                }
                break;
            case 36 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:221: INTEGER
                {
                mINTEGER(); 

                }
                break;
            case 37 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:1:229: ANY
                {
                mANY(); 

                }
                break;

        }

    }


    protected DFA17 dfa17 = new DFA17(this);
    static final String DFA17_eotS =
        "\1\uffff\1\47\2\uffff\1\51\1\43\11\uffff\1\66\1\67\1\70\1\43\1\73"+
        "\1\75\1\76\2\100\1\45\1\102\1\103\5\105\1\uffff\2\113\7\uffff\2"+
        "\115\30\uffff\1\105\1\uffff\4\105\2\uffff\1\113\5\uffff\5\105\1"+
        "\113\1\115\1\uffff\2\115\3\105\1\151\3\105\1\113\1\115\1\uffff\2"+
        "\105\1\151\1\uffff\2\151\3\105\5\115\4\151\1\105\1\151";
    static final String DFA17_eofS =
        "\172\uffff";
    static final String DFA17_minS =
        "\1\0\1\52\2\uffff\1\56\1\156\11\uffff\3\75\1\45\1\46\2\75\1\74\1"+
        "\76\3\75\1\157\1\151\1\156\1\151\1\157\1\uffff\2\56\7\uffff\2\60"+
        "\22\uffff\1\0\5\uffff\1\165\1\uffff\1\156\1\164\1\156\1\157\1\60"+
        "\1\uffff\1\56\1\uffff\1\60\1\53\2\uffff\1\142\1\147\1\61\1\164\1"+
        "\154\1\56\4\60\2\154\1\66\1\44\1\62\1\61\1\145\2\53\1\60\2\145\1"+
        "\44\1\uffff\2\44\1\66\1\62\1\141\5\60\4\44\1\156\1\44";
    static final String DFA17_maxS =
        "\1\ufffe\1\75\2\uffff\1\71\1\156\11\uffff\3\75\1\45\1\75\1\174\1"+
        "\75\2\76\3\75\1\157\1\151\1\156\1\151\1\157\1\uffff\1\170\1\145"+
        "\7\uffff\1\170\1\145\22\uffff\1\0\5\uffff\1\165\1\uffff\1\156\1"+
        "\164\1\156\1\157\1\146\1\uffff\1\145\1\uffff\1\146\1\71\2\uffff"+
        "\1\142\1\147\1\70\1\164\1\154\2\146\1\71\1\170\1\71\2\154\1\66\1"+
        "\172\1\62\1\70\1\145\3\146\2\145\1\172\1\uffff\2\172\1\66\1\62\1"+
        "\141\2\146\1\170\2\146\4\172\1\156\1\172";
    static final String DFA17_acceptS =
        "\2\uffff\2\3\2\uffff\1\5\1\6\1\7\1\10\1\11\1\12\1\14\1\15\1\16\21"+
        "\uffff\1\41\2\uffff\1\45\1\2\1\35\1\1\1\22\1\3\1\13\2\uffff\1\4"+
        "\1\5\1\6\1\7\1\10\1\11\1\12\1\14\1\15\1\16\1\17\1\20\1\21\1\23\1"+
        "\24\1\26\1\25\1\27\1\uffff\1\32\1\34\1\33\1\36\1\37\1\uffff\1\41"+
        "\5\uffff\1\44\1\uffff\1\42\2\uffff\1\30\1\31\27\uffff\1\40\20\uffff";
    static final String DFA17_specialS =
        "\76\uffff\1\0\73\uffff}>";
    static final String[] DFA17_transitionS = {
            "\11\43\1\3\26\43\1\2\1\31\2\43\1\40\1\22\1\23\1\43\1\6\1\7\1"+
            "\21\1\17\1\14\1\20\1\4\1\1\1\41\11\42\1\15\1\16\1\26\1\30\1"+
            "\27\2\43\32\40\1\12\1\5\1\13\1\25\1\40\1\43\1\40\1\37\1\40\1"+
            "\33\4\40\1\35\11\40\1\34\1\40\1\36\5\40\1\10\1\24\1\11\1\32"+
            "\uff80\43",
            "\1\46\4\uffff\1\44\15\uffff\1\45",
            "",
            "",
            "\1\50\1\uffff\1\52\11\53",
            "\1\54",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\45",
            "\1\45",
            "\1\45",
            "\1\71",
            "\1\72\26\uffff\1\45",
            "\1\45\76\uffff\1\74",
            "\1\45",
            "\1\77\1\uffff\1\101",
            "\1\77",
            "\1\101",
            "\1\101",
            "\1\101",
            "\1\104",
            "\1\106",
            "\1\107",
            "\1\110",
            "\1\111",
            "",
            "\1\115\1\uffff\12\114\13\uffff\1\115\22\uffff\1\112\14\uffff"+
            "\1\115\22\uffff\1\112",
            "\1\115\1\uffff\12\114\13\uffff\1\115\37\uffff\1\115",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\53\13\uffff\1\117\22\uffff\1\116\14\uffff\1\117\22\uffff"+
            "\1\116",
            "\12\53\13\uffff\1\117\37\uffff\1\117",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "\1\122",
            "",
            "\1\123",
            "\1\124",
            "\1\125",
            "\1\126",
            "\12\127\7\uffff\6\127\32\uffff\6\127",
            "",
            "\1\115\1\uffff\12\114\13\uffff\1\115\37\uffff\1\115",
            "",
            "\12\130\7\uffff\6\130\32\uffff\6\130",
            "\1\131\4\uffff\1\132\11\133",
            "",
            "",
            "\1\134",
            "\1\135",
            "\1\136\1\uffff\1\140\4\uffff\1\137",
            "\1\141",
            "\1\142",
            "\1\115\1\uffff\12\127\7\uffff\4\127\1\143\1\127\32\uffff\4\127"+
            "\1\143\1\127",
            "\12\130\7\uffff\4\130\1\144\1\130\32\uffff\4\130\1\144\1\130",
            "\1\132\11\133",
            "\12\133\36\uffff\1\145\37\uffff\1\145",
            "\12\133",
            "\1\146",
            "\1\147",
            "\1\150",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "\1\152",
            "\1\154\1\uffff\1\155\4\uffff\1\153",
            "\1\156",
            "\1\115\2\uffff\1\115\1\uffff\1\157\11\160\7\uffff\4\127\1\143"+
            "\1\127\32\uffff\4\127\1\143\1\127",
            "\1\131\4\uffff\1\161\11\162\7\uffff\4\130\1\144\1\130\32\uffff"+
            "\4\130\1\144\1\130",
            "\12\163\7\uffff\6\163\32\uffff\6\163",
            "\1\164",
            "\1\165",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "\1\166",
            "\1\167",
            "\1\170",
            "\12\160\7\uffff\4\127\1\143\1\127\32\uffff\4\127\1\143\1\127",
            "\12\160\7\uffff\4\127\1\143\1\127\32\uffff\4\127\1\143\1\127",
            "\12\162\7\uffff\4\130\1\144\1\130\21\uffff\1\145\10\uffff\4"+
            "\130\1\144\1\130\21\uffff\1\145",
            "\12\162\7\uffff\4\130\1\144\1\130\32\uffff\4\130\1\144\1\130",
            "\12\163\7\uffff\6\163\32\uffff\6\163",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105",
            "\1\171",
            "\1\105\13\uffff\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff"+
            "\32\105"
    };

    static final short[] DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
    static final short[] DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
    static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
    static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
    static final short[] DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
    static final short[] DFA17_special = DFA.unpackEncodedString(DFA17_specialS);
    static final short[][] DFA17_transition;

    static {
        int numStates = DFA17_transitionS.length;
        DFA17_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
        }
    }

    class DFA17 extends DFA {

        public DFA17(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 17;
            this.eot = DFA17_eot;
            this.eof = DFA17_eof;
            this.min = DFA17_min;
            this.max = DFA17_max;
            this.accept = DFA17_accept;
            this.special = DFA17_special;
            this.transition = DFA17_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( COMMENT | RCOMMENT | WS | NL | LPAREN | RPAREN | LCURLB | RCURLB | LSQUB | RSQUB | DOT | COMMA | COLON | SCOLON | PLUS | MINUS | STAR | SLASH | MOD | LAND | LOR | AND | OR | POWER | XOR | SHIFT | EQ_OP | COMP_OP | ASSIGN | NOT | TILDE | SL_DATA_TYPE | IDENT | NUMBER | DOTNUMBER | INTEGER | ANY );";
        }
        public int specialStateTransition(int s, IntStream input) throws NoViableAltException {
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA17_62 = input.LA(1);

                         
                        int index17_62 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (!cBitOptsOn ) ) {s = 80;}

                        else if ( ( cBitOptsOn ) ) {s = 81;}

                        else if ( (true) ) {s = 35;}

                         
                        input.seek(index17_62);
                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 17, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}