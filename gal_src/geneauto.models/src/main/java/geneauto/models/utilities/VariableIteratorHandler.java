/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/VariableIteratorHandler.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.utils.PrivilegedAccessor;

import java.util.ArrayList;
import java.util.List;

/**
 * class for iteration new Variables
 * 
 *
 */
public class VariableIteratorHandler {

    private int counter;
    private List<Variable_CM> iterVarList;
    
    public VariableIteratorHandler() {
        iterVarList = new ArrayList<Variable_CM>();
        counter = 0;
    }
    
    public int getCounter() {
        return counter;
    }
    
    public void increaseCounter() {
        counter++;
    }
    
    public void releaseVariable() {
        counter--;
    }
    
    public Variable_CM getNext() {
        if ((!iterVarList.isEmpty()) && (counter <= iterVarList.size())) {
            return iterVarList.get(counter-1);
        } else {
            return null;
        }
    }

    public void addIterVar(Variable_CM var) {
        // Set the initial dataType. The type will be enlarged according to need.
        TRealInteger real = (TRealInteger) PrivilegedAccessor
            .getObjectInPackage(TRealInteger.class, var);
        real.setNBits(8);
        real.setSigned(false);
        var.setDataType(real);
        iterVarList.add(var);
    }

    /**
     * Checks, if the data type of the given iterator variable is large enough
     * to handle given loop count. If not, then updates the type of the variable
     * to a larger type.
     * 
     * @param loopCount
     */
    public static void checkSizeOfIterator(Variable_CM var, int loopCount) {        
        TRealInteger currDt = (TRealInteger) var.getDataType();
        int currMax = (int) Math.pow(2, currDt.getNBits()) - 1;
        if (loopCount > currMax) {
            // We have only following possibilities available:
            // UINT8 (initial), UINT16 and UINT32. 
            // Larger types are not considered.
            // Maximum loop count is max of UINT32.
            int max16 = (int) (Math.pow(2, 16) - 1);
            int max32 = (int) (Math.pow(2, 32) - 1);
            if (loopCount <= max16) {
                currDt.setNBits(16);
            } else if (loopCount <= max32) {
                currDt.setNBits(32);
            } else {
                // Actually not possible, if Java int is 32 bits.
                EventHandler.handle(EventLevel.ERROR, 
                        "VariableIteratorHandler.checkSizeOfIterator()", "", 
                        "Requested loop count: " 
                        + loopCount + " exceeds maximal allowed: " + max32);
            }
        }
    }
        
}
