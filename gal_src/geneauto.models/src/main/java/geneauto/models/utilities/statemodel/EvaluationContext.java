/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/statemodel/EvaluationContext.java,v $
 *  @version	$Revision: 1.30 $
 *	@date		$Date: 2011-11-29 22:28:25 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities.statemodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.FunctionArgument_CM;
import geneauto.models.gacodemodel.FunctionBody;
import geneauto.models.gacodemodel.Function_CM;
import geneauto.models.gacodemodel.Label;
import geneauto.models.gacodemodel.Module;
import geneauto.models.gacodemodel.NameSpaceElement_CM;
import geneauto.models.gacodemodel.SequentialFunctionBody;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.gaenumtypes.FunctionStyle;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.ExpressionStatement;
import geneauto.models.gacodemodel.statement.GotoStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.TPointer;
import geneauto.models.gadatatypes.TVoid;
import geneauto.models.gasystemmodel.GASystemModelElement;
import geneauto.models.gasystemmodel.common.ContainerNode;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gastatemodel.GraphicalFunction;
import geneauto.models.gasystemmodel.gastatemodel.Junction;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.assertions.Assert;
import geneauto.utils.general.NameHandler;
import geneauto.utils.tuple.Triple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Evaluation context for abstract evaluation of the StateModel charts.
 * 
 */
public class EvaluationContext {

    // Non-static fields
    
    /**
     * Parent context.
     */
    private EvaluationContext parent;

    /**
     * Junctions seen during the abstract evaluation.
     */
    private HashMap<Junction, Label> junctionLabels = new HashMap<Junction, Label>();
    
    /**
     * Parameter to tune how to generate code from joining junctions.
     */
    private static boolean junctionJoinsAsFunctions = true;
    
    // Static fields - NOTE! Initialise *ALL* non-final static fields in the init() 
    // method. All empty collections are created there also
    
    /**
     * NameHandler for managing label names - there might be several required
     * for some Junctions - one for each context. However, they must have unique
     * names in generated code. Hence, we have a global handler of names.
     */
    private static NameHandler nameHandler;

    /**
     * As all labels are unique, we store the code for labels globally. This
     * allows to do label handling only once and avoids quadratic complexity
     * (repetitive scanning of statements, when local contexts go out of scope).
     * 
     */
    private static HashMap<String, List<Statement>> junctionCode;

    /** List of jumps that are kept. */
    private static List<GotoStatement> keptJumps;

    /**
     * The expression that refers to the currently active Event (only one Event
     * is active at any time). Only required for evaluating the chart or code
     * generation. Not for describing the chart.
     */
    private static Expression activeEventExpression;

    /**
     * Reference to the Chart's context structure (Chart's I/O and memory).
     */
    private static Expression currentContextExpression;

    // Getters - Setters

    public static void setJunctionJoinsAsFunctions(boolean junctionJoinsAsFunctions) {
		EvaluationContext.junctionJoinsAsFunctions = junctionJoinsAsFunctions;
	}

	public static boolean isJunctionJoinsAsFunctions() {
		return junctionJoinsAsFunctions;
	}

	public static Expression getCurrentContextExpression() {
        return currentContextExpression;
    }

    public static void setCurrentContextExpression(
            Expression currentContextExpression) {
        EvaluationContext.currentContextExpression = currentContextExpression;
    }

    public static Expression getActiveEventExpression() {
        return activeEventExpression;
    }

    public static void setActiveEventExpression(Expression exp) {
        activeEventExpression = exp;
    }

    // Constructors

    /** Constructor with a parent Context */
    public EvaluationContext(EvaluationContext parent) {
        this.parent = parent;
    }
    
    // Methods

    /** Global initialiser. Initialises all static fields. */
    public static void init() {
        nameHandler  = new NameHandler();
        junctionCode = new HashMap<String, List<Statement>>();
        keptJumps  	 = new LinkedList<GotoStatement>();
        activeEventExpression 	 = null;
        currentContextExpression = null;
    }

    /**
     * Returns a label for a junction, if existing in current context or its
     * parent.
     */
    public Label getJunctionLabel(Junction j) {
        Label l = junctionLabels.get(j);
        if (l != null) {
            return l;
        } else if (parent != null) {
            return parent.getJunctionLabel(j);
        } else {
            return null;
        }
    }

    /**
     * Makes a label with a unique name for a given junction.
     * 
     * We need to create different labels for contexts. E.g. one for the entry
     * and one for the during. Jumping between entry and during paths might not
     * be always correct.
     */
    private Label makeJunctionLabel(Junction j) {
        String lblString = "L" + j.getExternalID();
        // If we generate functions, then we don't need to generate unique labels
        // Otherwise. we make unique labels to avoid awkward jumps to distant (but
        // identical) branches
        if (!isJunctionJoinsAsFunctions()) {
        	lblString = nameHandler.getUniqueName(lblString);        
        }
        return new Label(lblString);
    }

    /**
     * Generates a label for a given Junction and remembers the Junction.
     * 
     * @param Junction
     * @return generated label
     * 
     */
    public Label addJunctionLabel(Junction j) {
        Label l = makeJunctionLabel(j);
        junctionLabels.put(j, l);
        return l;
    }

    /**
     * Adds code to evaluate a given junction.
     */
    public void addJunctionCode(String lName, List<Statement> evalCode) {
        junctionCode.put(lName, evalCode);
    }

    /**
     * Receives a list of statements generated from one Chart or
     * GraphicalFunction and handles goto statements generated from Junctions.
     * One of the goto statements generated from each Junction will be replaced
     * by a label and code.
     * 
     * @param stmts
     *            list of statements to process
     * @param sourceEl
     *            Chart or graphical function from which the code is generated
     *            
     * @return list of processed statements
     */
    public static List<Statement> handleJunctionRefs(
    		List<Statement> stmts, GASystemModelElement sourceEl) {
        
        keptJumps.clear();
        
        // A temporary root node for handling the statement list
        CompoundStatement tmpStmt = new CompoundStatement(null, stmts);

        List<GAModelElement> gotos = tmpStmt.getAllChildren(GotoStatement.class);

        processGotos(gotos, new HashSet<String>());
        
        resolveLabels(tmpStmt.getAllChildren(Statement.class));
        
        if (isJunctionJoinsAsFunctions()) {        	
        	// Convert the left GotoStatements to function calls
        	// together with the respective target locations
        	jumpsToFunctions(sourceEl);
        }
        
        return tmpStmt.getStatements();
    }

	/**
     * Find a GotoStatement that is a child of an actually used code branch -
     * Some generated code sections might have actually ended up as orphans -
     * ignore these.
     * 
     * Unfortunately, we cannot determine right away, which code section in the
     * junctionCode table is actually unreferenced code. So, we have to add code
     * pieces one by one, until there is no more code that can be added to the
     * main code section.
     * 
     * The code is initially generated with gotos, but alternatively the gotos are 
     * replaced with function calls instead.
     * 
     * @param gotos - GotoStatements to be processed 
     * @param track - Track of seen Labels to avoid getting in a loop. 
     *          If we encounter goto to a label that has been processed already, 
     *          then leave the goto there
     */
    private static void processGotos(List<GAModelElement> gotos,
            HashSet<String> track) {
        
        for (GAModelElement e : gotos) {
            GotoStatement g = (GotoStatement) e;
            String targetLbl = g.getTargetName();
            
            if (track.contains(targetLbl)) {
                // Seen target label. Leave the jump.
            	keptJumps.add(g);
                continue;
                
            } else {       
                // "Push" the label to the track
                track.add(targetLbl);
            
                // Check, if we have any code for this label.
                // Note that there might exist also entries with null code!                    
                List<Statement> lblCode = junctionCode.get(targetLbl);                    
                if (lblCode == null) {
                    
                    if (junctionCode.containsKey(targetLbl)) {
                        // Remove a redundant jump
                        g.removeMe();
                    } else {
                        // A goto from unknown source - stays.
                    	keptJumps.add(g);
                    }
                    
                } else {
                    
                    // There exists code for this goto in the table.
                    
                    // Move the code found in the table.
                    // Add also any gotos in the retrieved code to a list for 
                    // later processing.
                    List<GAModelElement> newGotos  = new LinkedList<GAModelElement>();
                    for (Statement s : lblCode) {
                        if (s instanceof GotoStatement) {
                            newGotos.add(s);
                        } else {
                            newGotos.addAll(s.getAllChildren(GotoStatement.class));
                        }
                    }
                    
                	// Replace the original goto with the retrieved code
                    // Note: We have to do it before going deeper, otherwise we can't
                    // properly replace the inner gotos.                    
                    g.replaceMe(lblCode);                    	                   	
                    
                    // Remove the entry from the table.
                    // All subsequent references to this junction must be jumps (gotos or calls)
                    junctionCode.remove(targetLbl);
                        
					// Process the gotos in the retrieved code and add the
					// returned functions to the list.
                    processGotos(newGotos, track);
                    
                }

                // "Pop" the label from the track
                track.remove(targetLbl);                
            }
        }
    }

	/** 
	 * Convert the left GotoStatements to function calls
	 * together with the respective target locations
	 *  
     * @param sourceEl
     *            Chart or graphical function from which the code is generated
	 */
    @SuppressWarnings("unchecked")
	private static void jumpsToFunctions(GASystemModelElement sourceEl) {
    	Map<String, Function_CM> newFunMap = new HashMap<String, Function_CM>();
    	
    	final String methodName = "jumpsToFunctions";
    	
    	Module m;
    	Function_CM rootFun;
    	
    	if (sourceEl instanceof ChartBlock) {
    		rootFun = ((ChartBlock) sourceEl).getComputeFunction().getCodeModelElement();
    	} else if (sourceEl instanceof GraphicalFunction) {
    		rootFun = ((GraphicalFunction) sourceEl).getCodeModelElement();
    	} else {
    		EventHandler.handle(EventLevel.ERROR, methodName, "", 
    				"Unexpected sourceElement type: " + sourceEl.getReferenceString());
    		return;
    	}
    	
		m = rootFun.getModule();
    	
    	for (GotoStatement g : keptJumps) {
    		String targetName = g.getTargetName();
    		Function_CM fun = newFunMap.get(targetName);
    		
    		if (fun == null) {
    			List<Variable_CM>         oldVars = new ArrayList<Variable_CM>();
    			List<FunctionArgument_CM> newArgs = new ArrayList<FunctionArgument_CM>();
    			
    			fun = makeJunctionFunctionStub(g, targetName, rootFun, oldVars, newArgs);
    			
    			// Add the newly created function to a module
    			m.getNameSpace().addElement(fun);
    			// And to the map
    			newFunMap.put(targetName, fun);
    			
    			// Make the function's body    			
    			Statement targStmt = g.getTargetStatement();
    			GAModelElement targParent = targStmt.getParent();
    			targStmt.setLabel(null); // Label no more needed  			
    			List<Statement> stmts = new LinkedList<Statement>();    			
   			
    			// Add the target statement and any immediate following statements 
    			// to the function body
    			Triple<String, List, Integer> res = PrivilegedAccessor.searchChildInListFields(targParent, targStmt);
    			// res should never be null
    			String fieldName = res.getLeft();
    			List   lst 		 = res.getMiddle();
    			int    pos 		 = res.getRight();
    			
    			// Statements to keep in the old location (excludes targStmt)  
    			List keepList = lst.subList(0, pos);    			
    			// Statements to move to the created function (includes targStmt)
    			stmts.addAll(lst.subList(pos, lst.size())); 
    			
    			// Update the truncated list in the original parent.
    			PrivilegedAccessor.setValue(targParent, fieldName, keepList, true);
    			    			
    			// Add a call to the created function    			
    			CallExpression callExp  = new CallExpression(fun, null);
    			Statement 	   callStmt = new ExpressionStatement(callExp);
    			callStmt.setParent(targParent);
    			keepList.add(callStmt);
    			
				// Move all old pointers in the to-be body of the function to
				// the variables that are now passed as arguments to the new
				// arguments
    			moveVarReferences(oldVars, newArgs, stmts);
    			
    			fun.getBody().addStatements(stmts);    			
    		}
    		
    		// Replace the current goto
			CallExpression callExp = new CallExpression(fun, null);
			g.replaceMe(new ExpressionStatement(callExp));
    		
    	}	
    }

	/**
	 * Move all old pointers in the to-be body of the function to the variables 
	 * that are now passed as arguments to the new arguments
	 * @param oldVars
	 * @param newArgs
	 * @param stmts
	 */
	private static void moveVarReferences(List<Variable_CM> oldVars,
			List<FunctionArgument_CM> newArgs, List<Statement> stmts) {
		
		Map <Variable_CM, List<VariableExpression>> oldRefMap = 
			new HashMap<Variable_CM, List<VariableExpression>>();
		
		// Temporary parent node
		CompoundStatement tmpStmt = new CompoundStatement(null, stmts);
		
		// Get all references to variables in the stmts
		List<GAModelElement> oldRefs = tmpStmt.getAllChildren(VariableExpression.class);
		
		// Fill the map for fast access.
		for (GAModelElement el : oldRefs) {
			VariableExpression refExp = (VariableExpression) el;
			// At this point references should be only
			//     to a valid Variable_CM or 
			//     null (general arguments: _io_ and _state_ are resolved later)			
			Variable_CM var = (Variable_CM) refExp.getVariable();
			if (var != null) {
				List<VariableExpression> lRefs = oldRefMap.get(var);
				if (lRefs == null) {
					lRefs = new ArrayList<VariableExpression>();
					oldRefMap.put(var, lRefs);
				}
				lRefs.add(refExp);
			}
		}
		
		// Move pointers
		int i = 0;
		for (Variable_CM oldVar : oldVars) {
			List<VariableExpression> lRefs = oldRefMap.get(oldVar);
			if (lRefs != null) {
				// Adjust the reference - If the original variable wasn't a
				// pointer variable, then we need to make a dereference
				// expression instead, because all new variables are pointers.
				if (!(oldVar.getDataType() instanceof TPointer)) {
					// Make a dereference and replace expression
					for (VariableExpression ve : lRefs) {
						Expression newVarExp = new VariableExpression(newArgs.get(i));
						Expression refExp    = new UnaryExpression(newVarExp, UnaryOperator.DEREF_OPERATOR);
						ve.moveCommonAttributes(refExp);
						ve.replaceMe(refExp);
					}
				} else {
					// Just change the referenced variable
					for (VariableExpression ve : lRefs) {
						ve.setVariable(newArgs.get(i));
					}
				}
			}
			i++;
		}
	}

	/**
	 * @param g GotoStatement
	 * @param targetName
	 * @param rootFun function from which the call is made
	 * @param oldVars arguments and local variables of the rootFun
	 * @param newArgs Arguments to the newly created function
	 * 			(1-1 correspondence between oldVars and newArgs) 
	 * @return
	 */
	private static Function_CM makeJunctionFunctionStub(GotoStatement g,
				String targetName, Function_CM rootFun, 
				List<Variable_CM> oldVars, List<FunctionArgument_CM> newArgs) {
		// Make a function from the code fragment that the goto points to
		// The function will be created from the code statements starting
		// from the target statement up to the end of the immediate container 
		// of the target statement. Given the way this code is generated this 
		// is semantically correct.
		
		// Make function
		FunctionBody funBody = new SequentialFunctionBody(); // Don't add statements yet
		Function_CM fun = new Function_CM(null, funBody, new TVoid(), DefinitionScope.LOCAL, FunctionStyle.FUNCTION);

		// Set back-reference to the source element of the new function
		// This will be used, when updating arguments of the function.
		Junction j = (Junction) g.getSourceElement();
		fun.setSourceElement(j);
		
		// Construct a name for the created function
		String funName;
		GAModelElement parent = j.getParent().getParent(); // Junction's direct parent is a Composition
		if (parent instanceof ChartBlock) {
			funName = parent.getName();
		} else if (parent instanceof ContainerNode) {
			funName = ((ContainerNode) parent).getPathString("_",
	                PathOption.STATEMODEL_NO_CHART);    				
		} else if (parent instanceof GraphicalFunction) {
			funName = parent.getName();    				
		} else {
			EventHandler.handle(EventLevel.ERROR, "jumpsToFunctions()", "", 
					"Unexpected parent class for a junction:\n  " + j.getReferenceString());
			return null;
		}
		// We assume that the name of the target label is of form "L###[_###]"
		fun.setName(funName + "_J" + targetName.substring(1));
		// Annotation
		fun.addAnnotation("Function to evaluate joining branches at junction " + j.getExternalID());

		// Handle arguments and local variables of the rootFunction

		List<Expression> newRefs = new ArrayList<Expression>();

		// Formal parameters	
		for (FunctionArgument_CM arg : rootFun.getArguments()) {
			oldVars.add(arg);
			FunctionArgument_CM newArg = arg.getCopy();
			// Make the arguments implicit. The argument values will be
			// taken from the caller function.
			newArg.setImplicit(true); 
			newArg.setOptimizable(true); 
			// Adjust the datatype - At this stage we don't now, if the argument
			// will be written to or not. So, we need it to be a pointer to
			// enable (possible) value change back-propagation.
			if (!(newArg.getDataType() instanceof TPointer)) {
				newArg.setDataType(new TPointer(newArg.getDataType()));
			}
			newArgs.add(newArg);
			// Implicit reference for passing deeper from the new function
			Expression argRef = new VariableExpression(newArg);
			newRefs.add(argRef);
		}
		
		// Local variables in the function
		for (NameSpaceElement_CM el : rootFun.getBody().getNameSpace().getNsElements()) {
			if (el instanceof Variable_CM) {
				Variable_CM var = (Variable_CM) el;
				oldVars.add(var);
				FunctionArgument_CM newArg = new FunctionArgument_CM();
				// Make the arguments implicit. The argument values will be
				// taken from the caller function.
				newArg.setName(var.getName());
				newArg.setImplicit(true);
				newArg.setOptimizable(true);
				newArg.setDataType(var.getDataType());
				// Adjust the datatype - At this stage we don't now, if the argument
				// will be written to or not. So, we need it to be a pointer to
				// enable (possible) value change back-propagation.
				if (!(newArg.getDataType() instanceof TPointer)) {
					newArg.setDataType(new TPointer(newArg.getDataType()));
				}
				newArgs.add(newArg);
				// Implicit reference for passing deeper from the new function
				Expression argRef = new VariableExpression(newArg);
				newRefs.add(argRef);
			}
		}
		
		fun.setArguments(newArgs);
		
		// Implicit argument values for passing deeper
		fun.getBody().getNameSpace().setImplicitArgumentValues(newRefs);
		
		return fun;
	}
	
    /**
     * Tries to resolve by-name references to labels and removes all unused
     * labels.
     * 
     * List<GAModelElement> stmts
     * 
     */
    private static void resolveLabels(List<GAModelElement> stmts) {
        HashMap<String, Label> targets = new HashMap<String, Label>();
        HashSet<Label> usedTargets = new HashSet<Label>();

        collectTargets(targets, stmts);

        resolveGotoRefs(targets, usedTargets, keptJumps);
            
        // Scan all labels and remove unused ones
        for (Label l : targets.values()) {
            if (!usedTargets.contains(l)) {
                l.removeMe();
            }
        }
    }

    /**
     * Scan all GotoStatements and resolve labels.
     * Removes jumps that lead to the end of a compound statement - illegal in C. 
     * NOTE: this transformation is valid, due to the way code from charts is currently 
     * generated AND we assume that jumps only originate from auto-generated code in charts. 
     * There is never a jump generated to outside the statement parenting the goto statement.
     *  
     * I.e following can occur:
     *      if (...) {
     *          ...
     *  L1:
     *      } else {
     *          ...
     *          goto L1;
     *      }
     *      
     * But following cannot/must not occur:
     *      if (...) {
     *          ...
     *          goto L1;
     *      }
     *      <some code>
     *      if (...) {
     *          ...
     *  L1:
     *      ...
     *      
     *  Also following cannot/must not occur:
     *      if (...) {
     *          if (...) {
     *              ...
     *  L1:
     *          }
     *          <some code>  
     *      } else {
     *          ...
     *          goto L1;
     *      }
     *  
     * In the first case omitting the jump is legal, but in the other cases not.
     * This method only checks, if the jump is to the end of the compound statement.
     *  
     * @param targets
     * @param usedTargets
     * @param gotos
     */
    private static void resolveGotoRefs(HashMap<String, Label> targets,
            HashSet<Label> usedTargets, List<GotoStatement> gotos) {
    	
    	List <GotoStatement> removedGotos = new LinkedList<GotoStatement>();
    	
        for (GotoStatement g : gotos) {
            Label lTarget = null;
            if (g.getTarget() == null) {
                // Try to resolve by-name reference
                String targetName = g.getTargetName();
                Label l = targets.get(targetName);
                if (l != null) {
                    g.setTarget(l);
                    lTarget = l;
                } else {
                    EventHandler.handle(EventLevel.ERROR, "", "",
                        "Cannot resolve target label " + targetName
                                + " in a goto statement:\n"
                                + g.getReferenceString());
                }
            } else {
                lTarget = g.getTarget();                
                // Check, if the label exists in the current module
                if (!targets.containsKey(lTarget.getName())) {
                    EventHandler.handle(
                        EventLevel.ERROR, "", "",
                        "Target label:\n"
                            + lTarget.getReferenceString()
                            + "does not exist in the module containing the goto statement:\n"
                            + g.getReferenceString());
                }
            }
            
            // Check, if the jump is redundant.
            // NOTE: We assume that jumps only exist in code generated from charts and
            // they occur only according to a specific pattern (see also comments in the header)  
            Statement target = g.getTargetStatement();
            Assert.assertNotNull(target, "Target of a GotoStatement: " + g.getReferenceString());
            List <Statement> block = target.getStartingCodeBlock();
            if (Statement.isEffectFree(block)) {
                g.removeMe();
                removedGotos.add(g);
                lTarget = null; // Avoids that the label will be marked as used below                
            }            
            
            // Mark the label as used
            if (lTarget != null) {
                usedTargets.add(lTarget);
            }
        }
        
        // Remove obsolete gotos from the list
        gotos.removeAll(removedGotos);
        
    }

    /**
     * Scan all Statements and store jump targets 
     */
    private static void collectTargets(HashMap<String, Label> targets,
            List<GAModelElement> els) {
        for (GAModelElement el : els) {
        	Statement s = (Statement) el;
            Label lNew = s.getLabel();
            if (lNew != null) {
                Label lOld = targets.get(lNew.getName());
                if (lOld == null) {
                    targets.put(lNew.getName(), lNew);
                } else {
                	EventHandler.handle(EventLevel.ERROR, "", "",
                            "Duplicate label " + lNew
                          + "\n  Statement: " + s.getReferenceString());                 
                }
            }
        }
    }	

}
