/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/DataTypeUtils.java,v $
 *  @version	$Revision: 1.34 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealInteger;

import java.util.List;


/**
 * Utility functions for DataTypes.
 * 
 */
public class DataTypeUtils {

    /**
     * Chooses more "strongest" data type among 2 TRealNumeric data types
     * 
     * @deprecated Use chooseLeastGeneralType instead
     * 
     * @param firstDT
     * @param secondDT
     * @return
     */
    
    public static TPrimitive chooseStrongestScalarDT(TPrimitive firstDT, 
            TPrimitive secondDT) {
        
        GADataType resultDT;
        if (firstDT.isSubType(secondDT)) {
            resultDT = secondDT;
        } else {
            resultDT = firstDT;
        }
        if ((firstDT instanceof TRealInteger)
            && (secondDT instanceof TRealInteger)) {
            TRealInteger intResultDT = (TRealInteger) resultDT;
            TRealInteger firstIntDT = (TRealInteger) firstDT;
            TRealInteger secondIntDT = (TRealInteger) secondDT;
            if (!firstIntDT.isSigned()) {
                intResultDT.setSigned(false);
            } else {
                intResultDT.setSigned(secondIntDT.isSigned());
            }
        } 
        if (resultDT != null) {
            return (TPrimitive) resultDT;
        } else {
            return null;
        }
    }
    
    /**
     * Chooses more "strongest" data type
     * 	- in case of scalar types returns strongest data type using 
     * 			chooseStrongesScalarDT
     *  - in case of array types returns largest array
     *  
     * @deprecated Use chooseLeastGeneralType instead
     * 
     * @param leftDT 
     * @param rightDT
     * @return
     */
    public static GADataType chooseStrongestDT(GADataType leftDT, 
            GADataType rightDT) {
        boolean leftIsScalar = leftDT.isScalar();
        boolean rightIsScalar = rightDT.isScalar();
        TPrimitive baseType = chooseStrongestScalarDT(
                leftDT.getPrimitiveType(), rightDT.getPrimitiveType());
        
        // both dt are scalar
        if (leftIsScalar && rightIsScalar) {
            return baseType;
            
        // one of the dt-s is scalar, second is not
        } else if (leftIsScalar && !rightIsScalar) {
                if (rightDT.getPrimitiveType() != baseType) {
                    TArray rightADT = (TArray) rightDT.getCopy();
                    rightADT.setBaseType(leftDT.getPrimitiveType());
                    return rightADT;
                } else {
                    return rightDT;
                }
                
                
        } else if (!leftIsScalar && rightIsScalar) {
                if (leftDT.getPrimitiveType() != baseType) {
                    TArray leftADT = (TArray) leftDT.getCopy();
                    leftADT.setBaseType(leftDT.getPrimitiveType());
                    return leftADT;
                } else {
                    return leftDT;
                }
        // both dt are not scalar
        } else {
            TArray leftADT = (TArray) leftDT;
            TArray rightADT = (TArray) rightDT;
            if (leftADT.isSubType(rightADT)) {
                return rightADT;
            } else if (rightADT.isSubType(leftADT)) {
                return leftADT;
            } else {
                return null;
            }
        }
    }

    /** 
     * If leftDT and rightDT have the same dimension, returns strongest type
     * using chooseStrongestDT, null otherwise
     * 
     * @deprecated Use chooseLeastGeneralType instead
     *  
     * @param leftDT
     * @param rightDT
     * @return
     */
    public static GADataType chooseStrongestDT_forceDim(GADataType leftDT, 
            GADataType rightDT) {
    	
    	if (checkDimensions(leftDT, rightDT, true, false)) {
        	return chooseStrongestDT(leftDT, rightDT);
    	} else {
    		return null;
    	}
    }
    
    /**
     * compares dimensions of two array types - checks that their dimensions
     * are the same. See also argument vectorPromotion.
     * 
     * @param fstDT data type of the first argument to compare
     * @param sndDT data type of the first argument to compare
     * @param vectorPromotion
     *            if true and one type is a vector and the other a
     *            vectorMatrix (row or column matrix, then the vector is
     *            promoted to a matrix)
     * @param checkDimensionTypes flag, in case value is true -> checks that 
     * types of corresponding dimensions are equal (used in TAdaPrinter only)
     *            
     * @return true if dimensions are equal, false otherwise
     */
    public static boolean checkDimensions(TArray fstDT, TArray sndDT, 
            boolean vectorPromotion, boolean checkDimensionTypes) {
    	if (fstDT == null || sndDT == null) {
    		return false;
    	}
    	
        int[] fstDims = fstDT.getDimensionsLengths();
        int[] sndDims = sndDT.getDimensionsLengths();
        
        Expression fstDTDim;
        Expression sndDTDim;
        if (fstDims.length != sndDims.length) {
            
            // Different dimensionalities, check vector promotion
            if (!vectorPromotion) {
                return false;
                
            } else {
                boolean validPromotion = false;
                boolean validDimDataTypes = true;
                if (fstDT.isVector() && sndDT.isVectorMatrix()) {
                    int n = sndDT.isRowMatrix() ? sndDims[1] : sndDims[0];  
                    validPromotion = fstDims[0] == n;
                    if (checkDimensionTypes) {
                        fstDTDim = fstDT.getDimensions().get(0);
                        sndDTDim = sndDT.getDimensions().get(n);
                        validDimDataTypes = fstDTDim.getDataType()
                            .equalsTo(sndDTDim.getDataType());
                    }
                } else if (fstDT.isVectorMatrix() && sndDT.isVector()) {
                    int n = fstDT.isRowMatrix() ? fstDims[1] : fstDims[0];  
                    validPromotion = sndDims[0] == n;
                    if (checkDimensionTypes) {
                        fstDTDim = fstDT.getDimensions().get(n);
                        sndDTDim = sndDT.getDimensions().get(0);
                        validDimDataTypes = fstDTDim.getDataType()
                            .equalsTo(sndDTDim.getDataType());
                    }
                } 
                
                if (checkDimensionTypes) {
                    return validPromotion && validDimDataTypes;
                } else {
                    return validPromotion;
                }
            }  
            
        
        } else {
            
            // Same dimensionalities, check lengths
            for (int i = 0; i < fstDims.length; i++) {
                if (fstDims[i] != sndDims[i]) {
                    return false;
                }  
                if (checkDimensionTypes) {
                    fstDTDim = fstDT.getDimensions().get(i);
                    sndDTDim = sndDT.getDimensions().get(i);
                    if (!fstDTDim.getDataType().equalsTo(sndDTDim.getDataType())) {
                        return false;
                    }
                }
            }                    
            return true;
        }
    }

    /**
     * compares dimensions of two data types - in case of two scalars returns
     * true - in case of two arrays checks that their dimensionality is the same
     * - returns false in case of incompatible dimensionality or mixture of
     * arrays and scalars. See also argument vectorPromotion.
     * 
     * @param firstDT
     * @param secondDT
     * @param vectorPromotion
     *            if true and one type is a vector and the other a
     *            vectorMatrix (row or column matrix, then the vector is
     *            promoted to a matrix)
     * @return
     */
    public static boolean checkDimensions(GADataType firstDT, GADataType secondDT, 
            boolean vectorPromotion, boolean checkDimensionTypes) {
    	if (firstDT == null || secondDT == null) {
    		return false;
    	}

    	if (firstDT instanceof TArray &&
    			secondDT instanceof TArray) {
    		// both are arrays -- check the dimensions
    		return checkDimensions((TArray) firstDT, (TArray) secondDT, 
    		        vectorPromotion, checkDimensionTypes);
    	} else if (firstDT.isScalar() && secondDT.isScalar()) {
    		// both are scalars 
    		return true;
    	} else {
    		// combination of array and scalar
    		return false;
    	}
    }
    
    /**
     * List version of chooseLeastGeneralType(GADataType dt1, GADataType dt2,
     *       boolean scalarExpransion)
     */
    public static GADataType chooseLeastCommonType(List <GADataType> dtList, 
            boolean scalarExpansion) {
        GADataType resultDT = null;
        for (GADataType dt: dtList) {
            if (resultDT == null) {
                resultDT = dt;
            } else {
                resultDT = chooseLeastCommonType(resultDT, dt, scalarExpansion);
            }
        }
        return resultDT;
    }
    
    /**
     * List version of chooseLeastCommonType(TPrimitive dt1, TPrimitive dt2)
     */
    public static TPrimitive chooseLeastCommonPrimitiveType(List <GADataType> dtList) {
        TPrimitive resultDT = null;
        for (GADataType dt: dtList) {
            if (resultDT == null) {
                resultDT = dt.getPrimitiveType();
            } else {
                resultDT = chooseLeastCommonType(resultDT, dt.getPrimitiveType());
            }
        }
        return resultDT;
    }

    /**
     * @param dt1
     *            first data type
     * @param dt2
     *            first data type
     * @param scalarExpransion
     *            if true, then a combination of scalars and non-scalars is also
     *            accepted, providing that the primitive types can be unified.
     * @return least common supertype of given data types or null,
     *         when such a type cannot be found.
     */
    public static GADataType chooseLeastCommonType(GADataType dt1, GADataType dt2,
            boolean scalarExpransion) {
        
    	if (dt1 == null || dt2 == null) {
    		return null;
    	}
    	
        TArray arResult;
        
        // both data types are scalars
        if (dt1.isScalar() && dt2.isScalar()) {
            return chooseLeastCommonType( (TPrimitive) dt1, (TPrimitive) dt2);
            
        // 1 of the data types is scalar
        } else if (dt1.isScalar() || dt2.isScalar()) {
            if (scalarExpransion) {
                arResult = dt1.isScalar() ? (TArray) dt2.getCopy() : (TArray) dt1.getCopy();
            } else {
                return null;
            }
            
        // both data types are arrays    
        } else  {
            // Check dimensions (without vector promotion).
            if (checkDimensions((TArray) dt1, (TArray) dt2, false, false)) {
                arResult = (TArray) dt1;
            } else {
                return null;
            }
        }
        
        arResult.setBaseType(chooseLeastCommonType(dt1.getPrimitiveType(), 
                dt2.getPrimitiveType()));
        
        return arResult;
    }
    
    /**
     * 
     * @param dt1
     *            first primitive data type
     * @param dt2
     *            first primitive data type
     * @return least common supertype of given data types or null,
     *         when such a type cannot be found.
     */
    public static TPrimitive chooseLeastCommonType(TPrimitive dt1, 
            TPrimitive dt2) {
    	if (dt1 == null || dt2 == null) {
    		return null;
    	}

        if ((dt1 instanceof TRealInteger) && (dt2 instanceof TRealInteger)) {
            return chooseLeastCommonType( (TRealInteger) dt1, (TRealInteger) dt2);
        } else if (dt1.isSubType(dt2)) {
            return dt2;
        } else if (dt2.isSubType(dt1)) {
            return dt1;
        } else {
            return null;
        }
    }
    
    
    /**
     * 
     * 
     * @param dt1
     *            first integer data type
     * @param dt2
     *            second integer data type
     * @return least common supertype for 2 integer data types
     */
    public static TRealInteger chooseLeastCommonType(TRealInteger dt1, TRealInteger dt2) {
    	
    	if (dt1 == null || dt2 == null) {
    		return null;
    	}

        int nBits = - 1;
        boolean isSigned = dt1.isSigned() || dt2.isSigned()? 
                true: false;
        if (dt1.isSigned() && (!dt2.isSigned())) {
            if (dt2.getNBits() >= dt1.getNBits()) {
            	// TODO: (TF:862) NB! This is not always compatible with what SImulink does (ToNa 14/07/10)
                nBits = dt2.getNBits() * 2;
            } else {
                nBits = dt1.getNBits();
            }
        } else if (dt2.isSigned() && (!dt1.isSigned())) {
            if (dt1.getNBits() >= dt2.getNBits()) {
            	// TODO: (TF:862) NB! This is not always compatible with what SImulink does (ToNa 14/07/10)
                nBits = dt1.getNBits() * 2;
            } else {
                nBits = dt1.getNBits();
            }
        } else {
            nBits = dt1.getNBits() > dt2.getNBits() ? 
                    dt1.getNBits() : dt2.getNBits(); 
        }
        TRealInteger result = (TRealInteger) dt1.getCopy();
        result.setNBits(nBits);
        result.setSigned(isSigned);
        if (result.getNBits() > 32) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    "DataTypeUtils.chooseLeastGeneralType", "", 
                    "number of bits of the result data type is "
                    + "greater than 32. \nData Types: " 
                    + "\n" + dt1
                    + "\n" + dt2
                    + "\nResult data type" + result);
            return null;
        }
        return result;
    }

    /**
     * 
     * checks if dt1 and dt2 are valid for assignment
     * 
     * @param dt1
     *            data type of the left side of an assign statement 
     * @param dt2
     *            data type of the right side of an assign statement
     *            
     * @return true if the assignment is valid, false otherwise
     */
    public static boolean isValidNonStructureAssignment(GADataType dt1, 
    														GADataType dt2) {
        
    	if (dt1 == null || dt2 == null) {
    		return false;
    	}

        // Check dimensions. If the second one is scalar, then it is always ok.
        if (!dt2.isScalar()) {
            if (!checkDimensions(dt1, dt2, true, false)) {
                return false;
            }
        }        
        
        // Check primitive types
        TPrimitive dtPrim1 = dt1.getPrimitiveType();
        TPrimitive dtPrim2 = dt2.getPrimitiveType();
        
        // left expression type must be sub type of right expression type
        // or right expression type sub type of left expression type
        if (dtPrim2.isSubType(dtPrim1) || dtPrim1.isSubType(dtPrim2)) {
            return true;
            
        } else if (dtPrim1 instanceof TBoolean && dtPrim2 instanceof TRealInteger) {
            // We allow assignment of integers to booleans. The type conversion
            // is implicit and added automatically to the generated code.
            return true; 
            
        } else {
            return false;
        }
    }
    
    
    /**
     * checks if dt1 and dt2 are valid for assignment
     * 
     * @param dt1 
     *          data type of the left side of an assign statement 
     * @param 
     *          dt2 data type of the right side of an assign statement
     * 
     * @return true if the assignment is valid, false otherwise
     */
    public static boolean isValidAssignment(GADataType dt1, GADataType dt2) {
        
    	if (dt1 == null || dt2 == null) {
    		return false;
    	}

        // dt1 is structure
        if (dt1 instanceof TCustom) {
            
            // both are structures
            if (dt2 instanceof TCustom) {
                
                // both are the same structure data type
                if (dt1.equalsTo(dt2)) {
                    return true;
                }
            
            // dt1 is structure, dt2 is not structure
            } else {
                TCustom customDT1 = (TCustom) dt1;
                for (StructureMember member: customDT1.getMembers()) {
                    if (!(isValidAssignment(member.getDataType(), dt2.getPrimitiveType()))) {
                        return false;
                    }
                }
                
                // all members were correct
                return true;
            }
            
        // dt1 is not structure
        } else {
            return isValidNonStructureAssignment(dt1, dt2);
        }
        return false;
    }

    /**
     * Sets the right nBites and signed to the integer type
     * that is suitable for given value
     * 
     * @param integer integer data type
     * @param value value
     * @return
     */
    public static TRealInteger scaleFor(TRealInteger integer, int value) {
        int minValue;
        int maxValue;
        int nBits = integer.getNBits();
        TRealInteger copy = (TRealInteger) integer.getCopy();
        boolean wasChanged = false;
        if (value < 0 && !integer.isSigned()) {
            copy.setSigned(true);
            wasChanged = true;
        }
        if (nBits > 0) {
            maxValue = (int) Math.pow(2, nBits) - 1;
            minValue = 0;
            if (copy.isSigned()) {
                maxValue = (maxValue + 1)/ 2 - 1;
                minValue = minValue - maxValue - 1;
            }
            if (value < minValue || value > maxValue) {
                if (nBits >= 32) {
                    EventHandler.handle(EventLevel.ERROR, 
                            "scaleFor", "",
                            "value of nBits (" + ") is greater than it is acceptable"
                            		+ nBits + "for TRealInteger: " 
                            		+ integer.getReferenceString());
                    return null;
                } else {
                    copy.setNBits(nBits * 2);
                    return scaleFor(copy, value);
                }
    
            } else {
                if (wasChanged) {
                    return copy;
                } else {
                    return integer;
                }
            }
        } else {
            EventHandler.handle(EventLevel.ERROR, "", "",
                    "nBits count must be determined " + "in that stage. nBits:"
                            + nBits);
            return null;
    
        }
    }
    
    /**
     * Sets the right nBites and signed the integer type
     * that is suitable for the range fromValue .. toValue
     * 
     * @param integer data type
     * @param fromValue first value of the range
     * @param toValue second value of the range
     */
    public static void scaleFor(TRealInteger integer, int fromValue, 
            int toValue) {
        int min = fromValue < toValue? fromValue: toValue;
        int max = fromValue > toValue? fromValue: toValue;
        boolean signed = min < 0;
        
        TRealInteger dt = DataTypeUtils.scaleFor((TRealInteger) integer.getCopy(), max);
        int nBits = dt.getNBits();
        if (signed) {
            nBits *= 2;
        }
        integer.setNBits(nBits);
        integer.setSigned(signed);
    }

    /**
     * Prepares data type for attaching to a code model object
     * If the parent of given data type is already set, makes copy
     * of the data type. Otherwise returns the argument dt in 
     * unchanged form.
     * 
     * @param dt	- GADataType to prepare 
     * @return		- GADataType for attaching to code model element 
     */
    public static GADataType attachDataType(GADataType dt) {
        if (dt == null) {
            return null;
        } else {        
            if (dt.getParent() == null) {
                return dt;
            } else {
                return dt.getCopy();
            }
        }
    }
    
    /**
     * Checks if dt1 and dt2 data type combination is one of the
     * following: 
     * 1) both are signed integers
     * 2) both are unsigned integers
     * 
     * @return true if data type combination is correct, otherwise 
     * return false
     */
    public static boolean integerSignCheck(GADataType dt1, 
            GADataType dt2) {
        if ((dt1 instanceof TRealInteger)
                && (dt2 instanceof TRealInteger)) {
            TRealInteger leftIntDT = (TRealInteger) dt1;
            TRealInteger rightIntDT = (TRealInteger) dt2;
            if (leftIntDT.isSigned() && rightIntDT.isSigned()) {
                return true;
            } else if (!leftIntDT.isSigned() && !rightIntDT.isSigned()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
