package geneauto.models.utilities.emlconverter;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.NumericExpression;
import geneauto.models.gacodemodel.expression.ParenthesisExpression;
import geneauto.models.gacodemodel.expression.TrueExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealFloatingPoint;
import geneauto.models.gadatatypes.TVoid;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.DataTypeAccessor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.antlr.runtime.tree.Tree;

public class EMLConverter {
	
	/**
	 * Takes AST with expression as input and returns corresponding
	 * GACOdeModel expression object.
	 * 
	 * @param exprAST -- AST containing expression components
	 * @param bitwise -- bitwise C-operations on 
	 * @param element -- GACodeModel element containing the expression
	 * 					 if passed, expression annotations will be attached to
	 * 					 this element
	 * @return		  -- Expression
	 */
    public static Expression transformExpression(
    		Tree exprAST, boolean bitwise, GAModelElement element) {
        Expression expr = null;
        String text = exprAST.getText();
        if ("RealExpression".equals(text)) {
            expr = new DoubleExpression(exprAST.getChild(0).getText());

        } else if ("IntegerExpression".equals(text)) {
            expr = new IntegerExpression(exprAST.getChild(0).getText());

        } else if ("VariableExpression".equals(text)) {
            String name = nameFromASTNode(exprAST.getChild(0));
            expr = new VariableExpression(name);
            if (exprAST.getChildCount() > 1) {
                // Handle index expressions and annotations
                for (int i = 1; i < exprAST.getChildCount(); i++) {
                	Tree childAST = exprAST.getChild(i);
                	if (childAST.getText().equals("Annotations")) {
                        addAnnotations(expr, childAST);
                	} else {
                		expr.addIndexExpression(transformExpression(
                				childAST, bitwise, element));
                	}
                }
            }

        } else if ("ParenthesisExpression".equals(text)) {
            Expression cExpr = transformExpression(
            		exprAST.getChild(0),bitwise, element);
            expr = new ParenthesisExpression(cExpr);

        } else if ("CallExpression".equals(text)) {

            // CallExpression
            String name = nameFromASTNode(exprAST.getChild(0));
            List<Expression> arguments = new ArrayList<Expression>();
            BlankStatement tmpStmt = new BlankStatement();
            for (int i = 1; i < exprAST.getChildCount(); i++) {
            	Tree childAST = exprAST.getChild(i);
            	if (childAST.getText().equals("Annotations")) {
            		// Store the annotations to a temporary location
                    addAnnotations(tmpStmt, childAST);
            	} else {
            		arguments.add(transformExpression(
            				childAST, bitwise, element));
                }
            }

            // Check, whether it is a call to a data type conversion function,
            // like uint8(...), or ordinary call.
            GADataType dt = DataTypeAccessor.parseType(name, false);
            if (dt == null) {
                
                /* 
                 * In case data type is not defined it is assumed 
                 * that it is TVoid 
                 * 
                 * See TF 859: Wrong data type definition of call expression's 
                 * data type in EMLConverter.transformExpression
                 */
                expr = new CallExpression(name, arguments, new TVoid());
            } else {
                if (arguments == null || arguments.size() != 1) {
                    // TODO Add reference to the location in the source model.
                    EventHandler.handle(EventLevel.ERROR, "", "",
                            "Expecting exactly 1 argument to a datatype conversion function: "
                                    + name + ". Number of given arguments: "
                                    + arguments.size(), "");
                    return null;
                }
                Expression cExpr = transformExpression(exprAST.getChild(1),
                        bitwise, element);
                expr = new UnaryExpression(cExpr, UnaryOperator.CAST_OPERATOR);
                expr.setDataType(dt);
            }
            // Get the annotations from the temporary location
            expr.addAnnotations(tmpStmt.getAnnotations());
        } else if ("TypedExpression".equals(text)) {

            // Expression with explicit type cast
            String name = nameFromASTNode(exprAST.getChild(0));
            
            List<Expression> arguments = new ArrayList<Expression>();
            BlankStatement tmpStmt = new BlankStatement();
            for (int i = 1; i < exprAST.getChildCount(); i++) {
            	Tree childAST = exprAST.getChild(i);
            	if (childAST.getText().equals("Annotations")) {
            		// Store the annotations to a temporary location
                    addAnnotations(tmpStmt, childAST);
            	} else {
            		arguments.add(transformExpression(
            				childAST, bitwise, element));
                }
            }

            // Check, whether it is a call to a data type conversion function,
            // like uint8(...), or ordinary call.
            GADataType dt = DataTypeAccessor.parseType(name, false);
            if (dt == null) {
            	// for robustness -- if we reach this part then parser and AST
            	// converter are out of sync
                EventHandler.handle(EventLevel.ERROR, "", "",
                        "Unknown data type " + name + " used in type cast", "");
                return null;
            } else if (dt instanceof TPrimitive){
                if (arguments == null || arguments.size() != 1) {
                    // TODO Add reference to the location in the source model.
                    EventHandler.handle(EventLevel.ERROR, "", "",
                            "Expecting exactly 1 argument to a datatype conversion function: "
                                    + name + ". Number of given arguments: "
                                    + arguments.size(), "");
                    return null;
                }
                Expression cExpr = transformExpression(exprAST.getChild(1),
                        bitwise, element);
                
                expr = castExpression(cExpr, (TPrimitive) dt);
            } else {
                EventHandler.handle(EventLevel.ERROR, "", "",
                        "Can not cast to non-primitive data type"
                                + name + "!", "");
                return null;            	
            }
            // Get the annotations from the temporary location
            expr.addAnnotations(tmpStmt.getAnnotations());
                        
        } else if ("UnaryExpression".equals(text)) {

            // UnaryExpression
            Expression cExpr = transformExpression(exprAST.getChild(0),
                    bitwise, element);
            String opStr = exprAST.getChild(1).getText();
            UnaryOperator op = UnaryOperator.fromEML(opStr, bitwise);
            
            if (op == null) {
                // TODO Add reference to the location in the source model.
                EventHandler.handle(EventLevel.ERROR, "", "",
                        "Unsupported unary operator: " + opStr, "");
                return null;
            }
            
            // in case of "-" and numeric literal argument simply change the 
            // sign of the argument
            if (op.equals(UnaryOperator.UNARY_MINUS_OPERATOR)
            		&& cExpr instanceof NumericExpression) {
            	((NumericExpression) cExpr).toggleSign();
            	expr = cExpr;
            } else {
            	expr = new UnaryExpression(cExpr, op);
            }

        } else if (exprAST.getChildCount() >= 2
                && BinaryOperator.fromEML(text, bitwise, true) != null) {
            // AnTo: It would be nicer to have here an AST tree here starting
            // with a node "BinaryExpression" as in previous cases.

            // BinaryExpression
            String opStr = text;
            BinaryOperator op = BinaryOperator.fromEML(opStr, bitwise, true);
            if (op == null) {
                // TODO Add reference to the location in the source model.
                EventHandler.handle(EventLevel.ERROR, "", "",
                        "Unsupported binary operator: " + opStr, "");
                return null;
            }
            Expression leftExp = transformExpression(exprAST.getChild(0),
                    bitwise, element);
            Expression rightExp = transformExpression(exprAST.getChild(1),
                    bitwise, element);
            expr = new BinaryExpression(leftExp, rightExp, op);

        } else if ("Annotations".equals(text)) {
        	if (element == null) {
        		// if no element was passed attach annotation to expression
        		addAnnotations(expr, exprAST);
        	} else {
        		// attach element to container
        		addAnnotations(element, exprAST);
        	}
        } else if ("TrueExpression".equals(text)) {
        	expr = new TrueExpression();
        } else if ("FalseExpression".equals(text)) {
        	expr = new FalseExpression();
        } else {
        	// the token was not recognised by this method. Check if package-
        	// specific extension knows the token
        	expr = transformExpressionExt(text, exprAST, bitwise, element);
        	
        	if (expr == null) {
	            // For robustness - code that should be never visited
	            EventHandler.handle(EventLevel.ERROR, "transformExpression",
	                    "", "Illegal expression type \"" + text + "\". "
	                            + "labelParser and this method are out of sync!?",
	                    "");
        	}
        }

        return expr;
    }

	/**
	 * A method to be implemented by subclass of this method
	 * If subclass wants to extend the list of symbols accepted by the 
	 * transformExpression method, it shall override this method.
	 * By default it is empty and return null
	 * 
	 * @param nodeName -- name of the currently parsed node
	 * @param exprAST -- AST containing expression components
	 * @param bitwise -- bitwise C-operations on 
	 * @param element -- GACodeModel element containing the expression
	 * 					 if passed, expression annotations will be attached to
	 * 					 this element
	 * @return		  -- Expression
	 */
    public static Expression transformExpressionExt(String nodeName,
    		Tree exprAST, boolean bitwise, GAModelElement element) {
    	
    	Expression expr = null;
    	// TODO: this should appear in EML parser , not in the basic parser
    	if ("ListExpression".equals(nodeName)) {
    		expr = new GeneralListExpression();
    		
            for (int i = 0; i < exprAST.getChildCount(); i++) {
                // Adds a new Expression (recursive call with children) to
                // the ListExpression
                ((GeneralListExpression)expr).addExpression(
                				transformExpression(exprAST.getChild(i), 
                												bitwise,
                												element));
            }
    	} else {
    		return null;
    	}
    	
    	return expr;
    }
    
	/**
	 * Takes AST with expression as input and returns corresponding
	 * GACodeModel expression object.
	 * This method is for Simulink import, were C-bit operations are not
	 * used
	 * 
	 * @param exprAST -- AST containing expression components
	 * @param element -- GACodeModel element containing the expression
	 * 					 if passed, expression annotations will be attached to
	 * 					 this element
	 * @return		  -- Expression
	 */
    public static Expression transformExpression(
    		Tree exprAST, GAModelElement element) {
    	return transformExpression(exprAST, false, element);
    }

	/**
	 * Checks if the input expression is explicitly typed. If it is, 
	 * invokes trsnforExpression and returns the result. 
	 * If the expression is not explicitly typed, raises an
	 * error and returns null.
	 * 
	 * @param exprAST -- AST containing expression components
	 * @param element -- GACodeModel element containing the expression
	 * 					 if passed, expression annotations will be attached to
	 * 					 this element
	 * @return		  -- Expression
	 */
    public static Expression transformTypedExpression(
    		Tree exprAST, GAModelElement element) {
        String text = exprAST.getText();
        if ("TypedExpression".equals(text)) {
        	return transformExpression(exprAST, element);
        } else {
            EventHandler.handle(EventLevel.ERROR, 
            		"EMLConverter.transformTypedExpression", 
            		"",
                    "Expression is not explicity typed!", "");
            return null;
        }
        
    }

    
    /**
     * @param nameAST
     *            AST representing an identifier, what might be a qualified name
     *            (e.g "s1.s2.x"). Each part of the name, including "." is
     *            expected to be a separate AST child.
     * @return a (possibly qualified) identifier string
     */
    public static String nameFromASTNode(Tree nameAST) {
        if ("DotIdent".equals(nameAST.getText())) {
            String name = nameAST.getChild(0).getText();
            for (int i = 1; i < nameAST.getChildCount(); i++) {
                name += nameAST.getChild(i).getText();
            }
            return name;
        } else {
            return nameAST.getText();
        }
    }

    public static void addAnnotations(GAModelElement elem, Tree annotationsAST) {
        for (int i = 0; i < annotationsAST.getChildCount(); i++) {
            String text = annotationsAST.getChild(i).getText();
            // Remove all /*, */ and // from the text.
            // These will be added automatically by the target language printer
            // TODO (to VaKo, AnTo) Solve this in grammar
            text = text.replaceAll("/\\*|\\*/|//", "");
            elem.addAnnotation(text);
        }
    }

    /**
     * Casts argument expr to given data type:
     * 		- in case of a list expression recursively casts each element
     * 		- in case of an IntegerExpression and floating point data type
     * 			replaces the integer expression with RealExpressio
     * 		- in case of any other numeric expression simply changes the 
     * 		data type
     * 		- in case of other types of expression creates new unary expression
     * 		with CAST_OPERATOR
     *  
     * @param expr		-- expression to cast
     * @param dataType	-- primitive type of each element
     * @return			-- resulting expression with applied data type
     */
    private static Expression castExpression(Expression expr, 
    								TPrimitive dataType ) {
    	Expression res = null;
    	
        if (expr instanceof NumericExpression) {
        	if (expr instanceof IntegerExpression
        			&& dataType instanceof TRealFloatingPoint) {
        		/* in case of a type cast of integer value to floating 
        		 * point data type replace IntegerExpression with 
        		 * RealExpression 
        		 */
        		res = new DoubleExpression(
        					((IntegerExpression) expr).getLitValue(), 
        					dataType);
        	} else if (dataType instanceof TBoolean) {
        	    /* in case of a type cast of numeric value to boolean
        	     * data type replace it with 
                 * True- or FalseExpression
                 */
                double value = ((NumericExpression) expr).evalReal().getRealValue();
                if (value == 0) {
                    res = new FalseExpression();
                } else {
                    res = new TrueExpression();
                }
            } else {
        		res = expr;
        	}
        	
        	res.setDataType(dataType);
        	
        } else if (expr instanceof GeneralListExpression) {
        	/* 
        	 * in case of a list expression apply the type cast to
        	 * each element in the list
        	 */
        	List<Expression> exprList = new LinkedList<Expression>();
        	boolean newObjectsInList = false;
        	for (Expression e : ((GeneralListExpression) expr).getExpressions()) {
        		// cast the element
        		Expression resE = castExpression(e, dataType);
        		// memorise, if new object was returned -- it means we will need
        		// to reset the expression list of expr later
        		if (e != resE) {
        			newObjectsInList = true;
        		}
        		exprList.add(resE);
        	}
        	if (newObjectsInList) {
        		((GeneralListExpression) expr).setExpressions(exprList);
        	}
        	
           	// recalculate the array data type from element types
        	expr.resetDataType();
        	
        	res = expr;
        } else {
        	res = new UnaryExpression(expr, UnaryOperator.CAST_OPERATOR);
        	res.setDataType(dataType);        	
        }        
        
        return res;
    }    
}
