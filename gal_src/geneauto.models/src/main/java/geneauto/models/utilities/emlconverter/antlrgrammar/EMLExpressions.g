//* Gene-Auto code generator
//* 
//*	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/emlconverter/antlrgrammar/EMLExpressions.g,v $
//*  @version	$Revision: 1.2 $
//*	@date		$Date: 2010-04-02 06:05:48 $
//*
//*  Copyright (c) 2006-2010 IB Krates OU
//*  	http://www.krates.ee, geneauto@krates.ee
//*
//*  
//*  This program is free software; you can redistribute it and/or modify
//*  it under the terms of the GNU General Public License as published by
//*  the Free Software Foundation, either version 3 of the License, or
//*  (at your option) any later version.
//*
//*  This program is distributed in the hope that it will be useful,
//*  but WITHOUT ANY WARRANTY; without even the implied warranty of
//*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//*  GNU General Public License for more details.
//*
//*  You should have received a copy of the GNU General Public License
//*  along with this program. If not, see <http://www.gnu.org/licenses/>
//*  
//*  Gene-Auto was originally developed by Gene-Auto consortium with support 
//*  of ITEA (www.itea2-org) and national public funding bodies in France, 
//*  Estonia, Belgium and Israel.  
//*  
//*  The members of Gene-Auto consortium are
//*  	Continental SAS
//*  	Airbus France SAS
//*  	Thales Alenia Space France SAS
//*  	ASTRIUM SAS
//*  	IB KRATES OU
//*  	Barco NV
//*  	Israel Aircraft Industries
//*  	Alyotech
//*  	Tallinn University of Technology
//*  	FERIA - Institut National Polytechnique de Toulouse
//*  	INRIA - Institut National de Recherche en Informatique et en Automatique

//HINTS
// local and rulebased rewriting rules together cause an exception when generating code

// Grammar for pasing EML expressions that can appear in Simulink block properites
// The grammar is derived from EMLBase with following modificaitons:
//  - parsing of statements and comments is removed

grammar EMLExpressions;

// ---------- COPIED from EMLBace.g, do not modify here -- make changes to EMLBase 
// ---------- and then copy the modified common part back here

// generation options
options {
	// the result is an Abstract syntax tree
   	output=AST;
   	// the java type of the result Tree 
   	ASTLabelType=CommonTree;
   	
//  backtrack=true;
//	filter=true;
}

tokens {
// ---- REMOVE		
	CompoundStatement;
	AssignStatement;
	IncStatement;
	DecStatement;
	ExpressionStatement;
// ---- END REMOVE	
	
	BinaryExpression;
	CallExpression;
	IntegerExpression;
	ListExpression;
	RealExpression;
	MemberExpression;
	ParenthesisExpression;
	TernaryExpression;
	UnaryExpression;
	VariableExpression;
	IndexExpressions;
	TypedExpression;

// ---- REMOVE		
	Annotations;
// ---- END REMOVE	
	DotIdent;
}

@header {
package geneauto.models.utilities.emlconverter.antlr;

/* move manually next to the class definition after geenrating the parser and
remove this commetnafter that */
@SuppressWarnings("unused")
}

@lexer::header {
package geneauto.models.utilities.emlconverter.antlr;

// Required for lexer::members netToken()
/* Comment out when debugging in AntLrWorks
*/
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventHandler.EventLevel;

/* move manually next to the class definition after geenrating the lexer and
remove this commetnafter that */
@SuppressWarnings("unused")
}

@members {
// Semantic predicate - indicates that the C-bitwise operations flag was set in the source language
public boolean cBitOptsOn = false;

protected void mismatch(IntStream input, int ttype, BitSet follow) throws RecognitionException {
	throw new MismatchedTokenException(ttype, input);
}
}

@lexer::members {
// Semantic predicate - indicates that the C-bitwise operations flag was set in the source language
// TODO: check if it makes any sense in Simulink
public boolean cBitOptsOn = false;

/* Comment to run AntLRWorks debugger
   Uncomment before release
*/
public Token nextToken() {
  while (true) {
   this.token = null;
   this.channel = Token.DEFAULT_CHANNEL;
   this.tokenStartCharIndex = input.index();
   this.tokenStartCharPositionInLine = input.getCharPositionInLine();
   this.tokenStartLine = input.getLine();
   this.text = null;
   if ( input.LA(1)==CharStream.EOF ) {
    return Token.EOF_TOKEN;
   }
   try {
    mTokens();
    if ( this.token==null ) {
     emit();
    }
    else if ( this.token==Token.SKIP_TOKEN ) {
     continue;
    }
    return this.token;
   }
   catch (RecognitionException re) {
    reportError(re);
    EventHandler.handle( EventLevel.ERROR, "", "", "Errors while parsing - unable to continue.", "");
   }
  }
 }
}

@rulecatch {
	catch (RecognitionException re) {
		reportError(re);
		throw re;
	}
}


// ---- REMOVED STATEMENT RULES FROM EMLBase.g 
// Basic SL PARSER RULES

nlComments
	:	NL!* comments NL!* 
	|	NL!+ 
	;
comments	:	comment (NL!* comment)* ;
comment		:	RCOMMENT | COMMENT ;

// Statement
stmt:	
	(	varExpr
			(	/* empty */ -> ^(ExpressionStatement varExpr)
			|	PLUS PLUS   -> ^(IncStatement varExpr)
			|	MINUS MINUS -> ^(DecStatement varExpr)
			|	ASSIGN expr -> ^(AssignStatement ASSIGN varExpr expr)
			) 
	)
	|	callExpr -> ^(ExpressionStatement callExpr)
	;
// ---- END REMOVE ----

// Expression
expr    	:	enumeration | lOrExpr^;
lOrExpr 	:	lAndExpr (LOR^ lAndExpr)* ;
lAndExpr 	:	orExpr (LAND^ orExpr)* ;
orExpr 		:	xorExpr (OR^ xorExpr)* ;
xorExpr		:	andExpr (XOR^ andExpr)* ;
andExpr 	:	eqExpr (AND^ eqExpr)* ;
eqExpr		:	compExpr (EQ_OP^ compExpr)?;
compExpr	:	shiftExpr (COMP_OP^ shiftExpr)?;
shiftExpr 	:	sumExpr (SHIFT^ sumExpr)* ;
sumExpr 	:	prodExpr ((PLUS|MINUS)^ prodExpr)* ;
prodExpr	:	prefExpr ((STAR|SLASH|MOD|POWER)^ prefExpr)* ;
prefExpr
	:	MINUS primExpr -> ^(UnaryExpression primExpr MINUS)
	|	PLUS primExpr -> ^(UnaryExpression primExpr PLUS)
	|	NOT primExpr -> ^(UnaryExpression primExpr NOT)
	|	TILDE primExpr -> ^(UnaryExpression primExpr TILDE)
	|	STAR primExpr -> ^(UnaryExpression primExpr STAR)
	|	AND primExpr -> ^(UnaryExpression primExpr AND)
	|	/*Empty*/ primExpr ;
typedExpr	:	SL_DATA_TYPE LPAREN expr RPAREN -> ^(TypedExpression SL_DATA_TYPE expr);
primExpr	:	varExpr | litExpr | intExpr | callExpr | parenExpr | typedExpr | array;

/* TODO: consider using indexed expression with simple brackets instead -- square brackets are 
notion of Stateflow
varExpr 	:	dotIdent indexExpr* -> ^(VariableExpression dotIdent indexExpr*) ;
indexExpr	:	LSQUB! expr RSQUB! ;
*/
varExpr 	:	dotIdent -> ^(VariableExpression dotIdent) ;
litExpr 	:	NUMBER -> ^(RealExpression NUMBER) ;
intExpr		:	INTEGER -> ^(IntegerExpression INTEGER) ;
parenExpr	:	LPAREN expr RPAREN -> ^(ParenthesisExpression expr) ;
callExpr 	:	dotIdent LPAREN exprList? RPAREN -> ^(CallExpression dotIdent exprList?) ;
exprList	:	expr (COMMA! expr)* ;

arrayElem	:	varExpr | litExpr | intExpr | parenExpr | typedExpr | array | arrayElemNeg;
arrayElemNeg	:	MINUS arrayElem -> ^(UnaryExpression arrayElem MINUS) ;
arrayLn		: 	(arrayElem (COMMA? arrayElem)*) -> ^(ListExpression arrayElem+) ;
array		:	LSQUB arrayLn (SCOLON arrayLn)* RSQUB -> ^(ListExpression arrayLn arrayLn*);
enumeration	:	LCURLB arrayLn RCURLB -> ^(ListExpression arrayLn);

// List of identifiers: "a.b.c, e.f"
dotIdents	:	dotIdent (COMMA! dotIdent)* ;
dotIdent	:	dotIdent_  -> ^(DotIdent dotIdent_) ;
dotIdent_	:	ident (DOT ident)* ;
// Identifier with comments
cmtIdent	:	nlComments? ident -> ^(ident ^(Annotations nlComments?)) ;
// ---- END REMOVE ----
ident	:	IDENT ;


//LEXER RULES


// ---- REMOVED TOKENS RELATED TO COMMENTS ----
COMMENT	: '/*'! .* '*/'! ;
// Right comment is everything but "\n" after "//"
//RCOMMENT: '//' (~'\\' | '\\' ~'n' )* ;
RCOMMENT: '//'! (~'\\')*;
// ---- END REMOVE ----


// Basic SL LEXER RULES
WS	:	(' '|'\t'|'...\\n') {skip();} ;
NL 	:	'\\n' ;

// Tokens
LPAREN	:	'(';
RPAREN	:	')';
LCURLB	:	'{';
RCURLB	:	'}';
LSQUB	:	'[';
RSQUB	:	']';
DOT 	:	'.';
COMMA	:	',';
COLON	:	':';
SCOLON	:	';';
PLUS	:	'+';
MINUS	:	'-';
STAR	:	'*';
SLASH	:	'/';
MOD 	:	'%%';
LAND 	:	'&&';
LOR  	:	'||';
AND 	:	'&';
OR  	:	'|';
POWER 	:	{!cBitOptsOn }?=> '^';
XOR 	:	{ cBitOptsOn }?=> '^';
SHIFT	:	('<<'|'>>');
EQ_OP	:	('=='|'!='|'~='|'<>');
COMP_OP	:	('>='|'<='|'>'|'<');
ASSIGN 	:	('='|'+='|'-='|'*='|'/='|'&='|'|='|'^=');
NOT  	:	'!';
TILDE	:	'~';
SL_DATA_TYPE	:	('double'|'single'|'int8'|'int16'|'int32'|'uint8'|'uint16'|'uint32'|'boolean');


// an identifier. 
IDENT	:	(LETTER|SYMBOL) (LETTER|SYMBOL|DIGIT)*;
// a number of the form 123, 0.12, 123e4, 123.45e-6
NUMBER	:	(INTEGER ( DOT | DOTNUMBER | EXPONENT )) 
		| DOTNUMBER ;
// a number of the form .12, .123e-4
DOTNUMBER	:	DOT INTEGER (EXPONENT)? ;
// a number of the form 1, 123, 0043
INTEGER	:	 (DIGIT+ | '0' ('x'|'X') (DIGIT | 'A'..'F' | 'a'..'f' )+) ;


// Fragments - do not become tokens 
fragment
EXPONENT  :	('e'|'E') (PLUS?|MINUS?) INTEGER;
fragment
LETTER	: 'a'..'z'|'A'..'Z';
fragment
DIGIT	: '0'..'9';
fragment
SYMBOL	:	'_'|'$' ;

//WS		:	(' '|'\t') {skip();};

/* Comment to run AntLRWorks debugger
   Uncomment before release
*/
ANY :  . {
EventHandler.handle( EventLevel.ERROR, "labelParser", "",
	"Unsupported character \'" + $text + "\' - skipped ", "" );
skip();
};
