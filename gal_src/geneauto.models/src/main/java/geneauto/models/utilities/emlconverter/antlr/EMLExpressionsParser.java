// $ANTLR 3.0.1 T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g 2010-03-10 16:59:08

package geneauto.models.utilities.emlconverter.antlr;



import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.BitSet;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.IntStream;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.MismatchedTokenException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.ParserRuleReturnScope;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.RewriteEarlyExitException;
import org.antlr.runtime.tree.RewriteRuleSubtreeStream;
import org.antlr.runtime.tree.RewriteRuleTokenStream;
import org.antlr.runtime.tree.TreeAdaptor;

@SuppressWarnings("unused")
public class EMLExpressionsParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "CompoundStatement", "AssignStatement", "IncStatement", "DecStatement", "ExpressionStatement", "BinaryExpression", "CallExpression", "IntegerExpression", "ListExpression", "RealExpression", "MemberExpression", "ParenthesisExpression", "TernaryExpression", "UnaryExpression", "VariableExpression", "IndexExpressions", "TypedExpression", "Annotations", "DotIdent", "NL", "RCOMMENT", "COMMENT", "PLUS", "MINUS", "ASSIGN", "LOR", "LAND", "OR", "XOR", "AND", "EQ_OP", "COMP_OP", "SHIFT", "STAR", "SLASH", "MOD", "POWER", "NOT", "TILDE", "SL_DATA_TYPE", "LPAREN", "RPAREN", "NUMBER", "INTEGER", "COMMA", "LSQUB", "SCOLON", "RSQUB", "LCURLB", "RCURLB", "DOT", "IDENT", "WS", "COLON", "LETTER", "SYMBOL", "DIGIT", "DOTNUMBER", "EXPONENT", "ANY"
    };
    public static final int DOTNUMBER=61;
    public static final int LOR=29;
    public static final int ExpressionStatement=8;
    public static final int EXPONENT=62;
    public static final int EQ_OP=34;
    public static final int COMP_OP=35;
    public static final int STAR=37;
    public static final int LETTER=58;
    public static final int MOD=39;
    public static final int SHIFT=36;
    public static final int AssignStatement=5;
    public static final int ParenthesisExpression=15;
    public static final int NOT=41;
    public static final int AND=33;
    public static final int EOF=-1;
    public static final int LPAREN=44;
    public static final int RCOMMENT=24;
    public static final int SL_DATA_TYPE=43;
    public static final int RPAREN=45;
    public static final int SLASH=38;
    public static final int COMMA=48;
    public static final int CompoundStatement=4;
    public static final int IndexExpressions=19;
    public static final int TILDE=42;
    public static final int IDENT=55;
    public static final int CallExpression=10;
    public static final int PLUS=26;
    public static final int DIGIT=60;
    public static final int NL=23;
    public static final int DOT=54;
    public static final int COMMENT=25;
    public static final int TypedExpression=20;
    public static final int UnaryExpression=17;
    public static final int SCOLON=50;
    public static final int INTEGER=47;
    public static final int TernaryExpression=16;
    public static final int XOR=32;
    public static final int SYMBOL=59;
    public static final int RealExpression=13;
    public static final int NUMBER=46;
    public static final int POWER=40;
    public static final int LSQUB=49;
    public static final int DecStatement=7;
    public static final int MINUS=27;
    public static final int IntegerExpression=11;
    public static final int RSQUB=51;
    public static final int VariableExpression=18;
    public static final int Annotations=21;
    public static final int COLON=57;
    public static final int BinaryExpression=9;
    public static final int ANY=63;
    public static final int WS=56;
    public static final int OR=31;
    public static final int DotIdent=22;
    public static final int ASSIGN=28;
    public static final int RCURLB=53;
    public static final int ListExpression=12;
    public static final int LCURLB=52;
    public static final int MemberExpression=14;
    public static final int LAND=30;
    public static final int IncStatement=6;

        public EMLExpressionsParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g"; }

    
    // Semantic predicate - indicates that the C-bitwise operations flag was set in the source language
    public boolean cBitOptsOn = false;
    
    protected void mismatch(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    	throw new MismatchedTokenException(ttype, input);
    }


    public static class nlComments_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start nlComments
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:167:1: nlComments : ( ( NL )* comments ( NL )* | ( NL )+ );
    public final nlComments_return nlComments() throws RecognitionException {
        nlComments_return retval = new nlComments_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token NL1=null;
        Token NL3=null;
        Token NL4=null;
        comments_return comments2 = null;


        CommonTree NL1_tree=null;
        CommonTree NL3_tree=null;
        CommonTree NL4_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:168:2: ( ( NL )* comments ( NL )* | ( NL )+ )
            int alt4=2;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:168:4: ( NL )* comments ( NL )*
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:168:6: ( NL )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==NL) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:168:6: NL
                    	    {
                    	    NL1=(Token)input.LT(1);
                    	    match(input,NL,FOLLOW_NL_in_nlComments227); 

                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comments_in_nlComments231);
                    comments2=comments();
                    _fsp--;

                    adaptor.addChild(root_0, comments2.getTree());
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:168:20: ( NL )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==NL) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:168:20: NL
                    	    {
                    	    NL3=(Token)input.LT(1);
                    	    match(input,NL,FOLLOW_NL_in_nlComments233); 

                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:169:4: ( NL )+
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:169:6: ( NL )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==NL) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:169:6: NL
                    	    {
                    	    NL4=(Token)input.LT(1);
                    	    match(input,NL,FOLLOW_NL_in_nlComments241); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end nlComments

    public static class comments_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start comments
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:171:1: comments : comment ( ( NL )* comment )* ;
    public final comments_return comments() throws RecognitionException {
        comments_return retval = new comments_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token NL6=null;
        comment_return comment5 = null;

        comment_return comment7 = null;


        CommonTree NL6_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:171:10: ( comment ( ( NL )* comment )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:171:12: comment ( ( NL )* comment )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_comment_in_comments253);
            comment5=comment();
            _fsp--;

            adaptor.addChild(root_0, comment5.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:171:20: ( ( NL )* comment )*
            loop6:
            do {
                int alt6=2;
                alt6 = dfa6.predict(input);
                switch (alt6) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:171:21: ( NL )* comment
            	    {
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:171:23: ( NL )*
            	    loop5:
            	    do {
            	        int alt5=2;
            	        int LA5_0 = input.LA(1);

            	        if ( (LA5_0==NL) ) {
            	            alt5=1;
            	        }


            	        switch (alt5) {
            	    	case 1 :
            	    	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:171:23: NL
            	    	    {
            	    	    NL6=(Token)input.LT(1);
            	    	    match(input,NL,FOLLOW_NL_in_comments256); 

            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop5;
            	        }
            	    } while (true);

            	    pushFollow(FOLLOW_comment_in_comments260);
            	    comment7=comment();
            	    _fsp--;

            	    adaptor.addChild(root_0, comment7.getTree());

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end comments

    public static class comment_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start comment
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:172:1: comment : ( RCOMMENT | COMMENT );
    public final comment_return comment() throws RecognitionException {
        comment_return retval = new comment_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set8=null;

        CommonTree set8_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:172:10: ( RCOMMENT | COMMENT )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:
            {
            root_0 = (CommonTree)adaptor.nil();

            set8=(Token)input.LT(1);
            if ( (input.LA(1)>=RCOMMENT && input.LA(1)<=COMMENT) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set8));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_comment0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end comment

    public static class stmt_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start stmt
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:175:1: stmt : ( ( varExpr ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) ) ) | callExpr -> ^( ExpressionStatement callExpr ) );
    public final stmt_return stmt() throws RecognitionException {
        stmt_return retval = new stmt_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token PLUS10=null;
        Token PLUS11=null;
        Token MINUS12=null;
        Token MINUS13=null;
        Token ASSIGN14=null;
        varExpr_return varExpr9 = null;

        expr_return expr15 = null;

        callExpr_return callExpr16 = null;


        CommonTree PLUS10_tree=null;
        CommonTree PLUS11_tree=null;
        CommonTree MINUS12_tree=null;
        CommonTree MINUS13_tree=null;
        CommonTree ASSIGN14_tree=null;
        RewriteRuleTokenStream stream_PLUS=new RewriteRuleTokenStream(adaptor,"token PLUS");
        RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
        RewriteRuleTokenStream stream_ASSIGN=new RewriteRuleTokenStream(adaptor,"token ASSIGN");
        RewriteRuleSubtreeStream stream_varExpr=new RewriteRuleSubtreeStream(adaptor,"rule varExpr");
        RewriteRuleSubtreeStream stream_callExpr=new RewriteRuleSubtreeStream(adaptor,"rule callExpr");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:175:5: ( ( varExpr ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) ) ) | callExpr -> ^( ExpressionStatement callExpr ) )
            int alt8=2;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:176:2: ( varExpr ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) ) )
                    {
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:176:2: ( varExpr ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) ) )
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:176:4: varExpr ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) )
                    {
                    pushFollow(FOLLOW_varExpr_in_stmt288);
                    varExpr9=varExpr();
                    _fsp--;

                    stream_varExpr.add(varExpr9.getTree());
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:177:4: ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) )
                    int alt7=4;
                    switch ( input.LA(1) ) {
                    case EOF:
                        {
                        alt7=1;
                        }
                        break;
                    case PLUS:
                        {
                        alt7=2;
                        }
                        break;
                    case MINUS:
                        {
                        alt7=3;
                        }
                        break;
                    case ASSIGN:
                        {
                        alt7=4;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("177:4: ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) )", 7, 0, input);

                        throw nvae;
                    }

                    switch (alt7) {
                        case 1 :
                            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:177:18: 
                            {

                            // AST REWRITE
                            // elements: varExpr
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                            root_0 = (CommonTree)adaptor.nil();
                            // 177:18: -> ^( ExpressionStatement varExpr )
                            {
                                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:177:21: ^( ExpressionStatement varExpr )
                                {
                                CommonTree root_1 = (CommonTree)adaptor.nil();
                                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(ExpressionStatement, "ExpressionStatement"), root_1);

                                adaptor.addChild(root_1, stream_varExpr.next());

                                adaptor.addChild(root_0, root_1);
                                }

                            }



                            }
                            break;
                        case 2 :
                            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:178:6: PLUS PLUS
                            {
                            PLUS10=(Token)input.LT(1);
                            match(input,PLUS,FOLLOW_PLUS_in_stmt310); 
                            stream_PLUS.add(PLUS10);

                            PLUS11=(Token)input.LT(1);
                            match(input,PLUS,FOLLOW_PLUS_in_stmt312); 
                            stream_PLUS.add(PLUS11);


                            // AST REWRITE
                            // elements: varExpr
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                            root_0 = (CommonTree)adaptor.nil();
                            // 178:18: -> ^( IncStatement varExpr )
                            {
                                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:178:21: ^( IncStatement varExpr )
                                {
                                CommonTree root_1 = (CommonTree)adaptor.nil();
                                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(IncStatement, "IncStatement"), root_1);

                                adaptor.addChild(root_1, stream_varExpr.next());

                                adaptor.addChild(root_0, root_1);
                                }

                            }



                            }
                            break;
                        case 3 :
                            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:179:6: MINUS MINUS
                            {
                            MINUS12=(Token)input.LT(1);
                            match(input,MINUS,FOLLOW_MINUS_in_stmt329); 
                            stream_MINUS.add(MINUS12);

                            MINUS13=(Token)input.LT(1);
                            match(input,MINUS,FOLLOW_MINUS_in_stmt331); 
                            stream_MINUS.add(MINUS13);


                            // AST REWRITE
                            // elements: varExpr
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                            root_0 = (CommonTree)adaptor.nil();
                            // 179:18: -> ^( DecStatement varExpr )
                            {
                                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:179:21: ^( DecStatement varExpr )
                                {
                                CommonTree root_1 = (CommonTree)adaptor.nil();
                                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(DecStatement, "DecStatement"), root_1);

                                adaptor.addChild(root_1, stream_varExpr.next());

                                adaptor.addChild(root_0, root_1);
                                }

                            }



                            }
                            break;
                        case 4 :
                            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:180:6: ASSIGN expr
                            {
                            ASSIGN14=(Token)input.LT(1);
                            match(input,ASSIGN,FOLLOW_ASSIGN_in_stmt346); 
                            stream_ASSIGN.add(ASSIGN14);

                            pushFollow(FOLLOW_expr_in_stmt348);
                            expr15=expr();
                            _fsp--;

                            stream_expr.add(expr15.getTree());

                            // AST REWRITE
                            // elements: expr, varExpr, ASSIGN
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                            root_0 = (CommonTree)adaptor.nil();
                            // 180:18: -> ^( AssignStatement ASSIGN varExpr expr )
                            {
                                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:180:21: ^( AssignStatement ASSIGN varExpr expr )
                                {
                                CommonTree root_1 = (CommonTree)adaptor.nil();
                                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(AssignStatement, "AssignStatement"), root_1);

                                adaptor.addChild(root_1, stream_ASSIGN.next());
                                adaptor.addChild(root_1, stream_varExpr.next());
                                adaptor.addChild(root_1, stream_expr.next());

                                adaptor.addChild(root_0, root_1);
                                }

                            }



                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:183:4: callExpr
                    {
                    pushFollow(FOLLOW_callExpr_in_stmt374);
                    callExpr16=callExpr();
                    _fsp--;

                    stream_callExpr.add(callExpr16.getTree());

                    // AST REWRITE
                    // elements: callExpr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 183:13: -> ^( ExpressionStatement callExpr )
                    {
                        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:183:16: ^( ExpressionStatement callExpr )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(ExpressionStatement, "ExpressionStatement"), root_1);

                        adaptor.addChild(root_1, stream_callExpr.next());

                        adaptor.addChild(root_0, root_1);
                        }

                    }



                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end stmt

    public static class expr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start expr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:188:1: expr : ( enumeration | lOrExpr );
    public final expr_return expr() throws RecognitionException {
        expr_return retval = new expr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        enumeration_return enumeration17 = null;

        lOrExpr_return lOrExpr18 = null;



        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:188:10: ( enumeration | lOrExpr )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==LCURLB) ) {
                alt9=1;
            }
            else if ( ((LA9_0>=PLUS && LA9_0<=MINUS)||LA9_0==AND||LA9_0==STAR||(LA9_0>=NOT && LA9_0<=LPAREN)||(LA9_0>=NUMBER && LA9_0<=INTEGER)||LA9_0==LSQUB||LA9_0==IDENT) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("188:1: expr : ( enumeration | lOrExpr );", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:188:12: enumeration
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_enumeration_in_expr398);
                    enumeration17=enumeration();
                    _fsp--;

                    adaptor.addChild(root_0, enumeration17.getTree());

                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:188:26: lOrExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_lOrExpr_in_expr402);
                    lOrExpr18=lOrExpr();
                    _fsp--;

                    root_0 = (CommonTree)adaptor.becomeRoot(lOrExpr18.getTree(), root_0);

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end expr

    public static class lOrExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start lOrExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:189:1: lOrExpr : lAndExpr ( LOR lAndExpr )* ;
    public final lOrExpr_return lOrExpr() throws RecognitionException {
        lOrExpr_return retval = new lOrExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LOR20=null;
        lAndExpr_return lAndExpr19 = null;

        lAndExpr_return lAndExpr21 = null;


        CommonTree LOR20_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:189:10: ( lAndExpr ( LOR lAndExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:189:12: lAndExpr ( LOR lAndExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_lAndExpr_in_lOrExpr411);
            lAndExpr19=lAndExpr();
            _fsp--;

            adaptor.addChild(root_0, lAndExpr19.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:189:21: ( LOR lAndExpr )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==LOR) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:189:22: LOR lAndExpr
            	    {
            	    LOR20=(Token)input.LT(1);
            	    match(input,LOR,FOLLOW_LOR_in_lOrExpr414); 
            	    LOR20_tree = (CommonTree)adaptor.create(LOR20);
            	    root_0 = (CommonTree)adaptor.becomeRoot(LOR20_tree, root_0);

            	    pushFollow(FOLLOW_lAndExpr_in_lOrExpr417);
            	    lAndExpr21=lAndExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, lAndExpr21.getTree());

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end lOrExpr

    public static class lAndExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start lAndExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:190:1: lAndExpr : orExpr ( LAND orExpr )* ;
    public final lAndExpr_return lAndExpr() throws RecognitionException {
        lAndExpr_return retval = new lAndExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LAND23=null;
        orExpr_return orExpr22 = null;

        orExpr_return orExpr24 = null;


        CommonTree LAND23_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:190:11: ( orExpr ( LAND orExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:190:13: orExpr ( LAND orExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_orExpr_in_lAndExpr428);
            orExpr22=orExpr();
            _fsp--;

            adaptor.addChild(root_0, orExpr22.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:190:20: ( LAND orExpr )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==LAND) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:190:21: LAND orExpr
            	    {
            	    LAND23=(Token)input.LT(1);
            	    match(input,LAND,FOLLOW_LAND_in_lAndExpr431); 
            	    LAND23_tree = (CommonTree)adaptor.create(LAND23);
            	    root_0 = (CommonTree)adaptor.becomeRoot(LAND23_tree, root_0);

            	    pushFollow(FOLLOW_orExpr_in_lAndExpr434);
            	    orExpr24=orExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, orExpr24.getTree());

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end lAndExpr

    public static class orExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start orExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:191:1: orExpr : xorExpr ( OR xorExpr )* ;
    public final orExpr_return orExpr() throws RecognitionException {
        orExpr_return retval = new orExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token OR26=null;
        xorExpr_return xorExpr25 = null;

        xorExpr_return xorExpr27 = null;


        CommonTree OR26_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:191:10: ( xorExpr ( OR xorExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:191:12: xorExpr ( OR xorExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_xorExpr_in_orExpr446);
            xorExpr25=xorExpr();
            _fsp--;

            adaptor.addChild(root_0, xorExpr25.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:191:20: ( OR xorExpr )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==OR) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:191:21: OR xorExpr
            	    {
            	    OR26=(Token)input.LT(1);
            	    match(input,OR,FOLLOW_OR_in_orExpr449); 
            	    OR26_tree = (CommonTree)adaptor.create(OR26);
            	    root_0 = (CommonTree)adaptor.becomeRoot(OR26_tree, root_0);

            	    pushFollow(FOLLOW_xorExpr_in_orExpr452);
            	    xorExpr27=xorExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, xorExpr27.getTree());

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end orExpr

    public static class xorExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start xorExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:192:1: xorExpr : andExpr ( XOR andExpr )* ;
    public final xorExpr_return xorExpr() throws RecognitionException {
        xorExpr_return retval = new xorExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token XOR29=null;
        andExpr_return andExpr28 = null;

        andExpr_return andExpr30 = null;


        CommonTree XOR29_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:192:10: ( andExpr ( XOR andExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:192:12: andExpr ( XOR andExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_andExpr_in_xorExpr463);
            andExpr28=andExpr();
            _fsp--;

            adaptor.addChild(root_0, andExpr28.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:192:20: ( XOR andExpr )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==XOR) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:192:21: XOR andExpr
            	    {
            	    XOR29=(Token)input.LT(1);
            	    match(input,XOR,FOLLOW_XOR_in_xorExpr466); 
            	    XOR29_tree = (CommonTree)adaptor.create(XOR29);
            	    root_0 = (CommonTree)adaptor.becomeRoot(XOR29_tree, root_0);

            	    pushFollow(FOLLOW_andExpr_in_xorExpr469);
            	    andExpr30=andExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, andExpr30.getTree());

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end xorExpr

    public static class andExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start andExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:193:1: andExpr : eqExpr ( AND eqExpr )* ;
    public final andExpr_return andExpr() throws RecognitionException {
        andExpr_return retval = new andExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token AND32=null;
        eqExpr_return eqExpr31 = null;

        eqExpr_return eqExpr33 = null;


        CommonTree AND32_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:193:10: ( eqExpr ( AND eqExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:193:12: eqExpr ( AND eqExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_eqExpr_in_andExpr480);
            eqExpr31=eqExpr();
            _fsp--;

            adaptor.addChild(root_0, eqExpr31.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:193:19: ( AND eqExpr )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==AND) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:193:20: AND eqExpr
            	    {
            	    AND32=(Token)input.LT(1);
            	    match(input,AND,FOLLOW_AND_in_andExpr483); 
            	    AND32_tree = (CommonTree)adaptor.create(AND32);
            	    root_0 = (CommonTree)adaptor.becomeRoot(AND32_tree, root_0);

            	    pushFollow(FOLLOW_eqExpr_in_andExpr486);
            	    eqExpr33=eqExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, eqExpr33.getTree());

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end andExpr

    public static class eqExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start eqExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:194:1: eqExpr : compExpr ( EQ_OP compExpr )? ;
    public final eqExpr_return eqExpr() throws RecognitionException {
        eqExpr_return retval = new eqExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token EQ_OP35=null;
        compExpr_return compExpr34 = null;

        compExpr_return compExpr36 = null;


        CommonTree EQ_OP35_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:194:9: ( compExpr ( EQ_OP compExpr )? )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:194:11: compExpr ( EQ_OP compExpr )?
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_compExpr_in_eqExpr497);
            compExpr34=compExpr();
            _fsp--;

            adaptor.addChild(root_0, compExpr34.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:194:20: ( EQ_OP compExpr )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==EQ_OP) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:194:21: EQ_OP compExpr
                    {
                    EQ_OP35=(Token)input.LT(1);
                    match(input,EQ_OP,FOLLOW_EQ_OP_in_eqExpr500); 
                    EQ_OP35_tree = (CommonTree)adaptor.create(EQ_OP35);
                    root_0 = (CommonTree)adaptor.becomeRoot(EQ_OP35_tree, root_0);

                    pushFollow(FOLLOW_compExpr_in_eqExpr503);
                    compExpr36=compExpr();
                    _fsp--;

                    adaptor.addChild(root_0, compExpr36.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end eqExpr

    public static class compExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start compExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:195:1: compExpr : shiftExpr ( COMP_OP shiftExpr )? ;
    public final compExpr_return compExpr() throws RecognitionException {
        compExpr_return retval = new compExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token COMP_OP38=null;
        shiftExpr_return shiftExpr37 = null;

        shiftExpr_return shiftExpr39 = null;


        CommonTree COMP_OP38_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:195:10: ( shiftExpr ( COMP_OP shiftExpr )? )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:195:12: shiftExpr ( COMP_OP shiftExpr )?
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_shiftExpr_in_compExpr512);
            shiftExpr37=shiftExpr();
            _fsp--;

            adaptor.addChild(root_0, shiftExpr37.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:195:22: ( COMP_OP shiftExpr )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==COMP_OP) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:195:23: COMP_OP shiftExpr
                    {
                    COMP_OP38=(Token)input.LT(1);
                    match(input,COMP_OP,FOLLOW_COMP_OP_in_compExpr515); 
                    COMP_OP38_tree = (CommonTree)adaptor.create(COMP_OP38);
                    root_0 = (CommonTree)adaptor.becomeRoot(COMP_OP38_tree, root_0);

                    pushFollow(FOLLOW_shiftExpr_in_compExpr518);
                    shiftExpr39=shiftExpr();
                    _fsp--;

                    adaptor.addChild(root_0, shiftExpr39.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end compExpr

    public static class shiftExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start shiftExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:196:1: shiftExpr : sumExpr ( SHIFT sumExpr )* ;
    public final shiftExpr_return shiftExpr() throws RecognitionException {
        shiftExpr_return retval = new shiftExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token SHIFT41=null;
        sumExpr_return sumExpr40 = null;

        sumExpr_return sumExpr42 = null;


        CommonTree SHIFT41_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:196:12: ( sumExpr ( SHIFT sumExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:196:14: sumExpr ( SHIFT sumExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_sumExpr_in_shiftExpr528);
            sumExpr40=sumExpr();
            _fsp--;

            adaptor.addChild(root_0, sumExpr40.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:196:22: ( SHIFT sumExpr )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==SHIFT) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:196:23: SHIFT sumExpr
            	    {
            	    SHIFT41=(Token)input.LT(1);
            	    match(input,SHIFT,FOLLOW_SHIFT_in_shiftExpr531); 
            	    SHIFT41_tree = (CommonTree)adaptor.create(SHIFT41);
            	    root_0 = (CommonTree)adaptor.becomeRoot(SHIFT41_tree, root_0);

            	    pushFollow(FOLLOW_sumExpr_in_shiftExpr534);
            	    sumExpr42=sumExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, sumExpr42.getTree());

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end shiftExpr

    public static class sumExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start sumExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:197:1: sumExpr : prodExpr ( ( PLUS | MINUS ) prodExpr )* ;
    public final sumExpr_return sumExpr() throws RecognitionException {
        sumExpr_return retval = new sumExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set44=null;
        prodExpr_return prodExpr43 = null;

        prodExpr_return prodExpr45 = null;


        CommonTree set44_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:197:10: ( prodExpr ( ( PLUS | MINUS ) prodExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:197:12: prodExpr ( ( PLUS | MINUS ) prodExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_prodExpr_in_sumExpr545);
            prodExpr43=prodExpr();
            _fsp--;

            adaptor.addChild(root_0, prodExpr43.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:197:21: ( ( PLUS | MINUS ) prodExpr )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=PLUS && LA18_0<=MINUS)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:197:22: ( PLUS | MINUS ) prodExpr
            	    {
            	    set44=(Token)input.LT(1);
            	    if ( (input.LA(1)>=PLUS && input.LA(1)<=MINUS) ) {
            	        input.consume();
            	        root_0 = (CommonTree)adaptor.becomeRoot(adaptor.create(set44), root_0);
            	        errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recoverFromMismatchedSet(input,mse,FOLLOW_set_in_sumExpr548);    throw mse;
            	    }

            	    pushFollow(FOLLOW_prodExpr_in_sumExpr555);
            	    prodExpr45=prodExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, prodExpr45.getTree());

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end sumExpr

    public static class prodExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prodExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:198:1: prodExpr : prefExpr ( ( STAR | SLASH | MOD | POWER ) prefExpr )* ;
    public final prodExpr_return prodExpr() throws RecognitionException {
        prodExpr_return retval = new prodExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set47=null;
        prefExpr_return prefExpr46 = null;

        prefExpr_return prefExpr48 = null;


        CommonTree set47_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:198:10: ( prefExpr ( ( STAR | SLASH | MOD | POWER ) prefExpr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:198:12: prefExpr ( ( STAR | SLASH | MOD | POWER ) prefExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_prefExpr_in_prodExpr565);
            prefExpr46=prefExpr();
            _fsp--;

            adaptor.addChild(root_0, prefExpr46.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:198:21: ( ( STAR | SLASH | MOD | POWER ) prefExpr )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=STAR && LA19_0<=POWER)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:198:22: ( STAR | SLASH | MOD | POWER ) prefExpr
            	    {
            	    set47=(Token)input.LT(1);
            	    if ( (input.LA(1)>=STAR && input.LA(1)<=POWER) ) {
            	        input.consume();
            	        root_0 = (CommonTree)adaptor.becomeRoot(adaptor.create(set47), root_0);
            	        errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recoverFromMismatchedSet(input,mse,FOLLOW_set_in_prodExpr568);    throw mse;
            	    }

            	    pushFollow(FOLLOW_prefExpr_in_prodExpr579);
            	    prefExpr48=prefExpr();
            	    _fsp--;

            	    adaptor.addChild(root_0, prefExpr48.getTree());

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end prodExpr

    public static class prefExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prefExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:199:1: prefExpr : ( MINUS primExpr -> ^( UnaryExpression primExpr MINUS ) | PLUS primExpr -> ^( UnaryExpression primExpr PLUS ) | NOT primExpr -> ^( UnaryExpression primExpr NOT ) | TILDE primExpr -> ^( UnaryExpression primExpr TILDE ) | STAR primExpr -> ^( UnaryExpression primExpr STAR ) | AND primExpr -> ^( UnaryExpression primExpr AND ) | primExpr );
    public final prefExpr_return prefExpr() throws RecognitionException {
        prefExpr_return retval = new prefExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token MINUS49=null;
        Token PLUS51=null;
        Token NOT53=null;
        Token TILDE55=null;
        Token STAR57=null;
        Token AND59=null;
        primExpr_return primExpr50 = null;

        primExpr_return primExpr52 = null;

        primExpr_return primExpr54 = null;

        primExpr_return primExpr56 = null;

        primExpr_return primExpr58 = null;

        primExpr_return primExpr60 = null;

        primExpr_return primExpr61 = null;


        CommonTree MINUS49_tree=null;
        CommonTree PLUS51_tree=null;
        CommonTree NOT53_tree=null;
        CommonTree TILDE55_tree=null;
        CommonTree STAR57_tree=null;
        CommonTree AND59_tree=null;
        RewriteRuleTokenStream stream_PLUS=new RewriteRuleTokenStream(adaptor,"token PLUS");
        RewriteRuleTokenStream stream_STAR=new RewriteRuleTokenStream(adaptor,"token STAR");
        RewriteRuleTokenStream stream_NOT=new RewriteRuleTokenStream(adaptor,"token NOT");
        RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
        RewriteRuleTokenStream stream_AND=new RewriteRuleTokenStream(adaptor,"token AND");
        RewriteRuleTokenStream stream_TILDE=new RewriteRuleTokenStream(adaptor,"token TILDE");
        RewriteRuleSubtreeStream stream_primExpr=new RewriteRuleSubtreeStream(adaptor,"rule primExpr");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:200:2: ( MINUS primExpr -> ^( UnaryExpression primExpr MINUS ) | PLUS primExpr -> ^( UnaryExpression primExpr PLUS ) | NOT primExpr -> ^( UnaryExpression primExpr NOT ) | TILDE primExpr -> ^( UnaryExpression primExpr TILDE ) | STAR primExpr -> ^( UnaryExpression primExpr STAR ) | AND primExpr -> ^( UnaryExpression primExpr AND ) | primExpr )
            int alt20=7;
            switch ( input.LA(1) ) {
            case MINUS:
                {
                alt20=1;
                }
                break;
            case PLUS:
                {
                alt20=2;
                }
                break;
            case NOT:
                {
                alt20=3;
                }
                break;
            case TILDE:
                {
                alt20=4;
                }
                break;
            case STAR:
                {
                alt20=5;
                }
                break;
            case AND:
                {
                alt20=6;
                }
                break;
            case SL_DATA_TYPE:
            case LPAREN:
            case NUMBER:
            case INTEGER:
            case LSQUB:
            case IDENT:
                {
                alt20=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("199:1: prefExpr : ( MINUS primExpr -> ^( UnaryExpression primExpr MINUS ) | PLUS primExpr -> ^( UnaryExpression primExpr PLUS ) | NOT primExpr -> ^( UnaryExpression primExpr NOT ) | TILDE primExpr -> ^( UnaryExpression primExpr TILDE ) | STAR primExpr -> ^( UnaryExpression primExpr STAR ) | AND primExpr -> ^( UnaryExpression primExpr AND ) | primExpr );", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:200:4: MINUS primExpr
                    {
                    MINUS49=(Token)input.LT(1);
                    match(input,MINUS,FOLLOW_MINUS_in_prefExpr590); 
                    stream_MINUS.add(MINUS49);

                    pushFollow(FOLLOW_primExpr_in_prefExpr592);
                    primExpr50=primExpr();
                    _fsp--;

                    stream_primExpr.add(primExpr50.getTree());

                    // AST REWRITE
                    // elements: primExpr, MINUS
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 200:19: -> ^( UnaryExpression primExpr MINUS )
                    {
                        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:200:22: ^( UnaryExpression primExpr MINUS )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(UnaryExpression, "UnaryExpression"), root_1);

                        adaptor.addChild(root_1, stream_primExpr.next());
                        adaptor.addChild(root_1, stream_MINUS.next());

                        adaptor.addChild(root_0, root_1);
                        }

                    }



                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:201:4: PLUS primExpr
                    {
                    PLUS51=(Token)input.LT(1);
                    match(input,PLUS,FOLLOW_PLUS_in_prefExpr607); 
                    stream_PLUS.add(PLUS51);

                    pushFollow(FOLLOW_primExpr_in_prefExpr609);
                    primExpr52=primExpr();
                    _fsp--;

                    stream_primExpr.add(primExpr52.getTree());

                    // AST REWRITE
                    // elements: primExpr, PLUS
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 201:18: -> ^( UnaryExpression primExpr PLUS )
                    {
                        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:201:21: ^( UnaryExpression primExpr PLUS )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(UnaryExpression, "UnaryExpression"), root_1);

                        adaptor.addChild(root_1, stream_primExpr.next());
                        adaptor.addChild(root_1, stream_PLUS.next());

                        adaptor.addChild(root_0, root_1);
                        }

                    }



                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:202:4: NOT primExpr
                    {
                    NOT53=(Token)input.LT(1);
                    match(input,NOT,FOLLOW_NOT_in_prefExpr624); 
                    stream_NOT.add(NOT53);

                    pushFollow(FOLLOW_primExpr_in_prefExpr626);
                    primExpr54=primExpr();
                    _fsp--;

                    stream_primExpr.add(primExpr54.getTree());

                    // AST REWRITE
                    // elements: primExpr, NOT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 202:17: -> ^( UnaryExpression primExpr NOT )
                    {
                        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:202:20: ^( UnaryExpression primExpr NOT )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(UnaryExpression, "UnaryExpression"), root_1);

                        adaptor.addChild(root_1, stream_primExpr.next());
                        adaptor.addChild(root_1, stream_NOT.next());

                        adaptor.addChild(root_0, root_1);
                        }

                    }



                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:203:4: TILDE primExpr
                    {
                    TILDE55=(Token)input.LT(1);
                    match(input,TILDE,FOLLOW_TILDE_in_prefExpr641); 
                    stream_TILDE.add(TILDE55);

                    pushFollow(FOLLOW_primExpr_in_prefExpr643);
                    primExpr56=primExpr();
                    _fsp--;

                    stream_primExpr.add(primExpr56.getTree());

                    // AST REWRITE
                    // elements: TILDE, primExpr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 203:19: -> ^( UnaryExpression primExpr TILDE )
                    {
                        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:203:22: ^( UnaryExpression primExpr TILDE )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(UnaryExpression, "UnaryExpression"), root_1);

                        adaptor.addChild(root_1, stream_primExpr.next());
                        adaptor.addChild(root_1, stream_TILDE.next());

                        adaptor.addChild(root_0, root_1);
                        }

                    }



                    }
                    break;
                case 5 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:204:4: STAR primExpr
                    {
                    STAR57=(Token)input.LT(1);
                    match(input,STAR,FOLLOW_STAR_in_prefExpr658); 
                    stream_STAR.add(STAR57);

                    pushFollow(FOLLOW_primExpr_in_prefExpr660);
                    primExpr58=primExpr();
                    _fsp--;

                    stream_primExpr.add(primExpr58.getTree());

                    // AST REWRITE
                    // elements: STAR, primExpr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 204:18: -> ^( UnaryExpression primExpr STAR )
                    {
                        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:204:21: ^( UnaryExpression primExpr STAR )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(UnaryExpression, "UnaryExpression"), root_1);

                        adaptor.addChild(root_1, stream_primExpr.next());
                        adaptor.addChild(root_1, stream_STAR.next());

                        adaptor.addChild(root_0, root_1);
                        }

                    }



                    }
                    break;
                case 6 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:205:4: AND primExpr
                    {
                    AND59=(Token)input.LT(1);
                    match(input,AND,FOLLOW_AND_in_prefExpr675); 
                    stream_AND.add(AND59);

                    pushFollow(FOLLOW_primExpr_in_prefExpr677);
                    primExpr60=primExpr();
                    _fsp--;

                    stream_primExpr.add(primExpr60.getTree());

                    // AST REWRITE
                    // elements: AND, primExpr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 205:17: -> ^( UnaryExpression primExpr AND )
                    {
                        // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:205:20: ^( UnaryExpression primExpr AND )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(UnaryExpression, "UnaryExpression"), root_1);

                        adaptor.addChild(root_1, stream_primExpr.next());
                        adaptor.addChild(root_1, stream_AND.next());

                        adaptor.addChild(root_0, root_1);
                        }

                    }



                    }
                    break;
                case 7 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:206:14: primExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_primExpr_in_prefExpr694);
                    primExpr61=primExpr();
                    _fsp--;

                    adaptor.addChild(root_0, primExpr61.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end prefExpr

    public static class typedExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start typedExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:207:1: typedExpr : SL_DATA_TYPE LPAREN expr RPAREN -> ^( TypedExpression SL_DATA_TYPE expr ) ;
    public final typedExpr_return typedExpr() throws RecognitionException {
        typedExpr_return retval = new typedExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token SL_DATA_TYPE62=null;
        Token LPAREN63=null;
        Token RPAREN65=null;
        expr_return expr64 = null;


        CommonTree SL_DATA_TYPE62_tree=null;
        CommonTree LPAREN63_tree=null;
        CommonTree RPAREN65_tree=null;
        RewriteRuleTokenStream stream_SL_DATA_TYPE=new RewriteRuleTokenStream(adaptor,"token SL_DATA_TYPE");
        RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
        RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:207:11: ( SL_DATA_TYPE LPAREN expr RPAREN -> ^( TypedExpression SL_DATA_TYPE expr ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:207:13: SL_DATA_TYPE LPAREN expr RPAREN
            {
            SL_DATA_TYPE62=(Token)input.LT(1);
            match(input,SL_DATA_TYPE,FOLLOW_SL_DATA_TYPE_in_typedExpr702); 
            stream_SL_DATA_TYPE.add(SL_DATA_TYPE62);

            LPAREN63=(Token)input.LT(1);
            match(input,LPAREN,FOLLOW_LPAREN_in_typedExpr704); 
            stream_LPAREN.add(LPAREN63);

            pushFollow(FOLLOW_expr_in_typedExpr706);
            expr64=expr();
            _fsp--;

            stream_expr.add(expr64.getTree());
            RPAREN65=(Token)input.LT(1);
            match(input,RPAREN,FOLLOW_RPAREN_in_typedExpr708); 
            stream_RPAREN.add(RPAREN65);


            // AST REWRITE
            // elements: expr, SL_DATA_TYPE
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 207:45: -> ^( TypedExpression SL_DATA_TYPE expr )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:207:48: ^( TypedExpression SL_DATA_TYPE expr )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(TypedExpression, "TypedExpression"), root_1);

                adaptor.addChild(root_1, stream_SL_DATA_TYPE.next());
                adaptor.addChild(root_1, stream_expr.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end typedExpr

    public static class primExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start primExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:1: primExpr : ( varExpr | litExpr | intExpr | callExpr | parenExpr | typedExpr | array );
    public final primExpr_return primExpr() throws RecognitionException {
        primExpr_return retval = new primExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        varExpr_return varExpr66 = null;

        litExpr_return litExpr67 = null;

        intExpr_return intExpr68 = null;

        callExpr_return callExpr69 = null;

        parenExpr_return parenExpr70 = null;

        typedExpr_return typedExpr71 = null;

        array_return array72 = null;



        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:10: ( varExpr | litExpr | intExpr | callExpr | parenExpr | typedExpr | array )
            int alt21=7;
            alt21 = dfa21.predict(input);
            switch (alt21) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:12: varExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_varExpr_in_primExpr725);
                    varExpr66=varExpr();
                    _fsp--;

                    adaptor.addChild(root_0, varExpr66.getTree());

                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:22: litExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_litExpr_in_primExpr729);
                    litExpr67=litExpr();
                    _fsp--;

                    adaptor.addChild(root_0, litExpr67.getTree());

                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:32: intExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_intExpr_in_primExpr733);
                    intExpr68=intExpr();
                    _fsp--;

                    adaptor.addChild(root_0, intExpr68.getTree());

                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:42: callExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_callExpr_in_primExpr737);
                    callExpr69=callExpr();
                    _fsp--;

                    adaptor.addChild(root_0, callExpr69.getTree());

                    }
                    break;
                case 5 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:53: parenExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_parenExpr_in_primExpr741);
                    parenExpr70=parenExpr();
                    _fsp--;

                    adaptor.addChild(root_0, parenExpr70.getTree());

                    }
                    break;
                case 6 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:65: typedExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_typedExpr_in_primExpr745);
                    typedExpr71=typedExpr();
                    _fsp--;

                    adaptor.addChild(root_0, typedExpr71.getTree());

                    }
                    break;
                case 7 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:208:77: array
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_array_in_primExpr749);
                    array72=array();
                    _fsp--;

                    adaptor.addChild(root_0, array72.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end primExpr

    public static class varExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start varExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:215:1: varExpr : dotIdent -> ^( VariableExpression dotIdent ) ;
    public final varExpr_return varExpr() throws RecognitionException {
        varExpr_return retval = new varExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        dotIdent_return dotIdent73 = null;


        RewriteRuleSubtreeStream stream_dotIdent=new RewriteRuleSubtreeStream(adaptor,"rule dotIdent");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:215:10: ( dotIdent -> ^( VariableExpression dotIdent ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:215:12: dotIdent
            {
            pushFollow(FOLLOW_dotIdent_in_varExpr760);
            dotIdent73=dotIdent();
            _fsp--;

            stream_dotIdent.add(dotIdent73.getTree());

            // AST REWRITE
            // elements: dotIdent
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 215:21: -> ^( VariableExpression dotIdent )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:215:24: ^( VariableExpression dotIdent )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(VariableExpression, "VariableExpression"), root_1);

                adaptor.addChild(root_1, stream_dotIdent.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end varExpr

    public static class litExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start litExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:216:1: litExpr : NUMBER -> ^( RealExpression NUMBER ) ;
    public final litExpr_return litExpr() throws RecognitionException {
        litExpr_return retval = new litExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token NUMBER74=null;

        CommonTree NUMBER74_tree=null;
        RewriteRuleTokenStream stream_NUMBER=new RewriteRuleTokenStream(adaptor,"token NUMBER");

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:216:10: ( NUMBER -> ^( RealExpression NUMBER ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:216:12: NUMBER
            {
            NUMBER74=(Token)input.LT(1);
            match(input,NUMBER,FOLLOW_NUMBER_in_litExpr777); 
            stream_NUMBER.add(NUMBER74);


            // AST REWRITE
            // elements: NUMBER
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 216:19: -> ^( RealExpression NUMBER )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:216:22: ^( RealExpression NUMBER )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(RealExpression, "RealExpression"), root_1);

                adaptor.addChild(root_1, stream_NUMBER.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end litExpr

    public static class intExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start intExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:217:1: intExpr : INTEGER -> ^( IntegerExpression INTEGER ) ;
    public final intExpr_return intExpr() throws RecognitionException {
        intExpr_return retval = new intExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token INTEGER75=null;

        CommonTree INTEGER75_tree=null;
        RewriteRuleTokenStream stream_INTEGER=new RewriteRuleTokenStream(adaptor,"token INTEGER");

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:217:10: ( INTEGER -> ^( IntegerExpression INTEGER ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:217:12: INTEGER
            {
            INTEGER75=(Token)input.LT(1);
            match(input,INTEGER,FOLLOW_INTEGER_in_intExpr794); 
            stream_INTEGER.add(INTEGER75);


            // AST REWRITE
            // elements: INTEGER
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 217:20: -> ^( IntegerExpression INTEGER )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:217:23: ^( IntegerExpression INTEGER )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(IntegerExpression, "IntegerExpression"), root_1);

                adaptor.addChild(root_1, stream_INTEGER.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end intExpr

    public static class parenExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parenExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:218:1: parenExpr : LPAREN expr RPAREN -> ^( ParenthesisExpression expr ) ;
    public final parenExpr_return parenExpr() throws RecognitionException {
        parenExpr_return retval = new parenExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LPAREN76=null;
        Token RPAREN78=null;
        expr_return expr77 = null;


        CommonTree LPAREN76_tree=null;
        CommonTree RPAREN78_tree=null;
        RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
        RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:218:11: ( LPAREN expr RPAREN -> ^( ParenthesisExpression expr ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:218:13: LPAREN expr RPAREN
            {
            LPAREN76=(Token)input.LT(1);
            match(input,LPAREN,FOLLOW_LPAREN_in_parenExpr810); 
            stream_LPAREN.add(LPAREN76);

            pushFollow(FOLLOW_expr_in_parenExpr812);
            expr77=expr();
            _fsp--;

            stream_expr.add(expr77.getTree());
            RPAREN78=(Token)input.LT(1);
            match(input,RPAREN,FOLLOW_RPAREN_in_parenExpr814); 
            stream_RPAREN.add(RPAREN78);


            // AST REWRITE
            // elements: expr
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 218:32: -> ^( ParenthesisExpression expr )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:218:35: ^( ParenthesisExpression expr )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(ParenthesisExpression, "ParenthesisExpression"), root_1);

                adaptor.addChild(root_1, stream_expr.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end parenExpr

    public static class callExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start callExpr
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:219:1: callExpr : dotIdent LPAREN ( exprList )? RPAREN -> ^( CallExpression dotIdent ( exprList )? ) ;
    public final callExpr_return callExpr() throws RecognitionException {
        callExpr_return retval = new callExpr_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LPAREN80=null;
        Token RPAREN82=null;
        dotIdent_return dotIdent79 = null;

        exprList_return exprList81 = null;


        CommonTree LPAREN80_tree=null;
        CommonTree RPAREN82_tree=null;
        RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
        RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
        RewriteRuleSubtreeStream stream_dotIdent=new RewriteRuleSubtreeStream(adaptor,"rule dotIdent");
        RewriteRuleSubtreeStream stream_exprList=new RewriteRuleSubtreeStream(adaptor,"rule exprList");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:219:11: ( dotIdent LPAREN ( exprList )? RPAREN -> ^( CallExpression dotIdent ( exprList )? ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:219:13: dotIdent LPAREN ( exprList )? RPAREN
            {
            pushFollow(FOLLOW_dotIdent_in_callExpr831);
            dotIdent79=dotIdent();
            _fsp--;

            stream_dotIdent.add(dotIdent79.getTree());
            LPAREN80=(Token)input.LT(1);
            match(input,LPAREN,FOLLOW_LPAREN_in_callExpr833); 
            stream_LPAREN.add(LPAREN80);

            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:219:29: ( exprList )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( ((LA22_0>=PLUS && LA22_0<=MINUS)||LA22_0==AND||LA22_0==STAR||(LA22_0>=NOT && LA22_0<=LPAREN)||(LA22_0>=NUMBER && LA22_0<=INTEGER)||LA22_0==LSQUB||LA22_0==LCURLB||LA22_0==IDENT) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:219:29: exprList
                    {
                    pushFollow(FOLLOW_exprList_in_callExpr835);
                    exprList81=exprList();
                    _fsp--;

                    stream_exprList.add(exprList81.getTree());

                    }
                    break;

            }

            RPAREN82=(Token)input.LT(1);
            match(input,RPAREN,FOLLOW_RPAREN_in_callExpr838); 
            stream_RPAREN.add(RPAREN82);


            // AST REWRITE
            // elements: dotIdent, exprList
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 219:46: -> ^( CallExpression dotIdent ( exprList )? )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:219:49: ^( CallExpression dotIdent ( exprList )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(CallExpression, "CallExpression"), root_1);

                adaptor.addChild(root_1, stream_dotIdent.next());
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:219:75: ( exprList )?
                if ( stream_exprList.hasNext() ) {
                    adaptor.addChild(root_1, stream_exprList.next());

                }
                stream_exprList.reset();

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end callExpr

    public static class exprList_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start exprList
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:220:1: exprList : expr ( COMMA expr )* ;
    public final exprList_return exprList() throws RecognitionException {
        exprList_return retval = new exprList_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token COMMA84=null;
        expr_return expr83 = null;

        expr_return expr85 = null;


        CommonTree COMMA84_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:220:10: ( expr ( COMMA expr )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:220:12: expr ( COMMA expr )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_expr_in_exprList857);
            expr83=expr();
            _fsp--;

            adaptor.addChild(root_0, expr83.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:220:17: ( COMMA expr )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==COMMA) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:220:18: COMMA expr
            	    {
            	    COMMA84=(Token)input.LT(1);
            	    match(input,COMMA,FOLLOW_COMMA_in_exprList860); 
            	    pushFollow(FOLLOW_expr_in_exprList863);
            	    expr85=expr();
            	    _fsp--;

            	    adaptor.addChild(root_0, expr85.getTree());

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end exprList

    public static class arrayElem_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start arrayElem
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:1: arrayElem : ( varExpr | litExpr | intExpr | parenExpr | typedExpr | array | arrayElemNeg );
    public final arrayElem_return arrayElem() throws RecognitionException {
        arrayElem_return retval = new arrayElem_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        varExpr_return varExpr86 = null;

        litExpr_return litExpr87 = null;

        intExpr_return intExpr88 = null;

        parenExpr_return parenExpr89 = null;

        typedExpr_return typedExpr90 = null;

        array_return array91 = null;

        arrayElemNeg_return arrayElemNeg92 = null;



        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:11: ( varExpr | litExpr | intExpr | parenExpr | typedExpr | array | arrayElemNeg )
            int alt24=7;
            switch ( input.LA(1) ) {
            case IDENT:
                {
                alt24=1;
                }
                break;
            case NUMBER:
                {
                alt24=2;
                }
                break;
            case INTEGER:
                {
                alt24=3;
                }
                break;
            case LPAREN:
                {
                alt24=4;
                }
                break;
            case SL_DATA_TYPE:
                {
                alt24=5;
                }
                break;
            case LSQUB:
                {
                alt24=6;
                }
                break;
            case MINUS:
                {
                alt24=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("222:1: arrayElem : ( varExpr | litExpr | intExpr | parenExpr | typedExpr | array | arrayElemNeg );", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:13: varExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_varExpr_in_arrayElem874);
                    varExpr86=varExpr();
                    _fsp--;

                    adaptor.addChild(root_0, varExpr86.getTree());

                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:23: litExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_litExpr_in_arrayElem878);
                    litExpr87=litExpr();
                    _fsp--;

                    adaptor.addChild(root_0, litExpr87.getTree());

                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:33: intExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_intExpr_in_arrayElem882);
                    intExpr88=intExpr();
                    _fsp--;

                    adaptor.addChild(root_0, intExpr88.getTree());

                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:43: parenExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_parenExpr_in_arrayElem886);
                    parenExpr89=parenExpr();
                    _fsp--;

                    adaptor.addChild(root_0, parenExpr89.getTree());

                    }
                    break;
                case 5 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:55: typedExpr
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_typedExpr_in_arrayElem890);
                    typedExpr90=typedExpr();
                    _fsp--;

                    adaptor.addChild(root_0, typedExpr90.getTree());

                    }
                    break;
                case 6 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:67: array
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_array_in_arrayElem894);
                    array91=array();
                    _fsp--;

                    adaptor.addChild(root_0, array91.getTree());

                    }
                    break;
                case 7 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:222:75: arrayElemNeg
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_arrayElemNeg_in_arrayElem898);
                    arrayElemNeg92=arrayElemNeg();
                    _fsp--;

                    adaptor.addChild(root_0, arrayElemNeg92.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end arrayElem

    public static class arrayElemNeg_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start arrayElemNeg
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:223:1: arrayElemNeg : MINUS arrayElem -> ^( UnaryExpression arrayElem MINUS ) ;
    public final arrayElemNeg_return arrayElemNeg() throws RecognitionException {
        arrayElemNeg_return retval = new arrayElemNeg_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token MINUS93=null;
        arrayElem_return arrayElem94 = null;


        CommonTree MINUS93_tree=null;
        RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
        RewriteRuleSubtreeStream stream_arrayElem=new RewriteRuleSubtreeStream(adaptor,"rule arrayElem");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:223:14: ( MINUS arrayElem -> ^( UnaryExpression arrayElem MINUS ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:223:16: MINUS arrayElem
            {
            MINUS93=(Token)input.LT(1);
            match(input,MINUS,FOLLOW_MINUS_in_arrayElemNeg905); 
            stream_MINUS.add(MINUS93);

            pushFollow(FOLLOW_arrayElem_in_arrayElemNeg907);
            arrayElem94=arrayElem();
            _fsp--;

            stream_arrayElem.add(arrayElem94.getTree());

            // AST REWRITE
            // elements: arrayElem, MINUS
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 223:32: -> ^( UnaryExpression arrayElem MINUS )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:223:35: ^( UnaryExpression arrayElem MINUS )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(UnaryExpression, "UnaryExpression"), root_1);

                adaptor.addChild(root_1, stream_arrayElem.next());
                adaptor.addChild(root_1, stream_MINUS.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end arrayElemNeg

    public static class arrayLn_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start arrayLn
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:1: arrayLn : ( arrayElem ( ( COMMA )? arrayElem )* ) -> ^( ListExpression ( arrayElem )+ ) ;
    public final arrayLn_return arrayLn() throws RecognitionException {
        arrayLn_return retval = new arrayLn_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token COMMA96=null;
        arrayElem_return arrayElem95 = null;

        arrayElem_return arrayElem97 = null;


        CommonTree COMMA96_tree=null;
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleSubtreeStream stream_arrayElem=new RewriteRuleSubtreeStream(adaptor,"rule arrayElem");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:10: ( ( arrayElem ( ( COMMA )? arrayElem )* ) -> ^( ListExpression ( arrayElem )+ ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:13: ( arrayElem ( ( COMMA )? arrayElem )* )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:13: ( arrayElem ( ( COMMA )? arrayElem )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:14: arrayElem ( ( COMMA )? arrayElem )*
            {
            pushFollow(FOLLOW_arrayElem_in_arrayLn928);
            arrayElem95=arrayElem();
            _fsp--;

            stream_arrayElem.add(arrayElem95.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:24: ( ( COMMA )? arrayElem )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==MINUS||(LA26_0>=SL_DATA_TYPE && LA26_0<=LPAREN)||(LA26_0>=NUMBER && LA26_0<=LSQUB)||LA26_0==IDENT) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:25: ( COMMA )? arrayElem
            	    {
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:25: ( COMMA )?
            	    int alt25=2;
            	    int LA25_0 = input.LA(1);

            	    if ( (LA25_0==COMMA) ) {
            	        alt25=1;
            	    }
            	    switch (alt25) {
            	        case 1 :
            	            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:25: COMMA
            	            {
            	            COMMA96=(Token)input.LT(1);
            	            match(input,COMMA,FOLLOW_COMMA_in_arrayLn931); 
            	            stream_COMMA.add(COMMA96);


            	            }
            	            break;

            	    }

            	    pushFollow(FOLLOW_arrayElem_in_arrayLn934);
            	    arrayElem97=arrayElem();
            	    _fsp--;

            	    stream_arrayElem.add(arrayElem97.getTree());

            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            // AST REWRITE
            // elements: arrayElem
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 224:45: -> ^( ListExpression ( arrayElem )+ )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:224:48: ^( ListExpression ( arrayElem )+ )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(ListExpression, "ListExpression"), root_1);

                if ( !(stream_arrayElem.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_arrayElem.hasNext() ) {
                    adaptor.addChild(root_1, stream_arrayElem.next());

                }
                stream_arrayElem.reset();

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end arrayLn

    public static class array_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start array
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:225:1: array : LSQUB arrayLn ( SCOLON arrayLn )* RSQUB -> ^( ListExpression arrayLn ( arrayLn )* ) ;
    public final array_return array() throws RecognitionException {
        array_return retval = new array_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LSQUB98=null;
        Token SCOLON100=null;
        Token RSQUB102=null;
        arrayLn_return arrayLn99 = null;

        arrayLn_return arrayLn101 = null;


        CommonTree LSQUB98_tree=null;
        CommonTree SCOLON100_tree=null;
        CommonTree RSQUB102_tree=null;
        RewriteRuleTokenStream stream_LSQUB=new RewriteRuleTokenStream(adaptor,"token LSQUB");
        RewriteRuleTokenStream stream_RSQUB=new RewriteRuleTokenStream(adaptor,"token RSQUB");
        RewriteRuleTokenStream stream_SCOLON=new RewriteRuleTokenStream(adaptor,"token SCOLON");
        RewriteRuleSubtreeStream stream_arrayLn=new RewriteRuleSubtreeStream(adaptor,"rule arrayLn");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:225:8: ( LSQUB arrayLn ( SCOLON arrayLn )* RSQUB -> ^( ListExpression arrayLn ( arrayLn )* ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:225:10: LSQUB arrayLn ( SCOLON arrayLn )* RSQUB
            {
            LSQUB98=(Token)input.LT(1);
            match(input,LSQUB,FOLLOW_LSQUB_in_array955); 
            stream_LSQUB.add(LSQUB98);

            pushFollow(FOLLOW_arrayLn_in_array957);
            arrayLn99=arrayLn();
            _fsp--;

            stream_arrayLn.add(arrayLn99.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:225:24: ( SCOLON arrayLn )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==SCOLON) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:225:25: SCOLON arrayLn
            	    {
            	    SCOLON100=(Token)input.LT(1);
            	    match(input,SCOLON,FOLLOW_SCOLON_in_array960); 
            	    stream_SCOLON.add(SCOLON100);

            	    pushFollow(FOLLOW_arrayLn_in_array962);
            	    arrayLn101=arrayLn();
            	    _fsp--;

            	    stream_arrayLn.add(arrayLn101.getTree());

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            RSQUB102=(Token)input.LT(1);
            match(input,RSQUB,FOLLOW_RSQUB_in_array966); 
            stream_RSQUB.add(RSQUB102);


            // AST REWRITE
            // elements: arrayLn, arrayLn
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 225:48: -> ^( ListExpression arrayLn ( arrayLn )* )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:225:51: ^( ListExpression arrayLn ( arrayLn )* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(ListExpression, "ListExpression"), root_1);

                adaptor.addChild(root_1, stream_arrayLn.next());
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:225:76: ( arrayLn )*
                while ( stream_arrayLn.hasNext() ) {
                    adaptor.addChild(root_1, stream_arrayLn.next());

                }
                stream_arrayLn.reset();

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end array

    public static class enumeration_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start enumeration
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:226:1: enumeration : LCURLB arrayLn RCURLB -> ^( ListExpression arrayLn ) ;
    public final enumeration_return enumeration() throws RecognitionException {
        enumeration_return retval = new enumeration_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LCURLB103=null;
        Token RCURLB105=null;
        arrayLn_return arrayLn104 = null;


        CommonTree LCURLB103_tree=null;
        CommonTree RCURLB105_tree=null;
        RewriteRuleTokenStream stream_RCURLB=new RewriteRuleTokenStream(adaptor,"token RCURLB");
        RewriteRuleTokenStream stream_LCURLB=new RewriteRuleTokenStream(adaptor,"token LCURLB");
        RewriteRuleSubtreeStream stream_arrayLn=new RewriteRuleSubtreeStream(adaptor,"rule arrayLn");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:226:13: ( LCURLB arrayLn RCURLB -> ^( ListExpression arrayLn ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:226:15: LCURLB arrayLn RCURLB
            {
            LCURLB103=(Token)input.LT(1);
            match(input,LCURLB,FOLLOW_LCURLB_in_enumeration984); 
            stream_LCURLB.add(LCURLB103);

            pushFollow(FOLLOW_arrayLn_in_enumeration986);
            arrayLn104=arrayLn();
            _fsp--;

            stream_arrayLn.add(arrayLn104.getTree());
            RCURLB105=(Token)input.LT(1);
            match(input,RCURLB,FOLLOW_RCURLB_in_enumeration988); 
            stream_RCURLB.add(RCURLB105);


            // AST REWRITE
            // elements: arrayLn
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 226:37: -> ^( ListExpression arrayLn )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:226:40: ^( ListExpression arrayLn )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(ListExpression, "ListExpression"), root_1);

                adaptor.addChild(root_1, stream_arrayLn.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end enumeration

    public static class dotIdents_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start dotIdents
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:229:1: dotIdents : dotIdent ( COMMA dotIdent )* ;
    public final dotIdents_return dotIdents() throws RecognitionException {
        dotIdents_return retval = new dotIdents_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token COMMA107=null;
        dotIdent_return dotIdent106 = null;

        dotIdent_return dotIdent108 = null;


        CommonTree COMMA107_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:229:11: ( dotIdent ( COMMA dotIdent )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:229:13: dotIdent ( COMMA dotIdent )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_dotIdent_in_dotIdents1005);
            dotIdent106=dotIdent();
            _fsp--;

            adaptor.addChild(root_0, dotIdent106.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:229:22: ( COMMA dotIdent )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==COMMA) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:229:23: COMMA dotIdent
            	    {
            	    COMMA107=(Token)input.LT(1);
            	    match(input,COMMA,FOLLOW_COMMA_in_dotIdents1008); 
            	    pushFollow(FOLLOW_dotIdent_in_dotIdents1011);
            	    dotIdent108=dotIdent();
            	    _fsp--;

            	    adaptor.addChild(root_0, dotIdent108.getTree());

            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end dotIdents

    public static class dotIdent_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start dotIdent
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:230:1: dotIdent : dotIdent_ -> ^( DotIdent dotIdent_ ) ;
    public final dotIdent_return dotIdent() throws RecognitionException {
        dotIdent_return retval = new dotIdent_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        dotIdent__return dotIdent_109 = null;


        RewriteRuleSubtreeStream stream_dotIdent_=new RewriteRuleSubtreeStream(adaptor,"rule dotIdent_");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:230:10: ( dotIdent_ -> ^( DotIdent dotIdent_ ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:230:12: dotIdent_
            {
            pushFollow(FOLLOW_dotIdent__in_dotIdent1021);
            dotIdent_109=dotIdent_();
            _fsp--;

            stream_dotIdent_.add(dotIdent_109.getTree());

            // AST REWRITE
            // elements: dotIdent_
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 230:23: -> ^( DotIdent dotIdent_ )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:230:26: ^( DotIdent dotIdent_ )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(adaptor.create(DotIdent, "DotIdent"), root_1);

                adaptor.addChild(root_1, stream_dotIdent_.next());

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end dotIdent

    public static class dotIdent__return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start dotIdent_
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:231:1: dotIdent_ : ident ( DOT ident )* ;
    public final dotIdent__return dotIdent_() throws RecognitionException {
        dotIdent__return retval = new dotIdent__return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token DOT111=null;
        ident_return ident110 = null;

        ident_return ident112 = null;


        CommonTree DOT111_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:231:11: ( ident ( DOT ident )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:231:13: ident ( DOT ident )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_ident_in_dotIdent_1038);
            ident110=ident();
            _fsp--;

            adaptor.addChild(root_0, ident110.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:231:19: ( DOT ident )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==DOT) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:231:20: DOT ident
            	    {
            	    DOT111=(Token)input.LT(1);
            	    match(input,DOT,FOLLOW_DOT_in_dotIdent_1041); 
            	    DOT111_tree = (CommonTree)adaptor.create(DOT111);
            	    adaptor.addChild(root_0, DOT111_tree);

            	    pushFollow(FOLLOW_ident_in_dotIdent_1043);
            	    ident112=ident();
            	    _fsp--;

            	    adaptor.addChild(root_0, ident112.getTree());

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end dotIdent_

    public static class cmtIdent_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start cmtIdent
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:1: cmtIdent : ( nlComments )? ident -> ^( ident ^( Annotations ( nlComments )? ) ) ;
    public final cmtIdent_return cmtIdent() throws RecognitionException {
        cmtIdent_return retval = new cmtIdent_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        nlComments_return nlComments113 = null;

        ident_return ident114 = null;


        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_nlComments=new RewriteRuleSubtreeStream(adaptor,"rule nlComments");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:10: ( ( nlComments )? ident -> ^( ident ^( Annotations ( nlComments )? ) ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:12: ( nlComments )? ident
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:12: ( nlComments )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( ((LA30_0>=NL && LA30_0<=COMMENT)) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:12: nlComments
                    {
                    pushFollow(FOLLOW_nlComments_in_cmtIdent1054);
                    nlComments113=nlComments();
                    _fsp--;

                    stream_nlComments.add(nlComments113.getTree());

                    }
                    break;

            }

            pushFollow(FOLLOW_ident_in_cmtIdent1057);
            ident114=ident();
            _fsp--;

            stream_ident.add(ident114.getTree());

            // AST REWRITE
            // elements: nlComments, ident
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 233:30: -> ^( ident ^( Annotations ( nlComments )? ) )
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:33: ^( ident ^( Annotations ( nlComments )? ) )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_1);

                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:41: ^( Annotations ( nlComments )? )
                {
                CommonTree root_2 = (CommonTree)adaptor.nil();
                root_2 = (CommonTree)adaptor.becomeRoot(adaptor.create(Annotations, "Annotations"), root_2);

                // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:233:55: ( nlComments )?
                if ( stream_nlComments.hasNext() ) {
                    adaptor.addChild(root_2, stream_nlComments.next());

                }
                stream_nlComments.reset();

                adaptor.addChild(root_1, root_2);
                }

                adaptor.addChild(root_0, root_1);
                }

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end cmtIdent

    public static class ident_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start ident
    // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:235:1: ident : IDENT ;
    public final ident_return ident() throws RecognitionException {
        ident_return retval = new ident_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token IDENT115=null;

        CommonTree IDENT115_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:235:7: ( IDENT )
            // T:\\geneauto-dev\\Eclipse\\geneauto.models\\src\\main\\java\\geneauto\\models\\utilities\\emlconverter\\antlrgrammar\\EMLExpressions.g:235:9: IDENT
            {
            root_0 = (CommonTree)adaptor.nil();

            IDENT115=(Token)input.LT(1);
            match(input,IDENT,FOLLOW_IDENT_in_ident1079); 
            IDENT115_tree = (CommonTree)adaptor.create(IDENT115);
            adaptor.addChild(root_0, IDENT115_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end ident


    protected DFA4 dfa4 = new DFA4(this);
    protected DFA6 dfa6 = new DFA6(this);
    protected DFA8 dfa8 = new DFA8(this);
    protected DFA21 dfa21 = new DFA21(this);
    static final String DFA4_eotS =
        "\4\uffff";
    static final String DFA4_eofS =
        "\4\uffff";
    static final String DFA4_minS =
        "\2\27\2\uffff";
    static final String DFA4_maxS =
        "\1\31\1\67\2\uffff";
    static final String DFA4_acceptS =
        "\2\uffff\1\1\1\2";
    static final String DFA4_specialS =
        "\4\uffff}>";
    static final String[] DFA4_transitionS = {
            "\1\1\2\2",
            "\1\1\2\2\35\uffff\1\3",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "167:1: nlComments : ( ( NL )* comments ( NL )* | ( NL )+ );";
        }
    }
    static final String DFA6_eotS =
        "\4\uffff";
    static final String DFA6_eofS =
        "\4\uffff";
    static final String DFA6_minS =
        "\2\27\2\uffff";
    static final String DFA6_maxS =
        "\2\67\2\uffff";
    static final String DFA6_acceptS =
        "\2\uffff\1\2\1\1";
    static final String DFA6_specialS =
        "\4\uffff}>";
    static final String[] DFA6_transitionS = {
            "\1\1\2\3\35\uffff\1\2",
            "\1\1\2\3\35\uffff\1\2",
            "",
            ""
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "()* loopback of 171:20: ( ( NL )* comment )*";
        }
    }
    static final String DFA8_eotS =
        "\6\uffff";
    static final String DFA8_eofS =
        "\1\uffff\1\3\3\uffff\1\3";
    static final String DFA8_minS =
        "\1\67\1\32\1\67\2\uffff\1\32";
    static final String DFA8_maxS =
        "\1\67\1\66\1\67\2\uffff\1\66";
    static final String DFA8_acceptS =
        "\3\uffff\1\1\1\2\1\uffff";
    static final String DFA8_specialS =
        "\6\uffff}>";
    static final String[] DFA8_transitionS = {
            "\1\1",
            "\3\3\17\uffff\1\4\11\uffff\1\2",
            "\1\5",
            "",
            "",
            "\3\3\17\uffff\1\4\11\uffff\1\2"
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        public String getDescription() {
            return "175:1: stmt : ( ( varExpr ( -> ^( ExpressionStatement varExpr ) | PLUS PLUS -> ^( IncStatement varExpr ) | MINUS MINUS -> ^( DecStatement varExpr ) | ASSIGN expr -> ^( AssignStatement ASSIGN varExpr expr ) ) ) | callExpr -> ^( ExpressionStatement callExpr ) );";
        }
    }
    static final String DFA21_eotS =
        "\13\uffff";
    static final String DFA21_eofS =
        "\1\uffff\1\11\10\uffff\1\11";
    static final String DFA21_minS =
        "\1\53\1\32\5\uffff\1\67\2\uffff\1\32";
    static final String DFA21_maxS =
        "\1\67\1\66\5\uffff\1\67\2\uffff\1\66";
    static final String DFA21_acceptS =
        "\2\uffff\1\2\1\3\1\5\1\6\1\7\1\uffff\1\4\1\1\1\uffff";
    static final String DFA21_specialS =
        "\13\uffff}>";
    static final String[] DFA21_transitionS = {
            "\1\5\1\4\1\uffff\1\2\1\3\1\uffff\1\6\5\uffff\1\1",
            "\2\11\1\uffff\14\11\3\uffff\1\10\1\11\2\uffff\1\11\5\uffff\1"+
            "\7",
            "",
            "",
            "",
            "",
            "",
            "\1\12",
            "",
            "",
            "\2\11\1\uffff\14\11\3\uffff\1\10\1\11\2\uffff\1\11\5\uffff\1"+
            "\7"
    };

    static final short[] DFA21_eot = DFA.unpackEncodedString(DFA21_eotS);
    static final short[] DFA21_eof = DFA.unpackEncodedString(DFA21_eofS);
    static final char[] DFA21_min = DFA.unpackEncodedStringToUnsignedChars(DFA21_minS);
    static final char[] DFA21_max = DFA.unpackEncodedStringToUnsignedChars(DFA21_maxS);
    static final short[] DFA21_accept = DFA.unpackEncodedString(DFA21_acceptS);
    static final short[] DFA21_special = DFA.unpackEncodedString(DFA21_specialS);
    static final short[][] DFA21_transition;

    static {
        int numStates = DFA21_transitionS.length;
        DFA21_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA21_transition[i] = DFA.unpackEncodedString(DFA21_transitionS[i]);
        }
    }

    class DFA21 extends DFA {

        public DFA21(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = DFA21_eot;
            this.eof = DFA21_eof;
            this.min = DFA21_min;
            this.max = DFA21_max;
            this.accept = DFA21_accept;
            this.special = DFA21_special;
            this.transition = DFA21_transition;
        }
        public String getDescription() {
            return "208:1: primExpr : ( varExpr | litExpr | intExpr | callExpr | parenExpr | typedExpr | array );";
        }
    }
 

    public static final BitSet FOLLOW_NL_in_nlComments227 = new BitSet(new long[]{0x0000000003800000L});
    public static final BitSet FOLLOW_comments_in_nlComments231 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_NL_in_nlComments233 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_NL_in_nlComments241 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_comment_in_comments253 = new BitSet(new long[]{0x0000000003800002L});
    public static final BitSet FOLLOW_NL_in_comments256 = new BitSet(new long[]{0x0000000003800000L});
    public static final BitSet FOLLOW_comment_in_comments260 = new BitSet(new long[]{0x0000000003800002L});
    public static final BitSet FOLLOW_set_in_comment0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varExpr_in_stmt288 = new BitSet(new long[]{0x000000001C000002L});
    public static final BitSet FOLLOW_PLUS_in_stmt310 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_PLUS_in_stmt312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_stmt329 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_MINUS_in_stmt331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ASSIGN_in_stmt346 = new BitSet(new long[]{0x0092DE220C000000L});
    public static final BitSet FOLLOW_expr_in_stmt348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callExpr_in_stmt374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_enumeration_in_expr398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lOrExpr_in_expr402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lAndExpr_in_lOrExpr411 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_LOR_in_lOrExpr414 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_lAndExpr_in_lOrExpr417 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_orExpr_in_lAndExpr428 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_LAND_in_lAndExpr431 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_orExpr_in_lAndExpr434 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_xorExpr_in_orExpr446 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_OR_in_orExpr449 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_xorExpr_in_orExpr452 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_andExpr_in_xorExpr463 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_XOR_in_xorExpr466 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_andExpr_in_xorExpr469 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_eqExpr_in_andExpr480 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_AND_in_andExpr483 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_eqExpr_in_andExpr486 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_compExpr_in_eqExpr497 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_EQ_OP_in_eqExpr500 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_compExpr_in_eqExpr503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_shiftExpr_in_compExpr512 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_COMP_OP_in_compExpr515 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_shiftExpr_in_compExpr518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_sumExpr_in_shiftExpr528 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_SHIFT_in_shiftExpr531 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_sumExpr_in_shiftExpr534 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_prodExpr_in_sumExpr545 = new BitSet(new long[]{0x000000000C000002L});
    public static final BitSet FOLLOW_set_in_sumExpr548 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_prodExpr_in_sumExpr555 = new BitSet(new long[]{0x000000000C000002L});
    public static final BitSet FOLLOW_prefExpr_in_prodExpr565 = new BitSet(new long[]{0x000001E000000002L});
    public static final BitSet FOLLOW_set_in_prodExpr568 = new BitSet(new long[]{0x0082DE220C000000L});
    public static final BitSet FOLLOW_prefExpr_in_prodExpr579 = new BitSet(new long[]{0x000001E000000002L});
    public static final BitSet FOLLOW_MINUS_in_prefExpr590 = new BitSet(new long[]{0x0082D80000000000L});
    public static final BitSet FOLLOW_primExpr_in_prefExpr592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PLUS_in_prefExpr607 = new BitSet(new long[]{0x0082D80000000000L});
    public static final BitSet FOLLOW_primExpr_in_prefExpr609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NOT_in_prefExpr624 = new BitSet(new long[]{0x0082D80000000000L});
    public static final BitSet FOLLOW_primExpr_in_prefExpr626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TILDE_in_prefExpr641 = new BitSet(new long[]{0x0082D80000000000L});
    public static final BitSet FOLLOW_primExpr_in_prefExpr643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STAR_in_prefExpr658 = new BitSet(new long[]{0x0082D80000000000L});
    public static final BitSet FOLLOW_primExpr_in_prefExpr660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AND_in_prefExpr675 = new BitSet(new long[]{0x0082D80000000000L});
    public static final BitSet FOLLOW_primExpr_in_prefExpr677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primExpr_in_prefExpr694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SL_DATA_TYPE_in_typedExpr702 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_LPAREN_in_typedExpr704 = new BitSet(new long[]{0x0092DE220C000000L});
    public static final BitSet FOLLOW_expr_in_typedExpr706 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_RPAREN_in_typedExpr708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varExpr_in_primExpr725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_litExpr_in_primExpr729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_intExpr_in_primExpr733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callExpr_in_primExpr737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parenExpr_in_primExpr741 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_typedExpr_in_primExpr745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_array_in_primExpr749 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dotIdent_in_varExpr760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NUMBER_in_litExpr777 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_in_intExpr794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_parenExpr810 = new BitSet(new long[]{0x0092DE220C000000L});
    public static final BitSet FOLLOW_expr_in_parenExpr812 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_RPAREN_in_parenExpr814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dotIdent_in_callExpr831 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_LPAREN_in_callExpr833 = new BitSet(new long[]{0x0092FE220C000000L});
    public static final BitSet FOLLOW_exprList_in_callExpr835 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_RPAREN_in_callExpr838 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_exprList857 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_COMMA_in_exprList860 = new BitSet(new long[]{0x0092DE220C000000L});
    public static final BitSet FOLLOW_expr_in_exprList863 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_varExpr_in_arrayElem874 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_litExpr_in_arrayElem878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_intExpr_in_arrayElem882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parenExpr_in_arrayElem886 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_typedExpr_in_arrayElem890 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_array_in_arrayElem894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arrayElemNeg_in_arrayElem898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_arrayElemNeg905 = new BitSet(new long[]{0x0082D80008000000L});
    public static final BitSet FOLLOW_arrayElem_in_arrayElemNeg907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arrayElem_in_arrayLn928 = new BitSet(new long[]{0x0083D80008000002L});
    public static final BitSet FOLLOW_COMMA_in_arrayLn931 = new BitSet(new long[]{0x0082D80008000000L});
    public static final BitSet FOLLOW_arrayElem_in_arrayLn934 = new BitSet(new long[]{0x0083D80008000002L});
    public static final BitSet FOLLOW_LSQUB_in_array955 = new BitSet(new long[]{0x0082D80008000000L});
    public static final BitSet FOLLOW_arrayLn_in_array957 = new BitSet(new long[]{0x000C000000000000L});
    public static final BitSet FOLLOW_SCOLON_in_array960 = new BitSet(new long[]{0x0082D80008000000L});
    public static final BitSet FOLLOW_arrayLn_in_array962 = new BitSet(new long[]{0x000C000000000000L});
    public static final BitSet FOLLOW_RSQUB_in_array966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LCURLB_in_enumeration984 = new BitSet(new long[]{0x0082D80008000000L});
    public static final BitSet FOLLOW_arrayLn_in_enumeration986 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_RCURLB_in_enumeration988 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dotIdent_in_dotIdents1005 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_COMMA_in_dotIdents1008 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_dotIdent_in_dotIdents1011 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_dotIdent__in_dotIdent1021 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ident_in_dotIdent_1038 = new BitSet(new long[]{0x0040000000000002L});
    public static final BitSet FOLLOW_DOT_in_dotIdent_1041 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_ident_in_dotIdent_1043 = new BitSet(new long[]{0x0040000000000002L});
    public static final BitSet FOLLOW_nlComments_in_cmtIdent1054 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_ident_in_cmtIdent1057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_ident1079 = new BitSet(new long[]{0x0000000000000002L});

}