/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/utilities/EnableHandler.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:23:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.StructureContent;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.expression.TrueExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class EnableHandler {
    
    /**
     * Function to compute the falling edge
     */
    public final static String FUNCTION_FALLING = "falling";
    
    /**
     * Function to compute the rising edge
     */
    public final static String FUNCTION_RISING = "rising";
    
    /**
     * a list of expressions referring to state variables that need to be
     * reset when enabling the subsystem
     */
    private static Stack<List<MemberExpression>> resettableMemory = 
    									new Stack<List<MemberExpression>>();
    
    /**
     * a list of expressions referring to output variables that need to be
     * reset when enabling the subsystem
     */
    private static Stack<List<MemberExpression>> resettableOutputs = 
										new Stack<List<MemberExpression>>();
    
    /**
     * Current block that collects memory reset information
     */
    private static Stack<Block> currentMemoryResetBlock = new Stack<Block>();

    /**
     * Current block that collects output reset information
     */
    private static Stack<Block> currentOutputResetBlock = new Stack<Block>();

    /**
     * Processes a enable and edge-enable ports and 
     * returns corresponding statement(s).
     * 
     */
    public static List<Statement> processEnablePorts(
    		List<Inport> ports, NameSpace namespace, Expression condition) {
        // TODO copy the corresponding code from FMCMG SystemBlock processing 
    	// section here to reduce the number of lines there 
        return null;
    }

    /**
     * 
     * @param enablerVarExp
     * @param portName
     * @return
     */
    public static IfStatement getEnableIfStatement(
            Expression enablerVarExp, String portName) {
    	IfStatement ifStmt = null;
        List<Expression> dims = enablerVarExp.getDataType().getDimensions();
        if (dims.size()>0) {
        	// if (%i[0] || %i[1] || ...) {}
            Expression condExpr = null;
        	for (int i = 0; i < Integer.parseInt(
        			(((IntegerExpression)dims.get(0)).getLitValue())); i++) {
                Expression eVarExp = enablerVarExp.getCopy();
                eVarExp.addIndexExpression(new IntegerExpression(i));
                
                if (i==0) {
                	// First iteration and first expression
                	condExpr = eVarExp;
                } else {
                	// Disjunction of expressions
                    condExpr = new BinaryExpression(condExpr, eVarExp,
            				BinaryOperator.LOGICAL_OR_OPERATOR );
                }
         	}
        	ifStmt = new IfStatement(condExpr, null, null);
        } else {
        	// if (%i) {}
        	ifStmt = new IfStatement(enablerVarExp.getCopy(), null, null);
        }
        // giving a name to the if statement
        ifStmt.setName(portName + " Enable IfStatement");
        return ifStmt;
    }

    /**
     * Create a memory variable for edge detection
     * 
     * @param enablerVarExp Expression that is checked for rising or falling edge 
     * @param suffix Optional suffix that is added to the name 
     * @return 
     */
    public static StructureMember makeEdgeEnableMemory(
            	Expression enablerVarExp, Block block) {
        // variable declaration
    	StructureMember memory = new StructureMember();

        memory.setDataType(enablerVarExp.getDataType().getCopy());
        // block memory is not optimisable
        memory.setOptimizable(false);

        // set variable name
        String name = block.getName() + "_edge_mem";

        // Printer does correct composition for vectors
        memory.setName(name);
        
        return memory;
    }

    /**
     * composes a variable to memorise whether the block is enabled
     * or disabled during the last tick
     *  
     * @param block
     * @return
     */
    public static StructureMember makeEnableMode (Block block) {
    	if (blockHasReset(block)) {
            // variable declaration
        	StructureMember memory = new StructureMember();

            memory.setDataType(new TBoolean());
            // block memory is not optimisable
            memory.setOptimizable(false);

            // set variable name
            String name = block.getName() + "_ENABLED";

            // Printer does correct composition for vectors
            memory.setName(name);
            
            return memory;
    	} else {
    		// the block does not need the mode attribute
    		return null;
    	}
    }
    
    /**
     * @param enablerIf - INOUT PARAMETER modified statement
     * @param block
     * @param memories
     * @param p_statements - Statements to be embedded in the enablerIf
     */
    public static void fillInEnablerIf (IfStatement enablerIf, 
    										Block block, 
    										CustomType memories, 
    										List<Statement> p_statements) {
    	
    	if (enablerIf == null) {
    		// TODO: decide if we need error here
    		return;
    	}
    	// make variable to memorise enable mode
        StructureMember modeVar = EnableHandler.makeEnableMode(block);
        IfStatement ifEnabled = null;
        IfStatement ifDisabled = null;

        List<Statement> thenStatements = new LinkedList<Statement>();
        List<Statement> elseStatements = new LinkedList<Statement>();

        // if variable was created, complement the if statement
        if (modeVar != null) {
        	((StructureContent) memories.getContent()).addStructureMember(modeVar);
        	
        	// assume that the memory is in the function attribute with fixed name
        	Expression modeVarRef = new MemberExpression(modeVar,
    						new VariableExpression(GAConst.VAR_MEM_NAME));
        	
        	List<Statement> resetStatements = 
        		createResetStatements(resettableOutputs);
        	Statement ifEnableThen = null;
        	if (!(resetStatements.isEmpty())) {
        		ifEnableThen = new CompoundStatement(resetStatements);
        		((CompoundStatement) ifEnableThen).addStatement(
        				new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
        						modeVarRef,
        						new FalseExpression()));
        	} else {
        		ifEnableThen = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
    					modeVarRef,
    					new FalseExpression());
        	}
        	// statement to put the mode flag off
        	ifEnabled = new IfStatement(
        			modeVarRef,
        			ifEnableThen,
        			null);
        	
        	
        	elseStatements.add(ifEnabled);
        	
        	// statement to put the mode flag on
        	resetStatements = createResetStatements(resettableMemory);
        	Statement ifDisableThen = null;
        	if (!(resetStatements.isEmpty())) {
        		ifDisableThen = new CompoundStatement(resetStatements);
        		((CompoundStatement) ifDisableThen).addStatement(
        				new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
        						modeVarRef,
        						new TrueExpression()));
        	} else {
        		ifDisableThen = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
    					modeVarRef,
    					new TrueExpression());
        	}
        	
        	ifDisabled = new IfStatement(
        			new UnaryExpression(modeVarRef, UnaryOperator.NOT_OPERATOR),
        			ifDisableThen,
        			null);
        	thenStatements.add(ifDisabled);
        }

        // Set the thenStatement attribute of the IfStatement
        // with the retrieved compound statement
        enablerIf.addAnnotation("Guarded block " + block.getOriginalFullName());
        
        if (enablerIf.getThenStatement() != null) {
            List<Statement> ifStatements = new ArrayList<Statement>();
            // add first the existing statements
            if (enablerIf.getThenStatement() instanceof CompoundStatement) {
                ifStatements.addAll(((CompoundStatement) enablerIf
                          				.getThenStatement()).getStatements());
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        "EnableHandler.fillInEnablerIf", "",
                        "Statement corresponding to control port "
                                + "handling is expected to be a "
                                + "CompoundStatement \n Block: "
                                + block.getReferenceString(), "");
            }
        }
        thenStatements.addAll(p_statements);

        if (enablerIf.getElseStatement() != null) {
            List<Statement> ifStatements = new ArrayList<Statement>();
            // add first the existing statements
            if (enablerIf.getElseStatement() instanceof CompoundStatement) {
                ifStatements.addAll(((CompoundStatement) enablerIf
                          				.getElseStatement()).getStatements());
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        "EnableHandler.fillInEnablerIf", "",
                        "Statement corresponding to control port "
                                + "handling is expected to be a "
                                + "CompoundStatement \n Block: "
                                + block.getReferenceString(), "");
            }
        }

        if(!(thenStatements.isEmpty())) {
        	enablerIf.setThenStatement(new CompoundStatement(thenStatements));
        }
        if(!(elseStatements.isEmpty())) {
        	enablerIf.setElseStatement(new CompoundStatement(elseStatements));
        }

        enablerIf.setName("enablerif");
    }
    
    /**
     * determines if given block resets either memory or states when 
     * enable mode is changed
     * 
     * @param block
     * @return
     */
    private static boolean blockHasReset (Block block) {
    	return (blockResetsMemories(block) || blockResetsOutputs(block));
    }
    
    /**
     * Check if the given block resets memories if enabling status changes
     * 
     * @param block
     * @return
     */
    private static boolean blockResetsMemories(Block block) {
    	for(InControlPort p : block.getInControlPorts()) {
    		if (p.isResetStates()) {
    			return true;
    		}
    	}
    	
    	if (block.getInEnablePort() != null 
    			&& block.getInEnablePort().isResetStates()) {
    		return true;
    	}
    	
    	return false;
    }
    
    /**
     * Check if the given block resets any outputs if enabling status changes
     * 
     * @param block
     * @return
     */
    private static boolean blockResetsOutputs(Block block) {
    	for(OutDataPort p : block.getOutDataPorts()) {
    		if (p.isResetOutput()) {
    			return true;
    		}
    	}
    	
    	return false;
    }

    /** 
     * checks if the block needs to reset memories or outputs. 
     * If it does, creates new list for storing collect statements
     * and puts the block in appropriate stack
     */
    public static void activateResetStatementCollection(Block block) {
		if (blockResetsMemories(block)) {
    		currentMemoryResetBlock.add(block);
			resettableMemory.add(new LinkedList<MemberExpression>());
		}
		
		if (blockResetsOutputs(block)) {
    		currentOutputResetBlock.add(block);
			resettableOutputs.add(new LinkedList<MemberExpression>());
		}
    }

    /**
     * Stops collecting resetable memories and outputs for the given block
     * In case of memories the already collected elements are carried one level 
     * up. The output references are simply thrown away.
     */
    public static void deactivateResetStatementCollection(Block block) {
    	// deactivate state variable collection started from the current block
    	if (!currentMemoryResetBlock.isEmpty() 
    			&& currentMemoryResetBlock.peek() == block) {
    		// remove the current block from stack
    		currentMemoryResetBlock.pop();
    		// remove the list of current block statements from the stack
    		List<MemberExpression> lastStmtList = resettableMemory.pop();
    		// in case of nested subsystems, the lower level memories must be 
    		// also reset by the higher level system 
    		// add them to the element remaining in the stack
    		if (!(resettableMemory.isEmpty())) {
    			resettableMemory.peek().addAll(lastStmtList);
    		}
    	}
    	
    	if (!(currentOutputResetBlock.isEmpty()) 
    			&& currentOutputResetBlock.peek() == block) {
    		currentOutputResetBlock.pop();
    		resettableOutputs.pop();
    	}
    }

    /**
     * Check if memorising resetable memories is activated by the current 
     * block or any of its parents. If if is, stores reference to the given
     * variable in top of the stack
     *  
     * @param mem
     */
    public static void memoriseResettableMemory(StructureMember mem) {
    	if (!resettableMemory.isEmpty()) {
    		resettableMemory.peek().add(new MemberExpression(mem,
    					new VariableExpression(GAConst.VAR_MEM_NAME)));
    	}
    }

    /**
     * Check if memorising resetable outputs is activated by the current 
     * block.. If if is, stores reference to the given variable in top of the 
     * stack
     *  
     * @param mem
     */
    public static void memoriseResettableOutputs(List<Expression> outputs, Block block) {
    	if (!(currentOutputResetBlock.isEmpty()) 
    			&& currentOutputResetBlock.peek() == block
    			&& !(resettableOutputs.isEmpty())) {
    		int idx = 0;
    		for(OutDataPort p : block.getOutDataPorts()) {
    			if (p.isResetOutput()) {
    				Expression output = outputs.get(idx);
    				if (output instanceof MemberExpression) {
    		    		resettableOutputs.peek().add((MemberExpression) output);
    				} else {
    					// TODO: consider raising an error here
    				}
    			}
    		}
    	}
    }

    /**
     * Creates reset statements from the given list
     * 
     * @param vars
     * @return
     */
    private static List<Statement> createResetStatements(
    									Stack<List<MemberExpression>> varStack) {
    	
    	List<Statement> stmts = new LinkedList<Statement>();
    	if (varStack.isEmpty()) {
    		return stmts;
    	}
    	
    	for (MemberExpression var : varStack.peek()) {
    		stmts.add(new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
    				var.getCopy(),
    				var.getMember().getInitialValue().getCopy()));
    	}
    	
    	return stmts;
    }
    
    /**
     * Return statements for checking the change of in value and updating memory
     * 
     * @param triggerType
     * @param enablerVarExp
     * @param identifier
     * @param portName
     * @param mVar
     *            memory variable
     * @param index
     *            if a non-negative index is given, then it means that only one
     *            element of the enabler expression and memory variable is used.
     *            Otherwise, if the enabler expression is a non-scalar, then all
     *            elements of it are queried.
     * @return statements for checking the change of in value and updating memory
     * 
     */
    public static List<Statement> getEdgeEnableCode(
    		String triggerType, Expression enablerVarExp, 
    		String portName, Expression mVarRef, int index) {

        // List for statements
    	List<Statement> statements = new ArrayList<Statement>();

        /*
         * creating if statement
         */
        Expression condExpr = null;
        if (triggerType.equals("rising")) {
            
            condExpr = getEnableCodeForDetector(enablerVarExp, mVarRef, index,
                    condExpr, FUNCTION_RISING);
            
        } else if (triggerType.equals("falling")) {
            
            condExpr = getEnableCodeForDetector(enablerVarExp, mVarRef, index,
                    condExpr, FUNCTION_FALLING);
            
        } else if (triggerType.equals("either")) {
        	// if(falling(%i1, memory) || rising(%i1, memory)){}
            
            List<Expression> dims = enablerVarExp.getDataType().getDimensions();
            if (dims.size()>0) {
                
                if (index >= 0) {
                    Expression eVarExp_r = enablerVarExp.getCopy();
                    Expression mVarExp_r = mVarRef.getCopy();
                    List<Expression> args_r = new ArrayList<Expression>();
                    List<Expression> args_f = new ArrayList<Expression>();
                    args_r.add(eVarExp_r);
                    args_r.add(mVarExp_r);
                    eVarExp_r.addIndexExpression(new IntegerExpression(index));                        
                    mVarExp_r.addIndexExpression(new IntegerExpression(index));
                    args_f.add(eVarExp_r.getCopy());
                    args_f.add(mVarExp_r.getCopy());
                    
                    condExpr = new BinaryExpression(
                            new CallExpression(FUNCTION_FALLING, args_f, new TBoolean()), 
                            new CallExpression(FUNCTION_RISING, args_r, new TBoolean()),
                            BinaryOperator.LOGICAL_OR_OPERATOR );
                    
                } else {                
                    // if(falling(%i1[0], memory_f[0]) || rising(%i1[0], memory_r[0])){}
                	for (int i = 0; i < ((IntegerExpression) dims.get(0)).getIntValue(); i++) {
                        Expression eVarExp_r = enablerVarExp.getCopy();
                        Expression mVarExp_r = mVarRef.getCopy();
                        List<Expression> args_r = new ArrayList<Expression>();
                        List<Expression> args_f = new ArrayList<Expression>();
                        args_r.add(eVarExp_r);
                        args_r.add(mVarExp_r);
                        eVarExp_r.addIndexExpression(new IntegerExpression(i));                        
                        mVarExp_r.addIndexExpression(new IntegerExpression(i));
                        args_f.add(eVarExp_r.getCopy());
                        args_f.add(mVarExp_r.getCopy());
    
                        if (i==0) {
                        	// First iteration and first call expression
                        	condExpr = new BinaryExpression(
                                    new CallExpression(FUNCTION_FALLING, args_f, new TBoolean()), 
                                    new CallExpression(FUNCTION_RISING, args_r, new TBoolean()),
                        			BinaryOperator.LOGICAL_OR_OPERATOR );
                        } else {
                        	// Disjunction of call expressions
                            condExpr = new BinaryExpression(
                            		condExpr, 
                            		new BinaryExpression(
                                            new CallExpression(FUNCTION_FALLING, args_f, new TBoolean()), 
                                            new CallExpression(FUNCTION_RISING, args_r, new TBoolean()),
                                			BinaryOperator.LOGICAL_OR_OPERATOR ),
                    				BinaryOperator.LOGICAL_OR_OPERATOR );
                        }
                	}
                }
            } else {
                // Arguments for call expression
                List<Expression> args_r = new ArrayList<Expression>();
                args_r.add(enablerVarExp.getCopy());
                args_r.add(mVarRef.getCopy());
                List<Expression> args_f = new ArrayList<Expression>();
                args_f.add(enablerVarExp.getCopy());
                args_f.add(mVarRef.getCopy());
                // Compose the expression
                condExpr = new BinaryExpression(
                        new CallExpression(FUNCTION_FALLING, args_f, new TBoolean()), 
                        new CallExpression(FUNCTION_RISING, args_r, new TBoolean()),
                        BinaryOperator.LOGICAL_OR_OPERATOR );
            }
        } else {
        	// TODO add error message
        }
            
        IfStatement ifStmt = new IfStatement(condExpr, null, null);
        // giving a name to the if statement
        ifStmt.setName(portName + " EdgeEnable If-Statement");
        // Add the if-statement to the list of statements
        statements.add(ifStmt);
        // Add statement to update memory value:  memory = %i1;
        Expression updateEnabler = enablerVarExp.getCopy();
        Expression updateMemory  = mVarRef.getCopy();
        if (index >= 0) {
            updateEnabler.addIndexExpression(new IntegerExpression(index));
            updateMemory.addIndexExpression(new IntegerExpression(index));
        }
        statements.add(new AssignStatement(
        		AssignOperator.SIMPLE_ASSIGN,
        		updateMemory, updateEnabler)
        );
        
        return statements;
    }

    /**
     * TODO Reduce the amount of duplicate code in this method
     * 
     * @param enablerVarExp
     * @param mVar
     * @param index
     * @param condExpr
     * @param detectFun
     * @return
     */
    private static Expression getEnableCodeForDetector(
            Expression enablerVarExp, Expression mVarRef, int index,
            Expression condExpr, String detectFun) {
        List<Expression> dims = enablerVarExp.getDataType().getDimensions();
        if (dims.size()>0) {
            
            if (index >= 0) {
                // rising(%i[index], memory[index])
                List<Expression> args = new ArrayList<Expression>();
                Expression eVarExp = enablerVarExp.getCopy();
                Expression mVarExp = mVarRef.getCopy();
                args.add(eVarExp);
                args.add(mVarExp);
                eVarExp.addIndexExpression(new IntegerExpression(index));                    
                mVarExp.addIndexExpression(new IntegerExpression(index));
                
                condExpr = new CallExpression(detectFun, args, new TBoolean());
                
            } else {
            	// rising(%i[0], memory[0]) || rising(%i[1], memory[1]) ...
            	for (int i = 0; i < ((IntegerExpression) dims.get(0)).getIntValue(); i++) {
                    List<Expression> args = new ArrayList<Expression>();
                    Expression eVarExp = enablerVarExp.getCopy();
                    Expression mVarExp = mVarRef.getCopy();
                    args.add(eVarExp);
                    args.add(mVarExp);                    
                    eVarExp.addIndexExpression(new IntegerExpression(i));
                    mVarExp.addIndexExpression(new IntegerExpression(i));
                    
                    if (i==0) {
                    	// First iteration and first call expression
                    	condExpr = new CallExpression(detectFun, args, new TBoolean());
                    } else {
                    	// Disjunction of call expressions
                        condExpr = new BinaryExpression(
                        		condExpr, 
                				new CallExpression(detectFun, args, new TBoolean()),
                				BinaryOperator.LOGICAL_OR_OPERATOR );
                    }
             	}
            }
        } else {
        	// rising(%i1, memory)
        	// Arguments for call expression
        	List<Expression> args = new ArrayList<Expression>();
        	args.add(enablerVarExp.getCopy());
        	args.add(mVarRef.getCopy());
        	condExpr = new CallExpression(detectFun, args, new TBoolean());
        }
        return condExpr;
    }
    
}
