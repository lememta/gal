/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/genericmodel/GAModelElement.java,v $
 *  @version	$Revision: 1.108 $
 *	@date		$Date: 2012-03-08 10:01:49 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.genericmodel;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.NameSpaceElement;
import geneauto.models.gacodemodel.GACodeModelElement;
import geneauto.utils.PrivilegedAccessor;
import geneauto.utils.map.ConstantMap;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Abstract base class for all geneauto language elements
 */
public abstract class GAModelElement {

    /**
     * Unique ID of the model element.
     * 
     * An element gets id when it is inserted in the model. setParent method
     * takes care, that an id is updated when the id from model becomes
     * available
     */
    protected int id;

    /**
     * User defined name of an element (block, port, data element ... ). The
     * name could be mangled or automatically modified. In that case the
     * originalName field contains the original name that was in the input
     * model.
     */
    protected String name;

    /**
     * This field has value only, when the tool has automatically modified the
     * name on an element that was in the input model. This avoids having
     * duplicate names most of the time.
     */
    protected String originalName;

    /**
     * Full name in the original model including the path to the root of the
     * model. This string can be used for finding the element from model based
     * on error message.
     */
    protected String originalFullName;

    /**
     * Reference to the container.
     */
    protected GAModelElement parent;

    /**
     * Comments or other textual descriptions of the element.
     */
    protected List<Annotation> annotations = new ArrayList<Annotation>();

    /**
     * ID of the model element in original source model. In case of stateflow,
     * there will be unique element ID, in case of Simulink block full name etc.
     */
    protected String externalID;

    /*
     * reference to model where the model element belongs
     */
    protected Model model;
    
    /**
     * Forbids name mangling.
     * If this flag is true, the element name must not be changed by the code
     * generator at any stage.
     * 
     * NB! The flag is used to state explicitly, that name mangling is 
     * forbidden. In addition there may be implicit constraints that demand
     * the name to remain unchanged. 
     * See the method isFixedname()   
     */
    protected boolean isFixedName;

    public GAModelElement() {
        
    }

    public GAModelElement(Model m) {
        if (this instanceof GACodeModelElement) {
            setParent(((GACodeModelElement) this).getModule());
        }
        
        setModel(m);
    }

    public String getReferenceString() {
    	return getReferenceString(false);
    }
    /**
     * Returns a string identifying this model element for information purposes.
     * The return value contains full path information.
     * 
     * @param noId if true, object IDs are not printed in annotations
     * 
     * @return a string identifying the model element with path information
     */
    public String getReferenceString(boolean noId) {
        String parentPart = "";

        // originalFullName contains the name and path from the input model
        if (originalFullName != null) {
            return originalFullName;
        }

        // this map is required to ensure that we do not end up in a loop
        // in case of erroneous parent attribute -- getReferenceString
        // is used in error messages, so it can happen
        Map<Integer, GAModelElement> parentIdMap = new HashMap<Integer, GAModelElement>();
        GAModelElement parentEl = getParent();
        while (parentEl != null) {
            if (!parentIdMap.containsKey(parentEl.getId())) {
            	if (parentEl.getOriginalFullName() == null) {
            		// when parent does not have original full name either, 
            		// go recursively up
	                parentPart = parentEl.getShortReferenceString(noId) + "/"
	                        + parentPart;
	                parentIdMap.put(parentEl.getId(), parentEl);
	                parentEl = parentEl.getParent();
            	} else {
            		// of parent has original full name, use it and stop
            		parentPart = parentEl.getOriginalFullName() + "/"
            				+ parentPart;
            		break;
            	}
            } else {
                break;
            }
        }
        return parentPart + getShortReferenceString(noId);
    }

    /**
     * Returns a string identifying this model element for information purposes.
     * Similar to getReferenceString(), but the description is shorter.
     * 
     * @return a string identifying the model element for information purposes.
     */
    public String getShortReferenceString() {
        return getShortReferenceString(false);
    }

    /**
     * Returns a string identifying this model element for information purposes.
     * Similar to getReferenceString(), but the description is shorter.
     * 
     * @param noId
     *            -- when true, no ID is included in the reference string
     * @return a string identifying the model element for information purposes.
     */
    public String getShortReferenceString(boolean noId) {
        String refStr = getOriginalName();
        if (refStr == null) {
            refStr = "";
        }
        refStr = "<" + getClass().getSimpleName() + ": " + "name=" + refStr;
        if (!noId) {
            refStr += ", " + "id=" + getId();
        }
        refStr += ">";
        return refStr;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public String getExternalID() {
        return externalID;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public GAModelElement getParent() {
        return parent;
    }

    public GAModelElement getTopLevelParent() {
        if (parent == null) {
            return this;
        } else {
            return parent.getTopLevelParent();
        }
    }

    public void setExternalID(String externalID) {
        this.externalID = externalID;
    }

    /**
     * If the object does not have an Id yet, then retrieves automatically a
     * fresh Id and assigns it to the element.
     * 
     * @see setNewId()
     */
    public void setId() {
        // we can manipulate ID's only when the model is known
        if (model != null) {
            if (this.id < 1) {
                // set new id when we did not have one before
                this.id = model.getNewId(this);
            } else {
                // if we have an ID, then make sure the model.lastID is aware of
                // it
                model.updateLastId(this);
            }
        }
    }

    /**
     * Assigns a new fresh Id to the element regardless, whether it had an id
     * already or not.
     * 
     * @see setId()
     */
    public void setNewId() {
        // we can manipulate ID's only when the model is known
        if (model != null) {
            this.id = model.getNewId(this);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParent(GAModelElement parent) {
        this.parent = parent;
        // ensure, that the reference to a model is correct
        if (parent != null && model != parent.getModel()) {
            // set the model reference (this will also update ID if necessary)
            setModel(parent.getModel());
        }
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = null; 
        addAnnotations(annotations);
    }

    public void addAnnotations(List<Annotation> annotations) {
        if (annotations != null) {
            for (Annotation a : annotations) {
                addAnnotation(a);
            }
        }
    }

    /**
     * Adds the given annotation to the end of the list of annotations
     * 
     * @param Annotation
     *            annotation
     * 
     *            TODO: this should be actually sorted list. add sorting
     */
    public void addAnnotation(Annotation annotation) {
        if (annotations == null) {
            annotations = new LinkedList<Annotation>();
        }
        if (annotation.getOrdinal() == 0) {
            // ordinal was not set when creating the annotation
            int l_ordinal = annotations.size();
            if (l_ordinal > 0) {
                l_ordinal = annotations.get(l_ordinal - 1).getOrdinal();
            }
            annotation.setOrdinal(l_ordinal + 1);
        }

        annotations.add(annotation);
    }
    
    /**
     * Adds the given annotation to the end of the list of annotations
     * 
     * @param String
     *            annotation text
     * @param isFromInputModel
     *            value of annotation field with the same name
     */
    public void addAnnotation(String annotationText, boolean isFromInputModel) {
        Annotation a = new Annotation(annotationText, false, isFromInputModel);
        addAnnotation(a);
    }
    
    public void addAnnotation(String annotationText) {
        addAnnotation(new Annotation(annotationText));
    }

    /**
     * Method getOriginalName.
     * 
     * When the "originalName" attribute exists, then returns that. If not, then
     * returns the attribute "name". This avoids having duplicate names most of
     * the time. The "originalName" attribute has a value only, if it is
     * different from the "name" attribute.
     * 
     * @return the original name of the model element.
     */
    public String getOriginalName() {
        if (originalName == null || "".equals(originalName)) {
            return name;
        } else {
            return originalName;
        }
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getOriginalFullName() {
        return originalFullName;
    }

    public void setOriginalFullName(String originalFullName) {
        this.originalFullName = originalFullName;
    }
    
    public void setFixedName(boolean isFixedName) {
        this.isFixedName = isFixedName;
    }
    
    /**
     * If this method returns true, the element name must not be 
     * changed by the code generator at any stage.

     * by default the method simply returns the value of the isFixedName flag
     * However, some subclasses of GAModelElement may impose additional 
     * implicit constraints. See overrides of this method.
     */
    public boolean hasFixedName() {
        return this.isFixedName;
    }

    /**
     * Returns list of immediate children of the current element. All elements
     * of type GAModelElement are returned if their parent
     * attribute refers to current element
     * 
     * NB! if the child element happens to contain collection, where each
     * element is another collection and this contained collection contains
     * GAModelElement instances then those are not returned
     * 
     * NB2! caution when modifying or overriding this method. There are other
     * methods in models package that assume that this method does not return
     * duplicate values. Relying on always checking the parent value guarantees
     * this. If an override adds any element without parent check we can not
     * guarantee any more non-duplication
     */
    @SuppressWarnings("unchecked")
    public List<GAModelElement> getChildren() {
        List<GAModelElement> elements = new LinkedList<GAModelElement>();
        Object attrValue;

        // Scan the fields of the given class put all occurrences of owned
        // GAModelElemens to the result list
        for (Field f : PrivilegedAccessor.getClassFields(this.getClass())) {
            try {
                f.setAccessible(true);
                attrValue = f.get(this);

                if (attrValue == null) {
                    continue;
                }

                if (attrValue instanceof GAModelElement) {

                    // attribute value is object of type GAModelElement
                    if (((GAModelElement) attrValue).getParent() == this) {

                        // attribute value is owned element
                        elements.add((GAModelElement) attrValue);
                    }
                } else if (attrValue instanceof Collection) {

                    // attribute value is a collection, retrieve the collection
                    // contents
                    for (Object listEl : (Collection) attrValue) {
                        if (listEl != null
                                && listEl instanceof GAModelElement
                                && ((GAModelElement) listEl).getParent() == this) {

                            // the list element is owned sub-element of the
                            // current class
                            elements.add((GAModelElement) listEl);
                        }
                    }
                }
            } catch (IllegalArgumentException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.getChildren", "",
                        "Unable to access field + " + f.getName()
                                + " of object " + this, e.getMessage());
            } catch (IllegalAccessException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.getChildren", "",
                        "Unable to access field + " + f.getName()
                                + " of object " + this, e.getMessage());
            }
        }

        return elements;
    }

    /*
     * Returns immediate children of current class that are of type c or of its
     * sub-type
     */
    @SuppressWarnings("unchecked")
    public List<GAModelElement> getChildren(Class c) {
        List<GAModelElement> elements = new LinkedList<GAModelElement>();
        for (GAModelElement e : getChildren()) {

            // if returned object can be cast to class specified by c
            // add it to the elements list
            if (c.isInstance(e)) {
                elements.add(e);
            }
        }

        return elements;
    }

    /*
     * Returns all children of current object or of its sub-type. Found elements
     * are added to the end of a list given as elements attribute
     */
    public List<GAModelElement> getAllChildren(List<GAModelElement> elements) {
        for (GAModelElement e : getChildren()) {

            // add element list
            elements.add(e);

            // check all children of the element and add them to the end of
            // elements list
            e.getAllChildren(elements);
        }

        return elements;
    }

    /**
     * Returns all children of current object
     * 
     * Added for backward compatibility and for usage in for statements when
     * required for recursive usage use List<GAModelElement>
     * getAllChildren(List<GAModelElement> elements) instead
     */
    public List<GAModelElement> getAllChildren() {
        List<GAModelElement> elements = new LinkedList<GAModelElement>();
        return getAllChildren(elements);
    }

    /**
     * Returns all children of current object of given class or of its sub-type.
     * When required for recursive usage use List<GAModelElement>
     * getAllChildren(Class c, List<GAModelElement> elements)
     */
    @SuppressWarnings("unchecked")
    public List<GAModelElement> getAllChildren(Class c) {
        List<GAModelElement> elements = new LinkedList<GAModelElement>();
        return getAllChildren(c, elements);
    }

    /**
     * Returns all children of current object that are of type c or of its
     * sub-type. Found elements are added to the end of a list given as elements
     * attribute
     */
    @SuppressWarnings("unchecked")
    public List<GAModelElement> getAllChildren(Class c,
            List<GAModelElement> elements) {
        for (GAModelElement e : getChildren()) {

            // if returned object can be cast to class specified by c
            // add it to the elements list
            if (c.isInstance(e)) {
                elements.add(e);
            }

            // check all children of the element and add them to the end of
            // elements list
            e.getAllChildren(c, elements);
        }

        return elements;
    }

    /**
     * Returns first child of current class that is of type c or of its
     * sub-type.
     * 
     * All children in the subtree are checked in depth-first fashion
     */
    @SuppressWarnings("unchecked")
    public GAModelElement getChildByType(Class c) {
        for (GAModelElement e : getChildren()) {
            // direct descendant is of correct type
            if (c.isInstance(e)) {
                return e;
            }

            // check children of the current child
            e = e.getChildByType(c);
            if (e != null) {
                return e;
            }
        }

        return null;
    }

    /**
     * Returns first child of current class that is of type c or of its sub-type
     * and name equals to name argument
     * 
     * All children in the subtree are checked in depth-first fashion
     */
    @SuppressWarnings("unchecked")
    public GAModelElement getChildByName(String name, Class c) {
        for (GAModelElement e : getChildren()) {

            // direct descendant is of correct type
            if (c.isInstance(e) && name.equals(e.getName())) {
                return e;
            }

            // check children of the current child
            e = e.getChildByName(name, c);
            if (e != null) {
                return e;
            }
        }

        return null;
    }

    /**
     * Returns first child of current class with name equals to name argument
     * 
     * All children in the subtree are checked in depth-first fashion
     */
    public GAModelElement getChildByName(String name) {
        for (GAModelElement e : getChildren()) {

            // direct descendant is of correct type
            if (name.equals(e.getName())) {
                return e;
            }

            // check children of the current child
            e = e.getChildByName(name);
            if (e != null) {
                return e;
            }
        }

        return null;
    }

    /**
     * @return The list of not null owned children that are immediate
     *         descendants and can be referred by other model elements
     */
    public List<GAModelElement> getReferableChildren() {
        List<GAModelElement> lst = new LinkedList<GAModelElement>();
        for (GAModelElement c : getChildren()) {
            if (c.isReferable()) {
                lst.add(c);
            }
        }
        return lst;
    }

    /**
     * Returns the element itself, when it has the id given as argument Returns
     * the child of this model element in case it has the given id Return null
     * when given id is not found in current subtree
     */
    public GAModelElement getChildById(int p_id) {
        GAModelElement result = null;
        if (this.getId() == p_id) {
            return this;
        }
        for (GAModelElement e : getChildren()) {
            result = e.getChildById(p_id);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    /**
     * Adds all the immediate owned children of type childClass that might be
     * referred by-name from other children to the name-element map used by
     * resolveReferences(..).
     * 
     * Note! It is allowed to have a child overriding, (hiding) a higher level
     * node with the same name.
     * 
     * @param final ConstantMap<String, GAModelElement> nameMap
     * @return ConstantMap<String, GAModelElement> a copy of the nameMap that
     *         has been extended with the local named children. The "changed"
     *         flag of the map is false.
     */
    @SuppressWarnings("unchecked")
    private ConstantMap<String, GAModelElement> namedChildrenToMap(
            final ConstantMap<String, GAModelElement> sourceMap,
            Class childClass) {
        String tmpName;

        // Get owned and named referable children
        Collection<GAModelElement> children = getReferableChildren();
        if (!children.isEmpty()) {

            // Add the owned named children to the name map
            Map<String, GAModelElement> childMap = new HashMap<String, GAModelElement>();
            for (GAModelElement c : children) {
                if (childClass.isInstance(c)) {
                    tmpName = c.getName();
                    if (!(tmpName == null || tmpName.isEmpty())) {
                        /*
                         * Check, if there are two identically named children.
                         * If there are and they are actually different objects,
                         * then this is potentially an error. 
                         * The model may contain duplicate names (they will be 
                         * solved in printer) HOwever, such elements shall never
                         * be sought by name.
                         * 
                         * Store null in the map indicating that the name is not
                         * valid for solving named references
                         */
                        if (childMap.containsKey(tmpName)) {
                            if (childMap.get(tmpName) != c) {
                            	childMap.put(tmpName, null);
                                continue;
                            }
                        }
                        childMap.put(tmpName, c);
                    }
                }
            }
            return sourceMap.extend(new ConstantMap<String, GAModelElement>(
                    childMap, false));
        } else {

            // Return the original map.
            return sourceMap;
        }
    }

    /**
     * Moves common attributes from the current GAModelElement to the argument.
     * 
     * Notes:
     * 
     * The source element can be partly destroyed in this process!
     * 
     * Proper implementations should also break the downward references from the
     * source element to any moved child elements, e.g. by calling
     * breakReferences() or setting the corresponding field to null (more
     * efficient)!
     * 
     * See also copyCommonAttributes(...)
     * 
     * @param element
     *            element that gets the common attributes
     */
    public void moveCommonAttributes(GAModelElement element) {
        element.name = this.name;
        element.externalID = this.externalID;
        element.originalName = this.originalName;
        element.annotations.clear();
        element.isFixedName = this.isFixedName;
        for (Annotation a : this.getAnnotations()) {
            element.addAnnotation(a);            
        }
    }
    
    /**
     * Copies common attributes from the current GAModelElement to the argument.
     * 
     * Notes:
     * 
     * Any elements owned by the source class are duplicated first, so the
     * target class contains copies of these.
     * 
     * In case the corresponding fields contain collections, then the existing
     * collection is reset and the collection from the source class is copied.
     * 
     * Because the attributes are copied it is preferable to use
     * moveCommonAttributes(...), when it is ok to partly destroy the source 
     * element!
     * 
     * @param element
     *            element that gets the copied attributes
     */
    public void copyCommonAttributes(GAModelElement element) {
        element.name = this.name;
        element.externalID = this.externalID;
        element.originalName = this.originalName;
        element.annotations.clear();
        element.isFixedName = this.isFixedName;
        for (Annotation a : this.getAnnotations()) {
            element.addAnnotation(a.getCopy());            
        }
    }

    /**
     * Resolves by-name references in the element and all of its children. The
     * mapping of names to higher level elements visible to the particular
     * element is given as argument. The map is of type ConstantMap, to make
     * sure that each child gets the same contents of the nameMap and not the
     * mapping that has been modified by another child.
     * 
     * @param final ConstantMap<String, GAModelElement> nameMap -- a list of
     *        named elements in the tree branch above
     * @param Model
     *            rootModel -- model where the resolve process started this is
     *            required to avoid infinite loops with model references
     */
    public void resolveReferences(
            final ConstantMap<String, GAModelElement> nameMap, Model rootModel) {

        // Add the owned named children to the name map, if there are any.
        ConstantMap<String, GAModelElement> localMap = namedChildrenToMap(
                nameMap, NameSpaceElement.class);

        // Resolves references in the current element
        GAModelElement newElt = resolveCurrentElement(localMap);
        if (newElt != null) {

            // The element itself was changed. Resolve references in the new
            // element
            newElt.resolveReferences(nameMap, rootModel);
        } else {

            // Scan all immediate and owned children and ask them to resolve
            // references
            for (GAModelElement el : getChildren()) {
                el.resolveReferences(localMap, rootModel);
            }
        }
    }

    /**
     * Resolves by-name references in the current element. Typically looks up
     * some name in the supplied table and updates some field of itself.
     * Alternatively, replaces itself in the parent object by some new element
     * with possibly different type. In such cases the reference to the new
     * element must be returned, so that the new element can be processed also.
     * 
     * Called by resolveReferences(..).
     * 
     * @param final ConstantMap<String, GAModelElement> nameMap -- a map with 
     * 				the names that are visible in the scope of the resolved 
     * 				element. The key of the map is element name and value
     * 				is reference to the element. If reference is "null", then
     * 				it means the same name is inserted to the map several times 
     * 				and name resolving process shall raise an error 
     * 				
     * @return reference to the new element in case the current element was
     *         replaced by the new element
     */
    public GAModelElement resolveCurrentElement(
            final ConstantMap<String, GAModelElement> nameMap) {

        // Dummy in the base class
        return null;
    }

    /**
     * Return true if current metamodel element can be referred by other model
     * elements except its parent (i.e. the element has model-wide scope).
     * 
     * by default we assume, that model elements are not referable, when an
     * element has global scope, this method should be overridden
     */
    public boolean isReferable() {
        return false;
    }

	/**
	 * Checks, whether the element belongs to the "elements" collection of a
	 * model. The model, whose elements collection is searched is the model
	 * referred by the current element. If the element's getModel() returns null,
	 * then this check cannot be performed and an error is raised.
	 */
    public boolean isRootLevelElement() {
    	Model m = getModel();
    	if (m == null) {
    		EventHandler.handle(EventLevel.ERROR, "isRootLevelElement()", "", 
    				"Current object's containing model cannot be determined." +
    				"\n Element: " + getReferenceString());
			return true; // Since the error is a raised, the tool will be
						 // stopped later. Returning "true" might be slightly
						 // better than "false" here.
    	}
        return m.getElements().contains(this);
    }

    /**
     * Replaces the element in its parent with a given new element (can be null
     * or a list of elements). The current element must have a parent.
     * 
     * @param Object
     *            newElement - The element is expected to be either of type
     *            GAModelElement or List<GAModelElement>
     */
    public void replaceMe(Object newElement) {
    	replaceMe(newElement, false);
    }    
    
    /**
     * Replaces the element in its parent with a given new element (can be null
     * or a list of elements). The current element must have a parent.
     * 
     * @param Object
     *            newElement - The element is expected to be either of type
     *            GAModelElement or List<GAModelElement>
     * @param force
     * 			  if true, the element is replaced even if it is replaceable
     */
    public void replaceMe(Object newElement, boolean force) {
        if (getParent() != null) {
            getParent().replaceObjectInChildren(this, newElement, force);    
        } else {
            EventHandler.handle(
                    EventLevel.CRITICAL_ERROR, "GAModelElement.replaceMe()", "", 
                    "Cannot replace an orphan element: " + getReferenceString());
        }        
    }    
    
    /**
     * Same as replaceObjectInChildren(oldElement, newElement, false)
     * 
     * @param oldElement
     *            old Element
     * @param Object
     *            newElement - The element is expected to be either of type
     *            GAModelElement or List<GAModelElement>
     */
    public void replaceObjectInChildren(GAModelElement oldElement,
            Object newElement) {
        replaceObjectInChildren(oldElement, newElement, false);
    }

    /**
     * Replaces all occurrences of the given object in the immediate children of
     * the element. Can be used for changing the type of a child.
     * 
     * @param GAModelElement
     *            oldElement
     * @param Object
     *            newElement - The element can be null. Otherwise, it is
     *            expected to be either of type GAModelElement or
     *            List<GAModelElement>
     * @param force
     *            if this flag is true allows to replace referable children
     * 
     */
    @SuppressWarnings("unchecked")
    public void replaceObjectInChildren(GAModelElement oldElement,
            Object newElement, boolean force) {

    	Boolean isFound = false;
        // Check if the child element is referable.
        // Operation only allowed in non-referable elements
        // NOTE: This is purely a design restriction to distinguish objects that
        // are normally referred by other elements and objects that are not.
        // If required, then this check can be omitted.
        if (!force && oldElement.isReferable()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "GAModelElement.replaceObjectInChildren()", "",
                    "Cannot replace children in a referable class: "
                            + getReferenceString(), "");
        }

        // Scan the fields of the given class and check for occurrence
        // of oldElement. Replace, when found.
        for (Field f : PrivilegedAccessor.getClassFields(this.getClass())) {
            try {
                f.setAccessible(true);
                Object value = f.get(this);
                
                // Check, if the field's value matches oldElement                
                if (value == oldElement) {
                    isFound = true;
                    if (newElement != null) {
                        ((GAModelElement) newElement).setParent(this);
                    }
                    f.set(this, newElement);
                    oldElement.breakReferences();                    
                    return;
                    
                // Check, if the current field value is a List 
                // (can be generalised to Collection)
                } else if (value instanceof List) {
                    // Check all of its members
                    int idx = -1;
                    ListIterator<Object> iter = ((List) value).listIterator();
                    while (iter.hasNext()) {
                        Object member = iter.next();
                        if (member == oldElement) {
                            isFound = true;
                            if (newElement == null) {
                                // Remove old element
                                iter.remove();                                
                            } else if (newElement instanceof List && 
                                    !(member instanceof List)) {
                                // If the supplied element is a List and the existing 
                                // element not, then insert the supplied list instead 
                                // of the old element.
                                //
                                // Memorise the position and remove the old element
                                idx = iter.nextIndex() - 1;
                                iter.remove();
                            } else {
                                // Replace element
                                iter.set(newElement);
                            }
                            // Exit the search in the field value list
                            // Some more handling done below
                            break; 
                        }
                    }
                    if (isFound) {
                        if (idx != -1) {
                            // Insert a list of values to the memorised position
                            for (Object o : ((List) newElement)) {
                                ((GAModelElement) o).setParent(this);
                            }
                            ((List) value).addAll(idx, (List) newElement);
                        } else {
                            if (newElement != null) {
                                ((GAModelElement) newElement).setParent(this);
                            }
                        }
                    }
                    if (isFound) {
                        // Break references in the old element and exit 
                        // the search in the object's fields 
                        
                        // do not break references in 
                        // case new element is list that
                        // contains old element
                        boolean breakReferences = true;
                        if (newElement instanceof List) {
                            List list = (List) newElement;
                            if (list.contains(oldElement)) {
                                breakReferences = false;
                            }
                        }
                        if (breakReferences) {
                            oldElement.breakReferences();
                        }
                        return;                        
                    }
                }
            } catch (IllegalArgumentException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.replaceObjectInChildren()", "",
                        "Unable to access field " + f.getName()
                                + "\n of object " + getReferenceString(), 
                        e.getMessage());
            } catch (IllegalAccessException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.replaceObjectInChildren()", "",
                        "Unable to access field " + f.getName()
                                + "\n of object " + getReferenceString(), 
                         e.getMessage());
            }
        }

    }

    /**
     * finds and attribute that points to oldElement and sets this attribute to null
     * if oldElement is contained in a collection removes it
     * finally sets parent of oldElement to null
     * 
     * @param oldElement -- element to be removed
     */
    @SuppressWarnings("unchecked")
	public void removeChild(GAModelElement oldElement) {
    	
    	boolean isFound = false;
    	
        // Scan the fields of the given class and check for occurrence
        // of oldElement. Remove, when found.
        for (Field f : PrivilegedAccessor.getClassFields(this.getClass())) {
            try {
                f.setAccessible(true);
                Object value = f.get(this);
                if (value == oldElement) {
                    f.set(this, null);
                    isFound = true;
                    break;
                } else if (value != null) {
                    // Check, if the current element is a Collection.
                    // If it is, then check all of its members
                    if (value instanceof List) {
                        ListIterator<Object> iter = ((List) value)
                                .listIterator();
                        while (iter.hasNext()) {
                            Object member = iter.next();
                            if (member == oldElement) {
                                iter.remove();
                                isFound = true;
                                break;		// break the while loop
                            }
                        }
                        if (isFound) {
                        	break;			// break for loop
                        }
                    }
                }
            } catch (IllegalArgumentException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.replaceObjectInChildren", "",
                        "Unable to access field " + f.getName()
                                + "\n of object " + getReferenceString(), 
                        e.getMessage());
            } catch (IllegalAccessException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.replaceObjectInChildren", "",
                        "Unable to access field " + f.getName()
                                + "\n of object " + getReferenceString(), 
                         e.getMessage());
            }
        }
        
        if (isFound) {
        	oldElement.breakReferences();
        } else {
            EventHandler.handle(EventLevel.ERROR,
                    "GAModelElement.replaceObjectInChildren", "",
                    "Object " + oldElement.getReferenceString()
                            + "\n is not parent of " + getReferenceString(), 
                     "");        	
        }
    }
    
    /**
     * Removes the element from model by calling parent's
     * removeChild method
     */
    public void removeMe(){
    	// if parent is null, then the element must be already removed
    	if (parent != null) {    		
    		parent.removeChild(this);
    	}
    }

    public Model getModel() {
        return model;
    }

    /**
     * Sets reference to parent model and updates id when the value of model
     * attribute was really changed. When force=true the id is updated
     * regardless of old model attribute value
     */
    public void setModel(Model model, boolean force) {
        // when the model is changed, change the model for all children too
        boolean modelChange = (this.model != null) && (this.model != model);
        if (force || this.model == null || modelChange) {
            this.model = model;
            if (modelChange) {
                setNewId();
            } else {
                setId();
            }
            for (GAModelElement e : getChildren()) {

                // TODO: modify getChildren so that it wont return null elements
                if (e != null) {
                    e.setModel(model);
                }
            }
        }

    }

    /**
     * Sets reference to parent model and updates id when the value of model
     * attribute was really changed.
     */
    public void setModel(Model m) {
        setModel(m, false);
    }

    /**
     * Returns a copy of the object
     * 
     * Copies values of all attributes of non-GAModelElement type In case the
     * parent of GAModelElement attribute is not equal to this (i.e. current
     * object does not own the child) copies pointer to the attribute value In
     * case the current object owns the child (the parent attribute of the child
     * points to this), returns copy of the attribute value
     * 
     */
    public GAModelElement getCopy() {
        GAModelElement e = (GAModelElement) GAModelElement.getCopy(this);
        // reset the original name with path -- it is not relevant any more
        e.setOriginalFullName(null);
        return e;
    }

    /**
     * TODO: move this to PrivilegedAccessor (or remove if decided, that copying
     * anything else than GAModelElement is required
     * 
     * @param copiedObject
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Object getCopy(Object copiedObject) {
        Object duplicatedObject = null;
        Object attrValue = null;
        Object newValue = null;
        String attrName = null;

        duplicatedObject = PrivilegedAccessor
                .getObjectForClassName(copiedObject.getClass().getName());

        // TODO: make annotation GAmodel element or implement exception below
        // for duplicating
        // annotations
        for (Field field : PrivilegedAccessor.getClassFields(copiedObject
                .getClass())) {
            attrName = field.getName();
            if (copiedObject instanceof GAModelElement
                    && (attrName == "id" 
                    	|| attrName == "parent" 
                    	|| attrName == "model")) {

                // we do not copy id, parent and model -- they will be added
                // later by corresponding setters
                continue;
            } else {
                attrValue = PrivilegedAccessor.getValue(copiedObject, attrName);

                if (attrValue == null) {
                    newValue = null;
                } else if (attrValue instanceof GAModelElement) {

                    // attribute value is object of type GAModelElement
                    if (copiedObject instanceof GAModelElement
                            && copiedObject == ((GAModelElement) attrValue)
                                    .getParent()) {

                        // owned element, lets make a copy
                        newValue = ((GAModelElement) attrValue).getCopy();

                        // when creating a new object we must set its parent
                        // it is important to use the setter as this takes also
                        // care of
                        // setting the model
                        ((GAModelElement) newValue)
                                .setParent((GAModelElement) duplicatedObject);
                    } else {
                        newValue = attrValue;
                    }
                } else if (attrValue instanceof Collection) {

                    // attribute value is a collection, we make new list
                    newValue = PrivilegedAccessor
                            .getObjectForClassName(attrValue.getClass()
                                    .getName());

                    for (Object listEl : (Collection) attrValue) {
                        if (listEl != null
                                && copiedObject instanceof GAModelElement
                                && listEl instanceof GAModelElement
                                && ((GAModelElement) listEl).getParent() == copiedObject) {

                            // the list element is owned sub-element of the
                            // current class
                            listEl = ((GAModelElement) listEl).getCopy();
                            ((GAModelElement) listEl)
                                    .setParent((GAModelElement) duplicatedObject);
                            ((Collection) newValue).add(listEl);
                        } else {

                            // the element is simple type. Assign value
                            ((Collection) newValue).add(listEl);
                        }
                    }
                } else {
                    newValue = attrValue;
                }

                // write attribute value to the newly created object
                PrivilegedAccessor.setValue(duplicatedObject, attrName,
                        newValue, true);
            }
        }

        return duplicatedObject;
    }

    // TODO: I would put it to GABlockLibraryElement to make sure the id's are
    // not mangled in other tools
    public void forceId(int newId) {
        id = newId;
    }
    
    /**
     * Removes cross-references from model. To be sued when model element 
     * is removed from model. By default sets parent and model to null
     * if a class has additional specific references, then those must 
     * be reset with an override in this particular class element
     */
    public void breakReferences() {
    	setParent(null);
    	setModel(null);
    }
    
    /**
     * For debugging purposes - returns the name (if present) and id 
     * of the model element
     */
    public String toString() {
        String s = "";
        
        // Distinguish elements that have an upward reference and that don't.
        if (getParent() != null) {
            s += "* ";
        } else {
            s += "o ";
        }
        
    	if (name != null && !name.isEmpty()) {
    		s += name + " ";
    	}
    	s += "id=" + id +" [" + getClass().getSimpleName() + "]";
        if (getExternalID() != null && !getExternalID().isEmpty()) {
            s+= " extId=" + getExternalID();
        }
    	
    	return s;
    }
    
    /**
     * @param e
     * @return true, when the current element is descendant of element e
     */
    public boolean isDecendantOf(GAModelElement e) {
        if (getParent() != null) {
            if (getParent() == e) {
                return true;
            } else {
                return getParent().isDecendantOf(e);
            }
        } else {
            return false;
        }
    }
    
	/**
	 * Check, if the current element and the other element are instances of
	 * exactly same class
	 * 
	 * @param other
	 * @return
	 */
    public boolean equalClass(GAModelElement other) {
        if (other == null) {
            return false;
        }
    	return (this.getClass() == other.getClass());
    }
	
}