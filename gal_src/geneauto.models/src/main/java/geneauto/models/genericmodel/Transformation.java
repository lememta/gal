/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/genericmodel/Transformation.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2010-04-02 06:05:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.genericmodel;

import geneauto.utils.TimeUtils;

import java.util.Date;

/**
 * Class which is used for storing information about the transformations of the
 * model.
 *
 */
public class Transformation {

    private String readTime;

    private String writeTime;

    private String toolName;

	/**
	 * Default constructor. Only to be used for generic model serialisation and
	 * deserialisation. Normally, use constructors with parameters instead
	 */
    @SuppressWarnings("unused")
	private Transformation() {

	}

	/**
     * Constructor with parameters.
     * 
     * @param p_readTime
     *        readTime of the transformation.
     * @param p_writeTime
     *        writeTime of the transformation.
     * @param p_toolName
     *        tool which made the transformation.
     */
    public Transformation(Date p_readTime, Date p_writeTime, String p_toolName) {
        this.readTime = "";
		if (p_readTime != null) {
			this.readTime = TimeUtils.getFormattedDate(p_readTime);
		}
        this.writeTime = "";
		if (p_writeTime != null) {
			this.writeTime = TimeUtils.getFormattedDate(p_writeTime);
		}
        this.toolName = p_toolName;
    }

    /**
     * Constructor with parameters.
     * 
     * @param p_readTime
     *        readTime of the transformation.
     * @param p_writeTime
     *        writeTime of the transformation.
     * @param p_toolName
     *        tool which made the transformation.
     */
    public Transformation(String p_readTime, String p_writeTime, String p_toolName) {
        this.readTime = p_readTime;
        this.writeTime = p_writeTime;
        this.toolName = p_toolName;
    }

    /**
     * Sets the readTime of this transformation.
     */
    public void setReadTime() {
        readTime = TimeUtils.getFormattedDate();
    }

    /**
     * Sets the writeTime of this transformation.
     */
    public void setWriteTime() {
        writeTime = TimeUtils.getFormattedDate();
    }
    
    /**
     * Method getReadTime.
     * @return the attribute readTime.
     */
    public String getReadTime() {
        return readTime;
    }

    
    /**
     * Method getToolName.
     * @return the attribute toolName.
     */
    public String getToolName() {
        return toolName;
    }

    
    /**
     * Method getWriteTime.
     * @return the attribute writeTime.
     */
    public String getWriteTime() {
        return writeTime;
    }
}
