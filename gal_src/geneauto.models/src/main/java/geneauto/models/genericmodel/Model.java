/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/genericmodel/Model.java,v $
 *  @version	$Revision: 1.46 $
 *	@date		$Date: 2011-07-07 12:23:42 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.genericmodel;


import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.TempModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Base class for representing a model. The model type can be : - GACodeModel -
 * BlockLibrary - GASystemModel
 */
public abstract class Model {
	
    /**
     * List of the transformations of the model.
     */
    protected List<Transformation> transformations = new ArrayList<Transformation>();
    
    protected String modelName; 
    
    protected String modelVersion;
    
    protected String lastSavedBy;
    
    protected String lastSavedOn;

	/**
	 * Last id number given to elements of the model.
	 * When adding a new element we should always increment this value.
	 */
	protected int lastId = 0;

	/**
	 * A map of all model elements and their ID's 
	 */
	protected Map<String, GAModelElement> idMap = new HashMap<String, GAModelElement>();

    /**
	 * Determines if model shall assign a new id when an object does not have one.
	 * This attribute is important for reading elements from file. If there happens to be elements
	 * with no ID in file, we do not want to give out any new id's before all elements are read 
	 * from the file.
	 *  
	 */
	protected boolean noNewId = false;
	
	
	public void setNoNewId(boolean p_noNewId) {
		this.noNewId = p_noNewId;
	}

	/**
	 * Returns a string identifying this model for information purposes.
	 * 
	 * @return a string identifying the model
	 */
	public String getReferenceString() {
		return "name=" + modelName + ", type=" + getClass().getSimpleName();
	}
    
	/**
	 * Returns all elements of type GAModelElement in current model.
	 * 
	 * The tree is unfolded depth-first i.e. a tree node is followed by its children 
	 * and after the child list the next node on the same tree level is added. E.g in the 
	 * tree
	 * 	n1
	 * 		n1.1
	 * 		n1.2
	 * 			n1.2.1
	 * 			n1.2.2
	 * 		n1.3
	 *  n2
	 *  	n2.1
	 *  
	 *  Will be serialised as
	 *  	n1, n1.1, n1.2, n1.2.1, n1.2.2, n1.3, n2, n2.1 ...
	 */
	public List<GAModelElement> getAllElements() {		
        // (AnTo) Probably a LinkedList is much better suited here than an ArrayList
        // no need to resize a huge collection and allocate contiguous space.
        List<GAModelElement> resultList = new LinkedList<GAModelElement>();
  
        for (Object o : getElements()){
        	// add the element itself
        	resultList.add((GAModelElement) o);
        	// add all its children
        	// NB! here we assume that getChildren is correct i.e. it can
        	// not return duplicate elements
        	((GAModelElement) o).getAllChildren(resultList);
        }
        
        return resultList;
    }
    
	/**
	 * Returns all elements in this model that are instance of
	 * class c or its subclasses
	 */
    @SuppressWarnings("unchecked")
	public List<GAModelElement> getAllElements(Class c) {
        // (AnTo) Probably a LinkedList is much better suited here than an ArrayList
        // no need to resize a huge collection and allocate contiguous space.
        List<GAModelElement> resultList = new LinkedList<GAModelElement>();
        
        return getAllElements(c, resultList);
    }
    
	/**
	 * Returns all elements in this model that are instance of
	 * class c or its subclasses
	 */
    @SuppressWarnings("unchecked")
	public List<GAModelElement> getAllElements(Class c, List<GAModelElement> resultList) {        
       for (Object o : getElements()){
    	   // add the element itself
    	   if(c.isInstance(o)){
        	   resultList.add((GAModelElement) o);    		   
    	   }
    		    
    	   // add all its children
    	   ((GAModelElement) o).getAllChildren(c, resultList);
       }
       
       return resultList;
    }        
	
	/**
	 * Returns first element in this model that is instance of
	 * class c or its subclasses
	 * 
	 * The model tree is checked in depth-first fashion
	 */
    @SuppressWarnings("unchecked")
	public GAModelElement getElementByType(Class c) {        
       for (Object o : getElements()){
    	   // add the element itself
    	   if(c.isInstance(o)){
        	   return (GAModelElement) o;    		   
    	   }
    		    
    	   GAModelElement child = ((GAModelElement) o).getChildByType(c);
    	   if (child != null){
    		   return child;
    	   }
       }
       
       return null;
    }        

	/**
	 * Returns first element in this model that is instance of
	 * class c or its subclasses and name equals to name argument
	 * 
	 * The model tree is checked in depth-first fashion
	 */
    @SuppressWarnings("unchecked")
	public GAModelElement getElementByName(String name, Class c) {        
       for (Object o : getElements()){
    	   GAModelElement e = (GAModelElement) o;
    	   // add the element itself
    	   if(c.isInstance(e) && name.equals(e.getName())){
        	   return e;    		   
    	   }
    		    
    	   e = e.getChildByName(name, c);
    	   if (e != null){
    		   return e;
    	   }
       }
       
       return null;
    }        

	/**
	 * Returns first element in this model with name equal 
	 * to the name argument
	 * 
	 * The model tree is checked in depth-first fashion
	 */
	public GAModelElement getElementByName(String name) {        
       for (Object o : getElements()){
    	   // add the element itself
    	   GAModelElement e = (GAModelElement) o;
    	   if(name.equals(e.getName())){
        	   return e;    		   
    	   }
    		    
    	   e = e.getChildByName(name);
    	   if (e != null){
    		   return e;
    	   }
       }
       
       return null;
    }        
    
    /*
     * when given ID is found returns corresponding element
     * null otherwise
     */
	public GAModelElement getElementById(int id){
		return idMap.get(id + "");
	}	
	
    /**
     * @return all elements from idMap
     */
    public Collection<GAModelElement> getAllElementByIdMap(){
        return idMap.values();
    }

	/**
	 * Removes all elements of class TempModel from the elements collection.
	 * NOTE! This is a slow method (quadratic complexity) and should be only
	 * used on short lists. E.g for the collections of root objects in the model
	 */
    public void removeTemporaryElements() {
        List<TempModel> tmpElts = new LinkedList<TempModel>();
        List<?> elList = getElements();
        for (Object o : elList) {
            if (o instanceof TempModel) {
                tmpElts.add((TempModel) o);
            }
        }
        for (GAModelElement elt : tmpElts) {            
        	elList.remove(elt);
        }
    }
    
    /**
     * Method getModelVersion.
     * @return the attribute "modelVersion".
     */
    public String getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(String modelVersion) {
        this.modelVersion = modelVersion;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
    
    public String getLastSavedBy() {
        return lastSavedBy;
    }

    public void setLastSavedBy(String lastSB) {
        this.lastSavedBy = lastSB;
    }
    
    public String getLastSavedOn() {
        return lastSavedOn;
    }

    public void setLastSavedOn(String lastSO) {
        this.lastSavedOn = lastSO;
    }

    // Default constructor
    public Model() {
    }

	/**
	 * Return the collection of root elements. They are all assumed to be of
	 * type GAModelElement, but we don't declare it in the method signature,
	 * because this would prevent different Model subclasses from having
	 * different root level collections consisting of different subclasses of
	 * GAModelElement. This is because in Java List<T1> is not a subclass of
	 * List<T>, even if T1 is a subclass of T.
	 */
    public abstract List<?> getElements();

    /** Set root level elements. @see getElements() */    
    public abstract void setElements(List<?> elements);
    
    /**
     * Return all top level elements that are of type c
     */
    @SuppressWarnings("unchecked")
	public List<?> getElements(Class c) {
    	List<Object> resultList = new ArrayList<Object>();
    	
    	for (Object e : getElements()){
    		if (c.isInstance(e)){
    			resultList.add(e);
    		}
    	}
        return resultList;
    }
    
    /** Adds a root level element. @see getElements() */
    public abstract void addElement(Object p_element);

    /** Adds a number of root level elements. @see getElements() */
    public abstract void addElements(List<?> p_elements);

    public List<Transformation> getTransformations() {
        return transformations;
    }

	/**
	 * Method for retrieving a new id number for a new element
	 * NB! the attribute ++lastId is static i.e. when there is different
	 * subclasses of Model, they still share the same instance of lastID 
	 * attribute. This is intended behaviour in current case, not side effect!
	 * 
	 * The object that requested the id will be stored in idMap
	 * 
	 * @return unique id number
	 */
	public int getNewId(GAModelElement reqObj) {
		if (!noNewId){
			lastId ++;
			idMap.put(lastId + "", reqObj);
			return lastId;
		}
		else{
			return 0;
		}	
	}

	/**
	 * Checks, if the id of given object is greater than lastId 
	 * and the id is still free
	 *  
	 * If it is then updates the lastId() 
	 * @param id
	 */
	public void updateLastId(GAModelElement reqObj) {
		int id = reqObj.getId();
		
		if (reqObj.getModel() != this){
			reqObj.setModel(this);
		}
		
		if (id < 1){
			// an object does not have ID yet. Assign one
			if (!noNewId){
				reqObj.setModel(this, true);
			}
			// we do not update the map in case of illegal ID
			// when new id was assigned, it is already registered in map 
			// so we can safely return
			return;
		}
		
		// check if id was in the map
		GAModelElement obj = idMap.get(id + "");
		if (obj == null){
			// given id was free
			if (id > lastId) {
				lastId = id;
			}	
			idMap.put(id + "", reqObj);
		} else if (obj != reqObj){
		    // duplicate ID, raise error
			EventHandler.handle(EventLevel.CRITICAL_ERROR, 
					"Model.upadteLastID",
					"",
					"An attempt to register object with duplicate ID \"" + id + "\""
							+ "\n Existing object: " + obj.getReferenceString()
							+ "\n New object: " + reqObj.getReferenceString()
							+ "\n Model: " + getReferenceString(),
					"");
		}
		else {
			// the same object was registered twice. This is OK
		}
	}
	
	/**
	 * Scans the model, checks all Id-s in the model and updates the (global) lastId 
	 * if required (i.e, if the model contains an Id greater than the lastId).
	 * 
	 * Note: Calling this (lengthy) method can be avoided, when the ModelFactory keeps 
	 * the lastId up to date, when first reading the model and all subsequently created 
	 * Id-d elements do the same, e.g. by calling the setId() method in their constructors.
	 * 
	 */
	public void updateLastId() {
		idMap.clear();
		
    	for (GAModelElement e : getAllElements()) {
    		updateLastId(e);
    	}
	}
	
	/*
	 * @return returns model that is related to this model. 
	 * When model contains GAModelElementReference type items, it uses this method
	 * for retrieving the model where reference targets reside.
	 */
	public abstract Model getDependentModel();
}
