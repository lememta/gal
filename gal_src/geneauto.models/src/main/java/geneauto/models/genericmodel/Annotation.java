/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.models/src/main/java/geneauto/models/genericmodel/Annotation.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2010-10-01 13:41:32 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.models.genericmodel;

/**
 * Annotation is an arbitrary text that can be attached to language element (any
 * GAModelElement). If attribute toPrint is true, the annotation is printed to
 * generated code as comment. Otherwise it simply provides additional
 * information about the model element, but is ignored during the final
 * transformation to source code
 */
public class Annotation {

    /**
     * Text contained in the annotation.
     */
    private String value;
    /**
     * Sequence number of an annotation. This attribute is useful when there is
     * more than one annotation attached to a modelElement.
     */
    private int ordinal;
    /**
     * Reference to annotation source. The string identifies a component that
     * created this annotation. In case source is empty, the annotation
     * originates from the source model. Otherwise the string will identify code
     * generated ' component that created the annotation
     */
    private String source;
    /**
     * If true, the annotation will be included in generated code.
     */
    private boolean toPrint;
    
    private boolean fromInputModel;
    
    /**
     * If true the annotation is traceability link with the source 
	 * block in the corresponding system model
     */
    private boolean isBlockDescription;
    
    /**
     * If true, the annotation is aimed at being used for verification
     * purposes. It must be printed according to source code type.
     */
    private boolean isVerificationAnnotation;
   
    private boolean isPostAnnotation;
    
    /**
     * If true then the annotations aimed at being printed before the 
     * code element.
     */
    private boolean isPrefixOfStatement;
 
    /**
     * Constructor of this class. ord=0, toPrint=true
     * 
     * @param txt
     *            Text contained in the annotation.
     */
    public Annotation(String value) {
        this.value = value;
        this.toPrint = true;
    }

    public Annotation(String value, boolean toPrint) {
        this.value = value;
        this.toPrint = toPrint;
    }
    
    public Annotation(String value, boolean toPrint, 
            boolean fromInputModel) {
        this.value = value;
        this.toPrint = toPrint;
        this.fromInputModel = fromInputModel;
    }

    public Annotation() {
    }

    /**
     * Constructor of this class.
     * 
     * @param ord
     *            Sequence number of an annotation.
     * @param txt
     *            Text contained in the annotation.
     * @param src
     *            Reference to annotation source.
     * @param toprint
     *            True means that the annotation must be included in generated
     *            code.
     */
    public Annotation(int ord, String txt, String src, boolean toprint) {
        value = txt;
        ordinal = ord;
        source = src;
        toPrint = toprint;
    }

    /**
     * Getter of the attribute "ordinal".
     * 
     * @return ordinal.
     */
    public int getOrdinal() {
        return ordinal;
    }

    /**
     * Getter of the attribute "source".
     * 
     * @return source.
     */
    public String getSource() {
        return source;
    }

    /**
     * Getter of the attribute "value".
     * 
     * @return value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Getter of the attribute "toPrint".
     * 
     * @return toPrint.
     */
    public boolean isToPrint() {
        return toPrint;
    }

    public void setValue(String val) {
        this.value = val;
    }

    public void setOrdinal(int ord) {
        this.ordinal = ord;
    }

    public void setSource(String elemSource) {
        this.source = elemSource;
    }

    public void setToPrint(boolean toPrt) {
        this.toPrint = toPrt;
    }

    public Annotation getCopy() {
        Annotation annotation = new Annotation(value, toPrint, fromInputModel);
        annotation.setBlockDescription(isBlockDescription);
        return annotation;
    }
    
    public boolean isFromInputModel() {
        return fromInputModel;
    }

    public void setFromInputModel(boolean fromInputModel) {
        this.fromInputModel = fromInputModel;
    }

    public boolean isBlockDescription() {
        return isBlockDescription;
    }

    public void setBlockDescription(boolean isBlockDescription) {
        this.isBlockDescription = isBlockDescription;
    }

    	public void setVerificationAnnotation(boolean isVerificationAnnotation) {
		this.isVerificationAnnotation = isVerificationAnnotation;
		if (isVerificationAnnotation){
			isBlockDescription = false;
			setToPrint(true);
		}
	}

	public boolean isVerificationAnnotation() {
		return isVerificationAnnotation;
	}

	public void setPrefixOfStatement(boolean isPrefixOfStatement) {
		this.isPrefixOfStatement = isPrefixOfStatement;
	}

	public boolean isPrefixOfStatement() {
		return isPrefixOfStatement;
	}

	public void setPostAnnotation(boolean isPostAnnotation) {
		this.isPostAnnotation = isPostAnnotation;
	}

	public boolean isPostAnnotation() {
		return isPostAnnotation;
	}

}
