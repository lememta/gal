//* Gene-Auto code generator
//* 
//*	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/antlr_grammar/SFLabel.g,v $
//*  @version	$Revision: 1.30 $
//*	@date		$Date: 2009-11-10 08:19:27 $
//*
//*  Copyright (c) 2006-2009 IB Krates OU
//*  	http://www.krates.ee, geneauto@krates.ee
//*  Copyright (c) 2006-2009 Alyotech
//*  	http://www.alyotech.fr, geneauto@alyotech.fr
//*
//*  
//*  This program is free software; you can redistribute it and/or modify
//*  it under the terms of the GNU General Public License as published by
//*  the Free Software Foundation, either version 3 of the License, or
//*  (at your option) any later version.
//*
//*  This program is distributed in the hope that it will be useful,
//*  but WITHOUT ANY WARRANTY; without even the implied warranty of
//*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//*  GNU General Public License for more details.
//*
//*  You should have received a copy of the GNU General Public License
//*  along with this program. If not, see <http://www.gnu.org/licenses/>
//*  
//*  Gene-Auto was originally developed by Gene-Auto consortium with support 
//*  of ITEA (www.itea2-org) and national public funding bodies in France, 
//*  Estonia, Belgium and Israel.  
//*  
//*  The members of Gene-Auto consortium are
//*  	Continental SAS
//*  	Airbus France SAS
//*  	Thales Alenia Space France SAS
//*  	ASTRIUM SAS
//*  	IB KRATES OU
//*  	Barco NV
//*  	Israel Aircraft Industries
//*  	Alyotech
//*  	Tallinn University of Technology
//*  	FERIA - Institut National Polytechnique de Toulouse
//*  	INRIA - Institut National de Recherche en Informatique et en Automatique

// base name for the lexer and parser
grammar EML_;

// generation options
options {
	// the result is an Abstract syntax tree
    	output=AST;
    	// the java type of the result Tree 
    	ASTLabelType=CommonTree; 
}

tokens {
	Array;
	CaseExpr;
	IndexExpr;
	ExpoExpr;
}

@header {
package geneauto.models.utilities.emlconverter.antlr;
}

@lexer::header {
package geneauto.models.utilities.emlconverter.antlr;

// Required for lexer::members netToken()
/* Comment out when debuging in AntLrWorks
*/
import geneauto.eventshandler.EventsHandler;
import geneauto.eventshandler.EventsHandler.EventLevel;
}

@parser::members {
protected void mismatch(IntStream input, int ttype, BitSet follow)
  throws RecognitionException
{
  throw new MismatchedTokenException(ttype, input);
}
}

@lexer::members {
/* Comment to run AntLRWorks debugger
   Uncomment before release
*/
public Token nextToken() {
  while (true) {
   this.token = null;
   this.channel = Token.DEFAULT_CHANNEL;
   this.tokenStartCharIndex = input.index();
   this.tokenStartCharPositionInLine = input.getCharPositionInLine();
   this.tokenStartLine = input.getLine();
   this.text = null;
   if ( input.LA(1)==CharStream.EOF ) {
    return Token.EOF_TOKEN;
   }
   try {
    mTokens();
    if ( this.token==null ) {
     emit();
    }
    else if ( this.token==Token.SKIP_TOKEN ) {
     continue;
    }
    return this.token;
   }
   catch (RecognitionException re) {
    reportError(re);
    EventsHandler.handle( EventLevel.CRITICAL_ERROR, "", "", "Errors while parsing - unable to continue.", "");
   }
  }
 }
}

@rulecatch {
catch (RecognitionException re) {
reportError(re);
throw re;
}
}

parseEMLexpr :	expr | typedExpr ;

typedExpr:	MLTYPE! parenExpr ;
expr     : condExpr|caseExpr ;
caseExpr : OPEN_EMB litExpr (COMMA litExpr)* CLOSE_EMB -> ^(CaseExpr litExpr litExpr*);
condExpr :  sumExpr (OPECOND^ sumExpr)?;
sumExpr  :  prodExpr ((PLUS^|MINUS^) prodExpr)* ;
prodExpr :  primExpr ((MUL^|DIV^) primExpr)* ;
primExpr :	litExpr | parenExpr | funcExpr;
funcExpr :  function^ parenExpr ;
parenExpr:	LPAREN! expr RPAREN! ;
litExpr	 :  array | iExpr | atom;
iExpr 	 :  ((MINUS)? KEY) OPEN_SQU atom CLOSE_SQU -> ^(IndexExpr ^(MINUS KEY) atom);
atom     :  ((MINUS^)? ( INT | EXP | HEX | KEY )) ;   
arrayln
	:	(typedExpr (COMMA? typedExpr)*) -> ^(Array typedExpr+) 
	| 	(atom (COMMA? atom)*) -> ^(Array atom+) 
	|	(array (COMMA? array)*) -> ^(Array array+)
	;
array	 :  OPEN_SQU (arrayln) (SEMICOLON (arrayln))* CLOSE_SQU -> ^(Array arrayln arrayln*);	
function :  TRIGO;

LPAREN 		: 	'(';
RPAREN 		: 	')';
PLUS		: 	'+';
MINUS		: 	'-';
MUL		: 	'*';
DIV		: 	'/';
INT		: 	('0'..'9')+ (DOT! (('0'..'9')+)?)?;
EXP		:	INT (('e'|'E')^ (PLUS|MINUS)? INT)?;
HEX		:	'0' ('x'|'X') ('0'..'9'|'A'..'F'|'a'..'f')+;
DOT		: 	'.';
OPECOND		:	('>'|'<'|'>='|'<='|'==');
TRIGO		:	('cos'|'sin');
MLTYPE		:	('int8'|'int16'|'int32'|'uint8'|'uint16'|'uint32'|'single'|'double'|'boolean');
OPEN_EMB	:	'{';
CLOSE_EMB	:	'}';
COMMA		:	',';
SEMICOLON	:	';';
OPEN_SQU	:	'[';
CLOSE_SQU	:	']';
KEY		:	(LETTER|UNDERSC) (LETTER|UNDERSC|DOT|INT)*;	
fragment
LETTER		: 	'A'..'Z'|'a'..'z';
fragment
UNDERSC		: 	'_';	
WS		:	(' '|'\t')+ {skip();};
