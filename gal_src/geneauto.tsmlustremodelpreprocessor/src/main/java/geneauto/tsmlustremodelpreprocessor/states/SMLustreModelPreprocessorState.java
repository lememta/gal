/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/FMPreprocessorState.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2009-11-10 09:58:55 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmlustremodelpreprocessor.states;

import geneauto.models.gacodemodel.GACodeModel;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.statemachine.State;
import geneauto.tsmlustremodelpreprocessor.main.SMLustreModelPreprocessorTool;


/**
 * Abstract class which represents a state of the FMPreprocessor state machine.
 * 
 *
 */
public abstract class SMLustreModelPreprocessorState extends State{
    /**
     * The state machine to which this state is linked.
     */
    protected SMLustreModelPreprocessorTool stateMachine;
    
    /**
     * 
     * System model on which each state will work.
     * 
     */
    public GASystemModel gaSystemModel;
    
    /**
     * 
     * Code model on which each state will work.
     * 
     */
    public GACodeModel gaCodeModel;
    
    /**
     * Constructor of this class. 
     * @param machine
     * The state machine to which this state is linked.
     */
    public SMLustreModelPreprocessorState(SMLustreModelPreprocessorTool machine, String stateName) {
    	super(stateName);
    	stateMachine = machine;
    }

	public SMLustreModelPreprocessorTool getMachine() {
		return stateMachine;
	}
}
