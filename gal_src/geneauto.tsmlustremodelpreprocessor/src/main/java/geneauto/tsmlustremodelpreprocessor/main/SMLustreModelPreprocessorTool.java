/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/main/FMPreprocessorTool.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-11-28 22:44:11 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmlustremodelpreprocessor.main;

import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.statemachine.StateMachine;
import geneauto.tsmlustremodelpreprocessor.states.SMLustreModelPreprocessorState;
import geneauto.tsmlustremodelpreprocessor.states.InitializingState;
import geneauto.tsmlustremodelpreprocessor.states.MultiInputBlocksToTwoInputsBlockState;
import geneauto.tsmlustremodelpreprocessor.states.SavingModelState;
import geneauto.utils.FileUtil;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * State machine of the FMPreprocessor. It takes as an input a GASystemModel,
 * and performs several processing on it. It provides in output a SystemModel
 * file and a CodeModel file. This class is a singleton. As there is only one
 * typer allowed to run at the same time, this class is a singleton.
 * 
 */
public class SMLustreModelPreprocessorTool extends StateMachine {

    /**
     * Unique instance of this class.
     */
    private static SMLustreModelPreprocessorTool instance;

    // Ordered list of the possible states of the FMPreprocessor.

    /**
     * Initialises the tool according to the given parameters.
     */
    private SMLustreModelPreprocessorState initializingState;

    /**
     * Replace goto and from blocks.
     */
    private SMLustreModelPreprocessorState refactoringMultiInputBlocksState;

    /**
     * Saves the model into the output directory, using the model factory.
     */
    private SMLustreModelPreprocessorState savingModelState;

    /**
     * Model read by the SystemModelFactory. It contains all of the
     * GAModelElements which will need to be typed.
     */
    private GASystemModel systemModel;

    /**
     * Library factory which will help to type the model.
     */
    private GABlockLibrary blockLibrary;

    /**
     * Factory which is used to read and write the model in the xml file.
     */
    private SystemModelFactory systemModelFactory;

    /**
     * Factory which is used to read and write the model in the library file.
     */
    private BlockLibraryFactory blockLibraryFactory;
    
    private Map<String,GAModelElement> namesMap = new HashMap<String, GAModelElement>();

    /**
     * Private constructor of this class. Instantiates all of the states of the
     * state machine. Sets the current State to initializingState.
     */
    private SMLustreModelPreprocessorTool() {
        super("DEV", ""); // TODO: put version number here

        String toolClassPath = "geneauto/tfmpreprocessor/main/FMPreprocessorTool.class";
        URL url = this.getClass().getClassLoader().getResource(toolClassPath);

        String version = FileUtil.getVersionFromManifest(url, toolClassPath);
        String releaseDate = FileUtil.getDateFromManifest(url, toolClassPath);

        if (version != null) {
            setVerString(version);
        }
        if (releaseDate != null) {
            setReleaseDate(releaseDate);
        }

        initializingState = new InitializingState(this);
        
        refactoringMultiInputBlocksState = new MultiInputBlocksToTwoInputsBlockState(this);

        savingModelState = new SavingModelState(this);
    }

    /**
     * Unique way to access the typer.
     * 
     * @return the unique instance of this class.
     */
    public static SMLustreModelPreprocessorTool getInstance() {
        if (instance == null) {
            instance = new SMLustreModelPreprocessorTool();
        }
        return instance;
    }

    /**
     * @return the blockLibrary
     */
    public GABlockLibrary getBlockLibrary() {
        return blockLibrary;
    }

    /**
     * @return the blockLibraryFactory
     */
    public ModelFactory getBlockLibraryFactory() {
        return blockLibraryFactory;
    }

    /**
     * @return the systemModel
     */
    public GASystemModel getSystemModel() {
        return systemModel;
    }

    /**
     * @return the systemModelFactory
     */
    public SystemModelFactory getSystemModelFactory() {
        return systemModelFactory;
    }

    /**
     * Initialises the FMPreprocessor tool.
     * 
     * @param arguments
     */
    public void init() {
        setState(initializingState);
    }

    /**
     * @param blockLibrary
     *            the blockLibrary to set
     */
    public void setBlockLibrary(GABlockLibrary blockLibrary) {
        this.blockLibrary = blockLibrary;
    }

    /**
     * @param blockLibraryFactory
     *            the blockLibraryFactory to set
     */
    public void setBlockLibraryFactory(BlockLibraryFactory blockLibraryFactory) {
        this.blockLibraryFactory = blockLibraryFactory;
    }

    /**
     * @param systemModel
     *            the systemModel to set
     */
    public void setSystemModel(GASystemModel systemModel) {
        this.systemModel = systemModel;
    }

    /**
     * @param systemModelFactory
     *            the systemModelFactory to set
     */
    public void setSystemModelFactory(SystemModelFactory systemModelFactory) {
        this.systemModelFactory = systemModelFactory;
    }

    /**
     * @return the initializingState
     */
    public SMLustreModelPreprocessorState getInitializingState() {
        return initializingState;
    }

    /**
     * @return the savingModelState
     */
    public SMLustreModelPreprocessorState getSavingModelState() {
        return savingModelState;
    }

    public SMLustreModelPreprocessorState getRefactoringMultiInputBlocksState() {
        return refactoringMultiInputBlocksState;
    }

    public Map<String, GAModelElement> getNamesMap() {
        return namesMap;
    }

    public void setNamesMap(Map<String, GAModelElement> namesMap) {
        this.namesMap = namesMap;
    }

}
