
*******************************************************************************

                        Gene-Auto EMF/Ecore Readme
                            
*******************************************************************************

                            www.geneauto.org
                           geneauto.krates.ee

This readme contains following sections:

    * What are Gene-Auto and Gene-Auto Eclipse tools?
    * Who develops them?
    * What licence do they have?
    * Package contents
    * Where to find more information?


----------------------------------------------------------------------
    What are Gene-Auto and Gene-Auto Eclipse tools?
----------------------------------------------------------------------

- Gene-Auto

Gene-Auto is an open-source toolset for converting Simulink, Stateflow 
and Scicos models to executable program code. Output to C code is currently
available, Ada output is under development. The toolset development 
conforms with the DO-178B/ED-12B standards and aims to be qualified to 
be used in the safety critical domain.

- Gene-Auto Eclipse tools

The Gene-Auto base toolset is primarily intended to be a qualifiable embedded 
code generation toolchain. It is implemented in core Java using only a minimal
set of external libraries. Gene-Auto Eclipse tools provide complementary 
functionalities to work with models using Eclipse based tooling.

-- Gene-Auto EMF Ecore

The Eclipse Modeling Framework (EMF) is a framework within Eclipse that provides
facilities for (meta)modelling, code generation, tool building etc, based on a 
structured data model. Ecore is the central metamodelling language (a dialect of 
UML) of the EMF. This package contains the Gene-Auto Ecore-based metamodel and 
related resources.

-- Gene-Auto EMF Model Converters

This package contains tools that allow importing/exporting Gene-Auto models to 
the EMF/Ecore-based format and to use standard Eclipse-based tooling to:

  * export Gene-Auto models to other languages and tools to perform 
    additional verification and validation tasks, optimisation etc
  * import models from other languages to perform code generation with
    Gene-Auto.

- Gene-Auto Eclipse UI tools

This package contains the Gene-Auto System Model editor that has been generated 
from the Gene-Auto EMF/Ecore metamodel and a separate plugin geneauto.eclipse.menus 
that allows to use the Gene-Auto code generator and the Gene-Auto EMF model 
conversion tools directly from Eclipse.


----------------------------------------------------------------------
    Who develops them?
----------------------------------------------------------------------

Gene-Auto was originally developed by the Gene-Auto consortium with 
support from ITEA (www.itea2.org) and national public funding bodies 
in France, Estonia, Belgium and Israel. It is currently developed
further within several other projects. For more information about the 
Gene-Auto toolset see the Gene-Auto ReadMe and the webpages.

The Gene-Auto Ecore model and EMF toolset are developed by FeRIA/IRIT
- INPT/University of Toulouse, Institute of Cybernetics at Tallinn 
University of Technology and IB Krates OU. 

The copyright of the different components belongs to their respective 
developers.


----------------------------------------------------------------------
    What licence do they have?
----------------------------------------------------------------------

The Gene-Auto EMF toolset is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published 
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>


----------------------------------------------------------------------
    Package contents
----------------------------------------------------------------------

- geneauto.ecore
-----------------
Gene-Auto Ecore metamodel. Generated from the Enterprise Architect UML model

- geneauto.genmodel
--------------------
EMF Generator model used to import the main Ecore model from UML and to create 
the Java implementation of the metamodel (geneauto.emf.models) and the standard 
editors (geneauto.emf.edit and geneauto.emf.editor)

Optionally it can be used to export the Ecore model back to a (different) UML 
model and generate the XSD of the Gene-Auto Ecore file format.

NOTE: See also Generation_notes.txt

- geneauto.uml
---------------
UML model exported from the Ecore model GeneAuto.ecore by using GeneAuto.genmodel.

- geneauto.ecorediag
---------------------
Diagram of the Ecore model. Generated from geneauto.ecore. The layout has been 
slightly manually adjusted.


NOTE: In the snapshot releases and in the repository the automatically generated 
files might not necessarily be in sync with the latest versions of the respective 
input models. However, they should be always up-to-date in the standard releases.


----------------------------------------------------------------------
    Where to find more information?
----------------------------------------------------------------------

You can find more technical or background information and the latest 
toolset releases on two websites:
                www.geneauto.org and geneauto.krates.ee
                
There is a lot of useful information in the Gene-Auto FAQ:
                 http://geneauto.krates.ee/faq
    
You are also welcome to contact the Gene-Auto team through the web-sites
to share your experience, ask for new features and/or contribute to 
the development. We hope you will find Gene-Auto useful!

                        The Gene-Auto team