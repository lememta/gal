/**
 * 
 */


import geneauto.emf.utilities.GAEMFConst;
import geneauto.emf.validator.main.EMFValidator;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.io.File;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Test with explicitly specified output file name and 
 * expected negative result
 */
public abstract class ExplicitOutputNegMainTest extends TestCase {
	
	protected String inputRoot   = "src/test/output/";
	
	protected String modelName;
	
	protected String oclFiles;
	
	protected String inputFileName;
	
	private File testOutputFile;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		// Copy the input file(s) to the project's local folder 
		String fromPath = 
			"../geneauto.emf.ecore2gaxml/src/test/output/"
			+ modelName + "_ga/" + modelName + "_" + GAConst.SUFFIX_SLIM + "." + GAEMFConst.EXT_GAESM;
		inputFileName = 
				inputRoot
				+ modelName + "_ga/" + modelName + "_" + GAConst.SUFFIX_SLIM + "." + GAEMFConst.EXT_GAESM;
		FileUtil.copy(fromPath, inputFileName);

		// Test output file
		String testOutputFileName = 
				inputRoot 
				+ modelName + "_ga/" + modelName + "_" + "errors.txt";
		testOutputFile = new File(testOutputFileName);
		// Delete the test output file, if existing
		if (testOutputFile.exists()) {
			testOutputFile.delete();
		}
	}

	/**
	 * Test method for {@link geneauto.emf.validator.main.EMFValidator#main(java.lang.String[])}.
	 * @Test
	 */
	public void testMain() {

		String[] args = new String[5];
		
		args[0] = inputFileName;
		args[1] = "--ocl";
		args[2] = "src/test/constraints/" + "sm_basic_neg.ocl";
		args[3] = "-o";
		args[4] = testOutputFile.getPath();
		
		EMFValidator.main(args);
		
		// Check that the output file does - some constraints have failed
		Assert.assertTrue(testOutputFile.exists());
	}
}
