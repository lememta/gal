/**
 * 
 */

import geneauto.emf.validator.main.EMFValidator;
import junit.framework.TestCase;

/** Same as BasicMainTest, but without setup phase.
 * NOTE: This means the referenced models need to be generated 
 * beforehand and EACH TEST INSTANCE SHOULD DOCUMENT a way to 
 * update them.
 */
public abstract class DirectMainTest extends TestCase {
	
	protected String inputRoot   = "src/test/models/";
	
	protected String oclFiles;
	
	protected String inputFileName;
	
	/**
	 * Test method for {@link geneauto.emf.validator.main.EMFValidator#main(java.lang.String[])}.
	 * @Test
	 */
	public void testMain() {
		String[] args = new String[3];
		
		args[0] = inputFileName;
		args[1] = "--ocl";
		args[2] = oclFiles;
		EMFValidator.main(args);
		
	}
}
