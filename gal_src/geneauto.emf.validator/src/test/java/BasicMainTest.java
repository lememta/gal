/**
 * 
 */

import geneauto.emf.utilities.GAEMFConst;
import geneauto.emf.validator.main.EMFValidator;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;
import junit.framework.TestCase;

/**
 */
public abstract class BasicMainTest extends TestCase {
	
	// We are using the test output folder of the Ecore2GAXML tool
	// During automated build this tool is built and tested before,
	// hence the resources should be existing
	protected String inputRoot   = "src/test/output/";
	
	protected String modelName;

	protected String oclFiles;
	
	private String inputFileName;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();

		// Copy the input file(s) to the project's local folder 
		String fromPath = 
				"../geneauto.emf.ecore2gaxml/src/test/output/"
				+ modelName + "_ga/tmp/" + "1-" + modelName + "_" + GAConst.SUFFIX_SLIM + "." + GAEMFConst.EXT_GAESM;
		inputFileName = 
				inputRoot
				+ modelName + "_ga/tmp/" + "1-" + modelName + "_" + GAConst.SUFFIX_SLIM + "." + GAEMFConst.EXT_GAESM;
		FileUtil.copy(fromPath, inputFileName);
	}

	/**
	 * Test method for {@link geneauto.emf.validator.main.EMFValidator#main(java.lang.String[])}.
	 * @Test
	 */
	public void testMain() {
		String[] args = new String[3];
		
		args[0] = inputFileName;
		args[1] = "--ocl";
		args[2] = oclFiles;
		EMFValidator.main(args);
		
	}
}
