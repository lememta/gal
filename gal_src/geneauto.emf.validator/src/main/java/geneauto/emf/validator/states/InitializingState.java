/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.validator.states;

import geneauto.emf.utilities.Debug;
import geneauto.emf.validator.main.GAEMFValidatorConst;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.io.File;
import java.io.IOException;

/**
 * First state which is executed by the tool. It checks the java parameters,
 * imports the model from the xml file, reads the block library, initialises the
 * whole tool.
 * 
 */
public class InitializingState extends EMFValidatorState {

    public InitializingState() {
        // set state name for messages
        super("Initializing");
    }

    /**
     * Reads the Model.
     */
    public void stateExecute() {
    	
        // Get parameters.
        String inputFile  = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);
        String oclFilePar = ArgumentReader.getParameter(GAEMFValidatorConst.ARG_OCL_FILES);
        String outputFile = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFILE);
        
        String stDbgLev     = ArgumentReader.getParameter(GAEMFValidatorConst.ARG_DEBUG_LEVEL);
        
        if (stDbgLev != null) {
        	int iDbgLev;
        	try {
        		iDbgLev = Integer.parseInt(stDbgLev);
        	} catch (NumberFormatException e) {
        		EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
        				"Value of the debug level argument must be an integer!");
        		return;
        	}
			Debug.setDebugLevel(iDbgLev);
        }
        
        // Split ocl file list
        String[] oclFiles = oclFilePar.split(";");        

        // If output file name not given
        if (outputFile == null) {
			// replace the default GASystemModel extension with the GAEcore
			// GASystemModel extension
        	outputFile = FileUtil.setFileExtension(inputFile, GAEMFValidatorConst.EXT_ERROR_REPORT);
        }

		// Canonicalise filenames for better readability. This is not strictly
		// necessary, but makes the output more readable
        try {
			inputFile = (new File(inputFile)).getCanonicalPath();
		} catch (IOException e) {
			// Do nothing, continue with the uncanonicalised name
		}
		
		for (int i = 0; i < oclFiles.length; i++) {
	        try {
				oclFiles[i] = (new File(oclFiles[i])).getCanonicalPath();
			} catch (IOException e) {
				// Do nothing, continue with the uncanonicalised name
			}
		}
		
        try {
        	outputFile = (new File(outputFile)).getCanonicalPath();
		} catch (IOException e) {
			// Do nothing, continue with the uncanonicalised name
		}
        
        // check for read/write permissions
        FileUtil.assertCanRead(inputFile);
		for (String oclFile : oclFiles) {
			FileUtil.assertCanRead(oclFile);
		}
        FileUtil.assertCanWrite(outputFile);

        // Delete the old output file, if exists
    	File outF = new File(outputFile);
    	if (outF.exists()) {
    		EventHandler.handle(EventLevel.INFO, "", "", 
    				"Deleting existing file with the same name as new error report\n File: " + outputFile);
    		if (!outF.delete()) {
    			EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
    				"Cannot delete existing file, before writing new output.\n File: " + outputFile);
    		}
    	}
    	
    	// Store the file references under the machine
    	getMachine().setInputFileName(inputFile);
    	getMachine().setOclFileNames(oclFiles);
    	getMachine().setOutputFileName(outputFile);
		
        // Go to next state 
        getMachine().setState(new ReadingModelState());
    }
}
