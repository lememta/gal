/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.validator.states;

import geneauto.emf.utilities.EcoreUtils;
import geneauto.emf.utilities.ocl.OCLValidator;
import geneauto.utils.tuple.Triple;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.ecore.Constraint;


/**
  */
public class ValidatingOCLState extends EMFValidatorState {

    public ValidatingOCLState() {
        // set state name for messages
        super("Validating model");
    }

    /** Validate the model against OCL constraints from file */
    public void stateExecute() {
    	
    	EObject model = getMachine().getModel();
    	
    	List<EObject> elems = EcoreUtils.getAllElements(model);
    	
    	OCLValidator validator = getMachine().getOclValidator();
    	
    	List<Triple<String, EObject, List<Constraint>>> failures =
    		getMachine().getFailures();
    	
    	if (failures == null) {
    		failures = new LinkedList<Triple<String,EObject,List<Constraint>>>();
    	}
    	
    	for (EObject elem : elems) {    		
    		List<Constraint> failedConstraints = validator.validateObj(elem);
    		if (failedConstraints != null && !failedConstraints.isEmpty()) {
    			failures.add(
    					// Short name of OCL file, element and failed constraints
    					new Triple<String, EObject, List<Constraint>>(
    							(new File(getMachine().getCurrentOclFile())).getName(), 
    							elem, failedConstraints));
    		}
    	}
    	
    	// Store failures under machine only if some constraints really failed
    	if (!failures.isEmpty()) {
    		getMachine().setFailures(failures);
    	}
   	
        // Go to next state
    	if (!getMachine().oclFilesOver()) {
    		getMachine().setState(new ReadingOCLState());
    	} else {
    		getMachine().setState(new SavingModelState());
    	}
    }
}
