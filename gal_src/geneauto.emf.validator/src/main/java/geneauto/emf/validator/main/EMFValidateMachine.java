/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.validator.main;

import geneauto.emf.utilities.ocl.OCLValidator;
import geneauto.emf.validator.states.InitializingState;
import geneauto.statemachine.StateMachine;
import geneauto.utils.tuple.Triple;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.ocl.ecore.Constraint;

public class EMFValidateMachine extends StateMachine {

	/** Unique instance of this class. */
	protected static EMFValidateMachine instance;
	
	// Instance fields
	
	/** Input file name */
	private String inputFileName;
	
	/** Array of OCL file names */
	private String[] oclFileNames;
	
	/** Output file name */
	private String outputFileName;
	
	/** Current OCL file pointer */
	private int oclFilePtr;
	
	/** Reference to the package that implements the metamodel. */
	private EPackage ePackage;
	
	/** Reference to the handled model object. */
	private EObject model;

	/** Reference to an OCLValidator instance */
	private OCLValidator oclValidator;
	
	/** List of : Ocl file name, failed element and respective contraints */
	private List<Triple<String, EObject, List<Constraint>>> failures;

    public String getInputFileName() {
		return inputFileName;
	}

	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	public String[] getOclFileNames() {
		return oclFileNames;
	}

	public void setOclFileNames(String[] oclFileNames) {
		this.oclFileNames = oclFileNames;
		this.oclFilePtr = 0;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

	public EPackage getPackage() {
		return ePackage;
	}

	public void setPackage(EPackage ePack) {
		this.ePackage = ePack;
	}

	public EObject getModel() {
		return model;
	}

	public void setModel(EObject model) {
		this.model = model;
	}

	public OCLValidator getOclValidator() {
		return oclValidator;
	}

	public void setOclValidator(OCLValidator oclValidator) {
		this.oclValidator = oclValidator;
	}

    public List<Triple<String, EObject, List<Constraint>>> getFailures() {
		return failures;
	}

	public void setFailures(List<Triple<String, EObject, List<Constraint>>> failures) {
		this.failures = failures;
	}
	
	public String getCurrentOclFile() {
		return oclFileNames[oclFilePtr-1];
	}

	public String getNextOclFile() {
		if (oclFilePtr < oclFileNames.length) {
			return oclFileNames[oclFilePtr++];
		} else {
			return null;
		}
	}

	public boolean oclFilesOver() {
		return oclFilePtr >= oclFileNames.length;
	}

    /**
     * Private constructor of this class. Initialises the state machine
     */
    private EMFValidateMachine() {
    	// set the version number of the elementary tool machine
    	super(GAEMFValidatorConst.TOOL_VER, "");

        String version 	   = GAEMFValidatorConst.TOOL_VER;  
        String releaseDate = GAEMFValidatorConst.TOOL_DATE; 
        
        if (version!=null) {
            setVerString(version);
        }
        if (releaseDate!=null) {
            setReleaseDate(releaseDate);
        }
        
		// Set initial state
		initialState = new InitializingState();
    }

	public static EMFValidateMachine getInstance(){
    	if (instance == null){
    		instance = new EMFValidateMachine();
    	}
    	return instance;
    }

	/** Creates a new static instance of the class */
	public static EMFValidateMachine newInstance() {
		instance = new EMFValidateMachine();
    	return instance;
    }

}