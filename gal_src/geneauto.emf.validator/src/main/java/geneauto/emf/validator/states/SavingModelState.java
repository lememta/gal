/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.validator.states;

import geneauto.emf.utilities.EcoreUtils;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.tuple.Triple;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.ecore.Constraint;



public class SavingModelState extends EMFValidatorState {
	public SavingModelState(){
		// set state name for messages
		super("Saving Validation Result");
	}

	public void stateExecute() {
    	
    	// Check, if any errors have occurred so far
    	if (EventHandler.getAnErrorHasOccured()) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
				"Errors have occured during the tool's execution. Output will not be written.");
    	}
    	
    	List<Triple<String, EObject, List<Constraint>>> failures = getMachine().getFailures();
    	if (failures == null || failures.isEmpty()) {
    		// Info message
    		EventHandler.handle(EventLevel.INFO, "", "",
    				"Validation succeeded. Report file will not be written.");
    	} else {
    		// Info message
    		EventHandler.handle(EventLevel.INFO, "", "",
    				"Writing validation result to file:\n "
    						+ getMachine().getOutputFileName());
    		
    		BufferedWriter bufferedWriter = null;
            
            try {
                bufferedWriter = new BufferedWriter(
                		new FileWriter(getMachine().getOutputFileName()));
                
        		for (Triple<String, EObject, List<Constraint>> f : failures) {
        			String oclFileName				   = f.getLeft();
        			EObject obj 					   = f.getMiddle();
        			List<Constraint> failedConstraints = f.getRight();
        			// Write element reference
                    bufferedWriter.write("\n-----------------------------");
                    bufferedWriter.newLine();
                    bufferedWriter.write(EcoreUtils.getSimpleRefStr(obj));
                    bufferedWriter.newLine();
        			// Write names of failed constraints
                    bufferedWriter.write("\nFailed constraints:");
                    bufferedWriter.newLine();
        			for (Constraint c : failedConstraints) {
                        bufferedWriter.write("\t" + oclFileName + " " + c.getName());
                        bufferedWriter.newLine();
        			}
        		}
                bufferedWriter.write("\n-----------------------------");
                
            } catch (Exception e) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
                		"Error saving result", e);

            } 
            // Close the BufferedWriter
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            } catch (Exception e) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
                		"Error saving result", e);
            }
    	}
    	
        // This is the last state of the statemachine. Stop the machine
        getMachine().stop();
	}
}
