/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.validator.main;

public interface GAEMFValidatorConst {

    // Tool name
    public final static String TOOL_NAME = "GAEMFValidate";

    // version number and date
    // NB! These must be updated each time the release is compiled
    public final static String TOOL_VER = "1.0.0";
    public final static String TOOL_DATE = "05/05/2011";

	public static final String EXT_ERROR_REPORT = "validation_errors.txt";
	
	public static final String ARG_OCL_FILES = "oclFiles";

	public static final String ARG_OCL_FILE_NAME_ROOT = "oclFileRoot";

	public static final String ARG_DEBUG_LEVEL = "debugLevel";
	
}
