package geneauto.gaesm2lustre.launcher;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.m2m.atl.core.IModel;

public class GAESM2LustreFolder {

	private static List<File> gaesmFiles;
	private static File modelsFolder;

	private static String excludeFLFlag = "--no-fl";
	public static Boolean doLibraryGeneration = true;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		gaesmFiles = new ArrayList<File>();
		
		parseArgs(args);
		
		gaesmFiles = findFiles(modelsFolder, ".gaesm");
		
		for (File model : gaesmFiles) {
			String[] command = {model.getAbsolutePath(), doLibraryGeneration ? "--fl" : ""};
			GAESM2Lustre.main(command);
		}
	}
	
	private static void parseArgs(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals(excludeFLFlag)){
				doLibraryGeneration = false;
			} else {
				modelsFolder = new File(args[i]);
			}
		}
	}
	
	public static List<File> findFiles(File modelsFolder, String extension){
		List<File> lstResult = new ArrayList<File>();
		if (modelsFolder != null) {
			for (final File fileEntry : modelsFolder.listFiles()) {
		        if (fileEntry.isDirectory()) {
		        	findFiles(fileEntry,extension);
		        } else {
		            String filePath = fileEntry.getAbsolutePath();
		            if (filePath.endsWith(extension)){
		            	lstResult.add(fileEntry);
		            }
		        }
		    }
		}
		return lstResult;
	}
}
