package geneauto.gaesm2lustre.launcher;

import geneauto.emf.transformationconfiguration.BooleanParameter;
import geneauto.emf.transformationconfiguration.Parameter;
import geneauto.emf.transformationconfiguration.TransformationConfiguration;
import geneauto.emf.transformationconfiguration.TransformationconfigurationFactory;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.xtext.LustreRuntimeModule;
import geneauto.xtext.LustreStandaloneSetup;
import geneauto.xtext.lustre.LustreFactory;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.Program;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.atl.common.ATLLaunchConstants;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFExtractor;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFReferenceModel;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.impl.CompositeNode;
import org.eclipse.xtext.nodemodel.impl.CompositeNodeWithSemanticElement;
import org.eclipse.xtext.parsetree.reconstr.ICommentAssociater;
import org.eclipse.xtext.parsetree.reconstr.impl.DefaultCommentAssociater;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.serializer.impl.Serializer;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class GAESM2Lustre {

	// Development mode generation constants
	private static String GENEAUTO_MM_PATH = "../geneauto.emf.ecore/geneauto.ecore";
	private static String LUSTRE_MM_PATH = "../geneauto.xtext.lustre/src-gen/geneauto/xtext/Lustre.ecore";
	private static String CONFIGURATION_MM_PATH = "../geneauto.atl.gaesm2lustre/model/TransformationConfiguration.ecore";
	private static String TRANSFO_PATH = "../geneauto.atl.gaesm2lustre/transformations/";
	private static String UTILS_TRANSFO_PATH = TRANSFO_PATH + "utils.asm";
	private static String MAIN_TRANSFO_PATH = "../geneauto.atl.gaesm2lustre/transformations/gaesm2luse.asm";
	
	// Deployment mode generation constants
//	private static String GENEAUTO_MM_PATH = "geneauto.ecore";
//	private static String LUSTRE_MM_PATH = "Lustre.ecore";
//	private static String TRANSFO_PATH = "";
//	private static String UTILS_TRANSFO_PATH = TRANSFO_PATH + "utils.asm";
//	private static String MAIN_TRANSFO_PATH = "gaesm2luse.asm";
//	private static String CONFIGURATION_MM_PATH = "TransformationConfiguration.ecore";
	
	private static String TOOL_GAESM2Lustre_NAME = "GAESM2Lustre";
	private static String TOOL_GAESM2LustreXMI_NAME = "GAESM2LustreXMI";
	private static String TOOL_GAESM2LustreFunctionLibraryXMI_NAME = "GAESM2LustreFunctionLibraryXMI";
	private static String TOOL_LustreXMI2Lustre_NAME = "LustreXMI2Lustre";
	private static String TOOL_LustreFLXMI2LustreFL_NAME = "LustreFLXMI2LustreFL";
	private static String TOOL_LustreLookup2LustreText_NAME = "LustreLookupXMI2LustreLookupText";
	private static String TOOL_LustreMetrics_NAME = "LustreMetrics";
	
	private static String TOOL_RELEASE_VERSION = "r550";
	
	private static String excludeFLFlag = "--no-fl";
	private static String noLibraryOpenFlag = "--no-fl-open";
	private static String excludeTraceAnnotations = "--no-trace";
	private static String nonLinearMultFlag = "--non-linear";
	
	public static List<String> libraries = Arrays.asList("math","array");
	public static String functionLibraryPostFix = "FunctionsLibrary.lusxmi";
	public static String lookupTableNodesFileName = "LookupFunctions.xmi";
	
	public static HashMap<String,String> mapOutFiles;
	public static HashMap<String, IModel> mapFLModels;
	
	public static String inputFile;
	public static String baseFolder;
	
	public static Boolean doLibraryGeneration = true;
	public static Boolean doLibraryOpenGeneration = true;
	public static Boolean doTraceAnnotationGeneration = true;
	public static Boolean doNonLinearMultPrinting = false;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length > 3){
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					TOOL_GAESM2LustreXMI_NAME,
					"",
					"Number of arguments does not match");
		}else{
			parseArgs(args);
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreXMI_NAME,
					"Selected file",
					inputFile);
			
			mapOutFiles = new HashMap<String, String>();
			mapFLModels = new HashMap<String, IModel>();
			
			EventHandler.startTimer(TOOL_GAESM2Lustre_NAME);
			
			if (doLibraryGeneration){
				for (String flName : libraries) {
					runFunctionLibraryGeneration(flName);
					runFunctionLibraryPrinting(flName);
				}
			} else {
				for (String flName : libraries) {
					baseFolder = inputFile.substring(0, inputFile.lastIndexOf('/')+1);
					String outputFLFile = baseFolder + flName + functionLibraryPostFix;
					mapOutFiles.put(flName, outputFLFile);
				}
			}
			
			runGAESM2LusXMI();
			
			runLusXMI2Lus();
			
			//runLookupXMI2Lus();
			
			EventHandler.handle(EventLevel.INFO, 
	    			TOOL_GAESM2Lustre_NAME,
	        		"",
	        		"GAESM to Lustre Toolset finished processing at "+
	        		EventHandler.getDateTimeForMessage() + ", execution time " +
	        			EventHandler.getDuration(TOOL_GAESM2Lustre_NAME, 
	        					true) 
	        			+ "\n-------------------------------------\n\n", 
	        		"" );
			
			runMetricsPrinter();

		}
	}

	private static void parseArgs(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (!args[i].equals("")){
				if (args[i].equals(excludeFLFlag)){
					doLibraryGeneration = false;
				} else if (args[i].equals(excludeTraceAnnotations)){
					doTraceAnnotationGeneration = false;
				} else if (args[i].equals(noLibraryOpenFlag)){
					doLibraryOpenGeneration = false;
				} else if (args[i].equals(nonLinearMultFlag)){
					doNonLinearMultPrinting = true;
				} else {
					inputFile = args[i];
				}
			}
		}
	}

	/**
	 * Serializes the FunctionLibrary '_FL.lusxmi' model at 'outputFLFile' as a '.lusi' file
	 */
	private static void runFunctionLibraryPrinting(String name) {
		EventHandler.startTimer(TOOL_LustreFLXMI2LustreFL_NAME);
		
		EventHandler.handle(
				EventLevel.INFO, 
				TOOL_LustreFLXMI2LustreFL_NAME,
				"Init",
				"Initialization");
		
    	EPackage.Registry.INSTANCE.put(LustrePackage.eNS_URI, LustrePackage.eINSTANCE);
    	Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("lus", LustreFactory.eINSTANCE);
    	
		URI xtextFLURI = URI.createURI(mapOutFiles.get(name));

	    XtextResourceSet resourceSet = new XtextResourceSet();

	    // Create new resource for this file
	    Resource xtextFLResource = resourceSet.createResource(xtextFLURI);
	    
	    // The new resource exists
	    if (xtextFLResource != null){
		    // Add options to resolve all proxies
		    Map<String,Boolean> options = new HashMap<String,Boolean>();
		    options.put(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		    
		    // Try to load the file
		    try {
		    	xtextFLResource.load(options);
				
				EventHandler.handle(
						EventLevel.INFO, 
						TOOL_LustreFLXMI2LustreFL_NAME,
						"",
						xtextFLURI.toString() + " Ressource loaded");
				
				EObject lusInstance = xtextFLResource.getContents().get(0);
			    
				if (lusInstance != null){
				
					String outputFilePath = xtextFLURI.trimFileExtension().appendFileExtension("lusi").toString();
					
					EventHandler.handle(
							EventLevel.INFO, 
							TOOL_LustreFLXMI2LustreFL_NAME,
							"Saving file",
							outputFilePath);
					
				    Injector injector = Guice.createInjector(new LustreRuntimeModule());  
				    Serializer serializer = injector.getInstance(Serializer.class);
				    serializer.serialize(lusInstance,
				    		new BufferedWriter(new FileWriter(outputFilePath)),
				    		SaveOptions.defaultOptions());

				}else{
					EventHandler.handle(
							EventLevel.CRITICAL_ERROR, 
							TOOL_LustreFLXMI2LustreFL_NAME,
							"",
							"No root element on the provided XML based file");
				}
		    } catch (IOException e1) {
				e1.printStackTrace();
			}
	    }else{
	    	EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					TOOL_LustreFLXMI2LustreFL_NAME,
					"Ressource loading",
					"Impossible to load ressources in this file");
	    }
	    
	    String message = "Completed at ";
    	
    	EventHandler.handle(EventLevel.INFO, 
    			TOOL_LustreFLXMI2LustreFL_NAME,
        		"Timing", 
        		message + EventHandler.getDateTimeForMessage() 
        			+ " (execution time " 
        			+ EventHandler.getDuration(TOOL_LustreFLXMI2LustreFL_NAME, 
        					true) 
        			+ ")\n-------------------------------------\n\n", 
        		"" );
	}

	/**
	 * Generates a '_FL.lusxmi' from the input file provided in 'outputFile'
	 * Saves the full path to this _FL.lusxmi file to 'outputFLFile'
	 */
	private static void runFunctionLibraryGeneration(String name) {
		try {
    		
    		EventHandler.startTimer(TOOL_GAESM2LustreFunctionLibraryXMI_NAME);
    		
    		EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreFunctionLibraryXMI_NAME,
					"Init",
					"Initializing ATL transformation factories");
			/*
			* Initializations
			*/
			ILauncher transformationLauncher = new EMFVMLauncher();
			ModelFactory modelFactory = new EMFModelFactory();
			IInjector injector = new EMFInjector();
			IExtractor extractor = new EMFExtractor();
			
			/*
			* Load metamodels
			*/
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreFunctionLibraryXMI_NAME,
					"Init",
					"Loading MetaModels");
			
			IReferenceModel geneautoMM = modelFactory.newReferenceModel();
			IReferenceModel lustreMM = modelFactory.newReferenceModel();
			
			injector.inject(geneautoMM, GENEAUTO_MM_PATH);
			injector.inject(lustreMM, LUSTRE_MM_PATH);
			
			/*
			 * Load Models
			 */
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreFunctionLibraryXMI_NAME,
					"Init",
					"Loading input model");
			
			// TODO: (ArDi) create an empty geneautoModel to gain performance in this step
			IModel geneautoModel = modelFactory.newModel(geneautoMM);
			injector.inject(geneautoModel, inputFile); // TEME: GAESM model
			IModel lustreModel = modelFactory.newModel(lustreMM); // TEME: Output model
			
			/*
			* Run "gaesm2luse" transformation
			*/
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreFunctionLibraryXMI_NAME,
					"Transformation",
					"Launching libraries generation");
			
			InputStream lib = null;
			try {
				lib = new File(UTILS_TRANSFO_PATH).toURI().toURL().openStream();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// TEME: Launch transformation
			transformationLauncher.initialize(new HashMap<String,Object>());
			transformationLauncher.addInModel(geneautoModel, "IN", "geneauto");
			transformationLauncher.addOutModel(lustreModel, "OUT", "lustre");
			transformationLauncher.addLibrary("utils", lib);
			
			transformationLauncher.launch(
					ILauncher.RUN_MODE, 
					new NullProgressMonitor(), 
					new HashMap<String,Object>(),
					new FileInputStream(TRANSFO_PATH + name + "FunctionsLibrary.asm"));
			
			String outputFLFile = inputFile.substring(0, inputFile.lastIndexOf('/')+1);
			outputFLFile = outputFLFile + name + functionLibraryPostFix;
			
			mapOutFiles.put(name, outputFLFile);
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreFunctionLibraryXMI_NAME,
					"Saving file",
					outputFLFile);
			
			// TEME : Extract the output of the transformation
			extractor.extract(lustreModel, outputFLFile);
			
			/*
			* Unload all models and metamodels (EMF-specific)
			*/
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreFunctionLibraryXMI_NAME,
					"",
					"Unloading ressources");
			
			EMFModelFactory emfModelFactory = (EMFModelFactory) modelFactory;
			emfModelFactory.unload((EMFReferenceModel) geneautoMM);
			emfModelFactory.unload((EMFReferenceModel) lustreMM);
	
		} catch (ATLCoreException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
    	String message = "Completed at ";
    	
    	EventHandler.handle(EventLevel.INFO, 
    			TOOL_GAESM2LustreFunctionLibraryXMI_NAME,
        		"Timing", 
        		message + EventHandler.getDateTimeForMessage() 
        			+ " (execution time " 
        			+ EventHandler.getDuration(TOOL_GAESM2LustreFunctionLibraryXMI_NAME, 
        					true) 
        			+ ")\n-------------------------------------\n\n", 
        		"" );
	}

	/**
	 * Generates a '.lusxmi' file from the input file provided in 'outputFile'
	 * Saves the full path to this '.lusxmi' file to 'outputFile'
	 */
	private static void runGAESM2LusXMI() {
    	try {
    		
    		EventHandler.startTimer(TOOL_GAESM2LustreXMI_NAME);
    		
    		EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreXMI_NAME,
					"Init",
					"Initializing ATL transformation factories");
			/*
			* Initializations
			*/
			ILauncher transformationLauncher = new EMFVMLauncher();
			ModelFactory modelFactory = new EMFModelFactory();
			IInjector injector = new EMFInjector();
			IExtractor extractor = new EMFExtractor();
			
			/*
			 * Set generation options
			 */
			HashMap<String,Object> options = new HashMap<String, Object>();
			options.put("allowInterModelReferences","true");
			
			/*
			* Load metamodels
			*/
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreXMI_NAME,
					"Init",
					"Loading MetaModels");
			
			
			
			IReferenceModel geneautoMM = modelFactory.newReferenceModel();
			IReferenceModel lustreMM = modelFactory.newReferenceModel();
			IReferenceModel configurationMM = modelFactory.newReferenceModel();
			
			injector.inject(geneautoMM, GENEAUTO_MM_PATH, options);
			injector.inject(lustreMM, LUSTRE_MM_PATH, options);
			injector.inject(configurationMM, CONFIGURATION_MM_PATH, options);
			
			/*
			 * Load Models
			 */
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreXMI_NAME,
					"Init",
					"Loading input model");
			
			IModel geneautoModel = modelFactory.newModel(geneautoMM);
			injector.inject(geneautoModel, inputFile, options);
			
			IModel lustreModel = modelFactory.newModel(lustreMM);
			
			for (String flName : libraries) {
				IModel functionsLibraryModel = modelFactory.newModel(lustreMM);
				injector.inject(functionsLibraryModel, mapOutFiles.get(flName), options);
				mapFLModels.put(flName, functionsLibraryModel);
			}
			
			// Create configuration
			IModel configurationModel = modelFactory.newModel(configurationMM);
			
			TransformationconfigurationFactory configFactory = TransformationconfigurationFactory.eINSTANCE;
			TransformationConfiguration config = configFactory.createTransformationConfiguration();
			BooleanParameter traceParameter = configFactory.createBooleanParameter();
			traceParameter.setValue(doTraceAnnotationGeneration);
			traceParameter.setName("PrintTrace");
			config.getParameters().add(traceParameter);
			
			BooleanParameter generateOpenParameter = configFactory.createBooleanParameter();
			generateOpenParameter.setValue(doLibraryOpenGeneration);
			generateOpenParameter.setName("useOpen");
			config.getParameters().add(generateOpenParameter);
			
			BooleanParameter generateNonLinearMultParameter = configFactory.createBooleanParameter();
			generateNonLinearMultParameter.setValue(doNonLinearMultPrinting);
			generateNonLinearMultParameter.setName("setNonLinearMult");
			config.getParameters().add(generateNonLinearMultParameter);
			
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		    Map<String, Object> m = reg.getExtensionToFactoryMap();
		    m.put("config", new XMIResourceFactoryImpl());

		    // Obtain a new resource set
		    ResourceSet resSet = new ResourceSetImpl();

		    // create a resource
		    Resource configRes = resSet.createResource(URI
		        .createURI(URI.createFileURI(inputFile).trimSegments(1).appendSegment("transformationconfiguration.xmi").toFileString()));
		    
		    configRes.getContents().add(config);
		    
		    try {
				configRes.save(options);
			    injector.inject(configurationModel, configRes.getURI().toString(), options);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		    
		    /**
		     * Load lookupFunctions / Removed for now
		     */
//		    IModel lookupFunctionsModel = modelFactory.newModel(lustreMM);
//		    // Obtain a new resource set
//		    ResourceSet lookupResSet = new ResourceSetImpl();
//
//		    // create a resource
//		    Resource lookupRes = lookupResSet.createResource(URI
//		        .createURI(URI.createFileURI(inputFile).trimSegments(1).appendSegment(lookupTableNodesFileName).toFileString()));
//			
//		    try {
//		    	lookupRes.load(options);
//			    injector.inject(lookupFunctionsModel, lookupRes.getURI().toString(), options);
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
		    
			/*
			* Run "gaesm2luse" transformation
			*/
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreXMI_NAME,
					"Transformation",
					"Launching transformation");
			
			InputStream lib = null;
			try {
				lib = new File(UTILS_TRANSFO_PATH).toURI().toURL().openStream();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			transformationLauncher.initialize(new HashMap<String,Object>());
			transformationLauncher.addInModel(geneautoModel, "IN", "geneauto");
			
			for (String flName : libraries) {
				transformationLauncher.addInModel(mapFLModels.get(flName), flName + "FunLibIn", "lustre");
			}
			
//			transformationLauncher.addInModel(lookupFunctionsModel, "lookupNodes", "lustre");
			
			transformationLauncher.addInModel(configurationModel, "config", "configuration");
			
			transformationLauncher.addOutModel(lustreModel, "OUT", "lustre");
			transformationLauncher.addLibrary("utils", lib);
			
			transformationLauncher.launch(
					ILauncher.RUN_MODE, 
					new NullProgressMonitor(), 
					options,
					new FileInputStream(MAIN_TRANSFO_PATH));
			
			
			String mainXMIOutputFile = inputFile.substring(0, inputFile.lastIndexOf('.')) + ".lusxmi";
			
			mapOutFiles.put("mainXMI", mainXMIOutputFile);
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreXMI_NAME,
					"Saving file",
					mainXMIOutputFile);
			
			extractor.extract(lustreModel, mainXMIOutputFile, options);
	
			/*
			* Unload all models and metamodels (EMF-specific)
			*/
			
			EventHandler.handle(
					EventLevel.INFO, 
					TOOL_GAESM2LustreXMI_NAME,
					"",
					"Unloading ressources");
			
			EMFModelFactory emfModelFactory = (EMFModelFactory) modelFactory;
			emfModelFactory.unload((EMFReferenceModel) geneautoMM);
			emfModelFactory.unload((EMFReferenceModel) lustreMM);
	
		} catch (ATLCoreException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
    	String message = "Completed at ";
    	
    	EventHandler.handle(EventLevel.INFO, 
    			TOOL_GAESM2LustreXMI_NAME,
        		"Timing", 
        		message + EventHandler.getDateTimeForMessage() 
        			+ " (execution time " 
        			+ EventHandler.getDuration(TOOL_GAESM2LustreXMI_NAME, 
        					true) 
        			+ ")\n-------------------------------------\n\n", 
        		"" );
    }
	
	/**
	 * Serializes the Lustre program '.lusxmi' model at 'outputFile' as a '.lus' file
	 */
	private static void runLusXMI2Lus() {
    	
		EventHandler.startTimer(TOOL_LustreXMI2Lustre_NAME);
		
		EventHandler.handle(
				EventLevel.INFO, 
				TOOL_LustreXMI2Lustre_NAME,
				"Init",
				"Initialization");
		
    	EPackage.Registry.INSTANCE.put(LustrePackage.eNS_URI, LustrePackage.eINSTANCE);
    	Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("lus", LustreFactory.eINSTANCE);
    	
		URI lustreModelURI = URI.createURI(mapOutFiles.get("mainXMI"));

	    XtextResourceSet resourceSet = new XtextResourceSet();
	    resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);

	    // Create new resource for this file
	    //Resource xtextFunctionLibResource = resourceSet.getResource(funLibModelURI, true);
	    Resource xtextResource = resourceSet.getResource(lustreModelURI, true);
	    
	    // The new resource exists
	    if (xtextResource != null){
		    // Add options to resolve all dependencies
		    Map<Object, Object> options = resourceSet.getLoadOptions();
		    BufferedWriter writer = null;
		    
		    // Try to load the file
		    try {
		    	FileInputStream fis = new FileInputStream(mapOutFiles.get("mainXMI"));
				xtextResource.load(options);
				
				EventHandler.handle(
						EventLevel.INFO, 
						TOOL_LustreXMI2Lustre_NAME,
						"",
						"Ressource loaded");
				
				EObject lusInstance = xtextResource.getContents().get(0);
			    
				if (lusInstance != null && lusInstance instanceof Program){
					
					Program lusProg = (Program) lusInstance;
					
				    Injector injector = Guice.createInjector(new LustreRuntimeModule());
				    Serializer serializer = injector.getInstance(Serializer.class);
				    
				    String mainOutputFile = lustreModelURI.trimFileExtension().appendFileExtension("lus").toString();
				    mapOutFiles.put("main", mainOutputFile);
				    
				    EventHandler.handle(
							EventLevel.INFO, 
							TOOL_LustreXMI2Lustre_NAME,
							"Saving file",
							mainOutputFile);
				    
				    String fileContent = serializer.serialize(lusInstance);
				    String copyright = "-- File generated using Simulink2Lustre " + TOOL_RELEASE_VERSION +"\n";
				    copyright += "-- More informations at https://cavale.enseeiht.fr/redmine/projects/cristalcavegem\n";
				    
				    copyright += "--Trace FROMFILE : " + lustreModelURI.trimFileExtension().appendFileExtension("mdl").lastSegment() + " ;\n";
				    copyright += "--Trace FROMFILE_SHA256 : " + getSHAForFile(inputFile) + " ;\n\n";
				    
				    writer = new BufferedWriter(new FileWriter(lustreModelURI.trimFileExtension().appendFileExtension("lus").toString()));
				    writer.write(copyright + fileContent);
				    writer.close();
//				    
//				    serializer.serialize(lusInstance,
//				    		new BufferedWriter(new FileWriter(lustreModelURI.trimFileExtension().appendFileExtension("lus").toString())),
//				    		SaveOptions.defaultOptions());
				    
				}else{
					EventHandler.handle(
							EventLevel.CRITICAL_ERROR, 
							TOOL_LustreXMI2Lustre_NAME,
							"",
							"No root element on the provided XML based file");
				}
		    } catch (IOException e1) {
				e1.printStackTrace();
			}
	    }else{
	    	EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					TOOL_LustreXMI2Lustre_NAME,
					"Ressource loading",
					"Impossible to load ressources in this file");
	    }
	    
	    String message = "Completed at ";
    	
    	EventHandler.handle(EventLevel.INFO, 
    			TOOL_LustreXMI2Lustre_NAME,
        		"Timing", 
        		message + EventHandler.getDateTimeForMessage() 
        			+ " (execution time " 
        			+ EventHandler.getDuration(TOOL_LustreXMI2Lustre_NAME, 
        					true) 
        			+ ")\n-------------------------------------\n\n", 
        		"" );
    }
	
	/**
	 * Serializes the Lookup program '.xmi' model as a '.lus' file
	 */
	private static void runLookupXMI2Lus() {
    	
		EventHandler.startTimer(TOOL_LustreLookup2LustreText_NAME);
		
		EventHandler.handle(
				EventLevel.INFO, 
				TOOL_LustreLookup2LustreText_NAME,
				"Init",
				"Initialization");
		
    	EPackage.Registry.INSTANCE.put(LustrePackage.eNS_URI, LustrePackage.eINSTANCE);
    	Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("lus", LustreFactory.eINSTANCE);
    	
		URI lustreModelURI = URI.createURI(mapOutFiles.get("mainXMI"));
		lustreModelURI = lustreModelURI.trimSegments(1).appendSegment("LookupFunctions.xmi");
		
	    XtextResourceSet resourceSet = new XtextResourceSet();
	    resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);

	    // Create new resource for this file
	    Resource xtextResource = resourceSet.getResource(lustreModelURI, true);
	    
	    // The new resource exists
	    if (xtextResource != null){
		    // Add options to resolve all dependencies
		    Map<Object, Object> options = resourceSet.getLoadOptions();
		    BufferedWriter writer = null;
		    
		    // Try to load the file
		    try {
		    	FileInputStream fis = new FileInputStream(lustreModelURI.toString());
				xtextResource.load(options);
				
				EventHandler.handle(
						EventLevel.INFO, 
						TOOL_LustreLookup2LustreText_NAME,
						"",
						"Ressource loaded");
				
				EObject lusInstance = xtextResource.getContents().get(0);
			    
				if (lusInstance != null && lusInstance instanceof Program){
					
					Program lusProg = (Program) lusInstance;
					
				    Injector injector = Guice.createInjector(new LustreRuntimeModule());
				    Serializer serializer = injector.getInstance(Serializer.class);
				    
				    String mainOutputFile = lustreModelURI.trimFileExtension().appendFileExtension("lus").toString();
				    
				    EventHandler.handle(
							EventLevel.INFO, 
							TOOL_LustreLookup2LustreText_NAME,
							"Saving file",
							mainOutputFile);
				    
				    String fileContent = serializer.serialize(lusInstance);
				    
				    writer = new BufferedWriter(new FileWriter(lustreModelURI.trimFileExtension().appendFileExtension("lus").toString()));
				    writer.write(fileContent);
				    writer.close();
//				    
//				    serializer.serialize(lusInstance,
//				    		new BufferedWriter(new FileWriter(lustreModelURI.trimFileExtension().appendFileExtension("lus").toString())),
//				    		SaveOptions.defaultOptions());
				    
				}else{
					EventHandler.handle(
							EventLevel.CRITICAL_ERROR, 
							TOOL_LustreLookup2LustreText_NAME,
							"",
							"No root element on the provided XML based file");
				}
		    } catch (IOException e1) {
				e1.printStackTrace();
			}
	    }else{
	    	EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					TOOL_LustreLookup2LustreText_NAME,
					"Ressource loading",
					"Impossible to load ressources in this file");
	    }
	    
	    String message = "Completed at ";
    	
    	EventHandler.handle(EventLevel.INFO, 
    			TOOL_LustreLookup2LustreText_NAME,
        		"Timing", 
        		message + EventHandler.getDateTimeForMessage() 
        			+ " (execution time " 
        			+ EventHandler.getDuration(TOOL_LustreXMI2Lustre_NAME, 
        					true) 
        			+ ")\n-------------------------------------\n\n", 
        		"" );
    }
	
	/**
	 * Prints some metrics on the generated Lustre model:
	 * - Number of nodes
	 * - Number of global declarations
	 */
	private static void runMetricsPrinter() {
    	
		EventHandler.startTimer(TOOL_LustreMetrics_NAME);
		
		EventHandler.handle(
				EventLevel.INFO, 
				TOOL_LustreMetrics_NAME,
				"Init",
				"Initialization");
		
    	EPackage.Registry.INSTANCE.put(LustrePackage.eNS_URI, LustrePackage.eINSTANCE);
    	Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("lus", LustreFactory.eINSTANCE);
    	
		URI xtextURI = URI.createURI(mapOutFiles.get("mainXMI"));

	    XtextResourceSet resourceSet = new XtextResourceSet();

	    // Create new resource for this file
	    Resource xtextResource = resourceSet.createResource(xtextURI);
	    
	    // The new resource exists
	    if (xtextResource != null){
		    // Add options to resolve all proxies
		    Map<String,Boolean> options = new HashMap<String,Boolean>();
		    options.put(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		    
		    // Try to load the file
		    try {
				xtextResource.load(options);
				
				EventHandler.handle(
						EventLevel.INFO, 
						TOOL_LustreMetrics_NAME,
						"",
						"Ressource loaded");
				
				EObject lusInstance = xtextResource.getContents().get(0);
				
				if (lusInstance != null){
				
				    if (lusInstance instanceof Program){
				    	
				    	EventHandler.handle(
								EventLevel.INFO, 
								TOOL_LustreMetrics_NAME,
								"Calculating metrics on model",
								xtextURI.trimFileExtension().appendFileExtension("lus").toString());
				    	
				    	Program prog = (Program) lusInstance;
				    	int nbNodes = prog.getNodes().size();
				    	int nbDeclarations = prog.getDeclarations().size();
				    	
				    	EventHandler.handle(
								EventLevel.INFO, 
								TOOL_LustreMetrics_NAME,
								"Metrics",
								"\nNode count : " + nbNodes + "\n" +
								"Declarations count : " + nbDeclarations);
				    	
				    } else {
					    EventHandler.handle(
								EventLevel.ERROR, 
								TOOL_LustreMetrics_NAME,
								"Impossible to load a Lustre program from input file",
								xtextURI.trimFileExtension().appendFileExtension("lus").toString());
				    }
				}else{
					EventHandler.handle(
							EventLevel.CRITICAL_ERROR, 
							TOOL_LustreMetrics_NAME,
							"",
							"No root element on the provided XML based file");
				}
		    } catch (IOException e1) {
				e1.printStackTrace();
			}
	    }else{
	    	EventHandler.handle(
					EventLevel.CRITICAL_ERROR, 
					TOOL_LustreMetrics_NAME,
					"Ressource loading",
					"Impossible to load ressources in this file");
	    }
	    
	    String message = "Completed at ";
    	
    	EventHandler.handle(EventLevel.INFO, 
    			TOOL_LustreMetrics_NAME,
        		"Timing", 
        		message + EventHandler.getDateTimeForMessage() 
        			+ " (execution time " 
        			+ EventHandler.getDuration(TOOL_LustreMetrics_NAME, 
        					true) 
        			+ ")\n-------------------------------------\n\n", 
        		"" );
    }

	/**
	 * Calculates SHA256 CheckSum for a file
	 * Code taken from: http://www.mkyong.com/java/java-sha-hashing-example/
	 * @param filePath The full path of the file
	 * @return SHA26 checksum for the file
	 */
	private static String getSHAForFile(String filePath){
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			FileInputStream fis = new FileInputStream(filePath);
			byte[] dataBytes = new byte[1024];
			int nread = 0;
			while ((nread = fis.read(dataBytes)) != -1) {
				 md.update(dataBytes, 0, nread);
			}
			byte[] mdbytes = md.digest();
			StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < mdbytes.length; i++) {
	          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        return sb.toString();
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}