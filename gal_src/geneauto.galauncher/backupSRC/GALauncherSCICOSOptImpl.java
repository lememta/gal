/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncherSCICOSOptImpl.java,v $
 *  @version	$Revision: 1.1 $
 *	@date		$Date: 2010-11-25 13:59:52 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;

import geneauto.utils.FileUtil;


/**
 * Gene-Auto launcher starting code generation from GASystemModel XML file
 * Does not include stateflow tools.
 *
 */
public class GALauncherSCICOSOptImpl extends GALauncherImpl{
	
    /**
     * Executes all tools in predefined order
     * Override this method if you need to change execution order of tools
     */
    public void runTools() {
    	
    	// make sure TFMPreprocessor has its input in correct location
    	prepareInput();
    	
        // 2 TFMPreProcessor
    	runTFMPreProcessor();
    	
        // 5 TBlockSequencer
     	runTBlockSequencer();
    	
        // 6 TTyper
    	runTTyper();
    	
        // 7 TFMCodeModelGenerator
    	runTFMCodeModelGenerator();
    	
        // 8 TSMCodeModelGenerator
    	runTSMCodeModelGenerator();
    	
        // 9 TSMPostProcessor
    	runTSMPostProcessor();
    	
        // 10 TCodeModeloptimizer
    	runTCodeModelOptimizer();
    	
        // 11 TCPrinter
    	runTPrinter();
    }

    private void prepareInput(){
    	
    	// pretend, that importer has finished and produced the input file
    	lastSMFile = FileUtil.appendPath(tempPath, fileName);
    	
    	// copy the input file to temp folder as FMPreprocessor expects to read 
    	// its input there 
    	FileUtil.copy(FileUtil.appendPath(inputFolder, fileName), 
    			lastSMFile);
    }
}
