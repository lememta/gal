/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncherOptImpl.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-03-07 18:40:51 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;

public class GALauncherOptImpl extends GALauncherImpl{

	/**
	 * Runs the tool-chain with CodeModel optimiser
	 */
	@Override
    public void runTools() {
        
        // 1 TSimulinkImporter
    	runTSimulinkImporter();
    	
        // 2 TFMPreProcessor
    	runTFMPreProcessor();
    	
        // 3 TStateflowImporter
    	runTStateflowImporter();
    	
        // 4 TBlockSequencer
     	runTBlockSequencer();
    	
        // 5 TTyper
    	runTTyper();
    	
        // 6 TSMPreProcessor
    	runTSMPreProcessor();
    	
        // 7 TFMCodeModelGenerator
    	runTFMCodeModelGenerator();    	
    	
        // 8 TSMCodeModelGenerator
    	runTSMCodeModelGenerator();
    	
        // 9 TSMPostProcessor
    	runTSMPostProcessor();
        
        // 10 TCodeModeloptimizer
    	runTCodeModelOptimizer();
    	
        // 11 TCPrinter
    	runTPrinter();
    }

}
