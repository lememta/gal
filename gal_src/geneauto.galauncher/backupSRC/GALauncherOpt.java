/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncherOpt.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2009-11-10 09:58:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;

public class GALauncherOpt extends GALauncher{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launcher = new GALauncherOptImpl();
		
		launcher.gaMain(args, "GALauncherOpt");
	}

}
