/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncherSCICOSOpt.java,v $
 *  @version	$Revision: 1.1 $
 *	@date		$Date: 2010-11-25 13:59:52 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;

public class GALauncherSCICOSOpt extends GALauncher{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launcher = new GALauncherSCICOSOptImpl();
		
		launcher.gaMain(args, "GALauncherSCICOSOpt");
	}

}
