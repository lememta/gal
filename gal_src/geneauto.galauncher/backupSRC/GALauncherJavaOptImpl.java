/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncherJavaOptImpl.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2010-02-13 17:33:36 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;

public class GALauncherJavaOptImpl extends GALauncherJavaImpl{

    /**
     * Executes all tools in predefined order
     * Override this method if you need to change execution order of tools
     */
    public void runTools() {
    	        
        
        // 1 TSimulinkImporter
    	runTSimulinkImporter();
    	
        // 2 TFMPreProcessor
    	runTFMPreProcessor();
    	
        // 3 TStateflowImporter
    	runTStateflowImporter();
    	
        // 4 TBlockSequencer
     	runTBlockSequencer();
    	
        // 5 TTyper
    	runTTyper();
    	
        // 6 TSMPreProcessor
    	runTSMPreProcessor();
    	
        // 7 TFMCodeModelGenerator
    	runTFMCodeModelGenerator();    	
    	
        // 8 TSMCodeModelGenerator
    	runTSMCodeModelGenerator();
    	
        // 9 TSMPostProcessor
    	runTSMPostProcessor();
        
        // 10 TCodeModeloptimizer
    	runTCodeModelOptimizer();
    	
        // 11 TCPrinter
    	runTPrinter();
    }

}
