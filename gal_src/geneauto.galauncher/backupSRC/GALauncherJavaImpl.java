/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncherJavaImpl.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2010-01-12 13:44:56 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;


public class GALauncherJavaImpl extends GALauncherImpl{
	

	/**
	 * Gene-Auto launcher replacing the default COQ-based 
	 * block sequencer with Java-based implementation
	 *
	 */
    // ToNa 02/12/08 removed temporarily until the blocksequencer compoent is updated
/*	
	protected void runTBlockSequencer() {

	    stepNo++;
	    tmpCmdln.clear();
	    ArgumentReader.compileCmdlnArray(tmpCmdln, "", lastSMFile);
	    lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
	            + "." + GAConst.EXT_GSM);
	    ArgumentReader.compileCmdlnArray(tmpCmdln, "-o", lastSMFile);
	    ArgumentReader.compileCmdlnArray(tmpCmdln, "-b", blockLibraryFile);
	    strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
	
	    geneauto.tblocksequencer.BlockSequencer.main(strCmdln);
	
	    stopOnError();
	}
*/    
}
