/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncherImpl.java,v $
 *  @version	$Revision: 1.47 $
 *	@date		$Date: 2012-02-05 21:03:32 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;
import geneauto.utils.GASysInfo;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This is the main launcher for Gene-Auto tool
 */
public class GALauncherImpl {

    // root folder where the model is read
    protected String inputFolder = "";

    // input file name
    protected String fileName = "";

    // model name without the file extension
    protected String modelName = "";

    // path to library file
    protected String blockLibraryFile = "";

    // output folder
    protected String outputFolder = "";

    // folder for model references
    protected String externalLibFolder = "";

    // temp folder
    protected String tempPath = "";

    // matlab m file
    protected String constFile = "";

    // transformation step number
    protected int stepNo = 0;
    // filename of the last generated system model file
    protected String lastSMFile;
    // filename of the last generated code model file
    protected String lastCMFile;
    // temporary pointers for command line
    protected List<String> tmpCmdln = new ArrayList<String>();
    protected String[] strCmdln;

    /** Parameter to tune TSMCodeModelGenerator */
    protected boolean optSMJunctionFunctions = false;
    
    // To know if there is a stateflow part or not in the input model
	private boolean containsCharts = true;
	
    // ***** START: flag "--inline" ******
	// Inline flag value. If it is set, value is "<TRUE>", otherwise 
	// value is not set
	private boolean inlineMode;
    // ***** END: flag "--inline" ******

    // ***** START: flag "--global-io" ******
	// Global io flag value. If it is set, value is "<TRUE>", otherwise 
	// value is not set
	private boolean globalIOMode;
    // ***** END: flag "--global-io" ******
	
	/*
	 *  Defines how to print annotations, 
	 *  in case value is false Ids are not printed
	 */
	protected boolean debugMode;
	
	protected String tempModel;

	/**
	 * @return the name of the launcher that runs this LauncherImpl.
	 * 
	 * NOTE! Override this in implementing subclasses, where this does
	 * not produce the right launcher name!
	 */
	protected String getLauncherPackage() {
		// TODO Currently, the folder/project/jar name and the package name are different.
		//      The folder/project/jar should be renamed to geneauto.launcher, then
		//      we can get the right package name automatically
		//return getClass().getPackage().getName();
		return "geneauto.galauncher";
	}

    /**
     * Parses command line and writes parameters to GALauncherImpl classes
     * 
     * @param args
     */
    protected void parseCmdLn(String[] args) {
        ArgumentReader.clear(); // to be sure that we start up with clean
        						// configuration

        // Add arguments that are allowed on the command line
        configureArguments();

        ArgumentReader.setArguments(args);
        
        // Initialise the installation directory
        GASysInfo.initBinInstallPath(getLauncherPackage());        
        
        // set default file paths and force all paths to absolute path
        // if log file exists delete it
        ArgumentReader.initFilePathsAndLog(false, true);

		// TODO: Actually these values get overwritten now (AnTo 22/06/2010)
        String version = "DEV";
        String releaseDate = "";

        String toolClassPath = "geneauto/launcher/GALauncherImpl.class";
        URL url = this.getClass().getClassLoader().getResource(toolClassPath);

        version = FileUtil.getVersionFromManifest(url, toolClassPath);
        releaseDate = FileUtil.getDateFromManifest(url, toolClassPath);

        EventHandler.handle(EventLevel.INFO, "GALauncherImpl", "GI0001",
                "Gene-Auto toolset ver " + version + " " + releaseDate + " "
                        + "started at " + EventHandler.getDateTimeForMessage()
                        + "\n\t command line: "
                        + ArgumentReader.getArgumentString(), "");
        
        EventHandler.startTimer("Full toolset");

        // root folder where the model is read
        inputFolder = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE_DIR);

        // filename
        fileName = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE_NAME);

        // path to library file
        blockLibraryFile = ArgumentReader.getParameter(GAConst.ARG_LIBFILE);

        // model name without the file extension
        // specific handling needed to deal with double extensions
        if (fileName.endsWith(GAConst.EXT_GSM)) {
            modelName = FileUtil.getFileNameNoExt(fileName, GAConst.EXT_GSM);
        } else if (fileName.endsWith(GAConst.EXT_GCM)) {
            modelName = FileUtil.getFileNameNoExt(fileName, GAConst.EXT_GCM);
        } else {
        	modelName = FileUtil.getFileNameNoExt(fileName);
        }

        // output folder
        outputFolder = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFOLDER);

        // default folder
        externalLibFolder = ArgumentReader
                .getParameter(GAConst.ARG_MODELLIBFOLDER);

        // temp folder
        tempPath = ArgumentReader.getParameter(GAConst.ARG_TMPFOLDER);
        
        // ***** START: flag "--inline" ******
        // inline mode
        inlineMode = ArgumentReader.isFlagSet(GAConst.ARG_INLINE);
        // ***** END: flag "--inline" ******
        
        // ***** START: flag "--global-io" ******
        // inline mode
        globalIOMode = ArgumentReader.isFlagSet(GAConst.ARG_GLOBAL_IO);
        // ***** END: flag "--global-io" ******
        
        debugMode = ArgumentReader.isFlagSet(GAConst.ARG_DEBUG);
        
        tempModel = ArgumentReader.getParameter(GAConst.ARG_TEMP_MODEL);
                
		// Clean target folders
        
		// The temp folder is always cleaned. However, it normally contains
		// already an open log at this state. Exclude this from deletion.
        Set<String> exclusions = new HashSet<String>();
        exclusions.add(EventHandler.getLogPath());
        FileUtil.cleanFolder(tempPath, exclusions);
        
        // The primary output folder is cleaned only in case it was automatically derived
        if (ArgumentReader.getParameter(GAConst.ARG_OUTPUTFOLDER_MODE).equals("AUTO")) {
        	// Clean folder
        	// We need to keep the same exclusion set, because the temp folder 
        	// might have been under the output folder
            FileUtil.cleanFolder(outputFolder, exclusions);
        } else if (!FileUtil.isFolderEmpty(outputFolder)) {
            EventHandler.handle(
                EventLevel.INFO,
                "GALauncher",
                "GI0011",
                "The user-defined output folder is not empty and it is not safe to delete the files. "
                        + "\n If the name of a generated file overlaps with the one in the folder the old file is overwritten."
                        + "\n Any other files remain in the folder. Please check the change dates of files before using them!"
                        + "\n\n To force the code generator to delete all old files run the tool without \"-O\" switch.\n");
        }                
        
        // matlab m file
        constFile = ArgumentReader.getParameter(GAConst.ARG_CONSTANTSFILE);
    }

	/**
	 * Add arguments that are allowed on the command line.
	 */
	protected void configureArguments() {
		ArgumentReader.addArgument("", GAConst.ARG_INPUTFILE, true, true,
                true, GAConst.ARG_INPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-b", GAConst.ARG_LIBFILE, false, true,
                true, GAConst.ARG_LIBFILE_NAME_ROOT);
        ArgumentReader.addArgument("-m", GAConst.ARG_CONSTANTSFILE, false,
                true, true, GAConst.ARG_CONSTANTSFILE_NAME_ROOT);
        ArgumentReader.addArgument("-O", GAConst.ARG_OUTPUTFOLDER, false,
                true, false, "");
        ArgumentReader.addArgument("--mLib", GAConst.ARG_MODELLIBFOLDER, false,
                true, false, "");
        ArgumentReader.addArgument("--tempModel", GAConst.ARG_TEMP_MODEL, false, 
        		true, false, "");
        ArgumentReader.addArgument("-d", GAConst.ARG_DEBUG, false, 
        		false, false, "");
        
        // ***** START: flag "--inline" ******
        ArgumentReader.addArgument("--" + GAConst.ARG_INLINE, GAConst.ARG_INLINE, false,
                false, false, "");
        // ***** END: flag "--inline" ******

        // ***** START: flag "--global-io" ******
        ArgumentReader.addArgument("--" + GAConst.ARG_GLOBAL_IO, GAConst.ARG_GLOBAL_IO, false,
                false, false, "");
        // ***** END: flag "--global-io" ******
	}

    /**
     * gene-Auto main method. parses command line and executes all tools in the
     * order defined in runTools();
     * 
     * @param args
     *            -- command line argument of the tool
     * @param toolsetName
     *            -- a name to be included in messages
     */
    public void gaMain(String[] args, String toolsetName) {

        try {
        	EventHandler.setLauncherName(toolsetName);
        	
            // parse command line
            parseCmdLn(args);

            // run tools
            runTools();

            // display some final messages
            // set the tool name to be the toolset (launcher) name for these
            EventHandler.setToolName(toolsetName);
            
            EventHandler.handle(
                    EventLevel.INFO, "", "", 
                    		"Gene-Auto Toolset Finished processing." +
                    		" at " + EventHandler.getDateTimeForMessage() 
                    		+ ", execution time " 
                    		+ EventHandler.getDuration("Full toolset", true));
            
            // print summary of elementary tool execution times
    		EventHandler.printTimerSummary();
    		
    		// A clean close. Just in case
    		EventHandler.close();
    		
        } catch (Exception e) {
            EventHandler.handle(
            		EventLevel.CRITICAL_ERROR,
            		toolsetName, "", "Unhandled exception", e);
        }
    }

    /**
     * Executes all tools in predefined order Override this method if you need
     * to change execution order of tools
     */
    public void runTools() {

        // 1 TSimulinkImporter
        runTSimulinkImporter();

        // 2 TFMPreProcessor
        runTFMPreProcessor();

        // 3 TStateflowImporter
        runTStateflowImporter();
        
        // 4 TBlockSequencer
        //runTBlockSequencer();

        // 5 TTyper
        //runTTyper();

    	// 6 TSMPreProcessor
    	//runTSMPreProcessor();
        
        // 7 TFMCodeModelGenerator
        //runTFMCodeModelGenerator();

    	// 8 TSMCodeModelGenerator
    	//runTSMCodeModelGenerator();

    	// 9 TSMPostProcessor
    	//runTSMPostProcessor();

        // 11 TCPrinter
        //runTPrinter();
    }

    /**
     * Launch methods for each tool.
     * 
     * If you need to replace an elementary tool with different implementation,
     * override the corresponding run method.
     * 
     */

    protected void runTSimulinkImporter() {

        stepNo++;
        tmpCmdln.clear();
        ArgumentReader.addArgToCmdLine(tmpCmdln, null, FileUtil.appendPath(
                inputFolder, fileName));
        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
                + "_" + GAConst.SUFFIX_SLIM + "." + GAConst.EXT_GSM);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "--mLib", externalLibFolder);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-m", constFile);
        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

        geneauto.tsimulinkimporter.main.TSimulinkImporter.main(strCmdln);

        stopOnError();
    }

    protected void runTFMPreProcessor() {

        stepNo++;
        tmpCmdln.clear();
        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
        		+ "_" + GAConst.SUFFIX_FMPR + "." + GAConst.EXT_GSM);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
        // ***** START: flag "--inline" ******
	    ArgumentReader.addFlagToCmdLine(tmpCmdln, "--" + GAConst.ARG_INLINE, inlineMode);        
        // ***** END: flag "--inline" ******
        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

        geneauto.tfmpreprocessor.main.FMPreprocessor.main(strCmdln);

        stopOnError();
    }

    protected void runTStateflowImporter() {
    	
    	String oldLastSMFile = lastSMFile;

        stepNo++;
        tmpCmdln.clear();
        ArgumentReader.addArgToCmdLine(tmpCmdln, null, FileUtil.appendPath(
                inputFolder, fileName));
        ArgumentReader.addArgToCmdLine(tmpCmdln, "--iSM", quoteLastSMFile());
        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
        		+ "_" + GAConst.SUFFIX_SFIM + "." + GAConst.EXT_GSM);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

        geneauto.tstateflowimporter.main.StateflowImporter.main(strCmdln);

        stopOnError();
        
        // To know if the state flow files have been generated or not
        // If no files have been generated => there is no state flow
        File tStateflowImporterOutput = new File(lastSMFile);
        if(!tStateflowImporterOutput.exists()){
        	// No state flow generated => no state flow part
        	containsCharts = false;	
        	// Change step number because any files have been generated by the stateflowImporter
        	stepNo--;
            lastSMFile = oldLastSMFile;
        }
    }

//    protected void runTBlockSequencer(){
//    	
//        stepNo++;
//        tmpCmdln.clear();
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
//        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName 
//        		+ "_" + GAConst.SUFFIX_FMBS + "." + GAConst.EXT_GSM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.tblocksequencer_coq.main.BlockSequencerCoq.main(strCmdln);
//        
//        stopOnError();
//    }	

//    protected void runTTyper() {
//
//        stepNo++;
//        tmpCmdln.clear();
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
//        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_FMTY + "." + GAConst.EXT_GSM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.typer.main.Typer.main(strCmdln);
//
//        stopOnError();
//    }

//    protected void runTSMPreProcessor() {
//
//    	// run the tool only if there is charts in the model 
//    	if(!containsCharts){
//    		return;
//    	}
//        
//        stepNo++;
//        tmpCmdln.clear();
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
//        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_SMPR + "." + GAConst.EXT_GSM);
//        lastCMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_SMPR + "." + GAConst.EXT_GCM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "--oCM", quoteLastCMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.tsmpreprocessor.main.SMPreProcessor.main(strCmdln);
//
//        stopOnError();
//    }

//    protected void runTFMCodeModelGenerator() {
//
//        stepNo++;
//        tmpCmdln.clear();
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "--iCM", quoteLastCMFile());
//        lastCMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_FMCG + "." + GAConst.EXT_GCM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "--oCM", quoteLastCMFile());
//        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_FMCG + "." + GAConst.EXT_GSM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "--oSM", quoteLastSMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
//        // ***** START: flag "--inline" ******
//	    ArgumentReader.addFlagToCmdLine(tmpCmdln, "--" + GAConst.ARG_INLINE, inlineMode);        
//        // ***** END: flag "--inline" ******
//        // ***** START: flag "--global-io" ******
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "--" + GAConst.ARG_GLOBAL_IO, globalIOMode);
//        // ***** END: flag "--global-io" ******
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.tfmcodemodelgenerator.main.CodeModelGenerator.main(strCmdln);
//
//        stopOnError();
//    }

//    protected void runTSMCodeModelGenerator() {
//
//    	// run the tool only if there is charts in the model 
//    	if(!containsCharts){
//    		return;
//    	}
//
//        stepNo++;
//        tmpCmdln.clear();
//        // functional model compiled BEFORE TFMCodeModelGenerator
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "--iCM", quoteLastCMFile());
//        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_SMCG + "." + GAConst.EXT_GSM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
//        lastCMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_SMCG + "." + GAConst.EXT_GCM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "--oCM", quoteLastCMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-jf", optSMJunctionFunctions);
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.tsmcodemodelgenerator.main.SMCodeModelGenerator.main(strCmdln);
//
//        stopOnError();
//    }

//    protected void runTSMPostProcessor() {
//
//    	// run the tool only if there is charts in the model 
//    	if(!containsCharts){
//    		return;
//    	}
//
//        stepNo++;
//        tmpCmdln.clear();
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
//        // code model compiled by TFMCodeModelGenerator
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "--iCM", quoteLastCMFile());
//        lastCMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_SMPO + "." + GAConst.EXT_GCM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastCMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.tsmpostprocessor.main.SMPostProcessor.main(strCmdln);
//
//        stopOnError();
//    }

//    protected void runTCodeModelOptimizer() {
//
//        stepNo++;
//        tmpCmdln.clear();
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastCMFile());
//        lastCMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//        		+ "_" + GAConst.SUFFIX_CMOP + "." + GAConst.EXT_GCM);
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastCMFile());
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.tcodemodeloptimizer.main.CodeModelOptimizer.main(strCmdln);
//        stopOnError();
//    }

//    protected void runTPrinter() {
//
//        stepNo++;
//        tmpCmdln.clear();
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastCMFile());
//        ArgumentReader.addArgToCmdLine(tmpCmdln, "-O", outputFolder);
//        if (tempModel.equals(GAConst.TEMP_MODEL_ALL)) {
//            lastCMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
//            		+ "_" + GAConst.SUFFIX_CMPR + "." + GAConst.EXT_GCM);
//            ArgumentReader.addArgToCmdLine(tmpCmdln, "--oCM", quoteLastCMFile());
//        }
//        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
//        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//        geneauto.tcprinter.main.CPrinter.main(strCmdln);
//
//        stopOnError();
//    }

    protected String quoteLastSMFile() {
		if (lastSMFile == null) {
			return null;
		} else if (lastSMFile.contains(" ")) {
			return "\"" + lastSMFile + "\"";
		} else {
			return lastSMFile;
		}
	}

	protected String quoteLastCMFile() {
		if (lastCMFile == null) {
			return null;
		} else if (lastCMFile.contains(" ")) {
			return "\"" + lastCMFile + "\"";
		} else {
			return lastCMFile;
		}
	}

    protected void stopOnError() {
        if (EventHandler.getAnErrorHasOccured()) {
            System.exit(1);
        }
    }
}
