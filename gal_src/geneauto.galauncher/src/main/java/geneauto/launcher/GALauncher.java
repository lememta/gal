/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.galauncher/src/main/java/geneauto/launcher/GALauncher.java,v $
 *  @version	$Revision: 1.37 $
 *	@date		$Date: 2009-11-10 09:58:40 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  $License: $ 
 */
package geneauto.launcher;

import geneauto.utils.GAConst;

public class GALauncher {

	protected static GALauncherImpl launcher;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launcher = new GALauncherImpl();
		
		System.out.println(GAConst.LICENSE_MESSAGE);
		
		launcher.gaMain(args, "GALauncher");
	}

}
