/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/SumTyper.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of Sum Blocks of a GASystemModel.
 * 
 */
public class SumTyper extends BlockTyper {

    /**
     * The block which is being typed.
     */
    private Block block;

    /**
     * Assign a type to the output port according to the datatype of the block's
     * inports. if the input ports have no datatype, then it is the execution of
     * the rule "back propagation". In this case, the output port must not be
     * null.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block pblock) {
        block = pblock;
        
        if (block.getOutDataPorts().get(0).getDataType() == null) {

            GADataType signalType = null;

            // specific treatment if the sum block has a single output.
            if (block.getInDataPorts().size() == 1) {
                signalType = assignTypeFromSingleInput();

            } else {
                signalType = assignTypeFromMultipleInputs();
            }

            // if no type is returned, exit the function
            if (signalType == null) {
                return false;
            }

            Parameter paramOutDataTypeMode = block
                    .getParameterByName("OutDataTypeMode");

            String outDataTypeMode = ((StringExpression) paramOutDataTypeMode.getValue()).getLitValue();

            GADataType assignedType = null;
            GADataType specifiedType = null;
            if (outDataTypeMode.equals("Inherit via internal rule")) {
                assignedType = signalType;
            } else if (outDataTypeMode.equals("Specify via dialog")) {

                Parameter paramOutDataType = block
                        .getParameterByName("OutDataType");

                if (paramOutDataType != null) {
                    String outDataType = ((StringExpression) paramOutDataType.getValue()).getLitValue();

                    specifiedType = DataTypeAccessor.getDataType(outDataType,
                            false);

                    if (specifiedType == null) {
                        EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                                .getClass().getCanonicalName(), "",
                                "Specified type: " + outDataType
                                        + " in block: "
                                        + block.getReferenceString()
                                        + "is not valid.");
                        return false;
                    } else {
                        if (signalType instanceof TArray) {
                            ((TArray) signalType)
                                    .setBaseType((TPrimitive) specifiedType);
                            assignedType = signalType;
                        } else {
                            assignedType = specifiedType;
                        }
                    }
                } else {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                            .getClass().getCanonicalName(), "",
                            "No specified type in block: "
                                    + block.getReferenceString() + ".");
                    return false;
                }
            } else {
                specifiedType = DataTypeAccessor.getDataType(outDataTypeMode,
                        false);

                if (specifiedType == null) {
                    assignedType = signalType;
                } else {
                    if (signalType instanceof TArray) {
                        ((TArray) signalType)
                                .setBaseType((TPrimitive) specifiedType);
                        assignedType = signalType;
                    } else {
                        assignedType = specifiedType;
                    }
                }
            }
            block.getOutDataPorts().get(0).setDataType(assignedType);

            return true;
        }
        return true;
    }

    /**
     * Check that all blocks are typed.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "SumTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
        }

        return result;

    }

    /**
     * Method which allow to type a SumBlock containing a unique input port.
     * 
     * @return true if the typing was executed properly.
     */
    private GADataType assignTypeFromSingleInput() {

        GADataType result = null;

        InDataPort inport = block.getInDataPorts().get(0);

        // the input port's datatype mus tbe neither null
        // nor a TArray datatype.
        if (inport.getDataType() == null) {
            EventHandler.handle(EventLevel.ERROR, "SumTyper - Typing error",
                    "", "Block " + block.getReferenceString()
                            + "'s unique input port has no datatype.", "");
        } else {
            if (inport.getDataType().isMatrix()) {
                EventHandler.handle(EventLevel.ERROR,
                        "SumTyper - Typing error", "", "Block "
                                + block.getReferenceString()
                                + "'s unique input port is a matrix.", "");
            } else if (inport.getDataType().isVector()) {
                // if the datatype of the unique input port is a vector,
                // the output port datatype is the primitive type of
                // this vector.
                // the result is the sum of the different elements of the
                // vector.
                TArray vector = (TArray) inport.getDataType();
                result = vector.getBaseType();
            } else {
                result = inport.getDataType();
            }
        }

        return result;
    }

    /**
     * Method which allow to type a SumBlock containing several input ports.
     * 
     * @return true if the typing was executed properly.
     */
    private GADataType assignTypeFromMultipleInputs() {

        GADataType result = null;

        GADataType toAssign = null;
        // operator is required for determining the data type
        // from the viewpoint of typing there is no difference whether it is
        // + or -, so we do not check sign here
        BinaryOperator op = BinaryOperator.ADD_OPERATOR;
        
        InDataPort lastInport = null;
        for (InDataPort inport : block.getInDataPorts()) {
        	// Assert that the inport has been given a datatype
            if (inport.getDataType() == null) {
                EventHandler.handle(EventLevel.ERROR, "SumTyper", "",
                        " Untyped input port: " + "\n "
                                + inport.getShortReferenceString()
                                + "\n in block " + block.getReferenceString());
                return null;
            }
            
            if (toAssign == null) {
                if (lastInport != null) {
                    toAssign = op.getDataType(lastInport.getDataType(), inport
                            .getDataType(), false);
                }
            } else {
                // derive the type from +/- operation between
                // previous expression and the current port type
                toAssign = op.getDataType(toAssign, inport.getDataType(), false);
            }

            // after the second loop we should have resulting data type
            // of we do not, there is typing error
            if (lastInport != null && toAssign == null) {
                EventHandler.handle(EventLevel.ERROR, "SumTyper", "",
                        " Typing error when comparing data types of " + "\n "
                                + lastInport.getShortReferenceString()
                                + " (data type " 
                                + lastInport.getDataType().toString() + ")"
                                + "\n and " + inport.getShortReferenceString()
                                + " (data type " 
                                + DataTypeAccessor.toString(inport.getDataType()) 
                                + ")"
                                + "\n in block " + block.getReferenceString(),
                        "");
                return null;
            }
            lastInport = inport;
        }

        if (toAssign != null) {
            result = toAssign;
        } else {
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "SumTyper - Typing error",
                            "",
                            block.getReferenceString() + " could not be typed.",
                            "");
        }
        return result;
    }
}
