/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ProductTyper.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.blocklibrary.utils.ProductBackendUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.utils.assertions.Assert;

import java.util.List;

/**
 * This class allows to type and check types of Product Blocks of a
 * GASystemModel.
 * 
 * TODO: (TF:556) there is currently too much duplication -- if valdiateTypes is
 * implemented properly, it does the same check that assigntTypes Review there
 * roles of both methods (ToNa 22/12/08)
 */
public class ProductTyper extends BlockTyper {

    /**
     * The block which is being typed.
     */
    private Block block;

    /**
     * Assign a type to the output port.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block pblock) {

    	block = pblock;
        
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            // assign output type only when type is not explicitly set
            // checking number of inputs and outputs
            Assert.assertNotEmpty(block.getInDataPorts(),
                    "Block has no outputs, expected one!\n "
                            + block.getReferenceString());

            Assert.assertNotEmpty(block.getOutDataPorts(),
                    "Block has no inputs, expected one or more\n "
                            + block.getReferenceString());

            // reading block parameter
            Parameter paramMul = block.getParameterByName("Multiplication");
            Parameter paramInputs = block.getParameterByName("Inputs");

            Assert.assertNotNull(paramMul,
                    "Parameter 'Multiplication' not defined: "
                            + block.getReferenceString());

            Assert.assertNotNull(paramInputs,
                    "Parameter 'Inputs' not defined: "
                            + block.getReferenceString());

            // determining operators (* or /)
            List<BinaryOperator> operators = ProductBackendUtil
                    .parseProductParameter(paramInputs.getStringValue(),
                            paramMul.getStringValue());

            /*
             * Compute output type via internal rules
             */

            GADataType resultType = getDataType(block.getInDataPorts(),
                    operators, block);

            // if no type is returned, exit the function
            if (resultType == null) {
                return false;
            }

            /*
             * Check type specification of the block
             */

            Parameter paramOutDataTypeMode = block
                    .getParameterByName("OutDataTypeMode");

            String outDataTypeMode = ((StringExpression) paramOutDataTypeMode.getValue()).getLitValue();

            GADataType assignedType = null;
            GADataType specifiedType = null;
            if (outDataTypeMode.equals("Inherit via internal rule")) {
                assignedType = resultType;
            } else if (outDataTypeMode.equals("Specify via dialog")) {

                Parameter paramOutDataType = block
                        .getParameterByName("OutDataType");

                if (paramOutDataType != null) {
                    String outDataType = ((StringExpression) paramOutDataType.getValue()).getLitValue();

                    specifiedType = DataTypeAccessor.getDataType(outDataType,
                            false);

                    if (specifiedType == null) {
                        EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                                .getClass().getCanonicalName(), "",
                                "Specified type: " + outDataType
                                        + " in block: "
                                        + block.getReferenceString()
                                        + "is not valid.");
                        return false;
                    } else {
                        if (resultType instanceof TArray) {
                            ((TArray) resultType)
                                    .setBaseType((TPrimitive) specifiedType);
                            assignedType = resultType;
                        } else {
                            assignedType = specifiedType;
                        }
                    }
                } else {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                            .getClass().getCanonicalName(), "",
                            "No specified type in block: "
                                    + block.getReferenceString() + ".");
                    return false;
                }
            } else {
                specifiedType = DataTypeAccessor.getDataType(outDataTypeMode,
                        false);

                if (specifiedType == null) {
                    assignedType = resultType;
                } else {
                    if (resultType instanceof TArray) {
                        ((TArray) resultType)
                                .setBaseType((TPrimitive) specifiedType);
                        assignedType = resultType;
                    } else {
                        assignedType = specifiedType;
                    }
                }
            }

            block.getOutDataPorts().get(0).setDataType(assignedType);

            return true;
        }

        return true;
    }

    /**
     */
    public boolean validateTypes(Block pblock) {
        block = pblock;
    	
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                    .getCanonicalName(), "", "Untyped output port in block \n "
                    + block.getReferenceString());

        } else {
            // assign output type only when type is not explicitly set
            // checking number of inputs and outputs
            Assert.assertNotEmpty(block.getInDataPorts(),
                    "Block has no outputs, expected one!\n "
                            + block.getReferenceString());

            Assert.assertNotEmpty(block.getOutDataPorts(),
                    "Block has no inputs, expected one or more\n "
                            + block.getReferenceString());

            // reading block parameter
            Parameter paramMul = block.getParameterByName("Multiplication");
            Parameter paramInputs = block.getParameterByName("Inputs");

            Assert.assertNotNull(paramMul,
                    "Parameter 'Multiplication' not defined: "
                            + block.getReferenceString());

            Assert.assertNotNull(paramInputs,
                    "Parameter 'Inputs' not defined: "
                            + block.getReferenceString());

            // determining operators (* or /)
            List<BinaryOperator> operators = ProductBackendUtil
                    .parseProductParameter(paramInputs.getStringValue(),
                            paramMul.getStringValue());

            GADataType resultType = getDataType(block.getInDataPorts(),
                    operators, block);

            AssignOperator op = AssignOperator.SIMPLE_ASSIGN;

            return op.checkTypeValidity(block.getOutDataPorts().get(0)
                    .getDataType(), resultType, false);
        }

        return true;
    }

    private GADataType getDataType(List<InDataPort> inputs,
            List<BinaryOperator> operators,
            Block block) {

        if (inputs.size() == 1) {
            return getTypeFromSingleInput(inputs.get(0), operators, block);
        } else {
            return getTypeFromMultipleInputs(inputs, operators);
        }
    }

    private GADataType getTypeFromMultipleInputs(List<InDataPort> inputs,
            List<BinaryOperator> operators) {

        int size = inputs.size();
        GADataType resultType = null;
        GADataType prevType = null;
        
        for (int i = 0; i < size; i++) {
            InDataPort currentPort = inputs.get(i);
            if (i == 0) {
                // in the first loop we only handle "/" (add 1/<arg>)
                // "*" is left unhandled
                if (BinaryOperator.DIV_OPERATOR.equals(operators.get(i))) {
                    prevType = new TRealDouble();
                    resultType = operators.get(i).getDataType(prevType,
                            currentPort.getDataType(), false);

                } else {
                    resultType = currentPort.getDataType();
                }
            } else {
                resultType = operators.get(i).getDataType(resultType,
                        currentPort.getDataType(), false);
            }

            if (resultType == null) {
                if (prevType == null) {
                    // this can happen only in first iteration when the first
                    // input did not have data type
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                            .getClass().getCanonicalName(), "",
                            "Untyped first input port in block \n "
                                    + block.getReferenceString());

                } else {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                            .getClass().getCanonicalName(), "", "Type "
                            + prevType.getReferenceString() + " and type "
                            + currentPort.getDataType().getReferenceString()
                            + "\n are not compliant with operator "
                            + operators.get(i).getName());
                }
                return null;
            }

            prevType = resultType;
        }

        return resultType;
    }

    private GADataType getTypeFromSingleInput(InDataPort expression,
            List<BinaryOperator> operators,
            Block block) {

        int dim = expression.getDataType().getDimensionality();
        GADataType resultType = null;

        if (dim > 2) {
            // not applicable to matrices
            EventHandler.handle(EventLevel.ERROR, "ProductBackendUtil", "",
                    "Single-input product operation can't be applied "
                            + "on more than 2 dimensions inputs\n Block: "
                            + block.getReferenceString(), "");
            return null;
        } else {
            // TODO: (to FaCa, TF:557) here we assume that in case of single 
        	// input there is always only one operator. Check that this is 
        	// correct
            if (expression.getDataType() == null) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                        .getCanonicalName(), "",
                        "Untyped input port in block \n "
                                + block.getReferenceString());
                return null;
            }

            if (BinaryOperator.DIV_OPERATOR.equals(operators.get(0))) {
                GADataType prevType = new TRealDouble();
                
                Parameter paramMul = block.getParameterByName("Multiplication");
                String paramMulString = ((StringExpression) paramMul.getValue()).getLitValue();
                
                if (paramMul.getStringValue().equals("Element-wise(.*)")){
                	resultType = operators.get(0).getDataType(prevType,
                		expression.getDataType().getPrimitiveType()
                		, true);
                } else {
                	resultType = operators.get(0).getDataType(prevType,
                		expression.getDataType()
                        , true);
                }
                
                if (resultType == null) {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                            .getClass().getCanonicalName(), "", "Type "
                            + prevType.getReferenceString() + " and type "
                            + expression.getDataType().getReferenceString()
                            + "\n are not compliant with operator "
                            + operators.get(0).getName());
                    return null;
                }
            } else {
                resultType = expression.getDataType().getPrimitiveType();

            }
        }

        return resultType;
    }

}
