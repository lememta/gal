/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ReadMapArrayTyper.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:23:16 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of ReadMapArray Blocks of a GASystemModel.
 * 
 *
 */
public class ReadMapArrayTyper extends BlockTyper {

	/**
	 * ReadMapArray has two inputs and one output. 
	 * Its output type must be the same as its
	 * second input base type. If an input type is null, 
	 * then an error is raised.
	 * 
	 * @param block
	 *            The block which is to be typed.
	 */
	public boolean assignTypes(Block block) {

		if (!checkPorts(block)) {
			return false;
		}
		
		// we assign to output port scalar signal type and 
		// same type as the base type of the second input
		block.getOutDataPorts().get(0).setDataType(
				((TArray) block.getInDataPorts().get(1).getDataType())
						.getBaseType());
		
		return true;		
	}

	/**
	 * We check that the second base type input and output type are the same
	 * 
	 * @param block
	 *            the block which is being checked.
	 */
	public boolean validateTypes(Block block) {

		if (!checkPorts(block)) {
			return false;
		}

		if (!((TArray)block.getInDataPorts().get(1).getDataType()).getBaseType()
					.isSubType(block.getOutDataPorts().get(0).getDataType())) {
			EventHandler.handle(EventLevel.ERROR,
					this.getClass().getCanonicalName(), 
					"",
					"Incompatible input type of second input ("
					+ block.getInDataPorts().get(1).getDataType().toString()
					+ ") and block output ("
					+ DataTypeAccessor.toString(
								block.getOutDataPorts().get(0).getDataType())
					+ "\n Block "
					+ block.getReferenceString(),
					"");
			return false;
		}
		
		return true;		
	}

	/**
	 * check correctness of port count and input port types
	 * 
	 * @return true in case of success and false when types are not compatible
	 */
	private boolean checkPorts(Block block) {
		// we expect exactly two inputs
		if (block.getInDataPorts() == null 
					|| block.getInDataPorts().size() != 2) {
			
			EventHandler.handle(EventLevel.ERROR,
					this.getClass().getCanonicalName(), 
					"",
					"Incorrect number of input ports ("
					+ (block.getInDataPorts() == null? "0":block.getInDataPorts().size())
					+ ") expected 2!"
					+ "\n Block "
					+ block.getReferenceString(),
					"");
			return false;
		
		}
		
		// we expect exactly one input
		if (block.getOutDataPorts() == null ||
				block.getOutDataPorts().size() != 1) {
			EventHandler.handle(EventLevel.ERROR,
					this.getClass().getCanonicalName(), 
					"",
					"Incorrect number of output ports ("
					+ (block.getOutDataPorts() == null? "0":block.getOutDataPorts().size())
					+ ") expected 1!"
					+ "\n Block "
					+ block.getReferenceString(),
					"");
			
			return false;
		}	

		// we check that the input ports are typed.
		for (InDataPort port : block.getInDataPorts()) {
			if (port.getDataType() == null) {

				// if no input datatype -> error event.
				EventHandler.handle(EventLevel.ERROR,
						this.getClass().getCanonicalName(), 
						"",
						"Untyped input port " 
							+ port.getShortReferenceString() 
							+ "in block\n "
							+ block.getReferenceString(), 
						"");

				return false;
			}
		}

		// we check that first port is scalar (numeric)
		if (!(block.getInDataPorts().get(0).getDataType() 
											instanceof TPrimitive)) {

			EventHandler.handle(EventLevel.ERROR,
							this.getClass().getCanonicalName(), 
							"",
							"The first input port in block \n "
							+ block.getReferenceString()
							+ "\n has data type "
							+ DataTypeAccessor
									.toString(block.getInDataPorts()
												.get(0).getDataType())
							+ "\n The block requires scalar numeric type!",
							"");
			return false;
		}

		// we check that second port is vector
		if (!(block.getInDataPorts().get(1).getDataType().isVector())) {

			EventHandler.handle(EventLevel.ERROR,
					this.getClass().getCanonicalName(), 
					"",
					"The second input port in block \n "
					+ block.getReferenceString()
					+ "\n has data type "
					+ DataTypeAccessor
							.toString(block.getInDataPorts()
										.get(1).getDataType())
					+ "\n The block requires vector type!",
					"");
			return false;
		}

		return true;
	}
}
