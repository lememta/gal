/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/WriteMapArrayTyper.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;

/**
 * This class allows to type and check types of WriteMapArray Blocks of a
 * GASystemModel.
 * 
 * 
 */
public class WriteMapArrayTyper extends BlockTyper {

	/**
	 * WriteMapArray has three input and one output. Its output type must be the
	 * same as its second input type. If an input type is null, then an error is
	 * raised.
	 * 
	 * @param block
	 *            The block which is to be typed.
	 */
	public boolean assignTypes(Block block) {

		// we check that the input and output port list are defined
		if ((block.getInDataPorts() != null)
				&& (block.getOutDataPorts() != null)) {

			// we check that the input and output port list have correct size
			if (block.getInDataPorts().size() == 3
					&& block.getOutDataPorts().size() == 1) {

				// we check that the input ports are typed.
				for (int i = 0; i < 3; i++) {
					if (block.getInDataPorts().get(0).getDataType() == null) {

						// if no input datatype -> error event.
						EventHandler.handle(EventLevel.ERROR,
								"WriteMapArrayTyper - Typing error", "",
								"WriteMapArray block "
										+ block.getReferenceString()
										+ "'s input port is not typed.", "");

						return false;
					}
				}

				// we check that first port is scalar numeric
				if (!(block.getInDataPorts().get(0).getDataType() instanceof TPrimitive)) {

					EventHandler
							.handle(
									EventLevel.ERROR,
									"WriteMapArrayTyper - Typing error",
									"",
									"WriteMapArray block "
											+ block.getReferenceString()
											+ " first input port has not expected type.",
									"");
				}

				// we check that second port is vector
				if (!(block.getInDataPorts().get(1).getDataType().isVector())) {

					EventHandler
							.handle(
									EventLevel.ERROR,
									"WriteMapArrayTyper - Typing error",
									"",
									"WriteMapArray block "
											+ block.getReferenceString()
											+ " second input port has not expected type.",
									"");
				}

				// we check that third input is scalar and has same data type
				// than second input base type
				if (!block.getInDataPorts().get(2).getDataType().getTypeName()
						.equals(
								((TArray) block.getInDataPorts().get(1)
										.getDataType()).getBaseType()
										.getTypeName())) {

					EventHandler
							.handle(
									EventLevel.ERROR,
									"WriteMapArrayTyper - Typing error",
									"",
									"WriteMapArray block "
											+ block.getReferenceString()
											+ " second and third input types are not compatibles.",
									"");

				}

				// we assign to output port same type as the second input
				block.getOutDataPorts().get(0).setDataType(
						block.getInDataPorts().get(1).getDataType());

				return true;
			} else {
				// if input data port or output data port hasn't correct size ->
				// error event.
				EventHandler
						.handle(
								EventLevel.ERROR,
								"WriteMapArrayTyper - Typing error",
								"",
								"WriteMapArray block "
										+ block.getReferenceString()
										+ "'s input data port or output data port list has not correct size.",
								"");
			}
		} else {
			// if no input data port or output data port list defined -> error
			// event.
			EventHandler
					.handle(
							EventLevel.ERROR,
							"WriteMapArrayTyper - Typing error",
							"",
							"WriteMapArray block "
									+ block.getReferenceString()
									+ "'s input data port or output data port list is not defined.",
							"");
		}
		return false;
	}

	/**
	 * We check that the second input type and output type are the same
	 * 
	 * @param block
	 *            the block which is being checked.
	 */
	public boolean validateTypes(Block block) {


		GADataType inputType = block.getInDataPorts().get(1).getDataType();
		GADataType outputType = block.getOutDataPorts().get(0).getDataType();

		if (!inputType.getTypeName().equals(outputType.getTypeName())) {

			EventHandler
					.handle(
							EventLevel.ERROR,
							"WriteMapArrayTyper - Typing error",
							"",
							"WriteMapArray block "
									+ block.getReferenceString()
									+ " input and output type are not compatibles.",
							"");

			return false;
		}
		return true;
	}

}
