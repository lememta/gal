/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/DataTypeConversionTyper.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TComplexNumeric;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of DataTypeConversion Blocks of a
 * GASystemModel.
 * 
 */
public class DataTypeConversionTyper extends BlockTyper {

	/**
	 * Assign the types according to the parameter DataType of the block. Unable
	 * to type input ports using the back propagation.
	 * 
	 * @param block
	 *            The block which is to be typed.
	 */
	public boolean assignTypes(Block block) {

		// we check that the input and output port list are defined
		if (block.getInDataPorts() != null && block.getOutDataPorts() != null) {

			// we check that the input and output port list have correct size
			if (block.getOutDataPorts().size() == 1
					&& block.getInDataPorts().size() == 1
					&& block.getInDataPorts().get(0).getDataType() !=null) {

				String newDataType = "";
				boolean error = false;

				// fetch the parameter DataType, which corresponds to the field
				// output data type
				for (Parameter param : block.getParameters()) {
					if ("DataType".equals(param.getName())) {
						newDataType = ((StringExpression) param.getValue()).getLitValue();
					}
					if ("OutDataTypeMode".equals(param.getName())) {
                        newDataType = ((StringExpression) param.getValue()).getLitValue();
                    }
				}

				// if the parameter has been read
				if (!"".equals(newDataType)) {
				    // A rule that inherits a data type,
					// for example, Inherit: Inherit via back propagation
					if (newDataType.contains("Inherit")) {
						// get the parameter value
						StringExpression paramValue = new StringExpression();
						paramValue.setLitValue(parsePropagationRule(newDataType));
						String paramKey = "OutDataTypeMode";

						// create a new BlockParameter which is to be read
						// during the type propagation, between the typing and
						// the validation
						Parameter param = new BlockParameter();
						param.setName(paramKey);
						param.setValue(paramValue);
						
						// TODO: (TF:561) Modify this when back propagation 
						// will be ok
						GADataType inputType = 
								block.getInDataPorts().get(0).getDataType();
						block.getOutDataPorts().get(0).setDataType(inputType);
						return true;
					}

					// An expression that evaluates to a data type,
					// for example, float('single')
					else if (newDataType.contains("(")
							&& newDataType.contains(")")) {
						GADataType datatype = DataTypeAccessor
								.getDataType(newDataType);
						if (datatype != null) {
							block.getOutDataPorts().get(0)
									.setDataType(datatype);
							return true;
						} else {
							error = true;
						}
					}

					// The name of a built-in data type, for example, single
					else if (newDataType.equals("single")
							|| newDataType.equals("double")
							|| newDataType.equals("int8")
							|| newDataType.equals("uint8")
							|| newDataType.equals("int16")
							|| newDataType.equals("uint16")
							|| newDataType.equals("int32")
							|| newDataType.equals("uint32")
							|| newDataType.equals("boolean")) {
						// check the input port primitive type
						String boolOrComplex = "real";
						
						if (block.getInDataPorts().get(0).getDataType()
								.getPrimitiveType() instanceof TComplexNumeric) {
							boolOrComplex = "complex";
						}

						// get the corresponding data type
						TPrimitive primitivetype = DataTypeAccessor
								.parsePrimitiveType(newDataType, 
										"real".equals(boolOrComplex));
						GADataType inputType = block.getInDataPorts().get(0)
								.getDataType();

						// if not null assign it to the output port
						if (primitivetype != null) {
							// if vector or matrix as input, copy it and set
							// primitive type else assign primitive type 
							// directly
							if (inputType instanceof TArray) {
								TArray outputType = (TArray) inputType.getCopy();
								outputType.setBaseType(primitivetype);
								block.getOutDataPorts().get(0).setDataType(
										outputType);
							} else {
								block.getOutDataPorts().get(0).setDataType(
										primitivetype);
							}
							return true;
						} else {
							// else raise an error event.
							error = true;
						}

						return true;
					} else {
						error = true;
					}

				} else {
					error = true;
				}

				// if error while reading
				if (error) {
					EventHandler
							.handle(
									EventLevel.ERROR,
									"DataTypeConversionTyper - Typing error",
									"",
									"Block "
											+ block.getReferenceString()
											+ " 's Output data type parameter could not be read",
									"");
					return false;
				}

				return true;

			} else {
				// if input data port or output data port hasn't correct size ->
				// error event.
				EventHandler
						.handle(
								EventLevel.ERROR,
								"DataTypeConversionTyper - Typing error",
								"",
								"DataTypeConversion block "
										+ block.getReferenceString()
										+ "'s input data port or output data port list has not correct size.",
								"");
			}
		} else {
			// if no input data port or output data port list defined -> error
			// event.
			EventHandler
					.handle(
							EventLevel.ERROR,
							"DataTypeConversionTyper - Typing error",
							"",
							"DataTypeConversion block "
									+ block.getReferenceString()
									+ "'s input data port or output data port list is not defined.",
							"");
		}
		return false;

	}

	/**
	 * Check that the output port has a datatype.
	 * 
	 * @param block
	 *            the block which is to be checked.
	 * @return true if the validation is ok.
	 */
	public boolean validateTypes(Block block) {

		if (block.getOutDataPorts().get(0).getDataType() == null) {
			EventHandler.handle(EventLevel.ERROR,
					"DataTypeConversionTyper - Validation error", "", "Block "
							+ block.getReferenceString()
							+ " has at least one untyped port.", "");
			return false;
		}

		return true;
	}

	/**
	 * If a propagation rule is defined instead of an explicit type then create
	 * a parameter OutDataTypeMode which will be read by the typer.
	 * 
	 * @param propagationValue
	 *            the propagation rule which is to be read.
	 * @return the value to put in new BlockParameter OutDataTypeMode.
	 */
	private String parsePropagationRule(String propagationRule) {
		String result = "";

		return result;
	}
}
