/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/MinMaxTyper.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import java.util.ArrayList;
import java.util.List;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of Min and Max Blocks of a
 * GASystemModel.
 * 
 */
public class MinMaxTyper extends BlockTyper {

    /**
     * Assign the same type to the output port as the input port's. if the input
     * port has no datatype, then the block must be typed according to a
     * propagation rule, which is mandatorily stored in its parameter
     * OutDataTypeMode.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block block) {

        // Checks the correct number of input ports
        Assert.assertNotEmpty(block.getInDataPorts(),
                "Input data ports list is null or empty : "
                        + block.getReferenceString());
        Assert.assertNotEmpty(block.getOutDataPorts(),
                "Output data ports list is null or empty : "
                        + block.getReferenceString());

        GADataType signalType = null;
        
        if (block.getOutDataPorts().size() == 1) {
            signalType = computeSignalType(block);
        } else {
            // No input port error.
            EventHandler.handle(EventLevel.ERROR,
                    "MinMaxTyper - Typing error", "",
                    "Invalid number of output ports : "
                            + block.getReferenceString(), "");
            return false;
        }
        
     // if no type is returned, exit the function
        if (signalType == null) {
            return false;
        }

        Parameter paramOutDataTypeMode = block
                .getParameterByName("OutDataTypeMode");

        String outDataTypeMode = ((StringExpression) paramOutDataTypeMode.getValue()).getLitValue();

        GADataType assignedType = null;
        GADataType specifiedType = null;
        if (outDataTypeMode.equals("Inherit via internal rule")) {
            assignedType = signalType;
        } else if (outDataTypeMode.equals("Specify via dialog")) {

            Parameter paramOutDataType = block
                    .getParameterByName("OutDataType");

            if (paramOutDataType != null) {
                String outDataType = ((StringExpression) paramOutDataType.getValue()).getLitValue();

                specifiedType = DataTypeAccessor
                        .getDataType(outDataType, false);

                if (specifiedType == null) {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                            .getClass().getCanonicalName(), "",
                            "Specified type: " + outDataType + " in block: "
                                    + block.getReferenceString()
                                    + "is not valid.");
                    return false;
                } else {
                    if (signalType instanceof TArray) {
                        ((TArray) signalType)
                                .setBaseType((TPrimitive) specifiedType);
                        assignedType = signalType;
                    } else {
                        assignedType = specifiedType;
                    }
                }
            } else {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                        .getCanonicalName(), "", "No specified type in block: "
                        + block.getReferenceString() + ".");
                return false;
            }
        } else {
            specifiedType = DataTypeAccessor
                    .getDataType(outDataTypeMode, false);

            if (specifiedType == null) {
                assignedType = signalType;
            } else {
                if (signalType instanceof TArray) {
                    ((TArray) signalType)
                            .setBaseType((TPrimitive) specifiedType);
                    assignedType = signalType;
                } else {
                    assignedType = specifiedType;
                }
            }
        }
        block.getOutDataPorts().get(0).setDataType(assignedType);

        return true;
    }

    private GADataType computeSignalType(Block block) {
        
        GADataType outDataType = null;
        if (DataTypeAccessor.checkTyping(block.getInDataPorts())) {
            // Retrieves the OutDataType parameter
            for (Parameter param : block.getParameters()) {
                if (param.getName().equals("OutputDataType")) {
                    outDataType = DataTypeAccessor
                            .getDataType(((StringExpression) param.getValue()).getLitValue());
                }
            }
            if (outDataType == null) {
                // the typer assigns to the output port the same datatype as
                // the
                // inputs.
                outDataType = block.getInDataPorts().get(0).getDataType();
            }
            // if there is a single matrix or vector input
            if (outDataType instanceof TArray) {
                if (block.getInDataPorts().size() == 1) {
                    // output is scalar and data type is primitive type
                    outDataType = ((TArray) outDataType).getBaseType();
                }
            }
            TArray vector = null;
            TArray matrix = null;
            Expression lengthExpr = null;
            Expression length1Expr = null;
            Expression length2Expr = null;
            TPrimitive primType = null;
            Boolean isVector = false;
            Boolean isMatrix = false;
            if (DataTypeAccessor.hasComplexTypeInput(block)) {
                if (block.getInDataPorts().size() > 1) {
                    for (InDataPort currentInPort : block.getInDataPorts()) {
                        if (currentInPort.getDataType() instanceof TArray) {
                            if (currentInPort.getDataType().isMatrix()) {
                                for (Expression exp : currentInPort
                                        .getDataType().getDimensions()) {
                                    if ((exp.evalReal() != null)
                                            && (exp.evalReal()
                                                    .getRealValue() == 1)) {
                                        isVector = true;
                                    }else{
                                    	isMatrix = true;
                                    }
                                }
                            } else if (currentInPort.getDataType().isVector()) {
                                isVector = true;
                            } else {
                                // Unknown port type.
                                EventHandler
                                        .handle(
                                                EventLevel.ERROR,
                                                "MinMaxTyper - Typing error",
                                                "",
                                                block.getReferenceString()
                                                        + " has a port which has unknown complex type.",
                                                "");
                            }
                            if (isMatrix && isVector){
                            	EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "MinMaxTyper - Typing error",
                                        "",
                                        block.getReferenceString()
                                                + " has a combination of inputs matrices and vectors types that is not allowed.",
                                        "");
                            } else if (isVector) {
                                lengthExpr = ((TArray) currentInPort
                                        .getDataType()).getDimensions()
                                        .get(0).getCopy();
                                primType = ((TArray) currentInPort
                                        .getDataType()).getPrimitiveType();
                                vector = new TArray();
                                vector.setBaseType(primType);
                                vector.addDimension(lengthExpr);
                            } else if (isMatrix){
                            	if (matrix == null){
                            		length1Expr = ((TArray) currentInPort
                                            .getDataType()).getDimensions()
                                            .get(0).getCopy();
                            		length2Expr = ((TArray) currentInPort
                                            .getDataType()).getDimensions()
                                            .get(1).getCopy();
                                    primType = ((TArray) currentInPort
                                            .getDataType()).getPrimitiveType();
                                    matrix = new TArray();
                                    matrix.setBaseType(primType);
                                    matrix.addDimension(length1Expr);
                                    matrix.addDimension(length2Expr);
                            	}else{
                            		if (!DataTypeUtils.checkDimensions(
                            				matrix, 
                            				currentInPort.getDataType(), 
                            				false, false)) {
                            			EventHandler
                                        .handle(
                                                EventLevel.ERROR,
                                                "MinMaxTyper - Typing error",
                                                "",
                                                block.getReferenceString()
                                                        + " has a combination of matrices inputs types that are not compatibles.",
                                                "");
                            		}
                            	}
                            } else {
                                // Unsupported port type.
                                EventHandler
                                        .handle(
                                                EventLevel.ERROR,
                                                "MinMaxTyper - Typing error",
                                                "",
                                                block.getReferenceString()
                                                        + " has a port which has a forbidden complex type.",
                                                "");
                            }
                        }
                    }
                }
            }

            if (vector != null) {
                outDataType = vector;
            }
        } else {
            // Untyped input port error.
            EventHandler.handle(EventLevel.ERROR,
                    "MinMaxTyper - Typing error", "", block
                            .getReferenceString()
                            + " has untyped input ports or input ports"
                            + "which have different data types.", "");
            return null;
        }
        return outDataType;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "MinMaxTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
        }

        return result;
    }

}
