/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/DataStoreReadTyper.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check data types of DataStoreRead block
 * 
 */
public class DataStoreReadTyper extends BlockTyper {

    /**
     * DataStoreRead has no input and one output.
     * The output must have the same data type as the corresponding 
     * DataStoreMemory block.
     * If DataStoreMemory block is not found an error is raised 
     * 
     * @param block
     *            The block to be typed.
     */
    public boolean assignTypes(Block block) {

        // checking number of outputs
    	Assert.assertSingleElement(block.getOutDataPorts(), 
    			"block outputs", 
    			this.getClass().getCanonicalName() + ".assignTypes", 
    			block.getReferenceString(), "");
        
        // variable containing the data-store value
        Expression varRef = null;
        
        // the block must be source block
        if (block instanceof SourceBlock) {
        	varRef = ((SourceBlock) block).getVariableReference();
        } 
        
        if (varRef == null) {
            EventHandler.handle(
                    EventLevel.ERROR,
                    this.getClass().getCanonicalName(),
                    "assingTypes",
                    "Missing reference to data store variable. \n Block: "
                            + block.getReferenceString(),
                    "");
            return false;
        }

        GADataType varDt = varRef.getDataType();
        if (varDt == null) {
            EventHandler.handle(
                    EventLevel.ERROR,
                    this.getClass().getCanonicalName(),
                    "assingTypes",
                    "Data store varaible data type unknown. \n Block: "
                            + block.getReferenceString(),
                    "");
            return false;        	
        } else {
	        // assign variable data type to block output
	        block.getOutDataPorts().get(0).setDataType(varDt);
        }
        
        return true;
    }

    /**
     * No verification is done.
     */
    public boolean validateTypes(Block block) {
        return true;
    }

}
