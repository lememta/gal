/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/DemuxTyper.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.utilities.CodeModeUtilities;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of Demux Blocks of a GASystemModel.
 * 
 */
public class DemuxTyper extends BlockTyper {

    /**
     * Types the output port of the Demux block according to the parameter
     * Outputs.
     */
    public boolean assignTypes(Block block) {

    	/* 
    	 * CHECKS
    	 */
    	// Demux block must have exactly one input
    	Assert.assertSingleElement(block.getInDataPorts(), 
    				"InDataPorts",
    				this.getClass().getCanonicalName() + ".assignTypes", 
    				block.getReferenceString(), null);
    	
    	// Demux block must have one or more outputs
    	Assert.assertNotEmpty(block.getOutDataPorts(), 
				"OutDataPorts",
				this.getClass().getCanonicalName() + ".assignTypes", 
				block.getReferenceString(), null);

        // get the input port:
        InDataPort inport = block.getInDataPorts().get(0);

    	// the input port must be typed
    	Assert.assertNotNull(inport.getDataType(), 
				"input port is not typed",
				this.getClass().getCanonicalName() + ".assignTypes", 
				block.getReferenceString(), null);
    	
    	/*
    	 * PROCESSING
    	 */
        // plain datatype of the input port
        TPrimitive primitive = null;
        // length of the input vector (1 in case of scalar)
        int length = 0;

        // determine the input type
        if (inport.getDataType() instanceof TPrimitive) {
        	// in case of primitive type the output will have the 
        	// same type
            primitive = (TPrimitive) inport.getDataType();
            length = 1;
        } else if (inport.getDataType().isVector()) {
        	// in case of vector the output will have the base type of 
        	// vector 
            length = inport.getDataType().getDimensionsLength(0);
            primitive = inport.getDataType().getPrimitiveType();
        } else if (inport.getDataType().isVectorMatrix()) {
        	// in case of row or column matrix, it is reduced to vector
            length = inport.getDataType()
            					.normalize(true)
            					.getDimensionsLength(0);        	
            primitive = inport.getDataType().getPrimitiveType();        	
        } else {
            raiseError("Typing error", block.getReferenceString()
                    + " has unsupported input datatype : "
                    + inport.getDataType() + ".");

            return false;
        }

        // Read and preprocess the "Outputs" parameter
        Parameter paramOutputs = block.getParameterByName("Outputs");
        Assert.assertNotNull(paramOutputs, "Parameter 'Outputs' not defined : "
                + block.getReferenceString());
        Expression value = paramOutputs.getValue();
        int[] outputDimensions = CodeModeUtilities.expressionToIntArray(value);
        if (outputDimensions == null) {
        	EventHandler.handle(EventLevel.ERROR, getClass().getName(), "", 
        			"Unexpected kind of expression in parameter \"Outputs\"" + 
        					value.getReferenceString());
        	return false;
        }

        // if number of outputs is a scalar value
        if (outputDimensions.length == 1) {
            int nboutputs = 0;

            // get the output ports dimensions
            for (int i = 0; i < outputDimensions.length; i++) {
                nboutputs += outputDimensions[i];
            }

            // if only one output port, same type as input port.
            if (nboutputs == 1) {
                block.getOutDataPorts().get(0)
                        .setDataType(
                                block.getInDataPorts().get(0)
                                        .getDataType());
            }

            // if same number of inputs and outputs,
            // each output port is assigned a scalar data type
            else if (nboutputs == length) {
                for (OutDataPort outport : block.getOutDataPorts()) {
                    outport.setDataType(primitive);
                }
            }

            // if more outputs than inputs, error
            else if (nboutputs > length) {
                raiseError("Typing error", 
                		"\n " + block.getReferenceString()
                        + "\n has more output ports (" + nboutputs
                        + ") than there is input values (" + length
                        + ").");
                return false;
            }

            // p vector signals :
            // - first m vector signals'
            // size is (n/p)+1 and primitive type is the input
            // port's primitive type.
            // - other vector signals'
            // size is (n/p) and primitive type is the input port's
            // primitive type.

            else {

                int modulo = length % nboutputs;
                int sizeBig = length / nboutputs + 1;
                int sizeSmall = length / nboutputs;

                for (OutDataPort outport : block.getOutDataPorts()) {
                    if (block.getOutDataPorts().indexOf(outport) < modulo) {
                        TArray vector = new TArray(
                                new IntegerExpression(sizeBig),
                                primitive);
                        outport.setDataType(vector);
                    } else {
                        TArray vector = new TArray(
                                new IntegerExpression(sizeSmall),
                                primitive);
                        outport.setDataType(vector);
                    }
                }
            }
        } else {
            // if it is a vector, then assign to each output port
            // a vector datatype of size outputDimensions[i]

            for (int i = 0; i < outputDimensions.length; i++) {
                if (outputDimensions[i] > 1) {
                    TArray vector = new TArray(
                            new IntegerExpression(
                                    outputDimensions[i]), primitive);
                    block.getOutDataPorts().get(i).setDataType(
                            vector);
                } else {
                    block.getOutDataPorts().get(i).setDataType(
                            primitive);
                }
            }
        }
        
        return true;
    }

    /**
     * Check that all ports of the block are typed.
     */
    public boolean validateTypes(Block block) {
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            raiseError("Validation error", block.getReferenceString()
                    + "'s outDataPort is not typed.");
            return false;
        }

        for (InDataPort port : block.getInDataPorts()) {
            if (port.getDataType() == null) {
                raiseError("Validation error", block.getReferenceString()
                        + "'s inport " + port.getPortNumber()
                        + " is not typed.");
                return false;
            }
        }

        return true;
    }

    /**
     * Method which raises an error event, this avoid copy-pastes of code ...
     * 
     * @param message
     *            the message which is to be printed.
     */
    private void raiseError(String place, String message) {
        EventHandler.handle(EventLevel.ERROR, this.getClass()
                .getCanonicalName(), "", "Demux block " + message, "");
    }

}
