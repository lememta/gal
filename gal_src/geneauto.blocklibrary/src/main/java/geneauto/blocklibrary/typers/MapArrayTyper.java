/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/MapArrayTyper.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of ReadMapArray Blocks of a
 * GASystemModel.
 * 
 */
public class MapArrayTyper extends BlockTyper {

    /**
     * Assign the second input port data type to the output port data type
     */
    public boolean assignTypes(Block block) {
        // Checks the correct number of input ports
        if (DataTypeAccessor.checkNbIO(block, 3, 1)) {
            if (DataTypeAccessor.checkTyping(block.getInDataPorts())) {
                GADataType inputType = block.getInDataPorts().get(1)
                        .getDataType();
                block.getOutDataPorts().get(0).setDataType(inputType);
                // Output has been typed information.
                EventHandler.handle(EventLevel.DEBUG, this.getClass()
                        .getCanonicalName(), "", block.getReferenceString()
                        + "'s output port was assigned type "
                        + DataTypeAccessor.toString(block.getOutDataPorts()
                                .get(0).getDataType()), "");
            } else {
                // Untyped input port error.
                EventHandler.handle(EventLevel.ERROR,
                        "MapArrayTyper - Typing error", "", block
                                .getReferenceString()
                                + " has untyped input ports.", "");
                return false;
            }
            return true;
        } else {
            // No input port error.
            EventHandler.handle(EventLevel.ERROR,
                    "MapArrayTyper - Typing error", "", block
                            .getReferenceString()
                            + " must have 3 inputs and 1 output.", "");
            return false;
        }
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "MapArrayTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
        }

        return result;
    }

}
