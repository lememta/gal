/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/PulseTyper.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of PulseOne and PulseTwo Blocks of
 * a GASystemModel.
 * 
 * 
 */
public class PulseTyper extends BlockTyper {

    /**
     * Pulse (1 or 2) has one main input (and two optional input that aren't
     * useful for typing process) and one output. Its output type must be
     * boolean. If the input type is null, then an error is raised.
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are well defined
        if (block.getInDataPorts() != null
                && block.getOutDataPorts() != null
                && block.getOutDataPorts().size() == 1
                && ((block.getInDataPorts().size() == 1) || (block
                        .getInDataPorts().size() == 3))) {

            // we check that the input port is typed.
            for (InDataPort inport : block.getInDataPorts()) {
                if (inport.getDataType() == null) {

                    // if no input datatype -> error event.
                    EventHandler.handle(EventLevel.ERROR,
                            "PulseTyper - Typing error", "", "Pulse block "
                                    + block.getReferenceString()
                                    + "'s input port is not typed.", "");

                    return false;

                }
            }

            // we check if all inputs have boolean data type

            for (int i = 0; i < block.getInDataPorts().size(); i++) {

                if (!(block.getInDataPorts().get(i).getDataType() instanceof TBoolean)) {

                    // if no input datatype -> error event.
                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "PulseTyper - Input type error",
                                    "",
                                    "Pulse block "
                                            + block.getReferenceString()
                                            + "'s optinal input ports haven't boolean data type.",
                                    "");
                }
            }

            // else we assign to the out data port boolean data type
            block.getOutDataPorts().get(0).setDataType(new TBoolean());

            EventHandler.handle(EventLevel.DEBUG, this.getClass()
                    .getCanonicalName(), "", block.getReferenceString()
                    + "'s output port was assigned type "
                    + DataTypeAccessor.toString(block.getOutDataPorts().get(0)
                            .getDataType()), "");

            return true;
        } else {
            // if no input data port or output data port list defined -> error
            // event.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "PulseTyper - Typing error",
                            "",
                            "Pulse block "
                                    + block.getReferenceString()
                                    + "'s input data port or output data port list is not well defined.",
                            "");
        }
        return false;
    }

    /**
     * We check that the output port has boolean data type
     * 
     * @param block
     *            the block which is being checked.
     */
    public boolean validateTypes(Block block) {
        GADataType outputType = block.getOutDataPorts().get(0).getDataType();

        if (!DataTypeAccessor.hasBooleanDataType(outputType)) {
            EventHandler.handle(EventLevel.ERROR,
                    "PulseTyper - Validation error", "", "Pulse block "
                            + block.getReferenceString()
                            + "hasn't a boolean datatype on its output port.",
                    "");
            return false;
        }
        return true;
    }

}
