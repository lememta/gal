/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/MultiPortSwitchTyper.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.blocklibrary.utils.BlockLibraryUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.assertions.Assert;

import java.util.List;

/**
 * Derive types for the MultiPortSwitch block
 * 
 * TODO: (TF:617) extend typing to take into account user-defined output type
 */
public class MultiPortSwitchTyper extends BlockTyper {

    /**
     * MultiPortSwitch's first input (the control input)
     * must be a numeric scalar value. 
     * 
     * The following inputs can be of any compatible data data type:
     * 	- in case of scalars any combination of numeric types is allowed
     *  - in case of mixture of scalars and arrays, the output is array, and
     *  scalar expansion is applied to scalar inputs
     *  - in case of several array inputs they all must have the same dimension
     * 
     * @param block
     *            The block which is to be typed.
     * @return true if all was OK
     */
    public boolean assignTypes(Block block) {

    	// CHECKING PART
        // checking block validity
        if (!block.getType().equals("MultiPortSwitch")) {
            EventHandler.handle(EventLevel.ERROR, 
            		getClass().getCanonicalName(),
                    "", "Block type error : " + block.getReferenceString(), "");
        }

        List<InDataPort> inputs = block.getInDataPorts();
        List<OutDataPort> outputs = block.getOutDataPorts();
        
        // block must have one output
        Assert.assertSingleElement(outputs, "block outputs", 
        		getClass().getCanonicalName(), block.getReferenceString(), "");

        // block must have two or more inputs
        Assert.assertNPlusElements(inputs, 2, "block inputs", 
        		getClass().getCanonicalName(), block.getReferenceString(), "");

        // all inputs must be typed
        if (!(BlockLibraryUtil.inputsAreTyped(block, 
        								getClass().getCanonicalName()))) {
        	return false;
        }

        // the first input must be numeric
        GADataType controlInputDT = inputs.get(0).getDataType(); 
        if (!(controlInputDT instanceof TRealNumeric) && 
        		!(controlInputDT instanceof TBoolean)) {
             EventHandler.handle(EventLevel.ERROR,
            		getClass().getCanonicalName(), "",
                    	"The control input must be scalar numeric, provided" +
                    	"data type is" 
                    	+ DataTypeAccessor.toString(controlInputDT)
                    	+ "\n Block: " + block.getReferenceString(), "");
            
            return false;
        }
        
    	// PROCESSING PART
        GADataType toAssign = null;

        
        if (block.getInDataPorts().size() == 2) {
        // two inputs -- the data input must be vector and the output must be 
        // scalar
        	GADataType inType = inputs.get(1).getDataType();
        	if (inType.isVector()) {
        		// the output is element of this vector
        		toAssign = inType.getPrimitiveType();
        	} else {
                EventHandler.handle(EventLevel.ERROR,
                        "MultiPortSwitchTyper", "",
                        	"In case of single data input the type of this" +
                        	"input must be vector!\n Provided data type: "
                        	+ DataTypeAccessor.toString(inType)
                        	+ "\n Block: " + block.getReferenceString(), "");
                
                return false;        		
        	}	
        } else {
    	/* multiple inputs -- the output type must be common data type
    	 * of the data inputs  
    	 */
        	GADataType inType = null;
        	GADataType tmpType = null;
        	// loop all ports starting index 1 (excludes the control port)
        	for (int i = 1; i < inputs.size(); i++) {
        		inType = inputs.get(i).getDataType();
        		if (inType == null) {
                    EventHandler.handle(EventLevel.ERROR,
                            "MultiPortSwitchTyper", "",
                            	"Untyped input port " + i
                            	+ "\n Block: " + block.getReferenceString(), "");
                    
                    return false;        		        			
        		}
        		
        		/* if toAssing is null just take the type from port */
        		if (toAssign == null) {
        			toAssign = inType; 
        		} else if (toAssign.isScalar()) {
        			/* find common type of last output type candidate and
        			 * the current input. The result is NULL when types are 
        			 * incompatible (in compatible base type or different 
        			 * matrix dimensions 
        			 */
            		tmpType = DataTypeUtils.chooseLeastCommonType(toAssign, 
            													inType, true);
            		if (tmpType == null) {
                        EventHandler.handle(EventLevel.ERROR,
                                "MultiPortSwitchTyper", "",
                                "Unable to type output! Data type  "
                                + inType.toString()
                                + " of port " + i 
                                + "is incompatible with the type "
                                + toAssign.toString()
                                + " derived from previous ports." 
                                	+ "\n Block: " + block.getReferenceString(), "");
                        
                        return false;        		        			            			
            		} else {
            			toAssign = tmpType;
            		}
        		}
        	}
        }
        

        if (toAssign != null) {
            outputs.get(0).setDataType(toAssign);
            return true;
        } else {
            EventHandler.handle(EventLevel.ERROR,
                    "MultiPortSwitchTyper", "",
                    "Unable to type block output!" 
                   + "\n Block: " + block.getReferenceString(), "");
            return false;
        }
    }

    /**
     * Checks if all ports are typed
     * 
     * @param block
     *            the block which is being checked.
     * @return true if the block is correctly typed.
     */
    public boolean validateTypes(Block block) {
        return BlockLibraryUtil.portsAreTyped(block, 
        			this.getClass().getCanonicalName());
    }
}
