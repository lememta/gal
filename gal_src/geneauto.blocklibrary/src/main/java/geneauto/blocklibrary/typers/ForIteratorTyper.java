/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ForIteratorTyper.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of ForIterator Blocks of a
 * GASystemModel.
 * 
 * 
 */
public class ForIteratorTyper extends BlockTyper {

    /**
     * ForIterator has an optional input and one output. Its output port must
     * have scalar signal type and real or integer data type, depending on the
     * value of the block parameter 'OutputDataType'
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are well defined
        if (block.getOutDataPorts() != null) {

            if ((block.getInDataPorts().size() <= 2)
                    && (block.getOutDataPorts().size() == 1)) {

                // we read the block parameter
                Parameter paramType = block
                        .getParameterByName("OutputDataType");

                if (paramType == null) {
                    // depending on the designing tool used, block parameter
                    // should have different name
                    paramType = block
                            .getParameterByName("IterationVariableDataType");
                }

                // if block parameter is not defined and block has no input,
                // apply int32 by default
                if (paramType == null) {
                    block.getOutDataPorts().get(0).setDataType(
                            new TRealInteger(32, false));

                    return true;
                }

                GADataType type = DataTypeAccessor
                        .getDataType(((StringExpression) paramType.getValue()).getLitValue());

                if ((type instanceof TRealInteger)
                        || (type instanceof TRealDouble)) {

                    // we assign parameter-defined type to the output port
                    block.getOutDataPorts().get(0).setDataType(type);

                } else {
                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "ForIteratorTyper - Typing error",
                                    "",
                                    "ForIterator block "
                                            + block.getReferenceString()
                                            + "'s parameter 'OutPutDataType' hasn't an expected value (must be double, int8, int16 or int 32).",
                                    "");
                }

                EventHandler.handle(EventLevel.DEBUG, this.getClass()
                        .getCanonicalName(), "", block.getReferenceString()
                        + "'s output port was assigned type "
                        + DataTypeAccessor.toString(block.getOutDataPorts()
                                .get(0).getDataType()), "");
                return true;
            } else {
                // if input data port or output data port hasn't correct size ->
                // error event.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "ForIteratorTyper - Typing error",
                                "",
                                "ForIterator block "
                                        + block.getReferenceString()
                                        + "'s output data port list has not correct size.",
                                "");
            }
        } else {
            // if no output data port list defined -> error event.
            EventHandler.handle(EventLevel.ERROR,
                    "ForIteratorTyper - Typing error", "", "ForIterator block "
                            + block.getReferenceString()
                            + "'s output data port list is not defined.", "");
        }
        return false;
    }

    /**
     * We check that the output port type is double or int.
     * 
     * @param block
     *            the block which is being checked.
     */
    public boolean validateTypes(Block block) {
        GADataType type = block.getOutDataPorts().get(0).getDataType();

        if ((type instanceof TRealDouble) || (type instanceof TRealInteger)) {
            return true;
        }

        EventHandler.handle(EventLevel.ERROR,
                "ForIteratorTyper - Validation error", "", "ForIterator block "
                        + block.getReferenceString()
                        + "'s has not expected output data type", "");

        return false;
    }

}
