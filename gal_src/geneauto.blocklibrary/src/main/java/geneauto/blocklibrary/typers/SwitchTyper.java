/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/SwitchTyper.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.blocklibrary.utils.BlockLibraryUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.assertions.Assert;

import java.util.List;

/**
 * This class allows to type and check types of Switch Blocks of a
 * GASystemModel.
 * 
 */
public class SwitchTyper extends BlockTyper {

    /**
     * Switch block second input (the control input) must be a numeric 
     * value (scalar or vector). 
     * 
     * First and second inputs must follow those rules:
     * 	- in case of scalars any combination of numeric types is allowed
     *  - in case of mixture of scalars and arrays, the output is array, and
     *  scalar expansion is applied to scalar inputs
     *  - in case of several array inputs they all must have the same dimension
     * 
     * @param block
     *            The block which is to be typed.
     * @return true if all was OK
     */
    public boolean assignTypes(Block block) {

    	// CHECKING PART
        // checking block validity
        if (!block.getType().equals("Switch")) {
            EventHandler.handle(EventLevel.ERROR, getClass().getCanonicalName(),
                    "", "Block type error : " + block.getReferenceString(), "");
        }

        List<InDataPort> inputs = block.getInDataPorts();
        List<OutDataPort> outputs = block.getOutDataPorts();
        
        // block must have one output
        Assert.assertSingleElement(outputs, "block outputs", 
        		getClass().getCanonicalName(), block.getReferenceString(), "");

        // block must have three inputs
        Assert.assertNPlusElements(inputs, 2, "block inputs", 
        		getClass().getCanonicalName(), block.getReferenceString(), "");

        // all inputs must be typed
        if (!(BlockLibraryUtil.inputsAreTyped(block, 
        		getClass().getCanonicalName()))) {
        	return false;
        }
        
        // the second input must be numeric or boolean
        {
            // NB! The check is placed in separate scope on purpose 
        	// -- to make sure that controlInputDT is not used afterwards
	        GADataType controlInputDT = inputs.get(1).getDataType();
	        // check dimensions
	        if (controlInputDT instanceof TArray) {
	        	if (!(controlInputDT.isVector())) {
	                EventHandler.handle(EventLevel.ERROR,
	                		getClass().getCanonicalName(), "",
	                        	"Only vectors are allowed as non-scalar control " +
	                        	"inputs, provided data type is " 
	                        	+ DataTypeAccessor.toString(controlInputDT)
	                        	+ "\n Block: " + block.getReferenceString(), "");
	                
	                return false;        		
	        	} else {
	        		// force the next check to apply to the base type
	        		controlInputDT = ((TArray) controlInputDT).getBaseType();
	        	}
	        }
	        // check datatype
	        if (!(controlInputDT instanceof TRealNumeric) && 
	        		!(controlInputDT instanceof TBoolean)) {
	            EventHandler.handle(EventLevel.ERROR,
	            		getClass().getCanonicalName(), "",
	                    	"The control input must be numeric, provided" +
	                    	" data type is " 
	                    	+ DataTypeAccessor.toString(controlInputDT)
	                    	+ "\n Block: " + block.getReferenceString(), "");
	            
	            return false;
	        }
        }

    	GADataType inType1 = inputs.get(0).getDataType();
    	GADataType inType3 = inputs.get(2).getDataType();

    	// both data inputs must be either scalar or have the same dimensions
    	if (!(inType1.isScalar() || inType3.isScalar())) {
    		if (!(DataTypeUtils.checkDimensions(inType1, inType3, false, false))) {
                EventHandler.handle(EventLevel.ERROR,
                        "SwitchTyper - Typing error", "",
                        	"In case of non-scalar data inputs, "
                        	+ "their dimensions must be equal"
                        	+ "\n port 1: " + inType1.toString() 
                        	+ "\n port 3: " + inType3.toString() 
                        	+ "\n Block: " + block.getReferenceString(), "");
    			
    		}
    	}
    	
    	// PROCESSING PART
        GADataType toAssign = null;

        
    	/* find output type that suits both inputs */
        toAssign = DataTypeUtils.chooseLeastCommonType(inType1, inType3, true);         

        if (toAssign != null) {
            outputs.get(0).setDataType(toAssign);
            return true;
        } else {
            EventHandler.handle(EventLevel.ERROR,
                    "SwitchTyper", "",
                    "Unable to type block output!" 
                   + "\n Block: " + block.getReferenceString(), "");
            return false;
        }
    }

    /**
     * Checks if all ports are typed
     * 
     * @param block
     *            the block which is being checked.
     * @return true if the block is correctly typed.
     */
    public boolean validateTypes(Block block) {
        return BlockLibraryUtil.portsAreTyped(block, 
        			this.getClass().getCanonicalName());
    }
}
