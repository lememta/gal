/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/TrigonometryTyper.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TComplexDouble;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealFloatingPoint;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of Trigonometry Blocks of a
 * GASystemModel.
 * 
 */
public class TrigonometryTyper extends BlockTyper {

    /**
     * Assign the input data type to the output datatype
     */
    public boolean assignTypes(Block block) {
        // Check the correct number of inputs and outputs
        if (DataTypeAccessor.checkNbIO(block, 1, 1)
                || DataTypeAccessor.checkNbIO(block, 2, 1)) {
            if (DataTypeAccessor.checkTyping(block.getInDataPorts())) {
                GADataType inputType = block.getInDataPorts().get(0)
                        .getDataType();
                if (inputType instanceof TRealFloatingPoint) {
                    block.getOutDataPorts().get(0).setDataType(inputType);
                    // Output has been typed information.
                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + " - "
                            + block.getId()
                            + " - "
                            + block.getName()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()), "");
                } else if (inputType instanceof TArray) {
                    TPrimitive primType = ((TArray) inputType)
                            .getPrimitiveType();
                    if (primType instanceof TRealDouble
                            || primType instanceof TComplexDouble) {
                        block.getOutDataPorts().get(0).setDataType(inputType);
                        // Output has been typed information.
                        EventHandler.handle(EventLevel.DEBUG, this.getClass()
                                .getCanonicalName(), "", block
                                .getReferenceString()
                                + " - "
                                + block.getId()
                                + " - "
                                + block.getName()
                                + "'s output port was assigned type "
                                + DataTypeAccessor
                                        .toString(block.getOutDataPorts()
                                                .get(0).getDataType()), "");
                    } else {
                        // No input/output port error.
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "TrigonometryTyper - Typing error",
                                        "",
                                        block.getReferenceString()
                                                + " - "
                                                + block.getId()
                                                + " - "
                                                + block.getName()
                                                + " has a forbidden input port data type.",
                                        "");
                        return false;
                    }
                } else {
                    // No input/output port error.
                    EventHandler.handle(EventLevel.ERROR,
                            "TrigonometryTyper - Typing error", "", block
                                    .getReferenceString()
                                    + " - "
                                    + block.getId()
                                    + " - "
                                    + block.getName()
                                    + " has a forbidden input port data type.",
                            "");
                    return false;
                }
            } else {
                // Untyped input port error.
                EventHandler.handle(EventLevel.ERROR,
                        "TrigonometryTyper - Typing error", "",
                        block.getReferenceString() + " - " + block.getId()
                                + " - " + block.getName()
                                + " has untyped input ports.", "");
                return false;
            }
        } else {
            // No input/output port error.
            EventHandler.handle(EventLevel.ERROR,
                    "TrigonometryTyper - Typing error", "", block
                            .getReferenceString()
                            + " - "
                            + block.getId()
                            + " - "
                            + block.getName()
                            + " must have 1 input and 1 output ports.", "");
            return false;
        }
        return true;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "TrigonometryTyper - Validation error", "", "Block "
                            + block.getReferenceString() + " - "
                            + block.getId() + " - " + block.getName()
                            + " has at least one untyped port.", "");
        }

        return result;
    }
}
