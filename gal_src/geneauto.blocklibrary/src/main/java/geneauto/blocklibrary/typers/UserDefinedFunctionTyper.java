/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/UserDefinedFunctionTyper.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.FunctionPrototypeParserEML;
import geneauto.utils.assertions.Assert;

public class UserDefinedFunctionTyper extends BlockTyper {

    @Override
    public boolean assignTypes(Block block) {
        String parsedString = "";

        // Check if the block is portless
        if (block.getInDataPorts().size()==0 
        		&& block.getOutDataPorts().size()==0) {
        	// There is nothing to type
        	return true;
        }
        
        // check input type
        for (InDataPort port : block.getInDataPorts()) {
            Assert.assertNotNull(port.getDataType(), "Input data port "
                    + port.getReferenceString() + " is untyped");
        }

        BlockType blockType = block.getBlockType();// TODO: replace by correct
        // blockType
        if (blockType.getFunctionSignature() != null) {
            parsedString = blockType.getFunctionSignature();
        } else {
            Parameter param = block.getParameterByName("SFunctionSpec");
            parsedString = param.getValue().getStringValue();
        }
        FunctionPrototypeParserEML funParser = 
        		new FunctionPrototypeParserEML(parsedString, block);
        
        int nbOutput = 0;
        int nbInput = 0;

        for (String s : funParser.arguments) {
            String tmpS = cleanFunArgument(s);

            if (tmpS.startsWith("u")) {
                nbInput++;
            }
            if (tmpS.startsWith("y")) {
                nbOutput++;
            }
        }

        // check in function signature if there's no more arguments referring to
        // input/output than input/output count
        if ((block.getInDataPorts().size() < nbInput)
                || (block.getOutDataPorts().size() < nbOutput)) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		this.getClass().getCanonicalName(), "",
                    "Number of inputs ("
            				+ block.getInDataPorts().size()
            				+ ") or outputs (" 
            				+ block.getOutDataPorts().size()
            				+ ")\n of block "
                            + block.getReferenceString()
                            + "\n does not match with function signature \"" 
                            + parsedString + "\"",
                            "");
            return false;
        }

        GADataType dt;
        for (OutDataPort currentPort : block.getOutDataPorts()) {
            dt = funParser.getOutputPortType(currentPort.getPortNumber());
            if (dt != null) {
                currentPort.setDataType(dt);
            } else {
                EventHandler.handle(EventLevel.ERROR, 
                		this.getClass().getCanonicalName(), "",
                        "Unable to determine port type for output port " 
	                		+ currentPort.getPortNumber() 
	                        + " \"" + currentPort.getName() 
                				+ ")\n in block "
                                + block.getReferenceString()
                                + "\n from signature \"" 
                                + parsedString + "\"",
                                "");
                return false;
            }
        }

        /* TODO: (TF:559) reconsider this. Inputs must already have type when we 
         * get here. If we do here something then it is checking compliance of 
         * function signature and externally derived type

        for (InDataPort currentPort : block.getInDataPorts()) {
            dt = funParser.getInputPortType(currentPort.getPortNumber());
            if (dt != null) {
                currentPort.setDataType(funParser.getInputPortType(currentPort
                        .getPortNumber()));
            } else {
                EventsHandler.handle(EventLevel.ERROR, 
                		this.getClass().getCanonicalName(), "",
                        "Unable to determine port type for input port " 
	                		+ currentPort.getPortNumber() 
	                        + " \"" + currentPort.getName() 
                				+ ")\n in block "
                                + block.getReferenceString()
                                + "\n from signature \"" 
                                + parsedString + "\"",
                                "");
                return false;
            }
        }
        */
        return true;
    }

    private String cleanFunArgument(String s) {
        String tmpS = "";
        if (s.startsWith("uint32")) {
            tmpS = s.replace("uint32 ", "");
        } else if (s.startsWith("uint8")) {
            tmpS = s.replace("uint8 ", "");
        } else if (s.startsWith("uint16")) {
            tmpS = s.replace("uint16 ", "");
        } else if (s.startsWith("int32")) {
            tmpS = s.replace("int32 ", "");
        } else if (s.startsWith("int8")) {
            tmpS = s.replace("int8 ", "");
        } else if (s.startsWith("int16")) {
            tmpS = s.replace("int16 ", "");
        } else if (s.startsWith("double")) {
            tmpS = s.replace("double ", "");
        } else if (s.startsWith("single")) {
            tmpS = s.replace("single ", "");
        } else {
            tmpS = s;
        }
        return tmpS;
    }

    @Override
    public boolean validateTypes(Block block) {
        return true;
    }

}
