/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/BusSelectorTyper.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.StructureContent;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * This class allows to type and check types of BusSelectorTyper Blocks of a
 * GASystemModel.
 * 
 */
public class BusSelectorTyper extends BlockTyper {

    /**
     * Parses a CustomType into single types and assigns these types to the
     * outputs of the block. if the input port has no datatype, then the block
     * must be typed according to a propagation rule, which is mandatorily
     * stored in its parameter OutDataTypeMode (TODO (TF564): PROPAGATION RULE
     * NOT SUPPORTED FOR NOW).
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block block) {
        // Checks the correct number of input ports
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null
                && block.getInDataPorts().size() == 1
                && block.getOutDataPorts().size() >= 1) {
            if (DataTypeAccessor.checkTyping(block.getInDataPorts())) {
                // Checks the input data type is TCustom
                if (block.getInDataPorts().get(0).getDataType() instanceof TCustom) {
                    Boolean muxedOutput = null;
                    Vector<String> outputSignals = null;
                    // Retrieves the MuxedOutput and OutputSignals parameter
                    // values
                    for (Parameter param : block.getParameters()) {
                        if (param.getName().equals("MuxedOutput")
                                || param.getName().equals("OutputAsBus")) {
                            muxedOutput = param.getName()
                                    .equals(
                                            ((StringExpression) param.getValue()).getLitValue());
                        }
                        if (param.getName().equals("OutputSignals")) {
                            outputSignals = parseOutputSignals(((StringExpression) param.getValue()).getLitValue());
                        }
                    }

                    // Retrieves the TCustom type of the input port
                    TCustom type = (TCustom) block.getInDataPorts().get(0)
                            .getDataType();
                    // Retrieves the StructureMemberContent of the TCustom
                    CustomType_SM custom_sm = (CustomType_SM) type
                            .getCustomType();

                    StructureContent content = (StructureContent) custom_sm
                            .getContent();
                    // If MuxedOutput is set to "on" and number of outDataPort
                    // is 1
                    if ((muxedOutput != null) && (muxedOutput)
                            && (block.getOutDataPorts().size() == 1)) {
                        // Rebuilds the memberList
                        List<StructureMember> memberList = new ArrayList<StructureMember>();
                        // For each selected signal index, ...
                        for (String currentIndex : outputSignals) {
                            // Adds the corresponding signal to the new
                            // memberList
                            memberList.add(content.getStructMembers().get(
                                    Integer.parseInt(currentIndex)));
                        }
                        // Affects the new memberList
                        content.setStructMembers(memberList);
                        // Set outDataPort data type
                        block.getOutDataPorts().get(0).setDataType(type);
                        // Output has been typed information.
                        EventHandler.handle(EventLevel.INFO, this.getClass()
                                .getCanonicalName(), "", block
                                .getReferenceString()
                                + "'s output port was assigned type "
                                + DataTypeAccessor
                                        .toString(block.getOutDataPorts()
                                                .get(0).getDataType()), "");
                    } else {
                        // For each selected signal index, ...
                        for (int i = 0; i < outputSignals.size(); i++) {

                            try {
                                StructureMember member = content
                                        .getStructMembers().get(
                                                Integer.parseInt(outputSignals
                                                        .get(i)) - 1);
                                // Sets the outDataPort data type according to
                                // the
                                // selected signal data types
                                block.getOutDataPorts().get(i).setDataType(
                                        member.getDataType());
                            } catch (NumberFormatException nbE) {

                                String signalName = outputSignals.get(i)
                                        .replace("<", "").replace(">", "");
                                SystemBlock parent = (SystemBlock) block
                                        .getParent();
                                InDataPort inport = block.getInDataPorts().get(
                                        0);

                                StructureMember member;
                                for (Signal sig : parent.getSignals()) {
                                    member = ((TCustom) inport
                                            .getDataType())
                                            .getCustomType()
                                            .getMemberByName(
                                                    signalName);
                                    if (member == null) {
                                        EventHandler.handle(EventLevel.ERROR,
                                                "BusSelectorTyper - Typing error", "", block
                                                        .getReferenceString()
                                                        + " - "
                                                        + block.getId()
                                                        + " - "
                                                        + "member with name " + signalName + " was not found in "
                                                        + "following customType:"
                                                        + ((TCustom) inport.getDataType())
                                                            .getCustomType().getReferenceString(),
                                                "");
                                        return false;
                                    }
                                    if (inport.equals(sig.getDstPort())) {
                                        block
                                                .getOutDataPorts()
                                                .get(i)
                                                .setDataType(member.getDataType());
                                        break;
                                    }
                                }
                            }

                            // Output has been typed information.
                            EventHandler
                                    .handle(
                                            EventLevel.DEBUG,
                                            "BusSelectorTyper",
                                            "",
                                            block.getReferenceString()
                                                    + "'s output port was assigned type "
                                                    + DataTypeAccessor
                                                            .toString(block
                                                                    .getOutDataPorts()
                                                                    .get(i)
                                                                    .getDataType()),
                                            "");
                        }
                    }
                    return true;
                } else {
                    // Wrong input port type error.
                    EventHandler.handle(EventLevel.ERROR,
                            "BusSelectorTyper - Typing error", "",
                            "Unable to type block "
                                    + block.getReferenceString()
                                    + ". Input port must have TCustom type, ",
                            "");
                    return false;
                }
            } else {
                // Untyped input port error.
                EventHandler.handle(EventLevel.ERROR,
                        "BusSelectorTyper - Typing error", "", block
                                .getReferenceString()
                                + " has untyped input ports.", "");
                return false;
            }
        } else {
            // No input port error.
            EventHandler.handle(EventLevel.ERROR,
                    "BusSelectorTyper - Typing error", "", block
                            .getReferenceString()
                            + " - "
                            + block.getId()
                            + " - "
                            + block.getName()
                            + " has invalid number of input and output ports.",
                    "");
            return false;
        }
    }

    /**
     * Parses the OutputSignals parameter
     * 
     * @param value
     *            , the parameter value
     * @return a vector of String containing the selected signal indexes
     */
    private Vector<String> parseOutputSignals(String value) {
        Vector<String> portIndexes = new Vector<String>();
        value = value.replace("signal", "");
        while (value.indexOf(',') != -1) {
            int i = value.indexOf(',');
            portIndexes.add(value.substring(0, i));
            value = value.substring(i + 1, value.length());
        }
        portIndexes.add(value);
        return portIndexes;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }

        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "BusCreatorTyper - Validation error", "", "Block "
                            + block.getReferenceString() + " - "
                            + block.getId() + " - " + block.getName()
                            + " has at least one untyped port.", "");
        }

        return result;
    }

}
