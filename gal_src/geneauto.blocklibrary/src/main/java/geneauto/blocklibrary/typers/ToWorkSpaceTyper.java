/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ToWorkSpaceTyper.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of ToWorkSpace Blocks of a
 * GASystemModel.
 * 
 */
public class ToWorkSpaceTyper extends BlockTyper {
    /**
     * Cannot type the output port of ToWorkSpace blocks because they do not
     * have output ports. Just check the correct number of inputs.
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean assignTypes(Block block) {
        if (!DataTypeAccessor.checkNbIO(block, 2, 0)) {
            // No input/output port error.
            EventHandler.handle(EventLevel.ERROR,
                    "ToWorkSpaceTyper - Typing error", "", block
                            .getReferenceString() + " - " + block.getId() + " - " + block.getName() 
                            + " must have 2 inputs and no output.", "");
            return false;
        }
        return true;
    }

    /**
     * As ToWorkSpace blocks do not have output ports, only the two input ports
     * have to be checked
     */
    public boolean validateTypes(Block block) {
        boolean result = true;
        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        return result;
    }
}
