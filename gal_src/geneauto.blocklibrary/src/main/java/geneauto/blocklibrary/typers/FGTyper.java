/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/FGTyper.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of F and G Blocks of a
 * GASystemModel.
 * 
 * 
 */
public class FGTyper extends BlockTyper {

    /**
     * F and G blocks have one inputs and one output. Its output type must be
     * the same as its input type. If an input type is null, then an error is
     * raised.
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are defined
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null) {

            // we check that the input and output port list have correct size
            if (block.getOutDataPorts().size() == 1
                    && ((block.getInDataPorts().size() == 2) || (block
                            .getInDataPorts().size() == 1))) {

                // we check that the input port is typed
                if (block.getInDataPorts().get(0).getDataType() == null) {

                    EventHandler.handle(EventLevel.ERROR,
                            "FGTyper - Typing error", "", "F/G block "
                                    + block.getReferenceString()
                                    + "'s input port is not typed.", "");

                }

                // check if input has real data type and scalar signal
                // type
                if (!(block.getInDataPorts().get(0).getDataType() instanceof TRealNumeric)) {

                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "FGTyper - Typing error",
                                    "",
                                    "F/G block "
                                            + block.getReferenceString()
                                            + "'s input port hasn't expected type (it must be have real data type and scalar signal type).",
                                    "");
                }

                // we assign to the output same type than the input
                block.getOutDataPorts().get(0).setDataType(
                        block.getInDataPorts().get(0).getDataType());

                EventHandler.handle(EventLevel.DEBUG, this.getClass()
                        .getCanonicalName(), "", block.getReferenceString()
                        + "'s output port was assigned type "
                        + DataTypeAccessor.toString(block.getOutDataPorts()
                                .get(0).getDataType()), "");

                return true;
            } else {
                // if input data port or output data port hasn't correct size ->
                // error event.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "FGTyper - Typing error",
                                "",
                                "F/G block "
                                        + block.getReferenceString()
                                        + "'s input data port or output data port list has not correct size.",
                                "");
            }
        } else {
            // if no input data port or output data port list defined -> error
            // event.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "FGTyper - Typing error",
                            "",
                            "F/G block "
                                    + block.getReferenceString()
                                    + "'s input data port or output data port list is not defined.",
                            "");
        }
        return false;
    }

    /**
     * We check that the output port has same data type than input port.
     */
    public boolean validateTypes(Block block) {
        GADataType inputType = block.getInDataPorts().get(0).getDataType();
        GADataType outputType = block.getOutDataPorts().get(0).getDataType();

        if (!inputType.getTypeName().equals(outputType.getTypeName())) {
            EventHandler.handle(EventLevel.ERROR,
                    "FGTyper - Validation error", "", "F/G block "
                            + block.getReferenceString()
                            + "'s has not the same datatype on"
                            + " its input and output ports.", "");
            return false;
        }
        return true;
    }

}
