/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/GainTyper.java,v $
 *  @version	$Revision: 1.33 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of Gain Blocks of a GASystemModel.
 * 
 */
public class GainTyper extends BlockTyper {

    /**
     * Assign the same type to the output port as the input port's. if the input
     * port has no datatype, then the block must be typed according to a
     * propagation rule, which is mandatorily stored in its parameter
     * OutDataTypeMode.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are defined
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null) {

            // we check that the input and output port list have correct size
            if (block.getOutDataPorts().size() == 1
                    && block.getInDataPorts().size() == 1) {

                // tests the values of the input ports.
                if (block.getInDataPorts().get(0).getDataType() == null) {
                    // if the input port is not typed, we check the parameter
                    // OutDataTypeMode.
                    Parameter param = block
                            .getParameterByName("OutDataTypeMode");
                    boolean error = true;

                    if (!"".equals(((StringExpression) param.getValue()).getLitValue())) {
                        error = false;
                    }

                    // if the propagation rule is not defined, then the
                    // typer
                    // raises an event level and returns false;
                    if (error) {
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "GainTyper - Typing error",
                                        "",
                                        "Unable to type block "
                                                + block.getReferenceString()
                                                + ". Input port is not typed, "
                                                + "and no propagation rule is defined.");
                        return false;
                    }

                } else {
                    // the typer use BinaryOperator class to calculate a type
                    // according to input type, gain value type and operator
                    Parameter gainParam = block.getParameterByName("Gain");
                    Parameter mulModeParam = block
                            .getParameterByName("Multiplication");

                    String mulMode = "";

                    if (mulModeParam != null) {
                        mulMode = ((StringExpression) mulModeParam.getValue()).getLitValue();
                    }

                    Expression gainExp = gainParam.getValue();
                    GADataType type = null;

                    if (gainExp.getDataType() == null) {
                        String msg = "Error determining the type of parameter \"Gain\"";
                        if (gainExp instanceof VariableExpression) {
                            if (((VariableExpression) gainExp).getVariable() == null) {
                                msg = "\n Unresolved reference: "
                                        + ((VariableExpression) gainExp)
                                                .getName();
                            }
                        }
                        msg += "\n Block: " + block.getReferenceString();
                        EventHandler.handle(EventLevel.ERROR,
                                "GainTyper - Typing error", "", msg);
                        return false;
                    }

                    GADataType gainDTCpy = gainExp.getDataType().getCopy();
                    GADataType inputDTCpy = block.getInDataPorts().get(0)
                            .getDataType().getCopy();

                    if (mulMode.equals("")
                            || mulMode.equals("Element-wise(K.*u)")) {
                        // default case is Element-wise product
                        /*
                         * In the Element-wise case user inputs like [1, 2, 3]
                         * or [1; 2; 3] are treated as vectors (1-dimensional
                         * array).
                         */
                        if (gainDTCpy.isVectorMatrix()) {
                            // In the element-wise case we treat the parameter
                            // expression
                            // as a vector, if it is a vector-matrix.
                            // Convert the parameter expression
                            gainExp = gainExp.normalizeShape(true);
                            gainParam.setValue(gainExp);
                            // Refresh the datatype
                            gainDTCpy = gainExp.getDataType().getCopy();
                        }
                        BinaryOperator op = BinaryOperator.MUL_OPERATOR;
                        type = op.getDataType(gainDTCpy, inputDTCpy, false);

                    } else if (mulMode.equals("Matrix(K*u)")) {
                        // Matrix product case with Gain value in left branch
                        BinaryOperator op = BinaryOperator.MUL_MAT_OPERATOR;
                        type = op.getDataType(gainDTCpy, inputDTCpy, false);

                    } else if (mulMode.equals("Matrix(u*K)")) {
                        // Matrix product case with Gain value in right branch
                        BinaryOperator op = BinaryOperator.MUL_MAT_OPERATOR;
                        type = op.getDataType(inputDTCpy, gainDTCpy, false);
                    } else if (mulMode.equals("Matrix(K*u) (u vector)")) {

                        // Scalar product case with vector input value
                        if ((inputDTCpy instanceof TArray)
                                && ((TArray) inputDTCpy).getDimensionality() == 2) {
                            EventHandler
                                    .handle(
                                            EventLevel.CRITICAL_ERROR,
                                            this.getClass().getCanonicalName(),
                                            "",
                                            "In block : "
                                                    + block
                                                            .getReferenceString()
                                                    + ", Matrix input type is not allowed with the option 'Matrix(K*u) (u vector)'");
                        }

                        BinaryOperator op = BinaryOperator.MUL_MAT_OPERATOR;
                        type = op.getDataType(gainDTCpy, inputDTCpy, false);
                    }

                    if (type != null) {
                        block.getOutDataPorts().get(0).setDataType(type);
                    } else {
                        EventHandler.handle(EventLevel.ERROR, this.getClass()
                                .getCanonicalName(), "",
                                "Input type and gain parameter type are not compatible."
                                        + "\nBlock: "
                                        + block.getReferenceString());
                    }

                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()));
                }

                return true;

            } else {
                // if input data port or output data port hasn't correct size ->
                // error event.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "GainTyper - Typing error",
                                "",
                                "Gain block "
                                        + block.getReferenceString()
                                        + "'s input data port or output data port list has not correct size.");
            }
        } else {
            // if no input data port or output data port list defined -> error
            // event.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "GainTyper - Typing error",
                            "",
                            "Gain block "
                                    + block.getReferenceString()
                                    + "'s input data port or output data port list is not defined.");
        }
        return false;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * The gain's input port and its output port must have the same type.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        GADataType inType = block.getInDataPorts().get(0).getDataType();
        GADataType outType = block.getOutDataPorts().get(0).getDataType();

        // check that the two ports have a datatype
        if ((inType == null) || (outType == null)) {
            EventHandler.handle(EventLevel.ERROR,
                    "GainTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
            return false;
        }

        // check that the gain parameter has the same data type
        // as the input port.
        for (Parameter param : block.getParameters()) {
            if ("Gain".equals(param.getName())) {
                Expression val = param.getValue();
                GADataType gainType = val.getDataType();

                if (gainType != null) {
                    if (!compliantTypes(gainType, inType)) {
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "GainTyper - Validation error",
                                        "",
                                        "Type validation failed for block "
                                                + block.getReferenceString()
                                                + " Block's input is not compliant with the Gain value."
                                                + "\nInput type: "
                                                + inType.toString()
                                                + "\nGain type: "
                                                + gainType.toString(), "");
                    }
                }
            }
        }

        // if all is ok, return true;
        return true;
    }

    /**
     * Method which allows to indicate if the input type of the block is
     * compliant with the value of parameter Gain.
     * 
     * @param gainType
     *            The type of the parameter "gain".
     * @param inType
     *            The type of the input port.
     * @return true if the two types are compliant.
     */
    private boolean compliantTypes(GADataType gainType, GADataType inType) {
        if (gainType != null && inType != null) {
            if (gainType.getDimensionality() == inType.getDimensionality()
                    || gainType.getDimensionality() == inType
                            .getDimensionality() + 1
                    || gainType.getDimensionality() == inType
                            .getDimensionality() - 1 || inType.isScalar()
                    || gainType.isScalar()) {
                return true;
            }
        }
        return false;
    }
}