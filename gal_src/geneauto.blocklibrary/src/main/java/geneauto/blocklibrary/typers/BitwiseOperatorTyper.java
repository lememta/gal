/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/BitwiseOperatorTyper.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TNumeric;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of BitwiseOperator Blocks of a
 * GASystemModel.
 * 
 */
public class BitwiseOperatorTyper extends BlockTyper {

    /**
     * Assign the same type to the output port as the input port's. if the input
     * port has no datatype, then the block must be typed according to a
     * propagation rule, which is mandatorily stored in its parameter
     * OutDataTypeMode (TODO : PROPAGATION RULE NOT SUPPORTED FOR NOW).
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block block) {
        // Checks the correct number of input ports
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null
                && block.getInDataPorts().size() >= 1
                && block.getOutDataPorts().size() == 1) {
            if (DataTypeAccessor.checkSameTyping(block.getInDataPorts())) {
                // check if the first InDataPort have a supported Type
                if (block.getInDataPorts().get(0).getDataType() instanceof TRealInteger
                        || block.getInDataPorts().get(0).getDataType() instanceof TRealNumeric
                        || block.getInDataPorts().get(0).getDataType() instanceof TNumeric
                        || block.getInDataPorts().get(0).getDataType() instanceof TBoolean) {
                    GADataType inType = block.getInDataPorts().get(0)
                            .getDataType();
                    // the typer assigns to the output port the same datatype as
                    // the
                    // input.
                    block.getOutDataPorts().get(0).setDataType(inType);

                    // Output has been typed information.
                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()), "");
                    // check if input is TArray with a correct primitive type
                } else if ((block.getInDataPorts().get(0).getDataType() instanceof TArray)
                        && (((TArray) block.getInDataPorts().get(0)
                                .getDataType()).getBaseType() instanceof TRealInteger
                                || ((TArray) block.getInDataPorts().get(0)
                                        .getDataType()).getBaseType() instanceof TRealNumeric
                                || ((TArray) block.getInDataPorts().get(0)
                                        .getDataType()).getBaseType() instanceof TNumeric || ((TArray) block
                                .getInDataPorts().get(0).getDataType())
                                .getBaseType() instanceof TBoolean)) {
                    GADataType inType = (TArray) block.getInDataPorts().get(0)
                            .getDataType();
                    // the typer assigns to the output port the same datatype as
                    // the
                    // input.
                    block.getOutDataPorts().get(0).setDataType(inType);

                    // Output has been typed information.
                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()), "");

                } else {
                    // Unsupported input type error.
                    EventHandler.handle(EventLevel.ERROR, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + " has unsupported input. Datatype is : "
                            + block.getInDataPorts().get(0).getDataType()
                                    .getReferenceString() + ".", "");
                    return false;
                }
            } else {
                // Untyped input port error.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "BitwiseOperatorTyper",
                                "",
                                block.getReferenceString()
                                        + " has untyped input ports or input ports which have different data types.",
                                "");
                return false;
            }
        } else {
            // No input port error.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "BitwiseOperatorTyper - Typing error",
                            "",
                            block.getReferenceString()
                                    + " has invalid number of input and output ports. ",
                            "");
            return false;
        }

        return true;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * The bitwiseoperator's input ports and its output port must have the same
     * type.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "BitwiseOperatorTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
        }

        return result;
    }
}
