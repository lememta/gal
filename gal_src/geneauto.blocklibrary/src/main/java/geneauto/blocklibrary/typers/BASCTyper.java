/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/BASCTyper.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of BASCR and BASCS Blocks of a
 * GASystemModel.
 * 
 * 
 */
public class BASCTyper extends BlockTyper {

    /**
     * BASC blocks have two main inputs and two optional inputs, and one output.
     * Inputs have all boolean data type and scalar signal type. Its output type
     * must be boolean too. If the input type is null, then an error is raised.
     * 
     * @param block
     *            The block which is to be typed.
     */
    @Override
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are defined
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null) {

            // we check that the input and output port list have correct size
            if (block.getOutDataPorts().size() == 1
                    && ((block.getInDataPorts().size() == 2) || (block
                            .getInDataPorts().size() == 4))) {

                // we check that the input ports are typed.
                for (InDataPort inport : block.getInDataPorts()) {
                    if (inport.getDataType() == null) {
                        // if no input datatype -> error event.
                        EventHandler.handle(EventLevel.ERROR,
                                "BASCTyper - Typing error", "", "BASC block "
                                        + block.getReferenceString()
                                        + "has an untyped input port.", "");
                        return false;
                    }

                    // we check that the input ports have boolean data type and
                    // scalar signal type
                    if (!inport.getDataType().getTypeName().equals("TBoolean")) {

                        // if no input datatype -> error event.
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "BASCTyper - Typing error",
                                        "",
                                        "BASC block "
                                                + block.getReferenceString()
                                                + "'s input port number "
                                                + inport.getPortNumber()
                                                + " has not boolean data type and scalar signal type.",
                                        "");

                        return false;
                    }
                }

                // we assign to the out data port boolean data type
                block.getOutDataPorts().get(0).setDataType(new TBoolean());

                EventHandler.handle(EventLevel.DEBUG, this.getClass()
                        .getCanonicalName(), "", block.getReferenceString()
                        + "'s output port was assigned type "
                        + DataTypeAccessor.toString(block.getOutDataPorts()
                                .get(0).getDataType()), "");

                return true;
            } else {
                // if input data port or output data port hasn't correct size ->
                // error event.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "BASCTyper - Typing error",
                                "",
                                "BASC block "
                                        + block.getReferenceString()
                                        + "'s input data port or output data port list has not correct size.",
                                "");
            }
        } else {
            // if no input data port or output data port list defined -> error
            // event.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "BASCTyper - Typing error",
                            "",
                            "BASC block "
                                    + block.getReferenceString()
                                    + "'s input data port or output data port list is not defined.",
                            "");
        }
        return false;
    }

    /**
     * We check that the output ports have boolean data type.
     * 
     * @param block
     *            the block which is being checked.
     */
    public boolean validateTypes(Block block) {

        for (OutDataPort outport : block.getOutDataPorts()) {

            if (!DataTypeAccessor.hasBooleanDataType(outport.getDataType())) {
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "BASCTyper - Validation error",
                                "",
                                "BASC block "
                                        + block.getReferenceString()
                                        + "'s output port number "
                                        + outport.getPortNumber()
                                        + "has not boolean data type and scalar signal type.",
                                "");
                return false;
            }
        }

        return true;
    }

}
