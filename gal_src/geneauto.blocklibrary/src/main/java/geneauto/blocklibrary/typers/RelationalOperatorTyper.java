/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/RelationalOperatorTyper.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.DataTypeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * This class allows to type and check types of RelationalOperator Blocks of a
 * GASystemModel.
 * 
 */
public class RelationalOperatorTyper extends BlockTyper {

    /**
     * Assign a TBoolean type to the output port data type
     */
    public boolean assignTypes(Block block) {
        // Checks the correct number of input ports
        if (DataTypeAccessor.checkNbIO(block, 2, 1)) {

           /* if (DataTypeAccessor.checkSamePrimitiveTyping(block
                    .getInDataPorts())) {*/
            List<GADataType> dtList = new ArrayList<GADataType>();    
            for (InDataPort inPort: block.getInDataPorts()) {
                if (inPort.getDataType() != null) {
                    dtList.add(inPort.getDataType());
                }
            }
            boolean portDTareCompatible = DataTypeUtils.chooseLeastCommonType(dtList, true) 
                != null;
            if (portDTareCompatible) {

                // we determine signal type of the inputs and we check that all
                // input have same primitive data type
                int nbScalar = 0;
                int nbVector = 0;
                int nbMatrix = 0;

                int bool = 0;
                //GADataType firstType = null;

                // reference type initialization
                if (block.getInDataPorts().get(0).getDataType() instanceof TPrimitive) {
                    //firstType = block.getInDataPorts().get(0).getDataType();
                }
                if (block.getInDataPorts().get(0).getDataType() instanceof TArray) {
                    //firstType = ((TArray) block.getInDataPorts().get(0)
                            //.getDataType()).getBaseType();
                }

                for (InDataPort port : block.getInDataPorts()) {
                    if (port.getDataType() instanceof TPrimitive) {
                        /*if (port.getDataType().getTypeName().equals(
                                firstType.getTypeName())) {*/
                        if (portDTareCompatible) {
                            nbScalar++;
                        } else {
                            bool = 1;
                        }
                    }
                    if (port.getDataType().isVector()) {
                        /*if (((TArray) port.getDataType()).getBaseType()
                                .getTypeName().equals(firstType.getTypeName())) {*/
                        if (portDTareCompatible) {
                            nbVector++;
                        } else {
                            bool = 1;
                        }
                    }
                    if (port.getDataType().isMatrix()) {
                        /*if (((TArray) port.getDataType()).getBaseType()
                                .getTypeName().equals(firstType.getTypeName()))*/ 
                        if (portDTareCompatible) {
                            nbMatrix++;
                        } else {
                            bool = 1;
                        }
                    }
                    if (bool == 1) {
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "RelationalOperatorTyper - Typing error",
                                        "",
                                        "RelationalOperator block "
                                                + block.getReferenceString()
                                                + "'s input must all have same data type.",
                                        "");

                        return false;
                    }
                }

                if (nbScalar == block.getInDataPorts().size()) {
                    // if all input have scalar signal type, assign scalar
                    // signal type
                    block.getOutDataPorts().get(0).setDataType(new TBoolean());

                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()), "");
                    return true;
                }
                if (nbVector + nbScalar == block.getInDataPorts().size()) {
                    // if there is vector inputs, check if all have the same
                    // dimension
                    int dim = 0;
                    for (InDataPort port : block.getInDataPorts()) {
                        if ((dim == 0)
                                && (port.getDataType().isVector())) {
                            dim = Integer
                                    .valueOf(((LiteralExpression) ((TArray) port
                                            .getDataType()).getDimensions()
                                            .get(0)).getLitValue());
                        }
                        if (port.getDataType().isVector()) {
                            if (Integer
                                    .valueOf(((LiteralExpression) ((TArray) port
                                            .getDataType()).getDimensions()
                                            .get(0)).getLitValue()) != dim) {
                                EventHandler
                                        .handle(
                                                EventLevel.ERROR,
                                                "LogicTyper - Typing error",
                                                "",
                                                "Logic block "
                                                        + block
                                                                .getReferenceString()
                                                        + "'s input data port have not same dimension.",
                                                "");
                                return false;
                            }
                        }
                    }

                    // if they have all same dimension, assign this vector
                    // signal type to the output
                    for (InDataPort port : block.getInDataPorts()) {
                        // we find the first port with vector data type
                        if (port.getDataType().isVector()) {

                            TArray type = new TArray();
                            type.addDimension(new IntegerExpression(dim));
                            type.setBaseType(new TBoolean());

                            block.getOutDataPorts().get(0).setDataType(type);

                            EventHandler.handle(EventLevel.DEBUG, this
                                    .getClass().getCanonicalName(), "", block
                                    .getReferenceString()
                                    + "'s output port was assigned type "
                                    + DataTypeAccessor.toString(block
                                            .getOutDataPorts().get(0)
                                            .getDataType()), "");
                            return true;
                        }
                    }
                }
                if (nbMatrix + nbScalar == block.getInDataPorts().size()) {
                    // if there is matrix inputs, check if all have the same
                    // dimension
                    int dimX = 0;
                    int dimY = 0;
                    for (InDataPort port : block.getInDataPorts()) {
                        if ((dimX == 0) && (dimY == 0)
                                && (port.getDataType().isMatrix())) {
                            dimX = Integer
                                    .valueOf(((LiteralExpression) (((TArray) port
                                            .getDataType()).getDimensions()
                                            .get(0))).getLitValue());

                            dimY = Integer
                                    .valueOf(((LiteralExpression) (((TArray) port
                                            .getDataType()).getDimensions()
                                            .get(1))).getLitValue());

                        }
                        if (port.getDataType().isMatrix()) {
                            if ((Integer
                                    .valueOf(((LiteralExpression) (((TArray) port
                                            .getDataType()).getDimensions()
                                            .get(0))).getLitValue()) != dimX)
                                    || (Integer
                                            .valueOf(((LiteralExpression) (((TArray) port
                                                    .getDataType())
                                                    .getDimensions().get(1)))
                                                    .getLitValue()) != dimY)) {
                                EventHandler
                                        .handle(
                                                EventLevel.ERROR,
                                                "LogicTyper - Typing error",
                                                "",
                                                "Logic block "
                                                        + block
                                                                .getReferenceString()
                                                        + "'s input data port have not same dimension.",
                                                "");
                                return false;
                            }
                        }
                    }

                    // if they have all same dimension, assign this matrix
                    // signal type to the output
                    for (InDataPort port : block.getInDataPorts()) {
                        // we find the first port with vector data type
                        if (port.getDataType().isMatrix()) {

                            TArray type = new TArray();

                            List<Expression> dims = new ArrayList<Expression>();
                            dims.add(new IntegerExpression(dimX));
                            dims.add(new IntegerExpression(dimY));

                            type.setDimensions(dims);
                            type.setBaseType(new TBoolean());

                            block.getOutDataPorts().get(0).setDataType(type);

                            EventHandler.handle(EventLevel.DEBUG, this
                                    .getClass().getCanonicalName(), "", block
                                    .getReferenceString()
                                    + "'s output port was assigned type "
                                    + DataTypeAccessor.toString(block
                                            .getOutDataPorts().get(0)
                                            .getDataType()), "");
                            return true;
                        }
                    }
                }
            } else {
                // Untyped input port error.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "RelationalOperatorTyper - Typing error",
                                "",
                                block.getReferenceString()
                                        + " has untyped input ports or input ports which have different data types.",
                                "");
                return false;
            }
        } else {
            // No input/output port error.
            EventHandler.handle(EventLevel.ERROR,
                    "RelationalOperatorTyper - Typing error", "", block
                            .getReferenceString()
                            + " must have 2 inputs and 1 output.", "");
            return false;
        }
        return true;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "RelationalOperatorTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
        }

        return result;
    }

}
