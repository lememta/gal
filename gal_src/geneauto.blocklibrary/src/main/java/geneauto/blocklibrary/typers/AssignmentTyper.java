/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/AssignmentTyper.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2011-07-07 12:23:16 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of Assignment Blocks of a
 * GASystemModel.
 * 
 * 
 */
public class AssignmentTyper extends BlockTyper {
    /**
     * Assignment has two inputs and one output. Its output type must be the
     * same as its first input type. If an input type is null, then an error is
     * raised.
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are defined
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null) {

            // we check that the input and output port list have correct size
            if (DataTypeAccessor.checkNbIO(block, 2, 1)
                    || DataTypeAccessor.checkNbIO(block, 3, 1)
                    || DataTypeAccessor.checkNbIO(block, 4, 1)) {

                // we check that the input ports are typed.
                for (InDataPort inport : block.getInDataPorts()) {
                    if (inport.getDataType() == null) {

                        // if no input datatype -> error event.
                        EventHandler.handle(EventLevel.ERROR,
                                "AssignmentTyper - Typing error", "",
                                "Assignment block "
                                        + block.getReferenceString()
                                        + "'s input port is not typed.", "");

                        return false;

                    }
                }

                // we check that primitive type of the first input is coherent
                // with
                // type of the second input
                if (block.getInDataPorts().get(0).getDataType() instanceof TArray) {
                    if (!((TArray) block.getInDataPorts().get(0).getDataType())
                            .getBaseType().getTypeName().equals(
                                    block.getInDataPorts().get(1).getDataType()
                                            .getTypeName())) {

                        // error : uncompliant datatypes
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "AssignmentTyper - Typing error",
                                        "",
                                        "Block "
                                                + block.getReferenceString()
                                                + "s input ports have different primitive types.",
                                        "");

                        return false;
                    }
                } else if (block.getInDataPorts().get(0).getDataType() instanceof TPrimitive) {

                    if (!block.getInDataPorts().get(0).getDataType()
                            .getTypeName().equals(
                                    block.getInDataPorts().get(1).getDataType()
                                            .getTypeName())) {

                        // error : uncompliant datatypes
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "AssignmentTyper - Typing error",
                                        "",
                                        "Block "
                                                + block.getReferenceString()
                                                + "s input ports have different primitive types.",
                                        "");

                        return false;
                    }

                } else {
                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "AssignmentTyper - Typing error",
                                    "",
                                    "Block "
                                            + block.getReferenceString()
                                            + "s first input ports must have vector or scalar signal type.",
                                    "");

                }

                // we read the OutputDataType parameter
                Parameter paramOutType = block
                        .getParameterByName("OutputDataType");

                if (paramOutType != null) {

                    // we assign to the output the type specified by the
                    // parameter
                    GADataType specifiedType = DataTypeAccessor
                            .getDataType(((StringExpression) paramOutType.getValue()).getLitValue());

                    block.getOutDataPorts().get(0).setDataType(specifiedType);

                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()), "");

                    return true;
                } else {
                    // we assign to the outdataport the same datatype as the
                    // indataport.
                    block.getOutDataPorts().get(0).setDataType(
                            block.getInDataPorts().get(0).getDataType());

                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()), "");

                    return true;
                }
            } else {
                // if input data port or output data port hasn't correct size ->
                // error event.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "AssignmentTyper - Typing error",
                                "",
                                "Assignment block "
                                        + block.getReferenceString()
                                        + "'s input data port or output data port list has not correct size.",
                                "");
            }
        } else {
            // if no input data port or output data port list defined -> error
            // event.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "AssignmentTyper - Typing error",
                            "",
                            "Assignment block "
                                    + block.getReferenceString()
                                    + "'s input data port or output data port list is not defined.",
                            "");
        }
        return true;
    }

    /**
     * We check that the first input and output ports have the same datatype.
     * 
     * @param block
     *            the block which is being checked.
     */
    public boolean validateTypes(Block block) {
        GADataType inputType = block.getInDataPorts().get(0).getDataType();
        GADataType outputType = block.getOutDataPorts().get(0).getDataType();

        if (!inputType.getTypeName().equals(outputType.getTypeName())) {
            EventHandler.handle(EventLevel.ERROR,
                    "AbsTyper - Validation error", "", "Abs block "
                            + block.getReferenceString()
                            + "'s has not the same datatype on"
                            + " its first input and output ports.", "");
            return false;
        }

        return true;
    }
}
