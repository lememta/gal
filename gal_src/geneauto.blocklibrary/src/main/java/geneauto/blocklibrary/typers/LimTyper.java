/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/LimTyper.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of Lim Blocks of a GASystemModel.
 * 
 * 
 */
public class LimTyper extends BlockTyper {

    /**
     * Lim blocks have one main and two optional inputs, and one output. All
     * inputs must be of real data type and scalar signal type, and must have
     * all the same type. Output must have same type than inputs.
     * 
     * @param block
     *            The block which is to be typed.
     */
    @Override
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are defined
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null) {

            // we check that the input and output port list have correct size
            if (block.getOutDataPorts().size() == 1
                    && ((block.getInDataPorts().size() == 1) || (block
                            .getInDataPorts().size() == 3))) {

                // we check if first input hasn't null data type
                if (block.getInDataPorts().get(0).getDataType() != null) {

                    // we check if all inputs have scalar signal type and real
                    // data type
                    for (InDataPort port : block.getInDataPorts()) {

                        if (!(port.getDataType() instanceof TRealNumeric)) {
                            EventHandler
                                    .handle(
                                            EventLevel.ERROR,
                                            "LimTyper - Typing error",
                                            "",
                                            "Lim block "
                                                    + block
                                                            .getReferenceString()
                                                    + "'s input data port hasn't expected type (must be scalar signal type and real data type).",
                                            "");

                            return false;
                        }
                    }

                    // we read the OutputDataType parameter
                    Parameter paramOutType = block
                            .getParameterByName("OutputDataType");

                    if (paramOutType != null) {

                        // we assign to the output the type specified by the
                        // parameter
                        GADataType specifiedType = DataTypeAccessor
                                .getDataType(((StringExpression) paramOutType.getValue()).getLitValue());

                        block.getOutDataPorts().get(0).setDataType(
                                specifiedType);

                        EventHandler.handle(EventLevel.DEBUG, this.getClass()
                                .getCanonicalName(), "", block
                                .getReferenceString()
                                + "'s output port was assigned type "
                                + DataTypeAccessor
                                        .toString(block.getOutDataPorts()
                                                .get(0).getDataType()), "");

                        return true;
                    } else {

                        // we assign to the output the same data type than the
                        // input
                        block.getOutDataPorts().get(0).setDataType(
                                block.getInDataPorts().get(0).getDataType());

                        EventHandler.handle(EventLevel.DEBUG, this.getClass()
                                .getCanonicalName(), "", block
                                .getReferenceString()
                                + "'s output port was assigned type "
                                + DataTypeAccessor
                                        .toString(block.getOutDataPorts()
                                                .get(0).getDataType()), "");

                        return true;
                    }
                } else {
                    // if input data port or output data port hasn't correct
                    // size ->
                    // error event.
                    EventHandler.handle(EventLevel.ERROR,
                            "LimTyper - Typing error", "", "Lim block "
                                    + block.getReferenceString()
                                    + "'s input data port has null data type.",
                            "");
                }

            } else {
                // if input data port or output data port hasn't correct size ->
                // error event.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "LimTyper - Typing error",
                                "",
                                "Lim block "
                                        + block.getReferenceString()
                                        + "'s input data port or output data port list has not correct size.",
                                "");
            }
        } else {
            // if no input data port or output data port list defined -> error
            // event.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "LimTyper - Typing error",
                            "",
                            "Lim block "
                                    + block.getReferenceString()
                                    + "'s input data port or output data port list is not defined.",
                            "");
        }
        return false;
    }

    /**
     * We check that the input and output ports have the same data t ype.
     * 
     * @param block
     *            the block which is being checked.
     */
    @Override
    public boolean validateTypes(Block block) {
        GADataType inputType = block.getInDataPorts().get(0).getDataType();
        GADataType outputType = block.getOutDataPorts().get(0).getDataType();

        if (!inputType.getTypeName().equals(outputType.getTypeName())) {
            EventHandler.handle(EventLevel.ERROR,
                    "LimTyper - Validation error", "", "Lim block "
                            + block.getReferenceString()
                            + "'s has not the same datatype on"
                            + " its first input and output ports.", "");
            return false;
        }

        return true;
    }

}
