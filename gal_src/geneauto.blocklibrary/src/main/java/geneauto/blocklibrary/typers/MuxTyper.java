/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/MuxTyper.java,v $
 *  @version	$Revision: 1.9 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.blocklibrary.utils.BlockLibraryUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.assertions.Assert;

import java.util.List;

/**
 * This class allows to type and check types of Mux Blocks of a GASystemModel.
 * 
 */
public class MuxTyper extends BlockTyper {

    /**
     * Combines inputs of the blocks into a single vector output. 
     * The block must have one or more inputs, all inputs shall be of any 
     * numeric type and must be either vectors or scalars.
     * 
     * The output is vector of all input values. The base type of this vector 
     * is least common supertype of input base types, the dimension is sum of 
     * input dimensions.
     * 
     * TODO: (TF:648) Here we loose the original data type of inputs. 
     * 	The Mux/Demux should operate with structures rather than vectors.
     * 
     * @param block
     *            the block which is being typed.
     *            
     * @return true if the typing was done correctly.
     */
    public boolean assignTypes(Block block) {

    	// CHECKING PART
        // checking block validity
        if (!block.getType().equals("Mux")) {
            EventHandler.handle(EventLevel.ERROR, 
            		getClass().getCanonicalName(),
                    "", 
                    "Block type error : " + block.getReferenceString(), "");
            return false;
        }

        List<InDataPort> inputs = block.getInDataPorts();
        List<OutDataPort> outputs = block.getOutDataPorts();
        
        // block must have one output
        Assert.assertSingleElement(outputs, "block outputs", 
        		getClass().getCanonicalName(), block.getReferenceString(), "");

        // block must have one or more inputs
        Assert.assertNPlusElements(inputs, 1, "block inputs", 
        		getClass().getCanonicalName(), block.getReferenceString(), "");

        // all inputs must be typed
        if (!(BlockLibraryUtil.inputsAreTyped(block, 
        								getClass().getCanonicalName()))) {
        	return false;
        }

        // all inputs must be either scalar or vector numeric types
        boolean checkingSuccess = true;
        GADataType currentDT = null;
        /*
         * 
         * GADataType previousDT = null;*/
        for (InDataPort p : block.getInDataPorts()) {
        	currentDT = p.getDataType();
        	if (currentDT == null) {
        	    EventHandler.handle(EventLevel.ERROR, 
                        getClass().getCanonicalName(),
                        "", 
                        "Input's data type is not defined." 
                        + "\nInput: \n" + p.getReferenceString()
                        + "\nBlock: " + block.getReferenceString(), "");
        	    checkingSuccess =  false;;
        	}
        	/*
        	 * Primitive data types of all inputs must be the same. 
        	 * Otherwise print error
        	 */
        	/* NOTE: this check is to be clarified (AnRo)
        	 * if (previousDT != null 
        	        && !(previousDT.getPrimitiveType().equalsTo(currentDT.getPrimitiveType()))) {
        	    EventHandler.handle(EventLevel.ERROR, 
                        getClass().getCanonicalName(),
                        "", 
                        "Different primitive data types in inputs are not accepted in Mux block." 
                        + "\nPrimitive types: \n" + currentDT.getPrimitiveType().toString()
                        + "\n" + previousDT.getPrimitiveType().toString()
                        + "\nBlock: " + block.getReferenceString(), "");
        	    checkingSuccess =  false;;
        	}*/
        	TPrimitive primitiveType = currentDT.getPrimitiveType();
        	if (currentDT.isScalar() || currentDT.isVector()) {
        		// scalars and vector are accepted
        	} else {
                EventHandler.handle(EventLevel.ERROR, 
                		getClass().getCanonicalName(),
                        "", 
                        "Mux block accepts only scalars and vectors"
                        + "\n Data type " + currentDT.toString()
                        + " in port " + p.getPortNumber()
                        + "\n Block: " + block.getReferenceString(), "");
                checkingSuccess =  false;
        	}
        	
            if (!(primitiveType instanceof TRealNumeric) && 
            		!(primitiveType instanceof TBoolean)) {
                     EventHandler.handle(EventLevel.ERROR, 
                		getClass().getCanonicalName(),
                        "", 
                        "Mux block accpets only numeric inputs."
                        + "\n Data type " + currentDT.toString()
                        + " in port " + p.getPortNumber()
                        + "\n Block: " + block.getReferenceString(), "");
                checkingSuccess =  false;
        	}
            /*
             * 
             * previousDT = currentDT;*/
        }
        
        if (!checkingSuccess) {
        	return false;
        }
        
        // PROCESSING PART
        
        // base type of the output vector
        TPrimitive outBaseType = null;
        // dimensionality of the output vector
        int outDims = 0;
        for (InDataPort p : block.getInDataPorts()) {
        	GADataType dt = p.getDataType(); 
        	if (outBaseType == null) {
        		// first loop, just take the base type
        		outBaseType = dt.getPrimitiveType();
        	} else {
        		// all subsequent loops find common super-type for input types
        		// it is guaranteed that super-type of two primitives is 
        		// primitive
        		outBaseType = (TPrimitive) DataTypeUtils.chooseLeastCommonType(
        										outBaseType, 
        										dt.getPrimitiveType(), 
        										false);
        	}
        	
        	if (dt.getDimensionality() == 0) {
        		// scalar input
        		outDims ++;
        	} else {
        		// vector input, add the lenght of the vector
        		outDims += dt.getDimensionsLength(0);
        	}
        	
        	// after each processign step we must have base type defined
        	if (outBaseType == null) {
                EventHandler.handle(EventLevel.ERROR, 
                		getClass().getCanonicalName(),
                        "", 
                        "Could not find common supertype for input types. " +
                        "\n Stopping at port " + p.getPortNumber()
                        + " with data type " + dt.toString()
                        + "\n Block: " + block.getReferenceString(), "");        		
        	}
        }
        
        // construct output vector
        GADataType outType = new TArray(
        			new IntegerExpression(outDims), 
        			outBaseType); 

        // set output type
        block.getOutDataPorts().get(0).setDataType(outType);
        
        return true;
    }

    /**
     * Checks if all ports are typed
     * 
     * @param block
     *            the block which is being checked.
     * @return true if the block is correctly typed.
     */
    public boolean validateTypes(Block block) {
        return BlockLibraryUtil.portsAreTyped(block, 
        			this.getClass().getCanonicalName());
    }

    
}
