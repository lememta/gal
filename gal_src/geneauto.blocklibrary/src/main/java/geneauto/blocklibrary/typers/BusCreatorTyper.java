/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/BusCreatorTyper.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-11-28 22:44:23 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.StructureContent;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of BitwiseOperator Blocks of a
 * GASystemModel.
 * 
 */
public class BusCreatorTyper extends BlockTyper {

    /**
     * Creates a CustomType containing all the input element types. if the input
     * port has no datatype, then the block must be typed according to a
     * propagation rule, which is mandatorily stored in its parameter
     * OutDataTypeMode (TODO (TF:563) : PROPAGATION RULE NOT SUPPORTED FOR NOW)
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block block) {
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null
                && block.getInDataPorts().size() >= 1
                && block.getOutDataPorts().size() == 1) {
            if (DataTypeAccessor.checkTyping(block.getInDataPorts())) {
                
                CustomType_SM struct;
                GADataType outType = block.getOutDataPorts().get(0).getDataType();
                StructureContent content;
                boolean wasCreated = true;
                /*
                 *  Creates a CustomType for the bus content in case outType 
                 *  is not defined or uses one created before otherwise
                 */
                if ((outType != null)
                        && (outType instanceof TCustom)
                        && ((TCustom) outType).getCustomType() instanceof CustomType_SM){
                    struct = (CustomType_SM) ((TCustom) outType).getCustomType();
                    content = (StructureContent) struct.getContent();
                    wasCreated = false;
                } else {
                    struct = new CustomType_SM();
                    struct.setName("bus_struct_" + block.getName());
                    // Content is a StructureMemberContent because element could
                    // have different types
                    content = new StructureContent();
                    struct.setContent(content);
                }
                
                

                // check if name of the member of the structure are specified
                Parameter inputs = block.getParameterByName("Inputs");
                Assert.assertNotNull(inputs,
                        "Parameter 'Inputs' not defined : "
                                + block.getReferenceString());

                String inputsValue = ((StringExpression) inputs.getValue()).getLitValue();
                String[] values = null;
                if (inputsValue.contains(",")) {
                    inputsValue = inputsValue.replace("'", "");
                    values = inputsValue.split(",");
                }

                // for each input port :
                // check that primitive type is ok
                // add dimensions to outputLength.
                StructureMember member;
                String memberName;
                GADataType inType;
                for (int i = 0; i < block.getInDataPorts().size(); i++) {
                    memberName = null;
                    // define member's name
                    if (values != null) {
                        memberName = values[i];
                    } else {
                        InDataPort inport = block.getInDataPorts().get(i);
                        for (Signal sig : ((SystemBlock) block.getParent())
                                .getSignals()) {
                            if (inport.equals(sig.getDstPort())) {
                                if (sig.getName() != null) {
                                    memberName = sig.getName();
                                } else {
                                    if (sig.getSrcPort().getParent().getName() != null) {
                                        memberName = sig.getSrcPort()
                                        .getParent().getName();
                                    }
                                }

                            }
                        }
                    }
                    if (memberName == null) {
                        memberName = "signal_" + (i + 1);
                    }
                    if (!wasCreated) {
                        member = content.getMemberByName(memberName);
                    } else {

                        // Adds a structure member with the input port type
                        member = new StructureMember();
                        member.setName(memberName);
                        content.addStructureMember(member);
                    }
                    
                    // set member's new data type in case of new custom type changed 
                    // data type of current member
                    inType = block.getInDataPorts().get(i).getDataType();
                    if (wasCreated || !(inType.equalsTo(member.getDataType()))) {
                        member.setDataType(inType);
                    }
                }

                if (wasCreated) {

                    // Assigns to output port a TCustom type with created CustomType
                    TCustom custom = new TCustom();

                    custom.setParent(block.getOutDataPorts().get(0));
                    custom.setCustomType(struct);

                    struct.setModel(block.getModel());
                    ((SystemBlock) block.getParent()).addCustomType(struct);

                    block.getOutDataPorts().get(0).setDataType(custom);

                    // Output has been typed information.
                    EventHandler.handle(EventLevel.DEBUG, this.getClass()
                            .getCanonicalName(), "", block.getReferenceString()
                            + "'s output port was assigned type "
                            + DataTypeAccessor.toString(block.getOutDataPorts()
                                    .get(0).getDataType()), "");
                }
                

            } else {
                // Untyped input port error.
                EventHandler.handle(EventLevel.ERROR,
                        "BusCreatorTyper - Typing error", "", block
                                .getReferenceString()
                                + " has untyped input ports.", "");
                return false;
            }
        } else {
            // No input/output port error.
            EventHandler.handle(EventLevel.ERROR,
                    "BusCreatorTyper - Typing error", "", block
                            .getReferenceString()
                            + " must have no input and 2 outputs.", "");
            return false;
        }
        return true;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "BusCreatorTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
        }

        return result;
    }

}
