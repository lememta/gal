/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ChartTyper.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2009-11-10 09:58:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;

/**
 * Class to type and typecheck of ChartBlock of GASystemModel.
 */
public class ChartTyper extends BlockTyper {

    /**
     * //TODO complete specs of typing process.
     */
    public boolean assignTypes(Block block) {
		boolean success = true;
		for (InDataPort port : block.getInDataPorts()) {
			success &= checkPortType(port, port.getVariable());
		}
		for (OutDataPort port : block.getOutDataPorts()) {
			success &= setPortType(port, port.getVariable());
		}
		return success;
    }

    private boolean checkPortType(InDataPort port, Variable_SM variable) {
    	if (port.getDataType() != null) {
    		//TODO add comparison of data types here
    		return true;
    	} else if (variable.getDataType()!=null) {
    		port.setDataType(variable.getDataType());
    		return true;
    	} else {
    		return false;
    	}
    }
    
    private boolean setPortType(OutDataPort port, Variable_SM variable) {
    	if (port.getDataType() != null) {
    		//TODO add comparison of data types here
    		return true;
    	// TODO: (to VaKo) check if "variable != null" is correct
        // I added it, because the typer was gettign null variables (ToNa 29/07/08)
    	} else if (variable != null && variable.getDataType()!=null) {
    		port.setDataType(variable.getDataType());
    		return true;
    	} else {
    		return false;
    	}
    }
    
    /**
     * //TODO complete specs of validating process.
     */
    public boolean validateTypes(Block block) {
        return true;
    }
}
