/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/DelayRETyper.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of DelayRE Blocks of a
 * GASystemModel.
 * 
 * 
 */
public class DelayRETyper extends BlockTyper {

    /**
     * DelayRE blocks have five inputs and two output. The third input must have
     * scalar signal type and any data type. Other inputs and outputs can have
     * any signal and data type, but must all have the same one.
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean assignTypes(Block block) {

        // we check that the input and output port list are well defined
        if (block.getInDataPorts() != null && block.getOutDataPorts() != null) {

            if ((block.getInDataPorts().size() == 5)
                    && (block.getOutDataPorts().size() == 2)) {

                // we check that second and third input are scalar
                if (!(block.getInDataPorts().get(2).getDataType() instanceof TPrimitive)) {
                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "DelayRETyper - Typing error",
                                    "",
                                    "DelayRE block "
                                            + block.getReferenceString()
                                            + "'s third input must have scalar signal type.",
                                    "");
                }

                if (!(block.getInDataPorts().get(1).getDataType() instanceof TPrimitive)) {
                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "DelayRETyper - Typing error",
                                    "",
                                    "DelayRE block "
                                            + block.getReferenceString()
                                            + "'s second input must have scalar signal type.",
                                    "");
                }

                // we check that all other inputs same data and signal type
                GADataType type = block.getInDataPorts().get(0).getDataType();

                for (int i = 0; i < block.getOutDataPorts().size(); i++) {
                    if ((i != 2) && (i != 1)) {
                        if (!block.getInDataPorts().get(i).getDataType()
                                .getTypeName().equals(type.getTypeName())) {
                            EventHandler
                                    .handle(
                                            EventLevel.ERROR,
                                            "DelayRETyper - Typing error",
                                            "",
                                            "DelayRE block "
                                                    + block
                                                            .getReferenceString()
                                                    + "'s input port haven't expected type (input 1,2,4 and 5 must have same type).",
                                            "");
                        }
                    }
                }

                // we assign to output ports same data type than inputs
                block.getOutDataPorts().get(0).setDataType(type);
                block.getOutDataPorts().get(1).setDataType(type);

                EventHandler.handle(EventLevel.DEBUG, this.getClass()
                        .getCanonicalName(), "", block.getReferenceString()
                        + "'s output ports was assigned type "
                        + DataTypeAccessor.toString(block.getOutDataPorts()
                                .get(0).getDataType()), "");

                return true;
            } else {
                // if input data port or output data port hasn't correct size ->
                // error event.
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "DelayRETyper - Typing error",
                                "",
                                "DelayRE block "
                                        + block.getReferenceString()
                                        + "'s input data port or output data port list has not correct size.",
                                "");
            }
        } else {
            // if no input data port or output data port list defined -> error
            // event.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "DelayRETyper - Typing error",
                            "",
                            "DelayRE block "
                                    + block.getReferenceString()
                                    + "'s input data port or output data port list is not defined.",
                            "");
        }
        return false;
    }

    /**
     * We check that the output ports have same data type than first input port.
     */
    public boolean validateTypes(Block block) {

        GADataType inputType = block.getInDataPorts().get(0).getDataType();
        GADataType outputType1 = block.getOutDataPorts().get(0).getDataType();
        GADataType outputType2 = block.getOutDataPorts().get(1).getDataType();

        if ((!inputType.getTypeName().equals(outputType1.getTypeName()))
                || ((!inputType.getTypeName().equals(outputType2.getTypeName())))) {
            EventHandler.handle(EventLevel.ERROR,
                    "DelayRETyper - Validation error", "", "DelayRE block "
                            + block.getReferenceString()
                            + "'s has not the same datatype on"
                            + " its first input and output ports.", "");
            return false;
        }

        return true;
    }

}
