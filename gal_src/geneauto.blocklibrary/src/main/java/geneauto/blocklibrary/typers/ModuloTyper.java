/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ModuloTyper.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of Modulo Blocks of a
 * GASystemModel.
 * 
 */
public class ModuloTyper extends BlockTyper {

    /**
     * Assign types according to the parameters OutputSignalType and the inport
     * type of the block. For more information refer to StandardBlockLibrary
     * requirement document : chapter 3.32.1.
     * 
     * @param block
     *            The Block which is to be typed.
     * @returns true if the typing was executed properly.
     */
    public boolean assignTypes(Block block) {

        // Checks the correct number of input ports
        if (DataTypeAccessor.checkNbIO(block, 1, 1)) {
            if (DataTypeAccessor.checkTyping(block.getInDataPorts())) {
                // Retrieves the OutDataType parameter
                GADataType outDataType = null;
                for (Parameter param : block.getParameters()) {
                    if (param.getName().equals("OutputDataType")) {
                        outDataType = DataTypeAccessor
                                .getDataType(((StringExpression) param.getValue()).getLitValue());
                    }
                }
                if (outDataType == null) {
                    // the typer assigns to the output port the same datatype as
                    // the
                    // inputs.
                    outDataType = block.getInDataPorts().get(0).getDataType();
                }
                block.getOutDataPorts().get(0).setDataType(outDataType);

                // Output has been typed information.
                EventHandler.handle(EventLevel.DEBUG, this.getClass()
                        .getCanonicalName(), "",
                        "Output port was assigned type "
                                + DataTypeAccessor
                                        .toString(block.getOutDataPorts()
                                                .get(0).getDataType()) + " : "
                                + block.getReferenceString(), "");
            } else {
                // Untyped input port error.
                EventHandler.handle(EventLevel.ERROR,
                        "ModuloTyper - Typing error", "", block
                                .getReferenceString()
                                + " has untyped input ports.", "");
                return false;
            }
        } else {
            // No input port error.
            EventHandler.handle(EventLevel.ERROR,
                    "ModuloTyper - Typing error", "", block
                            .getReferenceString()
                            + " must have 1 input and 1 output ports.", "");
            return false;
        }
        return true;

    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {

        if (block.getOutDataPorts().get(0).getDataType() == null) {
            raiseError("Validation error", "Output data port is not typed : "
                    + block.getReferenceString());
            return false;
        }

        for (InDataPort port : block.getInDataPorts()) {
            if (port.getDataType() == null) {
                raiseError("Validation error", "Inport " + port.getPortNumber()
                        + " is not typed : " + block.getReferenceString());
                return false;
            }
        }

        return true;
    }

    /**
     * Method which raises an error event, this avoid copy-pastes of code ...
     * 
     * @param message
     *            the message which is to be printed.
     */
    private void raiseError(String place, String message) {
        EventHandler.handle(EventLevel.ERROR, this.getClass()
                .getCanonicalName(), "", "MathFunction block " + message, "");
    }

}
