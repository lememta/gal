/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ZeroOrderHoldTyper.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeAccessor;

/**
 * This class allows to type and check types of ZeroOrderHold Blocks of a
 * GASystemModel.
 * 
 */
public class ZeroOrderHoldTyper extends BlockTyper {

    /**
     * Assign the same type to the output port as the input port's. if the input
     * port has no datatype, then the block must be typed according to a
     * propagation rule, which is mandatorily stored in its parameter
     * OutDataTypeMode.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block block) {
        // tests the values of the input ports.
        if (block.getInDataPorts().get(0).getDataType() == null) {
            // if the input port is not typed, we wheck the parameter
            // OutDataTypeMode.
            for (Parameter param : block.getParameters()) {
                boolean error = true;
                if (param.getName().equals("OutDataTypeMode")) {
                    if (!"".equals(((StringExpression) param.getValue()).getLitValue())) {
                        error = false;
                    }
                }
                // if the propagation rule is not defined, then the typer
                // raises an event level and returns false;
                if (error) {
                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "ZeroOrderHoldTyper - Typing error",
                                    "",
                                    "Unable to type block "
                                            + block.getReferenceString()
                                            + ". Its input port is not typed "
                                            + "and no propagation rule is defined.",
                                    "");
                    return false;
                }
            }
        } else {

            if (!(block.getInDataPorts().get(0).getDataType().isVector() || block
                    .getInDataPorts().get(0).getDataType() instanceof TPrimitive)) {
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "ZeroOrderHoldTyper - Typing error",
                                "",
                                "Block "
                                        + block.getReferenceString()
                                        + "s input type as neither scalar nor vector datatype.",
                                "");
                return false;
            }

            // the typer assigns to the output port the same datatype as the
            // inputs.
            block.getOutDataPorts().get(0).setDataType(
                    block.getInDataPorts().get(0).getDataType());

            EventHandler.handle(EventLevel.DEBUG, this.getClass()
                    .getCanonicalName(), "", block.getReferenceString()
                    + "'s output port was assigned type "
                    + DataTypeAccessor.toString(block.getOutDataPorts().get(0)
                            .getDataType()), "");

        }

        return true;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * The ZeroOrderHold's input port and its output port must have the same
     * type.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        GADataType inType = block.getInDataPorts().get(0).getDataType();
        GADataType outType = block.getOutDataPorts().get(0).getDataType();

        // check that the two ports have a datatype
        if ((inType == null) || (outType == null)) {
            EventHandler.handle(EventLevel.ERROR,
                    "ZeroOrderHoldTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
            return false;
        } else if (!inType.getClass().getSimpleName().equals(
                outType.getClass().getSimpleName())) {
            // check that they have the same datatype.
            EventHandler
                    .handle(
                            EventLevel.ERROR,
                            "ZeroOrderHoldTyper - Validation error",
                            "",
                            "Block "
                                    + block.getReferenceString()
                                    + "'s input and output ports do not have the same datatype.",
                            "");
            return false;
        }

        // if all is ok, return true;
        return true;
    }
}
