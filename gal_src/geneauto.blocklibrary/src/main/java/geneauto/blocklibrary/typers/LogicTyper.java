/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/LogicTyper.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of Logic Blocks of a GASystemModel.
 * 
 * 
 */
public class LogicTyper extends BlockTyper {

    /**
     * Logic has one or more inputs and one output. Its output type depends on
     * the inputs data and signal type. If they have all same signal type,
     * output has this signal type. If they are a mix between scalar and vector
     * (or scalar and matrix) output has same signal type and dimension than
     * vector inputs (or matrix inputs). All vector (or matrix) input must have
     * same dimension.
     * 
     * @param block
     *            The block which is to be typed.
     */
    @Override
    public boolean assignTypes(Block block) {

    	// --- CHECKING ---
    	// logic block must have at least one input
    	Assert.assertNotEmpty(block.getInDataPorts(),
                "block inputs",
                this.getClass().getCanonicalName() + ".assignTypes()",
                block.getReferenceString(),
                "");

    	// logic block must have exactly one output
    	Assert.assertSingleElement(block.getOutDataPorts(),
                "block outputs",
                this.getClass().getCanonicalName() + ".assignTypes()",
                block.getReferenceString(),
                "");

        // get the operator
        Parameter opParam = block.getParameterByName("Operator");
        if (opParam == null 
        		|| opParam.getStringValue() == null
        		|| opParam.getStringValue().isEmpty()) {
            EventHandler.handle(EventLevel.ERROR, 
            		this.getClass().getCanonicalName(), 
                    "",
                    "Parameter 'Operator' not defined for block\n "
                            + block.getReferenceString(), "");
            return false;
        }
        String opStr = opParam.getStringValue();

    	// only scalar and vector signals are allowed
        int portCnt = 0;
    	for (InDataPort p : block.getInDataPorts()) {
    		portCnt++;
    		if (p.getDataType() == null) {
				EventHandler.handle(
                		EventLevel.ERROR,
                        this.getClass().getCanonicalName(), 
                        "",
                        "Untyped input port " + portCnt
                        + "\n Block: "
                        + block.getReferenceString(),
                        "");
    		} else if (p.getDataType() instanceof TCustom) {
				EventHandler.handle(
                		EventLevel.ERROR,
                        this.getClass().getCanonicalName(), 
                        "",
                        "Structures are not allowed as input of this block!. " +
                        " Structure detected in port " + portCnt
                        + "\n Block: "
                        + block.getReferenceString(),
                        "");
    		}
    		
    		// "NOT" operator takes one input only
    		if ("NOT".equals(opStr) && portCnt > 1) {
    				EventHandler.handle(
                		EventLevel.ERROR,
                        this.getClass().getCanonicalName(), 
                        "",
                        "Single input allowed in case of a \"NOT\" operator"
                        + "\n Block: "
                        + block.getReferenceString(),
                        "");
                return false;    			
    		}
    	}
    	// --- PROCESSING ---


        // logical operator always has boolean output type. Dimension still
        // needs to be determined
        GADataType outDataType = new TBoolean();
        
        /* The NOT operator accepts only one input, which can be a scalar,
		   vector or matrix. The output is always boolean with the same 
		   dimension as the input port.
		*/
        if ("NOT".equals(opStr)) {
        	GADataType inDT = block.getInDataPorts().get(0).getDataType(); 
        	if (inDT instanceof TPrimitive) {
        		outDataType = new TBoolean();
        	} else if (inDT instanceof TArray) {
        		// make copy of input data type to get the same dimensions
        		outDataType = inDT.getCopy(); 
        		((TArray) outDataType).setBaseType(new TBoolean());
        	}
        	
        } else if (block.getInDataPorts().size() == 1){
        /*
         * In case of a single input with other operators than NOT the output
         * is always scalar boolean. In case of array types, the specified 
         * operation is performed between all elements
         * 
         * do nothing -- the default type TBoolean applies
         */
        	
        } else {
        /*
         * If the block has more than one input, all nonscalar inputs
         * must have the same dimensions. 
         * 
         * Scalar inputs are expanded to have the same dimensions as the 
         * nonscalar inputs
         */
        	        	
        	for (InDataPort port : block.getInDataPorts()) {
        		if (port.getDataType() instanceof TArray) {
        			if (outDataType instanceof TBoolean) {
        				// first input or all other inputs have been scalars
        				// make copy to get correct dimensions
        				outDataType = port.getDataType().getCopy();
        				// set base type to boolean
        				((TArray) outDataType).setBaseType(new TBoolean());
        				
        			} else {
        				// previous input was already an array
        				// make sure this one has the same dimensions
        				if (!DataTypeUtils.checkDimensions((TArray) outDataType, 
        										(TArray) port.getDataType(), 
        										false, false)) {
        					EventHandler.handle(
        	                		EventLevel.ERROR,
        	                        this.getClass().getCanonicalName(), 
        	                        "",
        	                        "In case of several non-scalar inputs all "
        	                        + "of them must have the same dimensions. "
        	                        + "\n Block: "
        	                        + block.getReferenceString(),
        	                        "");
        					return false;
        				}
        			}
        		} else {
        			// do nothing -- scalar is always OK
        		}
        	}        	
        }
        
        if (outDataType == null) {
            EventHandler.handle(
            		EventLevel.DEBUG,
                    this.getClass().getCanonicalName(), 
                    "",
                    "Unknown error when assigning output type to block\n "
                    + block.getReferenceString(),
                    "");    
            return false;
        }
        
        block.getOutDataPorts().get(0).setDataType(outDataType);
        
        return true;
    }

    @Override
    public boolean validateTypes(Block block) {

        return true;
    }

}
