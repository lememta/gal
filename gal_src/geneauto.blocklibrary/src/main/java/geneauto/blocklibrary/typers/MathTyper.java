/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/MathTyper.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TComplexNumeric;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of MathFunction Blocks of a
 * GASystemModel.
 * 
 */
public class MathTyper extends BlockTyper {

    /**
     * Assign types according to the parameters OutputSignalType, Function and
     * the inport type of the block. For more information refer to
     * StandardBlockLibrary requirement document : chapter 3.32.1.
     * 
     * @param block
     *            The Block which is to be typed.
     * @returns true if the typing was executed properly.
     */
    public boolean assignTypes(Block block) {
        String outputSignalType = "auto";
        String function = "";
        GADataType firstInputType = null;
        
        // tests the values of the parameters Value and OutDataTypeMode
        for (Parameter param : block.getParameters()) {
            if (param.getName().equals("OutputSignalType")) {
                if (!"".equals(((StringExpression) param.getValue()).getLitValue())) {
                    outputSignalType = ((StringExpression) param.getValue()).getLitValue();
                }
            }

            if (param.getName().equals("Function")) {
                if (!"".equals(((StringExpression) param.getValue()).getLitValue())) {
                    function = ((StringExpression) param.getValue()).getLitValue();
                }
            }
        }

        // get the input datatypes.
        firstInputType = block.getInDataPorts().get(0).getDataType();

        // check that the first input type is not null.
        Assert.assertNotNull(firstInputType,
                "Typing error : input port has no datatype"
                        + block.getReferenceString());
        
        // TODO (AnTo 19.07.2012) Rewise the code below. Complex data should not be supported
        // and thus the code is at least partly obsolete        

        // check the "function" parameter
        if ("exp".equals(function) || "log".equals(function)
                || "10^u".equals(function) || "log10".equals(function)
                || "square".equals(function) || "sqrt".equals(function)
                || "pow".equals(function) || "reciprocal".equals(function)
                
                // AnTo 19.07.2012 - Complex operations and data types are actually not supported 
                // || "conjugate".equals(function)
                // || "hermitian".equals(function)
                
        		) {

            // expected typing
            // input
            // auto real complex
            // real real real complex
            // complex complex error complex

            if (firstInputType instanceof TComplexNumeric
                    && "real".equals(outputSignalType)) {

                EventHandler.handle(EventLevel.ERROR, this.getClass()
                        .getCanonicalName(), "", "Input type "
                        + DataTypeAccessor.toString(firstInputType)
                        + " and output type " + outputSignalType
                        + " are not compliant using the function " + function
                        + " : " + block.getReferenceString(), "");

                return false;
            } else {
            	
                if (firstInputType instanceof TRealNumeric
                        && "complex".equals(outputSignalType)) {
                    // switch datatype REAL -> COMPLEX
                    block.getOutDataPorts().get(0).setDataType(
                            DataTypeAccessor.switchRealComplex(firstInputType));
                } else {
                    // assign to output port the indatatype of input port 1
                    block.getOutDataPorts().get(0).setDataType(firstInputType);
                }
                return true;
            }
        }        

        // check the parameters values
        
// AnTo 19.07.2012 - Complex operations and data types are actually not supported
// For real numbers "magnitude^2" = "square". But we reject it now for clarity.        
//        else if ("magnitude^2".equals(function)) {
//
//            // expected typing
//            // input
//            // auto real complex
//            // real real real complex
//            // complex real real complex
//
//            if ("complex".equals(outputSignalType)
//                    && firstInputType instanceof TRealNumeric) {
//
//                // switch datatype REAL -> COMPLEX
//                block.getOutDataPorts().get(0).setDataType(
//                        DataTypeAccessor.switchRealComplex(firstInputType));
//
//            } else if ((("real".equals(outputSignalType)) || ("auto"
//                    .equals(outputSignalType)))
//                    && firstInputType instanceof TComplexNumeric) {
//
//                // switch datatype COMPLEX -> REAL
//                block.getOutDataPorts().get(0).setDataType(
//                        DataTypeAccessor.switchComplexReal(firstInputType));
//
//            } else {
//                // assign to output port the indatatype of input port 1
//                block.getOutDataPorts().get(0).setDataType(firstInputType);
//            }
//            return true;
//        }

        // check the parameters values
        else if ("hypot".equals(function) || "rem".equals(function)
                || "mod".equals(function)) {

            // expected typing
            // input
            // auto real complex
            // real real real complex
            // complex error error error

            if (firstInputType instanceof TComplexNumeric) {

                EventHandler.handle(EventLevel.ERROR, this.getClass()
                        .getCanonicalName(), "", "Input type "
                        + DataTypeAccessor.toString(firstInputType)
                        + " and output type " + outputSignalType
                        + " are not compliant using the function " + function
                        + " : " + block.getReferenceString(), "");

                return false;
            } else {
                if ("complex".equals(outputSignalType)) {

                    // switch datatype REAL -> COMPLEX
                    block.getOutDataPorts().get(0).setDataType(
                            DataTypeAccessor.switchRealComplex(firstInputType));

                } else {

                    // assign to output port the indatatype of input port 1
                    block.getOutDataPorts().get(0).setDataType(firstInputType);
                }

                return true;
            }
            
        } else if ("transpose".equals(function)) {
        	
        	// Limit to only matrices (2D arrays) 
        	if (firstInputType.getDimensionality() != 2) {
                EventHandler.handle(EventLevel.ERROR, this.getClass()
                        .getCanonicalName(), "", 
                              "Block: " + block.getReferenceString() + "\n"
                            + "  The transpose function is applicable to matrices (2D arrays) only."
                            + "  Supplied type: " + firstInputType.toString()
                           );
                return false;
        	} else {
        		TArray arIn = (TArray) firstInputType;
        		TArray outputType = new TArray(arIn.getDimensions().get(1), 
        				arIn.getDimensions().get(0), arIn.getBaseType());
        		block.getOutDataPorts().get(0).setDataType(outputType);
        		return true;
        	}            
            
        // Unknown function
        } else {        	
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", 
                         "Block: " + block.getReferenceString() + "\n"
                       + "  Unsupported Function: " + function);

            return false;
        }
    }

    /**
     * There can be no conflicts between input and output ports of the block
     * Constant, as it has no input port. The only possible conflict is between
     * the value and the propagation rule. We only check that the output port
     * has a datatype.
     */
    public boolean validateTypes(Block block) {

        Assert.assertNotNull(block.getOutDataPorts().get(0).getDataType(),
                "ValidationError : output port not typed : "
                        + block.getReferenceString());

        for (InDataPort port : block.getInDataPorts()) {
            Assert.assertNotNull(port.getDataType(),
                    "ValidationError : input port not typed : "
                            + block.getReferenceString());
        }

        return true;
    }

}
