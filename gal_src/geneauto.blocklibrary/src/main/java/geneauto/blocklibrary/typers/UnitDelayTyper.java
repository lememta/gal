/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/UnitDelayTyper.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:16 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of UnitDelay Blocks of a
 * GASystemModel.
 * 
 */
public class UnitDelayTyper extends BlockTyper {

    /**
     * Assign the same type to the output port as the input port's. 
     * If input type is not assigned the normalised form of type of 
     * InitialValue parameter is used.  
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean assignTypes(Block block) {
    	/*
    	 * CHECKING
    	 */
    	// Unit delay block must have exactly one input
    	Assert.assertSingleElement(block.getInDataPorts(), 
    				"InDataPorts",
    				this.getClass().getCanonicalName() + ".assignTypes", 
    				block.getReferenceString(), null);
    	
    	// Unit delay must have exactly one output
    	Assert.assertSingleElement(block.getOutDataPorts(), 
				"OutDataPorts",
				this.getClass().getCanonicalName() + ".assignTypes", 
				block.getReferenceString(), null);


    	/*
    	 * PROCESSING
    	 */
        // get the input port:
        InDataPort inport = block.getInDataPorts().get(0);
        // slot for memorising new output type
        GADataType outputType = null;
        
        // tests the values of the input port.
        if (inport.getDataType() == null) {
            Parameter initialValue = block.getParameterByName("InitialValue");
            
            if (initialValue != null) {
                Expression value = initialValue.getValue();
		        if (value != null && value.getDataType() != null) {
		        	// assign data type of initial value to block output
		        	// NB! this data type is always normalised. Row or column 
		        	// matrix can be in the output only if this type is derived
		        	// form the block input.
		        	// NB2! getCopy is important here. normalise will move
		        	// dimension expressions from one type to another
		        	outputType = value.getDataType().getCopy().normalize(true);
		        } else {
        			EventHandler.handle(EventLevel.CRITICAL_ERROR, 
        					this.getClass().getCanonicalName(), "",
        					"Can not type block\n "
        					+ block.getReferenceString()
        					+ " unable to determine data type of " 
        					+ "\"InitialValue\" parameter");
        			return false;
		        }            	
            }
            
        } else {
            // the typer assigns to the output port the same datatype as the
            // inputs.
        	outputType = inport.getDataType().getCopy();
        }

        block.getOutDataPorts().get(0).setDataType(outputType);
        return true;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * The block input type should be sub-type of the output type
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean validateTypes(Block block) {
        GADataType inType = block.getInDataPorts().get(0).getDataType();
        GADataType outType = block.getOutDataPorts().get(0).getDataType();

        // check that the two ports have a datatype
        if ((inType == null) || (outType == null)) {
            EventHandler.handle(EventLevel.ERROR,
                    "UnitDelayTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
            return false;
        } else if (!inType.equalsTo(outType)) {
            EventHandler.handle(
                    EventLevel.ERROR,
					this.getClass().getCanonicalName() + ".validateTypes", 
                    "",
                    "Type mismatch in block\n "
                       + block.getReferenceString()
                       + "\n input type (" + inType.toString()
                       + ")\n shall be equal to output type (" 
                       + outType.toString() + ")",
                    "");
            
            return false;
        }

        return true;
    }
}
