/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/MergeTyper.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of Merge Blocks of a GASystemModel.
 * 
 * 
 */
public class MergeTyper extends BlockTyper {

    /**
     * Merge block has few inputs and one output. Inputs have all the same type.
     * Its output type must be the same as its inputs type. If the input type is
     * null, then an error is raised.
     * 
     * @param block
     *            The block which is to be typed.
     */
    @Override
    public boolean assignTypes(Block block) {

        Assert.assertNotEmpty(block.getOutDataPorts(),
                "Output data ports list is null or empty: "
                        + block.getReferenceString());
        
        if (block.getOutDataPorts().get(0).getDataType() != null) {
        	// ouptut port already has type
        	// do nothing. The compatibility of input and output types
        	// is checked in validateTypes
        	return true;
        }
        
    	// find strongest input type
    	GADataType type = mergeInputDataTypes(block, "assignTypes");
    	
    	if (type != null) {
    		block.getOutDataPorts().get(0).setDataType(type);
        
    		EventHandler.handle(EventLevel.DEBUG, 
        		this.getClass().getCanonicalName(), 
        		"", 
        		"Output port was assigned type "
                + DataTypeAccessor.toString(type) + ": "
                + block.getReferenceString(), "");

    		return true;
    	} else {
    		// error is already thrown by mergeInputDataTypes, do nothing
    		return false;
    	}    	
    }

    @Override
    public boolean validateTypes(Block block) {


    	// find strongest input type
    	GADataType inputType = mergeInputDataTypes(block, "validateTypes");

    	if (inputType != null) {
            GADataType outputType = block.getOutDataPorts().get(0).getDataType();
            
	        // construct temporary assignoperator to check type compatibility
	        AssignOperator op = AssignOperator.SIMPLE_ASSIGN;
	        if (!op.checkTypeValidity(inputType, outputType, false)) {
	            EventHandler.handle(EventLevel.ERROR,
	            		this.getClass().getCanonicalName() + ".validateTypes", 
	            		"", 
	            		"Incompatible input and output data types in block \n "
	                            + block.getReferenceString()
	                            + "\n input datatype: "
	                            + DataTypeAccessor.toString(inputType)
	                            + "\n output datatype: "
	                            + DataTypeAccessor.toString(outputType)	                            
	                            );
	            
	            return false;
	        }
	        
	        return true;
    	} else {
    		// error is already thrown by mergeInputDataTypes, do nothing
    		return false;
    	}
    }
    
    /**
     * loops input ports and finds returns strongest type over all input ports
     * returns null and throws error if types are not compatible
     *  
     * @return
     */
    private GADataType mergeInputDataTypes(Block block, String methodName) {
        // we check that the input and output port list are defined
        Assert.assertNotEmpty(block.getInDataPorts(),
                "Input data ports list is null or empty: "
                        + block.getReferenceString());
        Assert.assertNotEmpty(block.getOutDataPorts(),
                "Output data ports list is null or empty: "
                        + block.getReferenceString());

        // we check that the input and output port list have correct size
        if (block.getOutDataPorts().size() == 1) {

        	GADataType prevType = null;
        	InDataPort prevPort = null;
        	
        	for (InDataPort port : block.getInDataPorts()) {
        		if (port.getDataType() == null) {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    		this.getClass().getCanonicalName()
                    		+ "." + methodName, "",
                            "Untyped input port "
                                    + port.getReferenceString(), "");        			
        		}
        		
        		// we ignore this check while on the first port
        		if (prevType != null) {
        			prevType = DataTypeUtils.chooseLeastCommonType(
								prevType, 
								port.getDataType(),
								false);
        			
        			// if did not get the data type, then give error message
            		if (prevType == null) {
                        EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                        		this.getClass().getCanonicalName()
                        		+ "." + methodName, "",
                                "Incompatible data types in ports\n "
                        		+ prevPort.getReferenceString()
                        		+ " " 
                        		+ DataTypeAccessor.toString(prevPort
                        										.getDataType())
                        		+ " and \n "
                        		+ port.getReferenceString()
                        		+ " " 
                        		+ DataTypeAccessor.toString(port.getDataType()), 
                        		"");            			
            		}
        		} else {
        			prevType = port.getDataType();
        		}
        		
        		// remember the processed port for possible error message in 
        		// the next round
        		prevPort = port;
        	}

        	return prevType;

        } else {
            // if input data port or output data port hasn't correct size ->
            // error event.
            EventHandler.handle(EventLevel.ERROR, 
            		this.getClass().getCanonicalName()
            		+ "." + methodName,
                    "", 
                    "Detected " + block.getOutDataPorts().size()
                    + " output data ports in block \n " 
                    + block.getReferenceString()
                    + "\n Merge blocks are assumed to have one output port.", 
                    "");
            
            return null;
        }
    	
    }
}
