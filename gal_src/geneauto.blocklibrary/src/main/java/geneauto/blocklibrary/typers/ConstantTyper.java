/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ConstantTyper.java,v $
 *  @version	$Revision: 1.39 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.typers;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockTyper;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of Constant Blocks of a
 * GASystemModel.
 * 
 */
public class ConstantTyper extends BlockTyper {

    /**
     * Assign types according to the parameter Value of the Constant Block. If
     * value is null, then the propagation must not be null, else the
     * ConstantTyper raises an error. Base type is defined by the value
     * expression ("Value" parameter) associated to the block - If base type is
     * matrix type and the "VectorParams1D" is activated and the matrix is a
     * single row/column then the output type is a vector - Else the output type
     * is the base type
     * 
     * @param block
     *            The Block which is to be typed.
     * @returns true if the typing was executed properly.
     */
    public boolean assignTypes(Block block) {
        /*
         * CHECKING
         */
        // Constant must have exactly one output
        Assert.assertSingleElement(block.getOutDataPorts(), "OutDataPorts",
                this.getClass().getCanonicalName() + ".assignTypes", block
                        .getReferenceString(), null);

        // get parameters of the block
        Parameter outDataTypeMode = block.getParameterByName("OutDataTypeMode");
        Parameter value = block.getParameterByName("Value");
        Parameter vectorParam1D = block.getParameterByName("VectorParams1D");

        Assert.assertNotNull(outDataTypeMode,
                "Parameter OutDataTypeMode in block "
                        + block.getReferenceString() + " is not defined.");
        Assert.assertNotNull(value, "Parameter Value in block "
                + block.getReferenceString() + " is not defined.");
        Assert.assertNotNull(vectorParam1D, "Parameter VectorParam1D in block "
                + block.getReferenceString() + " is not defined.");

        // retrieve value of this parameters
        Expression valueExp = value.getValue();
        String propagation = outDataTypeMode.getStringValue();
        boolean vectorParams = "on".equals(vectorParam1D.getStringValue());

        Assert.assertNotNull(valueExp, "Value of parameter Value in block "
                + block.getReferenceString() + " is not defined.");
        Assert.assertNotNull(propagation,
                "Value of parameter OutDataTypeMode in block "
                        + block.getReferenceString() + " is not defined.");
        Assert.assertNotNull(vectorParams,
                "Value of parameter vectorParams in block "
                        + block.getReferenceString() + " is not defined.");

        GADataType valueType = valueExp.getDataType();

        Assert.assertNotNull(valueType, "Type of parameter Value in block "
                + block.getReferenceString() + " is not defined.");

        /*
         * PROCESSING
         */
        // Assigned type to the output port
        GADataType outportTypeCpy = null;

        if (propagation.equals("")
                || propagation.equals("Inherit from 'Constant value'")) {
            // If signal data type is not specified or is "Inherit from
            // 'Constant
            // value'" the data type is taken from parameter value
            outportTypeCpy = valueType.getCopy();

        } else {
            // Otherwise type is retrieved from OutDataTypeMode parameter and
            // Value parameter

            GADataType propagatedType = DataTypeAccessor
                    .getDataType(propagation);

            if (valueType instanceof TPrimitive) {
                // if Value signal type is primitive, output type is fully
                // retrieved
                // from OutDataTypeMode parameter
                outportTypeCpy = propagatedType.getCopy();
            } else if (valueType instanceof TArray) {
                // if Value signal type is Array, output signal type is
                // retrieved from Value parameter and output data type from
                // OutDataTypeMode parameter
                ((TArray) valueType).setBaseType((TPrimitive) propagatedType);
                outportTypeCpy = valueType.getCopy();
            } else {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                        .getCanonicalName(), "", "Type of parameter "
                        + value.getName() + "in block "
                        + block.getReferenceString() + " can't be determined.",
                        "");
            }
        }

        Assert.assertNotNull(outportTypeCpy,
                "Value of constant parameter has null data type.");

        if (vectorParams && outportTypeCpy.isMatrix()) {
            // if vectorParams is true and value is a vector matrix
            // (row or column matrix) then the output is normalised
            outportTypeCpy = outportTypeCpy.normalize(true);
        }

        block.getOutDataPorts().get(0).setDataType(outportTypeCpy);

        EventHandler.handle(EventLevel.DEBUG, this.getClass()
                .getCanonicalName(), "", block.getReferenceString()
                + "'s output port was assigned type "
                + DataTypeAccessor.toString(block.getOutDataPorts().get(0)
                        .getDataType()), "");
        return true;

    }

    /**
     * There can be no conflicts between input and output ports of the block
     * Constant, as it has no input port. The only possible conflict is between
     * the value and the propagation rule. We only check that the output port
     * has a datatype.
     */
    public boolean validateTypes(Block block) {

        if (block.getOutDataPorts().get(0).getDataType() == null) {
            EventHandler.handle(EventLevel.ERROR,
                    "ConstantTyper - Validation error", "", "Constant block "
                            + block.getReferenceString()
                            + "'s output port has no datatype.", "");
            return false;
        }

        return true;
    }
}
