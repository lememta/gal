//package geneauto.blocklibrary.backends;
//
//import geneauto.models.gacodemodel.Variable_CM;
//import geneauto.models.gacodemodel.expression.BinaryExpression;
//import geneauto.models.gacodemodel.expression.Expression;
//import geneauto.models.gacodemodel.expression.IntegerExpression;
//import geneauto.models.gacodemodel.expression.VariableExpression;
//import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
//import geneauto.models.gacodemodel.operator.AssignOperator;
//import geneauto.models.gacodemodel.operator.BinaryOperator;
//import geneauto.models.gacodemodel.statement.AssignStatement;
//import geneauto.models.gacodemodel.statement.ForStatement;
//import geneauto.models.gacodemodel.statement.IncStatement;
//import geneauto.models.gadatatypes.TRealInteger;
//import geneauto.models.gasystemmodel.common.Parameter;
//import geneauto.models.gasystemmodel.common.StringExpression;
//import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
//import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
//import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
//import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
//import geneauto.models.genericmodel.GAModelElement;
//import geneauto.models.utilities.BackendUtilities;
//import geneauto.utils.assertions.Assert;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Backend class for ForIterator block.
// * 
//// * 
// */
//public class SystemBackend extends SubSystemImplBackend {
//
//    @Override
//    public List<GAModelElement> getComputationCode(SystemBlock block,
//            List<Expression> inputs, List<Expression> outputs,
//            List<Expression> controls, Expression memory) {
//
//        // // checking block validity
//        // if (!block.getType().equals("SubSystem")) {
//        // EventsHandler.handle(EventLevel.ERROR, "SystemBackend", "",
//        // "Block type error : " + block.getReferenceString(), "");
//        // }
//
//        for (Block b : block.getBlocks()) {
//            if (b.getType().equals("ForIterator")) {
//                return forIteratorCode(block, inputs, outputs, controls, memory);
//            }
//        }
//
//        return null;
//    }
//
//    private List<GAModelElement> forIteratorCode(SystemBlock block,
//            List<Expression> inputs, List<Expression> outputs,
//            List<Expression> controls, Expression memory) {
//
//        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();
//
//        Block forIteratorBlock = block.getForIteratorBlock();
//        Assert.assertNotNull(forIteratorBlock, "");
//
//        Parameter paramIterSrc = forIteratorBlock
//                .getParameterByName("IterationSource");
//        Assert.assertNotNull(paramIterSrc, "");
//        String iterationSrc = ((StringExpression) paramIterSrc.getValue())
//                .getValue();
//
//        Expression numIters = null;
//
//        if (iterationSrc.equals("internal")) {
//
//            Parameter paramNumIters = forIteratorBlock
//                    .getParameterByName("NumIters");
//            Assert.assertNotNull(paramNumIters, "");
//
//            numIters = (Expression)paramNumIters.getCodeModelExpression();
//            
//        } else if (iterationSrc.equals("external")) {
//            
//            // find the system input that is related to the forIterator block
//            Inport inport = forIteratorBlock.getInDataPorts().get(0);
//            Block inputPort = null;
//            for(Signal s : block.getSignals()){
//                if(s.getDstPort() == inport){
//                    inputPort = (Block)s.getSrcPort().getParent();
//                }
//            }
//            
//            Parameter paramNumInputPort = inputPort.getParameterByName("Port");
//            Assert.assertNotNull(paramNumInputPort, "");
//            
//            int numInputPort = Integer.valueOf(((StringExpression) paramNumInputPort.getValue()).getLitValue());
//                        
//            numIters = inputs.get(numInputPort - 1).getCopy(); 
//            
//        } else {
//            // error
//        }
//
//        /*
//         * index variable initialization
//         */
//        Variable_CM indexVar = new Variable_CM();
//
//        indexVar.setName("index" + block.getId());
//        if ((forIteratorBlock.getOutDataPorts() != null)
//                && (forIteratorBlock.getOutDataPorts().size() > 0)) {
//            indexVar.setDataType(forIteratorBlock.getOutDataPorts().get(0)
//                    .getDataType());
//        } else {
//            indexVar.setDataType(new TRealInteger(8, false));
//        }
//        indexVar.setScope(DefinitionScope.Local);
//
//        VariableExpression index = new VariableExpression(indexVar);
//
//        /*
//         * PostStatement index = 0
//         */
//        AssignStatement assignIndex = new AssignStatement(
//                AssignOperator.SimpleAssign, index.getCopy(),
//                new IntegerExpression(0));
//
//        /*
//         * PreStatement index++
//         */
//        Parameter paramExtInc = forIteratorBlock
//                .getParameterByName("ExternalIncrement");
//
//        IncStatement unaryIndex = new IncStatement(index.getCopy());
//
//
//        if (paramExtInc != null) {
//            String extInc = ((StringExpression) paramExtInc.getValue()).getLitValue();
//            if (extInc.equals("on")) {
//                unaryIndex = null;
//            }
//        }
//        
//        BinaryExpression conditionalExp = new BinaryExpression(
//                index.getCopy(),numIters.getCopy(),BinaryOperator.LTOperator); 
//                
//        
//        /*
//         * ForStatement creation
//         */
//        ForStatement forState = new ForStatement(conditionalExp, assignIndex, unaryIndex,
//                null);
//        
//        // giving a name to the if statement
//        forState.setName(block.getName() + "ForStatement");
//
//        /*
//         * Setting indexVariable field in the system block
//         */
//        block.setIndexVariable(indexVar);
//        
//        codemodel.add(indexVar);
//        codemodel.add(forState);
//
//        return codemodel;
//
//    }
//}
