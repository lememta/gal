/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/SumBackend.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2012-03-13 12:41:00 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Sum block
 * 
 * 
 */
public class SumBackend extends CombinatorialImplBackend {

    /**
     * Parse sum parameters (for instance "|++-" for a 3-inputs sum block)
     */
    List<BinaryOperator> parseSumParameter(String param) {

        int index = 0;
        List<BinaryOperator> operators = new ArrayList<BinaryOperator>();

        param = param.replace("|", "");

        if (param.startsWith("+") || param.startsWith("-")) {
            for (index = 0; index < param.length(); index++) {
                if (param.charAt(index) == '+') {
                    operators.add(BinaryOperator.ADD_OPERATOR);
                }
                if (param.charAt(index) == '-') {
                    operators.add(BinaryOperator.SUB_OPERATOR);
                }
            }
        } else {
            // if value of parameter is a number, operators are + by default
            int plusNumber = Integer.valueOf(param);

            for (index = 0; index < plusNumber; index++) {
                operators.add(BinaryOperator.ADD_OPERATOR);
            }
        }
        return operators;
    }

    /**
     * Return assign statement : output = (-) input1 +/- input2 +/- input3 + ...
     * Operator is + or - according to operators list argument
     * 
     * @param inputs
     *            input variables
     * @param output
     *            output variable
     * @param operators
     *            list of binary operator (+ or -)
     * @return Assign statement
     */
    private AssignStatement assignMultipleInputs(List<Expression> inputs,
            Expression output, List<BinaryOperator> operators) {

        // creating statement
        AssignStatement assign = new AssignStatement();

        // assign operator
        assign.setOperator(AssignOperator.SIMPLE_ASSIGN);

        // output expression (LEFT)
        assign.setLeftExpression(output);

        // product expression (RIGHT)
        int inputNumber = inputs.size();

        List<BinaryExpression> exprs;
        exprs = new ArrayList<BinaryExpression>();

        if (operators.get(0) == BinaryOperator.SUB_OPERATOR) {
            inputs.set(0, new UnaryExpression(inputs.get(0).getCopy(),
                    UnaryOperator.UNARY_MINUS_OPERATOR));
        }

        for (int i = 0; i < inputNumber - 1; i++) {
            exprs.add(new BinaryExpression(inputs.get(i).getCopy(), null,
                    operators.get(i + 1)));

            if ((i == 0) && (i == inputNumber - 2)) {
                assign.setRightExpression(exprs.get(i));
                exprs.get(i).setRightArgument(inputs.get(i + 1).getCopy());

            } else if (i == 0) {
                assign.setRightExpression(exprs.get(i));
            } else if (i == inputNumber - 2) {
                exprs.get(i).setRightArgument(inputs.get(i + 1).getCopy());
            }
            if (i != 0) {
                exprs.get(i - 1).setRightArgument(exprs.get(i));
            }
        }

        return assign;
    }

    /**
     * Return assign statement : output = input[0] +/- input[1] + ... Operator
     * is + or - according to operator argument
     * 
     * @param input
     *            input vector variable
     * @param output
     *            output scalar variable
     * @param operator
     *            Binary operator (+ or -)
     * @return
     */
    private AssignStatement assignSingleVectorInput(Expression input,
            Expression output, BinaryOperator operator) {

        // creating statement
        AssignStatement assign = new AssignStatement();

        // assign operator
        assign.setOperator(AssignOperator.SIMPLE_ASSIGN);

        // output expression (LEFT)
        assign.setLeftExpression(output);

        // product expression (RIGHT)
        // determining vector size
        int vectorSize = Integer.valueOf(((IntegerExpression) ((TArray) input
                .getDataType()).getDimensions().get(0)).getLitValue());

        List<BinaryExpression> exprs;
        exprs = new ArrayList<BinaryExpression>();

        for (int i = 0; i < vectorSize - 1; i++) {

            if ((i == 0) && (operator == BinaryOperator.SUB_OPERATOR)) {
                exprs.add(new BinaryExpression(new UnaryExpression(
                        BackendUtilities.varIndex(input, i),
                        UnaryOperator.UNARY_MINUS_OPERATOR), null, operator));
            } else {
                exprs.add(new BinaryExpression(BackendUtilities.varIndex(input,
                        i), null, operator));
            }

            if ((i == 0) && (i == vectorSize - 2)) {
                assign.setRightExpression(exprs.get(i));

                exprs.get(i).setRightArgument(
                        BackendUtilities.varIndex(input, i + 1));
            } else if (i == 0) {
                assign.setRightExpression(exprs.get(i));
            } else if (i == vectorSize - 2) {
                exprs.get(i).setRightArgument(
                        BackendUtilities.varIndex(input, i + 1));
            }
            if (i != 0) {
                exprs.get(i - 1).setRightArgument(exprs.get(i));
            }
        }

        return assign;
    }

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        if (!block.getType().equals("Sum")) {
            EventHandler.handle(EventLevel.ERROR, "SumBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Reading block parameter
        Parameter paramInputs = block.getParameterByName("Inputs");

        Assert.assertNotNull(paramInputs, "Parameter 'Inputs' not defined : "
                + block.getReferenceString());

        // code model initialization
        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // determining operators (+ or -)
        String operatorString = ((StringExpression) paramInputs.getValue()).getLitValue();
        List<BinaryOperator> operators = parseSumParameter(operatorString);

        AssignStatement assign = null;

        if (inputs.size() > 1) {

            /*
             * %o1 = +/- %i1 +/- %i2 +/- ...
             */

            assign = assignMultipleInputs(inputs, outputs.get(0), operators);

        } else {
            GADataType inType = inputs.get(0).getDataType();
            if (inType instanceof TPrimitive) {
            	Expression rhsExp; 
            	if (operators.get(0).equals(BinaryOperator.ADD_OPERATOR)) {
            		/* %o1 = %i1 */
            		rhsExp = inputs.get(0).getCopy();
            	} else {
            		/* %o1 = - %i1 */
            		rhsExp = new UnaryExpression(inputs.get(0).getCopy(),
            				UnaryOperator.UNARY_MINUS_OPERATOR);
            	}            	
                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        outputs.get(0), rhsExp);

            } else if (inType instanceof TArray) {                
                /*
                 * %o1 = +/- %i1 +/- %i2 +/- ...
                 */
                
                if (((TArray) inType).getDimensionality() == 1) {
                    assign = assignSingleVectorInput(inputs.get(0).getCopy(),
                            outputs.get(0), operators.get(0));
                    
                } else {
                    EventHandler.handle(EventLevel.ERROR, "SumBackend", "",
                            "Input type error, single input can't have greater dimensionality than 1; "
                            + block.getReferenceString(), "");             
                }

            } else {
                EventHandler.handle(EventLevel.ERROR, "SumBackend", "",
                        "Unexpected input type: " + inType.getReferenceString()
                            + "\nBlock: " + block.getReferenceString());
            }
        }

        codeModel.add(assign);

        // giving a name to the assign statement
        assign.setName(block.getName() + "Assign");

        return codeModel;
    }

}
