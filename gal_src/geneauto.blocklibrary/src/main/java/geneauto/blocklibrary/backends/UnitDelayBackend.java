/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/UnitDelayBackend.java,v $
 *  @version	$Revision: 1.22 $
 *	@date		$Date: 2012-03-13 20:51:43 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;
import geneauto.utils.general.NameFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for UnitDelay block.
 * 
 * 
 */
public class UnitDelayBackend extends SequentialImplBackend {

    /**
     * Return declaration code (global variable or statement that need to be
     * declared before block computation code)
     * 
     * @param block
     *            block for which is called the backend
     * @return GACodeModel fragment describing declarations involve by block
     */
    @Override
    public List<GAModelElement> getDeclarationCode(Block block) {

        if (!block.getType().equals("UnitDelay")) {
            EventHandler.handle(EventLevel.ERROR, "UnitDelayBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // Model declaration
        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // determine variable type
        GADataType type = block.getInDataPorts().get(0).getDataType();

        // variable declaration
        Variable_CM memory = new StructureMember();

        memory.setDataType(type);
        // block memory is not optimisable
        memory.setOptimizable(false);

        // set variable name
        String name = block.getName() + "_memory";

        // replace all unavailable characters (/,\...) and set variable name
        memory.setName(NameFormatter.normaliseVariableName(name, 
        		block.getReferenceString(), 
        		true));

        codeModel.add(memory);

        return codeModel;
    }

    /**
     * Return initialisation code for sequential blocks
     * 
     * @param block
     *            block for which is called the backend
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block initialisation code
     * 
     * memory = init_block_parameter
     */
    @Override
    public List<GAModelElement> getInitCode(Block block, Expression memory) {

        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        if (!block.getType().equals("UnitDelay")) {
            EventHandler.handle(EventLevel.ERROR, "UnitDelayBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        Parameter paramInit = block.getParameterByName("InitialValue");

        Assert.assertNotNull(paramInit,
                "Parameter 'InitialValue' not defined : "
                        + block.getReferenceString());

        // assign statement
        AssignStatement assign = new AssignStatement();

        assign.setOperator(AssignOperator.SIMPLE_ASSIGN);
        assign.setLeftExpression(memory.getCopy());

        Expression exprInit = null;

        exprInit = paramInit.getCodeModelExpression();

        assign.setRightExpression(exprInit);

        // giving a name to the assign statement
        assign.setName(block.getName() + "Assign");

        codemodel.add(assign);

        return codemodel;
    }

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls, Expression memory) {

        if (!block.getType().equals("UnitDelay")) {
            EventHandler.handle(EventLevel.ERROR, "UnitDelayBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        AssignStatement outputAssign = null;

        // output assign statement : output = memory
        outputAssign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0).getCopy(), memory.getCopy());

        // giving a name to the assign statement
        outputAssign.setName(block.getName() + "OutputAssign");

        // Updating code model
        codemodel.add(outputAssign);

        return codemodel;
    }

    @Override
    public List<GAModelElement> getUpdateMemoryCode(Block block,
            List<Expression> inputs, List<Expression> controls,
            Expression memory) {

        if (!block.getType().equals("UnitDelay")) {
            EventHandler.handle(EventLevel.ERROR, "UnitDelayBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        AssignStatement inputAssign = null;

        // input assign statement : memory = input
        inputAssign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, memory,
                inputs.get(0).getCopy());

        // giving a name to the assign statement
        inputAssign.setName(block.getName() + "InputAssign");

        // Updating code model
        codemodel.add(inputAssign);

        return codemodel;
    }

}
