/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/LogicBackend.java,v $
 *  @version	$Revision: 1.21 $
 *	@date		$Date: 2012-02-21 11:15:02 $
 *
 *  Copyright (c) 2006-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Logic block.
 * 
 * TODO (TF:797): consider inlining the loop in case it contains only one element
 * 		 i.e. use simple assignments in case the vector is composed of 
 * 		 one or two elements only (ToNa 27/04/10)

 */
public class LogicBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Logic")) {
            EventHandler.handle(EventLevel.ERROR, 
        		this.getClass().getCanonicalName() + ".getComputationCode()", "",
            	"Incorrect block type \"" + block.getType() 
            	+ "\"\n Block: "
            	+block.getReferenceString());
        }

    	// --- CHECKING ---
    	// logic block must have at least one input
    	Assert.assertNotEmpty(block.getInDataPorts(),
                "block inputs",
                this.getClass().getCanonicalName() + ".getComputationCode()",
                block.getReferenceString(),
                "");

    	// logic block must have exactly one output
    	Assert.assertSingleElement(block.getOutDataPorts(),
                "block outputs",
                this.getClass().getCanonicalName() + ".getComputationCode()",
                block.getReferenceString(),
                "");

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // get the operator
        Parameter opParam = block.getParameterByName("Operator");
        if (opParam == null 
        		|| opParam.getStringValue() == null
        		|| opParam.getStringValue().isEmpty()) {
            EventHandler.handle(EventLevel.ERROR, 
            		this.getClass().getCanonicalName(), 
                    "",
                    "Parameter 'Operator' not defined for block\n "
                            + block.getReferenceString());
            return null;
        }
        
        String opString = opParam.getStringValue();
        BinaryOperator binOp = BinaryOperator.fromParameter(opString);

        // assign statement initialisation
        Statement assignOutput = null;

        if (inputs.size() == 1) {
        	// in case of a not operator we simply apply not to each element
        	// in the input, regardless of its dimension
        	if ("NOT".equals(opString)) {
                assignOutput = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        				outputs.get(0), 
                        				new UnaryExpression(
                        						inputs.get(0).getCopy(), 
                        						UnaryOperator.NOT_OPERATOR));
        		
        	} else if (inputs.get(0).getDataType().isVector()) {
        		// collapse vector to scalar
        		codemodel.addAll(BackendUtilities.assignVector2Scalar(
        								inputs.get(0), outputs.get(0), 
        								binOp, opString));
        	} else {
                EventHandler.handle(EventLevel.ERROR, 
                		this.getClass().getCanonicalName() + ".getComputationCode()", "",
                    	"Operator \"" + opString 
                    	+ "\" is not appliccable to vector data when there " 
                    	+ "is only one input port\n Block: "
                    	+ block.getReferenceString());
        	}        	
        } else {
        	Expression resultExpr = null;
        	if ("XOR".equals(opString)) {
        		// the result of x != 0 must be cast to integer type with 
        		// appropriate dimension
        		GADataType intType = null;
        		if (outputs.get(0).getDataType() instanceof TArray) {
        			// if output type is array, get dimension from this 
        			// array and change the base type 
        			intType = new TArray();
        			((TArray) intType).setBaseType(new TRealInteger(8, false));
        			((TArray) intType).copyDimensions(
        					outputs.get(0).getDataType().getDimensions());
        		} else {
        			intType = new TRealInteger(8, false);
        		}
        		
        		// in case of an expression y_bool = x1 xor x2 xor x3 we do:
        		// y_bool = (boolean) (((x1 != 0) ^ (x2 != 0)) ^ (x3 != 0))
	        	for (Expression ie : inputs) {
	        		if (resultExpr == null) {
	        			resultExpr = new UnaryExpression( 
	        					new BinaryExpression( 
	        							ie.getCopy(),
	        							new IntegerExpression(0),
	        							BinaryOperator.NE_OPERATOR),
	        					UnaryOperator.CAST_OPERATOR,
	        					intType);
	        		} else {
	        			resultExpr = new BinaryExpression(resultExpr, 
	    	        			new UnaryExpression( 
	    	        					new BinaryExpression( 
	    	        							ie.getCopy(),
	    	        							new IntegerExpression(0),
	    	        							BinaryOperator.NE_OPERATOR),
	    	        					UnaryOperator.CAST_OPERATOR,
	    	        					intType),
	        					binOp);
	        		}
	        	}

        	} else {
	        	// perform elementwise binary operation between the arguments
	        	for (Expression ie : inputs) {
	        		if (resultExpr == null) {
	        			resultExpr = ie.getCopy();
	        		} else {
	        			resultExpr = new BinaryExpression(resultExpr, 
	        					ie.getCopy(),
	        					binOp);
	        		}
	        	}
	        	
	        	// apply NOT to the result
	        	if ("NAND".equals(opString) 
	            		|| "NOR".equals(opString)) {
	        		resultExpr = new UnaryExpression(resultExpr, 
	        								UnaryOperator.NOT_OPERATOR);
	        	}
        	}
        	
        	resultExpr.setDataType(outputs.get(0).getDataType());
        	
            assignOutput = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
    							outputs.get(0).getCopy(), 
    							resultExpr);        	
        }

        if (assignOutput != null) {
        	codemodel.add(assignOutput);
        }
        
        return codemodel;
    }
        
}
