/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/PulseOneBackend.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.BooleanExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.TrueExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Pulse1 block.
 * 
 * 
 */
public class PulseOneBackend extends SequentialImplBackend {

    @Override
    public List<GAModelElement> getDeclarationCode(Block block) {

        if (!block.getType().equals("PULSE1")) {
            EventHandler.handle(EventLevel.ERROR, "PulseOneBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // Model declaration
        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // variable declaration
        Variable_CM memory = new Variable_CM();

        memory.setDataType(new TBoolean());
        memory.setScope(DefinitionScope.LOCAL);

        // set variable name
        String name = block.getName() + "_" + block.getId() + "_memory";

        // replace all unavailable characters (/,\...) and set variable name
        memory.setName(BackendUtilities.cleanStringName(name));

        codeModel.add(memory);

        return codeModel;
    }

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls, Expression memory) {

        if (!block.getType().equals("PULSE1")) {
            EventHandler.handle(EventLevel.ERROR, "PulseOneBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramInit = block.getParameterByName("PARH1");
        Parameter paramBInit = block.getParameterByName("PARH2");

        // general if statement initialization
        IfStatement ifState = null;

        // if parameter are not defined, the 2 optional input must be defined
        if ((paramInit == null) || (paramBInit == null)) {

            Assert.assertNotNull(inputs.get(1), "Input not defined : Init "
                    + block.getReferenceString());

            Assert.assertNotNull(inputs.get(2), "Input not defined : BInit "
                    + block.getReferenceString());

            // if both input are defined, codeModel can be created

            /*
             * Template code generated : if (%i3 != false) { %o1 = %i2; } ...
             */

            ifState = new IfStatement(new BinaryExpression(inputs.get(2)
                    .getCopy(), new FalseExpression(),
                    BinaryOperator.NE_OPERATOR),
                    new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                            .get(0), inputs.get(1)), null);

        }
        // if both parameters are defined, optional inputs are not taken into
        // account
        else {

            /*
             * Template code generated : if (paramBInit != false) { %o1 =
             * %paramInit; } ...
             */

            // determining parameters value
            LiteralExpression init = BooleanExpression.getBooleanInstance(
                    ((StringExpression) paramInit.getValue()).getLitValue());

            LiteralExpression bInit = BooleanExpression.getBooleanInstance(
                    ((StringExpression) paramBInit.getValue()).getLitValue());

            ifState = new IfStatement(new BinaryExpression(bInit,
                    new FalseExpression(), BinaryOperator.NE_OPERATOR),
                    new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                            .get(0), init), null);

        }

        /*
         * ... else if ((data_structure_memory == false) && (%i1 != false)){ %o1 =
         * true; }else{ %o1 = false; } data_structure_memory = %o1;
         */
        ifState.setElseStatement(new IfStatement(new BinaryExpression(
                new BinaryExpression(memory, new FalseExpression(),
                        BinaryOperator.EQ_OPERATOR), new BinaryExpression(inputs
                        .get(0).getCopy(), new FalseExpression(),
                        BinaryOperator.NE_OPERATOR),
                BinaryOperator.LOGICAL_AND_OPERATOR), new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0),
                new TrueExpression()), new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0),
                new FalseExpression())));

        // giving a name to the statements
        ifState.setName(block.getName() + "IfStatement");

        codeModel.add(ifState);

        return codeModel;
    }

    @Override
    public List<GAModelElement> getUpdateMemoryCode(Block block,
            List<Expression> inputs, List<Expression> controls,
            Expression memory) {

        if (!block.getType().equals("PULSE1")) {
            EventHandler.handle(EventLevel.ERROR, "PulseOneBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // Model declaration
        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // assign output to data_structure_memory
        AssignStatement assignMemory = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, memory, inputs.get(0).getCopy());

        // giving a name to the statements
        assignMemory.setName(block.getName() + "assignMemory");

        codeModel.add(assignMemory);

        return codeModel;
    }

}
