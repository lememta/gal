/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/BusCreatorBackend.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.CustomType_CM;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for BusCreator block
 * 
 * 
 */
public class BusCreatorBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("BusCreator")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error : "
                    + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Emtpy output list for block\n "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Emtpy input list for block\n "
                        + block.getReferenceString());

        // check that the data type is of type TCustom
        if (!(outputs.get(0).getDataType() instanceof TCustom)) {
            EventHandler.handle(
                    EventLevel.CRITICAL_ERROR,
                    this.getClass().getCanonicalName(),
                    "",
                    "Block first output is of type "
                            + outputs.get(0).getDataType().toString()
                            + "\n. Data type \"TCustom \" expected!\n "
                            + "Block: "
                            + block.getReferenceString(), "");
            return null;
        }

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        AssignStatement assign = null;

        // determining signal type of the current output
        CustomType outputType = ((TCustom) outputs.get(0)
        										.getDataType())
        										.getCustomType();
        
        // if output has system model type, then try to find corresponding 
        // code model type
        // TODO: reconsider this! Here we actually fix the incorrect data
        // type assigned somewhere earlier. The data type assignment 
        // must be fixed where it happens (ToNa 13/03/09)
        if (outputType instanceof CustomType_SM) {
        	outputType = (CustomType_CM) ((CustomType_SM) outputType)
        											.getCodeModelElement();
        }
                
        Assert.assertNotNull(outputType,
           "Code model type of the output port can not be determined in block\n  "
                          + block.getReferenceString());

        List<StructureMember> structMembers = outputType.getMembers();
        
        Assert.assertNotEmpty(structMembers,
                "Empty structure in the output data type of block\n  "
                          + block.getReferenceString());
        
        // checking, if sizes match
        if (inputs.size() != structMembers.size()) {
        	String stInputs = "";
        	String stMems = "";
        	for (Expression inExp : inputs) {
        		stInputs += inExp.getReferenceString() + ";";
        	}
        	for (StructureMember stMem : structMembers) {
        		stMems += stMem.getName() + ";";
        	}
        	EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
        			"Number of inputs does not match number of StructureMembers in the outputType." +
        			"\n Inputs:           " + stInputs +
        			"\n StructureMembers: " + stMems +
        			"\n Block: " + block.getReferenceString()
        			);
        }

        // checking type validity
        for (int i = 0; i < inputs.size(); i++) {

            /*
             * output.signal_i = input_i
             */

            StructureMember currentMember = structMembers.get(i);            

            if (inputs.get(i).getDataType().isSubType(currentMember.getDataType())) {

                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        new MemberExpression(currentMember, 
                        		outputs.get(0).getCopy()),
                        inputs.get(i).getCopy());

                // giving a name to the assign statement
                assign.setName(block.getName() + "Assign_" + i);

                codemodel.add(assign);
            } else {
                EventHandler.handle(
                		EventLevel.ERROR,
                		this.getClass().getCanonicalName(),
                		"",
                		"Input type "
                		+ inputs.get(i).getDataType().toString()
                		+ "doesn't match the input member type: "
                		+ currentMember.getDataType().toString()
                		+ "\n Block "
                		+ block.getReferenceString(), "");
            }
        }
        return codemodel;
    }
}
