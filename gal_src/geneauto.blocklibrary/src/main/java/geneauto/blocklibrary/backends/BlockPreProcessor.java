package geneauto.blocklibrary.backends;

import geneauto.models.gasystemmodel.GASystemModel;

public interface BlockPreProcessor {
    public void preProcess(GASystemModel m);
}
