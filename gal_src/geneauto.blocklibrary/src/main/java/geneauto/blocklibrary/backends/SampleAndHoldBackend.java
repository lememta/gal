/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/SampleAndHoldBackend.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for SampleAndHold block.
 * 
 * 
 */
public class SampleAndHoldBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("SampleAndHold")) {
            EventHandler.handle(EventLevel.ERROR, "SampleAndHoldBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // main if statement declaration
        IfStatement ifState = null;
        CompoundStatement thenState = null;
        CompoundStatement elseState = null;
        CompoundStatement elseIfState = null;

        /*
         * if (%i3 != 0) { %o1 = %i4; %o2 = %i4; } else if (%i2 != 0){ %o2 =
         * %i1; %o1 = %i1; } else { %o2 = %i5; %o1 = %i5; }
         */

        thenState = new CompoundStatement();
        ArrayList<Statement> thenStatements = new ArrayList<Statement>();

        thenStatements.add(new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                outputs.get(0), inputs.get(3).getCopy()));
        thenStatements.add(new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                outputs.get(1), inputs.get(3).getCopy()));

        thenState.setStatements(thenStatements);

        elseIfState = new CompoundStatement();
        ArrayList<Statement> elseIfStatements = new ArrayList<Statement>();

        elseIfStatements.add(new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                outputs.get(1), inputs.get(0).getCopy()));
        elseIfStatements.add(new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                outputs.get(0), inputs.get(0).getCopy()));

        elseIfState.setStatements(elseIfStatements);

        elseState = new CompoundStatement();
        ArrayList<Statement> elseStatements = new ArrayList<Statement>();

        elseStatements.add(new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                outputs.get(1), inputs.get(4).getCopy()));
        elseStatements.add(new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                outputs.get(0), inputs.get(4).getCopy()));

        elseState.setStatements(elseStatements);

        ifState = new IfStatement(new BinaryExpression(inputs.get(2).getCopy(),
                new IntegerExpression(0), BinaryOperator.NE_OPERATOR),
                thenState, new IfStatement(new BinaryExpression(inputs.get(1)
                        .getCopy(), new IntegerExpression(0),
                        BinaryOperator.NE_OPERATOR), elseIfState, elseState));

        ifState.setName(block.getName() + "ifStatement");

        // updating code model
        codemodel.add(ifState);

        return codemodel;
    }

}
