/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/LimBackend.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Lim block.
 * 
 * 
 */
public class LimBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Lim")) {
            EventHandler.handle(EventLevel.ERROR, "LimBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // max and min value determination
        Expression minValue = null;
        Expression maxValue = null;

        if (inputs.size() == 3) {
            // min and max value are defined by block inputs
            minValue = inputs.get(1).getCopy();
            maxValue = inputs.get(2).getCopy();

        } else if (inputs.size() == 1) {
            // min and max value are defined by block parameters

            // reading block parameter
            Parameter paramMinValue = block.getParameterByName("PARH1");
            Parameter paramMaxValue = block.getParameterByName("PARH2");

            // if at least one parameter isn't defined, an error is raised
            Assert.assertNotNull(paramMinValue,
                    "Parameter 'PARH1' not defined : "
                            + block.getReferenceString());

            Assert.assertNotNull(paramMaxValue,
                    "Parameter 'PARH2' not defined : "
                            + block.getReferenceString());

            minValue = new DoubleExpression(((StringExpression) paramMinValue.getValue()).getLitValue());

            maxValue = new DoubleExpression(((StringExpression) paramMaxValue.getValue()).getLitValue());

        } else {
            EventHandler.handle(EventLevel.ERROR, "LimBackend", "",
                    "Block hasn't expected number of input : "
                            + block.getReferenceString(), "");
        }

        Statement resultSt = null;
        
        /*
         * ifMaxValue is statement with following pseudo code:
         * 
         * 		if (%i1 > max_value) {   
         *          %o1 = max_value; 
         *      } else {
         *          if (%i1 < min_value){ 
         *              %o1 = min_value;
         *          } else { 
         *              %o1 = %i1; 
         *          }
         *      }
         */
        IfStatement ifMaxValue = new IfStatement(
                new BinaryExpression(
                        inputs.get(0).getCopy(), 
                        maxValue.getCopy(),
                        BinaryOperator.GT_OPERATOR), 
                new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, 
                        outputs.get(0).getCopy(),
                        maxValue.getCopy()), 
                null);
        IfStatement ifMaxValueElseStatement = new IfStatement(
                new BinaryExpression (
                        inputs.get(0).getCopy(), 
                        minValue.getCopy(),
                        BinaryOperator.LT_OPERATOR), 
                new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, 
                        outputs.get(0).getCopy(), 
                        minValue.getCopy()), 
                new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, 
                        outputs.get(0).getCopy(), 
                        inputs.get(0).getCopy()));
        ifMaxValue.setElseStatement(ifMaxValueElseStatement);
        
        /*
         * ifMinValue is statement with following pseudo code: 
         * 
         * 		%o1 = max_value;
         */
        Statement ifMinValue = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, 
                outputs.get(0).getCopy(), 
                maxValue.getCopy());
        
        /*
         * 3 inputs => result is IfStatement with following pseudo code: 
         * 
         * 
         * if (max_value >= min_value) { 
         *      <ifMaxValue>
         * } else { 
         *      <ifMinValue>
         * }
         * 
         */
        if (inputs.size() == 3) {
            resultSt = new IfStatement(
                    new BinaryExpression(
                            maxValue.getCopy(), 
                            minValue.getCopy(), 
                            BinaryOperator.GE_OPERATOR),
                    ifMaxValue,
                    ifMinValue);
            
        /*
         * 1 input => result is Statement with one of following pseudo codes: 
         * 
         * 1) if max_value >= min value then:
         *      
         *      <ifMinValue>
         * 2) otherwise: 
         *      
         *      <ifMaxValue>
         * 
         */
        } else if (inputs.size() == 1) {
            resultSt = 
                maxValue.evalReal().getRealValue() >= minValue.evalReal().getRealValue() ?
                        ifMaxValue: ifMinValue;
        }

        // updating code model
        codemodel.add(resultSt);

        return codemodel;
    }

}
