/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/BusSelectorBackend.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.CustomType;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.MemberExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Backend class for BusSelector block
 * 
 */
public class BusSelectorBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("BusSelector")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error: "
                    + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Emtpy output list for block\n "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Emtpy input list for block\n "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramOutput = block.getParameterByName("OutputSignals");

        Assert.assertNotNull(paramOutput,
                "Parameter 'OutputSignals' not defined in block\n  "
                        + block.getReferenceString());

        AssignStatement assign = null;

        // parse the "OutputSignals" parameter
        Vector<String> outputSignals = BackendUtilities
                .parseOutputSignals(paramOutput.getStringValue());

        // --- determine data type of the input ---
        // check if the first input has data type
        Assert.assertNotNull(inputs.get(0).getDataType(),
                "Untyped first input port in block\n "
                        + block.getReferenceString());

        // check that the data type is of type TCustom
        if (!(inputs.get(0).getDataType() instanceof TCustom)) {
            EventHandler.handle(
                    EventLevel.CRITICAL_ERROR,
                    this.getClass().getCanonicalName(),
                    "",
                    "Block first input is of type "
                            + inputs.get(0).getDataType().toString()
                            + "\n. Data type \"TCustom \" expected!\n "
                            + "Block: "
                            + block.getReferenceString(), "");
            return null;
        }

        CustomType inputType = null;
        inputType = (CustomType) ((TCustom) inputs.get(0)
        								.getDataType())
        								.getCustomType();

        if(inputType instanceof CustomType_SM){
            inputType = (CustomType_SM) ((CustomType_SM) inputType)
            										.getCodeModelElement();
        }
        
        Assert.assertNotNull(inputType,
          "Code model type of the input port can not be determined in block\n  "
                    + block.getReferenceString());
        
        // retrieve the list of structure members
        List<StructureMember> structMembers = inputType.getMembers();
        Assert.assertNotEmpty(structMembers,
                "Empty structure in the input data type of block\n  "
                          + block.getReferenceString());
        
        StructureMember currentMember;
        for (int i = 0; i < outputSignals.size(); i++) {
        	currentMember = null;
        	
            try {
            	int memberNo = Integer.parseInt(outputSignals.get(i)) - 1;
            	if (memberNo >= structMembers.size()){
                    EventHandler.handle(
                            EventLevel.CRITICAL_ERROR,
                            this.getClass().getCanonicalName(),
                            "",
                            "Element " 
                            + i
                            + " of parameter \"OutputSignals\" contains out of " 
                            + "bound index!\n There is " 
                            + structMembers.size()
                            + " signals in input data type, the index value is "
                            + memberNo 
                            + "\n Block: "
                            + block.getReferenceString(), "");
                    return null;
                }
            		
            	currentMember = structMembers.get(memberNo);
            } catch (NumberFormatException nbE) {
            	// the element was not a string
            	// try to find structure member by name
                String inputSignalName = outputSignals.get(i);
                currentMember = inputType.getMemberByName(inputSignalName);
            	if (currentMember == null){
                    EventHandler.handle(
                            EventLevel.CRITICAL_ERROR,
                            this.getClass().getCanonicalName(),
                            "",
                            "Element " 
                            + i
                            + " of parameter \"OutputSignals\" contains unknown"
                            + "reference !\n There is no element named \"" 
                            + inputSignalName
                            + "\" in the input type."
                            + "\n Block: "
                            + block.getReferenceString(), "");
                    return null;
                }
            }
            
            GADataType currentType = currentMember.getDataType();

            if (currentType.isSubType(outputs.get(i).getDataType())){

	            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
	                            outputs.get(i).getCopy(), 
	                            new MemberExpression(currentMember,
	                                    	inputs.get(0).getCopy()));
            } else {
                    EventHandler.handle(
                    		EventLevel.ERROR,
                    		this.getClass().getCanonicalName(),
                    		"",
                    		"Output type "
                    		+ outputs.get(i).getDataType().toString()
                    		+ "doesn't match the input member type: "
                    		+ currentType.toString()
                    		+ "\n Block "
                    		+ block.getReferenceString(), "");
                }

            // giving a name to the assign statement
            assign.setName(block.getName() + "Assign_" + i);

            codemodel.add(assign);

        }
        
        return codemodel;
    }
}
