/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/DemuxBackend.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-09-13 12:39:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.utilities.CodeModeUtilities;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.models.utilities.ExpressionUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Demux block.
 * 
 * TODO: (TF:649) Demux backend creates explicit assign statements for vector 
 * 			outputs, incorrect indexes used
 * TODO: (TF:648) Mux-Demux pair looses the original data types of Mux input 
 */
public class DemuxBackend extends CombinatorialImplBackend {

    
    /**
     * Auxiliary function to have statement : outputs_i[j] = input[i+j]
     * 
     * @param i
     * @param jStart
     * @param jEnd
     * @param inputs
     * @param outputs
     * @return
     */
    private AssignStatement assignOutput(Block block, int i, 
    		int jStart, int jEnd,
            List<Expression> inputs, List<Expression> outputs, int nAboveP) {

        AssignStatement assign = new AssignStatement();
        assign.setOperator(AssignOperator.SIMPLE_ASSIGN);

        // outputs(i)[j]
        Expression leftExp = ExpressionUtilities.getSubArrayExpression(outputs.get(i), 
        		jStart, jEnd);
        assign.setLeftExpression(leftExp);
        
        // in[i+j]
        Expression rightExp = ExpressionUtilities.getSubArrayExpression(inputs.get(0), 
        		nAboveP + jStart, nAboveP + jEnd);
        assign.setRightExpression(rightExp);

        return assign;
    }

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Demux")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error : "
                    + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // determining input dimension
        // n = input dimension
        int n = 0;
        boolean vector = true;

        GADataType inType = inputs.get(0).getDataType();
        if (inType instanceof TPrimitive) {
            n = 1;
        } else if (inType instanceof TArray) {
            TArray arDt = (TArray) inType;
            if (arDt.isVector()) {
                n = ((TArray) inputs.get(0).getDataType()).getSize();
            } else if (arDt.isVectorMatrix()) {
                n = inputs.get(0).getDataType().getCopy().normalize(true)
                        .getSize();
                vector = false;

            } else {
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "DemuxBackend",
                                "",
                                "Block input dimension error : Demux block can't handle data types of greater dimensionality than 1: "
                                        + block.getReferenceString());
            }
        } else {
            EventHandler.handle(EventLevel.ERROR, "DemuxBackend", "",
                    "Unexpected input type: " + inType.getReferenceString()
                            + "\nBlock: " + block.getReferenceString());
        }

        // determining output number
        // p = output number
        int p = outputs.size();

        // Error if output number is higher than input dimension
        if (p > n) {
            EventHandler.handle(EventLevel.ERROR, "DemuxBackend", "",
                    "Input signal dimension lower than outputs number : "
                            + block.getReferenceString(), "");
        }

        // Read and preprocess the "Outputs" parameter
        Parameter paramOutputs = block.getParameterByName("Outputs");
        Assert.assertNotNull(paramOutputs, "Parameter 'Outputs' not defined : "
                + block.getReferenceString());
        Expression value = paramOutputs.getValue();
        int[] outputDimensions = CodeModeUtilities.expressionToIntArray(value);
        if (outputDimensions == null) {
        	EventHandler.handle(EventLevel.ERROR, getClass().getName(), "", 
        			"Unexpected kind of expression in parameter \"Outputs\"" + 
        					value.getReferenceString());
        	return null;
        }

        // declaration of output assign statement
        AssignStatement assign = null;

        // p = outputNumber ; n = inputDimension
        // see D2.24 document, part 4.16-Demux for algorithm explanation

        // if (userDefinedDimension.size() == 0) {
        if (outputDimensions.length == 0) {
            EventHandler.handle(EventLevel.ERROR, "DemuxBackend", "",
                    "Parameter 'Outputs' hasn't expected format : "
                            + block.getReferenceString(), "");
        } else if (outputDimensions.length == 1) {
            // no specified user-defined dimension : automatic output dimension
            // assign
            // p=1
            if (p == 1) {
                assign = new AssignStatement();
                assign.setOperator(AssignOperator.SIMPLE_ASSIGN);
                assign.setLeftExpression(outputs.get(0).getCopy());
                assign.setRightExpression(inputs.get(0).getCopy());

                assign.setName(block.getName() + "Assign");

                codeModel.add(assign);
            } else if (p == n) {
                for (int i = 0; i < p; i++) {

                    if (vector) {
                        assign = new AssignStatement(
                                AssignOperator.SIMPLE_ASSIGN, outputs.get(i)
                                        .getCopy(), BackendUtilities.varIndex(
                                        inputs.get(0), i));
                    } else {
                        List<Expression> dims = inputs.get(0).getDataType()
                                .getDimensions();

                        if (dims.get(0) instanceof IntegerExpression
                                && dims.get(1) instanceof IntegerExpression) {
                            if (((IntegerExpression) dims.get(0)).getIntValue() == 1) {

                                assign = new AssignStatement(
                                        AssignOperator.SIMPLE_ASSIGN, outputs
                                                .get(i).getCopy(),
                                        BackendUtilities.varIndex2(inputs
                                                .get(0), 0, i));

                            } else if (((IntegerExpression) dims.get(1))
                                    .getIntValue() == 1) {

                                assign = new AssignStatement(
                                        AssignOperator.SIMPLE_ASSIGN, outputs
                                                .get(i).getCopy(),
                                        BackendUtilities.varIndex2(inputs
                                                .get(0), i, 0));

                            } else {
                                EventHandler.handle(EventLevel.ERROR,
                                        "DemuxBackend", "",
                                        "A vector matrix should have one of his dimension equal to 1 :"
                                                + block.getReferenceString(),
                                        "");
                            }
                        }
                    }

                    // giving a name to the assign statement
                    assign.setName(block.getName() + "Assign_" + i + "_" + i);

                    codeModel.add(assign);

                }
            }
            // p < n with n mod p = 0
            else if ((p <= n) && ((n % p) == 0)) {
                // p output signals of n/p dimension
                for (int i = 0; i < p; i++) {
                	assign = assignOutput(block, i, 0, (n / p) - 1, inputs, outputs, 
                			i * n / p);

                    // giving a name to the assign statement
                    assign.setName(block.getName() + "Assign_" + i);

                    codeModel.add(assign);
                }
            }
            // p < n with n mod p = m
            else if ((p <= n) && ((n % p) != 0)) {
            	int k = 0; // Base index of the input vector
                int m = n % p;
                for (int i = 0; i < m; i++) {
                	assign = assignOutput(block, 
                			i, 
                			0, ((Integer) n / p) , 
                			inputs, outputs, 
                			k);
                	k += n/p + 1;
                	assign.setName(block.getName() + "Assign_" + i);
                    codeModel.add(assign);
                }
                for (int i = m; i < p; i++) {
                	assign = assignOutput(block, 
                			i, 
                			0, ((Integer) n / p) - 1, 
                			inputs, outputs, 
                			k);
                	k += n/p;                	
                    assign.setName(block.getName() + "Assign_" + i);
                    codeModel.add(assign);
                }
            }
        } else {
            // specified user-defined dimension
        	int k = 0;
        	for (int i = 0; i < outputDimensions.length; i++) {
        		assign = assignOutput(block, i, 0, outputDimensions[i]-1, inputs, outputs, k);
        		k += outputDimensions[i];
        		
        		// giving a name to the assign statement
	        	assign.setName(block.getName() + "Assign_" + i);
                codeModel.add(assign);
        	}
        }

        return codeModel;
    }

}
