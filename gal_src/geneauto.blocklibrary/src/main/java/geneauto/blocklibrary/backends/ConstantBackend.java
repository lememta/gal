/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/ConstantBackend.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2011-09-13 10:44:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Constant block
 * 
 * 
 */
public class ConstantBackend extends SourceImplBackend {

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> outputs) {

        // checking block validity
        if (!block.getType().equals("Constant")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error : "
                    + block.getReferenceString(), "");
        }

        // checking number of outputs
        Assert.assertNotEmpty(outputs,
                "\n Block " 
        		+ block.getReferenceString()
                + "\n does not have expected number of outputs."
                + "\n (" + this.getClass().getCanonicalName() + ")");

        // reading block parameter
        Parameter valueParam = block.getParameterByName("Value");

        Assert.assertNotNull(valueParam, "Parameter 'Value' not defined : "
                + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        AssignStatement outputAssign = null;

        /*
         * output = parameter
         */

        // Output assign statement
        
// TODO (AnTo) Remove this after the normalisation has been implemented 
// in the preprocessor or typer.
        
        GADataType outPortType = block.getOutDataPorts().get(0).getDataType();
        Expression rightExp = valueParam.getCodeModelExpression();        
        
        // Convert the expression according to "VectorParams1D", if required.
        if (rightExp.getDataType().isMatrix() && 
                (outPortType.isVector() || outPortType.isScalar())) {
            
            if (rightExp instanceof GeneralListExpression) {
                // Convert the parameter expression. The ListExpression 
                // is a constant and it is only used for this assignment.
                // It is safe to reshape it, although it is not strictly required. 
                rightExp = ((GeneralListExpression) rightExp).normalizeShape(true);
                
            } else {
                // If the parameter is a reference to a variable or structure member, 
                // then do nothing. Leave the Expression as is.
                // If the parameter is a scalar, then there is also nothing to do.
            }
        }
       
// TODO (AnTo) Up to here
        
        outputAssign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0), rightExp);

        outputAssign.setName(block.getName() + "Assign");

        // updating code model
        codemodel.add(outputAssign);

        return codemodel;
    }
}
