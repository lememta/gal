/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/InOutPortBackend.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for InPort and OutPort blocks
 * 
 * 
 */
public class InOutPortBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if ((!block.getType().equals("Inport"))
                && (!block.getType().equals("Outport"))) {
            EventHandler.handle(EventLevel.ERROR, "InOutPortBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        	return null;
        }

        if ((block.getType().equals("Inport") 
        		&& block.getOutDataPorts().size() == 0)
        	|| (block.getType().equals("Outport") 
            		&& block.getInDataPorts().size() == 0)) {
        	return null;
        }
        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        AssignStatement assign = null;

        /*
         * %o1 = %i1;
         */

        Expression outExp;
        Expression inExp;
        if (outputs == null || outputs.isEmpty()) {
        	// Nothing is connected to the output of the port block
        	return null;
        } else {
        	outExp = outputs.get(0);
        }
        
        if (inputs == null || inputs.isEmpty()) {
        	// Nothing is connected to the input of the port block
        	return null;
        } else {
        	inExp = inputs.get(0);
        }
        
        assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outExp, inExp.getCopy());
        assign.setName(block.getName() + "_" + block.getParent().getName()
                + "_Assign");

        // updating code model
        codemodel.add(assign);

        return codemodel;

    }

}
