/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/DataTypeConversionBackend.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for DataTypeConversion block
 * 
 * 
 */
public class DataTypeConversionBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("DataTypeConversion")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error : "
                    + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramType = block.getParameterByName("DataType");

        if (paramType == null) {
            paramType = block.getParameterByName("OutDataTypeMode");
        }

        Assert.assertNotNull(paramType, "Parameter 'DataType' not defined : "
                + block.getReferenceString());

        // determining data type by parsing string value of the parameter
        GADataType type = DataTypeAccessor.getDataType(((StringExpression) paramType.getValue()).getLitValue());

        AssignStatement assign = null;

        // if input is scalar
        if (inputs.get(0).getDataType() instanceof TPrimitive) {

            /*
             * %o1 = (DataType)%i1;
             */

            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), new UnaryExpression(inputs.get(0).getCopy(),
                    UnaryOperator.CAST_OPERATOR, type));

            assign.setName(block.getName() + "Assign");

            // updating code model
            codemodel.add(assign);

        }
        // if input is vector
        else if (inputs.get(0).getDataType().isVector()) {

            // determining vector dimension
            int vectorDimension = Integer
                    .valueOf(((IntegerExpression) ((TArray) inputs.get(0)
                            .getDataType()).getDimensions().get(0))
                            .getLitValue());

            for (int i = 0; i < vectorDimension; i++) {

                /*
                 * %o1[i] = (DataType)%i1[i];
                 */

                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0), i),
                        new UnaryExpression(BackendUtilities.varIndex(inputs
                                .get(0).getCopy(), i),
                                UnaryOperator.CAST_OPERATOR, type));

                assign.setName(block.getName() + "Assign_" + i);

                // annotating generated code model to indicate source block of
                // the
                // code model part
                assign.setAnnotations(BackendUtilities.getAnnotation(block));

                // updating code model
                codemodel.add(assign);
            }

        }
        // if input is matrix
        else if (inputs.get(0).getDataType().isMatrix()) {

            // determining matrix dimensions
            IntegerExpression heightExpr = (IntegerExpression) ((TArray) inputs
                    .get(0).getDataType()).getDimensions().get(0);
            IntegerExpression widthExpr = (IntegerExpression) ((TArray) inputs
                    .get(0).getDataType()).getDimensions().get(1);

            int height = Integer.valueOf(heightExpr.getLitValue());
            int width = Integer.valueOf(widthExpr.getLitValue());

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {

                    /*
                     * %o1[i][j] = (DataType)%i1[i][j];
                     */

                    assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                            BackendUtilities.varIndex2(outputs.get(0), i, j),
                            new UnaryExpression(BackendUtilities.varIndex2(
                                    inputs.get(0).getCopy(), i, j),
                                    UnaryOperator.CAST_OPERATOR, type));

                    assign.setName(block.getName() + "Assign_" + i + j);

                    // annotating generated code model to indicate source block
                    // of
                    // the
                    // code model part
                    assign
                            .setAnnotations(BackendUtilities
                                    .getAnnotation(block));

                    // updating code model
                    codemodel.add(assign);
                }
            }
        }
        return codemodel;
    }
}
