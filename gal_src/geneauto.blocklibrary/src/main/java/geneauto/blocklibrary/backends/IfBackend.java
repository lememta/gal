/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/IfBackend.java,v $
 *  @version	$Revision: 1.26 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.NameSpaceElement_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;
import geneauto.utils.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for If block
 * 
 * 
 */
public class IfBackend extends CombinatorialImplBackend {

    /**
     * Auxiliary function to replace input reference (u1, u2...) in parsed
     * expression
     * 
     * @param ifExpression
     * @param inputs
     * @param block
     */
    private void replaceInputExpression(BinaryExpression ifExpression,
            List<Expression> inputs, Block block) {

        if (((ifExpression).getLeftArgument() instanceof VariableExpression)
                && (((VariableExpression) (ifExpression).getLeftArgument())
                        .getName().length() > 0)
                && (((VariableExpression) (ifExpression).getLeftArgument())
                        .getName().charAt(0) == 'u')) {

            String varName = ((VariableExpression) (ifExpression)
                    .getLeftArgument()).getName();

            int numberInput = 0;

            try {
                numberInput = Integer.valueOf(varName.substring(1, varName
                        .length()));
            } catch (Exception e) {
                EventHandler.handle(EventLevel.ERROR, "IfBackend", "",
                        "Parameter 'IfExpression' has not expected format : "
                                + block.getReferenceString(), "");
            }

            ifExpression.setLeftArgument(inputs.get(numberInput - 1).getCopy());
        }

        if (((ifExpression).getRightArgument() instanceof VariableExpression)
                && (((VariableExpression) (ifExpression).getRightArgument())
                        .getName().length() > 0)
                && (((VariableExpression) (ifExpression).getRightArgument())
                        .getName().charAt(0) == 'u')) {

            String varName = ((VariableExpression) (ifExpression)
                    .getRightArgument()).getName();

            int numberInput = 0;

            try {
                numberInput = Integer.valueOf(varName.substring(1));
            } catch (Exception e) {
                EventHandler.handle(EventLevel.ERROR, "IfBackend", "",
                        "Parameter 'IfExpression' has not expected format : "
                                + block.getReferenceString(), "");
            }

            ifExpression
                    .setRightArgument(inputs.get(numberInput - 1).getCopy());
        }

    }

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param enableInputs
     *            list of the input enable variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> enableInputs) {

        // checking block validity
        if (!block.getType().equals("If")) {
            EventHandler.handle(EventLevel.ERROR, "IfBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // reading block parameter
        Parameter paramIfExpression = block.getParameterByName("IfExpression");
        Parameter paramElseIfExpression = block
                .getParameterByName("ElseIfExpressions");
        
        Parameter paramShowElse = block.getParameterByName("ShowElse");
        boolean showElse;
        if (paramShowElse != null 
        		&& paramShowElse.getStringValue() != null
        		&& paramShowElse.getStringValue().equals("on")) {
        	showElse = true;
        } else {
        	showElse = false;
        }
        
        // count of "then" ports
        int ifPortCnt = 0;
        // last if statement created (in case of elseif statements we create
        // cascade and the else port (if such exists) shall be attached to
        // the last one
        IfStatement lastIfStatement;

        Assert.assertNotNull(paramIfExpression,
                "Parameter 'IfExpression' not defined : "
                        + block.getReferenceString());

        // creating code model
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // getting if expression from block parameter
        Expression ifExpression = null;
        try {
            ifExpression = paramIfExpression.getCodeModelExpression();
            // replacing literal expressions u1,u2... by corresponding input
            // variable expressions
            if (ifExpression instanceof BinaryExpression) {
                replaceInputExpression((BinaryExpression) ifExpression, inputs,
                        block);
            } else {
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "IfBackend",
                                "",
                                "Expression from parameter 'IfExpression' is expected to be a BinaryExpression:"
                                        + block.getReferenceString(), "");
            }
            // the if port exists
            ifPortCnt++;
        } catch (ClassCastException e) {
            EventHandler.handle(EventLevel.ERROR, "IfBackend", "",
                    "Failed to parse parameter 'IfExpression' of block "
                            + block.getReferenceString(), "");
        }

        // if statement : if(ifExpression) call_first_target();

        Pair<List<NameSpaceElement_CM>, List<Statement>> cmPair = BackendUtilities
                .getControlPortComputationCode(block.getOutControlPorts()
                        .get(0));
        // TODO Currently we support only one CompoundStatement stored into the
        // cmPair - extend the functionality to support also namespace elements
        // and multiple statements

        CompoundStatement cs = new CompoundStatement(cmPair.getRight());
        IfStatement ifStatement = new IfStatement(ifExpression, cs, null);
        lastIfStatement = ifStatement;

        if (paramElseIfExpression != null) {
            // elseIf statement : else if(elseIfExpression)
            // call_second_target();

            // getting else if expression from block parameter
            Expression elseIfExpressions = paramElseIfExpression
                    .getCodeModelExpression();
            IfStatement elseIfStmt;
            for (Expression elseIfExpression : elseIfExpressions
                    .getExpressions()) {
                try {
                    // replacing literal expressions u1,u2... by corresponding
                    // input
                    // variable expressions
                    if (elseIfExpression instanceof BinaryExpression) {
                        replaceInputExpression(
                                (BinaryExpression) elseIfExpression, inputs,
                                block);
                    }

                    cmPair = BackendUtilities
                            .getControlPortComputationCode(block
                                    .getOutControlPorts().get(ifPortCnt));

                    cs = new CompoundStatement(cmPair.getRight());
                    elseIfStmt = new IfStatement(elseIfExpression, cs, null);

                    lastIfStatement.setElseStatement(elseIfStmt);
                    lastIfStatement = elseIfStmt;
                    ifPortCnt++;
                } catch (ClassCastException e) {
                    EventHandler.handle(EventLevel.ERROR, "IfBackend", "",
                            "Failed to parse parameter 'ElseIfExpression' of block "
                                    + block.getReferenceString(), "");
                }
            }
        }

        // else statement
        if (showElse) {
            cmPair = BackendUtilities.getControlPortComputationCode(block
                    .getOutControlPorts().get(ifPortCnt));
            cs = new CompoundStatement(cmPair.getRight());
            lastIfStatement.setElseStatement(cs);
        }

        // giving a name to the if statement
        ifStatement.setName(block.getName() + "IfStatement");

        // updating code model
        codemodel.add(ifStatement);

        return codemodel;
    }

}
