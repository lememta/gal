/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/SwitchCaseBackend.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2012-02-20 09:16:49 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.NameSpaceElement_CM;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.CaseStatement;
import geneauto.models.gacodemodel.statement.DefaultCase;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gacodemodel.statement.WhenCase;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;
import geneauto.utils.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for SwitchCase block
 * 
 * 
 */
public class SwitchCaseBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("SwitchCase")) {
            EventHandler.handle(EventLevel.ERROR, "SwitchCaseBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        // Assert.assertNotEmpty(controls,
        // "Block has not expected number of outputs : "
        // + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // reading block parameter
        Parameter param = block.getParameterByName("CaseConditions");

        Assert.assertNotNull(param, "Parameter 'CaseConditions' not defined : "
                + block.getReferenceString());

        // getting the parameter value, as a ListExpression object
        Expression paramExp = param.getCodeModelExpression();

        GeneralListExpression conditions = null;
        if (paramExp instanceof GeneralListExpression) {
            conditions = (GeneralListExpression) paramExp;
        } else if (paramExp instanceof LiteralExpression) {
            conditions = new GeneralListExpression();
            conditions.addExpression((LiteralExpression) paramExp);
        } else {
            EventHandler.handle(EventLevel.ERROR, "SwitchCaseBackend", "",
                    "Parameter 'CaseConditions' has not been parsed correctly : "
                            + block.getReferenceString(), "");
        }

        // building the list of the case statements
        List<WhenCase> cases = new ArrayList<WhenCase>();

        WhenCase currentCase = null;
        int numOutput = 0;

        for (Expression expr : conditions.getExpressions()) {

            if (expr instanceof LiteralExpression) {

                // building the case statement corresponding to the current
                // expression in the parameter

                Pair<List<NameSpaceElement_CM>, List<Statement>> cmPair = BackendUtilities
                        .getControlPortComputationCode(block
                                .getOutControlPorts().get(numOutput));

                currentCase = new WhenCase(expr, cmPair.getRight());

                // adding the built case to the case list
                cases.add(currentCase);

                // incrementing the output number to assign true value to the
                // output corresponding to the current parameter expression
                numOutput++;

            } else if (expr instanceof GeneralListExpression) {

                // if current parameter expression is a list, add a case
                // statement for each element of this sub-list, but incrementing
                // the outputNumber just one time
                for (Expression subExpr : ((GeneralListExpression) expr)
                        .getExpressions()) {

                    if (subExpr instanceof LiteralExpression) {

                        Pair<List<NameSpaceElement_CM>, List<Statement>> cmPair = BackendUtilities
                                .getControlPortComputationCode(block
                                        .getOutControlPorts().get(numOutput));

                        currentCase = new WhenCase(subExpr, cmPair.getRight());

                        // adding the built case to the case list
                        cases.add(currentCase);

                    } else {
                        EventHandler.handle(EventLevel.ERROR,
                                "SwitchCaseBackend", "",
                                "Case condition in a sub list must be a literal expression : "
                                        + block.getReferenceString(), "");
                    }

                }
                numOutput++;

            } else {
                EventHandler.handle(EventLevel.ERROR, "SwitchCaseBackend", "",
                        "Case condition must be a literal expression or a list expression : "
                                + block.getReferenceString(), "");
            }

        }

        // building the default statement
        Parameter paramShowDefault = block
                .getParameterByName("CaseShowDefault");

        Assert.assertNotNull(paramShowDefault,
                "Parameter 'CaseShowDefault' not defined : "
                        + block.getReferenceString());
        
        DefaultCase defaultCase = null;
        
        if(((StringExpression) paramShowDefault.getValue()).getLitValue().equals("on")){
            Pair<List<NameSpaceElement_CM>, List<Statement>> cmPair = BackendUtilities
            .getControlPortComputationCode(block.getOutControlPorts().get(
                    numOutput));
            
            defaultCase = new DefaultCase(cmPair.getRight());
            

        }

        CaseStatement switchStatement = new CaseStatement(new UnaryExpression(
                inputs.get(0).getCopy(), UnaryOperator.CAST_OPERATOR,
                new TRealInteger(8, true)), cases, defaultCase);

        // creating code model
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // giving a name to the switch statement
        switchStatement.setName(block.getName() + "IfStatement");

        codemodel.add(switchStatement);

        return codemodel;
    }
}
