/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/SignumBackend.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.FunctionArgument_CM;
import geneauto.models.gacodemodel.FunctionBody;
import geneauto.models.gacodemodel.Function_CM;
import geneauto.models.gacodemodel.SequentialFunctionBody;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.ReturnStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Signum block.
 * 
 * 
 */
public class SignumBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Signum")) {
            EventHandler.handle(EventLevel.ERROR, "SignumBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // define input data type
        GADataType type = inputs.get(0).getDataType();
        GADataType primitiveType = null;

        // defining sign function
        /*
         * int sign(DataType val){ if(val > 0) return 1; else if(val < 0) return
         * -1; else return 0; }
         */

        Function_CM sign = new Function_CM();

        // setting name
        sign.setName("sign");

        // setting argument
        ArrayList<FunctionArgument_CM> arguments = new ArrayList<FunctionArgument_CM>();

        FunctionArgument_CM arg1 = new FunctionArgument_CM();
        arg1.setName("val");

        if (type instanceof TPrimitive) {
            primitiveType = type;
            arg1.setDataType(primitiveType);
        } else if (type.isVector()) {
            primitiveType = ((TArray) type).getBaseType();
            arg1.setDataType(primitiveType);
        } else if (type.isMatrix()) {
            primitiveType = ((TArray) type).getBaseType();
            arg1.setDataType(primitiveType);
        } else {
            EventHandler.handle(EventLevel.ERROR, "SignumBackend", "",
                    "Input type error : " + block.getReferenceString(), "");
        }

        arguments.add(arg1);
        sign.setArguments(arguments);

        // setting function output type
        sign.setDataType(primitiveType);

        ArrayList<Statement> signStatements = new ArrayList<Statement>();

        /*
         * if(val > 0) return 1; else if(val < 0) return -1; else return 0;
         */
        signStatements.add(new IfStatement(new BinaryExpression(
                new VariableExpression(arg1), LiteralExpression.getInstance(
                        "0", primitiveType), BinaryOperator.GT_OPERATOR),
                new ReturnStatement(LiteralExpression.getInstance("1",
                        primitiveType)), new IfStatement(new BinaryExpression(
                        new VariableExpression(arg1), LiteralExpression
                                .getInstance("0", primitiveType),
                        BinaryOperator.LT_OPERATOR), new ReturnStatement(
                        LiteralExpression.getInstance("-1", primitiveType)),
                        new ReturnStatement(LiteralExpression.getInstance("0",
                                primitiveType)))));

        FunctionBody signBody = new SequentialFunctionBody(null, signStatements);

        // setting function body
        sign.setBody(signBody);

        // adding function to generated code model
        codemodel.add(sign);

        // applying created function to the block input
        AssignStatement assign = null;

        if (type instanceof TPrimitive) {

            /*
             * %o1 = sign(%i1);
             */

            ArrayList<Expression> parameters = new ArrayList<Expression>();
            parameters.add(inputs.get(0).getCopy());

            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), new CallExpression("sign", parameters, type));

            assign.setName(block.getName() + "Assign");

            codemodel.add(assign);
        }
        else if (type.isVector()) {

            /*
             * %o1[0] = sign(%i1[0]); ...
             */

            // determining vector dimension
            int vectorDimension = Integer
                    .valueOf(((IntegerExpression) ((TArray) inputs.get(0)
                            .getDataType()).getDimensions().get(0))
                            .getLitValue());

            for (int i = 0; i < vectorDimension; i++) {

                ArrayList<Expression> parameters = new ArrayList<Expression>();
                parameters.add(BackendUtilities.varIndex(inputs.get(0), i));

                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0), i),
                        new CallExpression("sign", parameters, ((TArray) type)
                                .getBaseType()));

                assign.setName(block.getName() + "Assign_" + i);

                codemodel.add(assign);
            }
        }
        else if (type.isMatrix()) {

            /*
             * %o1[0][0] = sign(%i1[0][0]); ...
             */

            // determining matrix dimensions
            IntegerExpression heightExpr = (IntegerExpression) ((TArray) inputs
                    .get(0).getDataType()).getDimensions().get(0);
            IntegerExpression widthExpr = (IntegerExpression) ((TArray) inputs
                    .get(0).getDataType()).getDimensions().get(1);

            int height = Integer.valueOf(heightExpr.getLitValue());
            int width = Integer.valueOf(widthExpr.getLitValue());

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {

                    ArrayList<Expression> parameters = new ArrayList<Expression>();
                    parameters.add(BackendUtilities.varIndex2(inputs.get(0), i,
                            j));

                    assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                            BackendUtilities.varIndex2(outputs.get(0), i, j),
                            new CallExpression("sign", parameters,
                                    ((TArray) type).getBaseType()));

                    assign.setName(block.getName() + "Assign_" + i + j);

                    codemodel.add(assign);
                }
            }
        }
        return codemodel;
    }
}
