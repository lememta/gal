/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/ChartBackend.java,v $
 *  @version	$Revision: 1.46 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockBackend;
import geneauto.models.gacodemodel.CodeModelDependency;
import geneauto.models.gacodemodel.Function_CM;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.BlankStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.ExpressionStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gastatemodel.Event;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.EnableHandler;
import geneauto.models.utilities.statemodel.StateModelUtilities;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChartBackend implements BlockBackend {
	
	/**
	 * Return declaration code (global variable or statement that need to be
	 * declared before block computation code)
	 * 
	 * @param block
	 *            block for which is called the backend
	 * @return GACodeModel fragment describing declarations involve by block
	 */
	public List<GAModelElement> getDeclarationCode(
			Block block, List<Expression> enableInputs) {
		// Model declaration
		List<GAModelElement> codeModel = new ArrayList<GAModelElement>();
		ChartBlock cb = (ChartBlock) block;
		
		// Declare edge-enabler memory variable if edge-enabled
		// NB! Keep it first as it is referred later by index.
		if (cb.getInEdgeEnablePort()!=null) {
			// NB! Only one edge-enable port is allowed in case of charts but
			// there can be a vector of inputSignals and vector of events
			// related to it.

			// Create the memory variable (could be a vector)
			Variable_CM memVar = EnableHandler.makeEdgeEnableMemory(
											enableInputs.get(0), block);
			codeModel.add(memVar);
		}

		// If we have IO variables
		if (cb.getInstanceStructType()!=null) {
		    // Create a variable for the IO struct
			Variable_CM instanceStruct = new Variable_CM();
			// The IO struct must be exported, when the block has IncontrolPort-s. 
			// Otherwise, we generally can't export it, because there will be an issue 
			// with recursive #includes in C.
			if (!block.getInControlPorts().isEmpty()) {
			    instanceStruct.setScope(DefinitionScope.EXPORTED);
			} else {
                instanceStruct.setScope(DefinitionScope.LOCAL);
			}			
			instanceStruct.setDataType(cb.getInstanceStructType());
			String name = StateModelUtilities.normaliseName(cb.getName())
			            + "_io_" + block.getName();
			instanceStruct.setName(name);
			
			// Add a reference expression in the ChartBlock			
            Expression instanceRefExpression = 
            							new VariableExpression(instanceStruct);
			// store the reference expression into TempModel
			cb.getModel().getCodeModel().addTempElement(instanceRefExpression);
			cb.setInstanceOuterReference(instanceRefExpression);
			
			codeModel.add(instanceStruct);
		}
		
		return codeModel;
	}

	/**
	 * Return initialisation code
	 * 
	 * @param block
	 *            block for which is called the backend
	 * @return GACodeModel fragment describing block initialisation code
	 */
	public List<GAModelElement> getInitCode(Block block, List<Expression> mems) {
		List<GAModelElement> codemodel = new ArrayList<GAModelElement>();
		ChartBlock cb 			  = (ChartBlock) block;

		// Initialise the enablers memory if enabler is present
		// Declare edge-enabler memory variable if edge-enabled
		if (cb.getInEdgeEnablePort()!=null) {
			AssignStatement assign = new AssignStatement(
					AssignOperator.SIMPLE_ASSIGN,
					// We put it there in getDeclarationCode
					mems.get(0).getCopy(),
					new IntegerExpression(0));
			assign.setName(block.getName() + " edge-enabler memory");

			codemodel.add(assign);
		}
		
		// Function call command
		Function_CM initFunction  = cb.getInitFunction().getCodeModelElement();
		List<Expression> args     = new ArrayList<Expression>();
		// Add instance argument, when required
		if (cb.getInstanceOuterReference() != null) {
			Expression argExpr 		  = new UnaryExpression(
			        cb.getInstanceOuterReference().getCopy(), 
			        UnaryOperator.REF_OPERATOR);
			args.add(argExpr);
		}
		
		// Add states argument
		if (cb.getStateVariablesStructType() != null) {
			args.add(new VariableExpression(GAConst.VAR_MEM_NAME));
		}

		// Create call statement
		CallExpression callExpression = new CallExpression(initFunction, args);
		callExpression.setDependency(new CodeModelDependency(initFunction.getModule()));
		Statement  callStmt	= new ExpressionStatement(callExpression);
		codemodel.add(callStmt);
		
		return codemodel;
	}

	/**
	 * Return computation code (functional handling done by the block)
	 * 
	 * @param block
	 *            block for which is called the backend
	 * @param inputs
	 *            list of the input variables of the block
	 * @param outputs
	 *            list of the output variables of the block
	 * @param controls
	 *            list of the input control variables of the block
	 * @return GACodeModel fragment describing block behaviour
	 */
	public List<GAModelElement> getComputationCode(Block block,
			List<Expression> inputs, List<Expression> outputs,
			List<Expression> enableInputs, List<Expression> mems) {

		List<GAModelElement> codemodel = new ArrayList<GAModelElement>();
		ChartBlock cb = (ChartBlock) block;
		
		// Compose the computation code (statements)
		List<Statement> compCode = new ArrayList<Statement>();
		
		// 1. Assign statements to evaluate inputs of the IO structure
		AssignStatement assignStmt = null;
		for (InDataPort p : cb.getInDataPorts()) {
			// Assume that the inputs are ordered
			int index = p.getPortNumber()-1;
			
			// If the input ports have incoming signals then
			// create a read from the signal
			if (inputs != null && inputs.get(index) != null) {
                Expression right = inputs.get(index).getCopy();
    			Expression left = cb.getCodeModelReferenceForPort(p.getName());
    			assignStmt = new AssignStatement(
    					AssignOperator.SIMPLE_ASSIGN, left, right);
    			compCode.add(assignStmt);
			}
		}
		
		// 2. Function call expression
		Function_CM compFunction  = cb.getComputeFunction().getCodeModelElement();
		List<Expression> args     = new ArrayList<Expression>();
		// Add instance argument, when required
		if (cb.getInstanceOuterReference() != null) {
	        Expression argExpr = new UnaryExpression(
	                cb.getInstanceOuterReference().getCopy(),
	                UnaryOperator.REF_OPERATOR);
			args.add(argExpr);
		}
		
		// Add states argument
		if (cb.getStateVariablesStructType() != null) {
			args.add(new VariableExpression(GAConst.VAR_MEM_NAME));
		}
		
		CallExpression callExpression = new CallExpression(compFunction, args);		
        callExpression.setDependency(new CodeModelDependency(compFunction.getModule()));
		Statement callStmt = new ExpressionStatement(callExpression);
		compCode.add(callStmt);
		
		// 3. Assign IO structure outputs to the outputs
		for (OutDataPort p : block.getOutDataPorts()) {
			// Assume that the inputs are ordered
			int index = p.getPortNumber()-1;
            // If the input ports have incoming signals then
            // create a read from the signal
            if (outputs != null && outputs.get(index) != null) {
    			Expression left = outputs.get(index).getCopy();
    			Expression right = cb.getCodeModelReferenceForPort(p.getName());
    			assignStmt = new AssignStatement(
    					AssignOperator.SIMPLE_ASSIGN, left, right);
    			compCode.add(assignStmt);
            }
		}

		// Refine particular event and add the proper reference
		if (cb.getInControlPorts().size()>0) {
			for (InControlPort icp : cb.getInControlPorts()) {
				// Get copy of the computation code
				List<Statement> icpCc = new ArrayList<Statement>();
				for (Statement s : compCode) {
					icpCc.add(s.getCopy());
				}
				// Add the missing argument to the call expression
				for (Statement s : icpCc) {
					if (s instanceof ExpressionStatement) {
						// Assume that the ExpressionStatement is the 
						// only one we are looking for 
			           	((CallExpression)((ExpressionStatement)s)
			           			.getExpression()).addArgument(
			           					icp.getEvent().getReferenceExpression().getCopy());
			           	break;
					}
				}
				
				// Create a compound statement
				CompoundStatement cs = new CompoundStatement(null, icpCc);
				cs.addAnnotation("Call block " + cb.getOriginalFullName());

				// Store the statements to the inControlPort
				icp.getModel().getCodeModel().addTempElement(cs);
				icp.addComputeCodeStatement(cs);
			}
		} else if (cb.getInEdgeEnablePort()!=null) {
			// NB! Only one edge-enable port is allowed in case of charts but
			// there can be a vector of inputSignals and vector of events
			// related to it.
		    
            // Add conditional chart execution code for all input events
            
            // Sort the events first to get the right evaluation order. 
            // TODO Find out why the events appear out of order here,
            // They are sorted in the importer and appear sorted in the 
            // output xml.
            Collections.sort(cb.getInEdgeEnablePort().getEvents());
            
            int n = cb.getInEdgeEnablePort().getEvents().size();
			for (int i=0; i<n; i++) {
				Event e = cb.getInEdgeEnablePort().getEvents().get(i);
				// Get copy of the computation code
				List<Statement> icpCc = new ArrayList<Statement>();
				for (Statement s : compCode) {
					icpCc.add(s.getCopy());
				}

				// Add the missing argument to the call expression
				for (Statement s : icpCc) {
					if (s instanceof ExpressionStatement) {
						// Assume that the ExpressionStatement is the 
						// only one we are looking for 
			           	((CallExpression)((ExpressionStatement)s)
			           			.getExpression()).addArgument(
			           					e.getReferenceExpression());
			           	break;
					}
				}

				// Create a compound statement
				CompoundStatement cs = new CompoundStatement(null, icpCc);
				cs.addAnnotation("Call block "+cb.getOriginalFullName());
				
				int enableIdx;
				if (n == 1) {
				    enableIdx = -1;
				} else {
				    enableIdx = i;
				}

	            // Create the if-statement
            	List<Statement> stmts = EnableHandler.getEdgeEnableCode(
            				e.getTriggerType(),
            				enableInputs.get(0),
            				e.getName(),
            				// We put it on first place in getDeclarationCode
            				mems.get(0),
            				enableIdx);

            	// Create a comment statement with a space.
            	String comnt = "Conditional execution of Chart " 
                    + block.getName() + " for event " + e.getName();
            	BlankStatement bst = new BlankStatement(comnt);
            	bst.setEmptyLinesBefore(1);
                codemodel.add(bst);

                // Update the then-statement of the if-statement
                ((IfStatement) stmts.get(0)).setThenStatement(cs);

            	// Add the if statement to the return object
				codemodel.addAll(stmts);
			}
		} else {
			// The default case
		    Expression evtRef = cb.getDefaultEventReference();
		    if (evtRef != null) {
	            callExpression.addArgument(evtRef.getCopy());		        
		    } else {
		        EventHandler.handle(EventLevel.ERROR, "ChartBackend", "", 
		                "Default event reference not defined in an unconditionally executed Chart." +
		                "\nChart: " + cb.getReferenceString());		        
		    }
    		
            // Create a comment statement with a space.
            String comnt = "Executing Chart " + block.getName();
            BlankStatement bst = new BlankStatement(comnt);
            bst.setEmptyLinesBefore(1);
            codemodel.add(bst);
            
            // Add the computation code to the return object
    		codemodel.addAll(compCode);
		}

		return codemodel;
	}

	/**
	 * For the future - maybe one day we will support ChartBlocks having
	 * directFeedThrough=false
	 */
	public List<GAModelElement> getUpdateMemoryCode(Block block,
			List<Expression> inputs, List<Expression> controls) {
		return null;
	}

}
