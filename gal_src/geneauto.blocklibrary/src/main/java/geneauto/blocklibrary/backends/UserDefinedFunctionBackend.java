/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/UserDefinedFunctionBackend.java,v $
 *  @version	$Revision: 1.14 $
 *	@date		$Date: 2010-11-03 17:16:58 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.UserDefinedBlockBackend;
import geneauto.models.gacodemodel.Dependency;
import geneauto.models.gacodemodel.ExternalDependency;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.ExpressionStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.FunctionPrototypeParserEML;

import java.util.ArrayList;
import java.util.List;

public class UserDefinedFunctionBackend implements UserDefinedBlockBackend {

    public List<GAModelElement> getDeclarationCode(Block block) {

        return null;
    }

    public List<GAModelElement> getInitCode(Block block, Expression memory) {

        return null;
    }

    public List<GAModelElement> getComputationCode(Block block,
			List<Expression> inputs, List<Expression> outputs,
			List<Expression> controls, List<Expression> memory) {
	    String paramString = "";
	    BlockType blockType = block.getBlockType();
	    Parameter headerFileNameParam = block.getParameterByName("headerFileName");
	    
	    // define header file name
	    String headerFileName = null;
	    if (headerFileNameParam != null) {
	        headerFileName = headerFileNameParam.getStringValue(); 
	    }
	    
	    // when header file name is not available from block parameter 
	    // the value from block library is used for header file name
	    if (headerFileName == null) {
            headerFileName = blockType.getHeaderFileName();
        }
        
	     
	    if (blockType.getFunctionSignature() != null) {
            paramString = blockType.getFunctionSignature();
        } else {
            Parameter param = block.getParameterByName("SFunctionSpec");
            paramString = param.getValue().getStringValue();
        }
	    
	    FunctionPrototypeParserEML funParser = new FunctionPrototypeParserEML(
                paramString, block);
	    Statement st; 
	    Expression resultExp = funParser.getResultExpression(outputs);
	    List<Expression> argExps = funParser.getArgumentList(inputs, outputs);

		CallExpression callExp = new CallExpression(
				funParser.getFunctionName(), 
		        argExps,
		        funParser.getReturnType());
		
		// header file name was defined -> add external dependency to
		// the call expression
		if (headerFileName != null) {
		    Dependency dep = new ExternalDependency(headerFileName);
            callExp.setDependency(dep);
        }
		
		if (resultExp != null) {
		    st = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, resultExp, callExp);
		} else {
		    st = new ExpressionStatement(callExp);
		}
		List<GAModelElement> result = new ArrayList<GAModelElement>();
		result.add(st);
	    return result;
	}

    public List<GAModelElement> getUpdateMemoryCode(Block block,
            List<Expression> inputs, List<Expression> controls,
            List<Expression> memory) {

        return null;
    }

}
