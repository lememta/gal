/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/WriteMapArrayBackend.java,v $
 *  @version	$Revision: 1.21 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for WriteMapArray block.
 * 
 * 
 */
public class WriteMapArrayBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("WriteMapArray")) {
            EventHandler.handle(EventLevel.ERROR, "WriteMapArrayBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        /*
         * int index; for (index=0; index<vector_size; index++){ if (index ==
         * position) { %o1[index] = %i3; } else { %o1[index] = %i2[index]; } }
         */

        RangeExpression range = new RangeExpression(0, 
                ((TArray) inputs.get(1).getDataType())
                .getDimensionsLength(0) - 1);
        Variable_CM var = new Variable_CM(
        		"index" + block.getId(),
        		new IntegerExpression(0), 
        		false,
                false, 
                false, 
                DefinitionScope.LOCAL, 
                range.getDataType());

        VariableExpression varIndex = new VariableExpression(var);
        // for statement initialisation
        RangeIterationStatement forState = new RangeIterationStatement(
                null,
                var, range);
        forState.addNameSpaceElement(var);

        // for statement body
        forState.setBodyStatement(new IfStatement(
        		new BinaryExpression(
        				varIndex.getCopy(), 
        				inputs.get(0).getCopy(), 
        				BinaryOperator.EQ_OPERATOR),
                new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0), 
                        							varIndex.getCopy()),
                        inputs.get(2).getCopy()), 
               new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, 
                        BackendUtilities.varIndex(outputs.get(0), 
                        							varIndex.getCopy()), 
                        BackendUtilities.varIndex(inputs.get(1), 
                        							varIndex.getCopy()))));

        forState.setName(block.getName() + "ForState");

        // updating code model
        codemodel.add(forState);

        return codemodel;
    }

}
