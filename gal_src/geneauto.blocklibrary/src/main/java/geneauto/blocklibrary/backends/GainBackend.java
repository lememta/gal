/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/GainBackend.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Gain block.
 * 
 * 
 */
public class GainBackend extends CombinatorialImplBackend {

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Gain")) {
            EventHandler.handle(EventLevel.ERROR, "GainBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameters
        Parameter paramGain = block.getParameterByName("Gain");
        Parameter paramMulMode = block.getParameterByName("Multiplication");

        Assert.assertNotNull(paramGain, "Parameter 'Gain' not defined : "
                + block.getReferenceString());

        String mulMode = "";

        if (paramMulMode != null) {
            mulMode = ((StringExpression) paramMulMode.getValue()).getLitValue();
        }

        BinaryExpression productExp = null;
        Expression gainExp = paramGain.getCodeModelExpression();

        if (mulMode.equals("") || mulMode.equals("Element-wise(K.*u)")) {
            // default case is Element-wise product
            
            // In the element-wise case we treat the parameter expression
            // as a vector, if it is a vector-matrix.
// TODO (AnTo) Shape normalisation can be removed from here, if the 
// transformation is done in the preprocessor or typer already.
            GADataType outputType = outputs.get(0).getDataType();
            if (gainExp.getDataType().isVectorMatrix() && 
                    (outputType.isVector() || outputType.isScalar())) {
                gainExp = gainExp.normalizeShape(true);
            }
            
            productExp = new BinaryExpression(gainExp, inputs.get(0).getCopy(),
                    BinaryOperator.MUL_OPERATOR);
        } else if ((mulMode.equals("Matrix(K*u)"))
                || mulMode.equals("Matrix(K*u) (u vector)")) {
            if (inputs.get(0).getDataType().isVector()
                    && gainExp.getDataType().isVector()) {
                inputs.get(0).setDataType(
                        ((TArray) inputs.get(0).getDataType()).toColMatrix());
            }
            // Matrix product case with Gain value in left branch
            productExp = new BinaryExpression(gainExp, inputs.get(0).getCopy(),
                    BinaryOperator.MUL_MAT_OPERATOR);
        } else if (mulMode.equals("Matrix(u*K)")) {
            if (inputs.get(0).getDataType().isVector()
                    && gainExp.getDataType().isVector()) {
                inputs.get(0).setDataType(
                        ((TArray) inputs.get(0).getDataType()).toColMatrix());
            }
            // Matrix product case with Gain value in right branch
            productExp = new BinaryExpression(inputs.get(0).getCopy(), gainExp,
                    BinaryOperator.MUL_MAT_OPERATOR);
        }

        AssignStatement assign = null;

        // output = input * gain
        assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0), productExp);

        // giving a name to the assign statement
        assign.setName(block.getName() + "Assign");

        codemodel.add(assign);

        return codemodel;
    }

}
