/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/DataStoreMemoryBackend.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.blocklibrary.utils.BlockLibraryUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.common.VariableScope;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

/**
 * Backend class for DataStoreMemory block.
 * 
 * 
 */
public class DataStoreMemoryBackend extends CombinatorialImplBackend
        implements BlockPreProcessor {

    @Override
    public void preProcess(GASystemModel m) {
        CombinatorialBlock block;
        Parameter paramName;
        for (GAModelElement el: m.getAllElements(CombinatorialBlock.class)) {
            block = (CombinatorialBlock) el;
            if (block.getType().equals("DataStoreMemory")) {
                // reading block parameters

                // initial value
                Parameter paramIV = block.getParameterByName("InitialValue");

                // variable name
                paramName = block.getParameterByName("DataStoreName");
                Assert.assertNotNull(paramName,
                        "Parameter 'DataStoreName' not defined." +
                		"\n Block: "
                        + block.getReferenceString());

                // determine initial value and its data type
                GADataType initType = null;
                Expression initValExp;
                if (paramIV == null) {
                	// the initial value was not defined explicitly in the mdl file
                	initValExp = new IntegerExpression(0);
                	initType = initValExp.getDataType();
                } else {
                	// take value and datatype from the parameter
                	initType = paramIV.getDataType();
                	initValExp = paramIV.getValue();
                }
                
                GADataType dataType = BlockLibraryUtil.getOutportType(block, 
                		initType, 
                		this.getClass().getCanonicalName() + ".preProcess()");

                Assert.assertNotNull(dataType,
                        "Could not derive data type for the state variable!" +
                		"\n Block: "
                        + block.getReferenceString());

                // variable declaration
                Variable_SM memory = new Variable_SM();
                memory.setDataType(dataType);
                memory.setName(paramName.getStringValue());
                memory.setInitialValue(initValExp);
                memory.setScope(VariableScope.EXPORTED_VARIABLE);
                
                // check that data store memory block's parent
                // is SystemBlock, print an error if not
                if (block.getParent() instanceof SystemBlock) {
                    // add variable to system block
                    ((SystemBlock) block.getParent()).addVariable(memory);
                } else {
                    EventHandler.handle(EventLevel.ERROR, 
                    		this.getClass().getCanonicalName() + ".preProcess()", 
                            "", 
                            "Incorrect parent type, expected SystemBlock,"
                            + " found: " 
                            + (block.getParent() == null? 
                                    "null": block.getParent().getClass())
                            + "\n Block: "
                            + block.getReferenceString());
                }
            }
        }
    }

}
