/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/MathBackend.java,v $
 *  @version	$Revision: 1.19 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Math block.
 * 
 * 
 */
public class MathBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Math")) {
            EventHandler.handle(EventLevel.ERROR, "MathBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramFunction = block.getParameterByName("Function");
        
        // determining output data type
        GADataType dt = outputs.get(0).getDataType();

        Assert.assertNotNull(paramFunction,
                "Parameter 'Function' not defined : "
                        + block.getReferenceString());

        String function = ((StringExpression) paramFunction.getValue()).getLitValue();

        // output assign statement
        AssignStatement assign;

        if (function.equals("mod")) {
			// TODO The two branches below should be merged. Data type specific
			// handling should be done in the print pre-processing step. AnTo 20.07.12
            /*
             * %o1 = %i1 % %i2;
             */
            if (inputs.get(0).getDataType().getPrimitiveType() instanceof TRealInteger) {
                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        outputs.get(0), new BinaryExpression(inputs.get(0)
                                .getCopy(), inputs.get(1).getCopy(),
                                BinaryOperator.MOD_OPERATOR));
            } else {
            	
                /*
                 * %o1 = FMod(%i1,%i2);
                 */
                ArrayList<Expression> args = new ArrayList<Expression>();
                args.add(inputs.get(0).getCopy());
                args.add(inputs.get(1).getCopy());

                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        outputs.get(0), 
                        new CallExpression(
                        		"fmod", 
                        		args,
                        		dt));
            }
            assign.setName(block.getName() + "Assign");
            codemodel.add(assign);

        } else if (function.equals("rem")) {

                /*
                 * %o1 = remainder(%i1,%i2);
                 */
                ArrayList<Expression> args = new ArrayList<Expression>();
                args.add(inputs.get(0).getCopy());
                args.add(inputs.get(1).getCopy());

                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        outputs.get(0), 
                        new CallExpression(
                        		"remainder", 
                        		args,
                        		dt));
                assign.setName(block.getName() + "Assign");
                codemodel.add(assign);

        } else if (function.equals("square")) {
        	
            /*
             * %o1 = %i1 * i1;
             */

            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), new BinaryExpression(inputs.get(0).getCopy(),
                    inputs.get(0).getCopy(), BinaryOperator.MUL_OPERATOR));
            assign.setName(block.getName() + "Assign");
            codemodel.add(assign);

        } else if (function.equals("reciprocal")) {

            /*
             * %o1 = 1 / %i1;
             */
            DoubleExpression realExp = new DoubleExpression(1.0);
            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), new BinaryExpression(realExp, inputs.get(0).getCopy(),
                    BinaryOperator.DIV_OPERATOR));
            assign.setName(block.getName() + "Assign");
            codemodel.add(assign);
            
        } else if (function.equals("10^u")) {

            /*
             * %o1 = pow(10,%i1);
             */

            ArrayList<Expression> args = new ArrayList<Expression>();
            IntegerExpression intExp = new IntegerExpression(10);
            args.add(intExp);
            args.add(inputs.get(0).getCopy());

            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), 
                    new CallExpression("pow", 
                    		args,
                    		dt));
            assign.setName(block.getName() + "Assign");
            codemodel.add(assign);

        } else if (function.equals("hypot")) {

            /*
             * %o1 = sqrt(%i1%i1 + %i2%i2);
             */

            ArrayList<Expression> args = new ArrayList<Expression>();
            Expression sqr1Exp = new BinaryExpression(inputs.get(0).getCopy(), inputs
                    .get(0).getCopy(), BinaryOperator.MUL_OPERATOR);
                    
            Expression sqr2Exp = new BinaryExpression(inputs.get(1).getCopy(), inputs
                    .get(1).getCopy(), BinaryOperator.MUL_OPERATOR);
            
            Expression sumExp = new BinaryExpression(sqr1Exp, sqr2Exp, BinaryOperator.ADD_OPERATOR);

            args.add(sumExp);
            
            Expression sqrtExp = new CallExpression("sqrt", args, dt);
            
            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), sqrtExp);
            assign.setName(block.getName() + "Assign");
            codemodel.add(assign);

        } else if (function.equals("pow")) {

            /*
             * %o1 = pow(%i1,%i2);
             */

            ArrayList<Expression> args = new ArrayList<Expression>();
            args.add(inputs.get(0).getCopy());
            args.add(inputs.get(1).getCopy());

            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), 
                    new CallExpression("pow", 
                    		args,
                    		dt));
            assign.setName(block.getName() + "Assign");
            codemodel.add(assign);

        } else if (function.equals("transpose")) {

            /*
             * We expect the input two be a matrix (2D array) in this case
             * Create a double for loop for performing the transposition
             * 
             * TODO Factor the transpose operation to a standard function that
             * is only expanded in the print preprocessing step, if required.
             * AnTo 20.07.12
             */
        	
        	TArray inType = ((TArray) inputs.get(0).getDataType());
        	
			RangeExpression range1 = new RangeExpression(0, inType.getDimensionsLength(0) - 1);
			RangeExpression range2 = new RangeExpression(0, inType.getDimensionsLength(1) - 1);
			
			Variable_CM indexVar1 = new Variable_CM("i1",
					null, false, false, false, DefinitionScope.LOCAL,
					range1.getDataType());
			Variable_CM indexVar2 = new Variable_CM("i2",
					null, false, false, false, DefinitionScope.LOCAL,
					range2.getDataType());

			VariableExpression index1 = new VariableExpression(indexVar1);
			VariableExpression index2 = new VariableExpression(indexVar2);
			
			Expression assignLeft = outputs.get(0).getCopy();
			assignLeft.addIndexExpression(index1);
			assignLeft.addIndexExpression(index2);
			
			Expression assignRight = inputs.get(0).getCopy();
			assignRight.addIndexExpression(index2.getCopy());
			assignRight.addIndexExpression(index1.getCopy());
			
			assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, 
					assignLeft, assignRight); 
			
			// Inner loop
			RangeIterationStatement for2 = 
					new RangeIterationStatement(assign, indexVar2, range2);
            for2.addNameSpaceElement(indexVar2);
			
			// Outer loop
			RangeIterationStatement for1 = 
					new RangeIterationStatement(for2, indexVar1, range1);
            for1.addNameSpaceElement(indexVar1);
            
            for1.setName(block.getName() + "forStatement");

            codemodel.add(for1);

        } else {

            /*
             * %o1 = func(%i1);
             */

            ArrayList<Expression> args = new ArrayList<Expression>();
            args.add(inputs.get(0).getCopy());
            
            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), new CallExpression(function, 
                    		args,
                    		dt));
            
            assign.setName(block.getName() + "Assign");
            codemodel.add(assign);
        }

        return codemodel;
    }

}
