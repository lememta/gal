/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/AbsBackend.java,v $
 *  @version	$Revision: 1.31 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealNumeric;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Abs block
 * 
 * 
 */
public class AbsBackend extends CombinatorialImplBackend {

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Abs")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error : "
                    + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // determining input signal type
        GADataType type = inputs.get(0).getDataType();

        String function = "";
        if (type.getPrimitiveType() instanceof TRealNumeric) {
            // abs function is mapped in printer to correct numeric precision
            function = "abs";
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                    .getCanonicalName(), "", "Incorrect input type "
                    + DataTypeAccessor.toString(type.getPrimitiveType())
                    + " for Abs block\n " + block.getReferenceString());
        }

        /*
         * TODO(TF546) Remove special handling of vector and matrix on
         * callExpression when unfolding call expressions will be implemented in
         * C-printer
         */

        if (type instanceof TPrimitive) {

            ArrayList<Expression> args = new ArrayList<Expression>();
            args.add(inputs.get(0).getCopy());

            CallExpression call = new CallExpression(function, args, type);

            AssignStatement assign = new AssignStatement(
                    AssignOperator.SIMPLE_ASSIGN, outputs.get(0), call);

            // giving a name to the if statement
            assign.setName(block.getName() + "IfStatement");

            codemodel.add(assign);

        } else if (type.isVector()) {

            RangeExpression range = new RangeExpression(0, (
                        (TArray) outputs.get(0).getDataType())
                                        .getDimensionsLength(0) - 1); 
            Variable_CM indexVar = new Variable_CM(
            		"index" + block.getId(),
            		null, false, false, false,
                    DefinitionScope.LOCAL, 
                    range.getDataType());

            VariableExpression index = new VariableExpression(indexVar);

            ArrayList<Expression> args = new ArrayList<Expression>();
            args.add(BackendUtilities.varIndex(inputs.get(0), index));
            
            RangeIterationStatement forState = new RangeIterationStatement(
                    new AssignStatement(
                            AssignOperator.SIMPLE_ASSIGN,
                            BackendUtilities.varIndex(outputs.get(0), 
                                    index),
                            new CallExpression(
                                    function, 
                                    args, 
                                    ((TArray) type).getPrimitiveType())),
                    indexVar,
                    range);

            forState.addNameSpaceElement(indexVar);
            // giving a name to the if statement
            forState.setName(block.getName() + "forStatement");

            codemodel.add(forState);

        } else if (type.getDimensionality() == 2) {

            RangeExpression rangeX = new RangeExpression(0, ((TArray) inputs.get(0).getDataType())
                    .getDimensionsLength(0) - 1);
            RangeExpression rangeY = new RangeExpression(0, ((TArray) inputs.get(0).getDataType())
                    .getDimensionsLength(1) - 1);
            Variable_CM indexVarX = new Variable_CM(
            		"index_x" + block.getId(),
            		null, false, false, false,
                    DefinitionScope.LOCAL, 
                    rangeX.getDataType());

            Variable_CM indexVarY = new Variable_CM(
            		"index_y" + block.getId(),
            		null, false, false, false,
                    DefinitionScope.LOCAL, 
                    rangeY.getDataType());

            VariableExpression indexX = new VariableExpression(indexVarX);
            VariableExpression indexY = new VariableExpression(indexVarY);

            ArrayList<Expression> args = new ArrayList<Expression>();
            args.add(BackendUtilities.varIndex2(inputs.get(0), indexX, indexY));

            
            RangeIterationStatement forStateY = new RangeIterationStatement(
                new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                    BackendUtilities.varIndex2(outputs.get(0),
                        indexX, indexY),
                    new CallExpression(function, args,
                                ((TArray) type).getPrimitiveType())),
                                    indexVarY, rangeY
                                    );
            forStateY.addNameSpaceElement(indexVarY);
            RangeIterationStatement forStateX = new RangeIterationStatement(
                    forStateY,
                    indexVarX, rangeX
                    );
            forStateX.addNameSpaceElement(indexVarX);
            
            // giving a name to the if statement
            forStateX.setName(block.getName() + "forStatement");

            codemodel.add(forStateX);
        }
        return codemodel;
    }
}
