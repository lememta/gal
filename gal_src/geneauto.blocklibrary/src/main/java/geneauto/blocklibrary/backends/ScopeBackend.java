/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/ScopeBackend.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;
import geneauto.utils.general.NameFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Scope block.
 * 
 * 
 */
public class ScopeBackend extends SinkImplBackend {

    /**
     * Return computation code (functionnal handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getDeclarationCode(Block block) {

        if ((!block.getType().equals("Scope") && (!block.getType().equals(
                "Display")))) {
            EventHandler.handle(EventLevel.ERROR, "ScopeBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // determine variable type
        GADataType type = block.getInDataPorts().get(0).getDataType()
                .toCodeModelElement();

        // variable declaration
        StructureMember data = new StructureMember();

        // set variable name
        String name = block.getName() + "_memory";

        // replace all unavailable characters (/,\...) and set variable name
        data.setName(NameFormatter.normaliseVariableName(name, 
        		block.getReferenceString(), 
        		true));

        data.setDataType(type);

        codeModel.add(data);

        return codeModel;
    }

    /**
     * Return computation code (functionnal handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     * 
     * Corresponding generated code : memory = in1
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> controls, Expression scope) {

        // checking block validity
        if ((!block.getType().equals("Scope") && (!block.getType().equals(
                "Display")))) {
            EventHandler.handle(EventLevel.ERROR, "ScopeBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // initialising code model
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();
        AssignStatement assign = null;

        // check type validity
        if (scope.getDataType().getTypeName().equals(
                inputs.get(0).getDataType().getTypeName())) {

            // assign statement : memory = in
            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, scope,
                    inputs.get(0).getCopy());

            // giving a name to the assign statement
            assign.setName(block.getName() + "Assign");

            // updating code model
            codemodel.add(assign);
        }

        return codemodel;
    }

}
