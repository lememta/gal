/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/FromWorkspaceBackend.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for FromWorkspace block.
 * 
 * 
 */
public class FromWorkspaceBackend extends SourceImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> outputs) {

        if (!block.getType().equals("FromWorkspace")) {
            EventHandler.handle(EventLevel.ERROR, "FromWorkspaceBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramVarName = block.getParameterByName("VariableName");

        Assert.assertNotNull(paramVarName,
                "Parameter 'VariableName' not defined : "
                        + block.getReferenceString());

        Variable_CM workspaceVar = new Variable_CM();
        workspaceVar
                .setName(((StringExpression) paramVarName.getValue()).getLitValue());
        workspaceVar.setDataType(new TRealDouble());
        workspaceVar.setScope(DefinitionScope.IMPORTED);
        codeModel.add(workspaceVar);

        VariableExpression workspace = new VariableExpression(workspaceVar);

        /*
         * output = workspace_var;
         */

        // output assign statement
        AssignStatement assign = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0), workspace);

        assign.setName(block.getName() + "Assign");

        // updating code model
        codeModel.add(assign);

        return codeModel;
    }

}
