/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/DataStoreReadBackend.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for DataStoreRead block.
 * 
 * 
 */
public class DataStoreReadBackend extends SourceImplBackend 
        implements BlockPreProcessor {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> outputs) {
        if (!(block instanceof SourceBlock)) {
            EventHandler.handle(EventLevel.ERROR, 
                    this.getClass().getCanonicalName(), 
                    "", "Block type error : "
                    + "\nExprexted: SourceBlock"
                    + "\nFound: " + (block == null? "null": block.getClass()));
            return null;
        }
        if (!block.getType().equals("DataStoreRead")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error : "
                    + block.getReferenceString(), "");
        }

        // checking number of outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // output assign statement
        AssignStatement outputAssign = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, 
                outputs.get(0),
                ((SourceBlock) block).getVariableReference().getCopy());

        outputAssign.setName(block.getName() + "Assign");

        // updating code model
        codemodel.add(outputAssign);

        return codemodel;

    }

    @Override
    public void preProcess(GASystemModel m) {
        SourceBlock block;
        Parameter paramName;
        VariableExpression varReference;
        String varName;
        for (GAModelElement el: m.getAllElements(SourceBlock.class)) {
            block = (SourceBlock) el;
            if (block.getType().equals("DataStoreRead")) {
                // variable name
                paramName = block.getParameterByName("DataStoreName");

                Assert.assertNotNull(paramName,
                        "Parameter 'DataStoreName' not defined : "
                        + block.getReferenceString());
                
                varReference = new VariableExpression();
                varName = ((StringExpression) paramName.getValue()).getLitValue();
                varReference.setName(varName);
                block.setVariableReference(varReference);
            }
        }
    }

}
