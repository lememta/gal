/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/ProductBackend.java,v $
 *  @version	$Revision: 1.26 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.blocklibrary.utils.ProductBackendUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Product block
 * 
 * 
 */
public class ProductBackend extends CombinatorialImplBackend {

    /**
     * Return computation code (functional handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        if (!"Product".equals(block.getType())) {
            EventHandler.handle(EventLevel.ERROR, "ProductBackendUtil", "",
                    "Block type error: Product block " 
            		+ block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has no outputs, expected one!\n "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has no inputs, expected one or more\n "
                        + block.getReferenceString());

        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramMul = block.getParameterByName("Multiplication");
        Parameter paramInputs = block.getParameterByName("Inputs");

        Assert.assertNotNull(paramMul,
                "Parameter 'Multiplication' not defined: "
                        + block.getReferenceString());

        Assert.assertNotNull(paramInputs, "Parameter 'Inputs' not defined: "
                + block.getReferenceString());

        // determining operators (* or /)
        List<BinaryOperator> operators = 
        	ProductBackendUtil.parseProductParameter(
        							paramInputs.getStringValue(), 
        							paramMul.getStringValue());

        Expression productTree = getProductTree(inputs, operators, block);

        AssignStatement assign = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0), productTree);
        
        // giving a name to the assign statement
        assign.setName(block.getName() + "Assign");

        codeModel.add(assign);

        return codeModel;
    }

    private Expression getProductTree(List<Expression> inputs,
            List<BinaryOperator> operators, Block block) {

        if (inputs.size() == 1) {
            return getTreeFromSingleInput(inputs.get(0), operators, block);
        } else {
            return getTreeFromMultipleInputs(inputs, operators, block);
        }
    }

    private Expression getTreeFromMultipleInputs(List<Expression> inputs,
            List<BinaryOperator> operators, Block block) {

        int size = inputs.size();
        Expression productExpr = null;
        
        for (int i = 0; i < size; i++) {
            Expression currentExpr = inputs.get(i).getCopy();
            if (i == 0) {
            	// in the first loop we only handle "/" (add 1/<arg>)
            	// "*" is left unhandled
            	if (BinaryOperator.DIV_OPERATOR.equals(operators.get(i))) {
            		productExpr = new BinaryExpression(
                    			new DoubleExpression(1.0),
                    			currentExpr,
                    			operators.get(i));
            	} else {
            		productExpr = currentExpr;
            	}
            } else {
            	productExpr = new BinaryExpression(
            			productExpr,
            			currentExpr,
            			operators.get(i));                
            }
        }
     
        if (!(productExpr instanceof BinaryExpression)){
            EventHandler.handle(EventLevel.ERROR, "ProductBackendUtil", "",
                    "Insufficient number of inputs in\n " 
            		+ block.getReferenceString(), "");        	
        }
        	
        return productExpr;
    }

    private Expression getTreeFromSingleInput(Expression expression,
            List<BinaryOperator> operators,
            Block block) {

        int dim = expression.getDataType().getDimensionality();
        Expression productExpr = null;

        Parameter paramMul = block.getParameterByName("Multiplication");
        
        if (dim > 2) {
            // not applicable to more than 2 dimentional inputs
            EventHandler.handle(EventLevel.ERROR, "ProductBackendUtil", "",
                    "Single-input product operation can't be applied "
                            + "to more than 2 dimentional inputs\n Block: "
                            + block.getReferenceString(), "");
            return null;
        } else if (dim == 2 ) {
        	if (paramMul.getStringValue().equals("Matrix(*)")){
        		productExpr = expression.getCopy();
        	}
        } else if (dim > 1) {
        	// not applicable to matrices
            EventHandler.handle(EventLevel.ERROR, "ProductBackendUtil", "",
                    "Single-input product operation can be applied " 
            		+ "to vectors only\n Block: " 
            		+ block.getReferenceString(), "");
            return null;
        } else if (dim == 0) {
        	// do nothing in case of scalar value
        	if (BinaryOperator.DIV_OPERATOR.equals(operators.get(0))) {
        		productExpr = new BinaryExpression(
            			new DoubleExpression("1.0"),
            			expression.getCopy(),
            			operators.get(0));        		
        	} else {
        		productExpr = expression.getCopy();
        	}
        } else {
        	// find product of all the vector elements
            int size = expression.getDataType().getDimensionsLengths()[0];
            
            Expression currentExpr = null;
            
            Integer[] idx = new Integer[1];
            for (idx[0] = 0; idx[0] < size; idx[0]++) {
                currentExpr = expression.getCopy();
                currentExpr.setIndexes(idx);
            	if (idx[0] == 0) {
                	if (BinaryOperator.DIV_OPERATOR.equals(operators.get(idx[0]))) {
                		productExpr = new BinaryExpression(
                        			new DoubleExpression(1.0),
                        			currentExpr,
                        			operators.get(0));
                	} else {
                		productExpr = currentExpr;
                	}
            	} else {
            		productExpr = new BinaryExpression(
            				productExpr, 
            				currentExpr, 
            				operators.get(0));
            	}
            }
            
            if (!(productExpr instanceof BinaryExpression)){
                EventHandler.handle(EventLevel.ERROR, "ProductBackendUtil", "",
                        "Insufficient number of inputs in\n " 
                		+ block.getReferenceString(), "");        	
            }

        }
        return productExpr;
    }
    
}