/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/TrigonometryBackend.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-09-20 14:17:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Trigonometry block.
 * 
 * 
 */
public class TrigonometryBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Trigonometry")) {
            EventHandler.handle(EventLevel.ERROR, "TrigonometryBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramOperator = block.getParameterByName("Operator");

        Assert.assertNotNull(paramOperator,
                "Parameter 'Operator' not defined : "
                        + block.getReferenceString());

        String function = ((StringExpression) paramOperator.getValue()).getLitValue();

        // output assign statement initialisation
        AssignStatement assign = null;

        ArrayList<Expression> args = new ArrayList<Expression>();
        args.add(inputs.get(0).getCopy());

        if (function.equals("atan2")) {
            /*
             * %o1 = atan2(%i1,%i2);
             */

            args.add(inputs.get(1).getCopy());

        }
        /*
         * else : %o1 = func(%i1);
         * 
         * NOTE: In case of non-scalar input/output ports 
         * scalar expansion will be applied in the printer.
         * Thats why the data type of a call expression 
         * is always a primitive type
         */
        CallExpression callExpr = new CallExpression(function, 
        		args,
        		block.getOutDataPorts().get(0).getDataType().getPrimitiveType());
        assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0), callExpr);

        assign.setName(block.getName() + "Assign");

        // adding function to generated code model
        codemodel.add(assign);

        return codemodel;
    }

}
