/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/MultiportSwitchBackend.java,v $
 *  @version	$Revision: 1.19 $
 *	@date		$Date: 2011-07-26 07:00:07 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.CaseStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gacodemodel.statement.WhenCase;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for MultiportSwitch block.
 * 
 * 
 */
public class MultiportSwitchBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("MultiPortSwitch")) {
            EventHandler.handle(EventLevel.ERROR, "MultiportSwitchBackend",
                    "", "Block type error : " + block.getReferenceString(), "");
        }

        // block must have one output
        Assert.assertSingleElement(outputs, "block outputs", 
        		"MultiPortSwitchBackend", block.getReferenceString(), "");

        // block must have two or more inputs
        Assert.assertNPlusElements(inputs, 2, "block inputs", 
        		"MultiPortSwitchBackend", block.getReferenceString(), "");
        
        // Read parameter "Use zero-based indexing"
        // when the value is "on", the starting index is , 
        // otherwise we assume 1 
        Parameter param = block.getParameterByName("zeroidx");
        /* number to be subtracted from first input value to get
         * port index (vector index in case of a single data input) 
         */
        int idxAdjustment = 0;
        		
        if (param != null 
        		&& "on".equals(param.getStringValue())) {
        	// adjust by -1 (start from 0)
        	idxAdjustment = 1;
        } else {
        	// no adjustment, we start from 1
        }
      

        // Initialising code model object
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // Initialising assign statement
        AssignStatement assign = null;

        CaseStatement switchcase = null;

        if (inputs.size() == 2) {
            // if two input ports, block behaves as an index selector
        	

            /*
             * %o1 = %i2[%i1];
             */
        	// compose index
        	// TODO: TF:927 It would be nice to check here if the data type 
        	// really is integer and add a typecast in case it is not 
        	// TODO: TF_928 The index is treated incorrectly in case 
        	// of zero-based indexing
        	Expression idxExpr = inputs.get(0).getCopy();
        	if (idxAdjustment != 0) {
        		idxExpr = new BinaryExpression(idxExpr, 
        				new IntegerExpression(idxAdjustment),
        	            BinaryOperator.SUB_OPERATOR);
        	}

        	// compose assignment
            assign = new AssignStatement(
            			AssignOperator.SIMPLE_ASSIGN, 
            			outputs.get(0).getCopy(),
            			BackendUtilities.varIndex(inputs.get(1), idxExpr));

            // update returned code model
            codemodel.add(assign);

        } else {

            /*
             * switch((int)%i1){ case 1 : %o1 = %i2; break; case 2 : %o1 = %i3;
             * break; ...
             */

            ArrayList<WhenCase> whenCases = new ArrayList<WhenCase>();

            for (int i = 1; i < inputs.size(); i++) {

                ArrayList<Statement> whenStatements = new ArrayList<Statement>();
                whenStatements.add(new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, outputs.get(0).getCopy(),
                        inputs.get(i).getCopy()));

                whenCases.add(new WhenCase(
                        			new IntegerExpression(i-idxAdjustment),
                        			whenStatements));
            }

            // TODO: TF:926 this will limit the number of input ports to 127
            switchcase = new CaseStatement(
            		new UnaryExpression(inputs.get(0).getCopy(), 
            				UnaryOperator.CAST_OPERATOR, 
            				new TRealInteger(8,true)), 
            		whenCases, 
            		null);

            // updating returned code model
            codemodel.add(switchcase);

        }
        return codemodel;
    }
}
