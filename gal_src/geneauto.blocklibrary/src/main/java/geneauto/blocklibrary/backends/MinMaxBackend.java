/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/MinMaxBackend.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2011-10-04 12:18:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for MinMax block.
 * 
 * 
 */
public class MinMaxBackend extends CombinatorialImplBackend {

    private List<GAModelElement> oneScalarInput(Block block,
            List<Expression> inputs, List<Expression> outputs,
            BinaryOperator comparison) {

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // Assign statement declaration
        AssignStatement assign = null;

        // input is scalar : output = input
        assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0), inputs.get(0).getCopy());

        // updating code model
        codemodel.add(assign);

        return codemodel;
    }

    private List<GAModelElement> oneVectorOutput(Block block,
            List<Expression> inputs, List<Expression> outputs,
            BinaryOperator comparison) {
        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // Assign statement declaration
        AssignStatement assign = null;

        // input is vector
        /*
         * pseudo code : %out = %in[0]; for (int index=1; index< vector_size;
         * index++){ if (%in[index] < out) out = in[index]; }
         */

        // first assignment definition
        assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0), BackendUtilities.varIndex(inputs.get(0), 0));

        // updating code model
        codemodel.add(assign);

        // index variable declaration
        Variable_CM index = new Variable_CM();

        index.setName("index" + block.getId());
        index.setScope(DefinitionScope.LOCAL);
        
        
        RangeExpression range = new RangeExpression(1, ((TArray) inputs.get(0)
                .getDataType()).getDimensionsLength(0) - 1);
        index.setDataType(range.getDataType());
        


        RangeIterationStatement forState = new RangeIterationStatement();
        forState.setRange(range);
        forState.setIteratorVariable(index);
        forState.addNameSpaceElement(index);
        
        // for statement body
        /*
         * if (in[index] < out) out = in[index];
         */
        forState.setBodyStatement(new IfStatement(new BinaryExpression(
                BackendUtilities.varIndex(inputs.get(0),
                        new VariableExpression(index)), outputs.get(0)
                        .getCopy(), comparison), new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0).getCopy(),
                BackendUtilities.varIndex(inputs.get(0),
                        new VariableExpression(index))), null));

        // updating code model
        codemodel.add(forState);

        return codemodel;
    }

    private List<GAModelElement> oneMatrixOutput(Block block,
            List<Expression> inputs, List<Expression> outputs,
            BinaryOperator comparison) {

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // Assign statement declaration
        AssignStatement assign = null;

        // input is matrix
        /*
         * pseudo code : %out = %in[0][0]; for (int index_x=1; index < size_x;
         * index_x++) for (int index_y=1; index < size_y; index_y++){ if
         * (in[index_x][index_y] < out) out = in[index_x][index_y]; }
         */

        // first assignment definition
        assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0).getCopy(), BackendUtilities.varIndex2(inputs.get(0), 0,
                0));

        // updating code model
        codemodel.add(assign);

        // index variables declaration
        Variable_CM index_x = new Variable_CM();
        Variable_CM index_y = new Variable_CM();
        
        

        index_x.setName("index_x" + block.getId());
        index_x.setScope(DefinitionScope.LOCAL);

        index_y.setName("index_y" + block.getId());
        index_y.setScope(DefinitionScope.LOCAL);

        // determining matrix size
        IntegerExpression size_x = (IntegerExpression) ((TArray) block
                .getInDataPorts().get(0).getDataType()).getDimensions().get(0);
        IntegerExpression size_y = (IntegerExpression) ((TArray) block
                .getInDataPorts().get(0).getDataType()).getDimensions().get(1);
        
        RangeExpression range_x = new RangeExpression(1, size_x.getIntValue());
        RangeExpression range_y = new RangeExpression(1, size_y.getIntValue() - 1);
        
        index_x.setDataType(range_x.getDataType());
        index_y.setDataType(range_y.getDataType());

        // loop statement declaration
        RangeIterationStatement forState_x = new RangeIterationStatement();
        
        forState_x.setRange(range_x);
        forState_x.setIteratorVariable(index_x);

        // first for statement body
        /*
         * for (int index_y=1; index_y < size_y; index_y++)
         */
        RangeIterationStatement forState_y = new RangeIterationStatement();
        
        forState_y.setRange(range_y);
        forState_y.setIteratorVariable(index_y);

        // second for statement body
        /*
         * if (in[index_x][index_y] < out) out = in[index_x][index_y];
         */
        forState_y.setBodyStatement(new IfStatement(new BinaryExpression(
                BackendUtilities.varIndex2(inputs.get(0),
                        new VariableExpression(index_x),
                        new VariableExpression(index_y)), outputs.get(0)
                        .getCopy(), comparison), new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0).getCopy(),
                BackendUtilities.varIndex2(inputs.get(0),
                        new VariableExpression(index_x),
                        new VariableExpression(index_y))), null));

        // first for statement body assignment
        forState_x.setBodyStatement(forState_y);

        // updating code model
        codemodel.add(forState_x);

        return codemodel;
    }

    private List<GAModelElement> multipleScalarInputs(Block block,
            List<Expression> outputs, List<Expression> inputs,
            BinaryOperator comparison) {

        /*
         * pseudo code (for min case) : out = in0 if(in1<out) out = in1 if(in2<out)
         * out = in2 ... if(inN<out) out = inN
         */

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // Assign statement declaration
        AssignStatement assign = null;

        // first assignment definition
        // out = in.get(0)
        assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                .get(0), inputs.get(0).getCopy());

        // updating code model
        codemodel.add(assign);

        // creating an if statement for each input
        IfStatement ifState;

        for (int index = 0; index < inputs.size(); index++) {

            // if(inI<out) out = inI (in min case)
            ifState = new IfStatement(new BinaryExpression(inputs.get(index)
                    .getCopy(), outputs.get(0).getCopy(), comparison),
                    new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                            .get(0).getCopy(), inputs.get(index).getCopy()),
                    null);

            // updating code model
            codemodel.add(ifState);
        }

        return codemodel;
    }

    private IfStatement ifStateScalar(Expression index, Expression input,
            Expression output, BinaryOperator comparison) {

        /*
         * if( input < output[index] output[index] = input;
         */

        IfStatement ifState = new IfStatement(new BinaryExpression(input,
                BackendUtilities.varIndex(output, index), comparison),
                new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(output, index), input), null);

        return ifState;
    }

    private IfStatement ifStateVector(Expression index, Expression input,
            Expression output, BinaryOperator comparison) {

        /*
         * if( input[index] < output[index] output[index] = input[index];
         */

        IfStatement ifState = new IfStatement(new BinaryExpression(
                BackendUtilities.varIndex(input, index), BackendUtilities
                        .varIndex(output, index), comparison),
                new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(output, index),
                        BackendUtilities.varIndex(input, index)), null);

        return ifState;
    }

    private IfStatement ifStateRowMatrix(Expression index, Expression input,
            Expression output, BinaryOperator comparison) {

        /*
         * if( input[index][0] < output[index] output[index] = input[index][0];
         */
        IntegerExpression intExp = new IntegerExpression(0);
        IfStatement ifState = new IfStatement(new BinaryExpression(
                BackendUtilities.varIndex2(input, index, intExp.getCopy()), BackendUtilities.varIndex(output, index),
                comparison), new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                BackendUtilities.varIndex(output, index), BackendUtilities
                        .varIndex2(input, index, intExp.getCopy())),
                null);

        return ifState;
    }

    private IfStatement ifStateColumnMatrix(Expression index, Expression input,
            Expression output, BinaryOperator comparison) {

        /*
         * if( input[0][index] < output[index] output[index] = input[0][index];
         */

        IfStatement ifState = new IfStatement(new BinaryExpression(
                BackendUtilities.varIndex2(input, new IntegerExpression(0),
                        index), BackendUtilities.varIndex(output, index),
                comparison), new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                BackendUtilities.varIndex(output, index), BackendUtilities
                        .varIndex2(input, new IntegerExpression(0), index)),
                null);

        return ifState;
    }

    private List<GAModelElement> multipleMixedInputs(Block block,
            List<Expression> outputs, List<Expression> inputs,
            BinaryOperator comparison) {

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // index variable declaration
        RangeExpression range = new RangeExpression(0, ((TArray) outputs.get(0)
                .getDataType()).getDimensionsLength(0) - 1);
        Variable_CM indexVar = new Variable_CM();

        indexVar.setName("index" + block.getId());
        indexVar.setDataType(range.getDataType());
        indexVar.setScope(DefinitionScope.LOCAL);

        VariableExpression index = new VariableExpression(indexVar);

        // determining output vector dimension
        // determining vector dimension
        RangeIterationStatement forState = new RangeIterationStatement(
                null,
                indexVar, range
                );
        forState.addNameSpaceElement(indexVar);
        CompoundStatement forBody = new CompoundStatement();

        // adding initialization statement in the for body
        if (inputs.get(0).getDataType() instanceof TPrimitive) {
            /*
             * %o1[index] = %i1;
             */
            forBody.addStatement(new AssignStatement(
                    AssignOperator.SIMPLE_ASSIGN, BackendUtilities.varIndex(
                            outputs.get(0), index), inputs.get(0).getCopy()));

        }
        if (inputs.get(0).getDataType().isVector()) {
            /*
             * %o1[index] = %i1[index];
             */
            forBody.addStatement(new AssignStatement(
                    AssignOperator.SIMPLE_ASSIGN, BackendUtilities.varIndex(
                            outputs.get(0), index), BackendUtilities.varIndex(
                            inputs.get(0), index)));
        }
        if (inputs.get(0).getDataType().isMatrix()) {

            // determining matrix dimension
            IntegerExpression heightExpr = (IntegerExpression) ((TArray) inputs
                    .get(0).getDataType()).getDimensions().get(0);
            IntegerExpression widthExpr = (IntegerExpression) ((TArray) inputs
                    .get(0).getDataType()).getDimensions().get(1);

            int height = Integer.valueOf(heightExpr.getLitValue());
            int width = Integer.valueOf(widthExpr.getLitValue());

            // single column matrix
            if (height == 1) {
                /*
                 * %o1[index] = %i1[0][index];
                 */
                forBody.addStatement(new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, BackendUtilities.varIndex(
                                outputs.get(0), index), BackendUtilities
                                .varIndex2(inputs.get(0),
                                        new IntegerExpression(0), index)));
            }
            // single row matrix
            else if (width == 1) {
                /*
                 * %o1[index] = %i1[index][0];
                 */
                forBody.addStatement(new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, BackendUtilities.varIndex(
                                outputs.get(0), index), BackendUtilities
                                .varIndex2(inputs.get(0), index,
                                        new IntegerExpression(0))));
            }
        }

        // adding an if statement for each block input
        IfStatement ifState = null;

        for (int i = 0; i < inputs.size(); i++) {

            if (inputs.get(i).getDataType() instanceof TPrimitive) {
                ifState = ifStateScalar(index, inputs.get(i).getCopy(), outputs
                        .get(0), comparison);
            }
            if (inputs.get(i).getDataType().isVector()) {
                ifState = ifStateVector(index, inputs.get(i).getCopy(), outputs
                        .get(0), comparison);
            }
            if (inputs.get(i).getDataType().isMatrix()) {

                // determining matrix dimension
                IntegerExpression heightExpr = (IntegerExpression) ((TArray) inputs
                        .get(i).getDataType()).getDimensions().get(0);
                IntegerExpression widthExpr = (IntegerExpression) ((TArray) inputs
                        .get(i).getDataType()).getDimensions().get(1);

                int height = Integer.valueOf(heightExpr.getLitValue());
                int width = Integer.valueOf(widthExpr.getLitValue());

                if (height == 1) {

                    ifState = ifStateColumnMatrix(index, inputs.get(i)
                            .getCopy(), outputs.get(0).getCopy(), comparison);

                } else if (width == 1) {

                    ifState = ifStateRowMatrix(index, inputs.get(i).getCopy(),
                            outputs.get(0).getCopy(), comparison);

                }
            }

            forBody.addStatement(ifState);
        }

        forState.setBodyStatement(forBody);

        codemodel.add(forState);

        return codemodel;
    }

    private List<GAModelElement> minmaxComputeCode(Block block,
            List<Expression> outputs, List<Expression> inputs,
            BinaryOperator comparison) {

        if (inputs.size() == 1) {
            // block has a single input
            if (block.getInDataPorts().get(0).getDataType() instanceof TPrimitive) {

                // input is scalar : output = input
                return oneScalarInput(block, inputs, outputs, comparison);

            } else if (block.getInDataPorts().get(0).getDataType().isVector()) {
                // input is vector
                /*
                 * pseudo code (for min case): %out = %in[0]; for (int index=1;
                 * index< vector_size; index++){ if (%in[index] < out) out =
                 * in[index]; }
                 */
                return oneVectorOutput(block, inputs, outputs, comparison);

            } else if (block.getInDataPorts().get(0).getDataType() instanceof TArray) {

                // input is matrix
                /*
                 * pseudo code (for min case): %out = %in[0][0]; for (int
                 * index_x=1; index < size_x; index_x++) for (int index_y=1;
                 * index < size_y; index_y++){ if (in[index_x][index_y] < out)
                 * out = in[index_x][index_y]; }
                 */
                return oneMatrixOutput(block, inputs, outputs, comparison);
            }

        } else {
            // block has multiple inputs
            // determining inputs signal type (scalar, vector or matrix)
            // determining input signal type
            int nbScalar = 0, nbVector = 0, nbMatrix = 0;

            for (int i = 0; i < inputs.size(); i++) {
                if (inputs.get(i).getDataType() instanceof TPrimitive) {
                    nbScalar++;
                }
                else if (inputs.get(i).getDataType().isVector()) {
                    nbVector++;
                }
                else if (inputs.get(i).getDataType() instanceof TArray) {
                    nbMatrix++;
                }
            }

            if (nbScalar == inputs.size()) {
                // inputs are all scalar : output is scalar
                return multipleScalarInputs(block, outputs, inputs, comparison);

            } else {
                // inputs are mix between scalar, vector and single-row (or
                // single_column) matrix : output is vector
                return multipleMixedInputs(block, outputs, inputs, comparison);

            }

        }

        return null;
    }

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> outputs, List<Expression> inputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("MinMax")) {
            EventHandler.handle(EventLevel.ERROR, "MinMaxBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // reading block parameter
        Parameter paramFunction = block.getParameterByName("Function");

        Assert.assertNotNull(paramFunction, "Parameter Function not defined : "
                + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = null;

        String function = ((StringExpression) paramFunction.getValue()).getLitValue();

        if (function.equals("min")) {

            // operator < for min function
            codemodel = minmaxComputeCode(block, inputs, outputs,
                    BinaryOperator.LT_OPERATOR);

        } else if (function.equals("max")) {

            // operator > for max function
            codemodel = minmaxComputeCode(block, inputs, outputs,
                    BinaryOperator.GT_OPERATOR);

        } else {
            EventHandler.handle(EventLevel.ERROR, "MinMaxBackend", "",
                    "Parameter Function hasn't an expected value (must be 'min' or 'max') : "
                            + block.getReferenceString(), "");
        }

        return codemodel;

    }

}
