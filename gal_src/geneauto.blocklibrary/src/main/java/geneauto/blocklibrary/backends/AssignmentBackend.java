/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/AssignmentBackend.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Assignment block
 * 
 * 
 */
public class AssignmentBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Assignment")) {
            EventHandler.handle(EventLevel.ERROR, this.getClass()
                    .getCanonicalName(), "", "Block type error : "
                    + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // determining data type
        GADataType type = block.getOutDataPorts().get(0).getDataType();

        AssignStatement copy = null;
        AssignStatement assign = null;

        if (type instanceof TPrimitive) {

            /*
             * %o1 = %i2;
             */

            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), inputs.get(1).getCopy());

        } else {
            if (type.isVector()) {

                Expression elements = null;

                if (inputs.size() == 3) {

                    /*
                     * %o1 = %i1; %o1[%i3] = %i2;
                     */

                    elements = inputs.get(2).getCopy();

                } else {

                    /*
                     * %o1 = %i1; %o1[param] = %i2;
                     */

                    // reading block parameter
                    Parameter paramElements = block
                            .getParameterByName("Elements");

                    Assert.assertNotNull(paramElements,
                            "Parameter 'Elements' not defined : "
                                    + block.getReferenceString());

                    // getting param value
                    elements = paramElements.getCodeModelExpression();
                }

                // copying second input in the good location of the output
                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0), elements),
                        inputs.get(1));
            } else if (type.isMatrix()) {

                String rowSrc;
                String columnSrc;
                Expression rows = null;
                Expression columns = null;

                if (inputs.size() == 4) {

                    /*
                     * %o1 = %i1; %o1[%i3][%i4] = %i2;
                     */

                    rows = inputs.get(2).getCopy();
                    columns = inputs.get(3).getCopy();

                } else {

                    // reading block parameter
                    Parameter paramRowSrc = block.getParameterByName("RowSrc");
                    Parameter paramColumnSrc = block
                            .getParameterByName("ColumnSrc");
                    Parameter paramRows = block.getParameterByName("Rows");
                    Parameter paramColumns = block
                            .getParameterByName("Columns");

                    Assert.assertNotNull(paramRowSrc,
                            "Parameter 'RowSrc' not defined : "
                                    + block.getReferenceString());

                    Assert.assertNotNull(paramColumnSrc,
                            "Parameter 'ColumnSrc' not defined : "
                                    + block.getReferenceString());

                    if (inputs.size() == 2) {
                        rows = paramRows.getCodeModelExpression();
                        columns = paramColumns.getCodeModelExpression();
                    } else {

                        rowSrc = ((StringExpression) paramRowSrc.getValue()).getLitValue();
                        columnSrc = ((StringExpression) paramColumnSrc.getValue()).getLitValue();

                        if (rowSrc.equals("Internal")) {

                            Assert.assertNotNull(paramRows,
                                    "Parameter 'Rows' not defined :"
                                            + block.getReferenceString());

                            if (columnSrc.equals("External")) {
                                // row internal, column external
                                rows = paramRows.getCodeModelExpression();
                                columns = inputs.get(2).getCopy();
                            } else {
                                // error
                                EventHandler
                                        .handle(
                                                EventLevel.ERROR,
                                                "AssignmentBackend",
                                                "",
                                                "Value of parameters 'RowSrc' and 'ColumnSrc' doesn't match with block input number : "
                                                        + block
                                                                .getReferenceString(),
                                                "");
                            }
                        } else if (columnSrc.equals("Internal")) {

                            Assert.assertNotNull(paramRows,
                                    "Parameter 'Columns' not defined : "
                                            + block.getReferenceString());

                            if (rowSrc.equals("External")) {
                                // row external, column internal
                                columns = paramColumns.getCodeModelExpression();
                                rows = inputs.get(2).getCopy();
                            } else {
                                // error
                                EventHandler
                                        .handle(
                                                EventLevel.ERROR,
                                                "AssignmentBackend",
                                                "",
                                                "Value of parameters 'RowSrc' and 'ColumnSrc' doesn't match with block input number : "
                                                        + block
                                                                .getReferenceString(),
                                                "");
                            }

                        } else {
                            // error
                            EventHandler
                                    .handle(
                                            EventLevel.ERROR,
                                            "AssignmentBackend",
                                            "",
                                            "Value of parameters 'RowSrc' and 'ColumnSrc' doesn't match with block input number : "
                                                    + block
                                                            .getReferenceString(),
                                            "");
                        }
                    }
                }

                // copying second input in the good location of the output
                assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex2(outputs.get(0), columns,
                                rows), inputs.get(1).getCopy());
            }

            // copying first input in the output
            copy = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0).getCopy(), inputs.get(0).getCopy());

            copy.setName(block.getName() + "Copy");

            // adding assignment to generated code model
            codemodel.add(copy);

        }

        assign.setName(block.getName() + "Assign");

        // adding assignment to generated code model
        codemodel.add(assign);

        return codemodel;
    }
}
