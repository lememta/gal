/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/RelationalOperatorBackend.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2012-03-08 10:58:14 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for RelationalOperator block.
 * 
 * 
 */
public class RelationalOperatorBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("RelationalOperator")) {
            EventHandler.handle(EventLevel.ERROR, "RelationalOperatorBackend",
                    "", "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // determining inputs data type
        GADataType type1 = block.getInDataPorts().get(0).getDataType();
        GADataType type2 = block.getInDataPorts().get(1).getDataType();

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramOperator = block.getParameterByName("Operator");

        Assert.assertNotNull(paramOperator,
                "Parameter 'Operator' not defined : "
                        + block.getReferenceString());

        // parsing block parameter
        BinaryOperator operator = BinaryOperator.fromEML(
                ((StringExpression) paramOperator.getValue()).getLitValue(), false, false);

        AssignStatement assign = null;

		// We first check if the types are in a subtype relation. This also
		// covers equality. 
		// TODO Check, if all the other cases below are actually covered by this
		// first condition and any required expansions could be carried out by the 
        // printer. AnTo 08.03.2012
        if (type1.isSubType(type2) || type2.isSubType(type1) ) {

            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
                    .get(0), new BinaryExpression(inputs.get(0).getCopy(),
                    inputs.get(1).getCopy(), operator));

            // giving a name to the assign statement
            assign.setName(block.getName() + "Assign");

            codemodel.add(assign);
        } else {
            if (type1 instanceof TPrimitive) {
                if (type2.isVector()) {
                    // first input is scalar, second one is vector

                    /*
                     * %o1[0] = %i1 comp_op %i2[0] %o1[1] = %i1 comp_op %i2[1]
                     * ... %o1[vector_size-1] = %i1 comp_op %i2[vector_size-1] }
                     */

                    // determining vector dimension
                    int vectorDimension = Integer
                            .valueOf(((IntegerExpression) ((TArray) outputs
                                    .get(0).getDataType()).getDimensions().get(
                                    0)).getLitValue());

                    for (int i = 0; i < vectorDimension; i++) {

                        assign = new AssignStatement(
                                AssignOperator.SIMPLE_ASSIGN, BackendUtilities
                                        .varIndex(outputs.get(0), i),
                                new BinaryExpression(inputs.get(0).getCopy(),
                                        BackendUtilities.varIndex(
                                                inputs.get(1), i), operator));

                        // giving a name to the assign statement
                        assign.setName(block.getName() + "Assign_" + i);

                        codemodel.add(assign);
                    }
                } else if (type2.isMatrix()) {
                    // first input is scalar, second one is matrix

                    /*
                     * %o1[0][0] = %i1 comp_op %i2[0][0]; %o1[0][1] = %i1
                     * comp_op %i2[0][1]; ... %o1[size_x][size_y] = %i1 comp_op
                     * %i2[size_x][size_y];
                     */

                    // determining matrix dimensions
                    IntegerExpression heightExpr = (IntegerExpression) ((TArray) outputs
                            .get(0).getDataType()).getDimensions().get(0);
                    IntegerExpression widthExpr = (IntegerExpression) ((TArray) outputs
                            .get(0).getDataType()).getDimensions().get(1);

                    int height = Integer.valueOf(heightExpr.getLitValue());
                    int width = Integer.valueOf(widthExpr.getLitValue());

                    for (int i = 0; i < height; i++) {
                        for (int j = 0; j < width; j++) {

                            assign = new AssignStatement(
                                    AssignOperator.SIMPLE_ASSIGN,
                                    BackendUtilities.varIndex2(outputs.get(0),
                                            i, j), new BinaryExpression(inputs
                                            .get(0).getCopy(), BackendUtilities
                                            .varIndex2(inputs.get(1), i, j),
                                            operator));

                            // giving a name to the assign statement
                            assign.setName(block.getName() + "Assign_" + i
                                    + "_" + j);

                            codemodel.add(assign);
                        }
                    }
                } else {
                    EventHandler.handle(EventLevel.ERROR,
                            "RelationalOperatorBackend", "",
                            "Type error for second input : "
                                    + block.getReferenceString(), "");
                }
            } else if (type1.isVector()) {

                // determining vector dimension
                int vectorDimension = Integer
                        .valueOf(((IntegerExpression) ((TArray) outputs.get(0)
                                .getDataType()).getDimensions().get(0))
                                .getLitValue());

                if (type2 instanceof TPrimitive) {
                    // first input is vector, second one is scalar

                    /*
                     * %o1[0] = %i1[0] comp_op %i2; %o1[1] = %i1[1] comp_op %i2;
                     * ... %o1[vector_size-1] = %i1[vector_size-1] comp_op %i2;
                     */

                    for (int i = 0; i < vectorDimension; i++) {

                        assign = new AssignStatement(
                                AssignOperator.SIMPLE_ASSIGN, BackendUtilities
                                        .varIndex(outputs.get(0), i),
                                new BinaryExpression(BackendUtilities.varIndex(
                                        inputs.get(0), i), inputs.get(1)
                                        .getCopy(), operator));

                        // giving a name to the assign statement
                        assign.setName(block.getName() + "Assign_" + i);

                        codemodel.add(assign);
                    }
                } else {
                    EventHandler.handle(EventLevel.ERROR,
                            "RelationalOperatorBackend", "",
                            "Types doesn't match : first input : " + type1
                                    + ",second input : " + type2 + " "
                                    + block.getReferenceString(), "");
                }
            } else if (type1.isMatrix()) {

                // determining matrix dimensions
                IntegerExpression heightExpr = (IntegerExpression) ((TArray) outputs
                        .get(0).getDataType()).getDimensions().get(0);
                IntegerExpression widthExpr = (IntegerExpression) ((TArray) outputs
                        .get(0).getDataType()).getDimensions().get(1);

                int height = Integer.valueOf(heightExpr.getLitValue());
                int width = Integer.valueOf(widthExpr.getLitValue());

                if (type2 instanceof TPrimitive) {

                    // first input is matrix, second one is scalar

                    /*
                     * %o1[0][0] = %i1[0][0] comp_op %i2; %o1[0][1] = %i1[0][1]
                     * comp_op %i2; ... %o1[size_x][size_y] =
                     * %i1[size_x][size_y] comp_op %i2;
                     */

                    for (int i = 0; i < height; i++) {
                        for (int j = 0; j < width; j++) {

                            assign = new AssignStatement(
                                    AssignOperator.SIMPLE_ASSIGN,
                                    BackendUtilities.varIndex2(outputs.get(0),
                                            i, j), new BinaryExpression(
                                            BackendUtilities.varIndex2(inputs
                                                    .get(0).getCopy(), i, j),
                                            inputs.get(1).getCopy(), operator));

                            // giving a name to the assign statement
                            assign.setName(block.getName() + "Assign_" + i
                                    + "_" + j);

                            codemodel.add(assign);
                        }
                    }
                } else {
                    EventHandler.handle(EventLevel.ERROR,
                            "RelationalOperatorBackend", "",
                            "Types doesn't match : first input : " + type1
                                    + ",second input : " + type2 + " "
                                    + block.getReferenceString(), "");
                }
            } else {
                EventHandler.handle(EventLevel.ERROR,
                        "RelationalOperatorBackend", "",
                        "Type error for first input : " + type2 + " "
                                + block.getReferenceString(), "");
            }
        }

        return codemodel;
    }

}
