/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/ToWorkspaceBackend.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for ToWorkspace block.
 * 
 * 
 */
public class ToWorkspaceBackend extends SinkImplBackend {

    @Override
    public List<GAModelElement> getDeclarationCode(Block block) {

        // checking block validity
        if (!block.getType().equals("ToWorkspace")) {
            EventHandler.handle(EventLevel.ERROR, "ToWorkspaceBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // Model declaration
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramName = block.getParameterByName("VariableName");

        Assert.assertNotNull(paramName,
                "Parameter 'VariableName' not defined : "
                        + block.getReferenceString());

        // determine variable type
        GADataType type = block.getInDataPorts().get(0).getDataType();

        // variable declaration
        Variable_CM data = new Variable_CM();

        // set variable name
        String name = ((StringExpression) paramName.getValue()).getLitValue();

        // replace all unavailable characters (/,\...) and set variable name
        data.setName(BackendUtilities.cleanStringName(name));

        data.setName(name);
        data.setDataType(type);
        data.setScope(DefinitionScope.EXPORTED);

        codemodel.add(data);

        return codemodel;
    }

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> controls,
            Expression memory) {

        // checking block validity
        if (!block.getType().equals("ToWorkspace")) {
            EventHandler.handle(EventLevel.ERROR, "ToWorkspaceBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // initializing code model
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();
        AssignStatement assign = null;

        // check type validity
        if (memory.getDataType().getTypeName().equals(
                inputs.get(0).getDataType().getTypeName())) {

            // assign statement : memory = in
            assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, memory,
                    inputs.get(0).getCopy());

            // giving a name to the assign statement
            assign.setName(block.getName() + "Assign");

            // annotating generated code model to indicate source block of the
            // code model part
            assign.setAnnotations(BackendUtilities.getAnnotation(block));

            // updating code model
            codemodel.add(assign);
        }

        return codemodel;
    }
}
