/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/ForIteratorBackend.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-07-21 14:35:02 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.ForStatement;
import geneauto.models.gacodemodel.statement.IncStatement;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for ForIterator block.
 * 
 * 
 */
public class ForIteratorBackend extends SubSystemImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls, Expression memory) {

        // checking block validity
        if (!block.getType().equals("ForIterator")) {
            EventHandler.handle(EventLevel.ERROR, "ForIteratorBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // creating code model
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // index variable initialization
        Variable_CM indexVar = new Variable_CM("index" + block.getId(),
        		null, false, false, false, 
                DefinitionScope.LOCAL, new TRealInteger(8, true));

        VariableExpression index = new VariableExpression(indexVar);

        // assign statement index = <base>
        Parameter paramIndexMode = block.getParameterByName("IndexMode");
        String indexMode = "";
        if (paramIndexMode != null) {
            indexMode = ((StringExpression) paramIndexMode.getValue()).getLitValue();
        }

        AssignStatement assignIndex = null;
        boolean zeroBased;
        if (indexMode.equals("One-based")) {
            assignIndex = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                    index.getCopy(), new IntegerExpression(1));
            zeroBased = false;
        } else if (indexMode.equals("Zero-based")) {
            assignIndex = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                    index.getCopy(), new IntegerExpression(0));
            zeroBased = true;
        } else {
        	EventHandler.handle(EventLevel.ERROR, getClass().getSimpleName(), "", "Unsupported index mode value: " + indexMode);
        	return null;
        }

        assignIndex.setName("initIndex");

        IncStatement unaryIndex = new IncStatement(index.getCopy());
        // initializing for statement
        ForStatement forStmt = new ForStatement(null, assignIndex, null, null);

        /*
         * Reading block parameters
         */
        Parameter paramIterSource = block.getParameterByName("IterationSource");
        Parameter paramExternalIncrement = block
                .getParameterByName("ExternalIncrement");

        Assert.assertNotNull(paramIterSource,
                "Parameter 'IterationSource' not defined : "
                        + block.getReferenceString());

        String iterSource = ((StringExpression) paramIterSource.getValue()).getLitValue();

        String externalIncrement = "";
        if (paramExternalIncrement != null) {
            externalIncrement = ((StringExpression) paramExternalIncrement.getValue()).getLitValue();
        }

        /* setting for statement body */
        CompoundStatement body = new CompoundStatement();

        forStmt.setBodyStatement(body);

        /* compare operator */
        BinaryOperator compOper = zeroBased ? BinaryOperator.LT_OPERATOR : BinaryOperator.LE_OPERATOR;
        
        /* increment input handling */
        if (externalIncrement.equals("on")) {
            /*
             * if iteration source is external, external increment input is
             * second input, otherwise it's first input
             */
            if (iterSource.equals("external")) {
                body.addStatement(new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, index.getCopy(), inputs
                                .get(1).getCopy()));
            } else {
                body.addStatement(new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, index.getCopy(), inputs
                                .get(0).getCopy()));
            }
        } else {
            forStmt.setPostStatement(unaryIndex);
        }

        /* iteration source input handling */
        if (iterSource.equals("internal")) {
            // for condition is given by block parameter

            // reading block parameter
            Parameter paramNumIters = block.getParameterByName("NumIters");
            if(paramNumIters == null){
                paramNumIters = block.getParameterByName("IterationLimit");
            }
            
            Assert.assertNotNull(paramNumIters, "Parameter 'NumIters' not defined : "
                    + block.getReferenceString());

            // parsing parameter value
            Expression paramValue = paramNumIters.getCodeModelExpression();

            /*
             * int index; for(index=<base> ; index <compare> param ; index++){ }
             */

            forStmt.setConditionExpression(new BinaryExpression(index
                    .getCopy(), paramValue, compOper));
        } else if (iterSource.equals("external")) {

            // for condition is given by block data input

            /*
             * int index; for(index=<base> ; index <compare> %i1 ; index++){ }
             */

			forStmt.setConditionExpression(new BinaryExpression(index
                    .getCopy(), inputs.get(0).getCopy(), compOper 
                    ));
        } else {
            EventHandler.handle(EventLevel.ERROR, "ForIteratorBackend", "",
                    "Parameter 'IterationSource' has not expected value : "
                            + block.getReferenceString(), "");
        }

        /*
         * setting block output
         */

        if ((outputs != null) && (outputs.size() > 0)) {
            AssignStatement bodyAssign = new AssignStatement(
                    AssignOperator.SIMPLE_ASSIGN, outputs.get(0), index
                            .getCopy());

            bodyAssign.setName("bodyAssign");

            body.addStatement(bodyAssign);
        }

        // giving a name to the if statement
        forStmt.setName(block.getName() + "ForStatement");

        codemodel.add(indexVar);
        codemodel.add(forStmt);

        return codemodel;
    }
}
