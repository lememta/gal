/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/MuxBackend.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-09-13 12:39:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.models.utilities.ExpressionUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Mux block
 * 
 * 
 */
public class MuxBackend extends CombinatorialImplBackend {

    /**
     * Return computation code (functionnal handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        if (!block.getType().equals("Mux")) {
            EventHandler.handle(EventLevel.ERROR, "MuxBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // if output is scalar (1 single scalar input)
        if (block.getOutDataPorts().get(0).getDataType() instanceof TPrimitive) {
            if (inputs.size() != 1) {
                EventHandler.handle(EventLevel.ERROR, "MuxBackend", "",
                        "Block input dimension mismatch, single input needed when output is scalar : "
                                + block.getReferenceString(), "");
            } else {
                // assign statement : output = input
                AssignStatement assign = new AssignStatement(
                        AssignOperator.SIMPLE_ASSIGN, outputs.get(0), inputs
                                .get(0));

                // giving a name to the if statement
                assign.setName(block.getName() + "IfStatement");

                codeModel.add(assign);
            }
        }
        // if output is vector
        else if (block.getOutDataPorts().get(0).getDataType().isVector()) {

            // determining vector dimension
            int outputDimension = Integer
                    .valueOf(((IntegerExpression) ((TArray) block
                            .getOutDataPorts().get(0).getDataType())
                            .getDimensions().get(0)).getLitValue());

            // index variable to handle each input
            int currentInput = 0;
            int currentInputDimension = 0;

            int i = 0;

            while (i < outputDimension) {

                // if current input is a scalar
                if (block.getInDataPorts().get(currentInput).getDataType() instanceof TPrimitive) {

                    // output[i] = input
                    AssignStatement assign = new AssignStatement(
                            AssignOperator.SIMPLE_ASSIGN, BackendUtilities
                                    .varIndex(outputs.get(0), i), inputs
                                    .get(currentInput));

                    // giving a name to the assign statement
                    assign.setName(block.getName() + "Assign_" + i);

                    codeModel.add(assign);

                    i++;
                    currentInput++;

                }
                // if current input is a vector
                else if (block.getInDataPorts().get(currentInput).getDataType().isVector()) {

                    // determining current input dimension
                    currentInputDimension = Integer
                            .valueOf(((LiteralExpression) ((TArray) block
                                    .getInDataPorts().get(currentInput)
                                    .getDataType()).getDimensions().get(0))
                                    .getLitValue());

                    
                    AssignStatement assign = new AssignStatement(
                    		AssignOperator.SIMPLE_ASSIGN, 
                    		ExpressionUtilities.getSubArrayExpression(outputs.get(0), 
                    				i, i + currentInputDimension - 1),
                    		ExpressionUtilities.getSubArrayExpression(inputs.get(currentInput), 
                            		0, currentInputDimension - 1));
                    
                    // giving a name to the assign statement
                    assign.setName(block.getName() + "Assign_" + i);

                    codeModel.add(assign);
                    i += currentInputDimension;
                    currentInput++;

                } else {
                    EventHandler.handle(EventLevel.ERROR, "MuxBackend", "",
                            "Inputs must be scalar or vector : "
                                    + block.getReferenceString(), "");
                }
            }
        } else {
            EventHandler.handle(EventLevel.ERROR, "MuxBackend", "",
                    "Output must be scalar or vector : "
                            + block.getReferenceString(), "");
        }

        return codeModel;
    }

}
