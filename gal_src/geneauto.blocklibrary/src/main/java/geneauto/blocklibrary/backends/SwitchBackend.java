/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/SwitchBackend.java,v $
 *  @version	$Revision: 1.28 $
 *	@date		$Date: 2011-10-24 08:56:30 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Switch block
 * 
 * 
 */
public class SwitchBackend extends CombinatorialImplBackend {

    /**
     * Return computation code (functionnal handling done by the block)
     * 
     * @param block
     *            block for which is called the backend
     * @param inputs
     *            list of the input variables of the block
     * @param outputs
     *            list of the output variables of the block
     * @param controls
     *            list of the input control variables of the block
     * @param memory
     *            memory variable of the block
     * @return GACodeModel fragment describing block behaviour
     */
    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("Switch")) {
            EventHandler.handle(EventLevel.ERROR, "SwitchBackend", "",
                    "Block type error: " + block.getReferenceString(), "");
        }

        // The block must have three inputs
        Assert.assertNElements(inputs,
        		3,
        		"block inputs", 
        		"SwitchBackend", 
        		block.getReferenceString(), 
        		"");
        
        // The block must have one output
        Assert.assertSingleElement(outputs, 
        		"block outputs", 
        		"SwitchBackend", 
        		block.getReferenceString(), 
        		"");
        
        // reading block parameter
        Parameter paramThreshold = block.getParameterByName("Threshold");
        Parameter paramCriteria = block.getParameterByName("Criteria");

        Assert.assertNotNull(paramThreshold, 
        		"parameter \"Threshold\" not defined", 
        		"SwitchBackend", 
        		block.getReferenceString(), 
        		"");
        
        Assert.assertNotNull(paramCriteria, 
        		"parameter \"Criteria\" not defined", 
        		"SwitchBackend", 
        		block.getReferenceString(), 
        		"");
        
        String criteriaStr = paramCriteria.getStringValue(); 
        Expression threshold = paramThreshold.getCodeModelExpression();
        
        // determine the operator to be used in condition expression
        BinaryOperator op = null;
        if (criteriaStr.equals("u2 >= Threshold")) {
        	op = BinaryOperator.GE_OPERATOR;
        } else if (criteriaStr.equals("u2 > Threshold")) {
        	op = BinaryOperator.GT_OPERATOR;        	
        } else if (criteriaStr.equals("u2 ~= 0")) {
        	op = BinaryOperator.NE_OPERATOR; 
        	// we ignore the user-defined threshold
        	threshold = new IntegerExpression(0);
        } else {
        	EventHandler.handle(EventLevel.CRITICAL_ERROR, 
        			"SwitchBackend", 
        			"", 
        			"Uncknown criteria \"" + criteriaStr 
        			+ " \n block: "
        			+ block.getReferenceString());
        }

        GADataType inType1 = inputs.get(0).getDataType();
        GADataType inType2 = inputs.get(1).getDataType();
        GADataType inType3 = inputs.get(2).getDataType();

        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();
        Statement iFor = null;
        BinaryExpression cond = null;
        AssignStatement assignThen = null;
        AssignStatement assignElse = null;
        iFor = new RangeIterationStatement();
        
        if (inType2.isVector()) {
            /*
             * if type of control input is vector, there's a for loops
             */
            Variable_CM indexVar = new Variable_CM("i" + block.getId());
            RangeExpression range = new RangeExpression(0, ((TArray) 
                    inType2).getDimensionsLength(0) - 1);
            indexVar.setDataType(range.getDataType());

            VariableExpression index = new VariableExpression(indexVar);
            
            ((RangeIterationStatement) iFor).setRange(range);
            ((RangeIterationStatement) iFor).setIteratorVariable(indexVar);
            
            cond = new BinaryExpression(BackendUtilities.varIndex(
            			inputs.get(1).getCopy(),index.getCopy()), 
            			threshold.getCopy(), 
            			op);

            if (inType1.isVector()) {
                assignThen = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0).getCopy(),
                                index.getCopy()), BackendUtilities.varIndex(
                                inputs.get(0).getCopy(), index.getCopy()));
                assignThen.setName("then_statement_" + block.getName());

            } else if (inType1 instanceof TPrimitive) {
                assignThen = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0).getCopy(),
                                index.getCopy()), inputs.get(0).getCopy());
                assignThen.setName("then_statement_" + block.getName());
            } else {
                EventHandler
                        .handle(
                                EventLevel.CRITICAL_ERROR,
                                this.getClass().getCanonicalName(),
                                "",
                                "Dimension of first input of block "
                                        + block.getReferenceString()
                                        + " not compliant with dimension of second input.");
            }

            if (inType3.isVector()) {
                assignElse = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0).getCopy(),
                                index.getCopy()), BackendUtilities.varIndex(
                                inputs.get(2).getCopy(), index.getCopy()));
                assignElse.setName("else_statement_" + block.getName());

            } else if (inType3 instanceof TPrimitive) {
                assignElse = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0).getCopy(),
                                index.getCopy()), inputs.get(2).getCopy());
                assignElse.setName("else_statement_" + block.getName());
            } else {
                EventHandler
                        .handle(
                                EventLevel.CRITICAL_ERROR,
                                this.getClass().getCanonicalName(),
                                "",
                                "Dimension of third input of block "
                                        + block.getReferenceString()
                                        + " not compliant with dimension of second input.");
            }

            IfStatement ifstate = new IfStatement();

            ifstate.setConditionExpression(cond);
            ifstate.setThenStatement(assignThen);
            ifstate.setElseStatement(assignElse);

            ((RangeIterationStatement) iFor).setBodyStatement(ifstate);

        } else if (inType2.isMatrix()) {
            /*
             * if type of control input is matrix, there's two for loops
             */

            // creating index variables
            Variable_CM indexVar1 = new Variable_CM("i" + block.getId());
            RangeExpression range1 = new RangeExpression(0, ((TArray) 
                    inType2).getDimensionsLength(0) - 1);
            indexVar1.setDataType(range1.getDataType());
            codemodel.add(indexVar1);

            Variable_CM indexVar2 = new Variable_CM("j" + block.getId());
            RangeExpression range2 = new RangeExpression(0, ((TArray) 
                    inType2).getDimensionsLength(1) - 1);
            indexVar2.setDataType(range2.getDataType());
            codemodel.add(indexVar2);

            VariableExpression index1 = new VariableExpression(indexVar1);
            VariableExpression index2 = new VariableExpression(indexVar2);

            // creating first for loop
            ((RangeIterationStatement) iFor).setRange(range1);
            ((RangeIterationStatement) iFor).setIteratorVariable(indexVar1);

            // Creating second for loop
            RangeIterationStatement jFor = new RangeIterationStatement();
            ((RangeIterationStatement) iFor).setBodyStatement(jFor);
            ((RangeIterationStatement) jFor).setRange(range2);
            ((RangeIterationStatement) jFor).setIteratorVariable(indexVar2);

            // setting for loop body
            cond = new BinaryExpression(BackendUtilities.varIndex2(inputs
                    .get(1).getCopy(), index1.getCopy(), index2.getCopy()),
                    threshold.getCopy(), op);

            if (inType1.isMatrix()) {
                assignThen = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex2(outputs.get(0).getCopy(),
                                index1.getCopy(), index2.getCopy()),
                        BackendUtilities.varIndex2(inputs.get(0).getCopy(),
                                index1.getCopy(), index2.getCopy()));
                assignThen.setName("then_statement_" + block.getName());

            } else if (inType1 instanceof TPrimitive) {
                assignThen = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex2(outputs.get(0).getCopy(),
                                index1.getCopy(), index2.getCopy()), inputs
                                .get(0).getCopy());
                assignThen.setName("then_statement_" + block.getName());
            } else {
                EventHandler
                        .handle(
                                EventLevel.CRITICAL_ERROR,
                                this.getClass().getCanonicalName(),
                                "",
                                "Dimension of first input of block "
                                        + block.getReferenceString()
                                        + " not compliant with dimension of second input.");
            }

            if (inType3.isMatrix()) {
                assignElse = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex2(outputs.get(0).getCopy(),
                                index1.getCopy(), index2.getCopy()),
                        BackendUtilities.varIndex2(inputs.get(2).getCopy(),
                                index1.getCopy(), index2.getCopy()));
                assignElse.setName("else_statement_" + block.getName());

            } else if (inType3 instanceof TPrimitive) {
                assignElse = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex2(outputs.get(0).getCopy(),
                                index1.getCopy(), index2.getCopy()), inputs
                                .get(2).getCopy());
                assignElse.setName("else_statement_" + block.getName());
            } else {
                EventHandler
                        .handle(
                                EventLevel.CRITICAL_ERROR,
                                this.getClass().getCanonicalName(),
                                "",
                                "Dimension of third input of block "
                                        + block.getReferenceString()
                                        + " not compliant with dimension of second input.");
            }

            IfStatement ifstate = new IfStatement();

            ifstate.setConditionExpression(cond);
            ifstate.setThenStatement(assignThen);
            ifstate.setElseStatement(assignElse);

            jFor.setBodyStatement(ifstate);

        }

        else {

            // conditional expression : 
            cond = new BinaryExpression(inputs.get(1).getCopy(), 
            		threshold.getCopy(), 
            		op);

            // assign statements
            // out = in1
            assignThen = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                    outputs.get(0), inputs.get(0).getCopy());
            assignThen.setName("then_statement_" + block.getName());

            // out = in3
            assignElse = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                    outputs.get(0), inputs.get(2).getCopy());
            assignElse.setName("else_statement_" + block.getName());

            // "if" instance
            iFor = new IfStatement(cond, assignThen, assignElse);

        }
        // giving a name to the if statement
        iFor.setName(block.getName() + "SwitchStatement");

        // updating code model
        codemodel.add(iFor);

        return codemodel;
    }
}
