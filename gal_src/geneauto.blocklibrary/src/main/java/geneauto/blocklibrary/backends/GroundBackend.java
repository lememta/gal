/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/GroundBackend.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for Ground block.
 * 
 * 
 */
public class GroundBackend extends SourceImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> outputs) {

        // checking block validity
        if (!block.getType().equals("Ground")) {
            EventHandler.handle(EventLevel.ERROR, "GroundBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        // determining data type
        GADataType type = block.getOutDataPorts().get(0).getDataType();

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        AssignStatement outputAssign = null;

        if (type instanceof TPrimitive) {

            /*
             * PSEUDO CODE : ouptput = 0;
             */

            // output assign statement
            outputAssign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                    outputs.get(0), new IntegerExpression(0));

            outputAssign.setName(block.getName() + "Assign");

            // updating code model
            codemodel.add(outputAssign);

        } else if (type.isVector()) {

            /*
             * PSEUDO CODE : output[0] = 0; output[1] = 0; ... output[n-1] = 0;
             * (where n is the vector size)
             */

            // determining vector dimension
            int vectorDimension = Integer
                    .valueOf(((IntegerExpression) ((TArray) block
                            .getOutDataPorts().get(0).getDataType())
                            .getDimensions().get(0)).getLitValue());

            for (int i = 0; i < vectorDimension; i++) {
                outputAssign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        BackendUtilities.varIndex(outputs.get(0), i),
                        new IntegerExpression(0));

                outputAssign.setName(block.getName() + "Assign_" + i);

                // updating code model
                codemodel.add(outputAssign);
            }

        } else if (type.isMatrix()) {

            /*
             * PSEUDO CODE : output[0][0] = 0; output[0][1] = 0; ...
             * output[n-1][m-1] = 0; (where n and m are the matrix dimension)
             */

            // determining matrix dimensions
            IntegerExpression heightExpr = (IntegerExpression) ((TArray) outputs
                    .get(0).getDataType()).getDimensions().get(0);
            IntegerExpression widthExpr = (IntegerExpression) ((TArray) outputs
                    .get(0).getDataType()).getDimensions().get(1);

            int height = Integer.valueOf(heightExpr.getLitValue());
            int width = Integer.valueOf(widthExpr.getLitValue());

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {

                    outputAssign = new AssignStatement(
                            AssignOperator.SIMPLE_ASSIGN, BackendUtilities
                                    .varIndex2(outputs.get(0), i, j),
                            new IntegerExpression(0));

                    // updating code model
                    codemodel.add(outputAssign);
                }
            }
        }

        return codemodel;
    }

}
