package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.genericmodel.GAModelElement;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FromBackend extends CombinatorialImplBackend
                implements BlockPreProcessor{

	private Set<GAModelElement> postponedRemoval = new HashSet<GAModelElement>();
	
    @Override
    public void preProcess(GASystemModel m) {
        List<GAModelElement> toDelete = new LinkedList<GAModelElement>();

        Set<String> blockTypes = new HashSet<String>();
        blockTypes.add("Goto");
        blockTypes.add("From");
        SystemBlock modelRoot = m.getRootSystemBlock();
        Map<String, List<Block>> blockMap 
        	= modelRoot.getAllBlocksByTypes(blockTypes);
        /*
         * Do nothing if there are no 
         * from and goto block types 
         * in the map
         */
        if (blockMap.get("From") == null 
                && blockMap.get("Goto") == null) {
            return;
        }
        // list of all "From" blocks
        List<Block> froms = blockMap.get("From");
        // map of "Goto" blocks grouped by parent subsystem
        Map<SystemBlock, Map<String, Block>> gotos = new HashMap<SystemBlock, Map<String, Block>>();
        
        // sort gotos by parent and "GotoTag"
        for (Block gotoBlock : blockMap.get("Goto")) {
        	Map<String, Block> gotoMap = gotos.get((SystemBlock)gotoBlock.getParent());
        	if (gotoMap == null) {
        		gotoMap = new HashMap<String, Block>();
        		gotos.put((SystemBlock)gotoBlock.getParent(), gotoMap);
        	}
        	
            // read the 'GotoTag' parameter
            Parameter paramGoto = gotoBlock.getParameterByName("GotoTag");
            if (paramGoto == null) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "FromBackend.preProcess", "", 
                        "Parameter \"GotoTag\" not found\n Block: "
                        + gotoBlock.getReferenceString(), "");
                return;
            }
            
            String gotoTagValue = paramGoto.getStringValue();
            gotoMap.put(gotoTagValue, gotoBlock);
        }

        for (Block fromBlock : froms) {
            // read the 'GotoTag' parameter
            Parameter paramGoto = fromBlock.getParameterByName("GotoTag");
            if (paramGoto == null) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "FromBackend.preProcess", "", 
                        "Parameter \"GotoTag\" not found\n Block: "
                        + fromBlock.getReferenceString(), "");
                return;
            }
            
            String gotoTagValue = paramGoto.getStringValue();
        	Map<String, Block> gotoMap = gotos.get(fromBlock.getParent());
        	
        	if (gotoMap == null) {
                EventHandler.handle(EventLevel.ERROR,
                        "FromBackend.preProcess", "", 
                        "No Goto blocks in givens scope" 
                        + "\n Block: "
                        + fromBlock.getReferenceString(), "");
        	} else {
        		Block gotoBlock = gotoMap.get(gotoTagValue);
        		if (gotoBlock == null) {
                    EventHandler.handle(EventLevel.ERROR,
                            "FromBackend.preProcess", "", 
                            "No Goto block with tag \"" + gotoTagValue 
                            + "\"\n Block: "
                            + fromBlock.getReferenceString(), "");
        		} else {
        			mergeGotoFrom(gotoBlock, fromBlock, toDelete);
        		}
        		
        	}
        }    
        
        // remove elements that are scheduled for deletion
        for (GAModelElement el : postponedRemoval) {
        	el.removeMe();
        }
        postponedRemoval.clear();
    }
    
    /**
     * Merge the input signal of the goto block with the output signal of the
     * from block
     * 
     * @param gotoBlock
     * @param fromBlock
     * @param toDelete --
     *            list of elements to delete after replacement is finished
     */
    private void mergeGotoFrom(Block gotoBlock, Block fromBlock,
            List<GAModelElement> toDelete) {

        // find the signal between goto block and his previous block
        List<Signal> gotoInputs = gotoBlock.getIncomingSignals();
        Outport sourcePort = null;
        if (gotoInputs == null 
        		|| gotoInputs.isEmpty()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "FromBackend.mergeGotoFrom", "",
                    "Could not find input signal for GoTo block "
                            + gotoBlock.getReferenceString(), "");
            return;
        } else if (gotoInputs.size() > 1) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "FromBackend.mergeGotoFrom", "",
                    "Too many incoming signals in block \n "
                            + gotoBlock.getReferenceString()
                            + "\n expecting 1, found " + gotoInputs.size(), 
                    "");
            return;        	
        } else {
        	// memorise the source port
        	sourcePort = gotoInputs.get(0).getSrcPort();
        	// schedule signal for removal
        	postponedRemoval.add(gotoInputs.get(0));
        }
        
        /*
         * get the output signal(s) of the from block, and bind each of them to
         * the previous block of goto block
         */
        for (Signal signal : fromBlock.getOutgoingSignals()) {
        	signal.setSrcPort(sourcePort);
        }
        
        // remove from block
        fromBlock.removeMe();
        // schedule goto for removal (it can be used by other blocks)
        postponedRemoval.add(gotoBlock);
    }

	/* (non-Javadoc)
	 * @see geneauto.blocklibrary.backends.CombinatorialImplBackend#getComputationCode(geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block, java.util.List, java.util.List, java.util.List)
	 */
	@Override
	public List<GAModelElement> getComputationCode(Block block,
			List<Expression> inputs, List<Expression> outputs,
			List<Expression> controls) {
        EventHandler.handle(EventLevel.ERROR,
                "FromBackend.getComputationCode", "", 
                "From block should have been removed in the pre-processor" 
                + "\n Block: "
                + block.getReferenceString(), "");

		return super.getComputationCode(block, inputs, outputs, controls);
	}
    
}
