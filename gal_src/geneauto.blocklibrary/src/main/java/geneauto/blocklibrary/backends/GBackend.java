/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/GBackend.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.BreakStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

public class GBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        // checking block validity
        if (!block.getType().equals("G")) {
            EventHandler.handle(EventLevel.ERROR, "GBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration (computation code)
        List<GAModelElement> codemodel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramMatrix = block.getParameterByName("PARH1");

        Assert.assertNotNull(paramMatrix, "Parameter 'PARH1' not defined : "
                + block.getReferenceString());

        // parsing parameter value
        String matrixName = ((StringExpression) paramMatrix.getValue()).getLitValue();

        GADataType type = new TArray();

        ((TArray) type).setBaseType(new TRealDouble());

        List<Expression> dims = new ArrayList<Expression>();
        dims.add(new IntegerExpression(11));
        dims.add(new IntegerExpression(11));
        ((TArray) type).setDimensions(dims);

        Variable_CM matrixVar = new Variable_CM(
        		matrixName,
        		null, 
        		false, 
        		false, 
        		false,
                DefinitionScope.IMPORTED, 
                type);

        VariableExpression matrix = new VariableExpression(matrixVar);

        // creating the two index variables
        /*
         * int index_x = -1; int index_y = -1; double a = 0; double b = 0;
         */

        RangeExpression range1 = new RangeExpression(0, 8);
        Variable_CM indexVar1 = new Variable_CM("index_x",
        		new IntegerExpression(-1),
                false, 
                false, 
                false, 
                DefinitionScope.LOCAL, 
                range1.getDataType());
        VariableExpression index1 = new VariableExpression(indexVar1);
        index1.setName("index_x");
        
        

        RangeExpression range2 = new RangeExpression(0, 8);
        Variable_CM indexVar2 = new Variable_CM(
        		"index_y",
        		new IntegerExpression(-1),
                false, 
                false, 
                false, 
                DefinitionScope.LOCAL, 
                range2.getDataType());
        VariableExpression index2 = new VariableExpression(indexVar2);
        index2.setName("index_y");

        Variable_CM varA = new Variable_CM(
        		"a" + block.getId(),
        		new IntegerExpression(0), 
        		false,
                false, 
                false, 
                DefinitionScope.LOCAL, 
                new TRealDouble());
        codemodel.add(varA);
        VariableExpression a = new VariableExpression(varA);
        a.setName("a" + block.getId());

        Variable_CM varB = new Variable_CM(
        		"b" + block.getId(),
        		new IntegerExpression(0), 
        		false,
                false, 
                false, 
                DefinitionScope.LOCAL, 
                new TRealDouble());
        codemodel.add(varB);
        VariableExpression b = new VariableExpression(varB);
        b.setName("b" + block.getId());

        /*
         * if (%i1 < tab[0][1]) { index_x = 1; } else if (%i1 >= tab[0][10]){
         * index_x = 10; } else { for (index_x = 0; index_x < 9; index_x++){ if
         * ((%i1 >= tab[0][index_x])&&(%i1 < tab[0][index_x + 1])){ break; } } }
         */

        RangeIterationStatement loopSt = new RangeIterationStatement(
                new IfStatement(
                        new BinaryExpression(
                                new BinaryExpression(
                                        inputs.get(0).getCopy(),
                                        BackendUtilities
                                                .varIndex2(
                                                        matrix,
                                                        new IntegerExpression(
                                                                0),
                                                        index1
                                                                .getCopy()),
                                        BinaryOperator.GE_OPERATOR),
                                new BinaryExpression(
                                        inputs.get(0).getCopy(),
                                        BackendUtilities
                                                .varIndex2(
                                                        matrix,
                                                        new IntegerExpression(
                                                                0),
                                                        new BinaryExpression(
                                                                index1
                                                                        .getCopy(),
                                                                new IntegerExpression(
                                                                        1),
                                                                BinaryOperator.ADD_OPERATOR,
                                                                new TRealInteger(
                                                                        8,
                                                                        true))),
                                        BinaryOperator.LT_OPERATOR),
                                BinaryOperator.LOGICAL_AND_OPERATOR),
                        new BreakStatement(), null),
                    indexVar1, range1
                    );
        loopSt.addNameSpaceElement(indexVar1);
        IfStatement firstIf = new IfStatement(
                new BinaryExpression(inputs.get(0).getCopy(), BackendUtilities
                        .varIndex2(matrix, 0, 1), BinaryOperator.LT_OPERATOR),
                new AssignStatement(AssignOperator.SIMPLE_ASSIGN, index1
                        .getCopy(), new IntegerExpression(1)),
                new IfStatement(
                        new BinaryExpression(inputs.get(0).getCopy(),
                                BackendUtilities.varIndex2(matrix, 0, 10),
                                BinaryOperator.GE_OPERATOR),
                        new AssignStatement(AssignOperator.SIMPLE_ASSIGN, index1
                                .getCopy(), new IntegerExpression(10)), loopSt
                                ));

        // giving a name to the if statement
        firstIf.setName(block.getName() + "firstIf");

        // annotating generated code model to indicate source block of the
        // code model part
        firstIf.setAnnotations(BackendUtilities.getAnnotation(block));

        codemodel.add(firstIf);
        RangeIterationStatement loopSt2 = new RangeIterationStatement(
                new IfStatement(
                        new BinaryExpression(
                                new BinaryExpression(
                                        inputs.get(1).getCopy(),
                                        BackendUtilities
                                                .varIndex2(
                                                        matrix,
                                                        new IntegerExpression(
                                                                0),
                                                        index2
                                                                .getCopy()),
                                        BinaryOperator.GE_OPERATOR),
                                new BinaryExpression(
                                        inputs.get(1).getCopy(),
                                        BackendUtilities
                                                .varIndex2(
                                                        matrix,
                                                        new IntegerExpression(
                                                                0),
                                                        new BinaryExpression(
                                                                index2
                                                                        .getCopy(),
                                                                new IntegerExpression(
                                                                        1),
                                                                BinaryOperator.ADD_OPERATOR,
                                                                new TRealInteger(
                                                                        8,
                                                                        true))),
                                        BinaryOperator.LT_OPERATOR),
                                BinaryOperator.LOGICAL_AND_OPERATOR),
                        new BreakStatement(), null),
                indexVar2, range2
                );
        loopSt2.addNameSpaceElement(indexVar2);
        /*
         * if (%i2 < tab[1][0]) { index_y = 1; } else if (%i2 >= tab[10][0]){
         * index_y = 10; } else { for (index_y = 0; index_y < 9; index_y++){ if
         * ((%i2 >= tab[0][index_y])&&(%i2< tab[0][index_y + 1])){ break; } } }
         */

        IfStatement secondIf = new IfStatement(
                new BinaryExpression(inputs.get(1).getCopy(), BackendUtilities
                        .varIndex2(matrix, 1, 0), BinaryOperator.LT_OPERATOR),
                new AssignStatement(AssignOperator.SIMPLE_ASSIGN, index2
                        .getCopy(), new IntegerExpression(1)),
                new IfStatement(
                        new BinaryExpression(inputs.get(1).getCopy(),
                                BackendUtilities.varIndex2(matrix, 10, 0),
                                BinaryOperator.GE_OPERATOR),
                        new AssignStatement(AssignOperator.SIMPLE_ASSIGN, index2
                                .getCopy(), new IntegerExpression(10)), loopSt2
                                ));

        // giving a name to the if statement
        secondIf.setName(block.getName() + "secondIf");

        // annotating generated code model to indicate source block of the
        // code model part
        secondIf.setAnnotations(BackendUtilities.getAnnotation(block));

        codemodel.add(secondIf);
        
        /*
         * $assign1 : %o1 = %o1 + ((%i1 - tab[0][index_x])
         * ((tab[1+index_x][index_y] - tab[index_x][index_y])/
         * (tab[0][1+index_x] - tab[0][index_x])));
         */

        AssignStatement assign1 = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN,
                outputs.get(0).getCopy(),
                new BinaryExpression(
                        outputs.get(0).getCopy(),
                        new BinaryExpression(
                                new BinaryExpression(inputs.get(0).getCopy(),
                                        BackendUtilities.varIndex2(matrix,
                                                new IntegerExpression(0),
                                                index1.getCopy()),
                                        BinaryOperator.SUB_OPERATOR,
                                        new TRealDouble()),
                                new BinaryExpression(
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index1
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR),
                                                                index2
                                                                        .getCopy()),
                                                BackendUtilities.varIndex2(
                                                        matrix, index1
                                                                .getCopy(),
                                                        index2.getCopy()),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new IntegerExpression(
                                                                        0),
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index1
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR)),
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new IntegerExpression(
                                                                        0),
                                                                index1
                                                                        .getCopy()),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        BinaryOperator.DIV_OPERATOR,
                                        new TRealDouble()),
                                BinaryOperator.MUL_OPERATOR, new TRealDouble()),
                        BinaryOperator.ADD_OPERATOR, new TRealDouble()));

        assign1.setName("assign1");

        /*
         * $assign2 : %o1 = %o1 + ((%i2 - tab[index_y][0])
         * ((tab[index_x][1+index_y] - tab[index_x][index_y])/ (tab[index_y][0] -
         * tab[index_y][0])));
         */

        AssignStatement assign2 = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN,
                outputs.get(0).getCopy(),
                new BinaryExpression(
                        outputs.get(0).getCopy(),
                        new BinaryExpression(
                                new BinaryExpression(inputs.get(1).getCopy(),
                                        BackendUtilities.varIndex2(matrix,
                                                index2.getCopy(),
                                                new IntegerExpression(0)),
                                        BinaryOperator.SUB_OPERATOR,
                                        new TRealDouble()),
                                new BinaryExpression(
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                index1
                                                                        .getCopy(),
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index2
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR)),
                                                BackendUtilities.varIndex2(
                                                        matrix, index1
                                                                .getCopy(),
                                                        index2.getCopy()),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                index2
                                                                        .getCopy(),
                                                                new IntegerExpression(
                                                                        0)),
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                index2
                                                                        .getCopy(),
                                                                new IntegerExpression(
                                                                        0)),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        BinaryOperator.DIV_OPERATOR,
                                        new TRealDouble()),
                                BinaryOperator.MUL_OPERATOR, new TRealDouble()),
                        BinaryOperator.ADD_OPERATOR, new TRealDouble()));

        assign2.setName("assign2");

        /*
         * $assign3 : a = %o1 + ((%i1 - tab[0][index_x])
         * ((tab[1+index_x][index_y] - tab[index_x][index_y])/
         * (tab[0][1+index_x] - tab[0][index_x])));
         */

        AssignStatement assign3 = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN,
                a.getCopy(),
                new BinaryExpression(
                        outputs.get(0).getCopy(),
                        new BinaryExpression(
                                new BinaryExpression(inputs.get(0).getCopy(),
                                        BackendUtilities.varIndex2(matrix,
                                                new IntegerExpression(0),
                                                index1.getCopy()),
                                        BinaryOperator.SUB_OPERATOR,
                                        new TRealDouble()),
                                new BinaryExpression(
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index1
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR),
                                                                index2
                                                                        .getCopy()),
                                                BackendUtilities.varIndex2(
                                                        matrix, index1
                                                                .getCopy(),
                                                        index2.getCopy()),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new IntegerExpression(
                                                                        0),
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index1
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR)),
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new IntegerExpression(
                                                                        0),
                                                                index1
                                                                        .getCopy()),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        BinaryOperator.DIV_OPERATOR,
                                        new TRealDouble()),
                                BinaryOperator.MUL_OPERATOR, new TRealDouble()),
                        BinaryOperator.ADD_OPERATOR, new TRealDouble()));

        assign3.setName("assign3");

        /*
         * $assign4 : b = tab[index_x][1+index_y] + ((%o1 - tab[0][index_x])
         * ((tab[1+index_x][1+index_y] - tab[index_x][1+index_y])/
         * (tab[0][1+index_x] - tab[0][index_x])));
         */

        AssignStatement assign4 = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN,
                b.getCopy(),
                new BinaryExpression(
                        BackendUtilities.varIndex2(matrix, index1.getCopy(),
                                new BinaryExpression(new IntegerExpression(1),
                                        index2.getCopy(),
                                        BinaryOperator.ADD_OPERATOR)),
                        new BinaryExpression(
                                new BinaryExpression(outputs.get(0).getCopy(),
                                        BackendUtilities.varIndex2(matrix,
                                                new IntegerExpression(0),
                                                index1.getCopy()),
                                        BinaryOperator.SUB_OPERATOR,
                                        new TRealDouble()),
                                new BinaryExpression(
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index1
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR,
                                                                        new TRealInteger(
                                                                                8,
                                                                                true)),
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index2
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR,
                                                                        new TRealInteger(
                                                                                8,
                                                                                true))),
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                index1
                                                                        .getCopy(),
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index2
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR,
                                                                        new TRealInteger(
                                                                                8,
                                                                                true))),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new IntegerExpression(
                                                                        0),
                                                                new BinaryExpression(
                                                                        new IntegerExpression(
                                                                                1),
                                                                        index1
                                                                                .getCopy(),
                                                                        BinaryOperator.ADD_OPERATOR,
                                                                        new TRealInteger(
                                                                                8,
                                                                                true))),
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new IntegerExpression(
                                                                        0),
                                                                index1
                                                                        .getCopy()),
                                                BinaryOperator.SUB_OPERATOR,
                                                new TRealDouble()),
                                        BinaryOperator.DIV_OPERATOR,
                                        new TRealDouble()),
                                BinaryOperator.MUL_OPERATOR, new TRealDouble()),
                        BinaryOperator.ADD_OPERATOR, new TRealDouble()));

        assign4.setName("assign4");

        /*
         * assign5 : %o1 = a + ((%i2 - tab[index_y][0]) ((b - a)/
         * (tab[1+index_y][0] - tab[index_y][0])));
         */

        AssignStatement assign5 = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0).getCopy(),
                new BinaryExpression(a.getCopy(), new BinaryExpression(
                        new BinaryExpression(inputs.get(1).getCopy(),
                                BackendUtilities.varIndex2(matrix, index2
                                        .getCopy(), new IntegerExpression(0)),
                                BinaryOperator.SUB_OPERATOR, new TRealDouble()),
                        new BinaryExpression(new BinaryExpression(b.getCopy(),
                                a.getCopy(), BinaryOperator.SUB_OPERATOR,
                                new TRealDouble()), new BinaryExpression(
                                BackendUtilities.varIndex2(matrix,
                                        new BinaryExpression(
                                                new IntegerExpression(1),
                                                index2.getCopy(),
                                                BinaryOperator.ADD_OPERATOR),
                                        new IntegerExpression(0)),
                                BackendUtilities.varIndex2(matrix, index2
                                        .getCopy(), new IntegerExpression(0)),
                                BinaryOperator.SUB_OPERATOR, new TRealDouble()),
                                BinaryOperator.DIV_OPERATOR, new TRealDouble()),
                        BinaryOperator.MUL_OPERATOR, new TRealDouble()),
                        BinaryOperator.ADD_OPERATOR, new TRealDouble()));

        assign5.setName("assign5");

        /*
         * %o1 = tab[index_x][index_y]; if ((%i2 <= tab[1][0])||(%i2 >=
         * tab[10][0])){ if ((%i1 > tab[0][1])&&(%i1 < tab[0][10])){ $assign1 } }
         * else { if ((%i1 <= tab[0][1])||(%i1 >= tab[0][10])){ $assign2 } else {
         * $assign3 $assign4 $assign5 } }
         */

        AssignStatement assignInit = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0).getCopy(),
                BackendUtilities.varIndex2(matrix, index1.getCopy(), index2
                        .getCopy()));

        // giving a name to the assign statement
        assignInit.setName(block.getName() + "assignInit");

        // annotating generated code model to indicate source block of the
        // code model part
        assignInit.setAnnotations(BackendUtilities.getAnnotation(block));

        codemodel.add(assignInit);

        List<Statement> statements = new ArrayList<Statement>();
        statements.add(assign3);
        statements.add(assign4);
        statements.add(assign5);

        CompoundStatement assigns = new CompoundStatement(null, statements);

        IfStatement thirdIf = new IfStatement(new BinaryExpression(
                new BinaryExpression(inputs.get(1).getCopy(), BackendUtilities
                        .varIndex2(matrix, 1, 0), BinaryOperator.LE_OPERATOR),
                new BinaryExpression(inputs.get(1).getCopy(), BackendUtilities
                        .varIndex2(matrix, 10, 0), BinaryOperator.GE_OPERATOR),
                BinaryOperator.LOGICAL_OR_OPERATOR), new IfStatement(
                new BinaryExpression(new BinaryExpression(inputs.get(0)
                        .getCopy(), BackendUtilities.varIndex2(matrix, 0, 1),
                        BinaryOperator.GT_OPERATOR), new BinaryExpression(inputs
                        .get(0).getCopy(), BackendUtilities.varIndex2(matrix,
                        0, 10), BinaryOperator.LT_OPERATOR),
                        BinaryOperator.LOGICAL_AND_OPERATOR), assign1, null),
                new IfStatement(new BinaryExpression(new BinaryExpression(
                        inputs.get(0).getCopy(), BackendUtilities.varIndex2(
                                matrix, 0, 1), BinaryOperator.LE_OPERATOR),
                        new BinaryExpression(inputs.get(0).getCopy(),
                                BackendUtilities.varIndex2(matrix, 0, 10),
                                BinaryOperator.GE_OPERATOR),
                        BinaryOperator.LOGICAL_OR_OPERATOR), assign2, assigns));

        // giving a name to the if statement
        thirdIf.setName(block.getName() + "thirdIf");

        // annotating generated code model to indicate source block of the
        // code model part
        thirdIf.setAnnotations(BackendUtilities.getAnnotation(block));

        codemodel.add(thirdIf);

        return codemodel;
    }

}
