/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/backends/FBackend.java,v $
 *  @version	$Revision: 1.22 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.backends;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.NameSpace;
import geneauto.models.gacodemodel.NameSpaceElement_CM;
import geneauto.models.gacodemodel.Variable_CM;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.RangeExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.BreakStatement;
import geneauto.models.gacodemodel.statement.CompoundStatement;
import geneauto.models.gacodemodel.statement.IfStatement;
import geneauto.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Backend class for F block.
 * 
 * 
 */
public class FBackend extends CombinatorialImplBackend {

    @Override
    public List<GAModelElement> getComputationCode(Block block,
            List<Expression> inputs, List<Expression> outputs,
            List<Expression> controls) {

        if (!block.getType().equals("F")) {
            EventHandler.handle(EventLevel.ERROR, "FBackend", "",
                    "Block type error : " + block.getReferenceString(), "");
        }

        // checking number of inputs and outputs
        Assert.assertNotEmpty(outputs,
                "Block has not expected number of outputs : "
                        + block.getReferenceString());

        Assert.assertNotEmpty(inputs,
                "Block has not expected number of inputs : "
                        + block.getReferenceString());

        // Model declaration
        List<GAModelElement> codeModel = new ArrayList<GAModelElement>();

        // reading block parameter
        Parameter paramMatrix = block.getParameterByName("PARH1");

        Assert.assertNotNull(paramMatrix, "Parameter 'PARH1' not defined : "
                + block.getReferenceString());

        // parsing parameter value
        String matrixName = ((StringExpression) paramMatrix.getValue()).getLitValue();

        GADataType type = new TArray();

        ((TArray) type).setBaseType(new TRealDouble());

        List<Expression> dims = new ArrayList<Expression>();
        dims.add(new IntegerExpression(16));
        dims.add(new IntegerExpression(2));
        ((TArray) type).setDimensions(dims);

        Variable_CM matrixVar = new Variable_CM(matrixName,
        		null, 
        		false, 
        		false, 
        		false,
                DefinitionScope.IMPORTED, 
                type);

        VariableExpression matrix = new VariableExpression(matrixVar);
        matrix.setName(matrixName);

        /*
         * if (%i1 < tab[0][0]){ %o1 = tab[0][1]; } else if (%i1 >= tab[15][0]){
         * %o1 = tab[15][1]; } else { int it = -1; for (it = 0; it < 14; it++){
         * if ((%i1 >= tab[it][0])&&(%i1 < tab[it+1][0])){ break; } }
         */

        List<NameSpaceElement_CM> nsElements = new ArrayList<NameSpaceElement_CM>();
        RangeExpression range = new RangeExpression(0, 13);
        Variable_CM itVar = new Variable_CM("i" + block.getId(),
        		new IntegerExpression(-1), 
        		false,
                false, 
                false, 
                DefinitionScope.LOCAL, 
                range.getDataType());

        NameSpace elseNameSpace = new NameSpace(nsElements);

        VariableExpression it = new VariableExpression(itVar);
        it.setName("i" + block.getId());

        List<Statement> elseStatements = new ArrayList<Statement>();
        
        RangeIterationStatement loopSt = new RangeIterationStatement(
                new IfStatement(new BinaryExpression(new BinaryExpression(
                        inputs.get(0).getCopy(),
                        BackendUtilities.varIndex2(matrix, it.getCopy(),
                                new IntegerExpression(0)),
                        BinaryOperator.GE_OPERATOR), new BinaryExpression(inputs
                        .get(0).getCopy(), BackendUtilities.varIndex2(matrix,
                        new BinaryExpression(it.getCopy(),
                                new IntegerExpression(1),
                                BinaryOperator.ADD_OPERATOR),
                        new IntegerExpression(0)), BinaryOperator.LT_OPERATOR),
                        BinaryOperator.LOGICAL_AND_OPERATOR),
                        new BreakStatement(), null),
                    itVar, range
                    );
        loopSt.addNameSpaceElement(itVar);
        elseStatements.add(loopSt);

        CompoundStatement elseState = new CompoundStatement(elseNameSpace,
                elseStatements);

        IfStatement ifstate = new IfStatement(new BinaryExpression(inputs
                .get(0), BackendUtilities.varIndex2(matrix, 0, 0),
                BinaryOperator.LT_OPERATOR), new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN, outputs.get(0), BackendUtilities
                        .varIndex2(matrix, 0, 1)), new IfStatement(
                new BinaryExpression(inputs.get(0).getCopy(), BackendUtilities
                        .varIndex2(matrix, 15, 0), BinaryOperator.GE_OPERATOR),
                new AssignStatement(AssignOperator.SIMPLE_ASSIGN,
                        outputs.get(0), BackendUtilities.varIndex2(matrix, 15,
                                1)), elseState));

        /*
         * %o1 = tab[it][1] + ((%i1 tab[it][0])((tab[it+1][1]
         * tab[it][1])/tab[it+1][0] tab[it][0])));
         */

        AssignStatement assign = new AssignStatement(
                AssignOperator.SIMPLE_ASSIGN,
                outputs.get(0).getCopy(),
                new BinaryExpression(
                        new BinaryExpression(
                                inputs.get(0).getCopy(),
                                BackendUtilities.varIndex2(matrix,
                                        it.getCopy(), new IntegerExpression(0)),
                                BinaryOperator.SUB_OPERATOR),
                        new BinaryExpression(
                                new BinaryExpression(
                                        new BinaryExpression(
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                new BinaryExpression(
                                                                        it
                                                                                .getCopy(),
                                                                        new IntegerExpression(
                                                                                1),
                                                                        BinaryOperator.ADD_OPERATOR),
                                                                new IntegerExpression(
                                                                        1)),
                                                BackendUtilities
                                                        .varIndex2(
                                                                matrix,
                                                                it.getCopy(),
                                                                new IntegerExpression(
                                                                        1)),
                                                BinaryOperator.SUB_OPERATOR),
                                        BackendUtilities
                                                .varIndex2(
                                                        matrix,
                                                        new BinaryExpression(
                                                                it.getCopy(),
                                                                new IntegerExpression(
                                                                        1),
                                                                BinaryOperator.ADD_OPERATOR),
                                                        new IntegerExpression(0)),
                                        BinaryOperator.DIV_OPERATOR),
                                BackendUtilities.varIndex2(matrix,
                                        it.getCopy(), new IntegerExpression(0)),
                                BinaryOperator.SUB_OPERATOR),
                        BinaryOperator.MUL_OPERATOR));

        ifstate.setName(block.getName() + "IfState");
        assign.setName(block.getName() + "Assign");

        assign.setAnnotations(BackendUtilities.getAnnotation(block));

        // adding assignment to generated code model
        codeModel.add(matrixVar);
        codeModel.add(itVar);
        codeModel.add(ifstate);
        codeModel.add(assign);

        return codeModel;
    }

}
