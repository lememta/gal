/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/users/MAP2DBackend.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2010-02-02 12:26:07 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.users;

import geneauto.blocklibrary.backends.UserDefinedBlockImplBackend;
import geneauto.models.gacodemodel.ExternalDependency;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.BackendUtilities;
import geneauto.models.utilities.emlconverter.EMLAccessor;

import java.util.ArrayList;
import java.util.List;

public class MAP2DBackend extends UserDefinedBlockImplBackend{

	@Override
	public List<GAModelElement> getComputationCode(Block block,
			List<Expression> inputs, List<Expression> outputs,
			List<Expression> controls, List<Expression> memory) {
	
		//CallExpression exp = null;
		
		CallExpression exp = new CallExpression();
		
		List<Expression> args = new ArrayList<Expression>();
							
		Parameter param1 = null;
		Parameter param2 = null;
		Parameter param3 = null;
		Parameter param4 = null;
		Parameter param5 = null;
		
		for (Parameter p : block.getParameters()) {
			if (p.getName().equals("in1")) {
				param1 = p;
			}
			if (p.getName().equals("in2")) {
				param2 = p;
			}
			if (p.getName().equals("out")) {
				param3 = p;
			}
			if (p.getName().equals("IP_type")) {
				param4 = p;
			}
			if (p.getName().equals("IP_MAP")) {
				param5 = p;
			}
				}
		
		String val1 = ((StringExpression) param1.getValue()).getLitValue();
		String val2 = ((StringExpression) param2.getValue()).getLitValue();
		String val3 = ((StringExpression) param3.getValue()).getLitValue();
		String val4 = ((StringExpression) param4.getValue()).getLitValue();
		
		Expression val5 = EMLAccessor
			.convertEMLToCodeModel(((StringExpression) param5.getValue()).getLitValue(), false);
			
		((SystemBlock) block.getParent()).linkVartoVarExpr(
                ((SystemBlock) block.getParent()).getSystemVars(), val5);
			
		args.add(val5);
		args.add(inputs.get(0));
		args.add(inputs.get(1));
				
		exp.setDependency(new ExternalDependency("math"));
		exp.setDataType(new TRealDouble());		
		exp.setArguments(args);
		exp.setName(val3+"_"+val4+"_PTR_"+val1+"_"+val2);
	
			
		AssignStatement assign = null;
	
		assign = new AssignStatement(AssignOperator.SIMPLE_ASSIGN, outputs
				.get(0), exp);

		// giving a name to the assign statement
		assign.setName(block.getName() + "Assign");

		// annotating generated code model to indicate source block of the
		// code model part
		assign.setAnnotations(BackendUtilities.getAnnotation(block));

		//assign.setLeftExpression(outputs.get(0));
		//assign.setRightExpression(new BinaryExpression(inputs.get(0),inputs.get(1),BinaryOperator.AddOperator));
		
		
		List<GAModelElement> code = new ArrayList<GAModelElement>();
		code.add(assign);
		
		
		return code;
	}
	
}
