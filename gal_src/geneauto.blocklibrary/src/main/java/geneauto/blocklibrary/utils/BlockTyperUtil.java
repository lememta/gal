package geneauto.blocklibrary.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.utilities.DataTypeAccessor;

public class BlockTyperUtil {

	public static void typeInputPort(Inport port){
		Block parent = port.getParentBlock();
		
		for (Signal sig: port.getIncomingSignals()){
			if (sig.getDstPort() == port){
				OutDataPort outDP = (OutDataPort) sig.getSrcPort();
				if (outDP.getDataType() == null){
					if (outDP.getParentBlock() instanceof SystemBlock){
						SystemBlock sourceSystem = (SystemBlock) outDP.getParentBlock();
						int index = sourceSystem.getAllOutports().indexOf(outDP);
						Block outPortBlock = sourceSystem.getBlocksByType("Outport").get(index);
						if (outPortBlock.getInDataPorts().get(0).getDataType() == null){
							Parameter dtParameter = outPortBlock.getParameterByName("DataType");
							String dtValue = dtParameter.getValue().getStringValue();
							if (dtValue.equals("auto")){
								EventHandler.handle(EventLevel.ERROR, "TypingUtils", "",
		    	                        " Source input block impossible to type. \n"+
		    	                        " Try to type the source block output." + "\n "
		    	                                + outDP.getShortReferenceString()
		    	                                + "\n in block " + parent.getReferenceString());
							} else {
								port.setDataType(DataTypeAccessor.getDataType(dtValue));
							}
						} else {
							port.setDataType(outPortBlock.getInDataPorts().get(0).getDataType());
						}
					} else {
						Parameter dtParameter = outDP.getParentBlock().getParameterByName("OutDataTypeMode");
						if (dtParameter == null){
							dtParameter = outDP.getParentBlock().getParameterByName("DataType");
						}
						if (outDP.getParentBlock().getType().equals("RelationalOperator") ||
							outDP.getParentBlock().getType().equals("Logic")){
							port.setDataType(new TBoolean());
						} else {
							if (dtParameter == null){
								EventHandler.handle(EventLevel.ERROR, "TypingUtils", "",
		    	                        " Impossible to find Parameter OutDataTypeMode"
		    	                        + "\n in block " + outDP.getParentBlock().getReferenceString());
							}
							String dtValue = dtParameter.getValue().getStringValue();
							if (dtValue.equals("auto")){
								EventHandler.handle(EventLevel.ERROR, "TypingUtils", "",
		    	                        " Source input block impossible to type. \n"+
		    	                        " Try to type the source block output." + "\n "
		    	                                + outDP.getShortReferenceString()
		    	                                + "\n in block " + parent.getReferenceString());
							} else {
								port.setDataType(DataTypeAccessor.getDataType(dtValue));
							}
						}
					}
				}
			}
		}
	}
	
	public static void typeOutputPort(OutDataPort port){
		if (port.getParentBlock() instanceof SystemBlock){
			SystemBlock sourceSystem = (SystemBlock) port.getParentBlock();
			int index = sourceSystem.getAllOutports().indexOf(port);
			Block outPortBlock = sourceSystem.getBlocksByType("Outport").get(index);
			if (outPortBlock.getInDataPorts().get(0).getDataType() == null){
				Parameter dtParameter = outPortBlock.getParameterByName("DataType");
				String dtValue = dtParameter.getValue().getStringValue();
				if (dtValue.equals("auto")){
					EventHandler.handle(EventLevel.ERROR, "TypingUtils", "",
	                        " Source input block impossible to type. \n"+
	                        " Try to type the source block output." + "\n "
	                                + port.getShortReferenceString()
	                                + "\n in block " + port.getParentBlock().getReferenceString());
				} else {
					port.setDataType(DataTypeAccessor.getDataType(dtValue));
				}
			} else {
				port.setDataType(outPortBlock.getInDataPorts().get(0).getDataType());
			}
		} else {
			Parameter dtParameter = port.getParentBlock().getParameterByName("DataType");
			if (dtParameter == null){
				EventHandler.handle(EventLevel.ERROR, "TypingUtils", "",
                        " Impossible to find Parameter DataType"
                        + "\n in block " + port.getParentBlock().getReferenceString());
			}
			String dtValue = dtParameter.getValue().getStringValue();
			if (dtValue.equals("auto")){
				EventHandler.handle(EventLevel.ERROR, "TypingUtils", "",
                        " Source input block impossible to type. \n"+
                        " Try to type the source block output." + "\n "
                                + port.getShortReferenceString()
                                + "\n in block " + port.getParentBlock().getReferenceString());
			} else {
				port.setDataType(DataTypeAccessor.getDataType(dtValue));
			}
		}
	}
	
}
