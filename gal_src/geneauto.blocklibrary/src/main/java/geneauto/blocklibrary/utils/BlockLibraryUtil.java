/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/utils/BlockLibraryUtil.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2011-11-28 22:44:23 $
 *
 *  Copyright (c) 2006-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */

package geneauto.blocklibrary.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.DataTypeAccessor;

import java.util.LinkedList;
import java.util.List;

/* Common operations of typers and backends */
public class BlockLibraryUtil {
	/**
	 * Checks that block inputs are typed. 
	 * Throws EventLevel.ERROR when they are not
	 * 
	 * @param block	-- block to be checked
	 * @param location -- name of the error location to be displayed in 
	 * 						the error message
	 * @return	true if inputs are typed
	 */
    public static boolean inputsAreTyped(Block block, String location) {
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                EventHandler.handle(EventLevel.ERROR,
                        location, "",
                        "The block has an untyped intput!" 
                       + "\n Block: " + block.getReferenceString(), "");
                return false;
            }            
        }
        return true;
    }
    
	/**
	 * Checks that block inputs are typed. 
	 * Throws EventLevel.ERROR when they are not
	 * 
	 * @param block	-- block to be checked
	 * @param location -- name of the error location to be displayed in 
	 * 						the error message
	 * @return	true if inputs are typed
	 */
    public static boolean outputsAreTyped(Block block, String location) {
        for (OutDataPort inport : block.getOutDataPorts()) {
            if (inport.getDataType() == null) {
                EventHandler.handle(EventLevel.ERROR,
                        location, "",
                        "The block has an untyped output!" 
                       + "\n Block: " + block.getReferenceString(), "");
                return false;
            }            
        }
        
        return true;
    }
    
	/**
	 * Checks that block inputs and outputs are typed. 
	 * Throws EventLevel.ERROR when they are not
	 * 
	 * @param block	-- block to be checked
	 * @param location -- name of the error location to be displayed in 
	 * 						the error message
	 * @return	true if inputs are typed
	 */
    public static boolean portsAreTyped(Block block, String location) {
        
    	if (!(inputsAreTyped(block, location))) {
    		return false;
    	}
    	
        return outputsAreTyped(block, location);
    }
    
    /**
     * Reads signal data type from block parameters and tries to derive the
     * data type from those parameters. In case the data type is not defined 
     * in parameters, the defaultDT argument is returned. In case the data type 
     * is partially defined in block parameters, the missing parts are derived 
     * from defaultDT
     * 
     * For outport block, type can be totally specified (port dimension and data
     * type), partially specified (port dimension and data type) or not
     * specified at all. - If type is fully specified (both port dimension and
     * data type exists), a check is done to ensure compatibility between
     * derived type and specified type. Derived type can be null in this case. -
     * If type is partially defined (only dimension or data type is given), a
     * check is done between specification data and either signal or data type
     * of the derived type. Missing information is filled in from the derived
     * type. Derived type can't be null in this case. - If there's no
     * specification on the type, derived type is directly assigned to the port.
     * 
     * @param block -- the block that is supposed to hold the data type 
     * 				   definitions
     * @param defaultDT -- default data type used in case the block parameters 
     * 					do not define the data type or define it partially
     * @param location -- name of the class/method used in case of error message  
     *          
     * 
     * @return date type for corresponding output port
     */
    public static GADataType getOutportType(Block block, 
    										GADataType defaultDT,
    										String location) {
        //GADataType derivedDT = block.getInDataPorts().get(0).getDataType();

        // a flag to memorise if any components of user defined types were
        // by default we assume that there is no user-defined type
        boolean hasUserdefinedType = false;

        // determine data type of the system output port
        BlockParameter dataTypeParam = block.getParameterByName("DataType");
        String dataTypeStr = null;
        if (dataTypeParam == null) {
        	// no explicit parameter in the model -- the means "Auto"
        	dataTypeStr = "auto";

        } else {
        	// derive from parameter
        	dataTypeStr = dataTypeParam.getStringValue();
    	}

        if (dataTypeStr.equals("auto")) {
            if (defaultDT == null) {
                EventHandler.handle(
                                EventLevel.ERROR, 
                                location, 
                                "",
                                "Derived data type of block\n "
                                        + block.getReferenceString()
                                        + "\n can not be null when \"Data type\" "
                                        + "in signal specification is set to \"auto\".");
                return null;
            }
        } else {
            hasUserdefinedType = true;
        }

        /*
         * expression with port dimensions -1 Dimensions are inherited from
         * input signal (IntegerExpression) n Vector signal of width n accepted
         * (ListExpression or IntegerExpression) [m n] Matrix signal having m
         * rows and n columns accepted (ListExpression)
         */
        Expression portDimensions = null;
        try {
        	portDimensions = block
                .getParameterByName("PortDimensions").getValue();
        } catch (Exception e) {
        	// the block did not have PortDimensions parameter
        	// use the default value
        	portDimensions = new IntegerExpression(-1);
        }

        // number of dimensions (0 for scalar, 1 for vector etc)
        int portDimensionality = 0;
        // list for port dimensions
        List<Expression> portDimList = new LinkedList<Expression>();

        // makes sure that expressions like [-1] are treated as scalars
        portDimensions.normalizeShape(true);

        if (portDimensions instanceof IntegerExpression) {
            // if value is -1 derive the dimension from derived type
            if (((IntegerExpression) portDimensions).getIntValue() == -1) {
                // derivedDT must not be null here!
                if (defaultDT == null) {
                    EventHandler.handle(
                    		EventLevel.ERROR, location, "",
                            "Derived data type of block\n "
                                    + block.getReferenceString()
                                    + "\n can not be null when \"Port dimensions\" "
                                    + "in Signal specification is set to \"-1\".");
                    return null;
                }

                portDimensionality = defaultDT.getDimensionality();
                if (defaultDT.isScalar()) {
                    portDimensions = new IntegerExpression(0);
                } else {
                    for (Expression e : defaultDT.getDimensions()) {
                        portDimList.add(e.getCopy());
                    }
                }
                // otherwise the number is vector dimension
            } else if (((IntegerExpression) portDimensions).getIntValue() == 1) {
                hasUserdefinedType = true;
                portDimensionality = 0;
            } else {
                hasUserdefinedType = true;
                portDimensionality = 1;
                portDimList.add(portDimensions.getCopy());
            }
        } else if (portDimensions instanceof GeneralListExpression) {
            // take the number of dimension from the user-defined expression
            hasUserdefinedType = true;
            portDimensionality = portDimensions.getDataType()
                    .getDimensionality();
            Integer[] i = new Integer[1];
            for (i[0] = 0; i[0] < portDimensionality; i[0]++) {
                portDimList.add(portDimensions.getElementByIndex(i).getCopy());
            }
        } else {
            // we assume there is either IntegerExpression or ListExpression
            EventHandler.handle(EventLevel.CRITICAL_ERROR, location, "",
                    "Unknown format of port dimension expression in block \n "
                            + block.getReferenceString(), "");
            return null;
        }

        // make a GADataType instance with this data and signal type
        GADataType specifiedDT = null;

        // make specified type from specified informations about data
        // and signal type
        if (portDimensionality > 0) {
            // array type
            if (dataTypeStr.equals("auto")) {
                specifiedDT = new TArray(portDimList, defaultDT
                        .getPrimitiveType());
            } else {
                specifiedDT = new TArray(portDimList,
                        (TPrimitive) DataTypeAccessor.getDataType(dataTypeStr));
            }
        } else {
            // scalar type
            if (dataTypeStr.equals("auto")) {
                specifiedDT = defaultDT.getCopy();
            } else {
                specifiedDT = DataTypeAccessor.getDataType(dataTypeStr);
            }
        }

        /*
         * if there was a derived type check if the derived type is compatible
         * with the user-defined type.
         * 
         * NB! here we have extra check in case there was no user-defined
         */
        if (hasUserdefinedType && defaultDT != null
                && !defaultDT.isSubType(specifiedDT)) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
            		location, "", "Derived data type "
                    + DataTypeAccessor.toString(defaultDT) + " of block\n "
                    + block.getReferenceString()
                    + "\n is not compatible with user-defined datatype "
                    + DataTypeAccessor.toString(specifiedDT), "");
        }

        return specifiedDT;
    }

}
