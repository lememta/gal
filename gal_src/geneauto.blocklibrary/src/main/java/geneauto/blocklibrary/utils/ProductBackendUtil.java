/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/utils/ProductBackendUtil.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2010-01-26 21:42:12 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.blocklibrary.utils;

import geneauto.models.gacodemodel.operator.BinaryOperator;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for shared methods used by Product block backend and typer
 * 
 */
public class ProductBackendUtil {

    /**
     * Parse product parameters (for instance "/**" for a 3-inputs block)
     * 
     * @param operatorStr	 	string with operators for each input
     * @param mulType 	 		type of multiplication (applicable to vector 
     * 							and matrix types)
     * @return					list of BinaryOperator's derived from argument 
     * 							in operatorStr and mulType
     */
    public static List<BinaryOperator> parseProductParameter(
    													String operatorStr, 
    													String mulType) {

        List<BinaryOperator> operators = new ArrayList<BinaryOperator>();

        for (int index = 0; index < operatorStr.length(); index++) {
            if (operatorStr.charAt(index) == '/') {
                operators.add(BinaryOperator.DIV_OPERATOR);
            }
            if (operatorStr.charAt(index) == '*') {
                if (mulType.equals("Element-wise(.*)")) {
                    operators.add(BinaryOperator.MUL_OPERATOR);
                } else if (mulType.equals("Matrix(*)")) {
                    operators.add(BinaryOperator.MUL_MAT_OPERATOR);
                }

            }
        }

        if (operators.size() == 0) {
            // if there isn't any operator, the parameter is just the number of
            // output
            // operator list is * by default

            for (int i = 1; i <= Integer.valueOf(operatorStr); i++) {
                if (mulType.equals("Element-wise(.*)")) {
                    operators.add(BinaryOperator.MUL_OPERATOR);
                } else if (mulType.equals("Matrix(*)")) {
                    operators.add(BinaryOperator.MUL_MAT_OPERATOR);
                }
            }

        }

        return operators;
    }    
}