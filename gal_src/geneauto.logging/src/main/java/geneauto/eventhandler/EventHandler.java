/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/eventhandler/EventHandler.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-07 12:23:09 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.eventhandler;

import geneauto.utils.FileUtil;
import geneauto.utils.TimeUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Class to handle messages according to their event level. As there is only one
 * log file for a geneauto execution, this class is a singleton.
 * 
 */
public class EventHandler {

	/**
	 * Log level. For selecting the desired log level. Can be adjusted by
	 * command line argument -l <levelName> NB! To be removed for verification!
	 * Also the handle method should be changed accordingly.
	 */
	private static EventLevel logLevel = EventLevel.INFO;

	/**
	 * Object which is useful to write the messages into the file.
	 */
	private static FileWriter writer;

	/**
	 * Character corresponding to the lineSeparator.
	 */
	private static String lineSeparator = (String) java.security.AccessController
			.doPrivileged(new sun.security.action.GetPropertyAction(
					"line.separator"));

	/**
	 * Tool name to be included in messages
	 */
	private static String toolName = "";

	/**
	 * Last exception (temporary buffer to pass exception information between
	 * different methods). The value must be null for most of the time
	 */
	private static Exception lastException;
	
	/**
	 * Max number of errors before tool stops
	 * if errorCnt reaches this level, each error is handled as critical error
	 */
	private static int maxErrorCnt = 20;
	
	/**
	 * Global error count 
	 */
	private static int errorCnt=0;
	
	/**
	 * A map for calculation execution times
	 * Key of the map is token given when starting the timer and value is 
	 * current time
	 */
	private static Map<String, Long> timerMap = new HashMap<String, Long>();

	/**
	 * Name of the launcher executing the toolset
	 */
	private static String launcherName = "";
	
	public static String getLauncherName() {
		return launcherName;
	}

	public static void setLauncherName(String launcherName) {
		EventHandler.launcherName = launcherName;
	}

	/**
	 * A list for logging execution time summary
	 */
	private static List<String> executionLog = new LinkedList<String>();

	/** Absolute path of the current log file */
	private static String logPath;
	
	/**
	 * Initialisation of the EventsHandler.
	 * 
	 * @param pOutputFile
	 *            The relative or absolute path of the file in which will be
	 *            written the log.
	 * 
	 * @param pLogLevel
	 *            The log level of the EventsHandler. To be remover for
	 *            qualification.
	 * 
	 * @param pResetLog --
	 *            if true, the log file will be deleted when it exists
	 */
	public static void init(String pOutputFile, EventLevel pLogLevel,
			boolean pResetLog) {

		logLevel = pLogLevel;
		
		logPath = new File(pOutputFile).getAbsolutePath();

		writer = FileUtil.openFileWriter(pOutputFile, !pResetLog);

	}

	/**
	 * We close the writer.
	 */
	public static void close() {
		try {
			if (writer != null) {
				writer.flush();
				writer.close();
				writer = null;
			}
		} catch (IOException e) {
			writer = null;
			handle(EventLevel.ERROR, "EventsHandler", "",
					"Failed to close log file.", "");
		}
	}

	/**
	 * Process messages according to their event level.
	 * 
	 * @param level
	 *            Criticality level of the event.
	 * @param location
	 *            String identifying where the event comes from.
	 * @param code
	 *            Message code.
	 * @param time
	 *            Time when the message was issued. Required for copying the log from the BlockSequencer.exe tool.
	 * @param message
	 *            Message to be handled.
	 * @param exceptionMessage
	 *            Detailed description of the exception
	 * 
	 */
	public static void handle(EventLevel level, String location, String code, String time,
			String message, String exceptionMessage) {

		// skip messages that are below the desired logging level
		if (level.ordinal() < logLevel.ordinal()) {
			return;
		}
		
		// message to logfile
		String toLog = "[" + level + "][" 
						+ compileLocString(location) 
						+ "] (" + code + ") ";
		// time (taken from argument to allow tools to buffer messages)
		toLog += time;
		// prefix all lines with log level
		toLog += " " + message.replace("\n", lineSeparator + "[" + level + "]\t");

		// add information about the exception 
		if (lastException != null){
			exceptionMessage += lineSeparator
					+ lastException.getClass().toString()
					+ ((lastException.getMessage() != null) ? lineSeparator
							+ lastException.getMessage() : "");

			toLog += lineSeparator
				+ "["
				+ level
				+ "] "
				+ exceptionMessage.replace("\n", lineSeparator + "[" + level
						+ "]\t");
		}
		
		toLog += lineSeparator;

		// messages to be written on console (errors to std.err, others to
		// std.out))
		String toConsole = "[" + level + "][" 
						+ compileLocString(location)
						+ "] (" + code + ") ";
		// add the log message, prefix the line with space
		toConsole += message.replace("\n", lineSeparator + "  ");
		
		if (level.equals(EventLevel.CRITICAL_ERROR)
				|| level.equals(EventLevel.ERROR)) {
			// errors to System.err 
			errorCnt++; 
			System.err.println(toConsole + "\n" + exceptionMessage);
			System.err.flush();
		} else if (level != EventLevel.DEBUG) {
			// information messages to System.stdout (except DEBUG)
			System.out.println(toConsole);
			System.out.flush();
		}

		// write message to log file also
		logMessage(toLog);

		// print the stack trace if we have an exception
		if (lastException != null) {
			lastException.printStackTrace();
			lastException = null;
		}

		// check error treshold
		if (errorCnt >= maxErrorCnt){
			level = EventLevel.CRITICAL_ERROR;
			System.err.println("Too many errors (" + errorCnt + ")! The tool will stop now.");
			System.err.flush();
		}
			
		// exit the tool when the error level is critical
		if (level.equals(EventLevel.CRITICAL_ERROR)) {
			System.exit(1);
		}
	}

    /**
     * Process messages according to their event level.
     * 
     * @param level
     *            Criticality level of the event.
     * @param location
     *            String identifying where the event comes from.
     * @param code
     *            Message code.
     * @param message
     *            Message to be handled.
     */
    public static void handle(EventLevel level, String location, String code,
            String message) {
        handle(level, location, code, getDateTimeForLog(), message, "");
    }
    
    /**
     * Process messages according to their event level.
     * 
     * @param level
     *            Criticality level of the event.
     * @param location
     *            String identifying where the event comes from.
     * @param code
     *            Message code.
     * @param message
     *            Message to be handled.
     * @param exceptionMessage
     *            Detailed description of the exception.
     * 
     */
    public static void handle(EventLevel level, String location, String code,
            String message, String exceptionMessage) {
        handle(level, location, code, getDateTimeForLog(), message, exceptionMessage);
    }
    
	/**
	 * Process messages according to their event level.
	 * 
	 * @param level
	 *            Criticality level of the event.
	 * @param location
	 *            String identifying where the event comes from.
	 * @param code
	 *            Message code.
	 * @param message
	 *            Message to be handled.
	 * @param exception
	 *            catched exception for printing the stack trace
	 * 
	 */
	public static void handle(EventLevel level, String location, String code,
			String message, Exception exception) {
		lastException = exception;
		handle(level, location, code, message, "");
	}

	/**
	 * returns date time for displaying in user messages
	 * 
	 * @return String date string
	 */
	public static String getDateTimeForMessage() {
		return TimeUtils.getFormattedDate("dd/MM/yyyy HH:mm:ss.SSS");
	}

	/**
	 * returns date and time for displaying in log file
	 * 
	 * @return String date string
	 */
	public static String getDateTimeForLog() {
		return TimeUtils.getFormattedDate("yy-MM-dd HH:mm:ss.SSS");
	}

	/**
	 * Writes the message into the log file.
	 * 
	 * @param message
	 *            The message to be logged into the log file.
	 */
	private static void logMessage(String message) {
		if (writer != null) {
			try {
				writer.write(message);
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Close all before garbage is collected.
	 */
	public void finalize() throws Throwable {
		close();
		super.finalize();
	}

	/**
	 * @return error status
	 */
	public static boolean getAnErrorHasOccured() {
		return errorCnt > 0;
	}

	/**
	 * If any errors have occurred so far, then stop.
	 * @param locationRef Reference to the location that calls this check
	 */
	public static void stopOnError(String locationRef) {
		if (EventHandler.getAnErrorHasOccured()) {
			// Check, if any errors have occurred so far
			if (getAnErrorHasOccured()) {
				handle(EventLevel.CRITICAL_ERROR, locationRef, "", 
					"Errors have occured during the tool's execution. Can't continue.");
			}
		}
	}

	/**
	 * Adds tool name to the location string passed with the message
	 * 
	 * @return String toolName
	 */
	public static String compileLocString(String location) {
		if (toolName.isEmpty() && location.isEmpty())
			return "";

		if (toolName.isEmpty() || toolName.equals(location))
			return location;

		if (location.isEmpty())
			return toolName;

		return toolName + "." + location;
	}

	public static void setToolName(String p_toolName) {
		toolName = p_toolName;
	}

	public static String getToolName() {
		return toolName;
	}

	public static EventLevel getLogLevel() {
		return logLevel;
	}

	public static String getLogPath() {
		return logPath;
	}

	/**
	 * records current time in timer map
	 * @param tag
	 */
	public static void startTimer(String tag) {
		Date now = new Date();
		timerMap.put(tag, now.getTime());
	}

	public static String getDuration(String tag, boolean logMessage) {
		long startTime = timerMap.get(tag);
		Date endTime = new Date();
	
		Time duration_t = new Time(endTime.getTime() - startTime);
		
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		String timeStr = format.format(duration_t);
		
		// store the time message for execution summary
		if (logMessage) {
			executionLog.add(toTabRow(tag + ":", timeStr, 28));
		}
		
		return timeStr;
	}
	
	public static void printTimerSummary() {
		for (String msg : executionLog) {
			EventHandler.handle(EventLevel.INFO, "", 
					"GI000?",
					msg, 
					"");
		}
	}
	
	/**
	 * Takes two strings and formats them as tab row using spaces
	 * 
	 * @param col1	string to be printed in first column
	 * @param col2	string to be printed in second column
	 * @param colSize	number of characters in first column
	 * @return string containing col1 and col2 separated by appropriate amount
	 * of spaces 
	 * 
	 * TODO: find more optimised way to repeat spaces
	 */
	public static String toTabRow(String col1, String col2, int colSize) {
		String tabRow = col1;
		int spaceCnt = colSize - col1.length();
		if (spaceCnt < 0) {
			tabRow += " ";
		}
		else {
			for (int i = 0; i < spaceCnt; i++) {
				tabRow += " ";
			}
		}
	
		tabRow += col2;
		
		return tabRow;
	}
}
