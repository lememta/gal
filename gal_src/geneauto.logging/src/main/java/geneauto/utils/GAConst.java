/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/GAConst.java,v $
 *  @version	$Revision: 1.47 $
 *	@date		$Date: 2012-02-05 20:56:43 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils;

public interface GAConst {

	/** 
	 * Main Argument Constant Strings
	 * It is important to use the constant not hard-coded strings as some of 
	 * the parameters are implicitly processed by ArgumentReader and they must be same
	 * throughout all tools
	 * **/
	// first input
	public final static String ARG_INPUTFILE = "inputFilePath";
	public final static String ARG_INPUTFILE_NAME_ROOT = "inputFile";	
	public final static String ARG_INPUTFILE_NAME = "inputFileName";	
	public final static String ARG_INPUTFILE_DIR = "inputFileDirectory";
	
	// system model input in case it is second input file
	public final static String ARG_INPUTSMFILE = "inputSMFilePath";
	public final static String ARG_INPUTSMFILE_NAME_ROOT = "inputSMFile";	
	public final static String ARG_INPUTSMFILE_NAME = "inputSMFileName";	
	public final static String ARG_INPUTSMFILE_DIR = "inputSMFileDirectory";
	
	// code model input in case it is second input file
	public final static String ARG_INPUTCMFILE = "inputCMFilePath";
	public final static String ARG_INPUTCMFILE_NAME_ROOT = "inputCMFile";	
	public final static String ARG_INPUTCMFILE_NAME = "inputCMFileName";	
	public final static String ARG_INPUTCMFILE_DIR = "inputCMFileDirectory";

	// primary output file (used in case there is only one output file)
	public final static String ARG_OUTPUTFILE = "outputFilePath";
	public final static String ARG_OUTPUTFILE_DIR = "outputFileDirectory";
	public final static String ARG_OUTPUTFILE_NAME_ROOT = "outputFile";
	public final static String ARG_OUTPUTFILE_NAME = "outputFileName";

	// code model output file
	public final static String ARG_OUTPUTCMFILE = "outputCMFilePath";
	public final static String ARG_OUTPUTCMFILE_DIR = "outputCMFileDirectory";
	public final static String ARG_OUTPUTCMFILE_NAME_ROOT = "outputCMFile";
	public final static String ARG_OUTPUTCMFILE_NAME = "outputCMFileName";

	// system model output file
	public final static String ARG_OUTPUTSMFILE = "outputSMFilePath";
	public final static String ARG_OUTPUTSMFILE_DIR = "outputSMFileDirectory";
	public final static String ARG_OUTPUTSMFILE_NAME_ROOT = "outputSMFile";
	public final static String ARG_OUTPUTSMFILE_NAME = "outputSMFileName";

	public final static String ARG_OUTPUTFOLDER = "outputPath";
	// Note: The following parameter is reserved for the toolset level only!
	public final static String ARG_OUTPUTFOLDER_MODE = "outputPathMode";
	
	public final static String ARG_MODELLIBFOLDER = "externalLibFolder";
	
	// ***** START: flag "--inline" ******
	// inline flag
	public final static String ARG_INLINE = "inline";
	// ***** END: flag "--inline" ******
	
	// ***** START: flag "--global-io" ******
	// inline flag
	public final static String ARG_GLOBAL_IO = "global-io";
	// ***** END: flag "--global-io" ******
	
	// user-defined blocklibrary
	public final static String ARG_LIBFILE = "blockLibraryFilePath";
	public final static String ARG_LIBFILE_NAME_ROOT = "blockLibraryFile";
	public final static String ARG_LIBFILE_DIR = "blockLibraryFileDirectory";
		
	// default blocklibrary
	public final static String ARG_DEFLIBFILE = "defBlockLibraryFilePath";
	public final static String ARG_DEFLIBFILE_NAME_ROOT = "defBlockLibraryFile";
	public final static String ARG_DEFLIBFILE_DIR = "defBlockLibraryFileDirectory";
	
	// Junction functions
	public final static String ARG_JUNCTION_FUNS = "junctionFunctions";
	
	/**
	 * Default block library file. The path of this file will be resolved
	 * against the location of program file
	 */
	static String DEFAULT_BLOCKLIB = "blocklibrary.gbl.xml";
	// location of the block library file in source distribution
	// TODO : MaPa, 20080804, probably a bad idea as there will be several galauncher 
	// depending on the toolchain and they may all have different block libraries (that's
	// not the case with the Coq sequencer but it will surely be the case for optimiser).
	
	static String DEFAULT_BLOCKLIB_DIR = "../geneauto.galauncher/";
	
	static String DEFAULT_SRCLIB_ROOT  = "../geneauto.galauncher/";
	
	public final static String ARG_LOGLVL = "logLevel";
	
    // Debugging 
    public final static String ARG_DEBUG = "debugMode";
    public final static String ARG_TEMP_MODEL = "tempModel";
    public final static String TEMP_MODEL_NONE = "none";
    public final static String TEMP_MODEL_ALL = "all";
    public final static String TEMP_MODEL_EXCHANGE = "exchange";
	
	// argument name for temporary folder
	public final static String ARG_TMPFOLDER = "tmpPath";
	
	public final static String ARG_CONSTANTSFILE = "constantFilePath";
	public final static String ARG_CONSTANTSFILE_NAME_ROOT = "constantFile";
	
	public final static String LOG_FILE_NAME = "geneauto.log.txt";
	
	/** Extension Constant Strings (no leading dot!) **/
	public final static String EXT_GSM = "gsm.xml";
	public final static String EXT_GCM = "gcm.xml";
	public final static String EXT_GBL = "gbl.xml";
	
	// temporary folder
	public final static String TMP_DIR = "tmp";
	public final static String OUTPUTFOLDER_SUFFIX = "_ga";

	/** default prefix for temporary variables (loop counters etc) **/
	public final static String TMP_VAR_PREFIX = "i";
	
    // Functions for code generation. Cannot be put to the FMCMG, 
    // since they are required in the Models package
    public final static String FUN_FALLING = "falling";
    public final static String FUN_RISING  = "rising";

    /** prefixes and suffixes for code model elemenst */
	// suffix for init function
    public final static String FUNC_INIT_SUFFIX = "_init";
    // suffix for compute function
    public final static String FUNC_COMP_SUFFIX = "_compute";
	// suffix for the parameters module
    public final static String MODULE_PARAMS_SUFFIX = "_param";
    // prefix added to input port number when port name is not defined
    public final static String VAR_INPUT_PREFIX = "i_";
    // prefix added to output port number when port name is not defined
    public final static String VAR_OUTPUT_PREFIX = "o_";
    
    // prefix for IO structure data type
    public final static String STRUCT_ARG_PREFIX = "t_";
    // suffix for IO structure data type
    public final static String STRUCT_ARG_SUFFIX = "_io";
    // prefix for data type names of local variables
    public final static String STRUCT_LOCAL_PREFIX = "t_";
    // suffix for data type names of local variables
    public final static String STRUCT_LOCAL_SUFFIX = "_loc";
    // prefix for memories structure data type
    public final static String STRUCT_MEM_PREFIX = "t_";
    // suffix for memories structure data type
    public final static String STRUCT_MEM_SUFFIX = "_state";
    
    public final static String STRUCT_BUS = "Bus_";
    // suffix for function argument local variable and dataype 
    public final static String VAR_ARG_SUFFIX = "_io";
    // name for function argument 
    public final static String VAR_ARG_NAME = "_io_";
    // name for function argument with memories 
    public final static String VAR_MEM_NAME = "_state_";
    // prefix for local variables
    public final static String VAR_LOCAL_PREFIX = "_";
    // suffix for local variables
    public final static String VAR_LOCAL_SUFFIX = "_loc";
    public final static String STATEMENT_INIT_SUFFIX = "_iCStat";
    public final static String STATEMENT_COMP_SUFFIX = "_cCStat";
    
    // Modules
    public final static String MODULE_COMMON = "GACommon";

    // Reports
    public final static String MODEL_METRICS_FILE = "model_metrics.txt";
    
    // version information
    // NB! This must be updated each time a new version number is set
    // in the pom files
    public final static String TOOLSET_VER = "2.4.10";

    // version date
    // NB! This must be updated each time a release is compiled
    public final static String TOOLSET_DATE = "17/09/2012 (SNAPSHOT)";

    public final static String LICENSE_MESSAGE = "Gene-Auto toolset  " +
    		"Copyright (C) 2006-2012  IB Krates OU, Alyotech, FERIA/IRIT, " +
    		"Continental SAS" +
    		"\n This program comes with ABSOLUTELY NO WARRANTY; " +
    		"\n This is free software, and you are welcome to redistribute it" +
    		" under certain conditions;" +
    		"\n For licensing details see license.txt in the root " +
    		"folder of the program.\n";
    
    // Suffixes for intermediate model files
    // Naming rule: 4 characters: 
    // 		2 chars for input model 
    // 		2 chars for elementary tool (2 from beginning or first chars of a 2-part name)
	public static final String SUFFIX_SLIM = "slim";
	public static final String SUFFIX_FMPR = "fmpr";
	public static final String SUFFIX_SFIM = "sfim";
	public static final String SUFFIX_FMBS = "fmbs";
	public static final String SUFFIX_FMTY = "fmty";
	public static final String SUFFIX_FMCG = "fmcg";
	public static final String SUFFIX_SMPR = "smpr";
	public static final String SUFFIX_SMLUSPR = "smluspr";
	public static final String SUFFIX_SMCG = "smcg";
	public static final String SUFFIX_SMPO = "smpo";
	public static final String SUFFIX_CMOP = "cmop";
	public static final String SUFFIX_CMPR = "cmpr";

}
