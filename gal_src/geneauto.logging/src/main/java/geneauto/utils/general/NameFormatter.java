package geneauto.utils.general;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility to format normalise identifier names
 * This class performs basic formatting.
 *  
 * The Formatter of each printer tool will call methods 
 * from this basic class and may add additional rules  
 *
 */
public class NameFormatter {
    /**
     * Tests if input string is legal output language identifier
     * 	- name starts with a letter or underscore
     *  - name is composed of only letters, numbers and underscores
     * 
     * @param s -- string to test
     * @return boolean
     */
    public static boolean isLegalIdentifier(String s){
    	Pattern p = Pattern.compile("[_a-zA-Z][0-9._a-zA-Z]*");
        Matcher matcher = p.matcher(s);
        return matcher.matches();
    }
	
    /**
     * Takes identifier name as input and prefixes the name with "_"
     * in case it starts with a number. 

     * @param s -- string to to be checked 
     * @return updated string
     */
    
    public static String prefixLeadingNumber(String s){
    	if(s.matches("[0-9][0-9._a-zA-Z]*")) {
    		s = "_" + s;
    	}
    	
    	return s;
    }

    /**
     * Normalises function name:
     * 	- replaces illegal symbols with _
     *  - reports error when name starts with a number
     *  
     * @param function that needs to be normalised
     * @return String -- normalised names
     */
    public static String normaliseFunctionName(String name, String ref){
    	// remove newlines
    	name = name.replace("\\n", "_");
        
        // accepted symbols for function name
        Pattern p = Pattern.compile("[^0-9_a-zA-Z]");
        Matcher matcher = p.matcher(name);
        name = matcher.replaceAll("_");
    	
        // if identifier starts with a number, prefix it
        name = prefixLeadingNumber(name);
        
    	if (!isLegalIdentifier(name)){
    		EventHandler.handle(EventLevel.ERROR, 
    				"normaliseFunctionName", 
    				"CP0084", 
    				"Illegal name for a function: \"" 
    				+ name + "\" in "
    				+ ref, 
    				"");
    		return "";
    	}
    	
    	return name;
    }
    
    /**
     * Normalises variable name:
     * 	- replaces illegal symbols with _
     *  - reports error when name starts with a number
     *  
     * @param name -- element name to be normalised
     * @param ref -- reference string to be used in error messages
     * @param noStructures	 -- tells the function that "." shall not be allowed 
     * 
     *  
     * @return String -- normalised name
     */
    public static String normaliseVariableName(String name, String ref, 
    		boolean noStructures){
    	// remove newlines
    	name = name.replace("\\n", "_");
        
        // accepted symbols for variable/type name
    	Pattern p;
    	if (noStructures) {
    		p = Pattern.compile("[^0-9_a-zA-Z]");
    	} else {
    		p = Pattern.compile("[^0-9._a-zA-Z]");
    	}
        Matcher matcher = p.matcher(name);
        name = matcher.replaceAll("_");
        
        // if identifier starts with a number, prefix it
        name = prefixLeadingNumber(name);

    	if (!isLegalIdentifier(name)){
    		EventHandler.handle(EventLevel.ERROR, 
    				"normaliseVariableName", 
    				"GEM0085", 
    				"Illegal name for a variable: \"" 
    				+ name + "\" in "
    				+ ref, 
    				"");
    		return "";
    	}
    	
    	return name;
    }

    /**
     * Normalises file name:
     * 	- replaces illegal symbols with _
     *  
     * @param string that needs to be normalised
     * @return String -- normalised name
     */
    public static String normaliseFileName(String name){
    	// remove newlines
    	name = name.replace("\\n", "_");
        
        // accepted symbols for variable/type name
        Pattern p = Pattern.compile("[^0-9._a-zA-Z]");
        Matcher matcher = p.matcher(name);
        name = matcher.replaceAll("_");
        
        // if identifier starts with a number, prefix it
        name = prefixLeadingNumber(name);
    	
    	if (!isLegalIdentifier(name)){
    		EventHandler.handle(EventLevel.ERROR, 
    				"normaliseVariableName", 
    				"CP0083", 
    				"Illegal name for a file: \""
    				+ name + "\"", 
    				"");
    		return null;
    	}
    	
    	return name;
    }
    
    /**
     * Normalises name with points
     * 
     * @param name
     * @return name's sub string from last point to the end
     */
    public static String normaliseNameWithPoints(String name) {
        if (name != null && name.contains(".")) {
            name = name.substring(
                    name.lastIndexOf(".") + 1);
        } 
        return name;
    }
}
