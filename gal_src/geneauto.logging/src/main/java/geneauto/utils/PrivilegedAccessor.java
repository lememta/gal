/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/PrivilegedAccessor.java,v $
 *  @version	$Revision: 1.20 $
 *	@date		$Date: 2012-02-19 19:19:39 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.tuple.Pair;
import geneauto.utils.tuple.Triple;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class which role is to provide generic accessories to private or protected
 * attributes of objects. This class is as generic as possible, and thus is
 * totally independent from to input or output model. <br>
 * It simply allows to set or get the attribute of an object given the name of
 * the attribute and the name of the object. It also provides useful methods for
 * the model reading and writing.
 * 
 */
public final class PrivilegedAccessor {

    @SuppressWarnings("unchecked")
	private static HashMap<Class, List<Field>> fieldsMap = 
											new HashMap<Class, List<Field>>();

    /**
     * Returns true if the instance contains an attribute which name is name.
     * 
     * @param className
     *            The class' name.
     */
    @SuppressWarnings("unchecked")
    public static boolean isAttributeOfClass(String attrName, Class paramClass) {
        for (Field f : getClassFields(paramClass)) {
            if (attrName.equals(f.getName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Browses the model, checks the presence, in the attributes of object
     * parent's attributes, of attribute named attrName. If such an attribute is
     * found, then we return it.
     * 
     * @param attrName
     *            the name of the attribute we are looking for.
     * @param parent
     *            the object that we browse.
     * @return the attribute if it is found.
     */
    public static Object isAttributeOfAttribute(String attrName, Object parent) {
        for (Field f : getClassFields(parent.getClass())) {
            Object attribute = getValue(parent, f.getName());
            if (attribute != null) {
                List<Field> attributesAttributes = getClassFields(attribute
                        .getClass());

                for (Field childAttr : attributesAttributes) {
                    if (childAttr.getName().equals(attrName)) {
                        return attribute;
                    }
                }
            }

        }

        return null;
    }

    /**
     * Returns an object representing a class (different from an instance of
     * class).
     * 
     * @param className
     *            The class' name.
     */
    @SuppressWarnings("unchecked")
    public static Class getClassForName(String className) {
        Class result = null;
        try {
            result = Class.forName(className);
        } catch (ClassNotFoundException e) {
            EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor", "",
                    "The class " + className + " has not been found.", e
                            .getMessage());
        }
        return result;
    }

    /**
     * Allows to access a field of a class, whatever its visibility is. It can
     * be a method, an attribute... and it can be private, protected, or public.
     * 
     * @param thisClass
     *            Class from which we want to get the field.
     * @param fieldName
     *            Name of the Field we want to get.
     * @return The Field "fieldName" of the class "parentclass".
     */
    public static Field getField(final Class<?> parentclass,
            final String fieldName) {
        if (parentclass != null) {
            try {
                return parentclass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                // if not found, we try to get the field from the class'
                // ancestors.
                return getField(parentclass.getSuperclass(), fieldName);
            }
        }
        return null;
    }

    /**
	 * Searches all fields of type List in the parentObj for an occurrence of
	 * childObj. If found returns the name of the field, reference to the list
	 * and the position of childObj in it.
	 * 
	 * @param obj
	 * @return list of fields and objects 
	 */
	@SuppressWarnings("unchecked")
	public static List<Pair<Field, Object>> getFieldsAndObjects(Object obj) {
		
		final String methodName = "getFieldsAndObjects()";
		
		List<Pair<Field, Object>> lst = new ArrayList<Pair<Field,Object>>(); 
	
		// Scan the fields of the object class 
		
		for (Field f : PrivilegedAccessor.getClassFields(obj.getClass())) {			
			try {
				f.setAccessible(true);				
				Object value = f.get(obj);
				Pair<Field, Object> pair = new Pair<Field, Object>(f, value);
				lst.add(pair);
	
			} catch (IllegalArgumentException e) {
				EventHandler.handle(EventLevel.ERROR, methodName, "",
						"Unable to access field " + f.getName() + " of object of class: " 
						+ obj.getClass().getCanonicalName(), 
						e.getMessage());
			} catch (IllegalAccessException e) {
				EventHandler.handle(EventLevel.ERROR, methodName, "",
						"Unable to access field " + f.getName() + " of object of class: " 
						+ obj.getClass().getCanonicalName(), 
						e.getMessage());
			}
		}
		return lst;
	}

	/**
     * Returns all the fields of the object.
     */
    @SuppressWarnings("unchecked")
    public static List<Field> getClassFields(final Class classe) {

        List<Field> result = fieldsMap.get(classe);

        if (result == null) {
            
            result =  new ArrayList<Field>();
            
            try {
                if (!classe.equals(Object.class)) {
                    Field[] fields = classe.getDeclaredFields();
                    for (int i = 0; i < fields.length; i++) {
                        if (fields[i].getName().matches("\\$SWITCH_TABLE\\$.*")) {
                            // Ignore encoded SWITCH_TABLE -s
                            // Apparently, Java adds such extra fields, when some
                            // field has enumerator type and is used as a switch
                            // condition somewhere in the code.
                        } else {
                            result.add(fields[i]);
                        }
                    }
                    result.addAll(getClassFields(classe.getSuperclass()));
                }
            } catch (SecurityException e) {
                EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor",
                        "",
                        "A problem occured while trying to retrieve fields of class "
                                + classe, e.getMessage());
            }
            
            fieldsMap.put(classe, result);
            
        }

        return result;
    }

    /**
     * Allows to access the real class of a given field. It is useful when
     * manipulating abstract objects.
     * 
     * @param field
     *            The field from which we want to get the real Class.
     * @return an Object of type Class, corresponding to the real type of the
     *         field.
     */
    @SuppressWarnings("unchecked")
    public static Class getFieldType(final Field field) {
        Class result = null;
        if (field != null) {
            field.setAccessible(true);
            result = field.getType();
        }
        return result;
    }

	/**
	 * If a getter method with a standard name "get" + fieldName (case
	 * insensitive) and 0 arguments is present, then sets returns the 
	 * method and sets it accessible. Otherwise, returns null.
	 * 
	 * @param parentclass
	 *            Class from which we want to get the field.
	 * @param fieldName
	 *            Name of the Field we want to get.
	 * @return The Field "fieldName" of the class "parentclass".
	 */
    public static Method getFieldGetter(final Class<?> parentclass,
            final String fieldName) {
        if (parentclass != null) {
        	String getterName = "get" + fieldName;
        	// Check the current class (does not cover inherited methods or interfaces)
            for (Method m : parentclass.getDeclaredMethods()) {
				if (m.getName().equalsIgnoreCase(getterName)
						&& m.getParameterTypes().length == 0) {
					m.setAccessible(true);
					return m;
				}
            }
        	// Check the superclass
            Class<?> sClass = parentclass.getSuperclass();
            if (sClass != null) {
            	return getFieldGetter(sClass, fieldName);
            } 
            return null;
        }
        return null;
    }

	/**
	 * Searches for a given method in the class or its super classes.
	 * 
	 * NOTE The first method with a matching name is returned. In Java 
	 * there can be multiple methods with same name and different signatures.
	 * This is ignored.  
	 * 
	 * @param cls
	 *            Class from which we want to get the method.
	 * @param methodName
	 *            Name of the Field we want to get.
	 * @return The method, if it is found. Null otherwise.
	 */
    public static Method getClassMethod(final Class<?> cls,
            final String methodName) {
        if (cls != null) {        	
        	// Check the current class (does not cover inherited methods or interfaces)
            for (Method m : cls.getDeclaredMethods()) {
				if (m.getName().equalsIgnoreCase(methodName)) {
					m.setAccessible(true);
					return m;
				}
            }
        	// Check the superclass
            Class<?> sClass = cls.getSuperclass();
            if (sClass != null) {
            	return getClassMethod(sClass, methodName);
            } 
            return null;
        }
        return null;
    }

	/**
	 * If a setter method with a standard name "set" + fieldName (case
	 * insensitive) and 1 argument is present, then returns the 
	 * method and sets it accessible. Otherwise, returns null.
	 * 
	 * @param parentclass
	 *            Class on which we want to set a field.
	 * @param fieldName
	 *            Name of the Field we want to set.
	 * @return The Field "fieldName" of the class "parentclass".
	 */
    public static Method getFieldSetter(final Class<?> parentclass,
            final String fieldName) {
        if (parentclass != null) {
        	String getterName = "set" + fieldName;
        	// Check the current class (does not cover inherited methods or interfaces)
            for (Method m : parentclass.getDeclaredMethods()) {
				if (m.getName().equalsIgnoreCase(getterName)
						&& m.getParameterTypes().length == 1) {
					m.setAccessible(true);
					return m;
				}
            }
        	// Check the superclass
            Class<?> sClass = parentclass.getSuperclass();
            if (sClass != null) {
            	return getFieldSetter(sClass, fieldName);
            } 
            return null;
        }
        return null;
    }

    /**
     * Returns an instance of the class which name is given as a parameter.
     * 
     * @param className
     *            The name of the class from which we wish to get an instance.
     */
    @SuppressWarnings("unchecked")
    public static Object getObjectForClassName(String className) {
        Object result = null;
        if (className != null) {
            try {
            	
                Class tmp = Class.forName(className);
                
                if (tmp != null) {
                    if (tmp.isEnum()) {
                        result = tmp.getEnumConstants();
                    } else {
                    	try {
							result = tmp.newInstance();
						} catch (IllegalAccessException e) {
							// Try to obtain the default constructor, set it accessible and invoke
							try {
								Constructor c = tmp.getDeclaredConstructor((Class[]) null);
								c.setAccessible(true);
								result = c.newInstance((Object[]) null);
							} catch (SecurityException e1) {
				                EventHandler
		                        .handle(EventLevel.ERROR, "PrivilegedAccessor", "",
		                                "Unable to create an instance. Cannot access class. Class "
		                                        + className, e1);
							} catch (NoSuchMethodException e1) {
				                EventHandler
		                        .handle(EventLevel.ERROR, "PrivilegedAccessor", "",
		                                "Unable to create an instance. No default constructor. Class "
		                                        + className, e1);
							} catch (Exception e1) {
				                EventHandler
		                        .handle(EventLevel.ERROR, "PrivilegedAccessor", "",
		                                "Unable to create an instance. Unable to invoke the default constructor. Class "
		                                        + className, e1);
							}
						}
                    }
                }
            } catch (ClassNotFoundException e) {
                EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor",
                        "", "Unable to find the class " + className, e);
            } catch (InstantiationException e) {
                EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor",
                        "", "Unable to instantiate an Object of class "
                                + className, e);
            }
        }
        return result;
    }

    /**
     * Returns an instance of class targetClass in the same root package as 
     * the packageTeplate object.
     * 
     * For example the targetClass is geneauto.models.gadatatypes.TBoolean
     * An instance of this class is constructed by 
     * geneauto.tcprinter.gacodemodel.expression.CallExpression so we want 
     * the instance the TBoolean object to instantiate corresponding class
     * from TCPrinter package too. In given example this method shall combine 
     * class name of two first package names of packageTemplate object class
     * name followed by the rest of the package path of the targetClass
     * 	so an object of type geneauto.tcprinter.gadatatypes.TBoolean shall be
     * returned.
     * 
     * The method is useful in scenarios when a method of class that is 
     * potentially overridden by class in printer package wants to ensure, that
     * even if this method is not overridden, it still instantiates 
     * classes from the package where the class of instantiator resides
     * 
     * Typical usage:
     * 		this.setDataType(TBoolean.class, this);
     *  
     * @param targetClass -- class name of the target object. first two 
     * 							package names in this class name shall be 
     * 							replaced with those from package template  
     * @param packageTemplate -- a template object providing the root of the 
     * 							class package hierarchy
     * @return -- an object with the type combined from the package prefix of
     * 				package template and the rest of the class name path from 
     * 				target class
     * 			  In case of an error throws CRITICAL_ERROR and returns null 
     * 				 
     */
    @SuppressWarnings("unchecked")
	public static Object getObjectInPackage(Class targetClass, 
    												Object packageTemplate) {
    	String targetClassName = targetClass.getCanonicalName();
        String packageName = packageTemplate.getClass().getCanonicalName();
        
        String patternString = "^(geneauto\\.[^\\.]+).*$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher m = pattern.matcher(packageName);
        Object res = null;
        if (m.find()) {
            
            // two first package names of packageTemplate
            String targerPackageStart = m.group(1);
            pattern = Pattern.compile(patternString);
            m = pattern.matcher(targetClassName);
            if (m.find()) {
                String toReplace = m.group(1);
                try {
                    int index = targetClassName.indexOf(toReplace) + toReplace.length();
                    
                    // replace two first package names of targetClass by 
                    // two first package names of packageTemplate
                    String className = targerPackageStart + targetClassName.substring(index);
                    res = getObjectForClassName(className);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (res == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, 
                    "getObjectInPackage", "", 
                    "Unable to return object of class \"" 
                    + targetClass.getSimpleName()
                    + "\" using package template \"" 
                    + packageTemplate.getClass().getCanonicalName() + "\"");
                    ;
        }
        return res;
    }
    
    /**
     * Returns the Object objectValue casted as an instance 
     * of realType, if such casting is legal.
     * 
     * @param objectValue
     *            The object we want to be casted.
     * @param realType
     *            The new real class of the Object
     */
    @SuppressWarnings("unchecked")
    public static Object getTypedObject(Object objectValue, Class realType) {
        try {
            // Try to cast the object
            Object result = null;
            result = realType.cast(objectValue);
            return result;
        } catch (Exception e) {
            // Casting failed, analyse the value as String
            String stVal;
            if (objectValue instanceof String) {
                stVal = (String) objectValue;
            } else {
                stVal = objectValue.toString();
            }
            if (realType.getName().equals("int")) {
                int i = (new Integer(stVal)).intValue();
                return (i);
            } else if (realType.getName().equals("double")) {
                double i = (new Double(stVal)).doubleValue();
                return (i);
            } else if (realType.getName().equals("boolean")) {
                boolean b = (new Boolean(stVal)).booleanValue();
                return (b);
            } else {
                String objRef = objectValue.toString();
                String msg = "Cannot cast object " + objRef
                    + "\nof type: " + objectValue.getClass().getName() 
                    + "\nto type: " + realType.getName(); 
                EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor.getTypedObject()", "", msg);
                return null;
            }
        }
    }

    /**
	 * Get the value of the field given as a parameter. If the given field is
	 * not found in the supplied instance, then raises an error. See getValue
	 * with an extra argument to ignore this, if required.
	 * 
	 * @param instance
	 *            The instance of the object from which we want to get the value
	 *            of field fieldName.
	 * @param fieldName
	 *            The name of the field we want to access.
	 * @return The value of the field fieldName of object instance.
	 */
    public static Object getValue(final Object instance, final String fieldName) {
        return getValue(instance, fieldName, false);
    }

    /**
	 * Get the value of the field given as a parameter.
	 * 
	 * @param instance
	 *            The instance of the object from which we want to get the value
	 *            of field fieldName.
	 * @param fieldName
	 *            The name of the field we want to access.
	 * @param ignoreMissing
	 *            If such a field is not found in the supplied instance and this
	 *            parameter is true, then ignores and returns null. Otherwise,
	 *            raises error.
	 * @return The value of the field fieldName of object instance.
	 */
    public static Object getValue(final Object instance, final String fieldName, boolean ignoreMissing) {
        Object result = null;
        try {
            Field field = getField(instance.getClass(), fieldName);
            if (field != null) {
                field.setAccessible(true);
                result = field.get(instance);
            } else {
            	if (ignoreMissing) {
            		return null;
            	} else {
            		EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor", "",
                        "Class " + instance.getClass().getName() 
                        + " does not have a field named " + fieldName);
            	}
            }
        } catch (IllegalArgumentException e) {
            EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor", "",
                    "Unable to access field + " + fieldName + " of object "
                            + instance, e.getMessage());
        } catch (IllegalAccessException e) {
            EventHandler.handle(EventLevel.ERROR, "PrivilegedAccessor", "",
                    "Unable to access field + " + fieldName + " of object "
                            + instance, e.getMessage());
        }
        return result;

    }
    
    /**
	 * Get the value of the field given as a parameter using a getter method.
	 * 
	 * @param instance
	 *            The instance of the object from which we want to get the value
	 * @param getterName
	 *            The name of the getter method.
	 * @return The value returned by the getter method.
	 */
	@SuppressWarnings("unchecked")
	public static Object getValueViaGetter(Object instance, String getterName) {
		Class cls = PrivilegedAccessor.getClassForName(instance
				.getClass().getCanonicalName());
		Method getter = PrivilegedAccessor.getClassMethod(cls, getterName);
		if (getter == null) {
			EventHandler.handle(EventLevel.ERROR,
					"PrivilegedAccessor", "",
					"Cannot obtain a getter method:"
							+ "\n Class " + instance.getClass().getCanonicalName()
							+ "\n Method " + getterName);
			return null;
		} else {
			try {
				getter.setAccessible(true);
				Object val = getter.invoke(instance);
				return val;
			} catch (Exception e) {
				EventHandler.handle(EventLevel.CRITICAL_ERROR,
						"PrivilegedAccessor", "",
						"Cannot invoke method"
								+ "\n Class " + cls.getCanonicalName()
								+ "\n Method " + getterName,
								e);
				return null;
			}
		}
	}


    /**
     * Sets the Value of field "fieldName" of object "instance" to "value"
     * 
     * @param instance
     *            The object which must be modified.
     * @param fieldName
     *            The field of the object which must be modified.
     * @param value
     *            The new value of the field.
     * @param strict
     * 			  When true and the given object doesn't contain such 
     *            field an error is raised. Otherwise, it is ignored
     */
    @SuppressWarnings("unchecked")
    public static boolean setValue(final Object instance,
            final String fieldName, final Object value, boolean strict) {

        boolean success = true;        	
        Field field;
        
        try {
            field = getField(instance.getClass(), fieldName);
            if (strict && field == null) {
                EventHandler.handle(EventLevel.ERROR, 
                      "PrivilegedAccessor.setValue()", "", 
                      "\nClass " + instance.getClass().getCanonicalName() 
                      + " does not have field with name "
                          + fieldName
                          + "\nObject: " + instance);
            }
            field.setAccessible(true);
            
            if (value == null) {
            	field.set(instance, null);
            	return true;
            }
            
            Class classe = field.getType();

            if (Collection.class.isAssignableFrom(classe)
            		&& !(value instanceof Collection)){
                // The type of the field is an instance of Collection
            	// and the supplied value is not -- Try to retrieve the
            	// collection stored in the field and add the value to it.
            	// AnTo 05.08.2011 not sure, if this is a sensible case? 
            	// The field expects a Collection, but it is given a singleton. 
            	if (value != null) {
	            	try {
	            		Collection c = ((Collection) field.get(instance));
	            		if (c != null) {
		            		c.add(value);
	            		} else {
	            			EventHandler.handle(EventLevel.ERROR, 
	                                "PrivilegedAccessor.setValue()", "", 
		                            "\nCollection instance not existing. Cannot add element"
		                          + "\nClass " + instance.getClass().getCanonicalName() 
	                              + "\nField " + fieldName
	                              + "\nObject: " + instance);
	            			success = false;
	            		}
	            	} catch (IllegalArgumentException e) {
	                    success = false;
	                }
            	}
            } else {
                // The type of the field is either not an instance of 
            	// Collection or it is and so is the supplied value
	            if (value.getClass().equals(field.getType())) {
	                field.set(instance, value);
	            } else {
	                Object obj = getTypedObject(value, classe);
	                try {
	                    field.set(instance, obj);
	                } catch (IllegalArgumentException e) {
	                    success = false;
	                }
	            }
            } 
        } catch (Exception e) {
            success = false;
        }
        if (!success) {
            // TODO Raise error here or attribute for conditionally ignoring
            // errors. Failing to set the field value is normally an error.
            // The result code is also almost never checked. It would be better
            // to have error reporting here. However, it cannot be done now, since
            // there appear errors. (AnTo 081205)
//            EventsHandler.handle(EventLevel.ERROR, 
//                    "PrivilegedAccessor.setValue()", "", "\nFailed to set value of field: "
//                        + fieldName + " of object: " + instance + " to: " + value);
        }
        return success;
    }

	/**
	 * Searches all fields of type List in the parentObj for an occurrence of
	 * childObj. If found returns the name of the field, reference to the list
	 * and the position of childObj in it.
	 * 
	 * @param parentObj
	 * @param childObj
	 */
	@SuppressWarnings("unchecked")
	public static Triple<String, List, Integer> searchChildInListFields(Object parentObj,
			Object childObj) {
		
		final String methodName = "searchChildInListFields()";

		// Scan the fields of the parent class and check for occurrence
		// of childObj.
		int i;
		for (Field f : PrivilegedAccessor.getClassFields(parentObj.getClass())) {

			try {
				f.setAccessible(true);
				Object value = f.get(parentObj);
				if (value instanceof List) {
					i = ((List) value).indexOf(childObj);
					if (i > -1) {
						return new Triple<String, List, Integer>(f.getName(), (List) value, i);
					}
				}

			} catch (IllegalArgumentException e) {
				EventHandler.handle(EventLevel.ERROR, methodName, "",
						"Unable to access field " + f.getName() + " of object of class: " 
						+ parentObj.getClass().getCanonicalName(), 
						e.getMessage());
			} catch (IllegalAccessException e) {
				EventHandler.handle(EventLevel.ERROR, methodName, "",
						"Unable to access field " + f.getName() + " of object of class: " 
						+ parentObj.getClass().getCanonicalName(), 
						e.getMessage());
			}
		}
		return null;
	}
	
	/**
     * Returns a copy of the object given as a parameter.
     * 
     * ToNa 080707: NB! We can not rely on generic operation in case of model
     * objects. When making a copy of an object, some of the attributes need to
     * be duplicated whereas for some of them copying a pointer is sufficient
     * 
     * As all CodeModel expressions have now method getCopy, this method is
     * removed from backends as it did not behave correctly in all cases.
     * 
     * @deprecated
     */
    public static Object getCopy(Object objectToDuplicate) {
        Object duplicatedObject = null;
        if (objectToDuplicate != null) {
            duplicatedObject = getObjectForClassName(objectToDuplicate
                    .getClass().getName());
            for (Field field : getClassFields(objectToDuplicate.getClass())) {
                setValue(duplicatedObject, field.getName(), getValue(
                        objectToDuplicate, field.getName()), true);
            }
        }
        return duplicatedObject;
    }

    /**
     * Returns true if the object has a simple type
     * @see isSimpleType
     * 
     * @param object
     *            The object we want to evaluate.
     * @return true if the object has a simple type.
     */
    public static boolean hasSimpleType(Object object) {
        return isSimpleType(object.getClass().getName());
    }

    /**
     * Returns true if the field has a simple type
     * @see isSimpleType
     * 
     * @param field
     *            The field we want to evaluate.
     * @return true if the field has a simple type.
     */
    public static boolean hasSimpleType(Field f) {
        return isSimpleType(f.getType().getName());
    }

	/**
	 * Return true, if the given typeName matches java.lang. Double, Boolean,
	 * Integer, String or Character or respective primitive types: double,
	 * boolean, ...
	 * 
	 * @param typeName
	 * @return
	 */
	public static boolean isSimpleType(String typeName) {
		if (       typeName.equals("java.lang.Boolean")
                || typeName.equals("java.lang.Integer")
                || typeName.equals("java.lang.String")
                || typeName.equals("java.lang.Double")
                || typeName.equals("java.lang.Character")
                || typeName.equals("boolean")
                || typeName.equals("int")
                || typeName.equals("String")
                || typeName.equals("double")
                || typeName.equals("Character")
                ) {
        	return true;
        }
        return false;
	}

}
