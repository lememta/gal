/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/ListUtil.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2010-06-28 14:50:33 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Useful functions for handling lists
 */

public class ListUtil {

    /**
     * The function takes a list and an element and `intersperses' that element
     * between the elements of the list.
     * 
     * For example: intersperse "abcde" ',' == "a,b,c,d,e"
     * 
     * @param <T> - Type of the list
     * @param list 
     * @param separ
     * @return interspersed list
     */
    public static <T> List<T> intersperse(List<T> list, T separ) {
        if (list == null || list.isEmpty() || list.size() == 1) {
            return list;
        } else {
            int n = list.size();
            int m = 2 * n - 1;
            ArrayList<T> newList = new ArrayList<T>(m);
            int i = 0;
            for (T listElem : list) {
                newList.add(i++, listElem);
                if (i < m) {
                    newList.add(i++, separ);
                }
            }
            return newList;
        }
    }
    
    /**
     * Concatenate a list of lists.
     * 
     * For example: [[0], [1], [2]] -> [0, 1, 2]
     * 
     * @param <T> - Type of the list
     * @param list of lists
     * @return list
     */
    public static <T> List<T> concat(List<List<T>> list) {
        if (list == null) {
            return null;
        } else {
            LinkedList<T> newList = new LinkedList<T>();
            for (List<T> subList : list) {
                newList.addAll(subList);
            }
            return newList;
        }
    }

    /**
     * Make a list of given int array.
     * 
     * @param array
     * @return list
     */
    public static List<Integer> toList(int[] ar) {
        List<Integer> l = new LinkedList<Integer>();
        for (int e : ar) {
            l.add(e);
        }
        return l;
    }
    /**
     * Make a list of given array.
     * 
     * @param <T> - Type of the list
     * @param array
     * @return list
     */
    public static <T> List<T> toList(T[] ar) {
        List<T> l = new LinkedList<T>();
        for (T e : ar) {
            l.add(e);
        }
        return l;
    }

}
