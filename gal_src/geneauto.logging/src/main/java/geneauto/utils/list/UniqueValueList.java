package geneauto.utils.list;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

public class UniqueValueList<T> extends LinkedList<T> {
	/* (non-Javadoc)
	 * @see java.util.LinkedList#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends T> c) {
		for (T e : c) {
			if (add(e) == false) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8185127686445730136L;
	
	// set of list values to check element uniqueness
	private HashSet<T> set = new HashSet<T>();

	/* (non-Javadoc)
	 * @see java.util.LinkedList#add(int, java.lang.Object)
	 * 
	 * adds element only if it does not already exist in the list
	 * NB! this alters the original semantics of List.add(int index, T element)
	 * after the method completes we can be sure, that the element is in the 
	 * list but is not guaranteed to be on the given position  
	 */
	@Override
	public void add(int index, T element) {
		if (set.contains(element)) {
			// do not add elements that already are in the list
			return;
		}
		
		set.add(element);
		super.add(index, element);
	}

	/* (non-Javadoc)
	 * @see java.util.LinkedList#add(java.lang.Object)
	 */
	@Override
	public boolean add(T e) {
		if (set.contains(e)) {
			// do not add elements that already are in the list
			return true;
		}
		
		set.add(e);
		return super.add(e);
	}

	/* (non-Javadoc)
	 * @see java.util.LinkedList#contains(java.lang.Object)
	 * 
	 * check containment from set, not from the list
	 */
	@Override
	public boolean contains(Object o) {
		return set.contains(o);
	}
	
	

}
