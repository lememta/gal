/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/FileUtil.java,v $
 *  @version	$Revision: 1.45 $
 *	@date		$Date: 2012-02-05 21:00:38 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.tuple.Pair;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * General purpose file and file path handling utility methods.
 * 
 */
public class FileUtil {

    /**
     * Utility function that extends the java.io.File.getParent() method
     * 
     * Works like java.io.File.getParent(), except that if the parent is null,
     * then gets the absolute path and tries to determine the absolute parent.
     * 
     * @param path
     *            - Some file path
     * @return - parent directory name
     * @throws SecurityException
     *             - If the absolute path name cannot be determined (only in
     *             case the given path contains no parent information).
     */
    public static String getParent(String path) throws SecurityException {
        File f = new File(path);
        String result = f.getParent();
        if (result == null) {
            result = f.getAbsoluteFile().getParent();
        }
        return result;
    }

    /**
     * Receives a path string and adds a file or sub-directory part to it, using
     * either system default directory separator or if the given path string
     * uses a different separator, then uses that one. e.g. /foo/bar bas ->
     * /foo/bar/bas /foo/bar/ bas -> /foo/bar/bas c:\foo\bar bas ->
     * c:\foo\bar\bas
     * 
     * @param path
     *            Directory path
     * @param directory
     *            New directory that is to be added
     * @return Modified directory path
     */
    public static String appendPath(String path, String s) {
        File f = new File(path, s);
        try {
            // return canonical path, so that we don't get filenames with
            // redundant "." or ".."
            return f.getCanonicalPath();
        } catch (Exception e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "FileUtil.appendPath", "", "Error constructing file path",
                    e);
            return null;
        }
    }

    /**
     * Receives a path string and determines the path separator character used
     * in it. If none is found, then returns the system default.
     * 
     * @param path
     *            Some file path
     * @return path separator character (as string)
     */
    public static String getSeparator(String path) {
        String separator;
        if (path.contains("/")) {
            separator = "/"; // Unix style path
        } else if (path.contains("\\")) {
            separator = "\\"; // Windows style path
        } else {
            separator = File.separator; // System default
        }
        return separator;
    }

    /**
     * Splits the given string into two parts based on the last occurrence of
     * "." in the name
     * 
     * @param fileName
     *            Given filename
     * 
     * @return Pair<namePart, extPart>
     */
    public static Pair<String, String> splitFileByExtension(String fileName) {
        // Extract the base name from the given filename
        int pos = fileName.lastIndexOf(".");
        String extPart;
        String namePart;

        if (pos >= 0) {
            namePart = fileName.substring(0, pos);
            extPart = fileName.substring(pos + 1, fileName.length());
        } else {
            namePart = fileName;
            extPart = "";
        }
        return new Pair<String, String>(namePart, extPart);
    }

    /**
     * Returns filename extension (the string after the last ".")
     * 
     * @param filename
     *            Given filename
     * 
     * @return file name extension from the name
     */
    public static String getFileExtension(String filename) {
        // Extract the base name from the given filename
        int pos = filename.lastIndexOf(".");
        String extension;

        if (pos >= 0) {
            extension = filename.substring(pos + 1, filename.length());
        } else {
            extension = "";
        }

        return extension;
    }

    /**
     * Returns filename without the extension
     * 
	 * @param filename Given filename e.g. "archive.tar"
	 * @return         Filename without the extension e.g. "archive"
     */
    public static String getFileNameNoExt(String filename) {
    	
    	// Save a handle the original file
        File f = new File(filename); 

        filename = f.getName();

        // Extract the base name from the given filename
        int pos = filename.lastIndexOf(".");

        if (pos >= 0) {
            filename = filename.substring(0, pos);
        }

		// Next lines are to compensate, when the filename did not have an
		// extension, but there was "." in the file path
        if (f.getParentFile() != null) {
            filename = f.getParentFile() + File.separator + filename;
        }

        return filename;
    }

	/**
	 * Returns filename without the specified extension. Useful in cases, where
	 * only certain extensions are to be removed or the extension to be removed
	 * contains multiple parts (like "tar.gz").
	 * 
	 * @param filename Given filename e.g. "archive.tar"
	 * @param ext      Extension to be removed, e.g. "tar" or "tar.gz"
	 * @return         Filename without the extension e.g. "archive"
	 */
    public static String getFileNameNoExt(String filename, String ext) {
    	
    	if (!ext.startsWith(".")) {
    		ext = "." + ext;
    	}
        // Extract the base name from the given filename
        int pos = filename.lastIndexOf(ext);

        if (pos >= 0) {
            filename = filename.substring(0, pos);
        }

        return filename;
    }

	/**
	 * Sets a given extension to a given filename. If the filename already has
	 * an extension (i.e. ends with ".something") then this will be replaced by
	 * ".extension".
	 * 
	 * @param filename  Given filename
	 * @param extension Extension to be set, e.g. "txt"
	 * @return Given filename with the new extension
	 */
	public static String setFileExtension(String filename, String extension) {
	    if (!extension.startsWith("."))
	        extension = "." + extension;
	    return getFileNameNoExt(filename) + extension;
	}

	/**
	 * The supplied new extension is added to the given filename. If the
	 * original filename had an extension that matches the given old extension,
	 * then that will be removed first.
	 * 
	 * @param filename Given filename
	 * @param newExt   Extension to be set, e.g. "zip"
	 * @param oldExt   Extension to be removed, e.g. "tar" or "tar.gz"
	 * @return Given filename with the new extension
	 */
	public static String setFileExtension(String filename, String newExt, String oldExt) {
	    if (!newExt.startsWith("."))
	        newExt = "." + newExt;
	    return getFileNameNoExt(filename, oldExt) + newExt;
	}

	/**
	 * Returns a list of fileHandles to the subdirectories of the given
	 * directory
	 * 
	 * @param dirName Name of a directory 
	 * @return a list of fileHandles to the subdirectories of the given
	 *         directory
	 */
   public static File[] getSubDirs(String dirName) {
    	File dir = new File(dirName);
        FileFilter fileFilter = new FileFilter() {
            public boolean accept(File file) {
                return file.isDirectory();
            }
        };
        return dir.listFiles(fileFilter);
    }

    /**
     * Appends a string to a file. If a file with the same name is not existing,
     * then it is created. The required directories are created also.
     * 
     * @param filename
     *            Full output file path
     * @param content
     *            Content to written to the file
     * 
     */
    public static void appendFile(String filePath, String content) {
        File outFile = new File(filePath);
        File outDir = outFile.getParentFile();
        try {
            outDir.mkdirs();
            FileWriter writer = new FileWriter(outFile, true);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            EventHandler
                    .handle(EventLevel.CRITICAL_ERROR, "FileUtil.appendFile",
                            "", "Error writing to file", e);
        }
    }

    /**
     * Writes a string to a file. If a file with the same name exists and it is
     * writable, then it will be overwritten. Creates also any required
     * directories.
     * 
     * @param filename
     *            Full output file path
     * @param content
     *            Content to written to the file
     * 
     */
    public static void writeFile(String filePath, String content) {
        File outFile = new File(filePath);
        File outDir = outFile.getParentFile();
        try {
            outDir.mkdirs();
            FileWriter writer = new FileWriter(outFile);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            EventHandler
                    .handle(EventLevel.CRITICAL_ERROR, "FileUtil.writeFile",
                            "", "Error writing to file", e);
        }
    }

    /**
     * Writes a list of strings string to file. If a file with the same 
     * name exists and it is writable, then it will be overwritten. 
     * Creates also any required directories.
     * 
     * @param lines
     *            List of lines
     * @param content
     *            Content to written to the file
     * 
     */
    public static void writeLinesToFile(String filePath, List<String> lines) {
        File outFile = new File(filePath);
        File outDir = outFile.getParentFile();
        try {
            outDir.mkdirs();
            FileWriter writer = new FileWriter(outFile);
            if (lines != null) {
            	for (String line : lines) {
                    writer.write(line + "\n");
            	}
            }
            writer.flush();
            writer.close();
        } catch (Exception e) {
            EventHandler
                    .handle(EventLevel.CRITICAL_ERROR, "FileUtil.writeFile",
                            "", "Error writing to file", e);
        }
    }

    /**
     * @param filePath
     * @return the lines in the specified text file
     */
    public static List<String> readLines(String filePath) {
        List<String> lines = new LinkedList<String>();
        DataInputStream in = null;
        try {
            FileInputStream fstream = new FileInputStream(filePath);
            in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
            in.close();
        } catch (Exception e) {
            EventHandler
                    .handle(EventLevel.CRITICAL_ERROR, "FileUtil.readLines",
                            "", "Error reading file", e);
        } finally {
        	if (in != null) {
        		try {
					in.close();
				} catch (IOException e) {
					EventHandler.handle(EventLevel.CRITICAL_ERROR, "FileUtil.readLines",
                            "", "Error closing stream", e);
				}
        	}
        }

        return lines;
    }

    /**
     * Copies contents of file referenced by file1Name to file referenced by
     * file2Name. If file2 exists, it will be overwritten
     * 
     * @param file1Name
     * @param file2Name
     * 
     * @return true if copying was successful, false in case of error
     */
    public static boolean copy(String from, String to) {
        final int BUFF_SIZE = 100000;
        final byte[] buffer = new byte[BUFF_SIZE];
        
        // make sure that the folder for this file exists
        createFoldersForFile(to);
        
        InputStream in = null;
        OutputStream out = null;
        boolean success = false;
        try {
            try {
               in = new FileInputStream(from);
               out = new FileOutputStream(to);
               while (true) {
                  synchronized (buffer) {
                     int amountRead = in.read(buffer);
                     if (amountRead == -1) {
                        success = true;
                        break;
                     }
                     out.write(buffer, 0, amountRead); 
                  }
               }
            } finally {
               if (in != null) {
                  in.close();
               }
               if (out != null) {
                  out.close();
               }
            }
        } catch (IOException e) {
            EventHandler.handle(EventLevel.ERROR, 
                    "FileUtil.copy()", "",
                    "Error copying file.", e);
            success = false;
        }        
        return success;
     }

// TODO Cleanup - there should not be commented out code. AnTo 12.07.2011    
//    public static boolean copy(String file1Name, String file2Name) {
//        File f1 = new File(file1Name);
//        File f2 = new File(file2Name);
//
//        return copy(f1, f2);
//    }
//
//    /**
//     * Copies the file to the pointed directory and file name
//     * 
//     * @param File
//     *            source file
//     * @param File
//     *            destination file
//     * @return the result of operation success: true - if it is done, false - if
//     *         not
//     */
//    public static boolean copy(File fSource, File fCopy) {
//        boolean result = true;
//        try {
//            int c = 0;
//            FileInputStream in = new FileInputStream(fSource);
//            FileOutputStream out = new FileOutputStream(fCopy);
//            while ((c = in.read()) != -1)
//                out.write(c);
//            in.close();
//            out.close();
//        } catch (IOException iox) {
//            result = false;
//            EventsHandler.handle(EventLevel.ERROR, "FileUtil", "",
//                    "Error copying file: " + iox.getMessage(), "");
//            // iox.printStackTrace();
//        }
//        return result;
//    }

    public static boolean move(String source, String dest) {
        boolean result = copy(source, dest);
        if (result) {
            File fSource = new File(source);
            if (fSource.exists()) {
                try {
                    result = fSource.delete();
                } catch (SecurityException se) {
                    result = false;
                    EventHandler.handle(EventLevel.ERROR, "FileUtil.move()", "",
                            "No permission to delete file: " + se.getMessage());
                }
            } else {
                result = false;
                EventHandler.handle(EventLevel.WARNING, "FileUtil.move()", "",
                        "File doesn't exist: " + fSource.getName());
            }
        }
        return result;
    }

    /**
     * Checks if file can be read. Throws critical error if it can not.
     * 
     * @param path
     *            -- path to the file to be checked
     */
    public static void assertCanRead(String path) {
        File f = new File(path);

        if (!f.canRead()) {
			// Canonicalise the filename for better readability. This is not
			// strictly necessary, but makes the output more readable
            try {
    			path = f.getCanonicalPath();
    		} catch (IOException e) {
    			// Get the absolute path instead
    			f.getAbsolutePath();
    		}
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "GE0002",
                    "Failed to open file \"" + path
                            + "\" for reading", "");
        }
    }

    /**
     * Checks if file can be read. Throws critical error if it can not.
     * 
     * @param path
     *            -- path to the file to be checked
     */
    public static void assertCanWrite(String path) {
        File f = new File(path);

        if (f.exists() && !f.canWrite()) {
			// Canonicalise the filename for better readability. This is not
			// strictly necessary, but makes the output more readable
            try {
    			path = f.getCanonicalPath();
    		} catch (IOException e) {
    			// Get the absolute path instead
    			f.getAbsolutePath();
    		}
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "GE0002",
                    "Failed to open file \"" + path
                            + "\" for writing", "");
        }
    }

    /**
     * Opens a fileWriter
     * @param pFilePath -- File path
     * @param pAppend -- Append instead of reset and create new
     * @return
     */
    public static FileWriter openFileWriter(String pFilePath, boolean pAppend) {
        File file = new File(pFilePath);
        File folder;
        FileWriter writer = null;

        // if the directory does not exist, we create it.
        String filePath = file.getAbsolutePath();
        String fileName = file.getName();
        filePath = filePath.replace(fileName, "");
        folder = new File(filePath);

        try {
            // create the folder
            folder.mkdirs();
            // create new file if the file does not exist or
            // we have asked to reset the file
            if (!(file.exists() && pAppend)) {
                file.createNewFile();
                file.setWritable(true);
                writer = new FileWriter(file);
            }
            // open file for appending
            else {
                writer = new FileWriter(file, true);
            }
        } catch (Exception e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "FileUtil.openFileWriter", "GE0002",
                    "Failed to open file \"" + file.getAbsolutePath()
                            + "\" for writing", e.getStackTrace().toString());
        }

        return writer;
    }

	/**
	 * Deletes all files in the given folder, if it exists
	 * 
	 * @param folderName
	 * @param exclusions
	 *            Set of file paths to be excluded. Can be null. If not null 
	 *            then, it should be a HashSet for fast access.
	 *            NOTE: The map should contain *canonical* file paths
	 *            to avoid incorrect behaviour
	 */
    public static void cleanFolder(String folderName, Set<String> exclusions) {
        File folder = new File(folderName);
        
        // Canonicalise the folder name
        try {
			folder = folder.getCanonicalFile();
		} catch (IOException e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "cleanFolder", "", "Error constructing file path ", e);
		}

        // when folder does not exist there is nothing to do
        if (!folder.exists()) {
            return;
        }

        if (!folder.isDirectory()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "FileUtil.cleanFolder", "GE0030", "\"" + folderName
                            + "\" is not a directory", "");
        }

        for (File f : folder.listFiles()) {
        	
        	// If it is a directory, it must be emptied, before it can be deleted 
        	if (f.isDirectory()) {
        		cleanFolder(f.getAbsolutePath(), exclusions);
        	}
        	
			// Try to delete the file or directory, unless it is marked for exclusion
        	if (exclusions == null || exclusions.isEmpty() 
        		  || !exclusions.contains(f.getAbsolutePath())) {
		    		if (!f.delete()) {
		    			EventHandler.handle(EventLevel.CRITICAL_ERROR, "FileUtil.cleanFolder", "", 
		    					"Could not delete file :\n  " + f.getAbsolutePath());
	    		}
    		} else {
    			// The file is excluded from deletion
    			// Mark also its parent (the current directory) to be excluded
    			// Note: The folder will be still emptied, but it isn't deleted
    			exclusions.add(folder.getAbsolutePath());
        	}
        }

        EventHandler.handle(EventLevel.INFO, "FileUtil.cleanFolder", "GI0010",
                "Successfully cleaned folder \"" + folderName + "\".", "");
    }

    /**
     * returns true if folder is empty or does not exist
     */
    public static boolean isFolderEmpty(String folderName) {
        File tempFolder = new File(folderName);

        // When the tempFolder does not exist, there is nothing to do
        if (!tempFolder.exists()) {
            return true;
        }

        if (!tempFolder.isDirectory()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "FileUtil.isFolderEmpty", "GE0030", "\"" + folderName
                            + "\" is not directory", "");
            return true;
        }

        // Iterate through the directory content and return false if first 
        // file is found
        for (File f : tempFolder.listFiles()) {
            if (f.isDirectory()) {
            	// Found a dir
            	if (!isFolderEmpty(f.getAbsolutePath())) {
            		return false;
            	}
            } else {
            	// Found a file
                return false;
            }
        }
        
        return true;
    }

    /**
     * Takes filename as an argument and creates all necessary folders for 
     * storing this file
     * @param fileName
     */
    public static void createFoldersForFile(String fileName) {
    	File f = new File(fileName);
    	File dir = f.getParentFile();
    	if (dir != null && !dir.exists()) {
    		dir.mkdirs();
    	}
    }
    
    /**
     * Takes folder name as an argument and creates this folder together
     * with the necessary folder tree
     * @param dirName
     */
    public static void createFolders(String dirName) {
    	File dir = new File(dirName);
    	if (dir != null && !dir.exists()) {
    		dir.mkdirs();
    	}
    }

    /**
     * Copies all files from fromPath to toPath.
     * Ignores sub-directories 
     * 
     * @param sourceDir source direction path
     */
    public static void copyFolderContents(String fromPath, String toPath) {
    	File sourceDir = new File(fromPath);
    	
        if (!sourceDir.exists()) {
            EventHandler.handle(
            		EventLevel.ERROR, 
            		"FileUtil.copyFolderContents", 
            		"",
                    "Folder \"" 
            			+ sourceDir.getAbsolutePath() 
            			+ "\" does not exist!");   
            return;
        }
        
        for (File file: sourceDir.listFiles()) {
            if (!file.isDirectory()) {
                copy(appendPath(fromPath, file.getName()), 
                		appendPath(toPath, file.getName()));
                EventHandler.handle(
                		EventLevel.INFO, "", "",
                        "Copying file: " + file.getName(), "");
            }
        }
    }


    /*
     * TODO : Comments
     */
    public static String getVersionFromManifest(URL url, String toolClassPath) {

    	return GAConst.TOOLSET_VER;
    
    }

    /*
     * TODO : Comments
     */
    public static String getDateFromManifest(URL url, String toolClassPath) {

    	return GAConst.TOOLSET_DATE;

    }
}