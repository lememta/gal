/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.utilities/src/main/java/geneauto/utils/assertions/Assert.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-19 08:47:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 * *  
 */
package geneauto.utils.assertions;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.util.Collection;

public class Assert {

    /**
     * Ensures that the given object reference is not null. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param o			the object reference to check
     * @param msg		message to be displayed
     */
    public static void assertNotNull(Object o, String msg) {
    	assertNotNull(o, msg, null, null, null);
    }

    /**
     * Ensures that the given object reference is not null. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param o			the object reference to check
     * @param msg		message to be displayed
     * @param location	reference to message location
     * @param elRef		reference string of an associated model element
     */
    public static void assertNotNull(Object o, 
    								String msg, 
    								String location, 
    								String elRef,
    								String errNo) {
        if (o == null) {
        	raiseAssertError("Null pointer", msg, location, elRef, errNo, 
															"assertNotNull");

        }
    }

    /**
     * If strict is true and the check fails , then prints the message and raises an error
     * If strict is true and the check passes, then does nothing
     * If strict is false, then the check is ignored 
     * @param o
     * @param msg
     * @param strict
     */
    public static void assertNotNull(Object o, String msg, boolean strict) {
        if (strict) {
            assertNotNull(o, msg);
        }
    }
    
    
    /**
     * Ensures that the given integer value is not 0. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param value		value to check
     * @param msg		message to be displayed
     * @param location	reference to message location
     * @param elRef		reference string of an associated model element
     * @param errNo		error number
     */
    public static void assertNotZero(int value, 
    								String msg, 
    								String location, 
    								String elRef,
    								String errNo) {
        if (value == 0) {
        	raiseAssertError("Value is 0", msg, location, elRef, errNo, 
															"assertNotNull");

        }
    }
    
   /**
     * Ensures that a collection is neither null nor empty. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param c			collection to check 
     * @param msg		message
     */
    public static void assertNotEmpty(Collection<?> c, String msg) {
    	assertNotEmpty(c, msg, null, null, null);
    }

    /**
     * Ensures that a collection is neither null nor empty. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param c			collection to check 
     * @param msg		message
     * @param location	reference to check location
     * @param elRef		reference to associated model element
     * @param errNo		error code
     */
    public static void assertNotEmpty(Collection<?> c,
		String msg, 
		String location, 
		String elRef,
		String errNo) {
    	
        if (c == null || c.isEmpty()) {
        	raiseAssertError("Empty collection", msg, location, elRef, errNo, 
															"assertNotEmpty");
        }
    }

    /**
     * Ensures that a collection contains exactly one element. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param c			collection to check 
     * @param msg		message
     */
    public static void assertSingleElement(Collection<?> c, String msg) {
    	assertSingleElement(c, msg, null, null, null);
    }

    /**
     * Ensures that a collection contains exactly one element. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param c			collection to check 
     * @param msg		message
     * @param location	reference to check location
     * @param elRef		reference to associated model element
     * @param errNo		error code
     */
    public static void assertSingleElement(Collection<?> c,
		String msg, 
		String location, 
		String elRef,
		String errNo) {
    	
        if (c == null || c.size() != 1) {
        	String lMsg = "Single element expected, found ";
        	if (c == null) {
        		lMsg += "0";
        	} else {
        		lMsg += c.size();
        	}
        	
        	raiseAssertError(lMsg, msg, location, elRef, errNo, 
														"assertSingleElement");
        }
    }

    /**
     * Ensures that a collection contains exactly elCnt elements. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param c			collection to check
     * @param elCnt		expected element count 
     * @param msg		message
     * @param location	reference to check location
     * @param elRef		reference to associated model element
     * @param errNo		error code
     */
    public static void assertNElements(Collection<?> c,
    	int elCnt,
		String msg, 
		String location, 
		String elRef,
		String errNo) {
    	
        if (c == null || c.size() != elCnt) {
        	String lMsg = elCnt + " elements expected, found ";
        	if (c == null) {
        		lMsg += "0";
        	} else {
        		lMsg += c.size();
        	}
        	
        	raiseAssertError(lMsg, msg, location, elRef, errNo, 
												"assertNElements");
        }
    }

    /**
     * Ensures that a collection contains at least elCnt elements. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param c			collection to check
     * @param elCnt		expected element count 
     * @param msg		message
     * @param location	reference to check location
     * @param elRef		reference to associated model element
     * @param errNo		error code
     */
    public static void assertNPlusElements(Collection<?> c,
    	int elCnt,
		String msg, 
		String location, 
		String elRef,
		String errNo) {
    	
        if (c == null || c.size() < elCnt) {
        	String lMsg = "at least " + elCnt + " elements expected, found ";
        	if (c == null) {
        		lMsg += "0";
        	} else {
        		lMsg += c.size();
        	}
        	
        	raiseAssertError(lMsg, msg, location, elRef, errNo, 
        										"assertNPlusElements");
        }
    }

    /**
     * Ensures that a collection contains at least elCnt elements. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param sysMsg	message part composed by the assert method
     * @param userMsg	message given as argumetn to the assert method
     * @param location	reference to check location
     * @param elRef		reference to associated model element
     * @param errNo		error code
     * @param assertMethod 
     * 					name of the assert method calling this subroutine
     */
    private static void raiseAssertError(String sysMsg,
		String userMsg, 
		String location, 
		String elRef,
		String errNo,
		String assertMethod) {
    	
    	String lMsg = sysMsg;
    	if (userMsg != null) {
    		lMsg += ": " + userMsg;
    	}
    	
    	if (location == null) {
    		location = assertMethod;
    	}
    	// display element reference
    	if (elRef != null) {
    		lMsg += "\n Element: " + elRef;
    	}
    	
    	// default error number in case there is no location-specific no
    	if (errNo == null) {
    		errNo = ""; 
    	}
    	
        EventHandler.handle(EventLevel.CRITICAL_ERROR, location,
        		errNo, lMsg, "");
    }

    /**
     * Ensures that a string is neither null nor empty. 
     * Raises a CRITICAL_ERROR on failure.
     * This, by specification, stops the tool.
     * 
     * @param s
     *            String
     * @param msg
     *            Custom message, e.g name of the variable
     */
    public static void assertNotEmpty(String s, String msg) {
        if (s == null || s.isEmpty()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "assertNotEmpty",
                    "", "String is null or empty: " + msg, "");
        }
    }

 }
