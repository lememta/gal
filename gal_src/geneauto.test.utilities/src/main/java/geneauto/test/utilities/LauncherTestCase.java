package geneauto.test.utilities;

import java.security.Permission;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public abstract class LauncherTestCase extends TestCase {

   

    private SecurityManager securityManager;
    
    
    @Before
    public void setUp() {
        securityManager = System.getSecurityManager();
        System.setSecurityManager(new NoExitSecurityManager());
    }
    
    public void test(String launcherDir, String modelPath) {
    	String[] args = TestUtils.getArgs(launcherDir , 
				modelPath);
		try {
			runLauncher(args);
		} catch (ExitException ee) {
			Assert.assertEquals(0, ee.status);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
    }
    
    public abstract void runLauncher(String[] args);

    @After
    public void tearDown() {
        System.setSecurityManager(securityManager);
    }
    
    protected static class ExitException extends SecurityException {
    	
		private static final long serialVersionUID = 5750123062946445243L;
		
		public final int status;
		
        public ExitException(int status) {
                super("There is no escape!");
                this.status = status;
        }
    }

    private static class NoExitSecurityManager extends SecurityManager {
    	
        @Override
        public void checkPermission(Permission perm) {
                // allow anything.
        }
        
        @Override
        public void checkPermission(Permission perm, Object context) {
                // allow anything.
        }
        
        @Override
        public void checkExit(int status) {
                super.checkExit(status);
                throw new ExitException(status);
        }
    }
}
