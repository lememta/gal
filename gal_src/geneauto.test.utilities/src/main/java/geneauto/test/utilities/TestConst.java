package geneauto.test.utilities;

/**
 * Constants used in project
 */
public class TestConst {
    
    /**
     * Name of the CVS folder (shall not be taken into account 
     * when comparing files
     */
    public final static String CVS_FOLDER_NAME = "CVS";
}
