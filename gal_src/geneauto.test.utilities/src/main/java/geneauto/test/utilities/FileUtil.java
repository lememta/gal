/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.test.utilities/src/main/java/geneauto/test/utilities/FileUtil.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-07-07 12:23:58 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.test.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

/**
 * General purpose file and filepath handling utility methods.
 * 
 * Note! Error handling in this package is different from that in
 * geneauto.utils. We cannot use the same event handler as in geneauto.utils.
 * Instead we use methods from junit.Assert, since this class is to be used for
 * JUnit tests only.
 */
public class FileUtil {

    /**
     * Utility function that extends the java.io.File.getParent() method
     * 
     * Works like java.io.File.getParent(), except that if the parent is null,
     * then gets the absolute path and tries to determine the absolute parent.
     * 
     * TODO AnTo Maybe raise an error, when the parent is still null.
     * 
     * @param path
     *            - Some file path
     * @return - parent directory name
     * @throws SecurityException
     *             - If the absolute path name cannot be determined (only in
     *             case the given path contains no parent information).
     */
    public static String getParent(String path) throws SecurityException {
        File f = new File(path);
        String result = f.getParent();
        if (result == null) {
            result = f.getAbsoluteFile().getParent();
        }
        return result;
    }
    
    /**
     * Receives a path string and adds a file or sub-directory part to it, using
     * either system default directory separator or if the given path string
     * uses a different separator, then uses that one. e.g. /foo/bar bas ->
     * /foo/bar/bas /foo/bar/ bas -> /foo/bar/bas c:\foo\bar bas ->
     * c:\foo\bar\bas
     * 
     * @param path
     *            Directory path
     * @param directory
     *            New directory that is to be added
     * @return Modified directory path
     */
    public static String appendPath(String path, String s) {
        File f = new File(path, s);
        try {
            // return canonical path, so that we don't get filenames with
            // redundant "." or ".."
            return f.getCanonicalPath();
        } catch (Exception e) {
            String msg = "Error constructing file path: " + e.getMessage();
            throw new RuntimeException(msg);
        }
    }

	/**
	 * Tries to delete a file, if it exists. If it does and deleting fails, then
	 * prints an error message and throws an Exception.
	 * 
	 * @param filename
	 * @return true if file does not exist or if it exists and deleting was
	 *         successful
	 * 
	 */
    public static boolean deleteFileIfExists(String filePath, boolean failOnError) {
        File f = new File(filePath);
        if (!f.exists()) {
        	return true;
        }
        String msg = "";
        boolean result;
        try {
        	result = f.delete();
        	if (!result) {
                msg = getNormalisedPath(f);
        	}
        } catch (Exception e) {
        	result = false;
            msg = e.getMessage();
        }
        if (!result) {
            msg = "Error deleting file: " + msg;
            throw new RuntimeException(msg);
        }
        return result;
    }

	/**
	 * @param filePath
	 * @return If it is possible to obtain the canonical path of the given
	 *         filePath, then returns that. Otherwise, the absolute path is
	 *         returned
	 */
    public static String getNormalisedPath(String filePath) {
        return getNormalisedPath(new File(filePath));
    }    
    
	/**
	 * @param file
	 * @return If it is possible to obtain the canonical path of the given
	 *         file, then returns that. Otherwise, the absolute path is
	 *         returned
	 */
    public static String getNormalisedPath(File f) {
        String res;
        try {
        	res = f.getCanonicalPath();
        } catch (Exception e) {
            res = f.getAbsolutePath();
        }
        return res;
    }    
    
    
    /**
     * Writes a string to a file. If a file with the same name exists and it is
     * writable, then it will be overwritten. Creates also any required
     * directories.
     * 
     * @param filename
     *            Full output file path
     * @param content
     *            Content to written to the file
     * 
     */
    public static void writeFile(String filePath, String content) {
        File outFile = new File(filePath);
        writeFile(outFile, content);
    }

    /**
     * Writes a string to a file. If a file with the same name exists and it is
     * writable, then it will be overwritten. Creates also any required
     * directories.
     * 
     * @param outFile
     *            Reference to the file
     * @param content
     *            Content to written to the file
     * 
     */
    public static void writeFile(File outFile, String content) {
        File outDir = outFile.getParentFile();
        try {
            outDir.mkdirs();
            FileWriter writer = new FileWriter(outFile);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            String msg = "Error writing to file: " + e.getMessage();
            throw new RuntimeException(msg);
        }
    }

    /**
     * @param filePath
     * @return the lines in the specified text file
     */
    public static List<String> readLines(String filePath) {
        List<String> lines = new LinkedList<String>();
        try {
            FileInputStream fstream = new FileInputStream(filePath);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
            in.close();
        } catch (Exception e) {
            String msg = "Error reading from file: " + e.getMessage();
            throw new RuntimeException(msg);
        }
        return lines;
    }
    
    /**
     * Processes file line: removes commented parts (if necessary)
     * 
     * @param line File line to be processed 
     * @param compoundCommentStart Start of Compound (multi-line) comment
     * @param compoundCommentEnd End of compound (multi-line) comment
     * @param simpleCommentStart Start of simple (single-line) comment
     * 
     * @return processed file line
     */
    public static FileLine processLine(String line, 
            String compoundCommentStart, String compoundCommentEnd, 
            String simpleCommentStart, 
            boolean prevLineHasUnclosedComment) {
        
        FileLine fileLine = new FileLine(line, prevLineHasUnclosedComment, prevLineHasUnclosedComment);
        
        /*
         * No comment sign is specified -> do not change anything 
         */
        if ((compoundCommentStart == null
                && simpleCommentStart == null) || 
           (compoundCommentStart == null
                && compoundCommentEnd != null)) {
            return fileLine;
        }
        
        /*
         * Previous file line has unclosed comment
         */
        if (prevLineHasUnclosedComment) {
            
            /*
             * If previous line has unclosed comment and contains 
             * compoundCommentEnd then: 
             * 
             * 1) line does not have unclosed comment
             * 2) part before compoundCommentEnd is ignored
             */
            if(line.matches(".*" + compoundCommentEnd + ".*")) {
                line = line.replaceAll(".*" + compoundCommentEnd, "");
                fileLine.setHasUnclosedComment(false);                
                
            /*
             * If previous line has unclosed comment and contains 
             * no compoundCommentEnd then line is ignored
             */    
            } else {
                fileLine.setLineString( "");
                return fileLine;
            }
        }
        
        
        if (compoundCommentStart != null) {
        	
            if (line.matches(".*" + compoundCommentStart + ".*")) {
            	fileLine.setComment(true);
            }
            
            // cut all single line compound comments
            String regExp 
            	= compoundCommentStart + ".*" + compoundCommentEnd;
            line = line.replaceAll(regExp, "");
            
            /* 
             * if line still contains compoundCommentStart
             * after previous step then:  
             * 1) line has unclosed comment
             * 2) part from compoundCommentStart is ignored
             */
            
            if (line.matches(".*" + compoundCommentStart + ".*")) {
                fileLine.setHasUnclosedComment(true);
                line = line.replaceAll(compoundCommentStart + ".*", "");
            }
        } 
        
        if (simpleCommentStart != null) {
            int index = line.indexOf(simpleCommentStart);

            /* 
             * line contains simpleCommentStart -> part before 
             * it is ignored
             */
            if (index != -1) {
                line = line.substring(0, index);
                fileLine.setComment(true);
            }
        }
        
        fileLine.setLineString(line);
        return fileLine;
    }

    /**
     * Sets a given extension to a given filename. If the filename already has
     * an extension (i.e. ends with .something) then this will be replaced by
     * the given extension.
     * 
     * @param filename
     *            Given filename
     * @param extension
     *            Extension to be set, e.g. "txt"
     * @return Given filename with the new extension
     */
    public static String setFileExtension(String filename, String extension) {
        if (!extension.startsWith("."))
            extension = "." + extension;
        return getFileNameNoExt(filename) + extension;
    }

    /**
     * Returns filename extension
     * 
     * @param filename
     *            Given filename
     * 
     * @return file name extension from the name
     */
    public static String getFileExtension(String filename) {
        // Extract the base name from the given filename
        int pos = filename.lastIndexOf(".");
        String extension;

        if (pos >= 0) {
            extension = filename.substring(pos + 1, filename.length());
        } else {
            extension = "";
        }

        return extension;
    }

    /**
     * Returns filename without the extension
     * 
     * @param filename
     *            Given filename
     * 
     * @return filename without the extension
     */
    public static String getFileNameNoExt(String filename) {
        File f = new File(filename);

        filename = f.getName();

        // Extract the base name from the given filename
        int pos = filename.lastIndexOf(".");

        if (pos >= 0) {
            filename = filename.substring(0, pos);
        }

        if (f.getParentFile() != null)
            filename = f.getParentFile() + File.separator + filename;

        return filename;
    }
    
	/**
	 * Returns a list of fileHandles to the subdirectories of the given
	 * directory
	 * 
	 * @param dirName
	 *            Name of a directory
	 * 
	 * @return a list of fileHandles to the subdirectories of the given
	 *         directory
	 */
   public static File[] getSubDirs(String dirName) {
    	File dir = new File(dirName);
        FileFilter fileFilter = new FileFilter() {
            public boolean accept(File file) {
                return file.isDirectory();
            }
        };
        return dir.listFiles(fileFilter);
    }

   /**
     * checks if file/directory exist and is it file/directory
     * 
     * @param element file/directory for checking
     * @param isFile true -> check if element is a file
     *               false -> check if element is a directory
     * 
     * @return true if check is passed false otherwise
     */
    public static boolean fileCheck(File element, boolean isFile) {
        boolean result = true;
        String type = "";
        if (isFile) {
            type += " File ";
        } else {
            type += " Directory ";
        }
        
        // check for element existence
        if (!element.exists()) {
            
            // refDir does not exist
            result = false;
            EventHandler.handle(EventLevel.ERROR, 
                    "fileCheck", 
                    "", 
                    type + element.getAbsolutePath() 
                    + " does not exist");
        
        // check if element is file a file (isFile = true)
        // or if element is a directory (isFile = false)
        } else if ( ((isFile) && (!element.isFile())) 
                || ((!isFile) && (!element.isDirectory())) ) {
            EventHandler.handle(EventLevel.ERROR, 
                    "fileCheck", 
                    "", 
                    element.getAbsolutePath() + " is not a" + type
                    );
        }
        return result;
    }
    
    /**
     * compares file names of 2 directories
     * 
     * @param refPath path of the reference directory
     * @param genePath path of the generated directory
     * 
     * @return map where 
     *              key -  file from reference directory
     *              value - file from generated directory 
     *                  with the same name as key file name  
     */
    public static HashMap<File, File> compareDirFileNames(String refPath, 
            String genePath) {
    	
        // Check that both reference and generated paths are specified
        Assert.assertNotNull("Reference path is not specified", refPath);
        Assert.assertNotNull("Generated path is not specified", genePath);
        
        File refDir = new File(refPath);
        File geneDir = new File(genePath);
        
        // Check that both reference and generated paths exist
        Assert.assertNotNull("Reference folder does not exist", refDir);
        Assert.assertNotNull("Generated folder does not exist", geneDir);
        
        HashMap<File, File> result = new HashMap<File, File>();
        
        // refDir and geneDir exist and are directories
        if (fileCheck(refDir, false) && fileCheck(geneDir, false)) {
            String[] refFileNames = refDir.list();
            String[] geneFileNames = geneDir.list();
            
            // sort file names of  geneDir
            Arrays.sort(geneFileNames);
            int geneFileNameIndex = 0;
            
            // search for every fileName of the refDir in geneDir
            for (String refFileName: refFileNames) {
                
                /*
                 * CVS folder shall not be taken into account 
                 */
                if (refFileName.equals(TestConst.CVS_FOLDER_NAME)) {
                    continue;
                }
                geneFileNameIndex = Arrays.binarySearch(geneFileNames, 
                        refFileName);
                if (geneFileNameIndex >= 0) {

                    // file name from refDir is found in geneDir
                    result.put(new File(refPath + "//" + refFileName), 
                            new File(genePath 
                                    + "//" + geneFileNames[geneFileNameIndex]));
                
                // file name from refDir is not found in geneDir
                } else {
                    EventHandler.handle(EventLevel.ERROR, 
                            "compareDirectoryFileName", 
                            "", 
                            "File " + refFileName
                            + " from directory " + refDir.getAbsolutePath()
                            + " was not found in directory "
                            + geneDir.getAbsolutePath());
                    result = null;
                }
            }
        } else {
            result = null;
        }
        return result;
    }

    /**
     * checks if content of reference file or
     * reference directory contains in generated files
     * 
     * @param refElement reference file/directory
     * @param originalElement generated file
     * @param compoundCommentStart Start of compound (multi-line) comment
     * @param compoundCommentEnd End of compound (multi-line) comment
     * @param simpleCommentStart Start of simple (single-line) comment
     * 
     * @return
     */
    public static boolean compareFiles(File refElement, File originalElement, 
            String compoundCommentStart, String compoundCommentEnd, 
            String simpleCommentStart) {
        
        /*
         * One of the files is cvs folder -> no comparison is need
         */
        if ((isCVSFolder(refElement)) || (isCVSFolder(originalElement))) {
            return true;
        }
        boolean result;
        if (refElement.isDirectory()) {
            
        	result = true;
            for (String refFolderFileName: refElement.list()) {
                File refFolderFile = new File(refElement, refFolderFileName);
                if (refFolderFile.isFile() && !compareFiles(refFolderFile, originalElement)) {
                    result = false;
                    break; // Skip the rest of the loop
                }
            }
            printComparisonResultMessage(result, originalElement, refElement);
            return result;
        } else if (refElement.isFile()) {
            if (!fileCheck(refElement, true) || !(fileCheck(originalElement, true))) {
                result = false;
                printComparisonResultMessage(result, originalElement, refElement);
                return result;
            }
            
            // Check the file contents
            return checkContentContainment(refElement, originalElement,
					compoundCommentStart, compoundCommentEnd,
					simpleCommentStart);
        }
        result = false;
        printComparisonResultMessage(result, originalElement, refElement);
        return result;
    }

	/**
	 * Checks, if the contents of reference file is contained in the compared
	 * file as one contiguous block.
	 * 
	 * NOTE: Whitespace and one comment are skipped form the beginning of the 
	 * reference file!
	 * 
	 * @param refFile
	 * @param comparedFile
	 * @param compoundCommentStart
	 * @param compoundCommentEnd
	 * @param simpleCommentStart
	 * @return
	 */
	public static boolean checkContentContainment(
			File refFile, File comparedFile, 
			String compoundCommentStart, String compoundCommentEnd, 
			String simpleCommentStart) {
		
		boolean result;
		
		// Read reference file to buffer
		List<String> file1Lines = readLines(refFile.getAbsolutePath());
		if (file1Lines == null || file1Lines.isEmpty()) {
			// Reference file is empty
			// Compared file must be empty too
			List<String> file2Lines = readLines(comparedFile.getAbsolutePath());
			result = file2Lines == null || file2Lines.isEmpty();
		    printComparisonResultMessage(result, comparedFile, refFile);
		    return result;
		}
		
		// Reference file is not empty
		// Check the files line-by-line
		FileLine file1Line;
		FileLine file2Line;
		String file2LineString;
		int initialFile1Line = 
				seekStartofRefCode(file1Lines, compoundCommentStart, 
						compoundCommentEnd, simpleCommentStart) - 1;
		boolean prevFile1LineHasUnclosedComment = false;
		boolean prevFile2LineHasUnclosedComment = false;
		try {

		    int numberOfFile1Line = initialFile1Line;
		    int maxMatchingLines = initialFile1Line;
		    FileInputStream fstream = new FileInputStream(comparedFile.
		            getAbsoluteFile());
			DataInputStream in = new DataInputStream(fstream);
			try {
			    BufferedReader br = new BufferedReader(new InputStreamReader(in));
	
			    // Reading content of generated file line by line
			    while ((file2LineString = br.readLine()) != null) {
	
			        // current line of generated file is the same 
			    	// as the first(next) line of reference file ->
			    	// -> increase the number of reference file line to compare
			    	// otherwise begin comparison from the 1. line of 
			    	// the reference file
			        
			        file1Line = processLine(file1Lines.get(numberOfFile1Line), compoundCommentStart, compoundCommentEnd, 
			                simpleCommentStart, prevFile1LineHasUnclosedComment);
			        prevFile1LineHasUnclosedComment = file1Line.hasUnclosedComment();
			        file2Line = processLine(file2LineString, compoundCommentStart, compoundCommentEnd, 
			                simpleCommentStart, prevFile2LineHasUnclosedComment);
			        prevFile2LineHasUnclosedComment = file2Line.hasUnclosedComment();
			        
			        if (file2Line.getLineString().equals(file1Line.getLineString())
			        		&& file2Line.isComment() == file1Line.isComment() ) {
			            numberOfFile1Line++;
			            if (numberOfFile1Line > maxMatchingLines) {
			            	maxMatchingLines = numberOfFile1Line;
			            }
			        } else {
			            numberOfFile1Line = initialFile1Line;
			        }
	
			        // all lines of reference file are present in 
			        // generated file -> result is positive and no need
			        // to scan file further
			        
			        if (numberOfFile1Line == file1Lines.size()) {
			            result = true;
			            printComparisonResultMessage(result, comparedFile, refFile);
			            return result;
			        }
			    }
			    
			    EventHandler.handle(EventLevel.ERROR, 
			            "compareFiles", "", 
			              "\nContent of reference file:\n  " 
			            + refFile.getAbsolutePath() 
			            + "\nnot contained in compared file:\n  "
			            + comparedFile.getAbsolutePath()
			            + "\nNumber of matching reference lines: " + maxMatchingLines
			            );
			}
			
		    finally {
				in.close();
			} 		
		    
		} catch (Exception e) {
		    String msg = "Error reading from file: " + e.getMessage();
		    throw new RuntimeException(msg);
		}
		
		result = false;
		printComparisonResultMessage(result, comparedFile, refFile);
		return result;
	}
    
	/**
	 * Return the number of first line after initial whitespace and optional comment
	 * @param lines
	 * @param simpleCommentStart 
	 * @param compoundCommentStart 
	 * @param compoundCommentEnd 
	 * @return
	 */
    private static int seekStartofRefCode(
    		List<String> lines, String compoundCommentStart, String compoundCommentEnd, 
			String simpleCommentStart) {
    	int i = 0;
    	
    	// Skip starting whitespace
    	while (lines.get(i).trim().isEmpty() && i < lines.size()) {
    		i++;
    	}
    	if (i == lines.size()) {
    		// Comment and whitspace are not compared according to the current approach. 
    		// Hence, it does not make sense to have such reference file 
    		EventHandler.handle(EventLevel.ERROR, "", "", "Reference file contains only whitespace.");
    	}
    	
    	// Skip starting comment
    	if (simpleCommentStart != null 
    			&& lines.get(i).trim().startsWith(simpleCommentStart)) {
    		// Simple comment
        	while (lines.get(i).trim().startsWith(simpleCommentStart) && i < lines.size()) {
        		i++;
        	}
        	
    	} else if (
    			compoundCommentStart != null 
    			&& lines.get(i).matches(".*" + compoundCommentStart + ".*")) {
    		// Compound comment
    		while (!lines.get(i).matches(".*" + compoundCommentEnd + ".*") && i < lines.size()) {
        		i++;
        	}
    		i = i+1; // We found the line that ends the comment. The sought text should come after it
    		
    	}    	
    	if (i == lines.size()) {
    		// Comment and whitspace are not compared according to the current approach. 
    		// Hence, it does not make sense to have such reference file 
    		EventHandler.handle(EventLevel.ERROR, "", "", "Reference file contains only whitespace and comment.");
    	}

    	// List index -> number of line
		return i + 1;
	}

	public static boolean compareFiles(File refElement, File originalElement) {
        return compareFiles(refElement, originalElement, null, null, null);
    }
    
    
    private static boolean isCVSFolder(File file) {
        if (file != null && file.isDirectory() && file.getName().equals(TestConst.CVS_FOLDER_NAME)) {
            return true;
        }
        return false;
    }
    
    /**
     * Prints result of comparison of files/directories
     * 
     * @param result Result of comparison
     * @param file1
     * @param file2
     */
    private static void printComparisonResultMessage(boolean result, 
            File file1, File file2) {
        String msg = "Comparison result for following files: "  
            + "\n\t" + file1.getAbsolutePath()
            + "\n\t" + file2.getAbsolutePath()
            + "\nis ";
        if (result) {
            msg += "POSITIVE";
        } else {
            msg += "NEGATIVE";
        }
        EventHandler.handle(EventLevel.INFO, "FileUtil.compareFiles", 
                "", msg);
    }
    
    
    /**
     * 
     * @param refDirPath path of the reference directory
     * @param geneDirPath path of the generated directory
     * @param compoundCommentStart Start of compound (multi-line) comment
     * @param compoundCommentEnd End of compound (multi-line) comment
     * @param simpleCommentStart Start of simple (single-line) comment
     * 
     * @return true if content of files from the reference 
     * directory contains if files with the same name
     * in generated directory
     */
    public static boolean checkDirFilesContents (String refDirPath, 
            String geneDirPath, String compoundCommentStart, 
            String compoundCommentEnd, String simpleCommentStart) {
        HashMap<File, File> fileNameMap = compareDirFileNames(refDirPath, geneDirPath);
        boolean result = true;
        /*
         *  All file names from the reference directory are also 
         *  in the generated directory -> check the contents of the files
         *  from both directories with the same names
         */
        if (fileNameMap != null) {
            for (File refFile: fileNameMap.keySet()) {
                if (refFile.isFile()) {
                    if (!compareFiles(refFile, fileNameMap.get(refFile), 
                            compoundCommentStart, compoundCommentEnd, simpleCommentStart)) {
                        result = false;
                    }
                }
            }
        } else {
            result = false;
        }
        return result;
    }
    
    /**
     * Copies contents of file referenced by file1Name to file referenced by
     * file2Name. If file2 exists, it will be overwritten
     * 
     * @param file1Name
     * @param file2Name
     * 
     * @return true if copying was successful, false in case of error
     */
    public static boolean copy(String from, String to) {
        final int BUFF_SIZE = 100000;
        final byte[] buffer = new byte[BUFF_SIZE];
        
        // make sure that the folder for this file exists
        createFoldersForFile(to);
        
        InputStream in = null;
        OutputStream out = null;
        boolean success = false;
        try {
            try {
               in = new FileInputStream(from);
               out = new FileOutputStream(to);
               while (true) {
                  synchronized (buffer) {
                     int amountRead = in.read(buffer);
                     if (amountRead == -1) {
                        success = true;
                        break;
                     }
                     out.write(buffer, 0, amountRead); 
                  }
               }
            } finally {
               if (in != null) {
                  in.close();
               }
               if (out != null) {
                  out.close();
               }
            }
        } catch (IOException e) {
            EventHandler.handle(EventLevel.ERROR, 
                    "FileUtil.copy()", "",
                    "Error copying file.", e);
            success = false;
        }        
        return success;
     }
    
    /**
     * Copies all files from fromPath to toPath.
     * Ignores sub-directories 
     * 
     * @param sourceDir source direction path
     */
    public static void copyFolderContents(String fromPath, String toPath) {
    	File sourceDir = new File(fromPath);
    	
        if (!sourceDir.exists()) {
        	String msg = "Folder \"" + sourceDir.getAbsolutePath() + "\" does not exist!";   
            throw new RuntimeException(msg);
        }
        
        for (File file: sourceDir.listFiles()) {
            if (!file.isDirectory()) {
                copy(appendPath(fromPath, file.getName()), 
                		appendPath(toPath, file.getName()));
                EventHandler.handle(
                		EventLevel.INFO, "", "",
                        "Copying file: " + file.getName(), "");
            }
        }
    }
    
    /**
     * Takes filename as an argument and creates all necessary folders for 
     * storing this file
     * @param fileName
     */
    public static void createFoldersForFile(String fileName) {
    	File f = new File(fileName);
    	File dir = f.getParentFile();
    	if (dir != null && !dir.exists()) {
    		dir.mkdirs();
    	}
    }
    
    /**
     * 
     * @param refDirPath path of the reference directory
     * @param geneDirPath path of the generated directory
     * 
     * @return true if content of files from the reference 
     * directory contains if files with the same name
     * in generated directory
     */
    public static boolean checkDirFilesContents (String refDirPath, 
            String geneDirPath) {
        return checkDirFilesContents (refDirPath, geneDirPath, null, null, null);
    }

	/**
	 * Appends a string to a file. If a file with the same name is not existing,
	 * then it is created. The required directories are created also.
	 * 
	 * @param filename
	 *            Full output file path
	 * @param content
	 *            Content to written to the file
	 * 
	 */
	public static void appendFile(String filePath, String content) {
	    File outFile = new File(filePath);
	    File outDir = outFile.getParentFile();
	    try {
	        outDir.mkdirs();
	        FileWriter writer = new FileWriter(outFile, true);
	        writer.write(content);
	        writer.flush();
	        writer.close();
	    } catch (Exception e) {
	        EventHandler
	                .handle(EventLevel.CRITICAL_ERROR, "FileUtil.appendFile",
	                        "", "Error writing to file", e);
	    }
	}

	/**
	 * Seeks for a file with a given pattern in a given directory. Does not
	 * recurse into sub-directories. 
	 * 
	 * Raises an error, if multiple files match.
	 * 
	 * @param pattern
	 *            File name pattern
	 * @param directory
	 *            Directory, where to look
	 * 
	 */
	public static File findFileByPattern(final String pattern, String directory) {
	    File dir = new File(directory);
	    
	    FileFilter fileFilter = new FileFilter() {
	        public boolean accept(File file) {
	            return file.getName().matches(pattern);
	        }
	    };
	    
	    File[] files = dir.listFiles(fileFilter);
	    
	    if (files == null) {
	    	return null;
	    } else if (files.length > 1) {
	    	String msg = "findFileByPattern: Multiple matches for pattern" + pattern;
            throw new RuntimeException(msg);
	    } else if (files.length == 1) {
	    	return files[0];
	    } else {
	    	return null;
	    }
	}

}