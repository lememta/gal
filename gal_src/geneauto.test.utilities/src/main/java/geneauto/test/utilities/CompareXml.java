/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.test.utilities/src/main/java/geneauto/test/utilities/CompareXml.java,v $
 *  @version	$Revision: 1.11 $
 *	@date		$Date: 2011-07-07 12:23:58 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.test.utilities;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.components.XmlParser;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CompareXml {

	XmlParser parser;
	String file1;
	String file2;

	public CompareXml(String file1, String file2) {

		this.parser = XmlParser.newInstance();
		this.file1 = file1;
		this.file2 = file2;
	}

	/** Compares two nodes
	 * It is expected that the node (type) names match */
	private boolean compareNodeRec(Node n1, Node n2) {

		// assert that the node (type) names match
		if (!n1.getNodeName().equals(n2.getNodeName())) {
			EventHandler.handle(
					EventLevel.INFO, "CompareXML", "", 
					"Node names are different: " + n1.getNodeName() + "/"
					+ n2.getNodeName() + ": nodes do not have the same name");
			return false;
		}

		// do not compare TransformationHistory node
		// TODO Put to list: excludedNodes
		if ((n1.getNodeName().equals("TransformationHistory"))) {
			return true;
		}

		// check if nodes have same attributes
		if (!checkAttributes(n1, n2)) {
			return false;
		}

		// check if nodes have same child nodes
		if (!checkChildNodes(n1, n2)) {
			return false;
		}

		return true;
	}

	private boolean checkAttributes(Node n1, Node n2) {
		NamedNodeMap atts1 = n1.getAttributes();
		NamedNodeMap atts2 = n2.getAttributes();
		
		// Check the number of attributes.
		// We later iterate over the first one only, so this check is important.
		if (atts1 == null && atts2 == null) {
			return true;
		} else if (atts1 == null 
				|| atts2 == null 
				|| atts1.getLength() != atts2.getLength()) {
			EventHandler.handle(
					EventLevel.INFO, "CompareXML", "", 
					"Node '" + n1.getNodeName() + "' does not have the same number of " 
					+ "attributes in compared files.");
			return false;
		}
		
		// Iterate over attributes in Node 1
		for (int i = 0; i < atts1.getLength(); i++) {
			Node attr1 = atts1.item(i);
			Node attr2 = atts2.getNamedItem(attr1.getNodeName());
			if (attr2 == null) {				
				EventHandler.handle(
						EventLevel.INFO, "CompareXML", "", 
						"Node '" + n1.getNodeName() + "' attribute '" 
						+ attr1.getNodeName() + "' exists in File1, but not in the " 
						+ "corresponding node in File2.");
				return false;
			}
			
			if (!attr1.getNodeValue().equals(attr2.getNodeValue())) {
				EventHandler.handle(
						EventLevel.INFO, "CompareXML", "", 
						"Node '" + n1.getNodeName() + "' attribute '" 
						+ attr1.getNodeName() + "' values differ in compared files.");
				return false;
			}		
		}
		return true;
	}

	private String getAttributeValue(Node n, String attributeName) {
		NamedNodeMap atts = n.getAttributes();
		
		if (atts == null) {
			return null;
		} else {
			Node node = atts.getNamedItem(attributeName);
			if (node == null) {
				return null;
			} else {
				return node.getNodeValue();
			}
		}
	}

	private boolean checkChildNodes(Node n1, Node n2) {
		NodeList nList1 = n1.getChildNodes();
		NodeList nList2 = n2.getChildNodes();		
		
		// Check the number of attributes.
		// We later iterate over the first one only, so this check is important.
		if (nList1.getLength() != nList2.getLength()) {
			EventHandler.handle(
					EventLevel.INFO, "CompareXML", "",
					"Node '" + n1.getNodeName() + "' does not have the same number of " 
					+ "children in compared files.");
			return false;
		}
		
		// Put children of Node2 to a map for efficient access by name
		// Suffix the node's name (effectively the node type name) by the "name" and 
		// "id" attributes, if present
		Map<String, Node> nMap2 = new HashMap<String, Node>();
		for (int i = 0; i < nList2.getLength(); i++) {
			Node n = nList2.item(i);
			String name = n.getNodeName() 
				    + ":" + getAttributeValue(n, "id")
		            + ":" + getAttributeValue(n, "name");
			nMap2.put(name, n);
		}

		// Iterate over the children on Node1			
		for (int i = 0; i < nList1.getLength(); i++) {
			Node childNode1 = nList1.item(i);
			String name = childNode1.getNodeName() 
				    + ":" + getAttributeValue(childNode1, "id")
		            + ":" + getAttributeValue(childNode1, "name");
			Node childNode2 = nMap2.get(name);
			if (childNode2 == null) {				
				EventHandler.handle(
						EventLevel.INFO, "CompareXML", "", 
						"Node '" + n1.getNodeName() + "' child node '" 
						+ name + "' exists in File1, but not in the " 
						+ "corresponding node in File2.");
				return false;
			}

			// Compare the children recursively
			if (!compareNodeRec(childNode1, childNode2)) {
				// Error message already given
				return false;
			}		
		}
		return true;
	}

	public boolean compareFiles(){

		Document doc1 = parser.parseWithDom(file1);
		Document doc2 = parser.parseWithDom(file2);

		return compareNodeRec(doc1, doc2);
	}


	public static void main(String[] args){

		if(args.length != 2) {			
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR, "CompareXML", "", 
						"Bad number of parameters, " +
						"command line is : CompareXml file1 file2");
		}

		String file1;
		String file2;

		file1 = args[0];
		file2 = args[1];

		CompareXml comparator = new CompareXml(file1,file2);

		if(comparator.compareFiles()){
			EventHandler.handle(
					EventLevel.INFO, "CompareXML", "","Files are identical"
					+ "\n File1: " + file1
					+ "\n File2: " + file2);
		}
		else{
			EventHandler.handle(
					EventLevel.INFO, "CompareXML", "","Files are different"
					+ "\n File1: " + file1
					+ "\n File2: " + file2);
		}
	}

}
