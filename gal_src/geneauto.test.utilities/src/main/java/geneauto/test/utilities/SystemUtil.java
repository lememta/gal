/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.test.utilities/src/main/java/geneauto/test/utilities/SystemUtil.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2012-02-05 20:59:52 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.test.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


/**
 * Utility methods related to the operating system.
 * 
 * Note! Error handling in this package is different from that in
 * geneauto.utils. We cannot use the same event handler as in geneauto.utils.
 * Instead we use methods from junit.Assert, since this class is to be used for
 * JUnit tests only.
 */
public class SystemUtil {

    /**
     * Launches an external process and copies its output to the System out.
     * 
     * @param args
     *            arguments to the process
     * @param workingDirectory
     *            if not null, then sets the processbuilder to this directory
     * @return result code from the process
     * @throws IOException
     *             when the process cannot be launched
     * @throws InterruptedException 
     * 				if the current thread is interrupted by another 
     * 				thread while it is waiting, then the wait is ended and 
     * 				an InterruptedException is thrown.
     */
    public static int runProcess(String[] args, String workingDir) throws IOException, InterruptedException {
    	int res;
    	
    	System.out.println("");
    	System.out.print("> ");
    	for (String arg : args)
    		System.out.print(arg + " ");
    	System.out.println("");
    	
        ProcessBuilder pb = new ProcessBuilder(args);
        if (workingDir != null) {
            pb.directory(new File(workingDir));
        }
        pb.redirectErrorStream(true);
        Process p = pb.start();
        BufferedReader input = new BufferedReader(new InputStreamReader(p
                .getInputStream()));
        String line;
        while ((line = input.readLine()) != null) {
            System.out.println(line);
        }
        
        /* If there was no more output, make sure the process has really
         *  finished. If the running process does not produce any output, 
         *  it can easily happen that exitValue is requested from process 
         *  before it is finished
         */
        p.waitFor();
        
        // check the buffer once more
        while ((line = input.readLine()) != null) {
            System.out.println(line);
        }
        
        input.close();
        
        res = p.exitValue();
        return res;
    }
    
    /**
     * @see runProcess(..)
     */
    public static int runProcess(List<String> argList) throws IOException, InterruptedException {
        return runProcess(argList, null);
    }

    /**
     * @see runProcess(..)
     */
    public static int runProcess(List<String> argList, String workingDir) throws IOException, InterruptedException {
        String[] args = (String[]) argList.toArray(new String[argList.size()]);
        return runProcess(args, workingDir);
    }

    /**
     * Prints error message in case of error in testing
     * First flushes System.out and then writes the message to system.err
     * 
     * @param e Exception
     */
    public static void printError(Exception e){    	
	    System.out.flush();
	    //System.err.print(e.getMessage());
	    e.printStackTrace();
    }

}