package geneauto.test.utilities;


public class TestUtils {
	
	/**
	 * 
	 * @param launcherPath path of the related launcher
	 * @param modelPath path of the related model (it is assumed that 
	 * 		every model is in  [launcherPath]/src/test/resources/[modelPath]
	 * @return arguments for launching the launcher
	 */
	public static String[] getArgs(String launcherPath, String modelPath) {
		String modelFullPath = launcherPath + "src/test/resources/"
			+ modelPath;
		String args[] = {
				modelFullPath, 
				"-b",
				"../geneauto.galauncher/blocklibrary.gbl.xml",
				"--tempModel", 
				"all"};
		return args;
	}
}
