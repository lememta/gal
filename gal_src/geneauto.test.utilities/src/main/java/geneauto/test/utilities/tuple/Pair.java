/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.test.utilities/src/main/java/geneauto/test/utilities/tuple/Pair.java,v $
 *  @version	$Revision: 1.3 $
 *	@date		$Date: 2009-11-10 09:58:44 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.test.utilities.tuple;

/**
 * 
 * A structure containing two elements
 * 
 *
 * @param <T1> Type of the left element
 * @param <T2> Type of the right element
 */
public class Pair<T1, T2> {
    
    private T1 left;
    private T2 right;
    
    public Pair(T1 left, T2 right) {
        this.setLeft(left);
        this.setRight(right);
    }

	public void setLeft(T1 left) {
		this.left = left;
	}

	public T1 getLeft() {
		return left;
	}

	public void setRight(T2 right) {
		this.right = right;
	}

	public T2 getRight() {
		return right;
	}

    @Override
    public String toString() {
        return "<" + left + ", " + right + ">";
    }	

}
