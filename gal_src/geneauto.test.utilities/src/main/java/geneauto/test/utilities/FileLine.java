package geneauto.test.utilities;

/**
 * @author Andrey Rybinsky
 *
 */
public class FileLine {
	
	/**
     * True, when current line is part of a comment
     */
    private boolean isComment;
    /**
     * Flag that shows that current file line has unclosed 
     * comment (compound comment start without compound comment 
     * end at the same line)
     */
    private boolean hasUnclosedComment;
    /**
     * String with all characters that shall be taken 
     * into account when comparing file lines
     */
    private String lineString;
    
    public FileLine(String lineString, boolean hasUnclosedComment, boolean isComment) {
        this.hasUnclosedComment = hasUnclosedComment;
        this.isComment = isComment;
        this.lineString = lineString;
    }
    
    public FileLine(String lineString) {
        this(lineString, false, false);
    }

    public boolean isComment() {
        return isComment;
    }
    
    public void setComment(boolean isComment) {
		this.isComment = isComment;
	}

    public boolean hasUnclosedComment() {
        return hasUnclosedComment;
    }
    
    public void setHasUnclosedComment(boolean hasUnclosedComment) {
        this.hasUnclosedComment = hasUnclosedComment;
    }
    
    public String getLineString() {
        return lineString;
    }
    
    public void setLineString(String lineString) {
        this.lineString = lineString;
    }

	@Override
	public String toString() {
		return "[isComment=" + isComment + ", hasUnclosedComment=" + hasUnclosedComment + "] \"" + lineString + "\"";
	}
    
    
}
