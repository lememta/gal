package geneauto.emf.launcher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Simulink2GAESMFolder {

	private static List<File> mFiles;
	private static List<File> mdlFiles;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		mdlFiles = new ArrayList<File>();
		mFiles = new ArrayList<File>();
		
		File modelsFolder = new File(args[0]);
		
		mFiles = findFiles(modelsFolder, ".m");
		mdlFiles = findFiles(modelsFolder, ".mdl");
		
		for (File model : mdlFiles) {
			String mFile = "";
			if (!mFiles.isEmpty()){
				mFile = mFiles.get(0).getAbsolutePath();
				String[] command = {model.getAbsolutePath(),"-m",mFile};
				Simulink2GAESM.main(command);
			}else{
				String[] command = {model.getAbsolutePath()};
				Simulink2GAESM.main(command);
			}
		}
		
		
	}
	
	public static List<File> findFiles(File modelsFolder, String extension){
		List<File> lstResult = new ArrayList<File>();
		if (modelsFolder != null) {
			for (final File fileEntry : modelsFolder.listFiles()) {
		        if (fileEntry.isDirectory()) {
		        	findFiles(fileEntry,extension);
		        } else {
		            String filePath = fileEntry.getAbsolutePath();
		            if (filePath.endsWith(extension)){
		            	lstResult.add(fileEntry);
		            }
		        }
		    }
		}
		return lstResult;
	}
}
