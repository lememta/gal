/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.launcher;

import geneauto.emf.utilities.GAEMFSysInfo;
import geneauto.launcher.GALauncher;

/**
 * Converts a Simulink/Stateflow models belonging to the language subset
 * supported by Gene-Auto to the Gene-Auto EMF based (Ecore conformant) format.
 */
public class Simulink2GAESM extends GALauncher {
	
    // Tool name
    public final static String TOOL_NAME = "Simulink2GAESM";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launcher = new Simulink2GAESMImpl();
		
		System.out.println(GAEMFSysInfo.getLicenceMessage(TOOL_NAME));
		
		launcher.gaMain(args, TOOL_NAME);
	}

}
