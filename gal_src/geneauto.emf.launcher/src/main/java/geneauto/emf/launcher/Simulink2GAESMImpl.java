/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.launcher;

import geneauto.emf.gaxml2gaesm.main.GAXML2Ecore;
import geneauto.emf.utilities.GAEMFConst;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.launcher.GALauncherImpl;
import geneauto.test.utilities.FileUtil;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;
import java.io.File;
import java.io.IOException;

/**
 * Implements the functionality required by Simulink2GA
 */
public class Simulink2GAESMImpl extends GALauncherImpl {
	
	/** Location of the converted model */
	private String outputFile;

	// To know if there is a stateflow part or not in the input model
	//private boolean containsCharts = true;
		
	/**
	 * @return the name of the launcher that runs this LauncherImpl.
	 * 
	 * NOTE! Override this in implementing subclasses, where this does
	 * not produce the right launcher package name!
	 */
	@Override
	protected String getLauncherPackage() {
		return getClass().getPackage().getName();
	}
	
   /** Executes the required elementary tools */
    public void runTools() {
    	
    	// Prepare the output file
    	prepareOutputFile();

        // 1 TSimulinkImporter
        runTSimulinkImporter();

        // 2 TFMPreProcessor
        runTFMPreProcessor();

        // 3 TStateflowImporter
        runTStateflowImporter();
        
        // 4 TBlockSequencer
        runTBlockSequencer();

        // 5 TTyper
        runTTyper();
        
    	// 6 TSMExpansionMechanism
    	runTSMExpansionMechanism();
    	
    	// 6.1 TSMRefactoring
    	runTSMRefactoring();
    	
    	// 7 GAXML2Ecore
    	runGAXML2GAESM();
    	
    	// 8 TGAESMPostProcessor
    	//runTGAESMPostProcessor();
    	
    }

	private void runTSMRefactoring() {
		
		stepNo++;
        tmpCmdln.clear();
        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
        		+ "_" + GAConst.SUFFIX_SMLUSPR + "." + GAConst.EXT_GSM);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
        
        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

        geneauto.tsmrefactoring.main.SMRefactoring.main(strCmdln);

        stopOnError();
	
	}

	private void runTSMExpansionMechanism() {
		
		stepNo++;
        tmpCmdln.clear();
        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
        		+ "_" + GAConst.SUFFIX_SMLUSPR + "." + GAConst.EXT_GSM);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
        
        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

        geneauto.tsmexpansionmechanism.main.SMExpansionMechanism.main(strCmdln);

        stopOnError();
        
	}

	protected void runTBlockSequencer(){
    	
        stepNo++;
        tmpCmdln.clear();
        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName 
        		+ "_" + GAConst.SUFFIX_FMBS + "." + GAConst.EXT_GSM);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

        geneauto.tblocksequencer_coq.main.BlockSequencerCoq.main(strCmdln);
        
        stopOnError();
    }	

    protected void runTTyper() {

        stepNo++;
        tmpCmdln.clear();
        ArgumentReader.addArgToCmdLine(tmpCmdln, "", quoteLastSMFile());
        lastSMFile = FileUtil.appendPath(tempPath, stepNo + "-" + modelName
        		+ "_" + GAConst.SUFFIX_FMTY + "." + GAConst.EXT_GSM);
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", quoteLastSMFile());
        ArgumentReader.addArgToCmdLine(tmpCmdln, "-b", blockLibraryFile);
        ArgumentReader.addFlagToCmdLine(tmpCmdln, "-d", debugMode);
        strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

        geneauto.typer.main.Typer.main(strCmdln);

        stopOnError();
    }

//	private void runTGAESMPostProcessor() {
//		stepNo++;
//		tmpCmdln.clear();
//		
//		String inputFile = outputFile;
//		String outputFile = URI.createFileURI(inputFile).
//				trimFileExtension().
//				appendFileExtension(GAESMPostprocessorConst.EXT_GAESM_POST).toFileString();
//		
//		ArgumentReader.addArgToCmdLine(tmpCmdln, "", inputFile);
//		ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", outputFile);
//		strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);
//
//		TGAESMPostprocessor.main(strCmdln);
//
//		stopOnError();
//	}

	protected void runGAXML2GAESM() {

		stepNo++;
		tmpCmdln.clear();
		ArgumentReader.addArgToCmdLine(tmpCmdln, "", lastSMFile);
		ArgumentReader.addArgToCmdLine(tmpCmdln, "-o", outputFile);
		strCmdln = tmpCmdln.toArray(new String[tmpCmdln.size()]);

		GAXML2Ecore.main(strCmdln);

		stopOnError();
    }

	/** Determine the location for the output file and delete old, if exists */
	private void prepareOutputFile() {
		
		// Because the current launcher has same parameters as the main
		// Gene-Auto launcher, then the user cannot give explicitly the output
		// file (this option could be easily added though), but he/she could
		// potentially assign an output directory. If it was set by the user, 
		// we write the converted model there. Otherwise, we write it in the
		// directory, where the input model is.
		if (ArgumentReader.getParameter(GAConst.ARG_OUTPUTFOLDER_MODE).equals("USER")) {    		
			// Take the specified output directory
			String dir = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFOLDER);		
			outputFile = FileUtil.appendPath(dir, modelName + "." + GAEMFConst.EXT_GAESM);    		
		} else {
			// Take the input model's directory
			outputFile = FileUtil.appendPath(inputFolder  , modelName + "." + GAEMFConst.EXT_GAESM);
		}
		
		File fOut = new File(outputFile);
		File fIn  = new File(ArgumentReader.getParameter(GAConst.ARG_INPUTFILE));
		
		// Check that the computed output file is not equal to the input file
		try {
			if (fIn.getCanonicalPath().equals(fOut.getCanonicalPath())) {
				EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
						"The computed filename and location of the output file is the " +
						"same as that of the input file:\n  " + outputFile);
			}
		} catch (IOException e) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
					"Cannot determine the canonical path of a file", e);
		}
		
		// Try to delete the output file, if it exists		
		if (fOut.exists() && !fOut.delete()) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
					"Could not delete old file:\n  " + outputFile);
		}
	}
}
