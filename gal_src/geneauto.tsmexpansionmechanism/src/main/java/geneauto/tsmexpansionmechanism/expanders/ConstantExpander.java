/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ConstantTyper.java,v $
 *  @version	$Revision: 1.39 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmexpansionmechanism.expanders;

import java.util.ArrayList;
import java.util.List;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;

/**
 * This class allows to type and check types of Constant Blocks of a
 * GASystemModel.
 * 
 */
public class ConstantExpander extends GenericExpander {

    public ConstantExpander(SMExpansionMechanismTool machine) {
		super(machine);
	}

	/**
     * Assign types according to the parameter Value of the Constant Block. If
     * value is null, then the propagation must not be null, else the
     * ConstantTyper raises an error. Base type is defined by the value
     * expression ("Value" parameter) associated to the block - If base type is
     * matrix type and the "VectorParams1D" is activated and the matrix is a
     * single row/column then the output type is a vector - Else the output type
     * is the base type
     * 
     * @param block
     *            The Block which is to be typed.
     * @returns true if the typing was executed properly.
     */
    public boolean expandVariables(Block block) {
        
    	Parameter param = block.getParameterByName("VectorParams1D");
    	StringExpression vectorParamValue = (StringExpression) param.getValue();
    	Parameter valueParam = block.getParameterByName("Value");
    	Expression valueParamExp = valueParam.getValue();
    	
    	if (!(valueParamExp instanceof VariableExpression ||
    		  valueParamExp instanceof LiteralExpression ||
    		  valueParamExp instanceof ListExpression)){
  			EventHandler.handle(
  					EventLevel.ERROR, 
  					"Constant block expander", 
  					"", 
  					"Complex Value parameter in Constant block are not handled. Block: " + block.getReferenceString());
  		}
    	
    	if (valueParamExp.getDataType().isMatrix() && vectorParamValue.getLitValue().equals("on")){
			IntegerExpression firstDim = (IntegerExpression) valueParamExp.getDataType().getDimensions().get(0);
			IntegerExpression secondDim = (IntegerExpression) valueParamExp.getDataType().getDimensions().get(1);
			if (secondDim.getLitValue().equals("1") || firstDim.getLitValue().equals("1")){
				// Here we need to delete the first dimension and get the second one instead
				if (valueParamExp instanceof VariableExpression){
					VariableExpression varExp = (VariableExpression) valueParamExp;
					for (int i = 0; i< varExp.getDataType().getDimensions().size(); i++){
						if (((IntegerExpression)varExp.getDataType().getDimensions().get(i)).getLitValue().equals("1")){
							varExp.getDataType().getDimensions().remove(i);
							i--;
						}
					}
					Variable_SM var = (Variable_SM) varExp.getVariable();
					List<Expression> lstExp = new ArrayList<Expression>();
					if (firstDim.getLitValue().equals("1")){
						var.setInitialValue(var.getInitialValue().getExpressions().get(0));
					} else {
						for (Expression exp : var.getInitialValue().getExpressions()){
	    					lstExp.add(exp.getExpressions().get(0));
	    				}
						var.setInitialValue(new GeneralListExpression(lstExp));
						var.getInitialValue().setDataType(varExp.getDataType().getCopy());
					}
					for (int i = 0; i< var.getDataType().getDimensions().size(); i++){
						if (((IntegerExpression)var.getDataType().getDimensions().get(i)).getLitValue().equals("1")){
							var.getDataType().getDimensions().remove(i);
							i--;
						}
					}
				} else {
    				GeneralListExpression valueExpression = (GeneralListExpression) valueParamExp;
    				List<Expression> lstExp = new ArrayList<Expression>();
    				if (firstDim.getLitValue().equals("1")){
    					valueExpression = (GeneralListExpression) valueExpression.getExpressions().get(0);
    				}
    				for (Expression exp : valueExpression.getExpressions()){
    					lstExp.add(exp.getExpressions().get(0));
    				}
    				GeneralListExpression newExp = new GeneralListExpression(lstExp);
    				Expression finalDim = null;
    				finalDim = valueExpression.getDataType().getDimensions().get(0).getCopy();
    				finalDim.setParent(newExp.getDataType());
    				TArray dt = new TArray();
    				dt.setBaseType(valueExpression.getDataType().getPrimitiveType().getCopy());
    				dt.getBaseType().setParent(dt);
    				dt.addDimension(finalDim);
    				dt.getDimensions().get(0).setParent(dt);
    				dt.setParent(newExp);
    				newExp.setDataType(dt);
    				valueParam.setValue(newExp);
				}
			}
    	}
//        if (propagation.equals("")
//                || propagation.equals("Inherit from 'Constant value'")) {
//            // If signal data type is not specified or is "Inherit from
//            // 'Constant
//            // value'" the data type is taken from parameter value
//            outportTypeCpy = valueType.getCopy();
//
//        } else {
            // Otherwise type is retrieved from OutDataTypeMode parameter and
            // Value parameter

//        GADataType propagatedType = DataTypeAccessor
//                .getDataType(propagation);
//
//        if (!outportTypeCpy.equalsTo(valueType)){
//        	
//        }
//        if (valueType instanceof TPrimitive) {
//            // if Value signal type is primitive, output type is fully
//            // retrieved
//            // from OutDataTypeMode parameter
//            outportTypeCpy = propagatedType.getCopy();
//        } else if (valueType instanceof TArray) {
//            // if Value signal type is Array, output signal type is
//            // retrieved from Value parameter and output data type from
//            // OutDataTypeMode parameter
//            ((TArray) valueType).setBaseType((TPrimitive) propagatedType);
//            outportTypeCpy = valueType.getCopy();
//        } else {
//            EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
//                    .getCanonicalName(), "", "Type of parameter "
//                    + value.getName() + "in block "
//                    + block.getReferenceString() + " can't be determined.",
//                    "");
//        }
////        }
//
//        Assert.assertNotNull(outportTypeCpy,
//                "Value of constant parameter has null data type.");
//
//        if (vectorParams && outportTypeCpy.isMatrix()) {
//            // if vectorParams is true and value is a vector matrix
//            // (row or column matrix) then the output is normalised
//            outportTypeCpy = outportTypeCpy.normalize(true);
//        }
//
        block.getOutDataPorts().get(0).setDataType(valueParamExp.getDataType());
//
//        EventHandler.handle(EventLevel.DEBUG, this.getClass()
//                .getCanonicalName(), "", block.getReferenceString()
//                + "'s output port was assigned type "
//                + DataTypeAccessor.toString(block.getOutDataPorts().get(0)
//                        .getDataType()), "");
        return true;

    }

	public boolean strongTyping(Block block){
    	return true;
    }
}
