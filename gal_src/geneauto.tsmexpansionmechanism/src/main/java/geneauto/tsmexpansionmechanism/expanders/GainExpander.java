package geneauto.tsmexpansionmechanism.expanders;

/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/GainTyper.java,v $
 *  @version	$Revision: 1.33 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.CodeModelFactory;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.utilities.CodeModeUtilities;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;

/**
 * This class allows to type and check types of Gain Blocks of a GASystemModel.
 * 
 */
public class GainExpander extends GenericExpander {

	public GainExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Expand the variables values used as parameter of the block
	 * 
	 * @param block
	 *            the block which is to be typed.
	 */
	public boolean expandVariables(Block block) {

		// the typer use BinaryOperator class to calculate a type
		// according to input type, gain value type and operator
		Parameter gainParam = block.getParameterByName("Gain");
		Parameter mulModeParam = block.getParameterByName("Multiplication");

		String mulMode = "";

		if (mulModeParam != null) {
			mulMode = ((StringExpression) mulModeParam.getValue())
					.getLitValue();
		}

		Expression gainExp = gainParam.getValue();
		
		if (!(gainExp instanceof VariableExpression ||
			  gainExp instanceof LiteralExpression ||
			  gainExp instanceof ListExpression)){
			EventHandler.handle(
					EventLevel.ERROR, 
					"Gain block expander", 
					"", 
					"Complex Gain parameter in Gain block are not handled. Block: " + block.getReferenceString());
		}
		
		GADataType gainDTCpy = gainExp.getDataType().getCopy();
		GADataType inputDTCpy = block.getInDataPorts().get(0).getDataType()
				.getCopy();
		GADataType outDTCpy = block.getOutDataPorts().get(0).getDataType()
				.getCopy();
		
		if (mulMode.equals("") || mulMode.equals("Element-wise(K.*u)")) {
			// default case is Element-wise product
			
			if (!outDTCpy.equalsTo(inputDTCpy)){
				block.getInDataPorts().get(0).setDataType(outDTCpy.getCopy());
			}
			if (!outDTCpy.equalsTo(gainDTCpy)){
		        if (gainExp instanceof VariableExpression){
		        	convertValueTo((VariableExpression) gainExp, outDTCpy);
		        }else if (gainExp instanceof LiteralExpression){
		        	convertLitExpTo((LiteralExpression) gainExp, outDTCpy);
		        }
		        //gainParam.setDataType(gainParam.getValue().getDataType());
	        }

		} else if (mulMode.equals("Matrix(K*u)")) {
			// Matrix product case with Gain value in left branch
			
			if (!gainDTCpy.isScalar()){
				
			}

		} else if (mulMode.equals("Matrix(u*K)")) {
			// Matrix product case with Gain value in right branch
			// We decide for the moment to do nothing
			
		} else if (mulMode.equals("Matrix(K*u) (u vector)")) {
			// Scalar product case with vector input value
			
			// If the input is a scalar we need to expand it to vector
			if (inputDTCpy.isScalar() && !gainDTCpy.isScalar()) {
				GADataType newInDT = new TArray(
					new IntegerExpression(
						gainDTCpy.getDimensionsLength(1)
					), 
					inputDTCpy.getPrimitiveType()
				);
				block.getInDataPorts().get(0).setDataType(newInDT);
			}
			
			if (gainDTCpy.isScalar()){
				
			}
			
//			EventHandler.handle(EventLevel.ERROR, this.getClass()
//					.getCanonicalName(), "",
//					"Matrix(K*u) (u vector) gain is not handled yet"
//							+ "\nBlock: " + block.getReferenceString());

//			BinaryOperator op = BinaryOperator.MUL_MAT_OPERATOR;
//			type = op.getDataType(gainDTCpy, inputDTCpy, false);
//
//			if (type != null) {
//				block.getOutDataPorts().get(0).setDataType(type);
//			} else {
//				EventHandler.handle(EventLevel.ERROR, this.getClass()
//						.getCanonicalName(), "",
//						"Input type and gain parameter type are not compatible."
//								+ "\nBlock: " + block.getReferenceString());
//			}
//
//			EventHandler.handle(
//					EventLevel.DEBUG,
//					this.getClass().getCanonicalName(),
//					"",
//					block.getReferenceString()
//							+ "'s output port was assigned type "
//							+ DataTypeAccessor.toString(block.getOutDataPorts()
//									.get(0).getDataType()));
		}

		return true;
	}

	public boolean strongTyping(Block block){
    	return true;
    }
}