package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/RelationalOperatorTyper.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;

import java.util.ArrayList;
import java.util.List;

/**
 * This class allows to type and check types of RelationalOperator Blocks of a
 * GASystemModel.
 * 
 */
public class RelationalOperatorExpander extends GenericExpander {

    public RelationalOperatorExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

	/**
     * Assign a TBoolean type to the output port data type
     */
    public boolean expandVariables(Block block) {
        // Checks the correct number of input ports
        if (DataTypeAccessor.checkNbIO(block, 2, 1)) {

           /* if (DataTypeAccessor.checkSamePrimitiveTyping(block
                    .getInDataPorts())) {*/
            List<GADataType> dtList = new ArrayList<GADataType>();    
            for (InDataPort inPort: block.getInDataPorts()) {
                if (inPort.getDataType() != null) {
                    dtList.add(inPort.getDataType());
                }
            }
            GADataType leastCommonType = DataTypeUtils.chooseLeastCommonType(dtList, true);
            
            for (InDataPort inDP : block.getInDataPorts()) {
				if (!inDP.getDataType().equalsTo(leastCommonType)){
					inDP.setDataType(leastCommonType);
				}
			}
        } else {
            // No input/output port error.
            EventHandler.handle(EventLevel.ERROR,
                    "RelationalOperatorTyper - Typing error", "", block
                            .getReferenceString()
                            + " must have 2 inputs and 1 output.", "");
            return false;
        }
        return true;
    }

    /**
     * At the end of the typing, all ports of all system's blocks must be typed.
     * 
     * @param block
     *            the block which is to be validated.
     */
    public boolean strongTyping(Block block) {
        boolean result = true;

        // check that all ports have been typed.
        for (InDataPort inport : block.getInDataPorts()) {
            if (inport.getDataType() == null) {
                result = false;
            }
        }
        if (block.getOutDataPorts().get(0).getDataType() == null) {
            result = false;
        }

        if (!result) {
            EventHandler.handle(EventLevel.ERROR,
                    "RelationalOperatorTyper - Validation error", "", "Block "
                            + block.getReferenceString()
                            + " has at least one untyped port.", "");
        }
        
        if (result){
        	GADataType common = DataTypeUtils.chooseLeastCommonType(block.getInDataPorts().get(0).getDataType(), block.getInDataPorts().get(1).getDataType(), true);
        	
        	// Manage inputs DataTypes
        	for (InDataPort inDP : block.getInDataPorts()) {
				if (!inDP.getDataType().equalsTo(common)){
					inDP.setDataType(common.getCopy());
				}
			}
        	
        	// Manage output DataType
        	GADataType previousOutDT = block.getOutDataPorts().get(0).getDataType();
        	GADataType expectedDT = common.getCopy();
        	if (expectedDT.isScalar()){
        		expectedDT = new TBoolean();
        	}else{
        		((TArray) expectedDT).setBaseType(new TBoolean());
        	}
        	
        	if (!previousOutDT.equalsTo(expectedDT)){
        		block.getOutDataPorts().get(0).setDataType(expectedDT);
        	}
        	
        }
        
        return result;
    }

}
