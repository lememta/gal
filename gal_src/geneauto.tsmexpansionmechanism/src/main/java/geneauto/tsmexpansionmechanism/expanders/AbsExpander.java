package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/LogicTyper.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of Logic Blocks of a GASystemModel.
 * 
 * 
 */
public class AbsExpander extends GenericExpander {

    public AbsExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

	/**
     * Logic has one or more inputs and one output. Its output type depends on
     * the inputs data and signal type. If they have all same signal type,
     * output has this signal type. If they are a mix between scalar and vector
     * (or scalar and matrix) output has same signal type and dimension than
     * vector inputs (or matrix inputs). All vector (or matrix) input must have
     * same dimension.
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean expandVariables(Block block) {

    	// --- CHECKING ---
    	// logic block must have at least one input
    	Assert.assertNotEmpty(block.getInDataPorts(),
                "block inputs",
                this.getClass().getCanonicalName() + ".assignTypes()",
                block.getReferenceString(),
                "");

    	// logic block must have exactly one output
    	Assert.assertSingleElement(block.getOutDataPorts(),
                "block outputs",
                this.getClass().getCanonicalName() + ".assignTypes()",
                block.getReferenceString(),
                "");
        
        return true;
    }

	public boolean strongTyping(Block block){
    	return true;
    }
}
