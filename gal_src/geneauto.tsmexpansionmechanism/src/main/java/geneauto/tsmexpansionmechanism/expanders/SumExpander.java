package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/SumTyper.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import java.util.ArrayList;
import java.util.List;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;

/**
 * This class allows to type and check types of Sum Blocks of a GASystemModel.
 * 
 */
public class SumExpander extends GenericExpander {

    public SumExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

    public boolean strongTyping(Block block){
    	
    	if (block.getInDataPorts().size() == 1){
    		typeForOneInput(block);
    	}else{
    		typeForMultipleInputs(block);
    	}
    	
    	return true;
    }
    
    public void typeForOneInput(Block block){
    	InDataPort inDP = block.getInDataPorts().get(0);
    	if (inDP.getDataType().isScalar()){
    		if (inDP.getDataType() instanceof TBoolean){
    			EventHandler.handle(EventLevel.CRITICAL_ERROR, 
    					"TSMExpansionMechanism.SumExpander", 
    					"", 
    					"Boolean scalar input is not allowed on this block\n" + 
    					block.getReferenceString());
    		}
    	}
    }
    
    public void typeForMultipleInputs(Block block){
    	List<GADataType> lstDTs = new ArrayList<GADataType>();
    	
    	for (InDataPort inDP : block.getInDataPorts()) {
			if (inDP.getDataType().isScalar()){
				lstDTs.add(inDP.getDataType());
			} else if (inDP.getDataType().isVector()){
				TArray array = (TArray) inDP.getDataType();
				lstDTs.add(array.getBaseType().getCopy());
			} else if (inDP.getDataType().isMatrix()) {
				TArray array = (TArray) inDP.getDataType();
				lstDTs.add(array.getBaseType().getCopy());
			} else {
				EventHandler.handle(EventLevel.CRITICAL_ERROR, 
    					"TSMExpansionMechanism.SumExpander", 
    					"", 
    					"An input is not of the right type\n" + 
    					inDP.getReferenceString());
			}
		}
    	
    	TPrimitive commonDT = (TPrimitive) DataTypeUtils.chooseLeastCommonType(lstDTs, true);
    	
    	TPrimitive generalDT = commonDT.getCopy();
    	if (commonDT.equalsTo(new TBoolean())){
    		generalDT = new TRealInteger();
    		block.getOutDataPorts().get(0).setDataType(generalDT.getCopy());
    	}
    	
    	for (InDataPort inDP : block.getInDataPorts()) {
    		if (inDP.getDataType().isScalar() && !(inDP.getDataType().equalsTo(generalDT))){
				inDP.setDataType(generalDT.getCopy());
			} else if (inDP.getDataType().isColMatrix()){
				TArray array = (TArray) inDP.getDataType();
				if (! array.getBaseType().equalsTo(generalDT)) {
					array.setBaseType(generalDT.getCopy());
				}
			} else if (inDP.getDataType().isMatrix()) {
				TArray array = (TArray) inDP.getDataType();
				if (! array.getBaseType().equalsTo(generalDT)) {
					array.setBaseType(generalDT.getCopy());
				}
			}
    	}
    }
    /**
     * Assign a type to the output port according to the datatype of the block's
     * inports. if the input ports have no datatype, then it is the execution of
     * the rule "back propagation". In this case, the output port must not be
     * null.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean expandVariables(Block block) {
        
    	//computeCommonDataType(block);
    	List<GADataType> lstDT = new ArrayList<GADataType>();
    	
    	for (InDataPort inDP : block.getInDataPorts()) {
			lstDT.add(inDP.getDataType());
		}
    	
    	GADataType outDT = DataTypeUtils.chooseLeastCommonType(lstDT, true);
    	
    	for (InDataPort inDP : block.getInDataPorts()) {
			if (!inDP.getDataType().equalsTo(outDT)){
				inDP.setDataType(outDT.getCopy());
			}
		}
    	
        return true;
    }

}
