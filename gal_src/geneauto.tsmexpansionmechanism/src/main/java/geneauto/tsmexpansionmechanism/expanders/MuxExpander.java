package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/SinkTyper.java,v $
 *  @version	$Revision: 1.3 $
 *	@date		$Date: 2009-11-10 09:58:35 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;

/**
 * This class allows to type and check types of Switch Blocks of a
 * GASystemModel.
 * 
 */
public class MuxExpander extends GenericExpander {

    public MuxExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

    public boolean strongTyping(Block block) {
    	
    	OutDataPort outDP = block.getOutDataPorts().get(0);
    	
//    	if (!outDP.getDataType().isMatrix()){
//    		if (outDP.getDataType().isVector()){
//    			IntegerExpression intExp = new IntegerExpression(1);
//    			intExp.setParent(outDP.getDataType());
//    			outDP.getDataType().getDimensions().add(intExp);
//    		}
//    	}
    	
    	return true;
    }
    
	/**
     * Cannot type the output port of Sink blocks because they do 
     * not have output ports.
     * 
     * @param block
     *            The block which is to be typed.
     */
    public boolean expandVariables(Block block) {
        // nothing to be done here
        return true;
    }

}
