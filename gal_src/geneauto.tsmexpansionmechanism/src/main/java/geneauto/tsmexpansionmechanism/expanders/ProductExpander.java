package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/ProductTyper.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import geneauto.blocklibrary.utils.ProductBackendUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;
import geneauto.utils.assertions.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * This class allows to type and check types of Product Blocks of a
 * GASystemModel.
 * 
 * TODO: (TF:556) there is currently too much duplication -- if valdiateTypes is
 * implemented properly, it does the same check that assigntTypes Review there
 * roles of both methods (ToNa 22/12/08)
 */
public class ProductExpander extends GenericExpander {

    public ProductExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

    public boolean strongTyping(Block block){
    	
    	if (block.getInDataPorts().size() == 1){
    		typeForOneInput(block);
    	}else{
    		typeForMultipleInputs(block);
    	}
    	
    	return true;
    }
    
    public void typeForOneInput(Block block){
    	InDataPort inDP = block.getInDataPorts().get(0);
    	if (inDP.getDataType().isScalar()){
    		if (inDP.getDataType() instanceof TBoolean){
    			EventHandler.handle(EventLevel.CRITICAL_ERROR, 
    					"TSMExpansionMechanism.ProductExpander", 
    					"", 
    					"Boolean scalar input is not allowed on this block\n" + 
    					block.getReferenceString());
    		}
    	}
    }
    
    public void typeForMultipleInputs(Block block){
    	List<GADataType> lstDTs = new ArrayList<GADataType>();
    	
    	for (InDataPort inDP : block.getInDataPorts()) {
			if (inDP.getDataType().isScalar()){
				lstDTs.add(inDP.getDataType());
			} else if (inDP.getDataType().isColMatrix()){
				lstDTs.add(inDP.getDataType().getCopy());
			} else if (inDP.getDataType().isMatrix()) {
				lstDTs.add(inDP.getDataType().getCopy());
			} else {
				lstDTs.add(inDP.getDataType().getCopy());
			}
		}
    	
    	
    	
    	TPrimitive commonDT = DataTypeUtils.chooseLeastCommonPrimitiveType(lstDTs);
    	
    	if (commonDT.equalsTo(new TBoolean())){
    		commonDT = new TRealInteger();
    		if (block.getOutDataPorts().get(0).getDataType().isScalar()){
    			block.getOutDataPorts().get(0).setDataType(commonDT.getCopy());
    		}else{
    			TArray array = (TArray) block.getOutDataPorts().get(0).getDataType();
    			array.setBaseType((TPrimitive) commonDT.getCopy());
    		}
    	}
    	
    	for (InDataPort inDP : block.getInDataPorts()) {
    		if (inDP.getDataType().isScalar()) {
    			if (!(inDP.getDataType().equalsTo(commonDT))){
    				inDP.setDataType(commonDT.getCopy());
    			}
			} else {
				TArray commonArray = (TArray) inDP.getDataType();
				commonArray.setBaseType(commonDT.getCopy());
			}
    	}
    }
    
    /**
     * Assign a type to the output port.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean expandVariables(Block block) {

        // checking number of inputs and outputs
        Assert.assertNotEmpty(block.getInDataPorts(),
                "Block has no outputs, expected one!\n "
                        + block.getReferenceString());

        Assert.assertNotEmpty(block.getOutDataPorts(),
                "Block has no inputs, expected one or more\n "
                        + block.getReferenceString());

        // reading block parameter
        Parameter paramMul = block.getParameterByName("Multiplication");
        Parameter paramInputs = block.getParameterByName("Inputs");

        Assert.assertNotNull(paramMul,
                "Parameter 'Multiplication' not defined: "
                        + block.getReferenceString());

        Assert.assertNotNull(paramInputs,
                "Parameter 'Inputs' not defined: "
                        + block.getReferenceString());

        // inputs as scalar TBoolean.
        // If all boolean then transform as TrealInteger
        
        // Verify that no use of the block as matrix multiplication for array inputs
//        if (((StringExpression)paramMul.getValue()).getLitValue().equals("Matrix(*)")){
//        	for (InDataPort inDP : block.getInDataPorts()) {
//				if (inDP.getDataType().isScalar()){
//					EventHandler.handle(EventLevel.CRITICAL_ERROR, 
//	    					"TSMExpansionMechanism.ProductExpander", 
//	    					"", 
//	    					"Array input is not allowed on this block. Must be a Matrix.\n" + 
//	    					block.getReferenceString());
//				}
//			}
//        }
        
        if ((paramMul.getStringValue().equals("") || 
        	paramMul.getStringValue().equals("Element-wise(.*)")) &&
        	block.getInDataPorts().size() > 1){
        	// ElementWise multiplication
        	GADataType outDT = block.getOutDataPorts().get(0).getDataType();
        	for (InDataPort inDP : block.getInDataPorts()) {
				if (!inDP.getDataType().equalsTo(outDT)){
					inDP.setDataType(outDT.getCopy());
				}
			}
        }else{
        	// Matrix multiplication
        	
        }

        return true;
    }

}
