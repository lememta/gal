package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/SystemTyper.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:17 $
 *
 *  Copyright (c) 2006-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import geneauto.blocklibrary.utils.BlockLibraryUtil;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;

/**
 * This class allows to type and check types of System Blocks of a
 * GASystemModel.
 * 
 */
public class SystemExpander extends GenericExpander {

    public SystemExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

	/**
     * //TODO complete specs of typing process.
     */
    public boolean expandVariables(Block block) {
        if (block instanceof SystemBlock) {
            SystemBlock system = (SystemBlock) block;

            for (OutDataPort port : system.getOutDataPorts()) {

                // get data type from the outport block (derived and specified
                // type)
                GADataType dt = getOutportType(port);

                if (dt == null) {
                    EventHandler.handle(EventLevel.CRITICAL_ERROR, this
                            .getClass().getCanonicalName(), "",
                            "Could not find the input type "
                                    + "for outport block \n"
                                    + "Block: "
                                    + port.getSourceBlock()
                                            .getReferenceString() + ".", "");
                } else {
                    port.setDataType(dt);
                }

            }

            return true;
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                    .getCanonicalName(), "",
                    "An attempt to type a block which is not a SystemBlock."
                            + "\n Please check that your block library is "
                            + "correctly configured.\n" + "Block: "
                            + block.getReferenceString(), "");
            return false;
        }
    }

	public boolean strongTyping(Block block){
    	return true;
    }
    
    /**
     * Derives data type from the block parameters associated to the 
     * given port
     * 
     * @param block
     * @return
     */
    private GADataType getOutportType(OutDataPort port) {
    	Block block = port.getSourceBlock();
        GADataType derivedDT = block.getInDataPorts().get(0).getDataType();
        
        return BlockLibraryUtil.getOutportType(block, 
        		derivedDT, 
        		this.getClass().getCanonicalName());
    }
}
