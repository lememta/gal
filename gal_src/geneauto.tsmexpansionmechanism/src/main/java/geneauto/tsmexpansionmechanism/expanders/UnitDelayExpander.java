package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/UnitDelayTyper.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:16 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.BooleanExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.FalseExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.ListExpression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.NumericExpression;
import geneauto.models.gacodemodel.expression.SingleExpression;
import geneauto.models.gacodemodel.expression.TrueExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TBoolean;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealFixedPoint;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gadatatypes.TRealSingle;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.utilities.DataTypeUtils;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;
import geneauto.utils.assertions.Assert;

/**
 * This class allows to type and check types of UnitDelay Blocks of a
 * GASystemModel.
 * 
 */
public class UnitDelayExpander extends GenericExpander {

    public UnitDelayExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

    public boolean strongTyping(Block block) {
    	
    	Parameter initialValue = block.getParameterByName("InitialValue");
    	Expression initialValueExp = initialValue.getValue();
    	
    	if (!(initialValueExp instanceof VariableExpression ||
    		  initialValueExp instanceof LiteralExpression ||
    		  initialValueExp instanceof ListExpression)){
  			EventHandler.handle(
  					EventLevel.ERROR, 
  					"UnitDelay block expander", 
  					"", 
  					"Complex InitialValue parameter in UnitDelay block are not handled. Block: " + block.getReferenceString());
  		}
    	
    	GADataType initialValueExpDT = initialValueExp.getDataType();
    	
    	InDataPort inDP = block.getInDataPorts().get(0);
    	GADataType inDPDT = inDP.getDataType();
    	
    	OutDataPort outDP = block.getOutDataPorts().get(0);
    	GADataType outDPDT = outDP.getDataType();
    	
    	GADataType resultDT = DataTypeUtils.chooseLeastCommonType(initialValueExpDT, inDPDT, true);
    	
    	if (resultDT == null){
    		EventHandler.handle(EventLevel.ERROR, 
    				"UnitDelayExpander", "", 
    				"Input DataType and initial value parameter DataType not compatibles");
    		return false;
    	}
    	
    	if (!initialValueExpDT.equalsTo(outDPDT)){
    		if (outDPDT instanceof TBoolean){
    			if (initialValueExp instanceof IntegerExpression){
    				IntegerExpression exp = (IntegerExpression) initialValueExp;
    				BooleanExpression newExp;
    				if (exp.getIntValue() == 0){
    					newExp = new FalseExpression();
    				}else{
    					newExp = new TrueExpression();
    				}
    				initialValue.setValue(newExp);
    			}else if (initialValueExp instanceof DoubleExpression ||
    					initialValueExp instanceof SingleExpression){
    				NumericExpression exp = (NumericExpression) initialValueExp;
    				BooleanExpression newExp;
    				if (exp.getRealValue() == 0.0){
    					newExp = new FalseExpression();
    				}else{
    					newExp = new TrueExpression();
    				}
    				initialValue.setValue(newExp);
    			}else{
    				EventHandler.handle(EventLevel.ERROR, 
    	    				"UnitDelayExpander", "", 
    	    				"Impossible to convert from " + initialValueExp.getReferenceString() + " type to TBoolean");
    	    		return false;
    			}
    		}else if (outDPDT instanceof TRealInteger){
    			if (initialValueExp instanceof DoubleExpression ||
    				initialValueExp instanceof SingleExpression){
    				NumericExpression exp = (NumericExpression) initialValueExp;
    				DoubleExpression newExp = new DoubleExpression(exp.getLitValue());
    				initialValue.setValue(newExp);
    			}else if (initialValueExp instanceof BooleanExpression){
    				BooleanExpression exp = (BooleanExpression) initialValueExp;
    				IntegerExpression newExp = new IntegerExpression(exp.getBooleanValue()?"0":"1");
    				initialValue.setValue(newExp);
    			}else if (initialValueExp instanceof IntegerExpression){
    				IntegerExpression exp = (IntegerExpression) initialValueExp;
    				IntegerExpression newExp = new IntegerExpression(exp.getIntValue());
    				initialValue.setValue(newExp);
    			}else{
    				EventHandler.handle(EventLevel.ERROR, 
    	    				"UnitDelayExpander", "", 
    	    				"Impossible to convert from " + initialValueExp.getReferenceString() + " type to TRealInteger");
    	    		return false;
    			}
    		}else if (outDPDT instanceof TRealDouble || outDPDT instanceof TRealSingle){
    			if (initialValueExp instanceof BooleanExpression){
    				BooleanExpression exp = (BooleanExpression) initialValueExp;
    				DoubleExpression newExp = new DoubleExpression(exp.getBooleanValue()?"0.0":"1.0");
    				initialValue.setValue(newExp);
    			}else if (initialValueExp instanceof IntegerExpression){
    				IntegerExpression exp = (IntegerExpression) initialValueExp;
    				DoubleExpression newExp = new DoubleExpression(exp.getLitValue()+".0");
    				initialValue.setValue(newExp);
    			}else{
    				EventHandler.handle(EventLevel.ERROR, 
    	    				"UnitDelayExpander", "", 
    	    				"Impossible to convert from " + initialValueExp.getReferenceString() + " type to TRealFixedPoint");
    	    		return false;
    			}
    		}else{
				EventHandler.handle(EventLevel.ERROR, 
	    				"UnitDelayExpander", "", 
	    				"Impossible to convert from " + initialValueExp.getReferenceString() + " type to AnyType");
	    		return false;
			}
    	}
    	
    	return true;
    }

	/**
     * Assign the same type to the output port as the input port's. 
     * If input type is not assigned the normalised form of type of 
     * InitialValue parameter is used.  
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean expandVariables(Block block) {
    	/*
    	 * CHECKING
    	 */
    	// Unit delay block must have exactly one input
    	Assert.assertSingleElement(block.getInDataPorts(), 
    				"InDataPorts",
    				this.getClass().getCanonicalName() + ".assignTypes", 
    				block.getReferenceString(), null);
    	
    	// Unit delay must have exactly one output
    	Assert.assertSingleElement(block.getOutDataPorts(), 
				"OutDataPorts",
				this.getClass().getCanonicalName() + ".assignTypes", 
				block.getReferenceString(), null);

    	Parameter initialValue = block.getParameterByName("InitialValue");
    	Expression initExp = initialValue.getValue();
    	
    	/*
    	 * PROCESSING
    	 */
        // get the input port:
        InDataPort inport = block.getInDataPorts().get(0);
        // slot for memorising new output type
        GADataType outputType = block.getOutDataPorts().get(0).getDataType();
        
        if (!inport.getDataType().equalsTo(outputType)){
        	inport.setDataType(outputType.getCopy());
        }
        
        if (!initialValue.getDataType().equalsTo(outputType)){
        	if (initialValue.getDataType() instanceof TPrimitive){
        		if (initExp instanceof VariableExpression){
		        	convertValueTo((VariableExpression) initExp, outputType.getCopy());
		        }else if (initExp instanceof LiteralExpression){
		        	convertLitExpTo((LiteralExpression) initExp, outputType.getCopy());
		        }
        	}
        }
        
        return true;
    }
}
