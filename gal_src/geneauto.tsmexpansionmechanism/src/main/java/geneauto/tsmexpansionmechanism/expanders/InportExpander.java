package geneauto.tsmexpansionmechanism.expanders;
/**
 * Gene-Auto code generator
 * 
 *  $Source: /cvsroot/geneauto/geneauto.blocklibrary/src/main/java/geneauto/blocklibrary/typers/InportTyper.java,v $
 *  @version    $Revision: 1.22 $
 *  @date       $Date: 2011-09-13 10:44:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *      http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *      http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *      Continental SAS
 *      Airbus France SAS
 *      Thales Alenia Space France SAS
 *      ASTRIUM SAS
 *      IB KRATES OU
 *      Barco NV
 *      Israel Aircraft Industries
 *      Alyotech
 *      Tallinn University of Technology
 *      FERIA - Institut National Polytechnique de Toulouse
 *      INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */


import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.tsmexpansionmechanism.utils.GenericExpander;

/**
 * This class allows to type and check types of Inport Blocks of a
 * GASystemModel.
 * 
 */
public class InportExpander extends GenericExpander {

    public InportExpander(SMExpansionMechanismTool machine) {
		super(machine);
		// TODO Auto-generated constructor stub
	}

	/**
     * Assign a dataType to the inport block according to its parameters
     * DataType and SignalType.
     * 
     * @param block
     *            the block which is to be typed.
     */
    public boolean expandVariables(Block block) {
    	// Nothing to do here
//
//        /*
//         * CHECKING
//         */
//        if ((block.getOutDataPorts() == null)
//                || (block.getOutDataPorts().size() != 1)) {
//
//            // Control signals entering
//            // a SubSystem via an Inport block are not accepted
//            if ((block.getOutControlPorts() != null)
//                    && (block.getOutControlPorts().size() > 0)) {
//                EventHandler.handle(EventLevel.ERROR, this.getClass()
//                        .getCanonicalName(), "",
//                        "Function-call entering as input is not allowed. \n See "
//                                + block.getReferenceString(), "");
//            } else {
//                EventHandler.handle(EventLevel.ERROR, this.getClass()
//                        .getCanonicalName(), "",
//                        "Block has not expected number of outputs : "
//                                + block.getReferenceString(), "");
//            }
//            return false;
//        }
//
//        Parameter paramDimension = block.getParameterByName("PortDimensions");
//        Parameter paramType = block.getParameterByName("DataType");
//
//        /*
//         * PROCESSING
//         */
//
//        // if PortDimensions and DataType are defined process the user-defined
//        // data type
//        if (paramDimension != null && paramType != null) {
//	        TPrimitive baseType = null;
//	        GADataType resultType = null;
//	
//	        String sbaseType = paramType.getStringValue();
//	        if (sbaseType.equals("auto")) {
//	            baseType = null;
//	        } else {
//	            try {
//	                baseType = (TPrimitive) DataTypeAccessor.getDataType(sbaseType);
//	            } catch (ClassCastException e) {
//	                EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
//	                        .getCanonicalName(), "",
//	                        "Can not convert value of parameter \"DataType\"=\""
//	                                + sbaseType + "\" to valid GADataType"
//	                                + "\n block " + block.getReferenceString(), e);
//	            }
//	        }
//	
//	        int intDim = 0;
//	        Expression dimension = null;
//	        if (paramDimension != null) {
//	            dimension = paramDimension.getCodeModelExpression();
//	
//	            if (dimension instanceof IntegerExpression) {
//	                intDim = ((IntegerExpression) dimension).getIntValue();
//	            } else if (dimension instanceof GeneralListExpression) {
//	                try {
//	                    resultType = new TArray(((GeneralListExpression) dimension)
//	                            .getExpressions(), baseType);
//	
//	                    // TODO: why to we assign type here immediately and for 
//	                    // some other cases defer for later. It would be nicer 
//	                    // to use the same style in all branches (ToNa 23/04/10) 
//	                    block.getOutDataPorts().get(0).setDataType(resultType);
//	                    return true;
//	                } catch (ClassCastException e) {
//	                    EventHandler.handle(EventLevel.CRITICAL_ERROR, 
//	                    		this.getClass().getCanonicalName(), "",
//	                            "Dimension parameter has not been well parsed: "
//	                                    + block.getReferenceString(), e);
//	                }
//	            } else {
//	                EventHandler.handle(EventLevel.CRITICAL_ERROR, 
//	                		this.getClass().getCanonicalName(), "",
//	                        "The \"PortDimensions\" property should " +
//	                        "contain only literal values." +
//	                        "\n Expression: "
//	                        + ((dimension==null)?"null":dimension.printElement())
//	                        + "\n Block: " + block.getReferenceString(), "");
//	            }
//	        } else {
//	            EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
//	                    .getCanonicalName(), "",
//	                    "The PortDimension parameter not defined in block\n "
//	                            + block.getReferenceString(), "");
//	        }
//	        
//	        if (baseType != null && intDim != -1) {
//	            if (intDim == 1) {
//	                resultType = baseType;
//	            } else {
//	                resultType = new TArray(dimension, baseType);
//	            }
//	            
//	            block.getOutDataPorts().get(0).setDataType(resultType);
//	        }
//        }
//
//        if (block.getOutDataPorts().get(0).getDataType() == null) {
//        	if (block.getParent().getParent() == null) {
//        		// port of a top-level subsystem
//                EventHandler.handle(EventLevel.ERROR, 
//                		this.getClass().getCanonicalName(), "", 
//                		"Inport type of the top-level system "
//                        + "must be explicitly set!\n Block: "
//                        + block.getReferenceString(), "");        		
//        	} else {
//                EventHandler.handle(EventLevel.ERROR, 
//                		this.getClass().getCanonicalName(), "", 
//                		"Unable to type output port of block:\n "
//                        + block.getReferenceString(), "");        		        		
//        	}
//            return false;
//        } 
//        
        return true;
    }

	public boolean strongTyping(Block block){
    	return true;
    }
}
