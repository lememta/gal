/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/main/FMPreprocessor.java,v $
 *  @version	$Revision: 1.19 $
 *	@date		$Date: 2011-11-29 17:40:56 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmexpansionmechanism.main;

import geneauto.eventhandler.EventHandler;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

/**
 * Main class of the FMPreprocessor tool. It reads the arguments and launches
 * the GASystemModel preprocessing.
 * 
 */
public class SMExpansionMechanism {

    public static String VERSION = "DEV";
    public static String TOOL_NAME = "SMExpansionMechanism";

    /**
     * Main method of the FMPreprocessor tool. It reads the program parameters
     * and launches the GASystemModel preprocessing.
     * 
     * @param args
     *            List of the arguments of the typer.
     */
    public static void main(String[] args) {

        // set root of all location strings in messages
        EventHandler.setToolName("SMExpansionMechanism");

        // initialise ArgumentReader
        ArgumentReader.clear();

        // set expected argument list
        ArgumentReader.addArgument("", GAConst.ARG_INPUTFILE, true, true,
                true, GAConst.ARG_INPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-o", GAConst.ARG_OUTPUTFILE, true, true,
                true, GAConst.ARG_OUTPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-b", GAConst.ARG_LIBFILE, false, true,
                true, GAConst.ARG_LIBFILE_NAME_ROOT);
        
        // ***** START: flag "--inline" ******
        ArgumentReader.addArgument("--inline", GAConst.ARG_INLINE, false,
                false, false, "");
        // ***** END: flag "--inline" ******
        ArgumentReader.addArgument("-d", GAConst.ARG_DEBUG, false, 
        		false, false, "");

        // set arguments list to ArgumentReader and parse the list
        SMExpansionMechanismTool.getInstance().initTool(args, true);

        runSMLustreModelPreprocessor();
    }

    /**
     * Runs the FMPreprocessor according to the given parameters.
     */
    private static void runSMLustreModelPreprocessor() {
        SMExpansionMechanismTool.getInstance().init();
    }
}
