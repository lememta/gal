package geneauto.tsmexpansionmechanism.utils;

import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;

public interface IGenericExpander {

	public boolean expandVariables(Block block);
	
	public boolean strongTyping(Block block);
}
