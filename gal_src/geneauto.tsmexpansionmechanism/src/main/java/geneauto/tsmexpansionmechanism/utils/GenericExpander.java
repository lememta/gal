package geneauto.tsmexpansionmechanism.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.LiteralExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.utilities.emlconverter.EMLAccessor;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;

public abstract class GenericExpander implements IGenericExpander{
	
	private GASystemModel gaSM;
	private SystemModelFactory smFactory;

	public GenericExpander(SMExpansionMechanismTool machine) {
		gaSM = machine.getSystemModel();
		smFactory = machine.getSystemModelFactory();
	}
	
	public void convertValueTo(VariableExpression varExp, GADataType outDT){
		
		Variable var = varExp.getVariable();
		Expression exp = var.getInitialValueExpression();
		
		String litValueVar = exp.getStringValue();
		
//		varExp.setDataType(outDT.getCopy());
//		var.setDataType(outDT.getCopy());
		
		if (varExp.getDataType().isScalar() && outDT.isVector()){
			String vectorLitValue = "[";
			for (int i = 0; i < outDT.getDimensionsLength(0); i++) {
				vectorLitValue += litValueVar + " ";
			}
			vectorLitValue += "]";
			
			var.setInitialValueExpression(EMLAccessor.convertEMLToCodeModel(vectorLitValue, true));
			var.setDataType(outDT.getCopy());
			
		}else if (varExp.getDataType().isScalar() && outDT.isMatrix()){
			
			String vectorLitValue = "[";
			for (int i = 0; i < outDT.getDimensionsLength(0); i++) {
				for (int j = 0; j < outDT.getDimensionsLength(1); j++) {
					vectorLitValue += litValueVar + " ";
				}
				if (i != outDT.getDimensionsLength(0)-1){
					vectorLitValue += " ; ";
				}
			}
			vectorLitValue += "]";
			
			Expression tmpExp = EMLAccessor.convertEMLToCodeModel(vectorLitValue, true);
			var.setInitialValueExpression(tmpExp);
			var.setDataType(outDT.getCopy());
		}
	}

	public void convertLitExpTo(LiteralExpression litExp, GADataType outDT){
		String litValueExp = litExp.getLitValue();
		
		if (litExp.getDataType().isScalar() && outDT.isVector()){
			
			String vectorLitValue = "[";
			for (int i = 0; i < outDT.getDimensionsLength(0); i++) {
				vectorLitValue += litValueExp + " ";
			}
			vectorLitValue += "]";
			
			litExp.replaceMe(EMLAccessor.convertEMLToCodeModel(vectorLitValue, true));
			litExp.setDataType(outDT.getCopy());
			
		}else if (litExp.getDataType().isScalar() && outDT.isMatrix()){
			
			String vectorLitValue = "[";
			for (int i = 0; i < outDT.getDimensionsLength(0); i++) {
				for (int j = 0; j < outDT.getDimensionsLength(1); j++) {
					vectorLitValue += litValueExp + " ";
				}
				if (i != outDT.getDimensionsLength(0)-1){
					vectorLitValue += " ; ";
				}
			}
			vectorLitValue += "]";
			
			litExp.replaceMe(EMLAccessor.convertEMLToCodeModel(vectorLitValue, true));
			litExp.setDataType(outDT.getCopy());
		}
	}
	
	/**
	 * Check for inDataPorts that are not of the type of the output
	 * 
	 * @param block
	 *            The block on which we want set inputs data types
	 */
	public void computeCommonDataType(Block block) {
		GADataType outDT = block.getOutDataPorts().get(0).getDataType();
		List<InDataPort> lstInDPs = new ArrayList<InDataPort>();
		for (InDataPort inDP : block.getInDataPorts()) {
			if (!inDP.getDataType().equalsTo(outDT)) {
				lstInDPs.add(inDP);
			}
		}
		for (InDataPort inDP : lstInDPs) {
			InDataPort newInDP = convertTo(inDP, outDT);
			block.getInDataPorts().remove(inDP);
			inDP.getIncomingSignals().get(0).setDstPort(newInDP);
			block.getInDataPorts().add(newInDP);
		}
	}

	private InDataPort convertTo(InDataPort inDP, GADataType outDT) {
		Map<String, String> mapOutAttributes = new HashMap<String, String>();
		mapOutAttributes.put(TSMExpansionMechanismConstant.GA_PORTNUMBER,
				Integer.toString(inDP.getPortNumber()));
		InDataPort newInDP = (InDataPort) smFactory.createModelElement(
				TSMExpansionMechanismConstant.GA_INDATAPORT,
				TSMExpansionMechanismConstant.GA_INDATAPORT_LIST,
				inDP.getParent(), mapOutAttributes);
		inDP.copyCommonAttributes(newInDP);
		inDP.setModel(gaSM);
		inDP.setId();
		inDP.setDataType(outDT.getCopy());
		return inDP;
	}
	
	protected void convertBaseType(Expression gainExp, TPrimitive outDTPrimitive) {
		
	}
}
