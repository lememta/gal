/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/ReplacingGotoFromBlockState.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmexpansionmechanism.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tsmexpansionmechanism.expanders.ConstantExpander;
import geneauto.tsmexpansionmechanism.expanders.DemuxExpander;
import geneauto.tsmexpansionmechanism.expanders.GainExpander;
import geneauto.tsmexpansionmechanism.expanders.LogicExpander;
import geneauto.tsmexpansionmechanism.expanders.MathExpander;
import geneauto.tsmexpansionmechanism.expanders.MinMaxExpander;
import geneauto.tsmexpansionmechanism.expanders.MuxExpander;
import geneauto.tsmexpansionmechanism.expanders.ProductExpander;
import geneauto.tsmexpansionmechanism.expanders.RelationalOperatorExpander;
import geneauto.tsmexpansionmechanism.expanders.SumExpander;
import geneauto.tsmexpansionmechanism.expanders.SwitchExpander;
import geneauto.tsmexpansionmechanism.expanders.UnitDelayExpander;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;
import geneauto.utils.tuple.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This state transforms the blocks having multiple inputs (more than 2) to sets
 * of blocks having only two inputs
 */
public class InDataPortsTypesExpansionState extends SMExpansionMechanismState {

	SystemModelFactory smFactory;
	
	public InDataPortsTypesExpansionState(SMExpansionMechanismTool machine) {
		super(machine, "InDataPortsTypesExpansionState");
	}

	/**
	 * Logs the beginning of refactoring Multi inputs blocks.
	 */
	public void stateEntry() {
		super.stateEntry();

		gaSystemModel = stateMachine.getSystemModel();

		smFactory = stateMachine.getSystemModelFactory();
	}

	/**
	 * Look for Goto block in the system model, find the corresponding from
	 * block and replace them merging their input and output signals.
	 * 
	 */
	@Override
	public void stateExecute() {

		for (GAModelElement system : gaSystemModel
				.getAllElements(SystemBlock.class)) {
			SystemBlock sysBl = (SystemBlock) system;
			Pair<Integer, Map<Integer, List<Block>>> blockMapPair = createOrderedMap(sysBl);

			Map<Integer, List<Block>> modelElements = blockMapPair.getRight();

			// a list of blocks that can not be typed on first round (sequential
			// blocks, function call targets with input from another function
			// call

			for (int i = modelElements.size(); i > 0; i--) {
				List<Block> lstBlocks = modelElements.get(i);
				for (Block block : lstBlocks) {
					if (block instanceof CombinatorialBlock) {
						expandInputsForCombinatorialBlocks((CombinatorialBlock) block);
					} else if (block instanceof SinkBlock) {
						expandInputsForSinkBlocks((SinkBlock) block);
					} else if (block instanceof SourceBlock) {
						expandInputsForSourceBlocks((SourceBlock) block);
					} else if (block instanceof SequentialBlock) {
						expandInputsForSequentialBlocks((SequentialBlock) block);
					} else if (block instanceof SystemBlock){
						// Nothing to do on System blocks for the moments
					} else {
						EventHandler.handle(EventLevel.CRITICAL_ERROR,
								"InDataPortsTypesExpansionState", "",
								"This category of blocks is not handled yet : "
										+ block.getReferenceString());
					}
				}
			}
		}
		stateMachine.setState(new MatrixVariablesConversionState(getMachine()));
	}

	private void expandInputsForSequentialBlocks(SequentialBlock block) {
		if (block.getType().equals("UnitDelay")) {
			UnitDelayExpander expander = new UnitDelayExpander(stateMachine);
			expander.strongTyping(block);
			expander.expandVariables(block);
		} else {
			EventHandler.handle(EventLevel.CRITICAL_ERROR,
					"InDataPortsTypesExpansionState", "",
					"The type of Sequential blocks is not handled yet : "
							+ block.getReferenceString());
		}
	}

	private void expandInputsForSourceBlocks(SourceBlock block) {
		if (block.getType().equals("Constant")) {
			ConstantExpander expander = new ConstantExpander(stateMachine);
			expander.expandVariables(block);
		} else if (block.getType().equals("Inport")){
			// DO nothing as inports are supposed to have the right DataType
		} else {
			EventHandler.handle(
					EventLevel.CRITICAL_ERROR,
					"InDataPortsTypesExpansionState",
					"",
					"The type of Source blocks is not handled yet : "
							+ block.getReferenceString());
		}
	}

	private void expandInputsForSinkBlocks(SinkBlock block) {
		// Do nothing as SinkBlocks inPorts are already well typed
	}

	private void expandInputsForCombinatorialBlocks(CombinatorialBlock block) {
		if (block.getType().equals("Sum")) {
			if (block.getInDataPorts().size() > 1) {
				SumExpander expander = new SumExpander(stateMachine);
				expander.strongTyping(block);
				expander.expandVariables(block);
			}
		} else if (block.getType().equals("Gain")) {
			GainExpander expander = new GainExpander(stateMachine);
			expander.expandVariables(block);
		} else if (block.getType().equals("MinMax")) {
			MinMaxExpander expander = new MinMaxExpander(stateMachine);
			expander.expandVariables(block);
		} else if (block.getType().equals("Product")) {
			ProductExpander expander = new ProductExpander(stateMachine);
			expander.strongTyping(block);
			expander.expandVariables(block);
		} else if (block.getType().equals("Logic")) {
			LogicExpander expander = new LogicExpander(stateMachine);
			expander.expandVariables(block);
		} else if (block.getType().equals("Math")) {
			MathExpander expander = new MathExpander(stateMachine);
			expander.expandVariables(block);
		} else if (block.getType().equals("RelationalOperator")) {
			RelationalOperatorExpander expander = new RelationalOperatorExpander(stateMachine);
			expander.expandVariables(block);
			expander.strongTyping(block);
		} else if (block.getType().equals("Mux")){
			MuxExpander expander = new MuxExpander(stateMachine);
			expander.strongTyping(block);
			expander.expandVariables(block);
		} else if (block.getType().equals("Demux")){
			DemuxExpander expander = new DemuxExpander(stateMachine);
			expander.strongTyping(block);
		} else if (block.getType().equals("Switch")){
			SwitchExpander expander = new SwitchExpander(stateMachine);
			expander.strongTyping(block);
		} else if (block.getType().equals("Abs")){
//			AbsExpander expander = new AbsExpander(stateMachine);
//			expander.strongTyping(block);
		} else if (block.getType().equals("Trigonometry")){
//			AbsExpander expander = new AbsExpander(stateMachine);
//			expander.strongTyping(block);
		} else if (block.getType().equals("Lookup")){
//			AbsExpander expander = new AbsExpander(stateMachine);
//			expander.strongTyping(block);
		} else if (block.getType().equals("DiscreteIntegrator")){
			
		} else {
			EventHandler.handle(EventLevel.CRITICAL_ERROR,
					"InDataPortsTypesExpansionState", "Type:" + block.getType(),
					"The type of Combinatorial blocks is not handled yet : "
							+ block.getReferenceString());
		}
	}

	/**
	 * Puts the modelElements into a map, to be retrieved by execution order.
	 * Specific treatment for controlled subsystems, they are put in a list with
	 * the block feeds data for them.
	 * 
	 * Returns pair, where left element is minimum execution order value of
	 * sequential blocks and the right element is a map of execution order
	 * values and corresponding blocks.
	 */
	private Pair<Integer, Map<Integer, List<Block>>> createOrderedMap(
			SystemBlock system) {

		// compose map where execution order is the key
		// controlled blocks are in this map with the maximum execution order
		// of their data provider + 1
		Map<Integer, List<Block>> modelElements = new HashMap<Integer, List<Block>>();

		// maximum execution order of data-flow controlled blocks
		int currEO = 0;
		// minimum execution order of sequential blocks.
		// NB! here we assume that sequential block is never controlled
		int minSeq = -1;

		for (Block block : system.getBlocks()) {
			if (block.getExecutionOrder() < 0) {
				// if block is a controlled subsystem
				// then we put it in the map with
				// the execution order of highest
				// data input's execution order + 1
				// this is to ensure that all data inputs are typed before
				currEO = getDataSourceMaxEO(block, new HashSet<Block>()) + 1;

			} else {
				currEO = block.getExecutionOrder();
			}

			// put block to the map of all element
			if (modelElements.containsKey(currEO)) {
				insertBlockToList(block, modelElements.get(currEO));
			} else {
				modelElements.put(currEO, insertBlockToList(block, null));
			}

			// if block is sequential, put it also to specific map
			if (!block.isDirectFeedThrough()) {
				if (minSeq == -1 || block.getExecutionOrder() < minSeq) {
					minSeq = block.getExecutionOrder();
				}
			}
		}

		return new Pair<Integer, Map<Integer, List<Block>>>(minSeq,
				modelElements);
	}

	/**
	 * Returns max execution order of all blocks providing data inputs to the
	 * given block. In case of function-call activated block processes also
	 * inputs of the activator block.
	 * 
	 * 
	 * @param b
	 *            - block to be processed
	 * @param backtrackChain
	 *            - set of blocks already processed to avoid loops
	 * 
	 * @return int
	 */
	private int getDataSourceMaxEO(Block b, Set<Block> backTrackChain) {
		Block srcBlock = null;
		// maximum execution order. Initially execution order of a block
		// itself -- to cover the case of block with no inputs
		int maxEO = b.getExecutionOrder();

		// execution order of last processed input
		int currEO = 0;

		if (backTrackChain.contains(b)) {
			// we are in loop. break here
			return 0;
		} else {
			backTrackChain.add(b);
		}

		// process all incoming signals
		for (Signal s : b.getIncomingSignals()) {
			srcBlock = (Block) s.getSrcPort().getParent();
			if (srcBlock != null) {
				// in case of controlling block or function-call-activated
				// data block process its predecessors
				if (s.getDstPort() instanceof InControlPort
						|| srcBlock.getExecutionOrder() < 0) {
					currEO = getDataSourceMaxEO(srcBlock, backTrackChain);
				} else {
					currEO = srcBlock.getExecutionOrder();
				}

				if (currEO > maxEO) {
					maxEO = currEO;
				}
			}
		}

		// the block is processed, we go back in chain so it can
		// be removed from stack
		backTrackChain.remove(b);

		if (maxEO < 1) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
					.getCanonicalName(), "",
					"Unable to determine execution order of prdecessors for "
							+ "block\n " + b.getReferenceString(), "");
			return 0;
		}

		return maxEO;
	}

	/**
	 * Takes block b and list targetList as an input. Places b in the
	 * targetList. If b.executionOrder > -1 it will be placed in the end of the
	 * list If b.executionOrder < 0 it will be placed in the list sorted by
	 * execution order value as follows: prevBlock.executionOrder >
	 * b.executiOnorder AND( b.executionOrder > nextBlock.executionOrder OR
	 * nextBlock.executionOrder() > -1) If targetList is null new LinkedList
	 * will be created
	 * 
	 * @param b
	 *            - block to add to the list
	 * @param targetList
	 *            - list of blocks with zero or more controlled blocks
	 *            (executionOrder < 0) and zero or one data-flow ordered block
	 * @return targetList
	 */
	private List<Block> insertBlockToList(Block b, List<Block> targetList) {
		if (targetList == null) {
			targetList = new LinkedList<Block>();
		}

		if (b.getExecutionOrder() < 0) {
			/*
			 * controlled block. put it in the list so that
			 * prevBlock.getExecutionOrder() > b.getExecutionOrder AND(
			 * b.getExecutionOrder > nextBlock.getExecutionorder() OR
			 * nextBlock.getExecutionorder() > -1)
			 * 
			 * We assume that list contains zero or more unique negative
			 * execution order values and zero or one non-negative value
			 */
			int currentEO = 0;
			int idx = 0;
			for (idx = 0; idx < targetList.size(); idx++) {
				currentEO = targetList.get(idx).getExecutionOrder();
				if (currentEO > -1 || currentEO > b.getExecutionOrder()) {
					break;
				}
			}
			/*
			 * add block to the given position. If list was empty, the block
			 * will be at position 0. If the new block has lowest negative it
			 * will be added to the end of the list
			 */
			targetList.add(idx, b);
		} else {
			// data-flow-scheduled block goes to the end of the list.
			targetList.add(b);
		}

		return targetList;
	}

}
