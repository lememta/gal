/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/ReplacingGotoFromBlockState.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmexpansionmechanism.states;

import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;

import java.util.ArrayList;
import java.util.List;

/**
 * This state transforms the blocks having multiple inputs (more than 2)
 * to sets of blocks having only two inputs
 */
public class BlockOperatorsParameterConversionState extends SMExpansionMechanismState {

    public BlockOperatorsParameterConversionState(SMExpansionMechanismTool machine) {
        super(machine, "Converting Block Operators");
    }

    /**
     * Logs the beginning of refactoring Multi inputs blocks.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();

    }

    /**
     * Look for Goto block in the system model, find the corresponding from
     * block and replace them merging their input and output signals.
     * 
     */
    @Override
    public void stateExecute() {
    	
    	List<CombinatorialBlock> lstProduct = new ArrayList<CombinatorialBlock>();
    	List<CombinatorialBlock> lstSum = new ArrayList<CombinatorialBlock>();
    	
    	for (GAModelElement gaModelElement : gaSystemModel.getAllElements(CombinatorialBlock.class)) {
			CombinatorialBlock block = (CombinatorialBlock) gaModelElement;
			if (block.getType().equals("Product")){
				lstProduct.add(block);
			}else if (block.getType().equals("Sum")){
				lstSum.add(block);
			}
		}
    	
    	for (CombinatorialBlock sumBlock : lstSum) {
			setParameterValue(sumBlock.getParameterByName("Inputs"), "+");
		}
    	
    	for (CombinatorialBlock productBlock : lstProduct) {
    		setParameterValue(productBlock.getParameterByName("Inputs"), "*");
		}
    	
        stateMachine.setState(new ConstantEvaluationState(getMachine()));
    }

	private void setParameterValue(BlockParameter param, String string) {
		String value = ((StringExpression)param.getValue()).getLitValue();
		try{
			Integer valueint = Integer.parseInt(value);
			String newValue = "";
			for (int i = 0; i < valueint; i++) {
				newValue += string;
			}
			((StringExpression)param.getValue()).setLitValue(newValue);
		}
		catch (NumberFormatException e) {
			
		}
	}
}
