
package geneauto.tsmexpansionmechanism.states;

import java.util.List;

import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.common.Variable;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gadatatypes.GADataType;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;

/**
 * This state interprets parameters of an Observer block to generate annotations
 */
public class MatrixVariablesConversionState extends SMExpansionMechanismState {

	SystemModelFactory smFactory;
	
    public MatrixVariablesConversionState(SMExpansionMechanismTool machine) {
        super(machine, "Matrix variables Conversion state");
    }

    /**
     * Logs the beginning of matrix variables conversion
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
        
        smFactory = stateMachine.getSystemModelFactory();
        
    }

    /**
     * 
     */
    @Override
    public void stateExecute() {
    	
    	for (GAModelElement elem : gaSystemModel.getAllElements(BlockParameter.class)){
    		BlockParameter param = (BlockParameter) elem;
    		if (param.getValue() instanceof GeneralListExpression){
    			GeneralListExpression exp = (GeneralListExpression) param.getValue();
    			if (exp.getDataType().isMatrix()){
    				int nbElemPerDim = exp.getExpressions().get(0).getExpressions().size();
    				GeneralListExpression newListExp = (GeneralListExpression) exp.getCopy();
    				newListExp.getExpressions().clear();
    				newListExp.setDataType(revertDimensions(exp.getDataType()));
    				for (int i = 0; i < nbElemPerDim; i++) {
    					GeneralListExpression subList = new GeneralListExpression();
    					for (Expression innerExp : exp.getExpressions()) {
							subList.addExpression(innerExp.getExpressions().get(i).getCopy());
						}
    					subList.setParent(newListExp);
    					subList.setDataType(new TArray(new IntegerExpression(nbElemPerDim), exp.getDataType().getPrimitiveType()));
    					newListExp.getExpressions().add(subList);
    					newListExp.setParent(param);
					}
    				param.setValue(newListExp);
    			}
    		}
    	}
    	
    	for (GAModelElement elem : gaSystemModel.getAllElements(VariableExpression.class)){
    		VariableExpression varExp = (VariableExpression) elem;
    		Variable var = varExp.getVariable();
    		
    		if (varExp.getDataType().isMatrix()){
    			GeneralListExpression exp = (GeneralListExpression) var.getInitialValueExpression();
				int nbElemPerDim = exp.getExpressions().get(0).getExpressions().size();
				GeneralListExpression newListExp = (GeneralListExpression) exp.getCopy();
				newListExp.getExpressions().clear();
				newListExp.setDataType(revertDimensions(exp.getDataType()));
				for (int i = 0; i < nbElemPerDim; i++) {
					GeneralListExpression subList = new GeneralListExpression();
					for (Expression innerExp : exp.getExpressions()) {
						subList.addExpression(innerExp.getExpressions().get(i).getCopy());
					}
					subList.setParent(newListExp);
					subList.setDataType(new TArray(new IntegerExpression(nbElemPerDim), exp.getDataType().getPrimitiveType()));
					newListExp.getExpressions().add(subList);
					//newListExp.setParent(var);
				}
				var.setInitialValueExpression(newListExp);
			}
    	}

        stateMachine.setState(new SavingModelState(getMachine()));
    }
    
    public GADataType revertDimensions(GADataType dt){
    	List<Expression> lstDims = dt.getDimensions();
    	GADataType newDT = dt.getCopy();
    	newDT.getDimensions().clear();
    	for (Expression dim : lstDims) {
    		newDT.getDimensions().add(0, dim);
		}
    	return newDT;
    }

}
