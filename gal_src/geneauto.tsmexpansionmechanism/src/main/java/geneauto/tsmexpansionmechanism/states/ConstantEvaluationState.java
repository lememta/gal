/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/ReplacingGotoFromBlockState.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmexpansionmechanism.states;

import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.ParenthesisExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Port;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tsmexpansionmechanism.main.SMExpansionMechanismTool;

/**
 * This state interprets parameters of an Observer block to generate annotations
 */
public class ConstantEvaluationState extends SMExpansionMechanismState {

	SystemModelFactory smFactory;
	
    public ConstantEvaluationState(SMExpansionMechanismTool machine) {
        super(machine, "Constant evaluation state");
    }

    /**
     * Logs the beginning of refactoring Multi inputs blocks.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
        
        for (GAModelElement elem : gaSystemModel.getAllElements()){
        	if (elem instanceof Block){
        		Block bl = (Block) elem;
        		if (bl.getType().equals("Constant")){
        			BlockParameter valueParam = bl.getParameterByName("Value");
        			if (valueParam.getValue() instanceof BinaryExpression ||
        				valueParam.getValue() instanceof ParenthesisExpression ||
        				valueParam.getValue() instanceof UnaryExpression){
	        			Expression evaluatedExp = valueParam.getValue().eval();
	        			valueParam.setValue(evaluatedExp);
	        			evaluatedExp.setParent(valueParam);
        			}
        		}
        	} else if (elem instanceof InDataPort){
        		InDataPort port = (InDataPort) elem;
        		if (port.getDataType().isRowMatrix()){
        			port.setDataType(port.getDataType().normalize(true));
        		}
        	} else if (elem instanceof OutDataPort){
        		OutDataPort port = (OutDataPort) elem;
        		if (port.getDataType().isRowMatrix()){
        			port.setDataType(port.getDataType().normalize(true));
        		}
        	} else if (elem instanceof BlockParameter){
        		BlockParameter param = (BlockParameter) elem;
        		if (param.getValue() != null){
	        		if (!(param.getValue() instanceof VariableExpression)){
	        			if (param.getValue().getDataType().isRowMatrix()){
	            			param.setValue(param.getValue().normalizeShape(true));
	        			}
	        		}
        		}
        	}
        }
        
        smFactory = stateMachine.getSystemModelFactory();

    }

	/**
     * Look for Goto block in the system model, find the corresponding from
     * block and replace them merging their input and output signals.
     * 
     */
    @Override
    public void stateExecute() {
    	
//    	for (GAModelElement elem : gaSystemModel.getAllElements(SystemBlock.class)){
//    		SystemBlock block = (SystemBlock) elem;
//    		
//    		if (block.getParameterByName("annotationType") != null){
//    			BlockParameter param = block.getParameterByName("precondition");
//    			
//    			Expression exp = EMLAccessor.convertEMLToCodeModel(param.getStringValue(), true);
//    			exp.getValue();
//    			
//    			param.setValue(exp);
//    		}
//    	}

        stateMachine.setState(new InOutPortForceTypePropagationState(getMachine()));
    }

}
