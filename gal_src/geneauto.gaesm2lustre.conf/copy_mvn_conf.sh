#!/bin/sh

# go to the directory where the script is
cd `dirname $0`

# load some utlities
source ./util.sh

########################################################
# Helper functions
########################################################

# Copies or updates pom
# Parameter 1 is the project name
copy_readonly_pom()
{
  # Make old pom file writeable
  if [ -f "../${1}/pom.xml" ]; then
    chmod +w "../${1}/pom.xml"
	check_result $? "Can't make pom file writeable"
  fi
  # copy
  cp -v "mvn/${1}/pom.xml" "../${1}/"
  check_result $? "Can't copy pom file"
  # Make the copied file read-only
  chmod -w "../${1}/pom.xml"
  # Readme file with info
  cp -v "mvn/${1}/pom.xml.README" "../${1}/"  
}

########################################################
# Main script
########################################################

echo "Copying Maven configuration"

# Copy root configuration
cp -r -v mvn/root/*.* ../

check_result $? "Can't copy root configuration"

# These dirs are automatically generated by EMF and don't contain the pom. Copy now (or update)

copy_readonly_pom "geneauto.emf.models"

#copy_readonly_pom "geneauto.verif.genericmodel.model"

#copy_readonly_pom "geneauto.verif.links.model"

echo "Done copying Maven configuration"
