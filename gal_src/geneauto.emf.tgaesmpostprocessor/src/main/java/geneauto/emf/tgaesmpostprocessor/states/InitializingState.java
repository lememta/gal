/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.tgaesmpostprocessor.states;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;
import geneauto.emf.tgaesmpostprocessor.components.GAESMPostprocessorConst;
import geneauto.emf.utilities.EcoreUtils;
import geneauto.emf.utilities.GAEMFConst;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.genericmodel.Model;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.io.File;
import java.io.IOException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

/**
 * First state which is executed by the tool. It checks the java parameters,
 * imports the model from the xml file, reads the block library, initialises the
 * whole tool.
 * 
 */
public class InitializingState extends GAESMPostProcessorState {

    public InitializingState() {
        // set state name for messages
        super("Initializing");
    }

    /**
     * Reads the Model.
     */
    public void stateExecute() {
    	
        // Get parameters.
        String inputFile  = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);
        String outputFile = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFILE);
        
        // Determine the kind of model
        // We currently assume it is either a system or code model
        // Distinction is made by file extension. 
        boolean isEcoreSystemModel = false;
        
		if (inputFile.endsWith(GAEMFConst.EXT_GAESM)) {
			isEcoreSystemModel = true;
		}

        // If output file name not given
        if (outputFile == null) {
        	outputFile = inputFile;
        	outputFile = FileUtil.setFileExtension(
        			outputFile, 
        			GAESMPostprocessorConst.EXT_GAESM_POST);
        }

		// Canonicalise filenames for better readability. This is not strictly
		// necessary, but makes the output more readable
        try {
			inputFile = (new File(inputFile)).getCanonicalPath();
		} catch (IOException e) {
			// Do nothing, continue with the uncanonicalised name
		}
        try {
        	outputFile = (new File(outputFile)).getCanonicalPath();
		} catch (IOException e) {
			// Do nothing, continue with the uncanonicalised name
		}
        
        // check for read/write permissions
        FileUtil.assertCanRead(inputFile);
        FileUtil.assertCanWrite(outputFile);

        // Delete the old output file, if exists
    	File outF = new File(outputFile);
    	if (outF.exists()) {
    		if (!outF.delete()) {
    			EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
    				"Cannot delete existing file, before writing new output.\n File: " + outputFile);
    		}
    	}
		
		// Read the model
		EventHandler.handle(EventLevel.INFO, "",
				"", "Reading the model from file:\n " + inputFile);
		
		// Determine which kind of a model it is and create appropriate factory
	//	ModelFactory modelFactory;
		
			// Assume it is a SystemModel file
			
			// Initialise the block library 
	        // TODO (to AnTo) 5 Not sure, if referencing the block library is required for this application?
//	      String libraryFile = ArgumentReader.getParameter(GAConst.ARG_LIBFILE);
//	        // Form list of the input files of the Block Library Factory
//	        List<String> blockLibFiles = new ArrayList<String>();
//	        if (ArgumentReader.getParameter(GAConst.ARG_DEFLIBFILE) != null) {
//	            blockLibFiles.add(ArgumentReader
//	                    .getParameter(GAConst.ARG_DEFLIBFILE));
//	        } else {
//	            EventHandler.handle(EventLevel.CRITICAL_ERROR,
//	                    "FMCodeModelGenerator", "",
//	                    "Default block library is missing", "");
//	        }
//	        if (libraryFile != null) {
//	            blockLibFiles.add(libraryFile);
//	        }
	//
//	        // Create the blockLibraryFactory
//	        BlockLibraryFactory blockLibraryFactory = new BlockLibraryFactory(
//	        		new GABlockLibrary(), GA2Ecore.TOOL_NAME, blockLibFiles, null);

	        // Create the systemModelFactory
//			modelFactory = new SystemModelFactory(
//					new GASystemModel(), GAESMPostprocessorConst.TOOL_NAME, inputFile, 
//					outputFile, null, null);
//		
//		URI uri = URI.createFileURI(inputFile);
//		
//		EObject obj = EcoreUtils.readEObject(inputFile, GasystemmodelPackage.eINSTANCE, false);
		
//		GasystemmodelResourceImpl res = new GasystemmodelResourceImpl(uri);
//		
//		Map<Object, Object> options = new HashMap<Object, Object>();
//		options = res.getDefaultLoadOptions();
//		options.put(XMIResourceImpl.OPTION_USE_XMI_TYPE, true);
//		
//		GenericmodelXMLProcessor processor = new GenericmodelXMLProcessor();
//		FileInputStream fis;
//		
//		
//		
//		try {
//			fis = new FileInputStream(new File(inputFile));
//			Resource ressource = processor.load(fis, options);
//		} catch (FileNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		try {
//			res.load(options);
//		} catch (IOException e) {
//			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
//					"", "Unable to load model " + inputFile, e);
//			return;
//		}
		
		// Set the factory to ignore dependent model references - Currently the
		// tool does not support inter-related models
		//modelFactory.setIgnoreDependentModelRefs();

		getMachine().setInputFile(inputFile);
		getMachine().setOutputFile(outputFile);
		
        //getMachine().setModelFactory(modelFactory);
        
        // Read system model 
        //Model model = getMachine().getModelFactory().readModel();

        // Go to next state 
        getMachine().setState(new ReadingModelState());
    }

}
