/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.tgaesmpostprocessor.states;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;
import geneauto.emf.utilities.GAEMFConst;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.FileUtil;

import java.io.IOException;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

/** Reads the transformation model */
public class ReadingModelState extends GAESMPostProcessorState {

    public ReadingModelState() {
        // set state name for messages
        super("Reading Model");
    }

    /** Reads the Model. */
    public void stateExecute() {
    	
		String inputFile = getMachine().getInputFile();
		
    	// Extension map for the XML/XMI resource factory
    	Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> extMap = reg.getExtensionToFactoryMap();
		
		// Package that implements the metamodel.
		EPackage ePack;
		
    	// create resource set and resource 
    	ResourceSet resourceSet = new ResourceSetImpl();
    	
		// Determine the type of input file/model
		// NOTE: List all allowed types also in the error message below!
		String fileExt   = FileUtil.getFileExtension(inputFile);

		// Register factories
		if (GAEMFConst.EXT_GAESM.equals(fileExt)) {
			// GASystemModel
			ePack = GasystemmodelPackage.eINSTANCE;
	    	// Register package in local resource registry
	    	resourceSet.getPackageRegistry().put(ePack.getNsURI(), ePack);
	    	
	    	// Register the XML/XMI resource factory for the appropriate extension
			extMap.put(GAEMFConst.EXT_GAESM, new XMIResourceFactoryImpl());			
			
		} else {
			// Unknown file type
			EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
					"Unknown type of input file. Extension : " + fileExt
						+ "\n  Expecting one of : " 
					    + GAEMFConst.EXT_GAESM 
					);
			return;
		}

		// Read the model
		EventHandler.handle(EventLevel.INFO, "",
				"", "Reading the model from file:\n " + inputFile);      
    	
		Resource resource = null;
		try {
			resource = resourceSet.getResource(URI.createFileURI(inputFile), true);
		} catch (Exception e) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
					"Unable to read model. Wrong format of the input file: " + inputFile, e);
			return;
		}
    	
		// Load the model
    	try {
        	resource.load(null);
		} catch (IOException e) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
					"", "Unable to read the model from file: " + inputFile, e);
			return;
		}
		
		if (resource.getContents().size() > 1) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
					"", "Single rootnode expected. Input file: " + inputFile);
			return;
		}

		// Get the root object
		EObject eobj = resource.getContents().get(0);
		
// TODO decide, if element's type checking is useful here		
		// Check its type		
//		SM2SM model;
//		if (eobj instanceof SM2SM) {
//			model = (SM2SM) eobj;
//		} else {
//			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
//					"", "Expecting an element of type SM2SM." +
//							"\n Found: " + eobj.getClass().getName() +
//							"\n Input file: " + inputFile);
//			return;
//		}
		
		// Store the model under the machine		
		getMachine().setEModel(eobj);
		
		// Store the model package under the machine		
		getMachine().setEPackage(ePack);
		
        // Go to next state 
        getMachine().setState(new PostProcessingModelState());
    }
}
