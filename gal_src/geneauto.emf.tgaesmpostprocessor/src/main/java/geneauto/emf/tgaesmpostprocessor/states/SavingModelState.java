/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.tgaesmpostprocessor.states;

import geneauto.emf.models.genericmodel.GenericmodelFactory;
import geneauto.emf.models.genericmodel.Transformation;
import geneauto.emf.tgaesmpostprocessor.components.GAESMPostprocessorConst;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.TimeUtils;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;


public class SavingModelState extends GAESMPostProcessorState {
	public SavingModelState(){
		// set state name for messages
		super("SavingModel");
	}

	private Transformation makeTransformation() {
		Date readTime = getMachine().getModelFactory().getStartTime();
		Date writeTime = new Date();
		String toolname = GAESMPostprocessorConst.TOOL_NAME;
		String stRead = "";
		if (readTime != null) {
			stRead = TimeUtils.getFormattedDate(readTime);
		}
		String stWrite   = "";		
		if (writeTime != null) {
			stWrite = TimeUtils.getFormattedDate(writeTime);
		}

		Transformation t = GenericmodelFactory.eINSTANCE.createTransformation();
		t.setReadTime(stRead);
		t.setWriteTime(stWrite);
		t.setToolName(toolname);
		return t;
		}	
	

	public void stateExecute() {

    	// Check, if any errors have occurred so far
    	if (EventHandler.getAnErrorHasOccured()) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), "", 
				"Errors have occured during the tool's execution. Output will not be written.");
    	}
    	
    	// Get output file name
    	String outputFile = getMachine().getOutputFile();
    	
		// Info message    	
		EventHandler.handle(EventLevel.INFO, "", "",
				"Writing the converted model to file:\n "
					+ outputFile);
    	
    	// Get model
		geneauto.emf.models.genericmodel.Model eModel = getMachine().getEModel();
    	
		// Unset the block library reference - it is not needed to be stored with the model
    	if (eModel instanceof geneauto.emf.models.gasystemmodel.GASystemModel) {
    		((geneauto.emf.models.gasystemmodel.GASystemModel) eModel).setBlockLibrary(null);
    	}    	
		
		// Create a new transformation with tool name and read-write times and
		// add it to the model.
        Transformation t = makeTransformation();
        eModel.getTransformations().add(t);
        
		// Write the model
        URI uri = URI.createFileURI(outputFile);
        Resource resource;
	    switch (GAESMPostprocessorConst.SERIALIZATION_MODE) { 
		    case XML : 
		    	resource = new XMLResourceImpl(uri);
		    	break;
		    case XMI_INTRINSIC : 
		    	resource = new XMIResourceImpl(uri);
		    	break;
		    case XMI_EXTRINSIC : 
		    	resource = new XMIResourceImpl(URI.createFileURI(outputFile)) {
		    		protected boolean useUUIDs() {return true;}
		    	};
		    	break;
		    default :
				EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
						"", "Unknown serialization mode: " + GAESMPostprocessorConst.SERIALIZATION_MODE);
				return;
	    }

    	resource.getContents().add(eModel);
    	try {
			//TODO (To AnTo): Supressed xsi:type for testing. 
    		Map<Object, Object> options = new HashMap<Object, Object>();
    		options.put(XMIResourceImpl.OPTION_USE_XMI_TYPE, true);
    		resource.save(options);
		} catch (IOException e) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(),
							"", "Unable to store model " + eModel.getModelName()
									+ "\n to file: " + outputFile, e);
			return;
		}

        // This is the last state of the statemachine. Stop the machine
        getMachine().stop();
	}
}
