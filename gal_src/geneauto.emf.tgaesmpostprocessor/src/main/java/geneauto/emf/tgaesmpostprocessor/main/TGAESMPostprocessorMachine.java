/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.tgaesmpostprocessor.main;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import geneauto.emf.tgaesmpostprocessor.components.GAESMPostprocessorConst;
import geneauto.emf.tgaesmpostprocessor.states.InitializingState;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.statemachine.StateMachine;

/**
 * TODO (to AnTo) 3 Generalise the machine to support different types of models
 */
public class TGAESMPostprocessorMachine extends StateMachine {

	/** Unique instance of this class. */
	protected static StateMachine instance;
	
	/** Factory which is used to read the model from the xml file. */
    private ModelFactory modelFactory;

    /** Input and output files for the tool */
    private String inputFile;
    private String outputFile;
    
    /** Reference to the package that implements the metamodel. */
	private EPackage ePackage;
	
	/** Reference to the handled model object. */
	private EObject model;

	/** Model conforming to the Ecore metamodel. */
    private geneauto.emf.models.genericmodel.Model eModel;

    /** Private constructor of this class. Initialises the state machine */
    private TGAESMPostprocessorMachine() {
    	// set the version number of the elementary tool machine
    	super(GAESMPostprocessorConst.TOOL_VER, "");

        String version 	   = GAESMPostprocessorConst.TOOL_VER;
        String releaseDate = GAESMPostprocessorConst.TOOL_DATE;
        
        if (version!=null) {
            setVerString(version);
        }
        if (releaseDate!=null) {
            setReleaseDate(releaseDate);
        }
		// set initial state
        initialState = new InitializingState();
    }

    public static TGAESMPostprocessorMachine getInstance(){
    	if (instance == null){
    		instance = new TGAESMPostprocessorMachine();
    	}
    	
    	return (TGAESMPostprocessorMachine) instance;
    }

	public ModelFactory getModelFactory() {
		return this.modelFactory;
	}

	public void setModelFactory(ModelFactory modelFactory) {
		this.modelFactory = modelFactory;
	}

	public void setEModel(geneauto.emf.models.genericmodel.Model eModel) {
		this.eModel = eModel;
	}

	public geneauto.emf.models.genericmodel.Model getEModel() {
		return eModel;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public EPackage getEPackage() {
		return ePackage;
	}

	public void setEPackage(EPackage ePackage) {
		this.ePackage = ePackage;
	}

	public void setEModel(EObject model) {
		this.model = model;
	}   
 
}
