/**
  *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.gaxml2gaesm.main;

import geneauto.emf.gaxml2gaesm.components.GAESMPostprocessorConst;
import geneauto.emf.utilities.GAEMFSysInfo;
import geneauto.eventhandler.EventHandler;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

/**
 * Main class of the CPrinter tool. It reads the arguments and launches the
 * GASystemModel preprocessing.
 * 
 */
public class TGAESMPostprocessor {

    /**
     * reference to the tool statemachine
     */
    private static TGAESMPostprocessorMachine stateMachine;

    /**
     * Main method of the CPrinter tool. It reads the program parameters and
     * launches the GASystemModel pre-processing.
     * 
     * @param args
     *            List of the arguments of the tool.
     */
    public static void main(String[] args) {
    	
		System.out.println(GAEMFSysInfo.getLicenceMessage(GAESMPostprocessorConst.TOOL_NAME));
    	    	
        // set root of all location strings in messages
        EventHandler.setToolName(GAESMPostprocessorConst.TOOL_NAME);

        // initialise ArgumentReader
        ArgumentReader.clear();

        ArgumentReader.addArgument("", GAConst.ARG_INPUTFILE, true, true,
                true, GAConst.ARG_INPUTFILE_NAME_ROOT);
        // primary output -- the modified system model file
        ArgumentReader.addArgument("-o", GAConst.ARG_OUTPUTFILE, false, true,
                true, GAConst.ARG_OUTPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-b", GAConst.ARG_LIBFILE, false, true,
                true, GAConst.ARG_LIBFILE_NAME_ROOT);

        stateMachine = TGAESMPostprocessorMachine.getInstance();

        // set arguments list to ArgumentReader and parse the list
        stateMachine.initTool(args, true);

        // Run the state machine
        stateMachine.run();
    }
}
