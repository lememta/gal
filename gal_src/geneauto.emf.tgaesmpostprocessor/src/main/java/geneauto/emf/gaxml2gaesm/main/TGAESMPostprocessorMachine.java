/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.gaxml2gaesm.main;

import geneauto.emf.gaxml2gaesm.components.GAESMPostprocessorConst;
import geneauto.emf.gaxml2gaesm.states.InitializingState;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.statemachine.StateMachine;

/**
 * TODO (to AnTo) 3 Generalise the machine to support different types of models
 */
public class TGAESMPostprocessorMachine extends StateMachine {

	/** Unique instance of this class. */
	protected static StateMachine instance;
	
	/** Factory which is used to read the model from the xml file. */
    private ModelFactory modelFactory;

	/** Model conforming to the Ecore metamodel. */
    private geneauto.emf.models.genericmodel.Model eModel;

    /** Private constructor of this class. Initialises the state machine */
    private TGAESMPostprocessorMachine() {
    	// set the version number of the elementary tool machine
    	super(GAESMPostprocessorConst.TOOL_VER, "");

        String version 	   = GAESMPostprocessorConst.TOOL_VER;
        String releaseDate = GAESMPostprocessorConst.TOOL_DATE;
        
        if (version!=null) {
            setVerString(version);
        }
        if (releaseDate!=null) {
            setReleaseDate(releaseDate);
        }
		// set initial state
        initialState = new InitializingState();
    }

    public static TGAESMPostprocessorMachine getInstance(){
    	if (instance == null){
    		instance = new TGAESMPostprocessorMachine();
    	}
    	
    	return (TGAESMPostprocessorMachine) instance;
    }

	public ModelFactory getModelFactory() {
		return this.modelFactory;
	}

	public void setModelFactory(ModelFactory modelFactory) {
		this.modelFactory = modelFactory;
	}

	public void setEModel(geneauto.emf.models.genericmodel.Model eModel) {
		this.eModel = eModel;
	}

	public geneauto.emf.models.genericmodel.Model getEModel() {
		return eModel;
	}   
 
}
