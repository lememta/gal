package geneauto.xtext.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import geneauto.xtext.lustre.AddExpression;
import geneauto.xtext.lustre.AndExpression;
import geneauto.xtext.lustre.Annotation;
import geneauto.xtext.lustre.ArrayDeclaration;
import geneauto.xtext.lustre.ArrayExpression;
import geneauto.xtext.lustre.Assert;
import geneauto.xtext.lustre.BDeclaration;
import geneauto.xtext.lustre.Body;
import geneauto.xtext.lustre.BooleanLiteralExpression;
import geneauto.xtext.lustre.CallExpression;
import geneauto.xtext.lustre.DDeclaration;
import geneauto.xtext.lustre.DataType;
import geneauto.xtext.lustre.DataTypeDimValue;
import geneauto.xtext.lustre.DoubleLiteralExpression;
import geneauto.xtext.lustre.Equation;
import geneauto.xtext.lustre.FbyExpression;
import geneauto.xtext.lustre.Function;
import geneauto.xtext.lustre.IDeclaration;
import geneauto.xtext.lustre.ImpliesExpression;
import geneauto.xtext.lustre.Inputs;
import geneauto.xtext.lustre.IntegerLiteralExpression;
import geneauto.xtext.lustre.IteExpression;
import geneauto.xtext.lustre.LeftVariables;
import geneauto.xtext.lustre.Locals;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.MatrixExpression;
import geneauto.xtext.lustre.MultExpression;
import geneauto.xtext.lustre.Node;
import geneauto.xtext.lustre.NorExpression;
import geneauto.xtext.lustre.NotExpression;
import geneauto.xtext.lustre.Open;
import geneauto.xtext.lustre.OrExpression;
import geneauto.xtext.lustre.Outputs;
import geneauto.xtext.lustre.PreTraceAnnotation;
import geneauto.xtext.lustre.Program;
import geneauto.xtext.lustre.RDeclaration;
import geneauto.xtext.lustre.RealLiteralExpression;
import geneauto.xtext.lustre.RelExpression;
import geneauto.xtext.lustre.SpecificationLine;
import geneauto.xtext.lustre.TupleExpression;
import geneauto.xtext.lustre.UnExpression;
import geneauto.xtext.lustre.Variable;
import geneauto.xtext.lustre.VariableCall;
import geneauto.xtext.lustre.VariableExpression;
import geneauto.xtext.lustre.VariablesList;
import geneauto.xtext.lustre.WhenExpression;
import geneauto.xtext.services.LustreGrammarAccess;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class LustreSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private LustreGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == LustrePackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case LustrePackage.ADD_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_AddExpression(context, (AddExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.AND_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_AndExpression(context, (AndExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.ANNOTATION:
				if(context == grammarAccess.getAnnotationRule()) {
					sequence_Annotation(context, (Annotation) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.ARRAY_DECLARATION:
				if(context == grammarAccess.getAbstractDeclarationRule() ||
				   context == grammarAccess.getArrayDeclarationRule()) {
					sequence_ArrayDeclaration(context, (ArrayDeclaration) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.ARRAY_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getArrayDefinitionRule() ||
				   context == grammarAccess.getArrayExpressionRule() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_ArrayExpression(context, (ArrayExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.ASSERT:
				if(context == grammarAccess.getAssertRule()) {
					sequence_Assert(context, (Assert) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.BDECLARATION:
				if(context == grammarAccess.getAbstractDeclarationRule() ||
				   context == grammarAccess.getBDeclarationRule()) {
					sequence_BDeclaration(context, (BDeclaration) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.BODY:
				if(context == grammarAccess.getBodyRule()) {
					sequence_Body(context, (Body) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.BOOLEAN_LITERAL_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getBooleanLiteralExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getLiteralExpressionRule() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_BooleanLiteralExpression(context, (BooleanLiteralExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.CALL_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getCallExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_CallExpression(context, (CallExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.DDECLARATION:
				if(context == grammarAccess.getAbstractDeclarationRule() ||
				   context == grammarAccess.getDDeclarationRule()) {
					sequence_DDeclaration(context, (DDeclaration) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.DATA_TYPE:
				if(context == grammarAccess.getDataTypeRule()) {
					sequence_DataType(context, (DataType) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.DATA_TYPE_DIM_VALUE:
				if(context == grammarAccess.getDataTypeDimValueRule()) {
					sequence_DataTypeDimValue(context, (DataTypeDimValue) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.DOUBLE_LITERAL_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getDoubleLiteralExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getLiteralExpressionRule() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_DoubleLiteralExpression(context, (DoubleLiteralExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.EQUATION:
				if(context == grammarAccess.getEquationRule()) {
					sequence_Equation(context, (Equation) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.FBY_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_FbyExpression(context, (FbyExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.FUNCTION:
				if(context == grammarAccess.getCallableElementRule() ||
				   context == grammarAccess.getFunctionRule()) {
					sequence_Function(context, (Function) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.IDECLARATION:
				if(context == grammarAccess.getAbstractDeclarationRule() ||
				   context == grammarAccess.getIDeclarationRule()) {
					sequence_IDeclaration(context, (IDeclaration) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.IMPLIES_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_ImpliesExpression(context, (ImpliesExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.INPUTS:
				if(context == grammarAccess.getInputsRule()) {
					sequence_Inputs(context, (Inputs) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.INTEGER_LITERAL_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getIntegerLiteralExpressionRule() ||
				   context == grammarAccess.getLiteralExpressionRule() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_IntegerLiteralExpression(context, (IntegerLiteralExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.ITE_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getIteExpressionRule() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_IteExpression(context, (IteExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.LEFT_VARIABLES:
				if(context == grammarAccess.getLeftVariablesRule()) {
					sequence_LeftVariables(context, (LeftVariables) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.LOCALS:
				if(context == grammarAccess.getLocalsRule()) {
					sequence_Locals(context, (Locals) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.MATRIX_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getArrayDefinitionRule() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMatrixExpressionRule() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_MatrixExpression(context, (MatrixExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.MULT_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_MultExpression(context, (MultExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.NODE:
				if(context == grammarAccess.getCallableElementRule() ||
				   context == grammarAccess.getNodeRule()) {
					sequence_Node(context, (Node) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.NOR_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_NorExpression(context, (NorExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.NOT_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_NotExpression(context, (NotExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.OPEN:
				if(context == grammarAccess.getOpenRule()) {
					sequence_Open(context, (Open) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.OR_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_OrExpression(context, (OrExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.OUTPUTS:
				if(context == grammarAccess.getOutputsRule()) {
					sequence_Outputs(context, (Outputs) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.PRE_TRACE_ANNOTATION:
				if(context == grammarAccess.getPreTraceAnnotationRule()) {
					sequence_PreTraceAnnotation(context, (PreTraceAnnotation) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.PROGRAM:
				if(context == grammarAccess.getProgramRule()) {
					sequence_Program(context, (Program) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.RDECLARATION:
				if(context == grammarAccess.getAbstractDeclarationRule() ||
				   context == grammarAccess.getRDeclarationRule()) {
					sequence_RDeclaration(context, (RDeclaration) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.REAL_LITERAL_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getLiteralExpressionRule() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRealLiteralExpressionRule() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_RealLiteralExpression(context, (RealLiteralExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.REL_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_RelExpression(context, (RelExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.SPECIFICATION_LINE:
				if(context == grammarAccess.getSpecificationLineRule()) {
					sequence_SpecificationLine(context, (SpecificationLine) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.TUPLE_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_TupleExpression(context, (TupleExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.UN_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_UnExpression(context, (UnExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.VARIABLE:
				if(context == grammarAccess.getVariableRule()) {
					sequence_Variable(context, (Variable) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.VARIABLE_CALL:
				if(context == grammarAccess.getVariableCallRule()) {
					sequence_VariableCall(context, (VariableCall) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.VARIABLE_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getLiteralExpressionRule() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getVariableExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_VariableExpression(context, (VariableExpression) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.VARIABLES_LIST:
				if(context == grammarAccess.getVariablesListRule()) {
					sequence_VariablesList(context, (VariablesList) semanticObject); 
					return; 
				}
				else break;
			case LustrePackage.WHEN_EXPRESSION:
				if(context == grammarAccess.getAddExpressionRule() ||
				   context == grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getFbyExpressionRule() ||
				   context == grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getImpliesExpressionRule() ||
				   context == grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getMultExpressionRule() ||
				   context == grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNorExpressionRule() ||
				   context == grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getNotExpressionRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getRelExpressionRule() ||
				   context == grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0() ||
				   context == grammarAccess.getTupleExpressionRule() ||
				   context == grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0() ||
				   context == grammarAccess.getUnExpressionRule() ||
				   context == grammarAccess.getWhenExpressionRule() ||
				   context == grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0()) {
					sequence_WhenExpression(context, (WhenExpression) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (left=AddExpression_AddExpression_1_0_0 op=AddOp right=MultExpression)
	 */
	protected void sequence_AddExpression(EObject context, AddExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ADD_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ADD_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ADD_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ADD_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ADD_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ADD_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getAddExpressionAccess().getOpAddOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getAddExpressionAccess().getRightMultExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=AndExpression_AndExpression_1_0_0 op=AndOp right=ImpliesExpression)
	 */
	protected void sequence_AndExpression(EObject context, AndExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.AND_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.AND_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.AND_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.AND_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.AND_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.AND_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getAndExpressionAccess().getOpAndOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getAndExpressionAccess().getRightImpliesExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (identifier=BodyAnnotationIdentifier expression=FbyExpression)
	 */
	protected void sequence_Annotation(EObject context, Annotation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ANNOTATION__IDENTIFIER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ANNOTATION__IDENTIFIER));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ANNOTATION__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ANNOTATION__EXPRESSION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAnnotationAccess().getIdentifierBodyAnnotationIdentifierParserRuleCall_1_0(), semanticObject.getIdentifier());
		feeder.accept(grammarAccess.getAnnotationAccess().getExpressionFbyExpressionParserRuleCall_3_0(), semanticObject.getExpression());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (preTraceAnnotation=PreTraceAnnotation? var=Variable value=ArrayDefinition)
	 */
	protected void sequence_ArrayDeclaration(EObject context, ArrayDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dimVal+=LiteralExpression dimVal+=LiteralExpression*)
	 */
	protected void sequence_ArrayExpression(EObject context, ArrayExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     expression=FbyExpression
	 */
	protected void sequence_Assert(EObject context, Assert semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ASSERT__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ASSERT__EXPRESSION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAssertAccess().getExpressionFbyExpressionParserRuleCall_2_0(), semanticObject.getExpression());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (preTraceAnnotation=PreTraceAnnotation? var=Variable value=LUS_BOOLEAN)
	 */
	protected void sequence_BDeclaration(EObject context, BDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((equations+=Equation | asserts+=Assert) (equations+=Equation | asserts+=Assert)* annotations+=Annotation*)
	 */
	protected void sequence_Body(EObject context, Body semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=LUS_BOOLEAN
	 */
	protected void sequence_BooleanLiteralExpression(EObject context, BooleanLiteralExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.BOOLEAN_LITERAL_EXPRESSION__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.BOOLEAN_LITERAL_EXPRESSION__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBooleanLiteralExpressionAccess().getValueLUS_BOOLEANTerminalRuleCall_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (elem=[CallableElement|ID] arguments+=FbyExpression arguments+=FbyExpression*)
	 */
	protected void sequence_CallExpression(EObject context, CallExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (preTraceAnnotation=PreTraceAnnotation? var=Variable value=LUS_DOUBLE)
	 */
	protected void sequence_DDeclaration(EObject context, DDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value=LUS_INT | ref=[Variable|ID])
	 */
	protected void sequence_DataTypeDimValue(EObject context, DataTypeDimValue semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (baseType=BasicType (dim+=DataTypeDimValue dim+=DataTypeDimValue?)?)
	 */
	protected void sequence_DataType(EObject context, DataType semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=LUS_DOUBLE
	 */
	protected void sequence_DoubleLiteralExpression(EObject context, DoubleLiteralExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.DOUBLE_LITERAL_EXPRESSION__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.DOUBLE_LITERAL_EXPRESSION__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getDoubleLiteralExpressionAccess().getValueLUS_DOUBLETerminalRuleCall_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (preTraceAnnotation=PreTraceAnnotation? (leftPart=LeftVariables | leftPart=LeftVariables) expression=FbyExpression)
	 */
	protected void sequence_Equation(EObject context, Equation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=FbyExpression_FbyExpression_1_0_0 op=FbyOp right=FbyExpression)
	 */
	protected void sequence_FbyExpression(EObject context, FbyExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.FBY_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.FBY_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.FBY_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.FBY_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.FBY_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.FBY_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getFbyExpressionAccess().getOpFbyOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getFbyExpressionAccess().getRightFbyExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID inputs=Inputs? outputs=Outputs?)
	 */
	protected void sequence_Function(EObject context, Function semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (preTraceAnnotation=PreTraceAnnotation? var=Variable value=LUS_INT)
	 */
	protected void sequence_IDeclaration(EObject context, IDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=ImpliesExpression_ImpliesExpression_1_0_0 op=ImpliesOp right=ImpliesExpression)
	 */
	protected void sequence_ImpliesExpression(EObject context, ImpliesExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.IMPLIES_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.IMPLIES_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.IMPLIES_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.IMPLIES_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.IMPLIES_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.IMPLIES_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getImpliesExpressionAccess().getOpImpliesOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getImpliesExpressionAccess().getRightImpliesExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (inputs+=VariablesList inputs+=VariablesList*)
	 */
	protected void sequence_Inputs(EObject context, Inputs semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=LUS_INT
	 */
	protected void sequence_IntegerLiteralExpression(EObject context, IntegerLiteralExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.INTEGER_LITERAL_EXPRESSION__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.INTEGER_LITERAL_EXPRESSION__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntegerLiteralExpressionAccess().getValueLUS_INTParserRuleCall_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (guard=FbyExpression then=FbyExpression else=FbyExpression)
	 */
	protected void sequence_IteExpression(EObject context, IteExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ITE_EXPRESSION__GUARD) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ITE_EXPRESSION__GUARD));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ITE_EXPRESSION__THEN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ITE_EXPRESSION__THEN));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.ITE_EXPRESSION__ELSE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.ITE_EXPRESSION__ELSE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIteExpressionAccess().getGuardFbyExpressionParserRuleCall_1_0(), semanticObject.getGuard());
		feeder.accept(grammarAccess.getIteExpressionAccess().getThenFbyExpressionParserRuleCall_3_0(), semanticObject.getThen());
		feeder.accept(grammarAccess.getIteExpressionAccess().getElseFbyExpressionParserRuleCall_4_1_0(), semanticObject.getElse());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (variables+=VariableCall variables+=VariableCall*)
	 */
	protected void sequence_LeftVariables(EObject context, LeftVariables semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((locals+=VariablesList locals+=VariablesList*)?)
	 */
	protected void sequence_Locals(EObject context, Locals semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dimVal+=ArrayExpression dimVal+=ArrayExpression*)
	 */
	protected void sequence_MatrixExpression(EObject context, MatrixExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=MultExpression_MultExpression_1_0_0 op=MultOp right=WhenExpression)
	 */
	protected void sequence_MultExpression(EObject context, MultExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.MULT_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.MULT_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.MULT_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.MULT_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.MULT_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.MULT_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getMultExpressionAccess().getOpMultOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getMultExpressionAccess().getRightWhenExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         preTraceAnnotation=PreTraceAnnotation? 
	 *         specifications+=SpecificationLine* 
	 *         name=ID 
	 *         inputs=Inputs? 
	 *         outputs=Outputs? 
	 *         locals=Locals? 
	 *         body=Body?
	 *     )
	 */
	protected void sequence_Node(EObject context, Node semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=NorExpression_NorExpression_1_0_0 op=NorOp right=UnExpression)
	 */
	protected void sequence_NorExpression(EObject context, NorExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.NOR_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.NOR_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.NOR_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.NOR_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.NOR_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.NOR_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getNorExpressionAccess().getOpNorOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getNorExpressionAccess().getRightUnExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (op=NotOp uexp=NotExpression)
	 */
	protected void sequence_NotExpression(EObject context, NotExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.NOT_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.NOT_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.NOT_EXPRESSION__UEXP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.NOT_EXPRESSION__UEXP));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getNotExpressionAccess().getOpNotOpEnumRuleCall_1_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getNotExpressionAccess().getUexpNotExpressionParserRuleCall_1_2_0(), semanticObject.getUexp());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     importURI=STRING
	 */
	protected void sequence_Open(EObject context, Open semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.OPEN__IMPORT_URI) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.OPEN__IMPORT_URI));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOpenAccess().getImportURISTRINGTerminalRuleCall_1_0(), semanticObject.getImportURI());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=OrExpression_OrExpression_1_0_0 op=OrOp right=AndExpression)
	 */
	protected void sequence_OrExpression(EObject context, OrExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.OR_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.OR_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.OR_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.OR_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.OR_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.OR_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getOrExpressionAccess().getOpOrOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getOrExpressionAccess().getRightAndExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (outputs+=VariablesList outputs+=VariablesList*)
	 */
	protected void sequence_Outputs(EObject context, Outputs semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type=TraceAnnotationElementType value=STRING)
	 */
	protected void sequence_PreTraceAnnotation(EObject context, PreTraceAnnotation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.PRE_TRACE_ANNOTATION__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.PRE_TRACE_ANNOTATION__TYPE));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.PRE_TRACE_ANNOTATION__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.PRE_TRACE_ANNOTATION__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPreTraceAnnotationAccess().getTypeTraceAnnotationElementTypeParserRuleCall_1_0(), semanticObject.getType());
		feeder.accept(grammarAccess.getPreTraceAnnotationAccess().getValueSTRINGTerminalRuleCall_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (libraries+=Open* declarations+=AbstractDeclaration* functions+=Function* nodes+=Node*)
	 */
	protected void sequence_Program(EObject context, Program semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (preTraceAnnotation=PreTraceAnnotation? var=Variable value=LUS_REAL)
	 */
	protected void sequence_RDeclaration(EObject context, RDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=LUS_REAL
	 */
	protected void sequence_RealLiteralExpression(EObject context, RealLiteralExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.REAL_LITERAL_EXPRESSION__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.REAL_LITERAL_EXPRESSION__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRealLiteralExpressionAccess().getValueLUS_REALTerminalRuleCall_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=RelExpression_RelExpression_1_0_0 op=RelOp right=NotExpression)
	 */
	protected void sequence_RelExpression(EObject context, RelExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.REL_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.REL_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.REL_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.REL_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.REL_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.REL_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelExpressionAccess().getOpRelOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getRelExpressionAccess().getRightNotExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (type=SpecificationType specLine=FbyExpression)
	 */
	protected void sequence_SpecificationLine(EObject context, SpecificationLine semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.SPECIFICATION_LINE__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.SPECIFICATION_LINE__TYPE));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.SPECIFICATION_LINE__SPEC_LINE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.SPECIFICATION_LINE__SPEC_LINE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSpecificationLineAccess().getTypeSpecificationTypeEnumRuleCall_2_0(), semanticObject.getType());
		feeder.accept(grammarAccess.getSpecificationLineAccess().getSpecLineFbyExpressionParserRuleCall_3_0(), semanticObject.getSpecLine());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     ((expressionList+=TupleExpression_TupleExpression_2_0_0 expressionList+=FbyExpression*) | expressionList+=TupleExpression_TupleExpression_2_0_0)
	 */
	protected void sequence_TupleExpression(EObject context, TupleExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (op=UnOp uexp=BaseExpression)
	 */
	protected void sequence_UnExpression(EObject context, UnExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.UN_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.UN_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.UN_EXPRESSION__UEXP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.UN_EXPRESSION__UEXP));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnExpressionAccess().getOpUnOpEnumRuleCall_1_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getUnExpressionAccess().getUexpBaseExpressionParserRuleCall_1_2_0(), semanticObject.getUexp());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (var=[Variable|ID] (dim+=LUS_INT dim+=LUS_INT?)?)
	 */
	protected void sequence_VariableCall(EObject context, VariableCall semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     variable=VariableCall
	 */
	protected void sequence_VariableExpression(EObject context, VariableExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.VARIABLE_EXPRESSION__VARIABLE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.VARIABLE_EXPRESSION__VARIABLE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getVariableExpressionAccess().getVariableVariableCallParserRuleCall_1_0(), semanticObject.getVariable());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Variable(EObject context, Variable semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.VARIABLE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.VARIABLE__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (preTraceAnnotation=PreTraceAnnotation? const?='const'? variables+=Variable variables+=Variable* type=DataType)
	 */
	protected void sequence_VariablesList(EObject context, VariablesList semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=WhenExpression_WhenExpression_1_0_0 op=WhenOp right=NorExpression)
	 */
	protected void sequence_WhenExpression(EObject context, WhenExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.WHEN_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.WHEN_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.WHEN_EXPRESSION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.WHEN_EXPRESSION__OP));
			if(transientValues.isValueTransient(semanticObject, LustrePackage.Literals.WHEN_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.WHEN_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getWhenExpressionAccess().getOpWhenOpEnumRuleCall_1_0_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getWhenExpressionAccess().getRightNorExpressionParserRuleCall_1_0_2_0(), semanticObject.getRight());
		feeder.finish();
	}
}
