/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.CallExpression#getElem <em>Elem</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.CallExpression#getArguments <em>Arguments</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getCallExpression()
 * @model
 * @generated
 */
public interface CallExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Elem</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elem</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elem</em>' reference.
   * @see #setElem(CallableElement)
   * @see geneauto.xtext.lustre.LustrePackage#getCallExpression_Elem()
   * @model
   * @generated
   */
  CallableElement getElem();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.CallExpression#getElem <em>Elem</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Elem</em>' reference.
   * @see #getElem()
   * @generated
   */
  void setElem(CallableElement value);

  /**
   * Returns the value of the '<em><b>Arguments</b></em>' containment reference list.
   * The list contents are of type {@link geneauto.xtext.lustre.AbstractExpression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arguments</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arguments</em>' containment reference list.
   * @see geneauto.xtext.lustre.LustrePackage#getCallExpression_Arguments()
   * @model containment="true"
   * @generated
   */
  EList<AbstractExpression> getArguments();

} // CallExpression
