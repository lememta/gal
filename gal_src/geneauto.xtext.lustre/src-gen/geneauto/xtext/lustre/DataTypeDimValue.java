/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Type Dim Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.DataTypeDimValue#getValue <em>Value</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.DataTypeDimValue#getRef <em>Ref</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getDataTypeDimValue()
 * @model
 * @generated
 */
public interface DataTypeDimValue extends EObject
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see geneauto.xtext.lustre.LustrePackage#getDataTypeDimValue_Value()
   * @model
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.DataTypeDimValue#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' reference.
   * @see #setRef(Variable)
   * @see geneauto.xtext.lustre.LustrePackage#getDataTypeDimValue_Ref()
   * @model
   * @generated
   */
  Variable getRef();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.DataTypeDimValue#getRef <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' reference.
   * @see #getRef()
   * @generated
   */
  void setRef(Variable value);

} // DataTypeDimValue
