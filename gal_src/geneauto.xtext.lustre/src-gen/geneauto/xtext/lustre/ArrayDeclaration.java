/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.ArrayDeclaration#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getArrayDeclaration()
 * @model
 * @generated
 */
public interface ArrayDeclaration extends AbstractDeclaration
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(ArrayDefinition)
   * @see geneauto.xtext.lustre.LustrePackage#getArrayDeclaration_Value()
   * @model containment="true"
   * @generated
   */
  ArrayDefinition getValue();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.ArrayDeclaration#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(ArrayDefinition value);

} // ArrayDeclaration
