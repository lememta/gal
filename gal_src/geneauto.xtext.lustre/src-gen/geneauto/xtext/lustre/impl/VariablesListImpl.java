/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.DataType;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.PreTraceAnnotation;
import geneauto.xtext.lustre.Variable;
import geneauto.xtext.lustre.VariablesList;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variables List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.VariablesListImpl#getPreTraceAnnotation <em>Pre Trace Annotation</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.VariablesListImpl#isConst <em>Const</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.VariablesListImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.VariablesListImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VariablesListImpl extends MinimalEObjectImpl.Container implements VariablesList
{
  /**
   * The cached value of the '{@link #getPreTraceAnnotation() <em>Pre Trace Annotation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPreTraceAnnotation()
   * @generated
   * @ordered
   */
  protected PreTraceAnnotation preTraceAnnotation;

  /**
   * The default value of the '{@link #isConst() <em>Const</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConst()
   * @generated
   * @ordered
   */
  protected static final boolean CONST_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isConst() <em>Const</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConst()
   * @generated
   * @ordered
   */
  protected boolean const_ = CONST_EDEFAULT;

  /**
   * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariables()
   * @generated
   * @ordered
   */
  protected EList<Variable> variables;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected DataType type;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VariablesListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.VARIABLES_LIST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PreTraceAnnotation getPreTraceAnnotation()
  {
    return preTraceAnnotation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation, NotificationChain msgs)
  {
    PreTraceAnnotation oldPreTraceAnnotation = preTraceAnnotation;
    preTraceAnnotation = newPreTraceAnnotation;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION, oldPreTraceAnnotation, newPreTraceAnnotation);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation)
  {
    if (newPreTraceAnnotation != preTraceAnnotation)
    {
      NotificationChain msgs = null;
      if (preTraceAnnotation != null)
        msgs = ((InternalEObject)preTraceAnnotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION, null, msgs);
      if (newPreTraceAnnotation != null)
        msgs = ((InternalEObject)newPreTraceAnnotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION, null, msgs);
      msgs = basicSetPreTraceAnnotation(newPreTraceAnnotation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION, newPreTraceAnnotation, newPreTraceAnnotation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isConst()
  {
    return const_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConst(boolean newConst)
  {
    boolean oldConst = const_;
    const_ = newConst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.VARIABLES_LIST__CONST, oldConst, const_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Variable> getVariables()
  {
    if (variables == null)
    {
      variables = new EObjectContainmentEList<Variable>(Variable.class, this, LustrePackage.VARIABLES_LIST__VARIABLES);
    }
    return variables;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataType getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(DataType newType, NotificationChain msgs)
  {
    DataType oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.VARIABLES_LIST__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(DataType newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.VARIABLES_LIST__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.VARIABLES_LIST__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.VARIABLES_LIST__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION:
        return basicSetPreTraceAnnotation(null, msgs);
      case LustrePackage.VARIABLES_LIST__VARIABLES:
        return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
      case LustrePackage.VARIABLES_LIST__TYPE:
        return basicSetType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION:
        return getPreTraceAnnotation();
      case LustrePackage.VARIABLES_LIST__CONST:
        return isConst();
      case LustrePackage.VARIABLES_LIST__VARIABLES:
        return getVariables();
      case LustrePackage.VARIABLES_LIST__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)newValue);
        return;
      case LustrePackage.VARIABLES_LIST__CONST:
        setConst((Boolean)newValue);
        return;
      case LustrePackage.VARIABLES_LIST__VARIABLES:
        getVariables().clear();
        getVariables().addAll((Collection<? extends Variable>)newValue);
        return;
      case LustrePackage.VARIABLES_LIST__TYPE:
        setType((DataType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)null);
        return;
      case LustrePackage.VARIABLES_LIST__CONST:
        setConst(CONST_EDEFAULT);
        return;
      case LustrePackage.VARIABLES_LIST__VARIABLES:
        getVariables().clear();
        return;
      case LustrePackage.VARIABLES_LIST__TYPE:
        setType((DataType)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.VARIABLES_LIST__PRE_TRACE_ANNOTATION:
        return preTraceAnnotation != null;
      case LustrePackage.VARIABLES_LIST__CONST:
        return const_ != CONST_EDEFAULT;
      case LustrePackage.VARIABLES_LIST__VARIABLES:
        return variables != null && !variables.isEmpty();
      case LustrePackage.VARIABLES_LIST__TYPE:
        return type != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (const: ");
    result.append(const_);
    result.append(')');
    return result.toString();
  }

} //VariablesListImpl
