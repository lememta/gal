/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.ArrayExpression;
import geneauto.xtext.lustre.LustrePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ArrayExpressionImpl extends ArrayDefinitionImpl implements ArrayExpression
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ArrayExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.ARRAY_EXPRESSION;
  }

} //ArrayExpressionImpl
