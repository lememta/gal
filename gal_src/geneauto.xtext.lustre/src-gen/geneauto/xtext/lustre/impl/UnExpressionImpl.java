/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.AbstractExpression;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.UnExpression;
import geneauto.xtext.lustre.UnOp;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Un Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.UnExpressionImpl#getOp <em>Op</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.UnExpressionImpl#getUexp <em>Uexp</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UnExpressionImpl extends AbstractExpressionImpl implements UnExpression
{
  /**
   * The default value of the '{@link #getOp() <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOp()
   * @generated
   * @ordered
   */
  protected static final UnOp OP_EDEFAULT = UnOp.PRE;

  /**
   * The cached value of the '{@link #getOp() <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOp()
   * @generated
   * @ordered
   */
  protected UnOp op = OP_EDEFAULT;

  /**
   * The cached value of the '{@link #getUexp() <em>Uexp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUexp()
   * @generated
   * @ordered
   */
  protected AbstractExpression uexp;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UnExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.UN_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnOp getOp()
  {
    return op;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOp(UnOp newOp)
  {
    UnOp oldOp = op;
    op = newOp == null ? OP_EDEFAULT : newOp;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.UN_EXPRESSION__OP, oldOp, op));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractExpression getUexp()
  {
    return uexp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUexp(AbstractExpression newUexp, NotificationChain msgs)
  {
    AbstractExpression oldUexp = uexp;
    uexp = newUexp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.UN_EXPRESSION__UEXP, oldUexp, newUexp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUexp(AbstractExpression newUexp)
  {
    if (newUexp != uexp)
    {
      NotificationChain msgs = null;
      if (uexp != null)
        msgs = ((InternalEObject)uexp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.UN_EXPRESSION__UEXP, null, msgs);
      if (newUexp != null)
        msgs = ((InternalEObject)newUexp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.UN_EXPRESSION__UEXP, null, msgs);
      msgs = basicSetUexp(newUexp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.UN_EXPRESSION__UEXP, newUexp, newUexp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.UN_EXPRESSION__UEXP:
        return basicSetUexp(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.UN_EXPRESSION__OP:
        return getOp();
      case LustrePackage.UN_EXPRESSION__UEXP:
        return getUexp();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.UN_EXPRESSION__OP:
        setOp((UnOp)newValue);
        return;
      case LustrePackage.UN_EXPRESSION__UEXP:
        setUexp((AbstractExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.UN_EXPRESSION__OP:
        setOp(OP_EDEFAULT);
        return;
      case LustrePackage.UN_EXPRESSION__UEXP:
        setUexp((AbstractExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.UN_EXPRESSION__OP:
        return op != OP_EDEFAULT;
      case LustrePackage.UN_EXPRESSION__UEXP:
        return uexp != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (op: ");
    result.append(op);
    result.append(')');
    return result.toString();
  }

} //UnExpressionImpl
