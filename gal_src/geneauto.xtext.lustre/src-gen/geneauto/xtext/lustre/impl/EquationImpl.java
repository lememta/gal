/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.AbstractExpression;
import geneauto.xtext.lustre.Equation;
import geneauto.xtext.lustre.LeftVariables;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.PreTraceAnnotation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.EquationImpl#getPreTraceAnnotation <em>Pre Trace Annotation</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.EquationImpl#getLeftPart <em>Left Part</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.EquationImpl#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EquationImpl extends MinimalEObjectImpl.Container implements Equation
{
  /**
   * The cached value of the '{@link #getPreTraceAnnotation() <em>Pre Trace Annotation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPreTraceAnnotation()
   * @generated
   * @ordered
   */
  protected PreTraceAnnotation preTraceAnnotation;

  /**
   * The cached value of the '{@link #getLeftPart() <em>Left Part</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLeftPart()
   * @generated
   * @ordered
   */
  protected LeftVariables leftPart;

  /**
   * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression()
   * @generated
   * @ordered
   */
  protected AbstractExpression expression;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EquationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.EQUATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PreTraceAnnotation getPreTraceAnnotation()
  {
    return preTraceAnnotation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation, NotificationChain msgs)
  {
    PreTraceAnnotation oldPreTraceAnnotation = preTraceAnnotation;
    preTraceAnnotation = newPreTraceAnnotation;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.EQUATION__PRE_TRACE_ANNOTATION, oldPreTraceAnnotation, newPreTraceAnnotation);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation)
  {
    if (newPreTraceAnnotation != preTraceAnnotation)
    {
      NotificationChain msgs = null;
      if (preTraceAnnotation != null)
        msgs = ((InternalEObject)preTraceAnnotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.EQUATION__PRE_TRACE_ANNOTATION, null, msgs);
      if (newPreTraceAnnotation != null)
        msgs = ((InternalEObject)newPreTraceAnnotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.EQUATION__PRE_TRACE_ANNOTATION, null, msgs);
      msgs = basicSetPreTraceAnnotation(newPreTraceAnnotation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.EQUATION__PRE_TRACE_ANNOTATION, newPreTraceAnnotation, newPreTraceAnnotation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LeftVariables getLeftPart()
  {
    return leftPart;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLeftPart(LeftVariables newLeftPart, NotificationChain msgs)
  {
    LeftVariables oldLeftPart = leftPart;
    leftPart = newLeftPart;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.EQUATION__LEFT_PART, oldLeftPart, newLeftPart);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLeftPart(LeftVariables newLeftPart)
  {
    if (newLeftPart != leftPart)
    {
      NotificationChain msgs = null;
      if (leftPart != null)
        msgs = ((InternalEObject)leftPart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.EQUATION__LEFT_PART, null, msgs);
      if (newLeftPart != null)
        msgs = ((InternalEObject)newLeftPart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.EQUATION__LEFT_PART, null, msgs);
      msgs = basicSetLeftPart(newLeftPart, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.EQUATION__LEFT_PART, newLeftPart, newLeftPart));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractExpression getExpression()
  {
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpression(AbstractExpression newExpression, NotificationChain msgs)
  {
    AbstractExpression oldExpression = expression;
    expression = newExpression;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.EQUATION__EXPRESSION, oldExpression, newExpression);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpression(AbstractExpression newExpression)
  {
    if (newExpression != expression)
    {
      NotificationChain msgs = null;
      if (expression != null)
        msgs = ((InternalEObject)expression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.EQUATION__EXPRESSION, null, msgs);
      if (newExpression != null)
        msgs = ((InternalEObject)newExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.EQUATION__EXPRESSION, null, msgs);
      msgs = basicSetExpression(newExpression, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.EQUATION__EXPRESSION, newExpression, newExpression));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.EQUATION__PRE_TRACE_ANNOTATION:
        return basicSetPreTraceAnnotation(null, msgs);
      case LustrePackage.EQUATION__LEFT_PART:
        return basicSetLeftPart(null, msgs);
      case LustrePackage.EQUATION__EXPRESSION:
        return basicSetExpression(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.EQUATION__PRE_TRACE_ANNOTATION:
        return getPreTraceAnnotation();
      case LustrePackage.EQUATION__LEFT_PART:
        return getLeftPart();
      case LustrePackage.EQUATION__EXPRESSION:
        return getExpression();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.EQUATION__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)newValue);
        return;
      case LustrePackage.EQUATION__LEFT_PART:
        setLeftPart((LeftVariables)newValue);
        return;
      case LustrePackage.EQUATION__EXPRESSION:
        setExpression((AbstractExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.EQUATION__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)null);
        return;
      case LustrePackage.EQUATION__LEFT_PART:
        setLeftPart((LeftVariables)null);
        return;
      case LustrePackage.EQUATION__EXPRESSION:
        setExpression((AbstractExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.EQUATION__PRE_TRACE_ANNOTATION:
        return preTraceAnnotation != null;
      case LustrePackage.EQUATION__LEFT_PART:
        return leftPart != null;
      case LustrePackage.EQUATION__EXPRESSION:
        return expression != null;
    }
    return super.eIsSet(featureID);
  }

} //EquationImpl
