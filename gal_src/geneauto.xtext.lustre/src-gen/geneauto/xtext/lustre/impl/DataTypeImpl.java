/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.BasicType;
import geneauto.xtext.lustre.DataType;
import geneauto.xtext.lustre.DataTypeDimValue;
import geneauto.xtext.lustre.LustrePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.DataTypeImpl#getBaseType <em>Base Type</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.DataTypeImpl#getDim <em>Dim</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DataTypeImpl extends MinimalEObjectImpl.Container implements DataType
{
  /**
   * The default value of the '{@link #getBaseType() <em>Base Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseType()
   * @generated
   * @ordered
   */
  protected static final BasicType BASE_TYPE_EDEFAULT = BasicType.INTEGER;

  /**
   * The cached value of the '{@link #getBaseType() <em>Base Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseType()
   * @generated
   * @ordered
   */
  protected BasicType baseType = BASE_TYPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getDim() <em>Dim</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDim()
   * @generated
   * @ordered
   */
  protected EList<DataTypeDimValue> dim;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DataTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.DATA_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicType getBaseType()
  {
    return baseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBaseType(BasicType newBaseType)
  {
    BasicType oldBaseType = baseType;
    baseType = newBaseType == null ? BASE_TYPE_EDEFAULT : newBaseType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.DATA_TYPE__BASE_TYPE, oldBaseType, baseType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<DataTypeDimValue> getDim()
  {
    if (dim == null)
    {
      dim = new EObjectContainmentEList<DataTypeDimValue>(DataTypeDimValue.class, this, LustrePackage.DATA_TYPE__DIM);
    }
    return dim;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.DATA_TYPE__DIM:
        return ((InternalEList<?>)getDim()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.DATA_TYPE__BASE_TYPE:
        return getBaseType();
      case LustrePackage.DATA_TYPE__DIM:
        return getDim();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.DATA_TYPE__BASE_TYPE:
        setBaseType((BasicType)newValue);
        return;
      case LustrePackage.DATA_TYPE__DIM:
        getDim().clear();
        getDim().addAll((Collection<? extends DataTypeDimValue>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.DATA_TYPE__BASE_TYPE:
        setBaseType(BASE_TYPE_EDEFAULT);
        return;
      case LustrePackage.DATA_TYPE__DIM:
        getDim().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.DATA_TYPE__BASE_TYPE:
        return baseType != BASE_TYPE_EDEFAULT;
      case LustrePackage.DATA_TYPE__DIM:
        return dim != null && !dim.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (baseType: ");
    result.append(baseType);
    result.append(')');
    return result.toString();
  }

} //DataTypeImpl
