/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.AbstractDeclaration;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.PreTraceAnnotation;
import geneauto.xtext.lustre.Variable;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.AbstractDeclarationImpl#getPreTraceAnnotation <em>Pre Trace Annotation</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.AbstractDeclarationImpl#getVar <em>Var</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AbstractDeclarationImpl extends MinimalEObjectImpl.Container implements AbstractDeclaration
{
  /**
   * The cached value of the '{@link #getPreTraceAnnotation() <em>Pre Trace Annotation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPreTraceAnnotation()
   * @generated
   * @ordered
   */
  protected PreTraceAnnotation preTraceAnnotation;

  /**
   * The cached value of the '{@link #getVar() <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVar()
   * @generated
   * @ordered
   */
  protected Variable var;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AbstractDeclarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.ABSTRACT_DECLARATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PreTraceAnnotation getPreTraceAnnotation()
  {
    return preTraceAnnotation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation, NotificationChain msgs)
  {
    PreTraceAnnotation oldPreTraceAnnotation = preTraceAnnotation;
    preTraceAnnotation = newPreTraceAnnotation;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION, oldPreTraceAnnotation, newPreTraceAnnotation);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation)
  {
    if (newPreTraceAnnotation != preTraceAnnotation)
    {
      NotificationChain msgs = null;
      if (preTraceAnnotation != null)
        msgs = ((InternalEObject)preTraceAnnotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION, null, msgs);
      if (newPreTraceAnnotation != null)
        msgs = ((InternalEObject)newPreTraceAnnotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION, null, msgs);
      msgs = basicSetPreTraceAnnotation(newPreTraceAnnotation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION, newPreTraceAnnotation, newPreTraceAnnotation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable getVar()
  {
    return var;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVar(Variable newVar, NotificationChain msgs)
  {
    Variable oldVar = var;
    var = newVar;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.ABSTRACT_DECLARATION__VAR, oldVar, newVar);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVar(Variable newVar)
  {
    if (newVar != var)
    {
      NotificationChain msgs = null;
      if (var != null)
        msgs = ((InternalEObject)var).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.ABSTRACT_DECLARATION__VAR, null, msgs);
      if (newVar != null)
        msgs = ((InternalEObject)newVar).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.ABSTRACT_DECLARATION__VAR, null, msgs);
      msgs = basicSetVar(newVar, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.ABSTRACT_DECLARATION__VAR, newVar, newVar));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION:
        return basicSetPreTraceAnnotation(null, msgs);
      case LustrePackage.ABSTRACT_DECLARATION__VAR:
        return basicSetVar(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION:
        return getPreTraceAnnotation();
      case LustrePackage.ABSTRACT_DECLARATION__VAR:
        return getVar();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)newValue);
        return;
      case LustrePackage.ABSTRACT_DECLARATION__VAR:
        setVar((Variable)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)null);
        return;
      case LustrePackage.ABSTRACT_DECLARATION__VAR:
        setVar((Variable)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION:
        return preTraceAnnotation != null;
      case LustrePackage.ABSTRACT_DECLARATION__VAR:
        return var != null;
    }
    return super.eIsSet(featureID);
  }

} //AbstractDeclarationImpl
