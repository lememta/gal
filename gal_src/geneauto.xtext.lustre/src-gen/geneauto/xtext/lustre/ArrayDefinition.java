/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.ArrayDefinition#getDimVal <em>Dim Val</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getArrayDefinition()
 * @model
 * @generated
 */
public interface ArrayDefinition extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Dim Val</b></em>' containment reference list.
   * The list contents are of type {@link geneauto.xtext.lustre.AbstractExpression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dim Val</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dim Val</em>' containment reference list.
   * @see geneauto.xtext.lustre.LustrePackage#getArrayDefinition_DimVal()
   * @model containment="true"
   * @generated
   */
  EList<AbstractExpression> getDimVal();

} // ArrayDefinition
