/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Matrix Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.xtext.lustre.LustrePackage#getMatrixExpression()
 * @model
 * @generated
 */
public interface MatrixExpression extends ArrayDefinition
{
} // MatrixExpression
