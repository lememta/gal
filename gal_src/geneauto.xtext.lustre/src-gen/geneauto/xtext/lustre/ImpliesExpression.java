/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Implies Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.ImpliesExpression#getLeft <em>Left</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.ImpliesExpression#getOp <em>Op</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.ImpliesExpression#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getImpliesExpression()
 * @model
 * @generated
 */
public interface ImpliesExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getImpliesExpression_Left()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getLeft();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.ImpliesExpression#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(AbstractExpression value);

  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute.
   * The literals are from the enumeration {@link geneauto.xtext.lustre.ImpliesOp}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.ImpliesOp
   * @see #setOp(ImpliesOp)
   * @see geneauto.xtext.lustre.LustrePackage#getImpliesExpression_Op()
   * @model
   * @generated
   */
  ImpliesOp getOp();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.ImpliesExpression#getOp <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.ImpliesOp
   * @see #getOp()
   * @generated
   */
  void setOp(ImpliesOp value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getImpliesExpression_Right()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getRight();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.ImpliesExpression#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(AbstractExpression value);

} // ImpliesExpression
