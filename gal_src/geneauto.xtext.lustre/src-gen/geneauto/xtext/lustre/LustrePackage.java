/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.xtext.lustre.LustreFactory
 * @model kind="package"
 * @generated
 */
public interface LustrePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "lustre";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.geneauto/Lustre";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "lustre";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  LustrePackage eINSTANCE = geneauto.xtext.lustre.impl.LustrePackageImpl.init();

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.ProgramImpl <em>Program</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.ProgramImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getProgram()
   * @generated
   */
  int PROGRAM = 0;

  /**
   * The feature id for the '<em><b>Libraries</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__LIBRARIES = 0;

  /**
   * The feature id for the '<em><b>Declarations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__DECLARATIONS = 1;

  /**
   * The feature id for the '<em><b>Functions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__FUNCTIONS = 2;

  /**
   * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__NODES = 3;

  /**
   * The number of structural features of the '<em>Program</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.OpenImpl <em>Open</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.OpenImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOpen()
   * @generated
   */
  int OPEN = 1;

  /**
   * The feature id for the '<em><b>Import URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN__IMPORT_URI = 0;

  /**
   * The number of structural features of the '<em>Open</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.CallableElementImpl <em>Callable Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.CallableElementImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getCallableElement()
   * @generated
   */
  int CALLABLE_ELEMENT = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALLABLE_ELEMENT__NAME = 0;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALLABLE_ELEMENT__INPUTS = 1;

  /**
   * The feature id for the '<em><b>Outputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALLABLE_ELEMENT__OUTPUTS = 2;

  /**
   * The number of structural features of the '<em>Callable Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALLABLE_ELEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.NodeImpl <em>Node</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.NodeImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNode()
   * @generated
   */
  int NODE = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE__NAME = CALLABLE_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE__INPUTS = CALLABLE_ELEMENT__INPUTS;

  /**
   * The feature id for the '<em><b>Outputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE__OUTPUTS = CALLABLE_ELEMENT__OUTPUTS;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE__PRE_TRACE_ANNOTATION = CALLABLE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Specifications</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE__SPECIFICATIONS = CALLABLE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Locals</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE__LOCALS = CALLABLE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE__BODY = CALLABLE_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NODE_FEATURE_COUNT = CALLABLE_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.FunctionImpl <em>Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.FunctionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getFunction()
   * @generated
   */
  int FUNCTION = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__NAME = CALLABLE_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__INPUTS = CALLABLE_ELEMENT__INPUTS;

  /**
   * The feature id for the '<em><b>Outputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__OUTPUTS = CALLABLE_ELEMENT__OUTPUTS;

  /**
   * The number of structural features of the '<em>Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FEATURE_COUNT = CALLABLE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.AbstractDeclarationImpl <em>Abstract Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.AbstractDeclarationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAbstractDeclaration()
   * @generated
   */
  int ABSTRACT_DECLARATION = 5;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION = 0;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_DECLARATION__VAR = 1;

  /**
   * The number of structural features of the '<em>Abstract Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_DECLARATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.IDeclarationImpl <em>IDeclaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.IDeclarationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getIDeclaration()
   * @generated
   */
  int IDECLARATION = 6;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDECLARATION__PRE_TRACE_ANNOTATION = ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDECLARATION__VAR = ABSTRACT_DECLARATION__VAR;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDECLARATION__VALUE = ABSTRACT_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>IDeclaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDECLARATION_FEATURE_COUNT = ABSTRACT_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.DDeclarationImpl <em>DDeclaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.DDeclarationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDDeclaration()
   * @generated
   */
  int DDECLARATION = 7;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DDECLARATION__PRE_TRACE_ANNOTATION = ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DDECLARATION__VAR = ABSTRACT_DECLARATION__VAR;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DDECLARATION__VALUE = ABSTRACT_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>DDeclaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DDECLARATION_FEATURE_COUNT = ABSTRACT_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.RDeclarationImpl <em>RDeclaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.RDeclarationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRDeclaration()
   * @generated
   */
  int RDECLARATION = 8;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDECLARATION__PRE_TRACE_ANNOTATION = ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDECLARATION__VAR = ABSTRACT_DECLARATION__VAR;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDECLARATION__VALUE = ABSTRACT_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>RDeclaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDECLARATION_FEATURE_COUNT = ABSTRACT_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.BDeclarationImpl <em>BDeclaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.BDeclarationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBDeclaration()
   * @generated
   */
  int BDECLARATION = 9;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BDECLARATION__PRE_TRACE_ANNOTATION = ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BDECLARATION__VAR = ABSTRACT_DECLARATION__VAR;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BDECLARATION__VALUE = ABSTRACT_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>BDeclaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BDECLARATION_FEATURE_COUNT = ABSTRACT_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.ArrayDeclarationImpl <em>Array Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.ArrayDeclarationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getArrayDeclaration()
   * @generated
   */
  int ARRAY_DECLARATION = 10;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DECLARATION__PRE_TRACE_ANNOTATION = ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DECLARATION__VAR = ABSTRACT_DECLARATION__VAR;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DECLARATION__VALUE = ABSTRACT_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Array Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DECLARATION_FEATURE_COUNT = ABSTRACT_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.SpecificationLineImpl <em>Specification Line</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.SpecificationLineImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getSpecificationLine()
   * @generated
   */
  int SPECIFICATION_LINE = 11;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECIFICATION_LINE__TYPE = 0;

  /**
   * The feature id for the '<em><b>Spec Line</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECIFICATION_LINE__SPEC_LINE = 1;

  /**
   * The number of structural features of the '<em>Specification Line</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECIFICATION_LINE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.InputsImpl <em>Inputs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.InputsImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getInputs()
   * @generated
   */
  int INPUTS = 12;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUTS__INPUTS = 0;

  /**
   * The number of structural features of the '<em>Inputs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUTS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.OutputsImpl <em>Outputs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.OutputsImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOutputs()
   * @generated
   */
  int OUTPUTS = 13;

  /**
   * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUTS__OUTPUTS = 0;

  /**
   * The number of structural features of the '<em>Outputs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUTS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.LocalsImpl <em>Locals</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.LocalsImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getLocals()
   * @generated
   */
  int LOCALS = 14;

  /**
   * The feature id for the '<em><b>Locals</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOCALS__LOCALS = 0;

  /**
   * The number of structural features of the '<em>Locals</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOCALS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.BodyImpl <em>Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.BodyImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBody()
   * @generated
   */
  int BODY = 15;

  /**
   * The feature id for the '<em><b>Equations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BODY__EQUATIONS = 0;

  /**
   * The feature id for the '<em><b>Asserts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BODY__ASSERTS = 1;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BODY__ANNOTATIONS = 2;

  /**
   * The number of structural features of the '<em>Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BODY_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.AnnotationImpl <em>Annotation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.AnnotationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAnnotation()
   * @generated
   */
  int ANNOTATION = 16;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION__IDENTIFIER = 0;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION__EXPRESSION = 1;

  /**
   * The number of structural features of the '<em>Annotation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.PreTraceAnnotationImpl <em>Pre Trace Annotation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.PreTraceAnnotationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getPreTraceAnnotation()
   * @generated
   */
  int PRE_TRACE_ANNOTATION = 17;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRE_TRACE_ANNOTATION__TYPE = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRE_TRACE_ANNOTATION__VALUE = 1;

  /**
   * The number of structural features of the '<em>Pre Trace Annotation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRE_TRACE_ANNOTATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.EquationImpl <em>Equation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.EquationImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getEquation()
   * @generated
   */
  int EQUATION = 18;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUATION__PRE_TRACE_ANNOTATION = 0;

  /**
   * The feature id for the '<em><b>Left Part</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUATION__LEFT_PART = 1;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUATION__EXPRESSION = 2;

  /**
   * The number of structural features of the '<em>Equation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.AssertImpl <em>Assert</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.AssertImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAssert()
   * @generated
   */
  int ASSERT = 19;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSERT__EXPRESSION = 0;

  /**
   * The number of structural features of the '<em>Assert</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSERT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.LeftVariablesImpl <em>Left Variables</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.LeftVariablesImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getLeftVariables()
   * @generated
   */
  int LEFT_VARIABLES = 20;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEFT_VARIABLES__VARIABLES = 0;

  /**
   * The number of structural features of the '<em>Left Variables</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEFT_VARIABLES_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.VariablesListImpl <em>Variables List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.VariablesListImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariablesList()
   * @generated
   */
  int VARIABLES_LIST = 21;

  /**
   * The feature id for the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_LIST__PRE_TRACE_ANNOTATION = 0;

  /**
   * The feature id for the '<em><b>Const</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_LIST__CONST = 1;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_LIST__VARIABLES = 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_LIST__TYPE = 3;

  /**
   * The number of structural features of the '<em>Variables List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_LIST_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.VariableCallImpl <em>Variable Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.VariableCallImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariableCall()
   * @generated
   */
  int VARIABLE_CALL = 22;

  /**
   * The feature id for the '<em><b>Var</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_CALL__VAR = 0;

  /**
   * The feature id for the '<em><b>Dim</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_CALL__DIM = 1;

  /**
   * The number of structural features of the '<em>Variable Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_CALL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.VariableImpl <em>Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.VariableImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariable()
   * @generated
   */
  int VARIABLE = 23;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__NAME = 0;

  /**
   * The number of structural features of the '<em>Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.DataTypeImpl <em>Data Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.DataTypeImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDataType()
   * @generated
   */
  int DATA_TYPE = 24;

  /**
   * The feature id for the '<em><b>Base Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE__BASE_TYPE = 0;

  /**
   * The feature id for the '<em><b>Dim</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE__DIM = 1;

  /**
   * The number of structural features of the '<em>Data Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.DataTypeDimValueImpl <em>Data Type Dim Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.DataTypeDimValueImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDataTypeDimValue()
   * @generated
   */
  int DATA_TYPE_DIM_VALUE = 25;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE_DIM_VALUE__VALUE = 0;

  /**
   * The feature id for the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE_DIM_VALUE__REF = 1;

  /**
   * The number of structural features of the '<em>Data Type Dim Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_TYPE_DIM_VALUE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.AbstractExpressionImpl <em>Abstract Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.AbstractExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAbstractExpression()
   * @generated
   */
  int ABSTRACT_EXPRESSION = 26;

  /**
   * The number of structural features of the '<em>Abstract Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.ArrayDefinitionImpl <em>Array Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.ArrayDefinitionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getArrayDefinition()
   * @generated
   */
  int ARRAY_DEFINITION = 27;

  /**
   * The feature id for the '<em><b>Dim Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DEFINITION__DIM_VAL = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Array Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DEFINITION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.MatrixExpressionImpl <em>Matrix Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.MatrixExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getMatrixExpression()
   * @generated
   */
  int MATRIX_EXPRESSION = 28;

  /**
   * The feature id for the '<em><b>Dim Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATRIX_EXPRESSION__DIM_VAL = ARRAY_DEFINITION__DIM_VAL;

  /**
   * The number of structural features of the '<em>Matrix Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATRIX_EXPRESSION_FEATURE_COUNT = ARRAY_DEFINITION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.ArrayExpressionImpl <em>Array Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.ArrayExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getArrayExpression()
   * @generated
   */
  int ARRAY_EXPRESSION = 29;

  /**
   * The feature id for the '<em><b>Dim Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_EXPRESSION__DIM_VAL = ARRAY_DEFINITION__DIM_VAL;

  /**
   * The number of structural features of the '<em>Array Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_EXPRESSION_FEATURE_COUNT = ARRAY_DEFINITION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.IteExpressionImpl <em>Ite Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.IteExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getIteExpression()
   * @generated
   */
  int ITE_EXPRESSION = 30;

  /**
   * The feature id for the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITE_EXPRESSION__GUARD = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITE_EXPRESSION__THEN = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITE_EXPRESSION__ELSE = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Ite Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITE_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.FbyExpressionImpl <em>Fby Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.FbyExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getFbyExpression()
   * @generated
   */
  int FBY_EXPRESSION = 31;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBY_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBY_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBY_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Fby Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FBY_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.OrExpressionImpl <em>Or Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.OrExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOrExpression()
   * @generated
   */
  int OR_EXPRESSION = 32;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Or Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.AndExpressionImpl <em>And Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.AndExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAndExpression()
   * @generated
   */
  int AND_EXPRESSION = 33;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>And Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.ImpliesExpressionImpl <em>Implies Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.ImpliesExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getImpliesExpression()
   * @generated
   */
  int IMPLIES_EXPRESSION = 34;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLIES_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLIES_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLIES_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Implies Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLIES_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.RelExpressionImpl <em>Rel Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.RelExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRelExpression()
   * @generated
   */
  int REL_EXPRESSION = 35;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REL_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REL_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REL_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Rel Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REL_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.NotExpressionImpl <em>Not Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.NotExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNotExpression()
   * @generated
   */
  int NOT_EXPRESSION = 36;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Uexp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_EXPRESSION__UEXP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Not Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.AddExpressionImpl <em>Add Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.AddExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAddExpression()
   * @generated
   */
  int ADD_EXPRESSION = 37;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Add Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.MultExpressionImpl <em>Mult Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.MultExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getMultExpression()
   * @generated
   */
  int MULT_EXPRESSION = 38;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULT_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULT_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULT_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Mult Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULT_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.WhenExpressionImpl <em>When Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.WhenExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getWhenExpression()
   * @generated
   */
  int WHEN_EXPRESSION = 39;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHEN_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHEN_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHEN_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>When Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHEN_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.NorExpressionImpl <em>Nor Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.NorExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNorExpression()
   * @generated
   */
  int NOR_EXPRESSION = 40;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOR_EXPRESSION__LEFT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOR_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOR_EXPRESSION__RIGHT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Nor Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOR_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.UnExpressionImpl <em>Un Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.UnExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getUnExpression()
   * @generated
   */
  int UN_EXPRESSION = 41;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UN_EXPRESSION__OP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Uexp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UN_EXPRESSION__UEXP = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Un Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UN_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.VariableExpressionImpl <em>Variable Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.VariableExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariableExpression()
   * @generated
   */
  int VARIABLE_EXPRESSION = 42;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_EXPRESSION__VARIABLE = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.TupleExpressionImpl <em>Tuple Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.TupleExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getTupleExpression()
   * @generated
   */
  int TUPLE_EXPRESSION = 43;

  /**
   * The feature id for the '<em><b>Expression List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUPLE_EXPRESSION__EXPRESSION_LIST = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Tuple Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUPLE_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.IntegerLiteralExpressionImpl <em>Integer Literal Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.IntegerLiteralExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getIntegerLiteralExpression()
   * @generated
   */
  int INTEGER_LITERAL_EXPRESSION = 44;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL_EXPRESSION__VALUE = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Integer Literal Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.RealLiteralExpressionImpl <em>Real Literal Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.RealLiteralExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRealLiteralExpression()
   * @generated
   */
  int REAL_LITERAL_EXPRESSION = 45;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL_EXPRESSION__VALUE = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Real Literal Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.DoubleLiteralExpressionImpl <em>Double Literal Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.DoubleLiteralExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDoubleLiteralExpression()
   * @generated
   */
  int DOUBLE_LITERAL_EXPRESSION = 46;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_LITERAL_EXPRESSION__VALUE = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Double Literal Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOUBLE_LITERAL_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.BooleanLiteralExpressionImpl <em>Boolean Literal Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.BooleanLiteralExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBooleanLiteralExpression()
   * @generated
   */
  int BOOLEAN_LITERAL_EXPRESSION = 47;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL_EXPRESSION__VALUE = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Boolean Literal Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.impl.CallExpressionImpl <em>Call Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.impl.CallExpressionImpl
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getCallExpression()
   * @generated
   */
  int CALL_EXPRESSION = 48;

  /**
   * The feature id for the '<em><b>Elem</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_EXPRESSION__ELEM = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_EXPRESSION__ARGUMENTS = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Call Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_EXPRESSION_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.SpecificationType <em>Specification Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.SpecificationType
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getSpecificationType()
   * @generated
   */
  int SPECIFICATION_TYPE = 49;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.BasicType <em>Basic Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.BasicType
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBasicType()
   * @generated
   */
  int BASIC_TYPE = 50;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.FbyOp <em>Fby Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.FbyOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getFbyOp()
   * @generated
   */
  int FBY_OP = 51;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.OrOp <em>Or Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.OrOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOrOp()
   * @generated
   */
  int OR_OP = 52;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.AndOp <em>And Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.AndOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAndOp()
   * @generated
   */
  int AND_OP = 53;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.ImpliesOp <em>Implies Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.ImpliesOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getImpliesOp()
   * @generated
   */
  int IMPLIES_OP = 54;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.RelOp <em>Rel Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.RelOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRelOp()
   * @generated
   */
  int REL_OP = 55;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.NotOp <em>Not Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.NotOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNotOp()
   * @generated
   */
  int NOT_OP = 56;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.AddOp <em>Add Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.AddOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAddOp()
   * @generated
   */
  int ADD_OP = 57;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.MultOp <em>Mult Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.MultOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getMultOp()
   * @generated
   */
  int MULT_OP = 58;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.WhenOp <em>When Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.WhenOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getWhenOp()
   * @generated
   */
  int WHEN_OP = 59;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.NorOp <em>Nor Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.NorOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNorOp()
   * @generated
   */
  int NOR_OP = 60;

  /**
   * The meta object id for the '{@link geneauto.xtext.lustre.UnOp <em>Un Op</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see geneauto.xtext.lustre.UnOp
   * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getUnOp()
   * @generated
   */
  int UN_OP = 61;


  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Program <em>Program</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Program</em>'.
   * @see geneauto.xtext.lustre.Program
   * @generated
   */
  EClass getProgram();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Program#getLibraries <em>Libraries</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Libraries</em>'.
   * @see geneauto.xtext.lustre.Program#getLibraries()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Libraries();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Program#getDeclarations <em>Declarations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Declarations</em>'.
   * @see geneauto.xtext.lustre.Program#getDeclarations()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Declarations();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Program#getFunctions <em>Functions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Functions</em>'.
   * @see geneauto.xtext.lustre.Program#getFunctions()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Functions();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Program#getNodes <em>Nodes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Nodes</em>'.
   * @see geneauto.xtext.lustre.Program#getNodes()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Nodes();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Open <em>Open</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Open</em>'.
   * @see geneauto.xtext.lustre.Open
   * @generated
   */
  EClass getOpen();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.Open#getImportURI <em>Import URI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Import URI</em>'.
   * @see geneauto.xtext.lustre.Open#getImportURI()
   * @see #getOpen()
   * @generated
   */
  EAttribute getOpen_ImportURI();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.CallableElement <em>Callable Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Callable Element</em>'.
   * @see geneauto.xtext.lustre.CallableElement
   * @generated
   */
  EClass getCallableElement();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.CallableElement#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see geneauto.xtext.lustre.CallableElement#getName()
   * @see #getCallableElement()
   * @generated
   */
  EAttribute getCallableElement_Name();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.CallableElement#getInputs <em>Inputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Inputs</em>'.
   * @see geneauto.xtext.lustre.CallableElement#getInputs()
   * @see #getCallableElement()
   * @generated
   */
  EReference getCallableElement_Inputs();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.CallableElement#getOutputs <em>Outputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Outputs</em>'.
   * @see geneauto.xtext.lustre.CallableElement#getOutputs()
   * @see #getCallableElement()
   * @generated
   */
  EReference getCallableElement_Outputs();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Node <em>Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Node</em>'.
   * @see geneauto.xtext.lustre.Node
   * @generated
   */
  EClass getNode();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Node#getPreTraceAnnotation <em>Pre Trace Annotation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pre Trace Annotation</em>'.
   * @see geneauto.xtext.lustre.Node#getPreTraceAnnotation()
   * @see #getNode()
   * @generated
   */
  EReference getNode_PreTraceAnnotation();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Node#getSpecifications <em>Specifications</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Specifications</em>'.
   * @see geneauto.xtext.lustre.Node#getSpecifications()
   * @see #getNode()
   * @generated
   */
  EReference getNode_Specifications();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Node#getLocals <em>Locals</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Locals</em>'.
   * @see geneauto.xtext.lustre.Node#getLocals()
   * @see #getNode()
   * @generated
   */
  EReference getNode_Locals();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Node#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see geneauto.xtext.lustre.Node#getBody()
   * @see #getNode()
   * @generated
   */
  EReference getNode_Body();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function</em>'.
   * @see geneauto.xtext.lustre.Function
   * @generated
   */
  EClass getFunction();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.AbstractDeclaration <em>Abstract Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Abstract Declaration</em>'.
   * @see geneauto.xtext.lustre.AbstractDeclaration
   * @generated
   */
  EClass getAbstractDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.AbstractDeclaration#getPreTraceAnnotation <em>Pre Trace Annotation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pre Trace Annotation</em>'.
   * @see geneauto.xtext.lustre.AbstractDeclaration#getPreTraceAnnotation()
   * @see #getAbstractDeclaration()
   * @generated
   */
  EReference getAbstractDeclaration_PreTraceAnnotation();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.AbstractDeclaration#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see geneauto.xtext.lustre.AbstractDeclaration#getVar()
   * @see #getAbstractDeclaration()
   * @generated
   */
  EReference getAbstractDeclaration_Var();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.IDeclaration <em>IDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>IDeclaration</em>'.
   * @see geneauto.xtext.lustre.IDeclaration
   * @generated
   */
  EClass getIDeclaration();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.IDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.IDeclaration#getValue()
   * @see #getIDeclaration()
   * @generated
   */
  EAttribute getIDeclaration_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.DDeclaration <em>DDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>DDeclaration</em>'.
   * @see geneauto.xtext.lustre.DDeclaration
   * @generated
   */
  EClass getDDeclaration();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.DDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.DDeclaration#getValue()
   * @see #getDDeclaration()
   * @generated
   */
  EAttribute getDDeclaration_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.RDeclaration <em>RDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RDeclaration</em>'.
   * @see geneauto.xtext.lustre.RDeclaration
   * @generated
   */
  EClass getRDeclaration();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.RDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.RDeclaration#getValue()
   * @see #getRDeclaration()
   * @generated
   */
  EAttribute getRDeclaration_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.BDeclaration <em>BDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>BDeclaration</em>'.
   * @see geneauto.xtext.lustre.BDeclaration
   * @generated
   */
  EClass getBDeclaration();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.BDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.BDeclaration#getValue()
   * @see #getBDeclaration()
   * @generated
   */
  EAttribute getBDeclaration_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.ArrayDeclaration <em>Array Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Declaration</em>'.
   * @see geneauto.xtext.lustre.ArrayDeclaration
   * @generated
   */
  EClass getArrayDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.ArrayDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see geneauto.xtext.lustre.ArrayDeclaration#getValue()
   * @see #getArrayDeclaration()
   * @generated
   */
  EReference getArrayDeclaration_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.SpecificationLine <em>Specification Line</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Specification Line</em>'.
   * @see geneauto.xtext.lustre.SpecificationLine
   * @generated
   */
  EClass getSpecificationLine();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.SpecificationLine#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see geneauto.xtext.lustre.SpecificationLine#getType()
   * @see #getSpecificationLine()
   * @generated
   */
  EAttribute getSpecificationLine_Type();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.SpecificationLine#getSpecLine <em>Spec Line</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec Line</em>'.
   * @see geneauto.xtext.lustre.SpecificationLine#getSpecLine()
   * @see #getSpecificationLine()
   * @generated
   */
  EReference getSpecificationLine_SpecLine();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Inputs <em>Inputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Inputs</em>'.
   * @see geneauto.xtext.lustre.Inputs
   * @generated
   */
  EClass getInputs();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Inputs#getInputs <em>Inputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Inputs</em>'.
   * @see geneauto.xtext.lustre.Inputs#getInputs()
   * @see #getInputs()
   * @generated
   */
  EReference getInputs_Inputs();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Outputs <em>Outputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Outputs</em>'.
   * @see geneauto.xtext.lustre.Outputs
   * @generated
   */
  EClass getOutputs();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Outputs#getOutputs <em>Outputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Outputs</em>'.
   * @see geneauto.xtext.lustre.Outputs#getOutputs()
   * @see #getOutputs()
   * @generated
   */
  EReference getOutputs_Outputs();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Locals <em>Locals</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Locals</em>'.
   * @see geneauto.xtext.lustre.Locals
   * @generated
   */
  EClass getLocals();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Locals#getLocals <em>Locals</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Locals</em>'.
   * @see geneauto.xtext.lustre.Locals#getLocals()
   * @see #getLocals()
   * @generated
   */
  EReference getLocals_Locals();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Body <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Body</em>'.
   * @see geneauto.xtext.lustre.Body
   * @generated
   */
  EClass getBody();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Body#getEquations <em>Equations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Equations</em>'.
   * @see geneauto.xtext.lustre.Body#getEquations()
   * @see #getBody()
   * @generated
   */
  EReference getBody_Equations();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Body#getAsserts <em>Asserts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Asserts</em>'.
   * @see geneauto.xtext.lustre.Body#getAsserts()
   * @see #getBody()
   * @generated
   */
  EReference getBody_Asserts();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.Body#getAnnotations <em>Annotations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Annotations</em>'.
   * @see geneauto.xtext.lustre.Body#getAnnotations()
   * @see #getBody()
   * @generated
   */
  EReference getBody_Annotations();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Annotation <em>Annotation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Annotation</em>'.
   * @see geneauto.xtext.lustre.Annotation
   * @generated
   */
  EClass getAnnotation();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.Annotation#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see geneauto.xtext.lustre.Annotation#getIdentifier()
   * @see #getAnnotation()
   * @generated
   */
  EAttribute getAnnotation_Identifier();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Annotation#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see geneauto.xtext.lustre.Annotation#getExpression()
   * @see #getAnnotation()
   * @generated
   */
  EReference getAnnotation_Expression();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.PreTraceAnnotation <em>Pre Trace Annotation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pre Trace Annotation</em>'.
   * @see geneauto.xtext.lustre.PreTraceAnnotation
   * @generated
   */
  EClass getPreTraceAnnotation();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.PreTraceAnnotation#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see geneauto.xtext.lustre.PreTraceAnnotation#getType()
   * @see #getPreTraceAnnotation()
   * @generated
   */
  EAttribute getPreTraceAnnotation_Type();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.PreTraceAnnotation#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.PreTraceAnnotation#getValue()
   * @see #getPreTraceAnnotation()
   * @generated
   */
  EAttribute getPreTraceAnnotation_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Equation <em>Equation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Equation</em>'.
   * @see geneauto.xtext.lustre.Equation
   * @generated
   */
  EClass getEquation();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Equation#getPreTraceAnnotation <em>Pre Trace Annotation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pre Trace Annotation</em>'.
   * @see geneauto.xtext.lustre.Equation#getPreTraceAnnotation()
   * @see #getEquation()
   * @generated
   */
  EReference getEquation_PreTraceAnnotation();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Equation#getLeftPart <em>Left Part</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left Part</em>'.
   * @see geneauto.xtext.lustre.Equation#getLeftPart()
   * @see #getEquation()
   * @generated
   */
  EReference getEquation_LeftPart();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Equation#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see geneauto.xtext.lustre.Equation#getExpression()
   * @see #getEquation()
   * @generated
   */
  EReference getEquation_Expression();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Assert <em>Assert</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assert</em>'.
   * @see geneauto.xtext.lustre.Assert
   * @generated
   */
  EClass getAssert();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.Assert#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see geneauto.xtext.lustre.Assert#getExpression()
   * @see #getAssert()
   * @generated
   */
  EReference getAssert_Expression();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.LeftVariables <em>Left Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Left Variables</em>'.
   * @see geneauto.xtext.lustre.LeftVariables
   * @generated
   */
  EClass getLeftVariables();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.LeftVariables#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see geneauto.xtext.lustre.LeftVariables#getVariables()
   * @see #getLeftVariables()
   * @generated
   */
  EReference getLeftVariables_Variables();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.VariablesList <em>Variables List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variables List</em>'.
   * @see geneauto.xtext.lustre.VariablesList
   * @generated
   */
  EClass getVariablesList();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.VariablesList#getPreTraceAnnotation <em>Pre Trace Annotation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pre Trace Annotation</em>'.
   * @see geneauto.xtext.lustre.VariablesList#getPreTraceAnnotation()
   * @see #getVariablesList()
   * @generated
   */
  EReference getVariablesList_PreTraceAnnotation();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.VariablesList#isConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Const</em>'.
   * @see geneauto.xtext.lustre.VariablesList#isConst()
   * @see #getVariablesList()
   * @generated
   */
  EAttribute getVariablesList_Const();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.VariablesList#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see geneauto.xtext.lustre.VariablesList#getVariables()
   * @see #getVariablesList()
   * @generated
   */
  EReference getVariablesList_Variables();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.VariablesList#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see geneauto.xtext.lustre.VariablesList#getType()
   * @see #getVariablesList()
   * @generated
   */
  EReference getVariablesList_Type();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.VariableCall <em>Variable Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Call</em>'.
   * @see geneauto.xtext.lustre.VariableCall
   * @generated
   */
  EClass getVariableCall();

  /**
   * Returns the meta object for the reference '{@link geneauto.xtext.lustre.VariableCall#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Var</em>'.
   * @see geneauto.xtext.lustre.VariableCall#getVar()
   * @see #getVariableCall()
   * @generated
   */
  EReference getVariableCall_Var();

  /**
   * Returns the meta object for the attribute list '{@link geneauto.xtext.lustre.VariableCall#getDim <em>Dim</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Dim</em>'.
   * @see geneauto.xtext.lustre.VariableCall#getDim()
   * @see #getVariableCall()
   * @generated
   */
  EAttribute getVariableCall_Dim();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable</em>'.
   * @see geneauto.xtext.lustre.Variable
   * @generated
   */
  EClass getVariable();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.Variable#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see geneauto.xtext.lustre.Variable#getName()
   * @see #getVariable()
   * @generated
   */
  EAttribute getVariable_Name();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.DataType <em>Data Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Data Type</em>'.
   * @see geneauto.xtext.lustre.DataType
   * @generated
   */
  EClass getDataType();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.DataType#getBaseType <em>Base Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Base Type</em>'.
   * @see geneauto.xtext.lustre.DataType#getBaseType()
   * @see #getDataType()
   * @generated
   */
  EAttribute getDataType_BaseType();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.DataType#getDim <em>Dim</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Dim</em>'.
   * @see geneauto.xtext.lustre.DataType#getDim()
   * @see #getDataType()
   * @generated
   */
  EReference getDataType_Dim();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.DataTypeDimValue <em>Data Type Dim Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Data Type Dim Value</em>'.
   * @see geneauto.xtext.lustre.DataTypeDimValue
   * @generated
   */
  EClass getDataTypeDimValue();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.DataTypeDimValue#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.DataTypeDimValue#getValue()
   * @see #getDataTypeDimValue()
   * @generated
   */
  EAttribute getDataTypeDimValue_Value();

  /**
   * Returns the meta object for the reference '{@link geneauto.xtext.lustre.DataTypeDimValue#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref</em>'.
   * @see geneauto.xtext.lustre.DataTypeDimValue#getRef()
   * @see #getDataTypeDimValue()
   * @generated
   */
  EReference getDataTypeDimValue_Ref();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.AbstractExpression <em>Abstract Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Abstract Expression</em>'.
   * @see geneauto.xtext.lustre.AbstractExpression
   * @generated
   */
  EClass getAbstractExpression();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.ArrayDefinition <em>Array Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Definition</em>'.
   * @see geneauto.xtext.lustre.ArrayDefinition
   * @generated
   */
  EClass getArrayDefinition();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.ArrayDefinition#getDimVal <em>Dim Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Dim Val</em>'.
   * @see geneauto.xtext.lustre.ArrayDefinition#getDimVal()
   * @see #getArrayDefinition()
   * @generated
   */
  EReference getArrayDefinition_DimVal();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.MatrixExpression <em>Matrix Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Matrix Expression</em>'.
   * @see geneauto.xtext.lustre.MatrixExpression
   * @generated
   */
  EClass getMatrixExpression();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.ArrayExpression <em>Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Expression</em>'.
   * @see geneauto.xtext.lustre.ArrayExpression
   * @generated
   */
  EClass getArrayExpression();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.IteExpression <em>Ite Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ite Expression</em>'.
   * @see geneauto.xtext.lustre.IteExpression
   * @generated
   */
  EClass getIteExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.IteExpression#getGuard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guard</em>'.
   * @see geneauto.xtext.lustre.IteExpression#getGuard()
   * @see #getIteExpression()
   * @generated
   */
  EReference getIteExpression_Guard();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.IteExpression#getThen <em>Then</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Then</em>'.
   * @see geneauto.xtext.lustre.IteExpression#getThen()
   * @see #getIteExpression()
   * @generated
   */
  EReference getIteExpression_Then();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.IteExpression#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else</em>'.
   * @see geneauto.xtext.lustre.IteExpression#getElse()
   * @see #getIteExpression()
   * @generated
   */
  EReference getIteExpression_Else();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.FbyExpression <em>Fby Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fby Expression</em>'.
   * @see geneauto.xtext.lustre.FbyExpression
   * @generated
   */
  EClass getFbyExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.FbyExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.FbyExpression#getLeft()
   * @see #getFbyExpression()
   * @generated
   */
  EReference getFbyExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.FbyExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.FbyExpression#getOp()
   * @see #getFbyExpression()
   * @generated
   */
  EAttribute getFbyExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.FbyExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.FbyExpression#getRight()
   * @see #getFbyExpression()
   * @generated
   */
  EReference getFbyExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.OrExpression <em>Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Or Expression</em>'.
   * @see geneauto.xtext.lustre.OrExpression
   * @generated
   */
  EClass getOrExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.OrExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.OrExpression#getLeft()
   * @see #getOrExpression()
   * @generated
   */
  EReference getOrExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.OrExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.OrExpression#getOp()
   * @see #getOrExpression()
   * @generated
   */
  EAttribute getOrExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.OrExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.OrExpression#getRight()
   * @see #getOrExpression()
   * @generated
   */
  EReference getOrExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.AndExpression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And Expression</em>'.
   * @see geneauto.xtext.lustre.AndExpression
   * @generated
   */
  EClass getAndExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.AndExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.AndExpression#getLeft()
   * @see #getAndExpression()
   * @generated
   */
  EReference getAndExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.AndExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.AndExpression#getOp()
   * @see #getAndExpression()
   * @generated
   */
  EAttribute getAndExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.AndExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.AndExpression#getRight()
   * @see #getAndExpression()
   * @generated
   */
  EReference getAndExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.ImpliesExpression <em>Implies Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Implies Expression</em>'.
   * @see geneauto.xtext.lustre.ImpliesExpression
   * @generated
   */
  EClass getImpliesExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.ImpliesExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.ImpliesExpression#getLeft()
   * @see #getImpliesExpression()
   * @generated
   */
  EReference getImpliesExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.ImpliesExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.ImpliesExpression#getOp()
   * @see #getImpliesExpression()
   * @generated
   */
  EAttribute getImpliesExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.ImpliesExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.ImpliesExpression#getRight()
   * @see #getImpliesExpression()
   * @generated
   */
  EReference getImpliesExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.RelExpression <em>Rel Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Rel Expression</em>'.
   * @see geneauto.xtext.lustre.RelExpression
   * @generated
   */
  EClass getRelExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.RelExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.RelExpression#getLeft()
   * @see #getRelExpression()
   * @generated
   */
  EReference getRelExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.RelExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.RelExpression#getOp()
   * @see #getRelExpression()
   * @generated
   */
  EAttribute getRelExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.RelExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.RelExpression#getRight()
   * @see #getRelExpression()
   * @generated
   */
  EReference getRelExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.NotExpression <em>Not Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not Expression</em>'.
   * @see geneauto.xtext.lustre.NotExpression
   * @generated
   */
  EClass getNotExpression();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.NotExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.NotExpression#getOp()
   * @see #getNotExpression()
   * @generated
   */
  EAttribute getNotExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.NotExpression#getUexp <em>Uexp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Uexp</em>'.
   * @see geneauto.xtext.lustre.NotExpression#getUexp()
   * @see #getNotExpression()
   * @generated
   */
  EReference getNotExpression_Uexp();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.AddExpression <em>Add Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add Expression</em>'.
   * @see geneauto.xtext.lustre.AddExpression
   * @generated
   */
  EClass getAddExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.AddExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.AddExpression#getLeft()
   * @see #getAddExpression()
   * @generated
   */
  EReference getAddExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.AddExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.AddExpression#getOp()
   * @see #getAddExpression()
   * @generated
   */
  EAttribute getAddExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.AddExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.AddExpression#getRight()
   * @see #getAddExpression()
   * @generated
   */
  EReference getAddExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.MultExpression <em>Mult Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mult Expression</em>'.
   * @see geneauto.xtext.lustre.MultExpression
   * @generated
   */
  EClass getMultExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.MultExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.MultExpression#getLeft()
   * @see #getMultExpression()
   * @generated
   */
  EReference getMultExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.MultExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.MultExpression#getOp()
   * @see #getMultExpression()
   * @generated
   */
  EAttribute getMultExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.MultExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.MultExpression#getRight()
   * @see #getMultExpression()
   * @generated
   */
  EReference getMultExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.WhenExpression <em>When Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>When Expression</em>'.
   * @see geneauto.xtext.lustre.WhenExpression
   * @generated
   */
  EClass getWhenExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.WhenExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.WhenExpression#getLeft()
   * @see #getWhenExpression()
   * @generated
   */
  EReference getWhenExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.WhenExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.WhenExpression#getOp()
   * @see #getWhenExpression()
   * @generated
   */
  EAttribute getWhenExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.WhenExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.WhenExpression#getRight()
   * @see #getWhenExpression()
   * @generated
   */
  EReference getWhenExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.NorExpression <em>Nor Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nor Expression</em>'.
   * @see geneauto.xtext.lustre.NorExpression
   * @generated
   */
  EClass getNorExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.NorExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see geneauto.xtext.lustre.NorExpression#getLeft()
   * @see #getNorExpression()
   * @generated
   */
  EReference getNorExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.NorExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.NorExpression#getOp()
   * @see #getNorExpression()
   * @generated
   */
  EAttribute getNorExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.NorExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see geneauto.xtext.lustre.NorExpression#getRight()
   * @see #getNorExpression()
   * @generated
   */
  EReference getNorExpression_Right();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.UnExpression <em>Un Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Un Expression</em>'.
   * @see geneauto.xtext.lustre.UnExpression
   * @generated
   */
  EClass getUnExpression();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.UnExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see geneauto.xtext.lustre.UnExpression#getOp()
   * @see #getUnExpression()
   * @generated
   */
  EAttribute getUnExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.UnExpression#getUexp <em>Uexp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Uexp</em>'.
   * @see geneauto.xtext.lustre.UnExpression#getUexp()
   * @see #getUnExpression()
   * @generated
   */
  EReference getUnExpression_Uexp();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.VariableExpression <em>Variable Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Expression</em>'.
   * @see geneauto.xtext.lustre.VariableExpression
   * @generated
   */
  EClass getVariableExpression();

  /**
   * Returns the meta object for the containment reference '{@link geneauto.xtext.lustre.VariableExpression#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see geneauto.xtext.lustre.VariableExpression#getVariable()
   * @see #getVariableExpression()
   * @generated
   */
  EReference getVariableExpression_Variable();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.TupleExpression <em>Tuple Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Tuple Expression</em>'.
   * @see geneauto.xtext.lustre.TupleExpression
   * @generated
   */
  EClass getTupleExpression();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.TupleExpression#getExpressionList <em>Expression List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expression List</em>'.
   * @see geneauto.xtext.lustre.TupleExpression#getExpressionList()
   * @see #getTupleExpression()
   * @generated
   */
  EReference getTupleExpression_ExpressionList();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.IntegerLiteralExpression <em>Integer Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Literal Expression</em>'.
   * @see geneauto.xtext.lustre.IntegerLiteralExpression
   * @generated
   */
  EClass getIntegerLiteralExpression();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.IntegerLiteralExpression#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.IntegerLiteralExpression#getValue()
   * @see #getIntegerLiteralExpression()
   * @generated
   */
  EAttribute getIntegerLiteralExpression_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.RealLiteralExpression <em>Real Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Real Literal Expression</em>'.
   * @see geneauto.xtext.lustre.RealLiteralExpression
   * @generated
   */
  EClass getRealLiteralExpression();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.RealLiteralExpression#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.RealLiteralExpression#getValue()
   * @see #getRealLiteralExpression()
   * @generated
   */
  EAttribute getRealLiteralExpression_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.DoubleLiteralExpression <em>Double Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Double Literal Expression</em>'.
   * @see geneauto.xtext.lustre.DoubleLiteralExpression
   * @generated
   */
  EClass getDoubleLiteralExpression();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.DoubleLiteralExpression#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.DoubleLiteralExpression#getValue()
   * @see #getDoubleLiteralExpression()
   * @generated
   */
  EAttribute getDoubleLiteralExpression_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.BooleanLiteralExpression <em>Boolean Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Literal Expression</em>'.
   * @see geneauto.xtext.lustre.BooleanLiteralExpression
   * @generated
   */
  EClass getBooleanLiteralExpression();

  /**
   * Returns the meta object for the attribute '{@link geneauto.xtext.lustre.BooleanLiteralExpression#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see geneauto.xtext.lustre.BooleanLiteralExpression#getValue()
   * @see #getBooleanLiteralExpression()
   * @generated
   */
  EAttribute getBooleanLiteralExpression_Value();

  /**
   * Returns the meta object for class '{@link geneauto.xtext.lustre.CallExpression <em>Call Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Expression</em>'.
   * @see geneauto.xtext.lustre.CallExpression
   * @generated
   */
  EClass getCallExpression();

  /**
   * Returns the meta object for the reference '{@link geneauto.xtext.lustre.CallExpression#getElem <em>Elem</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Elem</em>'.
   * @see geneauto.xtext.lustre.CallExpression#getElem()
   * @see #getCallExpression()
   * @generated
   */
  EReference getCallExpression_Elem();

  /**
   * Returns the meta object for the containment reference list '{@link geneauto.xtext.lustre.CallExpression#getArguments <em>Arguments</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Arguments</em>'.
   * @see geneauto.xtext.lustre.CallExpression#getArguments()
   * @see #getCallExpression()
   * @generated
   */
  EReference getCallExpression_Arguments();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.SpecificationType <em>Specification Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Specification Type</em>'.
   * @see geneauto.xtext.lustre.SpecificationType
   * @generated
   */
  EEnum getSpecificationType();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.BasicType <em>Basic Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Basic Type</em>'.
   * @see geneauto.xtext.lustre.BasicType
   * @generated
   */
  EEnum getBasicType();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.FbyOp <em>Fby Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Fby Op</em>'.
   * @see geneauto.xtext.lustre.FbyOp
   * @generated
   */
  EEnum getFbyOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.OrOp <em>Or Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Or Op</em>'.
   * @see geneauto.xtext.lustre.OrOp
   * @generated
   */
  EEnum getOrOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.AndOp <em>And Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>And Op</em>'.
   * @see geneauto.xtext.lustre.AndOp
   * @generated
   */
  EEnum getAndOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.ImpliesOp <em>Implies Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Implies Op</em>'.
   * @see geneauto.xtext.lustre.ImpliesOp
   * @generated
   */
  EEnum getImpliesOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.RelOp <em>Rel Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Rel Op</em>'.
   * @see geneauto.xtext.lustre.RelOp
   * @generated
   */
  EEnum getRelOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.NotOp <em>Not Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Not Op</em>'.
   * @see geneauto.xtext.lustre.NotOp
   * @generated
   */
  EEnum getNotOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.AddOp <em>Add Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Add Op</em>'.
   * @see geneauto.xtext.lustre.AddOp
   * @generated
   */
  EEnum getAddOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.MultOp <em>Mult Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Mult Op</em>'.
   * @see geneauto.xtext.lustre.MultOp
   * @generated
   */
  EEnum getMultOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.WhenOp <em>When Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>When Op</em>'.
   * @see geneauto.xtext.lustre.WhenOp
   * @generated
   */
  EEnum getWhenOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.NorOp <em>Nor Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Nor Op</em>'.
   * @see geneauto.xtext.lustre.NorOp
   * @generated
   */
  EEnum getNorOp();

  /**
   * Returns the meta object for enum '{@link geneauto.xtext.lustre.UnOp <em>Un Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Un Op</em>'.
   * @see geneauto.xtext.lustre.UnOp
   * @generated
   */
  EEnum getUnOp();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  LustreFactory getLustreFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.ProgramImpl <em>Program</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.ProgramImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getProgram()
     * @generated
     */
    EClass PROGRAM = eINSTANCE.getProgram();

    /**
     * The meta object literal for the '<em><b>Libraries</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__LIBRARIES = eINSTANCE.getProgram_Libraries();

    /**
     * The meta object literal for the '<em><b>Declarations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__DECLARATIONS = eINSTANCE.getProgram_Declarations();

    /**
     * The meta object literal for the '<em><b>Functions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__FUNCTIONS = eINSTANCE.getProgram_Functions();

    /**
     * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__NODES = eINSTANCE.getProgram_Nodes();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.OpenImpl <em>Open</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.OpenImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOpen()
     * @generated
     */
    EClass OPEN = eINSTANCE.getOpen();

    /**
     * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPEN__IMPORT_URI = eINSTANCE.getOpen_ImportURI();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.CallableElementImpl <em>Callable Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.CallableElementImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getCallableElement()
     * @generated
     */
    EClass CALLABLE_ELEMENT = eINSTANCE.getCallableElement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CALLABLE_ELEMENT__NAME = eINSTANCE.getCallableElement_Name();

    /**
     * The meta object literal for the '<em><b>Inputs</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALLABLE_ELEMENT__INPUTS = eINSTANCE.getCallableElement_Inputs();

    /**
     * The meta object literal for the '<em><b>Outputs</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALLABLE_ELEMENT__OUTPUTS = eINSTANCE.getCallableElement_Outputs();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.NodeImpl <em>Node</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.NodeImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNode()
     * @generated
     */
    EClass NODE = eINSTANCE.getNode();

    /**
     * The meta object literal for the '<em><b>Pre Trace Annotation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NODE__PRE_TRACE_ANNOTATION = eINSTANCE.getNode_PreTraceAnnotation();

    /**
     * The meta object literal for the '<em><b>Specifications</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NODE__SPECIFICATIONS = eINSTANCE.getNode_Specifications();

    /**
     * The meta object literal for the '<em><b>Locals</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NODE__LOCALS = eINSTANCE.getNode_Locals();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NODE__BODY = eINSTANCE.getNode_Body();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.FunctionImpl <em>Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.FunctionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getFunction()
     * @generated
     */
    EClass FUNCTION = eINSTANCE.getFunction();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.AbstractDeclarationImpl <em>Abstract Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.AbstractDeclarationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAbstractDeclaration()
     * @generated
     */
    EClass ABSTRACT_DECLARATION = eINSTANCE.getAbstractDeclaration();

    /**
     * The meta object literal for the '<em><b>Pre Trace Annotation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ABSTRACT_DECLARATION__PRE_TRACE_ANNOTATION = eINSTANCE.getAbstractDeclaration_PreTraceAnnotation();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ABSTRACT_DECLARATION__VAR = eINSTANCE.getAbstractDeclaration_Var();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.IDeclarationImpl <em>IDeclaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.IDeclarationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getIDeclaration()
     * @generated
     */
    EClass IDECLARATION = eINSTANCE.getIDeclaration();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IDECLARATION__VALUE = eINSTANCE.getIDeclaration_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.DDeclarationImpl <em>DDeclaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.DDeclarationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDDeclaration()
     * @generated
     */
    EClass DDECLARATION = eINSTANCE.getDDeclaration();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DDECLARATION__VALUE = eINSTANCE.getDDeclaration_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.RDeclarationImpl <em>RDeclaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.RDeclarationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRDeclaration()
     * @generated
     */
    EClass RDECLARATION = eINSTANCE.getRDeclaration();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RDECLARATION__VALUE = eINSTANCE.getRDeclaration_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.BDeclarationImpl <em>BDeclaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.BDeclarationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBDeclaration()
     * @generated
     */
    EClass BDECLARATION = eINSTANCE.getBDeclaration();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BDECLARATION__VALUE = eINSTANCE.getBDeclaration_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.ArrayDeclarationImpl <em>Array Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.ArrayDeclarationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getArrayDeclaration()
     * @generated
     */
    EClass ARRAY_DECLARATION = eINSTANCE.getArrayDeclaration();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_DECLARATION__VALUE = eINSTANCE.getArrayDeclaration_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.SpecificationLineImpl <em>Specification Line</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.SpecificationLineImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getSpecificationLine()
     * @generated
     */
    EClass SPECIFICATION_LINE = eINSTANCE.getSpecificationLine();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SPECIFICATION_LINE__TYPE = eINSTANCE.getSpecificationLine_Type();

    /**
     * The meta object literal for the '<em><b>Spec Line</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SPECIFICATION_LINE__SPEC_LINE = eINSTANCE.getSpecificationLine_SpecLine();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.InputsImpl <em>Inputs</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.InputsImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getInputs()
     * @generated
     */
    EClass INPUTS = eINSTANCE.getInputs();

    /**
     * The meta object literal for the '<em><b>Inputs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INPUTS__INPUTS = eINSTANCE.getInputs_Inputs();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.OutputsImpl <em>Outputs</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.OutputsImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOutputs()
     * @generated
     */
    EClass OUTPUTS = eINSTANCE.getOutputs();

    /**
     * The meta object literal for the '<em><b>Outputs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OUTPUTS__OUTPUTS = eINSTANCE.getOutputs_Outputs();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.LocalsImpl <em>Locals</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.LocalsImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getLocals()
     * @generated
     */
    EClass LOCALS = eINSTANCE.getLocals();

    /**
     * The meta object literal for the '<em><b>Locals</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LOCALS__LOCALS = eINSTANCE.getLocals_Locals();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.BodyImpl <em>Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.BodyImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBody()
     * @generated
     */
    EClass BODY = eINSTANCE.getBody();

    /**
     * The meta object literal for the '<em><b>Equations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BODY__EQUATIONS = eINSTANCE.getBody_Equations();

    /**
     * The meta object literal for the '<em><b>Asserts</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BODY__ASSERTS = eINSTANCE.getBody_Asserts();

    /**
     * The meta object literal for the '<em><b>Annotations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BODY__ANNOTATIONS = eINSTANCE.getBody_Annotations();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.AnnotationImpl <em>Annotation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.AnnotationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAnnotation()
     * @generated
     */
    EClass ANNOTATION = eINSTANCE.getAnnotation();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ANNOTATION__IDENTIFIER = eINSTANCE.getAnnotation_Identifier();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ANNOTATION__EXPRESSION = eINSTANCE.getAnnotation_Expression();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.PreTraceAnnotationImpl <em>Pre Trace Annotation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.PreTraceAnnotationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getPreTraceAnnotation()
     * @generated
     */
    EClass PRE_TRACE_ANNOTATION = eINSTANCE.getPreTraceAnnotation();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRE_TRACE_ANNOTATION__TYPE = eINSTANCE.getPreTraceAnnotation_Type();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRE_TRACE_ANNOTATION__VALUE = eINSTANCE.getPreTraceAnnotation_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.EquationImpl <em>Equation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.EquationImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getEquation()
     * @generated
     */
    EClass EQUATION = eINSTANCE.getEquation();

    /**
     * The meta object literal for the '<em><b>Pre Trace Annotation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUATION__PRE_TRACE_ANNOTATION = eINSTANCE.getEquation_PreTraceAnnotation();

    /**
     * The meta object literal for the '<em><b>Left Part</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUATION__LEFT_PART = eINSTANCE.getEquation_LeftPart();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUATION__EXPRESSION = eINSTANCE.getEquation_Expression();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.AssertImpl <em>Assert</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.AssertImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAssert()
     * @generated
     */
    EClass ASSERT = eINSTANCE.getAssert();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSERT__EXPRESSION = eINSTANCE.getAssert_Expression();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.LeftVariablesImpl <em>Left Variables</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.LeftVariablesImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getLeftVariables()
     * @generated
     */
    EClass LEFT_VARIABLES = eINSTANCE.getLeftVariables();

    /**
     * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LEFT_VARIABLES__VARIABLES = eINSTANCE.getLeftVariables_Variables();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.VariablesListImpl <em>Variables List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.VariablesListImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariablesList()
     * @generated
     */
    EClass VARIABLES_LIST = eINSTANCE.getVariablesList();

    /**
     * The meta object literal for the '<em><b>Pre Trace Annotation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLES_LIST__PRE_TRACE_ANNOTATION = eINSTANCE.getVariablesList_PreTraceAnnotation();

    /**
     * The meta object literal for the '<em><b>Const</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIABLES_LIST__CONST = eINSTANCE.getVariablesList_Const();

    /**
     * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLES_LIST__VARIABLES = eINSTANCE.getVariablesList_Variables();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLES_LIST__TYPE = eINSTANCE.getVariablesList_Type();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.VariableCallImpl <em>Variable Call</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.VariableCallImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariableCall()
     * @generated
     */
    EClass VARIABLE_CALL = eINSTANCE.getVariableCall();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLE_CALL__VAR = eINSTANCE.getVariableCall_Var();

    /**
     * The meta object literal for the '<em><b>Dim</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIABLE_CALL__DIM = eINSTANCE.getVariableCall_Dim();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.VariableImpl <em>Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.VariableImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariable()
     * @generated
     */
    EClass VARIABLE = eINSTANCE.getVariable();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIABLE__NAME = eINSTANCE.getVariable_Name();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.DataTypeImpl <em>Data Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.DataTypeImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDataType()
     * @generated
     */
    EClass DATA_TYPE = eINSTANCE.getDataType();

    /**
     * The meta object literal for the '<em><b>Base Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_TYPE__BASE_TYPE = eINSTANCE.getDataType_BaseType();

    /**
     * The meta object literal for the '<em><b>Dim</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DATA_TYPE__DIM = eINSTANCE.getDataType_Dim();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.DataTypeDimValueImpl <em>Data Type Dim Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.DataTypeDimValueImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDataTypeDimValue()
     * @generated
     */
    EClass DATA_TYPE_DIM_VALUE = eINSTANCE.getDataTypeDimValue();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_TYPE_DIM_VALUE__VALUE = eINSTANCE.getDataTypeDimValue_Value();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DATA_TYPE_DIM_VALUE__REF = eINSTANCE.getDataTypeDimValue_Ref();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.AbstractExpressionImpl <em>Abstract Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.AbstractExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAbstractExpression()
     * @generated
     */
    EClass ABSTRACT_EXPRESSION = eINSTANCE.getAbstractExpression();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.ArrayDefinitionImpl <em>Array Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.ArrayDefinitionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getArrayDefinition()
     * @generated
     */
    EClass ARRAY_DEFINITION = eINSTANCE.getArrayDefinition();

    /**
     * The meta object literal for the '<em><b>Dim Val</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_DEFINITION__DIM_VAL = eINSTANCE.getArrayDefinition_DimVal();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.MatrixExpressionImpl <em>Matrix Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.MatrixExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getMatrixExpression()
     * @generated
     */
    EClass MATRIX_EXPRESSION = eINSTANCE.getMatrixExpression();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.ArrayExpressionImpl <em>Array Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.ArrayExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getArrayExpression()
     * @generated
     */
    EClass ARRAY_EXPRESSION = eINSTANCE.getArrayExpression();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.IteExpressionImpl <em>Ite Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.IteExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getIteExpression()
     * @generated
     */
    EClass ITE_EXPRESSION = eINSTANCE.getIteExpression();

    /**
     * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITE_EXPRESSION__GUARD = eINSTANCE.getIteExpression_Guard();

    /**
     * The meta object literal for the '<em><b>Then</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITE_EXPRESSION__THEN = eINSTANCE.getIteExpression_Then();

    /**
     * The meta object literal for the '<em><b>Else</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ITE_EXPRESSION__ELSE = eINSTANCE.getIteExpression_Else();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.FbyExpressionImpl <em>Fby Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.FbyExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getFbyExpression()
     * @generated
     */
    EClass FBY_EXPRESSION = eINSTANCE.getFbyExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FBY_EXPRESSION__LEFT = eINSTANCE.getFbyExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FBY_EXPRESSION__OP = eINSTANCE.getFbyExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FBY_EXPRESSION__RIGHT = eINSTANCE.getFbyExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.OrExpressionImpl <em>Or Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.OrExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOrExpression()
     * @generated
     */
    EClass OR_EXPRESSION = eINSTANCE.getOrExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_EXPRESSION__LEFT = eINSTANCE.getOrExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OR_EXPRESSION__OP = eINSTANCE.getOrExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_EXPRESSION__RIGHT = eINSTANCE.getOrExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.AndExpressionImpl <em>And Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.AndExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAndExpression()
     * @generated
     */
    EClass AND_EXPRESSION = eINSTANCE.getAndExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_EXPRESSION__LEFT = eINSTANCE.getAndExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute AND_EXPRESSION__OP = eINSTANCE.getAndExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_EXPRESSION__RIGHT = eINSTANCE.getAndExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.ImpliesExpressionImpl <em>Implies Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.ImpliesExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getImpliesExpression()
     * @generated
     */
    EClass IMPLIES_EXPRESSION = eINSTANCE.getImpliesExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IMPLIES_EXPRESSION__LEFT = eINSTANCE.getImpliesExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMPLIES_EXPRESSION__OP = eINSTANCE.getImpliesExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IMPLIES_EXPRESSION__RIGHT = eINSTANCE.getImpliesExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.RelExpressionImpl <em>Rel Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.RelExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRelExpression()
     * @generated
     */
    EClass REL_EXPRESSION = eINSTANCE.getRelExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REL_EXPRESSION__LEFT = eINSTANCE.getRelExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REL_EXPRESSION__OP = eINSTANCE.getRelExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REL_EXPRESSION__RIGHT = eINSTANCE.getRelExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.NotExpressionImpl <em>Not Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.NotExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNotExpression()
     * @generated
     */
    EClass NOT_EXPRESSION = eINSTANCE.getNotExpression();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NOT_EXPRESSION__OP = eINSTANCE.getNotExpression_Op();

    /**
     * The meta object literal for the '<em><b>Uexp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOT_EXPRESSION__UEXP = eINSTANCE.getNotExpression_Uexp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.AddExpressionImpl <em>Add Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.AddExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAddExpression()
     * @generated
     */
    EClass ADD_EXPRESSION = eINSTANCE.getAddExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD_EXPRESSION__LEFT = eINSTANCE.getAddExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ADD_EXPRESSION__OP = eINSTANCE.getAddExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD_EXPRESSION__RIGHT = eINSTANCE.getAddExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.MultExpressionImpl <em>Mult Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.MultExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getMultExpression()
     * @generated
     */
    EClass MULT_EXPRESSION = eINSTANCE.getMultExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULT_EXPRESSION__LEFT = eINSTANCE.getMultExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULT_EXPRESSION__OP = eINSTANCE.getMultExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULT_EXPRESSION__RIGHT = eINSTANCE.getMultExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.WhenExpressionImpl <em>When Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.WhenExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getWhenExpression()
     * @generated
     */
    EClass WHEN_EXPRESSION = eINSTANCE.getWhenExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHEN_EXPRESSION__LEFT = eINSTANCE.getWhenExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WHEN_EXPRESSION__OP = eINSTANCE.getWhenExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHEN_EXPRESSION__RIGHT = eINSTANCE.getWhenExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.NorExpressionImpl <em>Nor Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.NorExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNorExpression()
     * @generated
     */
    EClass NOR_EXPRESSION = eINSTANCE.getNorExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOR_EXPRESSION__LEFT = eINSTANCE.getNorExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NOR_EXPRESSION__OP = eINSTANCE.getNorExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOR_EXPRESSION__RIGHT = eINSTANCE.getNorExpression_Right();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.UnExpressionImpl <em>Un Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.UnExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getUnExpression()
     * @generated
     */
    EClass UN_EXPRESSION = eINSTANCE.getUnExpression();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UN_EXPRESSION__OP = eINSTANCE.getUnExpression_Op();

    /**
     * The meta object literal for the '<em><b>Uexp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UN_EXPRESSION__UEXP = eINSTANCE.getUnExpression_Uexp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.VariableExpressionImpl <em>Variable Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.VariableExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getVariableExpression()
     * @generated
     */
    EClass VARIABLE_EXPRESSION = eINSTANCE.getVariableExpression();

    /**
     * The meta object literal for the '<em><b>Variable</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLE_EXPRESSION__VARIABLE = eINSTANCE.getVariableExpression_Variable();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.TupleExpressionImpl <em>Tuple Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.TupleExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getTupleExpression()
     * @generated
     */
    EClass TUPLE_EXPRESSION = eINSTANCE.getTupleExpression();

    /**
     * The meta object literal for the '<em><b>Expression List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TUPLE_EXPRESSION__EXPRESSION_LIST = eINSTANCE.getTupleExpression_ExpressionList();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.IntegerLiteralExpressionImpl <em>Integer Literal Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.IntegerLiteralExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getIntegerLiteralExpression()
     * @generated
     */
    EClass INTEGER_LITERAL_EXPRESSION = eINSTANCE.getIntegerLiteralExpression();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_LITERAL_EXPRESSION__VALUE = eINSTANCE.getIntegerLiteralExpression_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.RealLiteralExpressionImpl <em>Real Literal Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.RealLiteralExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRealLiteralExpression()
     * @generated
     */
    EClass REAL_LITERAL_EXPRESSION = eINSTANCE.getRealLiteralExpression();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REAL_LITERAL_EXPRESSION__VALUE = eINSTANCE.getRealLiteralExpression_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.DoubleLiteralExpressionImpl <em>Double Literal Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.DoubleLiteralExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getDoubleLiteralExpression()
     * @generated
     */
    EClass DOUBLE_LITERAL_EXPRESSION = eINSTANCE.getDoubleLiteralExpression();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOUBLE_LITERAL_EXPRESSION__VALUE = eINSTANCE.getDoubleLiteralExpression_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.BooleanLiteralExpressionImpl <em>Boolean Literal Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.BooleanLiteralExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBooleanLiteralExpression()
     * @generated
     */
    EClass BOOLEAN_LITERAL_EXPRESSION = eINSTANCE.getBooleanLiteralExpression();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_LITERAL_EXPRESSION__VALUE = eINSTANCE.getBooleanLiteralExpression_Value();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.impl.CallExpressionImpl <em>Call Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.impl.CallExpressionImpl
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getCallExpression()
     * @generated
     */
    EClass CALL_EXPRESSION = eINSTANCE.getCallExpression();

    /**
     * The meta object literal for the '<em><b>Elem</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALL_EXPRESSION__ELEM = eINSTANCE.getCallExpression_Elem();

    /**
     * The meta object literal for the '<em><b>Arguments</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALL_EXPRESSION__ARGUMENTS = eINSTANCE.getCallExpression_Arguments();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.SpecificationType <em>Specification Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.SpecificationType
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getSpecificationType()
     * @generated
     */
    EEnum SPECIFICATION_TYPE = eINSTANCE.getSpecificationType();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.BasicType <em>Basic Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.BasicType
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getBasicType()
     * @generated
     */
    EEnum BASIC_TYPE = eINSTANCE.getBasicType();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.FbyOp <em>Fby Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.FbyOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getFbyOp()
     * @generated
     */
    EEnum FBY_OP = eINSTANCE.getFbyOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.OrOp <em>Or Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.OrOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getOrOp()
     * @generated
     */
    EEnum OR_OP = eINSTANCE.getOrOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.AndOp <em>And Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.AndOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAndOp()
     * @generated
     */
    EEnum AND_OP = eINSTANCE.getAndOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.ImpliesOp <em>Implies Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.ImpliesOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getImpliesOp()
     * @generated
     */
    EEnum IMPLIES_OP = eINSTANCE.getImpliesOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.RelOp <em>Rel Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.RelOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getRelOp()
     * @generated
     */
    EEnum REL_OP = eINSTANCE.getRelOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.NotOp <em>Not Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.NotOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNotOp()
     * @generated
     */
    EEnum NOT_OP = eINSTANCE.getNotOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.AddOp <em>Add Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.AddOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getAddOp()
     * @generated
     */
    EEnum ADD_OP = eINSTANCE.getAddOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.MultOp <em>Mult Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.MultOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getMultOp()
     * @generated
     */
    EEnum MULT_OP = eINSTANCE.getMultOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.WhenOp <em>When Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.WhenOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getWhenOp()
     * @generated
     */
    EEnum WHEN_OP = eINSTANCE.getWhenOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.NorOp <em>Nor Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.NorOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getNorOp()
     * @generated
     */
    EEnum NOR_OP = eINSTANCE.getNorOp();

    /**
     * The meta object literal for the '{@link geneauto.xtext.lustre.UnOp <em>Un Op</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see geneauto.xtext.lustre.UnOp
     * @see geneauto.xtext.lustre.impl.LustrePackageImpl#getUnOp()
     * @generated
     */
    EEnum UN_OP = eINSTANCE.getUnOp();

  }

} //LustrePackage
