/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.AndExpression#getLeft <em>Left</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.AndExpression#getOp <em>Op</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.AndExpression#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getAndExpression()
 * @model
 * @generated
 */
public interface AndExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getAndExpression_Left()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getLeft();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.AndExpression#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(AbstractExpression value);

  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute.
   * The literals are from the enumeration {@link geneauto.xtext.lustre.AndOp}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.AndOp
   * @see #setOp(AndOp)
   * @see geneauto.xtext.lustre.LustrePackage#getAndExpression_Op()
   * @model
   * @generated
   */
  AndOp getOp();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.AndExpression#getOp <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.AndOp
   * @see #getOp()
   * @generated
   */
  void setOp(AndOp value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getAndExpression_Right()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getRight();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.AndExpression#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(AbstractExpression value);

} // AndExpression
