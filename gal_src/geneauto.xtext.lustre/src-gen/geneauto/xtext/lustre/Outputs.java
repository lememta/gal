/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Outputs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.Outputs#getOutputs <em>Outputs</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getOutputs()
 * @model
 * @generated
 */
public interface Outputs extends EObject
{
  /**
   * Returns the value of the '<em><b>Outputs</b></em>' containment reference list.
   * The list contents are of type {@link geneauto.xtext.lustre.VariablesList}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Outputs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Outputs</em>' containment reference list.
   * @see geneauto.xtext.lustre.LustrePackage#getOutputs_Outputs()
   * @model containment="true"
   * @generated
   */
  EList<VariablesList> getOutputs();

} // Outputs
