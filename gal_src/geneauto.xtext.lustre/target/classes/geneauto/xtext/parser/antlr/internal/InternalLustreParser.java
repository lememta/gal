package geneauto.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import geneauto.xtext.services.LustreGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalLustreParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_LUS_DOUBLE", "RULE_LUS_REAL", "RULE_LUS_BOOLEAN", "RULE_INT", "RULE_SL_COMMENT", "RULE_ML_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "';'", "'#open'", "'node'", "'('", "')'", "'returns'", "'var'", "'let'", "'tel'", "'.'", "'function'", "'const'", "'='", "'--@'", "'--!'", "':'", "'--Trace'", "'BLOCK'", "'SIGNAL'", "'PARAMETER'", "'PORT'", "'MAIN'", "'/'", "'assert'", "','", "'['", "']'", "'^'", "'if'", "'then'", "'else'", "'requires'", "'ensures'", "'observer'", "'int'", "'real'", "'bool'", "'double'", "'->'", "'or'", "'xor'", "'and'", "'=>'", "'<>'", "'<='", "'<'", "'>='", "'>'", "'not'", "'+'", "'-'", "'*'", "'div'", "'mod'", "'when'", "'nor'", "'pre'", "'current'"
    };
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int RULE_ID=5;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__64=64;
    public static final int T__29=29;
    public static final int T__65=65;
    public static final int T__28=28;
    public static final int T__62=62;
    public static final int T__27=27;
    public static final int T__63=63;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int T__61=61;
    public static final int T__60=60;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__19=19;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__16=16;
    public static final int T__51=51;
    public static final int T__15=15;
    public static final int T__52=52;
    public static final int T__18=18;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int T__59=59;
    public static final int RULE_INT=9;
    public static final int T__50=50;
    public static final int RULE_LUS_REAL=7;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=10;
    public static final int RULE_ML_COMMENT=11;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=4;
    public static final int T__32=32;
    public static final int T__71=71;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__70=70;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_LUS_BOOLEAN=8;
    public static final int RULE_WS=12;
    public static final int RULE_LUS_DOUBLE=6;

    // delegates
    // delegators


        public InternalLustreParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalLustreParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalLustreParser.tokenNames; }
    public String getGrammarFileName() { return "../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g"; }



     	private LustreGrammarAccess grammarAccess;
     	
        public InternalLustreParser(TokenStream input, LustreGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Program";	
       	}
       	
       	@Override
       	protected LustreGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleProgram"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:68:1: entryRuleProgram returns [EObject current=null] : iv_ruleProgram= ruleProgram EOF ;
    public final EObject entryRuleProgram() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProgram = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:69:2: (iv_ruleProgram= ruleProgram EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:70:2: iv_ruleProgram= ruleProgram EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProgramRule()); 
            }
            pushFollow(FOLLOW_ruleProgram_in_entryRuleProgram75);
            iv_ruleProgram=ruleProgram();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProgram; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProgram85); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProgram"


    // $ANTLR start "ruleProgram"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:77:1: ruleProgram returns [EObject current=null] : ( ( (lv_libraries_0_0= ruleOpen ) )* ( (lv_declarations_1_0= ruleAbstractDeclaration ) )* ( ( (lv_functions_2_0= ruleFunction ) ) otherlv_3= ';' )* ( (lv_nodes_4_0= ruleNode ) )* ) ;
    public final EObject ruleProgram() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        EObject lv_libraries_0_0 = null;

        EObject lv_declarations_1_0 = null;

        EObject lv_functions_2_0 = null;

        EObject lv_nodes_4_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:80:28: ( ( ( (lv_libraries_0_0= ruleOpen ) )* ( (lv_declarations_1_0= ruleAbstractDeclaration ) )* ( ( (lv_functions_2_0= ruleFunction ) ) otherlv_3= ';' )* ( (lv_nodes_4_0= ruleNode ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:81:1: ( ( (lv_libraries_0_0= ruleOpen ) )* ( (lv_declarations_1_0= ruleAbstractDeclaration ) )* ( ( (lv_functions_2_0= ruleFunction ) ) otherlv_3= ';' )* ( (lv_nodes_4_0= ruleNode ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:81:1: ( ( (lv_libraries_0_0= ruleOpen ) )* ( (lv_declarations_1_0= ruleAbstractDeclaration ) )* ( ( (lv_functions_2_0= ruleFunction ) ) otherlv_3= ';' )* ( (lv_nodes_4_0= ruleNode ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:81:2: ( (lv_libraries_0_0= ruleOpen ) )* ( (lv_declarations_1_0= ruleAbstractDeclaration ) )* ( ( (lv_functions_2_0= ruleFunction ) ) otherlv_3= ';' )* ( (lv_nodes_4_0= ruleNode ) )*
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:81:2: ( (lv_libraries_0_0= ruleOpen ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==15) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:82:1: (lv_libraries_0_0= ruleOpen )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:82:1: (lv_libraries_0_0= ruleOpen )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:83:3: lv_libraries_0_0= ruleOpen
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getLibrariesOpenParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpen_in_ruleProgram131);
            	    lv_libraries_0_0=ruleOpen();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"libraries",
            	              		lv_libraries_0_0, 
            	              		"Open");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:99:3: ( (lv_declarations_1_0= ruleAbstractDeclaration ) )*
            loop2:
            do {
                int alt2=2;
                alt2 = dfa2.predict(input);
                switch (alt2) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:100:1: (lv_declarations_1_0= ruleAbstractDeclaration )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:100:1: (lv_declarations_1_0= ruleAbstractDeclaration )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:101:3: lv_declarations_1_0= ruleAbstractDeclaration
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getDeclarationsAbstractDeclarationParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAbstractDeclaration_in_ruleProgram153);
            	    lv_declarations_1_0=ruleAbstractDeclaration();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"declarations",
            	              		lv_declarations_1_0, 
            	              		"AbstractDeclaration");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:117:3: ( ( (lv_functions_2_0= ruleFunction ) ) otherlv_3= ';' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==24) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:117:4: ( (lv_functions_2_0= ruleFunction ) ) otherlv_3= ';'
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:117:4: ( (lv_functions_2_0= ruleFunction ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:118:1: (lv_functions_2_0= ruleFunction )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:118:1: (lv_functions_2_0= ruleFunction )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:119:3: lv_functions_2_0= ruleFunction
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getFunctionsFunctionParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleFunction_in_ruleProgram176);
            	    lv_functions_2_0=ruleFunction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"functions",
            	              		lv_functions_2_0, 
            	              		"Function");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleProgram188); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getProgramAccess().getSemicolonKeyword_2_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:139:3: ( (lv_nodes_4_0= ruleNode ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==16||LA4_0==27||LA4_0==30) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:140:1: (lv_nodes_4_0= ruleNode )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:140:1: (lv_nodes_4_0= ruleNode )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:141:3: lv_nodes_4_0= ruleNode
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getNodesNodeParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleNode_in_ruleProgram211);
            	    lv_nodes_4_0=ruleNode();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"nodes",
            	              		lv_nodes_4_0, 
            	              		"Node");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProgram"


    // $ANTLR start "entryRuleOpen"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:165:1: entryRuleOpen returns [EObject current=null] : iv_ruleOpen= ruleOpen EOF ;
    public final EObject entryRuleOpen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOpen = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:166:2: (iv_ruleOpen= ruleOpen EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:167:2: iv_ruleOpen= ruleOpen EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpenRule()); 
            }
            pushFollow(FOLLOW_ruleOpen_in_entryRuleOpen248);
            iv_ruleOpen=ruleOpen();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpen; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpen258); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpen"


    // $ANTLR start "ruleOpen"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:174:1: ruleOpen returns [EObject current=null] : (otherlv_0= '#open' ( (lv_importURI_1_0= RULE_STRING ) ) (otherlv_2= ';' )? ) ;
    public final EObject ruleOpen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:177:28: ( (otherlv_0= '#open' ( (lv_importURI_1_0= RULE_STRING ) ) (otherlv_2= ';' )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:178:1: (otherlv_0= '#open' ( (lv_importURI_1_0= RULE_STRING ) ) (otherlv_2= ';' )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:178:1: (otherlv_0= '#open' ( (lv_importURI_1_0= RULE_STRING ) ) (otherlv_2= ';' )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:178:3: otherlv_0= '#open' ( (lv_importURI_1_0= RULE_STRING ) ) (otherlv_2= ';' )?
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleOpen295); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getOpenAccess().getOpenKeyword_0());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:182:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:183:1: (lv_importURI_1_0= RULE_STRING )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:183:1: (lv_importURI_1_0= RULE_STRING )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:184:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleOpen312); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getOpenAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getOpenRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"STRING");
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:200:2: (otherlv_2= ';' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==14) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:200:4: otherlv_2= ';'
                    {
                    otherlv_2=(Token)match(input,14,FOLLOW_14_in_ruleOpen330); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getOpenAccess().getSemicolonKeyword_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpen"


    // $ANTLR start "entryRuleNode"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:214:1: entryRuleNode returns [EObject current=null] : iv_ruleNode= ruleNode EOF ;
    public final EObject entryRuleNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNode = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:215:2: (iv_ruleNode= ruleNode EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:216:2: iv_ruleNode= ruleNode EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNodeRule()); 
            }
            pushFollow(FOLLOW_ruleNode_in_entryRuleNode370);
            iv_ruleNode=ruleNode();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNode; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNode380); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNode"


    // $ANTLR start "ruleNode"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:223:1: ruleNode returns [EObject current=null] : ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_specifications_1_0= ruleSpecificationLine ) )* otherlv_2= 'node' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( (lv_inputs_5_0= ruleInputs ) )? otherlv_6= ')' otherlv_7= 'returns' otherlv_8= '(' ( (lv_outputs_9_0= ruleOutputs ) )? otherlv_10= ')' otherlv_11= ';' (otherlv_12= 'var' ( (lv_locals_13_0= ruleLocals ) ) )? otherlv_14= 'let' ( (lv_body_15_0= ruleBody ) )? otherlv_16= 'tel' (otherlv_17= ';' | otherlv_18= '.' )? ) ;
    public final EObject ruleNode() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_preTraceAnnotation_0_0 = null;

        EObject lv_specifications_1_0 = null;

        EObject lv_inputs_5_0 = null;

        EObject lv_outputs_9_0 = null;

        EObject lv_locals_13_0 = null;

        EObject lv_body_15_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:226:28: ( ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_specifications_1_0= ruleSpecificationLine ) )* otherlv_2= 'node' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( (lv_inputs_5_0= ruleInputs ) )? otherlv_6= ')' otherlv_7= 'returns' otherlv_8= '(' ( (lv_outputs_9_0= ruleOutputs ) )? otherlv_10= ')' otherlv_11= ';' (otherlv_12= 'var' ( (lv_locals_13_0= ruleLocals ) ) )? otherlv_14= 'let' ( (lv_body_15_0= ruleBody ) )? otherlv_16= 'tel' (otherlv_17= ';' | otherlv_18= '.' )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:227:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_specifications_1_0= ruleSpecificationLine ) )* otherlv_2= 'node' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( (lv_inputs_5_0= ruleInputs ) )? otherlv_6= ')' otherlv_7= 'returns' otherlv_8= '(' ( (lv_outputs_9_0= ruleOutputs ) )? otherlv_10= ')' otherlv_11= ';' (otherlv_12= 'var' ( (lv_locals_13_0= ruleLocals ) ) )? otherlv_14= 'let' ( (lv_body_15_0= ruleBody ) )? otherlv_16= 'tel' (otherlv_17= ';' | otherlv_18= '.' )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:227:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_specifications_1_0= ruleSpecificationLine ) )* otherlv_2= 'node' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( (lv_inputs_5_0= ruleInputs ) )? otherlv_6= ')' otherlv_7= 'returns' otherlv_8= '(' ( (lv_outputs_9_0= ruleOutputs ) )? otherlv_10= ')' otherlv_11= ';' (otherlv_12= 'var' ( (lv_locals_13_0= ruleLocals ) ) )? otherlv_14= 'let' ( (lv_body_15_0= ruleBody ) )? otherlv_16= 'tel' (otherlv_17= ';' | otherlv_18= '.' )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:227:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_specifications_1_0= ruleSpecificationLine ) )* otherlv_2= 'node' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( (lv_inputs_5_0= ruleInputs ) )? otherlv_6= ')' otherlv_7= 'returns' otherlv_8= '(' ( (lv_outputs_9_0= ruleOutputs ) )? otherlv_10= ')' otherlv_11= ';' (otherlv_12= 'var' ( (lv_locals_13_0= ruleLocals ) ) )? otherlv_14= 'let' ( (lv_body_15_0= ruleBody ) )? otherlv_16= 'tel' (otherlv_17= ';' | otherlv_18= '.' )?
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:227:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==30) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:228:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:228:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:229:3: lv_preTraceAnnotation_0_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNodeAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleNode426);
                    lv_preTraceAnnotation_0_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNodeRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_0_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:245:3: ( (lv_specifications_1_0= ruleSpecificationLine ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==27) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:246:1: (lv_specifications_1_0= ruleSpecificationLine )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:246:1: (lv_specifications_1_0= ruleSpecificationLine )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:247:3: lv_specifications_1_0= ruleSpecificationLine
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getNodeAccess().getSpecificationsSpecificationLineParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleSpecificationLine_in_ruleNode448);
            	    lv_specifications_1_0=ruleSpecificationLine();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getNodeRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"specifications",
            	              		lv_specifications_1_0, 
            	              		"SpecificationLine");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleNode461); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getNodeAccess().getNodeKeyword_2());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:267:1: ( (lv_name_3_0= RULE_ID ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:268:1: (lv_name_3_0= RULE_ID )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:268:1: (lv_name_3_0= RULE_ID )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:269:3: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleNode478); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_3_0, grammarAccess.getNodeAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getNodeRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_3_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,17,FOLLOW_17_in_ruleNode495); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getNodeAccess().getLeftParenthesisKeyword_4());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:289:1: ( (lv_inputs_5_0= ruleInputs ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID||LA8_0==25||LA8_0==30) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:290:1: (lv_inputs_5_0= ruleInputs )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:290:1: (lv_inputs_5_0= ruleInputs )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:291:3: lv_inputs_5_0= ruleInputs
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNodeAccess().getInputsInputsParserRuleCall_5_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleInputs_in_ruleNode516);
                    lv_inputs_5_0=ruleInputs();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNodeRule());
                      	        }
                             		set(
                             			current, 
                             			"inputs",
                              		lv_inputs_5_0, 
                              		"Inputs");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,18,FOLLOW_18_in_ruleNode529); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getNodeAccess().getRightParenthesisKeyword_6());
                  
            }
            otherlv_7=(Token)match(input,19,FOLLOW_19_in_ruleNode541); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getNodeAccess().getReturnsKeyword_7());
                  
            }
            otherlv_8=(Token)match(input,17,FOLLOW_17_in_ruleNode553); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getNodeAccess().getLeftParenthesisKeyword_8());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:319:1: ( (lv_outputs_9_0= ruleOutputs ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID||LA9_0==25||LA9_0==30) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:320:1: (lv_outputs_9_0= ruleOutputs )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:320:1: (lv_outputs_9_0= ruleOutputs )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:321:3: lv_outputs_9_0= ruleOutputs
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNodeAccess().getOutputsOutputsParserRuleCall_9_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleOutputs_in_ruleNode574);
                    lv_outputs_9_0=ruleOutputs();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNodeRule());
                      	        }
                             		set(
                             			current, 
                             			"outputs",
                              		lv_outputs_9_0, 
                              		"Outputs");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,18,FOLLOW_18_in_ruleNode587); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getNodeAccess().getRightParenthesisKeyword_10());
                  
            }
            otherlv_11=(Token)match(input,14,FOLLOW_14_in_ruleNode599); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getNodeAccess().getSemicolonKeyword_11());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:345:1: (otherlv_12= 'var' ( (lv_locals_13_0= ruleLocals ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==20) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:345:3: otherlv_12= 'var' ( (lv_locals_13_0= ruleLocals ) )
                    {
                    otherlv_12=(Token)match(input,20,FOLLOW_20_in_ruleNode612); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_12, grammarAccess.getNodeAccess().getVarKeyword_12_0());
                          
                    }
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:349:1: ( (lv_locals_13_0= ruleLocals ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:350:1: (lv_locals_13_0= ruleLocals )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:350:1: (lv_locals_13_0= ruleLocals )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:351:3: lv_locals_13_0= ruleLocals
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNodeAccess().getLocalsLocalsParserRuleCall_12_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLocals_in_ruleNode633);
                    lv_locals_13_0=ruleLocals();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNodeRule());
                      	        }
                             		set(
                             			current, 
                             			"locals",
                              		lv_locals_13_0, 
                              		"Locals");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_14=(Token)match(input,21,FOLLOW_21_in_ruleNode647); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_14, grammarAccess.getNodeAccess().getLetKeyword_13());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:371:1: ( (lv_body_15_0= ruleBody ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_ID||LA11_0==17||LA11_0==30||LA11_0==37) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:372:1: (lv_body_15_0= ruleBody )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:372:1: (lv_body_15_0= ruleBody )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:373:3: lv_body_15_0= ruleBody
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNodeAccess().getBodyBodyParserRuleCall_14_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleBody_in_ruleNode668);
                    lv_body_15_0=ruleBody();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNodeRule());
                      	        }
                             		set(
                             			current, 
                             			"body",
                              		lv_body_15_0, 
                              		"Body");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_16=(Token)match(input,22,FOLLOW_22_in_ruleNode681); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_16, grammarAccess.getNodeAccess().getTelKeyword_15());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:393:1: (otherlv_17= ';' | otherlv_18= '.' )?
            int alt12=3;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==14) ) {
                alt12=1;
            }
            else if ( (LA12_0==23) ) {
                alt12=2;
            }
            switch (alt12) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:393:3: otherlv_17= ';'
                    {
                    otherlv_17=(Token)match(input,14,FOLLOW_14_in_ruleNode694); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_17, grammarAccess.getNodeAccess().getSemicolonKeyword_16_0());
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:398:7: otherlv_18= '.'
                    {
                    otherlv_18=(Token)match(input,23,FOLLOW_23_in_ruleNode712); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_18, grammarAccess.getNodeAccess().getFullStopKeyword_16_1());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNode"


    // $ANTLR start "entryRuleFunction"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:410:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:411:2: (iv_ruleFunction= ruleFunction EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:412:2: iv_ruleFunction= ruleFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionRule()); 
            }
            pushFollow(FOLLOW_ruleFunction_in_entryRuleFunction750);
            iv_ruleFunction=ruleFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunction; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunction760); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:419:1: ruleFunction returns [EObject current=null] : (otherlv_0= 'function' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_inputs_3_0= ruleInputs ) )? otherlv_4= ')' otherlv_5= 'returns' otherlv_6= '(' ( (lv_outputs_7_0= ruleOutputs ) )? otherlv_8= ')' ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_inputs_3_0 = null;

        EObject lv_outputs_7_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:422:28: ( (otherlv_0= 'function' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_inputs_3_0= ruleInputs ) )? otherlv_4= ')' otherlv_5= 'returns' otherlv_6= '(' ( (lv_outputs_7_0= ruleOutputs ) )? otherlv_8= ')' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:423:1: (otherlv_0= 'function' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_inputs_3_0= ruleInputs ) )? otherlv_4= ')' otherlv_5= 'returns' otherlv_6= '(' ( (lv_outputs_7_0= ruleOutputs ) )? otherlv_8= ')' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:423:1: (otherlv_0= 'function' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_inputs_3_0= ruleInputs ) )? otherlv_4= ')' otherlv_5= 'returns' otherlv_6= '(' ( (lv_outputs_7_0= ruleOutputs ) )? otherlv_8= ')' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:423:3: otherlv_0= 'function' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_inputs_3_0= ruleInputs ) )? otherlv_4= ')' otherlv_5= 'returns' otherlv_6= '(' ( (lv_outputs_7_0= ruleOutputs ) )? otherlv_8= ')'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleFunction797); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getFunctionAccess().getFunctionKeyword_0());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:427:1: ( (lv_name_1_0= RULE_ID ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:428:1: (lv_name_1_0= RULE_ID )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:428:1: (lv_name_1_0= RULE_ID )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:429:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFunction814); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getFunctionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_17_in_ruleFunction831); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:449:1: ( (lv_inputs_3_0= ruleInputs ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_ID||LA13_0==25||LA13_0==30) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:450:1: (lv_inputs_3_0= ruleInputs )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:450:1: (lv_inputs_3_0= ruleInputs )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:451:3: lv_inputs_3_0= ruleInputs
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFunctionAccess().getInputsInputsParserRuleCall_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleInputs_in_ruleFunction852);
                    lv_inputs_3_0=ruleInputs();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFunctionRule());
                      	        }
                             		set(
                             			current, 
                             			"inputs",
                              		lv_inputs_3_0, 
                              		"Inputs");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,18,FOLLOW_18_in_ruleFunction865); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4());
                  
            }
            otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleFunction877); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getFunctionAccess().getReturnsKeyword_5());
                  
            }
            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleFunction889); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_6());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:479:1: ( (lv_outputs_7_0= ruleOutputs ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_ID||LA14_0==25||LA14_0==30) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:480:1: (lv_outputs_7_0= ruleOutputs )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:480:1: (lv_outputs_7_0= ruleOutputs )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:481:3: lv_outputs_7_0= ruleOutputs
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFunctionAccess().getOutputsOutputsParserRuleCall_7_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleOutputs_in_ruleFunction910);
                    lv_outputs_7_0=ruleOutputs();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFunctionRule());
                      	        }
                             		set(
                             			current, 
                             			"outputs",
                              		lv_outputs_7_0, 
                              		"Outputs");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,18,FOLLOW_18_in_ruleFunction923); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_8());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleAbstractDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:509:1: entryRuleAbstractDeclaration returns [EObject current=null] : iv_ruleAbstractDeclaration= ruleAbstractDeclaration EOF ;
    public final EObject entryRuleAbstractDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractDeclaration = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:510:2: (iv_ruleAbstractDeclaration= ruleAbstractDeclaration EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:511:2: iv_ruleAbstractDeclaration= ruleAbstractDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAbstractDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleAbstractDeclaration_in_entryRuleAbstractDeclaration959);
            iv_ruleAbstractDeclaration=ruleAbstractDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAbstractDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAbstractDeclaration969); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractDeclaration"


    // $ANTLR start "ruleAbstractDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:518:1: ruleAbstractDeclaration returns [EObject current=null] : (this_IDeclaration_0= ruleIDeclaration | this_DDeclaration_1= ruleDDeclaration | this_RDeclaration_2= ruleRDeclaration | this_BDeclaration_3= ruleBDeclaration | this_ArrayDeclaration_4= ruleArrayDeclaration ) ;
    public final EObject ruleAbstractDeclaration() throws RecognitionException {
        EObject current = null;

        EObject this_IDeclaration_0 = null;

        EObject this_DDeclaration_1 = null;

        EObject this_RDeclaration_2 = null;

        EObject this_BDeclaration_3 = null;

        EObject this_ArrayDeclaration_4 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:521:28: ( (this_IDeclaration_0= ruleIDeclaration | this_DDeclaration_1= ruleDDeclaration | this_RDeclaration_2= ruleRDeclaration | this_BDeclaration_3= ruleBDeclaration | this_ArrayDeclaration_4= ruleArrayDeclaration ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:522:1: (this_IDeclaration_0= ruleIDeclaration | this_DDeclaration_1= ruleDDeclaration | this_RDeclaration_2= ruleRDeclaration | this_BDeclaration_3= ruleBDeclaration | this_ArrayDeclaration_4= ruleArrayDeclaration )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:522:1: (this_IDeclaration_0= ruleIDeclaration | this_DDeclaration_1= ruleDDeclaration | this_RDeclaration_2= ruleRDeclaration | this_BDeclaration_3= ruleBDeclaration | this_ArrayDeclaration_4= ruleArrayDeclaration )
            int alt15=5;
            alt15 = dfa15.predict(input);
            switch (alt15) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:523:5: this_IDeclaration_0= ruleIDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractDeclarationAccess().getIDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIDeclaration_in_ruleAbstractDeclaration1016);
                    this_IDeclaration_0=ruleIDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:533:5: this_DDeclaration_1= ruleDDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractDeclarationAccess().getDDeclarationParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleDDeclaration_in_ruleAbstractDeclaration1043);
                    this_DDeclaration_1=ruleDDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_DDeclaration_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:543:5: this_RDeclaration_2= ruleRDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractDeclarationAccess().getRDeclarationParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleRDeclaration_in_ruleAbstractDeclaration1070);
                    this_RDeclaration_2=ruleRDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RDeclaration_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:553:5: this_BDeclaration_3= ruleBDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractDeclarationAccess().getBDeclarationParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBDeclaration_in_ruleAbstractDeclaration1097);
                    this_BDeclaration_3=ruleBDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BDeclaration_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:563:5: this_ArrayDeclaration_4= ruleArrayDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractDeclarationAccess().getArrayDeclarationParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleArrayDeclaration_in_ruleAbstractDeclaration1124);
                    this_ArrayDeclaration_4=ruleArrayDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ArrayDeclaration_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractDeclaration"


    // $ANTLR start "entryRuleIDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:579:1: entryRuleIDeclaration returns [EObject current=null] : iv_ruleIDeclaration= ruleIDeclaration EOF ;
    public final EObject entryRuleIDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIDeclaration = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:580:2: (iv_ruleIDeclaration= ruleIDeclaration EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:581:2: iv_ruleIDeclaration= ruleIDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleIDeclaration_in_entryRuleIDeclaration1159);
            iv_ruleIDeclaration=ruleIDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIDeclaration1169); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIDeclaration"


    // $ANTLR start "ruleIDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:588:1: ruleIDeclaration returns [EObject current=null] : ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleLUS_INT ) ) otherlv_5= ';' ) ;
    public final EObject ruleIDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_preTraceAnnotation_0_0 = null;

        EObject lv_var_2_0 = null;

        AntlrDatatypeRuleToken lv_value_4_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:591:28: ( ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleLUS_INT ) ) otherlv_5= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:592:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleLUS_INT ) ) otherlv_5= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:592:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleLUS_INT ) ) otherlv_5= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:592:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleLUS_INT ) ) otherlv_5= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:592:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==30) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:593:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:593:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:594:3: lv_preTraceAnnotation_0_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIDeclarationAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleIDeclaration1215);
                    lv_preTraceAnnotation_0_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_0_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleIDeclaration1228); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getIDeclarationAccess().getConstKeyword_1());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:614:1: ( (lv_var_2_0= ruleVariable ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:615:1: (lv_var_2_0= ruleVariable )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:615:1: (lv_var_2_0= ruleVariable )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:616:3: lv_var_2_0= ruleVariable
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIDeclarationAccess().getVarVariableParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariable_in_ruleIDeclaration1249);
            lv_var_2_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"var",
                      		lv_var_2_0, 
                      		"Variable");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,26,FOLLOW_26_in_ruleIDeclaration1261); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getIDeclarationAccess().getEqualsSignKeyword_3());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:636:1: ( (lv_value_4_0= ruleLUS_INT ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:637:1: (lv_value_4_0= ruleLUS_INT )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:637:1: (lv_value_4_0= ruleLUS_INT )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:638:3: lv_value_4_0= ruleLUS_INT
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIDeclarationAccess().getValueLUS_INTParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLUS_INT_in_ruleIDeclaration1282);
            lv_value_4_0=ruleLUS_INT();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_4_0, 
                      		"LUS_INT");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleIDeclaration1294); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getIDeclarationAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIDeclaration"


    // $ANTLR start "entryRuleDDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:666:1: entryRuleDDeclaration returns [EObject current=null] : iv_ruleDDeclaration= ruleDDeclaration EOF ;
    public final EObject entryRuleDDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDDeclaration = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:667:2: (iv_ruleDDeclaration= ruleDDeclaration EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:668:2: iv_ruleDDeclaration= ruleDDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleDDeclaration_in_entryRuleDDeclaration1330);
            iv_ruleDDeclaration=ruleDDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDDeclaration1340); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDDeclaration"


    // $ANTLR start "ruleDDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:675:1: ruleDDeclaration returns [EObject current=null] : ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_DOUBLE ) ) otherlv_5= ';' ) ;
    public final EObject ruleDDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_value_4_0=null;
        Token otherlv_5=null;
        EObject lv_preTraceAnnotation_0_0 = null;

        EObject lv_var_2_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:678:28: ( ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_DOUBLE ) ) otherlv_5= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:679:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_DOUBLE ) ) otherlv_5= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:679:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_DOUBLE ) ) otherlv_5= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:679:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_DOUBLE ) ) otherlv_5= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:679:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==30) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:680:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:680:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:681:3: lv_preTraceAnnotation_0_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDDeclarationAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleDDeclaration1386);
                    lv_preTraceAnnotation_0_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_0_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleDDeclaration1399); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getDDeclarationAccess().getConstKeyword_1());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:701:1: ( (lv_var_2_0= ruleVariable ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:702:1: (lv_var_2_0= ruleVariable )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:702:1: (lv_var_2_0= ruleVariable )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:703:3: lv_var_2_0= ruleVariable
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getDDeclarationAccess().getVarVariableParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariable_in_ruleDDeclaration1420);
            lv_var_2_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getDDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"var",
                      		lv_var_2_0, 
                      		"Variable");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,26,FOLLOW_26_in_ruleDDeclaration1432); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getDDeclarationAccess().getEqualsSignKeyword_3());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:723:1: ( (lv_value_4_0= RULE_LUS_DOUBLE ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:724:1: (lv_value_4_0= RULE_LUS_DOUBLE )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:724:1: (lv_value_4_0= RULE_LUS_DOUBLE )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:725:3: lv_value_4_0= RULE_LUS_DOUBLE
            {
            lv_value_4_0=(Token)match(input,RULE_LUS_DOUBLE,FOLLOW_RULE_LUS_DOUBLE_in_ruleDDeclaration1449); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_4_0, grammarAccess.getDDeclarationAccess().getValueLUS_DOUBLETerminalRuleCall_4_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getDDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_4_0, 
                      		"LUS_DOUBLE");
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleDDeclaration1466); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getDDeclarationAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDDeclaration"


    // $ANTLR start "entryRuleRDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:753:1: entryRuleRDeclaration returns [EObject current=null] : iv_ruleRDeclaration= ruleRDeclaration EOF ;
    public final EObject entryRuleRDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRDeclaration = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:754:2: (iv_ruleRDeclaration= ruleRDeclaration EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:755:2: iv_ruleRDeclaration= ruleRDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleRDeclaration_in_entryRuleRDeclaration1502);
            iv_ruleRDeclaration=ruleRDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRDeclaration1512); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRDeclaration"


    // $ANTLR start "ruleRDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:762:1: ruleRDeclaration returns [EObject current=null] : ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_REAL ) ) otherlv_5= ';' ) ;
    public final EObject ruleRDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_value_4_0=null;
        Token otherlv_5=null;
        EObject lv_preTraceAnnotation_0_0 = null;

        EObject lv_var_2_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:765:28: ( ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_REAL ) ) otherlv_5= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:766:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_REAL ) ) otherlv_5= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:766:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_REAL ) ) otherlv_5= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:766:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_REAL ) ) otherlv_5= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:766:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==30) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:767:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:767:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:768:3: lv_preTraceAnnotation_0_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRDeclarationAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleRDeclaration1558);
                    lv_preTraceAnnotation_0_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_0_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleRDeclaration1571); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRDeclarationAccess().getConstKeyword_1());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:788:1: ( (lv_var_2_0= ruleVariable ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:789:1: (lv_var_2_0= ruleVariable )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:789:1: (lv_var_2_0= ruleVariable )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:790:3: lv_var_2_0= ruleVariable
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRDeclarationAccess().getVarVariableParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariable_in_ruleRDeclaration1592);
            lv_var_2_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"var",
                      		lv_var_2_0, 
                      		"Variable");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,26,FOLLOW_26_in_ruleRDeclaration1604); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getRDeclarationAccess().getEqualsSignKeyword_3());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:810:1: ( (lv_value_4_0= RULE_LUS_REAL ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:811:1: (lv_value_4_0= RULE_LUS_REAL )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:811:1: (lv_value_4_0= RULE_LUS_REAL )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:812:3: lv_value_4_0= RULE_LUS_REAL
            {
            lv_value_4_0=(Token)match(input,RULE_LUS_REAL,FOLLOW_RULE_LUS_REAL_in_ruleRDeclaration1621); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_4_0, grammarAccess.getRDeclarationAccess().getValueLUS_REALTerminalRuleCall_4_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_4_0, 
                      		"LUS_REAL");
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleRDeclaration1638); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getRDeclarationAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRDeclaration"


    // $ANTLR start "entryRuleBDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:840:1: entryRuleBDeclaration returns [EObject current=null] : iv_ruleBDeclaration= ruleBDeclaration EOF ;
    public final EObject entryRuleBDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBDeclaration = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:841:2: (iv_ruleBDeclaration= ruleBDeclaration EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:842:2: iv_ruleBDeclaration= ruleBDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleBDeclaration_in_entryRuleBDeclaration1674);
            iv_ruleBDeclaration=ruleBDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBDeclaration1684); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBDeclaration"


    // $ANTLR start "ruleBDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:849:1: ruleBDeclaration returns [EObject current=null] : ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_BOOLEAN ) ) otherlv_5= ';' ) ;
    public final EObject ruleBDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_value_4_0=null;
        Token otherlv_5=null;
        EObject lv_preTraceAnnotation_0_0 = null;

        EObject lv_var_2_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:852:28: ( ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_BOOLEAN ) ) otherlv_5= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:853:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_BOOLEAN ) ) otherlv_5= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:853:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_BOOLEAN ) ) otherlv_5= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:853:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= RULE_LUS_BOOLEAN ) ) otherlv_5= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:853:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==30) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:854:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:854:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:855:3: lv_preTraceAnnotation_0_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getBDeclarationAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleBDeclaration1730);
                    lv_preTraceAnnotation_0_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getBDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_0_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleBDeclaration1743); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getBDeclarationAccess().getConstKeyword_1());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:875:1: ( (lv_var_2_0= ruleVariable ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:876:1: (lv_var_2_0= ruleVariable )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:876:1: (lv_var_2_0= ruleVariable )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:877:3: lv_var_2_0= ruleVariable
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getBDeclarationAccess().getVarVariableParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariable_in_ruleBDeclaration1764);
            lv_var_2_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getBDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"var",
                      		lv_var_2_0, 
                      		"Variable");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,26,FOLLOW_26_in_ruleBDeclaration1776); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getBDeclarationAccess().getEqualsSignKeyword_3());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:897:1: ( (lv_value_4_0= RULE_LUS_BOOLEAN ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:898:1: (lv_value_4_0= RULE_LUS_BOOLEAN )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:898:1: (lv_value_4_0= RULE_LUS_BOOLEAN )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:899:3: lv_value_4_0= RULE_LUS_BOOLEAN
            {
            lv_value_4_0=(Token)match(input,RULE_LUS_BOOLEAN,FOLLOW_RULE_LUS_BOOLEAN_in_ruleBDeclaration1793); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_4_0, grammarAccess.getBDeclarationAccess().getValueLUS_BOOLEANTerminalRuleCall_4_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_4_0, 
                      		"LUS_BOOLEAN");
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleBDeclaration1810); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getBDeclarationAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBDeclaration"


    // $ANTLR start "entryRuleArrayDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:927:1: entryRuleArrayDeclaration returns [EObject current=null] : iv_ruleArrayDeclaration= ruleArrayDeclaration EOF ;
    public final EObject entryRuleArrayDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayDeclaration = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:928:2: (iv_ruleArrayDeclaration= ruleArrayDeclaration EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:929:2: iv_ruleArrayDeclaration= ruleArrayDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArrayDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleArrayDeclaration_in_entryRuleArrayDeclaration1846);
            iv_ruleArrayDeclaration=ruleArrayDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArrayDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleArrayDeclaration1856); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayDeclaration"


    // $ANTLR start "ruleArrayDeclaration"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:936:1: ruleArrayDeclaration returns [EObject current=null] : ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleArrayDefinition ) ) otherlv_5= ';' ) ;
    public final EObject ruleArrayDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_preTraceAnnotation_0_0 = null;

        EObject lv_var_2_0 = null;

        EObject lv_value_4_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:939:28: ( ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleArrayDefinition ) ) otherlv_5= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:940:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleArrayDefinition ) ) otherlv_5= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:940:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleArrayDefinition ) ) otherlv_5= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:940:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? otherlv_1= 'const' ( (lv_var_2_0= ruleVariable ) ) otherlv_3= '=' ( (lv_value_4_0= ruleArrayDefinition ) ) otherlv_5= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:940:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==30) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:941:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:941:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:942:3: lv_preTraceAnnotation_0_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getArrayDeclarationAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleArrayDeclaration1902);
                    lv_preTraceAnnotation_0_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getArrayDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_0_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleArrayDeclaration1915); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getArrayDeclarationAccess().getConstKeyword_1());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:962:1: ( (lv_var_2_0= ruleVariable ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:963:1: (lv_var_2_0= ruleVariable )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:963:1: (lv_var_2_0= ruleVariable )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:964:3: lv_var_2_0= ruleVariable
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getArrayDeclarationAccess().getVarVariableParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariable_in_ruleArrayDeclaration1936);
            lv_var_2_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getArrayDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"var",
                      		lv_var_2_0, 
                      		"Variable");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,26,FOLLOW_26_in_ruleArrayDeclaration1948); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getArrayDeclarationAccess().getEqualsSignKeyword_3());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:984:1: ( (lv_value_4_0= ruleArrayDefinition ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:985:1: (lv_value_4_0= ruleArrayDefinition )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:985:1: (lv_value_4_0= ruleArrayDefinition )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:986:3: lv_value_4_0= ruleArrayDefinition
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getArrayDeclarationAccess().getValueArrayDefinitionParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleArrayDefinition_in_ruleArrayDeclaration1969);
            lv_value_4_0=ruleArrayDefinition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getArrayDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_4_0, 
                      		"ArrayDefinition");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleArrayDeclaration1981); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getArrayDeclarationAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayDeclaration"


    // $ANTLR start "entryRuleLUS_INT"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1014:1: entryRuleLUS_INT returns [String current=null] : iv_ruleLUS_INT= ruleLUS_INT EOF ;
    public final String entryRuleLUS_INT() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLUS_INT = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1015:2: (iv_ruleLUS_INT= ruleLUS_INT EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1016:2: iv_ruleLUS_INT= ruleLUS_INT EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLUS_INTRule()); 
            }
            pushFollow(FOLLOW_ruleLUS_INT_in_entryRuleLUS_INT2018);
            iv_ruleLUS_INT=ruleLUS_INT();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLUS_INT.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLUS_INT2029); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLUS_INT"


    // $ANTLR start "ruleLUS_INT"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1023:1: ruleLUS_INT returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleLUS_INT() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1026:28: (this_INT_0= RULE_INT )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1027:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleLUS_INT2068); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_INT_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_INT_0, grammarAccess.getLUS_INTAccess().getINTTerminalRuleCall()); 
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLUS_INT"


    // $ANTLR start "entryRuleSpecificationLine"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1042:1: entryRuleSpecificationLine returns [EObject current=null] : iv_ruleSpecificationLine= ruleSpecificationLine EOF ;
    public final EObject entryRuleSpecificationLine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSpecificationLine = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1043:2: (iv_ruleSpecificationLine= ruleSpecificationLine EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1044:2: iv_ruleSpecificationLine= ruleSpecificationLine EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSpecificationLineRule()); 
            }
            pushFollow(FOLLOW_ruleSpecificationLine_in_entryRuleSpecificationLine2112);
            iv_ruleSpecificationLine=ruleSpecificationLine();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSpecificationLine; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSpecificationLine2122); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSpecificationLine"


    // $ANTLR start "ruleSpecificationLine"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1051:1: ruleSpecificationLine returns [EObject current=null] : ( () otherlv_1= '--@' ( (lv_type_2_0= ruleSpecificationType ) ) ( (lv_specLine_3_0= ruleFbyExpression ) ) otherlv_4= ';' ) ;
    public final EObject ruleSpecificationLine() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Enumerator lv_type_2_0 = null;

        EObject lv_specLine_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1054:28: ( ( () otherlv_1= '--@' ( (lv_type_2_0= ruleSpecificationType ) ) ( (lv_specLine_3_0= ruleFbyExpression ) ) otherlv_4= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1055:1: ( () otherlv_1= '--@' ( (lv_type_2_0= ruleSpecificationType ) ) ( (lv_specLine_3_0= ruleFbyExpression ) ) otherlv_4= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1055:1: ( () otherlv_1= '--@' ( (lv_type_2_0= ruleSpecificationType ) ) ( (lv_specLine_3_0= ruleFbyExpression ) ) otherlv_4= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1055:2: () otherlv_1= '--@' ( (lv_type_2_0= ruleSpecificationType ) ) ( (lv_specLine_3_0= ruleFbyExpression ) ) otherlv_4= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1055:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1056:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getSpecificationLineAccess().getSpecificationLineAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,27,FOLLOW_27_in_ruleSpecificationLine2168); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getSpecificationLineAccess().getHyphenMinusHyphenMinusCommercialAtKeyword_1());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1065:1: ( (lv_type_2_0= ruleSpecificationType ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1066:1: (lv_type_2_0= ruleSpecificationType )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1066:1: (lv_type_2_0= ruleSpecificationType )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1067:3: lv_type_2_0= ruleSpecificationType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSpecificationLineAccess().getTypeSpecificationTypeEnumRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleSpecificationType_in_ruleSpecificationLine2189);
            lv_type_2_0=ruleSpecificationType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSpecificationLineRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_2_0, 
                      		"SpecificationType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1083:2: ( (lv_specLine_3_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1084:1: (lv_specLine_3_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1084:1: (lv_specLine_3_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1085:3: lv_specLine_3_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSpecificationLineAccess().getSpecLineFbyExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleSpecificationLine2210);
            lv_specLine_3_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSpecificationLineRule());
              	        }
                     		set(
                     			current, 
                     			"specLine",
                      		lv_specLine_3_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleSpecificationLine2222); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getSpecificationLineAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSpecificationLine"


    // $ANTLR start "entryRuleInputs"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1113:1: entryRuleInputs returns [EObject current=null] : iv_ruleInputs= ruleInputs EOF ;
    public final EObject entryRuleInputs() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputs = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1114:2: (iv_ruleInputs= ruleInputs EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1115:2: iv_ruleInputs= ruleInputs EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputsRule()); 
            }
            pushFollow(FOLLOW_ruleInputs_in_entryRuleInputs2258);
            iv_ruleInputs=ruleInputs();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputs; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleInputs2268); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputs"


    // $ANTLR start "ruleInputs"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1122:1: ruleInputs returns [EObject current=null] : ( ( (lv_inputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_inputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? ) ;
    public final EObject ruleInputs() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_inputs_0_0 = null;

        EObject lv_inputs_2_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1125:28: ( ( ( (lv_inputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_inputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1126:1: ( ( (lv_inputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_inputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1126:1: ( ( (lv_inputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_inputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1126:2: ( (lv_inputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_inputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )?
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1126:2: ( (lv_inputs_0_0= ruleVariablesList ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1127:1: (lv_inputs_0_0= ruleVariablesList )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1127:1: (lv_inputs_0_0= ruleVariablesList )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1128:3: lv_inputs_0_0= ruleVariablesList
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getInputsAccess().getInputsVariablesListParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariablesList_in_ruleInputs2314);
            lv_inputs_0_0=ruleVariablesList();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getInputsRule());
              	        }
                     		add(
                     			current, 
                     			"inputs",
                      		lv_inputs_0_0, 
                      		"VariablesList");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1144:2: (otherlv_1= ';' ( (lv_inputs_2_0= ruleVariablesList ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==14) ) {
                    int LA21_1 = input.LA(2);

                    if ( (LA21_1==RULE_ID||LA21_1==25||LA21_1==30) ) {
                        alt21=1;
                    }


                }


                switch (alt21) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1144:4: otherlv_1= ';' ( (lv_inputs_2_0= ruleVariablesList ) )
            	    {
            	    otherlv_1=(Token)match(input,14,FOLLOW_14_in_ruleInputs2327); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getInputsAccess().getSemicolonKeyword_1_0());
            	          
            	    }
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1148:1: ( (lv_inputs_2_0= ruleVariablesList ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1149:1: (lv_inputs_2_0= ruleVariablesList )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1149:1: (lv_inputs_2_0= ruleVariablesList )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1150:3: lv_inputs_2_0= ruleVariablesList
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getInputsAccess().getInputsVariablesListParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleVariablesList_in_ruleInputs2348);
            	    lv_inputs_2_0=ruleVariablesList();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getInputsRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"inputs",
            	              		lv_inputs_2_0, 
            	              		"VariablesList");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1166:4: (otherlv_3= ';' )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==14) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1166:6: otherlv_3= ';'
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleInputs2363); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getInputsAccess().getSemicolonKeyword_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputs"


    // $ANTLR start "entryRuleOutputs"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1178:1: entryRuleOutputs returns [EObject current=null] : iv_ruleOutputs= ruleOutputs EOF ;
    public final EObject entryRuleOutputs() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputs = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1179:2: (iv_ruleOutputs= ruleOutputs EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1180:2: iv_ruleOutputs= ruleOutputs EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputsRule()); 
            }
            pushFollow(FOLLOW_ruleOutputs_in_entryRuleOutputs2401);
            iv_ruleOutputs=ruleOutputs();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputs; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOutputs2411); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputs"


    // $ANTLR start "ruleOutputs"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1187:1: ruleOutputs returns [EObject current=null] : ( ( (lv_outputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_outputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? ) ;
    public final EObject ruleOutputs() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_outputs_0_0 = null;

        EObject lv_outputs_2_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1190:28: ( ( ( (lv_outputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_outputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1191:1: ( ( (lv_outputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_outputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1191:1: ( ( (lv_outputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_outputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1191:2: ( (lv_outputs_0_0= ruleVariablesList ) ) (otherlv_1= ';' ( (lv_outputs_2_0= ruleVariablesList ) ) )* (otherlv_3= ';' )?
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1191:2: ( (lv_outputs_0_0= ruleVariablesList ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1192:1: (lv_outputs_0_0= ruleVariablesList )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1192:1: (lv_outputs_0_0= ruleVariablesList )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1193:3: lv_outputs_0_0= ruleVariablesList
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getOutputsAccess().getOutputsVariablesListParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariablesList_in_ruleOutputs2457);
            lv_outputs_0_0=ruleVariablesList();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getOutputsRule());
              	        }
                     		add(
                     			current, 
                     			"outputs",
                      		lv_outputs_0_0, 
                      		"VariablesList");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1209:2: (otherlv_1= ';' ( (lv_outputs_2_0= ruleVariablesList ) ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==14) ) {
                    int LA23_1 = input.LA(2);

                    if ( (LA23_1==RULE_ID||LA23_1==25||LA23_1==30) ) {
                        alt23=1;
                    }


                }


                switch (alt23) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1209:4: otherlv_1= ';' ( (lv_outputs_2_0= ruleVariablesList ) )
            	    {
            	    otherlv_1=(Token)match(input,14,FOLLOW_14_in_ruleOutputs2470); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getOutputsAccess().getSemicolonKeyword_1_0());
            	          
            	    }
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1213:1: ( (lv_outputs_2_0= ruleVariablesList ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1214:1: (lv_outputs_2_0= ruleVariablesList )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1214:1: (lv_outputs_2_0= ruleVariablesList )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1215:3: lv_outputs_2_0= ruleVariablesList
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getOutputsAccess().getOutputsVariablesListParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleVariablesList_in_ruleOutputs2491);
            	    lv_outputs_2_0=ruleVariablesList();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getOutputsRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"outputs",
            	              		lv_outputs_2_0, 
            	              		"VariablesList");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1231:4: (otherlv_3= ';' )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==14) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1231:6: otherlv_3= ';'
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleOutputs2506); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getOutputsAccess().getSemicolonKeyword_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputs"


    // $ANTLR start "entryRuleLocals"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1243:1: entryRuleLocals returns [EObject current=null] : iv_ruleLocals= ruleLocals EOF ;
    public final EObject entryRuleLocals() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLocals = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1244:2: (iv_ruleLocals= ruleLocals EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1245:2: iv_ruleLocals= ruleLocals EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLocalsRule()); 
            }
            pushFollow(FOLLOW_ruleLocals_in_entryRuleLocals2544);
            iv_ruleLocals=ruleLocals();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLocals; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLocals2554); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLocals"


    // $ANTLR start "ruleLocals"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1252:1: ruleLocals returns [EObject current=null] : ( () ( ( (lv_locals_1_0= ruleVariablesList ) ) (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )* otherlv_4= ';' )? ) ;
    public final EObject ruleLocals() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_locals_1_0 = null;

        EObject lv_locals_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1255:28: ( ( () ( ( (lv_locals_1_0= ruleVariablesList ) ) (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )* otherlv_4= ';' )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1256:1: ( () ( ( (lv_locals_1_0= ruleVariablesList ) ) (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )* otherlv_4= ';' )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1256:1: ( () ( ( (lv_locals_1_0= ruleVariablesList ) ) (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )* otherlv_4= ';' )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1256:2: () ( ( (lv_locals_1_0= ruleVariablesList ) ) (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )* otherlv_4= ';' )?
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1256:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1257:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getLocalsAccess().getLocalsAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1262:2: ( ( (lv_locals_1_0= ruleVariablesList ) ) (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )* otherlv_4= ';' )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==RULE_ID||LA26_0==25||LA26_0==30) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1262:3: ( (lv_locals_1_0= ruleVariablesList ) ) (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )* otherlv_4= ';'
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1262:3: ( (lv_locals_1_0= ruleVariablesList ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1263:1: (lv_locals_1_0= ruleVariablesList )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1263:1: (lv_locals_1_0= ruleVariablesList )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1264:3: lv_locals_1_0= ruleVariablesList
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLocalsAccess().getLocalsVariablesListParserRuleCall_1_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleVariablesList_in_ruleLocals2610);
                    lv_locals_1_0=ruleVariablesList();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLocalsRule());
                      	        }
                             		add(
                             			current, 
                             			"locals",
                              		lv_locals_1_0, 
                              		"VariablesList");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1280:2: (otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) ) )*
                    loop25:
                    do {
                        int alt25=2;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==14) ) {
                            int LA25_1 = input.LA(2);

                            if ( (LA25_1==RULE_ID||LA25_1==25||LA25_1==30) ) {
                                alt25=1;
                            }


                        }


                        switch (alt25) {
                    	case 1 :
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1280:4: otherlv_2= ';' ( (lv_locals_3_0= ruleVariablesList ) )
                    	    {
                    	    otherlv_2=(Token)match(input,14,FOLLOW_14_in_ruleLocals2623); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_2, grammarAccess.getLocalsAccess().getSemicolonKeyword_1_1_0());
                    	          
                    	    }
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1284:1: ( (lv_locals_3_0= ruleVariablesList ) )
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1285:1: (lv_locals_3_0= ruleVariablesList )
                    	    {
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1285:1: (lv_locals_3_0= ruleVariablesList )
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1286:3: lv_locals_3_0= ruleVariablesList
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getLocalsAccess().getLocalsVariablesListParserRuleCall_1_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleVariablesList_in_ruleLocals2644);
                    	    lv_locals_3_0=ruleVariablesList();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getLocalsRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"locals",
                    	              		lv_locals_3_0, 
                    	              		"VariablesList");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop25;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleLocals2658); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getLocalsAccess().getSemicolonKeyword_1_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLocals"


    // $ANTLR start "entryRuleBody"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1314:1: entryRuleBody returns [EObject current=null] : iv_ruleBody= ruleBody EOF ;
    public final EObject entryRuleBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBody = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1315:2: (iv_ruleBody= ruleBody EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1316:2: iv_ruleBody= ruleBody EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBodyRule()); 
            }
            pushFollow(FOLLOW_ruleBody_in_entryRuleBody2696);
            iv_ruleBody=ruleBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBody; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBody2706); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBody"


    // $ANTLR start "ruleBody"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1323:1: ruleBody returns [EObject current=null] : ( ( ( (lv_equations_0_0= ruleEquation ) ) | ( (lv_asserts_1_0= ruleAssert ) ) ) ( ( (lv_equations_2_0= ruleEquation ) ) | ( (lv_asserts_3_0= ruleAssert ) ) )* ( (lv_annotations_4_0= ruleAnnotation ) )* ) ;
    public final EObject ruleBody() throws RecognitionException {
        EObject current = null;

        EObject lv_equations_0_0 = null;

        EObject lv_asserts_1_0 = null;

        EObject lv_equations_2_0 = null;

        EObject lv_asserts_3_0 = null;

        EObject lv_annotations_4_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1326:28: ( ( ( ( (lv_equations_0_0= ruleEquation ) ) | ( (lv_asserts_1_0= ruleAssert ) ) ) ( ( (lv_equations_2_0= ruleEquation ) ) | ( (lv_asserts_3_0= ruleAssert ) ) )* ( (lv_annotations_4_0= ruleAnnotation ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1327:1: ( ( ( (lv_equations_0_0= ruleEquation ) ) | ( (lv_asserts_1_0= ruleAssert ) ) ) ( ( (lv_equations_2_0= ruleEquation ) ) | ( (lv_asserts_3_0= ruleAssert ) ) )* ( (lv_annotations_4_0= ruleAnnotation ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1327:1: ( ( ( (lv_equations_0_0= ruleEquation ) ) | ( (lv_asserts_1_0= ruleAssert ) ) ) ( ( (lv_equations_2_0= ruleEquation ) ) | ( (lv_asserts_3_0= ruleAssert ) ) )* ( (lv_annotations_4_0= ruleAnnotation ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1327:2: ( ( (lv_equations_0_0= ruleEquation ) ) | ( (lv_asserts_1_0= ruleAssert ) ) ) ( ( (lv_equations_2_0= ruleEquation ) ) | ( (lv_asserts_3_0= ruleAssert ) ) )* ( (lv_annotations_4_0= ruleAnnotation ) )*
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1327:2: ( ( (lv_equations_0_0= ruleEquation ) ) | ( (lv_asserts_1_0= ruleAssert ) ) )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_ID||LA27_0==17||LA27_0==30) ) {
                alt27=1;
            }
            else if ( (LA27_0==37) ) {
                alt27=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1327:3: ( (lv_equations_0_0= ruleEquation ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1327:3: ( (lv_equations_0_0= ruleEquation ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1328:1: (lv_equations_0_0= ruleEquation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1328:1: (lv_equations_0_0= ruleEquation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1329:3: lv_equations_0_0= ruleEquation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getBodyAccess().getEquationsEquationParserRuleCall_0_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleEquation_in_ruleBody2753);
                    lv_equations_0_0=ruleEquation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getBodyRule());
                      	        }
                             		add(
                             			current, 
                             			"equations",
                              		lv_equations_0_0, 
                              		"Equation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1346:6: ( (lv_asserts_1_0= ruleAssert ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1346:6: ( (lv_asserts_1_0= ruleAssert ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1347:1: (lv_asserts_1_0= ruleAssert )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1347:1: (lv_asserts_1_0= ruleAssert )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1348:3: lv_asserts_1_0= ruleAssert
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getBodyAccess().getAssertsAssertParserRuleCall_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAssert_in_ruleBody2780);
                    lv_asserts_1_0=ruleAssert();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getBodyRule());
                      	        }
                             		add(
                             			current, 
                             			"asserts",
                              		lv_asserts_1_0, 
                              		"Assert");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1364:3: ( ( (lv_equations_2_0= ruleEquation ) ) | ( (lv_asserts_3_0= ruleAssert ) ) )*
            loop28:
            do {
                int alt28=3;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_ID||LA28_0==17||LA28_0==30) ) {
                    alt28=1;
                }
                else if ( (LA28_0==37) ) {
                    alt28=2;
                }


                switch (alt28) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1364:4: ( (lv_equations_2_0= ruleEquation ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1364:4: ( (lv_equations_2_0= ruleEquation ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1365:1: (lv_equations_2_0= ruleEquation )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1365:1: (lv_equations_2_0= ruleEquation )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1366:3: lv_equations_2_0= ruleEquation
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getBodyAccess().getEquationsEquationParserRuleCall_1_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleEquation_in_ruleBody2803);
            	    lv_equations_2_0=ruleEquation();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getBodyRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"equations",
            	              		lv_equations_2_0, 
            	              		"Equation");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1383:6: ( (lv_asserts_3_0= ruleAssert ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1383:6: ( (lv_asserts_3_0= ruleAssert ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1384:1: (lv_asserts_3_0= ruleAssert )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1384:1: (lv_asserts_3_0= ruleAssert )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1385:3: lv_asserts_3_0= ruleAssert
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getBodyAccess().getAssertsAssertParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAssert_in_ruleBody2830);
            	    lv_asserts_3_0=ruleAssert();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getBodyRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"asserts",
            	              		lv_asserts_3_0, 
            	              		"Assert");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1401:4: ( (lv_annotations_4_0= ruleAnnotation ) )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==28) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1402:1: (lv_annotations_4_0= ruleAnnotation )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1402:1: (lv_annotations_4_0= ruleAnnotation )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1403:3: lv_annotations_4_0= ruleAnnotation
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getBodyAccess().getAnnotationsAnnotationParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAnnotation_in_ruleBody2853);
            	    lv_annotations_4_0=ruleAnnotation();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getBodyRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"annotations",
            	              		lv_annotations_4_0, 
            	              		"Annotation");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBody"


    // $ANTLR start "entryRuleAnnotation"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1427:1: entryRuleAnnotation returns [EObject current=null] : iv_ruleAnnotation= ruleAnnotation EOF ;
    public final EObject entryRuleAnnotation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotation = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1428:2: (iv_ruleAnnotation= ruleAnnotation EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1429:2: iv_ruleAnnotation= ruleAnnotation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAnnotationRule()); 
            }
            pushFollow(FOLLOW_ruleAnnotation_in_entryRuleAnnotation2890);
            iv_ruleAnnotation=ruleAnnotation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAnnotation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnnotation2900); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotation"


    // $ANTLR start "ruleAnnotation"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1436:1: ruleAnnotation returns [EObject current=null] : (otherlv_0= '--!' ( (lv_identifier_1_0= ruleBodyAnnotationIdentifier ) ) otherlv_2= ':' ( (lv_expression_3_0= ruleFbyExpression ) ) otherlv_4= ';' ) ;
    public final EObject ruleAnnotation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_identifier_1_0 = null;

        EObject lv_expression_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1439:28: ( (otherlv_0= '--!' ( (lv_identifier_1_0= ruleBodyAnnotationIdentifier ) ) otherlv_2= ':' ( (lv_expression_3_0= ruleFbyExpression ) ) otherlv_4= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1440:1: (otherlv_0= '--!' ( (lv_identifier_1_0= ruleBodyAnnotationIdentifier ) ) otherlv_2= ':' ( (lv_expression_3_0= ruleFbyExpression ) ) otherlv_4= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1440:1: (otherlv_0= '--!' ( (lv_identifier_1_0= ruleBodyAnnotationIdentifier ) ) otherlv_2= ':' ( (lv_expression_3_0= ruleFbyExpression ) ) otherlv_4= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1440:3: otherlv_0= '--!' ( (lv_identifier_1_0= ruleBodyAnnotationIdentifier ) ) otherlv_2= ':' ( (lv_expression_3_0= ruleFbyExpression ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_28_in_ruleAnnotation2937); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getAnnotationAccess().getHyphenMinusHyphenMinusExclamationMarkKeyword_0());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1444:1: ( (lv_identifier_1_0= ruleBodyAnnotationIdentifier ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1445:1: (lv_identifier_1_0= ruleBodyAnnotationIdentifier )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1445:1: (lv_identifier_1_0= ruleBodyAnnotationIdentifier )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1446:3: lv_identifier_1_0= ruleBodyAnnotationIdentifier
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAnnotationAccess().getIdentifierBodyAnnotationIdentifierParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBodyAnnotationIdentifier_in_ruleAnnotation2958);
            lv_identifier_1_0=ruleBodyAnnotationIdentifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
              	        }
                     		set(
                     			current, 
                     			"identifier",
                      		lv_identifier_1_0, 
                      		"BodyAnnotationIdentifier");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,29,FOLLOW_29_in_ruleAnnotation2970); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getAnnotationAccess().getColonKeyword_2());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1466:1: ( (lv_expression_3_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1467:1: (lv_expression_3_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1467:1: (lv_expression_3_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1468:3: lv_expression_3_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAnnotationAccess().getExpressionFbyExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleAnnotation2991);
            lv_expression_3_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_3_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleAnnotation3003); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getAnnotationAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotation"


    // $ANTLR start "entryRulePreTraceAnnotation"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1496:1: entryRulePreTraceAnnotation returns [EObject current=null] : iv_rulePreTraceAnnotation= rulePreTraceAnnotation EOF ;
    public final EObject entryRulePreTraceAnnotation() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePreTraceAnnotation = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1497:2: (iv_rulePreTraceAnnotation= rulePreTraceAnnotation EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1498:2: iv_rulePreTraceAnnotation= rulePreTraceAnnotation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPreTraceAnnotationRule()); 
            }
            pushFollow(FOLLOW_rulePreTraceAnnotation_in_entryRulePreTraceAnnotation3039);
            iv_rulePreTraceAnnotation=rulePreTraceAnnotation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePreTraceAnnotation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePreTraceAnnotation3049); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePreTraceAnnotation"


    // $ANTLR start "rulePreTraceAnnotation"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1505:1: rulePreTraceAnnotation returns [EObject current=null] : (otherlv_0= '--Trace' ( (lv_type_1_0= ruleTraceAnnotationElementType ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) otherlv_4= ';' ) ;
    public final EObject rulePreTraceAnnotation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_type_1_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1508:28: ( (otherlv_0= '--Trace' ( (lv_type_1_0= ruleTraceAnnotationElementType ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) otherlv_4= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1509:1: (otherlv_0= '--Trace' ( (lv_type_1_0= ruleTraceAnnotationElementType ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) otherlv_4= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1509:1: (otherlv_0= '--Trace' ( (lv_type_1_0= ruleTraceAnnotationElementType ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) otherlv_4= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1509:3: otherlv_0= '--Trace' ( (lv_type_1_0= ruleTraceAnnotationElementType ) ) otherlv_2= ':' ( (lv_value_3_0= RULE_STRING ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_30_in_rulePreTraceAnnotation3086); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getPreTraceAnnotationAccess().getTraceKeyword_0());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1513:1: ( (lv_type_1_0= ruleTraceAnnotationElementType ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1514:1: (lv_type_1_0= ruleTraceAnnotationElementType )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1514:1: (lv_type_1_0= ruleTraceAnnotationElementType )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1515:3: lv_type_1_0= ruleTraceAnnotationElementType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getPreTraceAnnotationAccess().getTypeTraceAnnotationElementTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleTraceAnnotationElementType_in_rulePreTraceAnnotation3107);
            lv_type_1_0=ruleTraceAnnotationElementType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getPreTraceAnnotationRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_1_0, 
                      		"TraceAnnotationElementType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,29,FOLLOW_29_in_rulePreTraceAnnotation3119); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getPreTraceAnnotationAccess().getColonKeyword_2());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1535:1: ( (lv_value_3_0= RULE_STRING ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1536:1: (lv_value_3_0= RULE_STRING )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1536:1: (lv_value_3_0= RULE_STRING )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1537:3: lv_value_3_0= RULE_STRING
            {
            lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rulePreTraceAnnotation3136); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_3_0, grammarAccess.getPreTraceAnnotationAccess().getValueSTRINGTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getPreTraceAnnotationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_3_0, 
                      		"STRING");
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,14,FOLLOW_14_in_rulePreTraceAnnotation3153); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getPreTraceAnnotationAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePreTraceAnnotation"


    // $ANTLR start "entryRuleTraceAnnotationElementType"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1565:1: entryRuleTraceAnnotationElementType returns [String current=null] : iv_ruleTraceAnnotationElementType= ruleTraceAnnotationElementType EOF ;
    public final String entryRuleTraceAnnotationElementType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTraceAnnotationElementType = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1566:2: (iv_ruleTraceAnnotationElementType= ruleTraceAnnotationElementType EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1567:2: iv_ruleTraceAnnotationElementType= ruleTraceAnnotationElementType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTraceAnnotationElementTypeRule()); 
            }
            pushFollow(FOLLOW_ruleTraceAnnotationElementType_in_entryRuleTraceAnnotationElementType3190);
            iv_ruleTraceAnnotationElementType=ruleTraceAnnotationElementType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTraceAnnotationElementType.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTraceAnnotationElementType3201); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTraceAnnotationElementType"


    // $ANTLR start "ruleTraceAnnotationElementType"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1574:1: ruleTraceAnnotationElementType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'BLOCK' | kw= 'SIGNAL' | kw= 'PARAMETER' | kw= 'PORT' ) ;
    public final AntlrDatatypeRuleToken ruleTraceAnnotationElementType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1577:28: ( (kw= 'BLOCK' | kw= 'SIGNAL' | kw= 'PARAMETER' | kw= 'PORT' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1578:1: (kw= 'BLOCK' | kw= 'SIGNAL' | kw= 'PARAMETER' | kw= 'PORT' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1578:1: (kw= 'BLOCK' | kw= 'SIGNAL' | kw= 'PARAMETER' | kw= 'PORT' )
            int alt30=4;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt30=1;
                }
                break;
            case 32:
                {
                alt30=2;
                }
                break;
            case 33:
                {
                alt30=3;
                }
                break;
            case 34:
                {
                alt30=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1579:2: kw= 'BLOCK'
                    {
                    kw=(Token)match(input,31,FOLLOW_31_in_ruleTraceAnnotationElementType3239); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getTraceAnnotationElementTypeAccess().getBLOCKKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1586:2: kw= 'SIGNAL'
                    {
                    kw=(Token)match(input,32,FOLLOW_32_in_ruleTraceAnnotationElementType3258); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getTraceAnnotationElementTypeAccess().getSIGNALKeyword_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1593:2: kw= 'PARAMETER'
                    {
                    kw=(Token)match(input,33,FOLLOW_33_in_ruleTraceAnnotationElementType3277); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getTraceAnnotationElementTypeAccess().getPARAMETERKeyword_2()); 
                          
                    }

                    }
                    break;
                case 4 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1600:2: kw= 'PORT'
                    {
                    kw=(Token)match(input,34,FOLLOW_34_in_ruleTraceAnnotationElementType3296); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getTraceAnnotationElementTypeAccess().getPORTKeyword_3()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTraceAnnotationElementType"


    // $ANTLR start "entryRuleBodyAnnotationIdentifier"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1613:1: entryRuleBodyAnnotationIdentifier returns [String current=null] : iv_ruleBodyAnnotationIdentifier= ruleBodyAnnotationIdentifier EOF ;
    public final String entryRuleBodyAnnotationIdentifier() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBodyAnnotationIdentifier = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1614:2: (iv_ruleBodyAnnotationIdentifier= ruleBodyAnnotationIdentifier EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1615:2: iv_ruleBodyAnnotationIdentifier= ruleBodyAnnotationIdentifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBodyAnnotationIdentifierRule()); 
            }
            pushFollow(FOLLOW_ruleBodyAnnotationIdentifier_in_entryRuleBodyAnnotationIdentifier3337);
            iv_ruleBodyAnnotationIdentifier=ruleBodyAnnotationIdentifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBodyAnnotationIdentifier.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBodyAnnotationIdentifier3348); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBodyAnnotationIdentifier"


    // $ANTLR start "ruleBodyAnnotationIdentifier"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1622:1: ruleBodyAnnotationIdentifier returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'MAIN' | this_AnnotationIdentifier_1= ruleAnnotationIdentifier ) ;
    public final AntlrDatatypeRuleToken ruleBodyAnnotationIdentifier() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_AnnotationIdentifier_1 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1625:28: ( (kw= 'MAIN' | this_AnnotationIdentifier_1= ruleAnnotationIdentifier ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1626:1: (kw= 'MAIN' | this_AnnotationIdentifier_1= ruleAnnotationIdentifier )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1626:1: (kw= 'MAIN' | this_AnnotationIdentifier_1= ruleAnnotationIdentifier )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==35) ) {
                alt31=1;
            }
            else if ( (LA31_0==36) ) {
                alt31=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1627:2: kw= 'MAIN'
                    {
                    kw=(Token)match(input,35,FOLLOW_35_in_ruleBodyAnnotationIdentifier3386); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getBodyAnnotationIdentifierAccess().getMAINKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1634:5: this_AnnotationIdentifier_1= ruleAnnotationIdentifier
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBodyAnnotationIdentifierAccess().getAnnotationIdentifierParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleAnnotationIdentifier_in_ruleBodyAnnotationIdentifier3414);
                    this_AnnotationIdentifier_1=ruleAnnotationIdentifier();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_AnnotationIdentifier_1);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBodyAnnotationIdentifier"


    // $ANTLR start "entryRuleAnnotationIdentifier"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1652:1: entryRuleAnnotationIdentifier returns [String current=null] : iv_ruleAnnotationIdentifier= ruleAnnotationIdentifier EOF ;
    public final String entryRuleAnnotationIdentifier() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAnnotationIdentifier = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1653:2: (iv_ruleAnnotationIdentifier= ruleAnnotationIdentifier EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1654:2: iv_ruleAnnotationIdentifier= ruleAnnotationIdentifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAnnotationIdentifierRule()); 
            }
            pushFollow(FOLLOW_ruleAnnotationIdentifier_in_entryRuleAnnotationIdentifier3460);
            iv_ruleAnnotationIdentifier=ruleAnnotationIdentifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAnnotationIdentifier.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnnotationIdentifier3471); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationIdentifier"


    // $ANTLR start "ruleAnnotationIdentifier"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1661:1: ruleAnnotationIdentifier returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '/' (this_ID_1= RULE_ID kw= '/' )* ) ;
    public final AntlrDatatypeRuleToken ruleAnnotationIdentifier() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1664:28: ( (kw= '/' (this_ID_1= RULE_ID kw= '/' )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1665:1: (kw= '/' (this_ID_1= RULE_ID kw= '/' )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1665:1: (kw= '/' (this_ID_1= RULE_ID kw= '/' )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1666:2: kw= '/' (this_ID_1= RULE_ID kw= '/' )*
            {
            kw=(Token)match(input,36,FOLLOW_36_in_ruleAnnotationIdentifier3509); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current.merge(kw);
                      newLeafNode(kw, grammarAccess.getAnnotationIdentifierAccess().getSolidusKeyword_0()); 
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1671:1: (this_ID_1= RULE_ID kw= '/' )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==RULE_ID) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1671:6: this_ID_1= RULE_ID kw= '/'
            	    {
            	    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAnnotationIdentifier3525); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_1);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_1, grammarAccess.getAnnotationIdentifierAccess().getIDTerminalRuleCall_1_0()); 
            	          
            	    }
            	    kw=(Token)match(input,36,FOLLOW_36_in_ruleAnnotationIdentifier3543); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getAnnotationIdentifierAccess().getSolidusKeyword_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationIdentifier"


    // $ANTLR start "entryRuleEquation"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1692:1: entryRuleEquation returns [EObject current=null] : iv_ruleEquation= ruleEquation EOF ;
    public final EObject entryRuleEquation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEquation = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1693:2: (iv_ruleEquation= ruleEquation EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1694:2: iv_ruleEquation= ruleEquation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEquationRule()); 
            }
            pushFollow(FOLLOW_ruleEquation_in_entryRuleEquation3585);
            iv_ruleEquation=ruleEquation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEquation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEquation3595); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEquation"


    // $ANTLR start "ruleEquation"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1701:1: ruleEquation returns [EObject current=null] : ( () ( (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation ) )? ( ( (lv_leftPart_2_0= ruleLeftVariables ) ) | (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' ) ) otherlv_6= '=' ( (lv_expression_7_0= ruleFbyExpression ) ) otherlv_8= ';' ) ;
    public final EObject ruleEquation() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_preTraceAnnotation_1_0 = null;

        EObject lv_leftPart_2_0 = null;

        EObject lv_leftPart_4_0 = null;

        EObject lv_expression_7_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1704:28: ( ( () ( (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation ) )? ( ( (lv_leftPart_2_0= ruleLeftVariables ) ) | (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' ) ) otherlv_6= '=' ( (lv_expression_7_0= ruleFbyExpression ) ) otherlv_8= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1705:1: ( () ( (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation ) )? ( ( (lv_leftPart_2_0= ruleLeftVariables ) ) | (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' ) ) otherlv_6= '=' ( (lv_expression_7_0= ruleFbyExpression ) ) otherlv_8= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1705:1: ( () ( (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation ) )? ( ( (lv_leftPart_2_0= ruleLeftVariables ) ) | (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' ) ) otherlv_6= '=' ( (lv_expression_7_0= ruleFbyExpression ) ) otherlv_8= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1705:2: () ( (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation ) )? ( ( (lv_leftPart_2_0= ruleLeftVariables ) ) | (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' ) ) otherlv_6= '=' ( (lv_expression_7_0= ruleFbyExpression ) ) otherlv_8= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1705:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1706:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getEquationAccess().getEquationAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1711:2: ( (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==30) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1712:1: (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1712:1: (lv_preTraceAnnotation_1_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1713:3: lv_preTraceAnnotation_1_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEquationAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleEquation3650);
                    lv_preTraceAnnotation_1_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEquationRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_1_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1729:3: ( ( (lv_leftPart_2_0= ruleLeftVariables ) ) | (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' ) )
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==RULE_ID) ) {
                alt34=1;
            }
            else if ( (LA34_0==17) ) {
                alt34=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }
            switch (alt34) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1729:4: ( (lv_leftPart_2_0= ruleLeftVariables ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1729:4: ( (lv_leftPart_2_0= ruleLeftVariables ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1730:1: (lv_leftPart_2_0= ruleLeftVariables )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1730:1: (lv_leftPart_2_0= ruleLeftVariables )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1731:3: lv_leftPart_2_0= ruleLeftVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEquationAccess().getLeftPartLeftVariablesParserRuleCall_2_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLeftVariables_in_ruleEquation3673);
                    lv_leftPart_2_0=ruleLeftVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEquationRule());
                      	        }
                             		set(
                             			current, 
                             			"leftPart",
                              		lv_leftPart_2_0, 
                              		"LeftVariables");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1748:6: (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1748:6: (otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1748:8: otherlv_3= '(' ( (lv_leftPart_4_0= ruleLeftVariables ) ) otherlv_5= ')'
                    {
                    otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleEquation3692); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getEquationAccess().getLeftParenthesisKeyword_2_1_0());
                          
                    }
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1752:1: ( (lv_leftPart_4_0= ruleLeftVariables ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1753:1: (lv_leftPart_4_0= ruleLeftVariables )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1753:1: (lv_leftPart_4_0= ruleLeftVariables )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1754:3: lv_leftPart_4_0= ruleLeftVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getEquationAccess().getLeftPartLeftVariablesParserRuleCall_2_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLeftVariables_in_ruleEquation3713);
                    lv_leftPart_4_0=ruleLeftVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getEquationRule());
                      	        }
                             		set(
                             			current, 
                             			"leftPart",
                              		lv_leftPart_4_0, 
                              		"LeftVariables");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,18,FOLLOW_18_in_ruleEquation3725); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getEquationAccess().getRightParenthesisKeyword_2_1_2());
                          
                    }

                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,26,FOLLOW_26_in_ruleEquation3739); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getEquationAccess().getEqualsSignKeyword_3());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1778:1: ( (lv_expression_7_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1779:1: (lv_expression_7_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1779:1: (lv_expression_7_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1780:3: lv_expression_7_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEquationAccess().getExpressionFbyExpressionParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleEquation3760);
            lv_expression_7_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEquationRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_7_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_8=(Token)match(input,14,FOLLOW_14_in_ruleEquation3772); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getEquationAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEquation"


    // $ANTLR start "entryRuleAssert"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1808:1: entryRuleAssert returns [EObject current=null] : iv_ruleAssert= ruleAssert EOF ;
    public final EObject entryRuleAssert() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssert = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1809:2: (iv_ruleAssert= ruleAssert EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1810:2: iv_ruleAssert= ruleAssert EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssertRule()); 
            }
            pushFollow(FOLLOW_ruleAssert_in_entryRuleAssert3808);
            iv_ruleAssert=ruleAssert();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssert; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAssert3818); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssert"


    // $ANTLR start "ruleAssert"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1817:1: ruleAssert returns [EObject current=null] : ( () otherlv_1= 'assert' ( (lv_expression_2_0= ruleFbyExpression ) ) otherlv_3= ';' ) ;
    public final EObject ruleAssert() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1820:28: ( ( () otherlv_1= 'assert' ( (lv_expression_2_0= ruleFbyExpression ) ) otherlv_3= ';' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1821:1: ( () otherlv_1= 'assert' ( (lv_expression_2_0= ruleFbyExpression ) ) otherlv_3= ';' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1821:1: ( () otherlv_1= 'assert' ( (lv_expression_2_0= ruleFbyExpression ) ) otherlv_3= ';' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1821:2: () otherlv_1= 'assert' ( (lv_expression_2_0= ruleFbyExpression ) ) otherlv_3= ';'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1821:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1822:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getAssertAccess().getAssertAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,37,FOLLOW_37_in_ruleAssert3864); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAssertAccess().getAssertKeyword_1());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1831:1: ( (lv_expression_2_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1832:1: (lv_expression_2_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1832:1: (lv_expression_2_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1833:3: lv_expression_2_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAssertAccess().getExpressionFbyExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleAssert3885);
            lv_expression_2_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAssertRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleAssert3897); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getAssertAccess().getSemicolonKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssert"


    // $ANTLR start "entryRuleLeftVariables"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1861:1: entryRuleLeftVariables returns [EObject current=null] : iv_ruleLeftVariables= ruleLeftVariables EOF ;
    public final EObject entryRuleLeftVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLeftVariables = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1862:2: (iv_ruleLeftVariables= ruleLeftVariables EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1863:2: iv_ruleLeftVariables= ruleLeftVariables EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLeftVariablesRule()); 
            }
            pushFollow(FOLLOW_ruleLeftVariables_in_entryRuleLeftVariables3933);
            iv_ruleLeftVariables=ruleLeftVariables();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLeftVariables; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLeftVariables3943); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLeftVariables"


    // $ANTLR start "ruleLeftVariables"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1870:1: ruleLeftVariables returns [EObject current=null] : ( ( (lv_variables_0_0= ruleVariableCall ) ) (otherlv_1= ',' ( (lv_variables_2_0= ruleVariableCall ) ) )* ) ;
    public final EObject ruleLeftVariables() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_variables_0_0 = null;

        EObject lv_variables_2_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1873:28: ( ( ( (lv_variables_0_0= ruleVariableCall ) ) (otherlv_1= ',' ( (lv_variables_2_0= ruleVariableCall ) ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1874:1: ( ( (lv_variables_0_0= ruleVariableCall ) ) (otherlv_1= ',' ( (lv_variables_2_0= ruleVariableCall ) ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1874:1: ( ( (lv_variables_0_0= ruleVariableCall ) ) (otherlv_1= ',' ( (lv_variables_2_0= ruleVariableCall ) ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1874:2: ( (lv_variables_0_0= ruleVariableCall ) ) (otherlv_1= ',' ( (lv_variables_2_0= ruleVariableCall ) ) )*
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1874:2: ( (lv_variables_0_0= ruleVariableCall ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1875:1: (lv_variables_0_0= ruleVariableCall )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1875:1: (lv_variables_0_0= ruleVariableCall )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1876:3: lv_variables_0_0= ruleVariableCall
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLeftVariablesAccess().getVariablesVariableCallParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariableCall_in_ruleLeftVariables3989);
            lv_variables_0_0=ruleVariableCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLeftVariablesRule());
              	        }
                     		add(
                     			current, 
                     			"variables",
                      		lv_variables_0_0, 
                      		"VariableCall");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1892:2: (otherlv_1= ',' ( (lv_variables_2_0= ruleVariableCall ) ) )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==38) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1892:4: otherlv_1= ',' ( (lv_variables_2_0= ruleVariableCall ) )
            	    {
            	    otherlv_1=(Token)match(input,38,FOLLOW_38_in_ruleLeftVariables4002); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getLeftVariablesAccess().getCommaKeyword_1_0());
            	          
            	    }
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1896:1: ( (lv_variables_2_0= ruleVariableCall ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1897:1: (lv_variables_2_0= ruleVariableCall )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1897:1: (lv_variables_2_0= ruleVariableCall )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1898:3: lv_variables_2_0= ruleVariableCall
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getLeftVariablesAccess().getVariablesVariableCallParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleVariableCall_in_ruleLeftVariables4023);
            	    lv_variables_2_0=ruleVariableCall();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getLeftVariablesRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"variables",
            	              		lv_variables_2_0, 
            	              		"VariableCall");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLeftVariables"


    // $ANTLR start "entryRuleVariablesList"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1922:1: entryRuleVariablesList returns [EObject current=null] : iv_ruleVariablesList= ruleVariablesList EOF ;
    public final EObject entryRuleVariablesList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariablesList = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1923:2: (iv_ruleVariablesList= ruleVariablesList EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1924:2: iv_ruleVariablesList= ruleVariablesList EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariablesListRule()); 
            }
            pushFollow(FOLLOW_ruleVariablesList_in_entryRuleVariablesList4061);
            iv_ruleVariablesList=ruleVariablesList();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariablesList; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariablesList4071); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariablesList"


    // $ANTLR start "ruleVariablesList"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1931:1: ruleVariablesList returns [EObject current=null] : ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_const_1_0= 'const' ) )? ( (lv_variables_2_0= ruleVariable ) ) (otherlv_3= ',' ( (lv_variables_4_0= ruleVariable ) ) )* otherlv_5= ':' ( (lv_type_6_0= ruleDataType ) ) ) ;
    public final EObject ruleVariablesList() throws RecognitionException {
        EObject current = null;

        Token lv_const_1_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_preTraceAnnotation_0_0 = null;

        EObject lv_variables_2_0 = null;

        EObject lv_variables_4_0 = null;

        EObject lv_type_6_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1934:28: ( ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_const_1_0= 'const' ) )? ( (lv_variables_2_0= ruleVariable ) ) (otherlv_3= ',' ( (lv_variables_4_0= ruleVariable ) ) )* otherlv_5= ':' ( (lv_type_6_0= ruleDataType ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1935:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_const_1_0= 'const' ) )? ( (lv_variables_2_0= ruleVariable ) ) (otherlv_3= ',' ( (lv_variables_4_0= ruleVariable ) ) )* otherlv_5= ':' ( (lv_type_6_0= ruleDataType ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1935:1: ( ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_const_1_0= 'const' ) )? ( (lv_variables_2_0= ruleVariable ) ) (otherlv_3= ',' ( (lv_variables_4_0= ruleVariable ) ) )* otherlv_5= ':' ( (lv_type_6_0= ruleDataType ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1935:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )? ( (lv_const_1_0= 'const' ) )? ( (lv_variables_2_0= ruleVariable ) ) (otherlv_3= ',' ( (lv_variables_4_0= ruleVariable ) ) )* otherlv_5= ':' ( (lv_type_6_0= ruleDataType ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1935:2: ( (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==30) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1936:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1936:1: (lv_preTraceAnnotation_0_0= rulePreTraceAnnotation )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1937:3: lv_preTraceAnnotation_0_0= rulePreTraceAnnotation
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariablesListAccess().getPreTraceAnnotationPreTraceAnnotationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePreTraceAnnotation_in_ruleVariablesList4117);
                    lv_preTraceAnnotation_0_0=rulePreTraceAnnotation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariablesListRule());
                      	        }
                             		set(
                             			current, 
                             			"preTraceAnnotation",
                              		lv_preTraceAnnotation_0_0, 
                              		"PreTraceAnnotation");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1953:3: ( (lv_const_1_0= 'const' ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==25) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1954:1: (lv_const_1_0= 'const' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1954:1: (lv_const_1_0= 'const' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1955:3: lv_const_1_0= 'const'
                    {
                    lv_const_1_0=(Token)match(input,25,FOLLOW_25_in_ruleVariablesList4136); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_const_1_0, grammarAccess.getVariablesListAccess().getConstConstKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getVariablesListRule());
                      	        }
                             		setWithLastConsumed(current, "const", true, "const");
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1968:3: ( (lv_variables_2_0= ruleVariable ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1969:1: (lv_variables_2_0= ruleVariable )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1969:1: (lv_variables_2_0= ruleVariable )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1970:3: lv_variables_2_0= ruleVariable
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariablesListAccess().getVariablesVariableParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariable_in_ruleVariablesList4171);
            lv_variables_2_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariablesListRule());
              	        }
                     		add(
                     			current, 
                     			"variables",
                      		lv_variables_2_0, 
                      		"Variable");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1986:2: (otherlv_3= ',' ( (lv_variables_4_0= ruleVariable ) ) )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==38) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1986:4: otherlv_3= ',' ( (lv_variables_4_0= ruleVariable ) )
            	    {
            	    otherlv_3=(Token)match(input,38,FOLLOW_38_in_ruleVariablesList4184); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getVariablesListAccess().getCommaKeyword_3_0());
            	          
            	    }
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1990:1: ( (lv_variables_4_0= ruleVariable ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1991:1: (lv_variables_4_0= ruleVariable )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1991:1: (lv_variables_4_0= ruleVariable )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:1992:3: lv_variables_4_0= ruleVariable
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getVariablesListAccess().getVariablesVariableParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleVariable_in_ruleVariablesList4205);
            	    lv_variables_4_0=ruleVariable();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getVariablesListRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"variables",
            	              		lv_variables_4_0, 
            	              		"Variable");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

            otherlv_5=(Token)match(input,29,FOLLOW_29_in_ruleVariablesList4219); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getVariablesListAccess().getColonKeyword_4());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2012:1: ( (lv_type_6_0= ruleDataType ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2013:1: (lv_type_6_0= ruleDataType )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2013:1: (lv_type_6_0= ruleDataType )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2014:3: lv_type_6_0= ruleDataType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariablesListAccess().getTypeDataTypeParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleDataType_in_ruleVariablesList4240);
            lv_type_6_0=ruleDataType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariablesListRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_6_0, 
                      		"DataType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariablesList"


    // $ANTLR start "entryRuleVariableCall"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2038:1: entryRuleVariableCall returns [EObject current=null] : iv_ruleVariableCall= ruleVariableCall EOF ;
    public final EObject entryRuleVariableCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableCall = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2039:2: (iv_ruleVariableCall= ruleVariableCall EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2040:2: iv_ruleVariableCall= ruleVariableCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableCallRule()); 
            }
            pushFollow(FOLLOW_ruleVariableCall_in_entryRuleVariableCall4276);
            iv_ruleVariableCall=ruleVariableCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableCall; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariableCall4286); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableCall"


    // $ANTLR start "ruleVariableCall"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2047:1: ruleVariableCall returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_dim_2_0= ruleLUS_INT ) ) otherlv_3= ']' (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )? )? ) ;
    public final EObject ruleVariableCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_dim_2_0 = null;

        AntlrDatatypeRuleToken lv_dim_5_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2050:28: ( ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_dim_2_0= ruleLUS_INT ) ) otherlv_3= ']' (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )? )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2051:1: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_dim_2_0= ruleLUS_INT ) ) otherlv_3= ']' (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )? )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2051:1: ( ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_dim_2_0= ruleLUS_INT ) ) otherlv_3= ']' (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )? )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2051:2: ( (otherlv_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_dim_2_0= ruleLUS_INT ) ) otherlv_3= ']' (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )? )?
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2051:2: ( (otherlv_0= RULE_ID ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2052:1: (otherlv_0= RULE_ID )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2052:1: (otherlv_0= RULE_ID )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2053:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableCallRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVariableCall4331); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableCallAccess().getVarVariableCrossReference_0_0()); 
              	
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2064:2: (otherlv_1= '[' ( (lv_dim_2_0= ruleLUS_INT ) ) otherlv_3= ']' (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )? )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==39) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2064:4: otherlv_1= '[' ( (lv_dim_2_0= ruleLUS_INT ) ) otherlv_3= ']' (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )?
                    {
                    otherlv_1=(Token)match(input,39,FOLLOW_39_in_ruleVariableCall4344); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getVariableCallAccess().getLeftSquareBracketKeyword_1_0());
                          
                    }
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2068:1: ( (lv_dim_2_0= ruleLUS_INT ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2069:1: (lv_dim_2_0= ruleLUS_INT )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2069:1: (lv_dim_2_0= ruleLUS_INT )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2070:3: lv_dim_2_0= ruleLUS_INT
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableCallAccess().getDimLUS_INTParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLUS_INT_in_ruleVariableCall4365);
                    lv_dim_2_0=ruleLUS_INT();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableCallRule());
                      	        }
                             		add(
                             			current, 
                             			"dim",
                              		lv_dim_2_0, 
                              		"LUS_INT");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_3=(Token)match(input,40,FOLLOW_40_in_ruleVariableCall4377); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getVariableCallAccess().getRightSquareBracketKeyword_1_2());
                          
                    }
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2090:1: (otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']' )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==39) ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2090:3: otherlv_4= '[' ( (lv_dim_5_0= ruleLUS_INT ) ) otherlv_6= ']'
                            {
                            otherlv_4=(Token)match(input,39,FOLLOW_39_in_ruleVariableCall4390); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_4, grammarAccess.getVariableCallAccess().getLeftSquareBracketKeyword_1_3_0());
                                  
                            }
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2094:1: ( (lv_dim_5_0= ruleLUS_INT ) )
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2095:1: (lv_dim_5_0= ruleLUS_INT )
                            {
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2095:1: (lv_dim_5_0= ruleLUS_INT )
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2096:3: lv_dim_5_0= ruleLUS_INT
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getVariableCallAccess().getDimLUS_INTParserRuleCall_1_3_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleLUS_INT_in_ruleVariableCall4411);
                            lv_dim_5_0=ruleLUS_INT();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getVariableCallRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"dim",
                                      		lv_dim_5_0, 
                                      		"LUS_INT");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            otherlv_6=(Token)match(input,40,FOLLOW_40_in_ruleVariableCall4423); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_6, grammarAccess.getVariableCallAccess().getRightSquareBracketKeyword_1_3_2());
                                  
                            }

                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableCall"


    // $ANTLR start "entryRuleVariable"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2124:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2125:2: (iv_ruleVariable= ruleVariable EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2126:2: iv_ruleVariable= ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_ruleVariable_in_entryRuleVariable4463);
            iv_ruleVariable=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariable; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariable4473); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2133:1: ruleVariable returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2136:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2137:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2137:1: ( (lv_name_0_0= RULE_ID ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2138:1: (lv_name_0_0= RULE_ID )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2138:1: (lv_name_0_0= RULE_ID )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2139:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVariable4514); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleDataType"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2163:1: entryRuleDataType returns [EObject current=null] : iv_ruleDataType= ruleDataType EOF ;
    public final EObject entryRuleDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataType = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2164:2: (iv_ruleDataType= ruleDataType EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2165:2: iv_ruleDataType= ruleDataType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDataTypeRule()); 
            }
            pushFollow(FOLLOW_ruleDataType_in_entryRuleDataType4554);
            iv_ruleDataType=ruleDataType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDataType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDataType4564); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2172:1: ruleDataType returns [EObject current=null] : ( ( (lv_baseType_0_0= ruleBasicType ) ) (otherlv_1= '^' ( (lv_dim_2_0= ruleDataTypeDimValue ) ) (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )? )? ) ;
    public final EObject ruleDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_baseType_0_0 = null;

        EObject lv_dim_2_0 = null;

        EObject lv_dim_4_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2175:28: ( ( ( (lv_baseType_0_0= ruleBasicType ) ) (otherlv_1= '^' ( (lv_dim_2_0= ruleDataTypeDimValue ) ) (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )? )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2176:1: ( ( (lv_baseType_0_0= ruleBasicType ) ) (otherlv_1= '^' ( (lv_dim_2_0= ruleDataTypeDimValue ) ) (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )? )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2176:1: ( ( (lv_baseType_0_0= ruleBasicType ) ) (otherlv_1= '^' ( (lv_dim_2_0= ruleDataTypeDimValue ) ) (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )? )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2176:2: ( (lv_baseType_0_0= ruleBasicType ) ) (otherlv_1= '^' ( (lv_dim_2_0= ruleDataTypeDimValue ) ) (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )? )?
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2176:2: ( (lv_baseType_0_0= ruleBasicType ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2177:1: (lv_baseType_0_0= ruleBasicType )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2177:1: (lv_baseType_0_0= ruleBasicType )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2178:3: lv_baseType_0_0= ruleBasicType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getDataTypeAccess().getBaseTypeBasicTypeEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBasicType_in_ruleDataType4610);
            lv_baseType_0_0=ruleBasicType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getDataTypeRule());
              	        }
                     		set(
                     			current, 
                     			"baseType",
                      		lv_baseType_0_0, 
                      		"BasicType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2194:2: (otherlv_1= '^' ( (lv_dim_2_0= ruleDataTypeDimValue ) ) (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )? )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==41) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2194:4: otherlv_1= '^' ( (lv_dim_2_0= ruleDataTypeDimValue ) ) (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )?
                    {
                    otherlv_1=(Token)match(input,41,FOLLOW_41_in_ruleDataType4623); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getDataTypeAccess().getCircumflexAccentKeyword_1_0());
                          
                    }
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2198:1: ( (lv_dim_2_0= ruleDataTypeDimValue ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2199:1: (lv_dim_2_0= ruleDataTypeDimValue )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2199:1: (lv_dim_2_0= ruleDataTypeDimValue )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2200:3: lv_dim_2_0= ruleDataTypeDimValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDataTypeAccess().getDimDataTypeDimValueParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleDataTypeDimValue_in_ruleDataType4644);
                    lv_dim_2_0=ruleDataTypeDimValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDataTypeRule());
                      	        }
                             		add(
                             			current, 
                             			"dim",
                              		lv_dim_2_0, 
                              		"DataTypeDimValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2216:2: (otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) ) )?
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==41) ) {
                        alt41=1;
                    }
                    switch (alt41) {
                        case 1 :
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2216:4: otherlv_3= '^' ( (lv_dim_4_0= ruleDataTypeDimValue ) )
                            {
                            otherlv_3=(Token)match(input,41,FOLLOW_41_in_ruleDataType4657); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_3, grammarAccess.getDataTypeAccess().getCircumflexAccentKeyword_1_2_0());
                                  
                            }
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2220:1: ( (lv_dim_4_0= ruleDataTypeDimValue ) )
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2221:1: (lv_dim_4_0= ruleDataTypeDimValue )
                            {
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2221:1: (lv_dim_4_0= ruleDataTypeDimValue )
                            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2222:3: lv_dim_4_0= ruleDataTypeDimValue
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getDataTypeAccess().getDimDataTypeDimValueParserRuleCall_1_2_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleDataTypeDimValue_in_ruleDataType4678);
                            lv_dim_4_0=ruleDataTypeDimValue();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getDataTypeRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"dim",
                                      		lv_dim_4_0, 
                                      		"DataTypeDimValue");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleDataTypeDimValue"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2246:1: entryRuleDataTypeDimValue returns [EObject current=null] : iv_ruleDataTypeDimValue= ruleDataTypeDimValue EOF ;
    public final EObject entryRuleDataTypeDimValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataTypeDimValue = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2247:2: (iv_ruleDataTypeDimValue= ruleDataTypeDimValue EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2248:2: iv_ruleDataTypeDimValue= ruleDataTypeDimValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDataTypeDimValueRule()); 
            }
            pushFollow(FOLLOW_ruleDataTypeDimValue_in_entryRuleDataTypeDimValue4718);
            iv_ruleDataTypeDimValue=ruleDataTypeDimValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDataTypeDimValue; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDataTypeDimValue4728); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataTypeDimValue"


    // $ANTLR start "ruleDataTypeDimValue"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2255:1: ruleDataTypeDimValue returns [EObject current=null] : ( () ( ( (lv_value_1_0= ruleLUS_INT ) ) | ( (otherlv_2= RULE_ID ) ) ) ) ;
    public final EObject ruleDataTypeDimValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2258:28: ( ( () ( ( (lv_value_1_0= ruleLUS_INT ) ) | ( (otherlv_2= RULE_ID ) ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2259:1: ( () ( ( (lv_value_1_0= ruleLUS_INT ) ) | ( (otherlv_2= RULE_ID ) ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2259:1: ( () ( ( (lv_value_1_0= ruleLUS_INT ) ) | ( (otherlv_2= RULE_ID ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2259:2: () ( ( (lv_value_1_0= ruleLUS_INT ) ) | ( (otherlv_2= RULE_ID ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2259:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2260:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getDataTypeDimValueAccess().getDataTypeDimValueAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2265:2: ( ( (lv_value_1_0= ruleLUS_INT ) ) | ( (otherlv_2= RULE_ID ) ) )
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==RULE_INT) ) {
                alt43=1;
            }
            else if ( (LA43_0==RULE_ID) ) {
                alt43=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }
            switch (alt43) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2265:3: ( (lv_value_1_0= ruleLUS_INT ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2265:3: ( (lv_value_1_0= ruleLUS_INT ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2266:1: (lv_value_1_0= ruleLUS_INT )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2266:1: (lv_value_1_0= ruleLUS_INT )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2267:3: lv_value_1_0= ruleLUS_INT
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDataTypeDimValueAccess().getValueLUS_INTParserRuleCall_1_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLUS_INT_in_ruleDataTypeDimValue4784);
                    lv_value_1_0=ruleLUS_INT();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDataTypeDimValueRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_1_0, 
                              		"LUS_INT");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2284:6: ( (otherlv_2= RULE_ID ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2284:6: ( (otherlv_2= RULE_ID ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2285:1: (otherlv_2= RULE_ID )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2285:1: (otherlv_2= RULE_ID )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2286:3: otherlv_2= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getDataTypeDimValueRule());
                      	        }
                              
                    }
                    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDataTypeDimValue4810); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_2, grammarAccess.getDataTypeDimValueAccess().getRefVariableCrossReference_1_1_0()); 
                      	
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataTypeDimValue"


    // $ANTLR start "entryRuleFbyExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2305:1: entryRuleFbyExpression returns [EObject current=null] : iv_ruleFbyExpression= ruleFbyExpression EOF ;
    public final EObject entryRuleFbyExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFbyExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2306:2: (iv_ruleFbyExpression= ruleFbyExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2307:2: iv_ruleFbyExpression= ruleFbyExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFbyExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_entryRuleFbyExpression4847);
            iv_ruleFbyExpression=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFbyExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFbyExpression4857); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFbyExpression"


    // $ANTLR start "ruleFbyExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2314:1: ruleFbyExpression returns [EObject current=null] : (this_OrExpression_0= ruleOrExpression ( ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) ) )? ) ;
    public final EObject ruleFbyExpression() throws RecognitionException {
        EObject current = null;

        EObject this_OrExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2317:28: ( (this_OrExpression_0= ruleOrExpression ( ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) ) )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2318:1: (this_OrExpression_0= ruleOrExpression ( ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) ) )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2318:1: (this_OrExpression_0= ruleOrExpression ( ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) ) )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2319:5: this_OrExpression_0= ruleOrExpression ( ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getFbyExpressionAccess().getOrExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleOrExpression_in_ruleFbyExpression4904);
            this_OrExpression_0=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_OrExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2327:1: ( ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==52) ) {
                int LA44_1 = input.LA(2);

                if ( (synpred1_InternalLustre()) ) {
                    alt44=1;
                }
            }
            switch (alt44) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2327:2: ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2336:6: ( () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2336:7: () ( (lv_op_2_0= ruleFbyOp ) ) ( (lv_right_3_0= ruleFbyExpression ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2336:7: ()
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2337:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getFbyExpressionAccess().getFbyExpressionLeftAction_1_0_0(),
                                  current);
                          
                    }

                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2342:2: ( (lv_op_2_0= ruleFbyOp ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2343:1: (lv_op_2_0= ruleFbyOp )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2343:1: (lv_op_2_0= ruleFbyOp )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2344:3: lv_op_2_0= ruleFbyOp
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFbyExpressionAccess().getOpFbyOpEnumRuleCall_1_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleFbyOp_in_ruleFbyExpression4961);
                    lv_op_2_0=ruleFbyOp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFbyExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_2_0, 
                              		"FbyOp");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2360:2: ( (lv_right_3_0= ruleFbyExpression ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2361:1: (lv_right_3_0= ruleFbyExpression )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2361:1: (lv_right_3_0= ruleFbyExpression )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2362:3: lv_right_3_0= ruleFbyExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFbyExpressionAccess().getRightFbyExpressionParserRuleCall_1_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleFbyExpression_in_ruleFbyExpression4982);
                    lv_right_3_0=ruleFbyExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFbyExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"FbyExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFbyExpression"


    // $ANTLR start "entryRuleOrExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2386:1: entryRuleOrExpression returns [EObject current=null] : iv_ruleOrExpression= ruleOrExpression EOF ;
    public final EObject entryRuleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2387:2: (iv_ruleOrExpression= ruleOrExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2388:2: iv_ruleOrExpression= ruleOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleOrExpression_in_entryRuleOrExpression5021);
            iv_ruleOrExpression=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOrExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrExpression5031); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpression"


    // $ANTLR start "ruleOrExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2395:1: ruleOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) ) )* ) ;
    public final EObject ruleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AndExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2398:28: ( (this_AndExpression_0= ruleAndExpression ( ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2399:1: (this_AndExpression_0= ruleAndExpression ( ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2399:1: (this_AndExpression_0= ruleAndExpression ( ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2400:5: this_AndExpression_0= ruleAndExpression ( ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getOrExpressionAccess().getAndExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleAndExpression_in_ruleOrExpression5078);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AndExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2408:1: ( ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) ) )*
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( (LA45_0==53) ) {
                    int LA45_2 = input.LA(2);

                    if ( (synpred2_InternalLustre()) ) {
                        alt45=1;
                    }


                }
                else if ( (LA45_0==54) ) {
                    int LA45_3 = input.LA(2);

                    if ( (synpred2_InternalLustre()) ) {
                        alt45=1;
                    }


                }


                switch (alt45) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2408:2: ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2417:6: ( () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2417:7: () ( (lv_op_2_0= ruleOrOp ) ) ( (lv_right_3_0= ruleAndExpression ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2417:7: ()
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2418:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2423:2: ( (lv_op_2_0= ruleOrOp ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2424:1: (lv_op_2_0= ruleOrOp )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2424:1: (lv_op_2_0= ruleOrOp )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2425:3: lv_op_2_0= ruleOrOp
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getOrExpressionAccess().getOpOrOpEnumRuleCall_1_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOrOp_in_ruleOrExpression5135);
            	    lv_op_2_0=ruleOrOp();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getOrExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"op",
            	              		lv_op_2_0, 
            	              		"OrOp");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2441:2: ( (lv_right_3_0= ruleAndExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2442:1: (lv_right_3_0= ruleAndExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2442:1: (lv_right_3_0= ruleAndExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2443:3: lv_right_3_0= ruleAndExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getOrExpressionAccess().getRightAndExpressionParserRuleCall_1_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAndExpression_in_ruleOrExpression5156);
            	    lv_right_3_0=ruleAndExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getOrExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"right",
            	              		lv_right_3_0, 
            	              		"AndExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2467:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2468:2: (iv_ruleAndExpression= ruleAndExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2469:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleAndExpression_in_entryRuleAndExpression5195);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAndExpression5205); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2476:1: ruleAndExpression returns [EObject current=null] : (this_ImpliesExpression_0= ruleImpliesExpression ( ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )* ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ImpliesExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2479:28: ( (this_ImpliesExpression_0= ruleImpliesExpression ( ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2480:1: (this_ImpliesExpression_0= ruleImpliesExpression ( ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2480:1: (this_ImpliesExpression_0= ruleImpliesExpression ( ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2481:5: this_ImpliesExpression_0= ruleImpliesExpression ( ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAndExpressionAccess().getImpliesExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleImpliesExpression_in_ruleAndExpression5252);
            this_ImpliesExpression_0=ruleImpliesExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ImpliesExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2489:1: ( ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )*
            loop46:
            do {
                int alt46=2;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==55) ) {
                    int LA46_2 = input.LA(2);

                    if ( (synpred3_InternalLustre()) ) {
                        alt46=1;
                    }


                }


                switch (alt46) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2489:2: ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2498:6: ( () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2498:7: () ( (lv_op_2_0= ruleAndOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2498:7: ()
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2499:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2504:2: ( (lv_op_2_0= ruleAndOp ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2505:1: (lv_op_2_0= ruleAndOp )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2505:1: (lv_op_2_0= ruleAndOp )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2506:3: lv_op_2_0= ruleAndOp
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAndExpressionAccess().getOpAndOpEnumRuleCall_1_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAndOp_in_ruleAndExpression5309);
            	    lv_op_2_0=ruleAndOp();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAndExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"op",
            	              		lv_op_2_0, 
            	              		"AndOp");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2522:2: ( (lv_right_3_0= ruleImpliesExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2523:1: (lv_right_3_0= ruleImpliesExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2523:1: (lv_right_3_0= ruleImpliesExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2524:3: lv_right_3_0= ruleImpliesExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAndExpressionAccess().getRightImpliesExpressionParserRuleCall_1_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleImpliesExpression_in_ruleAndExpression5330);
            	    lv_right_3_0=ruleImpliesExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAndExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"right",
            	              		lv_right_3_0, 
            	              		"ImpliesExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop46;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleImpliesExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2548:1: entryRuleImpliesExpression returns [EObject current=null] : iv_ruleImpliesExpression= ruleImpliesExpression EOF ;
    public final EObject entryRuleImpliesExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImpliesExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2549:2: (iv_ruleImpliesExpression= ruleImpliesExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2550:2: iv_ruleImpliesExpression= ruleImpliesExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImpliesExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleImpliesExpression_in_entryRuleImpliesExpression5369);
            iv_ruleImpliesExpression=ruleImpliesExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImpliesExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleImpliesExpression5379); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImpliesExpression"


    // $ANTLR start "ruleImpliesExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2557:1: ruleImpliesExpression returns [EObject current=null] : (this_RelExpression_0= ruleRelExpression ( ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )? ) ;
    public final EObject ruleImpliesExpression() throws RecognitionException {
        EObject current = null;

        EObject this_RelExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2560:28: ( (this_RelExpression_0= ruleRelExpression ( ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2561:1: (this_RelExpression_0= ruleRelExpression ( ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2561:1: (this_RelExpression_0= ruleRelExpression ( ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2562:5: this_RelExpression_0= ruleRelExpression ( ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getImpliesExpressionAccess().getRelExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleRelExpression_in_ruleImpliesExpression5426);
            this_RelExpression_0=ruleRelExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2570:1: ( ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==56) ) {
                int LA47_1 = input.LA(2);

                if ( (synpred4_InternalLustre()) ) {
                    alt47=1;
                }
            }
            switch (alt47) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2570:2: ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2579:6: ( () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2579:7: () ( (lv_op_2_0= ruleImpliesOp ) ) ( (lv_right_3_0= ruleImpliesExpression ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2579:7: ()
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2580:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getImpliesExpressionAccess().getImpliesExpressionLeftAction_1_0_0(),
                                  current);
                          
                    }

                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2585:2: ( (lv_op_2_0= ruleImpliesOp ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2586:1: (lv_op_2_0= ruleImpliesOp )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2586:1: (lv_op_2_0= ruleImpliesOp )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2587:3: lv_op_2_0= ruleImpliesOp
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getImpliesExpressionAccess().getOpImpliesOpEnumRuleCall_1_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleImpliesOp_in_ruleImpliesExpression5483);
                    lv_op_2_0=ruleImpliesOp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getImpliesExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_2_0, 
                              		"ImpliesOp");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2603:2: ( (lv_right_3_0= ruleImpliesExpression ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2604:1: (lv_right_3_0= ruleImpliesExpression )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2604:1: (lv_right_3_0= ruleImpliesExpression )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2605:3: lv_right_3_0= ruleImpliesExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getImpliesExpressionAccess().getRightImpliesExpressionParserRuleCall_1_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleImpliesExpression_in_ruleImpliesExpression5504);
                    lv_right_3_0=ruleImpliesExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getImpliesExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"ImpliesExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImpliesExpression"


    // $ANTLR start "entryRuleRelExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2629:1: entryRuleRelExpression returns [EObject current=null] : iv_ruleRelExpression= ruleRelExpression EOF ;
    public final EObject entryRuleRelExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2630:2: (iv_ruleRelExpression= ruleRelExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2631:2: iv_ruleRelExpression= ruleRelExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleRelExpression_in_entryRuleRelExpression5543);
            iv_ruleRelExpression=ruleRelExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRelExpression5553); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelExpression"


    // $ANTLR start "ruleRelExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2638:1: ruleRelExpression returns [EObject current=null] : (this_NotExpression_0= ruleNotExpression ( ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) ) )? ) ;
    public final EObject ruleRelExpression() throws RecognitionException {
        EObject current = null;

        EObject this_NotExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2641:28: ( (this_NotExpression_0= ruleNotExpression ( ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) ) )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2642:1: (this_NotExpression_0= ruleNotExpression ( ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) ) )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2642:1: (this_NotExpression_0= ruleNotExpression ( ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) ) )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2643:5: this_NotExpression_0= ruleNotExpression ( ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelExpressionAccess().getNotExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleNotExpression_in_ruleRelExpression5600);
            this_NotExpression_0=ruleNotExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NotExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2651:1: ( ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) ) )?
            int alt48=2;
            switch ( input.LA(1) ) {
                case 26:
                    {
                    int LA48_1 = input.LA(2);

                    if ( (synpred5_InternalLustre()) ) {
                        alt48=1;
                    }
                    }
                    break;
                case 57:
                    {
                    int LA48_2 = input.LA(2);

                    if ( (synpred5_InternalLustre()) ) {
                        alt48=1;
                    }
                    }
                    break;
                case 58:
                    {
                    int LA48_3 = input.LA(2);

                    if ( (synpred5_InternalLustre()) ) {
                        alt48=1;
                    }
                    }
                    break;
                case 59:
                    {
                    int LA48_4 = input.LA(2);

                    if ( (synpred5_InternalLustre()) ) {
                        alt48=1;
                    }
                    }
                    break;
                case 60:
                    {
                    int LA48_5 = input.LA(2);

                    if ( (synpred5_InternalLustre()) ) {
                        alt48=1;
                    }
                    }
                    break;
                case 61:
                    {
                    int LA48_6 = input.LA(2);

                    if ( (synpred5_InternalLustre()) ) {
                        alt48=1;
                    }
                    }
                    break;
            }

            switch (alt48) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2651:2: ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2660:6: ( () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2660:7: () ( (lv_op_2_0= ruleRelOp ) ) ( (lv_right_3_0= ruleNotExpression ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2660:7: ()
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2661:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelExpressionAccess().getRelExpressionLeftAction_1_0_0(),
                                  current);
                          
                    }

                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2666:2: ( (lv_op_2_0= ruleRelOp ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2667:1: (lv_op_2_0= ruleRelOp )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2667:1: (lv_op_2_0= ruleRelOp )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2668:3: lv_op_2_0= ruleRelOp
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelExpressionAccess().getOpRelOpEnumRuleCall_1_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleRelOp_in_ruleRelExpression5657);
                    lv_op_2_0=ruleRelOp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_2_0, 
                              		"RelOp");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2684:2: ( (lv_right_3_0= ruleNotExpression ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2685:1: (lv_right_3_0= ruleNotExpression )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2685:1: (lv_right_3_0= ruleNotExpression )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2686:3: lv_right_3_0= ruleNotExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelExpressionAccess().getRightNotExpressionParserRuleCall_1_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleNotExpression_in_ruleRelExpression5678);
                    lv_right_3_0=ruleNotExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"NotExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelExpression"


    // $ANTLR start "entryRuleNotExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2710:1: entryRuleNotExpression returns [EObject current=null] : iv_ruleNotExpression= ruleNotExpression EOF ;
    public final EObject entryRuleNotExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2711:2: (iv_ruleNotExpression= ruleNotExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2712:2: iv_ruleNotExpression= ruleNotExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNotExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleNotExpression_in_entryRuleNotExpression5717);
            iv_ruleNotExpression=ruleNotExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNotExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNotExpression5727); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotExpression"


    // $ANTLR start "ruleNotExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2719:1: ruleNotExpression returns [EObject current=null] : (this_AddExpression_0= ruleAddExpression | ( () ( (lv_op_2_0= ruleNotOp ) ) ( (lv_uexp_3_0= ruleNotExpression ) ) ) ) ;
    public final EObject ruleNotExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AddExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_uexp_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2722:28: ( (this_AddExpression_0= ruleAddExpression | ( () ( (lv_op_2_0= ruleNotOp ) ) ( (lv_uexp_3_0= ruleNotExpression ) ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2723:1: (this_AddExpression_0= ruleAddExpression | ( () ( (lv_op_2_0= ruleNotOp ) ) ( (lv_uexp_3_0= ruleNotExpression ) ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2723:1: (this_AddExpression_0= ruleAddExpression | ( () ( (lv_op_2_0= ruleNotOp ) ) ( (lv_uexp_3_0= ruleNotExpression ) ) ) )
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( ((LA49_0>=RULE_ID && LA49_0<=RULE_INT)||LA49_0==17||LA49_0==39||LA49_0==42||LA49_0==64||(LA49_0>=70 && LA49_0<=71)) ) {
                alt49=1;
            }
            else if ( (LA49_0==62) ) {
                alt49=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2724:5: this_AddExpression_0= ruleAddExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNotExpressionAccess().getAddExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleAddExpression_in_ruleNotExpression5774);
                    this_AddExpression_0=ruleAddExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_AddExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2733:6: ( () ( (lv_op_2_0= ruleNotOp ) ) ( (lv_uexp_3_0= ruleNotExpression ) ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2733:6: ( () ( (lv_op_2_0= ruleNotOp ) ) ( (lv_uexp_3_0= ruleNotExpression ) ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2733:7: () ( (lv_op_2_0= ruleNotOp ) ) ( (lv_uexp_3_0= ruleNotExpression ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2733:7: ()
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2734:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getNotExpressionAccess().getNotExpressionAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2739:2: ( (lv_op_2_0= ruleNotOp ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2740:1: (lv_op_2_0= ruleNotOp )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2740:1: (lv_op_2_0= ruleNotOp )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2741:3: lv_op_2_0= ruleNotOp
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNotExpressionAccess().getOpNotOpEnumRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleNotOp_in_ruleNotExpression5810);
                    lv_op_2_0=ruleNotOp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNotExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_2_0, 
                              		"NotOp");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2757:2: ( (lv_uexp_3_0= ruleNotExpression ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2758:1: (lv_uexp_3_0= ruleNotExpression )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2758:1: (lv_uexp_3_0= ruleNotExpression )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2759:3: lv_uexp_3_0= ruleNotExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNotExpressionAccess().getUexpNotExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleNotExpression_in_ruleNotExpression5831);
                    lv_uexp_3_0=ruleNotExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNotExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"uexp",
                              		lv_uexp_3_0, 
                              		"NotExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotExpression"


    // $ANTLR start "entryRuleAddExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2783:1: entryRuleAddExpression returns [EObject current=null] : iv_ruleAddExpression= ruleAddExpression EOF ;
    public final EObject entryRuleAddExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAddExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2784:2: (iv_ruleAddExpression= ruleAddExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2785:2: iv_ruleAddExpression= ruleAddExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAddExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleAddExpression_in_entryRuleAddExpression5868);
            iv_ruleAddExpression=ruleAddExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAddExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAddExpression5878); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAddExpression"


    // $ANTLR start "ruleAddExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2792:1: ruleAddExpression returns [EObject current=null] : (this_MultExpression_0= ruleMultExpression ( ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) ) )* ) ;
    public final EObject ruleAddExpression() throws RecognitionException {
        EObject current = null;

        EObject this_MultExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2795:28: ( (this_MultExpression_0= ruleMultExpression ( ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2796:1: (this_MultExpression_0= ruleMultExpression ( ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2796:1: (this_MultExpression_0= ruleMultExpression ( ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2797:5: this_MultExpression_0= ruleMultExpression ( ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAddExpressionAccess().getMultExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleMultExpression_in_ruleAddExpression5925);
            this_MultExpression_0=ruleMultExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2805:1: ( ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) ) )*
            loop50:
            do {
                int alt50=2;
                int LA50_0 = input.LA(1);

                if ( (LA50_0==63) ) {
                    int LA50_2 = input.LA(2);

                    if ( (synpred6_InternalLustre()) ) {
                        alt50=1;
                    }


                }
                else if ( (LA50_0==64) ) {
                    int LA50_3 = input.LA(2);

                    if ( (synpred6_InternalLustre()) ) {
                        alt50=1;
                    }


                }


                switch (alt50) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2805:2: ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2814:6: ( () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2814:7: () ( (lv_op_2_0= ruleAddOp ) ) ( (lv_right_3_0= ruleMultExpression ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2814:7: ()
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2815:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getAddExpressionAccess().getAddExpressionLeftAction_1_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2820:2: ( (lv_op_2_0= ruleAddOp ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2821:1: (lv_op_2_0= ruleAddOp )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2821:1: (lv_op_2_0= ruleAddOp )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2822:3: lv_op_2_0= ruleAddOp
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAddExpressionAccess().getOpAddOpEnumRuleCall_1_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAddOp_in_ruleAddExpression5982);
            	    lv_op_2_0=ruleAddOp();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAddExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"op",
            	              		lv_op_2_0, 
            	              		"AddOp");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2838:2: ( (lv_right_3_0= ruleMultExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2839:1: (lv_right_3_0= ruleMultExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2839:1: (lv_right_3_0= ruleMultExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2840:3: lv_right_3_0= ruleMultExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAddExpressionAccess().getRightMultExpressionParserRuleCall_1_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleMultExpression_in_ruleAddExpression6003);
            	    lv_right_3_0=ruleMultExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAddExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"right",
            	              		lv_right_3_0, 
            	              		"MultExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop50;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAddExpression"


    // $ANTLR start "entryRuleMultExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2864:1: entryRuleMultExpression returns [EObject current=null] : iv_ruleMultExpression= ruleMultExpression EOF ;
    public final EObject entryRuleMultExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2865:2: (iv_ruleMultExpression= ruleMultExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2866:2: iv_ruleMultExpression= ruleMultExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleMultExpression_in_entryRuleMultExpression6042);
            iv_ruleMultExpression=ruleMultExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMultExpression6052); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultExpression"


    // $ANTLR start "ruleMultExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2873:1: ruleMultExpression returns [EObject current=null] : (this_WhenExpression_0= ruleWhenExpression ( ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) ) )* ) ;
    public final EObject ruleMultExpression() throws RecognitionException {
        EObject current = null;

        EObject this_WhenExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2876:28: ( (this_WhenExpression_0= ruleWhenExpression ( ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2877:1: (this_WhenExpression_0= ruleWhenExpression ( ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2877:1: (this_WhenExpression_0= ruleWhenExpression ( ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2878:5: this_WhenExpression_0= ruleWhenExpression ( ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultExpressionAccess().getWhenExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleWhenExpression_in_ruleMultExpression6099);
            this_WhenExpression_0=ruleWhenExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_WhenExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2886:1: ( ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) ) )*
            loop51:
            do {
                int alt51=2;
                switch ( input.LA(1) ) {
                case 65:
                    {
                    int LA51_2 = input.LA(2);

                    if ( (synpred7_InternalLustre()) ) {
                        alt51=1;
                    }


                    }
                    break;
                case 36:
                    {
                    int LA51_3 = input.LA(2);

                    if ( (synpred7_InternalLustre()) ) {
                        alt51=1;
                    }


                    }
                    break;
                case 66:
                    {
                    int LA51_4 = input.LA(2);

                    if ( (synpred7_InternalLustre()) ) {
                        alt51=1;
                    }


                    }
                    break;
                case 67:
                    {
                    int LA51_5 = input.LA(2);

                    if ( (synpred7_InternalLustre()) ) {
                        alt51=1;
                    }


                    }
                    break;

                }

                switch (alt51) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2886:2: ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2895:6: ( () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2895:7: () ( (lv_op_2_0= ruleMultOp ) ) ( (lv_right_3_0= ruleWhenExpression ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2895:7: ()
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2896:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getMultExpressionAccess().getMultExpressionLeftAction_1_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2901:2: ( (lv_op_2_0= ruleMultOp ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2902:1: (lv_op_2_0= ruleMultOp )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2902:1: (lv_op_2_0= ruleMultOp )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2903:3: lv_op_2_0= ruleMultOp
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMultExpressionAccess().getOpMultOpEnumRuleCall_1_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleMultOp_in_ruleMultExpression6156);
            	    lv_op_2_0=ruleMultOp();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMultExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"op",
            	              		lv_op_2_0, 
            	              		"MultOp");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2919:2: ( (lv_right_3_0= ruleWhenExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2920:1: (lv_right_3_0= ruleWhenExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2920:1: (lv_right_3_0= ruleWhenExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2921:3: lv_right_3_0= ruleWhenExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMultExpressionAccess().getRightWhenExpressionParserRuleCall_1_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleWhenExpression_in_ruleMultExpression6177);
            	    lv_right_3_0=ruleWhenExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMultExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"right",
            	              		lv_right_3_0, 
            	              		"WhenExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop51;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultExpression"


    // $ANTLR start "entryRuleWhenExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2945:1: entryRuleWhenExpression returns [EObject current=null] : iv_ruleWhenExpression= ruleWhenExpression EOF ;
    public final EObject entryRuleWhenExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhenExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2946:2: (iv_ruleWhenExpression= ruleWhenExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2947:2: iv_ruleWhenExpression= ruleWhenExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWhenExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleWhenExpression_in_entryRuleWhenExpression6216);
            iv_ruleWhenExpression=ruleWhenExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWhenExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhenExpression6226); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhenExpression"


    // $ANTLR start "ruleWhenExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2954:1: ruleWhenExpression returns [EObject current=null] : (this_NorExpression_0= ruleNorExpression ( ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) ) )* ) ;
    public final EObject ruleWhenExpression() throws RecognitionException {
        EObject current = null;

        EObject this_NorExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2957:28: ( (this_NorExpression_0= ruleNorExpression ( ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) ) )* ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2958:1: (this_NorExpression_0= ruleNorExpression ( ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) ) )* )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2958:1: (this_NorExpression_0= ruleNorExpression ( ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) ) )* )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2959:5: this_NorExpression_0= ruleNorExpression ( ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getWhenExpressionAccess().getNorExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleNorExpression_in_ruleWhenExpression6273);
            this_NorExpression_0=ruleNorExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NorExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2967:1: ( ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) ) )*
            loop52:
            do {
                int alt52=2;
                int LA52_0 = input.LA(1);

                if ( (LA52_0==68) ) {
                    int LA52_2 = input.LA(2);

                    if ( (synpred8_InternalLustre()) ) {
                        alt52=1;
                    }


                }


                switch (alt52) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2967:2: ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2976:6: ( () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2976:7: () ( (lv_op_2_0= ruleWhenOp ) ) ( (lv_right_3_0= ruleNorExpression ) )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2976:7: ()
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2977:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getWhenExpressionAccess().getWhenExpressionLeftAction_1_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2982:2: ( (lv_op_2_0= ruleWhenOp ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2983:1: (lv_op_2_0= ruleWhenOp )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2983:1: (lv_op_2_0= ruleWhenOp )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2984:3: lv_op_2_0= ruleWhenOp
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getWhenExpressionAccess().getOpWhenOpEnumRuleCall_1_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleWhenOp_in_ruleWhenExpression6330);
            	    lv_op_2_0=ruleWhenOp();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getWhenExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"op",
            	              		lv_op_2_0, 
            	              		"WhenOp");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3000:2: ( (lv_right_3_0= ruleNorExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3001:1: (lv_right_3_0= ruleNorExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3001:1: (lv_right_3_0= ruleNorExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3002:3: lv_right_3_0= ruleNorExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getWhenExpressionAccess().getRightNorExpressionParserRuleCall_1_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleNorExpression_in_ruleWhenExpression6351);
            	    lv_right_3_0=ruleNorExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getWhenExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"right",
            	              		lv_right_3_0, 
            	              		"NorExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop52;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhenExpression"


    // $ANTLR start "entryRuleNorExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3026:1: entryRuleNorExpression returns [EObject current=null] : iv_ruleNorExpression= ruleNorExpression EOF ;
    public final EObject entryRuleNorExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNorExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3027:2: (iv_ruleNorExpression= ruleNorExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3028:2: iv_ruleNorExpression= ruleNorExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNorExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleNorExpression_in_entryRuleNorExpression6390);
            iv_ruleNorExpression=ruleNorExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNorExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNorExpression6400); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNorExpression"


    // $ANTLR start "ruleNorExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3035:1: ruleNorExpression returns [EObject current=null] : (this_UnExpression_0= ruleUnExpression ( ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) ) )? ) ;
    public final EObject ruleNorExpression() throws RecognitionException {
        EObject current = null;

        EObject this_UnExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3038:28: ( (this_UnExpression_0= ruleUnExpression ( ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) ) )? ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3039:1: (this_UnExpression_0= ruleUnExpression ( ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) ) )? )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3039:1: (this_UnExpression_0= ruleUnExpression ( ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) ) )? )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3040:5: this_UnExpression_0= ruleUnExpression ( ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getNorExpressionAccess().getUnExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleUnExpression_in_ruleNorExpression6447);
            this_UnExpression_0=ruleUnExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_UnExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3048:1: ( ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==69) ) {
                int LA53_1 = input.LA(2);

                if ( (synpred9_InternalLustre()) ) {
                    alt53=1;
                }
            }
            switch (alt53) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3048:2: ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )=> ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3057:6: ( () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3057:7: () ( (lv_op_2_0= ruleNorOp ) ) ( (lv_right_3_0= ruleUnExpression ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3057:7: ()
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3058:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getNorExpressionAccess().getNorExpressionLeftAction_1_0_0(),
                                  current);
                          
                    }

                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3063:2: ( (lv_op_2_0= ruleNorOp ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3064:1: (lv_op_2_0= ruleNorOp )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3064:1: (lv_op_2_0= ruleNorOp )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3065:3: lv_op_2_0= ruleNorOp
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNorExpressionAccess().getOpNorOpEnumRuleCall_1_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleNorOp_in_ruleNorExpression6504);
                    lv_op_2_0=ruleNorOp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNorExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_2_0, 
                              		"NorOp");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3081:2: ( (lv_right_3_0= ruleUnExpression ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3082:1: (lv_right_3_0= ruleUnExpression )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3082:1: (lv_right_3_0= ruleUnExpression )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3083:3: lv_right_3_0= ruleUnExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNorExpressionAccess().getRightUnExpressionParserRuleCall_1_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleUnExpression_in_ruleNorExpression6525);
                    lv_right_3_0=ruleUnExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNorExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"UnExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNorExpression"


    // $ANTLR start "entryRuleUnExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3107:1: entryRuleUnExpression returns [EObject current=null] : iv_ruleUnExpression= ruleUnExpression EOF ;
    public final EObject entryRuleUnExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3108:2: (iv_ruleUnExpression= ruleUnExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3109:2: iv_ruleUnExpression= ruleUnExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleUnExpression_in_entryRuleUnExpression6564);
            iv_ruleUnExpression=ruleUnExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnExpression6574); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnExpression"


    // $ANTLR start "ruleUnExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3116:1: ruleUnExpression returns [EObject current=null] : (this_BaseExpression_0= ruleBaseExpression | ( () ( (lv_op_2_0= ruleUnOp ) ) ( (lv_uexp_3_0= ruleBaseExpression ) ) ) ) ;
    public final EObject ruleUnExpression() throws RecognitionException {
        EObject current = null;

        EObject this_BaseExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_uexp_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3119:28: ( (this_BaseExpression_0= ruleBaseExpression | ( () ( (lv_op_2_0= ruleUnOp ) ) ( (lv_uexp_3_0= ruleBaseExpression ) ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3120:1: (this_BaseExpression_0= ruleBaseExpression | ( () ( (lv_op_2_0= ruleUnOp ) ) ( (lv_uexp_3_0= ruleBaseExpression ) ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3120:1: (this_BaseExpression_0= ruleBaseExpression | ( () ( (lv_op_2_0= ruleUnOp ) ) ( (lv_uexp_3_0= ruleBaseExpression ) ) ) )
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( ((LA54_0>=RULE_ID && LA54_0<=RULE_INT)||LA54_0==17||LA54_0==39||LA54_0==42) ) {
                alt54=1;
            }
            else if ( (LA54_0==64||(LA54_0>=70 && LA54_0<=71)) ) {
                alt54=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }
            switch (alt54) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3121:5: this_BaseExpression_0= ruleBaseExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getUnExpressionAccess().getBaseExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBaseExpression_in_ruleUnExpression6621);
                    this_BaseExpression_0=ruleBaseExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BaseExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3130:6: ( () ( (lv_op_2_0= ruleUnOp ) ) ( (lv_uexp_3_0= ruleBaseExpression ) ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3130:6: ( () ( (lv_op_2_0= ruleUnOp ) ) ( (lv_uexp_3_0= ruleBaseExpression ) ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3130:7: () ( (lv_op_2_0= ruleUnOp ) ) ( (lv_uexp_3_0= ruleBaseExpression ) )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3130:7: ()
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3131:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnExpressionAccess().getUnExpressionAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3136:2: ( (lv_op_2_0= ruleUnOp ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3137:1: (lv_op_2_0= ruleUnOp )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3137:1: (lv_op_2_0= ruleUnOp )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3138:3: lv_op_2_0= ruleUnOp
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnExpressionAccess().getOpUnOpEnumRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleUnOp_in_ruleUnExpression6657);
                    lv_op_2_0=ruleUnOp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_2_0, 
                              		"UnOp");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3154:2: ( (lv_uexp_3_0= ruleBaseExpression ) )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3155:1: (lv_uexp_3_0= ruleBaseExpression )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3155:1: (lv_uexp_3_0= ruleBaseExpression )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3156:3: lv_uexp_3_0= ruleBaseExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnExpressionAccess().getUexpBaseExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleBaseExpression_in_ruleUnExpression6678);
                    lv_uexp_3_0=ruleBaseExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"uexp",
                              		lv_uexp_3_0, 
                              		"BaseExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnExpression"


    // $ANTLR start "entryRuleBaseExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3180:1: entryRuleBaseExpression returns [EObject current=null] : iv_ruleBaseExpression= ruleBaseExpression EOF ;
    public final EObject entryRuleBaseExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBaseExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3181:2: (iv_ruleBaseExpression= ruleBaseExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3182:2: iv_ruleBaseExpression= ruleBaseExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBaseExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_entryRuleBaseExpression6715);
            iv_ruleBaseExpression=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBaseExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBaseExpression6725); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBaseExpression"


    // $ANTLR start "ruleBaseExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3189:1: ruleBaseExpression returns [EObject current=null] : (this_TupleExpression_0= ruleTupleExpression | this_LiteralExpression_1= ruleLiteralExpression | this_CallExpression_2= ruleCallExpression | this_IteExpression_3= ruleIteExpression | this_ArrayDefinition_4= ruleArrayDefinition ) ;
    public final EObject ruleBaseExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TupleExpression_0 = null;

        EObject this_LiteralExpression_1 = null;

        EObject this_CallExpression_2 = null;

        EObject this_IteExpression_3 = null;

        EObject this_ArrayDefinition_4 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3192:28: ( (this_TupleExpression_0= ruleTupleExpression | this_LiteralExpression_1= ruleLiteralExpression | this_CallExpression_2= ruleCallExpression | this_IteExpression_3= ruleIteExpression | this_ArrayDefinition_4= ruleArrayDefinition ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3193:1: (this_TupleExpression_0= ruleTupleExpression | this_LiteralExpression_1= ruleLiteralExpression | this_CallExpression_2= ruleCallExpression | this_IteExpression_3= ruleIteExpression | this_ArrayDefinition_4= ruleArrayDefinition )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3193:1: (this_TupleExpression_0= ruleTupleExpression | this_LiteralExpression_1= ruleLiteralExpression | this_CallExpression_2= ruleCallExpression | this_IteExpression_3= ruleIteExpression | this_ArrayDefinition_4= ruleArrayDefinition )
            int alt55=5;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt55=1;
                }
                break;
            case RULE_LUS_DOUBLE:
            case RULE_LUS_REAL:
            case RULE_LUS_BOOLEAN:
            case RULE_INT:
                {
                alt55=2;
                }
                break;
            case RULE_ID:
                {
                int LA55_3 = input.LA(2);

                if ( (LA55_3==17) ) {
                    alt55=3;
                }
                else if ( (LA55_3==EOF||LA55_3==14||LA55_3==18||LA55_3==26||LA55_3==36||(LA55_3>=38 && LA55_3<=39)||(LA55_3>=43 && LA55_3<=44)||(LA55_3>=52 && LA55_3<=61)||(LA55_3>=63 && LA55_3<=69)) ) {
                    alt55=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 55, 3, input);

                    throw nvae;
                }
                }
                break;
            case 42:
                {
                alt55=4;
                }
                break;
            case 39:
                {
                alt55=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }

            switch (alt55) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3194:5: this_TupleExpression_0= ruleTupleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getTupleExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleTupleExpression_in_ruleBaseExpression6772);
                    this_TupleExpression_0=ruleTupleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TupleExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3204:5: this_LiteralExpression_1= ruleLiteralExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getLiteralExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLiteralExpression_in_ruleBaseExpression6799);
                    this_LiteralExpression_1=ruleLiteralExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LiteralExpression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3214:5: this_CallExpression_2= ruleCallExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getCallExpressionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleCallExpression_in_ruleBaseExpression6826);
                    this_CallExpression_2=ruleCallExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_CallExpression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3224:5: this_IteExpression_3= ruleIteExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getIteExpressionParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIteExpression_in_ruleBaseExpression6853);
                    this_IteExpression_3=ruleIteExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IteExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3234:5: this_ArrayDefinition_4= ruleArrayDefinition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getArrayDefinitionParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleArrayDefinition_in_ruleBaseExpression6880);
                    this_ArrayDefinition_4=ruleArrayDefinition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ArrayDefinition_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBaseExpression"


    // $ANTLR start "entryRuleArrayDefinition"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3250:1: entryRuleArrayDefinition returns [EObject current=null] : iv_ruleArrayDefinition= ruleArrayDefinition EOF ;
    public final EObject entryRuleArrayDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayDefinition = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3251:2: (iv_ruleArrayDefinition= ruleArrayDefinition EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3252:2: iv_ruleArrayDefinition= ruleArrayDefinition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArrayDefinitionRule()); 
            }
            pushFollow(FOLLOW_ruleArrayDefinition_in_entryRuleArrayDefinition6915);
            iv_ruleArrayDefinition=ruleArrayDefinition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArrayDefinition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleArrayDefinition6925); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayDefinition"


    // $ANTLR start "ruleArrayDefinition"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3259:1: ruleArrayDefinition returns [EObject current=null] : (this_ArrayExpression_0= ruleArrayExpression | this_MatrixExpression_1= ruleMatrixExpression ) ;
    public final EObject ruleArrayDefinition() throws RecognitionException {
        EObject current = null;

        EObject this_ArrayExpression_0 = null;

        EObject this_MatrixExpression_1 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3262:28: ( (this_ArrayExpression_0= ruleArrayExpression | this_MatrixExpression_1= ruleMatrixExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3263:1: (this_ArrayExpression_0= ruleArrayExpression | this_MatrixExpression_1= ruleMatrixExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3263:1: (this_ArrayExpression_0= ruleArrayExpression | this_MatrixExpression_1= ruleMatrixExpression )
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==39) ) {
                int LA56_1 = input.LA(2);

                if ( ((LA56_1>=RULE_ID && LA56_1<=RULE_INT)) ) {
                    alt56=1;
                }
                else if ( (LA56_1==39) ) {
                    alt56=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 56, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 56, 0, input);

                throw nvae;
            }
            switch (alt56) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3264:5: this_ArrayExpression_0= ruleArrayExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getArrayDefinitionAccess().getArrayExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleArrayExpression_in_ruleArrayDefinition6972);
                    this_ArrayExpression_0=ruleArrayExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ArrayExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3274:5: this_MatrixExpression_1= ruleMatrixExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getArrayDefinitionAccess().getMatrixExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleMatrixExpression_in_ruleArrayDefinition6999);
                    this_MatrixExpression_1=ruleMatrixExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_MatrixExpression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayDefinition"


    // $ANTLR start "entryRuleMatrixExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3290:1: entryRuleMatrixExpression returns [EObject current=null] : iv_ruleMatrixExpression= ruleMatrixExpression EOF ;
    public final EObject entryRuleMatrixExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMatrixExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3291:2: (iv_ruleMatrixExpression= ruleMatrixExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3292:2: iv_ruleMatrixExpression= ruleMatrixExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMatrixExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleMatrixExpression_in_entryRuleMatrixExpression7034);
            iv_ruleMatrixExpression=ruleMatrixExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMatrixExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMatrixExpression7044); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMatrixExpression"


    // $ANTLR start "ruleMatrixExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3299:1: ruleMatrixExpression returns [EObject current=null] : (otherlv_0= '[' ( (lv_dimVal_1_0= ruleArrayExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleArrayExpression ) ) )* otherlv_4= ']' ) ;
    public final EObject ruleMatrixExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_dimVal_1_0 = null;

        EObject lv_dimVal_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3302:28: ( (otherlv_0= '[' ( (lv_dimVal_1_0= ruleArrayExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleArrayExpression ) ) )* otherlv_4= ']' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3303:1: (otherlv_0= '[' ( (lv_dimVal_1_0= ruleArrayExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleArrayExpression ) ) )* otherlv_4= ']' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3303:1: (otherlv_0= '[' ( (lv_dimVal_1_0= ruleArrayExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleArrayExpression ) ) )* otherlv_4= ']' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3303:3: otherlv_0= '[' ( (lv_dimVal_1_0= ruleArrayExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleArrayExpression ) ) )* otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,39,FOLLOW_39_in_ruleMatrixExpression7081); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getMatrixExpressionAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3307:1: ( (lv_dimVal_1_0= ruleArrayExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3308:1: (lv_dimVal_1_0= ruleArrayExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3308:1: (lv_dimVal_1_0= ruleArrayExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3309:3: lv_dimVal_1_0= ruleArrayExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMatrixExpressionAccess().getDimValArrayExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleArrayExpression_in_ruleMatrixExpression7102);
            lv_dimVal_1_0=ruleArrayExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getMatrixExpressionRule());
              	        }
                     		add(
                     			current, 
                     			"dimVal",
                      		lv_dimVal_1_0, 
                      		"ArrayExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3325:2: (otherlv_2= ',' ( (lv_dimVal_3_0= ruleArrayExpression ) ) )*
            loop57:
            do {
                int alt57=2;
                int LA57_0 = input.LA(1);

                if ( (LA57_0==38) ) {
                    alt57=1;
                }


                switch (alt57) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3325:4: otherlv_2= ',' ( (lv_dimVal_3_0= ruleArrayExpression ) )
            	    {
            	    otherlv_2=(Token)match(input,38,FOLLOW_38_in_ruleMatrixExpression7115); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getMatrixExpressionAccess().getCommaKeyword_2_0());
            	          
            	    }
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3329:1: ( (lv_dimVal_3_0= ruleArrayExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3330:1: (lv_dimVal_3_0= ruleArrayExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3330:1: (lv_dimVal_3_0= ruleArrayExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3331:3: lv_dimVal_3_0= ruleArrayExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getMatrixExpressionAccess().getDimValArrayExpressionParserRuleCall_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleArrayExpression_in_ruleMatrixExpression7136);
            	    lv_dimVal_3_0=ruleArrayExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getMatrixExpressionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"dimVal",
            	              		lv_dimVal_3_0, 
            	              		"ArrayExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop57;
                }
            } while (true);

            otherlv_4=(Token)match(input,40,FOLLOW_40_in_ruleMatrixExpression7150); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getMatrixExpressionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMatrixExpression"


    // $ANTLR start "entryRuleArrayExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3359:1: entryRuleArrayExpression returns [EObject current=null] : iv_ruleArrayExpression= ruleArrayExpression EOF ;
    public final EObject entryRuleArrayExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3360:2: (iv_ruleArrayExpression= ruleArrayExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3361:2: iv_ruleArrayExpression= ruleArrayExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArrayExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleArrayExpression_in_entryRuleArrayExpression7186);
            iv_ruleArrayExpression=ruleArrayExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArrayExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleArrayExpression7196); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayExpression"


    // $ANTLR start "ruleArrayExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3368:1: ruleArrayExpression returns [EObject current=null] : (otherlv_0= '[' ( (lv_dimVal_1_0= ruleLiteralExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleLiteralExpression ) ) )* otherlv_4= ']' ) ;
    public final EObject ruleArrayExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_dimVal_1_0 = null;

        EObject lv_dimVal_3_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3371:28: ( (otherlv_0= '[' ( (lv_dimVal_1_0= ruleLiteralExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleLiteralExpression ) ) )* otherlv_4= ']' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3372:1: (otherlv_0= '[' ( (lv_dimVal_1_0= ruleLiteralExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleLiteralExpression ) ) )* otherlv_4= ']' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3372:1: (otherlv_0= '[' ( (lv_dimVal_1_0= ruleLiteralExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleLiteralExpression ) ) )* otherlv_4= ']' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3372:3: otherlv_0= '[' ( (lv_dimVal_1_0= ruleLiteralExpression ) ) (otherlv_2= ',' ( (lv_dimVal_3_0= ruleLiteralExpression ) ) )* otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,39,FOLLOW_39_in_ruleArrayExpression7233); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getArrayExpressionAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3376:1: ( (lv_dimVal_1_0= ruleLiteralExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3377:1: (lv_dimVal_1_0= ruleLiteralExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3377:1: (lv_dimVal_1_0= ruleLiteralExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3378:3: lv_dimVal_1_0= ruleLiteralExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getArrayExpressionAccess().getDimValLiteralExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLiteralExpression_in_ruleArrayExpression7254);
            lv_dimVal_1_0=ruleLiteralExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getArrayExpressionRule());
              	        }
                     		add(
                     			current, 
                     			"dimVal",
                      		lv_dimVal_1_0, 
                      		"LiteralExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3394:2: (otherlv_2= ',' ( (lv_dimVal_3_0= ruleLiteralExpression ) ) )*
            loop58:
            do {
                int alt58=2;
                int LA58_0 = input.LA(1);

                if ( (LA58_0==38) ) {
                    alt58=1;
                }


                switch (alt58) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3394:4: otherlv_2= ',' ( (lv_dimVal_3_0= ruleLiteralExpression ) )
            	    {
            	    otherlv_2=(Token)match(input,38,FOLLOW_38_in_ruleArrayExpression7267); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getArrayExpressionAccess().getCommaKeyword_2_0());
            	          
            	    }
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3398:1: ( (lv_dimVal_3_0= ruleLiteralExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3399:1: (lv_dimVal_3_0= ruleLiteralExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3399:1: (lv_dimVal_3_0= ruleLiteralExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3400:3: lv_dimVal_3_0= ruleLiteralExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getArrayExpressionAccess().getDimValLiteralExpressionParserRuleCall_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleLiteralExpression_in_ruleArrayExpression7288);
            	    lv_dimVal_3_0=ruleLiteralExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getArrayExpressionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"dimVal",
            	              		lv_dimVal_3_0, 
            	              		"LiteralExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop58;
                }
            } while (true);

            otherlv_4=(Token)match(input,40,FOLLOW_40_in_ruleArrayExpression7302); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getArrayExpressionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayExpression"


    // $ANTLR start "entryRuleVariableExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3428:1: entryRuleVariableExpression returns [EObject current=null] : iv_ruleVariableExpression= ruleVariableExpression EOF ;
    public final EObject entryRuleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3429:2: (iv_ruleVariableExpression= ruleVariableExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3430:2: iv_ruleVariableExpression= ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleVariableExpression_in_entryRuleVariableExpression7338);
            iv_ruleVariableExpression=ruleVariableExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariableExpression7348); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3437:1: ruleVariableExpression returns [EObject current=null] : ( () ( (lv_variable_1_0= ruleVariableCall ) ) ) ;
    public final EObject ruleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_variable_1_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3440:28: ( ( () ( (lv_variable_1_0= ruleVariableCall ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3441:1: ( () ( (lv_variable_1_0= ruleVariableCall ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3441:1: ( () ( (lv_variable_1_0= ruleVariableCall ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3441:2: () ( (lv_variable_1_0= ruleVariableCall ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3441:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3442:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getVariableExpressionAccess().getVariableExpressionAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3447:2: ( (lv_variable_1_0= ruleVariableCall ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3448:1: (lv_variable_1_0= ruleVariableCall )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3448:1: (lv_variable_1_0= ruleVariableCall )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3449:3: lv_variable_1_0= ruleVariableCall
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableVariableCallParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariableCall_in_ruleVariableExpression7403);
            lv_variable_1_0=ruleVariableCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"variable",
                      		lv_variable_1_0, 
                      		"VariableCall");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleTupleExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3473:1: entryRuleTupleExpression returns [EObject current=null] : iv_ruleTupleExpression= ruleTupleExpression EOF ;
    public final EObject entryRuleTupleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTupleExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3474:2: (iv_ruleTupleExpression= ruleTupleExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3475:2: iv_ruleTupleExpression= ruleTupleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTupleExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleTupleExpression_in_entryRuleTupleExpression7439);
            iv_ruleTupleExpression=ruleTupleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTupleExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTupleExpression7449); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTupleExpression"


    // $ANTLR start "ruleTupleExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3482:1: ruleTupleExpression returns [EObject current=null] : (otherlv_0= '(' this_FbyExpression_1= ruleFbyExpression ( ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )=> ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* ) )? otherlv_5= ')' ) ;
    public final EObject ruleTupleExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject this_FbyExpression_1 = null;

        EObject lv_expressionList_4_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3485:28: ( (otherlv_0= '(' this_FbyExpression_1= ruleFbyExpression ( ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )=> ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* ) )? otherlv_5= ')' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3486:1: (otherlv_0= '(' this_FbyExpression_1= ruleFbyExpression ( ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )=> ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* ) )? otherlv_5= ')' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3486:1: (otherlv_0= '(' this_FbyExpression_1= ruleFbyExpression ( ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )=> ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* ) )? otherlv_5= ')' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3486:3: otherlv_0= '(' this_FbyExpression_1= ruleFbyExpression ( ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )=> ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* ) )? otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleTupleExpression7486); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTupleExpressionAccess().getLeftParenthesisKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getTupleExpressionAccess().getFbyExpressionParserRuleCall_1()); 
                  
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleTupleExpression7508);
            this_FbyExpression_1=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_FbyExpression_1; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3499:1: ( ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )=> ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==38) && (synpred10_InternalLustre())) {
                alt60=1;
            }
            else if ( (LA60_0==18) ) {
                int LA60_2 = input.LA(2);

                if ( (synpred10_InternalLustre()) ) {
                    alt60=1;
                }
            }
            switch (alt60) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3499:2: ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )=> ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3505:8: ( () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )* )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3505:9: () (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )*
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3505:9: ()
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3506:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndAdd(
                                  grammarAccess.getTupleExpressionAccess().getTupleExpressionExpressionListAction_2_0_0(),
                                  current);
                          
                    }

                    }

                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3511:2: (otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) ) )*
                    loop59:
                    do {
                        int alt59=2;
                        int LA59_0 = input.LA(1);

                        if ( (LA59_0==38) ) {
                            alt59=1;
                        }


                        switch (alt59) {
                    	case 1 :
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3511:4: otherlv_3= ',' ( (lv_expressionList_4_0= ruleFbyExpression ) )
                    	    {
                    	    otherlv_3=(Token)match(input,38,FOLLOW_38_in_ruleTupleExpression7555); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_3, grammarAccess.getTupleExpressionAccess().getCommaKeyword_2_0_1_0());
                    	          
                    	    }
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3515:1: ( (lv_expressionList_4_0= ruleFbyExpression ) )
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3516:1: (lv_expressionList_4_0= ruleFbyExpression )
                    	    {
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3516:1: (lv_expressionList_4_0= ruleFbyExpression )
                    	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3517:3: lv_expressionList_4_0= ruleFbyExpression
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getTupleExpressionAccess().getExpressionListFbyExpressionParserRuleCall_2_0_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleFbyExpression_in_ruleTupleExpression7576);
                    	    lv_expressionList_4_0=ruleFbyExpression();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getTupleExpressionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"expressionList",
                    	              		lv_expressionList_4_0, 
                    	              		"FbyExpression");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop59;
                        }
                    } while (true);


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,18,FOLLOW_18_in_ruleTupleExpression7593); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getTupleExpressionAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTupleExpression"


    // $ANTLR start "entryRuleLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3545:1: entryRuleLiteralExpression returns [EObject current=null] : iv_ruleLiteralExpression= ruleLiteralExpression EOF ;
    public final EObject entryRuleLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3546:2: (iv_ruleLiteralExpression= ruleLiteralExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3547:2: iv_ruleLiteralExpression= ruleLiteralExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleLiteralExpression_in_entryRuleLiteralExpression7629);
            iv_ruleLiteralExpression=ruleLiteralExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteralExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLiteralExpression7639); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralExpression"


    // $ANTLR start "ruleLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3554:1: ruleLiteralExpression returns [EObject current=null] : (this_IntegerLiteralExpression_0= ruleIntegerLiteralExpression | this_RealLiteralExpression_1= ruleRealLiteralExpression | this_DoubleLiteralExpression_2= ruleDoubleLiteralExpression | this_BooleanLiteralExpression_3= ruleBooleanLiteralExpression | this_VariableExpression_4= ruleVariableExpression ) ;
    public final EObject ruleLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerLiteralExpression_0 = null;

        EObject this_RealLiteralExpression_1 = null;

        EObject this_DoubleLiteralExpression_2 = null;

        EObject this_BooleanLiteralExpression_3 = null;

        EObject this_VariableExpression_4 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3557:28: ( (this_IntegerLiteralExpression_0= ruleIntegerLiteralExpression | this_RealLiteralExpression_1= ruleRealLiteralExpression | this_DoubleLiteralExpression_2= ruleDoubleLiteralExpression | this_BooleanLiteralExpression_3= ruleBooleanLiteralExpression | this_VariableExpression_4= ruleVariableExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3558:1: (this_IntegerLiteralExpression_0= ruleIntegerLiteralExpression | this_RealLiteralExpression_1= ruleRealLiteralExpression | this_DoubleLiteralExpression_2= ruleDoubleLiteralExpression | this_BooleanLiteralExpression_3= ruleBooleanLiteralExpression | this_VariableExpression_4= ruleVariableExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3558:1: (this_IntegerLiteralExpression_0= ruleIntegerLiteralExpression | this_RealLiteralExpression_1= ruleRealLiteralExpression | this_DoubleLiteralExpression_2= ruleDoubleLiteralExpression | this_BooleanLiteralExpression_3= ruleBooleanLiteralExpression | this_VariableExpression_4= ruleVariableExpression )
            int alt61=5;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt61=1;
                }
                break;
            case RULE_LUS_REAL:
                {
                alt61=2;
                }
                break;
            case RULE_LUS_DOUBLE:
                {
                alt61=3;
                }
                break;
            case RULE_LUS_BOOLEAN:
                {
                alt61=4;
                }
                break;
            case RULE_ID:
                {
                alt61=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }

            switch (alt61) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3559:5: this_IntegerLiteralExpression_0= ruleIntegerLiteralExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLiteralExpressionAccess().getIntegerLiteralExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntegerLiteralExpression_in_ruleLiteralExpression7686);
                    this_IntegerLiteralExpression_0=ruleIntegerLiteralExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerLiteralExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3569:5: this_RealLiteralExpression_1= ruleRealLiteralExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLiteralExpressionAccess().getRealLiteralExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleRealLiteralExpression_in_ruleLiteralExpression7713);
                    this_RealLiteralExpression_1=ruleRealLiteralExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RealLiteralExpression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3579:5: this_DoubleLiteralExpression_2= ruleDoubleLiteralExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLiteralExpressionAccess().getDoubleLiteralExpressionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleDoubleLiteralExpression_in_ruleLiteralExpression7740);
                    this_DoubleLiteralExpression_2=ruleDoubleLiteralExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_DoubleLiteralExpression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3589:5: this_BooleanLiteralExpression_3= ruleBooleanLiteralExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLiteralExpressionAccess().getBooleanLiteralExpressionParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBooleanLiteralExpression_in_ruleLiteralExpression7767);
                    this_BooleanLiteralExpression_3=ruleBooleanLiteralExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanLiteralExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3599:5: this_VariableExpression_4= ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLiteralExpressionAccess().getVariableExpressionParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleVariableExpression_in_ruleLiteralExpression7794);
                    this_VariableExpression_4=ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableExpression_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralExpression"


    // $ANTLR start "entryRuleIntegerLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3615:1: entryRuleIntegerLiteralExpression returns [EObject current=null] : iv_ruleIntegerLiteralExpression= ruleIntegerLiteralExpression EOF ;
    public final EObject entryRuleIntegerLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerLiteralExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3616:2: (iv_ruleIntegerLiteralExpression= ruleIntegerLiteralExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3617:2: iv_ruleIntegerLiteralExpression= ruleIntegerLiteralExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerLiteralExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerLiteralExpression_in_entryRuleIntegerLiteralExpression7829);
            iv_ruleIntegerLiteralExpression=ruleIntegerLiteralExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerLiteralExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerLiteralExpression7839); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerLiteralExpression"


    // $ANTLR start "ruleIntegerLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3624:1: ruleIntegerLiteralExpression returns [EObject current=null] : ( () ( (lv_value_1_0= ruleLUS_INT ) ) ) ;
    public final EObject ruleIntegerLiteralExpression() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3627:28: ( ( () ( (lv_value_1_0= ruleLUS_INT ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3628:1: ( () ( (lv_value_1_0= ruleLUS_INT ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3628:1: ( () ( (lv_value_1_0= ruleLUS_INT ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3628:2: () ( (lv_value_1_0= ruleLUS_INT ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3628:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3629:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getIntegerLiteralExpressionAccess().getIntegerLiteralExpressionAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3634:2: ( (lv_value_1_0= ruleLUS_INT ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3635:1: (lv_value_1_0= ruleLUS_INT )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3635:1: (lv_value_1_0= ruleLUS_INT )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3636:3: lv_value_1_0= ruleLUS_INT
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIntegerLiteralExpressionAccess().getValueLUS_INTParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLUS_INT_in_ruleIntegerLiteralExpression7894);
            lv_value_1_0=ruleLUS_INT();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIntegerLiteralExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"LUS_INT");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerLiteralExpression"


    // $ANTLR start "entryRuleRealLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3660:1: entryRuleRealLiteralExpression returns [EObject current=null] : iv_ruleRealLiteralExpression= ruleRealLiteralExpression EOF ;
    public final EObject entryRuleRealLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealLiteralExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3661:2: (iv_ruleRealLiteralExpression= ruleRealLiteralExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3662:2: iv_ruleRealLiteralExpression= ruleRealLiteralExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealLiteralExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleRealLiteralExpression_in_entryRuleRealLiteralExpression7930);
            iv_ruleRealLiteralExpression=ruleRealLiteralExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealLiteralExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRealLiteralExpression7940); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealLiteralExpression"


    // $ANTLR start "ruleRealLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3669:1: ruleRealLiteralExpression returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_LUS_REAL ) ) ) ;
    public final EObject ruleRealLiteralExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3672:28: ( ( () ( (lv_value_1_0= RULE_LUS_REAL ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3673:1: ( () ( (lv_value_1_0= RULE_LUS_REAL ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3673:1: ( () ( (lv_value_1_0= RULE_LUS_REAL ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3673:2: () ( (lv_value_1_0= RULE_LUS_REAL ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3673:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3674:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getRealLiteralExpressionAccess().getRealLiteralExpressionAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3679:2: ( (lv_value_1_0= RULE_LUS_REAL ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3680:1: (lv_value_1_0= RULE_LUS_REAL )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3680:1: (lv_value_1_0= RULE_LUS_REAL )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3681:3: lv_value_1_0= RULE_LUS_REAL
            {
            lv_value_1_0=(Token)match(input,RULE_LUS_REAL,FOLLOW_RULE_LUS_REAL_in_ruleRealLiteralExpression7991); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_1_0, grammarAccess.getRealLiteralExpressionAccess().getValueLUS_REALTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRealLiteralExpressionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"LUS_REAL");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealLiteralExpression"


    // $ANTLR start "entryRuleDoubleLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3705:1: entryRuleDoubleLiteralExpression returns [EObject current=null] : iv_ruleDoubleLiteralExpression= ruleDoubleLiteralExpression EOF ;
    public final EObject entryRuleDoubleLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDoubleLiteralExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3706:2: (iv_ruleDoubleLiteralExpression= ruleDoubleLiteralExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3707:2: iv_ruleDoubleLiteralExpression= ruleDoubleLiteralExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDoubleLiteralExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleDoubleLiteralExpression_in_entryRuleDoubleLiteralExpression8032);
            iv_ruleDoubleLiteralExpression=ruleDoubleLiteralExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDoubleLiteralExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDoubleLiteralExpression8042); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDoubleLiteralExpression"


    // $ANTLR start "ruleDoubleLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3714:1: ruleDoubleLiteralExpression returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_LUS_DOUBLE ) ) ) ;
    public final EObject ruleDoubleLiteralExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3717:28: ( ( () ( (lv_value_1_0= RULE_LUS_DOUBLE ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3718:1: ( () ( (lv_value_1_0= RULE_LUS_DOUBLE ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3718:1: ( () ( (lv_value_1_0= RULE_LUS_DOUBLE ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3718:2: () ( (lv_value_1_0= RULE_LUS_DOUBLE ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3718:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3719:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getDoubleLiteralExpressionAccess().getDoubleLiteralExpressionAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3724:2: ( (lv_value_1_0= RULE_LUS_DOUBLE ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3725:1: (lv_value_1_0= RULE_LUS_DOUBLE )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3725:1: (lv_value_1_0= RULE_LUS_DOUBLE )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3726:3: lv_value_1_0= RULE_LUS_DOUBLE
            {
            lv_value_1_0=(Token)match(input,RULE_LUS_DOUBLE,FOLLOW_RULE_LUS_DOUBLE_in_ruleDoubleLiteralExpression8093); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_1_0, grammarAccess.getDoubleLiteralExpressionAccess().getValueLUS_DOUBLETerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getDoubleLiteralExpressionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"LUS_DOUBLE");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDoubleLiteralExpression"


    // $ANTLR start "entryRuleBooleanLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3750:1: entryRuleBooleanLiteralExpression returns [EObject current=null] : iv_ruleBooleanLiteralExpression= ruleBooleanLiteralExpression EOF ;
    public final EObject entryRuleBooleanLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanLiteralExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3751:2: (iv_ruleBooleanLiteralExpression= ruleBooleanLiteralExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3752:2: iv_ruleBooleanLiteralExpression= ruleBooleanLiteralExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanLiteralExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleBooleanLiteralExpression_in_entryRuleBooleanLiteralExpression8134);
            iv_ruleBooleanLiteralExpression=ruleBooleanLiteralExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanLiteralExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanLiteralExpression8144); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanLiteralExpression"


    // $ANTLR start "ruleBooleanLiteralExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3759:1: ruleBooleanLiteralExpression returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_LUS_BOOLEAN ) ) ) ;
    public final EObject ruleBooleanLiteralExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3762:28: ( ( () ( (lv_value_1_0= RULE_LUS_BOOLEAN ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3763:1: ( () ( (lv_value_1_0= RULE_LUS_BOOLEAN ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3763:1: ( () ( (lv_value_1_0= RULE_LUS_BOOLEAN ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3763:2: () ( (lv_value_1_0= RULE_LUS_BOOLEAN ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3763:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3764:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getBooleanLiteralExpressionAccess().getBooleanLiteralExpressionAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3769:2: ( (lv_value_1_0= RULE_LUS_BOOLEAN ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3770:1: (lv_value_1_0= RULE_LUS_BOOLEAN )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3770:1: (lv_value_1_0= RULE_LUS_BOOLEAN )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3771:3: lv_value_1_0= RULE_LUS_BOOLEAN
            {
            lv_value_1_0=(Token)match(input,RULE_LUS_BOOLEAN,FOLLOW_RULE_LUS_BOOLEAN_in_ruleBooleanLiteralExpression8195); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_1_0, grammarAccess.getBooleanLiteralExpressionAccess().getValueLUS_BOOLEANTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanLiteralExpressionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"LUS_BOOLEAN");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanLiteralExpression"


    // $ANTLR start "entryRuleCallExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3795:1: entryRuleCallExpression returns [EObject current=null] : iv_ruleCallExpression= ruleCallExpression EOF ;
    public final EObject entryRuleCallExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3796:2: (iv_ruleCallExpression= ruleCallExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3797:2: iv_ruleCallExpression= ruleCallExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleCallExpression_in_entryRuleCallExpression8236);
            iv_ruleCallExpression=ruleCallExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCallExpression8246); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallExpression"


    // $ANTLR start "ruleCallExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3804:1: ruleCallExpression returns [EObject current=null] : ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleFbyExpression ) ) (otherlv_4= ',' ( (lv_arguments_5_0= ruleFbyExpression ) ) )* otherlv_6= ')' ) ;
    public final EObject ruleCallExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_arguments_3_0 = null;

        EObject lv_arguments_5_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3807:28: ( ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleFbyExpression ) ) (otherlv_4= ',' ( (lv_arguments_5_0= ruleFbyExpression ) ) )* otherlv_6= ')' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3808:1: ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleFbyExpression ) ) (otherlv_4= ',' ( (lv_arguments_5_0= ruleFbyExpression ) ) )* otherlv_6= ')' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3808:1: ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleFbyExpression ) ) (otherlv_4= ',' ( (lv_arguments_5_0= ruleFbyExpression ) ) )* otherlv_6= ')' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3808:2: () ( (otherlv_1= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleFbyExpression ) ) (otherlv_4= ',' ( (lv_arguments_5_0= ruleFbyExpression ) ) )* otherlv_6= ')'
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3808:2: ()
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3809:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getCallExpressionAccess().getCallExpressionAction_0(),
                          current);
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3814:2: ( (otherlv_1= RULE_ID ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3815:1: (otherlv_1= RULE_ID )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3815:1: (otherlv_1= RULE_ID )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3816:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getCallExpressionRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCallExpression8300); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getCallExpressionAccess().getElemCallableElementCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_17_in_ruleCallExpression8312); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getCallExpressionAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3831:1: ( (lv_arguments_3_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3832:1: (lv_arguments_3_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3832:1: (lv_arguments_3_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3833:3: lv_arguments_3_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCallExpressionAccess().getArgumentsFbyExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleCallExpression8333);
            lv_arguments_3_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCallExpressionRule());
              	        }
                     		add(
                     			current, 
                     			"arguments",
                      		lv_arguments_3_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3849:2: (otherlv_4= ',' ( (lv_arguments_5_0= ruleFbyExpression ) ) )*
            loop62:
            do {
                int alt62=2;
                int LA62_0 = input.LA(1);

                if ( (LA62_0==38) ) {
                    alt62=1;
                }


                switch (alt62) {
            	case 1 :
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3849:4: otherlv_4= ',' ( (lv_arguments_5_0= ruleFbyExpression ) )
            	    {
            	    otherlv_4=(Token)match(input,38,FOLLOW_38_in_ruleCallExpression8346); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_4, grammarAccess.getCallExpressionAccess().getCommaKeyword_4_0());
            	          
            	    }
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3853:1: ( (lv_arguments_5_0= ruleFbyExpression ) )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3854:1: (lv_arguments_5_0= ruleFbyExpression )
            	    {
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3854:1: (lv_arguments_5_0= ruleFbyExpression )
            	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3855:3: lv_arguments_5_0= ruleFbyExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCallExpressionAccess().getArgumentsFbyExpressionParserRuleCall_4_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleFbyExpression_in_ruleCallExpression8367);
            	    lv_arguments_5_0=ruleFbyExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCallExpressionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"arguments",
            	              		lv_arguments_5_0, 
            	              		"FbyExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop62;
                }
            } while (true);

            otherlv_6=(Token)match(input,18,FOLLOW_18_in_ruleCallExpression8381); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getCallExpressionAccess().getRightParenthesisKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallExpression"


    // $ANTLR start "entryRuleIteExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3883:1: entryRuleIteExpression returns [EObject current=null] : iv_ruleIteExpression= ruleIteExpression EOF ;
    public final EObject entryRuleIteExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIteExpression = null;


        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3884:2: (iv_ruleIteExpression= ruleIteExpression EOF )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3885:2: iv_ruleIteExpression= ruleIteExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIteExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleIteExpression_in_entryRuleIteExpression8417);
            iv_ruleIteExpression=ruleIteExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIteExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIteExpression8427); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIteExpression"


    // $ANTLR start "ruleIteExpression"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3892:1: ruleIteExpression returns [EObject current=null] : (otherlv_0= 'if' ( (lv_guard_1_0= ruleFbyExpression ) ) otherlv_2= 'then' ( (lv_then_3_0= ruleFbyExpression ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_else_5_0= ruleFbyExpression ) ) ) ) ;
    public final EObject ruleIteExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_guard_1_0 = null;

        EObject lv_then_3_0 = null;

        EObject lv_else_5_0 = null;


         enterRule(); 
            
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3895:28: ( (otherlv_0= 'if' ( (lv_guard_1_0= ruleFbyExpression ) ) otherlv_2= 'then' ( (lv_then_3_0= ruleFbyExpression ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_else_5_0= ruleFbyExpression ) ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3896:1: (otherlv_0= 'if' ( (lv_guard_1_0= ruleFbyExpression ) ) otherlv_2= 'then' ( (lv_then_3_0= ruleFbyExpression ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_else_5_0= ruleFbyExpression ) ) ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3896:1: (otherlv_0= 'if' ( (lv_guard_1_0= ruleFbyExpression ) ) otherlv_2= 'then' ( (lv_then_3_0= ruleFbyExpression ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_else_5_0= ruleFbyExpression ) ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3896:3: otherlv_0= 'if' ( (lv_guard_1_0= ruleFbyExpression ) ) otherlv_2= 'then' ( (lv_then_3_0= ruleFbyExpression ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_else_5_0= ruleFbyExpression ) ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_42_in_ruleIteExpression8464); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIteExpressionAccess().getIfKeyword_0());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3900:1: ( (lv_guard_1_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3901:1: (lv_guard_1_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3901:1: (lv_guard_1_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3902:3: lv_guard_1_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIteExpressionAccess().getGuardFbyExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleIteExpression8485);
            lv_guard_1_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIteExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"guard",
                      		lv_guard_1_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,43,FOLLOW_43_in_ruleIteExpression8497); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getIteExpressionAccess().getThenKeyword_2());
                  
            }
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3922:1: ( (lv_then_3_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3923:1: (lv_then_3_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3923:1: (lv_then_3_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3924:3: lv_then_3_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIteExpressionAccess().getThenFbyExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleIteExpression8518);
            lv_then_3_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIteExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"then",
                      		lv_then_3_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3940:2: ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_else_5_0= ruleFbyExpression ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3940:3: ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_else_5_0= ruleFbyExpression ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3940:3: ( ( 'else' )=>otherlv_4= 'else' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3940:4: ( 'else' )=>otherlv_4= 'else'
            {
            otherlv_4=(Token)match(input,44,FOLLOW_44_in_ruleIteExpression8539); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getIteExpressionAccess().getElseKeyword_4_0());
                  
            }

            }

            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3945:2: ( (lv_else_5_0= ruleFbyExpression ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3946:1: (lv_else_5_0= ruleFbyExpression )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3946:1: (lv_else_5_0= ruleFbyExpression )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3947:3: lv_else_5_0= ruleFbyExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIteExpressionAccess().getElseFbyExpressionParserRuleCall_4_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFbyExpression_in_ruleIteExpression8561);
            lv_else_5_0=ruleFbyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIteExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"else",
                      		lv_else_5_0, 
                      		"FbyExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIteExpression"


    // $ANTLR start "ruleSpecificationType"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3971:1: ruleSpecificationType returns [Enumerator current=null] : ( (enumLiteral_0= 'requires' ) | (enumLiteral_1= 'ensures' ) | (enumLiteral_2= 'observer' ) ) ;
    public final Enumerator ruleSpecificationType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3973:28: ( ( (enumLiteral_0= 'requires' ) | (enumLiteral_1= 'ensures' ) | (enumLiteral_2= 'observer' ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3974:1: ( (enumLiteral_0= 'requires' ) | (enumLiteral_1= 'ensures' ) | (enumLiteral_2= 'observer' ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3974:1: ( (enumLiteral_0= 'requires' ) | (enumLiteral_1= 'ensures' ) | (enumLiteral_2= 'observer' ) )
            int alt63=3;
            switch ( input.LA(1) ) {
            case 45:
                {
                alt63=1;
                }
                break;
            case 46:
                {
                alt63=2;
                }
                break;
            case 47:
                {
                alt63=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 63, 0, input);

                throw nvae;
            }

            switch (alt63) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3974:2: (enumLiteral_0= 'requires' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3974:2: (enumLiteral_0= 'requires' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3974:4: enumLiteral_0= 'requires'
                    {
                    enumLiteral_0=(Token)match(input,45,FOLLOW_45_in_ruleSpecificationType8612); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getSpecificationTypeAccess().getRequiresEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getSpecificationTypeAccess().getRequiresEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3980:6: (enumLiteral_1= 'ensures' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3980:6: (enumLiteral_1= 'ensures' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3980:8: enumLiteral_1= 'ensures'
                    {
                    enumLiteral_1=(Token)match(input,46,FOLLOW_46_in_ruleSpecificationType8629); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getSpecificationTypeAccess().getEnsuresEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getSpecificationTypeAccess().getEnsuresEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3986:6: (enumLiteral_2= 'observer' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3986:6: (enumLiteral_2= 'observer' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3986:8: enumLiteral_2= 'observer'
                    {
                    enumLiteral_2=(Token)match(input,47,FOLLOW_47_in_ruleSpecificationType8646); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getSpecificationTypeAccess().getObserverEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getSpecificationTypeAccess().getObserverEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSpecificationType"


    // $ANTLR start "ruleBasicType"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3996:1: ruleBasicType returns [Enumerator current=null] : ( (enumLiteral_0= 'int' ) | (enumLiteral_1= 'real' ) | (enumLiteral_2= 'bool' ) | (enumLiteral_3= 'double' ) ) ;
    public final Enumerator ruleBasicType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3998:28: ( ( (enumLiteral_0= 'int' ) | (enumLiteral_1= 'real' ) | (enumLiteral_2= 'bool' ) | (enumLiteral_3= 'double' ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3999:1: ( (enumLiteral_0= 'int' ) | (enumLiteral_1= 'real' ) | (enumLiteral_2= 'bool' ) | (enumLiteral_3= 'double' ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3999:1: ( (enumLiteral_0= 'int' ) | (enumLiteral_1= 'real' ) | (enumLiteral_2= 'bool' ) | (enumLiteral_3= 'double' ) )
            int alt64=4;
            switch ( input.LA(1) ) {
            case 48:
                {
                alt64=1;
                }
                break;
            case 49:
                {
                alt64=2;
                }
                break;
            case 50:
                {
                alt64=3;
                }
                break;
            case 51:
                {
                alt64=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 64, 0, input);

                throw nvae;
            }

            switch (alt64) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3999:2: (enumLiteral_0= 'int' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3999:2: (enumLiteral_0= 'int' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3999:4: enumLiteral_0= 'int'
                    {
                    enumLiteral_0=(Token)match(input,48,FOLLOW_48_in_ruleBasicType8691); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getBasicTypeAccess().getIntegerEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getBasicTypeAccess().getIntegerEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4005:6: (enumLiteral_1= 'real' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4005:6: (enumLiteral_1= 'real' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4005:8: enumLiteral_1= 'real'
                    {
                    enumLiteral_1=(Token)match(input,49,FOLLOW_49_in_ruleBasicType8708); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getBasicTypeAccess().getRealEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getBasicTypeAccess().getRealEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4011:6: (enumLiteral_2= 'bool' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4011:6: (enumLiteral_2= 'bool' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4011:8: enumLiteral_2= 'bool'
                    {
                    enumLiteral_2=(Token)match(input,50,FOLLOW_50_in_ruleBasicType8725); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getBasicTypeAccess().getBooleanEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getBasicTypeAccess().getBooleanEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4017:6: (enumLiteral_3= 'double' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4017:6: (enumLiteral_3= 'double' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4017:8: enumLiteral_3= 'double'
                    {
                    enumLiteral_3=(Token)match(input,51,FOLLOW_51_in_ruleBasicType8742); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getBasicTypeAccess().getDoubleEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getBasicTypeAccess().getDoubleEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicType"


    // $ANTLR start "ruleFbyOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4027:1: ruleFbyOp returns [Enumerator current=null] : (enumLiteral_0= '->' ) ;
    public final Enumerator ruleFbyOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4029:28: ( (enumLiteral_0= '->' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4030:1: (enumLiteral_0= '->' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4030:1: (enumLiteral_0= '->' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4030:3: enumLiteral_0= '->'
            {
            enumLiteral_0=(Token)match(input,52,FOLLOW_52_in_ruleFbyOp8786); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current = grammarAccess.getFbyOpAccess().getFollowByEnumLiteralDeclaration().getEnumLiteral().getInstance();
                      newLeafNode(enumLiteral_0, grammarAccess.getFbyOpAccess().getFollowByEnumLiteralDeclaration()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFbyOp"


    // $ANTLR start "ruleOrOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4040:1: ruleOrOp returns [Enumerator current=null] : ( (enumLiteral_0= 'or' ) | (enumLiteral_1= 'xor' ) ) ;
    public final Enumerator ruleOrOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4042:28: ( ( (enumLiteral_0= 'or' ) | (enumLiteral_1= 'xor' ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4043:1: ( (enumLiteral_0= 'or' ) | (enumLiteral_1= 'xor' ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4043:1: ( (enumLiteral_0= 'or' ) | (enumLiteral_1= 'xor' ) )
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==53) ) {
                alt65=1;
            }
            else if ( (LA65_0==54) ) {
                alt65=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 65, 0, input);

                throw nvae;
            }
            switch (alt65) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4043:2: (enumLiteral_0= 'or' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4043:2: (enumLiteral_0= 'or' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4043:4: enumLiteral_0= 'or'
                    {
                    enumLiteral_0=(Token)match(input,53,FOLLOW_53_in_ruleOrOp8830); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getOrOpAccess().getOrEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getOrOpAccess().getOrEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4049:6: (enumLiteral_1= 'xor' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4049:6: (enumLiteral_1= 'xor' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4049:8: enumLiteral_1= 'xor'
                    {
                    enumLiteral_1=(Token)match(input,54,FOLLOW_54_in_ruleOrOp8847); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getOrOpAccess().getXorEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getOrOpAccess().getXorEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrOp"


    // $ANTLR start "ruleAndOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4059:1: ruleAndOp returns [Enumerator current=null] : (enumLiteral_0= 'and' ) ;
    public final Enumerator ruleAndOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4061:28: ( (enumLiteral_0= 'and' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4062:1: (enumLiteral_0= 'and' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4062:1: (enumLiteral_0= 'and' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4062:3: enumLiteral_0= 'and'
            {
            enumLiteral_0=(Token)match(input,55,FOLLOW_55_in_ruleAndOp8891); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current = grammarAccess.getAndOpAccess().getAndEnumLiteralDeclaration().getEnumLiteral().getInstance();
                      newLeafNode(enumLiteral_0, grammarAccess.getAndOpAccess().getAndEnumLiteralDeclaration()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndOp"


    // $ANTLR start "ruleImpliesOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4072:1: ruleImpliesOp returns [Enumerator current=null] : (enumLiteral_0= '=>' ) ;
    public final Enumerator ruleImpliesOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4074:28: ( (enumLiteral_0= '=>' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4075:1: (enumLiteral_0= '=>' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4075:1: (enumLiteral_0= '=>' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4075:3: enumLiteral_0= '=>'
            {
            enumLiteral_0=(Token)match(input,56,FOLLOW_56_in_ruleImpliesOp8934); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current = grammarAccess.getImpliesOpAccess().getImpliesEnumLiteralDeclaration().getEnumLiteral().getInstance();
                      newLeafNode(enumLiteral_0, grammarAccess.getImpliesOpAccess().getImpliesEnumLiteralDeclaration()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImpliesOp"


    // $ANTLR start "ruleRelOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4085:1: ruleRelOp returns [Enumerator current=null] : ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) ) ;
    public final Enumerator ruleRelOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4087:28: ( ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4088:1: ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4088:1: ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) )
            int alt66=6;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt66=1;
                }
                break;
            case 57:
                {
                alt66=2;
                }
                break;
            case 58:
                {
                alt66=3;
                }
                break;
            case 59:
                {
                alt66=4;
                }
                break;
            case 60:
                {
                alt66=5;
                }
                break;
            case 61:
                {
                alt66=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 66, 0, input);

                throw nvae;
            }

            switch (alt66) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4088:2: (enumLiteral_0= '=' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4088:2: (enumLiteral_0= '=' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4088:4: enumLiteral_0= '='
                    {
                    enumLiteral_0=(Token)match(input,26,FOLLOW_26_in_ruleRelOp8978); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelOpAccess().getEqEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getRelOpAccess().getEqEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4094:6: (enumLiteral_1= '<>' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4094:6: (enumLiteral_1= '<>' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4094:8: enumLiteral_1= '<>'
                    {
                    enumLiteral_1=(Token)match(input,57,FOLLOW_57_in_ruleRelOp8995); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelOpAccess().getDiseqEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getRelOpAccess().getDiseqEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4100:6: (enumLiteral_2= '<=' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4100:6: (enumLiteral_2= '<=' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4100:8: enumLiteral_2= '<='
                    {
                    enumLiteral_2=(Token)match(input,58,FOLLOW_58_in_ruleRelOp9012); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelOpAccess().getLteEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getRelOpAccess().getLteEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4106:6: (enumLiteral_3= '<' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4106:6: (enumLiteral_3= '<' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4106:8: enumLiteral_3= '<'
                    {
                    enumLiteral_3=(Token)match(input,59,FOLLOW_59_in_ruleRelOp9029); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelOpAccess().getLtEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getRelOpAccess().getLtEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4112:6: (enumLiteral_4= '>=' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4112:6: (enumLiteral_4= '>=' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4112:8: enumLiteral_4= '>='
                    {
                    enumLiteral_4=(Token)match(input,60,FOLLOW_60_in_ruleRelOp9046); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelOpAccess().getGteEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getRelOpAccess().getGteEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4118:6: (enumLiteral_5= '>' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4118:6: (enumLiteral_5= '>' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4118:8: enumLiteral_5= '>'
                    {
                    enumLiteral_5=(Token)match(input,61,FOLLOW_61_in_ruleRelOp9063); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelOpAccess().getGtEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getRelOpAccess().getGtEnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelOp"


    // $ANTLR start "ruleNotOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4128:1: ruleNotOp returns [Enumerator current=null] : (enumLiteral_0= 'not' ) ;
    public final Enumerator ruleNotOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4130:28: ( (enumLiteral_0= 'not' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4131:1: (enumLiteral_0= 'not' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4131:1: (enumLiteral_0= 'not' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4131:3: enumLiteral_0= 'not'
            {
            enumLiteral_0=(Token)match(input,62,FOLLOW_62_in_ruleNotOp9107); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current = grammarAccess.getNotOpAccess().getLNotEnumLiteralDeclaration().getEnumLiteral().getInstance();
                      newLeafNode(enumLiteral_0, grammarAccess.getNotOpAccess().getLNotEnumLiteralDeclaration()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotOp"


    // $ANTLR start "ruleAddOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4141:1: ruleAddOp returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) ;
    public final Enumerator ruleAddOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4143:28: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4144:1: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4144:1: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==63) ) {
                alt67=1;
            }
            else if ( (LA67_0==64) ) {
                alt67=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 67, 0, input);

                throw nvae;
            }
            switch (alt67) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4144:2: (enumLiteral_0= '+' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4144:2: (enumLiteral_0= '+' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4144:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,63,FOLLOW_63_in_ruleAddOp9151); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getAddOpAccess().getPlusEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getAddOpAccess().getPlusEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4150:6: (enumLiteral_1= '-' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4150:6: (enumLiteral_1= '-' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4150:8: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,64,FOLLOW_64_in_ruleAddOp9168); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getAddOpAccess().getMinusEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getAddOpAccess().getMinusEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAddOp"


    // $ANTLR start "ruleMultOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4160:1: ruleMultOp returns [Enumerator current=null] : ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= 'div' ) | (enumLiteral_3= 'mod' ) ) ;
    public final Enumerator ruleMultOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4162:28: ( ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= 'div' ) | (enumLiteral_3= 'mod' ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4163:1: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= 'div' ) | (enumLiteral_3= 'mod' ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4163:1: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= 'div' ) | (enumLiteral_3= 'mod' ) )
            int alt68=4;
            switch ( input.LA(1) ) {
            case 65:
                {
                alt68=1;
                }
                break;
            case 36:
                {
                alt68=2;
                }
                break;
            case 66:
                {
                alt68=3;
                }
                break;
            case 67:
                {
                alt68=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 68, 0, input);

                throw nvae;
            }

            switch (alt68) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4163:2: (enumLiteral_0= '*' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4163:2: (enumLiteral_0= '*' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4163:4: enumLiteral_0= '*'
                    {
                    enumLiteral_0=(Token)match(input,65,FOLLOW_65_in_ruleMultOp9213); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getMultOpAccess().getTimesEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getMultOpAccess().getTimesEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4169:6: (enumLiteral_1= '/' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4169:6: (enumLiteral_1= '/' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4169:8: enumLiteral_1= '/'
                    {
                    enumLiteral_1=(Token)match(input,36,FOLLOW_36_in_ruleMultOp9230); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getMultOpAccess().getDivEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getMultOpAccess().getDivEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4175:6: (enumLiteral_2= 'div' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4175:6: (enumLiteral_2= 'div' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4175:8: enumLiteral_2= 'div'
                    {
                    enumLiteral_2=(Token)match(input,66,FOLLOW_66_in_ruleMultOp9247); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getMultOpAccess().getDivvEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getMultOpAccess().getDivvEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4181:6: (enumLiteral_3= 'mod' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4181:6: (enumLiteral_3= 'mod' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4181:8: enumLiteral_3= 'mod'
                    {
                    enumLiteral_3=(Token)match(input,67,FOLLOW_67_in_ruleMultOp9264); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getMultOpAccess().getModEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getMultOpAccess().getModEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultOp"


    // $ANTLR start "ruleWhenOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4191:1: ruleWhenOp returns [Enumerator current=null] : (enumLiteral_0= 'when' ) ;
    public final Enumerator ruleWhenOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4193:28: ( (enumLiteral_0= 'when' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4194:1: (enumLiteral_0= 'when' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4194:1: (enumLiteral_0= 'when' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4194:3: enumLiteral_0= 'when'
            {
            enumLiteral_0=(Token)match(input,68,FOLLOW_68_in_ruleWhenOp9308); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current = grammarAccess.getWhenOpAccess().getWhenEnumLiteralDeclaration().getEnumLiteral().getInstance();
                      newLeafNode(enumLiteral_0, grammarAccess.getWhenOpAccess().getWhenEnumLiteralDeclaration()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhenOp"


    // $ANTLR start "ruleNorOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4204:1: ruleNorOp returns [Enumerator current=null] : (enumLiteral_0= 'nor' ) ;
    public final Enumerator ruleNorOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4206:28: ( (enumLiteral_0= 'nor' ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4207:1: (enumLiteral_0= 'nor' )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4207:1: (enumLiteral_0= 'nor' )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4207:3: enumLiteral_0= 'nor'
            {
            enumLiteral_0=(Token)match(input,69,FOLLOW_69_in_ruleNorOp9351); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current = grammarAccess.getNorOpAccess().getNorEnumLiteralDeclaration().getEnumLiteral().getInstance();
                      newLeafNode(enumLiteral_0, grammarAccess.getNorOpAccess().getNorEnumLiteralDeclaration()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNorOp"


    // $ANTLR start "ruleUnOp"
    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4217:1: ruleUnOp returns [Enumerator current=null] : ( (enumLiteral_0= 'pre' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= 'current' ) ) ;
    public final Enumerator ruleUnOp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4219:28: ( ( (enumLiteral_0= 'pre' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= 'current' ) ) )
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4220:1: ( (enumLiteral_0= 'pre' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= 'current' ) )
            {
            // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4220:1: ( (enumLiteral_0= 'pre' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= 'current' ) )
            int alt69=3;
            switch ( input.LA(1) ) {
            case 70:
                {
                alt69=1;
                }
                break;
            case 64:
                {
                alt69=2;
                }
                break;
            case 71:
                {
                alt69=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 69, 0, input);

                throw nvae;
            }

            switch (alt69) {
                case 1 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4220:2: (enumLiteral_0= 'pre' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4220:2: (enumLiteral_0= 'pre' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4220:4: enumLiteral_0= 'pre'
                    {
                    enumLiteral_0=(Token)match(input,70,FOLLOW_70_in_ruleUnOp9395); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getUnOpAccess().getPreEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getUnOpAccess().getPreEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4226:6: (enumLiteral_1= '-' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4226:6: (enumLiteral_1= '-' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4226:8: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,64,FOLLOW_64_in_ruleUnOp9412); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getUnOpAccess().getNegEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getUnOpAccess().getNegEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4232:6: (enumLiteral_2= 'current' )
                    {
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4232:6: (enumLiteral_2= 'current' )
                    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:4232:8: enumLiteral_2= 'current'
                    {
                    enumLiteral_2=(Token)match(input,71,FOLLOW_71_in_ruleUnOp9429); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getUnOpAccess().getCurrentEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getUnOpAccess().getCurrentEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnOp"

    // $ANTLR start synpred1_InternalLustre
    public final void synpred1_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2327:2: ( ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2327:3: ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2327:3: ( () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2327:4: () ( ( ruleFbyOp ) ) ( ( ruleFbyExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2327:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2328:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2328:2: ( ( ruleFbyOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2329:1: ( ruleFbyOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2329:1: ( ruleFbyOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2330:1: ruleFbyOp
        {
        pushFollow(FOLLOW_ruleFbyOp_in_synpred1_InternalLustre4922);
        ruleFbyOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2332:2: ( ( ruleFbyExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2333:1: ( ruleFbyExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2333:1: ( ruleFbyExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2334:1: ruleFbyExpression
        {
        pushFollow(FOLLOW_ruleFbyExpression_in_synpred1_InternalLustre4931);
        ruleFbyExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred1_InternalLustre

    // $ANTLR start synpred2_InternalLustre
    public final void synpred2_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2408:2: ( ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2408:3: ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2408:3: ( () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2408:4: () ( ( ruleOrOp ) ) ( ( ruleAndExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2408:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2409:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2409:2: ( ( ruleOrOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2410:1: ( ruleOrOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2410:1: ( ruleOrOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2411:1: ruleOrOp
        {
        pushFollow(FOLLOW_ruleOrOp_in_synpred2_InternalLustre5096);
        ruleOrOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2413:2: ( ( ruleAndExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2414:1: ( ruleAndExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2414:1: ( ruleAndExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2415:1: ruleAndExpression
        {
        pushFollow(FOLLOW_ruleAndExpression_in_synpred2_InternalLustre5105);
        ruleAndExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred2_InternalLustre

    // $ANTLR start synpred3_InternalLustre
    public final void synpred3_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2489:2: ( ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2489:3: ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2489:3: ( () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2489:4: () ( ( ruleAndOp ) ) ( ( ruleImpliesExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2489:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2490:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2490:2: ( ( ruleAndOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2491:1: ( ruleAndOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2491:1: ( ruleAndOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2492:1: ruleAndOp
        {
        pushFollow(FOLLOW_ruleAndOp_in_synpred3_InternalLustre5270);
        ruleAndOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2494:2: ( ( ruleImpliesExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2495:1: ( ruleImpliesExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2495:1: ( ruleImpliesExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2496:1: ruleImpliesExpression
        {
        pushFollow(FOLLOW_ruleImpliesExpression_in_synpred3_InternalLustre5279);
        ruleImpliesExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred3_InternalLustre

    // $ANTLR start synpred4_InternalLustre
    public final void synpred4_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2570:2: ( ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2570:3: ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2570:3: ( () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2570:4: () ( ( ruleImpliesOp ) ) ( ( ruleImpliesExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2570:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2571:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2571:2: ( ( ruleImpliesOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2572:1: ( ruleImpliesOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2572:1: ( ruleImpliesOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2573:1: ruleImpliesOp
        {
        pushFollow(FOLLOW_ruleImpliesOp_in_synpred4_InternalLustre5444);
        ruleImpliesOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2575:2: ( ( ruleImpliesExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2576:1: ( ruleImpliesExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2576:1: ( ruleImpliesExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2577:1: ruleImpliesExpression
        {
        pushFollow(FOLLOW_ruleImpliesExpression_in_synpred4_InternalLustre5453);
        ruleImpliesExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred4_InternalLustre

    // $ANTLR start synpred5_InternalLustre
    public final void synpred5_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2651:2: ( ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2651:3: ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2651:3: ( () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2651:4: () ( ( ruleRelOp ) ) ( ( ruleNotExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2651:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2652:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2652:2: ( ( ruleRelOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2653:1: ( ruleRelOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2653:1: ( ruleRelOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2654:1: ruleRelOp
        {
        pushFollow(FOLLOW_ruleRelOp_in_synpred5_InternalLustre5618);
        ruleRelOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2656:2: ( ( ruleNotExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2657:1: ( ruleNotExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2657:1: ( ruleNotExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2658:1: ruleNotExpression
        {
        pushFollow(FOLLOW_ruleNotExpression_in_synpred5_InternalLustre5627);
        ruleNotExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred5_InternalLustre

    // $ANTLR start synpred6_InternalLustre
    public final void synpred6_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2805:2: ( ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2805:3: ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2805:3: ( () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2805:4: () ( ( ruleAddOp ) ) ( ( ruleMultExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2805:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2806:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2806:2: ( ( ruleAddOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2807:1: ( ruleAddOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2807:1: ( ruleAddOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2808:1: ruleAddOp
        {
        pushFollow(FOLLOW_ruleAddOp_in_synpred6_InternalLustre5943);
        ruleAddOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2810:2: ( ( ruleMultExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2811:1: ( ruleMultExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2811:1: ( ruleMultExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2812:1: ruleMultExpression
        {
        pushFollow(FOLLOW_ruleMultExpression_in_synpred6_InternalLustre5952);
        ruleMultExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred6_InternalLustre

    // $ANTLR start synpred7_InternalLustre
    public final void synpred7_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2886:2: ( ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2886:3: ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2886:3: ( () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2886:4: () ( ( ruleMultOp ) ) ( ( ruleWhenExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2886:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2887:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2887:2: ( ( ruleMultOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2888:1: ( ruleMultOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2888:1: ( ruleMultOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2889:1: ruleMultOp
        {
        pushFollow(FOLLOW_ruleMultOp_in_synpred7_InternalLustre6117);
        ruleMultOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2891:2: ( ( ruleWhenExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2892:1: ( ruleWhenExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2892:1: ( ruleWhenExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2893:1: ruleWhenExpression
        {
        pushFollow(FOLLOW_ruleWhenExpression_in_synpred7_InternalLustre6126);
        ruleWhenExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred7_InternalLustre

    // $ANTLR start synpred8_InternalLustre
    public final void synpred8_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2967:2: ( ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2967:3: ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2967:3: ( () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2967:4: () ( ( ruleWhenOp ) ) ( ( ruleNorExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2967:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2968:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2968:2: ( ( ruleWhenOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2969:1: ( ruleWhenOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2969:1: ( ruleWhenOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2970:1: ruleWhenOp
        {
        pushFollow(FOLLOW_ruleWhenOp_in_synpred8_InternalLustre6291);
        ruleWhenOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2972:2: ( ( ruleNorExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2973:1: ( ruleNorExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2973:1: ( ruleNorExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:2974:1: ruleNorExpression
        {
        pushFollow(FOLLOW_ruleNorExpression_in_synpred8_InternalLustre6300);
        ruleNorExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred8_InternalLustre

    // $ANTLR start synpred9_InternalLustre
    public final void synpred9_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3048:2: ( ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3048:3: ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3048:3: ( () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3048:4: () ( ( ruleNorOp ) ) ( ( ruleUnExpression ) )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3048:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3049:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3049:2: ( ( ruleNorOp ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3050:1: ( ruleNorOp )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3050:1: ( ruleNorOp )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3051:1: ruleNorOp
        {
        pushFollow(FOLLOW_ruleNorOp_in_synpred9_InternalLustre6465);
        ruleNorOp();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3053:2: ( ( ruleUnExpression ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3054:1: ( ruleUnExpression )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3054:1: ( ruleUnExpression )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3055:1: ruleUnExpression
        {
        pushFollow(FOLLOW_ruleUnExpression_in_synpred9_InternalLustre6474);
        ruleUnExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred9_InternalLustre

    // $ANTLR start synpred10_InternalLustre
    public final void synpred10_InternalLustre_fragment() throws RecognitionException {   
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3499:2: ( ( () ( ',' ( ( ruleFbyExpression ) ) )* ) )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3499:3: ( () ( ',' ( ( ruleFbyExpression ) ) )* )
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3499:3: ( () ( ',' ( ( ruleFbyExpression ) ) )* )
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3499:4: () ( ',' ( ( ruleFbyExpression ) ) )*
        {
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3499:4: ()
        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3500:1: 
        {
        }

        // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3500:2: ( ',' ( ( ruleFbyExpression ) ) )*
        loop70:
        do {
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==38) ) {
                alt70=1;
            }


            switch (alt70) {
        	case 1 :
        	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3500:4: ',' ( ( ruleFbyExpression ) )
        	    {
        	    match(input,38,FOLLOW_38_in_synpred10_InternalLustre7524); if (state.failed) return ;
        	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3501:1: ( ( ruleFbyExpression ) )
        	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3502:1: ( ruleFbyExpression )
        	    {
        	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3502:1: ( ruleFbyExpression )
        	    // ../geneauto.xtext.lustre/src-gen/geneauto/xtext/parser/antlr/internal/InternalLustre.g:3503:1: ruleFbyExpression
        	    {
        	    pushFollow(FOLLOW_ruleFbyExpression_in_synpred10_InternalLustre7531);
        	    ruleFbyExpression();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }


        	    }


        	    }
        	    break;

        	default :
        	    break loop70;
            }
        } while (true);


        }


        }
    }
    // $ANTLR end synpred10_InternalLustre

    // Delegated rules

    public final boolean synpred3_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred10_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred10_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred9_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred9_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred7_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred4_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred5_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred6_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred6_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred1_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_InternalLustre() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalLustre_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA15 dfa15 = new DFA15(this);
    static final String DFA2_eotS =
        "\13\uffff";
    static final String DFA2_eofS =
        "\1\1\12\uffff";
    static final String DFA2_minS =
        "\1\20\1\uffff\1\37\1\uffff\4\35\1\4\1\16\1\20";
    static final String DFA2_maxS =
        "\1\36\1\uffff\1\42\1\uffff\4\35\1\4\1\16\1\33";
    static final String DFA2_acceptS =
        "\1\uffff\1\2\1\uffff\1\1\7\uffff";
    static final String DFA2_specialS =
        "\13\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\1\7\uffff\1\1\1\3\1\uffff\1\1\2\uffff\1\2",
            "",
            "\1\4\1\5\1\6\1\7",
            "",
            "\1\10",
            "\1\10",
            "\1\10",
            "\1\10",
            "\1\11",
            "\1\12",
            "\1\1\10\uffff\1\3\1\uffff\1\1"
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "()* loopback of 99:3: ( (lv_declarations_1_0= ruleAbstractDeclaration ) )*";
        }
    }
    static final String DFA15_eotS =
        "\21\uffff";
    static final String DFA15_eofS =
        "\21\uffff";
    static final String DFA15_minS =
        "\1\31\1\37\1\5\4\35\1\32\1\4\1\6\1\16\5\uffff\1\31";
    static final String DFA15_maxS =
        "\1\36\1\42\1\5\4\35\1\32\1\4\1\47\1\16\5\uffff\1\31";
    static final String DFA15_acceptS =
        "\13\uffff\1\2\1\4\1\5\1\1\1\3\1\uffff";
    static final String DFA15_specialS =
        "\21\uffff}>";
    static final String[] DFA15_transitionS = {
            "\1\2\4\uffff\1\1",
            "\1\3\1\4\1\5\1\6",
            "\1\7",
            "\1\10",
            "\1\10",
            "\1\10",
            "\1\10",
            "\1\11",
            "\1\12",
            "\1\13\1\17\1\14\1\16\35\uffff\1\15",
            "\1\20",
            "",
            "",
            "",
            "",
            "",
            "\1\2"
    };

    static final short[] DFA15_eot = DFA.unpackEncodedString(DFA15_eotS);
    static final short[] DFA15_eof = DFA.unpackEncodedString(DFA15_eofS);
    static final char[] DFA15_min = DFA.unpackEncodedStringToUnsignedChars(DFA15_minS);
    static final char[] DFA15_max = DFA.unpackEncodedStringToUnsignedChars(DFA15_maxS);
    static final short[] DFA15_accept = DFA.unpackEncodedString(DFA15_acceptS);
    static final short[] DFA15_special = DFA.unpackEncodedString(DFA15_specialS);
    static final short[][] DFA15_transition;

    static {
        int numStates = DFA15_transitionS.length;
        DFA15_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA15_transition[i] = DFA.unpackEncodedString(DFA15_transitionS[i]);
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;
        }
        public String getDescription() {
            return "522:1: (this_IDeclaration_0= ruleIDeclaration | this_DDeclaration_1= ruleDDeclaration | this_RDeclaration_2= ruleRDeclaration | this_BDeclaration_3= ruleBDeclaration | this_ArrayDeclaration_4= ruleArrayDeclaration )";
        }
    }
 

    public static final BitSet FOLLOW_ruleProgram_in_entryRuleProgram75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProgram85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpen_in_ruleProgram131 = new BitSet(new long[]{0x000000004B018002L});
    public static final BitSet FOLLOW_ruleAbstractDeclaration_in_ruleProgram153 = new BitSet(new long[]{0x000000004B010002L});
    public static final BitSet FOLLOW_ruleFunction_in_ruleProgram176 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleProgram188 = new BitSet(new long[]{0x0000000049010002L});
    public static final BitSet FOLLOW_ruleNode_in_ruleProgram211 = new BitSet(new long[]{0x0000000048010002L});
    public static final BitSet FOLLOW_ruleOpen_in_entryRuleOpen248 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpen258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleOpen295 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleOpen312 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_14_in_ruleOpen330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNode_in_entryRuleNode370 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNode380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleNode426 = new BitSet(new long[]{0x0000000008010000L});
    public static final BitSet FOLLOW_ruleSpecificationLine_in_ruleNode448 = new BitSet(new long[]{0x0000000008010000L});
    public static final BitSet FOLLOW_16_in_ruleNode461 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleNode478 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleNode495 = new BitSet(new long[]{0x0000000042040020L});
    public static final BitSet FOLLOW_ruleInputs_in_ruleNode516 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleNode529 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleNode541 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleNode553 = new BitSet(new long[]{0x0000000042040020L});
    public static final BitSet FOLLOW_ruleOutputs_in_ruleNode574 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleNode587 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleNode599 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_20_in_ruleNode612 = new BitSet(new long[]{0x0000000042200020L});
    public static final BitSet FOLLOW_ruleLocals_in_ruleNode633 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleNode647 = new BitSet(new long[]{0x0000002040420020L});
    public static final BitSet FOLLOW_ruleBody_in_ruleNode668 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleNode681 = new BitSet(new long[]{0x0000000000804002L});
    public static final BitSet FOLLOW_14_in_ruleNode694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleNode712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_entryRuleFunction750 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunction760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleFunction797 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFunction814 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleFunction831 = new BitSet(new long[]{0x0000000042040020L});
    public static final BitSet FOLLOW_ruleInputs_in_ruleFunction852 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleFunction865 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleFunction877 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleFunction889 = new BitSet(new long[]{0x0000000042040020L});
    public static final BitSet FOLLOW_ruleOutputs_in_ruleFunction910 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleFunction923 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAbstractDeclaration_in_entryRuleAbstractDeclaration959 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAbstractDeclaration969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIDeclaration_in_ruleAbstractDeclaration1016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDDeclaration_in_ruleAbstractDeclaration1043 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRDeclaration_in_ruleAbstractDeclaration1070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBDeclaration_in_ruleAbstractDeclaration1097 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayDeclaration_in_ruleAbstractDeclaration1124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIDeclaration_in_entryRuleIDeclaration1159 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIDeclaration1169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleIDeclaration1215 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleIDeclaration1228 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleIDeclaration1249 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleIDeclaration1261 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleLUS_INT_in_ruleIDeclaration1282 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleIDeclaration1294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDDeclaration_in_entryRuleDDeclaration1330 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDDeclaration1340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleDDeclaration1386 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleDDeclaration1399 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleDDeclaration1420 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleDDeclaration1432 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_LUS_DOUBLE_in_ruleDDeclaration1449 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleDDeclaration1466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRDeclaration_in_entryRuleRDeclaration1502 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRDeclaration1512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleRDeclaration1558 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleRDeclaration1571 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleRDeclaration1592 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleRDeclaration1604 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_LUS_REAL_in_ruleRDeclaration1621 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleRDeclaration1638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBDeclaration_in_entryRuleBDeclaration1674 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBDeclaration1684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleBDeclaration1730 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleBDeclaration1743 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleBDeclaration1764 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleBDeclaration1776 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_RULE_LUS_BOOLEAN_in_ruleBDeclaration1793 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleBDeclaration1810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayDeclaration_in_entryRuleArrayDeclaration1846 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArrayDeclaration1856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleArrayDeclaration1902 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleArrayDeclaration1915 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleArrayDeclaration1936 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleArrayDeclaration1948 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_ruleArrayDefinition_in_ruleArrayDeclaration1969 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleArrayDeclaration1981 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLUS_INT_in_entryRuleLUS_INT2018 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLUS_INT2029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleLUS_INT2068 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSpecificationLine_in_entryRuleSpecificationLine2112 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSpecificationLine2122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleSpecificationLine2168 = new BitSet(new long[]{0x0000E00000000000L});
    public static final BitSet FOLLOW_ruleSpecificationType_in_ruleSpecificationLine2189 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleSpecificationLine2210 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleSpecificationLine2222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInputs_in_entryRuleInputs2258 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInputs2268 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariablesList_in_ruleInputs2314 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_14_in_ruleInputs2327 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariablesList_in_ruleInputs2348 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_14_in_ruleInputs2363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOutputs_in_entryRuleOutputs2401 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOutputs2411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariablesList_in_ruleOutputs2457 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_14_in_ruleOutputs2470 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariablesList_in_ruleOutputs2491 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_14_in_ruleOutputs2506 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLocals_in_entryRuleLocals2544 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLocals2554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariablesList_in_ruleLocals2610 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleLocals2623 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariablesList_in_ruleLocals2644 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleLocals2658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBody_in_entryRuleBody2696 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBody2706 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEquation_in_ruleBody2753 = new BitSet(new long[]{0x0000002050020022L});
    public static final BitSet FOLLOW_ruleAssert_in_ruleBody2780 = new BitSet(new long[]{0x0000002050020022L});
    public static final BitSet FOLLOW_ruleEquation_in_ruleBody2803 = new BitSet(new long[]{0x0000002050020022L});
    public static final BitSet FOLLOW_ruleAssert_in_ruleBody2830 = new BitSet(new long[]{0x0000002050020022L});
    public static final BitSet FOLLOW_ruleAnnotation_in_ruleBody2853 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_ruleAnnotation_in_entryRuleAnnotation2890 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnnotation2900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_ruleAnnotation2937 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_ruleBodyAnnotationIdentifier_in_ruleAnnotation2958 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleAnnotation2970 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleAnnotation2991 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleAnnotation3003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_entryRulePreTraceAnnotation3039 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePreTraceAnnotation3049 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rulePreTraceAnnotation3086 = new BitSet(new long[]{0x0000000780000000L});
    public static final BitSet FOLLOW_ruleTraceAnnotationElementType_in_rulePreTraceAnnotation3107 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_rulePreTraceAnnotation3119 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_rulePreTraceAnnotation3136 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_rulePreTraceAnnotation3153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTraceAnnotationElementType_in_entryRuleTraceAnnotationElementType3190 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTraceAnnotationElementType3201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleTraceAnnotationElementType3239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleTraceAnnotationElementType3258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_ruleTraceAnnotationElementType3277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_ruleTraceAnnotationElementType3296 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBodyAnnotationIdentifier_in_entryRuleBodyAnnotationIdentifier3337 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBodyAnnotationIdentifier3348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleBodyAnnotationIdentifier3386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnnotationIdentifier_in_ruleBodyAnnotationIdentifier3414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnnotationIdentifier_in_entryRuleAnnotationIdentifier3460 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnnotationIdentifier3471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleAnnotationIdentifier3509 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAnnotationIdentifier3525 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleAnnotationIdentifier3543 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_ruleEquation_in_entryRuleEquation3585 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEquation3595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleEquation3650 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_ruleLeftVariables_in_ruleEquation3673 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_17_in_ruleEquation3692 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleLeftVariables_in_ruleEquation3713 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleEquation3725 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleEquation3739 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleEquation3760 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleEquation3772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssert_in_entryRuleAssert3808 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAssert3818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleAssert3864 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleAssert3885 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleAssert3897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLeftVariables_in_entryRuleLeftVariables3933 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLeftVariables3943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableCall_in_ruleLeftVariables3989 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_38_in_ruleLeftVariables4002 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleVariableCall_in_ruleLeftVariables4023 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_ruleVariablesList_in_entryRuleVariablesList4061 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariablesList4071 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePreTraceAnnotation_in_ruleVariablesList4117 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_25_in_ruleVariablesList4136 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleVariablesList4171 = new BitSet(new long[]{0x0000004020000000L});
    public static final BitSet FOLLOW_38_in_ruleVariablesList4184 = new BitSet(new long[]{0x0000000042000020L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleVariablesList4205 = new BitSet(new long[]{0x0000004020000000L});
    public static final BitSet FOLLOW_29_in_ruleVariablesList4219 = new BitSet(new long[]{0x000F000000000000L});
    public static final BitSet FOLLOW_ruleDataType_in_ruleVariablesList4240 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableCall_in_entryRuleVariableCall4276 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariableCall4286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVariableCall4331 = new BitSet(new long[]{0x0000008000000002L});
    public static final BitSet FOLLOW_39_in_ruleVariableCall4344 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleLUS_INT_in_ruleVariableCall4365 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleVariableCall4377 = new BitSet(new long[]{0x0000008000000002L});
    public static final BitSet FOLLOW_39_in_ruleVariableCall4390 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleLUS_INT_in_ruleVariableCall4411 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleVariableCall4423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_entryRuleVariable4463 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariable4473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVariable4514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDataType_in_entryRuleDataType4554 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDataType4564 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicType_in_ruleDataType4610 = new BitSet(new long[]{0x0000020000000002L});
    public static final BitSet FOLLOW_41_in_ruleDataType4623 = new BitSet(new long[]{0x0000000000000220L});
    public static final BitSet FOLLOW_ruleDataTypeDimValue_in_ruleDataType4644 = new BitSet(new long[]{0x0000020000000002L});
    public static final BitSet FOLLOW_41_in_ruleDataType4657 = new BitSet(new long[]{0x0000000000000220L});
    public static final BitSet FOLLOW_ruleDataTypeDimValue_in_ruleDataType4678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDataTypeDimValue_in_entryRuleDataTypeDimValue4718 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDataTypeDimValue4728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLUS_INT_in_ruleDataTypeDimValue4784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDataTypeDimValue4810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_entryRuleFbyExpression4847 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFbyExpression4857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleFbyExpression4904 = new BitSet(new long[]{0x0010000000000002L});
    public static final BitSet FOLLOW_ruleFbyOp_in_ruleFbyExpression4961 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleFbyExpression4982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrExpression_in_entryRuleOrExpression5021 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrExpression5031 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleOrExpression5078 = new BitSet(new long[]{0x0060000000000002L});
    public static final BitSet FOLLOW_ruleOrOp_in_ruleOrExpression5135 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleOrExpression5156 = new BitSet(new long[]{0x0060000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_entryRuleAndExpression5195 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAndExpression5205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImpliesExpression_in_ruleAndExpression5252 = new BitSet(new long[]{0x0080000000000002L});
    public static final BitSet FOLLOW_ruleAndOp_in_ruleAndExpression5309 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleImpliesExpression_in_ruleAndExpression5330 = new BitSet(new long[]{0x0080000000000002L});
    public static final BitSet FOLLOW_ruleImpliesExpression_in_entryRuleImpliesExpression5369 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImpliesExpression5379 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelExpression_in_ruleImpliesExpression5426 = new BitSet(new long[]{0x0100000000000002L});
    public static final BitSet FOLLOW_ruleImpliesOp_in_ruleImpliesExpression5483 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleImpliesExpression_in_ruleImpliesExpression5504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelExpression_in_entryRuleRelExpression5543 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRelExpression5553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNotExpression_in_ruleRelExpression5600 = new BitSet(new long[]{0x3E00000004000002L});
    public static final BitSet FOLLOW_ruleRelOp_in_ruleRelExpression5657 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleNotExpression_in_ruleRelExpression5678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNotExpression_in_entryRuleNotExpression5717 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNotExpression5727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAddExpression_in_ruleNotExpression5774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNotOp_in_ruleNotExpression5810 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleNotExpression_in_ruleNotExpression5831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAddExpression_in_entryRuleAddExpression5868 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAddExpression5878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultExpression_in_ruleAddExpression5925 = new BitSet(new long[]{0x8000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleAddOp_in_ruleAddExpression5982 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleMultExpression_in_ruleAddExpression6003 = new BitSet(new long[]{0x8000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleMultExpression_in_entryRuleMultExpression6042 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMultExpression6052 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenExpression_in_ruleMultExpression6099 = new BitSet(new long[]{0x0000001000000002L,0x000000000000000EL});
    public static final BitSet FOLLOW_ruleMultOp_in_ruleMultExpression6156 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleWhenExpression_in_ruleMultExpression6177 = new BitSet(new long[]{0x0000001000000002L,0x000000000000000EL});
    public static final BitSet FOLLOW_ruleWhenExpression_in_entryRuleWhenExpression6216 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhenExpression6226 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNorExpression_in_ruleWhenExpression6273 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000010L});
    public static final BitSet FOLLOW_ruleWhenOp_in_ruleWhenExpression6330 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleNorExpression_in_ruleWhenExpression6351 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000010L});
    public static final BitSet FOLLOW_ruleNorExpression_in_entryRuleNorExpression6390 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNorExpression6400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnExpression_in_ruleNorExpression6447 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000020L});
    public static final BitSet FOLLOW_ruleNorOp_in_ruleNorExpression6504 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleUnExpression_in_ruleNorExpression6525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnExpression_in_entryRuleUnExpression6564 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnExpression6574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleUnExpression6621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnOp_in_ruleUnExpression6657 = new BitSet(new long[]{0x00000480000203E0L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleUnExpression6678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_entryRuleBaseExpression6715 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBaseExpression6725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTupleExpression_in_ruleBaseExpression6772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralExpression_in_ruleBaseExpression6799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallExpression_in_ruleBaseExpression6826 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIteExpression_in_ruleBaseExpression6853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayDefinition_in_ruleBaseExpression6880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayDefinition_in_entryRuleArrayDefinition6915 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArrayDefinition6925 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayExpression_in_ruleArrayDefinition6972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMatrixExpression_in_ruleArrayDefinition6999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMatrixExpression_in_entryRuleMatrixExpression7034 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMatrixExpression7044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleMatrixExpression7081 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_ruleArrayExpression_in_ruleMatrixExpression7102 = new BitSet(new long[]{0x0000014000000000L});
    public static final BitSet FOLLOW_38_in_ruleMatrixExpression7115 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_ruleArrayExpression_in_ruleMatrixExpression7136 = new BitSet(new long[]{0x0000014000000000L});
    public static final BitSet FOLLOW_40_in_ruleMatrixExpression7150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArrayExpression_in_entryRuleArrayExpression7186 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArrayExpression7196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleArrayExpression7233 = new BitSet(new long[]{0x00000000000003E0L});
    public static final BitSet FOLLOW_ruleLiteralExpression_in_ruleArrayExpression7254 = new BitSet(new long[]{0x0000014000000000L});
    public static final BitSet FOLLOW_38_in_ruleArrayExpression7267 = new BitSet(new long[]{0x00000000000003E0L});
    public static final BitSet FOLLOW_ruleLiteralExpression_in_ruleArrayExpression7288 = new BitSet(new long[]{0x0000014000000000L});
    public static final BitSet FOLLOW_40_in_ruleArrayExpression7302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableExpression_in_entryRuleVariableExpression7338 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariableExpression7348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableCall_in_ruleVariableExpression7403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTupleExpression_in_entryRuleTupleExpression7439 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTupleExpression7449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleTupleExpression7486 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleTupleExpression7508 = new BitSet(new long[]{0x0000004000040000L});
    public static final BitSet FOLLOW_38_in_ruleTupleExpression7555 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleTupleExpression7576 = new BitSet(new long[]{0x0000004000040000L});
    public static final BitSet FOLLOW_18_in_ruleTupleExpression7593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteralExpression_in_entryRuleLiteralExpression7629 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLiteralExpression7639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerLiteralExpression_in_ruleLiteralExpression7686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRealLiteralExpression_in_ruleLiteralExpression7713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDoubleLiteralExpression_in_ruleLiteralExpression7740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanLiteralExpression_in_ruleLiteralExpression7767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableExpression_in_ruleLiteralExpression7794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerLiteralExpression_in_entryRuleIntegerLiteralExpression7829 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerLiteralExpression7839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLUS_INT_in_ruleIntegerLiteralExpression7894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRealLiteralExpression_in_entryRuleRealLiteralExpression7930 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRealLiteralExpression7940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LUS_REAL_in_ruleRealLiteralExpression7991 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDoubleLiteralExpression_in_entryRuleDoubleLiteralExpression8032 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDoubleLiteralExpression8042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LUS_DOUBLE_in_ruleDoubleLiteralExpression8093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanLiteralExpression_in_entryRuleBooleanLiteralExpression8134 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanLiteralExpression8144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_LUS_BOOLEAN_in_ruleBooleanLiteralExpression8195 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallExpression_in_entryRuleCallExpression8236 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCallExpression8246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCallExpression8300 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleCallExpression8312 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleCallExpression8333 = new BitSet(new long[]{0x0000004000040000L});
    public static final BitSet FOLLOW_38_in_ruleCallExpression8346 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleCallExpression8367 = new BitSet(new long[]{0x0000004000040000L});
    public static final BitSet FOLLOW_18_in_ruleCallExpression8381 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIteExpression_in_entryRuleIteExpression8417 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIteExpression8427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleIteExpression8464 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleIteExpression8485 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_ruleIteExpression8497 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleIteExpression8518 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44_in_ruleIteExpression8539 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_ruleIteExpression8561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleSpecificationType8612 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleSpecificationType8629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_ruleSpecificationType8646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleBasicType8691 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleBasicType8708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleBasicType8725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_ruleBasicType8742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_ruleFbyOp8786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_ruleOrOp8830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruleOrOp8847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_ruleAndOp8891 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_ruleImpliesOp8934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleRelOp8978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_57_in_ruleRelOp8995 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_58_in_ruleRelOp9012 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_ruleRelOp9029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_ruleRelOp9046 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_ruleRelOp9063 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_ruleNotOp9107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_63_in_ruleAddOp9151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_ruleAddOp9168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_ruleMultOp9213 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleMultOp9230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_ruleMultOp9247 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_67_in_ruleMultOp9264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_ruleWhenOp9308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_69_in_ruleNorOp9351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_70_in_ruleUnOp9395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_ruleUnOp9412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_71_in_ruleUnOp9429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFbyOp_in_synpred1_InternalLustre4922 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_synpred1_InternalLustre4931 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrOp_in_synpred2_InternalLustre5096 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleAndExpression_in_synpred2_InternalLustre5105 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAndOp_in_synpred3_InternalLustre5270 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleImpliesExpression_in_synpred3_InternalLustre5279 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImpliesOp_in_synpred4_InternalLustre5444 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleImpliesExpression_in_synpred4_InternalLustre5453 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelOp_in_synpred5_InternalLustre5618 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleNotExpression_in_synpred5_InternalLustre5627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAddOp_in_synpred6_InternalLustre5943 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleMultExpression_in_synpred6_InternalLustre5952 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultOp_in_synpred7_InternalLustre6117 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleWhenExpression_in_synpred7_InternalLustre6126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenOp_in_synpred8_InternalLustre6291 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleNorExpression_in_synpred8_InternalLustre6300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNorOp_in_synpred9_InternalLustre6465 = new BitSet(new long[]{0x00000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleUnExpression_in_synpred9_InternalLustre6474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_synpred10_InternalLustre7524 = new BitSet(new long[]{0x40000480000203E0L,0x00000000000000C1L});
    public static final BitSet FOLLOW_ruleFbyExpression_in_synpred10_InternalLustre7531 = new BitSet(new long[]{0x0000004000000002L});

}