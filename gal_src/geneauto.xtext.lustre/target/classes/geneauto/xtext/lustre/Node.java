/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.Node#getPreTraceAnnotation <em>Pre Trace Annotation</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.Node#getSpecifications <em>Specifications</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.Node#getLocals <em>Locals</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.Node#getBody <em>Body</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getNode()
 * @model
 * @generated
 */
public interface Node extends CallableElement
{
  /**
   * Returns the value of the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pre Trace Annotation</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pre Trace Annotation</em>' containment reference.
   * @see #setPreTraceAnnotation(PreTraceAnnotation)
   * @see geneauto.xtext.lustre.LustrePackage#getNode_PreTraceAnnotation()
   * @model containment="true"
   * @generated
   */
  PreTraceAnnotation getPreTraceAnnotation();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.Node#getPreTraceAnnotation <em>Pre Trace Annotation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pre Trace Annotation</em>' containment reference.
   * @see #getPreTraceAnnotation()
   * @generated
   */
  void setPreTraceAnnotation(PreTraceAnnotation value);

  /**
   * Returns the value of the '<em><b>Specifications</b></em>' containment reference list.
   * The list contents are of type {@link geneauto.xtext.lustre.SpecificationLine}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specifications</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specifications</em>' containment reference list.
   * @see geneauto.xtext.lustre.LustrePackage#getNode_Specifications()
   * @model containment="true"
   * @generated
   */
  EList<SpecificationLine> getSpecifications();

  /**
   * Returns the value of the '<em><b>Locals</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Locals</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Locals</em>' containment reference.
   * @see #setLocals(Locals)
   * @see geneauto.xtext.lustre.LustrePackage#getNode_Locals()
   * @model containment="true"
   * @generated
   */
  Locals getLocals();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.Node#getLocals <em>Locals</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Locals</em>' containment reference.
   * @see #getLocals()
   * @generated
   */
  void setLocals(Locals value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(Body)
   * @see geneauto.xtext.lustre.LustrePackage#getNode_Body()
   * @model containment="true"
   * @generated
   */
  Body getBody();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.Node#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(Body value);

} // Node
