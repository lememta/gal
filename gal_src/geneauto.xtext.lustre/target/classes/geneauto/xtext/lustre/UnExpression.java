/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Un Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.UnExpression#getOp <em>Op</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.UnExpression#getUexp <em>Uexp</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getUnExpression()
 * @model
 * @generated
 */
public interface UnExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute.
   * The literals are from the enumeration {@link geneauto.xtext.lustre.UnOp}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.UnOp
   * @see #setOp(UnOp)
   * @see geneauto.xtext.lustre.LustrePackage#getUnExpression_Op()
   * @model
   * @generated
   */
  UnOp getOp();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.UnExpression#getOp <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.UnOp
   * @see #getOp()
   * @generated
   */
  void setOp(UnOp value);

  /**
   * Returns the value of the '<em><b>Uexp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uexp</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uexp</em>' containment reference.
   * @see #setUexp(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getUnExpression_Uexp()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getUexp();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.UnExpression#getUexp <em>Uexp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uexp</em>' containment reference.
   * @see #getUexp()
   * @generated
   */
  void setUexp(AbstractExpression value);

} // UnExpression
