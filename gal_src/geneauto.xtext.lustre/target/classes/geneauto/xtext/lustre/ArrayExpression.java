/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.xtext.lustre.LustrePackage#getArrayExpression()
 * @model
 * @generated
 */
public interface ArrayExpression extends ArrayDefinition
{
} // ArrayExpression
