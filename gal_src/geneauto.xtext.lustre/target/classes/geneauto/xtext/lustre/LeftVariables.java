/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Left Variables</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.LeftVariables#getVariables <em>Variables</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getLeftVariables()
 * @model
 * @generated
 */
public interface LeftVariables extends EObject
{
  /**
   * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
   * The list contents are of type {@link geneauto.xtext.lustre.VariableCall}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variables</em>' containment reference list.
   * @see geneauto.xtext.lustre.LustrePackage#getLeftVariables_Variables()
   * @model containment="true"
   * @generated
   */
  EList<VariableCall> getVariables();

} // LeftVariables
