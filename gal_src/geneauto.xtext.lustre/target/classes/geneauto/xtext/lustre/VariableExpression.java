/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.VariableExpression#getVariable <em>Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getVariableExpression()
 * @model
 * @generated
 */
public interface VariableExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' containment reference.
   * @see #setVariable(VariableCall)
   * @see geneauto.xtext.lustre.LustrePackage#getVariableExpression_Variable()
   * @model containment="true"
   * @generated
   */
  VariableCall getVariable();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.VariableExpression#getVariable <em>Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' containment reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(VariableCall value);

} // VariableExpression
