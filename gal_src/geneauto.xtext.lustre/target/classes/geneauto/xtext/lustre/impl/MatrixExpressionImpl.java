/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.MatrixExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Matrix Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class MatrixExpressionImpl extends ArrayDefinitionImpl implements MatrixExpression
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MatrixExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.MATRIX_EXPRESSION;
  }

} //MatrixExpressionImpl
