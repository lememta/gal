/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.AbstractExpression;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.SpecificationLine;
import geneauto.xtext.lustre.SpecificationType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Specification Line</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.SpecificationLineImpl#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.SpecificationLineImpl#getSpecLine <em>Spec Line</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SpecificationLineImpl extends MinimalEObjectImpl.Container implements SpecificationLine
{
  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final SpecificationType TYPE_EDEFAULT = SpecificationType.REQUIRES;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected SpecificationType type = TYPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getSpecLine() <em>Spec Line</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpecLine()
   * @generated
   * @ordered
   */
  protected AbstractExpression specLine;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SpecificationLineImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.SPECIFICATION_LINE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecificationType getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(SpecificationType newType)
  {
    SpecificationType oldType = type;
    type = newType == null ? TYPE_EDEFAULT : newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.SPECIFICATION_LINE__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractExpression getSpecLine()
  {
    return specLine;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSpecLine(AbstractExpression newSpecLine, NotificationChain msgs)
  {
    AbstractExpression oldSpecLine = specLine;
    specLine = newSpecLine;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.SPECIFICATION_LINE__SPEC_LINE, oldSpecLine, newSpecLine);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSpecLine(AbstractExpression newSpecLine)
  {
    if (newSpecLine != specLine)
    {
      NotificationChain msgs = null;
      if (specLine != null)
        msgs = ((InternalEObject)specLine).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.SPECIFICATION_LINE__SPEC_LINE, null, msgs);
      if (newSpecLine != null)
        msgs = ((InternalEObject)newSpecLine).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.SPECIFICATION_LINE__SPEC_LINE, null, msgs);
      msgs = basicSetSpecLine(newSpecLine, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.SPECIFICATION_LINE__SPEC_LINE, newSpecLine, newSpecLine));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.SPECIFICATION_LINE__SPEC_LINE:
        return basicSetSpecLine(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.SPECIFICATION_LINE__TYPE:
        return getType();
      case LustrePackage.SPECIFICATION_LINE__SPEC_LINE:
        return getSpecLine();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.SPECIFICATION_LINE__TYPE:
        setType((SpecificationType)newValue);
        return;
      case LustrePackage.SPECIFICATION_LINE__SPEC_LINE:
        setSpecLine((AbstractExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.SPECIFICATION_LINE__TYPE:
        setType(TYPE_EDEFAULT);
        return;
      case LustrePackage.SPECIFICATION_LINE__SPEC_LINE:
        setSpecLine((AbstractExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.SPECIFICATION_LINE__TYPE:
        return type != TYPE_EDEFAULT;
      case LustrePackage.SPECIFICATION_LINE__SPEC_LINE:
        return specLine != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (type: ");
    result.append(type);
    result.append(')');
    return result.toString();
  }

} //SpecificationLineImpl
