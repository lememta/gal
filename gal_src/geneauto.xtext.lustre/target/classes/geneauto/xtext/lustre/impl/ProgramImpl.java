/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.AbstractDeclaration;
import geneauto.xtext.lustre.Function;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.Node;
import geneauto.xtext.lustre.Open;
import geneauto.xtext.lustre.Program;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.ProgramImpl#getLibraries <em>Libraries</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.ProgramImpl#getDeclarations <em>Declarations</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.ProgramImpl#getFunctions <em>Functions</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.ProgramImpl#getNodes <em>Nodes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProgramImpl extends MinimalEObjectImpl.Container implements Program
{
  /**
   * The cached value of the '{@link #getLibraries() <em>Libraries</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLibraries()
   * @generated
   * @ordered
   */
  protected EList<Open> libraries;

  /**
   * The cached value of the '{@link #getDeclarations() <em>Declarations</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeclarations()
   * @generated
   * @ordered
   */
  protected EList<AbstractDeclaration> declarations;

  /**
   * The cached value of the '{@link #getFunctions() <em>Functions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunctions()
   * @generated
   * @ordered
   */
  protected EList<Function> functions;

  /**
   * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNodes()
   * @generated
   * @ordered
   */
  protected EList<Node> nodes;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProgramImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.PROGRAM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Open> getLibraries()
  {
    if (libraries == null)
    {
      libraries = new EObjectContainmentEList<Open>(Open.class, this, LustrePackage.PROGRAM__LIBRARIES);
    }
    return libraries;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AbstractDeclaration> getDeclarations()
  {
    if (declarations == null)
    {
      declarations = new EObjectContainmentEList<AbstractDeclaration>(AbstractDeclaration.class, this, LustrePackage.PROGRAM__DECLARATIONS);
    }
    return declarations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Function> getFunctions()
  {
    if (functions == null)
    {
      functions = new EObjectContainmentEList<Function>(Function.class, this, LustrePackage.PROGRAM__FUNCTIONS);
    }
    return functions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Node> getNodes()
  {
    if (nodes == null)
    {
      nodes = new EObjectContainmentEList<Node>(Node.class, this, LustrePackage.PROGRAM__NODES);
    }
    return nodes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.PROGRAM__LIBRARIES:
        return ((InternalEList<?>)getLibraries()).basicRemove(otherEnd, msgs);
      case LustrePackage.PROGRAM__DECLARATIONS:
        return ((InternalEList<?>)getDeclarations()).basicRemove(otherEnd, msgs);
      case LustrePackage.PROGRAM__FUNCTIONS:
        return ((InternalEList<?>)getFunctions()).basicRemove(otherEnd, msgs);
      case LustrePackage.PROGRAM__NODES:
        return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.PROGRAM__LIBRARIES:
        return getLibraries();
      case LustrePackage.PROGRAM__DECLARATIONS:
        return getDeclarations();
      case LustrePackage.PROGRAM__FUNCTIONS:
        return getFunctions();
      case LustrePackage.PROGRAM__NODES:
        return getNodes();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.PROGRAM__LIBRARIES:
        getLibraries().clear();
        getLibraries().addAll((Collection<? extends Open>)newValue);
        return;
      case LustrePackage.PROGRAM__DECLARATIONS:
        getDeclarations().clear();
        getDeclarations().addAll((Collection<? extends AbstractDeclaration>)newValue);
        return;
      case LustrePackage.PROGRAM__FUNCTIONS:
        getFunctions().clear();
        getFunctions().addAll((Collection<? extends Function>)newValue);
        return;
      case LustrePackage.PROGRAM__NODES:
        getNodes().clear();
        getNodes().addAll((Collection<? extends Node>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.PROGRAM__LIBRARIES:
        getLibraries().clear();
        return;
      case LustrePackage.PROGRAM__DECLARATIONS:
        getDeclarations().clear();
        return;
      case LustrePackage.PROGRAM__FUNCTIONS:
        getFunctions().clear();
        return;
      case LustrePackage.PROGRAM__NODES:
        getNodes().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.PROGRAM__LIBRARIES:
        return libraries != null && !libraries.isEmpty();
      case LustrePackage.PROGRAM__DECLARATIONS:
        return declarations != null && !declarations.isEmpty();
      case LustrePackage.PROGRAM__FUNCTIONS:
        return functions != null && !functions.isEmpty();
      case LustrePackage.PROGRAM__NODES:
        return nodes != null && !nodes.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ProgramImpl
