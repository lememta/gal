/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.Body;
import geneauto.xtext.lustre.Locals;
import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.Node;
import geneauto.xtext.lustre.PreTraceAnnotation;
import geneauto.xtext.lustre.SpecificationLine;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.impl.NodeImpl#getPreTraceAnnotation <em>Pre Trace Annotation</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.NodeImpl#getSpecifications <em>Specifications</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.NodeImpl#getLocals <em>Locals</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.impl.NodeImpl#getBody <em>Body</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NodeImpl extends CallableElementImpl implements Node
{
  /**
   * The cached value of the '{@link #getPreTraceAnnotation() <em>Pre Trace Annotation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPreTraceAnnotation()
   * @generated
   * @ordered
   */
  protected PreTraceAnnotation preTraceAnnotation;

  /**
   * The cached value of the '{@link #getSpecifications() <em>Specifications</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpecifications()
   * @generated
   * @ordered
   */
  protected EList<SpecificationLine> specifications;

  /**
   * The cached value of the '{@link #getLocals() <em>Locals</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocals()
   * @generated
   * @ordered
   */
  protected Locals locals;

  /**
   * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBody()
   * @generated
   * @ordered
   */
  protected Body body;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NodeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LustrePackage.Literals.NODE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PreTraceAnnotation getPreTraceAnnotation()
  {
    return preTraceAnnotation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation, NotificationChain msgs)
  {
    PreTraceAnnotation oldPreTraceAnnotation = preTraceAnnotation;
    preTraceAnnotation = newPreTraceAnnotation;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.NODE__PRE_TRACE_ANNOTATION, oldPreTraceAnnotation, newPreTraceAnnotation);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPreTraceAnnotation(PreTraceAnnotation newPreTraceAnnotation)
  {
    if (newPreTraceAnnotation != preTraceAnnotation)
    {
      NotificationChain msgs = null;
      if (preTraceAnnotation != null)
        msgs = ((InternalEObject)preTraceAnnotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.NODE__PRE_TRACE_ANNOTATION, null, msgs);
      if (newPreTraceAnnotation != null)
        msgs = ((InternalEObject)newPreTraceAnnotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.NODE__PRE_TRACE_ANNOTATION, null, msgs);
      msgs = basicSetPreTraceAnnotation(newPreTraceAnnotation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.NODE__PRE_TRACE_ANNOTATION, newPreTraceAnnotation, newPreTraceAnnotation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SpecificationLine> getSpecifications()
  {
    if (specifications == null)
    {
      specifications = new EObjectContainmentEList<SpecificationLine>(SpecificationLine.class, this, LustrePackage.NODE__SPECIFICATIONS);
    }
    return specifications;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Locals getLocals()
  {
    return locals;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLocals(Locals newLocals, NotificationChain msgs)
  {
    Locals oldLocals = locals;
    locals = newLocals;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.NODE__LOCALS, oldLocals, newLocals);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLocals(Locals newLocals)
  {
    if (newLocals != locals)
    {
      NotificationChain msgs = null;
      if (locals != null)
        msgs = ((InternalEObject)locals).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.NODE__LOCALS, null, msgs);
      if (newLocals != null)
        msgs = ((InternalEObject)newLocals).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.NODE__LOCALS, null, msgs);
      msgs = basicSetLocals(newLocals, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.NODE__LOCALS, newLocals, newLocals));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Body getBody()
  {
    return body;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBody(Body newBody, NotificationChain msgs)
  {
    Body oldBody = body;
    body = newBody;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LustrePackage.NODE__BODY, oldBody, newBody);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBody(Body newBody)
  {
    if (newBody != body)
    {
      NotificationChain msgs = null;
      if (body != null)
        msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LustrePackage.NODE__BODY, null, msgs);
      if (newBody != null)
        msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LustrePackage.NODE__BODY, null, msgs);
      msgs = basicSetBody(newBody, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LustrePackage.NODE__BODY, newBody, newBody));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LustrePackage.NODE__PRE_TRACE_ANNOTATION:
        return basicSetPreTraceAnnotation(null, msgs);
      case LustrePackage.NODE__SPECIFICATIONS:
        return ((InternalEList<?>)getSpecifications()).basicRemove(otherEnd, msgs);
      case LustrePackage.NODE__LOCALS:
        return basicSetLocals(null, msgs);
      case LustrePackage.NODE__BODY:
        return basicSetBody(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LustrePackage.NODE__PRE_TRACE_ANNOTATION:
        return getPreTraceAnnotation();
      case LustrePackage.NODE__SPECIFICATIONS:
        return getSpecifications();
      case LustrePackage.NODE__LOCALS:
        return getLocals();
      case LustrePackage.NODE__BODY:
        return getBody();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LustrePackage.NODE__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)newValue);
        return;
      case LustrePackage.NODE__SPECIFICATIONS:
        getSpecifications().clear();
        getSpecifications().addAll((Collection<? extends SpecificationLine>)newValue);
        return;
      case LustrePackage.NODE__LOCALS:
        setLocals((Locals)newValue);
        return;
      case LustrePackage.NODE__BODY:
        setBody((Body)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.NODE__PRE_TRACE_ANNOTATION:
        setPreTraceAnnotation((PreTraceAnnotation)null);
        return;
      case LustrePackage.NODE__SPECIFICATIONS:
        getSpecifications().clear();
        return;
      case LustrePackage.NODE__LOCALS:
        setLocals((Locals)null);
        return;
      case LustrePackage.NODE__BODY:
        setBody((Body)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LustrePackage.NODE__PRE_TRACE_ANNOTATION:
        return preTraceAnnotation != null;
      case LustrePackage.NODE__SPECIFICATIONS:
        return specifications != null && !specifications.isEmpty();
      case LustrePackage.NODE__LOCALS:
        return locals != null;
      case LustrePackage.NODE__BODY:
        return body != null;
    }
    return super.eIsSet(featureID);
  }

} //NodeImpl
