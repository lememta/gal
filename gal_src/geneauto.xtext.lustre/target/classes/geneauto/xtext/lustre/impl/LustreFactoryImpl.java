/**
 */
package geneauto.xtext.lustre.impl;

import geneauto.xtext.lustre.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LustreFactoryImpl extends EFactoryImpl implements LustreFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static LustreFactory init()
  {
    try
    {
      LustreFactory theLustreFactory = (LustreFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.xtext.geneauto/Lustre"); 
      if (theLustreFactory != null)
      {
        return theLustreFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new LustreFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LustreFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case LustrePackage.PROGRAM: return createProgram();
      case LustrePackage.OPEN: return createOpen();
      case LustrePackage.CALLABLE_ELEMENT: return createCallableElement();
      case LustrePackage.NODE: return createNode();
      case LustrePackage.FUNCTION: return createFunction();
      case LustrePackage.ABSTRACT_DECLARATION: return createAbstractDeclaration();
      case LustrePackage.IDECLARATION: return createIDeclaration();
      case LustrePackage.DDECLARATION: return createDDeclaration();
      case LustrePackage.RDECLARATION: return createRDeclaration();
      case LustrePackage.BDECLARATION: return createBDeclaration();
      case LustrePackage.ARRAY_DECLARATION: return createArrayDeclaration();
      case LustrePackage.SPECIFICATION_LINE: return createSpecificationLine();
      case LustrePackage.INPUTS: return createInputs();
      case LustrePackage.OUTPUTS: return createOutputs();
      case LustrePackage.LOCALS: return createLocals();
      case LustrePackage.BODY: return createBody();
      case LustrePackage.ANNOTATION: return createAnnotation();
      case LustrePackage.PRE_TRACE_ANNOTATION: return createPreTraceAnnotation();
      case LustrePackage.EQUATION: return createEquation();
      case LustrePackage.ASSERT: return createAssert();
      case LustrePackage.LEFT_VARIABLES: return createLeftVariables();
      case LustrePackage.VARIABLES_LIST: return createVariablesList();
      case LustrePackage.VARIABLE_CALL: return createVariableCall();
      case LustrePackage.VARIABLE: return createVariable();
      case LustrePackage.DATA_TYPE: return createDataType();
      case LustrePackage.DATA_TYPE_DIM_VALUE: return createDataTypeDimValue();
      case LustrePackage.ABSTRACT_EXPRESSION: return createAbstractExpression();
      case LustrePackage.ARRAY_DEFINITION: return createArrayDefinition();
      case LustrePackage.MATRIX_EXPRESSION: return createMatrixExpression();
      case LustrePackage.ARRAY_EXPRESSION: return createArrayExpression();
      case LustrePackage.ITE_EXPRESSION: return createIteExpression();
      case LustrePackage.FBY_EXPRESSION: return createFbyExpression();
      case LustrePackage.OR_EXPRESSION: return createOrExpression();
      case LustrePackage.AND_EXPRESSION: return createAndExpression();
      case LustrePackage.IMPLIES_EXPRESSION: return createImpliesExpression();
      case LustrePackage.REL_EXPRESSION: return createRelExpression();
      case LustrePackage.NOT_EXPRESSION: return createNotExpression();
      case LustrePackage.ADD_EXPRESSION: return createAddExpression();
      case LustrePackage.MULT_EXPRESSION: return createMultExpression();
      case LustrePackage.WHEN_EXPRESSION: return createWhenExpression();
      case LustrePackage.NOR_EXPRESSION: return createNorExpression();
      case LustrePackage.UN_EXPRESSION: return createUnExpression();
      case LustrePackage.VARIABLE_EXPRESSION: return createVariableExpression();
      case LustrePackage.TUPLE_EXPRESSION: return createTupleExpression();
      case LustrePackage.INTEGER_LITERAL_EXPRESSION: return createIntegerLiteralExpression();
      case LustrePackage.REAL_LITERAL_EXPRESSION: return createRealLiteralExpression();
      case LustrePackage.DOUBLE_LITERAL_EXPRESSION: return createDoubleLiteralExpression();
      case LustrePackage.BOOLEAN_LITERAL_EXPRESSION: return createBooleanLiteralExpression();
      case LustrePackage.CALL_EXPRESSION: return createCallExpression();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case LustrePackage.SPECIFICATION_TYPE:
        return createSpecificationTypeFromString(eDataType, initialValue);
      case LustrePackage.BASIC_TYPE:
        return createBasicTypeFromString(eDataType, initialValue);
      case LustrePackage.FBY_OP:
        return createFbyOpFromString(eDataType, initialValue);
      case LustrePackage.OR_OP:
        return createOrOpFromString(eDataType, initialValue);
      case LustrePackage.AND_OP:
        return createAndOpFromString(eDataType, initialValue);
      case LustrePackage.IMPLIES_OP:
        return createImpliesOpFromString(eDataType, initialValue);
      case LustrePackage.REL_OP:
        return createRelOpFromString(eDataType, initialValue);
      case LustrePackage.NOT_OP:
        return createNotOpFromString(eDataType, initialValue);
      case LustrePackage.ADD_OP:
        return createAddOpFromString(eDataType, initialValue);
      case LustrePackage.MULT_OP:
        return createMultOpFromString(eDataType, initialValue);
      case LustrePackage.WHEN_OP:
        return createWhenOpFromString(eDataType, initialValue);
      case LustrePackage.NOR_OP:
        return createNorOpFromString(eDataType, initialValue);
      case LustrePackage.UN_OP:
        return createUnOpFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case LustrePackage.SPECIFICATION_TYPE:
        return convertSpecificationTypeToString(eDataType, instanceValue);
      case LustrePackage.BASIC_TYPE:
        return convertBasicTypeToString(eDataType, instanceValue);
      case LustrePackage.FBY_OP:
        return convertFbyOpToString(eDataType, instanceValue);
      case LustrePackage.OR_OP:
        return convertOrOpToString(eDataType, instanceValue);
      case LustrePackage.AND_OP:
        return convertAndOpToString(eDataType, instanceValue);
      case LustrePackage.IMPLIES_OP:
        return convertImpliesOpToString(eDataType, instanceValue);
      case LustrePackage.REL_OP:
        return convertRelOpToString(eDataType, instanceValue);
      case LustrePackage.NOT_OP:
        return convertNotOpToString(eDataType, instanceValue);
      case LustrePackage.ADD_OP:
        return convertAddOpToString(eDataType, instanceValue);
      case LustrePackage.MULT_OP:
        return convertMultOpToString(eDataType, instanceValue);
      case LustrePackage.WHEN_OP:
        return convertWhenOpToString(eDataType, instanceValue);
      case LustrePackage.NOR_OP:
        return convertNorOpToString(eDataType, instanceValue);
      case LustrePackage.UN_OP:
        return convertUnOpToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Program createProgram()
  {
    ProgramImpl program = new ProgramImpl();
    return program;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Open createOpen()
  {
    OpenImpl open = new OpenImpl();
    return open;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallableElement createCallableElement()
  {
    CallableElementImpl callableElement = new CallableElementImpl();
    return callableElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Node createNode()
  {
    NodeImpl node = new NodeImpl();
    return node;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Function createFunction()
  {
    FunctionImpl function = new FunctionImpl();
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractDeclaration createAbstractDeclaration()
  {
    AbstractDeclarationImpl abstractDeclaration = new AbstractDeclarationImpl();
    return abstractDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IDeclaration createIDeclaration()
  {
    IDeclarationImpl iDeclaration = new IDeclarationImpl();
    return iDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DDeclaration createDDeclaration()
  {
    DDeclarationImpl dDeclaration = new DDeclarationImpl();
    return dDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RDeclaration createRDeclaration()
  {
    RDeclarationImpl rDeclaration = new RDeclarationImpl();
    return rDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BDeclaration createBDeclaration()
  {
    BDeclarationImpl bDeclaration = new BDeclarationImpl();
    return bDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayDeclaration createArrayDeclaration()
  {
    ArrayDeclarationImpl arrayDeclaration = new ArrayDeclarationImpl();
    return arrayDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecificationLine createSpecificationLine()
  {
    SpecificationLineImpl specificationLine = new SpecificationLineImpl();
    return specificationLine;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Inputs createInputs()
  {
    InputsImpl inputs = new InputsImpl();
    return inputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Outputs createOutputs()
  {
    OutputsImpl outputs = new OutputsImpl();
    return outputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Locals createLocals()
  {
    LocalsImpl locals = new LocalsImpl();
    return locals;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Body createBody()
  {
    BodyImpl body = new BodyImpl();
    return body;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Annotation createAnnotation()
  {
    AnnotationImpl annotation = new AnnotationImpl();
    return annotation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PreTraceAnnotation createPreTraceAnnotation()
  {
    PreTraceAnnotationImpl preTraceAnnotation = new PreTraceAnnotationImpl();
    return preTraceAnnotation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Equation createEquation()
  {
    EquationImpl equation = new EquationImpl();
    return equation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Assert createAssert()
  {
    AssertImpl assert_ = new AssertImpl();
    return assert_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LeftVariables createLeftVariables()
  {
    LeftVariablesImpl leftVariables = new LeftVariablesImpl();
    return leftVariables;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariablesList createVariablesList()
  {
    VariablesListImpl variablesList = new VariablesListImpl();
    return variablesList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableCall createVariableCall()
  {
    VariableCallImpl variableCall = new VariableCallImpl();
    return variableCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataType createDataType()
  {
    DataTypeImpl dataType = new DataTypeImpl();
    return dataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataTypeDimValue createDataTypeDimValue()
  {
    DataTypeDimValueImpl dataTypeDimValue = new DataTypeDimValueImpl();
    return dataTypeDimValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractExpression createAbstractExpression()
  {
    AbstractExpressionImpl abstractExpression = new AbstractExpressionImpl();
    return abstractExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayDefinition createArrayDefinition()
  {
    ArrayDefinitionImpl arrayDefinition = new ArrayDefinitionImpl();
    return arrayDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MatrixExpression createMatrixExpression()
  {
    MatrixExpressionImpl matrixExpression = new MatrixExpressionImpl();
    return matrixExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayExpression createArrayExpression()
  {
    ArrayExpressionImpl arrayExpression = new ArrayExpressionImpl();
    return arrayExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IteExpression createIteExpression()
  {
    IteExpressionImpl iteExpression = new IteExpressionImpl();
    return iteExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FbyExpression createFbyExpression()
  {
    FbyExpressionImpl fbyExpression = new FbyExpressionImpl();
    return fbyExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrExpression createOrExpression()
  {
    OrExpressionImpl orExpression = new OrExpressionImpl();
    return orExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AndExpression createAndExpression()
  {
    AndExpressionImpl andExpression = new AndExpressionImpl();
    return andExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImpliesExpression createImpliesExpression()
  {
    ImpliesExpressionImpl impliesExpression = new ImpliesExpressionImpl();
    return impliesExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelExpression createRelExpression()
  {
    RelExpressionImpl relExpression = new RelExpressionImpl();
    return relExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotExpression createNotExpression()
  {
    NotExpressionImpl notExpression = new NotExpressionImpl();
    return notExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddExpression createAddExpression()
  {
    AddExpressionImpl addExpression = new AddExpressionImpl();
    return addExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultExpression createMultExpression()
  {
    MultExpressionImpl multExpression = new MultExpressionImpl();
    return multExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhenExpression createWhenExpression()
  {
    WhenExpressionImpl whenExpression = new WhenExpressionImpl();
    return whenExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NorExpression createNorExpression()
  {
    NorExpressionImpl norExpression = new NorExpressionImpl();
    return norExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnExpression createUnExpression()
  {
    UnExpressionImpl unExpression = new UnExpressionImpl();
    return unExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableExpression createVariableExpression()
  {
    VariableExpressionImpl variableExpression = new VariableExpressionImpl();
    return variableExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TupleExpression createTupleExpression()
  {
    TupleExpressionImpl tupleExpression = new TupleExpressionImpl();
    return tupleExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerLiteralExpression createIntegerLiteralExpression()
  {
    IntegerLiteralExpressionImpl integerLiteralExpression = new IntegerLiteralExpressionImpl();
    return integerLiteralExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RealLiteralExpression createRealLiteralExpression()
  {
    RealLiteralExpressionImpl realLiteralExpression = new RealLiteralExpressionImpl();
    return realLiteralExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DoubleLiteralExpression createDoubleLiteralExpression()
  {
    DoubleLiteralExpressionImpl doubleLiteralExpression = new DoubleLiteralExpressionImpl();
    return doubleLiteralExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanLiteralExpression createBooleanLiteralExpression()
  {
    BooleanLiteralExpressionImpl booleanLiteralExpression = new BooleanLiteralExpressionImpl();
    return booleanLiteralExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallExpression createCallExpression()
  {
    CallExpressionImpl callExpression = new CallExpressionImpl();
    return callExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecificationType createSpecificationTypeFromString(EDataType eDataType, String initialValue)
  {
    SpecificationType result = SpecificationType.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSpecificationTypeToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicType createBasicTypeFromString(EDataType eDataType, String initialValue)
  {
    BasicType result = BasicType.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertBasicTypeToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FbyOp createFbyOpFromString(EDataType eDataType, String initialValue)
  {
    FbyOp result = FbyOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertFbyOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrOp createOrOpFromString(EDataType eDataType, String initialValue)
  {
    OrOp result = OrOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertOrOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AndOp createAndOpFromString(EDataType eDataType, String initialValue)
  {
    AndOp result = AndOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertAndOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImpliesOp createImpliesOpFromString(EDataType eDataType, String initialValue)
  {
    ImpliesOp result = ImpliesOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertImpliesOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelOp createRelOpFromString(EDataType eDataType, String initialValue)
  {
    RelOp result = RelOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertRelOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotOp createNotOpFromString(EDataType eDataType, String initialValue)
  {
    NotOp result = NotOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNotOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddOp createAddOpFromString(EDataType eDataType, String initialValue)
  {
    AddOp result = AddOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertAddOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultOp createMultOpFromString(EDataType eDataType, String initialValue)
  {
    MultOp result = MultOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertMultOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhenOp createWhenOpFromString(EDataType eDataType, String initialValue)
  {
    WhenOp result = WhenOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertWhenOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NorOp createNorOpFromString(EDataType eDataType, String initialValue)
  {
    NorOp result = NorOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNorOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnOp createUnOpFromString(EDataType eDataType, String initialValue)
  {
    UnOp result = UnOp.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertUnOpToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LustrePackage getLustrePackage()
  {
    return (LustrePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static LustrePackage getPackage()
  {
    return LustrePackage.eINSTANCE;
  }

} //LustreFactoryImpl
