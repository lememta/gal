/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.xtext.lustre.LustrePackage#getFunction()
 * @model
 * @generated
 */
public interface Function extends CallableElement
{
} // Function
