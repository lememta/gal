/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.Equation#getPreTraceAnnotation <em>Pre Trace Annotation</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.Equation#getLeftPart <em>Left Part</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.Equation#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getEquation()
 * @model
 * @generated
 */
public interface Equation extends EObject
{
  /**
   * Returns the value of the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pre Trace Annotation</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pre Trace Annotation</em>' containment reference.
   * @see #setPreTraceAnnotation(PreTraceAnnotation)
   * @see geneauto.xtext.lustre.LustrePackage#getEquation_PreTraceAnnotation()
   * @model containment="true"
   * @generated
   */
  PreTraceAnnotation getPreTraceAnnotation();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.Equation#getPreTraceAnnotation <em>Pre Trace Annotation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pre Trace Annotation</em>' containment reference.
   * @see #getPreTraceAnnotation()
   * @generated
   */
  void setPreTraceAnnotation(PreTraceAnnotation value);

  /**
   * Returns the value of the '<em><b>Left Part</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left Part</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left Part</em>' containment reference.
   * @see #setLeftPart(LeftVariables)
   * @see geneauto.xtext.lustre.LustrePackage#getEquation_LeftPart()
   * @model containment="true"
   * @generated
   */
  LeftVariables getLeftPart();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.Equation#getLeftPart <em>Left Part</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left Part</em>' containment reference.
   * @see #getLeftPart()
   * @generated
   */
  void setLeftPart(LeftVariables value);

  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getEquation_Expression()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getExpression();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.Equation#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(AbstractExpression value);

} // Equation
