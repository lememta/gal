/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.NotExpression#getOp <em>Op</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.NotExpression#getUexp <em>Uexp</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getNotExpression()
 * @model
 * @generated
 */
public interface NotExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute.
   * The literals are from the enumeration {@link geneauto.xtext.lustre.NotOp}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.NotOp
   * @see #setOp(NotOp)
   * @see geneauto.xtext.lustre.LustrePackage#getNotExpression_Op()
   * @model
   * @generated
   */
  NotOp getOp();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.NotExpression#getOp <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' attribute.
   * @see geneauto.xtext.lustre.NotOp
   * @see #getOp()
   * @generated
   */
  void setOp(NotOp value);

  /**
   * Returns the value of the '<em><b>Uexp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uexp</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uexp</em>' containment reference.
   * @see #setUexp(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getNotExpression_Uexp()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getUexp();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.NotExpression#getUexp <em>Uexp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uexp</em>' containment reference.
   * @see #getUexp()
   * @generated
   */
  void setUexp(AbstractExpression value);

} // NotExpression
