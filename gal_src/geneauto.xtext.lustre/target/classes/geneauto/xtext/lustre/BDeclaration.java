/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BDeclaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.BDeclaration#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getBDeclaration()
 * @model
 * @generated
 */
public interface BDeclaration extends AbstractDeclaration
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see geneauto.xtext.lustre.LustrePackage#getBDeclaration_Value()
   * @model
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.BDeclaration#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

} // BDeclaration
