/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see geneauto.xtext.lustre.LustrePackage#getAbstractExpression()
 * @model
 * @generated
 */
public interface AbstractExpression extends EObject
{
} // AbstractExpression
