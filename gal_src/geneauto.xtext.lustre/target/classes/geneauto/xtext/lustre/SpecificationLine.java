/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specification Line</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.SpecificationLine#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.SpecificationLine#getSpecLine <em>Spec Line</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getSpecificationLine()
 * @model
 * @generated
 */
public interface SpecificationLine extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * The literals are from the enumeration {@link geneauto.xtext.lustre.SpecificationType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see geneauto.xtext.lustre.SpecificationType
   * @see #setType(SpecificationType)
   * @see geneauto.xtext.lustre.LustrePackage#getSpecificationLine_Type()
   * @model
   * @generated
   */
  SpecificationType getType();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.SpecificationLine#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see geneauto.xtext.lustre.SpecificationType
   * @see #getType()
   * @generated
   */
  void setType(SpecificationType value);

  /**
   * Returns the value of the '<em><b>Spec Line</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec Line</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec Line</em>' containment reference.
   * @see #setSpecLine(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getSpecificationLine_SpecLine()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getSpecLine();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.SpecificationLine#getSpecLine <em>Spec Line</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec Line</em>' containment reference.
   * @see #getSpecLine()
   * @generated
   */
  void setSpecLine(AbstractExpression value);

} // SpecificationLine
