/**
 */
package geneauto.xtext.lustre.util;

import geneauto.xtext.lustre.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.xtext.lustre.LustrePackage
 * @generated
 */
public class LustreAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static LustrePackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LustreAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = LustrePackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LustreSwitch<Adapter> modelSwitch =
    new LustreSwitch<Adapter>()
    {
      @Override
      public Adapter caseProgram(Program object)
      {
        return createProgramAdapter();
      }
      @Override
      public Adapter caseOpen(Open object)
      {
        return createOpenAdapter();
      }
      @Override
      public Adapter caseCallableElement(CallableElement object)
      {
        return createCallableElementAdapter();
      }
      @Override
      public Adapter caseNode(Node object)
      {
        return createNodeAdapter();
      }
      @Override
      public Adapter caseFunction(Function object)
      {
        return createFunctionAdapter();
      }
      @Override
      public Adapter caseAbstractDeclaration(AbstractDeclaration object)
      {
        return createAbstractDeclarationAdapter();
      }
      @Override
      public Adapter caseIDeclaration(IDeclaration object)
      {
        return createIDeclarationAdapter();
      }
      @Override
      public Adapter caseDDeclaration(DDeclaration object)
      {
        return createDDeclarationAdapter();
      }
      @Override
      public Adapter caseRDeclaration(RDeclaration object)
      {
        return createRDeclarationAdapter();
      }
      @Override
      public Adapter caseBDeclaration(BDeclaration object)
      {
        return createBDeclarationAdapter();
      }
      @Override
      public Adapter caseArrayDeclaration(ArrayDeclaration object)
      {
        return createArrayDeclarationAdapter();
      }
      @Override
      public Adapter caseSpecificationLine(SpecificationLine object)
      {
        return createSpecificationLineAdapter();
      }
      @Override
      public Adapter caseInputs(Inputs object)
      {
        return createInputsAdapter();
      }
      @Override
      public Adapter caseOutputs(Outputs object)
      {
        return createOutputsAdapter();
      }
      @Override
      public Adapter caseLocals(Locals object)
      {
        return createLocalsAdapter();
      }
      @Override
      public Adapter caseBody(Body object)
      {
        return createBodyAdapter();
      }
      @Override
      public Adapter caseAnnotation(Annotation object)
      {
        return createAnnotationAdapter();
      }
      @Override
      public Adapter casePreTraceAnnotation(PreTraceAnnotation object)
      {
        return createPreTraceAnnotationAdapter();
      }
      @Override
      public Adapter caseEquation(Equation object)
      {
        return createEquationAdapter();
      }
      @Override
      public Adapter caseAssert(Assert object)
      {
        return createAssertAdapter();
      }
      @Override
      public Adapter caseLeftVariables(LeftVariables object)
      {
        return createLeftVariablesAdapter();
      }
      @Override
      public Adapter caseVariablesList(VariablesList object)
      {
        return createVariablesListAdapter();
      }
      @Override
      public Adapter caseVariableCall(VariableCall object)
      {
        return createVariableCallAdapter();
      }
      @Override
      public Adapter caseVariable(Variable object)
      {
        return createVariableAdapter();
      }
      @Override
      public Adapter caseDataType(DataType object)
      {
        return createDataTypeAdapter();
      }
      @Override
      public Adapter caseDataTypeDimValue(DataTypeDimValue object)
      {
        return createDataTypeDimValueAdapter();
      }
      @Override
      public Adapter caseAbstractExpression(AbstractExpression object)
      {
        return createAbstractExpressionAdapter();
      }
      @Override
      public Adapter caseArrayDefinition(ArrayDefinition object)
      {
        return createArrayDefinitionAdapter();
      }
      @Override
      public Adapter caseMatrixExpression(MatrixExpression object)
      {
        return createMatrixExpressionAdapter();
      }
      @Override
      public Adapter caseArrayExpression(ArrayExpression object)
      {
        return createArrayExpressionAdapter();
      }
      @Override
      public Adapter caseIteExpression(IteExpression object)
      {
        return createIteExpressionAdapter();
      }
      @Override
      public Adapter caseFbyExpression(FbyExpression object)
      {
        return createFbyExpressionAdapter();
      }
      @Override
      public Adapter caseOrExpression(OrExpression object)
      {
        return createOrExpressionAdapter();
      }
      @Override
      public Adapter caseAndExpression(AndExpression object)
      {
        return createAndExpressionAdapter();
      }
      @Override
      public Adapter caseImpliesExpression(ImpliesExpression object)
      {
        return createImpliesExpressionAdapter();
      }
      @Override
      public Adapter caseRelExpression(RelExpression object)
      {
        return createRelExpressionAdapter();
      }
      @Override
      public Adapter caseNotExpression(NotExpression object)
      {
        return createNotExpressionAdapter();
      }
      @Override
      public Adapter caseAddExpression(AddExpression object)
      {
        return createAddExpressionAdapter();
      }
      @Override
      public Adapter caseMultExpression(MultExpression object)
      {
        return createMultExpressionAdapter();
      }
      @Override
      public Adapter caseWhenExpression(WhenExpression object)
      {
        return createWhenExpressionAdapter();
      }
      @Override
      public Adapter caseNorExpression(NorExpression object)
      {
        return createNorExpressionAdapter();
      }
      @Override
      public Adapter caseUnExpression(UnExpression object)
      {
        return createUnExpressionAdapter();
      }
      @Override
      public Adapter caseVariableExpression(VariableExpression object)
      {
        return createVariableExpressionAdapter();
      }
      @Override
      public Adapter caseTupleExpression(TupleExpression object)
      {
        return createTupleExpressionAdapter();
      }
      @Override
      public Adapter caseIntegerLiteralExpression(IntegerLiteralExpression object)
      {
        return createIntegerLiteralExpressionAdapter();
      }
      @Override
      public Adapter caseRealLiteralExpression(RealLiteralExpression object)
      {
        return createRealLiteralExpressionAdapter();
      }
      @Override
      public Adapter caseDoubleLiteralExpression(DoubleLiteralExpression object)
      {
        return createDoubleLiteralExpressionAdapter();
      }
      @Override
      public Adapter caseBooleanLiteralExpression(BooleanLiteralExpression object)
      {
        return createBooleanLiteralExpressionAdapter();
      }
      @Override
      public Adapter caseCallExpression(CallExpression object)
      {
        return createCallExpressionAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Program <em>Program</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Program
   * @generated
   */
  public Adapter createProgramAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Open <em>Open</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Open
   * @generated
   */
  public Adapter createOpenAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.CallableElement <em>Callable Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.CallableElement
   * @generated
   */
  public Adapter createCallableElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Node <em>Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Node
   * @generated
   */
  public Adapter createNodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Function
   * @generated
   */
  public Adapter createFunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.AbstractDeclaration <em>Abstract Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.AbstractDeclaration
   * @generated
   */
  public Adapter createAbstractDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.IDeclaration <em>IDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.IDeclaration
   * @generated
   */
  public Adapter createIDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.DDeclaration <em>DDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.DDeclaration
   * @generated
   */
  public Adapter createDDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.RDeclaration <em>RDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.RDeclaration
   * @generated
   */
  public Adapter createRDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.BDeclaration <em>BDeclaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.BDeclaration
   * @generated
   */
  public Adapter createBDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.ArrayDeclaration <em>Array Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.ArrayDeclaration
   * @generated
   */
  public Adapter createArrayDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.SpecificationLine <em>Specification Line</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.SpecificationLine
   * @generated
   */
  public Adapter createSpecificationLineAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Inputs <em>Inputs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Inputs
   * @generated
   */
  public Adapter createInputsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Outputs <em>Outputs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Outputs
   * @generated
   */
  public Adapter createOutputsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Locals <em>Locals</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Locals
   * @generated
   */
  public Adapter createLocalsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Body <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Body
   * @generated
   */
  public Adapter createBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Annotation <em>Annotation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Annotation
   * @generated
   */
  public Adapter createAnnotationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.PreTraceAnnotation <em>Pre Trace Annotation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.PreTraceAnnotation
   * @generated
   */
  public Adapter createPreTraceAnnotationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Equation <em>Equation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Equation
   * @generated
   */
  public Adapter createEquationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Assert <em>Assert</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Assert
   * @generated
   */
  public Adapter createAssertAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.LeftVariables <em>Left Variables</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.LeftVariables
   * @generated
   */
  public Adapter createLeftVariablesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.VariablesList <em>Variables List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.VariablesList
   * @generated
   */
  public Adapter createVariablesListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.VariableCall <em>Variable Call</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.VariableCall
   * @generated
   */
  public Adapter createVariableCallAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.Variable
   * @generated
   */
  public Adapter createVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.DataType <em>Data Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.DataType
   * @generated
   */
  public Adapter createDataTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.DataTypeDimValue <em>Data Type Dim Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.DataTypeDimValue
   * @generated
   */
  public Adapter createDataTypeDimValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.AbstractExpression <em>Abstract Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.AbstractExpression
   * @generated
   */
  public Adapter createAbstractExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.ArrayDefinition <em>Array Definition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.ArrayDefinition
   * @generated
   */
  public Adapter createArrayDefinitionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.MatrixExpression <em>Matrix Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.MatrixExpression
   * @generated
   */
  public Adapter createMatrixExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.ArrayExpression <em>Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.ArrayExpression
   * @generated
   */
  public Adapter createArrayExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.IteExpression <em>Ite Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.IteExpression
   * @generated
   */
  public Adapter createIteExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.FbyExpression <em>Fby Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.FbyExpression
   * @generated
   */
  public Adapter createFbyExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.OrExpression <em>Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.OrExpression
   * @generated
   */
  public Adapter createOrExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.AndExpression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.AndExpression
   * @generated
   */
  public Adapter createAndExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.ImpliesExpression <em>Implies Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.ImpliesExpression
   * @generated
   */
  public Adapter createImpliesExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.RelExpression <em>Rel Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.RelExpression
   * @generated
   */
  public Adapter createRelExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.NotExpression <em>Not Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.NotExpression
   * @generated
   */
  public Adapter createNotExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.AddExpression <em>Add Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.AddExpression
   * @generated
   */
  public Adapter createAddExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.MultExpression <em>Mult Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.MultExpression
   * @generated
   */
  public Adapter createMultExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.WhenExpression <em>When Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.WhenExpression
   * @generated
   */
  public Adapter createWhenExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.NorExpression <em>Nor Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.NorExpression
   * @generated
   */
  public Adapter createNorExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.UnExpression <em>Un Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.UnExpression
   * @generated
   */
  public Adapter createUnExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.VariableExpression <em>Variable Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.VariableExpression
   * @generated
   */
  public Adapter createVariableExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.TupleExpression <em>Tuple Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.TupleExpression
   * @generated
   */
  public Adapter createTupleExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.IntegerLiteralExpression <em>Integer Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.IntegerLiteralExpression
   * @generated
   */
  public Adapter createIntegerLiteralExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.RealLiteralExpression <em>Real Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.RealLiteralExpression
   * @generated
   */
  public Adapter createRealLiteralExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.DoubleLiteralExpression <em>Double Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.DoubleLiteralExpression
   * @generated
   */
  public Adapter createDoubleLiteralExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.BooleanLiteralExpression <em>Boolean Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.BooleanLiteralExpression
   * @generated
   */
  public Adapter createBooleanLiteralExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link geneauto.xtext.lustre.CallExpression <em>Call Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see geneauto.xtext.lustre.CallExpression
   * @generated
   */
  public Adapter createCallExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //LustreAdapterFactory
