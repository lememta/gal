/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.AbstractDeclaration#getPreTraceAnnotation <em>Pre Trace Annotation</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.AbstractDeclaration#getVar <em>Var</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getAbstractDeclaration()
 * @model
 * @generated
 */
public interface AbstractDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Pre Trace Annotation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pre Trace Annotation</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pre Trace Annotation</em>' containment reference.
   * @see #setPreTraceAnnotation(PreTraceAnnotation)
   * @see geneauto.xtext.lustre.LustrePackage#getAbstractDeclaration_PreTraceAnnotation()
   * @model containment="true"
   * @generated
   */
  PreTraceAnnotation getPreTraceAnnotation();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.AbstractDeclaration#getPreTraceAnnotation <em>Pre Trace Annotation</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pre Trace Annotation</em>' containment reference.
   * @see #getPreTraceAnnotation()
   * @generated
   */
  void setPreTraceAnnotation(PreTraceAnnotation value);

  /**
   * Returns the value of the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var</em>' containment reference.
   * @see #setVar(Variable)
   * @see geneauto.xtext.lustre.LustrePackage#getAbstractDeclaration_Var()
   * @model containment="true"
   * @generated
   */
  Variable getVar();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.AbstractDeclaration#getVar <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Var</em>' containment reference.
   * @see #getVar()
   * @generated
   */
  void setVar(Variable value);

} // AbstractDeclaration
