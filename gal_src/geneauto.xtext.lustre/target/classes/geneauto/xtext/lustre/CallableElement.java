/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Callable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.CallableElement#getName <em>Name</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.CallableElement#getInputs <em>Inputs</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.CallableElement#getOutputs <em>Outputs</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getCallableElement()
 * @model
 * @generated
 */
public interface CallableElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see geneauto.xtext.lustre.LustrePackage#getCallableElement_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.CallableElement#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inputs</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inputs</em>' containment reference.
   * @see #setInputs(Inputs)
   * @see geneauto.xtext.lustre.LustrePackage#getCallableElement_Inputs()
   * @model containment="true"
   * @generated
   */
  Inputs getInputs();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.CallableElement#getInputs <em>Inputs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inputs</em>' containment reference.
   * @see #getInputs()
   * @generated
   */
  void setInputs(Inputs value);

  /**
   * Returns the value of the '<em><b>Outputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Outputs</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Outputs</em>' containment reference.
   * @see #setOutputs(Outputs)
   * @see geneauto.xtext.lustre.LustrePackage#getCallableElement_Outputs()
   * @model containment="true"
   * @generated
   */
  Outputs getOutputs();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.CallableElement#getOutputs <em>Outputs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Outputs</em>' containment reference.
   * @see #getOutputs()
   * @generated
   */
  void setOutputs(Outputs value);

} // CallableElement
