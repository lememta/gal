/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.xtext.lustre.LustrePackage
 * @generated
 */
public interface LustreFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  LustreFactory eINSTANCE = geneauto.xtext.lustre.impl.LustreFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Program</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Program</em>'.
   * @generated
   */
  Program createProgram();

  /**
   * Returns a new object of class '<em>Open</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Open</em>'.
   * @generated
   */
  Open createOpen();

  /**
   * Returns a new object of class '<em>Callable Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Callable Element</em>'.
   * @generated
   */
  CallableElement createCallableElement();

  /**
   * Returns a new object of class '<em>Node</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Node</em>'.
   * @generated
   */
  Node createNode();

  /**
   * Returns a new object of class '<em>Function</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function</em>'.
   * @generated
   */
  Function createFunction();

  /**
   * Returns a new object of class '<em>Abstract Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Abstract Declaration</em>'.
   * @generated
   */
  AbstractDeclaration createAbstractDeclaration();

  /**
   * Returns a new object of class '<em>IDeclaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>IDeclaration</em>'.
   * @generated
   */
  IDeclaration createIDeclaration();

  /**
   * Returns a new object of class '<em>DDeclaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>DDeclaration</em>'.
   * @generated
   */
  DDeclaration createDDeclaration();

  /**
   * Returns a new object of class '<em>RDeclaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RDeclaration</em>'.
   * @generated
   */
  RDeclaration createRDeclaration();

  /**
   * Returns a new object of class '<em>BDeclaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>BDeclaration</em>'.
   * @generated
   */
  BDeclaration createBDeclaration();

  /**
   * Returns a new object of class '<em>Array Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Declaration</em>'.
   * @generated
   */
  ArrayDeclaration createArrayDeclaration();

  /**
   * Returns a new object of class '<em>Specification Line</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Specification Line</em>'.
   * @generated
   */
  SpecificationLine createSpecificationLine();

  /**
   * Returns a new object of class '<em>Inputs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Inputs</em>'.
   * @generated
   */
  Inputs createInputs();

  /**
   * Returns a new object of class '<em>Outputs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Outputs</em>'.
   * @generated
   */
  Outputs createOutputs();

  /**
   * Returns a new object of class '<em>Locals</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Locals</em>'.
   * @generated
   */
  Locals createLocals();

  /**
   * Returns a new object of class '<em>Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Body</em>'.
   * @generated
   */
  Body createBody();

  /**
   * Returns a new object of class '<em>Annotation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Annotation</em>'.
   * @generated
   */
  Annotation createAnnotation();

  /**
   * Returns a new object of class '<em>Pre Trace Annotation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pre Trace Annotation</em>'.
   * @generated
   */
  PreTraceAnnotation createPreTraceAnnotation();

  /**
   * Returns a new object of class '<em>Equation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Equation</em>'.
   * @generated
   */
  Equation createEquation();

  /**
   * Returns a new object of class '<em>Assert</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Assert</em>'.
   * @generated
   */
  Assert createAssert();

  /**
   * Returns a new object of class '<em>Left Variables</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Left Variables</em>'.
   * @generated
   */
  LeftVariables createLeftVariables();

  /**
   * Returns a new object of class '<em>Variables List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variables List</em>'.
   * @generated
   */
  VariablesList createVariablesList();

  /**
   * Returns a new object of class '<em>Variable Call</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Call</em>'.
   * @generated
   */
  VariableCall createVariableCall();

  /**
   * Returns a new object of class '<em>Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable</em>'.
   * @generated
   */
  Variable createVariable();

  /**
   * Returns a new object of class '<em>Data Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Data Type</em>'.
   * @generated
   */
  DataType createDataType();

  /**
   * Returns a new object of class '<em>Data Type Dim Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Data Type Dim Value</em>'.
   * @generated
   */
  DataTypeDimValue createDataTypeDimValue();

  /**
   * Returns a new object of class '<em>Abstract Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Abstract Expression</em>'.
   * @generated
   */
  AbstractExpression createAbstractExpression();

  /**
   * Returns a new object of class '<em>Array Definition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Definition</em>'.
   * @generated
   */
  ArrayDefinition createArrayDefinition();

  /**
   * Returns a new object of class '<em>Matrix Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Matrix Expression</em>'.
   * @generated
   */
  MatrixExpression createMatrixExpression();

  /**
   * Returns a new object of class '<em>Array Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Expression</em>'.
   * @generated
   */
  ArrayExpression createArrayExpression();

  /**
   * Returns a new object of class '<em>Ite Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ite Expression</em>'.
   * @generated
   */
  IteExpression createIteExpression();

  /**
   * Returns a new object of class '<em>Fby Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fby Expression</em>'.
   * @generated
   */
  FbyExpression createFbyExpression();

  /**
   * Returns a new object of class '<em>Or Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Or Expression</em>'.
   * @generated
   */
  OrExpression createOrExpression();

  /**
   * Returns a new object of class '<em>And Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>And Expression</em>'.
   * @generated
   */
  AndExpression createAndExpression();

  /**
   * Returns a new object of class '<em>Implies Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Implies Expression</em>'.
   * @generated
   */
  ImpliesExpression createImpliesExpression();

  /**
   * Returns a new object of class '<em>Rel Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Rel Expression</em>'.
   * @generated
   */
  RelExpression createRelExpression();

  /**
   * Returns a new object of class '<em>Not Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Not Expression</em>'.
   * @generated
   */
  NotExpression createNotExpression();

  /**
   * Returns a new object of class '<em>Add Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Add Expression</em>'.
   * @generated
   */
  AddExpression createAddExpression();

  /**
   * Returns a new object of class '<em>Mult Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mult Expression</em>'.
   * @generated
   */
  MultExpression createMultExpression();

  /**
   * Returns a new object of class '<em>When Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>When Expression</em>'.
   * @generated
   */
  WhenExpression createWhenExpression();

  /**
   * Returns a new object of class '<em>Nor Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nor Expression</em>'.
   * @generated
   */
  NorExpression createNorExpression();

  /**
   * Returns a new object of class '<em>Un Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Un Expression</em>'.
   * @generated
   */
  UnExpression createUnExpression();

  /**
   * Returns a new object of class '<em>Variable Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Expression</em>'.
   * @generated
   */
  VariableExpression createVariableExpression();

  /**
   * Returns a new object of class '<em>Tuple Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Tuple Expression</em>'.
   * @generated
   */
  TupleExpression createTupleExpression();

  /**
   * Returns a new object of class '<em>Integer Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Literal Expression</em>'.
   * @generated
   */
  IntegerLiteralExpression createIntegerLiteralExpression();

  /**
   * Returns a new object of class '<em>Real Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Real Literal Expression</em>'.
   * @generated
   */
  RealLiteralExpression createRealLiteralExpression();

  /**
   * Returns a new object of class '<em>Double Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Double Literal Expression</em>'.
   * @generated
   */
  DoubleLiteralExpression createDoubleLiteralExpression();

  /**
   * Returns a new object of class '<em>Boolean Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Literal Expression</em>'.
   * @generated
   */
  BooleanLiteralExpression createBooleanLiteralExpression();

  /**
   * Returns a new object of class '<em>Call Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Expression</em>'.
   * @generated
   */
  CallExpression createCallExpression();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  LustrePackage getLustrePackage();

} //LustreFactory
