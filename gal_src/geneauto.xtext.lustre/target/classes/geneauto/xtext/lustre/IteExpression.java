/**
 */
package geneauto.xtext.lustre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ite Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.IteExpression#getGuard <em>Guard</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.IteExpression#getThen <em>Then</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.IteExpression#getElse <em>Else</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getIteExpression()
 * @model
 * @generated
 */
public interface IteExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Guard</em>' containment reference.
   * @see #setGuard(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getIteExpression_Guard()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getGuard();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.IteExpression#getGuard <em>Guard</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Guard</em>' containment reference.
   * @see #getGuard()
   * @generated
   */
  void setGuard(AbstractExpression value);

  /**
   * Returns the value of the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Then</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Then</em>' containment reference.
   * @see #setThen(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getIteExpression_Then()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getThen();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.IteExpression#getThen <em>Then</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Then</em>' containment reference.
   * @see #getThen()
   * @generated
   */
  void setThen(AbstractExpression value);

  /**
   * Returns the value of the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else</em>' containment reference.
   * @see #setElse(AbstractExpression)
   * @see geneauto.xtext.lustre.LustrePackage#getIteExpression_Else()
   * @model containment="true"
   * @generated
   */
  AbstractExpression getElse();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.IteExpression#getElse <em>Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else</em>' containment reference.
   * @see #getElse()
   * @generated
   */
  void setElse(AbstractExpression value);

} // IteExpression
