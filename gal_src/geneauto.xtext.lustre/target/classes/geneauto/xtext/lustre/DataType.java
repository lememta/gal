/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.DataType#getBaseType <em>Base Type</em>}</li>
 *   <li>{@link geneauto.xtext.lustre.DataType#getDim <em>Dim</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getDataType()
 * @model
 * @generated
 */
public interface DataType extends EObject
{
  /**
   * Returns the value of the '<em><b>Base Type</b></em>' attribute.
   * The literals are from the enumeration {@link geneauto.xtext.lustre.BasicType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Base Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Base Type</em>' attribute.
   * @see geneauto.xtext.lustre.BasicType
   * @see #setBaseType(BasicType)
   * @see geneauto.xtext.lustre.LustrePackage#getDataType_BaseType()
   * @model
   * @generated
   */
  BasicType getBaseType();

  /**
   * Sets the value of the '{@link geneauto.xtext.lustre.DataType#getBaseType <em>Base Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Base Type</em>' attribute.
   * @see geneauto.xtext.lustre.BasicType
   * @see #getBaseType()
   * @generated
   */
  void setBaseType(BasicType value);

  /**
   * Returns the value of the '<em><b>Dim</b></em>' containment reference list.
   * The list contents are of type {@link geneauto.xtext.lustre.DataTypeDimValue}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dim</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dim</em>' containment reference list.
   * @see geneauto.xtext.lustre.LustrePackage#getDataType_Dim()
   * @model containment="true"
   * @generated
   */
  EList<DataTypeDimValue> getDim();

} // DataType
