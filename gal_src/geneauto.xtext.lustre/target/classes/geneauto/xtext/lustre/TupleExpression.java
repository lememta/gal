/**
 */
package geneauto.xtext.lustre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tuple Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.xtext.lustre.TupleExpression#getExpressionList <em>Expression List</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.xtext.lustre.LustrePackage#getTupleExpression()
 * @model
 * @generated
 */
public interface TupleExpression extends AbstractExpression
{
  /**
   * Returns the value of the '<em><b>Expression List</b></em>' containment reference list.
   * The list contents are of type {@link geneauto.xtext.lustre.AbstractExpression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression List</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression List</em>' containment reference list.
   * @see geneauto.xtext.lustre.LustrePackage#getTupleExpression_ExpressionList()
   * @model containment="true"
   * @generated
   */
  EList<AbstractExpression> getExpressionList();

} // TupleExpression
