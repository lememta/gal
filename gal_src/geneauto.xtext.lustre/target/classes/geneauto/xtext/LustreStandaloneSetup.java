
package geneauto.xtext;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class LustreStandaloneSetup extends LustreStandaloneSetupGenerated{

	public static void doSetup() {
		new LustreStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

