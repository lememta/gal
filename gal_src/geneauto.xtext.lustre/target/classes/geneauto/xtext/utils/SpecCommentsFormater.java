/****************************************************************************
 * Copyright (c) 2013 ENSEEIHT
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Guillaume Babin
 *     Romain Bobo
 *     Jason Crombez
 *     Guillaume Diep
 *     Mathieu Montin
 *
 ****************************************************************************/


package geneauto.xtext.utils;

import org.antlr.runtime.Token;

import geneauto.xtext.lustre.SpecificationLine;

/**
 * This class regroups the two methods that are used to formate the specification line.
 * @author Mathieu MONTIN
 */
public final class SpecCommentsFormater {
	
	/**
	 * A private constructor so noone can create an instance of this class.
	 */
	private SpecCommentsFormater() { }
	
	/**
	 * This method removes the "--@" and the "\n" from a line$
	 * it is called after the parsing and before the create of an eObject
	 * corresponding to this line of comment.
	 * @param line : the line to be processed
	 */
//	public static void formateIn(Token line) {
//		
//		String text = line.getText();
//		//System.out.println(text);
//		text = text.substring(3, text.length() - 1).trim();
//		//System.out.println	(text);
//		line.setText(text);
//	}
	
	/**
	 * This method appends the "--@" and the "\n" from a line.
	 * it is called before the serialization
	 * @param line : the line to be processed
	 * @return the String extracted from this line
	 */
//	public static String formateOut(SpecificationLine line) {
//
//		String text = line.getSpecLine();
//		
//		//System.out.println(text);
//		text = "--@ " + text + "\n";
//		//System.out.println(text);
//		return text;
//	}
}
