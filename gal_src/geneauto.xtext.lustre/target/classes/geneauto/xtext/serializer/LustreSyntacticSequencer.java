package geneauto.xtext.serializer;

import com.google.inject.Inject;
import geneauto.xtext.services.LustreGrammarAccess;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public class LustreSyntacticSequencer extends AbstractSyntacticSequencer {

	protected LustreGrammarAccess grammarAccess;
	protected AbstractElementAlias match_Inputs_SemicolonKeyword_2_q;
	protected AbstractElementAlias match_Node___FullStopKeyword_16_1_or_SemicolonKeyword_16_0__q;
	protected AbstractElementAlias match_Open_SemicolonKeyword_2_q;
	protected AbstractElementAlias match_Outputs_SemicolonKeyword_2_q;
	protected AbstractElementAlias match_TupleExpression_LeftParenthesisKeyword_0_a;
	protected AbstractElementAlias match_TupleExpression_LeftParenthesisKeyword_0_p;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (LustreGrammarAccess) access;
		match_Inputs_SemicolonKeyword_2_q = new TokenAlias(false, true, grammarAccess.getInputsAccess().getSemicolonKeyword_2());
		match_Node___FullStopKeyword_16_1_or_SemicolonKeyword_16_0__q = new AlternativeAlias(false, true, new TokenAlias(false, false, grammarAccess.getNodeAccess().getFullStopKeyword_16_1()), new TokenAlias(false, false, grammarAccess.getNodeAccess().getSemicolonKeyword_16_0()));
		match_Open_SemicolonKeyword_2_q = new TokenAlias(false, true, grammarAccess.getOpenAccess().getSemicolonKeyword_2());
		match_Outputs_SemicolonKeyword_2_q = new TokenAlias(false, true, grammarAccess.getOutputsAccess().getSemicolonKeyword_2());
		match_TupleExpression_LeftParenthesisKeyword_0_a = new TokenAlias(true, true, grammarAccess.getTupleExpressionAccess().getLeftParenthesisKeyword_0());
		match_TupleExpression_LeftParenthesisKeyword_0_p = new TokenAlias(true, false, grammarAccess.getTupleExpressionAccess().getLeftParenthesisKeyword_0());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_Inputs_SemicolonKeyword_2_q.equals(syntax))
				emit_Inputs_SemicolonKeyword_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Node___FullStopKeyword_16_1_or_SemicolonKeyword_16_0__q.equals(syntax))
				emit_Node___FullStopKeyword_16_1_or_SemicolonKeyword_16_0__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Open_SemicolonKeyword_2_q.equals(syntax))
				emit_Open_SemicolonKeyword_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Outputs_SemicolonKeyword_2_q.equals(syntax))
				emit_Outputs_SemicolonKeyword_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_TupleExpression_LeftParenthesisKeyword_0_a.equals(syntax))
				emit_TupleExpression_LeftParenthesisKeyword_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_TupleExpression_LeftParenthesisKeyword_0_p.equals(syntax))
				emit_TupleExpression_LeftParenthesisKeyword_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     ';'?
	 */
	protected void emit_Inputs_SemicolonKeyword_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     (';' | '.')?
	 */
	protected void emit_Node___FullStopKeyword_16_1_or_SemicolonKeyword_16_0__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ';'?
	 */
	protected void emit_Open_SemicolonKeyword_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ';'?
	 */
	protected void emit_Outputs_SemicolonKeyword_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '('*
	 */
	protected void emit_TupleExpression_LeftParenthesisKeyword_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '('+
	 */
	protected void emit_TupleExpression_LeftParenthesisKeyword_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
