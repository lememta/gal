/****************************************************************************
 * Copyright (c) 2013 ENSEEIHT
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Guillaume Babin
 *     Romain Bobo
 *     Jason Crombez
 *     Guillaume Diep
 *     Mathieu Montin
 *
 ****************************************************************************/

package geneauto.xtext.serializer;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

import geneauto.xtext.lustre.LustrePackage;
import geneauto.xtext.lustre.SpecificationLine;
import geneauto.xtext.lustre.Variable;
import geneauto.xtext.services.LustreGrammarAccess;
import geneauto.xtext.utils.SpecCommentsFormater;

import com.google.inject.Inject;

/**
 * A class meant to override the method sequence_SpecificationLine.
 * @author Mathieu MONTIN
 */
@SuppressWarnings("restriction")
public class CustomLustreSemanticSequencer extends LustreSemanticSequencer {

	/**
	 * The access to the lustre grammar.
	 */
	@Inject
	private LustreGrammarAccess grammarAccess;

	/** Adds "--@" to the line.
	 * @param context : the current context of the serialization
	 * @param semanticObject : the line of specification to be modified
	 * The point of this new method is to add "--@" at the beginning of 
	 * a specification line (and "\n" at its end) before the serialization
	 */
//	@Override
//	protected void sequence_SpecificationLine(EObject context,
//			SpecificationLine semanticObject) {
//
//		if (errorAcceptor != null) {
//			if (transientValues.isValueTransient(semanticObject, LustrePackage.Literals.SPECIFICATION_LINE__SPEC_LINE) == ValueTransient.YES)
//				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LustrePackage.Literals.SPECIFICATION_LINE__SPEC_LINE));
//		}
//		final INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
//		final SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
//		feeder.accept(grammarAccess.getSpecificationLineAccess().getSpecLineSPEC_COMMENTTerminalRuleCall_1_0(), 
//				SpecCommentsFormater.formateOut(semanticObject));
//		feeder.finish();
//	}
	
	/** RULE
	 * 	Variable :
			name=ID
		;
	 */
//	@Override
//	protected void sequence_Variable(EObject context, Variable semanticObject) {
//		final INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
//		final SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
//		
//		feeder.accept(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0(),
//				semanticObject.getName());
//		
//		feeder.finish();
//
//	}
	
}
