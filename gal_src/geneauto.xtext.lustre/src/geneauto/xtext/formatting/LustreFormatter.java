/****************************************************************************
 * Copyright (c) 2013 ENSEEIHT
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Guillaume Babin
 *     Romain Bobo
 *     Jason Crombez
 *     Guillaume Diep
 *     Mathieu Montin
 *
 ****************************************************************************/

package geneauto.xtext.formatting;

import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;
import org.eclipse.xtext.util.Pair;
import geneauto.xtext.services.LustreGrammarAccess;

/** This class contains custom formatting description.
 * @author Guillaume BABIN 
 * 
 * see : http://www.eclipse.org/Xtext/documentation/latest/xtext.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
public class LustreFormatter extends AbstractDeclarativeFormatter {

	/** A formatting method for the generated code.
	 * @param c : the config to customize
	 */
	@Override
	protected void configureFormatting(FormattingConfig c) {
// It's usually a good idea to activate the following three statements.
// They will add and preserve newlines around comments
//		c.setLinewrap(0, 1, 2).before(getGrammarAccess().getSL_COMMENTRule());
//		c.setLinewrap(0, 1, 2).before(getGrammarAccess().getML_COMMENTRule());
//		c.setLinewrap(0, 1, 1).after(getGrammarAccess().getML_COMMENTRule());

		final int max = 10000;
		final LustreGrammarAccess ga = (LustreGrammarAccess)getGrammarAccess();
		final String sautDoubleTab = "\n\t\t";
		final String saut = "\n";
		final String sautTab = "\n\t";
		
		c.setAutoLinewrap(max);

		// no spaces between parenthesis
		for (Pair<Keyword, Keyword> pair : ga.findKeywordPairs("(", ")")) {
			c.setNoSpace().after(pair.getFirst());
			c.setNoSpace().before(pair.getSecond());
		}

		// no spaces before commas
		for (Keyword comma : ga.findKeywords(",")) {
			c.setNoSpace().before(comma);
		}
		
		// Includes
		//c.setLinewrap(2).after(ga.getIncludeAccess().getSemicolonKeyword_2());
		c.setLinewrap(2).after(ga.getOpenAccess().getRule());
		
		// Variables list
		c.setSpace(sautTab).before(ga.getVariablesListAccess().getConstAssignment_1());

		// nodes
		c.setLinewrap(2).before(ga.getNodeAccess().getRule());
		
		// Specifications
		c.setLinewrap().before(ga.getSpecificationLineAccess().getHyphenMinusHyphenMinusCommercialAtKeyword_1());
		c.setLinewrap().after(ga.getSpecificationLineAccess().getSemicolonKeyword_4());
		
		// functions
		c.setLinewrap(2).before(ga.getFunctionAccess().getFunctionKeyword_0());
		
		// inputs
		c.setSpace(sautTab).before(ga.getInputsAccess().getInputsAssignment_0());
		c.setSpace(sautTab).before(ga.getInputsAccess().getInputsAssignment_1_1());
		
		// outputs
		c.setSpace(sautTab).before(ga.getOutputsAccess().getOutputsAssignment_0());
		c.setSpace(sautTab).before(ga.getOutputsAccess().getOutputsAssignment_1_1());

		// returns
		c.setSpace(saut).before(ga.getNodeAccess().getReturnsKeyword_7());

		// returns
		c.setSpace(saut).before(ga.getFunctionAccess().getReturnsKeyword_5());
				
		// Constants declarations
		c.setLinewrap().after(ga.getProgramAccess().getSemicolonKeyword_2_1());
		c.setLinewrap().after(ga.getIDeclarationAccess().getSemicolonKeyword_5());
		c.setLinewrap().after(ga.getRDeclarationAccess().getSemicolonKeyword_5());
		c.setLinewrap().after(ga.getDDeclarationAccess().getSemicolonKeyword_5());
		c.setLinewrap().after(ga.getBDeclarationAccess().getSemicolonKeyword_5());
		c.setLinewrap().after(ga.getArrayDeclarationAccess().getSemicolonKeyword_5());
		
		// var
		c.setLinewrap().before(ga.getNodeAccess().getVarKeyword_12_0());
		c.setSpace(sautTab).before(ga.getLocalsAccess().getLocalsAssignment_1_0());
		c.setSpace(sautTab).before(ga.getLocalsAccess().getLocalsAssignment_1_1_1());

		// let
		c.setLinewrap().before(ga.getNodeAccess().getLetKeyword_13());
		c.setLinewrap().after(ga.getNodeAccess().getLetKeyword_13());

		c.setIndentation(ga.getNodeAccess().getLetKeyword_13(), ga.getNodeAccess().getTelKeyword_15());

		// equations or asserts
		c.setLinewrap().after(ga.getEquationAccess().getSemicolonKeyword_5());
		c.setLinewrap().after(ga.getAssertAccess().getSemicolonKeyword_3());
		
		// equations annotations
		c.setLinewrap().after(ga.getPreTraceAnnotationAccess().getSemicolonKeyword_4());
		
		// if-the-else expressions
		c.setLinewrap().before(ga.getIteExpressionAccess().getIfKeyword_0());
		
		c.setLinewrap().after(ga.getIteExpressionAccess().getThenKeyword_2());
		
		c.setLinewrap().before(ga.getIteExpressionAccess().getElseKeyword_4_0());
	
		// tel
		c.setLinewrap().before(ga.getNodeAccess().getTelKeyword_15());
		c.setLinewrap(2).after(ga.getNodeAccess().getTelKeyword_15());

		// Array dimensions
		c.setNoSpace().before(ga.getDataTypeAccess().getCircumflexAccentKeyword_1_0());
		c.setNoSpace().before(ga.getDataTypeAccess().getCircumflexAccentKeyword_1_2_0());
		c.setNoSpace().after(ga.getDataTypeAccess().getCircumflexAccentKeyword_1_0());
		c.setNoSpace().after(ga.getDataTypeAccess().getCircumflexAccentKeyword_1_2_0());
		
		// if then else
		//c.setLinewrap().after(f.getIteExpressionAccess().getThenKeyword_2());
		//c.setIndentationIncrement().before(f.getIteExpressionAccess().getThenAssignment_3());
		//c.setIndentationDecrement().after(f.getIteExpressionAccess().getThenAssignment_3());
		//c.setLinewrap().before(f.getIteExpressionAccess().getElseKeyword_4_0());
	}
}
