/****************************************************************************
 * Copyright (c) 2013 ENSEEIHT
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Guillaume Babin
 *     Romain Bobo
 *     Jason Crombez
 *     Guillaume Diep
 *     Mathieu Montin
 *
 ****************************************************************************/

package geneauto.xtext.utils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import geneauto.xtext.lustre.Node;
import geneauto.xtext.lustre.Program;

/**
 * This class allows a user to look for a certain ancestor of a given eObject.
 * @author Mathieu MONTIN, Guillaume BABIN
 */
public final class AncestorSeeker {

	/**
	 * A private constructor so noone can create an instance of this class.
	 */
	private AncestorSeeker() { } 
	
	/**
	 * This method is meant to factorize the code of the other methods of this class.
	 * @param element : the elements to look for its first ancestor.
	 * @param type : the type of this first ancestor
	 * @return the ancestor
	 */
	private static <T extends EObject> T getFirstAncestorOfATypeFromAnElement(EObject element, Class<T> type) {

		EObject ancestor = element;

		while (!(type.isInstance(ancestor))) {
			ancestor = ancestor.eContainer();
			if (ancestor == null) {
				return null; 
			}
		}

		try {
			return type.cast(ancestor);
		} catch (ClassCastException e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
	}

	/**
	 * This method is meant to return the node containing the given eObject.
	 * @param eObject : The given object to look for its ancestors
	 * @return The first node encounters in the chain of ancestors
	 */
	public static Node getAncestorNode(EObject eObject) {

		return getFirstAncestorOfATypeFromAnElement(eObject, Node.class);
	}

	/**
	 * This method is meant to return the Program containing the given eObject.
	 * @param eObject : The given object to look for its ancestors
	 * @return The first Program encounters in the chain of ancestors
	 */
	public static Program getAncestorProgram(EObject eObject) {

		return getFirstAncestorOfATypeFromAnElement(eObject, Program.class);
	}

	/**
	 * This method is meant to return the resource set containing the given eObject.
	 * @param eObject : The given object to look for its resourceSet
	 * @return The resourceSet containing the object
	 */
	public static ResourceSet getAncestorResourceSet(EObject eObject) {

		return eObject.eResource().getResourceSet(); 
	}
}
