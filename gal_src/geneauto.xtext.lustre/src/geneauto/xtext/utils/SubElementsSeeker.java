/****************************************************************************
 * Copyright (c) 2013 ENSEEIHT
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Guillaume Babin
 *     Romain Bobo
 *     Jason Crombez
 *     Guillaume Diep
 *     Mathieu Montin
 *
 ****************************************************************************/

package geneauto.xtext.utils;

import java.util.Collection;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import geneauto.xtext.lustre.AbstractDeclaration;
import geneauto.xtext.lustre.LeftVariables;
import geneauto.xtext.lustre.Node;
import geneauto.xtext.lustre.Program;
import geneauto.xtext.lustre.Variable;

/**
 * This class provides methods to collect every object of a given type 
 * under a given element in the tree of the model. 
 * @author Mathieu MONTIN
 */
public final class SubElementsSeeker {
	
	/**
	 * Constructor to prevent anyone to create an instance of this class.
	 */
	private SubElementsSeeker() { }
	
	/**
	 * Method used to factorize other methods from this class.
	 * @param eObject : the element where to look under
	 * @param collection : the collection where to add the objects
	 * @param type : the type of the objects to add
	 */
	private static void addAllElementsOfGivenTypeUnderAnEObject(
			EObject eObject, Collection<EObject> collection, Class<?> type) {
		
		addAllElementsOfGivenTypeUnderATreeIterator(eObject.eAllContents(), collection, type);
	}

	/**
	 * Method used to factorize other methods from this class.
	 * @param iterator : the iterator where to look
	 * @param collection : the collection where to add the objects
	 * @param type : the type of the objects to add
	 */
	private static void addAllElementsOfGivenTypeUnderATreeIterator(
			TreeIterator<EObject> iterator, Collection<EObject> collection, Class<?> type) {
		EObject currentObject = null;
		while (iterator.hasNext()) {
			currentObject = iterator.next();
			if (type.isInstance(currentObject)) {
				collection.add(currentObject);
			}
		}
		
	}

	/**
	 * This method add all Variables located under the given element in the given collection.
	 * @param element : the element under which to seek
	 * @param collection : the collection where to add the Variables found
	 */
	public static void addAllElementsOfTypeVariableUnderAnElement(EObject element, Collection<EObject> collection) {
		addAllElementsOfGivenTypeUnderAnEObject(element, collection, Variable.class);
	}
	
	/**
	 * This method adds all Node located under the given program in the given collection.
	 * @param p : the element under which to seek
	 * @param collection : the collection where to add the Nodes found
	 */
	public static void addAllElementsOfTypeNodeUnderAProgram(Program p, Collection<EObject> collection) {
		addAllElementsOfGivenTypeUnderAnEObject(p, collection, Node.class);
	}
	
	/**
	 * This method adds all Declarations located under the given program in the given collection.
	 * @param prog : the element under which to seek
	 * @param c : the collection where to add the Nodes found
	 */
	public static void addAllElementsOfTypeDeclarationUnderAProgram(
			Program prog, Collection<EObject> c) {
		addAllElementsOfGivenTypeUnderAnEObject(prog, c, AbstractDeclaration.class); 
	}
	
	/** 
	 * This methods adds all the left parts of the equations in the given node in the collection.
	 * @param n : the node where to look
	 * @param collection : the collection where to add the elements
	 */
	public static void addAllElementsOfTypeLeftVariablesUnderANode(Node n, Collection<EObject> collection) {
		addAllElementsOfGivenTypeUnderAnEObject(n, collection, LeftVariables.class);
	}
}
