/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/TestingAlgebraicLoopsState.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This state tests if system doesn't contain any algebraic loop.
 * 
 */
public class TestingAlgebraicLoopsState extends FMPreprocessorState {

    /**
     * Attributes which know for each block type if it is direct feed through or
     * not.
     */
    private Map<String, Boolean> isDirectFeedThrough = new HashMap<String, Boolean>();

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public TestingAlgebraicLoopsState(FMPreprocessorTool machine) {
        super(machine, "TestingAlgebraicLoops");
    }

    /**
     * Logs the beginning of algebraic loop test.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
        GABlockLibrary blockLibrary = (GABlockLibrary) stateMachine
                .getBlockLibrary();
        for (GAModelElement blockType : blockLibrary.getAllElements()) {
            if (blockType instanceof BlockType) {
                isDirectFeedThrough.put(blockType.getName(),
                        ((BlockType) blockType).isDirectFeedThrough());
            }
        }
    }

    /**
     * Execution core of this state. Finds the upper level object and checks the
     * algebraic loops.
     */
    public void stateExecute() {
    	
    	checkLoopsInSystem(gaSystemModel.getRootSystemBlock());
//        for (GAModelElement element : gaSystemModel.getAllElements()) {
//            if (element instanceof Block) {
//                // get upper level object
//                if (element.getParent() == null) {
//                    SystemBlock parent = (SystemBlock) element;
//                    checkLoopsInSystem(parent);
//                }
//            }
//        }

        stateMachine.setState(stateMachine.getAssigningPrioritiesState());
    }

    /**
     * Checks the algebraic loops inside a GASystemModel.
     * 
     * @param parent
     *            the parent system.
     */
    private void checkLoopsInSystem(SystemBlock parent) {
        for (Block block : parent.getBlocks()) {
            // if element is a subsystem, we process its elements
            if (block instanceof SystemBlock) {
                checkLoopsInSystem((SystemBlock) block);
            }
            // else, we process each source block to make sure that
            // we do not forget any branch.
            if (block instanceof SourceBlock) {
                checkLoops((SourceBlock) block, parent, new ArrayList<Block>());
            }
        }

    }

    /**
     * Checks the loops into a flattened subsystem
     * 
     * @param parent
     *            the subsystem in which we are located.
     * @param ancestors
     *            the List of the ancestors.
     * @param source
     *            the Source from which we begin the loop browsing.
     */
    private void checkLoops(Block source, SystemBlock parent,
            List<Block> ancestors) {
        // we follow the signals to determine the loops.
        List<Signal> systemSignals = new ArrayList<Signal>();

        if (ancestors.contains(source)) {
            // we have a loop, need to test the presence
            // of a block having its attribute directFeedThrough set to
            // false.
            boolean isAlgebraic = true;
            for (Block ancestor : ancestors) {
                // if, in the loop, at least one block is not direct
                // feed through, then it is not an algebraic loop
                isAlgebraic = isAlgebraic
                        && isDirectFeedThrough.get(ancestor.getType());
            }
            if (isAlgebraic) {
                String detailledMessage = "";
                for (Block ancestor : ancestors) {
                    detailledMessage += ancestor.getId() + " : "
                            + ancestor.getName() + " --> ";
                }
                detailledMessage += source.getId() + " : " + source.getName();
                EventHandler.handle(EventLevel.ERROR, "TFMPreprocessor", "",
                        "An algebraic loop was detected in the input model.",
                        detailledMessage);
            }
        } else if (source instanceof CombinatorialBlock){
            ancestors.add(source);
            // if block is not among its ancestors,
            // we browse its children , following the signals.
            for (Outport outPort : source.getOutDataPorts()) {
            	for (Signal sig : outPort.getOutgoingSignals()){
            		checkLoops(sig.getDstBlock(), parent, ancestors);
            	}
            }
        }
    }
}
