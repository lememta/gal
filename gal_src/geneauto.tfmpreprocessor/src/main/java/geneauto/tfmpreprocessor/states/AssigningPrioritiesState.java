/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/AssigningPrioritiesState.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gasystemmodel.GASystemModelRoot;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;

import java.util.ArrayList;
import java.util.List;

/**
 * This state initialises assignedPriority attributes for all the blocks in the
 * system, basing on graphical position.
 * 
 */
public class AssigningPrioritiesState extends FMPreprocessorState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public AssigningPrioritiesState(FMPreprocessorTool machine) {
        super(machine, "AssignPriorities");
    }

    /**
     * Logs the beginning of priorities assignment.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
    }

    /**
     * Execution core of this state. Finds the upper level object and assign the
     * priorities according to the graphical positions.
     */
    public void stateExecute() {
        for (GASystemModelRoot element : gaSystemModel.getElements()) {
            if ((element instanceof SystemBlock)
                    && (element.getParent() == null)) {
                // get upper level object

                SystemBlock parent = (SystemBlock) element;
                assignPrioritiesInSystem(parent);

            } else {
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                "AssigningPrioritiesState",
                                "",
                                "elements attribute in Model class must be SystemBlock without parent.",
                                "");
            }
        }
        stateMachine.setState(stateMachine.getSavingModelState());
    }

    /**
     * Assign recursively the priorities in a system.
     * 
     * @param parent
     *            the parent system.
     */
    private void assignPrioritiesInSystem(SystemBlock parent) {
        List<Block> sortedList = new ArrayList<Block>();
        Block currMin = null;
        int minPosX = 0;
        int minPosY = 0;

        for (Block block : parent.getBlocks()) {
            // if element is a subsystem, we process its elements
            if (block instanceof SystemBlock) {
                assignPrioritiesInSystem((SystemBlock) block);
            }
        }

        // for each block of the model
        for (int currentPriority = 1; currentPriority <= parent.getBlocks()
                .size(); currentPriority++) {

            // we browse the model and get the lower priority one
            for (Block block : parent.getBlocks()) {
                if (!sortedList.contains(block)) {
                    if (currMin == null) {
                        currMin = block;
                        minPosY = block.getDiagramInfo().getPositionY();
                        minPosX = block.getDiagramInfo().getPositionX();
                    } else {
                        int posY = block.getDiagramInfo().getPositionY();
                        int posX = block.getDiagramInfo().getPositionX();
                        if (posY < minPosY
                                || ((posY == minPosY) && (posX < minPosX))) {
                            currMin = block;
                            minPosY = block.getDiagramInfo().getPositionY();
                            minPosX = block.getDiagramInfo().getPositionX();
                        }
                    }
                }
            }
            if (currMin != null) {
            	sortedList.add(currMin);
            	currMin.setAssignedPriority(currentPriority);
            	currMin.setAssignedPrioritySource("GrLoc");
            	currMin = null;
            }
        }
    }
}
