/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/ReplacingGotoFromBlockState.java,v $
 *  @version	$Revision: 1.17 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;
import geneauto.utils.tuple.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This state removed all goto and from block rerouting signal directly to the
 * right source and destination ports.
 * 
 * 
 */
public class ReplacingGotoFromBlockState extends FMPreprocessorState {

    public ReplacingGotoFromBlockState(FMPreprocessorTool machine) {
        super(machine, "ReplacingGotoFromBlock");
    }

    /**
     * Logs the beginning of Goto and From block replacement.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();

    }

    /**
     * Look for Goto block in the system model, find the corresponding from
     * block and replace them merging their input and output signals.
     * 
     */
    @Override
    public void stateExecute() {
        Block elemFrom;
        List<GAModelElement> toDelete = new LinkedList<GAModelElement>();

        Pair<List<Block>, List<Block>> gotoFromBlocks = getGotoFromBlock();

        List<Block> gotos = gotoFromBlocks.getLeft();
        List<Block> froms = gotoFromBlocks.getRight();

        for (Block elemGoto : gotos) {

            boolean replace = false;

            // read the 'GotoTag' parameter
            Parameter paramGoto = elemGoto.getParameterByName("GotoTag");

            if (paramGoto == null) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR,
                        "ReplacingGotoFromBlockState", "", "Block "
                                + elemGoto.getReferenceString()
                                + " does not have parameter \"GotoTag\"", "");
            }

            String gotoTagValue = paramGoto.getStringValue();

            // find the from block with same gotoTag value
            // TODO Quadratic complexity! Should be replaced with a hash lookup. AnTo 100225.
            elemFrom = null;
            for (Block elem : froms) {
                if (elem.getType().equals("From")) {
                    elemFrom = (Block) elem;

                    // read the 'GotoTag' parameter
                    Parameter paramFrom = elemFrom
                            .getParameterByName("GotoTag");

                    if (paramFrom == null) {
                        EventHandler.handle(
                                EventLevel.CRITICAL_ERROR, "ReplacingGotoFromBlockState", "",
                                "Block " 
                                    + elemGoto.getReferenceString()
                                    + " does not have parameter \"GotoTag\"");
                        continue; // Skip rest and continue with other blocks
                    }

                    String fromTagValue = paramFrom.getStringValue();

                    // Compare the two parameters
                    if (fromTagValue.equals(gotoTagValue)) {
                        mergeGotoFrom(elemGoto, elemFrom, toDelete);
                        replace = true;
                    }
                }
            }
            // checking if a block corresponding to the goto block has
            // been found
            if (replace == false) {
                // if we reached here,then there must be gotoBlock and no
                // from block
                EventHandler.handle(EventLevel.ERROR,
                        "ReplacingGotoFromBlockState", "", "Found GoTo block "
                                + elemGoto.getReferenceString()
                                + "\n with no correponding From block");
                continue; // Skip rest and continue with other blocks
            }

        }

        // delete elements scheduled for removal
        for (GAModelElement e : toDelete) {
            e.removeMe();
        }

        stateMachine.setState(stateMachine.getHandlingPortsState());
    }

    private Pair<List<Block>, List<Block>> getGotoFromBlock() {

        List<Block> gotos = new ArrayList<Block>();
        List<Block> froms = new ArrayList<Block>();

        for (GAModelElement e : gaSystemModel.getAllElements(Block.class)) {
            if (((Block) e).getType().equals("Goto")) {
                gotos.add((Block) e);
            }
            if (((Block) e).getType().equals("From")) {
                froms.add((Block) e);
            }
        }
        return new Pair<List<Block>, List<Block>>(gotos, froms);
    }

    /**
     * Merge the input signal of the goto block with the output signal of the
     * from block
     * 
     * @param gotoBlock
     * @param fromBlock
     * @param toDelete --
     *            list of elements to delete after replacement is finished
     */
    private void mergeGotoFrom(Block gotoBlock, Block fromBlock,
            List<GAModelElement> toDelete) {

        // get the input data port of the goto block
        InDataPort inGotoPort = gotoBlock.getInDataPorts().get(0);

        // get reference to system (goto and from are in the same subsystem
        SystemBlock parentSystem = (SystemBlock) gotoBlock.getParent();

        // find the signal between goto block and his previous block
        Signal signalInGoto = null;
        try {
            for (Signal signal : parentSystem.getSignals()) {
                if (signal.getDstPort() == inGotoPort) {
                    signalInGoto = (Signal) signal;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (signalInGoto == null) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "ReplacingGotoFromBlockState", "",
                    "Could not find input signal for GoTo block "
                            + gotoBlock.getReferenceString(), "");
        }

        // get the src port of this signal
        Outport previousGotoPort = signalInGoto.getSrcPort();

        /*
         * get the output signal(s) of the from block, and bind each of them to
         * the previous block of goto block
         */
        for (OutDataPort port : fromBlock.getOutDataPorts()) {
            for (Signal signal : parentSystem.getSignals()) {
                // if the signal is an output of the from block
                if (signal.getSrcPort() == port) {
                    signal.setSrcPort(previousGotoPort);
                }
            }
        }
        // sign up remove first signal and both blocks for deletion
        toDelete.add(signalInGoto);
        toDelete.add(fromBlock);
        toDelete.add(gotoBlock);
    }
}
