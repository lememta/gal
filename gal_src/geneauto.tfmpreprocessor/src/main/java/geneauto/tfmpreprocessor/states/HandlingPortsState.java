/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/HandlingPortsState.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2010-07-16 07:13:48 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.models.utilities.PortResolver;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;

/**
 * This state does some modifications related to block input, output and control
 * ports.
 * 
 */
public class HandlingPortsState extends FMPreprocessorState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public HandlingPortsState(FMPreprocessorTool machine) {
        super(machine, "HandlePorts");
    }

    /**
     * Logs the beginning port handling.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
    }

    @Override
    public void stateExecute() {

        /**
         * create port objects for the top-level subsystem
         */
        PortResolver.addPortsToTopSystem(gaSystemModel);


        /**
         * create connections between inport/outport block and corresponding
         * port object. Transfer port parameters to port object
         */
        PortResolver.connectPortBlocks(gaSystemModel);

        /*
         * Make sure that all control signals start in control port
         */
        PortResolver.backpropagateControlPorts(gaSystemModel);

        /*
         * Remove mux occurrences on control signal path.
         * 
         * TODO(TF551) Check if removing demux blocks from control signal is
         * necessary
         */
        PortResolver.cleanControlPath(gaSystemModel);

        /*
         * make sure input control ports of top level system are of correct type
         */
        PortResolver.checkSystemInControlPorts(gaSystemModel
                .getRootSystemBlock());

        stateMachine.setState(stateMachine.getReplacingGenericBlocksState());
    }
}
