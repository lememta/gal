/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/ReplacingGenericBlocksState.java,v $
 *  @version	$Revision: 1.46 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Port;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.ModelUtilities;
import geneauto.models.utilities.PortResolver;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;
import geneauto.utils.PrivilegedAccessor;

import java.util.HashMap;
import java.util.Map;

/**
 * This state replaces masked sub-system blocks and blocks defined by macro or
 * S-function by UserDefined blocks.
 * 
 * GenericBlocks can appear in model on two reasons:
 * 	- there was a chart block in the model
 * 	- a model was produced by some other tool than TSimulinkImport that created 
 *    block with no specific type, but added mask type
 *    
 *  TODO: it should be reconsidered in the future whether we need the maskType 
 *  property at all -- is we require applying masks in importer already, the 
 *  mask type is irrelevant. If the Stateflow block is the only exception 
 *  there should be an appropriate mechanism to reflect it (ToNa 08/12/09)
 */
public class ReplacingGenericBlocksState extends FMPreprocessorState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public ReplacingGenericBlocksState(FMPreprocessorTool machine) {
        super(machine, "ReplacingGenericBlocks");
    }

    /**
     * Logs the beginning of generic blocks replacement.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
    }

    /**
     * Replaces blocks LegacyFunction (S-Function), Macro, MaskedSubSystems 
     * and Charts.
     */
    public void stateExecute() {
    	// identify all blocks that need to be replaced and put them to 
    	// oldNewMapping    	
        Block bl = null;
        Block newBlock = null;
        BlockType newType = null;

        /**
         * temporary map for storing pairs of old and new blocks 
         */
        Map<Block, Block> oldNewMapping = new HashMap<Block, Block>();

        /* loop immediate children of the subsystem
         * put all blocks to replace in odNewMapping
         */
        for (GAModelElement el : gaSystemModel.getAllElements(Block.class)) {
            bl = (Block) el;
            /* when block has a mask and the mask type name does not equal
             * to block current type, see if there is a block type 
             * corresponding to block mask in block library
             */
            if (bl.getMaskType() != null && 
            		!(bl.getMaskType().equals(bl.getType()))) {
            	
            	newType = getBlockType(bl.getMaskType());            	
            	if (newType != null) {
            		newBlock = replaceMaskedBlock(bl, newType);
	                oldNewMapping.put(bl, newBlock);
            	}
            }
        }

        // replace blocks in in oldNewMapping with their new counterparts
        for (Block block : oldNewMapping.keySet()) {
        	block.replaceMe(oldNewMapping.get(block), true);
        }

        stateMachine.setState(stateMachine.getCheckingGenericBlocksState());
    }

    /**
     * Replaces a block with corresponding mask type
     * 
     * @param maskedBlock
     * @return
     */
    private Block replaceMaskedBlock(Block maskedBlock, BlockType newType) {
        Block result = null;

        String classpath = stateMachine.getSystemModelFactory()
                .getClassLocation().get(newType.getType());
        Object obj = PrivilegedAccessor.getObjectForClassName(classpath);

        if (obj != null && obj instanceof Block) {
            ModelUtilities.transferContent(maskedBlock, (Block) obj);
            ((Block) obj).setType(newType.getName());

            result = (Block) obj;
            
            if (maskedBlock instanceof SystemBlock) {
            	if (result instanceof ChartBlock) {
            		// copy chart parameters from masked block to chart block
            		result = processChart((SystemBlock) maskedBlock,
            								(ChartBlock) result);
            	}
            	
                // reseting block references in the ports
                result.cleanPorts();
                
                // in case of a system block reconnect ports and blocks
                if (result instanceof SystemBlock) {
                	PortResolver.connectPortBlocks((SystemBlock) result);
                }
            }

        } else {
            EventHandler.handle(
    			EventLevel.ERROR,
                this.getClass().getCanonicalName(),
                "",
                "Unable to find class"
		            + newType.getType()
		            + " in the classpath."
		            + " Unable to process mask of a block\n "
		            + maskedBlock.getReferenceString()
		            + "\n Please check that the block library " 
		            + "is correctly configured.",
		                "");
        }
        
        return result;
    }

    /**
     * Reads chart block parameters from subsystem contents and 
     * writes to the chart block
     * 
     * @param maskedBlock
     * @param newBlock
     */
    private ChartBlock processChart(SystemBlock maskedSubSystem, 
    											ChartBlock chartBlock) {
    	BlockParameter blockParam = null;
        Expression value;

        /**
         * Break connection between block ports and inner blocks The
         * blocks inside of a subsystem are removed later and this
         * link gets orphaned
         * 
         * TODO: this is workaround. If be subsystem contents are
         * properly removed, then the reference should be reset when
         * the block is removed from the model (ToNa 21/10/08)
         */
        for (Port p : maskedSubSystem.getAllPorts()) {
            if (p instanceof InEdgeEnablePort)
                ((InEdgeEnablePort) p)
                        .setRelatedToInportBlock(false);
            p.setSourceBlock(null);
        }

        for (Block block : maskedSubSystem.getBlocks()) {
            if (block.getType().equals("S-Function")) {
                // Set the chart file number of the chart block
                value = block.getParameterByName("Tag").getValue();
                String tag = ((StringExpression) value).getLitValue();
                String[] splitted = tag.split(" ");
                if (splitted.length >= 1) {
                    String chartFileNumber = splitted[splitted.length - 1];
                    blockParam = new BlockParameter();
                    blockParam.setName("ChartFileNumber");
                    blockParam.setValue(new StringExpression(
                            chartFileNumber));
                    chartBlock.addParameter(blockParam);
                }

            }
        }

        if (blockParam == null) {
            EventHandler.handle(
                     EventLevel.ERROR,
                     this.getClass().getCanonicalName(),
                      "",
                     "Unable to replace StateFlow block \n "
                     + maskedSubSystem.getReferenceString()
                     + "\n StateFlow's inner S-Function " 
                     + "block is missing.",
                     "");
        }    	
        
        return chartBlock;
    }
    
    /**
     * Given a block type name, returns the corresponding block type.
     * 
     * @param blockTypeName
     *            The name of the block type we want to access.
     * @return The correct block type.
     */
    private BlockType getBlockType(String blockTypeName) {
        return stateMachine.getBlockLibrary().getBlockTypeByName(blockTypeName);
    }
}
