/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/ExpandingVirtualSubSystemsState.java,v $
 *  @version	$Revision: 1.43 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.gaenumtypes.FunctionStyle;
import geneauto.models.gasystemmodel.GASystemModelRoot;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Port;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This replaces all virtual subsystems with their content.
 * 
 */
public class ExpandingVirtualSubSystemsState extends FMPreprocessorState {

    // ***** START: flag "--inline" ******
	/** The tool is ran in the inline mode */
	private boolean inlineMode;
    // ***** END: flag "--inline" ******

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public ExpandingVirtualSubSystemsState(FMPreprocessorTool machine) {
        super(machine, "ExpandingVirtualSubSystems");
    }

    /**
     * Logs the beginning of virtual subsystems' expansion.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
    }

    /**
     * The stateExecute method browses the whole model. When a subSystem is
     * found: - if the system has no parent, then it is the main system - do
     * nothing. - else : call the method flatten subsystem.
     */
    public void stateExecute() {
    	
        // ***** START: flag "--inline" ******
        // Set the inline mode internal flag
        inlineMode = ArgumentReader.isFlagSet(GAConst.ARG_INLINE);
        // ***** END: flag "--inline" ******
        
        // call flatten subsystem for each subsystem
        // it will recursively process the subsystems inside of this block
        // normally there is only one element in this list, but it does
        // not cost much to use iteration just in case
        for (GASystemModelRoot element : gaSystemModel.getElements()) {
            // make sure the element really is system block
            if (element instanceof SystemBlock) {
                flattenSubSystem((SystemBlock) element);

            } else {
                EventHandler
                        .handle(
                                EventLevel.CRITICAL_ERROR,
                                "ExpandingVirtualSubSystemsState.stateExecute",
                                "",
                                "Block \""
                                        + element.getReferenceString()
                                        + "\" of type \""
                                        + element.getClass().getName()
                                        + "\" detected as root element of system model tree."
                                        + "\"SystemBlock\" expected.", "");

            }
        }

        /*
         * all subsystems are flattened now, check the names of remaining
         * systems are unique
         * 
         * TODO(TF550) Check and fix mangling of signals and systems name in
         * FMPreprocessor
         */
        mangleSubsystemNames();

        mangleSignalName();
        
        stateMachine.setState(stateMachine.getTestingAlgebraicLoopsState());
    }

    private void mangleSignalName() {

        for (GAModelElement element : gaSystemModel
                .getAllElements(SystemBlock.class)) {

            List<String> usedNames = new ArrayList<String>();
            int suffix = 0;

            for (Signal s : ((SystemBlock) element).getSignals()) {
                if ((s.getName() != null) && (!s.getName().equals(""))) {
                    if (usedNames.contains(s.getName())) {
                        s.setName(s.getName() + "_" + suffix);
                        suffix++;
                    } else {
                        usedNames.add(s.getName());
                    }
                }
            }
        }

    }

    /**
     * This method checks, whether the given SubSystem should be flattened,
     * inlined at a later stage (code model generation) or left intact.
	 * Note! If the code generator in ran in the "inline" mode
     * then atomic subsystems will be inlined in tfmcodemodelgenerator, but they stay
     * unflattened in the System Model! 
	 *
     * @param system
     *            the subsystem to test and possibly flatten.
     * 
     * TODO(TF549) 090129 (?) Support code generation type "inline" to mark systems as
     * virtual 
     * TODO (TF549) AnTo 100820 Revise the above task - It might be solved or easily 
     * solvable after after TF:869 is done. 
     */
    private synchronized void flattenSubSystem(SystemBlock system) {
        /* Only subSystems can contain other subSystems. */
        SystemBlock parent = (SystemBlock) system.getParent();

        /*
         * As the process is recursive the block list may change each time we
         * process lower level system. So we first freeze the list on children
         * and to work on that list later
         */
        List<SystemBlock> childSystems = new LinkedList<SystemBlock>();
        for (Block block : system.getBlocks()) {
            if (block instanceof SystemBlock) {
                childSystems.add((SystemBlock) block);
            }
        }
        
        boolean toFlatten = false;
        
        if (parent == null) {
            /* top level of the model is always non-virtual */
            system.setStyle(FunctionStyle.FUNCTION);
        } else if (system.isControlled() || system.isEnabled()) {
            /* any controlled or enabled block is non-virtual */
            system.setStyle(FunctionStyle.FUNCTION);
        } else if (system.getMaskType() != null
                && system.getMaskType().equals("Stateflow")) {
            /* Stateflow blocks are non-virtual */
            system.setStyle(FunctionStyle.FUNCTION);
        } else {        	
        	/* other system - check the "atomic" flag and "inline mode" option */
            Parameter atomicParam = system.getParameterByName("AtomicSubSystem");
            if (atomicParam != null) {
                String parameterValue = atomicParam.getStringValue();
                if (parameterValue.equals("off")) {
                    toFlatten = true;
                } else {
                    // ***** START: flag "--inline" ******
                	// Atomic subsystem
                	if (inlineMode) {                	
                    	// To be inlined at a later stage
                		system.setStyle(FunctionStyle.INLINE);
	                } else {
                    	// Stays as a function
	                	system.setStyle(FunctionStyle.FUNCTION);
	                }
                    // ***** END: flag "--inline" ******
                }
            } else {
                // The user has not specified anything about the atomicity,
            	// we assume it is virtual.
                toFlatten = true;
            }
        }

        /*
         * if the current subsystem should be flatten, we now flatten it
         */

        if (toFlatten) {

            EventHandler.handle(EventLevel.DEBUG,
                    "ExpandingVirtualSubSystemsState", "",
                    "Starting flattening of system " + system.getName() + ".");

            // rewrite ports
            replaceInports(system, parent);
            replaceOutPorts(system, parent);
            // move blocks and signals away from current subsystem
            moveModelElements(system, parent);
            // remove the subsystem
            system.removeMe();

            EventHandler.handle(EventLevel.DEBUG,
                    "ExpandingVirtualSubSystemsState", "",
                    "Ending flattening of system " + system.getName() + ".");

            // make sure the signal names are unique after flattening
            mangleSignalNames(parent);
        }

        /*
         * iterate all system blocks and check if they need to be flattened too.
         */
        for (SystemBlock block : childSystems) {
            flattenSubSystem(block);
        }
    }

    /**
     * Method which role is to replace the input ports and Inport blocks.
     * 
     * @param system
     *            the system which is to be deleted
     * @param parent
     *            the parent system.
     */
    private synchronized void replaceInports(SystemBlock system,
            SystemBlock parent) {

        // list of signals to delete after the loop has finished
        List<Signal> toDelete = new ArrayList<Signal>();
        // list of blocks to delete after the loop has finished
        List<Block> blocksToDelete = new ArrayList<Block>();

        // List of all input ports
        // since we have ports in different attributes we need to
        // put them together in one list now
        List<Inport> inputPorts = system.getAllInports();
        if (inputPorts.size() == 0) {
            // no ports -- nothing to do
            return;
        }

        /*
         * make list of all blocks that are marked as source block for any of
         * the input ports, make list on output ports for those blocks
         */
        List<Outport> inputPortBlockPorts = new ArrayList<Outport>();
        Block tmpBlock;
        for (Inport p : inputPorts) {
            tmpBlock = p.getSourceBlock();
            // we delete all outport blocks regardless of if there was match
            // any outgoing signals or not
            if (tmpBlock != null) {
                blocksToDelete.add(tmpBlock);
                // store data and control ports of this block in a list
                inputPortBlockPorts.addAll(tmpBlock.getAllOutports());
            }
        }

        /*
         * Loop outer signals and put the ones that end in this subsystem to a
         * map
         */
        Map<Port, Signal> outerSignals = new HashMap<Port, Signal>();
        for (Signal outerSignal : parent.getSignals()) {
            if (inputPorts.contains(outerSignal.getDstPort())) {
                outerSignals.put(outerSignal.getDstPort(), outerSignal);
                // the port object will be deleted. Mark all outer signals
                // to be deleted too
                if (!toDelete.contains(outerSignal)) {
                    toDelete.add(outerSignal);
                }
            }
        }

        /*
         * loop all signals inside the subsystem. If the signals start in any of
         * the inport blocks replace them with corresponding signal outside the
         * subsystem
         */
        Signal outerSignal;
        Signal outerSignalCopy;
        for (Signal innerSignal : system.getSignals()) {
            if (inputPortBlockPorts.contains(innerSignal.getSrcPort())) {
                tmpBlock = (Block) innerSignal.getSrcPort().getParent();
                outerSignal = outerSignals.get(tmpBlock.getPortReference());
                if (innerSignal.getName() != null) {
                    outerSignal.setName(innerSignal.getName());
                }
				// If the signal contains observation points, then these need to be 
                // moved to the remaining logically equivalent signal
            	if (innerSignal.isObservable()) {
            		outerSignal.addObservationPoints(innerSignal.getObservationPoints());           		
            	}
                // if outer signal was found, bind it to target of inner signal
                if (outerSignal != null) {
                    // one outer signal may correspond to several input signals
                    // make copy
                    outerSignalCopy = (Signal) outerSignal.getCopy();
                    outerSignalCopy.setDstPort(innerSignal.getDstPort());
                    parent.addSignal(outerSignalCopy);
                } else {
                    blocksToDelete.remove(tmpBlock);
                    tmpBlock.setPortReference(null);
                }

                // delete the inner signal regardless of whether outer signal
                // was found -- the block will be deleted anyway
                if ((outerSignal != null) && (!toDelete.contains(innerSignal))) {
                    toDelete.add(innerSignal);
                }
            }
        }

        // we remove the signals which must be removed
        for (Signal s : toDelete) {
            s.removeMe();
        }

        // we remove the blocks which must be removed
        for (Block block : blocksToDelete) {
            block.removeMe();
        }
    }

    /**
     * Method which role is to replace the output ports of the subsystem Block
     * and the outPort Blocks from it.
     * 
     * @param system
     *            the system which is to be deleted
     * @param parent
     *            the parent system.
     */
    private synchronized void replaceOutPorts(SystemBlock system,
            SystemBlock parent) {

        // list of signals to delete after the loop has finished
        List<Signal> toDelete = new ArrayList<Signal>();
        // list of blocks to delete after the loop has finished
        List<Block> blocksToDelete = new ArrayList<Block>();

        // List of all output ports
        // since we have ports in different attributes we need to
        // put them together in one list now
        List<Outport> outputPorts = system.getAllOutports();
        if (outputPorts.size() == 0) {
            // no ports, nothing to do
            return;
        }

        /*
         * make list of all blocks that are marked as source block for any of
         * the output ports make list on input ports for those blocks
         */
        List<Inport> outputPortBlockPorts = new ArrayList<Inport>();
        Block tmpBlock;
        for (Outport p : outputPorts) {
            tmpBlock = p.getSourceBlock();
            if (tmpBlock != null) {
                // we delete all Outport blocks regardless of if there was match
                // any outgoing signals or not
                blocksToDelete.add(tmpBlock);
                // store data and control ports of this block in a list
                outputPortBlockPorts.addAll(tmpBlock.getAllInports());
            }
        }

        /*
         * Loop outer signals and put the ones that start from this subsystem to
         * a map
         */
        List<Signal> outerSignals = new LinkedList<Signal>();
        for (Signal outerSignal : parent.getSignals()) {
            if (outputPorts.contains(outerSignal.getSrcPort())) {
                outerSignals.add(outerSignal);
                // the port object will be deleted. Mark all outer signals
                // to be deleted too. This is to make sure, that we get rid
                // of outer signal even if there was no connections inside
                // of block
                if (!toDelete.contains(outerSignal)) {
                    toDelete.add(outerSignal);
                }
            }
        }

        /*
         * loop all signals inside the subsystem. If the signals end in any of
         * the outport blocks replace them with corresponding signal outside the
         * subsystem
         */
        for (Inport inp : outputPortBlockPorts) {
            tmpBlock = (Block) inp.getParent();
            Signal innerSignal = getIncomingSignal(inp, system);
            if (innerSignal == null) {
                innerSignal = getIncomingSignal(inp, (SystemBlock) system
                        .getParent());
            }

            if (outerSignals.size() > 0) {
                for (Signal outerSignal : outerSignals) {

                    // if outer signal was found, bind it to source of inner
                    // signal
                    if (outerSignal.getSrcPort() == tmpBlock.getPortReference()) {
                        // cancel the request to delete outer signal
                        if (toDelete.contains(outerSignal)) {
                            toDelete.remove(outerSignal);
                        }
        				// If the signal contains observation points, then these need to be 
                        // moved to the remaining logically equivalent signal
                    	if (innerSignal != null && innerSignal.isObservable()) {
                    		outerSignal.addObservationPoints(innerSignal.getObservationPoints());           		
                    	}
                        
                        // set outer signal to start from internal object
						// TODO (to AnTo) 0 Add TF. Review this step. If the inner signal is
						// missing, then maybe the port object and anything
						// connected to that one (the whole chain) can be
						// omitted? AnTo 100225
                        if (innerSignal == null) {
                        	EventHandler.handle(EventLevel.ERROR, "", "", 
                        			"Inner signal missing" +
                        			"\n port: " + outerSignal.getSrcPort().getReferenceString() +
                        			"\n outerSignal: " + outerSignal.getReferenceString());
                        	continue; // Skip and continue with the rest
                        }
                        outerSignal.setSrcPort(innerSignal.getSrcPort());
                    }
                }
                
            } else {
                blocksToDelete.remove(tmpBlock);
                tmpBlock.setPortReference(null);
            }

            // delete the inner signal regardless of whether outer signal
            // was found -- the block will be deleted anyway
            if ((outerSignals.size() > 0) && (!toDelete.contains(innerSignal))) {
                toDelete.add(innerSignal);
            }
        }

        // If any errors have occurred so far, then stop. 
        EventHandler.stopOnError(getClass().getName());
        
        // we remove the signals which must be removed
        for (Signal s : toDelete) {
            s.removeMe();
        }

        // we remove the blocks which must be removed
        for (Block block : blocksToDelete) {
            block.removeMe();
        }
    }

    private Signal getIncomingSignal(Inport inp, SystemBlock system) {
        for (Signal s : system.getSignals()) {
            if (s.getDstPort() == inp) {
                return s;
            }
        }
        return null;
    }

    /**
     * Moves signals and blocks to different parent element
     * 
     * @param system
     *            the system which is to be deleted
     * @param parent
     *            the parent system.
     */
    private synchronized void moveModelElements(SystemBlock system,
            SystemBlock newParent) {

        EventHandler.handle(EventLevel.DEBUG,
                "ExpandingVirtualSubSystemsState", "",
                "Moving model element \"" + system.getReferenceString()
                        + "\" 's under \"" + newParent.getReferenceString()
                        + "\".", "");
        // move all blocks
        newParent.addBlocks(system.getBlocks());
        system.setBlocks(null);
        // move all signals
        newParent.addSignals(system.getSignals());
        system.setSignals(null);
        // move all variables
        newParent.addVariables(system.getVariables());
        system.setVariables(null);
    }

    /**
     * Ensures that signals have unique names in a subsystem
     * 
     * @param system
     *            the system to check.
     * 
     */
    private synchronized void mangleSignalNames(SystemBlock system) {
        List<String> usedNames = new ArrayList<String>();
        int suffix = 0;

        for (GAModelElement element : system.getSignals()) {
            if (element.getName() != null) {
                if (usedNames.contains(element.getName())) {
                    element.setName(element.getName() + "_" + suffix);
                    suffix++;
                }
                usedNames.add(element.getName());
            }
        }
    }

    /**
     * Ensures that subsystems have unique names in a model
     * 
     * @param system
     *            the system to check.
     * 
     */
    private synchronized void mangleSubsystemNames() {
        List<String> usedNames = new ArrayList<String>();
        int suffix = 0;

        for (GAModelElement element : gaSystemModel
                .getAllElements(SystemBlock.class)) {
            if (element.getName() != null) {
                if (usedNames.contains(element.getName())) {
                    element.setName(element.getName() + "_" + suffix);
                    suffix++;
                }
                usedNames.add(element.getName());
            }
        }
    }

}
