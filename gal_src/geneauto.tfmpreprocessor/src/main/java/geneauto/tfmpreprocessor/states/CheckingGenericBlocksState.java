/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/CheckingGenericBlocksState.java,v $
 *  @version	$Revision: 1.19 $
 *	@date		$Date: 2012-02-21 10:12:24 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.GenericBlock;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;

/**
 * This state makes sure that no generic block still remains in the
 * GASystemModel. It also initialises attribute directFeedThrough of all the
 * blocks.
 * 
 */
public class CheckingGenericBlocksState extends FMPreprocessorState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public CheckingGenericBlocksState(FMPreprocessorTool machine) {
        super(machine, "CheckingGenericBlocks");
    }

    /**
     * Logs the beginning of generic blocks replacement checking.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
    }

    /**
     * Checks that all generic blocks were correctly replaced during the
     * previous step. Also set the attribute directFeedThrough of the
     * GAModelElement.
     */
    public void stateExecute() {
        for (GAModelElement element : gaSystemModel.getAllElements(Block.class)) {
            Block block = (Block) element;

            // checking generic blocks
            if (block instanceof GenericBlock) {
                EventHandler.handle(
                        EventLevel.ERROR,
                        "TFMPreprocessor",
                        "",
                        "Unsupported block type: " + block.getType()
                        + "\n Block: " 
                        + block.getReferenceString(),
                        "");
            }

            // initialising directFeedThrough attribute
            BlockType blockType = block.getBlockType(); 
            if (blockType != null) {
                block.setDirectFeedThrough(blockType.isDirectFeedThrough());
            }

        }

        stateMachine
                .setState(stateMachine.getExecuteBlockPreprocessorsState());
    }
}
