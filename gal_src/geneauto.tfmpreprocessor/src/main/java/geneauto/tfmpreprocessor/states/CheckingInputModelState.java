/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/CheckingInputModelState.java,v $
 *  @version	$Revision: 1.16 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tfmpreprocessor.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tfmpreprocessor.main.FMPreprocessorTool;

import java.util.ArrayList;
import java.util.List;

/**
 * This state is the second state of the tool FMPreprocessor. It makes sure that
 * all blocks which are read from the model factory are recognised by the
 * GABlockLibrary.
 * 
 * TODO(TF548) Reconsider necessity of the checking part in FMPreprocessor
 * 
 */
public class CheckingInputModelState extends FMPreprocessorState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public CheckingInputModelState(FMPreprocessorTool machine) {
        super(machine, "CheckingInputModel");
    }

    /**
     * Logs the beginning of input model checking.
     */
    public void stateEntry() {
        super.stateEntry();

        gaSystemModel = stateMachine.getSystemModel();
    }

    /**
     * Records original name for each model element
     * 
     * Checks that : <br>
     * <dd> - each block is declared in the block library.
     * 
     */
    public void stateExecute() {

        GABlockLibrary blockLib = (GABlockLibrary) stateMachine
                .getBlockLibrary();
        List<String> blockTypes = new ArrayList<String>();
        for (GAModelElement modelElement : blockLib.getAllElements()) {
            BlockType blockType = (BlockType) modelElement;
            blockTypes.add(blockType.getName());
        }

        for (GAModelElement modelElement : gaSystemModel.getAllElements()) {

            if (modelElement instanceof Block) {
                Block block = (Block) modelElement;
                if (!blockTypes.contains(block.getType())) {
                    EventHandler
                            .handle(
                                    EventLevel.ERROR,
                                    "TFMPreprocessor",
                                    "",
                                    "Block "
                                            + block.getType()
                                            + " was not found into the library file. "
                                            + "Reference: "
                                            + block.getExternalID(), "");
                }
            }
        }
        stateMachine.setState(stateMachine.getHandlingPortsState());
    }

}
