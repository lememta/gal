/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/InitializingState.java,v $
 *  @version	$Revision: 1.27 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmrefactoring.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.tsmrefactoring.main.SMRefactoring;
import geneauto.tsmrefactoring.main.SMRefactoringTool;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * First state which is executed by the FMPreprocessor tool. It checks the java
 * parameters, imports the model from the xml file, reads the block library,
 * initialises the whole tool.
 * 
 * 
 */
public class CheckingModelState extends SMRefactoringState {

	Map<String, Block> mapBlockNamesToBlock;
	
    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public CheckingModelState(SMRefactoringTool machine) {
        super(machine, "Checking structure");
        
        mapBlockNamesToBlock = new HashMap<String, Block>();
    }

    /**
     * Initialises the modelFactory according to the FMPreprocessor parameters.
     * 
     * Reads the systemModel. Doing this, the library file is read too.
     */
    public void stateExecute() {
        
    	GASystemModel model = getMachine().getSystemModel();
    	
    	
    	for (Object bl : model.getAllElements(SystemBlock.class)){
    		SystemBlock block = (SystemBlock) bl;
    		// Checks if a Subsystem has no input. Here any Subsystem is atomic because flattening has already been done
			if (block.getInDataPorts().size() == 0){
				EventHandler.handle(
                    EventLevel.ERROR,
                    "TSMRefactoring",
                    "",
                    "This SystemBlock has no inputs and is meant to be transformed as node. Impossible to continue transformation.",
                    "Block: " + block.getOriginalFullName());
			}
			
			// Check for duplicates names for blocks
			for (Block innerBl : block.getBlocks()){
    			String name = innerBl.getPathString("_", PathOption.COMPLETE);
    			name = name.replaceAll("\\\\n", "_")
        				.replaceAll(" ", "_")
        				.replaceAll("#", "sharp")
        				.replaceAll("[^A-Za-z0-9_ ]","")
        				.replaceAll("(^[0-9])","var$1");
    			if (mapBlockNamesToBlock.containsKey(name)){
    				Block otherBlock = mapBlockNamesToBlock.get(name);
    				EventHandler.handle(
	                    EventLevel.ERROR,
	                    "TSMRefactoring",
	                    "",
	                    "Two Blocks will have the same name after transformation.\n" +
	                    "Please consider using different names or set containers as Atomic.\n" +
	                    "Impossible to continue transformation.",
	                    "Block 1: " + innerBl.getOriginalFullName() + "\n" +
	                    "Block 2: " + otherBlock.getOriginalFullName());
    			} else {
    				mapBlockNamesToBlock.put(name, innerBl);
    			}
	    	}
			mapBlockNamesToBlock.clear();
    	}
    	
        getMachine().setState(new LookupBlockHandlingState(getMachine()));
    }

}
