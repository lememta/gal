/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/InitializingState.java,v $
 *  @version	$Revision: 1.27 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmrefactoring.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.tsmrefactoring.main.SMRefactoring;
import geneauto.tsmrefactoring.main.SMRefactoringTool;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;
import geneauto.xtext.lustre.BasicType;
import geneauto.xtext.lustre.DataType;
import geneauto.xtext.lustre.Inputs;
import geneauto.xtext.lustre.LustreFactory;
import geneauto.xtext.lustre.Node;
import geneauto.xtext.lustre.Program;
import geneauto.xtext.lustre.Variable;
import geneauto.xtext.lustre.VariablesList;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import sun.nio.cs.ext.MacHebrew;

import com.sun.org.apache.xalan.internal.xsltc.runtime.Parameter;

/**
 * First state which is executed by the FMPreprocessor tool. It checks the java
 * parameters, imports the model from the xml file, reads the block library,
 * initialises the whole tool.
 * 
 * 
 */
public class LookupBlockHandlingState extends SMRefactoringState {

	private GASystemModel systemModel;
	
	private Program lustreProgram;
	
	private LustreFactory factory;
	
	private SMRefactoringTool machine;
	
    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public LookupBlockHandlingState(SMRefactoringTool machine) {
        super(machine, "Initializing");
        systemModel = machine.getSystemModel();
        this.machine = machine;
        factory = machine.getLustreFactory();
    }

    /**
     * Initialises the modelFactory according to the FMPreprocessor parameters.
     * 
     * Reads the systemModel. Doing this, the library file is read too.
     */
    public void stateExecute() {
        
    	lustreProgram = machine.getLustreProgram();
    	
    	List<Block> lstLookup = systemModel.getRootSystemBlock().getAllBlocksByType("Lookup");
    	
    	for (Block lookupBl: lstLookup){
    		List<geneauto.models.gasystemmodel.common.Node> lstPathNodes = lookupBl.getPath(PathOption.COMPLETE);
    		String fullBlockName = "";
    		int listSize = lstPathNodes.size();
    		for (int i=0; i< listSize - 1; i++){
    			String nodeName = lstPathNodes.get(i).getName();
    			nodeName = nodeName
    					.replaceAll("\\\\n", "_")
        				.replaceAll(" ", "_")
        				.replaceAll("#", "sharp")
        				.replaceAll("[^A-Za-z0-9 ]","")
        				.replaceAll("(^[0-9])","var$1");
    			fullBlockName += nodeName + "_";
    		}
    		fullBlockName += lstPathNodes.get(listSize - 1).getName()
    				.replaceAll("\\\\n", "_")
    				.replaceAll(" ", "_")
    				.replaceAll("#", "sharp")
    				.replaceAll("[^A-Za-z0-9 ]","")
    				.replaceAll("(^[0-9])","var$1");
    		
    		//String fullBlockName = lookupBl.getPathString("_", PathOption.COMPLETE);
    		
//    		fullBlockName = fullBlockName
//    				.replaceAll("\\\\n", "_")
//    				.replaceAll(" ", "_")
//    				.replaceAll("#", "sharp")
//    				.replaceAll("[^A-Za-z0-9 ]","")
//    				.replaceAll("(^[0-9])","var$1");
    		
    		BlockParameter inputValuesParameter = lookupBl.getParameterByName("InputValues");
    		Expression inputValuesParameterValue = inputValuesParameter.getValue();
    		BlockParameter tableParameter = lookupBl.getParameterByName("Table");
    		Expression tableParameterValue = tableParameter.getValue();
    		
    		Node lookupNode = factory.createNode();
    		lookupNode.setName(fullBlockName + "_Lookup");
    		
    		VariablesList listInputVar = factory.createVariablesList();
    		Variable inputVar = factory.createVariable();
    		inputVar.setName("in0");
    		listInputVar.getVariables().add(inputVar);
    		DataType inputVarDT = factory.createDataType();
    		inputVarDT.setBaseType(BasicType.DOUBLE);
    		listInputVar.setType(inputVarDT);
    		Inputs inputs = factory.createInputs();
    		inputs.getInputs().add(listInputVar);
    		lookupNode.setInputs(inputs);
    		
    		lustreProgram.getNodes().add(lookupNode);
    	}
    	
    	machine.setLustreProgram(lustreProgram);
    	
        getMachine().setState(new RemoveScopeBlocksState(getMachine()));
    }

}
