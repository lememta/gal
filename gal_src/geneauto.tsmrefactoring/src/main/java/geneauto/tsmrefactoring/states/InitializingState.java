/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tfmpreprocessor/src/main/java/geneauto/tfmpreprocessor/states/InitializingState.java,v $
 *  @version	$Revision: 1.27 $
 *	@date		$Date: 2011-07-07 12:23:34 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsmrefactoring.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.tsmrefactoring.main.SMRefactoring;
import geneauto.tsmrefactoring.main.SMRefactoringTool;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.List;

/**
 * First state which is executed by the FMPreprocessor tool. It checks the java
 * parameters, imports the model from the xml file, reads the block library,
 * initialises the whole tool.
 * 
 * 
 */
public class InitializingState extends SMRefactoringState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public InitializingState(SMRefactoringTool machine) {
        super(machine, "Initializing");
    }

    /**
     * Initialises the modelFactory according to the FMPreprocessor parameters.
     * 
     * Reads the systemModel. Doing this, the library file is read too.
     */
    public void stateExecute() {
        // get parameters.
        String inputFile = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);
        String outputFile = ArgumentReader.getParameter(GAConst.ARG_OUTPUTFILE);
        String libraryFile = ArgumentReader.getParameter(GAConst.ARG_LIBFILE);
        String tempPath = ArgumentReader.getParameter(GAConst.ARG_TMPFOLDER);

        // check for read/write permissions
        FileUtil.assertCanRead(inputFile);
        FileUtil.assertCanWrite(outputFile);
        FileUtil.assertCanWrite(tempPath);

        // initialise the library files
        List<String> libraryFiles = new ArrayList<String>();
        if (ArgumentReader.getParameter(GAConst.ARG_DEFLIBFILE) != null) {
            libraryFiles.add(ArgumentReader
                    .getParameter(GAConst.ARG_DEFLIBFILE));
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    "", "",
                    "Default block library is missing", "");
        }
        if (libraryFile != null) {
            libraryFiles.add(libraryFile);
        }

        // initialises the BlockLibraryFactory
		BlockLibraryFactory blockLibraryFactory = new BlockLibraryFactory(
				new GABlockLibrary(), SMRefactoring.TOOL_NAME, libraryFiles, null);

        // initialises the systemModelfactory
        SystemModelFactory systemModelFactory = new SystemModelFactory(
                new GASystemModel(), SMRefactoring.TOOL_NAME, inputFile, outputFile,
                blockLibraryFactory, null);

        getMachine().setSystemModelFactory(systemModelFactory);
        // Read model
        getMachine().getSystemModelFactory().readModel();
        getMachine().setSystemModel(
                getMachine().getSystemModelFactory().getModel());

        getMachine().setBlockLibrary(
                (GABlockLibrary) blockLibraryFactory.getModel());

        getMachine().setState(new CheckingModelState(getMachine()));
    }

}
