package geneauto.tsmrefactoring.utils;

public interface TSMRefactoringConstant {

	public final static String GA_BLOCK = "Block";
    public final static String GA_INDATAPORT = "InDataPort";
    public final static String GA_INDATAPORT_LIST = "inDataPorts";
    public final static String GA_PORTNUMBER = "portNumber";
    public final static String GA_OUTDATAPORT = "OutDataPort";
    public final static String GA_OUTDATAPORT_LIST = "outDataPorts";
    public final static String GA_SRCPORT = "srcPort";
    public final static String GA_DSTPORT = "dstPort";
    public final static String GA_SIGNAL = "Signal";
    public final static String GA_SIGNAL_LIST = "signals";
    public final static String GA_ID = "id";
    public final static String GA_NAME = "name";
    

    /** Special Constant Strings * */
    public final static String NOT_USED = "0";
    
}
