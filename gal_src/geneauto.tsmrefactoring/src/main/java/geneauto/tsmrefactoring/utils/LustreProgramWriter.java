package geneauto.tsmrefactoring.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import geneauto.xtext.lustre.LustreFactory;
import geneauto.xtext.lustre.Program;

public class LustreProgramWriter {
    
	/**
	 * The program to write
	 */
	private Program lustreProgram;
	
	/**
	 * The URI to the file to create
	 */
	private URI fileURI;
	
	public LustreProgramWriter(Program lustreProgram, URI outputFileURI) {
		this.lustreProgram = lustreProgram;
		this.fileURI = outputFileURI;
	}
	
	public void write() {
		
//		EPackage.Registry.INSTANCE.put(LustrePackage.eNS_URI, LustrePackage.eINSTANCE);
//    	Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("lus", LustreFactory.eINSTANCE);
		
		XtextResourceSet resourceSet = new XtextResourceSet();
	    resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
	    resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("lus", LustreFactory.eINSTANCE);

	    // Create new resource for this file
	    Resource xtextLustreResource = resourceSet.createResource(fileURI.trimFileExtension().appendFileExtension("xmi"));
	    
	    try {
	    	// Add options to resolve all proxies
		    Map<String,Boolean> options = new HashMap<String,Boolean>();
		    options.put(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
			
		    xtextLustreResource.getContents().add(lustreProgram);
		    xtextLustreResource.save(options);
		    
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    
	}
	
}
