package geneauto.emf.utilities;
import geneauto.utils.FileUtil;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;


public class EcoreUtils {

	/**
	 * Return the value of the "name" attribute of the given EObject or null, if
	 * it has no such  attribute
	 * 
	 * @param obj
	 * @return
	 */
	public static String getName(EObject obj) {
		for (EAttribute a : obj.eClass().getEAllAttributes()) {
			if ("name".equals(a.getName())) {
				return (String) obj.eGet(a);
			}
		}
		return null;
	}

	/**
	 * Return the value of the "id" attribute of the given EObject or null, if
	 * it has no such attribute
	 * 
	 * @param obj
	 * @return
	 */
	public static Integer getId(EObject obj) {
		for (EAttribute a : obj.eClass().getEAllAttributes()) {
			if ("id".equals(a.getName())) {
				return (Integer) obj.eGet(a);
			}
		}
		return null;
	}

	/**
	 * Return the value of the "name" attribute of the given EObject or simple
	 * name of the class, if it has no "name" attribute
	 * 
	 * @param obj
	 * @return
	 */
	public static String getNameOrType(EObject obj) {
		String name = getName(obj);
		if (name != null) {
			return name;
		} else {
			return obj.getClass().getSimpleName();
		}
	}

	/**
	 * Return a simple object identification string
	 * 
	 * @param obj
	 * @return
	 */
	public static String getSimpleRefStr(EObject obj) {
		if (obj == null) {
			return "null";
		}
		String name = getName(obj);
		if (name == null || name.isEmpty()) {
			name = "<unnamed>";
		}
		Integer id = getId(obj);
		String idPart = "";
		if (id != null) {
			idPart = " id=" + id;
		}
		return "Object " + name + idPart + " of type " + obj.getClass().getSimpleName();
	}

	/**
	 * Read an EMF object from the given file
	 * 
	 * @param inputFile
	 * @param ePackage 	Meta-model package of the object
	 * @param XML		Serialisation format: true=XML, false=XMI
	 *  
	 * @return read object
	 */
	public static EObject readEObject(String inputFile, EPackage ePackage, boolean XML) {
		// Read the model
		Debug.out(0, "Reading object from file:\n " + inputFile);      
		
		// create resource set and resource 
		ResourceSet resourceSet = new ResourceSetImpl();
		
		// Register package in local resource registry
		resourceSet.getPackageRegistry().put(ePackage.getNsURI(), ePackage);
	
		// Register the XML resource factory for the appropriate extension
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> extMap = reg.getExtensionToFactoryMap();
		
		// Extract the extension from file
		String ext = FileUtil.getFileExtension(inputFile);
		
		if (!extMap.containsKey(ext)) {
			if (XML) {
				extMap.put(ext, new XMLResourceFactoryImpl());
			} else {
				extMap.put(ext, new XMIResourceFactoryImpl());
			}			
		}
	
		Resource resource = null;
		try {
			resource = resourceSet.getResource(URI.createFileURI(inputFile), true);
		} catch (Exception e) {
			System.err.println("Unable to read object. Wrong format of the input file: " + inputFile);
			e.printStackTrace();
			return null;
		}
		
		// Load the model
		try {
	    	resource.load(null);
		} catch (IOException e) {
			System.err.println("Unable to read the object from file: " + inputFile);
			e.printStackTrace();
			return null;
		}
		
		if (resource.getContents().size() > 1) {
			System.err.println("Single rootnode expected. Input file: " + inputFile);
			return null;
		}
	
		// Get the root object
		EObject eobj = resource.getContents().get(0);
		
		return eobj;
	}
	
	/**
	 * Serialise an EMF object to a file
	 * 
	 * @param eObj
	 * @param outputFile
	 * @param XML		Serialisation format: true=XML, false=XMI
	 *  
	 * @return true, if successful
	 */
	public static boolean writeEObject(EObject eObj, String outputFile, boolean XML) {
		// Write the object
		String objName = getSimpleRefStr(eObj);
		Debug.out(0, "Serialising " + objName + " to file:\n " + outputFile);  
		
		Resource resource;
		if (XML) {
			resource = new XMLResourceImpl(URI.createFileURI(outputFile));
		} else {
			resource = new XMIResourceImpl(URI.createFileURI(outputFile));
		}
    	resource.getContents().add(eObj);
    	
    	try {
			resource.save(null);
			return true;
		} catch (IOException e) {
			Debug.out(0, e.getMessage());
			Debug.out(0, "Unable to store " + objName
							+ "\n to file: " + outputFile);
			return false;
		}
	}	
	
	/** Return a list containing the given element and all its children */
	public static List<EObject> getAllElements(EObject obj) {
		List<EObject> elements = new LinkedList<EObject>();
		elements.add(obj);
		EcoreUtils.getAllChildren(obj, elements);
		return elements;
	}

	/** Return all children of the given object */
	public static List<EObject> getAllChildren(EObject obj) {
		List<EObject> children = new LinkedList<EObject>();
		getAllChildren(obj, children);
		return children;
	}

	/** Return all children of the given object using the supplied accumulator list */
	public static void getAllChildren(EObject obj, List<EObject> children) {
		
		TreeIterator<EObject> tIter = obj.eAllContents();
		// Get all elements until tree is exceeded. We could also query first
		// for hasNext(), but this is slightly more efficient
		try {
			while(true) {
				children.add(tIter.next());
			}
		} catch (NoSuchElementException e) {
			// Done. Do nothing
		} catch (ArrayIndexOutOfBoundsException e) {
			// Done - Some implementations throw this instead of
			// NoSuchElementException. Do nothing
		}
	}

}
