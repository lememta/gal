/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.utilities;

import java.io.File;

/**
 * Utility methods related to the program itself.
 */
public class GAEMFSysInfo {
	
	/** A flag telling, whether the licence has already been displayed or not */
	private static boolean licenceDisplayed;
	
	/**
	 * @param toolName
	 * @return a licence message for a given tool.
	 */
    public static String getLicenceMessage(String toolName) {
    	String msg = ""; 
    	if (!licenceDisplayed) {
    		msg = 
	    		"*******************************************************************************\n" 
	    		+ GAEMFConst.TOOLSET_NAME + " : " + toolName + ". " + GAEMFConst.LICENSE_MESSAGE + "\n" +
	    		"*******************************************************************************\n";
    		licenceDisplayed = true;    		
    	}
    	return msg;
    }
	

	/**
	 * @return the absolute path of the binary installation directory of the
	 *         current program or null, when the program is run from source.
	 */
    public static String getBinInstallPath() {
        // Resolved location
        String path = "";
        // pointer for temporary file objects
        File tmpF;
        // location of the project or .jar file from ClassPath
        String lClassPath = new File(System.getProperty("java.class.path"))
                .getAbsolutePath();

        // locate the path containing GeneAuto.jar
        String[] splittedClassPath = lClassPath.split(File.pathSeparator);
        // if we will not find GALauncher in the ClassPath
        // the location must be the active directory
        lClassPath = ".";
        
       for (int i = 0; i < splittedClassPath.length; i++) {
			// We cannot look for the models package, since according to the
			// classpath it might point to the geneauto.models package location
			// that might be different.
            if (splittedClassPath[i].matches("(?i).*" + GAEMFSysInfo.class.getPackage().getName() + ".*")) {
                lClassPath = splittedClassPath[i];
                break;
            }
        }

        // binary distribution
        if (lClassPath.endsWith(".jar")) {
            tmpF = new File(lClassPath).getParentFile();
        }
		// source distribution
        else {
        	return null;
        }

        if (tmpF == null) {
            path = new File("").getAbsolutePath();
        } else {
            path = tmpF.getAbsolutePath();
        }
        return path;
    }

}
