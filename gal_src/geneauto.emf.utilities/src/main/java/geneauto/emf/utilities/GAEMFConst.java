/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.utilities;

/**
 * Constants for GAEcore tools.
 */
public interface GAEMFConst {

	// Extension Constant Strings
	
	// Gene-Auto Ecore format - GASystemModel
	public final static String EXT_GAESM = "gaesm";
	
	// Gene-Auto Ecore format - GACodeModel
	public final static String EXT_GAECM = "gaecm";
	
	// Gene-Auto Ecore format - GABlockLibrary
	public final static String EXT_GAEBL = "gaebl";
	
	// Toolset name
    public final static String TOOLSET_NAME = "Gene-Auto EMF tools";
    	
	// Licence message
    public final static String LICENSE_MESSAGE =
		"Copyright (C) 2009-2012 FERIA/IRIT, IoC at TUT and IB Krates OU\n" +
		"\n This program comes with ABSOLUTELY NO WARRANTY; " +
		"\n This is free software, and you are welcome to redistribute it" +
		" under certain conditions;" +
		"\n For licensing details see license.txt in the root " +
		"folder of the program.";

}
