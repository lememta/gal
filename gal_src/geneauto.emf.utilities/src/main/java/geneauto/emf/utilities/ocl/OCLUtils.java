package geneauto.emf.utilities.ocl;
import geneauto.emf.utilities.Debug;
import geneauto.utils.map.ListMapHandler;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EClassImpl;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.OCLInput;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.SendSignalAction;

/** Utility functions for OCL manipulation */
public class OCLUtils {
	
	/**
	 * Reads an OCL file, parses the constraints in it and builds a map that
	 * relates classes and constraints
	 * 
	 * @param oclFile
	 *            - File with OCL constraints
	 * @param objPackage
	 *            - Package corresponding to the implementation of the metamodel
	 *            that the OCL file corresponds to
	 * @param invariantsOnly
	 * 			  - Add only invariants to map
	 */
	public static Map<EModelElement, List<Constraint>> 
			createContraintMap(String oclFile, EPackage objPackage, boolean invariantsOnly) {
		
    	// Create OCL object for the given package
		OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, 
			EParameter, EObject, CallOperationAction, SendSignalAction, 
			org.eclipse.ocl.ecore.Constraint, EClass, EObject> ocl = createOCLForPackage(objPackage);

		return createContraintMap(oclFile, ocl, invariantsOnly);		
	}

	/**
	 * Reads an OCL file, parses the constraints in it and builds a map that
	 * relates classes and constraints
	 * 
	 * @param oclFile
	 *            - File with OCL constraints
	 * @param ocl
	 *            - The OCL facade object for the given metamodel implementation (EPackage)
	 * @param invariantsOnly
	 * 			  - Add only invariants to map
	 */
	public static Map<EModelElement, List<Constraint>> 
			createContraintMap(String oclFile, 
					OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, 
					EParameter, EObject, CallOperationAction, SendSignalAction, 
					org.eclipse.ocl.ecore.Constraint, EClass, EObject> ocl, 
					boolean invariantsOnly) {
		
		Debug.out(2, "Reading OCL file:\n  " + oclFile);
		
		ListMapHandler<EModelElement, Constraint> lmh = 
				new ListMapHandler<EModelElement, Constraint>(true);
		
		// Read the constraints from the OCL file
		Debug.out(3, "Start parsing OCL");
		List<Constraint> constraints = parseOCLFile(oclFile, ocl);
		Debug.out(3, "End parsing OCL");
		
		// Process all constraints and add them to map
		for (Constraint c : constraints) {
		    
		    Debug.out(6, "\nConstraint: " + c.getName() + " (" + c.getStereotype() + ")");
		    
			// Loop by constrained elements (contexts)
		    for (EModelElement cEl : c.getConstrainedElements()) {	

		    	Debug.out(6, "ConstrainedElement: " + ((EClassImpl) cEl).getName());
		    	
			    if (!invariantsOnly || "invariant".equals(c.getStereotype())) {
			    	lmh.put(cEl, c);
			    }	
			}
		}
		return lmh.getMap();		
	}

	/** Reads constraints from an OCL file */
	public static List<Constraint> parseOCLFile(
			String inputOCLFile,
			OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, org.eclipse.ocl.ecore.Constraint, EClass, EObject> ocl) {
		// Parse the contents of the OCL document
		List<Constraint> constraints;
		
		FileInputStream fstream;
		try {
			fstream = new FileInputStream(inputOCLFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			throw new RuntimeException();			
		}
		DataInputStream in = new DataInputStream(fstream);

		try {
			OCLInput document = new OCLInput(in);
			constraints = ocl.parse(document);
			
		} catch (ParserException e) {
			e.printStackTrace();
			throw new RuntimeException();			

		} finally {
		    try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return constraints;
	}

	/** Creates an OCL object, based on a metamodel implementation package */
	public static OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, org.eclipse.ocl.ecore.Constraint, EClass, EObject> createOCLForPackage(
			EPackage epack) {
		OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, 
			EParameter, EObject, CallOperationAction, SendSignalAction, 
			org.eclipse.ocl.ecore.Constraint, EClass, EObject> ocl;

		EcoreEnvironmentFactory fact = new EcoreEnvironmentFactory();
		fact.getEPackageRegistry().put(epack.getNsURI(), epack);
		//ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
		ocl = OCL.newInstance(fact);
		return ocl;
	}
	
}
