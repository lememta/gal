package geneauto.emf.utilities;

/** Utility class for debugging */
public class Debug {
	
	/** Level of debug output 0 = default, positive numbers increase output */
	private static int debugLevel = 2;
	
	public static void setDebugLevel(int debugLevel) {
		Debug.debugLevel = debugLevel;
	}

	/** Output debug info depending on level */
	public static void out(int level, String message) {
		if (level <= debugLevel) {
			System.out.println(message);
		}
	}

}
