package geneauto.emf.utilities.ocl;
import geneauto.emf.utilities.Debug;
import geneauto.emf.utilities.EcoreUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.SendSignalAction;


public class OCLValidator {
	
	/** OCL object */
	private OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, 
		EParameter, EObject, CallOperationAction, SendSignalAction, 
		org.eclipse.ocl.ecore.Constraint, EClass, EObject> ocl;
	
	/** Map of constraints in the given OCL resource */
	private Map<EModelElement, List<Constraint>> constraintMap;
	
	/** Reference to the metamodel implementation package */
	private EPackage ePackage;

	private File oclFile;
	
	/**
	 * Construct an OCLValidator from a given file with OCL constraints and
	 * metamodel implementation package
	 * 
	 * @param inputOCLFile
	 * @param metamodel implementation package
	 */
	public OCLValidator(String inputOCLFile, EPackage ePackage) {
		this.oclFile = new File(inputOCLFile);
		this.ePackage = ePackage;
		this.ocl = OCLUtils.createOCLForPackage(this.ePackage);
		this.constraintMap = OCLUtils.createContraintMap(inputOCLFile, this.ocl, true);
	}

	/**
	 * Validates the given object against the constraints in the given supplied map
	 * 
	 * @param obj
	 * @param constraintMap
	 * @param ocl Ocl object
	 * @return List of failed constraints or null, if none failed
	 */
	public List<Constraint> validateObj(EObject obj) {
		
		Debug.out(3, "-----------------------------");
		Debug.out(3, "  Validating object " + EcoreUtils.getSimpleRefStr(obj));
		Debug.out(3, "  against OCL file " + oclFile.getName() + " (" + oclFile.getAbsolutePath() + ")");
		
		List<Constraint> failedConstraints = new LinkedList<Constraint>();
		
		// Collect all types that the object implements to a list
		// The list starts with more general types and ends with the exact type
		// that the object implements
	    EClass objCls = obj.eClass();
		EList<EClass> objTypesEList = objCls.getEAllSuperTypes();
		// EList is unmodifiable. We'll create a copy
		List<EClass> objTypes = new ArrayList<EClass>(objTypesEList);
		objTypes.add(objCls);

		// Validate the object constraint-by-constraint
		int numConstraints = 0;
		for (EClass t : objTypes) {
		    List<Constraint> constraints = constraintMap.get(t);
		    if (constraints != null) {
		    	for (Constraint c : constraints) {
		    		numConstraints++;
				    Debug.out(6, "\nConstraint: " + c.getName() + " (" + c.getStereotype() + ")");
		    		// Check whether the model element satisfies the constraint (assuming invariants)
			        try {
			        	boolean valid = ocl.check(obj, c);
			        	Debug.out(4, "Constraint: " + c.getName() + " (" + c.getStereotype() + ")" + " satisfied=" + valid);
			        	if (!valid) {
			        		failedConstraints.add(c);
			        	}
			        } catch (Exception e) {
			        	System.out.println("Error evaluating constraint " + c.getName());
			        	e.printStackTrace();
					}
		    	}
		    }
		}
		
		if (numConstraints == 0) {
			Debug.out(3, "\n  No constraints defined!");
		} else if (failedConstraints.isEmpty()) {
			Debug.out(3, "\n  Element passed all " + numConstraints + " constraints!");
		} else {
			Debug.out(2, "\n  " + EcoreUtils.getSimpleRefStr(obj));
			Debug.out(2, "  OCL file: " + oclFile.getName() + " (" + oclFile.getAbsolutePath() + ")");
			Debug.out(2, "  Failed " + failedConstraints.size() + " constraints of " + numConstraints + ":");
			for (Constraint c : failedConstraints) {
				Debug.out(2, "\t" + c.getName());
			}
			Debug.out(2, "");
		}
		if (failedConstraints.isEmpty()) {
			// Do not return empty list
			failedConstraints = null;
		}
		
		return failedConstraints;
	}
	
}
