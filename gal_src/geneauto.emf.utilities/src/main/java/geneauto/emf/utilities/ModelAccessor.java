/**
 *  Gene-Auto EMF tools (http://www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *		http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *  	Tallinn University of Technology
 *		http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.utilities;

import geneauto.emf.models.genericmodel.GAModelElement;
import geneauto.emf.models.genericmodel.Model;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.utils.PrivilegedAccessor;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * This utility class contains methods to generically access model elements. It
 * relies on some the functionality in the Gene-Auto main tool-chain and
 * specialises it for the usage in GAEcore tools.
 */
public class ModelAccessor {

    /**
     * Returns list of immediate children of the current element. All elements
     * of type GAModelElement are returned if their parent (eContainer)
     * attribute refers to current element
     * 
     * NB! if the child element happens to contain collection, where each
     * element is another collection and this contained collection contains
     * GAModelElement instances then those are not returned
     * 
     * NB2! caution when modifying or overriding this method. There are other
     * methods in models package that assume that this method does not return
     * duplicate values. Relying on always checking the parent value guarantees
     * this. If an override adds any element without parent check we can not
     * guarantee any more non-duplication
     * 
     * @param root
     * @return list of immediate children of the root object 
     */
    @SuppressWarnings("rawtypes")
	public static List<GAModelElement> getChildren(GAModelElement root) {
        List<GAModelElement> elements = new LinkedList<GAModelElement>();
        Object attrValue;

        // Scan the fields of the given class put all occurrences of owned
        // GAModelElemens to the result list
        for (Field f : PrivilegedAccessor.getClassFields(root.getClass())) {
            try {
                f.setAccessible(true);
                attrValue = f.get(root);

                if (attrValue == null) {
                    continue;
                }

                if (attrValue instanceof GAModelElement) {

                    // attribute value is object of type GAModelElement
                    if (((GAModelElement) attrValue).eContainer() == root) {

                        // attribute value is owned element
                        elements.add((GAModelElement) attrValue);
                    }
                } else if (attrValue instanceof Collection) {

                    // attribute value is a collection, retrieve the collection
                    // contents
                    for (Object listEl : (Collection) attrValue) {
                        if (listEl != null
                                && listEl instanceof GAModelElement
                                && ((GAModelElement) listEl).eContainer() == root) {

                            // the list element is owned sub-element of the
                            // current class
                            elements.add((GAModelElement) listEl);
                        }
                    }
                }
            } catch (IllegalArgumentException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.getChildren", "",
                        "Unable to access field + " + f.getName()
                                + " of object " + root, e.getMessage());
            } catch (IllegalAccessException e) {
                EventHandler.handle(EventLevel.ERROR,
                        "GAModelElement.getChildren", "",
                        "Unable to access field + " + f.getName()
                                + " of object " + root, e.getMessage());
            }
        }

        return elements;
    }

    /**
     * Returns all children of current object or of its sub-type. Found elements
     * are added to the end of a list given as elements attribute
     * 
     * @param root
     * @param elements - list accumulator
     * @return list of all children of the root object 
     */
    public static List<GAModelElement> getAllChildren(GAModelElement root, List<GAModelElement> elements) {
        for (GAModelElement e : getChildren(root)) {

            // add element list
            elements.add(e);

            // check all children of the element and add them to the end of
            // elements list
            getAllChildren(e, elements);
        }

        return elements;
    }

    /**
     * Returns all children of current object
     * 
     * Added for backward compatibility and for usage in for statements when
     * required for recursive usage use List<GAModelElement>
     * getAllChildren(List<GAModelElement> elements) instead
     * 
     * @param root
     * @return list of all children of the root object 
     */
    public static List<GAModelElement> getAllChildren(GAModelElement root) {
        List<GAModelElement> elements = new LinkedList<GAModelElement>();
        return getAllChildren(root, elements);
    }

    /**
     * Returns all model elements owned by the given Model
     * 
     * @param model
     * @return list of all children of the model
     */
    public static List<GAModelElement> getAllModelElements(Model model) {
        List<GAModelElement> elements = new LinkedList<GAModelElement>();        
        for (Object o : (List<?>) PrivilegedAccessor.getValue(model, "elements")) {
        	GAModelElement e = (GAModelElement) o;
        	elements.add(e);
        	getAllChildren(e, elements);
        }
        return elements;
    }
}
