/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.common.impl;

import geneauto.emf.models.common.CommonFactory;
import geneauto.emf.models.common.CommonPackage;
import geneauto.emf.models.common.CustomType;
import geneauto.emf.models.common.Function;
import geneauto.emf.models.common.HasVariable;
import geneauto.emf.models.common.NameSpaceElement;
import geneauto.emf.models.common.StandardFunction;
import geneauto.emf.models.common.Variable;

import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;

import geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;

import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;

import geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl;

import geneauto.emf.models.gacodemodel.expression.statemodel.StatemodelPackage;

import geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl;

import geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesPackage;

import geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl;

import geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl;

import geneauto.emf.models.gacodemodel.operator.OperatorPackage;

import geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl;

import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastPackage;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl;

import geneauto.emf.models.gadatatypes.GadatatypesPackage;

import geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.GafunctionalmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl;

import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;

import geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl;

import geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CommonPackageImpl extends EPackageImpl implements CommonPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameSpaceElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum standardFunctionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see geneauto.emf.models.common.CommonPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CommonPackageImpl() {
		super(eNS_URI, CommonFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CommonPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CommonPackage init() {
		if (isInited) return (CommonPackage)EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI);

		// Obtain or create and register package
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CommonPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		GadatatypesPackageImpl theGadatatypesPackage = (GadatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) instanceof GadatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) : GadatatypesPackage.eINSTANCE);
		GenericmodelPackageImpl theGenericmodelPackage = (GenericmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) instanceof GenericmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) : GenericmodelPackage.eINSTANCE);
		GacodemodelPackageImpl theGacodemodelPackage = (GacodemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) instanceof GacodemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) : GacodemodelPackage.eINSTANCE);
		ExpressionPackageImpl theExpressionPackage = (ExpressionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) instanceof ExpressionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) : ExpressionPackage.eINSTANCE);
		StatemodelPackageImpl theStatemodelPackage = (StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) instanceof StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) : StatemodelPackage.eINSTANCE);
		StatementPackageImpl theStatementPackage = (StatementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) instanceof StatementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) : StatementPackage.eINSTANCE);
		geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl theStatemodelPackage_1 = (geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) instanceof geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) : geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eINSTANCE);
		BroadcastPackageImpl theBroadcastPackage = (BroadcastPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) instanceof BroadcastPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) : BroadcastPackage.eINSTANCE);
		GaenumtypesPackageImpl theGaenumtypesPackage = (GaenumtypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) instanceof GaenumtypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) : GaenumtypesPackage.eINSTANCE);
		OperatorPackageImpl theOperatorPackage = (OperatorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) instanceof OperatorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) : OperatorPackage.eINSTANCE);
		GasystemmodelPackageImpl theGasystemmodelPackage = (GasystemmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) instanceof GasystemmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) : GasystemmodelPackage.eINSTANCE);
		geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl theCommonPackage_1 = (geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) instanceof geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) : geneauto.emf.models.gasystemmodel.common.CommonPackage.eINSTANCE);
		GastatemodelPackageImpl theGastatemodelPackage = (GastatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) instanceof GastatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) : GastatemodelPackage.eINSTANCE);
		GafunctionalmodelPackageImpl theGafunctionalmodelPackage = (GafunctionalmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) instanceof GafunctionalmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) : GafunctionalmodelPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);
		PortsPackageImpl thePortsPackage = (PortsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) instanceof PortsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) : PortsPackage.eINSTANCE);
		GablocklibraryPackageImpl theGablocklibraryPackage = (GablocklibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) instanceof GablocklibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) : GablocklibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theCommonPackage.createPackageContents();
		theGadatatypesPackage.createPackageContents();
		theGenericmodelPackage.createPackageContents();
		theGacodemodelPackage.createPackageContents();
		theExpressionPackage.createPackageContents();
		theStatemodelPackage.createPackageContents();
		theStatementPackage.createPackageContents();
		theStatemodelPackage_1.createPackageContents();
		theBroadcastPackage.createPackageContents();
		theGaenumtypesPackage.createPackageContents();
		theOperatorPackage.createPackageContents();
		theGasystemmodelPackage.createPackageContents();
		theCommonPackage_1.createPackageContents();
		theGastatemodelPackage.createPackageContents();
		theGafunctionalmodelPackage.createPackageContents();
		theBlocksPackage.createPackageContents();
		thePortsPackage.createPackageContents();
		theGablocklibraryPackage.createPackageContents();

		// Initialize created meta-data
		theCommonPackage.initializePackageContents();
		theGadatatypesPackage.initializePackageContents();
		theGenericmodelPackage.initializePackageContents();
		theGacodemodelPackage.initializePackageContents();
		theExpressionPackage.initializePackageContents();
		theStatemodelPackage.initializePackageContents();
		theStatementPackage.initializePackageContents();
		theStatemodelPackage_1.initializePackageContents();
		theBroadcastPackage.initializePackageContents();
		theGaenumtypesPackage.initializePackageContents();
		theOperatorPackage.initializePackageContents();
		theGasystemmodelPackage.initializePackageContents();
		theCommonPackage_1.initializePackageContents();
		theGastatemodelPackage.initializePackageContents();
		theGafunctionalmodelPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();
		thePortsPackage.initializePackageContents();
		theGablocklibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCommonPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CommonPackage.eNS_URI, theCommonPackage);
		return theCommonPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomType() {
		return customTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNameSpaceElement() {
		return nameSpaceElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunction() {
		return functionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_DataType() {
		return (EReference)functionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHasVariable() {
		return hasVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariable() {
		return variableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariable_DataType() {
		return (EReference)variableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariable_InitialValue() {
		return (EReference)variableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariable_IsConst() {
		return (EAttribute)variableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariable_IsOptimizable() {
		return (EAttribute)variableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariable_IsStatic() {
		return (EAttribute)variableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariable_IsVolatile() {
		return (EAttribute)variableEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStandardFunction() {
		return standardFunctionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonFactory getCommonFactory() {
		return (CommonFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		customTypeEClass = createEClass(CUSTOM_TYPE);

		nameSpaceElementEClass = createEClass(NAME_SPACE_ELEMENT);

		functionEClass = createEClass(FUNCTION);
		createEReference(functionEClass, FUNCTION__DATA_TYPE);

		hasVariableEClass = createEClass(HAS_VARIABLE);

		variableEClass = createEClass(VARIABLE);
		createEReference(variableEClass, VARIABLE__DATA_TYPE);
		createEReference(variableEClass, VARIABLE__INITIAL_VALUE);
		createEAttribute(variableEClass, VARIABLE__IS_CONST);
		createEAttribute(variableEClass, VARIABLE__IS_OPTIMIZABLE);
		createEAttribute(variableEClass, VARIABLE__IS_STATIC);
		createEAttribute(variableEClass, VARIABLE__IS_VOLATILE);

		// Create enums
		standardFunctionEEnum = createEEnum(STANDARD_FUNCTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GadatatypesPackage theGadatatypesPackage = (GadatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI);
		ExpressionPackage theExpressionPackage = (ExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		customTypeEClass.getESuperTypes().add(this.getNameSpaceElement());
		functionEClass.getESuperTypes().add(this.getNameSpaceElement());
		variableEClass.getESuperTypes().add(this.getNameSpaceElement());

		// Initialize classes and features; add operations and parameters
		initEClass(customTypeEClass, CustomType.class, "CustomType", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nameSpaceElementEClass, NameSpaceElement.class, "NameSpaceElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(functionEClass, Function.class, "Function", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunction_DataType(), theGadatatypesPackage.getGADataType(), null, "dataType", null, 0, 1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hasVariableEClass, HasVariable.class, "HasVariable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(variableEClass, Variable.class, "Variable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariable_DataType(), theGadatatypesPackage.getGADataType(), null, "dataType", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariable_InitialValue(), theExpressionPackage.getExpression(), null, "initialValue", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariable_IsConst(), ecorePackage.getEBoolean(), "isConst", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariable_IsOptimizable(), ecorePackage.getEBoolean(), "isOptimizable", "true", 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariable_IsStatic(), ecorePackage.getEBoolean(), "isStatic", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariable_IsVolatile(), ecorePackage.getEBoolean(), "isVolatile", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(standardFunctionEEnum, StandardFunction.class, "StandardFunction");
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.NONE);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ABS_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ACOS_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ACOSH_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ASIN_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ASINH_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ATAN_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ATAN2_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ATANH_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.COS_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.COSH_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.EXP_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.LOG_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.LOG10_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.REM_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.ROUND_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.SIGN_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.SIN_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.SINH_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.SQRT_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.TAN_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.TANH_FUN);
		addEEnumLiteral(standardFunctionEEnum, StandardFunction.TRANSPOSE_FUN);

		// Create resource
		createResource(eNS_URI);
	}

} //CommonPackageImpl
