/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.common;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gadatatypes.GADataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Abstract interface to identify system model elements that correspond to variables in generated program code
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.common.Variable#getDataType <em>Data Type</em>}</li>
 *   <li>{@link geneauto.emf.models.common.Variable#getInitialValue <em>Initial Value</em>}</li>
 *   <li>{@link geneauto.emf.models.common.Variable#isConst <em>Is Const</em>}</li>
 *   <li>{@link geneauto.emf.models.common.Variable#isOptimizable <em>Is Optimizable</em>}</li>
 *   <li>{@link geneauto.emf.models.common.Variable#isStatic <em>Is Static</em>}</li>
 *   <li>{@link geneauto.emf.models.common.Variable#isVolatile <em>Is Volatile</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.common.CommonPackage#getVariable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Variable extends NameSpaceElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #setDataType(GADataType)
	 * @see geneauto.emf.models.common.CommonPackage#getVariable_DataType()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	GADataType getDataType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.common.Variable#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(GADataType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.common.Variable#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDataType()
	 * @see #getDataType()
	 * @see #setDataType(GADataType)
	 * @generated
	 */
	void unsetDataType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.common.Variable#getDataType <em>Data Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Data Type</em>' containment reference is set.
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @see #setDataType(GADataType)
	 * @generated
	 */
	boolean isSetDataType();

	/**
	 * Returns the value of the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Value</em>' containment reference.
	 * @see #isSetInitialValue()
	 * @see #unsetInitialValue()
	 * @see #setInitialValue(Expression)
	 * @see geneauto.emf.models.common.CommonPackage#getVariable_InitialValue()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getInitialValue();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.common.Variable#getInitialValue <em>Initial Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Value</em>' containment reference.
	 * @see #isSetInitialValue()
	 * @see #unsetInitialValue()
	 * @see #getInitialValue()
	 * @generated
	 */
	void setInitialValue(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.common.Variable#getInitialValue <em>Initial Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInitialValue()
	 * @see #getInitialValue()
	 * @see #setInitialValue(Expression)
	 * @generated
	 */
	void unsetInitialValue();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.common.Variable#getInitialValue <em>Initial Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Initial Value</em>' containment reference is set.
	 * @see #unsetInitialValue()
	 * @see #getInitialValue()
	 * @see #setInitialValue(Expression)
	 * @generated
	 */
	boolean isSetInitialValue();

	/**
	 * Returns the value of the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Const</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Const</em>' attribute.
	 * @see #isSetIsConst()
	 * @see #unsetIsConst()
	 * @see #setIsConst(boolean)
	 * @see geneauto.emf.models.common.CommonPackage#getVariable_IsConst()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isConst();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.common.Variable#isConst <em>Is Const</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Const</em>' attribute.
	 * @see #isSetIsConst()
	 * @see #unsetIsConst()
	 * @see #isConst()
	 * @generated
	 */
	void setIsConst(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.common.Variable#isConst <em>Is Const</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsConst()
	 * @see #isConst()
	 * @see #setIsConst(boolean)
	 * @generated
	 */
	void unsetIsConst();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.common.Variable#isConst <em>Is Const</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Const</em>' attribute is set.
	 * @see #unsetIsConst()
	 * @see #isConst()
	 * @see #setIsConst(boolean)
	 * @generated
	 */
	boolean isSetIsConst();

	/**
	 * Returns the value of the '<em><b>Is Optimizable</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Optimizable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Optimizable</em>' attribute.
	 * @see #isSetIsOptimizable()
	 * @see #unsetIsOptimizable()
	 * @see #setIsOptimizable(boolean)
	 * @see geneauto.emf.models.common.CommonPackage#getVariable_IsOptimizable()
	 * @model default="true" unsettable="true"
	 * @generated
	 */
	boolean isOptimizable();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.common.Variable#isOptimizable <em>Is Optimizable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Optimizable</em>' attribute.
	 * @see #isSetIsOptimizable()
	 * @see #unsetIsOptimizable()
	 * @see #isOptimizable()
	 * @generated
	 */
	void setIsOptimizable(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.common.Variable#isOptimizable <em>Is Optimizable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsOptimizable()
	 * @see #isOptimizable()
	 * @see #setIsOptimizable(boolean)
	 * @generated
	 */
	void unsetIsOptimizable();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.common.Variable#isOptimizable <em>Is Optimizable</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Optimizable</em>' attribute is set.
	 * @see #unsetIsOptimizable()
	 * @see #isOptimizable()
	 * @see #setIsOptimizable(boolean)
	 * @generated
	 */
	boolean isSetIsOptimizable();

	/**
	 * Returns the value of the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Static</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Static</em>' attribute.
	 * @see #isSetIsStatic()
	 * @see #unsetIsStatic()
	 * @see #setIsStatic(boolean)
	 * @see geneauto.emf.models.common.CommonPackage#getVariable_IsStatic()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isStatic();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.common.Variable#isStatic <em>Is Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Static</em>' attribute.
	 * @see #isSetIsStatic()
	 * @see #unsetIsStatic()
	 * @see #isStatic()
	 * @generated
	 */
	void setIsStatic(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.common.Variable#isStatic <em>Is Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsStatic()
	 * @see #isStatic()
	 * @see #setIsStatic(boolean)
	 * @generated
	 */
	void unsetIsStatic();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.common.Variable#isStatic <em>Is Static</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Static</em>' attribute is set.
	 * @see #unsetIsStatic()
	 * @see #isStatic()
	 * @see #setIsStatic(boolean)
	 * @generated
	 */
	boolean isSetIsStatic();

	/**
	 * Returns the value of the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Volatile</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Volatile</em>' attribute.
	 * @see #isSetIsVolatile()
	 * @see #unsetIsVolatile()
	 * @see #setIsVolatile(boolean)
	 * @see geneauto.emf.models.common.CommonPackage#getVariable_IsVolatile()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isVolatile();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.common.Variable#isVolatile <em>Is Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Volatile</em>' attribute.
	 * @see #isSetIsVolatile()
	 * @see #unsetIsVolatile()
	 * @see #isVolatile()
	 * @generated
	 */
	void setIsVolatile(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.common.Variable#isVolatile <em>Is Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsVolatile()
	 * @see #isVolatile()
	 * @see #setIsVolatile(boolean)
	 * @generated
	 */
	void unsetIsVolatile();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.common.Variable#isVolatile <em>Is Volatile</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Volatile</em>' attribute is set.
	 * @see #unsetIsVolatile()
	 * @see #isVolatile()
	 * @see #setIsVolatile(boolean)
	 * @generated
	 */
	boolean isSetIsVolatile();

} // Variable
