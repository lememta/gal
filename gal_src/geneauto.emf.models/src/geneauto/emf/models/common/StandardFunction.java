/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Standard Function</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Functions of this class have fixed meanings for the code generator. The semantics is given by the code generator and not the user model and/or user libraries.
 * <!-- end-model-doc -->
 * @see geneauto.emf.models.common.CommonPackage#getStandardFunction()
 * @model
 * @generated
 */
public enum StandardFunction implements Enumerator {
	/**
	 * The '<em><b>NONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONE_VALUE
	 * @generated
	 * @ordered
	 */
	NONE(0, "NONE", "NONE"),

	/**
	 * The '<em><b>ABS FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABS_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ABS_FUN(1, "ABS_FUN", "ABS_FUN"),

	/**
	 * The '<em><b>ACOS FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACOS_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ACOS_FUN(2, "ACOS_FUN", "ACOS_FUN"),

	/**
	 * The '<em><b>ACOSH FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACOSH_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ACOSH_FUN(3, "ACOSH_FUN", "ACOSH_FUN"),

	/**
	 * The '<em><b>ASIN FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASIN_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ASIN_FUN(4, "ASIN_FUN", "ASIN_FUN"),

	/**
	 * The '<em><b>ASINH FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASINH_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ASINH_FUN(5, "ASINH_FUN", "ASINH_FUN"),

	/**
	 * The '<em><b>ATAN FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ATAN_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ATAN_FUN(6, "ATAN_FUN", "ATAN_FUN"),

	/**
	 * The '<em><b>ATAN2 FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ATAN2_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ATAN2_FUN(7, "ATAN2_FUN", "ATAN2_FUN"),

	/**
	 * The '<em><b>ATANH FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ATANH_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ATANH_FUN(8, "ATANH_FUN", "ATANH_FUN"),

	/**
	 * The '<em><b>COS FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COS_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	COS_FUN(9, "COS_FUN", "COS_FUN"),

	/**
	 * The '<em><b>COSH FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COSH_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	COSH_FUN(10, "COSH_FUN", "COSH_FUN"),

	/**
	 * The '<em><b>EXP FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXP_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	EXP_FUN(11, "EXP_FUN", "EXP_FUN"),

	/**
	 * The '<em><b>LOG FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOG_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	LOG_FUN(12, "LOG_FUN", "LOG_FUN"),

	/**
	 * The '<em><b>LOG10 FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOG10_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	LOG10_FUN(13, "LOG10_FUN", "LOG10_FUN"),

	/**
	 * The '<em><b>REM FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REM_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	REM_FUN(14, "REM_FUN", "REM_FUN"),

	/**
	 * The '<em><b>ROUND FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROUND_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	ROUND_FUN(15, "ROUND_FUN", "ROUND_FUN"),

	/**
	 * The '<em><b>SIGN FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIGN_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	SIGN_FUN(16, "SIGN_FUN", "SIGN_FUN"),

	/**
	 * The '<em><b>SIN FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIN_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	SIN_FUN(17, "SIN_FUN", "SIN_FUN"),

	/**
	 * The '<em><b>SINH FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SINH_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	SINH_FUN(18, "SINH_FUN", "SINH_FUN"),

	/**
	 * The '<em><b>SQRT FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SQRT_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	SQRT_FUN(19, "SQRT_FUN", "SQRT_FUN"),

	/**
	 * The '<em><b>TAN FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TAN_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	TAN_FUN(20, "TAN_FUN", "TAN_FUN"),

	/**
	 * The '<em><b>TANH FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TANH_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	TANH_FUN(21, "TANH_FUN", "TANH_FUN"),

	/**
	 * The '<em><b>TRANSPOSE FUN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRANSPOSE_FUN_VALUE
	 * @generated
	 * @ordered
	 */
	TRANSPOSE_FUN(22, "TRANSPOSE_FUN", "TRANSPOSE_FUN");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>NONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is a default value indicating that this field is not set.
	 * TODO Check, if it is possible to do without it.
	 * <!-- end-model-doc -->
	 * @see #NONE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NONE_VALUE = 0;

	/**
	 * The '<em><b>ABS FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The absolute value function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ABS_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ABS_FUN_VALUE = 1;

	/**
	 * The '<em><b>ACOS FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The inverse cosine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ACOS_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ACOS_FUN_VALUE = 2;

	/**
	 * The '<em><b>ACOSH FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The inverse hyperbolic cosine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ACOSH_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ACOSH_FUN_VALUE = 3;

	/**
	 * The '<em><b>ASIN FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The inverse sine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ASIN_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ASIN_FUN_VALUE = 4;

	/**
	 * The '<em><b>ASINH FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The inverse hyperbolic sine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ASINH_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ASINH_FUN_VALUE = 5;

	/**
	 * The '<em><b>ATAN FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The inverse tangent function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ATAN_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ATAN_FUN_VALUE = 6;

	/**
	 * The '<em><b>ATAN2 FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The Four-quadrant inverse tangent function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ATAN2_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ATAN2_FUN_VALUE = 7;

	/**
	 * The '<em><b>ATANH FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The inverse hyperbolic tangent function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ATANH_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ATANH_FUN_VALUE = 8;

	/**
	 * The '<em><b>COS FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The cosine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #COS_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int COS_FUN_VALUE = 9;

	/**
	 * The '<em><b>COSH FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The hyperbolic cosine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #COSH_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int COSH_FUN_VALUE = 10;

	/**
	 * The '<em><b>EXP FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The exponent function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #EXP_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXP_FUN_VALUE = 11;

	/**
	 * The '<em><b>LOG FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The natural logarithm function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #LOG_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LOG_FUN_VALUE = 12;

	/**
	 * The '<em><b>LOG10 FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The common (base 10) logarithm function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #LOG10_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LOG10_FUN_VALUE = 13;

	/**
	 * The '<em><b>REM FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The reminder function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #REM_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REM_FUN_VALUE = 14;

	/**
	 * The '<em><b>ROUND FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The rounding function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #ROUND_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ROUND_FUN_VALUE = 15;

	/**
	 * The '<em><b>SIGN FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The signum function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #SIGN_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIGN_FUN_VALUE = 16;

	/**
	 * The '<em><b>SIN FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The sine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #SIN_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIN_FUN_VALUE = 17;

	/**
	 * The '<em><b>SINH FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The hyperbolic sine function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #SINH_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SINH_FUN_VALUE = 18;

	/**
	 * The '<em><b>SQRT FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The square root function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #SQRT_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SQRT_FUN_VALUE = 19;

	/**
	 * The '<em><b>TAN FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The tangent function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #TAN_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TAN_FUN_VALUE = 20;

	/**
	 * The '<em><b>TANH FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The hyperbolic tangent function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #TANH_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TANH_FUN_VALUE = 21;

	/**
	 * The '<em><b>TRANSPOSE FUN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The transpose function. Specification follows ... TODO
	 * <!-- end-model-doc -->
	 * @see #TRANSPOSE_FUN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRANSPOSE_FUN_VALUE = 22;

	/**
	 * An array of all the '<em><b>Standard Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final StandardFunction[] VALUES_ARRAY =
		new StandardFunction[] {
			NONE,
			ABS_FUN,
			ACOS_FUN,
			ACOSH_FUN,
			ASIN_FUN,
			ASINH_FUN,
			ATAN_FUN,
			ATAN2_FUN,
			ATANH_FUN,
			COS_FUN,
			COSH_FUN,
			EXP_FUN,
			LOG_FUN,
			LOG10_FUN,
			REM_FUN,
			ROUND_FUN,
			SIGN_FUN,
			SIN_FUN,
			SINH_FUN,
			SQRT_FUN,
			TAN_FUN,
			TANH_FUN,
			TRANSPOSE_FUN,
		};

	/**
	 * A public read-only list of all the '<em><b>Standard Function</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<StandardFunction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Standard Function</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StandardFunction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StandardFunction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Standard Function</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StandardFunction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StandardFunction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Standard Function</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StandardFunction get(int value) {
		switch (value) {
			case NONE_VALUE: return NONE;
			case ABS_FUN_VALUE: return ABS_FUN;
			case ACOS_FUN_VALUE: return ACOS_FUN;
			case ACOSH_FUN_VALUE: return ACOSH_FUN;
			case ASIN_FUN_VALUE: return ASIN_FUN;
			case ASINH_FUN_VALUE: return ASINH_FUN;
			case ATAN_FUN_VALUE: return ATAN_FUN;
			case ATAN2_FUN_VALUE: return ATAN2_FUN;
			case ATANH_FUN_VALUE: return ATANH_FUN;
			case COS_FUN_VALUE: return COS_FUN;
			case COSH_FUN_VALUE: return COSH_FUN;
			case EXP_FUN_VALUE: return EXP_FUN;
			case LOG_FUN_VALUE: return LOG_FUN;
			case LOG10_FUN_VALUE: return LOG10_FUN;
			case REM_FUN_VALUE: return REM_FUN;
			case ROUND_FUN_VALUE: return ROUND_FUN;
			case SIGN_FUN_VALUE: return SIGN_FUN;
			case SIN_FUN_VALUE: return SIN_FUN;
			case SINH_FUN_VALUE: return SINH_FUN;
			case SQRT_FUN_VALUE: return SQRT_FUN;
			case TAN_FUN_VALUE: return TAN_FUN;
			case TANH_FUN_VALUE: return TANH_FUN;
			case TRANSPOSE_FUN_VALUE: return TRANSPOSE_FUN;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private StandardFunction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //StandardFunction
