/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gadatatypes;

import geneauto.emf.models.gacodemodel.expression.Expression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TArray</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Array data type
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gadatatypes.TArray#getBaseType <em>Base Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gadatatypes.TArray#getDimensions <em>Dimensions</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gadatatypes.GadatatypesPackage#getTArray()
 * @model
 * @generated
 */
public interface TArray extends GADataType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Base Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Type</em>' containment reference.
	 * @see #isSetBaseType()
	 * @see #unsetBaseType()
	 * @see #setBaseType(TPrimitive)
	 * @see geneauto.emf.models.gadatatypes.GadatatypesPackage#getTArray_BaseType()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	TPrimitive getBaseType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gadatatypes.TArray#getBaseType <em>Base Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Type</em>' containment reference.
	 * @see #isSetBaseType()
	 * @see #unsetBaseType()
	 * @see #getBaseType()
	 * @generated
	 */
	void setBaseType(TPrimitive value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gadatatypes.TArray#getBaseType <em>Base Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBaseType()
	 * @see #getBaseType()
	 * @see #setBaseType(TPrimitive)
	 * @generated
	 */
	void unsetBaseType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gadatatypes.TArray#getBaseType <em>Base Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Base Type</em>' containment reference is set.
	 * @see #unsetBaseType()
	 * @see #getBaseType()
	 * @see #setBaseType(TPrimitive)
	 * @generated
	 */
	boolean isSetBaseType();

	/**
	 * Returns the value of the '<em><b>Dimensions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.expression.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimensions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimensions</em>' containment reference list.
	 * @see #isSetDimensions()
	 * @see #unsetDimensions()
	 * @see geneauto.emf.models.gadatatypes.GadatatypesPackage#getTArray_Dimensions()
	 * @model containment="true" unsettable="true" required="true"
	 * @generated
	 */
	EList<Expression> getDimensions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gadatatypes.TArray#getDimensions <em>Dimensions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDimensions()
	 * @see #getDimensions()
	 * @generated
	 */
	void unsetDimensions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gadatatypes.TArray#getDimensions <em>Dimensions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Dimensions</em>' containment reference list is set.
	 * @see #unsetDimensions()
	 * @see #getDimensions()
	 * @generated
	 */
	boolean isSetDimensions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	TArray As_Column_Matrix();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	TArray As_Row_Matrix();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	EList<Integer> Get_dimensionValues();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String Get_infoString();

} // TArray
