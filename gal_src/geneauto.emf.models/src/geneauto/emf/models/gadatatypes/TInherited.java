/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gadatatypes;

import geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TInherited</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Inherited type - the element inherits its type by some fixed rule.  In Gene-Auto an approach has been taken to distinguish an inherited type from an unknown type. In both cases the proper type of the element is unknown, but in the first case there is a specific rule given, how to deduce this type, while in the second case this information is missing or implicit.  The type of the object must be resolved during the code generation process.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gadatatypes.TInherited#getRule <em>Rule</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gadatatypes.GadatatypesPackage#getTInherited()
 * @model
 * @generated
 */
public interface TInherited extends GADataType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Rule</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule
	 * @see #isSetRule()
	 * @see #unsetRule()
	 * @see #setRule(TypeInheritanceRule)
	 * @see geneauto.emf.models.gadatatypes.GadatatypesPackage#getTInherited_Rule()
	 * @model unsettable="true"
	 * @generated
	 */
	TypeInheritanceRule getRule();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gadatatypes.TInherited#getRule <em>Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule
	 * @see #isSetRule()
	 * @see #unsetRule()
	 * @see #getRule()
	 * @generated
	 */
	void setRule(TypeInheritanceRule value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gadatatypes.TInherited#getRule <em>Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRule()
	 * @see #getRule()
	 * @see #setRule(TypeInheritanceRule)
	 * @generated
	 */
	void unsetRule();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gadatatypes.TInherited#getRule <em>Rule</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Rule</em>' attribute is set.
	 * @see #unsetRule()
	 * @see #getRule()
	 * @see #setRule(TypeInheritanceRule)
	 * @generated
	 */
	boolean isSetRule();

} // TInherited
