/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gadatatypes.impl;

import geneauto.emf.models.gadatatypes.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GadatatypesFactoryImpl extends EFactoryImpl implements GadatatypesFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GadatatypesFactory init() {
		try {
			GadatatypesFactory theGadatatypesFactory = (GadatatypesFactory)EPackage.Registry.INSTANCE.getEFactory("http:///geneauto/emf/models/gadatatypes.ecore"); 
			if (theGadatatypesFactory != null) {
				return theGadatatypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GadatatypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GadatatypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case GadatatypesPackage.TEVENT: return createTEvent();
			case GadatatypesPackage.TARRAY: return createTArray();
			case GadatatypesPackage.TBOOLEAN: return createTBoolean();
			case GadatatypesPackage.TCOMPLEX_DOUBLE: return createTComplexDouble();
			case GadatatypesPackage.TCOMPLEX_FIXED_POINT: return createTComplexFixedPoint();
			case GadatatypesPackage.TCOMPLEX_INTEGER: return createTComplexInteger();
			case GadatatypesPackage.TCOMPLEX_SINGLE: return createTComplexSingle();
			case GadatatypesPackage.TINHERITED: return createTInherited();
			case GadatatypesPackage.TPOINTER: return createTPointer();
			case GadatatypesPackage.TREAL_DOUBLE: return createTRealDouble();
			case GadatatypesPackage.TREAL_FIXED_POINT: return createTRealFixedPoint();
			case GadatatypesPackage.TREAL_INTEGER: return createTRealInteger();
			case GadatatypesPackage.TREAL_SINGLE: return createTRealSingle();
			case GadatatypesPackage.TSTRING: return createTString();
			case GadatatypesPackage.TCUSTOM: return createTCustom();
			case GadatatypesPackage.TVOID: return createTVoid();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TEvent createTEvent() {
		TEventImpl tEvent = new TEventImpl();
		return tEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TArray createTArray() {
		TArrayImpl tArray = new TArrayImpl();
		return tArray;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TBoolean createTBoolean() {
		TBooleanImpl tBoolean = new TBooleanImpl();
		return tBoolean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TComplexDouble createTComplexDouble() {
		TComplexDoubleImpl tComplexDouble = new TComplexDoubleImpl();
		return tComplexDouble;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TComplexFixedPoint createTComplexFixedPoint() {
		TComplexFixedPointImpl tComplexFixedPoint = new TComplexFixedPointImpl();
		return tComplexFixedPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TComplexInteger createTComplexInteger() {
		TComplexIntegerImpl tComplexInteger = new TComplexIntegerImpl();
		return tComplexInteger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TComplexSingle createTComplexSingle() {
		TComplexSingleImpl tComplexSingle = new TComplexSingleImpl();
		return tComplexSingle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TInherited createTInherited() {
		TInheritedImpl tInherited = new TInheritedImpl();
		return tInherited;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TPointer createTPointer() {
		TPointerImpl tPointer = new TPointerImpl();
		return tPointer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TRealDouble createTRealDouble() {
		TRealDoubleImpl tRealDouble = new TRealDoubleImpl();
		return tRealDouble;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TRealFixedPoint createTRealFixedPoint() {
		TRealFixedPointImpl tRealFixedPoint = new TRealFixedPointImpl();
		return tRealFixedPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TRealInteger createTRealInteger() {
		TRealIntegerImpl tRealInteger = new TRealIntegerImpl();
		return tRealInteger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TRealSingle createTRealSingle() {
		TRealSingleImpl tRealSingle = new TRealSingleImpl();
		return tRealSingle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TString createTString() {
		TStringImpl tString = new TStringImpl();
		return tString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TCustom createTCustom() {
		TCustomImpl tCustom = new TCustomImpl();
		return tCustom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TVoid createTVoid() {
		TVoidImpl tVoid = new TVoidImpl();
		return tVoid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GadatatypesPackage getGadatatypesPackage() {
		return (GadatatypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GadatatypesPackage getPackage() {
		return GadatatypesPackage.eINSTANCE;
	}

} //GadatatypesFactoryImpl
