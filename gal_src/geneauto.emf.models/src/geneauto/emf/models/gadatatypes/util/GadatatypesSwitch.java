/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gadatatypes.util;

import geneauto.emf.models.gadatatypes.*;

import geneauto.emf.models.genericmodel.GAModelElement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gadatatypes.GadatatypesPackage
 * @generated
 */
public class GadatatypesSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GadatatypesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GadatatypesSwitch() {
		if (modelPackage == null) {
			modelPackage = GadatatypesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case GadatatypesPackage.GA_DATA_TYPE: {
				GADataType gaDataType = (GADataType)theEObject;
				T result = caseGADataType(gaDataType);
				if (result == null) result = caseGAModelElement(gaDataType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TBOTTOM: {
				TBottom tBottom = (TBottom)theEObject;
				T result = caseTBottom(tBottom);
				if (result == null) result = caseGADataType(tBottom);
				if (result == null) result = caseGAModelElement(tBottom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TCOMPLEX_FLOATING_POINT: {
				TComplexFloatingPoint tComplexFloatingPoint = (TComplexFloatingPoint)theEObject;
				T result = caseTComplexFloatingPoint(tComplexFloatingPoint);
				if (result == null) result = caseTComplexNumeric(tComplexFloatingPoint);
				if (result == null) result = caseTNumeric(tComplexFloatingPoint);
				if (result == null) result = caseTPrimitive(tComplexFloatingPoint);
				if (result == null) result = caseGADataType(tComplexFloatingPoint);
				if (result == null) result = caseGAModelElement(tComplexFloatingPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TCOMPLEX_NUMERIC: {
				TComplexNumeric tComplexNumeric = (TComplexNumeric)theEObject;
				T result = caseTComplexNumeric(tComplexNumeric);
				if (result == null) result = caseTNumeric(tComplexNumeric);
				if (result == null) result = caseTPrimitive(tComplexNumeric);
				if (result == null) result = caseGADataType(tComplexNumeric);
				if (result == null) result = caseGAModelElement(tComplexNumeric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TNUMERIC: {
				TNumeric tNumeric = (TNumeric)theEObject;
				T result = caseTNumeric(tNumeric);
				if (result == null) result = caseTPrimitive(tNumeric);
				if (result == null) result = caseGADataType(tNumeric);
				if (result == null) result = caseGAModelElement(tNumeric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TPRIMITIVE: {
				TPrimitive tPrimitive = (TPrimitive)theEObject;
				T result = caseTPrimitive(tPrimitive);
				if (result == null) result = caseGADataType(tPrimitive);
				if (result == null) result = caseGAModelElement(tPrimitive);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TEVENT: {
				TEvent tEvent = (TEvent)theEObject;
				T result = caseTEvent(tEvent);
				if (result == null) result = caseTPrimitive(tEvent);
				if (result == null) result = caseGADataType(tEvent);
				if (result == null) result = caseGAModelElement(tEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TREAL_FLOATING_POINT: {
				TRealFloatingPoint tRealFloatingPoint = (TRealFloatingPoint)theEObject;
				T result = caseTRealFloatingPoint(tRealFloatingPoint);
				if (result == null) result = caseTRealNumeric(tRealFloatingPoint);
				if (result == null) result = caseTNumeric(tRealFloatingPoint);
				if (result == null) result = caseTPrimitive(tRealFloatingPoint);
				if (result == null) result = caseGADataType(tRealFloatingPoint);
				if (result == null) result = caseGAModelElement(tRealFloatingPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TREAL_NUMERIC: {
				TRealNumeric tRealNumeric = (TRealNumeric)theEObject;
				T result = caseTRealNumeric(tRealNumeric);
				if (result == null) result = caseTNumeric(tRealNumeric);
				if (result == null) result = caseTPrimitive(tRealNumeric);
				if (result == null) result = caseGADataType(tRealNumeric);
				if (result == null) result = caseGAModelElement(tRealNumeric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TARRAY: {
				TArray tArray = (TArray)theEObject;
				T result = caseTArray(tArray);
				if (result == null) result = caseGADataType(tArray);
				if (result == null) result = caseGAModelElement(tArray);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TBOOLEAN: {
				TBoolean tBoolean = (TBoolean)theEObject;
				T result = caseTBoolean(tBoolean);
				if (result == null) result = caseTPrimitive(tBoolean);
				if (result == null) result = caseGADataType(tBoolean);
				if (result == null) result = caseGAModelElement(tBoolean);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TCOMPLEX_DOUBLE: {
				TComplexDouble tComplexDouble = (TComplexDouble)theEObject;
				T result = caseTComplexDouble(tComplexDouble);
				if (result == null) result = caseTComplexFloatingPoint(tComplexDouble);
				if (result == null) result = caseTComplexNumeric(tComplexDouble);
				if (result == null) result = caseTNumeric(tComplexDouble);
				if (result == null) result = caseTPrimitive(tComplexDouble);
				if (result == null) result = caseGADataType(tComplexDouble);
				if (result == null) result = caseGAModelElement(tComplexDouble);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TCOMPLEX_FIXED_POINT: {
				TComplexFixedPoint tComplexFixedPoint = (TComplexFixedPoint)theEObject;
				T result = caseTComplexFixedPoint(tComplexFixedPoint);
				if (result == null) result = caseTComplexNumeric(tComplexFixedPoint);
				if (result == null) result = caseTNumeric(tComplexFixedPoint);
				if (result == null) result = caseTPrimitive(tComplexFixedPoint);
				if (result == null) result = caseGADataType(tComplexFixedPoint);
				if (result == null) result = caseGAModelElement(tComplexFixedPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TCOMPLEX_INTEGER: {
				TComplexInteger tComplexInteger = (TComplexInteger)theEObject;
				T result = caseTComplexInteger(tComplexInteger);
				if (result == null) result = caseTComplexNumeric(tComplexInteger);
				if (result == null) result = caseTNumeric(tComplexInteger);
				if (result == null) result = caseTPrimitive(tComplexInteger);
				if (result == null) result = caseGADataType(tComplexInteger);
				if (result == null) result = caseGAModelElement(tComplexInteger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TCOMPLEX_SINGLE: {
				TComplexSingle tComplexSingle = (TComplexSingle)theEObject;
				T result = caseTComplexSingle(tComplexSingle);
				if (result == null) result = caseTComplexFloatingPoint(tComplexSingle);
				if (result == null) result = caseTComplexNumeric(tComplexSingle);
				if (result == null) result = caseTNumeric(tComplexSingle);
				if (result == null) result = caseTPrimitive(tComplexSingle);
				if (result == null) result = caseGADataType(tComplexSingle);
				if (result == null) result = caseGAModelElement(tComplexSingle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TINHERITED: {
				TInherited tInherited = (TInherited)theEObject;
				T result = caseTInherited(tInherited);
				if (result == null) result = caseGADataType(tInherited);
				if (result == null) result = caseGAModelElement(tInherited);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TPOINTER: {
				TPointer tPointer = (TPointer)theEObject;
				T result = caseTPointer(tPointer);
				if (result == null) result = caseGADataType(tPointer);
				if (result == null) result = caseGAModelElement(tPointer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TREAL_DOUBLE: {
				TRealDouble tRealDouble = (TRealDouble)theEObject;
				T result = caseTRealDouble(tRealDouble);
				if (result == null) result = caseTRealFloatingPoint(tRealDouble);
				if (result == null) result = caseTRealNumeric(tRealDouble);
				if (result == null) result = caseTNumeric(tRealDouble);
				if (result == null) result = caseTPrimitive(tRealDouble);
				if (result == null) result = caseGADataType(tRealDouble);
				if (result == null) result = caseGAModelElement(tRealDouble);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TREAL_FIXED_POINT: {
				TRealFixedPoint tRealFixedPoint = (TRealFixedPoint)theEObject;
				T result = caseTRealFixedPoint(tRealFixedPoint);
				if (result == null) result = caseTRealNumeric(tRealFixedPoint);
				if (result == null) result = caseTNumeric(tRealFixedPoint);
				if (result == null) result = caseTPrimitive(tRealFixedPoint);
				if (result == null) result = caseGADataType(tRealFixedPoint);
				if (result == null) result = caseGAModelElement(tRealFixedPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TREAL_INTEGER: {
				TRealInteger tRealInteger = (TRealInteger)theEObject;
				T result = caseTRealInteger(tRealInteger);
				if (result == null) result = caseTRealNumeric(tRealInteger);
				if (result == null) result = caseTNumeric(tRealInteger);
				if (result == null) result = caseTPrimitive(tRealInteger);
				if (result == null) result = caseGADataType(tRealInteger);
				if (result == null) result = caseGAModelElement(tRealInteger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TREAL_SINGLE: {
				TRealSingle tRealSingle = (TRealSingle)theEObject;
				T result = caseTRealSingle(tRealSingle);
				if (result == null) result = caseTRealFloatingPoint(tRealSingle);
				if (result == null) result = caseTRealNumeric(tRealSingle);
				if (result == null) result = caseTNumeric(tRealSingle);
				if (result == null) result = caseTPrimitive(tRealSingle);
				if (result == null) result = caseGADataType(tRealSingle);
				if (result == null) result = caseGAModelElement(tRealSingle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TSTRING: {
				TString tString = (TString)theEObject;
				T result = caseTString(tString);
				if (result == null) result = caseTPrimitive(tString);
				if (result == null) result = caseGADataType(tString);
				if (result == null) result = caseGAModelElement(tString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TCUSTOM: {
				TCustom tCustom = (TCustom)theEObject;
				T result = caseTCustom(tCustom);
				if (result == null) result = caseTPrimitive(tCustom);
				if (result == null) result = caseGADataType(tCustom);
				if (result == null) result = caseGAModelElement(tCustom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GadatatypesPackage.TVOID: {
				TVoid tVoid = (TVoid)theEObject;
				T result = caseTVoid(tVoid);
				if (result == null) result = caseTPrimitive(tVoid);
				if (result == null) result = caseGADataType(tVoid);
				if (result == null) result = caseGAModelElement(tVoid);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGADataType(GADataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TBottom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TBottom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTBottom(TBottom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TComplex Floating Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TComplex Floating Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTComplexFloatingPoint(TComplexFloatingPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TComplex Numeric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TComplex Numeric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTComplexNumeric(TComplexNumeric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TNumeric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TNumeric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTNumeric(TNumeric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TPrimitive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TPrimitive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTPrimitive(TPrimitive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TEvent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TEvent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTEvent(TEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TReal Floating Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TReal Floating Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTRealFloatingPoint(TRealFloatingPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TReal Numeric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TReal Numeric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTRealNumeric(TRealNumeric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TArray</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TArray</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTArray(TArray object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TBoolean</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TBoolean</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTBoolean(TBoolean object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TComplex Double</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TComplex Double</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTComplexDouble(TComplexDouble object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TComplex Fixed Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TComplex Fixed Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTComplexFixedPoint(TComplexFixedPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TComplex Integer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TComplex Integer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTComplexInteger(TComplexInteger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TComplex Single</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TComplex Single</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTComplexSingle(TComplexSingle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TInherited</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TInherited</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTInherited(TInherited object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TPointer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TPointer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTPointer(TPointer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TReal Double</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TReal Double</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTRealDouble(TRealDouble object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TReal Fixed Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TReal Fixed Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTRealFixedPoint(TRealFixedPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TReal Integer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TReal Integer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTRealInteger(TRealInteger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TReal Single</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TReal Single</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTRealSingle(TRealSingle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TString</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TString</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTString(TString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TCustom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TCustom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTCustom(TCustom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TVoid</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TVoid</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTVoid(TVoid object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGAModelElement(GAModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //GadatatypesSwitch
