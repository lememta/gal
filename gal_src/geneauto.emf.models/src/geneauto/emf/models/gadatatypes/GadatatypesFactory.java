/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gadatatypes;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gadatatypes.GadatatypesPackage
 * @generated
 */
public interface GadatatypesFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GadatatypesFactory eINSTANCE = geneauto.emf.models.gadatatypes.impl.GadatatypesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TEvent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TEvent</em>'.
	 * @generated
	 */
	TEvent createTEvent();

	/**
	 * Returns a new object of class '<em>TArray</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TArray</em>'.
	 * @generated
	 */
	TArray createTArray();

	/**
	 * Returns a new object of class '<em>TBoolean</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TBoolean</em>'.
	 * @generated
	 */
	TBoolean createTBoolean();

	/**
	 * Returns a new object of class '<em>TComplex Double</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TComplex Double</em>'.
	 * @generated
	 */
	TComplexDouble createTComplexDouble();

	/**
	 * Returns a new object of class '<em>TComplex Fixed Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TComplex Fixed Point</em>'.
	 * @generated
	 */
	TComplexFixedPoint createTComplexFixedPoint();

	/**
	 * Returns a new object of class '<em>TComplex Integer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TComplex Integer</em>'.
	 * @generated
	 */
	TComplexInteger createTComplexInteger();

	/**
	 * Returns a new object of class '<em>TComplex Single</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TComplex Single</em>'.
	 * @generated
	 */
	TComplexSingle createTComplexSingle();

	/**
	 * Returns a new object of class '<em>TInherited</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TInherited</em>'.
	 * @generated
	 */
	TInherited createTInherited();

	/**
	 * Returns a new object of class '<em>TPointer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TPointer</em>'.
	 * @generated
	 */
	TPointer createTPointer();

	/**
	 * Returns a new object of class '<em>TReal Double</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TReal Double</em>'.
	 * @generated
	 */
	TRealDouble createTRealDouble();

	/**
	 * Returns a new object of class '<em>TReal Fixed Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TReal Fixed Point</em>'.
	 * @generated
	 */
	TRealFixedPoint createTRealFixedPoint();

	/**
	 * Returns a new object of class '<em>TReal Integer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TReal Integer</em>'.
	 * @generated
	 */
	TRealInteger createTRealInteger();

	/**
	 * Returns a new object of class '<em>TReal Single</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TReal Single</em>'.
	 * @generated
	 */
	TRealSingle createTRealSingle();

	/**
	 * Returns a new object of class '<em>TString</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TString</em>'.
	 * @generated
	 */
	TString createTString();

	/**
	 * Returns a new object of class '<em>TCustom</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TCustom</em>'.
	 * @generated
	 */
	TCustom createTCustom();

	/**
	 * Returns a new object of class '<em>TVoid</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TVoid</em>'.
	 * @generated
	 */
	TVoid createTVoid();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GadatatypesPackage getGadatatypesPackage();

} //GadatatypesFactory
