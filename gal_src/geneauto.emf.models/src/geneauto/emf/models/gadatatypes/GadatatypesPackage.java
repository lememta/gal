/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gadatatypes;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gadatatypes.GadatatypesFactory
 * @model kind="package"
 * @generated
 */
public interface GadatatypesPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gadatatypes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gadatatypes.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gadatatypes";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GadatatypesPackage eINSTANCE = geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.GADataTypeImpl <em>GA Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.GADataTypeImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getGADataType()
	 * @generated
	 */
	int GA_DATA_TYPE = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE__MODULE = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>GA Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_DATA_TYPE_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TBottomImpl <em>TBottom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TBottomImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTBottom()
	 * @generated
	 */
	int TBOTTOM = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__EXTERNAL_ID = GA_DATA_TYPE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__ID = GA_DATA_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__IS_FIXED_NAME = GA_DATA_TYPE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__NAME = GA_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__ORIGINAL_FULL_NAME = GA_DATA_TYPE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__ORIGINAL_NAME = GA_DATA_TYPE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__PARENT = GA_DATA_TYPE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__MODEL = GA_DATA_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__ANNOTATIONS = GA_DATA_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM__MODULE = GA_DATA_TYPE__MODULE;

	/**
	 * The number of structural features of the '<em>TBottom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOTTOM_FEATURE_COUNT = GA_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TPrimitiveImpl <em>TPrimitive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TPrimitiveImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTPrimitive()
	 * @generated
	 */
	int TPRIMITIVE = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__EXTERNAL_ID = GA_DATA_TYPE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__ID = GA_DATA_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__IS_FIXED_NAME = GA_DATA_TYPE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__NAME = GA_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__ORIGINAL_FULL_NAME = GA_DATA_TYPE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__ORIGINAL_NAME = GA_DATA_TYPE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__PARENT = GA_DATA_TYPE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__MODEL = GA_DATA_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__ANNOTATIONS = GA_DATA_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE__MODULE = GA_DATA_TYPE__MODULE;

	/**
	 * The number of structural features of the '<em>TPrimitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPRIMITIVE_FEATURE_COUNT = GA_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TNumericImpl <em>TNumeric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TNumericImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTNumeric()
	 * @generated
	 */
	int TNUMERIC = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__EXTERNAL_ID = TPRIMITIVE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__ID = TPRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__IS_FIXED_NAME = TPRIMITIVE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__NAME = TPRIMITIVE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__ORIGINAL_FULL_NAME = TPRIMITIVE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__ORIGINAL_NAME = TPRIMITIVE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__PARENT = TPRIMITIVE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__MODEL = TPRIMITIVE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__ANNOTATIONS = TPRIMITIVE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC__MODULE = TPRIMITIVE__MODULE;

	/**
	 * The number of structural features of the '<em>TNumeric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TNUMERIC_FEATURE_COUNT = TPRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexNumericImpl <em>TComplex Numeric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TComplexNumericImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexNumeric()
	 * @generated
	 */
	int TCOMPLEX_NUMERIC = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__EXTERNAL_ID = TNUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__ID = TNUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__IS_FIXED_NAME = TNUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__NAME = TNUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__ORIGINAL_FULL_NAME = TNUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__ORIGINAL_NAME = TNUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__PARENT = TNUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__MODEL = TNUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__ANNOTATIONS = TNUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC__MODULE = TNUMERIC__MODULE;

	/**
	 * The number of structural features of the '<em>TComplex Numeric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_NUMERIC_FEATURE_COUNT = TNUMERIC_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexFloatingPointImpl <em>TComplex Floating Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TComplexFloatingPointImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexFloatingPoint()
	 * @generated
	 */
	int TCOMPLEX_FLOATING_POINT = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__EXTERNAL_ID = TCOMPLEX_NUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__ID = TCOMPLEX_NUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__IS_FIXED_NAME = TCOMPLEX_NUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__NAME = TCOMPLEX_NUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__ORIGINAL_FULL_NAME = TCOMPLEX_NUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__ORIGINAL_NAME = TCOMPLEX_NUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__PARENT = TCOMPLEX_NUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__MODEL = TCOMPLEX_NUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__ANNOTATIONS = TCOMPLEX_NUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT__MODULE = TCOMPLEX_NUMERIC__MODULE;

	/**
	 * The number of structural features of the '<em>TComplex Floating Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FLOATING_POINT_FEATURE_COUNT = TCOMPLEX_NUMERIC_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TEventImpl <em>TEvent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TEventImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTEvent()
	 * @generated
	 */
	int TEVENT = 6;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__EXTERNAL_ID = TPRIMITIVE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__ID = TPRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__IS_FIXED_NAME = TPRIMITIVE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__NAME = TPRIMITIVE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__ORIGINAL_FULL_NAME = TPRIMITIVE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__ORIGINAL_NAME = TPRIMITIVE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__PARENT = TPRIMITIVE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__MODEL = TPRIMITIVE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__ANNOTATIONS = TPRIMITIVE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT__MODULE = TPRIMITIVE__MODULE;

	/**
	 * The number of structural features of the '<em>TEvent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEVENT_FEATURE_COUNT = TPRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TRealNumericImpl <em>TReal Numeric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TRealNumericImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealNumeric()
	 * @generated
	 */
	int TREAL_NUMERIC = 8;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__EXTERNAL_ID = TNUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__ID = TNUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__IS_FIXED_NAME = TNUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__NAME = TNUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__ORIGINAL_FULL_NAME = TNUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__ORIGINAL_NAME = TNUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__PARENT = TNUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__MODEL = TNUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__ANNOTATIONS = TNUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC__MODULE = TNUMERIC__MODULE;

	/**
	 * The number of structural features of the '<em>TReal Numeric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_NUMERIC_FEATURE_COUNT = TNUMERIC_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TRealFloatingPointImpl <em>TReal Floating Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TRealFloatingPointImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealFloatingPoint()
	 * @generated
	 */
	int TREAL_FLOATING_POINT = 7;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__EXTERNAL_ID = TREAL_NUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__ID = TREAL_NUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__IS_FIXED_NAME = TREAL_NUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__NAME = TREAL_NUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__ORIGINAL_FULL_NAME = TREAL_NUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__ORIGINAL_NAME = TREAL_NUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__PARENT = TREAL_NUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__MODEL = TREAL_NUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__ANNOTATIONS = TREAL_NUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT__MODULE = TREAL_NUMERIC__MODULE;

	/**
	 * The number of structural features of the '<em>TReal Floating Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FLOATING_POINT_FEATURE_COUNT = TREAL_NUMERIC_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TArrayImpl <em>TArray</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TArrayImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTArray()
	 * @generated
	 */
	int TARRAY = 9;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__EXTERNAL_ID = GA_DATA_TYPE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__ID = GA_DATA_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__IS_FIXED_NAME = GA_DATA_TYPE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__NAME = GA_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__ORIGINAL_FULL_NAME = GA_DATA_TYPE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__ORIGINAL_NAME = GA_DATA_TYPE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__PARENT = GA_DATA_TYPE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__MODEL = GA_DATA_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__ANNOTATIONS = GA_DATA_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__MODULE = GA_DATA_TYPE__MODULE;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__BASE_TYPE = GA_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dimensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY__DIMENSIONS = GA_DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>TArray</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARRAY_FEATURE_COUNT = GA_DATA_TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TBooleanImpl <em>TBoolean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TBooleanImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTBoolean()
	 * @generated
	 */
	int TBOOLEAN = 10;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__EXTERNAL_ID = TPRIMITIVE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__ID = TPRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__IS_FIXED_NAME = TPRIMITIVE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__NAME = TPRIMITIVE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__ORIGINAL_FULL_NAME = TPRIMITIVE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__ORIGINAL_NAME = TPRIMITIVE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__PARENT = TPRIMITIVE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__MODEL = TPRIMITIVE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__ANNOTATIONS = TPRIMITIVE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN__MODULE = TPRIMITIVE__MODULE;

	/**
	 * The number of structural features of the '<em>TBoolean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TBOOLEAN_FEATURE_COUNT = TPRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexDoubleImpl <em>TComplex Double</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TComplexDoubleImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexDouble()
	 * @generated
	 */
	int TCOMPLEX_DOUBLE = 11;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__EXTERNAL_ID = TCOMPLEX_FLOATING_POINT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__ID = TCOMPLEX_FLOATING_POINT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__IS_FIXED_NAME = TCOMPLEX_FLOATING_POINT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__NAME = TCOMPLEX_FLOATING_POINT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__ORIGINAL_FULL_NAME = TCOMPLEX_FLOATING_POINT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__ORIGINAL_NAME = TCOMPLEX_FLOATING_POINT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__PARENT = TCOMPLEX_FLOATING_POINT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__MODEL = TCOMPLEX_FLOATING_POINT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__ANNOTATIONS = TCOMPLEX_FLOATING_POINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE__MODULE = TCOMPLEX_FLOATING_POINT__MODULE;

	/**
	 * The number of structural features of the '<em>TComplex Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_DOUBLE_FEATURE_COUNT = TCOMPLEX_FLOATING_POINT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexFixedPointImpl <em>TComplex Fixed Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TComplexFixedPointImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexFixedPoint()
	 * @generated
	 */
	int TCOMPLEX_FIXED_POINT = 12;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__EXTERNAL_ID = TCOMPLEX_NUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__ID = TCOMPLEX_NUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__IS_FIXED_NAME = TCOMPLEX_NUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__NAME = TCOMPLEX_NUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__ORIGINAL_FULL_NAME = TCOMPLEX_NUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__ORIGINAL_NAME = TCOMPLEX_NUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__PARENT = TCOMPLEX_NUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__MODEL = TCOMPLEX_NUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__ANNOTATIONS = TCOMPLEX_NUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__MODULE = TCOMPLEX_NUMERIC__MODULE;

	/**
	 * The feature id for the '<em><b>Gain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__GAIN = TCOMPLEX_NUMERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__OFFSET = TCOMPLEX_NUMERIC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT__SIZE = TCOMPLEX_NUMERIC_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>TComplex Fixed Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_FIXED_POINT_FEATURE_COUNT = TCOMPLEX_NUMERIC_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexIntegerImpl <em>TComplex Integer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TComplexIntegerImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexInteger()
	 * @generated
	 */
	int TCOMPLEX_INTEGER = 13;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__EXTERNAL_ID = TCOMPLEX_NUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__ID = TCOMPLEX_NUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__IS_FIXED_NAME = TCOMPLEX_NUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__NAME = TCOMPLEX_NUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__ORIGINAL_FULL_NAME = TCOMPLEX_NUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__ORIGINAL_NAME = TCOMPLEX_NUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__PARENT = TCOMPLEX_NUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__MODEL = TCOMPLEX_NUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__ANNOTATIONS = TCOMPLEX_NUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__MODULE = TCOMPLEX_NUMERIC__MODULE;

	/**
	 * The feature id for the '<em><b>NBits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__NBITS = TCOMPLEX_NUMERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER__SIGNED = TCOMPLEX_NUMERIC_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>TComplex Integer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_INTEGER_FEATURE_COUNT = TCOMPLEX_NUMERIC_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexSingleImpl <em>TComplex Single</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TComplexSingleImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexSingle()
	 * @generated
	 */
	int TCOMPLEX_SINGLE = 14;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__EXTERNAL_ID = TCOMPLEX_FLOATING_POINT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__ID = TCOMPLEX_FLOATING_POINT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__IS_FIXED_NAME = TCOMPLEX_FLOATING_POINT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__NAME = TCOMPLEX_FLOATING_POINT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__ORIGINAL_FULL_NAME = TCOMPLEX_FLOATING_POINT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__ORIGINAL_NAME = TCOMPLEX_FLOATING_POINT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__PARENT = TCOMPLEX_FLOATING_POINT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__MODEL = TCOMPLEX_FLOATING_POINT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__ANNOTATIONS = TCOMPLEX_FLOATING_POINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE__MODULE = TCOMPLEX_FLOATING_POINT__MODULE;

	/**
	 * The number of structural features of the '<em>TComplex Single</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCOMPLEX_SINGLE_FEATURE_COUNT = TCOMPLEX_FLOATING_POINT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TInheritedImpl <em>TInherited</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TInheritedImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTInherited()
	 * @generated
	 */
	int TINHERITED = 15;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__EXTERNAL_ID = GA_DATA_TYPE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__ID = GA_DATA_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__IS_FIXED_NAME = GA_DATA_TYPE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__NAME = GA_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__ORIGINAL_FULL_NAME = GA_DATA_TYPE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__ORIGINAL_NAME = GA_DATA_TYPE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__PARENT = GA_DATA_TYPE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__MODEL = GA_DATA_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__ANNOTATIONS = GA_DATA_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__MODULE = GA_DATA_TYPE__MODULE;

	/**
	 * The feature id for the '<em><b>Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED__RULE = GA_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>TInherited</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TINHERITED_FEATURE_COUNT = GA_DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TPointerImpl <em>TPointer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TPointerImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTPointer()
	 * @generated
	 */
	int TPOINTER = 16;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__EXTERNAL_ID = GA_DATA_TYPE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__ID = GA_DATA_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__IS_FIXED_NAME = GA_DATA_TYPE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__NAME = GA_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__ORIGINAL_FULL_NAME = GA_DATA_TYPE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__ORIGINAL_NAME = GA_DATA_TYPE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__PARENT = GA_DATA_TYPE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__MODEL = GA_DATA_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__ANNOTATIONS = GA_DATA_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__MODULE = GA_DATA_TYPE__MODULE;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER__BASE_TYPE = GA_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>TPointer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TPOINTER_FEATURE_COUNT = GA_DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TRealDoubleImpl <em>TReal Double</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TRealDoubleImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealDouble()
	 * @generated
	 */
	int TREAL_DOUBLE = 17;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__EXTERNAL_ID = TREAL_FLOATING_POINT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__ID = TREAL_FLOATING_POINT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__IS_FIXED_NAME = TREAL_FLOATING_POINT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__NAME = TREAL_FLOATING_POINT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__ORIGINAL_FULL_NAME = TREAL_FLOATING_POINT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__ORIGINAL_NAME = TREAL_FLOATING_POINT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__PARENT = TREAL_FLOATING_POINT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__MODEL = TREAL_FLOATING_POINT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__ANNOTATIONS = TREAL_FLOATING_POINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE__MODULE = TREAL_FLOATING_POINT__MODULE;

	/**
	 * The number of structural features of the '<em>TReal Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_DOUBLE_FEATURE_COUNT = TREAL_FLOATING_POINT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TRealFixedPointImpl <em>TReal Fixed Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TRealFixedPointImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealFixedPoint()
	 * @generated
	 */
	int TREAL_FIXED_POINT = 18;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__EXTERNAL_ID = TREAL_NUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__ID = TREAL_NUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__IS_FIXED_NAME = TREAL_NUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__NAME = TREAL_NUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__ORIGINAL_FULL_NAME = TREAL_NUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__ORIGINAL_NAME = TREAL_NUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__PARENT = TREAL_NUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__MODEL = TREAL_NUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__ANNOTATIONS = TREAL_NUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__MODULE = TREAL_NUMERIC__MODULE;

	/**
	 * The feature id for the '<em><b>Gain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__GAIN = TREAL_NUMERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>NBits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__NBITS = TREAL_NUMERIC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT__OFFSET = TREAL_NUMERIC_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>TReal Fixed Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_FIXED_POINT_FEATURE_COUNT = TREAL_NUMERIC_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TRealIntegerImpl <em>TReal Integer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TRealIntegerImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealInteger()
	 * @generated
	 */
	int TREAL_INTEGER = 19;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__EXTERNAL_ID = TREAL_NUMERIC__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__ID = TREAL_NUMERIC__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__IS_FIXED_NAME = TREAL_NUMERIC__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__NAME = TREAL_NUMERIC__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__ORIGINAL_FULL_NAME = TREAL_NUMERIC__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__ORIGINAL_NAME = TREAL_NUMERIC__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__PARENT = TREAL_NUMERIC__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__MODEL = TREAL_NUMERIC__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__ANNOTATIONS = TREAL_NUMERIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__MODULE = TREAL_NUMERIC__MODULE;

	/**
	 * The feature id for the '<em><b>NBits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__NBITS = TREAL_NUMERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER__SIGNED = TREAL_NUMERIC_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>TReal Integer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_INTEGER_FEATURE_COUNT = TREAL_NUMERIC_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TRealSingleImpl <em>TReal Single</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TRealSingleImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealSingle()
	 * @generated
	 */
	int TREAL_SINGLE = 20;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__EXTERNAL_ID = TREAL_FLOATING_POINT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__ID = TREAL_FLOATING_POINT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__IS_FIXED_NAME = TREAL_FLOATING_POINT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__NAME = TREAL_FLOATING_POINT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__ORIGINAL_FULL_NAME = TREAL_FLOATING_POINT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__ORIGINAL_NAME = TREAL_FLOATING_POINT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__PARENT = TREAL_FLOATING_POINT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__MODEL = TREAL_FLOATING_POINT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__ANNOTATIONS = TREAL_FLOATING_POINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE__MODULE = TREAL_FLOATING_POINT__MODULE;

	/**
	 * The number of structural features of the '<em>TReal Single</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREAL_SINGLE_FEATURE_COUNT = TREAL_FLOATING_POINT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TStringImpl <em>TString</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TStringImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTString()
	 * @generated
	 */
	int TSTRING = 21;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__EXTERNAL_ID = TPRIMITIVE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__ID = TPRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__IS_FIXED_NAME = TPRIMITIVE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__NAME = TPRIMITIVE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__ORIGINAL_FULL_NAME = TPRIMITIVE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__ORIGINAL_NAME = TPRIMITIVE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__PARENT = TPRIMITIVE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__MODEL = TPRIMITIVE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__ANNOTATIONS = TPRIMITIVE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING__MODULE = TPRIMITIVE__MODULE;

	/**
	 * The number of structural features of the '<em>TString</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSTRING_FEATURE_COUNT = TPRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TCustomImpl <em>TCustom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TCustomImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTCustom()
	 * @generated
	 */
	int TCUSTOM = 22;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__EXTERNAL_ID = TPRIMITIVE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__ID = TPRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__IS_FIXED_NAME = TPRIMITIVE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__NAME = TPRIMITIVE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__ORIGINAL_FULL_NAME = TPRIMITIVE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__ORIGINAL_NAME = TPRIMITIVE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__PARENT = TPRIMITIVE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__MODEL = TPRIMITIVE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__ANNOTATIONS = TPRIMITIVE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__MODULE = TPRIMITIVE__MODULE;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM__TYPE_REFERENCE = TPRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>TCustom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCUSTOM_FEATURE_COUNT = TPRIMITIVE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gadatatypes.impl.TVoidImpl <em>TVoid</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gadatatypes.impl.TVoidImpl
	 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTVoid()
	 * @generated
	 */
	int TVOID = 23;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__EXTERNAL_ID = TPRIMITIVE__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__ID = TPRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__IS_FIXED_NAME = TPRIMITIVE__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__NAME = TPRIMITIVE__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__ORIGINAL_FULL_NAME = TPRIMITIVE__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__ORIGINAL_NAME = TPRIMITIVE__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__PARENT = TPRIMITIVE__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__MODEL = TPRIMITIVE__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__ANNOTATIONS = TPRIMITIVE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID__MODULE = TPRIMITIVE__MODULE;

	/**
	 * The number of structural features of the '<em>TVoid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TVOID_FEATURE_COUNT = TPRIMITIVE_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.GADataType <em>GA Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA Data Type</em>'.
	 * @see geneauto.emf.models.gadatatypes.GADataType
	 * @generated
	 */
	EClass getGADataType();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gadatatypes.GADataType#getModule <em>Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Module</em>'.
	 * @see geneauto.emf.models.gadatatypes.GADataType#getModule()
	 * @see #getGADataType()
	 * @generated
	 */
	EReference getGADataType_Module();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TBottom <em>TBottom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TBottom</em>'.
	 * @see geneauto.emf.models.gadatatypes.TBottom
	 * @generated
	 */
	EClass getTBottom();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TComplexFloatingPoint <em>TComplex Floating Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TComplex Floating Point</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexFloatingPoint
	 * @generated
	 */
	EClass getTComplexFloatingPoint();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TComplexNumeric <em>TComplex Numeric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TComplex Numeric</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexNumeric
	 * @generated
	 */
	EClass getTComplexNumeric();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TNumeric <em>TNumeric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TNumeric</em>'.
	 * @see geneauto.emf.models.gadatatypes.TNumeric
	 * @generated
	 */
	EClass getTNumeric();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TPrimitive <em>TPrimitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TPrimitive</em>'.
	 * @see geneauto.emf.models.gadatatypes.TPrimitive
	 * @generated
	 */
	EClass getTPrimitive();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TEvent <em>TEvent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TEvent</em>'.
	 * @see geneauto.emf.models.gadatatypes.TEvent
	 * @generated
	 */
	EClass getTEvent();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TRealFloatingPoint <em>TReal Floating Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TReal Floating Point</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealFloatingPoint
	 * @generated
	 */
	EClass getTRealFloatingPoint();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TRealNumeric <em>TReal Numeric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TReal Numeric</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealNumeric
	 * @generated
	 */
	EClass getTRealNumeric();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TArray <em>TArray</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TArray</em>'.
	 * @see geneauto.emf.models.gadatatypes.TArray
	 * @generated
	 */
	EClass getTArray();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gadatatypes.TArray#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Base Type</em>'.
	 * @see geneauto.emf.models.gadatatypes.TArray#getBaseType()
	 * @see #getTArray()
	 * @generated
	 */
	EReference getTArray_BaseType();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gadatatypes.TArray#getDimensions <em>Dimensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dimensions</em>'.
	 * @see geneauto.emf.models.gadatatypes.TArray#getDimensions()
	 * @see #getTArray()
	 * @generated
	 */
	EReference getTArray_Dimensions();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TBoolean <em>TBoolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TBoolean</em>'.
	 * @see geneauto.emf.models.gadatatypes.TBoolean
	 * @generated
	 */
	EClass getTBoolean();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TComplexDouble <em>TComplex Double</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TComplex Double</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexDouble
	 * @generated
	 */
	EClass getTComplexDouble();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TComplexFixedPoint <em>TComplex Fixed Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TComplex Fixed Point</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexFixedPoint
	 * @generated
	 */
	EClass getTComplexFixedPoint();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TComplexFixedPoint#getGain <em>Gain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Gain</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexFixedPoint#getGain()
	 * @see #getTComplexFixedPoint()
	 * @generated
	 */
	EAttribute getTComplexFixedPoint_Gain();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TComplexFixedPoint#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexFixedPoint#getOffset()
	 * @see #getTComplexFixedPoint()
	 * @generated
	 */
	EAttribute getTComplexFixedPoint_Offset();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TComplexFixedPoint#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexFixedPoint#getSize()
	 * @see #getTComplexFixedPoint()
	 * @generated
	 */
	EAttribute getTComplexFixedPoint_Size();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TComplexInteger <em>TComplex Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TComplex Integer</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexInteger
	 * @generated
	 */
	EClass getTComplexInteger();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TComplexInteger#getNBits <em>NBits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NBits</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexInteger#getNBits()
	 * @see #getTComplexInteger()
	 * @generated
	 */
	EAttribute getTComplexInteger_NBits();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TComplexInteger#isSigned <em>Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signed</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexInteger#isSigned()
	 * @see #getTComplexInteger()
	 * @generated
	 */
	EAttribute getTComplexInteger_Signed();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TComplexSingle <em>TComplex Single</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TComplex Single</em>'.
	 * @see geneauto.emf.models.gadatatypes.TComplexSingle
	 * @generated
	 */
	EClass getTComplexSingle();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TInherited <em>TInherited</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TInherited</em>'.
	 * @see geneauto.emf.models.gadatatypes.TInherited
	 * @generated
	 */
	EClass getTInherited();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TInherited#getRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule</em>'.
	 * @see geneauto.emf.models.gadatatypes.TInherited#getRule()
	 * @see #getTInherited()
	 * @generated
	 */
	EAttribute getTInherited_Rule();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TPointer <em>TPointer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TPointer</em>'.
	 * @see geneauto.emf.models.gadatatypes.TPointer
	 * @generated
	 */
	EClass getTPointer();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gadatatypes.TPointer#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Base Type</em>'.
	 * @see geneauto.emf.models.gadatatypes.TPointer#getBaseType()
	 * @see #getTPointer()
	 * @generated
	 */
	EReference getTPointer_BaseType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TRealDouble <em>TReal Double</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TReal Double</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealDouble
	 * @generated
	 */
	EClass getTRealDouble();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TRealFixedPoint <em>TReal Fixed Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TReal Fixed Point</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealFixedPoint
	 * @generated
	 */
	EClass getTRealFixedPoint();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TRealFixedPoint#getGain <em>Gain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Gain</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealFixedPoint#getGain()
	 * @see #getTRealFixedPoint()
	 * @generated
	 */
	EAttribute getTRealFixedPoint_Gain();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TRealFixedPoint#getNBits <em>NBits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NBits</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealFixedPoint#getNBits()
	 * @see #getTRealFixedPoint()
	 * @generated
	 */
	EAttribute getTRealFixedPoint_NBits();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TRealFixedPoint#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealFixedPoint#getOffset()
	 * @see #getTRealFixedPoint()
	 * @generated
	 */
	EAttribute getTRealFixedPoint_Offset();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TRealInteger <em>TReal Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TReal Integer</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealInteger
	 * @generated
	 */
	EClass getTRealInteger();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TRealInteger#getNBits <em>NBits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NBits</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealInteger#getNBits()
	 * @see #getTRealInteger()
	 * @generated
	 */
	EAttribute getTRealInteger_NBits();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gadatatypes.TRealInteger#isSigned <em>Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signed</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealInteger#isSigned()
	 * @see #getTRealInteger()
	 * @generated
	 */
	EAttribute getTRealInteger_Signed();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TRealSingle <em>TReal Single</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TReal Single</em>'.
	 * @see geneauto.emf.models.gadatatypes.TRealSingle
	 * @generated
	 */
	EClass getTRealSingle();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TString <em>TString</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TString</em>'.
	 * @see geneauto.emf.models.gadatatypes.TString
	 * @generated
	 */
	EClass getTString();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TCustom <em>TCustom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TCustom</em>'.
	 * @see geneauto.emf.models.gadatatypes.TCustom
	 * @generated
	 */
	EClass getTCustom();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gadatatypes.TCustom#getTypeReference <em>Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type Reference</em>'.
	 * @see geneauto.emf.models.gadatatypes.TCustom#getTypeReference()
	 * @see #getTCustom()
	 * @generated
	 */
	EReference getTCustom_TypeReference();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gadatatypes.TVoid <em>TVoid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TVoid</em>'.
	 * @see geneauto.emf.models.gadatatypes.TVoid
	 * @generated
	 */
	EClass getTVoid();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GadatatypesFactory getGadatatypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.GADataTypeImpl <em>GA Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.GADataTypeImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getGADataType()
		 * @generated
		 */
		EClass GA_DATA_TYPE = eINSTANCE.getGADataType();

		/**
		 * The meta object literal for the '<em><b>Module</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_DATA_TYPE__MODULE = eINSTANCE.getGADataType_Module();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TBottomImpl <em>TBottom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TBottomImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTBottom()
		 * @generated
		 */
		EClass TBOTTOM = eINSTANCE.getTBottom();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexFloatingPointImpl <em>TComplex Floating Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TComplexFloatingPointImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexFloatingPoint()
		 * @generated
		 */
		EClass TCOMPLEX_FLOATING_POINT = eINSTANCE.getTComplexFloatingPoint();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexNumericImpl <em>TComplex Numeric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TComplexNumericImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexNumeric()
		 * @generated
		 */
		EClass TCOMPLEX_NUMERIC = eINSTANCE.getTComplexNumeric();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TNumericImpl <em>TNumeric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TNumericImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTNumeric()
		 * @generated
		 */
		EClass TNUMERIC = eINSTANCE.getTNumeric();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TPrimitiveImpl <em>TPrimitive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TPrimitiveImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTPrimitive()
		 * @generated
		 */
		EClass TPRIMITIVE = eINSTANCE.getTPrimitive();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TEventImpl <em>TEvent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TEventImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTEvent()
		 * @generated
		 */
		EClass TEVENT = eINSTANCE.getTEvent();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TRealFloatingPointImpl <em>TReal Floating Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TRealFloatingPointImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealFloatingPoint()
		 * @generated
		 */
		EClass TREAL_FLOATING_POINT = eINSTANCE.getTRealFloatingPoint();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TRealNumericImpl <em>TReal Numeric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TRealNumericImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealNumeric()
		 * @generated
		 */
		EClass TREAL_NUMERIC = eINSTANCE.getTRealNumeric();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TArrayImpl <em>TArray</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TArrayImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTArray()
		 * @generated
		 */
		EClass TARRAY = eINSTANCE.getTArray();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARRAY__BASE_TYPE = eINSTANCE.getTArray_BaseType();

		/**
		 * The meta object literal for the '<em><b>Dimensions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARRAY__DIMENSIONS = eINSTANCE.getTArray_Dimensions();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TBooleanImpl <em>TBoolean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TBooleanImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTBoolean()
		 * @generated
		 */
		EClass TBOOLEAN = eINSTANCE.getTBoolean();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexDoubleImpl <em>TComplex Double</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TComplexDoubleImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexDouble()
		 * @generated
		 */
		EClass TCOMPLEX_DOUBLE = eINSTANCE.getTComplexDouble();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexFixedPointImpl <em>TComplex Fixed Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TComplexFixedPointImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexFixedPoint()
		 * @generated
		 */
		EClass TCOMPLEX_FIXED_POINT = eINSTANCE.getTComplexFixedPoint();

		/**
		 * The meta object literal for the '<em><b>Gain</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TCOMPLEX_FIXED_POINT__GAIN = eINSTANCE.getTComplexFixedPoint_Gain();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TCOMPLEX_FIXED_POINT__OFFSET = eINSTANCE.getTComplexFixedPoint_Offset();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TCOMPLEX_FIXED_POINT__SIZE = eINSTANCE.getTComplexFixedPoint_Size();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexIntegerImpl <em>TComplex Integer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TComplexIntegerImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexInteger()
		 * @generated
		 */
		EClass TCOMPLEX_INTEGER = eINSTANCE.getTComplexInteger();

		/**
		 * The meta object literal for the '<em><b>NBits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TCOMPLEX_INTEGER__NBITS = eINSTANCE.getTComplexInteger_NBits();

		/**
		 * The meta object literal for the '<em><b>Signed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TCOMPLEX_INTEGER__SIGNED = eINSTANCE.getTComplexInteger_Signed();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TComplexSingleImpl <em>TComplex Single</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TComplexSingleImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTComplexSingle()
		 * @generated
		 */
		EClass TCOMPLEX_SINGLE = eINSTANCE.getTComplexSingle();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TInheritedImpl <em>TInherited</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TInheritedImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTInherited()
		 * @generated
		 */
		EClass TINHERITED = eINSTANCE.getTInherited();

		/**
		 * The meta object literal for the '<em><b>Rule</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TINHERITED__RULE = eINSTANCE.getTInherited_Rule();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TPointerImpl <em>TPointer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TPointerImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTPointer()
		 * @generated
		 */
		EClass TPOINTER = eINSTANCE.getTPointer();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TPOINTER__BASE_TYPE = eINSTANCE.getTPointer_BaseType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TRealDoubleImpl <em>TReal Double</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TRealDoubleImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealDouble()
		 * @generated
		 */
		EClass TREAL_DOUBLE = eINSTANCE.getTRealDouble();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TRealFixedPointImpl <em>TReal Fixed Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TRealFixedPointImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealFixedPoint()
		 * @generated
		 */
		EClass TREAL_FIXED_POINT = eINSTANCE.getTRealFixedPoint();

		/**
		 * The meta object literal for the '<em><b>Gain</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TREAL_FIXED_POINT__GAIN = eINSTANCE.getTRealFixedPoint_Gain();

		/**
		 * The meta object literal for the '<em><b>NBits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TREAL_FIXED_POINT__NBITS = eINSTANCE.getTRealFixedPoint_NBits();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TREAL_FIXED_POINT__OFFSET = eINSTANCE.getTRealFixedPoint_Offset();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TRealIntegerImpl <em>TReal Integer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TRealIntegerImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealInteger()
		 * @generated
		 */
		EClass TREAL_INTEGER = eINSTANCE.getTRealInteger();

		/**
		 * The meta object literal for the '<em><b>NBits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TREAL_INTEGER__NBITS = eINSTANCE.getTRealInteger_NBits();

		/**
		 * The meta object literal for the '<em><b>Signed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TREAL_INTEGER__SIGNED = eINSTANCE.getTRealInteger_Signed();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TRealSingleImpl <em>TReal Single</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TRealSingleImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTRealSingle()
		 * @generated
		 */
		EClass TREAL_SINGLE = eINSTANCE.getTRealSingle();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TStringImpl <em>TString</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TStringImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTString()
		 * @generated
		 */
		EClass TSTRING = eINSTANCE.getTString();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TCustomImpl <em>TCustom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TCustomImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTCustom()
		 * @generated
		 */
		EClass TCUSTOM = eINSTANCE.getTCustom();

		/**
		 * The meta object literal for the '<em><b>Type Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TCUSTOM__TYPE_REFERENCE = eINSTANCE.getTCustom_TypeReference();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gadatatypes.impl.TVoidImpl <em>TVoid</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gadatatypes.impl.TVoidImpl
		 * @see geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl#getTVoid()
		 * @generated
		 */
		EClass TVOID = eINSTANCE.getTVoid();

	}

} //GadatatypesPackage
