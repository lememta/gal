/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow Graph Composition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A collection of transitions and junctions.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getJunctions <em>Junctions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getDefaultTransitionList <em>Default Transition List</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getFlowGraphComposition()
 * @model
 * @generated
 */
public interface FlowGraphComposition extends Composition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Junctions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.Junction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Junctions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Junctions</em>' containment reference list.
	 * @see #isSetJunctions()
	 * @see #unsetJunctions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getFlowGraphComposition_Junctions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Junction> getJunctions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getJunctions <em>Junctions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetJunctions()
	 * @see #getJunctions()
	 * @generated
	 */
	void unsetJunctions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getJunctions <em>Junctions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Junctions</em>' containment reference list is set.
	 * @see #unsetJunctions()
	 * @see #getJunctions()
	 * @generated
	 */
	boolean isSetJunctions();

	/**
	 * Returns the value of the '<em><b>Default Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Transition List</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Transition List</em>' containment reference.
	 * @see #isSetDefaultTransitionList()
	 * @see #unsetDefaultTransitionList()
	 * @see #setDefaultTransitionList(TransitionList)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getFlowGraphComposition_DefaultTransitionList()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	TransitionList getDefaultTransitionList();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getDefaultTransitionList <em>Default Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Transition List</em>' containment reference.
	 * @see #isSetDefaultTransitionList()
	 * @see #unsetDefaultTransitionList()
	 * @see #getDefaultTransitionList()
	 * @generated
	 */
	void setDefaultTransitionList(TransitionList value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getDefaultTransitionList <em>Default Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDefaultTransitionList()
	 * @see #getDefaultTransitionList()
	 * @see #setDefaultTransitionList(TransitionList)
	 * @generated
	 */
	void unsetDefaultTransitionList();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getDefaultTransitionList <em>Default Transition List</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Default Transition List</em>' containment reference is set.
	 * @see #unsetDefaultTransitionList()
	 * @see #getDefaultTransitionList()
	 * @see #setDefaultTransitionList(TransitionList)
	 * @generated
	 */
	boolean isSetDefaultTransitionList();

} // FlowGraphComposition
