/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.common.CommonPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelFactory
 * @model kind="package"
 * @generated
 */
public interface GastatemodelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gastatemodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gasystemmodel/gastatemodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gasystemmodel.gastatemodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GastatemodelPackage eINSTANCE = geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.LocationImpl <em>Location</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.LocationImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getLocation()
	 * @generated
	 */
	int LOCATION = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>State Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__STATE_VARIABLE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Location</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.ChartRootImpl <em>Chart Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.ChartRootImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getChartRoot()
	 * @generated
	 */
	int CHART_ROOT = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__EXTERNAL_ID = LOCATION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__ID = LOCATION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__IS_FIXED_NAME = LOCATION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__NAME = LOCATION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__ORIGINAL_FULL_NAME = LOCATION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__ORIGINAL_NAME = LOCATION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__PARENT = LOCATION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__MODEL = LOCATION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__ANNOTATIONS = LOCATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__CODE_MODEL_ELEMENT = LOCATION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>State Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT__STATE_VARIABLE = LOCATION__STATE_VARIABLE;

	/**
	 * The number of structural features of the '<em>Chart Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_ROOT_FEATURE_COUNT = LOCATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.JunctionImpl <em>Junction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.JunctionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getJunction()
	 * @generated
	 */
	int JUNCTION = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__EXTERNAL_ID = LOCATION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__ID = LOCATION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__IS_FIXED_NAME = LOCATION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__NAME = LOCATION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__ORIGINAL_FULL_NAME = LOCATION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__ORIGINAL_NAME = LOCATION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__PARENT = LOCATION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__MODEL = LOCATION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__ANNOTATIONS = LOCATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__CODE_MODEL_ELEMENT = LOCATION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>State Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__STATE_VARIABLE = LOCATION__STATE_VARIABLE;

	/**
	 * The feature id for the '<em><b>Junction Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__JUNCTION_TYPE = LOCATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__TRANSITION_LIST = LOCATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Junction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_FEATURE_COUNT = LOCATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionListImpl <em>Transition List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionListImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getTransitionList()
	 * @generated
	 */
	int TRANSITION_LIST = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST__TRANSITIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Transition List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_LIST_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EXECUTION_ORDER = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DESTINATION = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EVENTS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TYPE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Condition Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__CONDITION_ACTIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Transition Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRANSITION_ACTIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__GUARD = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Event Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__EVENT_CODE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Trigger Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TRIGGER_TYPE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__SCOPE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Out Control Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__OUT_CONTROL_PORT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__REFERENCE_EXPRESSION = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Binder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__BINDER = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.CompositionImpl <em>Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.CompositionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getComposition()
	 * @generated
	 */
	int COMPOSITION = 6;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Boxes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__BOXES = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.BoxImpl <em>Box</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.BoxImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getBox()
	 * @generated
	 */
	int BOX = 7;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__FUNCTIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Boxes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX__BOXES = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Box</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOX_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.ActionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 9;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__OPERATION = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.SimpleActionImpl <em>Simple Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.SimpleActionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getSimpleAction()
	 * @generated
	 */
	int SIMPLE_ACTION = 8;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__EXTERNAL_ID = ACTION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__ID = ACTION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__IS_FIXED_NAME = ACTION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__NAME = ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__ORIGINAL_FULL_NAME = ACTION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__ORIGINAL_NAME = ACTION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__PARENT = ACTION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__MODEL = ACTION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__ANNOTATIONS = ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__CODE_MODEL_ELEMENT = ACTION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION__OPERATION = ACTION__OPERATION;

	/**
	 * The number of structural features of the '<em>Simple Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.OnEventActionImpl <em>On Event Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.OnEventActionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getOnEventAction()
	 * @generated
	 */
	int ON_EVENT_ACTION = 10;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__EXTERNAL_ID = ACTION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__ID = ACTION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__IS_FIXED_NAME = ACTION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__NAME = ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__ORIGINAL_FULL_NAME = ACTION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__ORIGINAL_NAME = ACTION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__PARENT = ACTION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__MODEL = ACTION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__ANNOTATIONS = ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__CODE_MODEL_ELEMENT = ACTION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__OPERATION = ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION__EVENT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>On Event Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_EVENT_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.OrCompositionImpl <em>Or Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.OrCompositionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getOrComposition()
	 * @generated
	 */
	int OR_COMPOSITION = 11;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__EXTERNAL_ID = COMPOSITION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__ID = COMPOSITION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__IS_FIXED_NAME = COMPOSITION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__NAME = COMPOSITION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__ORIGINAL_FULL_NAME = COMPOSITION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__ORIGINAL_NAME = COMPOSITION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__PARENT = COMPOSITION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__MODEL = COMPOSITION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__ANNOTATIONS = COMPOSITION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__CODE_MODEL_ELEMENT = COMPOSITION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Boxes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__BOXES = COMPOSITION__BOXES;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__STATES = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Junctions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__JUNCTIONS = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION__DEFAULT_TRANSITION_LIST = COMPOSITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Or Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_COMPOSITION_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.AndCompositionImpl <em>And Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.AndCompositionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getAndComposition()
	 * @generated
	 */
	int AND_COMPOSITION = 12;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__EXTERNAL_ID = COMPOSITION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__ID = COMPOSITION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__IS_FIXED_NAME = COMPOSITION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__NAME = COMPOSITION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__ORIGINAL_FULL_NAME = COMPOSITION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__ORIGINAL_NAME = COMPOSITION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__PARENT = COMPOSITION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__MODEL = COMPOSITION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__ANNOTATIONS = COMPOSITION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__CODE_MODEL_ELEMENT = COMPOSITION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Boxes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__BOXES = COMPOSITION__BOXES;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION__STATES = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>And Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_COMPOSITION_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.FlowGraphCompositionImpl <em>Flow Graph Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.FlowGraphCompositionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getFlowGraphComposition()
	 * @generated
	 */
	int FLOW_GRAPH_COMPOSITION = 13;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__EXTERNAL_ID = COMPOSITION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__ID = COMPOSITION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__IS_FIXED_NAME = COMPOSITION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__NAME = COMPOSITION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__ORIGINAL_FULL_NAME = COMPOSITION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__ORIGINAL_NAME = COMPOSITION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__PARENT = COMPOSITION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__MODEL = COMPOSITION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__ANNOTATIONS = COMPOSITION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__CODE_MODEL_ELEMENT = COMPOSITION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Boxes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__BOXES = COMPOSITION__BOXES;

	/**
	 * The feature id for the '<em><b>Junctions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__JUNCTIONS = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Default Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Flow Graph Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_GRAPH_COMPOSITION_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.GraphicalFunctionImpl <em>Graphical Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GraphicalFunctionImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getGraphicalFunction()
	 * @generated
	 */
	int GRAPHICAL_FUNCTION = 14;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__EXTERNAL_ID = CommonPackage.FUNCTION_SM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__ID = CommonPackage.FUNCTION_SM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__IS_FIXED_NAME = CommonPackage.FUNCTION_SM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__NAME = CommonPackage.FUNCTION_SM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__ORIGINAL_FULL_NAME = CommonPackage.FUNCTION_SM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__ORIGINAL_NAME = CommonPackage.FUNCTION_SM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__PARENT = CommonPackage.FUNCTION_SM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__MODEL = CommonPackage.FUNCTION_SM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__ANNOTATIONS = CommonPackage.FUNCTION_SM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__CODE_MODEL_ELEMENT = CommonPackage.FUNCTION_SM__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__DATA_TYPE = CommonPackage.FUNCTION_SM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__SCOPE = CommonPackage.FUNCTION_SM__SCOPE;

	/**
	 * The feature id for the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__REFERENCE_EXPRESSION = CommonPackage.FUNCTION_SM__REFERENCE_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__ARGUMENTS = CommonPackage.FUNCTION_SM__ARGUMENTS;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__VARIABLES = CommonPackage.FUNCTION_SM__VARIABLES;

	/**
	 * The feature id for the '<em><b>Composition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION__COMPOSITION = CommonPackage.FUNCTION_SM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Graphical Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICAL_FUNCTION_FEATURE_COUNT = CommonPackage.FUNCTION_SM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getState()
	 * @generated
	 */
	int STATE = 15;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__EXTERNAL_ID = LOCATION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ID = LOCATION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__IS_FIXED_NAME = LOCATION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = LOCATION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ORIGINAL_FULL_NAME = LOCATION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ORIGINAL_NAME = LOCATION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__PARENT = LOCATION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__MODEL = LOCATION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ANNOTATIONS = LOCATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__CODE_MODEL_ELEMENT = LOCATION__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>State Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__STATE_VARIABLE = LOCATION__STATE_VARIABLE;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__EXECUTION_ORDER = LOCATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Machine Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__MACHINE_TYPE = LOCATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__FUNCTIONS = LOCATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__VARIABLES = LOCATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__TYPE = LOCATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>During Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__DURING_ACTIONS = LOCATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Entry Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ENTRY_ACTIONS = LOCATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Exit Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__EXIT_ACTIONS = LOCATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Outer Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__OUTER_TRANSITION_LIST = LOCATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Inner Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__INNER_TRANSITION_LIST = LOCATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Composition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__COMPOSITION = LOCATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__EVENTS = LOCATION_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = LOCATION_FEATURE_COUNT + 12;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.JunctionType <em>Junction Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.JunctionType
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getJunctionType()
	 * @generated
	 */
	int JUNCTION_TYPE = 16;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.EventScope <em>Event Scope</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.EventScope
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getEventScope()
	 * @generated
	 */
	int EVENT_SCOPE = 17;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType <em>Transition Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getTransitionType()
	 * @generated
	 */
	int TRANSITION_TYPE = 18;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType <em>State Machine Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getStateMachineType()
	 * @generated
	 */
	int STATE_MACHINE_TYPE = 19;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.StateType <em>State Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateType
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getStateType()
	 * @generated
	 */
	int STATE_TYPE = 20;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Location <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Location</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Location
	 * @generated
	 */
	EClass getLocation();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Location#getStateVariable <em>State Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>State Variable</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Location#getStateVariable()
	 * @see #getLocation()
	 * @generated
	 */
	EReference getLocation_StateVariable();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.ChartRoot <em>Chart Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chart Root</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.ChartRoot
	 * @generated
	 */
	EClass getChartRoot();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Junction <em>Junction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Junction</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Junction
	 * @generated
	 */
	EClass getJunction();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Junction#getJunctionType <em>Junction Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Junction Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Junction#getJunctionType()
	 * @see #getJunction()
	 * @generated
	 */
	EAttribute getJunction_JunctionType();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Junction#getTransitionList <em>Transition List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transition List</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Junction#getTransitionList()
	 * @see #getJunction()
	 * @generated
	 */
	EReference getJunction_TransitionList();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.TransitionList <em>Transition List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition List</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.TransitionList
	 * @generated
	 */
	EClass getTransitionList();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.TransitionList#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.TransitionList#getTransitions()
	 * @see #getTransitionList()
	 * @generated
	 */
	EReference getTransitionList_Transitions();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getExecutionOrder <em>Execution Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Order</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getExecutionOrder()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_ExecutionOrder();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Destination</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getDestination()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Destination();

	/**
	 * Returns the meta object for the reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getEvents()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Events();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getType()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getConditionActions <em>Condition Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition Actions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getConditionActions()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_ConditionActions();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getTransitionActions <em>Transition Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transition Actions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getTransitionActions()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_TransitionActions();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getGuard()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Guard();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getEventCode <em>Event Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Code</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Event#getEventCode()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_EventCode();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getTriggerType <em>Trigger Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Event#getTriggerType()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_TriggerType();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Event#getScope()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Scope();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getOutControlPort <em>Out Control Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Out Control Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Event#getOutControlPort()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_OutControlPort();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getReferenceExpression <em>Reference Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference Expression</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Event#getReferenceExpression()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_ReferenceExpression();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getBinder <em>Binder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Binder</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Event#getBinder()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Binder();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Composition <em>Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Composition
	 * @generated
	 */
	EClass getComposition();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Composition#getBoxes <em>Boxes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Boxes</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Composition#getBoxes()
	 * @see #getComposition()
	 * @generated
	 */
	EReference getComposition_Boxes();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box <em>Box</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Box</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Box
	 * @generated
	 */
	EClass getBox();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getFunctions <em>Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Box#getFunctions()
	 * @see #getBox()
	 * @generated
	 */
	EReference getBox_Functions();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getBoxes <em>Boxes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Boxes</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Box#getBoxes()
	 * @see #getBox()
	 * @generated
	 */
	EReference getBox_Boxes();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.SimpleAction <em>Simple Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Action</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.SimpleAction
	 * @generated
	 */
	EClass getSimpleAction();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Action#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.Action#getOperation()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Operation();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.OnEventAction <em>On Event Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>On Event Action</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.OnEventAction
	 * @generated
	 */
	EClass getOnEventAction();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.OnEventAction#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.OnEventAction#getEvent()
	 * @see #getOnEventAction()
	 * @generated
	 */
	EReference getOnEventAction_Event();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition <em>Or Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Composition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition
	 * @generated
	 */
	EClass getOrComposition();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition#getStates()
	 * @see #getOrComposition()
	 * @generated
	 */
	EReference getOrComposition_States();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition#getJunctions <em>Junctions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Junctions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition#getJunctions()
	 * @see #getOrComposition()
	 * @generated
	 */
	EReference getOrComposition_Junctions();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition#getDefaultTransitionList <em>Default Transition List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Transition List</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.OrComposition#getDefaultTransitionList()
	 * @see #getOrComposition()
	 * @generated
	 */
	EReference getOrComposition_DefaultTransitionList();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.AndComposition <em>And Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Composition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.AndComposition
	 * @generated
	 */
	EClass getAndComposition();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.AndComposition#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.AndComposition#getStates()
	 * @see #getAndComposition()
	 * @generated
	 */
	EReference getAndComposition_States();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition <em>Flow Graph Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow Graph Composition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition
	 * @generated
	 */
	EClass getFlowGraphComposition();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getJunctions <em>Junctions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Junctions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getJunctions()
	 * @see #getFlowGraphComposition()
	 * @generated
	 */
	EReference getFlowGraphComposition_Junctions();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getDefaultTransitionList <em>Default Transition List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Transition List</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition#getDefaultTransitionList()
	 * @see #getFlowGraphComposition()
	 * @generated
	 */
	EReference getFlowGraphComposition_DefaultTransitionList();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.GraphicalFunction <em>Graphical Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graphical Function</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GraphicalFunction
	 * @generated
	 */
	EClass getGraphicalFunction();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.GraphicalFunction#getComposition <em>Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Composition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GraphicalFunction#getComposition()
	 * @see #getGraphicalFunction()
	 * @generated
	 */
	EReference getGraphicalFunction_Composition();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExecutionOrder <em>Execution Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Order</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getExecutionOrder()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_ExecutionOrder();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getMachineType <em>Machine Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Machine Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getMachineType()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_MachineType();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getFunctions <em>Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getFunctions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Functions();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getVariables()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Variables();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getType()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getDuringActions <em>During Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>During Actions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getDuringActions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_DuringActions();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEntryActions <em>Entry Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entry Actions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getEntryActions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_EntryActions();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExitActions <em>Exit Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Exit Actions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getExitActions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_ExitActions();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getOuterTransitionList <em>Outer Transition List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Outer Transition List</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getOuterTransitionList()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_OuterTransitionList();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getInnerTransitionList <em>Inner Transition List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inner Transition List</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getInnerTransitionList()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_InnerTransitionList();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getComposition <em>Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Composition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getComposition()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Composition();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.State#getEvents()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Events();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.gastatemodel.JunctionType <em>Junction Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Junction Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.JunctionType
	 * @generated
	 */
	EEnum getJunctionType();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.gastatemodel.EventScope <em>Event Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Event Scope</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.EventScope
	 * @generated
	 */
	EEnum getEventScope();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType <em>Transition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Transition Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType
	 * @generated
	 */
	EEnum getTransitionType();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType <em>State Machine Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>State Machine Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType
	 * @generated
	 */
	EEnum getStateMachineType();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.gastatemodel.StateType <em>State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>State Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateType
	 * @generated
	 */
	EEnum getStateType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GastatemodelFactory getGastatemodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.LocationImpl <em>Location</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.LocationImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getLocation()
		 * @generated
		 */
		EClass LOCATION = eINSTANCE.getLocation();

		/**
		 * The meta object literal for the '<em><b>State Variable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION__STATE_VARIABLE = eINSTANCE.getLocation_StateVariable();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.ChartRootImpl <em>Chart Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.ChartRootImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getChartRoot()
		 * @generated
		 */
		EClass CHART_ROOT = eINSTANCE.getChartRoot();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.JunctionImpl <em>Junction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.JunctionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getJunction()
		 * @generated
		 */
		EClass JUNCTION = eINSTANCE.getJunction();

		/**
		 * The meta object literal for the '<em><b>Junction Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JUNCTION__JUNCTION_TYPE = eINSTANCE.getJunction_JunctionType();

		/**
		 * The meta object literal for the '<em><b>Transition List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JUNCTION__TRANSITION_LIST = eINSTANCE.getJunction_TransitionList();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionListImpl <em>Transition List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionListImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getTransitionList()
		 * @generated
		 */
		EClass TRANSITION_LIST = eINSTANCE.getTransitionList();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION_LIST__TRANSITIONS = eINSTANCE.getTransitionList_Transitions();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.TransitionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Execution Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__EXECUTION_ORDER = eINSTANCE.getTransition_ExecutionOrder();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__DESTINATION = eINSTANCE.getTransition_Destination();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EVENTS = eINSTANCE.getTransition_Events();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__TYPE = eINSTANCE.getTransition_Type();

		/**
		 * The meta object literal for the '<em><b>Condition Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__CONDITION_ACTIONS = eINSTANCE.getTransition_ConditionActions();

		/**
		 * The meta object literal for the '<em><b>Transition Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TRANSITION_ACTIONS = eINSTANCE.getTransition_TransitionActions();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__GUARD = eINSTANCE.getTransition_Guard();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Event Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__EVENT_CODE = eINSTANCE.getEvent_EventCode();

		/**
		 * The meta object literal for the '<em><b>Trigger Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__TRIGGER_TYPE = eINSTANCE.getEvent_TriggerType();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__SCOPE = eINSTANCE.getEvent_Scope();

		/**
		 * The meta object literal for the '<em><b>Out Control Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__OUT_CONTROL_PORT = eINSTANCE.getEvent_OutControlPort();

		/**
		 * The meta object literal for the '<em><b>Reference Expression</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__REFERENCE_EXPRESSION = eINSTANCE.getEvent_ReferenceExpression();

		/**
		 * The meta object literal for the '<em><b>Binder</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__BINDER = eINSTANCE.getEvent_Binder();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.CompositionImpl <em>Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.CompositionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getComposition()
		 * @generated
		 */
		EClass COMPOSITION = eINSTANCE.getComposition();

		/**
		 * The meta object literal for the '<em><b>Boxes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITION__BOXES = eINSTANCE.getComposition_Boxes();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.BoxImpl <em>Box</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.BoxImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getBox()
		 * @generated
		 */
		EClass BOX = eINSTANCE.getBox();

		/**
		 * The meta object literal for the '<em><b>Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOX__FUNCTIONS = eINSTANCE.getBox_Functions();

		/**
		 * The meta object literal for the '<em><b>Boxes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOX__BOXES = eINSTANCE.getBox_Boxes();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.SimpleActionImpl <em>Simple Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.SimpleActionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getSimpleAction()
		 * @generated
		 */
		EClass SIMPLE_ACTION = eINSTANCE.getSimpleAction();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.ActionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__OPERATION = eINSTANCE.getAction_Operation();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.OnEventActionImpl <em>On Event Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.OnEventActionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getOnEventAction()
		 * @generated
		 */
		EClass ON_EVENT_ACTION = eINSTANCE.getOnEventAction();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ON_EVENT_ACTION__EVENT = eINSTANCE.getOnEventAction_Event();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.OrCompositionImpl <em>Or Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.OrCompositionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getOrComposition()
		 * @generated
		 */
		EClass OR_COMPOSITION = eINSTANCE.getOrComposition();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_COMPOSITION__STATES = eINSTANCE.getOrComposition_States();

		/**
		 * The meta object literal for the '<em><b>Junctions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_COMPOSITION__JUNCTIONS = eINSTANCE.getOrComposition_Junctions();

		/**
		 * The meta object literal for the '<em><b>Default Transition List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_COMPOSITION__DEFAULT_TRANSITION_LIST = eINSTANCE.getOrComposition_DefaultTransitionList();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.AndCompositionImpl <em>And Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.AndCompositionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getAndComposition()
		 * @generated
		 */
		EClass AND_COMPOSITION = eINSTANCE.getAndComposition();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND_COMPOSITION__STATES = eINSTANCE.getAndComposition_States();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.FlowGraphCompositionImpl <em>Flow Graph Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.FlowGraphCompositionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getFlowGraphComposition()
		 * @generated
		 */
		EClass FLOW_GRAPH_COMPOSITION = eINSTANCE.getFlowGraphComposition();

		/**
		 * The meta object literal for the '<em><b>Junctions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLOW_GRAPH_COMPOSITION__JUNCTIONS = eINSTANCE.getFlowGraphComposition_Junctions();

		/**
		 * The meta object literal for the '<em><b>Default Transition List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST = eINSTANCE.getFlowGraphComposition_DefaultTransitionList();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.GraphicalFunctionImpl <em>Graphical Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GraphicalFunctionImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getGraphicalFunction()
		 * @generated
		 */
		EClass GRAPHICAL_FUNCTION = eINSTANCE.getGraphicalFunction();

		/**
		 * The meta object literal for the '<em><b>Composition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPHICAL_FUNCTION__COMPOSITION = eINSTANCE.getGraphicalFunction_Composition();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Execution Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__EXECUTION_ORDER = eINSTANCE.getState_ExecutionOrder();

		/**
		 * The meta object literal for the '<em><b>Machine Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__MACHINE_TYPE = eINSTANCE.getState_MachineType();

		/**
		 * The meta object literal for the '<em><b>Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__FUNCTIONS = eINSTANCE.getState_Functions();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__VARIABLES = eINSTANCE.getState_Variables();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__TYPE = eINSTANCE.getState_Type();

		/**
		 * The meta object literal for the '<em><b>During Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__DURING_ACTIONS = eINSTANCE.getState_DuringActions();

		/**
		 * The meta object literal for the '<em><b>Entry Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__ENTRY_ACTIONS = eINSTANCE.getState_EntryActions();

		/**
		 * The meta object literal for the '<em><b>Exit Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__EXIT_ACTIONS = eINSTANCE.getState_ExitActions();

		/**
		 * The meta object literal for the '<em><b>Outer Transition List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__OUTER_TRANSITION_LIST = eINSTANCE.getState_OuterTransitionList();

		/**
		 * The meta object literal for the '<em><b>Inner Transition List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__INNER_TRANSITION_LIST = eINSTANCE.getState_InnerTransitionList();

		/**
		 * The meta object literal for the '<em><b>Composition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__COMPOSITION = eINSTANCE.getState_Composition();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__EVENTS = eINSTANCE.getState_Events();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.JunctionType <em>Junction Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.JunctionType
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getJunctionType()
		 * @generated
		 */
		EEnum JUNCTION_TYPE = eINSTANCE.getJunctionType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.EventScope <em>Event Scope</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.EventScope
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getEventScope()
		 * @generated
		 */
		EEnum EVENT_SCOPE = eINSTANCE.getEventScope();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType <em>Transition Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getTransitionType()
		 * @generated
		 */
		EEnum TRANSITION_TYPE = eINSTANCE.getTransitionType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType <em>State Machine Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getStateMachineType()
		 * @generated
		 */
		EEnum STATE_MACHINE_TYPE = eINSTANCE.getStateMachineType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.StateType <em>State Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateType
		 * @see geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl#getStateType()
		 * @generated
		 */
		EEnum STATE_TYPE = eINSTANCE.getStateType();

	}

} //GastatemodelPackage
