/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel;

import geneauto.emf.models.common.NameSpaceElement;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.gasystemmodel.common.Node;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Event object.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getEventCode <em>Event Code</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getTriggerType <em>Trigger Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getScope <em>Scope</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getOutControlPort <em>Out Control Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getReferenceExpression <em>Reference Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getBinder <em>Binder</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends GASystemModelElement, NameSpaceElement, Node {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Event Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Code</em>' attribute.
	 * @see #isSetEventCode()
	 * @see #unsetEventCode()
	 * @see #setEventCode(int)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getEvent_EventCode()
	 * @model unsettable="true"
	 * @generated
	 */
	int getEventCode();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getEventCode <em>Event Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Code</em>' attribute.
	 * @see #isSetEventCode()
	 * @see #unsetEventCode()
	 * @see #getEventCode()
	 * @generated
	 */
	void setEventCode(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getEventCode <em>Event Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEventCode()
	 * @see #getEventCode()
	 * @see #setEventCode(int)
	 * @generated
	 */
	void unsetEventCode();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getEventCode <em>Event Code</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Event Code</em>' attribute is set.
	 * @see #unsetEventCode()
	 * @see #getEventCode()
	 * @see #setEventCode(int)
	 * @generated
	 */
	boolean isSetEventCode();

	/**
	 * Returns the value of the '<em><b>Trigger Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger Type</em>' attribute.
	 * @see #isSetTriggerType()
	 * @see #unsetTriggerType()
	 * @see #setTriggerType(String)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getEvent_TriggerType()
	 * @model unsettable="true"
	 * @generated
	 */
	String getTriggerType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getTriggerType <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger Type</em>' attribute.
	 * @see #isSetTriggerType()
	 * @see #unsetTriggerType()
	 * @see #getTriggerType()
	 * @generated
	 */
	void setTriggerType(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getTriggerType <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTriggerType()
	 * @see #getTriggerType()
	 * @see #setTriggerType(String)
	 * @generated
	 */
	void unsetTriggerType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getTriggerType <em>Trigger Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Trigger Type</em>' attribute is set.
	 * @see #unsetTriggerType()
	 * @see #getTriggerType()
	 * @see #setTriggerType(String)
	 * @generated
	 */
	boolean isSetTriggerType();

	/**
	 * Returns the value of the '<em><b>Scope</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gasystemmodel.gastatemodel.EventScope}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.EventScope
	 * @see #isSetScope()
	 * @see #unsetScope()
	 * @see #setScope(EventScope)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getEvent_Scope()
	 * @model unsettable="true"
	 * @generated
	 */
	EventScope getScope();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.EventScope
	 * @see #isSetScope()
	 * @see #unsetScope()
	 * @see #getScope()
	 * @generated
	 */
	void setScope(EventScope value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetScope()
	 * @see #getScope()
	 * @see #setScope(EventScope)
	 * @generated
	 */
	void unsetScope();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getScope <em>Scope</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Scope</em>' attribute is set.
	 * @see #unsetScope()
	 * @see #getScope()
	 * @see #setScope(EventScope)
	 * @generated
	 */
	boolean isSetScope();

	/**
	 * Returns the value of the '<em><b>Out Control Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Control Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Control Port</em>' reference.
	 * @see #isSetOutControlPort()
	 * @see #unsetOutControlPort()
	 * @see #setOutControlPort(OutControlPort)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getEvent_OutControlPort()
	 * @model unsettable="true"
	 * @generated
	 */
	OutControlPort getOutControlPort();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getOutControlPort <em>Out Control Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Out Control Port</em>' reference.
	 * @see #isSetOutControlPort()
	 * @see #unsetOutControlPort()
	 * @see #getOutControlPort()
	 * @generated
	 */
	void setOutControlPort(OutControlPort value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getOutControlPort <em>Out Control Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOutControlPort()
	 * @see #getOutControlPort()
	 * @see #setOutControlPort(OutControlPort)
	 * @generated
	 */
	void unsetOutControlPort();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getOutControlPort <em>Out Control Port</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Out Control Port</em>' reference is set.
	 * @see #unsetOutControlPort()
	 * @see #getOutControlPort()
	 * @see #setOutControlPort(OutControlPort)
	 * @generated
	 */
	boolean isSetOutControlPort();

	/**
	 * Returns the value of the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Expression</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Expression</em>' reference.
	 * @see #isSetReferenceExpression()
	 * @see #unsetReferenceExpression()
	 * @see #setReferenceExpression(Expression)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getEvent_ReferenceExpression()
	 * @model unsettable="true"
	 * @generated
	 */
	Expression getReferenceExpression();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getReferenceExpression <em>Reference Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Expression</em>' reference.
	 * @see #isSetReferenceExpression()
	 * @see #unsetReferenceExpression()
	 * @see #getReferenceExpression()
	 * @generated
	 */
	void setReferenceExpression(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getReferenceExpression <em>Reference Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetReferenceExpression()
	 * @see #getReferenceExpression()
	 * @see #setReferenceExpression(Expression)
	 * @generated
	 */
	void unsetReferenceExpression();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getReferenceExpression <em>Reference Expression</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Reference Expression</em>' reference is set.
	 * @see #unsetReferenceExpression()
	 * @see #getReferenceExpression()
	 * @see #setReferenceExpression(Expression)
	 * @generated
	 */
	boolean isSetReferenceExpression();

	/**
	 * Returns the value of the '<em><b>Binder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binder</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binder</em>' reference.
	 * @see #isSetBinder()
	 * @see #unsetBinder()
	 * @see #setBinder(State)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getEvent_Binder()
	 * @model unsettable="true"
	 * @generated
	 */
	State getBinder();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getBinder <em>Binder</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binder</em>' reference.
	 * @see #isSetBinder()
	 * @see #unsetBinder()
	 * @see #getBinder()
	 * @generated
	 */
	void setBinder(State value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getBinder <em>Binder</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBinder()
	 * @see #getBinder()
	 * @see #setBinder(State)
	 * @generated
	 */
	void unsetBinder();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Event#getBinder <em>Binder</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Binder</em>' reference is set.
	 * @see #unsetBinder()
	 * @see #getBinder()
	 * @see #setBinder(State)
	 * @generated
	 */
	boolean isSetBinder();

} // Event
