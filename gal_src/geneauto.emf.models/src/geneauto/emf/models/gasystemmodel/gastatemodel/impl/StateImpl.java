/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel.impl;

import geneauto.emf.models.gasystemmodel.common.Function_SM;
import geneauto.emf.models.gasystemmodel.common.Variable_SM;

import geneauto.emf.models.gasystemmodel.gastatemodel.Action;
import geneauto.emf.models.gasystemmodel.gastatemodel.Composition;
import geneauto.emf.models.gasystemmodel.gastatemodel.Event;
import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;
import geneauto.emf.models.gasystemmodel.gastatemodel.SimpleAction;
import geneauto.emf.models.gasystemmodel.gastatemodel.State;
import geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType;
import geneauto.emf.models.gasystemmodel.gastatemodel.StateType;
import geneauto.emf.models.gasystemmodel.gastatemodel.TransitionList;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getExecutionOrder <em>Execution Order</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getMachineType <em>Machine Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getFunctions <em>Functions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getDuringActions <em>During Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getEntryActions <em>Entry Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getExitActions <em>Exit Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getOuterTransitionList <em>Outer Transition List</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getInnerTransitionList <em>Inner Transition List</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getComposition <em>Composition</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.StateImpl#getEvents <em>Events</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StateImpl extends LocationImpl implements State {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getExecutionOrder() <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionOrder()
	 * @generated
	 * @ordered
	 */
	protected static final int EXECUTION_ORDER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExecutionOrder() <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionOrder()
	 * @generated
	 * @ordered
	 */
	protected int executionOrder = EXECUTION_ORDER_EDEFAULT;

	/**
	 * This is true if the Execution Order attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean executionOrderESet;

	/**
	 * The default value of the '{@link #getMachineType() <em>Machine Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineType()
	 * @generated
	 * @ordered
	 */
	protected static final StateMachineType MACHINE_TYPE_EDEFAULT = StateMachineType.CLASSIC;

	/**
	 * The cached value of the '{@link #getMachineType() <em>Machine Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineType()
	 * @generated
	 * @ordered
	 */
	protected StateMachineType machineType = MACHINE_TYPE_EDEFAULT;

	/**
	 * This is true if the Machine Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean machineTypeESet;

	/**
	 * The cached value of the '{@link #getFunctions() <em>Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Function_SM> functions;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable_SM> variables;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final StateType TYPE_EDEFAULT = StateType.AND_STATE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected StateType type = TYPE_EDEFAULT;

	/**
	 * This is true if the Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * The cached value of the '{@link #getDuringActions() <em>During Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuringActions()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> duringActions;

	/**
	 * The cached value of the '{@link #getEntryActions() <em>Entry Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntryActions()
	 * @generated
	 * @ordered
	 */
	protected EList<SimpleAction> entryActions;

	/**
	 * The cached value of the '{@link #getExitActions() <em>Exit Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExitActions()
	 * @generated
	 * @ordered
	 */
	protected EList<SimpleAction> exitActions;

	/**
	 * The cached value of the '{@link #getOuterTransitionList() <em>Outer Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterTransitionList()
	 * @generated
	 * @ordered
	 */
	protected TransitionList outerTransitionList;

	/**
	 * This is true if the Outer Transition List containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean outerTransitionListESet;

	/**
	 * The cached value of the '{@link #getInnerTransitionList() <em>Inner Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerTransitionList()
	 * @generated
	 * @ordered
	 */
	protected TransitionList innerTransitionList;

	/**
	 * This is true if the Inner Transition List containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean innerTransitionListESet;

	/**
	 * The cached value of the '{@link #getComposition() <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComposition()
	 * @generated
	 * @ordered
	 */
	protected Composition composition;

	/**
	 * This is true if the Composition containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean compositionESet;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GastatemodelPackage.Literals.STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getExecutionOrder() {
		return executionOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionOrder(int newExecutionOrder) {
		int oldExecutionOrder = executionOrder;
		executionOrder = newExecutionOrder;
		boolean oldExecutionOrderESet = executionOrderESet;
		executionOrderESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__EXECUTION_ORDER, oldExecutionOrder, executionOrder, !oldExecutionOrderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExecutionOrder() {
		int oldExecutionOrder = executionOrder;
		boolean oldExecutionOrderESet = executionOrderESet;
		executionOrder = EXECUTION_ORDER_EDEFAULT;
		executionOrderESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__EXECUTION_ORDER, oldExecutionOrder, EXECUTION_ORDER_EDEFAULT, oldExecutionOrderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExecutionOrder() {
		return executionOrderESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachineType getMachineType() {
		return machineType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineType(StateMachineType newMachineType) {
		StateMachineType oldMachineType = machineType;
		machineType = newMachineType == null ? MACHINE_TYPE_EDEFAULT : newMachineType;
		boolean oldMachineTypeESet = machineTypeESet;
		machineTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__MACHINE_TYPE, oldMachineType, machineType, !oldMachineTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMachineType() {
		StateMachineType oldMachineType = machineType;
		boolean oldMachineTypeESet = machineTypeESet;
		machineType = MACHINE_TYPE_EDEFAULT;
		machineTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__MACHINE_TYPE, oldMachineType, MACHINE_TYPE_EDEFAULT, oldMachineTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMachineType() {
		return machineTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Function_SM> getFunctions() {
		if (functions == null) {
			functions = new EObjectContainmentEList.Unsettable<Function_SM>(Function_SM.class, this, GastatemodelPackage.STATE__FUNCTIONS);
		}
		return functions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFunctions() {
		if (functions != null) ((InternalEList.Unsettable<?>)functions).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFunctions() {
		return functions != null && ((InternalEList.Unsettable<?>)functions).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variable_SM> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList.Unsettable<Variable_SM>(Variable_SM.class, this, GastatemodelPackage.STATE__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVariables() {
		if (variables != null) ((InternalEList.Unsettable<?>)variables).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVariables() {
		return variables != null && ((InternalEList.Unsettable<?>)variables).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(StateType newType) {
		StateType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__TYPE, oldType, type, !oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetType() {
		StateType oldType = type;
		boolean oldTypeESet = typeESet;
		type = TYPE_EDEFAULT;
		typeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__TYPE, oldType, TYPE_EDEFAULT, oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Action> getDuringActions() {
		if (duringActions == null) {
			duringActions = new EObjectContainmentEList.Unsettable<Action>(Action.class, this, GastatemodelPackage.STATE__DURING_ACTIONS);
		}
		return duringActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDuringActions() {
		if (duringActions != null) ((InternalEList.Unsettable<?>)duringActions).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDuringActions() {
		return duringActions != null && ((InternalEList.Unsettable<?>)duringActions).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimpleAction> getEntryActions() {
		if (entryActions == null) {
			entryActions = new EObjectContainmentEList.Unsettable<SimpleAction>(SimpleAction.class, this, GastatemodelPackage.STATE__ENTRY_ACTIONS);
		}
		return entryActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEntryActions() {
		if (entryActions != null) ((InternalEList.Unsettable<?>)entryActions).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEntryActions() {
		return entryActions != null && ((InternalEList.Unsettable<?>)entryActions).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimpleAction> getExitActions() {
		if (exitActions == null) {
			exitActions = new EObjectContainmentEList.Unsettable<SimpleAction>(SimpleAction.class, this, GastatemodelPackage.STATE__EXIT_ACTIONS);
		}
		return exitActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExitActions() {
		if (exitActions != null) ((InternalEList.Unsettable<?>)exitActions).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExitActions() {
		return exitActions != null && ((InternalEList.Unsettable<?>)exitActions).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionList getOuterTransitionList() {
		return outerTransitionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOuterTransitionList(TransitionList newOuterTransitionList, NotificationChain msgs) {
		TransitionList oldOuterTransitionList = outerTransitionList;
		outerTransitionList = newOuterTransitionList;
		boolean oldOuterTransitionListESet = outerTransitionListESet;
		outerTransitionListESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__OUTER_TRANSITION_LIST, oldOuterTransitionList, newOuterTransitionList, !oldOuterTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOuterTransitionList(TransitionList newOuterTransitionList) {
		if (newOuterTransitionList != outerTransitionList) {
			NotificationChain msgs = null;
			if (outerTransitionList != null)
				msgs = ((InternalEObject)outerTransitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__OUTER_TRANSITION_LIST, null, msgs);
			if (newOuterTransitionList != null)
				msgs = ((InternalEObject)newOuterTransitionList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__OUTER_TRANSITION_LIST, null, msgs);
			msgs = basicSetOuterTransitionList(newOuterTransitionList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldOuterTransitionListESet = outerTransitionListESet;
			outerTransitionListESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__OUTER_TRANSITION_LIST, newOuterTransitionList, newOuterTransitionList, !oldOuterTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetOuterTransitionList(NotificationChain msgs) {
		TransitionList oldOuterTransitionList = outerTransitionList;
		outerTransitionList = null;
		boolean oldOuterTransitionListESet = outerTransitionListESet;
		outerTransitionListESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__OUTER_TRANSITION_LIST, oldOuterTransitionList, null, oldOuterTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOuterTransitionList() {
		if (outerTransitionList != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)outerTransitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__OUTER_TRANSITION_LIST, null, msgs);
			msgs = basicUnsetOuterTransitionList(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldOuterTransitionListESet = outerTransitionListESet;
			outerTransitionListESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__OUTER_TRANSITION_LIST, null, null, oldOuterTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOuterTransitionList() {
		return outerTransitionListESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionList getInnerTransitionList() {
		return innerTransitionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInnerTransitionList(TransitionList newInnerTransitionList, NotificationChain msgs) {
		TransitionList oldInnerTransitionList = innerTransitionList;
		innerTransitionList = newInnerTransitionList;
		boolean oldInnerTransitionListESet = innerTransitionListESet;
		innerTransitionListESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__INNER_TRANSITION_LIST, oldInnerTransitionList, newInnerTransitionList, !oldInnerTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnerTransitionList(TransitionList newInnerTransitionList) {
		if (newInnerTransitionList != innerTransitionList) {
			NotificationChain msgs = null;
			if (innerTransitionList != null)
				msgs = ((InternalEObject)innerTransitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__INNER_TRANSITION_LIST, null, msgs);
			if (newInnerTransitionList != null)
				msgs = ((InternalEObject)newInnerTransitionList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__INNER_TRANSITION_LIST, null, msgs);
			msgs = basicSetInnerTransitionList(newInnerTransitionList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInnerTransitionListESet = innerTransitionListESet;
			innerTransitionListESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__INNER_TRANSITION_LIST, newInnerTransitionList, newInnerTransitionList, !oldInnerTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInnerTransitionList(NotificationChain msgs) {
		TransitionList oldInnerTransitionList = innerTransitionList;
		innerTransitionList = null;
		boolean oldInnerTransitionListESet = innerTransitionListESet;
		innerTransitionListESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__INNER_TRANSITION_LIST, oldInnerTransitionList, null, oldInnerTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInnerTransitionList() {
		if (innerTransitionList != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)innerTransitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__INNER_TRANSITION_LIST, null, msgs);
			msgs = basicUnsetInnerTransitionList(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInnerTransitionListESet = innerTransitionListESet;
			innerTransitionListESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__INNER_TRANSITION_LIST, null, null, oldInnerTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInnerTransitionList() {
		return innerTransitionListESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composition getComposition() {
		return composition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComposition(Composition newComposition, NotificationChain msgs) {
		Composition oldComposition = composition;
		composition = newComposition;
		boolean oldCompositionESet = compositionESet;
		compositionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__COMPOSITION, oldComposition, newComposition, !oldCompositionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComposition(Composition newComposition) {
		if (newComposition != composition) {
			NotificationChain msgs = null;
			if (composition != null)
				msgs = ((InternalEObject)composition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__COMPOSITION, null, msgs);
			if (newComposition != null)
				msgs = ((InternalEObject)newComposition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__COMPOSITION, null, msgs);
			msgs = basicSetComposition(newComposition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldCompositionESet = compositionESet;
			compositionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.STATE__COMPOSITION, newComposition, newComposition, !oldCompositionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetComposition(NotificationChain msgs) {
		Composition oldComposition = composition;
		composition = null;
		boolean oldCompositionESet = compositionESet;
		compositionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__COMPOSITION, oldComposition, null, oldCompositionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetComposition() {
		if (composition != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)composition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.STATE__COMPOSITION, null, msgs);
			msgs = basicUnsetComposition(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldCompositionESet = compositionESet;
			compositionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.STATE__COMPOSITION, null, null, oldCompositionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetComposition() {
		return compositionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList.Unsettable<Event>(Event.class, this, GastatemodelPackage.STATE__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEvents() {
		if (events != null) ((InternalEList.Unsettable<?>)events).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEvents() {
		return events != null && ((InternalEList.Unsettable<?>)events).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GastatemodelPackage.STATE__FUNCTIONS:
				return ((InternalEList<?>)getFunctions()).basicRemove(otherEnd, msgs);
			case GastatemodelPackage.STATE__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case GastatemodelPackage.STATE__DURING_ACTIONS:
				return ((InternalEList<?>)getDuringActions()).basicRemove(otherEnd, msgs);
			case GastatemodelPackage.STATE__ENTRY_ACTIONS:
				return ((InternalEList<?>)getEntryActions()).basicRemove(otherEnd, msgs);
			case GastatemodelPackage.STATE__EXIT_ACTIONS:
				return ((InternalEList<?>)getExitActions()).basicRemove(otherEnd, msgs);
			case GastatemodelPackage.STATE__OUTER_TRANSITION_LIST:
				return basicUnsetOuterTransitionList(msgs);
			case GastatemodelPackage.STATE__INNER_TRANSITION_LIST:
				return basicUnsetInnerTransitionList(msgs);
			case GastatemodelPackage.STATE__COMPOSITION:
				return basicUnsetComposition(msgs);
			case GastatemodelPackage.STATE__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GastatemodelPackage.STATE__EXECUTION_ORDER:
				return getExecutionOrder();
			case GastatemodelPackage.STATE__MACHINE_TYPE:
				return getMachineType();
			case GastatemodelPackage.STATE__FUNCTIONS:
				return getFunctions();
			case GastatemodelPackage.STATE__VARIABLES:
				return getVariables();
			case GastatemodelPackage.STATE__TYPE:
				return getType();
			case GastatemodelPackage.STATE__DURING_ACTIONS:
				return getDuringActions();
			case GastatemodelPackage.STATE__ENTRY_ACTIONS:
				return getEntryActions();
			case GastatemodelPackage.STATE__EXIT_ACTIONS:
				return getExitActions();
			case GastatemodelPackage.STATE__OUTER_TRANSITION_LIST:
				return getOuterTransitionList();
			case GastatemodelPackage.STATE__INNER_TRANSITION_LIST:
				return getInnerTransitionList();
			case GastatemodelPackage.STATE__COMPOSITION:
				return getComposition();
			case GastatemodelPackage.STATE__EVENTS:
				return getEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GastatemodelPackage.STATE__EXECUTION_ORDER:
				setExecutionOrder((Integer)newValue);
				return;
			case GastatemodelPackage.STATE__MACHINE_TYPE:
				setMachineType((StateMachineType)newValue);
				return;
			case GastatemodelPackage.STATE__FUNCTIONS:
				getFunctions().clear();
				getFunctions().addAll((Collection<? extends Function_SM>)newValue);
				return;
			case GastatemodelPackage.STATE__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends Variable_SM>)newValue);
				return;
			case GastatemodelPackage.STATE__TYPE:
				setType((StateType)newValue);
				return;
			case GastatemodelPackage.STATE__DURING_ACTIONS:
				getDuringActions().clear();
				getDuringActions().addAll((Collection<? extends Action>)newValue);
				return;
			case GastatemodelPackage.STATE__ENTRY_ACTIONS:
				getEntryActions().clear();
				getEntryActions().addAll((Collection<? extends SimpleAction>)newValue);
				return;
			case GastatemodelPackage.STATE__EXIT_ACTIONS:
				getExitActions().clear();
				getExitActions().addAll((Collection<? extends SimpleAction>)newValue);
				return;
			case GastatemodelPackage.STATE__OUTER_TRANSITION_LIST:
				setOuterTransitionList((TransitionList)newValue);
				return;
			case GastatemodelPackage.STATE__INNER_TRANSITION_LIST:
				setInnerTransitionList((TransitionList)newValue);
				return;
			case GastatemodelPackage.STATE__COMPOSITION:
				setComposition((Composition)newValue);
				return;
			case GastatemodelPackage.STATE__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.STATE__EXECUTION_ORDER:
				unsetExecutionOrder();
				return;
			case GastatemodelPackage.STATE__MACHINE_TYPE:
				unsetMachineType();
				return;
			case GastatemodelPackage.STATE__FUNCTIONS:
				unsetFunctions();
				return;
			case GastatemodelPackage.STATE__VARIABLES:
				unsetVariables();
				return;
			case GastatemodelPackage.STATE__TYPE:
				unsetType();
				return;
			case GastatemodelPackage.STATE__DURING_ACTIONS:
				unsetDuringActions();
				return;
			case GastatemodelPackage.STATE__ENTRY_ACTIONS:
				unsetEntryActions();
				return;
			case GastatemodelPackage.STATE__EXIT_ACTIONS:
				unsetExitActions();
				return;
			case GastatemodelPackage.STATE__OUTER_TRANSITION_LIST:
				unsetOuterTransitionList();
				return;
			case GastatemodelPackage.STATE__INNER_TRANSITION_LIST:
				unsetInnerTransitionList();
				return;
			case GastatemodelPackage.STATE__COMPOSITION:
				unsetComposition();
				return;
			case GastatemodelPackage.STATE__EVENTS:
				unsetEvents();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.STATE__EXECUTION_ORDER:
				return isSetExecutionOrder();
			case GastatemodelPackage.STATE__MACHINE_TYPE:
				return isSetMachineType();
			case GastatemodelPackage.STATE__FUNCTIONS:
				return isSetFunctions();
			case GastatemodelPackage.STATE__VARIABLES:
				return isSetVariables();
			case GastatemodelPackage.STATE__TYPE:
				return isSetType();
			case GastatemodelPackage.STATE__DURING_ACTIONS:
				return isSetDuringActions();
			case GastatemodelPackage.STATE__ENTRY_ACTIONS:
				return isSetEntryActions();
			case GastatemodelPackage.STATE__EXIT_ACTIONS:
				return isSetExitActions();
			case GastatemodelPackage.STATE__OUTER_TRANSITION_LIST:
				return isSetOuterTransitionList();
			case GastatemodelPackage.STATE__INNER_TRANSITION_LIST:
				return isSetInnerTransitionList();
			case GastatemodelPackage.STATE__COMPOSITION:
				return isSetComposition();
			case GastatemodelPackage.STATE__EVENTS:
				return isSetEvents();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executionOrder: ");
		if (executionOrderESet) result.append(executionOrder); else result.append("<unset>");
		result.append(", machineType: ");
		if (machineTypeESet) result.append(machineType); else result.append("<unset>");
		result.append(", type: ");
		if (typeESet) result.append(type); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //StateImpl
