/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.gasystemmodel.common.ContainerNode;
import geneauto.emf.models.gasystemmodel.common.Function_SM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Box</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Box is used for grouping sate chart elements.  Box represents the Stateflow's box object with a following restriction: the box and its children can only contain graphical functions, truth tables, etc. but not states or flow-graph sections. I.e. it cannot be a part of the chart's main part and is only a place for grouping functions.  Justification: Using boxes to group also states or flow-graph networks creates strange and complex scoping. The logical scoping for executing the chart sections and the name scopes introduced by boxes do not match. Boxes are fine for grouping function objects (no interference with the main chart)..
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getFunctions <em>Functions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getBoxes <em>Boxes</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getBox()
 * @model
 * @generated
 */
public interface Box extends GASystemModelElement, ContainerNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Functions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.Function_SM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functions</em>' containment reference list.
	 * @see #isSetFunctions()
	 * @see #unsetFunctions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getBox_Functions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Function_SM> getFunctions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getFunctions <em>Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFunctions()
	 * @see #getFunctions()
	 * @generated
	 */
	void unsetFunctions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getFunctions <em>Functions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Functions</em>' containment reference list is set.
	 * @see #unsetFunctions()
	 * @see #getFunctions()
	 * @generated
	 */
	boolean isSetFunctions();

	/**
	 * Returns the value of the '<em><b>Boxes</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.Box}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Boxes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Boxes</em>' containment reference list.
	 * @see #isSetBoxes()
	 * @see #unsetBoxes()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getBox_Boxes()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Box> getBoxes();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getBoxes <em>Boxes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBoxes()
	 * @see #getBoxes()
	 * @generated
	 */
	void unsetBoxes();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Box#getBoxes <em>Boxes</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Boxes</em>' containment reference list is set.
	 * @see #unsetBoxes()
	 * @see #getBoxes()
	 * @generated
	 */
	boolean isSetBoxes();

} // Box
