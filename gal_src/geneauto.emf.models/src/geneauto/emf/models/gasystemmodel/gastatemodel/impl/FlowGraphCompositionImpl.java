/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel.impl;

import geneauto.emf.models.gasystemmodel.gastatemodel.FlowGraphComposition;
import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;
import geneauto.emf.models.gasystemmodel.gastatemodel.Junction;
import geneauto.emf.models.gasystemmodel.gastatemodel.TransitionList;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flow Graph Composition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.FlowGraphCompositionImpl#getJunctions <em>Junctions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.FlowGraphCompositionImpl#getDefaultTransitionList <em>Default Transition List</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FlowGraphCompositionImpl extends CompositionImpl implements FlowGraphComposition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getJunctions() <em>Junctions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Junction> junctions;

	/**
	 * The cached value of the '{@link #getDefaultTransitionList() <em>Default Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultTransitionList()
	 * @generated
	 * @ordered
	 */
	protected TransitionList defaultTransitionList;

	/**
	 * This is true if the Default Transition List containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean defaultTransitionListESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FlowGraphCompositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GastatemodelPackage.Literals.FLOW_GRAPH_COMPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Junction> getJunctions() {
		if (junctions == null) {
			junctions = new EObjectContainmentEList.Unsettable<Junction>(Junction.class, this, GastatemodelPackage.FLOW_GRAPH_COMPOSITION__JUNCTIONS);
		}
		return junctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetJunctions() {
		if (junctions != null) ((InternalEList.Unsettable<?>)junctions).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetJunctions() {
		return junctions != null && ((InternalEList.Unsettable<?>)junctions).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionList getDefaultTransitionList() {
		return defaultTransitionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultTransitionList(TransitionList newDefaultTransitionList, NotificationChain msgs) {
		TransitionList oldDefaultTransitionList = defaultTransitionList;
		defaultTransitionList = newDefaultTransitionList;
		boolean oldDefaultTransitionListESet = defaultTransitionListESet;
		defaultTransitionListESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST, oldDefaultTransitionList, newDefaultTransitionList, !oldDefaultTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultTransitionList(TransitionList newDefaultTransitionList) {
		if (newDefaultTransitionList != defaultTransitionList) {
			NotificationChain msgs = null;
			if (defaultTransitionList != null)
				msgs = ((InternalEObject)defaultTransitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST, null, msgs);
			if (newDefaultTransitionList != null)
				msgs = ((InternalEObject)newDefaultTransitionList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST, null, msgs);
			msgs = basicSetDefaultTransitionList(newDefaultTransitionList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDefaultTransitionListESet = defaultTransitionListESet;
			defaultTransitionListESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST, newDefaultTransitionList, newDefaultTransitionList, !oldDefaultTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDefaultTransitionList(NotificationChain msgs) {
		TransitionList oldDefaultTransitionList = defaultTransitionList;
		defaultTransitionList = null;
		boolean oldDefaultTransitionListESet = defaultTransitionListESet;
		defaultTransitionListESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST, oldDefaultTransitionList, null, oldDefaultTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDefaultTransitionList() {
		if (defaultTransitionList != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)defaultTransitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST, null, msgs);
			msgs = basicUnsetDefaultTransitionList(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDefaultTransitionListESet = defaultTransitionListESet;
			defaultTransitionListESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST, null, null, oldDefaultTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDefaultTransitionList() {
		return defaultTransitionListESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__JUNCTIONS:
				return ((InternalEList<?>)getJunctions()).basicRemove(otherEnd, msgs);
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST:
				return basicUnsetDefaultTransitionList(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__JUNCTIONS:
				return getJunctions();
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST:
				return getDefaultTransitionList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__JUNCTIONS:
				getJunctions().clear();
				getJunctions().addAll((Collection<? extends Junction>)newValue);
				return;
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST:
				setDefaultTransitionList((TransitionList)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__JUNCTIONS:
				unsetJunctions();
				return;
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST:
				unsetDefaultTransitionList();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__JUNCTIONS:
				return isSetJunctions();
			case GastatemodelPackage.FLOW_GRAPH_COMPOSITION__DEFAULT_TRANSITION_LIST:
				return isSetDefaultTransitionList();
		}
		return super.eIsSet(featureID);
	}

} //FlowGraphCompositionImpl
