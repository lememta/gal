/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel.impl;

import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;
import geneauto.emf.models.gasystemmodel.gastatemodel.Junction;
import geneauto.emf.models.gasystemmodel.gastatemodel.JunctionType;
import geneauto.emf.models.gasystemmodel.gastatemodel.TransitionList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Junction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.JunctionImpl#getJunctionType <em>Junction Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.JunctionImpl#getTransitionList <em>Transition List</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class JunctionImpl extends LocationImpl implements Junction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getJunctionType() <em>Junction Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJunctionType()
	 * @generated
	 * @ordered
	 */
	protected static final JunctionType JUNCTION_TYPE_EDEFAULT = JunctionType.CONNECTIVE_JUNCTION;

	/**
	 * The cached value of the '{@link #getJunctionType() <em>Junction Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJunctionType()
	 * @generated
	 * @ordered
	 */
	protected JunctionType junctionType = JUNCTION_TYPE_EDEFAULT;

	/**
	 * This is true if the Junction Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean junctionTypeESet;

	/**
	 * The cached value of the '{@link #getTransitionList() <em>Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionList()
	 * @generated
	 * @ordered
	 */
	protected TransitionList transitionList;

	/**
	 * This is true if the Transition List containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean transitionListESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GastatemodelPackage.Literals.JUNCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JunctionType getJunctionType() {
		return junctionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJunctionType(JunctionType newJunctionType) {
		JunctionType oldJunctionType = junctionType;
		junctionType = newJunctionType == null ? JUNCTION_TYPE_EDEFAULT : newJunctionType;
		boolean oldJunctionTypeESet = junctionTypeESet;
		junctionTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.JUNCTION__JUNCTION_TYPE, oldJunctionType, junctionType, !oldJunctionTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetJunctionType() {
		JunctionType oldJunctionType = junctionType;
		boolean oldJunctionTypeESet = junctionTypeESet;
		junctionType = JUNCTION_TYPE_EDEFAULT;
		junctionTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.JUNCTION__JUNCTION_TYPE, oldJunctionType, JUNCTION_TYPE_EDEFAULT, oldJunctionTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetJunctionType() {
		return junctionTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionList getTransitionList() {
		return transitionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransitionList(TransitionList newTransitionList, NotificationChain msgs) {
		TransitionList oldTransitionList = transitionList;
		transitionList = newTransitionList;
		boolean oldTransitionListESet = transitionListESet;
		transitionListESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GastatemodelPackage.JUNCTION__TRANSITION_LIST, oldTransitionList, newTransitionList, !oldTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransitionList(TransitionList newTransitionList) {
		if (newTransitionList != transitionList) {
			NotificationChain msgs = null;
			if (transitionList != null)
				msgs = ((InternalEObject)transitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.JUNCTION__TRANSITION_LIST, null, msgs);
			if (newTransitionList != null)
				msgs = ((InternalEObject)newTransitionList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.JUNCTION__TRANSITION_LIST, null, msgs);
			msgs = basicSetTransitionList(newTransitionList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldTransitionListESet = transitionListESet;
			transitionListESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.JUNCTION__TRANSITION_LIST, newTransitionList, newTransitionList, !oldTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetTransitionList(NotificationChain msgs) {
		TransitionList oldTransitionList = transitionList;
		transitionList = null;
		boolean oldTransitionListESet = transitionListESet;
		transitionListESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.JUNCTION__TRANSITION_LIST, oldTransitionList, null, oldTransitionListESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTransitionList() {
		if (transitionList != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)transitionList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GastatemodelPackage.JUNCTION__TRANSITION_LIST, null, msgs);
			msgs = basicUnsetTransitionList(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldTransitionListESet = transitionListESet;
			transitionListESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.JUNCTION__TRANSITION_LIST, null, null, oldTransitionListESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTransitionList() {
		return transitionListESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GastatemodelPackage.JUNCTION__TRANSITION_LIST:
				return basicUnsetTransitionList(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GastatemodelPackage.JUNCTION__JUNCTION_TYPE:
				return getJunctionType();
			case GastatemodelPackage.JUNCTION__TRANSITION_LIST:
				return getTransitionList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GastatemodelPackage.JUNCTION__JUNCTION_TYPE:
				setJunctionType((JunctionType)newValue);
				return;
			case GastatemodelPackage.JUNCTION__TRANSITION_LIST:
				setTransitionList((TransitionList)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.JUNCTION__JUNCTION_TYPE:
				unsetJunctionType();
				return;
			case GastatemodelPackage.JUNCTION__TRANSITION_LIST:
				unsetTransitionList();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.JUNCTION__JUNCTION_TYPE:
				return isSetJunctionType();
			case GastatemodelPackage.JUNCTION__TRANSITION_LIST:
				return isSetTransitionList();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (junctionType: ");
		if (junctionTypeESet) result.append(junctionType); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //JunctionImpl
