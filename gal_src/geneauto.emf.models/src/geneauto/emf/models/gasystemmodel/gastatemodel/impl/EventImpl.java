/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel.impl;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;

import geneauto.emf.models.gasystemmodel.gastatemodel.Event;
import geneauto.emf.models.gasystemmodel.gastatemodel.EventScope;
import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;
import geneauto.emf.models.gasystemmodel.gastatemodel.State;

import geneauto.emf.models.gasystemmodel.impl.GASystemModelElementImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl#getEventCode <em>Event Code</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl#getTriggerType <em>Trigger Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl#getOutControlPort <em>Out Control Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl#getReferenceExpression <em>Reference Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.impl.EventImpl#getBinder <em>Binder</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EventImpl extends GASystemModelElementImpl implements Event {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getEventCode() <em>Event Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventCode()
	 * @generated
	 * @ordered
	 */
	protected static final int EVENT_CODE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEventCode() <em>Event Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventCode()
	 * @generated
	 * @ordered
	 */
	protected int eventCode = EVENT_CODE_EDEFAULT;

	/**
	 * This is true if the Event Code attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean eventCodeESet;

	/**
	 * The default value of the '{@link #getTriggerType() <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerType()
	 * @generated
	 * @ordered
	 */
	protected static final String TRIGGER_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTriggerType() <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerType()
	 * @generated
	 * @ordered
	 */
	protected String triggerType = TRIGGER_TYPE_EDEFAULT;

	/**
	 * This is true if the Trigger Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean triggerTypeESet;

	/**
	 * The default value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected static final EventScope SCOPE_EDEFAULT = EventScope.INPUT_EVENT;

	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected EventScope scope = SCOPE_EDEFAULT;

	/**
	 * This is true if the Scope attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean scopeESet;

	/**
	 * The cached value of the '{@link #getOutControlPort() <em>Out Control Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutControlPort()
	 * @generated
	 * @ordered
	 */
	protected OutControlPort outControlPort;

	/**
	 * This is true if the Out Control Port reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean outControlPortESet;

	/**
	 * The cached value of the '{@link #getReferenceExpression() <em>Reference Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression referenceExpression;

	/**
	 * This is true if the Reference Expression reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean referenceExpressionESet;

	/**
	 * The cached value of the '{@link #getBinder() <em>Binder</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinder()
	 * @generated
	 * @ordered
	 */
	protected State binder;

	/**
	 * This is true if the Binder reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean binderESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GastatemodelPackage.Literals.EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEventCode() {
		return eventCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventCode(int newEventCode) {
		int oldEventCode = eventCode;
		eventCode = newEventCode;
		boolean oldEventCodeESet = eventCodeESet;
		eventCodeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.EVENT__EVENT_CODE, oldEventCode, eventCode, !oldEventCodeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEventCode() {
		int oldEventCode = eventCode;
		boolean oldEventCodeESet = eventCodeESet;
		eventCode = EVENT_CODE_EDEFAULT;
		eventCodeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.EVENT__EVENT_CODE, oldEventCode, EVENT_CODE_EDEFAULT, oldEventCodeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEventCode() {
		return eventCodeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTriggerType() {
		return triggerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTriggerType(String newTriggerType) {
		String oldTriggerType = triggerType;
		triggerType = newTriggerType;
		boolean oldTriggerTypeESet = triggerTypeESet;
		triggerTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.EVENT__TRIGGER_TYPE, oldTriggerType, triggerType, !oldTriggerTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTriggerType() {
		String oldTriggerType = triggerType;
		boolean oldTriggerTypeESet = triggerTypeESet;
		triggerType = TRIGGER_TYPE_EDEFAULT;
		triggerTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.EVENT__TRIGGER_TYPE, oldTriggerType, TRIGGER_TYPE_EDEFAULT, oldTriggerTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTriggerType() {
		return triggerTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventScope getScope() {
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(EventScope newScope) {
		EventScope oldScope = scope;
		scope = newScope == null ? SCOPE_EDEFAULT : newScope;
		boolean oldScopeESet = scopeESet;
		scopeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.EVENT__SCOPE, oldScope, scope, !oldScopeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetScope() {
		EventScope oldScope = scope;
		boolean oldScopeESet = scopeESet;
		scope = SCOPE_EDEFAULT;
		scopeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.EVENT__SCOPE, oldScope, SCOPE_EDEFAULT, oldScopeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetScope() {
		return scopeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort getOutControlPort() {
		if (outControlPort != null && outControlPort.eIsProxy()) {
			InternalEObject oldOutControlPort = (InternalEObject)outControlPort;
			outControlPort = (OutControlPort)eResolveProxy(oldOutControlPort);
			if (outControlPort != oldOutControlPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GastatemodelPackage.EVENT__OUT_CONTROL_PORT, oldOutControlPort, outControlPort));
			}
		}
		return outControlPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort basicGetOutControlPort() {
		return outControlPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutControlPort(OutControlPort newOutControlPort) {
		OutControlPort oldOutControlPort = outControlPort;
		outControlPort = newOutControlPort;
		boolean oldOutControlPortESet = outControlPortESet;
		outControlPortESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.EVENT__OUT_CONTROL_PORT, oldOutControlPort, outControlPort, !oldOutControlPortESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOutControlPort() {
		OutControlPort oldOutControlPort = outControlPort;
		boolean oldOutControlPortESet = outControlPortESet;
		outControlPort = null;
		outControlPortESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.EVENT__OUT_CONTROL_PORT, oldOutControlPort, null, oldOutControlPortESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOutControlPort() {
		return outControlPortESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getReferenceExpression() {
		if (referenceExpression != null && referenceExpression.eIsProxy()) {
			InternalEObject oldReferenceExpression = (InternalEObject)referenceExpression;
			referenceExpression = (Expression)eResolveProxy(oldReferenceExpression);
			if (referenceExpression != oldReferenceExpression) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GastatemodelPackage.EVENT__REFERENCE_EXPRESSION, oldReferenceExpression, referenceExpression));
			}
		}
		return referenceExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression basicGetReferenceExpression() {
		return referenceExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceExpression(Expression newReferenceExpression) {
		Expression oldReferenceExpression = referenceExpression;
		referenceExpression = newReferenceExpression;
		boolean oldReferenceExpressionESet = referenceExpressionESet;
		referenceExpressionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.EVENT__REFERENCE_EXPRESSION, oldReferenceExpression, referenceExpression, !oldReferenceExpressionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReferenceExpression() {
		Expression oldReferenceExpression = referenceExpression;
		boolean oldReferenceExpressionESet = referenceExpressionESet;
		referenceExpression = null;
		referenceExpressionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.EVENT__REFERENCE_EXPRESSION, oldReferenceExpression, null, oldReferenceExpressionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReferenceExpression() {
		return referenceExpressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getBinder() {
		if (binder != null && binder.eIsProxy()) {
			InternalEObject oldBinder = (InternalEObject)binder;
			binder = (State)eResolveProxy(oldBinder);
			if (binder != oldBinder) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GastatemodelPackage.EVENT__BINDER, oldBinder, binder));
			}
		}
		return binder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetBinder() {
		return binder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinder(State newBinder) {
		State oldBinder = binder;
		binder = newBinder;
		boolean oldBinderESet = binderESet;
		binderESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GastatemodelPackage.EVENT__BINDER, oldBinder, binder, !oldBinderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBinder() {
		State oldBinder = binder;
		boolean oldBinderESet = binderESet;
		binder = null;
		binderESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GastatemodelPackage.EVENT__BINDER, oldBinder, null, oldBinderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBinder() {
		return binderESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GastatemodelPackage.EVENT__EVENT_CODE:
				return getEventCode();
			case GastatemodelPackage.EVENT__TRIGGER_TYPE:
				return getTriggerType();
			case GastatemodelPackage.EVENT__SCOPE:
				return getScope();
			case GastatemodelPackage.EVENT__OUT_CONTROL_PORT:
				if (resolve) return getOutControlPort();
				return basicGetOutControlPort();
			case GastatemodelPackage.EVENT__REFERENCE_EXPRESSION:
				if (resolve) return getReferenceExpression();
				return basicGetReferenceExpression();
			case GastatemodelPackage.EVENT__BINDER:
				if (resolve) return getBinder();
				return basicGetBinder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GastatemodelPackage.EVENT__EVENT_CODE:
				setEventCode((Integer)newValue);
				return;
			case GastatemodelPackage.EVENT__TRIGGER_TYPE:
				setTriggerType((String)newValue);
				return;
			case GastatemodelPackage.EVENT__SCOPE:
				setScope((EventScope)newValue);
				return;
			case GastatemodelPackage.EVENT__OUT_CONTROL_PORT:
				setOutControlPort((OutControlPort)newValue);
				return;
			case GastatemodelPackage.EVENT__REFERENCE_EXPRESSION:
				setReferenceExpression((Expression)newValue);
				return;
			case GastatemodelPackage.EVENT__BINDER:
				setBinder((State)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.EVENT__EVENT_CODE:
				unsetEventCode();
				return;
			case GastatemodelPackage.EVENT__TRIGGER_TYPE:
				unsetTriggerType();
				return;
			case GastatemodelPackage.EVENT__SCOPE:
				unsetScope();
				return;
			case GastatemodelPackage.EVENT__OUT_CONTROL_PORT:
				unsetOutControlPort();
				return;
			case GastatemodelPackage.EVENT__REFERENCE_EXPRESSION:
				unsetReferenceExpression();
				return;
			case GastatemodelPackage.EVENT__BINDER:
				unsetBinder();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GastatemodelPackage.EVENT__EVENT_CODE:
				return isSetEventCode();
			case GastatemodelPackage.EVENT__TRIGGER_TYPE:
				return isSetTriggerType();
			case GastatemodelPackage.EVENT__SCOPE:
				return isSetScope();
			case GastatemodelPackage.EVENT__OUT_CONTROL_PORT:
				return isSetOutControlPort();
			case GastatemodelPackage.EVENT__REFERENCE_EXPRESSION:
				return isSetReferenceExpression();
			case GastatemodelPackage.EVENT__BINDER:
				return isSetBinder();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (eventCode: ");
		if (eventCodeESet) result.append(eventCode); else result.append("<unset>");
		result.append(", triggerType: ");
		if (triggerTypeESet) result.append(triggerType); else result.append("<unset>");
		result.append(", scope: ");
		if (scopeESet) result.append(scope); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //EventImpl
