/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A directed connector between States and/or Junctions (commonly named Locations). Either connects two locations or enters to a location from an imaginary start location (a default transition).
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getExecutionOrder <em>Execution Order</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getDestination <em>Destination</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getEvents <em>Events</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getConditionActions <em>Condition Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getTransitionActions <em>Transition Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getGuard <em>Guard</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends GASystemModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Order</em>' attribute.
	 * @see #isSetExecutionOrder()
	 * @see #unsetExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition_ExecutionOrder()
	 * @model unsettable="true"
	 * @generated
	 */
	int getExecutionOrder();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getExecutionOrder <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Order</em>' attribute.
	 * @see #isSetExecutionOrder()
	 * @see #unsetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @generated
	 */
	void setExecutionOrder(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getExecutionOrder <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @generated
	 */
	void unsetExecutionOrder();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getExecutionOrder <em>Execution Order</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Execution Order</em>' attribute is set.
	 * @see #unsetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @generated
	 */
	boolean isSetExecutionOrder();

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' reference.
	 * @see #isSetDestination()
	 * @see #unsetDestination()
	 * @see #setDestination(Location)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition_Destination()
	 * @model unsettable="true"
	 * @generated
	 */
	Location getDestination();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getDestination <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination</em>' reference.
	 * @see #isSetDestination()
	 * @see #unsetDestination()
	 * @see #getDestination()
	 * @generated
	 */
	void setDestination(Location value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getDestination <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDestination()
	 * @see #getDestination()
	 * @see #setDestination(Location)
	 * @generated
	 */
	void unsetDestination();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getDestination <em>Destination</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Destination</em>' reference is set.
	 * @see #unsetDestination()
	 * @see #getDestination()
	 * @see #setDestination(Location)
	 * @generated
	 */
	boolean isSetDestination();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' reference list.
	 * @see #isSetEvents()
	 * @see #unsetEvents()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition_Events()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getEvents <em>Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	void unsetEvents();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getEvents <em>Events</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Events</em>' reference list is set.
	 * @see #unsetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	boolean isSetEvents();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(TransitionType)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition_Type()
	 * @model unsettable="true"
	 * @generated
	 */
	TransitionType getType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.TransitionType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(TransitionType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(TransitionType)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(TransitionType)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Condition Actions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.SimpleAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Actions</em>' containment reference list.
	 * @see #isSetConditionActions()
	 * @see #unsetConditionActions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition_ConditionActions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<SimpleAction> getConditionActions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getConditionActions <em>Condition Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConditionActions()
	 * @see #getConditionActions()
	 * @generated
	 */
	void unsetConditionActions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getConditionActions <em>Condition Actions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Condition Actions</em>' containment reference list is set.
	 * @see #unsetConditionActions()
	 * @see #getConditionActions()
	 * @generated
	 */
	boolean isSetConditionActions();

	/**
	 * Returns the value of the '<em><b>Transition Actions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.SimpleAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition Actions</em>' containment reference list.
	 * @see #isSetTransitionActions()
	 * @see #unsetTransitionActions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition_TransitionActions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<SimpleAction> getTransitionActions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getTransitionActions <em>Transition Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTransitionActions()
	 * @see #getTransitionActions()
	 * @generated
	 */
	void unsetTransitionActions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getTransitionActions <em>Transition Actions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Transition Actions</em>' containment reference list is set.
	 * @see #unsetTransitionActions()
	 * @see #getTransitionActions()
	 * @generated
	 */
	boolean isSetTransitionActions();

	/**
	 * Returns the value of the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard</em>' containment reference.
	 * @see #isSetGuard()
	 * @see #unsetGuard()
	 * @see #setGuard(Expression)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getTransition_Guard()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getGuard();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getGuard <em>Guard</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard</em>' containment reference.
	 * @see #isSetGuard()
	 * @see #unsetGuard()
	 * @see #getGuard()
	 * @generated
	 */
	void setGuard(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getGuard <em>Guard</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetGuard()
	 * @see #getGuard()
	 * @see #setGuard(Expression)
	 * @generated
	 */
	void unsetGuard();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Transition#getGuard <em>Guard</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Guard</em>' containment reference is set.
	 * @see #unsetGuard()
	 * @see #getGuard()
	 * @see #setGuard(Expression)
	 * @generated
	 */
	boolean isSetGuard();

} // Transition
