/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.gasystemmodel.common.Variable_SM;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Location</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A graph node in state model -- generalises all types of start and end nodes for transitions.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.Location#getStateVariable <em>State Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getLocation()
 * @model abstract="true"
 * @generated
 */
public interface Location extends GASystemModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>State Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Variable</em>' containment reference.
	 * @see #isSetStateVariable()
	 * @see #unsetStateVariable()
	 * @see #setStateVariable(Variable_SM)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getLocation_StateVariable()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Variable_SM getStateVariable();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Location#getStateVariable <em>State Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Variable</em>' containment reference.
	 * @see #isSetStateVariable()
	 * @see #unsetStateVariable()
	 * @see #getStateVariable()
	 * @generated
	 */
	void setStateVariable(Variable_SM value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Location#getStateVariable <em>State Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStateVariable()
	 * @see #getStateVariable()
	 * @see #setStateVariable(Variable_SM)
	 * @generated
	 */
	void unsetStateVariable();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.Location#getStateVariable <em>State Variable</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>State Variable</em>' containment reference is set.
	 * @see #unsetStateVariable()
	 * @see #getStateVariable()
	 * @see #setStateVariable(Variable_SM)
	 * @generated
	 */
	boolean isSetStateVariable();

} // Location
