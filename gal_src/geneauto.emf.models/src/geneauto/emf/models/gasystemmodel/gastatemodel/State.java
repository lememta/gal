/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gastatemodel;

import geneauto.emf.models.gasystemmodel.common.ContainerNode;
import geneauto.emf.models.gasystemmodel.common.Function_SM;
import geneauto.emf.models.gasystemmodel.common.Variable_SM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A location in the chart that can stay persistently active during subsequent activations of the chart. A state can have further internal structure, contain variables, etc.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExecutionOrder <em>Execution Order</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getMachineType <em>Machine Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getFunctions <em>Functions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getVariables <em>Variables</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getDuringActions <em>During Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEntryActions <em>Entry Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExitActions <em>Exit Actions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getOuterTransitionList <em>Outer Transition List</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getInnerTransitionList <em>Inner Transition List</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getComposition <em>Composition</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEvents <em>Events</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState()
 * @model
 * @generated
 */
public interface State extends Location, ContainerNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Order</em>' attribute.
	 * @see #isSetExecutionOrder()
	 * @see #unsetExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_ExecutionOrder()
	 * @model unsettable="true"
	 * @generated
	 */
	int getExecutionOrder();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExecutionOrder <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Order</em>' attribute.
	 * @see #isSetExecutionOrder()
	 * @see #unsetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @generated
	 */
	void setExecutionOrder(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExecutionOrder <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @generated
	 */
	void unsetExecutionOrder();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExecutionOrder <em>Execution Order</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Execution Order</em>' attribute is set.
	 * @see #unsetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @generated
	 */
	boolean isSetExecutionOrder();

	/**
	 * Returns the value of the '<em><b>Machine Type</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Type</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType
	 * @see #isSetMachineType()
	 * @see #unsetMachineType()
	 * @see #setMachineType(StateMachineType)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_MachineType()
	 * @model unsettable="true"
	 * @generated
	 */
	StateMachineType getMachineType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getMachineType <em>Machine Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Type</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateMachineType
	 * @see #isSetMachineType()
	 * @see #unsetMachineType()
	 * @see #getMachineType()
	 * @generated
	 */
	void setMachineType(StateMachineType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getMachineType <em>Machine Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMachineType()
	 * @see #getMachineType()
	 * @see #setMachineType(StateMachineType)
	 * @generated
	 */
	void unsetMachineType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getMachineType <em>Machine Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Machine Type</em>' attribute is set.
	 * @see #unsetMachineType()
	 * @see #getMachineType()
	 * @see #setMachineType(StateMachineType)
	 * @generated
	 */
	boolean isSetMachineType();

	/**
	 * Returns the value of the '<em><b>Functions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.Function_SM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functions</em>' containment reference list.
	 * @see #isSetFunctions()
	 * @see #unsetFunctions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_Functions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Function_SM> getFunctions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getFunctions <em>Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFunctions()
	 * @see #getFunctions()
	 * @generated
	 */
	void unsetFunctions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getFunctions <em>Functions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Functions</em>' containment reference list is set.
	 * @see #unsetFunctions()
	 * @see #getFunctions()
	 * @generated
	 */
	boolean isSetFunctions();

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.Variable_SM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see #isSetVariables()
	 * @see #unsetVariables()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_Variables()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Variable_SM> getVariables();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getVariables <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVariables()
	 * @see #getVariables()
	 * @generated
	 */
	void unsetVariables();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getVariables <em>Variables</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Variables</em>' containment reference list is set.
	 * @see #unsetVariables()
	 * @see #getVariables()
	 * @generated
	 */
	boolean isSetVariables();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gasystemmodel.gastatemodel.StateType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(StateType)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_Type()
	 * @model unsettable="true"
	 * @generated
	 */
	StateType getType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.StateType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(StateType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(StateType)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(StateType)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>During Actions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>During Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>During Actions</em>' containment reference list.
	 * @see #isSetDuringActions()
	 * @see #unsetDuringActions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_DuringActions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Action> getDuringActions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getDuringActions <em>During Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDuringActions()
	 * @see #getDuringActions()
	 * @generated
	 */
	void unsetDuringActions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getDuringActions <em>During Actions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>During Actions</em>' containment reference list is set.
	 * @see #unsetDuringActions()
	 * @see #getDuringActions()
	 * @generated
	 */
	boolean isSetDuringActions();

	/**
	 * Returns the value of the '<em><b>Entry Actions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.SimpleAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Actions</em>' containment reference list.
	 * @see #isSetEntryActions()
	 * @see #unsetEntryActions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_EntryActions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<SimpleAction> getEntryActions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEntryActions <em>Entry Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEntryActions()
	 * @see #getEntryActions()
	 * @generated
	 */
	void unsetEntryActions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEntryActions <em>Entry Actions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Entry Actions</em>' containment reference list is set.
	 * @see #unsetEntryActions()
	 * @see #getEntryActions()
	 * @generated
	 */
	boolean isSetEntryActions();

	/**
	 * Returns the value of the '<em><b>Exit Actions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.SimpleAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exit Actions</em>' containment reference list.
	 * @see #isSetExitActions()
	 * @see #unsetExitActions()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_ExitActions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<SimpleAction> getExitActions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExitActions <em>Exit Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExitActions()
	 * @see #getExitActions()
	 * @generated
	 */
	void unsetExitActions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getExitActions <em>Exit Actions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Exit Actions</em>' containment reference list is set.
	 * @see #unsetExitActions()
	 * @see #getExitActions()
	 * @generated
	 */
	boolean isSetExitActions();

	/**
	 * Returns the value of the '<em><b>Outer Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outer Transition List</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outer Transition List</em>' containment reference.
	 * @see #isSetOuterTransitionList()
	 * @see #unsetOuterTransitionList()
	 * @see #setOuterTransitionList(TransitionList)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_OuterTransitionList()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	TransitionList getOuterTransitionList();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getOuterTransitionList <em>Outer Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outer Transition List</em>' containment reference.
	 * @see #isSetOuterTransitionList()
	 * @see #unsetOuterTransitionList()
	 * @see #getOuterTransitionList()
	 * @generated
	 */
	void setOuterTransitionList(TransitionList value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getOuterTransitionList <em>Outer Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOuterTransitionList()
	 * @see #getOuterTransitionList()
	 * @see #setOuterTransitionList(TransitionList)
	 * @generated
	 */
	void unsetOuterTransitionList();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getOuterTransitionList <em>Outer Transition List</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Outer Transition List</em>' containment reference is set.
	 * @see #unsetOuterTransitionList()
	 * @see #getOuterTransitionList()
	 * @see #setOuterTransitionList(TransitionList)
	 * @generated
	 */
	boolean isSetOuterTransitionList();

	/**
	 * Returns the value of the '<em><b>Inner Transition List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inner Transition List</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner Transition List</em>' containment reference.
	 * @see #isSetInnerTransitionList()
	 * @see #unsetInnerTransitionList()
	 * @see #setInnerTransitionList(TransitionList)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_InnerTransitionList()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	TransitionList getInnerTransitionList();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getInnerTransitionList <em>Inner Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner Transition List</em>' containment reference.
	 * @see #isSetInnerTransitionList()
	 * @see #unsetInnerTransitionList()
	 * @see #getInnerTransitionList()
	 * @generated
	 */
	void setInnerTransitionList(TransitionList value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getInnerTransitionList <em>Inner Transition List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInnerTransitionList()
	 * @see #getInnerTransitionList()
	 * @see #setInnerTransitionList(TransitionList)
	 * @generated
	 */
	void unsetInnerTransitionList();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getInnerTransitionList <em>Inner Transition List</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Inner Transition List</em>' containment reference is set.
	 * @see #unsetInnerTransitionList()
	 * @see #getInnerTransitionList()
	 * @see #setInnerTransitionList(TransitionList)
	 * @generated
	 */
	boolean isSetInnerTransitionList();

	/**
	 * Returns the value of the '<em><b>Composition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composition</em>' containment reference.
	 * @see #isSetComposition()
	 * @see #unsetComposition()
	 * @see #setComposition(Composition)
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_Composition()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Composition getComposition();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getComposition <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Composition</em>' containment reference.
	 * @see #isSetComposition()
	 * @see #unsetComposition()
	 * @see #getComposition()
	 * @generated
	 */
	void setComposition(Composition value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getComposition <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetComposition()
	 * @see #getComposition()
	 * @see #setComposition(Composition)
	 * @generated
	 */
	void unsetComposition();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getComposition <em>Composition</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Composition</em>' containment reference is set.
	 * @see #unsetComposition()
	 * @see #getComposition()
	 * @see #setComposition(Composition)
	 * @generated
	 */
	boolean isSetComposition();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see #isSetEvents()
	 * @see #unsetEvents()
	 * @see geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage#getState_Events()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEvents <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	void unsetEvents();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gastatemodel.State#getEvents <em>Events</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Events</em>' containment reference list is set.
	 * @see #unsetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	boolean isSetEvents();

} // State
