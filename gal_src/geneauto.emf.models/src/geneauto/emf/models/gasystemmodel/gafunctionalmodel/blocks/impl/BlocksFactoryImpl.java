/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BlocksFactoryImpl extends EFactoryImpl implements BlocksFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BlocksFactory init() {
		try {
			BlocksFactory theBlocksFactory = (BlocksFactory)EPackage.Registry.INSTANCE.getEFactory("http:///geneauto/emf/models/gasystemmodel/gafunctionalmodel/blocks.ecore"); 
			if (theBlocksFactory != null) {
				return theBlocksFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BlocksFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BlocksPackage.BLOCK: return createBlock();
			case BlocksPackage.DIAGRAM_INFO: return createDiagramInfo();
			case BlocksPackage.CHART_BLOCK: return createChartBlock();
			case BlocksPackage.COMBINATORIAL_BLOCK: return createCombinatorialBlock();
			case BlocksPackage.CONTROL_BLOCK: return createControlBlock();
			case BlocksPackage.GENERIC_BLOCK: return createGenericBlock();
			case BlocksPackage.REFERENCE_BLOCK: return createReferenceBlock();
			case BlocksPackage.ROUTING_BLOCK: return createRoutingBlock();
			case BlocksPackage.SEQUENTIAL_BLOCK: return createSequentialBlock();
			case BlocksPackage.SINK_BLOCK: return createSinkBlock();
			case BlocksPackage.SOURCE_BLOCK: return createSourceBlock();
			case BlocksPackage.SYSTEM_BLOCK: return createSystemBlock();
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK: return createUserDefinedFunctionBlock();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block createBlock() {
		BlockImpl block = new BlockImpl();
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiagramInfo createDiagramInfo() {
		DiagramInfoImpl diagramInfo = new DiagramInfoImpl();
		return diagramInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChartBlock createChartBlock() {
		ChartBlockImpl chartBlock = new ChartBlockImpl();
		return chartBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CombinatorialBlock createCombinatorialBlock() {
		CombinatorialBlockImpl combinatorialBlock = new CombinatorialBlockImpl();
		return combinatorialBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlBlock createControlBlock() {
		ControlBlockImpl controlBlock = new ControlBlockImpl();
		return controlBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericBlock createGenericBlock() {
		GenericBlockImpl genericBlock = new GenericBlockImpl();
		return genericBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceBlock createReferenceBlock() {
		ReferenceBlockImpl referenceBlock = new ReferenceBlockImpl();
		return referenceBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoutingBlock createRoutingBlock() {
		RoutingBlockImpl routingBlock = new RoutingBlockImpl();
		return routingBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequentialBlock createSequentialBlock() {
		SequentialBlockImpl sequentialBlock = new SequentialBlockImpl();
		return sequentialBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SinkBlock createSinkBlock() {
		SinkBlockImpl sinkBlock = new SinkBlockImpl();
		return sinkBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceBlock createSourceBlock() {
		SourceBlockImpl sourceBlock = new SourceBlockImpl();
		return sourceBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemBlock createSystemBlock() {
		SystemBlockImpl systemBlock = new SystemBlockImpl();
		return systemBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserDefinedFunctionBlock createUserDefinedFunctionBlock() {
		UserDefinedFunctionBlockImpl userDefinedFunctionBlock = new UserDefinedFunctionBlockImpl();
		return userDefinedFunctionBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksPackage getBlocksPackage() {
		return (BlocksPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BlocksPackage getPackage() {
		return BlocksPackage.eINSTANCE;
	}

} //BlocksFactoryImpl
