/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksFactory
 * @model kind="package"
 * @generated
 */
public interface BlocksPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "blocks";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gasystemmodel/gafunctionalmodel/blocks.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BlocksPackage eINSTANCE = geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getBlock()
	 * @generated
	 */
	int BLOCK = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ASSIGNED_PRIORITY = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ASSIGNED_PRIORITY_SOURCE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__DIRECT_FEED_THROUGH = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__EXECUTION_ORDER = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__IS_VIRTUAL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__MASK_TYPE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__SAMPLE_TIME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__TYPE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__USER_DEFINED_PRIORITY = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PORT_REFERENCE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PARAMETERS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__DIAGRAM_INFO = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__IN_DATA_PORTS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__IN_ENABLE_PORT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__IN_EDGE_ENABLE_PORT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__IN_CONTROL_PORTS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__OUT_DATA_PORTS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__OUT_CONTROL_PORTS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 17;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 18;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl <em>Diagram Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getDiagramInfo()
	 * @generated
	 */
	int DIAGRAM_INFO = 1;

	/**
	 * The feature id for the '<em><b>Position X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_INFO__POSITION_X = 0;

	/**
	 * The feature id for the '<em><b>Position Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_INFO__POSITION_Y = 1;

	/**
	 * The feature id for the '<em><b>Size X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_INFO__SIZE_X = 2;

	/**
	 * The feature id for the '<em><b>Size Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_INFO__SIZE_Y = 3;

	/**
	 * The number of structural features of the '<em>Diagram Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_INFO_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl <em>Chart Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getChartBlock()
	 * @generated
	 */
	int CHART_BLOCK = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Execute At Init</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__EXECUTE_AT_INIT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Export Functions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__EXPORT_FUNCTIONS = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Instance Outer Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__INSTANCE_OUTER_REFERENCE = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Init Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__INIT_FUNCTION = BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Active Event Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ACTIVE_EVENT_REFERENCE = BLOCK_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>State Arg Internal Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__STATE_ARG_INTERNAL_REF = BLOCK_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Context Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__CONTEXT_REFERENCE = BLOCK_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Instance Struct Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__INSTANCE_STRUCT_TYPE = BLOCK_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Compute Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__COMPUTE_FUNCTION = BLOCK_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__FUNCTIONS = BLOCK_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__VARIABLES = BLOCK_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Root Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__ROOT_LOCATION = BLOCK_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Composition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__COMPOSITION = BLOCK_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__EVENTS = BLOCK_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Default Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__DEFAULT_EVENT = BLOCK_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Code Model Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__CODE_MODEL_ELEMENTS = BLOCK_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>External Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK__EXTERNAL_DEPENDENCIES = BLOCK_FEATURE_COUNT + 16;

	/**
	 * The number of structural features of the '<em>Chart Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 17;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.CombinatorialBlockImpl <em>Combinatorial Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.CombinatorialBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getCombinatorialBlock()
	 * @generated
	 */
	int COMBINATORIAL_BLOCK = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The number of structural features of the '<em>Combinatorial Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ControlBlockImpl <em>Control Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ControlBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getControlBlock()
	 * @generated
	 */
	int CONTROL_BLOCK = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The number of structural features of the '<em>Control Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.GenericBlockImpl <em>Generic Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.GenericBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getGenericBlock()
	 * @generated
	 */
	int GENERIC_BLOCK = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The number of structural features of the '<em>Generic Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ReferenceBlockImpl <em>Reference Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ReferenceBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getReferenceBlock()
	 * @generated
	 */
	int REFERENCE_BLOCK = 6;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Source Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK__SOURCE_MODEL = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.RoutingBlockImpl <em>Routing Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.RoutingBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getRoutingBlock()
	 * @generated
	 */
	int ROUTING_BLOCK = 7;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The number of structural features of the '<em>Routing Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTING_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SequentialBlockImpl <em>Sequential Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SequentialBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSequentialBlock()
	 * @generated
	 */
	int SEQUENTIAL_BLOCK = 8;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK__VARIABLES = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sequential Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SinkBlockImpl <em>Sink Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SinkBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSinkBlock()
	 * @generated
	 */
	int SINK_BLOCK = 9;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Variable Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK__VARIABLE_REFERENCE = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sink Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SourceBlockImpl <em>Source Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SourceBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSourceBlock()
	 * @generated
	 */
	int SOURCE_BLOCK = 10;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Variable Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK__VARIABLE_REFERENCE = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Source Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl <em>System Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSystemBlock()
	 * @generated
	 */
	int SYSTEM_BLOCK = 11;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Fcn Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__FCN_NAME = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__STYLE = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Index Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__INDEX_VARIABLE = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__VARIABLES = BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__SIGNALS = BLOCK_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__BLOCKS = BLOCK_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__EVENTS = BLOCK_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Custom Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK__CUSTOM_TYPES = BLOCK_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>System Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.UserDefinedFunctionBlockImpl <em>User Defined Function Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.UserDefinedFunctionBlockImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getUserDefinedFunctionBlock()
	 * @generated
	 */
	int USER_DEFINED_FUNCTION_BLOCK = 12;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__EXTERNAL_ID = BLOCK__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__ID = BLOCK__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__IS_FIXED_NAME = BLOCK__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__NAME = BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__ORIGINAL_FULL_NAME = BLOCK__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__ORIGINAL_NAME = BLOCK__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__MODEL = BLOCK__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__CODE_MODEL_ELEMENT = BLOCK__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__ASSIGNED_PRIORITY = BLOCK__ASSIGNED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__ASSIGNED_PRIORITY_SOURCE = BLOCK__ASSIGNED_PRIORITY_SOURCE;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__DIRECT_FEED_THROUGH = BLOCK__DIRECT_FEED_THROUGH;

	/**
	 * The feature id for the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__EXECUTION_ORDER = BLOCK__EXECUTION_ORDER;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__IS_VIRTUAL = BLOCK__IS_VIRTUAL;

	/**
	 * The feature id for the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__MASK_TYPE = BLOCK__MASK_TYPE;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__SAMPLE_TIME = BLOCK__SAMPLE_TIME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__TYPE = BLOCK__TYPE;

	/**
	 * The feature id for the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__USER_DEFINED_PRIORITY = BLOCK__USER_DEFINED_PRIORITY;

	/**
	 * The feature id for the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__PORT_REFERENCE = BLOCK__PORT_REFERENCE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__PARAMETERS = BLOCK__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__DIAGRAM_INFO = BLOCK__DIAGRAM_INFO;

	/**
	 * The feature id for the '<em><b>In Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__IN_DATA_PORTS = BLOCK__IN_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__IN_ENABLE_PORT = BLOCK__IN_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__IN_EDGE_ENABLE_PORT = BLOCK__IN_EDGE_ENABLE_PORT;

	/**
	 * The feature id for the '<em><b>In Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__IN_CONTROL_PORTS = BLOCK__IN_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__OUT_DATA_PORTS = BLOCK__OUT_DATA_PORTS;

	/**
	 * The feature id for the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__OUT_CONTROL_PORTS = BLOCK__OUT_CONTROL_PORTS;

	/**
	 * The feature id for the '<em><b>Function Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Header File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>User Defined Function Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_FUNCTION_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block
	 * @generated
	 */
	EClass getBlock();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPriority <em>Assigned Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assigned Priority</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPriority()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_AssignedPriority();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPrioritySource <em>Assigned Priority Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assigned Priority Source</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPrioritySource()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_AssignedPrioritySource();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isDirectFeedThrough <em>Direct Feed Through</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direct Feed Through</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isDirectFeedThrough()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_DirectFeedThrough();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getExecutionOrder <em>Execution Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Order</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getExecutionOrder()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_ExecutionOrder();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isVirtual <em>Is Virtual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Virtual</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isVirtual()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_IsVirtual();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getMaskType <em>Mask Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mask Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getMaskType()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_MaskType();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getSampleTime <em>Sample Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sample Time</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getSampleTime()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_SampleTime();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getType()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_Type();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getUserDefinedPriority <em>User Defined Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User Defined Priority</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getUserDefinedPriority()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_UserDefinedPriority();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getPortReference <em>Port Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Reference</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getPortReference()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_PortReference();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getParameters()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getDiagramInfo <em>Diagram Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Diagram Info</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getDiagramInfo()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_DiagramInfo();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInDataPorts <em>In Data Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>In Data Ports</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInDataPorts()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_InDataPorts();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEnablePort <em>In Enable Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>In Enable Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEnablePort()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_InEnablePort();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEdgeEnablePort <em>In Edge Enable Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>In Edge Enable Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEdgeEnablePort()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_InEdgeEnablePort();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInControlPorts <em>In Control Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>In Control Ports</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInControlPorts()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_InControlPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutDataPorts <em>Out Data Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Out Data Ports</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutDataPorts()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_OutDataPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutControlPorts <em>Out Control Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Out Control Ports</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutControlPorts()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_OutControlPorts();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo <em>Diagram Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagram Info</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo
	 * @generated
	 */
	EClass getDiagramInfo();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getPositionX <em>Position X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position X</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getPositionX()
	 * @see #getDiagramInfo()
	 * @generated
	 */
	EAttribute getDiagramInfo_PositionX();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getPositionY <em>Position Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position Y</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getPositionY()
	 * @see #getDiagramInfo()
	 * @generated
	 */
	EAttribute getDiagramInfo_PositionY();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getSizeX <em>Size X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size X</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getSizeX()
	 * @see #getDiagramInfo()
	 * @generated
	 */
	EAttribute getDiagramInfo_SizeX();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getSizeY <em>Size Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size Y</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo#getSizeY()
	 * @see #getDiagramInfo()
	 * @generated
	 */
	EAttribute getDiagramInfo_SizeY();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock <em>Chart Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chart Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock
	 * @generated
	 */
	EClass getChartBlock();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExecuteAtInit <em>Execute At Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execute At Init</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExecuteAtInit()
	 * @see #getChartBlock()
	 * @generated
	 */
	EAttribute getChartBlock_ExecuteAtInit();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExportFunctions <em>Export Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Export Functions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExportFunctions()
	 * @see #getChartBlock()
	 * @generated
	 */
	EAttribute getChartBlock_ExportFunctions();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceOuterReference <em>Instance Outer Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance Outer Reference</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceOuterReference()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_InstanceOuterReference();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInitFunction <em>Init Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Init Function</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInitFunction()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_InitFunction();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getActiveEventReference <em>Active Event Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active Event Reference</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getActiveEventReference()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_ActiveEventReference();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getStateArgInternalRef <em>State Arg Internal Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State Arg Internal Ref</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getStateArgInternalRef()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_StateArgInternalRef();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getContextReference <em>Context Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Context Reference</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getContextReference()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_ContextReference();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceStructType <em>Instance Struct Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance Struct Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceStructType()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_InstanceStructType();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComputeFunction <em>Compute Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Compute Function</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComputeFunction()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_ComputeFunction();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getFunctions <em>Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functions</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getFunctions()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_Functions();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getVariables()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_Variables();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getRootLocation <em>Root Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root Location</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getRootLocation()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_RootLocation();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComposition <em>Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Composition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComposition()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_Composition();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getEvents()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_Events();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getDefaultEvent <em>Default Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Event</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getDefaultEvent()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_DefaultEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getCodeModelElements <em>Code Model Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Code Model Elements</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getCodeModelElements()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_CodeModelElements();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getExternalDependencies <em>External Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>External Dependencies</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getExternalDependencies()
	 * @see #getChartBlock()
	 * @generated
	 */
	EReference getChartBlock_ExternalDependencies();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock <em>Combinatorial Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Combinatorial Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock
	 * @generated
	 */
	EClass getCombinatorialBlock();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ControlBlock <em>Control Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ControlBlock
	 * @generated
	 */
	EClass getControlBlock();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.GenericBlock <em>Generic Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.GenericBlock
	 * @generated
	 */
	EClass getGenericBlock();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock <em>Reference Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock
	 * @generated
	 */
	EClass getReferenceBlock();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock#getSourceModel <em>Source Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Model</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock#getSourceModel()
	 * @see #getReferenceBlock()
	 * @generated
	 */
	EAttribute getReferenceBlock_SourceModel();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.RoutingBlock <em>Routing Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Routing Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.RoutingBlock
	 * @generated
	 */
	EClass getRoutingBlock();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock <em>Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequential Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock
	 * @generated
	 */
	EClass getSequentialBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock#getVariables()
	 * @see #getSequentialBlock()
	 * @generated
	 */
	EReference getSequentialBlock_Variables();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock <em>Sink Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sink Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock
	 * @generated
	 */
	EClass getSinkBlock();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock#getVariableReference <em>Variable Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Variable Reference</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock#getVariableReference()
	 * @see #getSinkBlock()
	 * @generated
	 */
	EReference getSinkBlock_VariableReference();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock <em>Source Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock
	 * @generated
	 */
	EClass getSourceBlock();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock#getVariableReference <em>Variable Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable Reference</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock#getVariableReference()
	 * @see #getSourceBlock()
	 * @generated
	 */
	EReference getSourceBlock_VariableReference();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock <em>System Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock
	 * @generated
	 */
	EClass getSystemBlock();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getFcnName <em>Fcn Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fcn Name</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getFcnName()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EAttribute getSystemBlock_FcnName();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getStyle <em>Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Style</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getStyle()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EAttribute getSystemBlock_Style();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getIndexVariable <em>Index Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Index Variable</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getIndexVariable()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EReference getSystemBlock_IndexVariable();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getVariables()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EReference getSystemBlock_Variables();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getSignals <em>Signals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Signals</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getSignals()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EReference getSystemBlock_Signals();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getBlocks <em>Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blocks</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getBlocks()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EReference getSystemBlock_Blocks();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getEvents()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EReference getSystemBlock_Events();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getCustomTypes <em>Custom Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Custom Types</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock#getCustomTypes()
	 * @see #getSystemBlock()
	 * @generated
	 */
	EReference getSystemBlock_CustomTypes();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock <em>User Defined Function Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Defined Function Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock
	 * @generated
	 */
	EClass getUserDefinedFunctionBlock();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getFunctionDefinition <em>Function Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Function Definition</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getFunctionDefinition()
	 * @see #getUserDefinedFunctionBlock()
	 * @generated
	 */
	EAttribute getUserDefinedFunctionBlock_FunctionDefinition();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getHeaderFile <em>Header File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header File</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getHeaderFile()
	 * @see #getUserDefinedFunctionBlock()
	 * @generated
	 */
	EAttribute getUserDefinedFunctionBlock_HeaderFile();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BlocksFactory getBlocksFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getBlock()
		 * @generated
		 */
		EClass BLOCK = eINSTANCE.getBlock();

		/**
		 * The meta object literal for the '<em><b>Assigned Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__ASSIGNED_PRIORITY = eINSTANCE.getBlock_AssignedPriority();

		/**
		 * The meta object literal for the '<em><b>Assigned Priority Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__ASSIGNED_PRIORITY_SOURCE = eINSTANCE.getBlock_AssignedPrioritySource();

		/**
		 * The meta object literal for the '<em><b>Direct Feed Through</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__DIRECT_FEED_THROUGH = eINSTANCE.getBlock_DirectFeedThrough();

		/**
		 * The meta object literal for the '<em><b>Execution Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__EXECUTION_ORDER = eINSTANCE.getBlock_ExecutionOrder();

		/**
		 * The meta object literal for the '<em><b>Is Virtual</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__IS_VIRTUAL = eINSTANCE.getBlock_IsVirtual();

		/**
		 * The meta object literal for the '<em><b>Mask Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__MASK_TYPE = eINSTANCE.getBlock_MaskType();

		/**
		 * The meta object literal for the '<em><b>Sample Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__SAMPLE_TIME = eINSTANCE.getBlock_SampleTime();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__TYPE = eINSTANCE.getBlock_Type();

		/**
		 * The meta object literal for the '<em><b>User Defined Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__USER_DEFINED_PRIORITY = eINSTANCE.getBlock_UserDefinedPriority();

		/**
		 * The meta object literal for the '<em><b>Port Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__PORT_REFERENCE = eINSTANCE.getBlock_PortReference();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__PARAMETERS = eINSTANCE.getBlock_Parameters();

		/**
		 * The meta object literal for the '<em><b>Diagram Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__DIAGRAM_INFO = eINSTANCE.getBlock_DiagramInfo();

		/**
		 * The meta object literal for the '<em><b>In Data Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__IN_DATA_PORTS = eINSTANCE.getBlock_InDataPorts();

		/**
		 * The meta object literal for the '<em><b>In Enable Port</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__IN_ENABLE_PORT = eINSTANCE.getBlock_InEnablePort();

		/**
		 * The meta object literal for the '<em><b>In Edge Enable Port</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__IN_EDGE_ENABLE_PORT = eINSTANCE.getBlock_InEdgeEnablePort();

		/**
		 * The meta object literal for the '<em><b>In Control Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__IN_CONTROL_PORTS = eINSTANCE.getBlock_InControlPorts();

		/**
		 * The meta object literal for the '<em><b>Out Data Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__OUT_DATA_PORTS = eINSTANCE.getBlock_OutDataPorts();

		/**
		 * The meta object literal for the '<em><b>Out Control Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__OUT_CONTROL_PORTS = eINSTANCE.getBlock_OutControlPorts();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl <em>Diagram Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getDiagramInfo()
		 * @generated
		 */
		EClass DIAGRAM_INFO = eINSTANCE.getDiagramInfo();

		/**
		 * The meta object literal for the '<em><b>Position X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM_INFO__POSITION_X = eINSTANCE.getDiagramInfo_PositionX();

		/**
		 * The meta object literal for the '<em><b>Position Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM_INFO__POSITION_Y = eINSTANCE.getDiagramInfo_PositionY();

		/**
		 * The meta object literal for the '<em><b>Size X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM_INFO__SIZE_X = eINSTANCE.getDiagramInfo_SizeX();

		/**
		 * The meta object literal for the '<em><b>Size Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM_INFO__SIZE_Y = eINSTANCE.getDiagramInfo_SizeY();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl <em>Chart Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getChartBlock()
		 * @generated
		 */
		EClass CHART_BLOCK = eINSTANCE.getChartBlock();

		/**
		 * The meta object literal for the '<em><b>Execute At Init</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_BLOCK__EXECUTE_AT_INIT = eINSTANCE.getChartBlock_ExecuteAtInit();

		/**
		 * The meta object literal for the '<em><b>Export Functions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_BLOCK__EXPORT_FUNCTIONS = eINSTANCE.getChartBlock_ExportFunctions();

		/**
		 * The meta object literal for the '<em><b>Instance Outer Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__INSTANCE_OUTER_REFERENCE = eINSTANCE.getChartBlock_InstanceOuterReference();

		/**
		 * The meta object literal for the '<em><b>Init Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__INIT_FUNCTION = eINSTANCE.getChartBlock_InitFunction();

		/**
		 * The meta object literal for the '<em><b>Active Event Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__ACTIVE_EVENT_REFERENCE = eINSTANCE.getChartBlock_ActiveEventReference();

		/**
		 * The meta object literal for the '<em><b>State Arg Internal Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__STATE_ARG_INTERNAL_REF = eINSTANCE.getChartBlock_StateArgInternalRef();

		/**
		 * The meta object literal for the '<em><b>Context Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__CONTEXT_REFERENCE = eINSTANCE.getChartBlock_ContextReference();

		/**
		 * The meta object literal for the '<em><b>Instance Struct Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__INSTANCE_STRUCT_TYPE = eINSTANCE.getChartBlock_InstanceStructType();

		/**
		 * The meta object literal for the '<em><b>Compute Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__COMPUTE_FUNCTION = eINSTANCE.getChartBlock_ComputeFunction();

		/**
		 * The meta object literal for the '<em><b>Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__FUNCTIONS = eINSTANCE.getChartBlock_Functions();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__VARIABLES = eINSTANCE.getChartBlock_Variables();

		/**
		 * The meta object literal for the '<em><b>Root Location</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__ROOT_LOCATION = eINSTANCE.getChartBlock_RootLocation();

		/**
		 * The meta object literal for the '<em><b>Composition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__COMPOSITION = eINSTANCE.getChartBlock_Composition();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__EVENTS = eINSTANCE.getChartBlock_Events();

		/**
		 * The meta object literal for the '<em><b>Default Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__DEFAULT_EVENT = eINSTANCE.getChartBlock_DefaultEvent();

		/**
		 * The meta object literal for the '<em><b>Code Model Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__CODE_MODEL_ELEMENTS = eINSTANCE.getChartBlock_CodeModelElements();

		/**
		 * The meta object literal for the '<em><b>External Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_BLOCK__EXTERNAL_DEPENDENCIES = eINSTANCE.getChartBlock_ExternalDependencies();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.CombinatorialBlockImpl <em>Combinatorial Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.CombinatorialBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getCombinatorialBlock()
		 * @generated
		 */
		EClass COMBINATORIAL_BLOCK = eINSTANCE.getCombinatorialBlock();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ControlBlockImpl <em>Control Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ControlBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getControlBlock()
		 * @generated
		 */
		EClass CONTROL_BLOCK = eINSTANCE.getControlBlock();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.GenericBlockImpl <em>Generic Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.GenericBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getGenericBlock()
		 * @generated
		 */
		EClass GENERIC_BLOCK = eINSTANCE.getGenericBlock();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ReferenceBlockImpl <em>Reference Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ReferenceBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getReferenceBlock()
		 * @generated
		 */
		EClass REFERENCE_BLOCK = eINSTANCE.getReferenceBlock();

		/**
		 * The meta object literal for the '<em><b>Source Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCE_BLOCK__SOURCE_MODEL = eINSTANCE.getReferenceBlock_SourceModel();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.RoutingBlockImpl <em>Routing Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.RoutingBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getRoutingBlock()
		 * @generated
		 */
		EClass ROUTING_BLOCK = eINSTANCE.getRoutingBlock();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SequentialBlockImpl <em>Sequential Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SequentialBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSequentialBlock()
		 * @generated
		 */
		EClass SEQUENTIAL_BLOCK = eINSTANCE.getSequentialBlock();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENTIAL_BLOCK__VARIABLES = eINSTANCE.getSequentialBlock_Variables();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SinkBlockImpl <em>Sink Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SinkBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSinkBlock()
		 * @generated
		 */
		EClass SINK_BLOCK = eINSTANCE.getSinkBlock();

		/**
		 * The meta object literal for the '<em><b>Variable Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SINK_BLOCK__VARIABLE_REFERENCE = eINSTANCE.getSinkBlock_VariableReference();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SourceBlockImpl <em>Source Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SourceBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSourceBlock()
		 * @generated
		 */
		EClass SOURCE_BLOCK = eINSTANCE.getSourceBlock();

		/**
		 * The meta object literal for the '<em><b>Variable Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOURCE_BLOCK__VARIABLE_REFERENCE = eINSTANCE.getSourceBlock_VariableReference();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl <em>System Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getSystemBlock()
		 * @generated
		 */
		EClass SYSTEM_BLOCK = eINSTANCE.getSystemBlock();

		/**
		 * The meta object literal for the '<em><b>Fcn Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_BLOCK__FCN_NAME = eINSTANCE.getSystemBlock_FcnName();

		/**
		 * The meta object literal for the '<em><b>Style</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_BLOCK__STYLE = eINSTANCE.getSystemBlock_Style();

		/**
		 * The meta object literal for the '<em><b>Index Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_BLOCK__INDEX_VARIABLE = eINSTANCE.getSystemBlock_IndexVariable();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_BLOCK__VARIABLES = eINSTANCE.getSystemBlock_Variables();

		/**
		 * The meta object literal for the '<em><b>Signals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_BLOCK__SIGNALS = eINSTANCE.getSystemBlock_Signals();

		/**
		 * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_BLOCK__BLOCKS = eINSTANCE.getSystemBlock_Blocks();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_BLOCK__EVENTS = eINSTANCE.getSystemBlock_Events();

		/**
		 * The meta object literal for the '<em><b>Custom Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_BLOCK__CUSTOM_TYPES = eINSTANCE.getSystemBlock_CustomTypes();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.UserDefinedFunctionBlockImpl <em>User Defined Function Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.UserDefinedFunctionBlockImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl#getUserDefinedFunctionBlock()
		 * @generated
		 */
		EClass USER_DEFINED_FUNCTION_BLOCK = eINSTANCE.getUserDefinedFunctionBlock();

		/**
		 * The meta object literal for the '<em><b>Function Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION = eINSTANCE.getUserDefinedFunctionBlock_FunctionDefinition();

		/**
		 * The meta object literal for the '<em><b>Header File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE = eINSTANCE.getUserDefinedFunctionBlock_HeaderFile();

	}

} //BlocksPackage
