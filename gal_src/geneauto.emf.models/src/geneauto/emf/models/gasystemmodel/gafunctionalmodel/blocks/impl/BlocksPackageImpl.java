/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.common.CommonPackage;

import geneauto.emf.models.common.impl.CommonPackageImpl;

import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;

import geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;

import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;

import geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl;

import geneauto.emf.models.gacodemodel.expression.statemodel.StatemodelPackage;

import geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl;

import geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesPackage;

import geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl;

import geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl;

import geneauto.emf.models.gacodemodel.operator.OperatorPackage;

import geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl;

import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastPackage;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl;

import geneauto.emf.models.gadatatypes.GadatatypesPackage;

import geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.GafunctionalmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksFactory;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ControlBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.GenericBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.RoutingBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl;

import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;

import geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl;

import geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BlocksPackageImpl extends EPackageImpl implements BlocksPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagramInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass chartBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass combinatorialBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass genericBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass routingBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequentialBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sinkBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sourceBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userDefinedFunctionBlockEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BlocksPackageImpl() {
		super(eNS_URI, BlocksFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BlocksPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BlocksPackage init() {
		if (isInited) return (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);

		// Obtain or create and register package
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BlocksPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) : CommonPackage.eINSTANCE);
		GadatatypesPackageImpl theGadatatypesPackage = (GadatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) instanceof GadatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) : GadatatypesPackage.eINSTANCE);
		GenericmodelPackageImpl theGenericmodelPackage = (GenericmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) instanceof GenericmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) : GenericmodelPackage.eINSTANCE);
		GacodemodelPackageImpl theGacodemodelPackage = (GacodemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) instanceof GacodemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) : GacodemodelPackage.eINSTANCE);
		ExpressionPackageImpl theExpressionPackage = (ExpressionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) instanceof ExpressionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) : ExpressionPackage.eINSTANCE);
		StatemodelPackageImpl theStatemodelPackage = (StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) instanceof StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) : StatemodelPackage.eINSTANCE);
		StatementPackageImpl theStatementPackage = (StatementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) instanceof StatementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) : StatementPackage.eINSTANCE);
		geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl theStatemodelPackage_1 = (geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) instanceof geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) : geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eINSTANCE);
		BroadcastPackageImpl theBroadcastPackage = (BroadcastPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) instanceof BroadcastPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) : BroadcastPackage.eINSTANCE);
		GaenumtypesPackageImpl theGaenumtypesPackage = (GaenumtypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) instanceof GaenumtypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) : GaenumtypesPackage.eINSTANCE);
		OperatorPackageImpl theOperatorPackage = (OperatorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) instanceof OperatorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) : OperatorPackage.eINSTANCE);
		GasystemmodelPackageImpl theGasystemmodelPackage = (GasystemmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) instanceof GasystemmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) : GasystemmodelPackage.eINSTANCE);
		geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl theCommonPackage_1 = (geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) instanceof geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) : geneauto.emf.models.gasystemmodel.common.CommonPackage.eINSTANCE);
		GastatemodelPackageImpl theGastatemodelPackage = (GastatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) instanceof GastatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) : GastatemodelPackage.eINSTANCE);
		GafunctionalmodelPackageImpl theGafunctionalmodelPackage = (GafunctionalmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) instanceof GafunctionalmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) : GafunctionalmodelPackage.eINSTANCE);
		PortsPackageImpl thePortsPackage = (PortsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) instanceof PortsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) : PortsPackage.eINSTANCE);
		GablocklibraryPackageImpl theGablocklibraryPackage = (GablocklibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) instanceof GablocklibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) : GablocklibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theBlocksPackage.createPackageContents();
		theCommonPackage.createPackageContents();
		theGadatatypesPackage.createPackageContents();
		theGenericmodelPackage.createPackageContents();
		theGacodemodelPackage.createPackageContents();
		theExpressionPackage.createPackageContents();
		theStatemodelPackage.createPackageContents();
		theStatementPackage.createPackageContents();
		theStatemodelPackage_1.createPackageContents();
		theBroadcastPackage.createPackageContents();
		theGaenumtypesPackage.createPackageContents();
		theOperatorPackage.createPackageContents();
		theGasystemmodelPackage.createPackageContents();
		theCommonPackage_1.createPackageContents();
		theGastatemodelPackage.createPackageContents();
		theGafunctionalmodelPackage.createPackageContents();
		thePortsPackage.createPackageContents();
		theGablocklibraryPackage.createPackageContents();

		// Initialize created meta-data
		theBlocksPackage.initializePackageContents();
		theCommonPackage.initializePackageContents();
		theGadatatypesPackage.initializePackageContents();
		theGenericmodelPackage.initializePackageContents();
		theGacodemodelPackage.initializePackageContents();
		theExpressionPackage.initializePackageContents();
		theStatemodelPackage.initializePackageContents();
		theStatementPackage.initializePackageContents();
		theStatemodelPackage_1.initializePackageContents();
		theBroadcastPackage.initializePackageContents();
		theGaenumtypesPackage.initializePackageContents();
		theOperatorPackage.initializePackageContents();
		theGasystemmodelPackage.initializePackageContents();
		theCommonPackage_1.initializePackageContents();
		theGastatemodelPackage.initializePackageContents();
		theGafunctionalmodelPackage.initializePackageContents();
		thePortsPackage.initializePackageContents();
		theGablocklibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBlocksPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BlocksPackage.eNS_URI, theBlocksPackage);
		return theBlocksPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlock() {
		return blockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_AssignedPriority() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_AssignedPrioritySource() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_DirectFeedThrough() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_ExecutionOrder() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_IsVirtual() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_MaskType() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_SampleTime() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_Type() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_UserDefinedPriority() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_PortReference() {
		return (EReference)blockEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_Parameters() {
		return (EReference)blockEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_DiagramInfo() {
		return (EReference)blockEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_InDataPorts() {
		return (EReference)blockEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_InEnablePort() {
		return (EReference)blockEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_InEdgeEnablePort() {
		return (EReference)blockEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_InControlPorts() {
		return (EReference)blockEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_OutDataPorts() {
		return (EReference)blockEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_OutControlPorts() {
		return (EReference)blockEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiagramInfo() {
		return diagramInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagramInfo_PositionX() {
		return (EAttribute)diagramInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagramInfo_PositionY() {
		return (EAttribute)diagramInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagramInfo_SizeX() {
		return (EAttribute)diagramInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagramInfo_SizeY() {
		return (EAttribute)diagramInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChartBlock() {
		return chartBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChartBlock_ExecuteAtInit() {
		return (EAttribute)chartBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChartBlock_ExportFunctions() {
		return (EAttribute)chartBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_InstanceOuterReference() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_InitFunction() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_ActiveEventReference() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_StateArgInternalRef() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_ContextReference() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_InstanceStructType() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_ComputeFunction() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_Functions() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_Variables() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_RootLocation() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_Composition() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_Events() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_DefaultEvent() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_CodeModelElements() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChartBlock_ExternalDependencies() {
		return (EReference)chartBlockEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCombinatorialBlock() {
		return combinatorialBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlBlock() {
		return controlBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenericBlock() {
		return genericBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceBlock() {
		return referenceBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferenceBlock_SourceModel() {
		return (EAttribute)referenceBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoutingBlock() {
		return routingBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequentialBlock() {
		return sequentialBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequentialBlock_Variables() {
		return (EReference)sequentialBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSinkBlock() {
		return sinkBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSinkBlock_VariableReference() {
		return (EReference)sinkBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSourceBlock() {
		return sourceBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSourceBlock_VariableReference() {
		return (EReference)sourceBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemBlock() {
		return systemBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystemBlock_FcnName() {
		return (EAttribute)systemBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystemBlock_Style() {
		return (EAttribute)systemBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemBlock_IndexVariable() {
		return (EReference)systemBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemBlock_Variables() {
		return (EReference)systemBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemBlock_Signals() {
		return (EReference)systemBlockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemBlock_Blocks() {
		return (EReference)systemBlockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemBlock_Events() {
		return (EReference)systemBlockEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemBlock_CustomTypes() {
		return (EReference)systemBlockEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserDefinedFunctionBlock() {
		return userDefinedFunctionBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserDefinedFunctionBlock_FunctionDefinition() {
		return (EAttribute)userDefinedFunctionBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserDefinedFunctionBlock_HeaderFile() {
		return (EAttribute)userDefinedFunctionBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksFactory getBlocksFactory() {
		return (BlocksFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		blockEClass = createEClass(BLOCK);
		createEAttribute(blockEClass, BLOCK__ASSIGNED_PRIORITY);
		createEAttribute(blockEClass, BLOCK__ASSIGNED_PRIORITY_SOURCE);
		createEAttribute(blockEClass, BLOCK__DIRECT_FEED_THROUGH);
		createEAttribute(blockEClass, BLOCK__EXECUTION_ORDER);
		createEAttribute(blockEClass, BLOCK__IS_VIRTUAL);
		createEAttribute(blockEClass, BLOCK__MASK_TYPE);
		createEAttribute(blockEClass, BLOCK__SAMPLE_TIME);
		createEAttribute(blockEClass, BLOCK__TYPE);
		createEAttribute(blockEClass, BLOCK__USER_DEFINED_PRIORITY);
		createEReference(blockEClass, BLOCK__PORT_REFERENCE);
		createEReference(blockEClass, BLOCK__PARAMETERS);
		createEReference(blockEClass, BLOCK__DIAGRAM_INFO);
		createEReference(blockEClass, BLOCK__IN_DATA_PORTS);
		createEReference(blockEClass, BLOCK__IN_ENABLE_PORT);
		createEReference(blockEClass, BLOCK__IN_EDGE_ENABLE_PORT);
		createEReference(blockEClass, BLOCK__IN_CONTROL_PORTS);
		createEReference(blockEClass, BLOCK__OUT_DATA_PORTS);
		createEReference(blockEClass, BLOCK__OUT_CONTROL_PORTS);

		diagramInfoEClass = createEClass(DIAGRAM_INFO);
		createEAttribute(diagramInfoEClass, DIAGRAM_INFO__POSITION_X);
		createEAttribute(diagramInfoEClass, DIAGRAM_INFO__POSITION_Y);
		createEAttribute(diagramInfoEClass, DIAGRAM_INFO__SIZE_X);
		createEAttribute(diagramInfoEClass, DIAGRAM_INFO__SIZE_Y);

		chartBlockEClass = createEClass(CHART_BLOCK);
		createEAttribute(chartBlockEClass, CHART_BLOCK__EXECUTE_AT_INIT);
		createEAttribute(chartBlockEClass, CHART_BLOCK__EXPORT_FUNCTIONS);
		createEReference(chartBlockEClass, CHART_BLOCK__INSTANCE_OUTER_REFERENCE);
		createEReference(chartBlockEClass, CHART_BLOCK__INIT_FUNCTION);
		createEReference(chartBlockEClass, CHART_BLOCK__ACTIVE_EVENT_REFERENCE);
		createEReference(chartBlockEClass, CHART_BLOCK__STATE_ARG_INTERNAL_REF);
		createEReference(chartBlockEClass, CHART_BLOCK__CONTEXT_REFERENCE);
		createEReference(chartBlockEClass, CHART_BLOCK__INSTANCE_STRUCT_TYPE);
		createEReference(chartBlockEClass, CHART_BLOCK__COMPUTE_FUNCTION);
		createEReference(chartBlockEClass, CHART_BLOCK__FUNCTIONS);
		createEReference(chartBlockEClass, CHART_BLOCK__VARIABLES);
		createEReference(chartBlockEClass, CHART_BLOCK__ROOT_LOCATION);
		createEReference(chartBlockEClass, CHART_BLOCK__COMPOSITION);
		createEReference(chartBlockEClass, CHART_BLOCK__EVENTS);
		createEReference(chartBlockEClass, CHART_BLOCK__DEFAULT_EVENT);
		createEReference(chartBlockEClass, CHART_BLOCK__CODE_MODEL_ELEMENTS);
		createEReference(chartBlockEClass, CHART_BLOCK__EXTERNAL_DEPENDENCIES);

		combinatorialBlockEClass = createEClass(COMBINATORIAL_BLOCK);

		controlBlockEClass = createEClass(CONTROL_BLOCK);

		genericBlockEClass = createEClass(GENERIC_BLOCK);

		referenceBlockEClass = createEClass(REFERENCE_BLOCK);
		createEAttribute(referenceBlockEClass, REFERENCE_BLOCK__SOURCE_MODEL);

		routingBlockEClass = createEClass(ROUTING_BLOCK);

		sequentialBlockEClass = createEClass(SEQUENTIAL_BLOCK);
		createEReference(sequentialBlockEClass, SEQUENTIAL_BLOCK__VARIABLES);

		sinkBlockEClass = createEClass(SINK_BLOCK);
		createEReference(sinkBlockEClass, SINK_BLOCK__VARIABLE_REFERENCE);

		sourceBlockEClass = createEClass(SOURCE_BLOCK);
		createEReference(sourceBlockEClass, SOURCE_BLOCK__VARIABLE_REFERENCE);

		systemBlockEClass = createEClass(SYSTEM_BLOCK);
		createEAttribute(systemBlockEClass, SYSTEM_BLOCK__FCN_NAME);
		createEAttribute(systemBlockEClass, SYSTEM_BLOCK__STYLE);
		createEReference(systemBlockEClass, SYSTEM_BLOCK__INDEX_VARIABLE);
		createEReference(systemBlockEClass, SYSTEM_BLOCK__VARIABLES);
		createEReference(systemBlockEClass, SYSTEM_BLOCK__SIGNALS);
		createEReference(systemBlockEClass, SYSTEM_BLOCK__BLOCKS);
		createEReference(systemBlockEClass, SYSTEM_BLOCK__EVENTS);
		createEReference(systemBlockEClass, SYSTEM_BLOCK__CUSTOM_TYPES);

		userDefinedFunctionBlockEClass = createEClass(USER_DEFINED_FUNCTION_BLOCK);
		createEAttribute(userDefinedFunctionBlockEClass, USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION);
		createEAttribute(userDefinedFunctionBlockEClass, USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GasystemmodelPackage theGasystemmodelPackage = (GasystemmodelPackage)EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI);
		geneauto.emf.models.gasystemmodel.common.CommonPackage theCommonPackage_1 = (geneauto.emf.models.gasystemmodel.common.CommonPackage)EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI);
		PortsPackage thePortsPackage = (PortsPackage)EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI);
		ExpressionPackage theExpressionPackage = (ExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI);
		GadatatypesPackage theGadatatypesPackage = (GadatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI);
		GastatemodelPackage theGastatemodelPackage = (GastatemodelPackage)EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI);
		GacodemodelPackage theGacodemodelPackage = (GacodemodelPackage)EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI);
		GaenumtypesPackage theGaenumtypesPackage = (GaenumtypesPackage)EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI);
		GafunctionalmodelPackage theGafunctionalmodelPackage = (GafunctionalmodelPackage)EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		blockEClass.getESuperTypes().add(theGasystemmodelPackage.getGASystemModelElement());
		blockEClass.getESuperTypes().add(theCommonPackage_1.getContainerNode());
		chartBlockEClass.getESuperTypes().add(this.getBlock());
		combinatorialBlockEClass.getESuperTypes().add(this.getBlock());
		controlBlockEClass.getESuperTypes().add(this.getBlock());
		genericBlockEClass.getESuperTypes().add(this.getBlock());
		referenceBlockEClass.getESuperTypes().add(this.getBlock());
		routingBlockEClass.getESuperTypes().add(this.getBlock());
		sequentialBlockEClass.getESuperTypes().add(this.getBlock());
		sinkBlockEClass.getESuperTypes().add(this.getBlock());
		sourceBlockEClass.getESuperTypes().add(this.getBlock());
		systemBlockEClass.getESuperTypes().add(this.getBlock());
		systemBlockEClass.getESuperTypes().add(theGasystemmodelPackage.getGASystemModelRoot());
		userDefinedFunctionBlockEClass.getESuperTypes().add(this.getBlock());

		// Initialize classes and features; add operations and parameters
		initEClass(blockEClass, Block.class, "Block", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlock_AssignedPriority(), ecorePackage.getEInt(), "assignedPriority", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_AssignedPrioritySource(), ecorePackage.getEString(), "assignedPrioritySource", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_DirectFeedThrough(), ecorePackage.getEBoolean(), "directFeedThrough", "true", 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_ExecutionOrder(), ecorePackage.getEInt(), "executionOrder", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_IsVirtual(), ecorePackage.getEBoolean(), "isVirtual", "true", 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_MaskType(), ecorePackage.getEString(), "maskType", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_SampleTime(), ecorePackage.getEDouble(), "sampleTime", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_Type(), ecorePackage.getEString(), "type", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_UserDefinedPriority(), ecorePackage.getEInt(), "userDefinedPriority", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_PortReference(), thePortsPackage.getPort(), null, "portReference", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_Parameters(), theCommonPackage_1.getBlockParameter(), null, "parameters", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_DiagramInfo(), this.getDiagramInfo(), null, "diagramInfo", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_InDataPorts(), thePortsPackage.getInDataPort(), null, "inDataPorts", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_InEnablePort(), thePortsPackage.getInEnablePort(), null, "inEnablePort", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_InEdgeEnablePort(), thePortsPackage.getInEdgeEnablePort(), null, "inEdgeEnablePort", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_InControlPorts(), thePortsPackage.getInControlPort(), null, "inControlPorts", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_OutDataPorts(), thePortsPackage.getOutDataPort(), null, "outDataPorts", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_OutControlPorts(), thePortsPackage.getOutControlPort(), null, "outControlPorts", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(diagramInfoEClass, DiagramInfo.class, "DiagramInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDiagramInfo_PositionX(), ecorePackage.getEInt(), "positionX", null, 0, 1, DiagramInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDiagramInfo_PositionY(), ecorePackage.getEInt(), "positionY", null, 0, 1, DiagramInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDiagramInfo_SizeX(), ecorePackage.getEInt(), "sizeX", null, 0, 1, DiagramInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDiagramInfo_SizeY(), ecorePackage.getEInt(), "sizeY", null, 0, 1, DiagramInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(chartBlockEClass, ChartBlock.class, "ChartBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChartBlock_ExecuteAtInit(), ecorePackage.getEBoolean(), "executeAtInit", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChartBlock_ExportFunctions(), ecorePackage.getEBoolean(), "exportFunctions", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_InstanceOuterReference(), theExpressionPackage.getExpression(), null, "instanceOuterReference", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_InitFunction(), theCommonPackage_1.getFunction_SM(), null, "initFunction", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_ActiveEventReference(), theExpressionPackage.getExpression(), null, "activeEventReference", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_StateArgInternalRef(), theExpressionPackage.getExpression(), null, "stateArgInternalRef", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_ContextReference(), theExpressionPackage.getExpression(), null, "contextReference", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_InstanceStructType(), theGadatatypesPackage.getGADataType(), null, "instanceStructType", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_ComputeFunction(), theCommonPackage_1.getFunction_SM(), null, "computeFunction", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_Functions(), theCommonPackage_1.getFunction_SM(), null, "functions", null, 0, -1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_Variables(), theCommonPackage_1.getVariable_SM(), null, "variables", null, 0, -1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_RootLocation(), theGastatemodelPackage.getChartRoot(), null, "rootLocation", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_Composition(), theGastatemodelPackage.getComposition(), null, "composition", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_Events(), theGastatemodelPackage.getEvent(), null, "events", null, 0, -1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_DefaultEvent(), theGastatemodelPackage.getEvent(), null, "defaultEvent", null, 0, 1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_CodeModelElements(), theGacodemodelPackage.getGACodeModelElement(), null, "codeModelElements", null, 0, -1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartBlock_ExternalDependencies(), theGacodemodelPackage.getDependency(), null, "externalDependencies", null, 0, -1, ChartBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(combinatorialBlockEClass, CombinatorialBlock.class, "CombinatorialBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(controlBlockEClass, ControlBlock.class, "ControlBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(genericBlockEClass, GenericBlock.class, "GenericBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(referenceBlockEClass, ReferenceBlock.class, "ReferenceBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReferenceBlock_SourceModel(), ecorePackage.getEInt(), "sourceModel", null, 0, 1, ReferenceBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(routingBlockEClass, RoutingBlock.class, "RoutingBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sequentialBlockEClass, SequentialBlock.class, "SequentialBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSequentialBlock_Variables(), theCommonPackage_1.getVariable_SM(), null, "variables", null, 0, -1, SequentialBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sinkBlockEClass, SinkBlock.class, "SinkBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSinkBlock_VariableReference(), theExpressionPackage.getVariableExpression(), null, "variableReference", null, 0, 1, SinkBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sourceBlockEClass, SourceBlock.class, "SourceBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSourceBlock_VariableReference(), theExpressionPackage.getVariableExpression(), null, "variableReference", null, 0, 1, SourceBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(systemBlockEClass, SystemBlock.class, "SystemBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSystemBlock_FcnName(), ecorePackage.getEString(), "fcnName", null, 0, 1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSystemBlock_Style(), theGaenumtypesPackage.getFunctionStyle(), "style", null, 0, 1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemBlock_IndexVariable(), theGacodemodelPackage.getVariable_CM(), null, "indexVariable", null, 0, 1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemBlock_Variables(), theCommonPackage_1.getVariable_SM(), null, "variables", null, 0, -1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemBlock_Signals(), theGafunctionalmodelPackage.getSignal(), null, "signals", null, 0, -1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemBlock_Blocks(), this.getBlock(), null, "blocks", null, 0, -1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemBlock_Events(), theGastatemodelPackage.getEvent(), null, "events", null, 0, -1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemBlock_CustomTypes(), theGacodemodelPackage.getCustomType_CM(), null, "customTypes", null, 0, -1, SystemBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userDefinedFunctionBlockEClass, UserDefinedFunctionBlock.class, "UserDefinedFunctionBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUserDefinedFunctionBlock_FunctionDefinition(), ecorePackage.getEString(), "functionDefinition", null, 0, 1, UserDefinedFunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserDefinedFunctionBlock_HeaderFile(), ecorePackage.getEString(), "headerFile", null, 0, 1, UserDefinedFunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //BlocksPackageImpl
