/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Enable Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEnablePortImpl#isResetStates <em>Reset States</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEnablePortImpl#getOutputDataType <em>Output Data Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InEnablePortImpl extends InportImpl implements InEnablePort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #isResetStates() <em>Reset States</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResetStates()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESET_STATES_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResetStates() <em>Reset States</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResetStates()
	 * @generated
	 * @ordered
	 */
	protected boolean resetStates = RESET_STATES_EDEFAULT;

	/**
	 * This is true if the Reset States attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean resetStatesESet;

	/**
	 * The cached value of the '{@link #getOutputDataType() <em>Output Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDataType()
	 * @generated
	 * @ordered
	 */
	protected GADataType outputDataType;

	/**
	 * This is true if the Output Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean outputDataTypeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InEnablePortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortsPackage.Literals.IN_ENABLE_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResetStates() {
		return resetStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResetStates(boolean newResetStates) {
		boolean oldResetStates = resetStates;
		resetStates = newResetStates;
		boolean oldResetStatesESet = resetStatesESet;
		resetStatesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.IN_ENABLE_PORT__RESET_STATES, oldResetStates, resetStates, !oldResetStatesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResetStates() {
		boolean oldResetStates = resetStates;
		boolean oldResetStatesESet = resetStatesESet;
		resetStates = RESET_STATES_EDEFAULT;
		resetStatesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.IN_ENABLE_PORT__RESET_STATES, oldResetStates, RESET_STATES_EDEFAULT, oldResetStatesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResetStates() {
		return resetStatesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getOutputDataType() {
		return outputDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputDataType(GADataType newOutputDataType, NotificationChain msgs) {
		GADataType oldOutputDataType = outputDataType;
		outputDataType = newOutputDataType;
		boolean oldOutputDataTypeESet = outputDataTypeESet;
		outputDataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE, oldOutputDataType, newOutputDataType, !oldOutputDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputDataType(GADataType newOutputDataType) {
		if (newOutputDataType != outputDataType) {
			NotificationChain msgs = null;
			if (outputDataType != null)
				msgs = ((InternalEObject)outputDataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE, null, msgs);
			if (newOutputDataType != null)
				msgs = ((InternalEObject)newOutputDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE, null, msgs);
			msgs = basicSetOutputDataType(newOutputDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldOutputDataTypeESet = outputDataTypeESet;
			outputDataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE, newOutputDataType, newOutputDataType, !oldOutputDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetOutputDataType(NotificationChain msgs) {
		GADataType oldOutputDataType = outputDataType;
		outputDataType = null;
		boolean oldOutputDataTypeESet = outputDataTypeESet;
		outputDataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE, oldOutputDataType, null, oldOutputDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOutputDataType() {
		if (outputDataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)outputDataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE, null, msgs);
			msgs = basicUnsetOutputDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldOutputDataTypeESet = outputDataTypeESet;
			outputDataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE, null, null, oldOutputDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOutputDataType() {
		return outputDataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE:
				return basicUnsetOutputDataType(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortsPackage.IN_ENABLE_PORT__RESET_STATES:
				return isResetStates();
			case PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE:
				return getOutputDataType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortsPackage.IN_ENABLE_PORT__RESET_STATES:
				setResetStates((Boolean)newValue);
				return;
			case PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE:
				setOutputDataType((GADataType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortsPackage.IN_ENABLE_PORT__RESET_STATES:
				unsetResetStates();
				return;
			case PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE:
				unsetOutputDataType();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortsPackage.IN_ENABLE_PORT__RESET_STATES:
				return isSetResetStates();
			case PortsPackage.IN_ENABLE_PORT__OUTPUT_DATA_TYPE:
				return isSetOutputDataType();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (resetStates: ");
		if (resetStatesESet) result.append(resetStates); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //InEnablePortImpl
