/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Defined Function Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Blocks that refer to external function (function written in target programming language). The only information code generator has for those functions is the function signature.  In simulink this class of blocks corresponds to a masked subsystems or S-functions.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getFunctionDefinition <em>Function Definition</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getHeaderFile <em>Header File</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getUserDefinedFunctionBlock()
 * @model
 * @generated
 */
public interface UserDefinedFunctionBlock extends Block {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Function Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Definition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Definition</em>' attribute.
	 * @see #isSetFunctionDefinition()
	 * @see #unsetFunctionDefinition()
	 * @see #setFunctionDefinition(String)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getUserDefinedFunctionBlock_FunctionDefinition()
	 * @model unsettable="true"
	 * @generated
	 */
	String getFunctionDefinition();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getFunctionDefinition <em>Function Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Definition</em>' attribute.
	 * @see #isSetFunctionDefinition()
	 * @see #unsetFunctionDefinition()
	 * @see #getFunctionDefinition()
	 * @generated
	 */
	void setFunctionDefinition(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getFunctionDefinition <em>Function Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFunctionDefinition()
	 * @see #getFunctionDefinition()
	 * @see #setFunctionDefinition(String)
	 * @generated
	 */
	void unsetFunctionDefinition();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getFunctionDefinition <em>Function Definition</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Function Definition</em>' attribute is set.
	 * @see #unsetFunctionDefinition()
	 * @see #getFunctionDefinition()
	 * @see #setFunctionDefinition(String)
	 * @generated
	 */
	boolean isSetFunctionDefinition();

	/**
	 * Returns the value of the '<em><b>Header File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header File</em>' attribute.
	 * @see #isSetHeaderFile()
	 * @see #unsetHeaderFile()
	 * @see #setHeaderFile(String)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getUserDefinedFunctionBlock_HeaderFile()
	 * @model unsettable="true"
	 * @generated
	 */
	String getHeaderFile();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getHeaderFile <em>Header File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header File</em>' attribute.
	 * @see #isSetHeaderFile()
	 * @see #unsetHeaderFile()
	 * @see #getHeaderFile()
	 * @generated
	 */
	void setHeaderFile(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getHeaderFile <em>Header File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHeaderFile()
	 * @see #getHeaderFile()
	 * @see #setHeaderFile(String)
	 * @generated
	 */
	void unsetHeaderFile();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock#getHeaderFile <em>Header File</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Header File</em>' attribute is set.
	 * @see #unsetHeaderFile()
	 * @see #getHeaderFile()
	 * @see #setHeaderFile(String)
	 * @generated
	 */
	boolean isSetHeaderFile();

} // UserDefinedFunctionBlock
