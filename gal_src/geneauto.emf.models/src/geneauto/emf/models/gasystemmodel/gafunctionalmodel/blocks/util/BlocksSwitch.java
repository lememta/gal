/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.util;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;
import geneauto.emf.models.gasystemmodel.GASystemModelRoot;

import geneauto.emf.models.gasystemmodel.common.ContainerNode;
import geneauto.emf.models.gasystemmodel.common.Node;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.*;

import geneauto.emf.models.genericmodel.GAModelElement;
import geneauto.emf.models.genericmodel.GANamed;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage
 * @generated
 */
public class BlocksSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BlocksPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksSwitch() {
		if (modelPackage == null) {
			modelPackage = BlocksPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case BlocksPackage.BLOCK: {
				Block block = (Block)theEObject;
				T result = caseBlock(block);
				if (result == null) result = caseGASystemModelElement(block);
				if (result == null) result = caseContainerNode(block);
				if (result == null) result = caseGAModelElement(block);
				if (result == null) result = caseNode(block);
				if (result == null) result = caseGANamed(block);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.DIAGRAM_INFO: {
				DiagramInfo diagramInfo = (DiagramInfo)theEObject;
				T result = caseDiagramInfo(diagramInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.CHART_BLOCK: {
				ChartBlock chartBlock = (ChartBlock)theEObject;
				T result = caseChartBlock(chartBlock);
				if (result == null) result = caseBlock(chartBlock);
				if (result == null) result = caseGASystemModelElement(chartBlock);
				if (result == null) result = caseContainerNode(chartBlock);
				if (result == null) result = caseGAModelElement(chartBlock);
				if (result == null) result = caseNode(chartBlock);
				if (result == null) result = caseGANamed(chartBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.COMBINATORIAL_BLOCK: {
				CombinatorialBlock combinatorialBlock = (CombinatorialBlock)theEObject;
				T result = caseCombinatorialBlock(combinatorialBlock);
				if (result == null) result = caseBlock(combinatorialBlock);
				if (result == null) result = caseGASystemModelElement(combinatorialBlock);
				if (result == null) result = caseContainerNode(combinatorialBlock);
				if (result == null) result = caseGAModelElement(combinatorialBlock);
				if (result == null) result = caseNode(combinatorialBlock);
				if (result == null) result = caseGANamed(combinatorialBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.CONTROL_BLOCK: {
				ControlBlock controlBlock = (ControlBlock)theEObject;
				T result = caseControlBlock(controlBlock);
				if (result == null) result = caseBlock(controlBlock);
				if (result == null) result = caseGASystemModelElement(controlBlock);
				if (result == null) result = caseContainerNode(controlBlock);
				if (result == null) result = caseGAModelElement(controlBlock);
				if (result == null) result = caseNode(controlBlock);
				if (result == null) result = caseGANamed(controlBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.GENERIC_BLOCK: {
				GenericBlock genericBlock = (GenericBlock)theEObject;
				T result = caseGenericBlock(genericBlock);
				if (result == null) result = caseBlock(genericBlock);
				if (result == null) result = caseGASystemModelElement(genericBlock);
				if (result == null) result = caseContainerNode(genericBlock);
				if (result == null) result = caseGAModelElement(genericBlock);
				if (result == null) result = caseNode(genericBlock);
				if (result == null) result = caseGANamed(genericBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.REFERENCE_BLOCK: {
				ReferenceBlock referenceBlock = (ReferenceBlock)theEObject;
				T result = caseReferenceBlock(referenceBlock);
				if (result == null) result = caseBlock(referenceBlock);
				if (result == null) result = caseGASystemModelElement(referenceBlock);
				if (result == null) result = caseContainerNode(referenceBlock);
				if (result == null) result = caseGAModelElement(referenceBlock);
				if (result == null) result = caseNode(referenceBlock);
				if (result == null) result = caseGANamed(referenceBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.ROUTING_BLOCK: {
				RoutingBlock routingBlock = (RoutingBlock)theEObject;
				T result = caseRoutingBlock(routingBlock);
				if (result == null) result = caseBlock(routingBlock);
				if (result == null) result = caseGASystemModelElement(routingBlock);
				if (result == null) result = caseContainerNode(routingBlock);
				if (result == null) result = caseGAModelElement(routingBlock);
				if (result == null) result = caseNode(routingBlock);
				if (result == null) result = caseGANamed(routingBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.SEQUENTIAL_BLOCK: {
				SequentialBlock sequentialBlock = (SequentialBlock)theEObject;
				T result = caseSequentialBlock(sequentialBlock);
				if (result == null) result = caseBlock(sequentialBlock);
				if (result == null) result = caseGASystemModelElement(sequentialBlock);
				if (result == null) result = caseContainerNode(sequentialBlock);
				if (result == null) result = caseGAModelElement(sequentialBlock);
				if (result == null) result = caseNode(sequentialBlock);
				if (result == null) result = caseGANamed(sequentialBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.SINK_BLOCK: {
				SinkBlock sinkBlock = (SinkBlock)theEObject;
				T result = caseSinkBlock(sinkBlock);
				if (result == null) result = caseBlock(sinkBlock);
				if (result == null) result = caseGASystemModelElement(sinkBlock);
				if (result == null) result = caseContainerNode(sinkBlock);
				if (result == null) result = caseGAModelElement(sinkBlock);
				if (result == null) result = caseNode(sinkBlock);
				if (result == null) result = caseGANamed(sinkBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.SOURCE_BLOCK: {
				SourceBlock sourceBlock = (SourceBlock)theEObject;
				T result = caseSourceBlock(sourceBlock);
				if (result == null) result = caseBlock(sourceBlock);
				if (result == null) result = caseGASystemModelElement(sourceBlock);
				if (result == null) result = caseContainerNode(sourceBlock);
				if (result == null) result = caseGAModelElement(sourceBlock);
				if (result == null) result = caseNode(sourceBlock);
				if (result == null) result = caseGANamed(sourceBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.SYSTEM_BLOCK: {
				SystemBlock systemBlock = (SystemBlock)theEObject;
				T result = caseSystemBlock(systemBlock);
				if (result == null) result = caseBlock(systemBlock);
				if (result == null) result = caseGASystemModelRoot(systemBlock);
				if (result == null) result = caseGASystemModelElement(systemBlock);
				if (result == null) result = caseContainerNode(systemBlock);
				if (result == null) result = caseGAModelElement(systemBlock);
				if (result == null) result = caseNode(systemBlock);
				if (result == null) result = caseGANamed(systemBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK: {
				UserDefinedFunctionBlock userDefinedFunctionBlock = (UserDefinedFunctionBlock)theEObject;
				T result = caseUserDefinedFunctionBlock(userDefinedFunctionBlock);
				if (result == null) result = caseBlock(userDefinedFunctionBlock);
				if (result == null) result = caseGASystemModelElement(userDefinedFunctionBlock);
				if (result == null) result = caseContainerNode(userDefinedFunctionBlock);
				if (result == null) result = caseGAModelElement(userDefinedFunctionBlock);
				if (result == null) result = caseNode(userDefinedFunctionBlock);
				if (result == null) result = caseGANamed(userDefinedFunctionBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Diagram Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Diagram Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiagramInfo(DiagramInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Chart Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Chart Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChartBlock(ChartBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Combinatorial Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Combinatorial Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCombinatorialBlock(CombinatorialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlBlock(ControlBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericBlock(GenericBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceBlock(ReferenceBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Routing Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Routing Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoutingBlock(RoutingBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialBlock(SequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sink Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sink Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSinkBlock(SinkBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSourceBlock(SourceBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemBlock(SystemBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Defined Function Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Defined Function Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserDefinedFunctionBlock(UserDefinedFunctionBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGAModelElement(GAModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA System Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA System Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGASystemModelElement(GASystemModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGANamed(GANamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Container Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Container Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainerNode(ContainerNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA System Model Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA System Model Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGASystemModelRoot(GASystemModelRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //BlocksSwitch
