/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks;

import geneauto.emf.models.gacodemodel.Dependency;
import geneauto.emf.models.gacodemodel.GACodeModelElement;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.common.Function_SM;
import geneauto.emf.models.gasystemmodel.common.Variable_SM;

import geneauto.emf.models.gasystemmodel.gastatemodel.ChartRoot;
import geneauto.emf.models.gasystemmodel.gastatemodel.Composition;
import geneauto.emf.models.gasystemmodel.gastatemodel.Event;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Chart Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Special block for Stateflow charts.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExecuteAtInit <em>Execute At Init</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExportFunctions <em>Export Functions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceOuterReference <em>Instance Outer Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInitFunction <em>Init Function</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getActiveEventReference <em>Active Event Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getStateArgInternalRef <em>State Arg Internal Ref</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getContextReference <em>Context Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceStructType <em>Instance Struct Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComputeFunction <em>Compute Function</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getFunctions <em>Functions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getVariables <em>Variables</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getRootLocation <em>Root Location</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComposition <em>Composition</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getEvents <em>Events</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getDefaultEvent <em>Default Event</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getCodeModelElements <em>Code Model Elements</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getExternalDependencies <em>External Dependencies</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock()
 * @model
 * @generated
 */
public interface ChartBlock extends Block {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Execute At Init</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execute At Init</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execute At Init</em>' attribute.
	 * @see #isSetExecuteAtInit()
	 * @see #unsetExecuteAtInit()
	 * @see #setExecuteAtInit(boolean)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_ExecuteAtInit()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isExecuteAtInit();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExecuteAtInit <em>Execute At Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execute At Init</em>' attribute.
	 * @see #isSetExecuteAtInit()
	 * @see #unsetExecuteAtInit()
	 * @see #isExecuteAtInit()
	 * @generated
	 */
	void setExecuteAtInit(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExecuteAtInit <em>Execute At Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExecuteAtInit()
	 * @see #isExecuteAtInit()
	 * @see #setExecuteAtInit(boolean)
	 * @generated
	 */
	void unsetExecuteAtInit();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExecuteAtInit <em>Execute At Init</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Execute At Init</em>' attribute is set.
	 * @see #unsetExecuteAtInit()
	 * @see #isExecuteAtInit()
	 * @see #setExecuteAtInit(boolean)
	 * @generated
	 */
	boolean isSetExecuteAtInit();

	/**
	 * Returns the value of the '<em><b>Export Functions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Functions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Functions</em>' attribute.
	 * @see #isSetExportFunctions()
	 * @see #unsetExportFunctions()
	 * @see #setExportFunctions(boolean)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_ExportFunctions()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isExportFunctions();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExportFunctions <em>Export Functions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Functions</em>' attribute.
	 * @see #isSetExportFunctions()
	 * @see #unsetExportFunctions()
	 * @see #isExportFunctions()
	 * @generated
	 */
	void setExportFunctions(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExportFunctions <em>Export Functions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExportFunctions()
	 * @see #isExportFunctions()
	 * @see #setExportFunctions(boolean)
	 * @generated
	 */
	void unsetExportFunctions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#isExportFunctions <em>Export Functions</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Export Functions</em>' attribute is set.
	 * @see #unsetExportFunctions()
	 * @see #isExportFunctions()
	 * @see #setExportFunctions(boolean)
	 * @generated
	 */
	boolean isSetExportFunctions();

	/**
	 * Returns the value of the '<em><b>Instance Outer Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Outer Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Outer Reference</em>' reference.
	 * @see #isSetInstanceOuterReference()
	 * @see #unsetInstanceOuterReference()
	 * @see #setInstanceOuterReference(Expression)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_InstanceOuterReference()
	 * @model unsettable="true"
	 * @generated
	 */
	Expression getInstanceOuterReference();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceOuterReference <em>Instance Outer Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Outer Reference</em>' reference.
	 * @see #isSetInstanceOuterReference()
	 * @see #unsetInstanceOuterReference()
	 * @see #getInstanceOuterReference()
	 * @generated
	 */
	void setInstanceOuterReference(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceOuterReference <em>Instance Outer Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInstanceOuterReference()
	 * @see #getInstanceOuterReference()
	 * @see #setInstanceOuterReference(Expression)
	 * @generated
	 */
	void unsetInstanceOuterReference();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceOuterReference <em>Instance Outer Reference</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Instance Outer Reference</em>' reference is set.
	 * @see #unsetInstanceOuterReference()
	 * @see #getInstanceOuterReference()
	 * @see #setInstanceOuterReference(Expression)
	 * @generated
	 */
	boolean isSetInstanceOuterReference();

	/**
	 * Returns the value of the '<em><b>Init Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Function</em>' reference.
	 * @see #isSetInitFunction()
	 * @see #unsetInitFunction()
	 * @see #setInitFunction(Function_SM)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_InitFunction()
	 * @model unsettable="true"
	 * @generated
	 */
	Function_SM getInitFunction();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInitFunction <em>Init Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Function</em>' reference.
	 * @see #isSetInitFunction()
	 * @see #unsetInitFunction()
	 * @see #getInitFunction()
	 * @generated
	 */
	void setInitFunction(Function_SM value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInitFunction <em>Init Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInitFunction()
	 * @see #getInitFunction()
	 * @see #setInitFunction(Function_SM)
	 * @generated
	 */
	void unsetInitFunction();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInitFunction <em>Init Function</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Init Function</em>' reference is set.
	 * @see #unsetInitFunction()
	 * @see #getInitFunction()
	 * @see #setInitFunction(Function_SM)
	 * @generated
	 */
	boolean isSetInitFunction();

	/**
	 * Returns the value of the '<em><b>Active Event Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Event Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Event Reference</em>' reference.
	 * @see #isSetActiveEventReference()
	 * @see #unsetActiveEventReference()
	 * @see #setActiveEventReference(Expression)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_ActiveEventReference()
	 * @model unsettable="true"
	 * @generated
	 */
	Expression getActiveEventReference();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getActiveEventReference <em>Active Event Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Event Reference</em>' reference.
	 * @see #isSetActiveEventReference()
	 * @see #unsetActiveEventReference()
	 * @see #getActiveEventReference()
	 * @generated
	 */
	void setActiveEventReference(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getActiveEventReference <em>Active Event Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetActiveEventReference()
	 * @see #getActiveEventReference()
	 * @see #setActiveEventReference(Expression)
	 * @generated
	 */
	void unsetActiveEventReference();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getActiveEventReference <em>Active Event Reference</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Active Event Reference</em>' reference is set.
	 * @see #unsetActiveEventReference()
	 * @see #getActiveEventReference()
	 * @see #setActiveEventReference(Expression)
	 * @generated
	 */
	boolean isSetActiveEventReference();

	/**
	 * Returns the value of the '<em><b>State Arg Internal Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Arg Internal Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Arg Internal Ref</em>' reference.
	 * @see #isSetStateArgInternalRef()
	 * @see #unsetStateArgInternalRef()
	 * @see #setStateArgInternalRef(Expression)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_StateArgInternalRef()
	 * @model unsettable="true"
	 * @generated
	 */
	Expression getStateArgInternalRef();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getStateArgInternalRef <em>State Arg Internal Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Arg Internal Ref</em>' reference.
	 * @see #isSetStateArgInternalRef()
	 * @see #unsetStateArgInternalRef()
	 * @see #getStateArgInternalRef()
	 * @generated
	 */
	void setStateArgInternalRef(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getStateArgInternalRef <em>State Arg Internal Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStateArgInternalRef()
	 * @see #getStateArgInternalRef()
	 * @see #setStateArgInternalRef(Expression)
	 * @generated
	 */
	void unsetStateArgInternalRef();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getStateArgInternalRef <em>State Arg Internal Ref</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>State Arg Internal Ref</em>' reference is set.
	 * @see #unsetStateArgInternalRef()
	 * @see #getStateArgInternalRef()
	 * @see #setStateArgInternalRef(Expression)
	 * @generated
	 */
	boolean isSetStateArgInternalRef();

	/**
	 * Returns the value of the '<em><b>Context Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Reference</em>' reference.
	 * @see #isSetContextReference()
	 * @see #unsetContextReference()
	 * @see #setContextReference(Expression)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_ContextReference()
	 * @model unsettable="true"
	 * @generated
	 */
	Expression getContextReference();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getContextReference <em>Context Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context Reference</em>' reference.
	 * @see #isSetContextReference()
	 * @see #unsetContextReference()
	 * @see #getContextReference()
	 * @generated
	 */
	void setContextReference(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getContextReference <em>Context Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContextReference()
	 * @see #getContextReference()
	 * @see #setContextReference(Expression)
	 * @generated
	 */
	void unsetContextReference();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getContextReference <em>Context Reference</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Context Reference</em>' reference is set.
	 * @see #unsetContextReference()
	 * @see #getContextReference()
	 * @see #setContextReference(Expression)
	 * @generated
	 */
	boolean isSetContextReference();

	/**
	 * Returns the value of the '<em><b>Instance Struct Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Struct Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Struct Type</em>' reference.
	 * @see #isSetInstanceStructType()
	 * @see #unsetInstanceStructType()
	 * @see #setInstanceStructType(GADataType)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_InstanceStructType()
	 * @model unsettable="true"
	 * @generated
	 */
	GADataType getInstanceStructType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceStructType <em>Instance Struct Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Struct Type</em>' reference.
	 * @see #isSetInstanceStructType()
	 * @see #unsetInstanceStructType()
	 * @see #getInstanceStructType()
	 * @generated
	 */
	void setInstanceStructType(GADataType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceStructType <em>Instance Struct Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInstanceStructType()
	 * @see #getInstanceStructType()
	 * @see #setInstanceStructType(GADataType)
	 * @generated
	 */
	void unsetInstanceStructType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getInstanceStructType <em>Instance Struct Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Instance Struct Type</em>' reference is set.
	 * @see #unsetInstanceStructType()
	 * @see #getInstanceStructType()
	 * @see #setInstanceStructType(GADataType)
	 * @generated
	 */
	boolean isSetInstanceStructType();

	/**
	 * Returns the value of the '<em><b>Compute Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compute Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compute Function</em>' reference.
	 * @see #isSetComputeFunction()
	 * @see #unsetComputeFunction()
	 * @see #setComputeFunction(Function_SM)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_ComputeFunction()
	 * @model unsettable="true"
	 * @generated
	 */
	Function_SM getComputeFunction();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComputeFunction <em>Compute Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compute Function</em>' reference.
	 * @see #isSetComputeFunction()
	 * @see #unsetComputeFunction()
	 * @see #getComputeFunction()
	 * @generated
	 */
	void setComputeFunction(Function_SM value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComputeFunction <em>Compute Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetComputeFunction()
	 * @see #getComputeFunction()
	 * @see #setComputeFunction(Function_SM)
	 * @generated
	 */
	void unsetComputeFunction();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComputeFunction <em>Compute Function</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Compute Function</em>' reference is set.
	 * @see #unsetComputeFunction()
	 * @see #getComputeFunction()
	 * @see #setComputeFunction(Function_SM)
	 * @generated
	 */
	boolean isSetComputeFunction();

	/**
	 * Returns the value of the '<em><b>Functions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.Function_SM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functions</em>' containment reference list.
	 * @see #isSetFunctions()
	 * @see #unsetFunctions()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_Functions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Function_SM> getFunctions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getFunctions <em>Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFunctions()
	 * @see #getFunctions()
	 * @generated
	 */
	void unsetFunctions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getFunctions <em>Functions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Functions</em>' containment reference list is set.
	 * @see #unsetFunctions()
	 * @see #getFunctions()
	 * @generated
	 */
	boolean isSetFunctions();

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.Variable_SM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see #isSetVariables()
	 * @see #unsetVariables()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_Variables()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Variable_SM> getVariables();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getVariables <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVariables()
	 * @see #getVariables()
	 * @generated
	 */
	void unsetVariables();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getVariables <em>Variables</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Variables</em>' containment reference list is set.
	 * @see #unsetVariables()
	 * @see #getVariables()
	 * @generated
	 */
	boolean isSetVariables();

	/**
	 * Returns the value of the '<em><b>Root Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Location</em>' containment reference.
	 * @see #isSetRootLocation()
	 * @see #unsetRootLocation()
	 * @see #setRootLocation(ChartRoot)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_RootLocation()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	ChartRoot getRootLocation();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getRootLocation <em>Root Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Location</em>' containment reference.
	 * @see #isSetRootLocation()
	 * @see #unsetRootLocation()
	 * @see #getRootLocation()
	 * @generated
	 */
	void setRootLocation(ChartRoot value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getRootLocation <em>Root Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRootLocation()
	 * @see #getRootLocation()
	 * @see #setRootLocation(ChartRoot)
	 * @generated
	 */
	void unsetRootLocation();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getRootLocation <em>Root Location</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Root Location</em>' containment reference is set.
	 * @see #unsetRootLocation()
	 * @see #getRootLocation()
	 * @see #setRootLocation(ChartRoot)
	 * @generated
	 */
	boolean isSetRootLocation();

	/**
	 * Returns the value of the '<em><b>Composition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composition</em>' containment reference.
	 * @see #isSetComposition()
	 * @see #unsetComposition()
	 * @see #setComposition(Composition)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_Composition()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Composition getComposition();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComposition <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Composition</em>' containment reference.
	 * @see #isSetComposition()
	 * @see #unsetComposition()
	 * @see #getComposition()
	 * @generated
	 */
	void setComposition(Composition value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComposition <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetComposition()
	 * @see #getComposition()
	 * @see #setComposition(Composition)
	 * @generated
	 */
	void unsetComposition();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getComposition <em>Composition</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Composition</em>' containment reference is set.
	 * @see #unsetComposition()
	 * @see #getComposition()
	 * @see #setComposition(Composition)
	 * @generated
	 */
	boolean isSetComposition();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see #isSetEvents()
	 * @see #unsetEvents()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_Events()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getEvents <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	void unsetEvents();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getEvents <em>Events</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Events</em>' containment reference list is set.
	 * @see #unsetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	boolean isSetEvents();

	/**
	 * Returns the value of the '<em><b>Default Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Event</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Event</em>' containment reference.
	 * @see #isSetDefaultEvent()
	 * @see #unsetDefaultEvent()
	 * @see #setDefaultEvent(Event)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_DefaultEvent()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Event getDefaultEvent();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getDefaultEvent <em>Default Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Event</em>' containment reference.
	 * @see #isSetDefaultEvent()
	 * @see #unsetDefaultEvent()
	 * @see #getDefaultEvent()
	 * @generated
	 */
	void setDefaultEvent(Event value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getDefaultEvent <em>Default Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDefaultEvent()
	 * @see #getDefaultEvent()
	 * @see #setDefaultEvent(Event)
	 * @generated
	 */
	void unsetDefaultEvent();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getDefaultEvent <em>Default Event</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Default Event</em>' containment reference is set.
	 * @see #unsetDefaultEvent()
	 * @see #getDefaultEvent()
	 * @see #setDefaultEvent(Event)
	 * @generated
	 */
	boolean isSetDefaultEvent();

	/**
	 * Returns the value of the '<em><b>Code Model Elements</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.GACodeModelElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code Model Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Model Elements</em>' containment reference list.
	 * @see #isSetCodeModelElements()
	 * @see #unsetCodeModelElements()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_CodeModelElements()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<GACodeModelElement> getCodeModelElements();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getCodeModelElements <em>Code Model Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCodeModelElements()
	 * @see #getCodeModelElements()
	 * @generated
	 */
	void unsetCodeModelElements();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getCodeModelElements <em>Code Model Elements</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Code Model Elements</em>' containment reference list is set.
	 * @see #unsetCodeModelElements()
	 * @see #getCodeModelElements()
	 * @generated
	 */
	boolean isSetCodeModelElements();

	/**
	 * Returns the value of the '<em><b>External Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.Dependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Dependencies</em>' containment reference list.
	 * @see #isSetExternalDependencies()
	 * @see #unsetExternalDependencies()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getChartBlock_ExternalDependencies()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Dependency> getExternalDependencies();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getExternalDependencies <em>External Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExternalDependencies()
	 * @see #getExternalDependencies()
	 * @generated
	 */
	void unsetExternalDependencies();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock#getExternalDependencies <em>External Dependencies</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>External Dependencies</em>' containment reference list is set.
	 * @see #unsetExternalDependencies()
	 * @see #getExternalDependencies()
	 * @generated
	 */
	boolean isSetExternalDependencies();

} // ChartBlock
