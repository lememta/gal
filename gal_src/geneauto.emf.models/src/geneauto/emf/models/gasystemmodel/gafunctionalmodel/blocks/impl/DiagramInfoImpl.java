/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Diagram Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl#getPositionX <em>Position X</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl#getPositionY <em>Position Y</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl#getSizeX <em>Size X</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.DiagramInfoImpl#getSizeY <em>Size Y</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DiagramInfoImpl extends EObjectImpl implements DiagramInfo {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getPositionX() <em>Position X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionX()
	 * @generated
	 * @ordered
	 */
	protected static final int POSITION_X_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPositionX() <em>Position X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionX()
	 * @generated
	 * @ordered
	 */
	protected int positionX = POSITION_X_EDEFAULT;

	/**
	 * This is true if the Position X attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean positionXESet;

	/**
	 * The default value of the '{@link #getPositionY() <em>Position Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionY()
	 * @generated
	 * @ordered
	 */
	protected static final int POSITION_Y_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPositionY() <em>Position Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionY()
	 * @generated
	 * @ordered
	 */
	protected int positionY = POSITION_Y_EDEFAULT;

	/**
	 * This is true if the Position Y attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean positionYESet;

	/**
	 * The default value of the '{@link #getSizeX() <em>Size X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizeX()
	 * @generated
	 * @ordered
	 */
	protected static final int SIZE_X_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSizeX() <em>Size X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizeX()
	 * @generated
	 * @ordered
	 */
	protected int sizeX = SIZE_X_EDEFAULT;

	/**
	 * This is true if the Size X attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sizeXESet;

	/**
	 * The default value of the '{@link #getSizeY() <em>Size Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizeY()
	 * @generated
	 * @ordered
	 */
	protected static final int SIZE_Y_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSizeY() <em>Size Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizeY()
	 * @generated
	 * @ordered
	 */
	protected int sizeY = SIZE_Y_EDEFAULT;

	/**
	 * This is true if the Size Y attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sizeYESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiagramInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.DIAGRAM_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPositionX() {
		return positionX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionX(int newPositionX) {
		int oldPositionX = positionX;
		positionX = newPositionX;
		boolean oldPositionXESet = positionXESet;
		positionXESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DIAGRAM_INFO__POSITION_X, oldPositionX, positionX, !oldPositionXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPositionX() {
		int oldPositionX = positionX;
		boolean oldPositionXESet = positionXESet;
		positionX = POSITION_X_EDEFAULT;
		positionXESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.DIAGRAM_INFO__POSITION_X, oldPositionX, POSITION_X_EDEFAULT, oldPositionXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPositionX() {
		return positionXESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPositionY() {
		return positionY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionY(int newPositionY) {
		int oldPositionY = positionY;
		positionY = newPositionY;
		boolean oldPositionYESet = positionYESet;
		positionYESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DIAGRAM_INFO__POSITION_Y, oldPositionY, positionY, !oldPositionYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPositionY() {
		int oldPositionY = positionY;
		boolean oldPositionYESet = positionYESet;
		positionY = POSITION_Y_EDEFAULT;
		positionYESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.DIAGRAM_INFO__POSITION_Y, oldPositionY, POSITION_Y_EDEFAULT, oldPositionYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPositionY() {
		return positionYESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSizeX() {
		return sizeX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSizeX(int newSizeX) {
		int oldSizeX = sizeX;
		sizeX = newSizeX;
		boolean oldSizeXESet = sizeXESet;
		sizeXESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DIAGRAM_INFO__SIZE_X, oldSizeX, sizeX, !oldSizeXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSizeX() {
		int oldSizeX = sizeX;
		boolean oldSizeXESet = sizeXESet;
		sizeX = SIZE_X_EDEFAULT;
		sizeXESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.DIAGRAM_INFO__SIZE_X, oldSizeX, SIZE_X_EDEFAULT, oldSizeXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSizeX() {
		return sizeXESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSizeY() {
		return sizeY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSizeY(int newSizeY) {
		int oldSizeY = sizeY;
		sizeY = newSizeY;
		boolean oldSizeYESet = sizeYESet;
		sizeYESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DIAGRAM_INFO__SIZE_Y, oldSizeY, sizeY, !oldSizeYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSizeY() {
		int oldSizeY = sizeY;
		boolean oldSizeYESet = sizeYESet;
		sizeY = SIZE_Y_EDEFAULT;
		sizeYESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.DIAGRAM_INFO__SIZE_Y, oldSizeY, SIZE_Y_EDEFAULT, oldSizeYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSizeY() {
		return sizeYESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.DIAGRAM_INFO__POSITION_X:
				return getPositionX();
			case BlocksPackage.DIAGRAM_INFO__POSITION_Y:
				return getPositionY();
			case BlocksPackage.DIAGRAM_INFO__SIZE_X:
				return getSizeX();
			case BlocksPackage.DIAGRAM_INFO__SIZE_Y:
				return getSizeY();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.DIAGRAM_INFO__POSITION_X:
				setPositionX((Integer)newValue);
				return;
			case BlocksPackage.DIAGRAM_INFO__POSITION_Y:
				setPositionY((Integer)newValue);
				return;
			case BlocksPackage.DIAGRAM_INFO__SIZE_X:
				setSizeX((Integer)newValue);
				return;
			case BlocksPackage.DIAGRAM_INFO__SIZE_Y:
				setSizeY((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.DIAGRAM_INFO__POSITION_X:
				unsetPositionX();
				return;
			case BlocksPackage.DIAGRAM_INFO__POSITION_Y:
				unsetPositionY();
				return;
			case BlocksPackage.DIAGRAM_INFO__SIZE_X:
				unsetSizeX();
				return;
			case BlocksPackage.DIAGRAM_INFO__SIZE_Y:
				unsetSizeY();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.DIAGRAM_INFO__POSITION_X:
				return isSetPositionX();
			case BlocksPackage.DIAGRAM_INFO__POSITION_Y:
				return isSetPositionY();
			case BlocksPackage.DIAGRAM_INFO__SIZE_X:
				return isSetSizeX();
			case BlocksPackage.DIAGRAM_INFO__SIZE_Y:
				return isSetSizeY();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (positionX: ");
		if (positionXESet) result.append(positionX); else result.append("<unset>");
		result.append(", positionY: ");
		if (positionYESet) result.append(positionY); else result.append("<unset>");
		result.append(", sizeX: ");
		if (sizeXESet) result.append(sizeX); else result.append("<unset>");
		result.append(", sizeY: ");
		if (sizeYESet) result.append(sizeY); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DiagramInfoImpl
