/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.gasystemmodel.common.BlockParameter;
import geneauto.emf.models.gasystemmodel.common.ContainerNode;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A node from the input model describing computation. On the highest level of abstraction the functional model is a graph consisting of Signals and Blocks. Block represents data processing (computation) while signal shows data transfer between two computations.  Block may be either a primitive block implementing directly a specific algorithm (defined in block library) or complex that has its functionality defined by lower-level system model or state model chart.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPriority <em>Assigned Priority</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPrioritySource <em>Assigned Priority Source</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isDirectFeedThrough <em>Direct Feed Through</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getExecutionOrder <em>Execution Order</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isVirtual <em>Is Virtual</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getMaskType <em>Mask Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getSampleTime <em>Sample Time</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getUserDefinedPriority <em>User Defined Priority</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getPortReference <em>Port Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getParameters <em>Parameters</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getDiagramInfo <em>Diagram Info</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInDataPorts <em>In Data Ports</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEnablePort <em>In Enable Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEdgeEnablePort <em>In Edge Enable Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInControlPorts <em>In Control Ports</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutDataPorts <em>Out Data Ports</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutControlPorts <em>Out Control Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock()
 * @model
 * @generated
 */
public interface Block extends GASystemModelElement, ContainerNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Assigned Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assigned Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assigned Priority</em>' attribute.
	 * @see #isSetAssignedPriority()
	 * @see #unsetAssignedPriority()
	 * @see #setAssignedPriority(int)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_AssignedPriority()
	 * @model unsettable="true"
	 * @generated
	 */
	int getAssignedPriority();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPriority <em>Assigned Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigned Priority</em>' attribute.
	 * @see #isSetAssignedPriority()
	 * @see #unsetAssignedPriority()
	 * @see #getAssignedPriority()
	 * @generated
	 */
	void setAssignedPriority(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPriority <em>Assigned Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAssignedPriority()
	 * @see #getAssignedPriority()
	 * @see #setAssignedPriority(int)
	 * @generated
	 */
	void unsetAssignedPriority();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPriority <em>Assigned Priority</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Assigned Priority</em>' attribute is set.
	 * @see #unsetAssignedPriority()
	 * @see #getAssignedPriority()
	 * @see #setAssignedPriority(int)
	 * @generated
	 */
	boolean isSetAssignedPriority();

	/**
	 * Returns the value of the '<em><b>Assigned Priority Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assigned Priority Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assigned Priority Source</em>' attribute.
	 * @see #isSetAssignedPrioritySource()
	 * @see #unsetAssignedPrioritySource()
	 * @see #setAssignedPrioritySource(String)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_AssignedPrioritySource()
	 * @model unsettable="true"
	 * @generated
	 */
	String getAssignedPrioritySource();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPrioritySource <em>Assigned Priority Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigned Priority Source</em>' attribute.
	 * @see #isSetAssignedPrioritySource()
	 * @see #unsetAssignedPrioritySource()
	 * @see #getAssignedPrioritySource()
	 * @generated
	 */
	void setAssignedPrioritySource(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPrioritySource <em>Assigned Priority Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAssignedPrioritySource()
	 * @see #getAssignedPrioritySource()
	 * @see #setAssignedPrioritySource(String)
	 * @generated
	 */
	void unsetAssignedPrioritySource();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getAssignedPrioritySource <em>Assigned Priority Source</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Assigned Priority Source</em>' attribute is set.
	 * @see #unsetAssignedPrioritySource()
	 * @see #getAssignedPrioritySource()
	 * @see #setAssignedPrioritySource(String)
	 * @generated
	 */
	boolean isSetAssignedPrioritySource();

	/**
	 * Returns the value of the '<em><b>Direct Feed Through</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direct Feed Through</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Feed Through</em>' attribute.
	 * @see #isSetDirectFeedThrough()
	 * @see #unsetDirectFeedThrough()
	 * @see #setDirectFeedThrough(boolean)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_DirectFeedThrough()
	 * @model default="true" unsettable="true"
	 * @generated
	 */
	boolean isDirectFeedThrough();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isDirectFeedThrough <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direct Feed Through</em>' attribute.
	 * @see #isSetDirectFeedThrough()
	 * @see #unsetDirectFeedThrough()
	 * @see #isDirectFeedThrough()
	 * @generated
	 */
	void setDirectFeedThrough(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isDirectFeedThrough <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDirectFeedThrough()
	 * @see #isDirectFeedThrough()
	 * @see #setDirectFeedThrough(boolean)
	 * @generated
	 */
	void unsetDirectFeedThrough();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isDirectFeedThrough <em>Direct Feed Through</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Direct Feed Through</em>' attribute is set.
	 * @see #unsetDirectFeedThrough()
	 * @see #isDirectFeedThrough()
	 * @see #setDirectFeedThrough(boolean)
	 * @generated
	 */
	boolean isSetDirectFeedThrough();

	/**
	 * Returns the value of the '<em><b>Execution Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Order</em>' attribute.
	 * @see #isSetExecutionOrder()
	 * @see #unsetExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_ExecutionOrder()
	 * @model unsettable="true"
	 * @generated
	 */
	int getExecutionOrder();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getExecutionOrder <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Order</em>' attribute.
	 * @see #isSetExecutionOrder()
	 * @see #unsetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @generated
	 */
	void setExecutionOrder(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getExecutionOrder <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @generated
	 */
	void unsetExecutionOrder();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getExecutionOrder <em>Execution Order</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Execution Order</em>' attribute is set.
	 * @see #unsetExecutionOrder()
	 * @see #getExecutionOrder()
	 * @see #setExecutionOrder(int)
	 * @generated
	 */
	boolean isSetExecutionOrder();

	/**
	 * Returns the value of the '<em><b>Is Virtual</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Virtual</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Virtual</em>' attribute.
	 * @see #isSetIsVirtual()
	 * @see #unsetIsVirtual()
	 * @see #setIsVirtual(boolean)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_IsVirtual()
	 * @model default="true" unsettable="true"
	 * @generated
	 */
	boolean isVirtual();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isVirtual <em>Is Virtual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Virtual</em>' attribute.
	 * @see #isSetIsVirtual()
	 * @see #unsetIsVirtual()
	 * @see #isVirtual()
	 * @generated
	 */
	void setIsVirtual(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isVirtual <em>Is Virtual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsVirtual()
	 * @see #isVirtual()
	 * @see #setIsVirtual(boolean)
	 * @generated
	 */
	void unsetIsVirtual();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#isVirtual <em>Is Virtual</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Virtual</em>' attribute is set.
	 * @see #unsetIsVirtual()
	 * @see #isVirtual()
	 * @see #setIsVirtual(boolean)
	 * @generated
	 */
	boolean isSetIsVirtual();

	/**
	 * Returns the value of the '<em><b>Mask Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mask Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mask Type</em>' attribute.
	 * @see #isSetMaskType()
	 * @see #unsetMaskType()
	 * @see #setMaskType(String)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_MaskType()
	 * @model unsettable="true"
	 * @generated
	 */
	String getMaskType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getMaskType <em>Mask Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mask Type</em>' attribute.
	 * @see #isSetMaskType()
	 * @see #unsetMaskType()
	 * @see #getMaskType()
	 * @generated
	 */
	void setMaskType(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getMaskType <em>Mask Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMaskType()
	 * @see #getMaskType()
	 * @see #setMaskType(String)
	 * @generated
	 */
	void unsetMaskType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getMaskType <em>Mask Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mask Type</em>' attribute is set.
	 * @see #unsetMaskType()
	 * @see #getMaskType()
	 * @see #setMaskType(String)
	 * @generated
	 */
	boolean isSetMaskType();

	/**
	 * Returns the value of the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sample Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sample Time</em>' attribute.
	 * @see #isSetSampleTime()
	 * @see #unsetSampleTime()
	 * @see #setSampleTime(double)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_SampleTime()
	 * @model unsettable="true"
	 * @generated
	 */
	double getSampleTime();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getSampleTime <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sample Time</em>' attribute.
	 * @see #isSetSampleTime()
	 * @see #unsetSampleTime()
	 * @see #getSampleTime()
	 * @generated
	 */
	void setSampleTime(double value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getSampleTime <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSampleTime()
	 * @see #getSampleTime()
	 * @see #setSampleTime(double)
	 * @generated
	 */
	void unsetSampleTime();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getSampleTime <em>Sample Time</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Sample Time</em>' attribute is set.
	 * @see #unsetSampleTime()
	 * @see #getSampleTime()
	 * @see #setSampleTime(double)
	 * @generated
	 */
	boolean isSetSampleTime();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(String)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_Type()
	 * @model unsettable="true"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(String)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(String)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>User Defined Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Defined Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Defined Priority</em>' attribute.
	 * @see #isSetUserDefinedPriority()
	 * @see #unsetUserDefinedPriority()
	 * @see #setUserDefinedPriority(int)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_UserDefinedPriority()
	 * @model unsettable="true"
	 * @generated
	 */
	int getUserDefinedPriority();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getUserDefinedPriority <em>User Defined Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Defined Priority</em>' attribute.
	 * @see #isSetUserDefinedPriority()
	 * @see #unsetUserDefinedPriority()
	 * @see #getUserDefinedPriority()
	 * @generated
	 */
	void setUserDefinedPriority(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getUserDefinedPriority <em>User Defined Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUserDefinedPriority()
	 * @see #getUserDefinedPriority()
	 * @see #setUserDefinedPriority(int)
	 * @generated
	 */
	void unsetUserDefinedPriority();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getUserDefinedPriority <em>User Defined Priority</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>User Defined Priority</em>' attribute is set.
	 * @see #unsetUserDefinedPriority()
	 * @see #getUserDefinedPriority()
	 * @see #setUserDefinedPriority(int)
	 * @generated
	 */
	boolean isSetUserDefinedPriority();

	/**
	 * Returns the value of the '<em><b>Port Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Reference</em>' reference.
	 * @see #isSetPortReference()
	 * @see #unsetPortReference()
	 * @see #setPortReference(Port)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_PortReference()
	 * @model unsettable="true"
	 * @generated
	 */
	Port getPortReference();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getPortReference <em>Port Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Reference</em>' reference.
	 * @see #isSetPortReference()
	 * @see #unsetPortReference()
	 * @see #getPortReference()
	 * @generated
	 */
	void setPortReference(Port value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getPortReference <em>Port Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPortReference()
	 * @see #getPortReference()
	 * @see #setPortReference(Port)
	 * @generated
	 */
	void unsetPortReference();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getPortReference <em>Port Reference</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Port Reference</em>' reference is set.
	 * @see #unsetPortReference()
	 * @see #getPortReference()
	 * @see #setPortReference(Port)
	 * @generated
	 */
	boolean isSetPortReference();

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.BlockParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see #isSetParameters()
	 * @see #unsetParameters()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_Parameters()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<BlockParameter> getParameters();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getParameters <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParameters()
	 * @see #getParameters()
	 * @generated
	 */
	void unsetParameters();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getParameters <em>Parameters</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parameters</em>' containment reference list is set.
	 * @see #unsetParameters()
	 * @see #getParameters()
	 * @generated
	 */
	boolean isSetParameters();

	/**
	 * Returns the value of the '<em><b>Diagram Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram Info</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram Info</em>' containment reference.
	 * @see #isSetDiagramInfo()
	 * @see #unsetDiagramInfo()
	 * @see #setDiagramInfo(DiagramInfo)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_DiagramInfo()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	DiagramInfo getDiagramInfo();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getDiagramInfo <em>Diagram Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram Info</em>' containment reference.
	 * @see #isSetDiagramInfo()
	 * @see #unsetDiagramInfo()
	 * @see #getDiagramInfo()
	 * @generated
	 */
	void setDiagramInfo(DiagramInfo value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getDiagramInfo <em>Diagram Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDiagramInfo()
	 * @see #getDiagramInfo()
	 * @see #setDiagramInfo(DiagramInfo)
	 * @generated
	 */
	void unsetDiagramInfo();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getDiagramInfo <em>Diagram Info</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Diagram Info</em>' containment reference is set.
	 * @see #unsetDiagramInfo()
	 * @see #getDiagramInfo()
	 * @see #setDiagramInfo(DiagramInfo)
	 * @generated
	 */
	boolean isSetDiagramInfo();

	/**
	 * Returns the value of the '<em><b>In Data Ports</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InDataPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Data Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Data Ports</em>' containment reference list.
	 * @see #isSetInDataPorts()
	 * @see #unsetInDataPorts()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_InDataPorts()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<InDataPort> getInDataPorts();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInDataPorts <em>In Data Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInDataPorts()
	 * @see #getInDataPorts()
	 * @generated
	 */
	void unsetInDataPorts();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInDataPorts <em>In Data Ports</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>In Data Ports</em>' containment reference list is set.
	 * @see #unsetInDataPorts()
	 * @see #getInDataPorts()
	 * @generated
	 */
	boolean isSetInDataPorts();

	/**
	 * Returns the value of the '<em><b>In Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Enable Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Enable Port</em>' containment reference.
	 * @see #isSetInEnablePort()
	 * @see #unsetInEnablePort()
	 * @see #setInEnablePort(InEnablePort)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_InEnablePort()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	InEnablePort getInEnablePort();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEnablePort <em>In Enable Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Enable Port</em>' containment reference.
	 * @see #isSetInEnablePort()
	 * @see #unsetInEnablePort()
	 * @see #getInEnablePort()
	 * @generated
	 */
	void setInEnablePort(InEnablePort value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEnablePort <em>In Enable Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInEnablePort()
	 * @see #getInEnablePort()
	 * @see #setInEnablePort(InEnablePort)
	 * @generated
	 */
	void unsetInEnablePort();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEnablePort <em>In Enable Port</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>In Enable Port</em>' containment reference is set.
	 * @see #unsetInEnablePort()
	 * @see #getInEnablePort()
	 * @see #setInEnablePort(InEnablePort)
	 * @generated
	 */
	boolean isSetInEnablePort();

	/**
	 * Returns the value of the '<em><b>In Edge Enable Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Edge Enable Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Edge Enable Port</em>' containment reference.
	 * @see #isSetInEdgeEnablePort()
	 * @see #unsetInEdgeEnablePort()
	 * @see #setInEdgeEnablePort(InEdgeEnablePort)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_InEdgeEnablePort()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	InEdgeEnablePort getInEdgeEnablePort();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEdgeEnablePort <em>In Edge Enable Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Edge Enable Port</em>' containment reference.
	 * @see #isSetInEdgeEnablePort()
	 * @see #unsetInEdgeEnablePort()
	 * @see #getInEdgeEnablePort()
	 * @generated
	 */
	void setInEdgeEnablePort(InEdgeEnablePort value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEdgeEnablePort <em>In Edge Enable Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInEdgeEnablePort()
	 * @see #getInEdgeEnablePort()
	 * @see #setInEdgeEnablePort(InEdgeEnablePort)
	 * @generated
	 */
	void unsetInEdgeEnablePort();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInEdgeEnablePort <em>In Edge Enable Port</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>In Edge Enable Port</em>' containment reference is set.
	 * @see #unsetInEdgeEnablePort()
	 * @see #getInEdgeEnablePort()
	 * @see #setInEdgeEnablePort(InEdgeEnablePort)
	 * @generated
	 */
	boolean isSetInEdgeEnablePort();

	/**
	 * Returns the value of the '<em><b>In Control Ports</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Control Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Control Ports</em>' containment reference list.
	 * @see #isSetInControlPorts()
	 * @see #unsetInControlPorts()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_InControlPorts()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<InControlPort> getInControlPorts();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInControlPorts <em>In Control Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInControlPorts()
	 * @see #getInControlPorts()
	 * @generated
	 */
	void unsetInControlPorts();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getInControlPorts <em>In Control Ports</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>In Control Ports</em>' containment reference list is set.
	 * @see #unsetInControlPorts()
	 * @see #getInControlPorts()
	 * @generated
	 */
	boolean isSetInControlPorts();

	/**
	 * Returns the value of the '<em><b>Out Data Ports</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Data Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Data Ports</em>' containment reference list.
	 * @see #isSetOutDataPorts()
	 * @see #unsetOutDataPorts()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_OutDataPorts()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<OutDataPort> getOutDataPorts();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutDataPorts <em>Out Data Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOutDataPorts()
	 * @see #getOutDataPorts()
	 * @generated
	 */
	void unsetOutDataPorts();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutDataPorts <em>Out Data Ports</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Out Data Ports</em>' containment reference list is set.
	 * @see #unsetOutDataPorts()
	 * @see #getOutDataPorts()
	 * @generated
	 */
	boolean isSetOutDataPorts();

	/**
	 * Returns the value of the '<em><b>Out Control Ports</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Control Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Control Ports</em>' containment reference list.
	 * @see #isSetOutControlPorts()
	 * @see #unsetOutControlPorts()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getBlock_OutControlPorts()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<OutControlPort> getOutControlPorts();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutControlPorts <em>Out Control Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOutControlPorts()
	 * @see #getOutControlPorts()
	 * @generated
	 */
	void unsetOutControlPorts();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block#getOutControlPorts <em>Out Control Ports</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Out Control Ports</em>' containment reference list is set.
	 * @see #unsetOutControlPorts()
	 * @see #getOutControlPorts()
	 * @generated
	 */
	boolean isSetOutControlPorts();

} // Block
