/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.common.impl.DataImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.GafunctionalmodelPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Inport;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Outport;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl#getObservationPoints <em>Observation Points</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl#getSampleTime <em>Sample Time</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl#getDstPort <em>Dst Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl#getSrcPort <em>Src Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl#getDataType <em>Data Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SignalImpl extends DataImpl implements Signal {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getObservationPoints() <em>Observation Points</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservationPoints()
	 * @generated
	 * @ordered
	 */
	protected EList<String> observationPoints;

	/**
	 * The default value of the '{@link #getSampleTime() <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampleTime()
	 * @generated
	 * @ordered
	 */
	protected static final int SAMPLE_TIME_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSampleTime() <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampleTime()
	 * @generated
	 * @ordered
	 */
	protected int sampleTime = SAMPLE_TIME_EDEFAULT;

	/**
	 * This is true if the Sample Time attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sampleTimeESet;

	/**
	 * The cached value of the '{@link #getDstPort() <em>Dst Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDstPort()
	 * @generated
	 * @ordered
	 */
	protected Inport dstPort;

	/**
	 * This is true if the Dst Port reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dstPortESet;

	/**
	 * The cached value of the '{@link #getSrcPort() <em>Src Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcPort()
	 * @generated
	 * @ordered
	 */
	protected Outport srcPort;

	/**
	 * This is true if the Src Port reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean srcPortESet;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected GADataType dataType;

	/**
	 * This is true if the Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dataTypeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GafunctionalmodelPackage.Literals.SIGNAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getObservationPoints() {
		if (observationPoints == null) {
			observationPoints = new EDataTypeUniqueEList.Unsettable<String>(String.class, this, GafunctionalmodelPackage.SIGNAL__OBSERVATION_POINTS);
		}
		return observationPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObservationPoints() {
		if (observationPoints != null) ((InternalEList.Unsettable<?>)observationPoints).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObservationPoints() {
		return observationPoints != null && ((InternalEList.Unsettable<?>)observationPoints).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSampleTime() {
		return sampleTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSampleTime(int newSampleTime) {
		int oldSampleTime = sampleTime;
		sampleTime = newSampleTime;
		boolean oldSampleTimeESet = sampleTimeESet;
		sampleTimeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GafunctionalmodelPackage.SIGNAL__SAMPLE_TIME, oldSampleTime, sampleTime, !oldSampleTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSampleTime() {
		int oldSampleTime = sampleTime;
		boolean oldSampleTimeESet = sampleTimeESet;
		sampleTime = SAMPLE_TIME_EDEFAULT;
		sampleTimeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GafunctionalmodelPackage.SIGNAL__SAMPLE_TIME, oldSampleTime, SAMPLE_TIME_EDEFAULT, oldSampleTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSampleTime() {
		return sampleTimeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Inport getDstPort() {
		if (dstPort != null && dstPort.eIsProxy()) {
			InternalEObject oldDstPort = (InternalEObject)dstPort;
			dstPort = (Inport)eResolveProxy(oldDstPort);
			if (dstPort != oldDstPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GafunctionalmodelPackage.SIGNAL__DST_PORT, oldDstPort, dstPort));
			}
		}
		return dstPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Inport basicGetDstPort() {
		return dstPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDstPort(Inport newDstPort) {
		Inport oldDstPort = dstPort;
		dstPort = newDstPort;
		boolean oldDstPortESet = dstPortESet;
		dstPortESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GafunctionalmodelPackage.SIGNAL__DST_PORT, oldDstPort, dstPort, !oldDstPortESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDstPort() {
		Inport oldDstPort = dstPort;
		boolean oldDstPortESet = dstPortESet;
		dstPort = null;
		dstPortESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GafunctionalmodelPackage.SIGNAL__DST_PORT, oldDstPort, null, oldDstPortESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDstPort() {
		return dstPortESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Outport getSrcPort() {
		if (srcPort != null && srcPort.eIsProxy()) {
			InternalEObject oldSrcPort = (InternalEObject)srcPort;
			srcPort = (Outport)eResolveProxy(oldSrcPort);
			if (srcPort != oldSrcPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GafunctionalmodelPackage.SIGNAL__SRC_PORT, oldSrcPort, srcPort));
			}
		}
		return srcPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Outport basicGetSrcPort() {
		return srcPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSrcPort(Outport newSrcPort) {
		Outport oldSrcPort = srcPort;
		srcPort = newSrcPort;
		boolean oldSrcPortESet = srcPortESet;
		srcPortESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GafunctionalmodelPackage.SIGNAL__SRC_PORT, oldSrcPort, srcPort, !oldSrcPortESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSrcPort() {
		Outport oldSrcPort = srcPort;
		boolean oldSrcPortESet = srcPortESet;
		srcPort = null;
		srcPortESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GafunctionalmodelPackage.SIGNAL__SRC_PORT, oldSrcPort, null, oldSrcPortESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSrcPort() {
		return srcPortESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataType(GADataType newDataType, NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = newDataType;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GafunctionalmodelPackage.SIGNAL__DATA_TYPE, oldDataType, newDataType, !oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(GADataType newDataType) {
		if (newDataType != dataType) {
			NotificationChain msgs = null;
			if (dataType != null)
				msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GafunctionalmodelPackage.SIGNAL__DATA_TYPE, null, msgs);
			if (newDataType != null)
				msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GafunctionalmodelPackage.SIGNAL__DATA_TYPE, null, msgs);
			msgs = basicSetDataType(newDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GafunctionalmodelPackage.SIGNAL__DATA_TYPE, newDataType, newDataType, !oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDataType(NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = null;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GafunctionalmodelPackage.SIGNAL__DATA_TYPE, oldDataType, null, oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDataType() {
		if (dataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GafunctionalmodelPackage.SIGNAL__DATA_TYPE, null, msgs);
			msgs = basicUnsetDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GafunctionalmodelPackage.SIGNAL__DATA_TYPE, null, null, oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDataType() {
		return dataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GafunctionalmodelPackage.SIGNAL__DATA_TYPE:
				return basicUnsetDataType(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GafunctionalmodelPackage.SIGNAL__OBSERVATION_POINTS:
				return getObservationPoints();
			case GafunctionalmodelPackage.SIGNAL__SAMPLE_TIME:
				return getSampleTime();
			case GafunctionalmodelPackage.SIGNAL__DST_PORT:
				if (resolve) return getDstPort();
				return basicGetDstPort();
			case GafunctionalmodelPackage.SIGNAL__SRC_PORT:
				if (resolve) return getSrcPort();
				return basicGetSrcPort();
			case GafunctionalmodelPackage.SIGNAL__DATA_TYPE:
				return getDataType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GafunctionalmodelPackage.SIGNAL__OBSERVATION_POINTS:
				getObservationPoints().clear();
				getObservationPoints().addAll((Collection<? extends String>)newValue);
				return;
			case GafunctionalmodelPackage.SIGNAL__SAMPLE_TIME:
				setSampleTime((Integer)newValue);
				return;
			case GafunctionalmodelPackage.SIGNAL__DST_PORT:
				setDstPort((Inport)newValue);
				return;
			case GafunctionalmodelPackage.SIGNAL__SRC_PORT:
				setSrcPort((Outport)newValue);
				return;
			case GafunctionalmodelPackage.SIGNAL__DATA_TYPE:
				setDataType((GADataType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GafunctionalmodelPackage.SIGNAL__OBSERVATION_POINTS:
				unsetObservationPoints();
				return;
			case GafunctionalmodelPackage.SIGNAL__SAMPLE_TIME:
				unsetSampleTime();
				return;
			case GafunctionalmodelPackage.SIGNAL__DST_PORT:
				unsetDstPort();
				return;
			case GafunctionalmodelPackage.SIGNAL__SRC_PORT:
				unsetSrcPort();
				return;
			case GafunctionalmodelPackage.SIGNAL__DATA_TYPE:
				unsetDataType();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GafunctionalmodelPackage.SIGNAL__OBSERVATION_POINTS:
				return isSetObservationPoints();
			case GafunctionalmodelPackage.SIGNAL__SAMPLE_TIME:
				return isSetSampleTime();
			case GafunctionalmodelPackage.SIGNAL__DST_PORT:
				return isSetDstPort();
			case GafunctionalmodelPackage.SIGNAL__SRC_PORT:
				return isSetSrcPort();
			case GafunctionalmodelPackage.SIGNAL__DATA_TYPE:
				return isSetDataType();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (observationPoints: ");
		result.append(observationPoints);
		result.append(", sampleTime: ");
		if (sampleTimeESet) result.append(sampleTime); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //SignalImpl
