/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Defined Function Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.UserDefinedFunctionBlockImpl#getFunctionDefinition <em>Function Definition</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.UserDefinedFunctionBlockImpl#getHeaderFile <em>Header File</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UserDefinedFunctionBlockImpl extends BlockImpl implements UserDefinedFunctionBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getFunctionDefinition() <em>Function Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionDefinition()
	 * @generated
	 * @ordered
	 */
	protected static final String FUNCTION_DEFINITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFunctionDefinition() <em>Function Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionDefinition()
	 * @generated
	 * @ordered
	 */
	protected String functionDefinition = FUNCTION_DEFINITION_EDEFAULT;

	/**
	 * This is true if the Function Definition attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean functionDefinitionESet;

	/**
	 * The default value of the '{@link #getHeaderFile() <em>Header File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFile()
	 * @generated
	 * @ordered
	 */
	protected static final String HEADER_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeaderFile() <em>Header File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFile()
	 * @generated
	 * @ordered
	 */
	protected String headerFile = HEADER_FILE_EDEFAULT;

	/**
	 * This is true if the Header File attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean headerFileESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserDefinedFunctionBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.USER_DEFINED_FUNCTION_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFunctionDefinition() {
		return functionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionDefinition(String newFunctionDefinition) {
		String oldFunctionDefinition = functionDefinition;
		functionDefinition = newFunctionDefinition;
		boolean oldFunctionDefinitionESet = functionDefinitionESet;
		functionDefinitionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION, oldFunctionDefinition, functionDefinition, !oldFunctionDefinitionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFunctionDefinition() {
		String oldFunctionDefinition = functionDefinition;
		boolean oldFunctionDefinitionESet = functionDefinitionESet;
		functionDefinition = FUNCTION_DEFINITION_EDEFAULT;
		functionDefinitionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION, oldFunctionDefinition, FUNCTION_DEFINITION_EDEFAULT, oldFunctionDefinitionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFunctionDefinition() {
		return functionDefinitionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHeaderFile() {
		return headerFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeaderFile(String newHeaderFile) {
		String oldHeaderFile = headerFile;
		headerFile = newHeaderFile;
		boolean oldHeaderFileESet = headerFileESet;
		headerFileESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE, oldHeaderFile, headerFile, !oldHeaderFileESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHeaderFile() {
		String oldHeaderFile = headerFile;
		boolean oldHeaderFileESet = headerFileESet;
		headerFile = HEADER_FILE_EDEFAULT;
		headerFileESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE, oldHeaderFile, HEADER_FILE_EDEFAULT, oldHeaderFileESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHeaderFile() {
		return headerFileESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION:
				return getFunctionDefinition();
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE:
				return getHeaderFile();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION:
				setFunctionDefinition((String)newValue);
				return;
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE:
				setHeaderFile((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION:
				unsetFunctionDefinition();
				return;
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE:
				unsetHeaderFile();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__FUNCTION_DEFINITION:
				return isSetFunctionDefinition();
			case BlocksPackage.USER_DEFINED_FUNCTION_BLOCK__HEADER_FILE:
				return isSetHeaderFile();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (functionDefinition: ");
		if (functionDefinitionESet) result.append(functionDefinition); else result.append("<unset>");
		result.append(", headerFile: ");
		if (headerFileESet) result.append(headerFile); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //UserDefinedFunctionBlockImpl
