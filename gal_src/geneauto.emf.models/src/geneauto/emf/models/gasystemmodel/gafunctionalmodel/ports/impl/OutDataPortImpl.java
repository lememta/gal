/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.common.Variable_SM;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out Data Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl#isResetOutput <em>Reset Output</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl#getVariable <em>Variable</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl#getInitialOutput <em>Initial Output</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OutDataPortImpl extends OutportImpl implements OutDataPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #isResetOutput() <em>Reset Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResetOutput()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESET_OUTPUT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResetOutput() <em>Reset Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResetOutput()
	 * @generated
	 * @ordered
	 */
	protected boolean resetOutput = RESET_OUTPUT_EDEFAULT;

	/**
	 * This is true if the Reset Output attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean resetOutputESet;

	/**
	 * The cached value of the '{@link #getVariable() <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable_SM variable;

	/**
	 * This is true if the Variable reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean variableESet;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected GADataType dataType;

	/**
	 * This is true if the Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dataTypeESet;

	/**
	 * The cached value of the '{@link #getInitialOutput() <em>Initial Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialOutput()
	 * @generated
	 * @ordered
	 */
	protected Expression initialOutput;

	/**
	 * This is true if the Initial Output containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initialOutputESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutDataPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortsPackage.Literals.OUT_DATA_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResetOutput() {
		return resetOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResetOutput(boolean newResetOutput) {
		boolean oldResetOutput = resetOutput;
		resetOutput = newResetOutput;
		boolean oldResetOutputESet = resetOutputESet;
		resetOutputESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.OUT_DATA_PORT__RESET_OUTPUT, oldResetOutput, resetOutput, !oldResetOutputESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResetOutput() {
		boolean oldResetOutput = resetOutput;
		boolean oldResetOutputESet = resetOutputESet;
		resetOutput = RESET_OUTPUT_EDEFAULT;
		resetOutputESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.OUT_DATA_PORT__RESET_OUTPUT, oldResetOutput, RESET_OUTPUT_EDEFAULT, oldResetOutputESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResetOutput() {
		return resetOutputESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable_SM getVariable() {
		if (variable != null && variable.eIsProxy()) {
			InternalEObject oldVariable = (InternalEObject)variable;
			variable = (Variable_SM)eResolveProxy(oldVariable);
			if (variable != oldVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PortsPackage.OUT_DATA_PORT__VARIABLE, oldVariable, variable));
			}
		}
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable_SM basicGetVariable() {
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariable(Variable_SM newVariable) {
		Variable_SM oldVariable = variable;
		variable = newVariable;
		boolean oldVariableESet = variableESet;
		variableESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.OUT_DATA_PORT__VARIABLE, oldVariable, variable, !oldVariableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVariable() {
		Variable_SM oldVariable = variable;
		boolean oldVariableESet = variableESet;
		variable = null;
		variableESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.OUT_DATA_PORT__VARIABLE, oldVariable, null, oldVariableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVariable() {
		return variableESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataType(GADataType newDataType, NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = newDataType;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortsPackage.OUT_DATA_PORT__DATA_TYPE, oldDataType, newDataType, !oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(GADataType newDataType) {
		if (newDataType != dataType) {
			NotificationChain msgs = null;
			if (dataType != null)
				msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.OUT_DATA_PORT__DATA_TYPE, null, msgs);
			if (newDataType != null)
				msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PortsPackage.OUT_DATA_PORT__DATA_TYPE, null, msgs);
			msgs = basicSetDataType(newDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.OUT_DATA_PORT__DATA_TYPE, newDataType, newDataType, !oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDataType(NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = null;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, PortsPackage.OUT_DATA_PORT__DATA_TYPE, oldDataType, null, oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDataType() {
		if (dataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.OUT_DATA_PORT__DATA_TYPE, null, msgs);
			msgs = basicUnsetDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.OUT_DATA_PORT__DATA_TYPE, null, null, oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDataType() {
		return dataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInitialOutput() {
		return initialOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitialOutput(Expression newInitialOutput, NotificationChain msgs) {
		Expression oldInitialOutput = initialOutput;
		initialOutput = newInitialOutput;
		boolean oldInitialOutputESet = initialOutputESet;
		initialOutputESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT, oldInitialOutput, newInitialOutput, !oldInitialOutputESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialOutput(Expression newInitialOutput) {
		if (newInitialOutput != initialOutput) {
			NotificationChain msgs = null;
			if (initialOutput != null)
				msgs = ((InternalEObject)initialOutput).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT, null, msgs);
			if (newInitialOutput != null)
				msgs = ((InternalEObject)newInitialOutput).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT, null, msgs);
			msgs = basicSetInitialOutput(newInitialOutput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInitialOutputESet = initialOutputESet;
			initialOutputESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT, newInitialOutput, newInitialOutput, !oldInitialOutputESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInitialOutput(NotificationChain msgs) {
		Expression oldInitialOutput = initialOutput;
		initialOutput = null;
		boolean oldInitialOutputESet = initialOutputESet;
		initialOutputESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT, oldInitialOutput, null, oldInitialOutputESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitialOutput() {
		if (initialOutput != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)initialOutput).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT, null, msgs);
			msgs = basicUnsetInitialOutput(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInitialOutputESet = initialOutputESet;
			initialOutputESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT, null, null, oldInitialOutputESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitialOutput() {
		return initialOutputESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortsPackage.OUT_DATA_PORT__DATA_TYPE:
				return basicUnsetDataType(msgs);
			case PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT:
				return basicUnsetInitialOutput(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortsPackage.OUT_DATA_PORT__RESET_OUTPUT:
				return isResetOutput();
			case PortsPackage.OUT_DATA_PORT__VARIABLE:
				if (resolve) return getVariable();
				return basicGetVariable();
			case PortsPackage.OUT_DATA_PORT__DATA_TYPE:
				return getDataType();
			case PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT:
				return getInitialOutput();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortsPackage.OUT_DATA_PORT__RESET_OUTPUT:
				setResetOutput((Boolean)newValue);
				return;
			case PortsPackage.OUT_DATA_PORT__VARIABLE:
				setVariable((Variable_SM)newValue);
				return;
			case PortsPackage.OUT_DATA_PORT__DATA_TYPE:
				setDataType((GADataType)newValue);
				return;
			case PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT:
				setInitialOutput((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortsPackage.OUT_DATA_PORT__RESET_OUTPUT:
				unsetResetOutput();
				return;
			case PortsPackage.OUT_DATA_PORT__VARIABLE:
				unsetVariable();
				return;
			case PortsPackage.OUT_DATA_PORT__DATA_TYPE:
				unsetDataType();
				return;
			case PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT:
				unsetInitialOutput();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortsPackage.OUT_DATA_PORT__RESET_OUTPUT:
				return isSetResetOutput();
			case PortsPackage.OUT_DATA_PORT__VARIABLE:
				return isSetVariable();
			case PortsPackage.OUT_DATA_PORT__DATA_TYPE:
				return isSetDataType();
			case PortsPackage.OUT_DATA_PORT__INITIAL_OUTPUT:
				return isSetInitialOutput();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (resetOutput: ");
		if (resetOutputESet) result.append(resetOutput); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //OutDataPortImpl
