/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.emf.models.gacodemodel.statement.Statement;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.gastatemodel.Event;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Control Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Input port accepting control signals. Control signal represents event-based block activation in the GAFMLaguage.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isPeriodicSampleTime <em>Periodic Sample Time</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isResetStates <em>Reset States</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getSampleTime <em>Sample Time</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getComputeCode <em>Compute Code</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getEvent <em>Event</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getOutputDataType <em>Output Data Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInControlPort()
 * @model
 * @generated
 */
public interface InControlPort extends Inport {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Periodic Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Periodic Sample Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Periodic Sample Time</em>' attribute.
	 * @see #isSetPeriodicSampleTime()
	 * @see #unsetPeriodicSampleTime()
	 * @see #setPeriodicSampleTime(boolean)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInControlPort_PeriodicSampleTime()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isPeriodicSampleTime();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isPeriodicSampleTime <em>Periodic Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Periodic Sample Time</em>' attribute.
	 * @see #isSetPeriodicSampleTime()
	 * @see #unsetPeriodicSampleTime()
	 * @see #isPeriodicSampleTime()
	 * @generated
	 */
	void setPeriodicSampleTime(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isPeriodicSampleTime <em>Periodic Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPeriodicSampleTime()
	 * @see #isPeriodicSampleTime()
	 * @see #setPeriodicSampleTime(boolean)
	 * @generated
	 */
	void unsetPeriodicSampleTime();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isPeriodicSampleTime <em>Periodic Sample Time</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Periodic Sample Time</em>' attribute is set.
	 * @see #unsetPeriodicSampleTime()
	 * @see #isPeriodicSampleTime()
	 * @see #setPeriodicSampleTime(boolean)
	 * @generated
	 */
	boolean isSetPeriodicSampleTime();

	/**
	 * Returns the value of the '<em><b>Reset States</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reset States</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reset States</em>' attribute.
	 * @see #isSetResetStates()
	 * @see #unsetResetStates()
	 * @see #setResetStates(boolean)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInControlPort_ResetStates()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isResetStates();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isResetStates <em>Reset States</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reset States</em>' attribute.
	 * @see #isSetResetStates()
	 * @see #unsetResetStates()
	 * @see #isResetStates()
	 * @generated
	 */
	void setResetStates(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isResetStates <em>Reset States</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResetStates()
	 * @see #isResetStates()
	 * @see #setResetStates(boolean)
	 * @generated
	 */
	void unsetResetStates();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isResetStates <em>Reset States</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Reset States</em>' attribute is set.
	 * @see #unsetResetStates()
	 * @see #isResetStates()
	 * @see #setResetStates(boolean)
	 * @generated
	 */
	boolean isSetResetStates();

	/**
	 * Returns the value of the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sample Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sample Time</em>' attribute.
	 * @see #isSetSampleTime()
	 * @see #unsetSampleTime()
	 * @see #setSampleTime(int)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInControlPort_SampleTime()
	 * @model unsettable="true"
	 * @generated
	 */
	int getSampleTime();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getSampleTime <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sample Time</em>' attribute.
	 * @see #isSetSampleTime()
	 * @see #unsetSampleTime()
	 * @see #getSampleTime()
	 * @generated
	 */
	void setSampleTime(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getSampleTime <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSampleTime()
	 * @see #getSampleTime()
	 * @see #setSampleTime(int)
	 * @generated
	 */
	void unsetSampleTime();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getSampleTime <em>Sample Time</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Sample Time</em>' attribute is set.
	 * @see #unsetSampleTime()
	 * @see #getSampleTime()
	 * @see #setSampleTime(int)
	 * @generated
	 */
	boolean isSetSampleTime();

	/**
	 * Returns the value of the '<em><b>Compute Code</b></em>' reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.statement.Statement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compute Code</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compute Code</em>' reference list.
	 * @see #isSetComputeCode()
	 * @see #unsetComputeCode()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInControlPort_ComputeCode()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<Statement> getComputeCode();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getComputeCode <em>Compute Code</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetComputeCode()
	 * @see #getComputeCode()
	 * @generated
	 */
	void unsetComputeCode();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getComputeCode <em>Compute Code</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Compute Code</em>' reference list is set.
	 * @see #unsetComputeCode()
	 * @see #getComputeCode()
	 * @generated
	 */
	boolean isSetComputeCode();

	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #isSetEvent()
	 * @see #unsetEvent()
	 * @see #setEvent(Event)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInControlPort_Event()
	 * @model unsettable="true"
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #isSetEvent()
	 * @see #unsetEvent()
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEvent()
	 * @see #getEvent()
	 * @see #setEvent(Event)
	 * @generated
	 */
	void unsetEvent();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getEvent <em>Event</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Event</em>' reference is set.
	 * @see #unsetEvent()
	 * @see #getEvent()
	 * @see #setEvent(Event)
	 * @generated
	 */
	boolean isSetEvent();

	/**
	 * Returns the value of the '<em><b>Output Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Data Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Data Type</em>' containment reference.
	 * @see #isSetOutputDataType()
	 * @see #unsetOutputDataType()
	 * @see #setOutputDataType(GADataType)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInControlPort_OutputDataType()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	GADataType getOutputDataType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getOutputDataType <em>Output Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Data Type</em>' containment reference.
	 * @see #isSetOutputDataType()
	 * @see #unsetOutputDataType()
	 * @see #getOutputDataType()
	 * @generated
	 */
	void setOutputDataType(GADataType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getOutputDataType <em>Output Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOutputDataType()
	 * @see #getOutputDataType()
	 * @see #setOutputDataType(GADataType)
	 * @generated
	 */
	void unsetOutputDataType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getOutputDataType <em>Output Data Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Output Data Type</em>' containment reference is set.
	 * @see #unsetOutputDataType()
	 * @see #getOutputDataType()
	 * @see #setOutputDataType(GADataType)
	 * @generated
	 */
	boolean isSetOutputDataType();

} // InControlPort
