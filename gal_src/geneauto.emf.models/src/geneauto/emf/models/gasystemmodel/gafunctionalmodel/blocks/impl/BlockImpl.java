/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.gasystemmodel.common.BlockParameter;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port;

import geneauto.emf.models.gasystemmodel.impl.GASystemModelElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getAssignedPriority <em>Assigned Priority</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getAssignedPrioritySource <em>Assigned Priority Source</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#isDirectFeedThrough <em>Direct Feed Through</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getExecutionOrder <em>Execution Order</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#isVirtual <em>Is Virtual</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getMaskType <em>Mask Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getSampleTime <em>Sample Time</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getUserDefinedPriority <em>User Defined Priority</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getPortReference <em>Port Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getDiagramInfo <em>Diagram Info</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getInDataPorts <em>In Data Ports</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getInEnablePort <em>In Enable Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getInEdgeEnablePort <em>In Edge Enable Port</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getInControlPorts <em>In Control Ports</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getOutDataPorts <em>Out Data Ports</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlockImpl#getOutControlPorts <em>Out Control Ports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BlockImpl extends GASystemModelElementImpl implements Block {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getAssignedPriority() <em>Assigned Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int ASSIGNED_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAssignedPriority() <em>Assigned Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedPriority()
	 * @generated
	 * @ordered
	 */
	protected int assignedPriority = ASSIGNED_PRIORITY_EDEFAULT;

	/**
	 * This is true if the Assigned Priority attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean assignedPriorityESet;

	/**
	 * The default value of the '{@link #getAssignedPrioritySource() <em>Assigned Priority Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedPrioritySource()
	 * @generated
	 * @ordered
	 */
	protected static final String ASSIGNED_PRIORITY_SOURCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAssignedPrioritySource() <em>Assigned Priority Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedPrioritySource()
	 * @generated
	 * @ordered
	 */
	protected String assignedPrioritySource = ASSIGNED_PRIORITY_SOURCE_EDEFAULT;

	/**
	 * This is true if the Assigned Priority Source attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean assignedPrioritySourceESet;

	/**
	 * The default value of the '{@link #isDirectFeedThrough() <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirectFeedThrough()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DIRECT_FEED_THROUGH_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isDirectFeedThrough() <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirectFeedThrough()
	 * @generated
	 * @ordered
	 */
	protected boolean directFeedThrough = DIRECT_FEED_THROUGH_EDEFAULT;

	/**
	 * This is true if the Direct Feed Through attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean directFeedThroughESet;

	/**
	 * The default value of the '{@link #getExecutionOrder() <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionOrder()
	 * @generated
	 * @ordered
	 */
	protected static final int EXECUTION_ORDER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExecutionOrder() <em>Execution Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionOrder()
	 * @generated
	 * @ordered
	 */
	protected int executionOrder = EXECUTION_ORDER_EDEFAULT;

	/**
	 * This is true if the Execution Order attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean executionOrderESet;

	/**
	 * The default value of the '{@link #isVirtual() <em>Is Virtual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVirtual()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_VIRTUAL_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isVirtual() <em>Is Virtual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVirtual()
	 * @generated
	 * @ordered
	 */
	protected boolean isVirtual = IS_VIRTUAL_EDEFAULT;

	/**
	 * This is true if the Is Virtual attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isVirtualESet;

	/**
	 * The default value of the '{@link #getMaskType() <em>Mask Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaskType()
	 * @generated
	 * @ordered
	 */
	protected static final String MASK_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaskType() <em>Mask Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaskType()
	 * @generated
	 * @ordered
	 */
	protected String maskType = MASK_TYPE_EDEFAULT;

	/**
	 * This is true if the Mask Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean maskTypeESet;

	/**
	 * The default value of the '{@link #getSampleTime() <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampleTime()
	 * @generated
	 * @ordered
	 */
	protected static final double SAMPLE_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getSampleTime() <em>Sample Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampleTime()
	 * @generated
	 * @ordered
	 */
	protected double sampleTime = SAMPLE_TIME_EDEFAULT;

	/**
	 * This is true if the Sample Time attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sampleTimeESet;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * This is true if the Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * The default value of the '{@link #getUserDefinedPriority() <em>User Defined Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserDefinedPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int USER_DEFINED_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getUserDefinedPriority() <em>User Defined Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserDefinedPriority()
	 * @generated
	 * @ordered
	 */
	protected int userDefinedPriority = USER_DEFINED_PRIORITY_EDEFAULT;

	/**
	 * This is true if the User Defined Priority attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean userDefinedPriorityESet;

	/**
	 * The cached value of the '{@link #getPortReference() <em>Port Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortReference()
	 * @generated
	 * @ordered
	 */
	protected Port portReference;

	/**
	 * This is true if the Port Reference reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean portReferenceESet;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<BlockParameter> parameters;

	/**
	 * The cached value of the '{@link #getDiagramInfo() <em>Diagram Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagramInfo()
	 * @generated
	 * @ordered
	 */
	protected DiagramInfo diagramInfo;

	/**
	 * This is true if the Diagram Info containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean diagramInfoESet;

	/**
	 * The cached value of the '{@link #getInDataPorts() <em>In Data Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInDataPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<InDataPort> inDataPorts;

	/**
	 * The cached value of the '{@link #getInEnablePort() <em>In Enable Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInEnablePort()
	 * @generated
	 * @ordered
	 */
	protected InEnablePort inEnablePort;

	/**
	 * This is true if the In Enable Port containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean inEnablePortESet;

	/**
	 * The cached value of the '{@link #getInEdgeEnablePort() <em>In Edge Enable Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInEdgeEnablePort()
	 * @generated
	 * @ordered
	 */
	protected InEdgeEnablePort inEdgeEnablePort;

	/**
	 * This is true if the In Edge Enable Port containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean inEdgeEnablePortESet;

	/**
	 * The cached value of the '{@link #getInControlPorts() <em>In Control Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInControlPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<InControlPort> inControlPorts;

	/**
	 * The cached value of the '{@link #getOutDataPorts() <em>Out Data Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutDataPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<OutDataPort> outDataPorts;

	/**
	 * The cached value of the '{@link #getOutControlPorts() <em>Out Control Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutControlPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<OutControlPort> outControlPorts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAssignedPriority() {
		return assignedPriority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignedPriority(int newAssignedPriority) {
		int oldAssignedPriority = assignedPriority;
		assignedPriority = newAssignedPriority;
		boolean oldAssignedPriorityESet = assignedPriorityESet;
		assignedPriorityESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__ASSIGNED_PRIORITY, oldAssignedPriority, assignedPriority, !oldAssignedPriorityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAssignedPriority() {
		int oldAssignedPriority = assignedPriority;
		boolean oldAssignedPriorityESet = assignedPriorityESet;
		assignedPriority = ASSIGNED_PRIORITY_EDEFAULT;
		assignedPriorityESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__ASSIGNED_PRIORITY, oldAssignedPriority, ASSIGNED_PRIORITY_EDEFAULT, oldAssignedPriorityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAssignedPriority() {
		return assignedPriorityESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAssignedPrioritySource() {
		return assignedPrioritySource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignedPrioritySource(String newAssignedPrioritySource) {
		String oldAssignedPrioritySource = assignedPrioritySource;
		assignedPrioritySource = newAssignedPrioritySource;
		boolean oldAssignedPrioritySourceESet = assignedPrioritySourceESet;
		assignedPrioritySourceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__ASSIGNED_PRIORITY_SOURCE, oldAssignedPrioritySource, assignedPrioritySource, !oldAssignedPrioritySourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAssignedPrioritySource() {
		String oldAssignedPrioritySource = assignedPrioritySource;
		boolean oldAssignedPrioritySourceESet = assignedPrioritySourceESet;
		assignedPrioritySource = ASSIGNED_PRIORITY_SOURCE_EDEFAULT;
		assignedPrioritySourceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__ASSIGNED_PRIORITY_SOURCE, oldAssignedPrioritySource, ASSIGNED_PRIORITY_SOURCE_EDEFAULT, oldAssignedPrioritySourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAssignedPrioritySource() {
		return assignedPrioritySourceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDirectFeedThrough() {
		return directFeedThrough;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectFeedThrough(boolean newDirectFeedThrough) {
		boolean oldDirectFeedThrough = directFeedThrough;
		directFeedThrough = newDirectFeedThrough;
		boolean oldDirectFeedThroughESet = directFeedThroughESet;
		directFeedThroughESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__DIRECT_FEED_THROUGH, oldDirectFeedThrough, directFeedThrough, !oldDirectFeedThroughESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDirectFeedThrough() {
		boolean oldDirectFeedThrough = directFeedThrough;
		boolean oldDirectFeedThroughESet = directFeedThroughESet;
		directFeedThrough = DIRECT_FEED_THROUGH_EDEFAULT;
		directFeedThroughESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__DIRECT_FEED_THROUGH, oldDirectFeedThrough, DIRECT_FEED_THROUGH_EDEFAULT, oldDirectFeedThroughESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDirectFeedThrough() {
		return directFeedThroughESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getExecutionOrder() {
		return executionOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionOrder(int newExecutionOrder) {
		int oldExecutionOrder = executionOrder;
		executionOrder = newExecutionOrder;
		boolean oldExecutionOrderESet = executionOrderESet;
		executionOrderESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__EXECUTION_ORDER, oldExecutionOrder, executionOrder, !oldExecutionOrderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExecutionOrder() {
		int oldExecutionOrder = executionOrder;
		boolean oldExecutionOrderESet = executionOrderESet;
		executionOrder = EXECUTION_ORDER_EDEFAULT;
		executionOrderESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__EXECUTION_ORDER, oldExecutionOrder, EXECUTION_ORDER_EDEFAULT, oldExecutionOrderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExecutionOrder() {
		return executionOrderESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVirtual() {
		return isVirtual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsVirtual(boolean newIsVirtual) {
		boolean oldIsVirtual = isVirtual;
		isVirtual = newIsVirtual;
		boolean oldIsVirtualESet = isVirtualESet;
		isVirtualESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__IS_VIRTUAL, oldIsVirtual, isVirtual, !oldIsVirtualESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsVirtual() {
		boolean oldIsVirtual = isVirtual;
		boolean oldIsVirtualESet = isVirtualESet;
		isVirtual = IS_VIRTUAL_EDEFAULT;
		isVirtualESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__IS_VIRTUAL, oldIsVirtual, IS_VIRTUAL_EDEFAULT, oldIsVirtualESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsVirtual() {
		return isVirtualESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMaskType() {
		return maskType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaskType(String newMaskType) {
		String oldMaskType = maskType;
		maskType = newMaskType;
		boolean oldMaskTypeESet = maskTypeESet;
		maskTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__MASK_TYPE, oldMaskType, maskType, !oldMaskTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMaskType() {
		String oldMaskType = maskType;
		boolean oldMaskTypeESet = maskTypeESet;
		maskType = MASK_TYPE_EDEFAULT;
		maskTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__MASK_TYPE, oldMaskType, MASK_TYPE_EDEFAULT, oldMaskTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMaskType() {
		return maskTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getSampleTime() {
		return sampleTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSampleTime(double newSampleTime) {
		double oldSampleTime = sampleTime;
		sampleTime = newSampleTime;
		boolean oldSampleTimeESet = sampleTimeESet;
		sampleTimeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__SAMPLE_TIME, oldSampleTime, sampleTime, !oldSampleTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSampleTime() {
		double oldSampleTime = sampleTime;
		boolean oldSampleTimeESet = sampleTimeESet;
		sampleTime = SAMPLE_TIME_EDEFAULT;
		sampleTimeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__SAMPLE_TIME, oldSampleTime, SAMPLE_TIME_EDEFAULT, oldSampleTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSampleTime() {
		return sampleTimeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__TYPE, oldType, type, !oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetType() {
		String oldType = type;
		boolean oldTypeESet = typeESet;
		type = TYPE_EDEFAULT;
		typeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__TYPE, oldType, TYPE_EDEFAULT, oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getUserDefinedPriority() {
		return userDefinedPriority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserDefinedPriority(int newUserDefinedPriority) {
		int oldUserDefinedPriority = userDefinedPriority;
		userDefinedPriority = newUserDefinedPriority;
		boolean oldUserDefinedPriorityESet = userDefinedPriorityESet;
		userDefinedPriorityESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__USER_DEFINED_PRIORITY, oldUserDefinedPriority, userDefinedPriority, !oldUserDefinedPriorityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUserDefinedPriority() {
		int oldUserDefinedPriority = userDefinedPriority;
		boolean oldUserDefinedPriorityESet = userDefinedPriorityESet;
		userDefinedPriority = USER_DEFINED_PRIORITY_EDEFAULT;
		userDefinedPriorityESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__USER_DEFINED_PRIORITY, oldUserDefinedPriority, USER_DEFINED_PRIORITY_EDEFAULT, oldUserDefinedPriorityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUserDefinedPriority() {
		return userDefinedPriorityESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getPortReference() {
		if (portReference != null && portReference.eIsProxy()) {
			InternalEObject oldPortReference = (InternalEObject)portReference;
			portReference = (Port)eResolveProxy(oldPortReference);
			if (portReference != oldPortReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.BLOCK__PORT_REFERENCE, oldPortReference, portReference));
			}
		}
		return portReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetPortReference() {
		return portReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortReference(Port newPortReference) {
		Port oldPortReference = portReference;
		portReference = newPortReference;
		boolean oldPortReferenceESet = portReferenceESet;
		portReferenceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__PORT_REFERENCE, oldPortReference, portReference, !oldPortReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPortReference() {
		Port oldPortReference = portReference;
		boolean oldPortReferenceESet = portReferenceESet;
		portReference = null;
		portReferenceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__PORT_REFERENCE, oldPortReference, null, oldPortReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPortReference() {
		return portReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BlockParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList.Unsettable<BlockParameter>(BlockParameter.class, this, BlocksPackage.BLOCK__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParameters() {
		if (parameters != null) ((InternalEList.Unsettable<?>)parameters).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParameters() {
		return parameters != null && ((InternalEList.Unsettable<?>)parameters).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiagramInfo getDiagramInfo() {
		return diagramInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiagramInfo(DiagramInfo newDiagramInfo, NotificationChain msgs) {
		DiagramInfo oldDiagramInfo = diagramInfo;
		diagramInfo = newDiagramInfo;
		boolean oldDiagramInfoESet = diagramInfoESet;
		diagramInfoESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__DIAGRAM_INFO, oldDiagramInfo, newDiagramInfo, !oldDiagramInfoESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiagramInfo(DiagramInfo newDiagramInfo) {
		if (newDiagramInfo != diagramInfo) {
			NotificationChain msgs = null;
			if (diagramInfo != null)
				msgs = ((InternalEObject)diagramInfo).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__DIAGRAM_INFO, null, msgs);
			if (newDiagramInfo != null)
				msgs = ((InternalEObject)newDiagramInfo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__DIAGRAM_INFO, null, msgs);
			msgs = basicSetDiagramInfo(newDiagramInfo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDiagramInfoESet = diagramInfoESet;
			diagramInfoESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__DIAGRAM_INFO, newDiagramInfo, newDiagramInfo, !oldDiagramInfoESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDiagramInfo(NotificationChain msgs) {
		DiagramInfo oldDiagramInfo = diagramInfo;
		diagramInfo = null;
		boolean oldDiagramInfoESet = diagramInfoESet;
		diagramInfoESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__DIAGRAM_INFO, oldDiagramInfo, null, oldDiagramInfoESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDiagramInfo() {
		if (diagramInfo != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)diagramInfo).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__DIAGRAM_INFO, null, msgs);
			msgs = basicUnsetDiagramInfo(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDiagramInfoESet = diagramInfoESet;
			diagramInfoESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__DIAGRAM_INFO, null, null, oldDiagramInfoESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDiagramInfo() {
		return diagramInfoESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InDataPort> getInDataPorts() {
		if (inDataPorts == null) {
			inDataPorts = new EObjectContainmentEList.Unsettable<InDataPort>(InDataPort.class, this, BlocksPackage.BLOCK__IN_DATA_PORTS);
		}
		return inDataPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInDataPorts() {
		if (inDataPorts != null) ((InternalEList.Unsettable<?>)inDataPorts).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInDataPorts() {
		return inDataPorts != null && ((InternalEList.Unsettable<?>)inDataPorts).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InEnablePort getInEnablePort() {
		return inEnablePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInEnablePort(InEnablePort newInEnablePort, NotificationChain msgs) {
		InEnablePort oldInEnablePort = inEnablePort;
		inEnablePort = newInEnablePort;
		boolean oldInEnablePortESet = inEnablePortESet;
		inEnablePortESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__IN_ENABLE_PORT, oldInEnablePort, newInEnablePort, !oldInEnablePortESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInEnablePort(InEnablePort newInEnablePort) {
		if (newInEnablePort != inEnablePort) {
			NotificationChain msgs = null;
			if (inEnablePort != null)
				msgs = ((InternalEObject)inEnablePort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__IN_ENABLE_PORT, null, msgs);
			if (newInEnablePort != null)
				msgs = ((InternalEObject)newInEnablePort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__IN_ENABLE_PORT, null, msgs);
			msgs = basicSetInEnablePort(newInEnablePort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInEnablePortESet = inEnablePortESet;
			inEnablePortESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__IN_ENABLE_PORT, newInEnablePort, newInEnablePort, !oldInEnablePortESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInEnablePort(NotificationChain msgs) {
		InEnablePort oldInEnablePort = inEnablePort;
		inEnablePort = null;
		boolean oldInEnablePortESet = inEnablePortESet;
		inEnablePortESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__IN_ENABLE_PORT, oldInEnablePort, null, oldInEnablePortESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInEnablePort() {
		if (inEnablePort != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)inEnablePort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__IN_ENABLE_PORT, null, msgs);
			msgs = basicUnsetInEnablePort(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInEnablePortESet = inEnablePortESet;
			inEnablePortESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__IN_ENABLE_PORT, null, null, oldInEnablePortESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInEnablePort() {
		return inEnablePortESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InEdgeEnablePort getInEdgeEnablePort() {
		return inEdgeEnablePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInEdgeEnablePort(InEdgeEnablePort newInEdgeEnablePort, NotificationChain msgs) {
		InEdgeEnablePort oldInEdgeEnablePort = inEdgeEnablePort;
		inEdgeEnablePort = newInEdgeEnablePort;
		boolean oldInEdgeEnablePortESet = inEdgeEnablePortESet;
		inEdgeEnablePortESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT, oldInEdgeEnablePort, newInEdgeEnablePort, !oldInEdgeEnablePortESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInEdgeEnablePort(InEdgeEnablePort newInEdgeEnablePort) {
		if (newInEdgeEnablePort != inEdgeEnablePort) {
			NotificationChain msgs = null;
			if (inEdgeEnablePort != null)
				msgs = ((InternalEObject)inEdgeEnablePort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT, null, msgs);
			if (newInEdgeEnablePort != null)
				msgs = ((InternalEObject)newInEdgeEnablePort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT, null, msgs);
			msgs = basicSetInEdgeEnablePort(newInEdgeEnablePort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInEdgeEnablePortESet = inEdgeEnablePortESet;
			inEdgeEnablePortESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT, newInEdgeEnablePort, newInEdgeEnablePort, !oldInEdgeEnablePortESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInEdgeEnablePort(NotificationChain msgs) {
		InEdgeEnablePort oldInEdgeEnablePort = inEdgeEnablePort;
		inEdgeEnablePort = null;
		boolean oldInEdgeEnablePortESet = inEdgeEnablePortESet;
		inEdgeEnablePortESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT, oldInEdgeEnablePort, null, oldInEdgeEnablePortESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInEdgeEnablePort() {
		if (inEdgeEnablePort != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)inEdgeEnablePort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT, null, msgs);
			msgs = basicUnsetInEdgeEnablePort(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInEdgeEnablePortESet = inEdgeEnablePortESet;
			inEdgeEnablePortESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT, null, null, oldInEdgeEnablePortESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInEdgeEnablePort() {
		return inEdgeEnablePortESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InControlPort> getInControlPorts() {
		if (inControlPorts == null) {
			inControlPorts = new EObjectContainmentEList.Unsettable<InControlPort>(InControlPort.class, this, BlocksPackage.BLOCK__IN_CONTROL_PORTS);
		}
		return inControlPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInControlPorts() {
		if (inControlPorts != null) ((InternalEList.Unsettable<?>)inControlPorts).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInControlPorts() {
		return inControlPorts != null && ((InternalEList.Unsettable<?>)inControlPorts).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutDataPort> getOutDataPorts() {
		if (outDataPorts == null) {
			outDataPorts = new EObjectContainmentEList.Unsettable<OutDataPort>(OutDataPort.class, this, BlocksPackage.BLOCK__OUT_DATA_PORTS);
		}
		return outDataPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOutDataPorts() {
		if (outDataPorts != null) ((InternalEList.Unsettable<?>)outDataPorts).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOutDataPorts() {
		return outDataPorts != null && ((InternalEList.Unsettable<?>)outDataPorts).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutControlPort> getOutControlPorts() {
		if (outControlPorts == null) {
			outControlPorts = new EObjectContainmentEList.Unsettable<OutControlPort>(OutControlPort.class, this, BlocksPackage.BLOCK__OUT_CONTROL_PORTS);
		}
		return outControlPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOutControlPorts() {
		if (outControlPorts != null) ((InternalEList.Unsettable<?>)outControlPorts).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOutControlPorts() {
		return outControlPorts != null && ((InternalEList.Unsettable<?>)outControlPorts).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.BLOCK__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BLOCK__DIAGRAM_INFO:
				return basicUnsetDiagramInfo(msgs);
			case BlocksPackage.BLOCK__IN_DATA_PORTS:
				return ((InternalEList<?>)getInDataPorts()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BLOCK__IN_ENABLE_PORT:
				return basicUnsetInEnablePort(msgs);
			case BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT:
				return basicUnsetInEdgeEnablePort(msgs);
			case BlocksPackage.BLOCK__IN_CONTROL_PORTS:
				return ((InternalEList<?>)getInControlPorts()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BLOCK__OUT_DATA_PORTS:
				return ((InternalEList<?>)getOutDataPorts()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BLOCK__OUT_CONTROL_PORTS:
				return ((InternalEList<?>)getOutControlPorts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY:
				return getAssignedPriority();
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY_SOURCE:
				return getAssignedPrioritySource();
			case BlocksPackage.BLOCK__DIRECT_FEED_THROUGH:
				return isDirectFeedThrough();
			case BlocksPackage.BLOCK__EXECUTION_ORDER:
				return getExecutionOrder();
			case BlocksPackage.BLOCK__IS_VIRTUAL:
				return isVirtual();
			case BlocksPackage.BLOCK__MASK_TYPE:
				return getMaskType();
			case BlocksPackage.BLOCK__SAMPLE_TIME:
				return getSampleTime();
			case BlocksPackage.BLOCK__TYPE:
				return getType();
			case BlocksPackage.BLOCK__USER_DEFINED_PRIORITY:
				return getUserDefinedPriority();
			case BlocksPackage.BLOCK__PORT_REFERENCE:
				if (resolve) return getPortReference();
				return basicGetPortReference();
			case BlocksPackage.BLOCK__PARAMETERS:
				return getParameters();
			case BlocksPackage.BLOCK__DIAGRAM_INFO:
				return getDiagramInfo();
			case BlocksPackage.BLOCK__IN_DATA_PORTS:
				return getInDataPorts();
			case BlocksPackage.BLOCK__IN_ENABLE_PORT:
				return getInEnablePort();
			case BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT:
				return getInEdgeEnablePort();
			case BlocksPackage.BLOCK__IN_CONTROL_PORTS:
				return getInControlPorts();
			case BlocksPackage.BLOCK__OUT_DATA_PORTS:
				return getOutDataPorts();
			case BlocksPackage.BLOCK__OUT_CONTROL_PORTS:
				return getOutControlPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY:
				setAssignedPriority((Integer)newValue);
				return;
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY_SOURCE:
				setAssignedPrioritySource((String)newValue);
				return;
			case BlocksPackage.BLOCK__DIRECT_FEED_THROUGH:
				setDirectFeedThrough((Boolean)newValue);
				return;
			case BlocksPackage.BLOCK__EXECUTION_ORDER:
				setExecutionOrder((Integer)newValue);
				return;
			case BlocksPackage.BLOCK__IS_VIRTUAL:
				setIsVirtual((Boolean)newValue);
				return;
			case BlocksPackage.BLOCK__MASK_TYPE:
				setMaskType((String)newValue);
				return;
			case BlocksPackage.BLOCK__SAMPLE_TIME:
				setSampleTime((Double)newValue);
				return;
			case BlocksPackage.BLOCK__TYPE:
				setType((String)newValue);
				return;
			case BlocksPackage.BLOCK__USER_DEFINED_PRIORITY:
				setUserDefinedPriority((Integer)newValue);
				return;
			case BlocksPackage.BLOCK__PORT_REFERENCE:
				setPortReference((Port)newValue);
				return;
			case BlocksPackage.BLOCK__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends BlockParameter>)newValue);
				return;
			case BlocksPackage.BLOCK__DIAGRAM_INFO:
				setDiagramInfo((DiagramInfo)newValue);
				return;
			case BlocksPackage.BLOCK__IN_DATA_PORTS:
				getInDataPorts().clear();
				getInDataPorts().addAll((Collection<? extends InDataPort>)newValue);
				return;
			case BlocksPackage.BLOCK__IN_ENABLE_PORT:
				setInEnablePort((InEnablePort)newValue);
				return;
			case BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT:
				setInEdgeEnablePort((InEdgeEnablePort)newValue);
				return;
			case BlocksPackage.BLOCK__IN_CONTROL_PORTS:
				getInControlPorts().clear();
				getInControlPorts().addAll((Collection<? extends InControlPort>)newValue);
				return;
			case BlocksPackage.BLOCK__OUT_DATA_PORTS:
				getOutDataPorts().clear();
				getOutDataPorts().addAll((Collection<? extends OutDataPort>)newValue);
				return;
			case BlocksPackage.BLOCK__OUT_CONTROL_PORTS:
				getOutControlPorts().clear();
				getOutControlPorts().addAll((Collection<? extends OutControlPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY:
				unsetAssignedPriority();
				return;
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY_SOURCE:
				unsetAssignedPrioritySource();
				return;
			case BlocksPackage.BLOCK__DIRECT_FEED_THROUGH:
				unsetDirectFeedThrough();
				return;
			case BlocksPackage.BLOCK__EXECUTION_ORDER:
				unsetExecutionOrder();
				return;
			case BlocksPackage.BLOCK__IS_VIRTUAL:
				unsetIsVirtual();
				return;
			case BlocksPackage.BLOCK__MASK_TYPE:
				unsetMaskType();
				return;
			case BlocksPackage.BLOCK__SAMPLE_TIME:
				unsetSampleTime();
				return;
			case BlocksPackage.BLOCK__TYPE:
				unsetType();
				return;
			case BlocksPackage.BLOCK__USER_DEFINED_PRIORITY:
				unsetUserDefinedPriority();
				return;
			case BlocksPackage.BLOCK__PORT_REFERENCE:
				unsetPortReference();
				return;
			case BlocksPackage.BLOCK__PARAMETERS:
				unsetParameters();
				return;
			case BlocksPackage.BLOCK__DIAGRAM_INFO:
				unsetDiagramInfo();
				return;
			case BlocksPackage.BLOCK__IN_DATA_PORTS:
				unsetInDataPorts();
				return;
			case BlocksPackage.BLOCK__IN_ENABLE_PORT:
				unsetInEnablePort();
				return;
			case BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT:
				unsetInEdgeEnablePort();
				return;
			case BlocksPackage.BLOCK__IN_CONTROL_PORTS:
				unsetInControlPorts();
				return;
			case BlocksPackage.BLOCK__OUT_DATA_PORTS:
				unsetOutDataPorts();
				return;
			case BlocksPackage.BLOCK__OUT_CONTROL_PORTS:
				unsetOutControlPorts();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY:
				return isSetAssignedPriority();
			case BlocksPackage.BLOCK__ASSIGNED_PRIORITY_SOURCE:
				return isSetAssignedPrioritySource();
			case BlocksPackage.BLOCK__DIRECT_FEED_THROUGH:
				return isSetDirectFeedThrough();
			case BlocksPackage.BLOCK__EXECUTION_ORDER:
				return isSetExecutionOrder();
			case BlocksPackage.BLOCK__IS_VIRTUAL:
				return isSetIsVirtual();
			case BlocksPackage.BLOCK__MASK_TYPE:
				return isSetMaskType();
			case BlocksPackage.BLOCK__SAMPLE_TIME:
				return isSetSampleTime();
			case BlocksPackage.BLOCK__TYPE:
				return isSetType();
			case BlocksPackage.BLOCK__USER_DEFINED_PRIORITY:
				return isSetUserDefinedPriority();
			case BlocksPackage.BLOCK__PORT_REFERENCE:
				return isSetPortReference();
			case BlocksPackage.BLOCK__PARAMETERS:
				return isSetParameters();
			case BlocksPackage.BLOCK__DIAGRAM_INFO:
				return isSetDiagramInfo();
			case BlocksPackage.BLOCK__IN_DATA_PORTS:
				return isSetInDataPorts();
			case BlocksPackage.BLOCK__IN_ENABLE_PORT:
				return isSetInEnablePort();
			case BlocksPackage.BLOCK__IN_EDGE_ENABLE_PORT:
				return isSetInEdgeEnablePort();
			case BlocksPackage.BLOCK__IN_CONTROL_PORTS:
				return isSetInControlPorts();
			case BlocksPackage.BLOCK__OUT_DATA_PORTS:
				return isSetOutDataPorts();
			case BlocksPackage.BLOCK__OUT_CONTROL_PORTS:
				return isSetOutControlPorts();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (assignedPriority: ");
		if (assignedPriorityESet) result.append(assignedPriority); else result.append("<unset>");
		result.append(", assignedPrioritySource: ");
		if (assignedPrioritySourceESet) result.append(assignedPrioritySource); else result.append("<unset>");
		result.append(", directFeedThrough: ");
		if (directFeedThroughESet) result.append(directFeedThrough); else result.append("<unset>");
		result.append(", executionOrder: ");
		if (executionOrderESet) result.append(executionOrder); else result.append("<unset>");
		result.append(", isVirtual: ");
		if (isVirtualESet) result.append(isVirtual); else result.append("<unset>");
		result.append(", maskType: ");
		if (maskTypeESet) result.append(maskType); else result.append("<unset>");
		result.append(", sampleTime: ");
		if (sampleTimeESet) result.append(sampleTime); else result.append("<unset>");
		result.append(", type: ");
		if (typeESet) result.append(type); else result.append("<unset>");
		result.append(", userDefinedPriority: ");
		if (userDefinedPriorityESet) result.append(userDefinedPriority); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //BlockImpl
