/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.gastatemodel.Event;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Edge Enable Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Edge-enable port calculates block activation condition based on input value change i.e. it needs to remember input signal value from previous clock-tick. The possible types of edge-enable port are "rising-edge", "falling-edge", "either-edge" trigger ports. From the viewpoint of data exchange, the enable port has dual behaviour. First, it checks the condition and determines if block can execute. In addition, the port can optionally pass also the incoming data value for internal processing in the block if activation condition is satisfied.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getTriggerType <em>Trigger Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getEvents <em>Events</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getOutputDataType <em>Output Data Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInEdgeEnablePort()
 * @model
 * @generated
 */
public interface InEdgeEnablePort extends Inport {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Trigger Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger Type</em>' attribute.
	 * @see #isSetTriggerType()
	 * @see #unsetTriggerType()
	 * @see #setTriggerType(String)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInEdgeEnablePort_TriggerType()
	 * @model unsettable="true"
	 * @generated
	 */
	String getTriggerType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getTriggerType <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger Type</em>' attribute.
	 * @see #isSetTriggerType()
	 * @see #unsetTriggerType()
	 * @see #getTriggerType()
	 * @generated
	 */
	void setTriggerType(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getTriggerType <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTriggerType()
	 * @see #getTriggerType()
	 * @see #setTriggerType(String)
	 * @generated
	 */
	void unsetTriggerType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getTriggerType <em>Trigger Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Trigger Type</em>' attribute is set.
	 * @see #unsetTriggerType()
	 * @see #getTriggerType()
	 * @see #setTriggerType(String)
	 * @generated
	 */
	boolean isSetTriggerType();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.gastatemodel.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' reference list.
	 * @see #isSetEvents()
	 * @see #unsetEvents()
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInEdgeEnablePort_Events()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getEvents <em>Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	void unsetEvents();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getEvents <em>Events</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Events</em>' reference list is set.
	 * @see #unsetEvents()
	 * @see #getEvents()
	 * @generated
	 */
	boolean isSetEvents();

	/**
	 * Returns the value of the '<em><b>Output Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Data Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Data Type</em>' containment reference.
	 * @see #isSetOutputDataType()
	 * @see #unsetOutputDataType()
	 * @see #setOutputDataType(GADataType)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getInEdgeEnablePort_OutputDataType()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	GADataType getOutputDataType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getOutputDataType <em>Output Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Data Type</em>' containment reference.
	 * @see #isSetOutputDataType()
	 * @see #unsetOutputDataType()
	 * @see #getOutputDataType()
	 * @generated
	 */
	void setOutputDataType(GADataType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getOutputDataType <em>Output Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOutputDataType()
	 * @see #getOutputDataType()
	 * @see #setOutputDataType(GADataType)
	 * @generated
	 */
	void unsetOutputDataType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getOutputDataType <em>Output Data Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Output Data Type</em>' containment reference is set.
	 * @see #unsetOutputDataType()
	 * @see #getOutputDataType()
	 * @see #setOutputDataType(GADataType)
	 * @generated
	 */
	boolean isSetOutputDataType();

} // InEdgeEnablePort
