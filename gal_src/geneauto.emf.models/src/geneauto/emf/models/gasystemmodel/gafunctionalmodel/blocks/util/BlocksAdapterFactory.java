/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.util;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;
import geneauto.emf.models.gasystemmodel.GASystemModelRoot;

import geneauto.emf.models.gasystemmodel.common.ContainerNode;
import geneauto.emf.models.gasystemmodel.common.Node;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.*;

import geneauto.emf.models.genericmodel.GAModelElement;
import geneauto.emf.models.genericmodel.GANamed;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage
 * @generated
 */
public class BlocksAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BlocksPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = BlocksPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlocksSwitch<Adapter> modelSwitch =
		new BlocksSwitch<Adapter>() {
			@Override
			public Adapter caseBlock(Block object) {
				return createBlockAdapter();
			}
			@Override
			public Adapter caseDiagramInfo(DiagramInfo object) {
				return createDiagramInfoAdapter();
			}
			@Override
			public Adapter caseChartBlock(ChartBlock object) {
				return createChartBlockAdapter();
			}
			@Override
			public Adapter caseCombinatorialBlock(CombinatorialBlock object) {
				return createCombinatorialBlockAdapter();
			}
			@Override
			public Adapter caseControlBlock(ControlBlock object) {
				return createControlBlockAdapter();
			}
			@Override
			public Adapter caseGenericBlock(GenericBlock object) {
				return createGenericBlockAdapter();
			}
			@Override
			public Adapter caseReferenceBlock(ReferenceBlock object) {
				return createReferenceBlockAdapter();
			}
			@Override
			public Adapter caseRoutingBlock(RoutingBlock object) {
				return createRoutingBlockAdapter();
			}
			@Override
			public Adapter caseSequentialBlock(SequentialBlock object) {
				return createSequentialBlockAdapter();
			}
			@Override
			public Adapter caseSinkBlock(SinkBlock object) {
				return createSinkBlockAdapter();
			}
			@Override
			public Adapter caseSourceBlock(SourceBlock object) {
				return createSourceBlockAdapter();
			}
			@Override
			public Adapter caseSystemBlock(SystemBlock object) {
				return createSystemBlockAdapter();
			}
			@Override
			public Adapter caseUserDefinedFunctionBlock(UserDefinedFunctionBlock object) {
				return createUserDefinedFunctionBlockAdapter();
			}
			@Override
			public Adapter caseGAModelElement(GAModelElement object) {
				return createGAModelElementAdapter();
			}
			@Override
			public Adapter caseGASystemModelElement(GASystemModelElement object) {
				return createGASystemModelElementAdapter();
			}
			@Override
			public Adapter caseGANamed(GANamed object) {
				return createGANamedAdapter();
			}
			@Override
			public Adapter caseNode(Node object) {
				return createNodeAdapter();
			}
			@Override
			public Adapter caseContainerNode(ContainerNode object) {
				return createContainerNodeAdapter();
			}
			@Override
			public Adapter caseGASystemModelRoot(GASystemModelRoot object) {
				return createGASystemModelRootAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block
	 * @generated
	 */
	public Adapter createBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo <em>Diagram Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.DiagramInfo
	 * @generated
	 */
	public Adapter createDiagramInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock <em>Chart Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock
	 * @generated
	 */
	public Adapter createChartBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock <em>Combinatorial Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.CombinatorialBlock
	 * @generated
	 */
	public Adapter createCombinatorialBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ControlBlock <em>Control Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ControlBlock
	 * @generated
	 */
	public Adapter createControlBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.GenericBlock <em>Generic Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.GenericBlock
	 * @generated
	 */
	public Adapter createGenericBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock <em>Reference Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock
	 * @generated
	 */
	public Adapter createReferenceBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.RoutingBlock <em>Routing Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.RoutingBlock
	 * @generated
	 */
	public Adapter createRoutingBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock <em>Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SequentialBlock
	 * @generated
	 */
	public Adapter createSequentialBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock <em>Sink Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock
	 * @generated
	 */
	public Adapter createSinkBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock <em>Source Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SourceBlock
	 * @generated
	 */
	public Adapter createSourceBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock <em>System Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock
	 * @generated
	 */
	public Adapter createSystemBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock <em>User Defined Function Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.UserDefinedFunctionBlock
	 * @generated
	 */
	public Adapter createUserDefinedFunctionBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.genericmodel.GAModelElement <em>GA Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.genericmodel.GAModelElement
	 * @generated
	 */
	public Adapter createGAModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.GASystemModelElement <em>GA System Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModelElement
	 * @generated
	 */
	public Adapter createGASystemModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.genericmodel.GANamed <em>GA Named</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.genericmodel.GANamed
	 * @generated
	 */
	public Adapter createGANamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.common.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.common.Node
	 * @generated
	 */
	public Adapter createNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.common.ContainerNode <em>Container Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.common.ContainerNode
	 * @generated
	 */
	public Adapter createContainerNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gasystemmodel.GASystemModelRoot <em>GA System Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModelRoot
	 * @generated
	 */
	public Adapter createGASystemModelRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //BlocksAdapterFactory
