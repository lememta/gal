/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsFactory
 * @model kind="package"
 * @generated
 */
public interface PortsPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ports";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gasystemmodel/gafunctionalmodel/ports.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PortsPackage eINSTANCE = geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__PORT_NUMBER = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__SOURCE_BLOCK = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InportImpl <em>Inport</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InportImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInport()
	 * @generated
	 */
	int INPORT = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__EXTERNAL_ID = PORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__ID = PORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__IS_FIXED_NAME = PORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__NAME = PORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__ORIGINAL_FULL_NAME = PORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__ORIGINAL_NAME = PORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__PARENT = PORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__MODEL = PORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__ANNOTATIONS = PORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__CODE_MODEL_ELEMENT = PORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__PORT_NUMBER = PORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__SOURCE_BLOCK = PORT__SOURCE_BLOCK;

	/**
	 * The feature id for the '<em><b>Related To Inport Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__RELATED_TO_INPORT_BLOCK = PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT__DATA_TYPE = PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Inport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPORT_FEATURE_COUNT = PORT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InDataPortImpl <em>In Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InDataPortImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInDataPort()
	 * @generated
	 */
	int IN_DATA_PORT = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__EXTERNAL_ID = INPORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__ID = INPORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__IS_FIXED_NAME = INPORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__NAME = INPORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__ORIGINAL_FULL_NAME = INPORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__ORIGINAL_NAME = INPORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__PARENT = INPORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__MODEL = INPORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__ANNOTATIONS = INPORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__CODE_MODEL_ELEMENT = INPORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__PORT_NUMBER = INPORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__SOURCE_BLOCK = INPORT__SOURCE_BLOCK;

	/**
	 * The feature id for the '<em><b>Related To Inport Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__RELATED_TO_INPORT_BLOCK = INPORT__RELATED_TO_INPORT_BLOCK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__DATA_TYPE = INPORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__VARIABLE = INPORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>In Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT_FEATURE_COUNT = INPORT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEnablePortImpl <em>In Enable Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEnablePortImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInEnablePort()
	 * @generated
	 */
	int IN_ENABLE_PORT = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__EXTERNAL_ID = INPORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__ID = INPORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__IS_FIXED_NAME = INPORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__NAME = INPORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__ORIGINAL_FULL_NAME = INPORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__ORIGINAL_NAME = INPORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__PARENT = INPORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__MODEL = INPORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__ANNOTATIONS = INPORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__CODE_MODEL_ELEMENT = INPORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__PORT_NUMBER = INPORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__SOURCE_BLOCK = INPORT__SOURCE_BLOCK;

	/**
	 * The feature id for the '<em><b>Related To Inport Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__RELATED_TO_INPORT_BLOCK = INPORT__RELATED_TO_INPORT_BLOCK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__DATA_TYPE = INPORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Reset States</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__RESET_STATES = INPORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT__OUTPUT_DATA_TYPE = INPORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>In Enable Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ENABLE_PORT_FEATURE_COUNT = INPORT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEdgeEnablePortImpl <em>In Edge Enable Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEdgeEnablePortImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInEdgeEnablePort()
	 * @generated
	 */
	int IN_EDGE_ENABLE_PORT = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__EXTERNAL_ID = INPORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__ID = INPORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__IS_FIXED_NAME = INPORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__NAME = INPORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__ORIGINAL_FULL_NAME = INPORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__ORIGINAL_NAME = INPORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__PARENT = INPORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__MODEL = INPORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__ANNOTATIONS = INPORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__CODE_MODEL_ELEMENT = INPORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__PORT_NUMBER = INPORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__SOURCE_BLOCK = INPORT__SOURCE_BLOCK;

	/**
	 * The feature id for the '<em><b>Related To Inport Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__RELATED_TO_INPORT_BLOCK = INPORT__RELATED_TO_INPORT_BLOCK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__DATA_TYPE = INPORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Trigger Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__TRIGGER_TYPE = INPORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__EVENTS = INPORT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Output Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE = INPORT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>In Edge Enable Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_EDGE_ENABLE_PORT_FEATURE_COUNT = INPORT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InControlPortImpl <em>In Control Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InControlPortImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInControlPort()
	 * @generated
	 */
	int IN_CONTROL_PORT = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__EXTERNAL_ID = INPORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__ID = INPORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__IS_FIXED_NAME = INPORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__NAME = INPORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__ORIGINAL_FULL_NAME = INPORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__ORIGINAL_NAME = INPORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__PARENT = INPORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__MODEL = INPORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__ANNOTATIONS = INPORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__CODE_MODEL_ELEMENT = INPORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__PORT_NUMBER = INPORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__SOURCE_BLOCK = INPORT__SOURCE_BLOCK;

	/**
	 * The feature id for the '<em><b>Related To Inport Block</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__RELATED_TO_INPORT_BLOCK = INPORT__RELATED_TO_INPORT_BLOCK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__DATA_TYPE = INPORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Periodic Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__PERIODIC_SAMPLE_TIME = INPORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reset States</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__RESET_STATES = INPORT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__SAMPLE_TIME = INPORT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Compute Code</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__COMPUTE_CODE = INPORT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__EVENT = INPORT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Output Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__OUTPUT_DATA_TYPE = INPORT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>In Control Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT_FEATURE_COUNT = INPORT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutportImpl <em>Outport</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutportImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getOutport()
	 * @generated
	 */
	int OUTPORT = 7;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__EXTERNAL_ID = PORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__ID = PORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__IS_FIXED_NAME = PORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__NAME = PORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__ORIGINAL_FULL_NAME = PORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__ORIGINAL_NAME = PORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__PARENT = PORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__MODEL = PORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__ANNOTATIONS = PORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__CODE_MODEL_ELEMENT = PORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__PORT_NUMBER = PORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT__SOURCE_BLOCK = PORT__SOURCE_BLOCK;

	/**
	 * The number of structural features of the '<em>Outport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPORT_FEATURE_COUNT = PORT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl <em>Out Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getOutDataPort()
	 * @generated
	 */
	int OUT_DATA_PORT = 6;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__EXTERNAL_ID = OUTPORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__ID = OUTPORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__IS_FIXED_NAME = OUTPORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__NAME = OUTPORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__ORIGINAL_FULL_NAME = OUTPORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__ORIGINAL_NAME = OUTPORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__PARENT = OUTPORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__MODEL = OUTPORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__ANNOTATIONS = OUTPORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__CODE_MODEL_ELEMENT = OUTPORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__PORT_NUMBER = OUTPORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__SOURCE_BLOCK = OUTPORT__SOURCE_BLOCK;

	/**
	 * The feature id for the '<em><b>Reset Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__RESET_OUTPUT = OUTPORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__VARIABLE = OUTPORT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__DATA_TYPE = OUTPORT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Initial Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__INITIAL_OUTPUT = OUTPORT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Out Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT_FEATURE_COUNT = OUTPORT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutControlPortImpl <em>Out Control Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutControlPortImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getOutControlPort()
	 * @generated
	 */
	int OUT_CONTROL_PORT = 8;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__EXTERNAL_ID = OUTPORT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__ID = OUTPORT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__IS_FIXED_NAME = OUTPORT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__NAME = OUTPORT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__ORIGINAL_FULL_NAME = OUTPORT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__ORIGINAL_NAME = OUTPORT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__PARENT = OUTPORT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__MODEL = OUTPORT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__ANNOTATIONS = OUTPORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__CODE_MODEL_ELEMENT = OUTPORT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__PORT_NUMBER = OUTPORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Source Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__SOURCE_BLOCK = OUTPORT__SOURCE_BLOCK;

	/**
	 * The number of structural features of the '<em>Out Control Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT_FEATURE_COUNT = OUTPORT_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port#getPortNumber <em>Port Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Number</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port#getPortNumber()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_PortNumber();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port#getSourceBlock <em>Source Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Port#getSourceBlock()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_SourceBlock();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InDataPort <em>In Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Data Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InDataPort
	 * @generated
	 */
	EClass getInDataPort();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InDataPort#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InDataPort#getVariable()
	 * @see #getInDataPort()
	 * @generated
	 */
	EReference getInDataPort_Variable();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Inport <em>Inport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inport</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Inport
	 * @generated
	 */
	EClass getInport();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Inport#isRelatedToInportBlock <em>Related To Inport Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Related To Inport Block</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Inport#isRelatedToInportBlock()
	 * @see #getInport()
	 * @generated
	 */
	EAttribute getInport_RelatedToInportBlock();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Inport#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Inport#getDataType()
	 * @see #getInport()
	 * @generated
	 */
	EReference getInport_DataType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort <em>In Enable Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Enable Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort
	 * @generated
	 */
	EClass getInEnablePort();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort#isResetStates <em>Reset States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reset States</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort#isResetStates()
	 * @see #getInEnablePort()
	 * @generated
	 */
	EAttribute getInEnablePort_ResetStates();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort#getOutputDataType <em>Output Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output Data Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEnablePort#getOutputDataType()
	 * @see #getInEnablePort()
	 * @generated
	 */
	EReference getInEnablePort_OutputDataType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort <em>In Edge Enable Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Edge Enable Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort
	 * @generated
	 */
	EClass getInEdgeEnablePort();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getTriggerType <em>Trigger Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getTriggerType()
	 * @see #getInEdgeEnablePort()
	 * @generated
	 */
	EAttribute getInEdgeEnablePort_TriggerType();

	/**
	 * Returns the meta object for the reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getEvents()
	 * @see #getInEdgeEnablePort()
	 * @generated
	 */
	EReference getInEdgeEnablePort_Events();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getOutputDataType <em>Output Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output Data Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort#getOutputDataType()
	 * @see #getInEdgeEnablePort()
	 * @generated
	 */
	EReference getInEdgeEnablePort_OutputDataType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort <em>In Control Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Control Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort
	 * @generated
	 */
	EClass getInControlPort();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isPeriodicSampleTime <em>Periodic Sample Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Periodic Sample Time</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isPeriodicSampleTime()
	 * @see #getInControlPort()
	 * @generated
	 */
	EAttribute getInControlPort_PeriodicSampleTime();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isResetStates <em>Reset States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reset States</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#isResetStates()
	 * @see #getInControlPort()
	 * @generated
	 */
	EAttribute getInControlPort_ResetStates();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getSampleTime <em>Sample Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sample Time</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getSampleTime()
	 * @see #getInControlPort()
	 * @generated
	 */
	EAttribute getInControlPort_SampleTime();

	/**
	 * Returns the meta object for the reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getComputeCode <em>Compute Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Compute Code</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getComputeCode()
	 * @see #getInControlPort()
	 * @generated
	 */
	EReference getInControlPort_ComputeCode();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getEvent()
	 * @see #getInControlPort()
	 * @generated
	 */
	EReference getInControlPort_Event();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getOutputDataType <em>Output Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output Data Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InControlPort#getOutputDataType()
	 * @see #getInControlPort()
	 * @generated
	 */
	EReference getInControlPort_OutputDataType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort <em>Out Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Data Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort
	 * @generated
	 */
	EClass getOutDataPort();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#isResetOutput <em>Reset Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reset Output</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#isResetOutput()
	 * @see #getOutDataPort()
	 * @generated
	 */
	EAttribute getOutDataPort_ResetOutput();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getVariable()
	 * @see #getOutDataPort()
	 * @generated
	 */
	EReference getOutDataPort_Variable();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getDataType()
	 * @see #getOutDataPort()
	 * @generated
	 */
	EReference getOutDataPort_DataType();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getInitialOutput <em>Initial Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial Output</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getInitialOutput()
	 * @see #getOutDataPort()
	 * @generated
	 */
	EReference getOutDataPort_InitialOutput();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Outport <em>Outport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Outport</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.Outport
	 * @generated
	 */
	EClass getOutport();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort <em>Out Control Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Control Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort
	 * @generated
	 */
	EClass getOutControlPort();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PortsFactory getPortsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Port Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__PORT_NUMBER = eINSTANCE.getPort_PortNumber();

		/**
		 * The meta object literal for the '<em><b>Source Block</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT__SOURCE_BLOCK = eINSTANCE.getPort_SourceBlock();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InDataPortImpl <em>In Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InDataPortImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInDataPort()
		 * @generated
		 */
		EClass IN_DATA_PORT = eINSTANCE.getInDataPort();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_DATA_PORT__VARIABLE = eINSTANCE.getInDataPort_Variable();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InportImpl <em>Inport</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InportImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInport()
		 * @generated
		 */
		EClass INPORT = eINSTANCE.getInport();

		/**
		 * The meta object literal for the '<em><b>Related To Inport Block</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INPORT__RELATED_TO_INPORT_BLOCK = eINSTANCE.getInport_RelatedToInportBlock();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INPORT__DATA_TYPE = eINSTANCE.getInport_DataType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEnablePortImpl <em>In Enable Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEnablePortImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInEnablePort()
		 * @generated
		 */
		EClass IN_ENABLE_PORT = eINSTANCE.getInEnablePort();

		/**
		 * The meta object literal for the '<em><b>Reset States</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_ENABLE_PORT__RESET_STATES = eINSTANCE.getInEnablePort_ResetStates();

		/**
		 * The meta object literal for the '<em><b>Output Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_ENABLE_PORT__OUTPUT_DATA_TYPE = eINSTANCE.getInEnablePort_OutputDataType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEdgeEnablePortImpl <em>In Edge Enable Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEdgeEnablePortImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInEdgeEnablePort()
		 * @generated
		 */
		EClass IN_EDGE_ENABLE_PORT = eINSTANCE.getInEdgeEnablePort();

		/**
		 * The meta object literal for the '<em><b>Trigger Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_EDGE_ENABLE_PORT__TRIGGER_TYPE = eINSTANCE.getInEdgeEnablePort_TriggerType();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_EDGE_ENABLE_PORT__EVENTS = eINSTANCE.getInEdgeEnablePort_Events();

		/**
		 * The meta object literal for the '<em><b>Output Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE = eINSTANCE.getInEdgeEnablePort_OutputDataType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InControlPortImpl <em>In Control Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InControlPortImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getInControlPort()
		 * @generated
		 */
		EClass IN_CONTROL_PORT = eINSTANCE.getInControlPort();

		/**
		 * The meta object literal for the '<em><b>Periodic Sample Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_CONTROL_PORT__PERIODIC_SAMPLE_TIME = eINSTANCE.getInControlPort_PeriodicSampleTime();

		/**
		 * The meta object literal for the '<em><b>Reset States</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_CONTROL_PORT__RESET_STATES = eINSTANCE.getInControlPort_ResetStates();

		/**
		 * The meta object literal for the '<em><b>Sample Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_CONTROL_PORT__SAMPLE_TIME = eINSTANCE.getInControlPort_SampleTime();

		/**
		 * The meta object literal for the '<em><b>Compute Code</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_CONTROL_PORT__COMPUTE_CODE = eINSTANCE.getInControlPort_ComputeCode();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_CONTROL_PORT__EVENT = eINSTANCE.getInControlPort_Event();

		/**
		 * The meta object literal for the '<em><b>Output Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_CONTROL_PORT__OUTPUT_DATA_TYPE = eINSTANCE.getInControlPort_OutputDataType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl <em>Out Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutDataPortImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getOutDataPort()
		 * @generated
		 */
		EClass OUT_DATA_PORT = eINSTANCE.getOutDataPort();

		/**
		 * The meta object literal for the '<em><b>Reset Output</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUT_DATA_PORT__RESET_OUTPUT = eINSTANCE.getOutDataPort_ResetOutput();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_DATA_PORT__VARIABLE = eINSTANCE.getOutDataPort_Variable();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_DATA_PORT__DATA_TYPE = eINSTANCE.getOutDataPort_DataType();

		/**
		 * The meta object literal for the '<em><b>Initial Output</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_DATA_PORT__INITIAL_OUTPUT = eINSTANCE.getOutDataPort_InitialOutput();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutportImpl <em>Outport</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutportImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getOutport()
		 * @generated
		 */
		EClass OUTPORT = eINSTANCE.getOutport();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutControlPortImpl <em>Out Control Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.OutControlPortImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl#getOutControlPort()
		 * @generated
		 */
		EClass OUT_CONTROL_PORT = eINSTANCE.getOutControlPort();

	}

} //PortsPackage
