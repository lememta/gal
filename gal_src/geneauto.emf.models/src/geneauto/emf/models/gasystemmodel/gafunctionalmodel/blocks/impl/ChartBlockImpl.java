/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.gacodemodel.Dependency;
import geneauto.emf.models.gacodemodel.GACodeModelElement;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.common.Function_SM;
import geneauto.emf.models.gasystemmodel.common.Variable_SM;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;

import geneauto.emf.models.gasystemmodel.gastatemodel.ChartRoot;
import geneauto.emf.models.gasystemmodel.gastatemodel.Composition;
import geneauto.emf.models.gasystemmodel.gastatemodel.Event;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Chart Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#isExecuteAtInit <em>Execute At Init</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#isExportFunctions <em>Export Functions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getInstanceOuterReference <em>Instance Outer Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getInitFunction <em>Init Function</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getActiveEventReference <em>Active Event Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getStateArgInternalRef <em>State Arg Internal Ref</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getContextReference <em>Context Reference</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getInstanceStructType <em>Instance Struct Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getComputeFunction <em>Compute Function</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getFunctions <em>Functions</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getRootLocation <em>Root Location</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getComposition <em>Composition</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getDefaultEvent <em>Default Event</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getCodeModelElements <em>Code Model Elements</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.ChartBlockImpl#getExternalDependencies <em>External Dependencies</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ChartBlockImpl extends BlockImpl implements ChartBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #isExecuteAtInit() <em>Execute At Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExecuteAtInit()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXECUTE_AT_INIT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExecuteAtInit() <em>Execute At Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExecuteAtInit()
	 * @generated
	 * @ordered
	 */
	protected boolean executeAtInit = EXECUTE_AT_INIT_EDEFAULT;

	/**
	 * This is true if the Execute At Init attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean executeAtInitESet;

	/**
	 * The default value of the '{@link #isExportFunctions() <em>Export Functions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExportFunctions()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXPORT_FUNCTIONS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExportFunctions() <em>Export Functions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExportFunctions()
	 * @generated
	 * @ordered
	 */
	protected boolean exportFunctions = EXPORT_FUNCTIONS_EDEFAULT;

	/**
	 * This is true if the Export Functions attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean exportFunctionsESet;

	/**
	 * The cached value of the '{@link #getInstanceOuterReference() <em>Instance Outer Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceOuterReference()
	 * @generated
	 * @ordered
	 */
	protected Expression instanceOuterReference;

	/**
	 * This is true if the Instance Outer Reference reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean instanceOuterReferenceESet;

	/**
	 * The cached value of the '{@link #getInitFunction() <em>Init Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitFunction()
	 * @generated
	 * @ordered
	 */
	protected Function_SM initFunction;

	/**
	 * This is true if the Init Function reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initFunctionESet;

	/**
	 * The cached value of the '{@link #getActiveEventReference() <em>Active Event Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveEventReference()
	 * @generated
	 * @ordered
	 */
	protected Expression activeEventReference;

	/**
	 * This is true if the Active Event Reference reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean activeEventReferenceESet;

	/**
	 * The cached value of the '{@link #getStateArgInternalRef() <em>State Arg Internal Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateArgInternalRef()
	 * @generated
	 * @ordered
	 */
	protected Expression stateArgInternalRef;

	/**
	 * This is true if the State Arg Internal Ref reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean stateArgInternalRefESet;

	/**
	 * The cached value of the '{@link #getContextReference() <em>Context Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContextReference()
	 * @generated
	 * @ordered
	 */
	protected Expression contextReference;

	/**
	 * This is true if the Context Reference reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean contextReferenceESet;

	/**
	 * The cached value of the '{@link #getInstanceStructType() <em>Instance Struct Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceStructType()
	 * @generated
	 * @ordered
	 */
	protected GADataType instanceStructType;

	/**
	 * This is true if the Instance Struct Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean instanceStructTypeESet;

	/**
	 * The cached value of the '{@link #getComputeFunction() <em>Compute Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputeFunction()
	 * @generated
	 * @ordered
	 */
	protected Function_SM computeFunction;

	/**
	 * This is true if the Compute Function reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean computeFunctionESet;

	/**
	 * The cached value of the '{@link #getFunctions() <em>Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Function_SM> functions;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable_SM> variables;

	/**
	 * The cached value of the '{@link #getRootLocation() <em>Root Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootLocation()
	 * @generated
	 * @ordered
	 */
	protected ChartRoot rootLocation;

	/**
	 * This is true if the Root Location containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean rootLocationESet;

	/**
	 * The cached value of the '{@link #getComposition() <em>Composition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComposition()
	 * @generated
	 * @ordered
	 */
	protected Composition composition;

	/**
	 * This is true if the Composition containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean compositionESet;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getDefaultEvent() <em>Default Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultEvent()
	 * @generated
	 * @ordered
	 */
	protected Event defaultEvent;

	/**
	 * This is true if the Default Event containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean defaultEventESet;

	/**
	 * The cached value of the '{@link #getCodeModelElements() <em>Code Model Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodeModelElements()
	 * @generated
	 * @ordered
	 */
	protected EList<GACodeModelElement> codeModelElements;

	/**
	 * The cached value of the '{@link #getExternalDependencies() <em>External Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependency> externalDependencies;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChartBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.CHART_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExecuteAtInit() {
		return executeAtInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecuteAtInit(boolean newExecuteAtInit) {
		boolean oldExecuteAtInit = executeAtInit;
		executeAtInit = newExecuteAtInit;
		boolean oldExecuteAtInitESet = executeAtInitESet;
		executeAtInitESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__EXECUTE_AT_INIT, oldExecuteAtInit, executeAtInit, !oldExecuteAtInitESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExecuteAtInit() {
		boolean oldExecuteAtInit = executeAtInit;
		boolean oldExecuteAtInitESet = executeAtInitESet;
		executeAtInit = EXECUTE_AT_INIT_EDEFAULT;
		executeAtInitESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__EXECUTE_AT_INIT, oldExecuteAtInit, EXECUTE_AT_INIT_EDEFAULT, oldExecuteAtInitESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExecuteAtInit() {
		return executeAtInitESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExportFunctions() {
		return exportFunctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExportFunctions(boolean newExportFunctions) {
		boolean oldExportFunctions = exportFunctions;
		exportFunctions = newExportFunctions;
		boolean oldExportFunctionsESet = exportFunctionsESet;
		exportFunctionsESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__EXPORT_FUNCTIONS, oldExportFunctions, exportFunctions, !oldExportFunctionsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExportFunctions() {
		boolean oldExportFunctions = exportFunctions;
		boolean oldExportFunctionsESet = exportFunctionsESet;
		exportFunctions = EXPORT_FUNCTIONS_EDEFAULT;
		exportFunctionsESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__EXPORT_FUNCTIONS, oldExportFunctions, EXPORT_FUNCTIONS_EDEFAULT, oldExportFunctionsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExportFunctions() {
		return exportFunctionsESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInstanceOuterReference() {
		if (instanceOuterReference != null && instanceOuterReference.eIsProxy()) {
			InternalEObject oldInstanceOuterReference = (InternalEObject)instanceOuterReference;
			instanceOuterReference = (Expression)eResolveProxy(oldInstanceOuterReference);
			if (instanceOuterReference != oldInstanceOuterReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.CHART_BLOCK__INSTANCE_OUTER_REFERENCE, oldInstanceOuterReference, instanceOuterReference));
			}
		}
		return instanceOuterReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression basicGetInstanceOuterReference() {
		return instanceOuterReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstanceOuterReference(Expression newInstanceOuterReference) {
		Expression oldInstanceOuterReference = instanceOuterReference;
		instanceOuterReference = newInstanceOuterReference;
		boolean oldInstanceOuterReferenceESet = instanceOuterReferenceESet;
		instanceOuterReferenceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__INSTANCE_OUTER_REFERENCE, oldInstanceOuterReference, instanceOuterReference, !oldInstanceOuterReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInstanceOuterReference() {
		Expression oldInstanceOuterReference = instanceOuterReference;
		boolean oldInstanceOuterReferenceESet = instanceOuterReferenceESet;
		instanceOuterReference = null;
		instanceOuterReferenceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__INSTANCE_OUTER_REFERENCE, oldInstanceOuterReference, null, oldInstanceOuterReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInstanceOuterReference() {
		return instanceOuterReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function_SM getInitFunction() {
		if (initFunction != null && initFunction.eIsProxy()) {
			InternalEObject oldInitFunction = (InternalEObject)initFunction;
			initFunction = (Function_SM)eResolveProxy(oldInitFunction);
			if (initFunction != oldInitFunction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.CHART_BLOCK__INIT_FUNCTION, oldInitFunction, initFunction));
			}
		}
		return initFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function_SM basicGetInitFunction() {
		return initFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitFunction(Function_SM newInitFunction) {
		Function_SM oldInitFunction = initFunction;
		initFunction = newInitFunction;
		boolean oldInitFunctionESet = initFunctionESet;
		initFunctionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__INIT_FUNCTION, oldInitFunction, initFunction, !oldInitFunctionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitFunction() {
		Function_SM oldInitFunction = initFunction;
		boolean oldInitFunctionESet = initFunctionESet;
		initFunction = null;
		initFunctionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__INIT_FUNCTION, oldInitFunction, null, oldInitFunctionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitFunction() {
		return initFunctionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getActiveEventReference() {
		if (activeEventReference != null && activeEventReference.eIsProxy()) {
			InternalEObject oldActiveEventReference = (InternalEObject)activeEventReference;
			activeEventReference = (Expression)eResolveProxy(oldActiveEventReference);
			if (activeEventReference != oldActiveEventReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.CHART_BLOCK__ACTIVE_EVENT_REFERENCE, oldActiveEventReference, activeEventReference));
			}
		}
		return activeEventReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression basicGetActiveEventReference() {
		return activeEventReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveEventReference(Expression newActiveEventReference) {
		Expression oldActiveEventReference = activeEventReference;
		activeEventReference = newActiveEventReference;
		boolean oldActiveEventReferenceESet = activeEventReferenceESet;
		activeEventReferenceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__ACTIVE_EVENT_REFERENCE, oldActiveEventReference, activeEventReference, !oldActiveEventReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetActiveEventReference() {
		Expression oldActiveEventReference = activeEventReference;
		boolean oldActiveEventReferenceESet = activeEventReferenceESet;
		activeEventReference = null;
		activeEventReferenceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__ACTIVE_EVENT_REFERENCE, oldActiveEventReference, null, oldActiveEventReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetActiveEventReference() {
		return activeEventReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getStateArgInternalRef() {
		if (stateArgInternalRef != null && stateArgInternalRef.eIsProxy()) {
			InternalEObject oldStateArgInternalRef = (InternalEObject)stateArgInternalRef;
			stateArgInternalRef = (Expression)eResolveProxy(oldStateArgInternalRef);
			if (stateArgInternalRef != oldStateArgInternalRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.CHART_BLOCK__STATE_ARG_INTERNAL_REF, oldStateArgInternalRef, stateArgInternalRef));
			}
		}
		return stateArgInternalRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression basicGetStateArgInternalRef() {
		return stateArgInternalRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateArgInternalRef(Expression newStateArgInternalRef) {
		Expression oldStateArgInternalRef = stateArgInternalRef;
		stateArgInternalRef = newStateArgInternalRef;
		boolean oldStateArgInternalRefESet = stateArgInternalRefESet;
		stateArgInternalRefESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__STATE_ARG_INTERNAL_REF, oldStateArgInternalRef, stateArgInternalRef, !oldStateArgInternalRefESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStateArgInternalRef() {
		Expression oldStateArgInternalRef = stateArgInternalRef;
		boolean oldStateArgInternalRefESet = stateArgInternalRefESet;
		stateArgInternalRef = null;
		stateArgInternalRefESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__STATE_ARG_INTERNAL_REF, oldStateArgInternalRef, null, oldStateArgInternalRefESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStateArgInternalRef() {
		return stateArgInternalRefESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getContextReference() {
		if (contextReference != null && contextReference.eIsProxy()) {
			InternalEObject oldContextReference = (InternalEObject)contextReference;
			contextReference = (Expression)eResolveProxy(oldContextReference);
			if (contextReference != oldContextReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.CHART_BLOCK__CONTEXT_REFERENCE, oldContextReference, contextReference));
			}
		}
		return contextReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression basicGetContextReference() {
		return contextReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContextReference(Expression newContextReference) {
		Expression oldContextReference = contextReference;
		contextReference = newContextReference;
		boolean oldContextReferenceESet = contextReferenceESet;
		contextReferenceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__CONTEXT_REFERENCE, oldContextReference, contextReference, !oldContextReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContextReference() {
		Expression oldContextReference = contextReference;
		boolean oldContextReferenceESet = contextReferenceESet;
		contextReference = null;
		contextReferenceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__CONTEXT_REFERENCE, oldContextReference, null, oldContextReferenceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContextReference() {
		return contextReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getInstanceStructType() {
		if (instanceStructType != null && instanceStructType.eIsProxy()) {
			InternalEObject oldInstanceStructType = (InternalEObject)instanceStructType;
			instanceStructType = (GADataType)eResolveProxy(oldInstanceStructType);
			if (instanceStructType != oldInstanceStructType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.CHART_BLOCK__INSTANCE_STRUCT_TYPE, oldInstanceStructType, instanceStructType));
			}
		}
		return instanceStructType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType basicGetInstanceStructType() {
		return instanceStructType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstanceStructType(GADataType newInstanceStructType) {
		GADataType oldInstanceStructType = instanceStructType;
		instanceStructType = newInstanceStructType;
		boolean oldInstanceStructTypeESet = instanceStructTypeESet;
		instanceStructTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__INSTANCE_STRUCT_TYPE, oldInstanceStructType, instanceStructType, !oldInstanceStructTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInstanceStructType() {
		GADataType oldInstanceStructType = instanceStructType;
		boolean oldInstanceStructTypeESet = instanceStructTypeESet;
		instanceStructType = null;
		instanceStructTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__INSTANCE_STRUCT_TYPE, oldInstanceStructType, null, oldInstanceStructTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInstanceStructType() {
		return instanceStructTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function_SM getComputeFunction() {
		if (computeFunction != null && computeFunction.eIsProxy()) {
			InternalEObject oldComputeFunction = (InternalEObject)computeFunction;
			computeFunction = (Function_SM)eResolveProxy(oldComputeFunction);
			if (computeFunction != oldComputeFunction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.CHART_BLOCK__COMPUTE_FUNCTION, oldComputeFunction, computeFunction));
			}
		}
		return computeFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function_SM basicGetComputeFunction() {
		return computeFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComputeFunction(Function_SM newComputeFunction) {
		Function_SM oldComputeFunction = computeFunction;
		computeFunction = newComputeFunction;
		boolean oldComputeFunctionESet = computeFunctionESet;
		computeFunctionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__COMPUTE_FUNCTION, oldComputeFunction, computeFunction, !oldComputeFunctionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetComputeFunction() {
		Function_SM oldComputeFunction = computeFunction;
		boolean oldComputeFunctionESet = computeFunctionESet;
		computeFunction = null;
		computeFunctionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__COMPUTE_FUNCTION, oldComputeFunction, null, oldComputeFunctionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetComputeFunction() {
		return computeFunctionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Function_SM> getFunctions() {
		if (functions == null) {
			functions = new EObjectContainmentEList.Unsettable<Function_SM>(Function_SM.class, this, BlocksPackage.CHART_BLOCK__FUNCTIONS);
		}
		return functions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFunctions() {
		if (functions != null) ((InternalEList.Unsettable<?>)functions).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFunctions() {
		return functions != null && ((InternalEList.Unsettable<?>)functions).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variable_SM> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList.Unsettable<Variable_SM>(Variable_SM.class, this, BlocksPackage.CHART_BLOCK__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVariables() {
		if (variables != null) ((InternalEList.Unsettable<?>)variables).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVariables() {
		return variables != null && ((InternalEList.Unsettable<?>)variables).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChartRoot getRootLocation() {
		return rootLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootLocation(ChartRoot newRootLocation, NotificationChain msgs) {
		ChartRoot oldRootLocation = rootLocation;
		rootLocation = newRootLocation;
		boolean oldRootLocationESet = rootLocationESet;
		rootLocationESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__ROOT_LOCATION, oldRootLocation, newRootLocation, !oldRootLocationESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootLocation(ChartRoot newRootLocation) {
		if (newRootLocation != rootLocation) {
			NotificationChain msgs = null;
			if (rootLocation != null)
				msgs = ((InternalEObject)rootLocation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__ROOT_LOCATION, null, msgs);
			if (newRootLocation != null)
				msgs = ((InternalEObject)newRootLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__ROOT_LOCATION, null, msgs);
			msgs = basicSetRootLocation(newRootLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldRootLocationESet = rootLocationESet;
			rootLocationESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__ROOT_LOCATION, newRootLocation, newRootLocation, !oldRootLocationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetRootLocation(NotificationChain msgs) {
		ChartRoot oldRootLocation = rootLocation;
		rootLocation = null;
		boolean oldRootLocationESet = rootLocationESet;
		rootLocationESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__ROOT_LOCATION, oldRootLocation, null, oldRootLocationESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRootLocation() {
		if (rootLocation != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)rootLocation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__ROOT_LOCATION, null, msgs);
			msgs = basicUnsetRootLocation(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldRootLocationESet = rootLocationESet;
			rootLocationESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__ROOT_LOCATION, null, null, oldRootLocationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRootLocation() {
		return rootLocationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composition getComposition() {
		return composition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComposition(Composition newComposition, NotificationChain msgs) {
		Composition oldComposition = composition;
		composition = newComposition;
		boolean oldCompositionESet = compositionESet;
		compositionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__COMPOSITION, oldComposition, newComposition, !oldCompositionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComposition(Composition newComposition) {
		if (newComposition != composition) {
			NotificationChain msgs = null;
			if (composition != null)
				msgs = ((InternalEObject)composition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__COMPOSITION, null, msgs);
			if (newComposition != null)
				msgs = ((InternalEObject)newComposition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__COMPOSITION, null, msgs);
			msgs = basicSetComposition(newComposition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldCompositionESet = compositionESet;
			compositionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__COMPOSITION, newComposition, newComposition, !oldCompositionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetComposition(NotificationChain msgs) {
		Composition oldComposition = composition;
		composition = null;
		boolean oldCompositionESet = compositionESet;
		compositionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__COMPOSITION, oldComposition, null, oldCompositionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetComposition() {
		if (composition != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)composition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__COMPOSITION, null, msgs);
			msgs = basicUnsetComposition(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldCompositionESet = compositionESet;
			compositionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__COMPOSITION, null, null, oldCompositionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetComposition() {
		return compositionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList.Unsettable<Event>(Event.class, this, BlocksPackage.CHART_BLOCK__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEvents() {
		if (events != null) ((InternalEList.Unsettable<?>)events).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEvents() {
		return events != null && ((InternalEList.Unsettable<?>)events).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getDefaultEvent() {
		return defaultEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultEvent(Event newDefaultEvent, NotificationChain msgs) {
		Event oldDefaultEvent = defaultEvent;
		defaultEvent = newDefaultEvent;
		boolean oldDefaultEventESet = defaultEventESet;
		defaultEventESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__DEFAULT_EVENT, oldDefaultEvent, newDefaultEvent, !oldDefaultEventESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultEvent(Event newDefaultEvent) {
		if (newDefaultEvent != defaultEvent) {
			NotificationChain msgs = null;
			if (defaultEvent != null)
				msgs = ((InternalEObject)defaultEvent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__DEFAULT_EVENT, null, msgs);
			if (newDefaultEvent != null)
				msgs = ((InternalEObject)newDefaultEvent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__DEFAULT_EVENT, null, msgs);
			msgs = basicSetDefaultEvent(newDefaultEvent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDefaultEventESet = defaultEventESet;
			defaultEventESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CHART_BLOCK__DEFAULT_EVENT, newDefaultEvent, newDefaultEvent, !oldDefaultEventESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDefaultEvent(NotificationChain msgs) {
		Event oldDefaultEvent = defaultEvent;
		defaultEvent = null;
		boolean oldDefaultEventESet = defaultEventESet;
		defaultEventESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__DEFAULT_EVENT, oldDefaultEvent, null, oldDefaultEventESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDefaultEvent() {
		if (defaultEvent != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)defaultEvent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.CHART_BLOCK__DEFAULT_EVENT, null, msgs);
			msgs = basicUnsetDefaultEvent(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDefaultEventESet = defaultEventESet;
			defaultEventESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.CHART_BLOCK__DEFAULT_EVENT, null, null, oldDefaultEventESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDefaultEvent() {
		return defaultEventESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GACodeModelElement> getCodeModelElements() {
		if (codeModelElements == null) {
			codeModelElements = new EObjectContainmentEList.Unsettable<GACodeModelElement>(GACodeModelElement.class, this, BlocksPackage.CHART_BLOCK__CODE_MODEL_ELEMENTS);
		}
		return codeModelElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCodeModelElements() {
		if (codeModelElements != null) ((InternalEList.Unsettable<?>)codeModelElements).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCodeModelElements() {
		return codeModelElements != null && ((InternalEList.Unsettable<?>)codeModelElements).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependency> getExternalDependencies() {
		if (externalDependencies == null) {
			externalDependencies = new EObjectContainmentEList.Unsettable<Dependency>(Dependency.class, this, BlocksPackage.CHART_BLOCK__EXTERNAL_DEPENDENCIES);
		}
		return externalDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExternalDependencies() {
		if (externalDependencies != null) ((InternalEList.Unsettable<?>)externalDependencies).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExternalDependencies() {
		return externalDependencies != null && ((InternalEList.Unsettable<?>)externalDependencies).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.CHART_BLOCK__FUNCTIONS:
				return ((InternalEList<?>)getFunctions()).basicRemove(otherEnd, msgs);
			case BlocksPackage.CHART_BLOCK__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case BlocksPackage.CHART_BLOCK__ROOT_LOCATION:
				return basicUnsetRootLocation(msgs);
			case BlocksPackage.CHART_BLOCK__COMPOSITION:
				return basicUnsetComposition(msgs);
			case BlocksPackage.CHART_BLOCK__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
			case BlocksPackage.CHART_BLOCK__DEFAULT_EVENT:
				return basicUnsetDefaultEvent(msgs);
			case BlocksPackage.CHART_BLOCK__CODE_MODEL_ELEMENTS:
				return ((InternalEList<?>)getCodeModelElements()).basicRemove(otherEnd, msgs);
			case BlocksPackage.CHART_BLOCK__EXTERNAL_DEPENDENCIES:
				return ((InternalEList<?>)getExternalDependencies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.CHART_BLOCK__EXECUTE_AT_INIT:
				return isExecuteAtInit();
			case BlocksPackage.CHART_BLOCK__EXPORT_FUNCTIONS:
				return isExportFunctions();
			case BlocksPackage.CHART_BLOCK__INSTANCE_OUTER_REFERENCE:
				if (resolve) return getInstanceOuterReference();
				return basicGetInstanceOuterReference();
			case BlocksPackage.CHART_BLOCK__INIT_FUNCTION:
				if (resolve) return getInitFunction();
				return basicGetInitFunction();
			case BlocksPackage.CHART_BLOCK__ACTIVE_EVENT_REFERENCE:
				if (resolve) return getActiveEventReference();
				return basicGetActiveEventReference();
			case BlocksPackage.CHART_BLOCK__STATE_ARG_INTERNAL_REF:
				if (resolve) return getStateArgInternalRef();
				return basicGetStateArgInternalRef();
			case BlocksPackage.CHART_BLOCK__CONTEXT_REFERENCE:
				if (resolve) return getContextReference();
				return basicGetContextReference();
			case BlocksPackage.CHART_BLOCK__INSTANCE_STRUCT_TYPE:
				if (resolve) return getInstanceStructType();
				return basicGetInstanceStructType();
			case BlocksPackage.CHART_BLOCK__COMPUTE_FUNCTION:
				if (resolve) return getComputeFunction();
				return basicGetComputeFunction();
			case BlocksPackage.CHART_BLOCK__FUNCTIONS:
				return getFunctions();
			case BlocksPackage.CHART_BLOCK__VARIABLES:
				return getVariables();
			case BlocksPackage.CHART_BLOCK__ROOT_LOCATION:
				return getRootLocation();
			case BlocksPackage.CHART_BLOCK__COMPOSITION:
				return getComposition();
			case BlocksPackage.CHART_BLOCK__EVENTS:
				return getEvents();
			case BlocksPackage.CHART_BLOCK__DEFAULT_EVENT:
				return getDefaultEvent();
			case BlocksPackage.CHART_BLOCK__CODE_MODEL_ELEMENTS:
				return getCodeModelElements();
			case BlocksPackage.CHART_BLOCK__EXTERNAL_DEPENDENCIES:
				return getExternalDependencies();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.CHART_BLOCK__EXECUTE_AT_INIT:
				setExecuteAtInit((Boolean)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__EXPORT_FUNCTIONS:
				setExportFunctions((Boolean)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__INSTANCE_OUTER_REFERENCE:
				setInstanceOuterReference((Expression)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__INIT_FUNCTION:
				setInitFunction((Function_SM)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__ACTIVE_EVENT_REFERENCE:
				setActiveEventReference((Expression)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__STATE_ARG_INTERNAL_REF:
				setStateArgInternalRef((Expression)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__CONTEXT_REFERENCE:
				setContextReference((Expression)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__INSTANCE_STRUCT_TYPE:
				setInstanceStructType((GADataType)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__COMPUTE_FUNCTION:
				setComputeFunction((Function_SM)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__FUNCTIONS:
				getFunctions().clear();
				getFunctions().addAll((Collection<? extends Function_SM>)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends Variable_SM>)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__ROOT_LOCATION:
				setRootLocation((ChartRoot)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__COMPOSITION:
				setComposition((Composition)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__DEFAULT_EVENT:
				setDefaultEvent((Event)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__CODE_MODEL_ELEMENTS:
				getCodeModelElements().clear();
				getCodeModelElements().addAll((Collection<? extends GACodeModelElement>)newValue);
				return;
			case BlocksPackage.CHART_BLOCK__EXTERNAL_DEPENDENCIES:
				getExternalDependencies().clear();
				getExternalDependencies().addAll((Collection<? extends Dependency>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.CHART_BLOCK__EXECUTE_AT_INIT:
				unsetExecuteAtInit();
				return;
			case BlocksPackage.CHART_BLOCK__EXPORT_FUNCTIONS:
				unsetExportFunctions();
				return;
			case BlocksPackage.CHART_BLOCK__INSTANCE_OUTER_REFERENCE:
				unsetInstanceOuterReference();
				return;
			case BlocksPackage.CHART_BLOCK__INIT_FUNCTION:
				unsetInitFunction();
				return;
			case BlocksPackage.CHART_BLOCK__ACTIVE_EVENT_REFERENCE:
				unsetActiveEventReference();
				return;
			case BlocksPackage.CHART_BLOCK__STATE_ARG_INTERNAL_REF:
				unsetStateArgInternalRef();
				return;
			case BlocksPackage.CHART_BLOCK__CONTEXT_REFERENCE:
				unsetContextReference();
				return;
			case BlocksPackage.CHART_BLOCK__INSTANCE_STRUCT_TYPE:
				unsetInstanceStructType();
				return;
			case BlocksPackage.CHART_BLOCK__COMPUTE_FUNCTION:
				unsetComputeFunction();
				return;
			case BlocksPackage.CHART_BLOCK__FUNCTIONS:
				unsetFunctions();
				return;
			case BlocksPackage.CHART_BLOCK__VARIABLES:
				unsetVariables();
				return;
			case BlocksPackage.CHART_BLOCK__ROOT_LOCATION:
				unsetRootLocation();
				return;
			case BlocksPackage.CHART_BLOCK__COMPOSITION:
				unsetComposition();
				return;
			case BlocksPackage.CHART_BLOCK__EVENTS:
				unsetEvents();
				return;
			case BlocksPackage.CHART_BLOCK__DEFAULT_EVENT:
				unsetDefaultEvent();
				return;
			case BlocksPackage.CHART_BLOCK__CODE_MODEL_ELEMENTS:
				unsetCodeModelElements();
				return;
			case BlocksPackage.CHART_BLOCK__EXTERNAL_DEPENDENCIES:
				unsetExternalDependencies();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.CHART_BLOCK__EXECUTE_AT_INIT:
				return isSetExecuteAtInit();
			case BlocksPackage.CHART_BLOCK__EXPORT_FUNCTIONS:
				return isSetExportFunctions();
			case BlocksPackage.CHART_BLOCK__INSTANCE_OUTER_REFERENCE:
				return isSetInstanceOuterReference();
			case BlocksPackage.CHART_BLOCK__INIT_FUNCTION:
				return isSetInitFunction();
			case BlocksPackage.CHART_BLOCK__ACTIVE_EVENT_REFERENCE:
				return isSetActiveEventReference();
			case BlocksPackage.CHART_BLOCK__STATE_ARG_INTERNAL_REF:
				return isSetStateArgInternalRef();
			case BlocksPackage.CHART_BLOCK__CONTEXT_REFERENCE:
				return isSetContextReference();
			case BlocksPackage.CHART_BLOCK__INSTANCE_STRUCT_TYPE:
				return isSetInstanceStructType();
			case BlocksPackage.CHART_BLOCK__COMPUTE_FUNCTION:
				return isSetComputeFunction();
			case BlocksPackage.CHART_BLOCK__FUNCTIONS:
				return isSetFunctions();
			case BlocksPackage.CHART_BLOCK__VARIABLES:
				return isSetVariables();
			case BlocksPackage.CHART_BLOCK__ROOT_LOCATION:
				return isSetRootLocation();
			case BlocksPackage.CHART_BLOCK__COMPOSITION:
				return isSetComposition();
			case BlocksPackage.CHART_BLOCK__EVENTS:
				return isSetEvents();
			case BlocksPackage.CHART_BLOCK__DEFAULT_EVENT:
				return isSetDefaultEvent();
			case BlocksPackage.CHART_BLOCK__CODE_MODEL_ELEMENTS:
				return isSetCodeModelElements();
			case BlocksPackage.CHART_BLOCK__EXTERNAL_DEPENDENCIES:
				return isSetExternalDependencies();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executeAtInit: ");
		if (executeAtInitESet) result.append(executeAtInit); else result.append("<unset>");
		result.append(", exportFunctions: ");
		if (exportFunctionsESet) result.append(exportFunctions); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ChartBlockImpl
