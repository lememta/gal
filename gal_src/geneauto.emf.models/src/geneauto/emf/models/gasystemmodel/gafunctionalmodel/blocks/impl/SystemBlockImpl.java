/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.gacodemodel.CustomType_CM;
import geneauto.emf.models.gacodemodel.Variable_CM;

import geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle;

import geneauto.emf.models.gasystemmodel.common.Variable_SM;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;

import geneauto.emf.models.gasystemmodel.gastatemodel.Event;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getFcnName <em>Fcn Name</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getStyle <em>Style</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getIndexVariable <em>Index Variable</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getSignals <em>Signals</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getBlocks <em>Blocks</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SystemBlockImpl#getCustomTypes <em>Custom Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SystemBlockImpl extends BlockImpl implements SystemBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getFcnName() <em>Fcn Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFcnName()
	 * @generated
	 * @ordered
	 */
	protected static final String FCN_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFcnName() <em>Fcn Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFcnName()
	 * @generated
	 * @ordered
	 */
	protected String fcnName = FCN_NAME_EDEFAULT;

	/**
	 * This is true if the Fcn Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fcnNameESet;

	/**
	 * The default value of the '{@link #getStyle() <em>Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStyle()
	 * @generated
	 * @ordered
	 */
	protected static final FunctionStyle STYLE_EDEFAULT = FunctionStyle.FUNCTION;

	/**
	 * The cached value of the '{@link #getStyle() <em>Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStyle()
	 * @generated
	 * @ordered
	 */
	protected FunctionStyle style = STYLE_EDEFAULT;

	/**
	 * This is true if the Style attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean styleESet;

	/**
	 * The cached value of the '{@link #getIndexVariable() <em>Index Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable_CM indexVariable;

	/**
	 * This is true if the Index Variable reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean indexVariableESet;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable_SM> variables;

	/**
	 * The cached value of the '{@link #getSignals() <em>Signals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignals()
	 * @generated
	 * @ordered
	 */
	protected EList<Signal> signals;

	/**
	 * The cached value of the '{@link #getBlocks() <em>Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<Block> blocks;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getCustomTypes() <em>Custom Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<CustomType_CM> customTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.SYSTEM_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFcnName() {
		return fcnName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFcnName(String newFcnName) {
		String oldFcnName = fcnName;
		fcnName = newFcnName;
		boolean oldFcnNameESet = fcnNameESet;
		fcnNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SYSTEM_BLOCK__FCN_NAME, oldFcnName, fcnName, !oldFcnNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFcnName() {
		String oldFcnName = fcnName;
		boolean oldFcnNameESet = fcnNameESet;
		fcnName = FCN_NAME_EDEFAULT;
		fcnNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SYSTEM_BLOCK__FCN_NAME, oldFcnName, FCN_NAME_EDEFAULT, oldFcnNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFcnName() {
		return fcnNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionStyle getStyle() {
		return style;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStyle(FunctionStyle newStyle) {
		FunctionStyle oldStyle = style;
		style = newStyle == null ? STYLE_EDEFAULT : newStyle;
		boolean oldStyleESet = styleESet;
		styleESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SYSTEM_BLOCK__STYLE, oldStyle, style, !oldStyleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStyle() {
		FunctionStyle oldStyle = style;
		boolean oldStyleESet = styleESet;
		style = STYLE_EDEFAULT;
		styleESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SYSTEM_BLOCK__STYLE, oldStyle, STYLE_EDEFAULT, oldStyleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStyle() {
		return styleESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable_CM getIndexVariable() {
		if (indexVariable != null && indexVariable.eIsProxy()) {
			InternalEObject oldIndexVariable = (InternalEObject)indexVariable;
			indexVariable = (Variable_CM)eResolveProxy(oldIndexVariable);
			if (indexVariable != oldIndexVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.SYSTEM_BLOCK__INDEX_VARIABLE, oldIndexVariable, indexVariable));
			}
		}
		return indexVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable_CM basicGetIndexVariable() {
		return indexVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexVariable(Variable_CM newIndexVariable) {
		Variable_CM oldIndexVariable = indexVariable;
		indexVariable = newIndexVariable;
		boolean oldIndexVariableESet = indexVariableESet;
		indexVariableESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SYSTEM_BLOCK__INDEX_VARIABLE, oldIndexVariable, indexVariable, !oldIndexVariableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIndexVariable() {
		Variable_CM oldIndexVariable = indexVariable;
		boolean oldIndexVariableESet = indexVariableESet;
		indexVariable = null;
		indexVariableESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SYSTEM_BLOCK__INDEX_VARIABLE, oldIndexVariable, null, oldIndexVariableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIndexVariable() {
		return indexVariableESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variable_SM> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList.Unsettable<Variable_SM>(Variable_SM.class, this, BlocksPackage.SYSTEM_BLOCK__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVariables() {
		if (variables != null) ((InternalEList.Unsettable<?>)variables).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVariables() {
		return variables != null && ((InternalEList.Unsettable<?>)variables).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Signal> getSignals() {
		if (signals == null) {
			signals = new EObjectContainmentEList.Unsettable<Signal>(Signal.class, this, BlocksPackage.SYSTEM_BLOCK__SIGNALS);
		}
		return signals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSignals() {
		if (signals != null) ((InternalEList.Unsettable<?>)signals).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSignals() {
		return signals != null && ((InternalEList.Unsettable<?>)signals).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Block> getBlocks() {
		if (blocks == null) {
			blocks = new EObjectContainmentEList.Unsettable<Block>(Block.class, this, BlocksPackage.SYSTEM_BLOCK__BLOCKS);
		}
		return blocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBlocks() {
		if (blocks != null) ((InternalEList.Unsettable<?>)blocks).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBlocks() {
		return blocks != null && ((InternalEList.Unsettable<?>)blocks).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList.Unsettable<Event>(Event.class, this, BlocksPackage.SYSTEM_BLOCK__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEvents() {
		if (events != null) ((InternalEList.Unsettable<?>)events).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEvents() {
		return events != null && ((InternalEList.Unsettable<?>)events).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CustomType_CM> getCustomTypes() {
		if (customTypes == null) {
			customTypes = new EObjectContainmentEList.Unsettable<CustomType_CM>(CustomType_CM.class, this, BlocksPackage.SYSTEM_BLOCK__CUSTOM_TYPES);
		}
		return customTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCustomTypes() {
		if (customTypes != null) ((InternalEList.Unsettable<?>)customTypes).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCustomTypes() {
		return customTypes != null && ((InternalEList.Unsettable<?>)customTypes).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.SYSTEM_BLOCK__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case BlocksPackage.SYSTEM_BLOCK__SIGNALS:
				return ((InternalEList<?>)getSignals()).basicRemove(otherEnd, msgs);
			case BlocksPackage.SYSTEM_BLOCK__BLOCKS:
				return ((InternalEList<?>)getBlocks()).basicRemove(otherEnd, msgs);
			case BlocksPackage.SYSTEM_BLOCK__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
			case BlocksPackage.SYSTEM_BLOCK__CUSTOM_TYPES:
				return ((InternalEList<?>)getCustomTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.SYSTEM_BLOCK__FCN_NAME:
				return getFcnName();
			case BlocksPackage.SYSTEM_BLOCK__STYLE:
				return getStyle();
			case BlocksPackage.SYSTEM_BLOCK__INDEX_VARIABLE:
				if (resolve) return getIndexVariable();
				return basicGetIndexVariable();
			case BlocksPackage.SYSTEM_BLOCK__VARIABLES:
				return getVariables();
			case BlocksPackage.SYSTEM_BLOCK__SIGNALS:
				return getSignals();
			case BlocksPackage.SYSTEM_BLOCK__BLOCKS:
				return getBlocks();
			case BlocksPackage.SYSTEM_BLOCK__EVENTS:
				return getEvents();
			case BlocksPackage.SYSTEM_BLOCK__CUSTOM_TYPES:
				return getCustomTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.SYSTEM_BLOCK__FCN_NAME:
				setFcnName((String)newValue);
				return;
			case BlocksPackage.SYSTEM_BLOCK__STYLE:
				setStyle((FunctionStyle)newValue);
				return;
			case BlocksPackage.SYSTEM_BLOCK__INDEX_VARIABLE:
				setIndexVariable((Variable_CM)newValue);
				return;
			case BlocksPackage.SYSTEM_BLOCK__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends Variable_SM>)newValue);
				return;
			case BlocksPackage.SYSTEM_BLOCK__SIGNALS:
				getSignals().clear();
				getSignals().addAll((Collection<? extends Signal>)newValue);
				return;
			case BlocksPackage.SYSTEM_BLOCK__BLOCKS:
				getBlocks().clear();
				getBlocks().addAll((Collection<? extends Block>)newValue);
				return;
			case BlocksPackage.SYSTEM_BLOCK__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case BlocksPackage.SYSTEM_BLOCK__CUSTOM_TYPES:
				getCustomTypes().clear();
				getCustomTypes().addAll((Collection<? extends CustomType_CM>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.SYSTEM_BLOCK__FCN_NAME:
				unsetFcnName();
				return;
			case BlocksPackage.SYSTEM_BLOCK__STYLE:
				unsetStyle();
				return;
			case BlocksPackage.SYSTEM_BLOCK__INDEX_VARIABLE:
				unsetIndexVariable();
				return;
			case BlocksPackage.SYSTEM_BLOCK__VARIABLES:
				unsetVariables();
				return;
			case BlocksPackage.SYSTEM_BLOCK__SIGNALS:
				unsetSignals();
				return;
			case BlocksPackage.SYSTEM_BLOCK__BLOCKS:
				unsetBlocks();
				return;
			case BlocksPackage.SYSTEM_BLOCK__EVENTS:
				unsetEvents();
				return;
			case BlocksPackage.SYSTEM_BLOCK__CUSTOM_TYPES:
				unsetCustomTypes();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.SYSTEM_BLOCK__FCN_NAME:
				return isSetFcnName();
			case BlocksPackage.SYSTEM_BLOCK__STYLE:
				return isSetStyle();
			case BlocksPackage.SYSTEM_BLOCK__INDEX_VARIABLE:
				return isSetIndexVariable();
			case BlocksPackage.SYSTEM_BLOCK__VARIABLES:
				return isSetVariables();
			case BlocksPackage.SYSTEM_BLOCK__SIGNALS:
				return isSetSignals();
			case BlocksPackage.SYSTEM_BLOCK__BLOCKS:
				return isSetBlocks();
			case BlocksPackage.SYSTEM_BLOCK__EVENTS:
				return isSetEvents();
			case BlocksPackage.SYSTEM_BLOCK__CUSTOM_TYPES:
				return isSetCustomTypes();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fcnName: ");
		if (fcnNameESet) result.append(fcnName); else result.append("<unset>");
		result.append(", style: ");
		if (styleESet) result.append(style); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //SystemBlockImpl
