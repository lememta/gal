/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel;

import geneauto.emf.models.gasystemmodel.common.CommonPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.GafunctionalmodelFactory
 * @model kind="package"
 * @generated
 */
public interface GafunctionalmodelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gafunctionalmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gasystemmodel/gafunctionalmodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gasystemmodel.gafunctionalmodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GafunctionalmodelPackage eINSTANCE = geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl <em>Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl#getSignal()
	 * @generated
	 */
	int SIGNAL = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__EXTERNAL_ID = CommonPackage.DATA__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__ID = CommonPackage.DATA__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__IS_FIXED_NAME = CommonPackage.DATA__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__NAME = CommonPackage.DATA__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__ORIGINAL_FULL_NAME = CommonPackage.DATA__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__ORIGINAL_NAME = CommonPackage.DATA__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__PARENT = CommonPackage.DATA__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__MODEL = CommonPackage.DATA__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__ANNOTATIONS = CommonPackage.DATA__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__CODE_MODEL_ELEMENT = CommonPackage.DATA__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__STORAGE_CLASS = CommonPackage.DATA__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Observation Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__OBSERVATION_POINTS = CommonPackage.DATA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sample Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__SAMPLE_TIME = CommonPackage.DATA_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dst Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__DST_PORT = CommonPackage.DATA_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Src Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__SRC_PORT = CommonPackage.DATA_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__DATA_TYPE = CommonPackage.DATA_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FEATURE_COUNT = CommonPackage.DATA_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskImpl <em>Mask</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl#getMask()
	 * @generated
	 */
	int MASK = 1;

	/**
	 * The feature id for the '<em><b>Mask Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MASK__MASK_VARIABLES = 0;

	/**
	 * The number of structural features of the '<em>Mask</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MASK_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskVariableImpl <em>Mask Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskVariableImpl
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl#getMaskVariable()
	 * @generated
	 */
	int MASK_VARIABLE = 2;

	/**
	 * The number of structural features of the '<em>Mask Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MASK_VARIABLE_FEATURE_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal
	 * @generated
	 */
	EClass getSignal();

	/**
	 * Returns the meta object for the attribute list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getObservationPoints <em>Observation Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Observation Points</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getObservationPoints()
	 * @see #getSignal()
	 * @generated
	 */
	EAttribute getSignal_ObservationPoints();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getSampleTime <em>Sample Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sample Time</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getSampleTime()
	 * @see #getSignal()
	 * @generated
	 */
	EAttribute getSignal_SampleTime();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getDstPort <em>Dst Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dst Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getDstPort()
	 * @see #getSignal()
	 * @generated
	 */
	EReference getSignal_DstPort();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getSrcPort <em>Src Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Src Port</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getSrcPort()
	 * @see #getSignal()
	 * @generated
	 */
	EReference getSignal_SrcPort();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal#getDataType()
	 * @see #getSignal()
	 * @generated
	 */
	EReference getSignal_DataType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Mask <em>Mask</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mask</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Mask
	 * @generated
	 */
	EClass getMask();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.Mask#getMaskVariables <em>Mask Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Mask Variables</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.Mask#getMaskVariables()
	 * @see #getMask()
	 * @generated
	 */
	EReference getMask_MaskVariables();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.MaskVariable <em>Mask Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mask Variable</em>'.
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.MaskVariable
	 * @generated
	 */
	EClass getMaskVariable();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GafunctionalmodelFactory getGafunctionalmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl <em>Signal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.SignalImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl#getSignal()
		 * @generated
		 */
		EClass SIGNAL = eINSTANCE.getSignal();

		/**
		 * The meta object literal for the '<em><b>Observation Points</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL__OBSERVATION_POINTS = eINSTANCE.getSignal_ObservationPoints();

		/**
		 * The meta object literal for the '<em><b>Sample Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL__SAMPLE_TIME = eINSTANCE.getSignal_SampleTime();

		/**
		 * The meta object literal for the '<em><b>Dst Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL__DST_PORT = eINSTANCE.getSignal_DstPort();

		/**
		 * The meta object literal for the '<em><b>Src Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL__SRC_PORT = eINSTANCE.getSignal_SrcPort();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL__DATA_TYPE = eINSTANCE.getSignal_DataType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskImpl <em>Mask</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl#getMask()
		 * @generated
		 */
		EClass MASK = eINSTANCE.getMask();

		/**
		 * The meta object literal for the '<em><b>Mask Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MASK__MASK_VARIABLES = eINSTANCE.getMask_MaskVariables();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskVariableImpl <em>Mask Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.MaskVariableImpl
		 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl#getMaskVariable()
		 * @generated
		 */
		EClass MASK_VARIABLE = eINSTANCE.getMaskVariable();

	}

} //GafunctionalmodelPackage
