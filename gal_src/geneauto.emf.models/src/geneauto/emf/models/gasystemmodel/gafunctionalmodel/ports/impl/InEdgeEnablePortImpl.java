/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.InEdgeEnablePort;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import geneauto.emf.models.gasystemmodel.gastatemodel.Event;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Edge Enable Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEdgeEnablePortImpl#getTriggerType <em>Trigger Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEdgeEnablePortImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.InEdgeEnablePortImpl#getOutputDataType <em>Output Data Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InEdgeEnablePortImpl extends InportImpl implements InEdgeEnablePort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getTriggerType() <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerType()
	 * @generated
	 * @ordered
	 */
	protected static final String TRIGGER_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTriggerType() <em>Trigger Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerType()
	 * @generated
	 * @ordered
	 */
	protected String triggerType = TRIGGER_TYPE_EDEFAULT;

	/**
	 * This is true if the Trigger Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean triggerTypeESet;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getOutputDataType() <em>Output Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputDataType()
	 * @generated
	 * @ordered
	 */
	protected GADataType outputDataType;

	/**
	 * This is true if the Output Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean outputDataTypeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InEdgeEnablePortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortsPackage.Literals.IN_EDGE_ENABLE_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTriggerType() {
		return triggerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTriggerType(String newTriggerType) {
		String oldTriggerType = triggerType;
		triggerType = newTriggerType;
		boolean oldTriggerTypeESet = triggerTypeESet;
		triggerTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.IN_EDGE_ENABLE_PORT__TRIGGER_TYPE, oldTriggerType, triggerType, !oldTriggerTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTriggerType() {
		String oldTriggerType = triggerType;
		boolean oldTriggerTypeESet = triggerTypeESet;
		triggerType = TRIGGER_TYPE_EDEFAULT;
		triggerTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.IN_EDGE_ENABLE_PORT__TRIGGER_TYPE, oldTriggerType, TRIGGER_TYPE_EDEFAULT, oldTriggerTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTriggerType() {
		return triggerTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectResolvingEList.Unsettable<Event>(Event.class, this, PortsPackage.IN_EDGE_ENABLE_PORT__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEvents() {
		if (events != null) ((InternalEList.Unsettable<?>)events).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEvents() {
		return events != null && ((InternalEList.Unsettable<?>)events).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getOutputDataType() {
		return outputDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputDataType(GADataType newOutputDataType, NotificationChain msgs) {
		GADataType oldOutputDataType = outputDataType;
		outputDataType = newOutputDataType;
		boolean oldOutputDataTypeESet = outputDataTypeESet;
		outputDataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE, oldOutputDataType, newOutputDataType, !oldOutputDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputDataType(GADataType newOutputDataType) {
		if (newOutputDataType != outputDataType) {
			NotificationChain msgs = null;
			if (outputDataType != null)
				msgs = ((InternalEObject)outputDataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE, null, msgs);
			if (newOutputDataType != null)
				msgs = ((InternalEObject)newOutputDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE, null, msgs);
			msgs = basicSetOutputDataType(newOutputDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldOutputDataTypeESet = outputDataTypeESet;
			outputDataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE, newOutputDataType, newOutputDataType, !oldOutputDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetOutputDataType(NotificationChain msgs) {
		GADataType oldOutputDataType = outputDataType;
		outputDataType = null;
		boolean oldOutputDataTypeESet = outputDataTypeESet;
		outputDataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE, oldOutputDataType, null, oldOutputDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOutputDataType() {
		if (outputDataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)outputDataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE, null, msgs);
			msgs = basicUnsetOutputDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldOutputDataTypeESet = outputDataTypeESet;
			outputDataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE, null, null, oldOutputDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOutputDataType() {
		return outputDataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE:
				return basicUnsetOutputDataType(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortsPackage.IN_EDGE_ENABLE_PORT__TRIGGER_TYPE:
				return getTriggerType();
			case PortsPackage.IN_EDGE_ENABLE_PORT__EVENTS:
				return getEvents();
			case PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE:
				return getOutputDataType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortsPackage.IN_EDGE_ENABLE_PORT__TRIGGER_TYPE:
				setTriggerType((String)newValue);
				return;
			case PortsPackage.IN_EDGE_ENABLE_PORT__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE:
				setOutputDataType((GADataType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortsPackage.IN_EDGE_ENABLE_PORT__TRIGGER_TYPE:
				unsetTriggerType();
				return;
			case PortsPackage.IN_EDGE_ENABLE_PORT__EVENTS:
				unsetEvents();
				return;
			case PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE:
				unsetOutputDataType();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortsPackage.IN_EDGE_ENABLE_PORT__TRIGGER_TYPE:
				return isSetTriggerType();
			case PortsPackage.IN_EDGE_ENABLE_PORT__EVENTS:
				return isSetEvents();
			case PortsPackage.IN_EDGE_ENABLE_PORT__OUTPUT_DATA_TYPE:
				return isSetOutputDataType();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (triggerType: ");
		if (triggerTypeESet) result.append(triggerType); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //InEdgeEnablePortImpl
