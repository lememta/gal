/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl;

import geneauto.emf.models.gacodemodel.expression.VariableExpression;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;
import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.SinkBlock;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sink Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.SinkBlockImpl#getVariableReference <em>Variable Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SinkBlockImpl extends BlockImpl implements SinkBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getVariableReference() <em>Variable Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableReference()
	 * @generated
	 * @ordered
	 */
	protected VariableExpression variableReference;

	/**
	 * This is true if the Variable Reference containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean variableReferenceESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SinkBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.SINK_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableExpression getVariableReference() {
		return variableReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariableReference(VariableExpression newVariableReference, NotificationChain msgs) {
		VariableExpression oldVariableReference = variableReference;
		variableReference = newVariableReference;
		boolean oldVariableReferenceESet = variableReferenceESet;
		variableReferenceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE, oldVariableReference, newVariableReference, !oldVariableReferenceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariableReference(VariableExpression newVariableReference) {
		if (newVariableReference != variableReference) {
			NotificationChain msgs = null;
			if (variableReference != null)
				msgs = ((InternalEObject)variableReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE, null, msgs);
			if (newVariableReference != null)
				msgs = ((InternalEObject)newVariableReference).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE, null, msgs);
			msgs = basicSetVariableReference(newVariableReference, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldVariableReferenceESet = variableReferenceESet;
			variableReferenceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE, newVariableReference, newVariableReference, !oldVariableReferenceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetVariableReference(NotificationChain msgs) {
		VariableExpression oldVariableReference = variableReference;
		variableReference = null;
		boolean oldVariableReferenceESet = variableReferenceESet;
		variableReferenceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE, oldVariableReference, null, oldVariableReferenceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVariableReference() {
		if (variableReference != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)variableReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE, null, msgs);
			msgs = basicUnsetVariableReference(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldVariableReferenceESet = variableReferenceESet;
			variableReferenceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE, null, null, oldVariableReferenceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVariableReference() {
		return variableReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE:
				return basicUnsetVariableReference(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE:
				return getVariableReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE:
				setVariableReference((VariableExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE:
				unsetVariableReference();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.SINK_BLOCK__VARIABLE_REFERENCE:
				return isSetVariableReference();
		}
		return super.eIsSet(featureID);
	}

} //SinkBlockImpl
