/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Reference to a subsystem in different model file  TODO: not in use. Decide if it will be required in the future or should be removed from the model
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock#getSourceModel <em>Source Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getReferenceBlock()
 * @model
 * @generated
 */
public interface ReferenceBlock extends Block {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Source Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Model</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Model</em>' attribute.
	 * @see #isSetSourceModel()
	 * @see #unsetSourceModel()
	 * @see #setSourceModel(int)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage#getReferenceBlock_SourceModel()
	 * @model unsettable="true"
	 * @generated
	 */
	int getSourceModel();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock#getSourceModel <em>Source Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Model</em>' attribute.
	 * @see #isSetSourceModel()
	 * @see #unsetSourceModel()
	 * @see #getSourceModel()
	 * @generated
	 */
	void setSourceModel(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock#getSourceModel <em>Source Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSourceModel()
	 * @see #getSourceModel()
	 * @see #setSourceModel(int)
	 * @generated
	 */
	void unsetSourceModel();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.ReferenceBlock#getSourceModel <em>Source Model</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Source Model</em>' attribute is set.
	 * @see #unsetSourceModel()
	 * @see #getSourceModel()
	 * @see #setSourceModel(int)
	 * @generated
	 */
	boolean isSetSourceModel();

} // ReferenceBlock
