/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.common.Variable_SM;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Out Data Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Block interface producing data signals
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#isResetOutput <em>Reset Output</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getVariable <em>Variable</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getDataType <em>Data Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getInitialOutput <em>Initial Output</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getOutDataPort()
 * @model
 * @generated
 */
public interface OutDataPort extends Outport {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Reset Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reset Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reset Output</em>' attribute.
	 * @see #isSetResetOutput()
	 * @see #unsetResetOutput()
	 * @see #setResetOutput(boolean)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getOutDataPort_ResetOutput()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isResetOutput();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#isResetOutput <em>Reset Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reset Output</em>' attribute.
	 * @see #isSetResetOutput()
	 * @see #unsetResetOutput()
	 * @see #isResetOutput()
	 * @generated
	 */
	void setResetOutput(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#isResetOutput <em>Reset Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResetOutput()
	 * @see #isResetOutput()
	 * @see #setResetOutput(boolean)
	 * @generated
	 */
	void unsetResetOutput();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#isResetOutput <em>Reset Output</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Reset Output</em>' attribute is set.
	 * @see #unsetResetOutput()
	 * @see #isResetOutput()
	 * @see #setResetOutput(boolean)
	 * @generated
	 */
	boolean isSetResetOutput();

	/**
	 * Returns the value of the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' reference.
	 * @see #isSetVariable()
	 * @see #unsetVariable()
	 * @see #setVariable(Variable_SM)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getOutDataPort_Variable()
	 * @model unsettable="true"
	 * @generated
	 */
	Variable_SM getVariable();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getVariable <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' reference.
	 * @see #isSetVariable()
	 * @see #unsetVariable()
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(Variable_SM value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getVariable <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVariable()
	 * @see #getVariable()
	 * @see #setVariable(Variable_SM)
	 * @generated
	 */
	void unsetVariable();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getVariable <em>Variable</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Variable</em>' reference is set.
	 * @see #unsetVariable()
	 * @see #getVariable()
	 * @see #setVariable(Variable_SM)
	 * @generated
	 */
	boolean isSetVariable();

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #setDataType(GADataType)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getOutDataPort_DataType()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	GADataType getDataType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(GADataType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDataType()
	 * @see #getDataType()
	 * @see #setDataType(GADataType)
	 * @generated
	 */
	void unsetDataType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getDataType <em>Data Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Data Type</em>' containment reference is set.
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @see #setDataType(GADataType)
	 * @generated
	 */
	boolean isSetDataType();

	/**
	 * Returns the value of the '<em><b>Initial Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Output</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Output</em>' containment reference.
	 * @see #isSetInitialOutput()
	 * @see #unsetInitialOutput()
	 * @see #setInitialOutput(Expression)
	 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage#getOutDataPort_InitialOutput()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getInitialOutput();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getInitialOutput <em>Initial Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Output</em>' containment reference.
	 * @see #isSetInitialOutput()
	 * @see #unsetInitialOutput()
	 * @see #getInitialOutput()
	 * @generated
	 */
	void setInitialOutput(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getInitialOutput <em>Initial Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInitialOutput()
	 * @see #getInitialOutput()
	 * @see #setInitialOutput(Expression)
	 * @generated
	 */
	void unsetInitialOutput();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort#getInitialOutput <em>Initial Output</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Initial Output</em>' containment reference is set.
	 * @see #unsetInitialOutput()
	 * @see #getInitialOutput()
	 * @see #setInitialOutput(Expression)
	 * @generated
	 */
	boolean isSetInitialOutput();

} // OutDataPort
