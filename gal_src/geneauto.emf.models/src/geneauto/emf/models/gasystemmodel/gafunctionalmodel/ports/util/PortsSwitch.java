/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.util;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.*;

import geneauto.emf.models.genericmodel.GAModelElement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage
 * @generated
 */
public class PortsSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PortsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortsSwitch() {
		if (modelPackage == null) {
			modelPackage = PortsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case PortsPackage.PORT: {
				Port port = (Port)theEObject;
				T result = casePort(port);
				if (result == null) result = caseGASystemModelElement(port);
				if (result == null) result = caseGAModelElement(port);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.IN_DATA_PORT: {
				InDataPort inDataPort = (InDataPort)theEObject;
				T result = caseInDataPort(inDataPort);
				if (result == null) result = caseInport(inDataPort);
				if (result == null) result = casePort(inDataPort);
				if (result == null) result = caseGASystemModelElement(inDataPort);
				if (result == null) result = caseGAModelElement(inDataPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.INPORT: {
				Inport inport = (Inport)theEObject;
				T result = caseInport(inport);
				if (result == null) result = casePort(inport);
				if (result == null) result = caseGASystemModelElement(inport);
				if (result == null) result = caseGAModelElement(inport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.IN_ENABLE_PORT: {
				InEnablePort inEnablePort = (InEnablePort)theEObject;
				T result = caseInEnablePort(inEnablePort);
				if (result == null) result = caseInport(inEnablePort);
				if (result == null) result = casePort(inEnablePort);
				if (result == null) result = caseGASystemModelElement(inEnablePort);
				if (result == null) result = caseGAModelElement(inEnablePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.IN_EDGE_ENABLE_PORT: {
				InEdgeEnablePort inEdgeEnablePort = (InEdgeEnablePort)theEObject;
				T result = caseInEdgeEnablePort(inEdgeEnablePort);
				if (result == null) result = caseInport(inEdgeEnablePort);
				if (result == null) result = casePort(inEdgeEnablePort);
				if (result == null) result = caseGASystemModelElement(inEdgeEnablePort);
				if (result == null) result = caseGAModelElement(inEdgeEnablePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.IN_CONTROL_PORT: {
				InControlPort inControlPort = (InControlPort)theEObject;
				T result = caseInControlPort(inControlPort);
				if (result == null) result = caseInport(inControlPort);
				if (result == null) result = casePort(inControlPort);
				if (result == null) result = caseGASystemModelElement(inControlPort);
				if (result == null) result = caseGAModelElement(inControlPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.OUT_DATA_PORT: {
				OutDataPort outDataPort = (OutDataPort)theEObject;
				T result = caseOutDataPort(outDataPort);
				if (result == null) result = caseOutport(outDataPort);
				if (result == null) result = casePort(outDataPort);
				if (result == null) result = caseGASystemModelElement(outDataPort);
				if (result == null) result = caseGAModelElement(outDataPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.OUTPORT: {
				Outport outport = (Outport)theEObject;
				T result = caseOutport(outport);
				if (result == null) result = casePort(outport);
				if (result == null) result = caseGASystemModelElement(outport);
				if (result == null) result = caseGAModelElement(outport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortsPackage.OUT_CONTROL_PORT: {
				OutControlPort outControlPort = (OutControlPort)theEObject;
				T result = caseOutControlPort(outControlPort);
				if (result == null) result = caseOutport(outControlPort);
				if (result == null) result = casePort(outControlPort);
				if (result == null) result = caseGASystemModelElement(outControlPort);
				if (result == null) result = caseGAModelElement(outControlPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Data Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInDataPort(InDataPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inport</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inport</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInport(Inport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Enable Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Enable Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInEnablePort(InEnablePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Edge Enable Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Edge Enable Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInEdgeEnablePort(InEdgeEnablePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Control Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Control Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInControlPort(InControlPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Out Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Out Data Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutDataPort(OutDataPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Outport</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Outport</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutport(Outport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Out Control Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Out Control Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutControlPort(OutControlPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGAModelElement(GAModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA System Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA System Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGASystemModelElement(GASystemModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //PortsSwitch
