/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel;

import geneauto.emf.models.gablocklibrary.GABlockLibrary;

import geneauto.emf.models.gacodemodel.GACodeModel;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block;

import geneauto.emf.models.genericmodel.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GA System Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * GASystemModel language is the gene-auto language for encoding system models.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.GASystemModel#getCodeModel <em>Code Model</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.GASystemModel#getBlockLibrary <em>Block Library</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.GASystemModel#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.GasystemmodelPackage#getGASystemModel()
 * @model
 * @generated
 */
public interface GASystemModel extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Code Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Model</em>' reference.
	 * @see #isSetCodeModel()
	 * @see #unsetCodeModel()
	 * @see #setCodeModel(GACodeModel)
	 * @see geneauto.emf.models.gasystemmodel.GasystemmodelPackage#getGASystemModel_CodeModel()
	 * @model unsettable="true"
	 * @generated
	 */
	GACodeModel getCodeModel();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getCodeModel <em>Code Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code Model</em>' reference.
	 * @see #isSetCodeModel()
	 * @see #unsetCodeModel()
	 * @see #getCodeModel()
	 * @generated
	 */
	void setCodeModel(GACodeModel value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getCodeModel <em>Code Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCodeModel()
	 * @see #getCodeModel()
	 * @see #setCodeModel(GACodeModel)
	 * @generated
	 */
	void unsetCodeModel();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getCodeModel <em>Code Model</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Code Model</em>' reference is set.
	 * @see #unsetCodeModel()
	 * @see #getCodeModel()
	 * @see #setCodeModel(GACodeModel)
	 * @generated
	 */
	boolean isSetCodeModel();

	/**
	 * Returns the value of the '<em><b>Block Library</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Library</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Library</em>' reference.
	 * @see #isSetBlockLibrary()
	 * @see #unsetBlockLibrary()
	 * @see #setBlockLibrary(GABlockLibrary)
	 * @see geneauto.emf.models.gasystemmodel.GasystemmodelPackage#getGASystemModel_BlockLibrary()
	 * @model unsettable="true"
	 * @generated
	 */
	GABlockLibrary getBlockLibrary();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getBlockLibrary <em>Block Library</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block Library</em>' reference.
	 * @see #isSetBlockLibrary()
	 * @see #unsetBlockLibrary()
	 * @see #getBlockLibrary()
	 * @generated
	 */
	void setBlockLibrary(GABlockLibrary value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getBlockLibrary <em>Block Library</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBlockLibrary()
	 * @see #getBlockLibrary()
	 * @see #setBlockLibrary(GABlockLibrary)
	 * @generated
	 */
	void unsetBlockLibrary();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getBlockLibrary <em>Block Library</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Block Library</em>' reference is set.
	 * @see #unsetBlockLibrary()
	 * @see #getBlockLibrary()
	 * @see #setBlockLibrary(GABlockLibrary)
	 * @generated
	 */
	boolean isSetBlockLibrary();

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.GASystemModelRoot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see #isSetElements()
	 * @see #unsetElements()
	 * @see geneauto.emf.models.gasystemmodel.GasystemmodelPackage#getGASystemModel_Elements()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<GASystemModelRoot> getElements();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getElements <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElements()
	 * @see #getElements()
	 * @generated
	 */
	void unsetElements();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getElements <em>Elements</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Elements</em>' containment reference list is set.
	 * @see #unsetElements()
	 * @see #getElements()
	 * @generated
	 */
	boolean isSetElements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	EList<Block> All_Blocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	EList<Signal> All_Signals();

} // GASystemModel
