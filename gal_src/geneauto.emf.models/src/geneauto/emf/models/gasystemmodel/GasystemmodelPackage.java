/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.GasystemmodelFactory
 * @model kind="package"
 * @generated
 */
public interface GasystemmodelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gasystemmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gasystemmodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gasystemmodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GasystemmodelPackage eINSTANCE = geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.GASystemModelRoot <em>GA System Model Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.GASystemModelRoot
	 * @see geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl#getGASystemModelRoot()
	 * @generated
	 */
	int GA_SYSTEM_MODEL_ROOT = 0;

	/**
	 * The number of structural features of the '<em>GA System Model Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ROOT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelImpl <em>GA System Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.impl.GASystemModelImpl
	 * @see geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl#getGASystemModel()
	 * @generated
	 */
	int GA_SYSTEM_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Last Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__LAST_ID = GenericmodelPackage.MODEL__LAST_ID;

	/**
	 * The feature id for the '<em><b>Last Saved By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__LAST_SAVED_BY = GenericmodelPackage.MODEL__LAST_SAVED_BY;

	/**
	 * The feature id for the '<em><b>Last Saved On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__LAST_SAVED_ON = GenericmodelPackage.MODEL__LAST_SAVED_ON;

	/**
	 * The feature id for the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__MODEL_NAME = GenericmodelPackage.MODEL__MODEL_NAME;

	/**
	 * The feature id for the '<em><b>Model Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__MODEL_VERSION = GenericmodelPackage.MODEL__MODEL_VERSION;

	/**
	 * The feature id for the '<em><b>No New Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__NO_NEW_ID = GenericmodelPackage.MODEL__NO_NEW_ID;

	/**
	 * The feature id for the '<em><b>Transformations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__TRANSFORMATIONS = GenericmodelPackage.MODEL__TRANSFORMATIONS;

	/**
	 * The feature id for the '<em><b>Code Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__CODE_MODEL = GenericmodelPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Block Library</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__BLOCK_LIBRARY = GenericmodelPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL__ELEMENTS = GenericmodelPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>GA System Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_FEATURE_COUNT = GenericmodelPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelElementImpl <em>GA System Model Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.impl.GASystemModelElementImpl
	 * @see geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl#getGASystemModelElement()
	 * @generated
	 */
	int GA_SYSTEM_MODEL_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>GA System Model Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.GASystemModelRoot <em>GA System Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA System Model Root</em>'.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModelRoot
	 * @generated
	 */
	EClass getGASystemModelRoot();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.GASystemModel <em>GA System Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA System Model</em>'.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModel
	 * @generated
	 */
	EClass getGASystemModel();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getCodeModel <em>Code Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Code Model</em>'.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModel#getCodeModel()
	 * @see #getGASystemModel()
	 * @generated
	 */
	EReference getGASystemModel_CodeModel();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getBlockLibrary <em>Block Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Block Library</em>'.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModel#getBlockLibrary()
	 * @see #getGASystemModel()
	 * @generated
	 */
	EReference getGASystemModel_BlockLibrary();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.GASystemModel#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModel#getElements()
	 * @see #getGASystemModel()
	 * @generated
	 */
	EReference getGASystemModel_Elements();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.GASystemModelElement <em>GA System Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA System Model Element</em>'.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModelElement
	 * @generated
	 */
	EClass getGASystemModelElement();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.GASystemModelElement#getCodeModelElement <em>Code Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Code Model Element</em>'.
	 * @see geneauto.emf.models.gasystemmodel.GASystemModelElement#getCodeModelElement()
	 * @see #getGASystemModelElement()
	 * @generated
	 */
	EReference getGASystemModelElement_CodeModelElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GasystemmodelFactory getGasystemmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.GASystemModelRoot <em>GA System Model Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.GASystemModelRoot
		 * @see geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl#getGASystemModelRoot()
		 * @generated
		 */
		EClass GA_SYSTEM_MODEL_ROOT = eINSTANCE.getGASystemModelRoot();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelImpl <em>GA System Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.impl.GASystemModelImpl
		 * @see geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl#getGASystemModel()
		 * @generated
		 */
		EClass GA_SYSTEM_MODEL = eINSTANCE.getGASystemModel();

		/**
		 * The meta object literal for the '<em><b>Code Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_SYSTEM_MODEL__CODE_MODEL = eINSTANCE.getGASystemModel_CodeModel();

		/**
		 * The meta object literal for the '<em><b>Block Library</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_SYSTEM_MODEL__BLOCK_LIBRARY = eINSTANCE.getGASystemModel_BlockLibrary();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_SYSTEM_MODEL__ELEMENTS = eINSTANCE.getGASystemModel_Elements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelElementImpl <em>GA System Model Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.impl.GASystemModelElementImpl
		 * @see geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl#getGASystemModelElement()
		 * @generated
		 */
		EClass GA_SYSTEM_MODEL_ELEMENT = eINSTANCE.getGASystemModelElement();

		/**
		 * The meta object literal for the '<em><b>Code Model Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT = eINSTANCE.getGASystemModelElement_CodeModelElement();

	}

} //GasystemmodelPackage
