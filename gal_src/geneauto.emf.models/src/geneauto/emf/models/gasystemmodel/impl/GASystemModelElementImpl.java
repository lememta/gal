/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.impl;

import geneauto.emf.models.gacodemodel.GACodeModelElement;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;
import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block;

import geneauto.emf.models.genericmodel.impl.GAModelElementImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GA System Model Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelElementImpl#getCodeModelElement <em>Code Model Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class GASystemModelElementImpl extends GAModelElementImpl implements GASystemModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getCodeModelElement() <em>Code Model Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodeModelElement()
	 * @generated
	 * @ordered
	 */
	protected GACodeModelElement codeModelElement;

	/**
	 * This is true if the Code Model Element reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean codeModelElementESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GASystemModelElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GasystemmodelPackage.Literals.GA_SYSTEM_MODEL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GACodeModelElement getCodeModelElement() {
		if (codeModelElement != null && codeModelElement.eIsProxy()) {
			InternalEObject oldCodeModelElement = (InternalEObject)codeModelElement;
			codeModelElement = (GACodeModelElement)eResolveProxy(oldCodeModelElement);
			if (codeModelElement != oldCodeModelElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT, oldCodeModelElement, codeModelElement));
			}
		}
		return codeModelElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GACodeModelElement basicGetCodeModelElement() {
		return codeModelElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCodeModelElement(GACodeModelElement newCodeModelElement) {
		GACodeModelElement oldCodeModelElement = codeModelElement;
		codeModelElement = newCodeModelElement;
		boolean oldCodeModelElementESet = codeModelElementESet;
		codeModelElementESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT, oldCodeModelElement, codeModelElement, !oldCodeModelElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCodeModelElement() {
		GACodeModelElement oldCodeModelElement = codeModelElement;
		boolean oldCodeModelElementESet = codeModelElementESet;
		codeModelElement = null;
		codeModelElementESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT, oldCodeModelElement, null, oldCodeModelElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCodeModelElement() {
		return codeModelElementESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block Owner() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT:
				if (resolve) return getCodeModelElement();
				return basicGetCodeModelElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT:
				setCodeModelElement((GACodeModelElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT:
				unsetCodeModelElement();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT:
				return isSetCodeModelElement();
		}
		return super.eIsSet(featureID);
	}

} //GASystemModelElementImpl
