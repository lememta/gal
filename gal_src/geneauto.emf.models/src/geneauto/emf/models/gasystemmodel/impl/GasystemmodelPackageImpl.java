/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.impl;

import geneauto.emf.models.common.CommonPackage;

import geneauto.emf.models.common.impl.CommonPackageImpl;

import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;

import geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;

import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;

import geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl;

import geneauto.emf.models.gacodemodel.expression.statemodel.StatemodelPackage;

import geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl;

import geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesPackage;

import geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl;

import geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl;

import geneauto.emf.models.gacodemodel.operator.OperatorPackage;

import geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl;

import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastPackage;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl;

import geneauto.emf.models.gadatatypes.GadatatypesPackage;

import geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl;

import geneauto.emf.models.gasystemmodel.GASystemModel;
import geneauto.emf.models.gasystemmodel.GASystemModelElement;
import geneauto.emf.models.gasystemmodel.GASystemModelRoot;
import geneauto.emf.models.gasystemmodel.GasystemmodelFactory;
import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.GafunctionalmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl;

import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;

import geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GasystemmodelPackageImpl extends EPackageImpl implements GasystemmodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaSystemModelRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaSystemModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaSystemModelElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see geneauto.emf.models.gasystemmodel.GasystemmodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GasystemmodelPackageImpl() {
		super(eNS_URI, GasystemmodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GasystemmodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GasystemmodelPackage init() {
		if (isInited) return (GasystemmodelPackage)EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI);

		// Obtain or create and register package
		GasystemmodelPackageImpl theGasystemmodelPackage = (GasystemmodelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GasystemmodelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GasystemmodelPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) : CommonPackage.eINSTANCE);
		GadatatypesPackageImpl theGadatatypesPackage = (GadatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) instanceof GadatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) : GadatatypesPackage.eINSTANCE);
		GenericmodelPackageImpl theGenericmodelPackage = (GenericmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) instanceof GenericmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) : GenericmodelPackage.eINSTANCE);
		GacodemodelPackageImpl theGacodemodelPackage = (GacodemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) instanceof GacodemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) : GacodemodelPackage.eINSTANCE);
		ExpressionPackageImpl theExpressionPackage = (ExpressionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) instanceof ExpressionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) : ExpressionPackage.eINSTANCE);
		StatemodelPackageImpl theStatemodelPackage = (StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) instanceof StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) : StatemodelPackage.eINSTANCE);
		StatementPackageImpl theStatementPackage = (StatementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) instanceof StatementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) : StatementPackage.eINSTANCE);
		geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl theStatemodelPackage_1 = (geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) instanceof geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) : geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eINSTANCE);
		BroadcastPackageImpl theBroadcastPackage = (BroadcastPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) instanceof BroadcastPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) : BroadcastPackage.eINSTANCE);
		GaenumtypesPackageImpl theGaenumtypesPackage = (GaenumtypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) instanceof GaenumtypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) : GaenumtypesPackage.eINSTANCE);
		OperatorPackageImpl theOperatorPackage = (OperatorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) instanceof OperatorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) : OperatorPackage.eINSTANCE);
		geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl theCommonPackage_1 = (geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) instanceof geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) : geneauto.emf.models.gasystemmodel.common.CommonPackage.eINSTANCE);
		GastatemodelPackageImpl theGastatemodelPackage = (GastatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) instanceof GastatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) : GastatemodelPackage.eINSTANCE);
		GafunctionalmodelPackageImpl theGafunctionalmodelPackage = (GafunctionalmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) instanceof GafunctionalmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) : GafunctionalmodelPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);
		PortsPackageImpl thePortsPackage = (PortsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) instanceof PortsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) : PortsPackage.eINSTANCE);
		GablocklibraryPackageImpl theGablocklibraryPackage = (GablocklibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) instanceof GablocklibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) : GablocklibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theGasystemmodelPackage.createPackageContents();
		theCommonPackage.createPackageContents();
		theGadatatypesPackage.createPackageContents();
		theGenericmodelPackage.createPackageContents();
		theGacodemodelPackage.createPackageContents();
		theExpressionPackage.createPackageContents();
		theStatemodelPackage.createPackageContents();
		theStatementPackage.createPackageContents();
		theStatemodelPackage_1.createPackageContents();
		theBroadcastPackage.createPackageContents();
		theGaenumtypesPackage.createPackageContents();
		theOperatorPackage.createPackageContents();
		theCommonPackage_1.createPackageContents();
		theGastatemodelPackage.createPackageContents();
		theGafunctionalmodelPackage.createPackageContents();
		theBlocksPackage.createPackageContents();
		thePortsPackage.createPackageContents();
		theGablocklibraryPackage.createPackageContents();

		// Initialize created meta-data
		theGasystemmodelPackage.initializePackageContents();
		theCommonPackage.initializePackageContents();
		theGadatatypesPackage.initializePackageContents();
		theGenericmodelPackage.initializePackageContents();
		theGacodemodelPackage.initializePackageContents();
		theExpressionPackage.initializePackageContents();
		theStatemodelPackage.initializePackageContents();
		theStatementPackage.initializePackageContents();
		theStatemodelPackage_1.initializePackageContents();
		theBroadcastPackage.initializePackageContents();
		theGaenumtypesPackage.initializePackageContents();
		theOperatorPackage.initializePackageContents();
		theCommonPackage_1.initializePackageContents();
		theGastatemodelPackage.initializePackageContents();
		theGafunctionalmodelPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();
		thePortsPackage.initializePackageContents();
		theGablocklibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGasystemmodelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GasystemmodelPackage.eNS_URI, theGasystemmodelPackage);
		return theGasystemmodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGASystemModelRoot() {
		return gaSystemModelRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGASystemModel() {
		return gaSystemModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGASystemModel_CodeModel() {
		return (EReference)gaSystemModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGASystemModel_BlockLibrary() {
		return (EReference)gaSystemModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGASystemModel_Elements() {
		return (EReference)gaSystemModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGASystemModelElement() {
		return gaSystemModelElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGASystemModelElement_CodeModelElement() {
		return (EReference)gaSystemModelElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GasystemmodelFactory getGasystemmodelFactory() {
		return (GasystemmodelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		gaSystemModelRootEClass = createEClass(GA_SYSTEM_MODEL_ROOT);

		gaSystemModelEClass = createEClass(GA_SYSTEM_MODEL);
		createEReference(gaSystemModelEClass, GA_SYSTEM_MODEL__CODE_MODEL);
		createEReference(gaSystemModelEClass, GA_SYSTEM_MODEL__BLOCK_LIBRARY);
		createEReference(gaSystemModelEClass, GA_SYSTEM_MODEL__ELEMENTS);

		gaSystemModelElementEClass = createEClass(GA_SYSTEM_MODEL_ELEMENT);
		createEReference(gaSystemModelElementEClass, GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		geneauto.emf.models.gasystemmodel.common.CommonPackage theCommonPackage_1 = (geneauto.emf.models.gasystemmodel.common.CommonPackage)EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI);
		GastatemodelPackage theGastatemodelPackage = (GastatemodelPackage)EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI);
		GafunctionalmodelPackage theGafunctionalmodelPackage = (GafunctionalmodelPackage)EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI);
		GenericmodelPackage theGenericmodelPackage = (GenericmodelPackage)EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI);
		GacodemodelPackage theGacodemodelPackage = (GacodemodelPackage)EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI);
		GablocklibraryPackage theGablocklibraryPackage = (GablocklibraryPackage)EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theCommonPackage_1);
		getESubpackages().add(theGastatemodelPackage);
		getESubpackages().add(theGafunctionalmodelPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		gaSystemModelEClass.getESuperTypes().add(theGenericmodelPackage.getModel());
		gaSystemModelElementEClass.getESuperTypes().add(theGenericmodelPackage.getGAModelElement());

		// Initialize classes and features; add operations and parameters
		initEClass(gaSystemModelRootEClass, GASystemModelRoot.class, "GASystemModelRoot", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(gaSystemModelEClass, GASystemModel.class, "GASystemModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGASystemModel_CodeModel(), theGacodemodelPackage.getGACodeModel(), null, "codeModel", null, 0, 1, GASystemModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGASystemModel_BlockLibrary(), theGablocklibraryPackage.getGABlockLibrary(), null, "blockLibrary", null, 0, 1, GASystemModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGASystemModel_Elements(), this.getGASystemModelRoot(), null, "elements", null, 0, -1, GASystemModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(gaSystemModelEClass, theBlocksPackage.getBlock(), "All_Blocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gaSystemModelEClass, theGafunctionalmodelPackage.getSignal(), "All_Signals", 0, -1, !IS_UNIQUE, IS_ORDERED);

		initEClass(gaSystemModelElementEClass, GASystemModelElement.class, "GASystemModelElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGASystemModelElement_CodeModelElement(), theGacodemodelPackage.getGACodeModelElement(), null, "codeModelElement", null, 0, 1, GASystemModelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(gaSystemModelElementEClass, theBlocksPackage.getBlock(), "Owner", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //GasystemmodelPackageImpl
