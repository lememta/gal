/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.impl;

import geneauto.emf.models.gablocklibrary.GABlockLibrary;

import geneauto.emf.models.gacodemodel.GACodeModel;

import geneauto.emf.models.gasystemmodel.GASystemModel;
import geneauto.emf.models.gasystemmodel.GASystemModelRoot;
import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.Signal;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.Block;

import geneauto.emf.models.genericmodel.impl.ModelImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GA System Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelImpl#getCodeModel <em>Code Model</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelImpl#getBlockLibrary <em>Block Library</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.impl.GASystemModelImpl#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GASystemModelImpl extends ModelImpl implements GASystemModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getCodeModel() <em>Code Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodeModel()
	 * @generated
	 * @ordered
	 */
	protected GACodeModel codeModel;

	/**
	 * This is true if the Code Model reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean codeModelESet;

	/**
	 * The cached value of the '{@link #getBlockLibrary() <em>Block Library</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockLibrary()
	 * @generated
	 * @ordered
	 */
	protected GABlockLibrary blockLibrary;

	/**
	 * This is true if the Block Library reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean blockLibraryESet;

	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<GASystemModelRoot> elements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GASystemModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GasystemmodelPackage.Literals.GA_SYSTEM_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GACodeModel getCodeModel() {
		if (codeModel != null && codeModel.eIsProxy()) {
			InternalEObject oldCodeModel = (InternalEObject)codeModel;
			codeModel = (GACodeModel)eResolveProxy(oldCodeModel);
			if (codeModel != oldCodeModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GasystemmodelPackage.GA_SYSTEM_MODEL__CODE_MODEL, oldCodeModel, codeModel));
			}
		}
		return codeModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GACodeModel basicGetCodeModel() {
		return codeModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCodeModel(GACodeModel newCodeModel) {
		GACodeModel oldCodeModel = codeModel;
		codeModel = newCodeModel;
		boolean oldCodeModelESet = codeModelESet;
		codeModelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GasystemmodelPackage.GA_SYSTEM_MODEL__CODE_MODEL, oldCodeModel, codeModel, !oldCodeModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCodeModel() {
		GACodeModel oldCodeModel = codeModel;
		boolean oldCodeModelESet = codeModelESet;
		codeModel = null;
		codeModelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GasystemmodelPackage.GA_SYSTEM_MODEL__CODE_MODEL, oldCodeModel, null, oldCodeModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCodeModel() {
		return codeModelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GABlockLibrary getBlockLibrary() {
		if (blockLibrary != null && blockLibrary.eIsProxy()) {
			InternalEObject oldBlockLibrary = (InternalEObject)blockLibrary;
			blockLibrary = (GABlockLibrary)eResolveProxy(oldBlockLibrary);
			if (blockLibrary != oldBlockLibrary) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GasystemmodelPackage.GA_SYSTEM_MODEL__BLOCK_LIBRARY, oldBlockLibrary, blockLibrary));
			}
		}
		return blockLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GABlockLibrary basicGetBlockLibrary() {
		return blockLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlockLibrary(GABlockLibrary newBlockLibrary) {
		GABlockLibrary oldBlockLibrary = blockLibrary;
		blockLibrary = newBlockLibrary;
		boolean oldBlockLibraryESet = blockLibraryESet;
		blockLibraryESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GasystemmodelPackage.GA_SYSTEM_MODEL__BLOCK_LIBRARY, oldBlockLibrary, blockLibrary, !oldBlockLibraryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBlockLibrary() {
		GABlockLibrary oldBlockLibrary = blockLibrary;
		boolean oldBlockLibraryESet = blockLibraryESet;
		blockLibrary = null;
		blockLibraryESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GasystemmodelPackage.GA_SYSTEM_MODEL__BLOCK_LIBRARY, oldBlockLibrary, null, oldBlockLibraryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBlockLibrary() {
		return blockLibraryESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GASystemModelRoot> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList.Unsettable<GASystemModelRoot>(GASystemModelRoot.class, this, GasystemmodelPackage.GA_SYSTEM_MODEL__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElements() {
		if (elements != null) ((InternalEList.Unsettable<?>)elements).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElements() {
		return elements != null && ((InternalEList.Unsettable<?>)elements).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Block> All_Blocks() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Signal> All_Signals() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL__CODE_MODEL:
				if (resolve) return getCodeModel();
				return basicGetCodeModel();
			case GasystemmodelPackage.GA_SYSTEM_MODEL__BLOCK_LIBRARY:
				if (resolve) return getBlockLibrary();
				return basicGetBlockLibrary();
			case GasystemmodelPackage.GA_SYSTEM_MODEL__ELEMENTS:
				return getElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL__CODE_MODEL:
				setCodeModel((GACodeModel)newValue);
				return;
			case GasystemmodelPackage.GA_SYSTEM_MODEL__BLOCK_LIBRARY:
				setBlockLibrary((GABlockLibrary)newValue);
				return;
			case GasystemmodelPackage.GA_SYSTEM_MODEL__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends GASystemModelRoot>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL__CODE_MODEL:
				unsetCodeModel();
				return;
			case GasystemmodelPackage.GA_SYSTEM_MODEL__BLOCK_LIBRARY:
				unsetBlockLibrary();
				return;
			case GasystemmodelPackage.GA_SYSTEM_MODEL__ELEMENTS:
				unsetElements();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GasystemmodelPackage.GA_SYSTEM_MODEL__CODE_MODEL:
				return isSetCodeModel();
			case GasystemmodelPackage.GA_SYSTEM_MODEL__BLOCK_LIBRARY:
				return isSetBlockLibrary();
			case GasystemmodelPackage.GA_SYSTEM_MODEL__ELEMENTS:
				return isSetElements();
		}
		return super.eIsSet(featureID);
	}

} //GASystemModelImpl
