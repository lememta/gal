/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.common.util;

import geneauto.emf.models.common.CustomType;
import geneauto.emf.models.common.Function;
import geneauto.emf.models.common.NameSpaceElement;
import geneauto.emf.models.common.Variable;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.gasystemmodel.common.*;

import geneauto.emf.models.genericmodel.GAModelElement;
import geneauto.emf.models.genericmodel.GANamed;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage
 * @generated
 */
public class CommonSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CommonPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonSwitch() {
		if (modelPackage == null) {
			modelPackage = CommonPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CommonPackage.BASIC_PARAMETER: {
				BasicParameter basicParameter = (BasicParameter)theEObject;
				T result = caseBasicParameter(basicParameter);
				if (result == null) result = caseParameter(basicParameter);
				if (result == null) result = caseData(basicParameter);
				if (result == null) result = caseNameSpaceElement(basicParameter);
				if (result == null) result = caseGASystemModelElement(basicParameter);
				if (result == null) result = caseGAModelElement(basicParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				T result = caseParameter(parameter);
				if (result == null) result = caseData(parameter);
				if (result == null) result = caseNameSpaceElement(parameter);
				if (result == null) result = caseGASystemModelElement(parameter);
				if (result == null) result = caseGAModelElement(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.DATA: {
				Data data = (Data)theEObject;
				T result = caseData(data);
				if (result == null) result = caseGASystemModelElement(data);
				if (result == null) result = caseGAModelElement(data);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.BLOCK_PARAMETER: {
				BlockParameter blockParameter = (BlockParameter)theEObject;
				T result = caseBlockParameter(blockParameter);
				if (result == null) result = caseParameter(blockParameter);
				if (result == null) result = caseData(blockParameter);
				if (result == null) result = caseNameSpaceElement(blockParameter);
				if (result == null) result = caseGASystemModelElement(blockParameter);
				if (result == null) result = caseGAModelElement(blockParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.CUSTOM_TYPE_SM: {
				CustomType_SM customType_SM = (CustomType_SM)theEObject;
				T result = caseCustomType_SM(customType_SM);
				if (result == null) result = caseGASystemModelElement(customType_SM);
				if (result == null) result = caseCustomType(customType_SM);
				if (result == null) result = caseNameSpaceElement_SM(customType_SM);
				if (result == null) result = caseGAModelElement(customType_SM);
				if (result == null) result = caseNameSpaceElement(customType_SM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.NAME_SPACE_ELEMENT_SM: {
				NameSpaceElement_SM nameSpaceElement_SM = (NameSpaceElement_SM)theEObject;
				T result = caseNameSpaceElement_SM(nameSpaceElement_SM);
				if (result == null) result = caseNameSpaceElement(nameSpaceElement_SM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.FUNCTION_ARGUMENT_SM: {
				FunctionArgument_SM functionArgument_SM = (FunctionArgument_SM)theEObject;
				T result = caseFunctionArgument_SM(functionArgument_SM);
				if (result == null) result = caseVariable_SM(functionArgument_SM);
				if (result == null) result = caseData(functionArgument_SM);
				if (result == null) result = caseNode(functionArgument_SM);
				if (result == null) result = caseSystemVariable(functionArgument_SM);
				if (result == null) result = caseGASystemModelElement(functionArgument_SM);
				if (result == null) result = caseGANamed(functionArgument_SM);
				if (result == null) result = caseNameSpaceElement_SM(functionArgument_SM);
				if (result == null) result = caseVariable(functionArgument_SM);
				if (result == null) result = caseGAModelElement(functionArgument_SM);
				if (result == null) result = caseNameSpaceElement(functionArgument_SM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.VARIABLE_SM: {
				Variable_SM variable_SM = (Variable_SM)theEObject;
				T result = caseVariable_SM(variable_SM);
				if (result == null) result = caseData(variable_SM);
				if (result == null) result = caseNode(variable_SM);
				if (result == null) result = caseSystemVariable(variable_SM);
				if (result == null) result = caseGASystemModelElement(variable_SM);
				if (result == null) result = caseGANamed(variable_SM);
				if (result == null) result = caseNameSpaceElement_SM(variable_SM);
				if (result == null) result = caseVariable(variable_SM);
				if (result == null) result = caseGAModelElement(variable_SM);
				if (result == null) result = caseNameSpaceElement(variable_SM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.SYSTEM_VARIABLE: {
				SystemVariable systemVariable = (SystemVariable)theEObject;
				T result = caseSystemVariable(systemVariable);
				if (result == null) result = caseNameSpaceElement_SM(systemVariable);
				if (result == null) result = caseVariable(systemVariable);
				if (result == null) result = caseNameSpaceElement(systemVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.NODE: {
				Node node = (Node)theEObject;
				T result = caseNode(node);
				if (result == null) result = caseGANamed(node);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.CONTAINER_NODE: {
				ContainerNode containerNode = (ContainerNode)theEObject;
				T result = caseContainerNode(containerNode);
				if (result == null) result = caseNode(containerNode);
				if (result == null) result = caseGANamed(containerNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.FUNCTION_SM: {
				Function_SM function_SM = (Function_SM)theEObject;
				T result = caseFunction_SM(function_SM);
				if (result == null) result = caseGASystemModelElement(function_SM);
				if (result == null) result = caseNode(function_SM);
				if (result == null) result = caseSystemFunction(function_SM);
				if (result == null) result = caseGAModelElement(function_SM);
				if (result == null) result = caseGANamed(function_SM);
				if (result == null) result = caseFunction(function_SM);
				if (result == null) result = caseNameSpaceElement_SM(function_SM);
				if (result == null) result = caseNameSpaceElement(function_SM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.SYSTEM_FUNCTION: {
				SystemFunction systemFunction = (SystemFunction)theEObject;
				T result = caseSystemFunction(systemFunction);
				if (result == null) result = caseFunction(systemFunction);
				if (result == null) result = caseNameSpaceElement_SM(systemFunction);
				if (result == null) result = caseNameSpaceElement(systemFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.EML_FUNCTION: {
				EMLFunction emlFunction = (EMLFunction)theEObject;
				T result = caseEMLFunction(emlFunction);
				if (result == null) result = caseFunction_SM(emlFunction);
				if (result == null) result = caseGASystemModelElement(emlFunction);
				if (result == null) result = caseNode(emlFunction);
				if (result == null) result = caseSystemFunction(emlFunction);
				if (result == null) result = caseGAModelElement(emlFunction);
				if (result == null) result = caseGANamed(emlFunction);
				if (result == null) result = caseFunction(emlFunction);
				if (result == null) result = caseNameSpaceElement_SM(emlFunction);
				if (result == null) result = caseNameSpaceElement(emlFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.TRUTH_TABLE_FUNCTION: {
				TruthTableFunction truthTableFunction = (TruthTableFunction)theEObject;
				T result = caseTruthTableFunction(truthTableFunction);
				if (result == null) result = caseFunction_SM(truthTableFunction);
				if (result == null) result = caseGASystemModelElement(truthTableFunction);
				if (result == null) result = caseNode(truthTableFunction);
				if (result == null) result = caseSystemFunction(truthTableFunction);
				if (result == null) result = caseGAModelElement(truthTableFunction);
				if (result == null) result = caseGANamed(truthTableFunction);
				if (result == null) result = caseFunction(truthTableFunction);
				if (result == null) result = caseNameSpaceElement_SM(truthTableFunction);
				if (result == null) result = caseNameSpaceElement(truthTableFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicParameter(BasicParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseData(Data object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlockParameter(BlockParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Type SM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Type SM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomType_SM(CustomType_SM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Space Element SM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Space Element SM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameSpaceElement_SM(NameSpaceElement_SM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Argument SM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Argument SM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionArgument_SM(FunctionArgument_SM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable SM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable SM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable_SM(Variable_SM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemVariable(SystemVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Container Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Container Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainerNode(ContainerNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function SM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function SM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunction_SM(Function_SM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemFunction(SystemFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EML Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EML Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEMLFunction(EMLFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Truth Table Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Truth Table Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTruthTableFunction(TruthTableFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGAModelElement(GAModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA System Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA System Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGASystemModelElement(GASystemModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Space Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Space Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameSpaceElement(NameSpaceElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomType(CustomType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGANamed(GANamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunction(Function object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //CommonSwitch
