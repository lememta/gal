/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.common;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.common.CommonFactory
 * @model kind="package"
 * @generated
 */
public interface CommonPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "common";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gasystemmodel/common.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gasystemmodel.common";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CommonPackage eINSTANCE = geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.DataImpl <em>Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.DataImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getData()
	 * @generated
	 */
	int DATA = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__STORAGE_CLASS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.ParameterImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__EXTERNAL_ID = DATA__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__ID = DATA__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__IS_FIXED_NAME = DATA__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = DATA__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__ORIGINAL_FULL_NAME = DATA__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__ORIGINAL_NAME = DATA__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PARENT = DATA__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__MODEL = DATA__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__ANNOTATIONS = DATA__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__CODE_MODEL_ELEMENT = DATA__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__STORAGE_CLASS = DATA__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__INDEX = DATA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__VALUE = DATA_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__DATA_TYPE = DATA_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = DATA_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.BasicParameterImpl <em>Basic Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.BasicParameterImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getBasicParameter()
	 * @generated
	 */
	int BASIC_PARAMETER = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__EXTERNAL_ID = PARAMETER__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__ID = PARAMETER__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__IS_FIXED_NAME = PARAMETER__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__NAME = PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__ORIGINAL_FULL_NAME = PARAMETER__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__ORIGINAL_NAME = PARAMETER__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__PARENT = PARAMETER__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__MODEL = PARAMETER__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__ANNOTATIONS = PARAMETER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__CODE_MODEL_ELEMENT = PARAMETER__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__STORAGE_CLASS = PARAMETER__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__INDEX = PARAMETER__INDEX;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__VALUE = PARAMETER__VALUE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER__DATA_TYPE = PARAMETER__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Basic Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.BlockParameterImpl <em>Block Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.BlockParameterImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getBlockParameter()
	 * @generated
	 */
	int BLOCK_PARAMETER = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__EXTERNAL_ID = PARAMETER__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__ID = PARAMETER__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__IS_FIXED_NAME = PARAMETER__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__NAME = PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__ORIGINAL_FULL_NAME = PARAMETER__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__ORIGINAL_NAME = PARAMETER__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__PARENT = PARAMETER__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__MODEL = PARAMETER__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__ANNOTATIONS = PARAMETER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__CODE_MODEL_ELEMENT = PARAMETER__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__STORAGE_CLASS = PARAMETER__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__INDEX = PARAMETER__INDEX;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__VALUE = PARAMETER__VALUE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER__DATA_TYPE = PARAMETER__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Block Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.CustomType_SMImpl <em>Custom Type SM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CustomType_SMImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getCustomType_SM()
	 * @generated
	 */
	int CUSTOM_TYPE_SM = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM__CONTENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Custom Type SM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_SM_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.NameSpaceElement_SM <em>Name Space Element SM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.NameSpaceElement_SM
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getNameSpaceElement_SM()
	 * @generated
	 */
	int NAME_SPACE_ELEMENT_SM = 5;

	/**
	 * The number of structural features of the '<em>Name Space Element SM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_SM_FEATURE_COUNT = geneauto.emf.models.common.CommonPackage.NAME_SPACE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl <em>Variable SM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getVariable_SM()
	 * @generated
	 */
	int VARIABLE_SM = 7;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__EXTERNAL_ID = DATA__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__ID = DATA__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__IS_FIXED_NAME = DATA__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__NAME = DATA__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__ORIGINAL_FULL_NAME = DATA__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__ORIGINAL_NAME = DATA__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__PARENT = DATA__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__MODEL = DATA__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__ANNOTATIONS = DATA__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__CODE_MODEL_ELEMENT = DATA__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__STORAGE_CLASS = DATA__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__DATA_TYPE = DATA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__INITIAL_VALUE = DATA_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__IS_CONST = DATA_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__IS_OPTIMIZABLE = DATA_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__IS_STATIC = DATA_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__IS_VOLATILE = DATA_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__REFERENCE_EXPRESSION = DATA_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__SCOPE = DATA_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Binder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM__BINDER = DATA_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Variable SM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SM_FEATURE_COUNT = DATA_FEATURE_COUNT + 9;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.FunctionArgument_SMImpl <em>Function Argument SM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.FunctionArgument_SMImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getFunctionArgument_SM()
	 * @generated
	 */
	int FUNCTION_ARGUMENT_SM = 6;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__EXTERNAL_ID = VARIABLE_SM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__ID = VARIABLE_SM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__IS_FIXED_NAME = VARIABLE_SM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__NAME = VARIABLE_SM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__ORIGINAL_FULL_NAME = VARIABLE_SM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__ORIGINAL_NAME = VARIABLE_SM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__PARENT = VARIABLE_SM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__MODEL = VARIABLE_SM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__ANNOTATIONS = VARIABLE_SM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__CODE_MODEL_ELEMENT = VARIABLE_SM__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__STORAGE_CLASS = VARIABLE_SM__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__DATA_TYPE = VARIABLE_SM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__INITIAL_VALUE = VARIABLE_SM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__IS_CONST = VARIABLE_SM__IS_CONST;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__IS_OPTIMIZABLE = VARIABLE_SM__IS_OPTIMIZABLE;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__IS_STATIC = VARIABLE_SM__IS_STATIC;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__IS_VOLATILE = VARIABLE_SM__IS_VOLATILE;

	/**
	 * The feature id for the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__REFERENCE_EXPRESSION = VARIABLE_SM__REFERENCE_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__SCOPE = VARIABLE_SM__SCOPE;

	/**
	 * The feature id for the '<em><b>Binder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__BINDER = VARIABLE_SM__BINDER;

	/**
	 * The feature id for the '<em><b>Is Redundant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM__IS_REDUNDANT = VARIABLE_SM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Function Argument SM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_SM_FEATURE_COUNT = VARIABLE_SM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.SystemVariable <em>System Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.SystemVariable
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getSystemVariable()
	 * @generated
	 */
	int SYSTEM_VARIABLE = 8;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_VARIABLE__DATA_TYPE = NAME_SPACE_ELEMENT_SM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_VARIABLE__INITIAL_VALUE = NAME_SPACE_ELEMENT_SM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_VARIABLE__IS_CONST = NAME_SPACE_ELEMENT_SM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_VARIABLE__IS_OPTIMIZABLE = NAME_SPACE_ELEMENT_SM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_VARIABLE__IS_STATIC = NAME_SPACE_ELEMENT_SM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_VARIABLE__IS_VOLATILE = NAME_SPACE_ELEMENT_SM_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>System Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_VARIABLE_FEATURE_COUNT = NAME_SPACE_ELEMENT_SM_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.Node <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.Node
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 9;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = GenericmodelPackage.GA_NAMED_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.ContainerNode <em>Container Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.ContainerNode
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getContainerNode()
	 * @generated
	 */
	int CONTAINER_NODE = 10;

	/**
	 * The number of structural features of the '<em>Container Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.Function_SMImpl <em>Function SM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.Function_SMImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getFunction_SM()
	 * @generated
	 */
	int FUNCTION_SM = 11;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__EXTERNAL_ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__ID = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__IS_FIXED_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__ORIGINAL_FULL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__ORIGINAL_NAME = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__PARENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__MODEL = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__ANNOTATIONS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__CODE_MODEL_ELEMENT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__DATA_TYPE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__SCOPE = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__REFERENCE_EXPRESSION = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__ARGUMENTS = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM__VARIABLES = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Function SM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SM_FEATURE_COUNT = GasystemmodelPackage.GA_SYSTEM_MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.SystemFunction <em>System Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.SystemFunction
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getSystemFunction()
	 * @generated
	 */
	int SYSTEM_FUNCTION = 12;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FUNCTION__DATA_TYPE = geneauto.emf.models.common.CommonPackage.FUNCTION__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>System Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FUNCTION_FEATURE_COUNT = geneauto.emf.models.common.CommonPackage.FUNCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.EMLFunctionImpl <em>EML Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.EMLFunctionImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getEMLFunction()
	 * @generated
	 */
	int EML_FUNCTION = 13;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__EXTERNAL_ID = FUNCTION_SM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__ID = FUNCTION_SM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__IS_FIXED_NAME = FUNCTION_SM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__NAME = FUNCTION_SM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__ORIGINAL_FULL_NAME = FUNCTION_SM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__ORIGINAL_NAME = FUNCTION_SM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__PARENT = FUNCTION_SM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__MODEL = FUNCTION_SM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__ANNOTATIONS = FUNCTION_SM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__CODE_MODEL_ELEMENT = FUNCTION_SM__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__DATA_TYPE = FUNCTION_SM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__SCOPE = FUNCTION_SM__SCOPE;

	/**
	 * The feature id for the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__REFERENCE_EXPRESSION = FUNCTION_SM__REFERENCE_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__ARGUMENTS = FUNCTION_SM__ARGUMENTS;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION__VARIABLES = FUNCTION_SM__VARIABLES;

	/**
	 * The number of structural features of the '<em>EML Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EML_FUNCTION_FEATURE_COUNT = FUNCTION_SM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.impl.TruthTableFunctionImpl <em>Truth Table Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.impl.TruthTableFunctionImpl
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getTruthTableFunction()
	 * @generated
	 */
	int TRUTH_TABLE_FUNCTION = 14;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__EXTERNAL_ID = FUNCTION_SM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__ID = FUNCTION_SM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__IS_FIXED_NAME = FUNCTION_SM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__NAME = FUNCTION_SM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__ORIGINAL_FULL_NAME = FUNCTION_SM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__ORIGINAL_NAME = FUNCTION_SM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__PARENT = FUNCTION_SM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__MODEL = FUNCTION_SM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__ANNOTATIONS = FUNCTION_SM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Code Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__CODE_MODEL_ELEMENT = FUNCTION_SM__CODE_MODEL_ELEMENT;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__DATA_TYPE = FUNCTION_SM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__SCOPE = FUNCTION_SM__SCOPE;

	/**
	 * The feature id for the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__REFERENCE_EXPRESSION = FUNCTION_SM__REFERENCE_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__ARGUMENTS = FUNCTION_SM__ARGUMENTS;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION__VARIABLES = FUNCTION_SM__VARIABLES;

	/**
	 * The number of structural features of the '<em>Truth Table Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUTH_TABLE_FUNCTION_FEATURE_COUNT = FUNCTION_SM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.VariableScope <em>Variable Scope</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.VariableScope
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getVariableScope()
	 * @generated
	 */
	int VARIABLE_SCOPE = 15;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.FunctionScope <em>Function Scope</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.FunctionScope
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getFunctionScope()
	 * @generated
	 */
	int FUNCTION_SCOPE = 16;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gasystemmodel.common.PathOption <em>Path Option</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gasystemmodel.common.PathOption
	 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getPathOption()
	 * @generated
	 */
	int PATH_OPTION = 17;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.BasicParameter <em>Basic Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Parameter</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.BasicParameter
	 * @generated
	 */
	EClass getBasicParameter();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.common.Parameter#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Parameter#getIndex()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Index();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.common.Parameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Parameter#getValue()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_Value();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.common.Parameter#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Type</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Parameter#getDataType()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_DataType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.Data <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Data
	 * @generated
	 */
	EClass getData();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.common.Data#getStorageClass <em>Storage Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Storage Class</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Data#getStorageClass()
	 * @see #getData()
	 * @generated
	 */
	EAttribute getData_StorageClass();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.BlockParameter <em>Block Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Parameter</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.BlockParameter
	 * @generated
	 */
	EClass getBlockParameter();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.CustomType_SM <em>Custom Type SM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Type SM</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.CustomType_SM
	 * @generated
	 */
	EClass getCustomType_SM();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gasystemmodel.common.CustomType_SM#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.CustomType_SM#getContent()
	 * @see #getCustomType_SM()
	 * @generated
	 */
	EReference getCustomType_SM_Content();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.NameSpaceElement_SM <em>Name Space Element SM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name Space Element SM</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.NameSpaceElement_SM
	 * @generated
	 */
	EClass getNameSpaceElement_SM();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.FunctionArgument_SM <em>Function Argument SM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Argument SM</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.FunctionArgument_SM
	 * @generated
	 */
	EClass getFunctionArgument_SM();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.common.FunctionArgument_SM#isRedundant <em>Is Redundant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Redundant</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.FunctionArgument_SM#isRedundant()
	 * @see #getFunctionArgument_SM()
	 * @generated
	 */
	EAttribute getFunctionArgument_SM_IsRedundant();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.Variable_SM <em>Variable SM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable SM</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Variable_SM
	 * @generated
	 */
	EClass getVariable_SM();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.common.Variable_SM#getReferenceExpression <em>Reference Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference Expression</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Variable_SM#getReferenceExpression()
	 * @see #getVariable_SM()
	 * @generated
	 */
	EReference getVariable_SM_ReferenceExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.common.Variable_SM#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Variable_SM#getScope()
	 * @see #getVariable_SM()
	 * @generated
	 */
	EAttribute getVariable_SM_Scope();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.common.Variable_SM#getBinder <em>Binder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Binder</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Variable_SM#getBinder()
	 * @see #getVariable_SM()
	 * @generated
	 */
	EReference getVariable_SM_Binder();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.SystemVariable <em>System Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Variable</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.SystemVariable
	 * @generated
	 */
	EClass getSystemVariable();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.ContainerNode <em>Container Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container Node</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.ContainerNode
	 * @generated
	 */
	EClass getContainerNode();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.Function_SM <em>Function SM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function SM</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Function_SM
	 * @generated
	 */
	EClass getFunction_SM();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Function_SM#getScope()
	 * @see #getFunction_SM()
	 * @generated
	 */
	EAttribute getFunction_SM_Scope();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getReferenceExpression <em>Reference Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference Expression</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Function_SM#getReferenceExpression()
	 * @see #getFunction_SM()
	 * @generated
	 */
	EReference getFunction_SM_ReferenceExpression();

	/**
	 * Returns the meta object for the reference list '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getArguments <em>Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Arguments</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Function_SM#getArguments()
	 * @see #getFunction_SM()
	 * @generated
	 */
	EReference getFunction_SM_Arguments();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.Function_SM#getVariables()
	 * @see #getFunction_SM()
	 * @generated
	 */
	EReference getFunction_SM_Variables();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.SystemFunction <em>System Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Function</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.SystemFunction
	 * @generated
	 */
	EClass getSystemFunction();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.EMLFunction <em>EML Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EML Function</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.EMLFunction
	 * @generated
	 */
	EClass getEMLFunction();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gasystemmodel.common.TruthTableFunction <em>Truth Table Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Truth Table Function</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.TruthTableFunction
	 * @generated
	 */
	EClass getTruthTableFunction();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.common.VariableScope <em>Variable Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Variable Scope</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.VariableScope
	 * @generated
	 */
	EEnum getVariableScope();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.common.FunctionScope <em>Function Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Function Scope</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.FunctionScope
	 * @generated
	 */
	EEnum getFunctionScope();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gasystemmodel.common.PathOption <em>Path Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Path Option</em>'.
	 * @see geneauto.emf.models.gasystemmodel.common.PathOption
	 * @generated
	 */
	EEnum getPathOption();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CommonFactory getCommonFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.BasicParameterImpl <em>Basic Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.BasicParameterImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getBasicParameter()
		 * @generated
		 */
		EClass BASIC_PARAMETER = eINSTANCE.getBasicParameter();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.ParameterImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__INDEX = eINSTANCE.getParameter_Index();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__VALUE = eINSTANCE.getParameter_Value();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__DATA_TYPE = eINSTANCE.getParameter_DataType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.DataImpl <em>Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.DataImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getData()
		 * @generated
		 */
		EClass DATA = eINSTANCE.getData();

		/**
		 * The meta object literal for the '<em><b>Storage Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA__STORAGE_CLASS = eINSTANCE.getData_StorageClass();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.BlockParameterImpl <em>Block Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.BlockParameterImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getBlockParameter()
		 * @generated
		 */
		EClass BLOCK_PARAMETER = eINSTANCE.getBlockParameter();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.CustomType_SMImpl <em>Custom Type SM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CustomType_SMImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getCustomType_SM()
		 * @generated
		 */
		EClass CUSTOM_TYPE_SM = eINSTANCE.getCustomType_SM();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOM_TYPE_SM__CONTENT = eINSTANCE.getCustomType_SM_Content();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.NameSpaceElement_SM <em>Name Space Element SM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.NameSpaceElement_SM
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getNameSpaceElement_SM()
		 * @generated
		 */
		EClass NAME_SPACE_ELEMENT_SM = eINSTANCE.getNameSpaceElement_SM();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.FunctionArgument_SMImpl <em>Function Argument SM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.FunctionArgument_SMImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getFunctionArgument_SM()
		 * @generated
		 */
		EClass FUNCTION_ARGUMENT_SM = eINSTANCE.getFunctionArgument_SM();

		/**
		 * The meta object literal for the '<em><b>Is Redundant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_ARGUMENT_SM__IS_REDUNDANT = eINSTANCE.getFunctionArgument_SM_IsRedundant();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl <em>Variable SM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getVariable_SM()
		 * @generated
		 */
		EClass VARIABLE_SM = eINSTANCE.getVariable_SM();

		/**
		 * The meta object literal for the '<em><b>Reference Expression</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_SM__REFERENCE_EXPRESSION = eINSTANCE.getVariable_SM_ReferenceExpression();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_SM__SCOPE = eINSTANCE.getVariable_SM_Scope();

		/**
		 * The meta object literal for the '<em><b>Binder</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_SM__BINDER = eINSTANCE.getVariable_SM_Binder();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.SystemVariable <em>System Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.SystemVariable
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getSystemVariable()
		 * @generated
		 */
		EClass SYSTEM_VARIABLE = eINSTANCE.getSystemVariable();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.Node <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.Node
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.ContainerNode <em>Container Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.ContainerNode
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getContainerNode()
		 * @generated
		 */
		EClass CONTAINER_NODE = eINSTANCE.getContainerNode();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.Function_SMImpl <em>Function SM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.Function_SMImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getFunction_SM()
		 * @generated
		 */
		EClass FUNCTION_SM = eINSTANCE.getFunction_SM();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_SM__SCOPE = eINSTANCE.getFunction_SM_Scope();

		/**
		 * The meta object literal for the '<em><b>Reference Expression</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_SM__REFERENCE_EXPRESSION = eINSTANCE.getFunction_SM_ReferenceExpression();

		/**
		 * The meta object literal for the '<em><b>Arguments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_SM__ARGUMENTS = eINSTANCE.getFunction_SM_Arguments();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_SM__VARIABLES = eINSTANCE.getFunction_SM_Variables();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.SystemFunction <em>System Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.SystemFunction
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getSystemFunction()
		 * @generated
		 */
		EClass SYSTEM_FUNCTION = eINSTANCE.getSystemFunction();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.EMLFunctionImpl <em>EML Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.EMLFunctionImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getEMLFunction()
		 * @generated
		 */
		EClass EML_FUNCTION = eINSTANCE.getEMLFunction();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.impl.TruthTableFunctionImpl <em>Truth Table Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.impl.TruthTableFunctionImpl
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getTruthTableFunction()
		 * @generated
		 */
		EClass TRUTH_TABLE_FUNCTION = eINSTANCE.getTruthTableFunction();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.VariableScope <em>Variable Scope</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.VariableScope
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getVariableScope()
		 * @generated
		 */
		EEnum VARIABLE_SCOPE = eINSTANCE.getVariableScope();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.FunctionScope <em>Function Scope</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.FunctionScope
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getFunctionScope()
		 * @generated
		 */
		EEnum FUNCTION_SCOPE = eINSTANCE.getFunctionScope();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gasystemmodel.common.PathOption <em>Path Option</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gasystemmodel.common.PathOption
		 * @see geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl#getPathOption()
		 * @generated
		 */
		EEnum PATH_OPTION = eINSTANCE.getPathOption();

	}

} //CommonPackage
