/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.common;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage
 * @generated
 */
public interface CommonFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CommonFactory eINSTANCE = geneauto.emf.models.gasystemmodel.common.impl.CommonFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Basic Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Parameter</em>'.
	 * @generated
	 */
	BasicParameter createBasicParameter();

	/**
	 * Returns a new object of class '<em>Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data</em>'.
	 * @generated
	 */
	Data createData();

	/**
	 * Returns a new object of class '<em>Block Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block Parameter</em>'.
	 * @generated
	 */
	BlockParameter createBlockParameter();

	/**
	 * Returns a new object of class '<em>Custom Type SM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Custom Type SM</em>'.
	 * @generated
	 */
	CustomType_SM createCustomType_SM();

	/**
	 * Returns a new object of class '<em>Function Argument SM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Argument SM</em>'.
	 * @generated
	 */
	FunctionArgument_SM createFunctionArgument_SM();

	/**
	 * Returns a new object of class '<em>Variable SM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable SM</em>'.
	 * @generated
	 */
	Variable_SM createVariable_SM();

	/**
	 * Returns a new object of class '<em>EML Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EML Function</em>'.
	 * @generated
	 */
	EMLFunction createEMLFunction();

	/**
	 * Returns a new object of class '<em>Truth Table Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Truth Table Function</em>'.
	 * @generated
	 */
	TruthTableFunction createTruthTableFunction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CommonPackage getCommonPackage();

} //CommonFactory
