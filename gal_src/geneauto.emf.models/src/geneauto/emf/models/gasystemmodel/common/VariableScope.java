/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Variable Scope</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Scope of a variable TODO Maybe convert to a class. There is infact some hierarchy of scopes. e.g. FunctionOutputVariable and LocalVariable
 * <!-- end-model-doc -->
 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage#getVariableScope()
 * @model
 * @generated
 */
public enum VariableScope implements Enumerator {
	/**
	 * The '<em><b>LOCAL VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOCAL_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	LOCAL_VARIABLE(0, "LOCAL_VARIABLE", "LOCAL_VARIABLE"),

	/**
	 * The '<em><b>INPUT VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INPUT_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	INPUT_VARIABLE(1, "INPUT_VARIABLE", "INPUT_VARIABLE"),

	/**
	 * The '<em><b>OUTPUT VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OUTPUT_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	OUTPUT_VARIABLE(2, "OUTPUT_VARIABLE", "OUTPUT_VARIABLE"),

	/**
	 * The '<em><b>FUNCTION INPUT VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FUNCTION_INPUT_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	FUNCTION_INPUT_VARIABLE(3, "FUNCTION_INPUT_VARIABLE", "FUNCTION_INPUT_VARIABLE"),

	/**
	 * The '<em><b>FUNCTION OUTPUT VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FUNCTION_OUTPUT_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	FUNCTION_OUTPUT_VARIABLE(4, "FUNCTION_OUTPUT_VARIABLE", "FUNCTION_OUTPUT_VARIABLE"),

	/**
	 * The '<em><b>IMPORTED VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPORTED_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	IMPORTED_VARIABLE(5, "IMPORTED_VARIABLE", "IMPORTED_VARIABLE"),

	/**
	 * The '<em><b>EXPORTED VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXPORTED_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	EXPORTED_VARIABLE(6, "EXPORTED_VARIABLE", "EXPORTED_VARIABLE"),

	/**
	 * The '<em><b>STATE VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STATE_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	STATE_VARIABLE(7, "STATE_VARIABLE", "STATE_VARIABLE");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>LOCAL VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOCAL VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOCAL_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LOCAL_VARIABLE_VALUE = 0;

	/**
	 * The '<em><b>INPUT VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INPUT VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INPUT_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INPUT_VARIABLE_VALUE = 1;

	/**
	 * The '<em><b>OUTPUT VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OUTPUT VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OUTPUT_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OUTPUT_VARIABLE_VALUE = 2;

	/**
	 * The '<em><b>FUNCTION INPUT VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FUNCTION INPUT VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FUNCTION_INPUT_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FUNCTION_INPUT_VARIABLE_VALUE = 3;

	/**
	 * The '<em><b>FUNCTION OUTPUT VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FUNCTION OUTPUT VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FUNCTION_OUTPUT_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FUNCTION_OUTPUT_VARIABLE_VALUE = 4;

	/**
	 * The '<em><b>IMPORTED VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IMPORTED VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPORTED_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IMPORTED_VARIABLE_VALUE = 5;

	/**
	 * The '<em><b>EXPORTED VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EXPORTED VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EXPORTED_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXPORTED_VARIABLE_VALUE = 6;

	/**
	 * The '<em><b>STATE VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>STATE VARIABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STATE_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STATE_VARIABLE_VALUE = 7;

	/**
	 * An array of all the '<em><b>Variable Scope</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final VariableScope[] VALUES_ARRAY =
		new VariableScope[] {
			LOCAL_VARIABLE,
			INPUT_VARIABLE,
			OUTPUT_VARIABLE,
			FUNCTION_INPUT_VARIABLE,
			FUNCTION_OUTPUT_VARIABLE,
			IMPORTED_VARIABLE,
			EXPORTED_VARIABLE,
			STATE_VARIABLE,
		};

	/**
	 * A public read-only list of all the '<em><b>Variable Scope</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<VariableScope> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Variable Scope</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VariableScope get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			VariableScope result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Variable Scope</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VariableScope getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			VariableScope result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Variable Scope</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VariableScope get(int value) {
		switch (value) {
			case LOCAL_VARIABLE_VALUE: return LOCAL_VARIABLE;
			case INPUT_VARIABLE_VALUE: return INPUT_VARIABLE;
			case OUTPUT_VARIABLE_VALUE: return OUTPUT_VARIABLE;
			case FUNCTION_INPUT_VARIABLE_VALUE: return FUNCTION_INPUT_VARIABLE;
			case FUNCTION_OUTPUT_VARIABLE_VALUE: return FUNCTION_OUTPUT_VARIABLE;
			case IMPORTED_VARIABLE_VALUE: return IMPORTED_VARIABLE;
			case EXPORTED_VARIABLE_VALUE: return EXPORTED_VARIABLE;
			case STATE_VARIABLE_VALUE: return STATE_VARIABLE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private VariableScope(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //VariableScope
