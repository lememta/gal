/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.common;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function SM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Function defined in the system model. The examples of function types include graphical function and truth-table from state-flow and Embedded Matlab (EML) functions that can be defined both in Stateflow or Simulink. Functions from other sources (SCICOS) can be also uses, assuming that the function body in the source model can be converted to one of following representations:
 * <ul>
 * <li>graphical function or truth table encoded using GASMLanguage </li>
 * <li>EML function encoded in GACodeModel language</li>
 * </ul>
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getScope <em>Scope</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getReferenceExpression <em>Reference Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getArguments <em>Arguments</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getVariables <em>Variables</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage#getFunction_SM()
 * @model abstract="true"
 * @generated
 */
public interface Function_SM extends GASystemModelElement, Node, SystemFunction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Scope</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gasystemmodel.common.FunctionScope}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.common.FunctionScope
	 * @see #isSetScope()
	 * @see #unsetScope()
	 * @see #setScope(FunctionScope)
	 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage#getFunction_SM_Scope()
	 * @model unsettable="true"
	 * @generated
	 */
	FunctionScope getScope();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' attribute.
	 * @see geneauto.emf.models.gasystemmodel.common.FunctionScope
	 * @see #isSetScope()
	 * @see #unsetScope()
	 * @see #getScope()
	 * @generated
	 */
	void setScope(FunctionScope value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetScope()
	 * @see #getScope()
	 * @see #setScope(FunctionScope)
	 * @generated
	 */
	void unsetScope();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getScope <em>Scope</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Scope</em>' attribute is set.
	 * @see #unsetScope()
	 * @see #getScope()
	 * @see #setScope(FunctionScope)
	 * @generated
	 */
	boolean isSetScope();

	/**
	 * Returns the value of the '<em><b>Reference Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Expression</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Expression</em>' reference.
	 * @see #isSetReferenceExpression()
	 * @see #unsetReferenceExpression()
	 * @see #setReferenceExpression(Expression)
	 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage#getFunction_SM_ReferenceExpression()
	 * @model unsettable="true"
	 * @generated
	 */
	Expression getReferenceExpression();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getReferenceExpression <em>Reference Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Expression</em>' reference.
	 * @see #isSetReferenceExpression()
	 * @see #unsetReferenceExpression()
	 * @see #getReferenceExpression()
	 * @generated
	 */
	void setReferenceExpression(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getReferenceExpression <em>Reference Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetReferenceExpression()
	 * @see #getReferenceExpression()
	 * @see #setReferenceExpression(Expression)
	 * @generated
	 */
	void unsetReferenceExpression();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getReferenceExpression <em>Reference Expression</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Reference Expression</em>' reference is set.
	 * @see #unsetReferenceExpression()
	 * @see #getReferenceExpression()
	 * @see #setReferenceExpression(Expression)
	 * @generated
	 */
	boolean isSetReferenceExpression();

	/**
	 * Returns the value of the '<em><b>Arguments</b></em>' reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.FunctionArgument_SM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arguments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments</em>' reference list.
	 * @see #isSetArguments()
	 * @see #unsetArguments()
	 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage#getFunction_SM_Arguments()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<FunctionArgument_SM> getArguments();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getArguments <em>Arguments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetArguments()
	 * @see #getArguments()
	 * @generated
	 */
	void unsetArguments();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getArguments <em>Arguments</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Arguments</em>' reference list is set.
	 * @see #unsetArguments()
	 * @see #getArguments()
	 * @generated
	 */
	boolean isSetArguments();

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gasystemmodel.common.Variable_SM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see #isSetVariables()
	 * @see #unsetVariables()
	 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage#getFunction_SM_Variables()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Variable_SM> getVariables();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getVariables <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVariables()
	 * @see #getVariables()
	 * @generated
	 */
	void unsetVariables();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gasystemmodel.common.Function_SM#getVariables <em>Variables</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Variables</em>' containment reference list is set.
	 * @see #unsetVariables()
	 * @see #getVariables()
	 * @generated
	 */
	boolean isSetVariables();

} // Function_SM
