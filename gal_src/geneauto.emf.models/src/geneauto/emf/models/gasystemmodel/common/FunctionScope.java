/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Function Scope</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Scope of a function
 * <!-- end-model-doc -->
 * @see geneauto.emf.models.gasystemmodel.common.CommonPackage#getFunctionScope()
 * @model
 * @generated
 */
public enum FunctionScope implements Enumerator {
	/**
	 * The '<em><b>LOCAL FUNCTION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOCAL_FUNCTION_VALUE
	 * @generated
	 * @ordered
	 */
	LOCAL_FUNCTION(0, "LOCAL_FUNCTION", "LOCAL_FUNCTION"),

	/**
	 * The '<em><b>IMPORTED FUNCTION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPORTED_FUNCTION_VALUE
	 * @generated
	 * @ordered
	 */
	IMPORTED_FUNCTION(1, "IMPORTED_FUNCTION", "IMPORTED_FUNCTION"),

	/**
	 * The '<em><b>EXPORTED FUNCTION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXPORTED_FUNCTION_VALUE
	 * @generated
	 * @ordered
	 */
	EXPORTED_FUNCTION(2, "EXPORTED_FUNCTION", "EXPORTED_FUNCTION");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>LOCAL FUNCTION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOCAL FUNCTION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOCAL_FUNCTION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LOCAL_FUNCTION_VALUE = 0;

	/**
	 * The '<em><b>IMPORTED FUNCTION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IMPORTED FUNCTION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPORTED_FUNCTION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IMPORTED_FUNCTION_VALUE = 1;

	/**
	 * The '<em><b>EXPORTED FUNCTION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EXPORTED FUNCTION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EXPORTED_FUNCTION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXPORTED_FUNCTION_VALUE = 2;

	/**
	 * An array of all the '<em><b>Function Scope</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FunctionScope[] VALUES_ARRAY =
		new FunctionScope[] {
			LOCAL_FUNCTION,
			IMPORTED_FUNCTION,
			EXPORTED_FUNCTION,
		};

	/**
	 * A public read-only list of all the '<em><b>Function Scope</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FunctionScope> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Function Scope</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FunctionScope get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FunctionScope result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Function Scope</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FunctionScope getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FunctionScope result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Function Scope</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FunctionScope get(int value) {
		switch (value) {
			case LOCAL_FUNCTION_VALUE: return LOCAL_FUNCTION;
			case IMPORTED_FUNCTION_VALUE: return IMPORTED_FUNCTION;
			case EXPORTED_FUNCTION_VALUE: return EXPORTED_FUNCTION;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FunctionScope(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FunctionScope
