/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gasystemmodel.common.impl;

import geneauto.emf.models.common.NameSpaceElement;
import geneauto.emf.models.common.Variable;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gadatatypes.GADataType;

import geneauto.emf.models.gasystemmodel.common.CommonPackage;
import geneauto.emf.models.gasystemmodel.common.NameSpaceElement_SM;
import geneauto.emf.models.gasystemmodel.common.Node;
import geneauto.emf.models.gasystemmodel.common.SystemVariable;
import geneauto.emf.models.gasystemmodel.common.VariableScope;
import geneauto.emf.models.gasystemmodel.common.Variable_SM;

import geneauto.emf.models.gasystemmodel.gastatemodel.State;

import geneauto.emf.models.genericmodel.GANamed;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable SM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#getInitialValue <em>Initial Value</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#isConst <em>Is Const</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#isOptimizable <em>Is Optimizable</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#isStatic <em>Is Static</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#isVolatile <em>Is Volatile</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#getReferenceExpression <em>Reference Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link geneauto.emf.models.gasystemmodel.common.impl.Variable_SMImpl#getBinder <em>Binder</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Variable_SMImpl extends DataImpl implements Variable_SM {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected GADataType dataType;

	/**
	 * This is true if the Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dataTypeESet;

	/**
	 * The cached value of the '{@link #getInitialValue() <em>Initial Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialValue()
	 * @generated
	 * @ordered
	 */
	protected Expression initialValue;

	/**
	 * This is true if the Initial Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initialValueESet;

	/**
	 * The default value of the '{@link #isConst() <em>Is Const</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConst()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CONST_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConst() <em>Is Const</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConst()
	 * @generated
	 * @ordered
	 */
	protected boolean isConst = IS_CONST_EDEFAULT;

	/**
	 * This is true if the Is Const attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isConstESet;

	/**
	 * The default value of the '{@link #isOptimizable() <em>Is Optimizable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptimizable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_OPTIMIZABLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isOptimizable() <em>Is Optimizable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptimizable()
	 * @generated
	 * @ordered
	 */
	protected boolean isOptimizable = IS_OPTIMIZABLE_EDEFAULT;

	/**
	 * This is true if the Is Optimizable attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isOptimizableESet;

	/**
	 * The default value of the '{@link #isStatic() <em>Is Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_STATIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStatic() <em>Is Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected boolean isStatic = IS_STATIC_EDEFAULT;

	/**
	 * This is true if the Is Static attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isStaticESet;

	/**
	 * The default value of the '{@link #isVolatile() <em>Is Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_VOLATILE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVolatile() <em>Is Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected boolean isVolatile = IS_VOLATILE_EDEFAULT;

	/**
	 * This is true if the Is Volatile attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isVolatileESet;

	/**
	 * The cached value of the '{@link #getReferenceExpression() <em>Reference Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression referenceExpression;

	/**
	 * This is true if the Reference Expression reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean referenceExpressionESet;

	/**
	 * The default value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected static final VariableScope SCOPE_EDEFAULT = VariableScope.LOCAL_VARIABLE;

	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected VariableScope scope = SCOPE_EDEFAULT;

	/**
	 * This is true if the Scope attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean scopeESet;

	/**
	 * The cached value of the '{@link #getBinder() <em>Binder</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinder()
	 * @generated
	 * @ordered
	 */
	protected State binder;

	/**
	 * This is true if the Binder reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean binderESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Variable_SMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CommonPackage.Literals.VARIABLE_SM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataType(GADataType newDataType, NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = newDataType;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__DATA_TYPE, oldDataType, newDataType, !oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(GADataType newDataType) {
		if (newDataType != dataType) {
			NotificationChain msgs = null;
			if (dataType != null)
				msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CommonPackage.VARIABLE_SM__DATA_TYPE, null, msgs);
			if (newDataType != null)
				msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CommonPackage.VARIABLE_SM__DATA_TYPE, null, msgs);
			msgs = basicSetDataType(newDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__DATA_TYPE, newDataType, newDataType, !oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDataType(NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = null;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__DATA_TYPE, oldDataType, null, oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDataType() {
		if (dataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CommonPackage.VARIABLE_SM__DATA_TYPE, null, msgs);
			msgs = basicUnsetDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__DATA_TYPE, null, null, oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDataType() {
		return dataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInitialValue() {
		return initialValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitialValue(Expression newInitialValue, NotificationChain msgs) {
		Expression oldInitialValue = initialValue;
		initialValue = newInitialValue;
		boolean oldInitialValueESet = initialValueESet;
		initialValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__INITIAL_VALUE, oldInitialValue, newInitialValue, !oldInitialValueESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialValue(Expression newInitialValue) {
		if (newInitialValue != initialValue) {
			NotificationChain msgs = null;
			if (initialValue != null)
				msgs = ((InternalEObject)initialValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CommonPackage.VARIABLE_SM__INITIAL_VALUE, null, msgs);
			if (newInitialValue != null)
				msgs = ((InternalEObject)newInitialValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CommonPackage.VARIABLE_SM__INITIAL_VALUE, null, msgs);
			msgs = basicSetInitialValue(newInitialValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInitialValueESet = initialValueESet;
			initialValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__INITIAL_VALUE, newInitialValue, newInitialValue, !oldInitialValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInitialValue(NotificationChain msgs) {
		Expression oldInitialValue = initialValue;
		initialValue = null;
		boolean oldInitialValueESet = initialValueESet;
		initialValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__INITIAL_VALUE, oldInitialValue, null, oldInitialValueESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitialValue() {
		if (initialValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)initialValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CommonPackage.VARIABLE_SM__INITIAL_VALUE, null, msgs);
			msgs = basicUnsetInitialValue(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInitialValueESet = initialValueESet;
			initialValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__INITIAL_VALUE, null, null, oldInitialValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitialValue() {
		return initialValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConst() {
		return isConst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConst(boolean newIsConst) {
		boolean oldIsConst = isConst;
		isConst = newIsConst;
		boolean oldIsConstESet = isConstESet;
		isConstESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__IS_CONST, oldIsConst, isConst, !oldIsConstESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsConst() {
		boolean oldIsConst = isConst;
		boolean oldIsConstESet = isConstESet;
		isConst = IS_CONST_EDEFAULT;
		isConstESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__IS_CONST, oldIsConst, IS_CONST_EDEFAULT, oldIsConstESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsConst() {
		return isConstESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptimizable() {
		return isOptimizable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsOptimizable(boolean newIsOptimizable) {
		boolean oldIsOptimizable = isOptimizable;
		isOptimizable = newIsOptimizable;
		boolean oldIsOptimizableESet = isOptimizableESet;
		isOptimizableESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE, oldIsOptimizable, isOptimizable, !oldIsOptimizableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsOptimizable() {
		boolean oldIsOptimizable = isOptimizable;
		boolean oldIsOptimizableESet = isOptimizableESet;
		isOptimizable = IS_OPTIMIZABLE_EDEFAULT;
		isOptimizableESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE, oldIsOptimizable, IS_OPTIMIZABLE_EDEFAULT, oldIsOptimizableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsOptimizable() {
		return isOptimizableESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStatic() {
		return isStatic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsStatic(boolean newIsStatic) {
		boolean oldIsStatic = isStatic;
		isStatic = newIsStatic;
		boolean oldIsStaticESet = isStaticESet;
		isStaticESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__IS_STATIC, oldIsStatic, isStatic, !oldIsStaticESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsStatic() {
		boolean oldIsStatic = isStatic;
		boolean oldIsStaticESet = isStaticESet;
		isStatic = IS_STATIC_EDEFAULT;
		isStaticESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__IS_STATIC, oldIsStatic, IS_STATIC_EDEFAULT, oldIsStaticESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsStatic() {
		return isStaticESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVolatile() {
		return isVolatile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsVolatile(boolean newIsVolatile) {
		boolean oldIsVolatile = isVolatile;
		isVolatile = newIsVolatile;
		boolean oldIsVolatileESet = isVolatileESet;
		isVolatileESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__IS_VOLATILE, oldIsVolatile, isVolatile, !oldIsVolatileESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsVolatile() {
		boolean oldIsVolatile = isVolatile;
		boolean oldIsVolatileESet = isVolatileESet;
		isVolatile = IS_VOLATILE_EDEFAULT;
		isVolatileESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__IS_VOLATILE, oldIsVolatile, IS_VOLATILE_EDEFAULT, oldIsVolatileESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsVolatile() {
		return isVolatileESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getReferenceExpression() {
		if (referenceExpression != null && referenceExpression.eIsProxy()) {
			InternalEObject oldReferenceExpression = (InternalEObject)referenceExpression;
			referenceExpression = (Expression)eResolveProxy(oldReferenceExpression);
			if (referenceExpression != oldReferenceExpression) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CommonPackage.VARIABLE_SM__REFERENCE_EXPRESSION, oldReferenceExpression, referenceExpression));
			}
		}
		return referenceExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression basicGetReferenceExpression() {
		return referenceExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceExpression(Expression newReferenceExpression) {
		Expression oldReferenceExpression = referenceExpression;
		referenceExpression = newReferenceExpression;
		boolean oldReferenceExpressionESet = referenceExpressionESet;
		referenceExpressionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__REFERENCE_EXPRESSION, oldReferenceExpression, referenceExpression, !oldReferenceExpressionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReferenceExpression() {
		Expression oldReferenceExpression = referenceExpression;
		boolean oldReferenceExpressionESet = referenceExpressionESet;
		referenceExpression = null;
		referenceExpressionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__REFERENCE_EXPRESSION, oldReferenceExpression, null, oldReferenceExpressionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReferenceExpression() {
		return referenceExpressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableScope getScope() {
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(VariableScope newScope) {
		VariableScope oldScope = scope;
		scope = newScope == null ? SCOPE_EDEFAULT : newScope;
		boolean oldScopeESet = scopeESet;
		scopeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__SCOPE, oldScope, scope, !oldScopeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetScope() {
		VariableScope oldScope = scope;
		boolean oldScopeESet = scopeESet;
		scope = SCOPE_EDEFAULT;
		scopeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__SCOPE, oldScope, SCOPE_EDEFAULT, oldScopeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetScope() {
		return scopeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getBinder() {
		if (binder != null && binder.eIsProxy()) {
			InternalEObject oldBinder = (InternalEObject)binder;
			binder = (State)eResolveProxy(oldBinder);
			if (binder != oldBinder) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CommonPackage.VARIABLE_SM__BINDER, oldBinder, binder));
			}
		}
		return binder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetBinder() {
		return binder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinder(State newBinder) {
		State oldBinder = binder;
		binder = newBinder;
		boolean oldBinderESet = binderESet;
		binderESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.VARIABLE_SM__BINDER, oldBinder, binder, !oldBinderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBinder() {
		State oldBinder = binder;
		boolean oldBinderESet = binderESet;
		binder = null;
		binderESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.VARIABLE_SM__BINDER, oldBinder, null, oldBinderESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBinder() {
		return binderESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.VARIABLE_SM__DATA_TYPE:
				return basicUnsetDataType(msgs);
			case CommonPackage.VARIABLE_SM__INITIAL_VALUE:
				return basicUnsetInitialValue(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.VARIABLE_SM__DATA_TYPE:
				return getDataType();
			case CommonPackage.VARIABLE_SM__INITIAL_VALUE:
				return getInitialValue();
			case CommonPackage.VARIABLE_SM__IS_CONST:
				return isConst();
			case CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE:
				return isOptimizable();
			case CommonPackage.VARIABLE_SM__IS_STATIC:
				return isStatic();
			case CommonPackage.VARIABLE_SM__IS_VOLATILE:
				return isVolatile();
			case CommonPackage.VARIABLE_SM__REFERENCE_EXPRESSION:
				if (resolve) return getReferenceExpression();
				return basicGetReferenceExpression();
			case CommonPackage.VARIABLE_SM__SCOPE:
				return getScope();
			case CommonPackage.VARIABLE_SM__BINDER:
				if (resolve) return getBinder();
				return basicGetBinder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.VARIABLE_SM__DATA_TYPE:
				setDataType((GADataType)newValue);
				return;
			case CommonPackage.VARIABLE_SM__INITIAL_VALUE:
				setInitialValue((Expression)newValue);
				return;
			case CommonPackage.VARIABLE_SM__IS_CONST:
				setIsConst((Boolean)newValue);
				return;
			case CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE:
				setIsOptimizable((Boolean)newValue);
				return;
			case CommonPackage.VARIABLE_SM__IS_STATIC:
				setIsStatic((Boolean)newValue);
				return;
			case CommonPackage.VARIABLE_SM__IS_VOLATILE:
				setIsVolatile((Boolean)newValue);
				return;
			case CommonPackage.VARIABLE_SM__REFERENCE_EXPRESSION:
				setReferenceExpression((Expression)newValue);
				return;
			case CommonPackage.VARIABLE_SM__SCOPE:
				setScope((VariableScope)newValue);
				return;
			case CommonPackage.VARIABLE_SM__BINDER:
				setBinder((State)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.VARIABLE_SM__DATA_TYPE:
				unsetDataType();
				return;
			case CommonPackage.VARIABLE_SM__INITIAL_VALUE:
				unsetInitialValue();
				return;
			case CommonPackage.VARIABLE_SM__IS_CONST:
				unsetIsConst();
				return;
			case CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE:
				unsetIsOptimizable();
				return;
			case CommonPackage.VARIABLE_SM__IS_STATIC:
				unsetIsStatic();
				return;
			case CommonPackage.VARIABLE_SM__IS_VOLATILE:
				unsetIsVolatile();
				return;
			case CommonPackage.VARIABLE_SM__REFERENCE_EXPRESSION:
				unsetReferenceExpression();
				return;
			case CommonPackage.VARIABLE_SM__SCOPE:
				unsetScope();
				return;
			case CommonPackage.VARIABLE_SM__BINDER:
				unsetBinder();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.VARIABLE_SM__DATA_TYPE:
				return isSetDataType();
			case CommonPackage.VARIABLE_SM__INITIAL_VALUE:
				return isSetInitialValue();
			case CommonPackage.VARIABLE_SM__IS_CONST:
				return isSetIsConst();
			case CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE:
				return isSetIsOptimizable();
			case CommonPackage.VARIABLE_SM__IS_STATIC:
				return isSetIsStatic();
			case CommonPackage.VARIABLE_SM__IS_VOLATILE:
				return isSetIsVolatile();
			case CommonPackage.VARIABLE_SM__REFERENCE_EXPRESSION:
				return isSetReferenceExpression();
			case CommonPackage.VARIABLE_SM__SCOPE:
				return isSetScope();
			case CommonPackage.VARIABLE_SM__BINDER:
				return isSetBinder();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == GANamed.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Node.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == NameSpaceElement.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == NameSpaceElement_SM.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Variable.class) {
			switch (derivedFeatureID) {
				case CommonPackage.VARIABLE_SM__DATA_TYPE: return geneauto.emf.models.common.CommonPackage.VARIABLE__DATA_TYPE;
				case CommonPackage.VARIABLE_SM__INITIAL_VALUE: return geneauto.emf.models.common.CommonPackage.VARIABLE__INITIAL_VALUE;
				case CommonPackage.VARIABLE_SM__IS_CONST: return geneauto.emf.models.common.CommonPackage.VARIABLE__IS_CONST;
				case CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE: return geneauto.emf.models.common.CommonPackage.VARIABLE__IS_OPTIMIZABLE;
				case CommonPackage.VARIABLE_SM__IS_STATIC: return geneauto.emf.models.common.CommonPackage.VARIABLE__IS_STATIC;
				case CommonPackage.VARIABLE_SM__IS_VOLATILE: return geneauto.emf.models.common.CommonPackage.VARIABLE__IS_VOLATILE;
				default: return -1;
			}
		}
		if (baseClass == SystemVariable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == GANamed.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Node.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == NameSpaceElement.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == NameSpaceElement_SM.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Variable.class) {
			switch (baseFeatureID) {
				case geneauto.emf.models.common.CommonPackage.VARIABLE__DATA_TYPE: return CommonPackage.VARIABLE_SM__DATA_TYPE;
				case geneauto.emf.models.common.CommonPackage.VARIABLE__INITIAL_VALUE: return CommonPackage.VARIABLE_SM__INITIAL_VALUE;
				case geneauto.emf.models.common.CommonPackage.VARIABLE__IS_CONST: return CommonPackage.VARIABLE_SM__IS_CONST;
				case geneauto.emf.models.common.CommonPackage.VARIABLE__IS_OPTIMIZABLE: return CommonPackage.VARIABLE_SM__IS_OPTIMIZABLE;
				case geneauto.emf.models.common.CommonPackage.VARIABLE__IS_STATIC: return CommonPackage.VARIABLE_SM__IS_STATIC;
				case geneauto.emf.models.common.CommonPackage.VARIABLE__IS_VOLATILE: return CommonPackage.VARIABLE_SM__IS_VOLATILE;
				default: return -1;
			}
		}
		if (baseClass == SystemVariable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isConst: ");
		if (isConstESet) result.append(isConst); else result.append("<unset>");
		result.append(", isOptimizable: ");
		if (isOptimizableESet) result.append(isOptimizable); else result.append("<unset>");
		result.append(", isStatic: ");
		if (isStaticESet) result.append(isStatic); else result.append("<unset>");
		result.append(", isVolatile: ");
		if (isVolatileESet) result.append(isVolatile); else result.append("<unset>");
		result.append(", scope: ");
		if (scopeESet) result.append(scope); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //Variable_SMImpl
