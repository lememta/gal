/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.common.CommonPackage;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.GacodemodelFactory
 * @model kind="package"
 * @generated
 */
public interface GacodemodelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gacodemodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gacodemodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gacodemodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GacodemodelPackage eINSTANCE = geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl <em>GA Code Model Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getGACodeModelElement()
	 * @generated
	 */
	int GA_CODE_MODEL_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__SOURCE_ACTION = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT__MODULE = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>GA Code Model Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ELEMENT_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.CustomTypeContentImpl <em>Custom Type Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.CustomTypeContentImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCustomTypeContent()
	 * @generated
	 */
	int CUSTOM_TYPE_CONTENT = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The number of structural features of the '<em>Custom Type Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CONTENT_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.HasNameSpace <em>Has Name Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.HasNameSpace
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getHasNameSpace()
	 * @generated
	 */
	int HAS_NAME_SPACE = 2;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_NAME_SPACE__NAME_SPACE = 0;

	/**
	 * The number of structural features of the '<em>Has Name Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_NAME_SPACE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.NameSpaceImpl <em>Name Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.NameSpaceImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getNameSpace()
	 * @generated
	 */
	int NAME_SPACE = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Ns Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__NS_ELEMENTS = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Implicit Argument Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__IMPLICIT_ARGUMENT_VALUES = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Name Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.NameSpaceElement_CMImpl <em>Name Space Element CM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.NameSpaceElement_CMImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getNameSpaceElement_CM()
	 * @generated
	 */
	int NAME_SPACE_ELEMENT_CM = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM__SCOPE = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Name Space Element CM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_ELEMENT_CM_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl <em>Variable CM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.Variable_CMImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getVariable_CM()
	 * @generated
	 */
	int VARIABLE_CM = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__EXTERNAL_ID = NAME_SPACE_ELEMENT_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__ID = NAME_SPACE_ELEMENT_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__IS_FIXED_NAME = NAME_SPACE_ELEMENT_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__NAME = NAME_SPACE_ELEMENT_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__ORIGINAL_FULL_NAME = NAME_SPACE_ELEMENT_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__ORIGINAL_NAME = NAME_SPACE_ELEMENT_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__PARENT = NAME_SPACE_ELEMENT_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__MODEL = NAME_SPACE_ELEMENT_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__ANNOTATIONS = NAME_SPACE_ELEMENT_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__SOURCE_ELEMENT = NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__SOURCE_ELEMENT_ID = NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__SOURCE_ACTION = NAME_SPACE_ELEMENT_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__MODULE = NAME_SPACE_ELEMENT_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__SCOPE = NAME_SPACE_ELEMENT_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__DATA_TYPE = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__INITIAL_VALUE = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__IS_CONST = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__IS_OPTIMIZABLE = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__IS_STATIC = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__IS_VOLATILE = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Auto Init</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__AUTO_INIT = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Observation Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM__OBSERVATION_POINTS = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Variable CM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_CM_FEATURE_COUNT = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.CodeModelVariable <em>Code Model Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.CodeModelVariable
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCodeModelVariable()
	 * @generated
	 */
	int CODE_MODEL_VARIABLE = 6;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_VARIABLE__DATA_TYPE = CommonPackage.VARIABLE__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_VARIABLE__INITIAL_VALUE = CommonPackage.VARIABLE__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_VARIABLE__IS_CONST = CommonPackage.VARIABLE__IS_CONST;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_VARIABLE__IS_OPTIMIZABLE = CommonPackage.VARIABLE__IS_OPTIMIZABLE;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_VARIABLE__IS_STATIC = CommonPackage.VARIABLE__IS_STATIC;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_VARIABLE__IS_VOLATILE = CommonPackage.VARIABLE__IS_VOLATILE;

	/**
	 * The number of structural features of the '<em>Code Model Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_VARIABLE_FEATURE_COUNT = CommonPackage.VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.SequentialComposition <em>Sequential Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.SequentialComposition
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getSequentialComposition()
	 * @generated
	 */
	int SEQUENTIAL_COMPOSITION = 7;

	/**
	 * The number of structural features of the '<em>Sequential Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_COMPOSITION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.LabelImpl <em>Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.LabelImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getLabel()
	 * @generated
	 */
	int LABEL = 8;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The number of structural features of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.DependencyImpl <em>Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.DependencyImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getDependency()
	 * @generated
	 */
	int DEPENDENCY = 9;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The number of structural features of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.CustomType_CMImpl <em>Custom Type CM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.CustomType_CMImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCustomType_CM()
	 * @generated
	 */
	int CUSTOM_TYPE_CM = 10;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__EXTERNAL_ID = NAME_SPACE_ELEMENT_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__ID = NAME_SPACE_ELEMENT_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__IS_FIXED_NAME = NAME_SPACE_ELEMENT_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__NAME = NAME_SPACE_ELEMENT_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__ORIGINAL_FULL_NAME = NAME_SPACE_ELEMENT_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__ORIGINAL_NAME = NAME_SPACE_ELEMENT_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__PARENT = NAME_SPACE_ELEMENT_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__MODEL = NAME_SPACE_ELEMENT_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__ANNOTATIONS = NAME_SPACE_ELEMENT_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__SOURCE_ELEMENT = NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__SOURCE_ELEMENT_ID = NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__SOURCE_ACTION = NAME_SPACE_ELEMENT_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__MODULE = NAME_SPACE_ELEMENT_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__SCOPE = NAME_SPACE_ELEMENT_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM__CONTENT = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Custom Type CM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TYPE_CM_FEATURE_COUNT = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.GACodeModelImpl <em>GA Code Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.GACodeModelImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getGACodeModel()
	 * @generated
	 */
	int GA_CODE_MODEL = 11;

	/**
	 * The feature id for the '<em><b>Last Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__LAST_ID = GenericmodelPackage.MODEL__LAST_ID;

	/**
	 * The feature id for the '<em><b>Last Saved By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__LAST_SAVED_BY = GenericmodelPackage.MODEL__LAST_SAVED_BY;

	/**
	 * The feature id for the '<em><b>Last Saved On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__LAST_SAVED_ON = GenericmodelPackage.MODEL__LAST_SAVED_ON;

	/**
	 * The feature id for the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__MODEL_NAME = GenericmodelPackage.MODEL__MODEL_NAME;

	/**
	 * The feature id for the '<em><b>Model Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__MODEL_VERSION = GenericmodelPackage.MODEL__MODEL_VERSION;

	/**
	 * The feature id for the '<em><b>No New Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__NO_NEW_ID = GenericmodelPackage.MODEL__NO_NEW_ID;

	/**
	 * The feature id for the '<em><b>Transformations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__TRANSFORMATIONS = GenericmodelPackage.MODEL__TRANSFORMATIONS;

	/**
	 * The feature id for the '<em><b>System Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__SYSTEM_MODEL = GenericmodelPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Temp Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__TEMP_MODEL = GenericmodelPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL__ELEMENTS = GenericmodelPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>GA Code Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_FEATURE_COUNT = GenericmodelPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.TempModelImpl <em>Temp Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.TempModelImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getTempModel()
	 * @generated
	 */
	int TEMP_MODEL = 12;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>State Variables Struct Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Temp Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__TEMP_NAME_SPACE = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL__ELEMENTS = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Temp Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMP_MODEL_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.GACodeModelRoot <em>GA Code Model Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.GACodeModelRoot
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getGACodeModelRoot()
	 * @generated
	 */
	int GA_CODE_MODEL_ROOT = 13;

	/**
	 * The number of structural features of the '<em>GA Code Model Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_CODE_MODEL_ROOT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.StructureMemberImpl <em>Structure Member</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.StructureMemberImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getStructureMember()
	 * @generated
	 */
	int STRUCTURE_MEMBER = 14;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__EXTERNAL_ID = VARIABLE_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__ID = VARIABLE_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__IS_FIXED_NAME = VARIABLE_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__NAME = VARIABLE_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__ORIGINAL_FULL_NAME = VARIABLE_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__ORIGINAL_NAME = VARIABLE_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__PARENT = VARIABLE_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__MODEL = VARIABLE_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__ANNOTATIONS = VARIABLE_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__SOURCE_ELEMENT = VARIABLE_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__SOURCE_ELEMENT_ID = VARIABLE_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__SOURCE_ACTION = VARIABLE_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__MODULE = VARIABLE_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__SCOPE = VARIABLE_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__DATA_TYPE = VARIABLE_CM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__INITIAL_VALUE = VARIABLE_CM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__IS_CONST = VARIABLE_CM__IS_CONST;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__IS_OPTIMIZABLE = VARIABLE_CM__IS_OPTIMIZABLE;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__IS_STATIC = VARIABLE_CM__IS_STATIC;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__IS_VOLATILE = VARIABLE_CM__IS_VOLATILE;

	/**
	 * The feature id for the '<em><b>Auto Init</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__AUTO_INIT = VARIABLE_CM__AUTO_INIT;

	/**
	 * The feature id for the '<em><b>Observation Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER__OBSERVATION_POINTS = VARIABLE_CM__OBSERVATION_POINTS;

	/**
	 * The number of structural features of the '<em>Structure Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_MEMBER_FEATURE_COUNT = VARIABLE_CM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.ArrayContentImpl <em>Array Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.ArrayContentImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getArrayContent()
	 * @generated
	 */
	int ARRAY_CONTENT = 15;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__EXTERNAL_ID = CUSTOM_TYPE_CONTENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__ID = CUSTOM_TYPE_CONTENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__IS_FIXED_NAME = CUSTOM_TYPE_CONTENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__NAME = CUSTOM_TYPE_CONTENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__ORIGINAL_FULL_NAME = CUSTOM_TYPE_CONTENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__ORIGINAL_NAME = CUSTOM_TYPE_CONTENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__PARENT = CUSTOM_TYPE_CONTENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__MODEL = CUSTOM_TYPE_CONTENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__ANNOTATIONS = CUSTOM_TYPE_CONTENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__SOURCE_ELEMENT = CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__SOURCE_ELEMENT_ID = CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__SOURCE_ACTION = CUSTOM_TYPE_CONTENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__MODULE = CUSTOM_TYPE_CONTENT__MODULE;

	/**
	 * The feature id for the '<em><b>Range</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__RANGE = CUSTOM_TYPE_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT__BASE_TYPE = CUSTOM_TYPE_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Array Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_CONTENT_FEATURE_COUNT = CUSTOM_TYPE_CONTENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.DerivedTypeImpl <em>Derived Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.DerivedTypeImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getDerivedType()
	 * @generated
	 */
	int DERIVED_TYPE = 16;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__EXTERNAL_ID = CUSTOM_TYPE_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__ID = CUSTOM_TYPE_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__IS_FIXED_NAME = CUSTOM_TYPE_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__NAME = CUSTOM_TYPE_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__ORIGINAL_FULL_NAME = CUSTOM_TYPE_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__ORIGINAL_NAME = CUSTOM_TYPE_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__PARENT = CUSTOM_TYPE_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__MODEL = CUSTOM_TYPE_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__ANNOTATIONS = CUSTOM_TYPE_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__SOURCE_ELEMENT = CUSTOM_TYPE_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__SOURCE_ELEMENT_ID = CUSTOM_TYPE_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__SOURCE_ACTION = CUSTOM_TYPE_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__MODULE = CUSTOM_TYPE_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__SCOPE = CUSTOM_TYPE_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__CONTENT = CUSTOM_TYPE_CM__CONTENT;

	/**
	 * The feature id for the '<em><b>Parent Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE__PARENT_TYPE = CUSTOM_TYPE_CM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Derived Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_TYPE_FEATURE_COUNT = CUSTOM_TYPE_CM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.FunctionBodyImpl <em>Function Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.FunctionBodyImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getFunctionBody()
	 * @generated
	 */
	int FUNCTION_BODY = 18;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The number of structural features of the '<em>Function Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BODY_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.ExpressionFunctionBodyImpl <em>Expression Function Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.ExpressionFunctionBodyImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getExpressionFunctionBody()
	 * @generated
	 */
	int EXPRESSION_FUNCTION_BODY = 17;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__EXTERNAL_ID = FUNCTION_BODY__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__ID = FUNCTION_BODY__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__IS_FIXED_NAME = FUNCTION_BODY__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__NAME = FUNCTION_BODY__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__ORIGINAL_FULL_NAME = FUNCTION_BODY__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__ORIGINAL_NAME = FUNCTION_BODY__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__PARENT = FUNCTION_BODY__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__MODEL = FUNCTION_BODY__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__ANNOTATIONS = FUNCTION_BODY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__SOURCE_ELEMENT = FUNCTION_BODY__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__SOURCE_ELEMENT_ID = FUNCTION_BODY__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__SOURCE_ACTION = FUNCTION_BODY__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__MODULE = FUNCTION_BODY__MODULE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY__EXPRESSION = FUNCTION_BODY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expression Function Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FUNCTION_BODY_FEATURE_COUNT = FUNCTION_BODY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.FunctionArgument_CMImpl <em>Function Argument CM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.FunctionArgument_CMImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getFunctionArgument_CM()
	 * @generated
	 */
	int FUNCTION_ARGUMENT_CM = 19;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__EXTERNAL_ID = VARIABLE_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__ID = VARIABLE_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__IS_FIXED_NAME = VARIABLE_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__NAME = VARIABLE_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__ORIGINAL_FULL_NAME = VARIABLE_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__ORIGINAL_NAME = VARIABLE_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__PARENT = VARIABLE_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__MODEL = VARIABLE_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__ANNOTATIONS = VARIABLE_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__SOURCE_ELEMENT = VARIABLE_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__SOURCE_ELEMENT_ID = VARIABLE_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__SOURCE_ACTION = VARIABLE_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__MODULE = VARIABLE_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__SCOPE = VARIABLE_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__DATA_TYPE = VARIABLE_CM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__INITIAL_VALUE = VARIABLE_CM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__IS_CONST = VARIABLE_CM__IS_CONST;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__IS_OPTIMIZABLE = VARIABLE_CM__IS_OPTIMIZABLE;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__IS_STATIC = VARIABLE_CM__IS_STATIC;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__IS_VOLATILE = VARIABLE_CM__IS_VOLATILE;

	/**
	 * The feature id for the '<em><b>Auto Init</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__AUTO_INIT = VARIABLE_CM__AUTO_INIT;

	/**
	 * The feature id for the '<em><b>Observation Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__OBSERVATION_POINTS = VARIABLE_CM__OBSERVATION_POINTS;

	/**
	 * The feature id for the '<em><b>Is Implicit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__IS_IMPLICIT = VARIABLE_CM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Redundant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__IS_REDUNDANT = VARIABLE_CM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM__DIRECTION = VARIABLE_CM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Function Argument CM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_ARGUMENT_CM_FEATURE_COUNT = VARIABLE_CM_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.StructureVariableImpl <em>Structure Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.StructureVariableImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getStructureVariable()
	 * @generated
	 */
	int STRUCTURE_VARIABLE = 20;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__EXTERNAL_ID = VARIABLE_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__ID = VARIABLE_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__IS_FIXED_NAME = VARIABLE_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__NAME = VARIABLE_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__ORIGINAL_FULL_NAME = VARIABLE_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__ORIGINAL_NAME = VARIABLE_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__PARENT = VARIABLE_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__MODEL = VARIABLE_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__ANNOTATIONS = VARIABLE_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__SOURCE_ELEMENT = VARIABLE_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__SOURCE_ELEMENT_ID = VARIABLE_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__SOURCE_ACTION = VARIABLE_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__MODULE = VARIABLE_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__SCOPE = VARIABLE_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__DATA_TYPE = VARIABLE_CM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__INITIAL_VALUE = VARIABLE_CM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__IS_CONST = VARIABLE_CM__IS_CONST;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__IS_OPTIMIZABLE = VARIABLE_CM__IS_OPTIMIZABLE;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__IS_STATIC = VARIABLE_CM__IS_STATIC;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__IS_VOLATILE = VARIABLE_CM__IS_VOLATILE;

	/**
	 * The feature id for the '<em><b>Auto Init</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__AUTO_INIT = VARIABLE_CM__AUTO_INIT;

	/**
	 * The feature id for the '<em><b>Observation Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE__OBSERVATION_POINTS = VARIABLE_CM__OBSERVATION_POINTS;

	/**
	 * The number of structural features of the '<em>Structure Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_VARIABLE_FEATURE_COUNT = VARIABLE_CM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.CodeModelDependencyImpl <em>Code Model Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.CodeModelDependencyImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCodeModelDependency()
	 * @generated
	 */
	int CODE_MODEL_DEPENDENCY = 21;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__EXTERNAL_ID = DEPENDENCY__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__ID = DEPENDENCY__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__IS_FIXED_NAME = DEPENDENCY__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__NAME = DEPENDENCY__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__ORIGINAL_FULL_NAME = DEPENDENCY__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__ORIGINAL_NAME = DEPENDENCY__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__PARENT = DEPENDENCY__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__MODEL = DEPENDENCY__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__ANNOTATIONS = DEPENDENCY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__SOURCE_ELEMENT = DEPENDENCY__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__SOURCE_ELEMENT_ID = DEPENDENCY__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__SOURCE_ACTION = DEPENDENCY__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__MODULE = DEPENDENCY__MODULE;

	/**
	 * The feature id for the '<em><b>Dependent Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY__DEPENDENT_MODULE = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Code Model Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_DEPENDENCY_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.ExternalDependencyImpl <em>External Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.ExternalDependencyImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getExternalDependency()
	 * @generated
	 */
	int EXTERNAL_DEPENDENCY = 22;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__EXTERNAL_ID = DEPENDENCY__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__ID = DEPENDENCY__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__IS_FIXED_NAME = DEPENDENCY__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__NAME = DEPENDENCY__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__ORIGINAL_FULL_NAME = DEPENDENCY__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__ORIGINAL_NAME = DEPENDENCY__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__PARENT = DEPENDENCY__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__MODEL = DEPENDENCY__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__ANNOTATIONS = DEPENDENCY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__SOURCE_ELEMENT = DEPENDENCY__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__SOURCE_ELEMENT_ID = DEPENDENCY__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__SOURCE_ACTION = DEPENDENCY__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__MODULE = DEPENDENCY__MODULE;

	/**
	 * The feature id for the '<em><b>Is System</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__IS_SYSTEM = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Non Standard Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>External Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_DEPENDENCY_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.NamedConstantImpl <em>Named Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.NamedConstantImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getNamedConstant()
	 * @generated
	 */
	int NAMED_CONSTANT = 23;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__EXTERNAL_ID = VARIABLE_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__ID = VARIABLE_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__IS_FIXED_NAME = VARIABLE_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__NAME = VARIABLE_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__ORIGINAL_FULL_NAME = VARIABLE_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__ORIGINAL_NAME = VARIABLE_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__PARENT = VARIABLE_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__MODEL = VARIABLE_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__ANNOTATIONS = VARIABLE_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__SOURCE_ELEMENT = VARIABLE_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__SOURCE_ELEMENT_ID = VARIABLE_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__SOURCE_ACTION = VARIABLE_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__MODULE = VARIABLE_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__SCOPE = VARIABLE_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__DATA_TYPE = VARIABLE_CM__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__INITIAL_VALUE = VARIABLE_CM__INITIAL_VALUE;

	/**
	 * The feature id for the '<em><b>Is Const</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__IS_CONST = VARIABLE_CM__IS_CONST;

	/**
	 * The feature id for the '<em><b>Is Optimizable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__IS_OPTIMIZABLE = VARIABLE_CM__IS_OPTIMIZABLE;

	/**
	 * The feature id for the '<em><b>Is Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__IS_STATIC = VARIABLE_CM__IS_STATIC;

	/**
	 * The feature id for the '<em><b>Is Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__IS_VOLATILE = VARIABLE_CM__IS_VOLATILE;

	/**
	 * The feature id for the '<em><b>Auto Init</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__AUTO_INIT = VARIABLE_CM__AUTO_INIT;

	/**
	 * The feature id for the '<em><b>Observation Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT__OBSERVATION_POINTS = VARIABLE_CM__OBSERVATION_POINTS;

	/**
	 * The number of structural features of the '<em>Named Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_CONSTANT_FEATURE_COUNT = VARIABLE_CM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.Function_CMImpl <em>Function CM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.Function_CMImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getFunction_CM()
	 * @generated
	 */
	int FUNCTION_CM = 24;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__EXTERNAL_ID = NAME_SPACE_ELEMENT_CM__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__ID = NAME_SPACE_ELEMENT_CM__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__IS_FIXED_NAME = NAME_SPACE_ELEMENT_CM__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__NAME = NAME_SPACE_ELEMENT_CM__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__ORIGINAL_FULL_NAME = NAME_SPACE_ELEMENT_CM__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__ORIGINAL_NAME = NAME_SPACE_ELEMENT_CM__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__PARENT = NAME_SPACE_ELEMENT_CM__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__MODEL = NAME_SPACE_ELEMENT_CM__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__ANNOTATIONS = NAME_SPACE_ELEMENT_CM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__SOURCE_ELEMENT = NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__SOURCE_ELEMENT_ID = NAME_SPACE_ELEMENT_CM__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__SOURCE_ACTION = NAME_SPACE_ELEMENT_CM__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__MODULE = NAME_SPACE_ELEMENT_CM__MODULE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__SCOPE = NAME_SPACE_ELEMENT_CM__SCOPE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__DATA_TYPE = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__NAME_SPACE = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Function Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__FUNCTION_STYLE = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__ARGUMENTS = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM__BODY = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Function CM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CM_FEATURE_COUNT = NAME_SPACE_ELEMENT_CM_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.CodeModelFunction <em>Code Model Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.CodeModelFunction
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCodeModelFunction()
	 * @generated
	 */
	int CODE_MODEL_FUNCTION = 25;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_FUNCTION__DATA_TYPE = CommonPackage.FUNCTION__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Code Model Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_MODEL_FUNCTION_FEATURE_COUNT = CommonPackage.FUNCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.SequentialFunctionBodyImpl <em>Sequential Function Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.SequentialFunctionBodyImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getSequentialFunctionBody()
	 * @generated
	 */
	int SEQUENTIAL_FUNCTION_BODY = 26;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__EXTERNAL_ID = FUNCTION_BODY__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__ID = FUNCTION_BODY__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__IS_FIXED_NAME = FUNCTION_BODY__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__NAME = FUNCTION_BODY__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__ORIGINAL_FULL_NAME = FUNCTION_BODY__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__ORIGINAL_NAME = FUNCTION_BODY__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__PARENT = FUNCTION_BODY__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__MODEL = FUNCTION_BODY__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__ANNOTATIONS = FUNCTION_BODY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__SOURCE_ELEMENT = FUNCTION_BODY__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__SOURCE_ELEMENT_ID = FUNCTION_BODY__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__SOURCE_ACTION = FUNCTION_BODY__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__MODULE = FUNCTION_BODY__MODULE;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__NAME_SPACE = FUNCTION_BODY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY__STATEMENTS = FUNCTION_BODY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sequential Function Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_FUNCTION_BODY_FEATURE_COUNT = FUNCTION_BODY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.LiteralContentImpl <em>Literal Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.LiteralContentImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getLiteralContent()
	 * @generated
	 */
	int LITERAL_CONTENT = 27;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__EXTERNAL_ID = CUSTOM_TYPE_CONTENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__ID = CUSTOM_TYPE_CONTENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__IS_FIXED_NAME = CUSTOM_TYPE_CONTENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__NAME = CUSTOM_TYPE_CONTENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__ORIGINAL_FULL_NAME = CUSTOM_TYPE_CONTENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__ORIGINAL_NAME = CUSTOM_TYPE_CONTENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__PARENT = CUSTOM_TYPE_CONTENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__MODEL = CUSTOM_TYPE_CONTENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__ANNOTATIONS = CUSTOM_TYPE_CONTENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__SOURCE_ELEMENT = CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__SOURCE_ELEMENT_ID = CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__SOURCE_ACTION = CUSTOM_TYPE_CONTENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__MODULE = CUSTOM_TYPE_CONTENT__MODULE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT__VALUE = CUSTOM_TYPE_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Literal Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_CONTENT_FEATURE_COUNT = CUSTOM_TYPE_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.StructureContentImpl <em>Structure Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.StructureContentImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getStructureContent()
	 * @generated
	 */
	int STRUCTURE_CONTENT = 28;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__EXTERNAL_ID = CUSTOM_TYPE_CONTENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__ID = CUSTOM_TYPE_CONTENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__IS_FIXED_NAME = CUSTOM_TYPE_CONTENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__NAME = CUSTOM_TYPE_CONTENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__ORIGINAL_FULL_NAME = CUSTOM_TYPE_CONTENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__ORIGINAL_NAME = CUSTOM_TYPE_CONTENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__PARENT = CUSTOM_TYPE_CONTENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__MODEL = CUSTOM_TYPE_CONTENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__ANNOTATIONS = CUSTOM_TYPE_CONTENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__SOURCE_ELEMENT = CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__SOURCE_ELEMENT_ID = CUSTOM_TYPE_CONTENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__SOURCE_ACTION = CUSTOM_TYPE_CONTENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__MODULE = CUSTOM_TYPE_CONTENT__MODULE;

	/**
	 * The feature id for the '<em><b>Struct Members</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT__STRUCT_MEMBERS = CUSTOM_TYPE_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Structure Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_CONTENT_FEATURE_COUNT = CUSTOM_TYPE_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl <em>Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.impl.ModuleImpl
	 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getModule()
	 * @generated
	 */
	int MODULE = 29;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__EXTERNAL_ID = GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__ID = GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__IS_FIXED_NAME = GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__NAME = GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__ORIGINAL_FULL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__ORIGINAL_NAME = GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__PARENT = GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__MODEL = GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__ANNOTATIONS = GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__SOURCE_ELEMENT = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__SOURCE_ELEMENT_ID = GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__SOURCE_ACTION = GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__MODULE = GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__NAME_SPACE = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Header File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__HEADER_FILE_NAME = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__SOURCE_FILE_NAME = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Init Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__INIT_FUNCTION = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__DEPENDENCIES = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Header Annotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE__HEADER_ANNOTATION = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_FEATURE_COUNT = GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 6;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.GACodeModelElement <em>GA Code Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA Code Model Element</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement
	 * @generated
	 */
	EClass getGACodeModelElement();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElement <em>Source Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Element</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElement()
	 * @see #getGACodeModelElement()
	 * @generated
	 */
	EReference getGACodeModelElement_SourceElement();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElementId <em>Source Element Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Element Id</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElementId()
	 * @see #getGACodeModelElement()
	 * @generated
	 */
	EAttribute getGACodeModelElement_SourceElementId();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceAction <em>Source Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Action</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceAction()
	 * @see #getGACodeModelElement()
	 * @generated
	 */
	EAttribute getGACodeModelElement_SourceAction();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getModule <em>Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Module</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement#getModule()
	 * @see #getGACodeModelElement()
	 * @generated
	 */
	EReference getGACodeModelElement_Module();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.CustomTypeContent <em>Custom Type Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Type Content</em>'.
	 * @see geneauto.emf.models.gacodemodel.CustomTypeContent
	 * @generated
	 */
	EClass getCustomTypeContent();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.HasNameSpace <em>Has Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Name Space</em>'.
	 * @see geneauto.emf.models.gacodemodel.HasNameSpace
	 * @generated
	 */
	EClass getHasNameSpace();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.HasNameSpace#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name Space</em>'.
	 * @see geneauto.emf.models.gacodemodel.HasNameSpace#getNameSpace()
	 * @see #getHasNameSpace()
	 * @generated
	 */
	EReference getHasNameSpace_NameSpace();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.NameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name Space</em>'.
	 * @see geneauto.emf.models.gacodemodel.NameSpace
	 * @generated
	 */
	EClass getNameSpace();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.NameSpace#getNsElements <em>Ns Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ns Elements</em>'.
	 * @see geneauto.emf.models.gacodemodel.NameSpace#getNsElements()
	 * @see #getNameSpace()
	 * @generated
	 */
	EReference getNameSpace_NsElements();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.NameSpace#getImplicitArgumentValues <em>Implicit Argument Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Implicit Argument Values</em>'.
	 * @see geneauto.emf.models.gacodemodel.NameSpace#getImplicitArgumentValues()
	 * @see #getNameSpace()
	 * @generated
	 */
	EReference getNameSpace_ImplicitArgumentValues();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.NameSpaceElement_CM <em>Name Space Element CM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name Space Element CM</em>'.
	 * @see geneauto.emf.models.gacodemodel.NameSpaceElement_CM
	 * @generated
	 */
	EClass getNameSpaceElement_CM();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.NameSpaceElement_CM#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see geneauto.emf.models.gacodemodel.NameSpaceElement_CM#getScope()
	 * @see #getNameSpaceElement_CM()
	 * @generated
	 */
	EAttribute getNameSpaceElement_CM_Scope();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.Variable_CM <em>Variable CM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable CM</em>'.
	 * @see geneauto.emf.models.gacodemodel.Variable_CM
	 * @generated
	 */
	EClass getVariable_CM();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.Variable_CM#isAutoInit <em>Auto Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Auto Init</em>'.
	 * @see geneauto.emf.models.gacodemodel.Variable_CM#isAutoInit()
	 * @see #getVariable_CM()
	 * @generated
	 */
	EAttribute getVariable_CM_AutoInit();

	/**
	 * Returns the meta object for the attribute list '{@link geneauto.emf.models.gacodemodel.Variable_CM#getObservationPoints <em>Observation Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Observation Points</em>'.
	 * @see geneauto.emf.models.gacodemodel.Variable_CM#getObservationPoints()
	 * @see #getVariable_CM()
	 * @generated
	 */
	EAttribute getVariable_CM_ObservationPoints();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.CodeModelVariable <em>Code Model Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Model Variable</em>'.
	 * @see geneauto.emf.models.gacodemodel.CodeModelVariable
	 * @generated
	 */
	EClass getCodeModelVariable();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.SequentialComposition <em>Sequential Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequential Composition</em>'.
	 * @see geneauto.emf.models.gacodemodel.SequentialComposition
	 * @generated
	 */
	EClass getSequentialComposition();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label</em>'.
	 * @see geneauto.emf.models.gacodemodel.Label
	 * @generated
	 */
	EClass getLabel();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.Dependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependency</em>'.
	 * @see geneauto.emf.models.gacodemodel.Dependency
	 * @generated
	 */
	EClass getDependency();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.CustomType_CM <em>Custom Type CM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Type CM</em>'.
	 * @see geneauto.emf.models.gacodemodel.CustomType_CM
	 * @generated
	 */
	EClass getCustomType_CM();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.CustomType_CM#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content</em>'.
	 * @see geneauto.emf.models.gacodemodel.CustomType_CM#getContent()
	 * @see #getCustomType_CM()
	 * @generated
	 */
	EReference getCustomType_CM_Content();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.GACodeModel <em>GA Code Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA Code Model</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModel
	 * @generated
	 */
	EClass getGACodeModel();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.GACodeModel#getSystemModel <em>System Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>System Model</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModel#getSystemModel()
	 * @see #getGACodeModel()
	 * @generated
	 */
	EReference getGACodeModel_SystemModel();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.GACodeModel#getTempModel <em>Temp Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Temp Model</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModel#getTempModel()
	 * @see #getGACodeModel()
	 * @generated
	 */
	EReference getGACodeModel_TempModel();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.GACodeModel#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModel#getElements()
	 * @see #getGACodeModel()
	 * @generated
	 */
	EReference getGACodeModel_Elements();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.TempModel <em>Temp Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Temp Model</em>'.
	 * @see geneauto.emf.models.gacodemodel.TempModel
	 * @generated
	 */
	EClass getTempModel();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.TempModel#getStateVariablesStructType <em>State Variables Struct Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State Variables Struct Type</em>'.
	 * @see geneauto.emf.models.gacodemodel.TempModel#getStateVariablesStructType()
	 * @see #getTempModel()
	 * @generated
	 */
	EReference getTempModel_StateVariablesStructType();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.TempModel#getTempNameSpace <em>Temp Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Temp Name Space</em>'.
	 * @see geneauto.emf.models.gacodemodel.TempModel#getTempNameSpace()
	 * @see #getTempModel()
	 * @generated
	 */
	EReference getTempModel_TempNameSpace();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.TempModel#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see geneauto.emf.models.gacodemodel.TempModel#getElements()
	 * @see #getTempModel()
	 * @generated
	 */
	EReference getTempModel_Elements();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.GACodeModelRoot <em>GA Code Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA Code Model Root</em>'.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelRoot
	 * @generated
	 */
	EClass getGACodeModelRoot();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.StructureMember <em>Structure Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Structure Member</em>'.
	 * @see geneauto.emf.models.gacodemodel.StructureMember
	 * @generated
	 */
	EClass getStructureMember();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.ArrayContent <em>Array Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Content</em>'.
	 * @see geneauto.emf.models.gacodemodel.ArrayContent
	 * @generated
	 */
	EClass getArrayContent();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.ArrayContent#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Range</em>'.
	 * @see geneauto.emf.models.gacodemodel.ArrayContent#getRange()
	 * @see #getArrayContent()
	 * @generated
	 */
	EReference getArrayContent_Range();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.ArrayContent#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Base Type</em>'.
	 * @see geneauto.emf.models.gacodemodel.ArrayContent#getBaseType()
	 * @see #getArrayContent()
	 * @generated
	 */
	EReference getArrayContent_BaseType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.DerivedType <em>Derived Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Derived Type</em>'.
	 * @see geneauto.emf.models.gacodemodel.DerivedType
	 * @generated
	 */
	EClass getDerivedType();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.DerivedType#getParentType <em>Parent Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent Type</em>'.
	 * @see geneauto.emf.models.gacodemodel.DerivedType#getParentType()
	 * @see #getDerivedType()
	 * @generated
	 */
	EReference getDerivedType_ParentType();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.ExpressionFunctionBody <em>Expression Function Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Function Body</em>'.
	 * @see geneauto.emf.models.gacodemodel.ExpressionFunctionBody
	 * @generated
	 */
	EClass getExpressionFunctionBody();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.ExpressionFunctionBody#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.ExpressionFunctionBody#getExpression()
	 * @see #getExpressionFunctionBody()
	 * @generated
	 */
	EReference getExpressionFunctionBody_Expression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.FunctionBody <em>Function Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Body</em>'.
	 * @see geneauto.emf.models.gacodemodel.FunctionBody
	 * @generated
	 */
	EClass getFunctionBody();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM <em>Function Argument CM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Argument CM</em>'.
	 * @see geneauto.emf.models.gacodemodel.FunctionArgument_CM
	 * @generated
	 */
	EClass getFunctionArgument_CM();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isImplicit <em>Is Implicit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Implicit</em>'.
	 * @see geneauto.emf.models.gacodemodel.FunctionArgument_CM#isImplicit()
	 * @see #getFunctionArgument_CM()
	 * @generated
	 */
	EAttribute getFunctionArgument_CM_IsImplicit();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isRedundant <em>Is Redundant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Redundant</em>'.
	 * @see geneauto.emf.models.gacodemodel.FunctionArgument_CM#isRedundant()
	 * @see #getFunctionArgument_CM()
	 * @generated
	 */
	EAttribute getFunctionArgument_CM_IsRedundant();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see geneauto.emf.models.gacodemodel.FunctionArgument_CM#getDirection()
	 * @see #getFunctionArgument_CM()
	 * @generated
	 */
	EAttribute getFunctionArgument_CM_Direction();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.StructureVariable <em>Structure Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Structure Variable</em>'.
	 * @see geneauto.emf.models.gacodemodel.StructureVariable
	 * @generated
	 */
	EClass getStructureVariable();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.CodeModelDependency <em>Code Model Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Model Dependency</em>'.
	 * @see geneauto.emf.models.gacodemodel.CodeModelDependency
	 * @generated
	 */
	EClass getCodeModelDependency();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.CodeModelDependency#getDependentModule <em>Dependent Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dependent Module</em>'.
	 * @see geneauto.emf.models.gacodemodel.CodeModelDependency#getDependentModule()
	 * @see #getCodeModelDependency()
	 * @generated
	 */
	EReference getCodeModelDependency_DependentModule();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.ExternalDependency <em>External Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Dependency</em>'.
	 * @see geneauto.emf.models.gacodemodel.ExternalDependency
	 * @generated
	 */
	EClass getExternalDependency();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isSystem <em>Is System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is System</em>'.
	 * @see geneauto.emf.models.gacodemodel.ExternalDependency#isSystem()
	 * @see #getExternalDependency()
	 * @generated
	 */
	EAttribute getExternalDependency_IsSystem();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isNonStandardExtension <em>Non Standard Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Non Standard Extension</em>'.
	 * @see geneauto.emf.models.gacodemodel.ExternalDependency#isNonStandardExtension()
	 * @see #getExternalDependency()
	 * @generated
	 */
	EAttribute getExternalDependency_NonStandardExtension();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.NamedConstant <em>Named Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Constant</em>'.
	 * @see geneauto.emf.models.gacodemodel.NamedConstant
	 * @generated
	 */
	EClass getNamedConstant();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.Function_CM <em>Function CM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function CM</em>'.
	 * @see geneauto.emf.models.gacodemodel.Function_CM
	 * @generated
	 */
	EClass getFunction_CM();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.Function_CM#getFunctionStyle <em>Function Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Function Style</em>'.
	 * @see geneauto.emf.models.gacodemodel.Function_CM#getFunctionStyle()
	 * @see #getFunction_CM()
	 * @generated
	 */
	EAttribute getFunction_CM_FunctionStyle();

	/**
	 * Returns the meta object for the reference list '{@link geneauto.emf.models.gacodemodel.Function_CM#getArguments <em>Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Arguments</em>'.
	 * @see geneauto.emf.models.gacodemodel.Function_CM#getArguments()
	 * @see #getFunction_CM()
	 * @generated
	 */
	EReference getFunction_CM_Arguments();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.Function_CM#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see geneauto.emf.models.gacodemodel.Function_CM#getBody()
	 * @see #getFunction_CM()
	 * @generated
	 */
	EReference getFunction_CM_Body();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.CodeModelFunction <em>Code Model Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Model Function</em>'.
	 * @see geneauto.emf.models.gacodemodel.CodeModelFunction
	 * @generated
	 */
	EClass getCodeModelFunction();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.SequentialFunctionBody <em>Sequential Function Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequential Function Body</em>'.
	 * @see geneauto.emf.models.gacodemodel.SequentialFunctionBody
	 * @generated
	 */
	EClass getSequentialFunctionBody();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.SequentialFunctionBody#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see geneauto.emf.models.gacodemodel.SequentialFunctionBody#getStatements()
	 * @see #getSequentialFunctionBody()
	 * @generated
	 */
	EReference getSequentialFunctionBody_Statements();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.LiteralContent <em>Literal Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal Content</em>'.
	 * @see geneauto.emf.models.gacodemodel.LiteralContent
	 * @generated
	 */
	EClass getLiteralContent();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.LiteralContent#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see geneauto.emf.models.gacodemodel.LiteralContent#getValue()
	 * @see #getLiteralContent()
	 * @generated
	 */
	EAttribute getLiteralContent_Value();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.StructureContent <em>Structure Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Structure Content</em>'.
	 * @see geneauto.emf.models.gacodemodel.StructureContent
	 * @generated
	 */
	EClass getStructureContent();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.StructureContent#getStructMembers <em>Struct Members</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Struct Members</em>'.
	 * @see geneauto.emf.models.gacodemodel.StructureContent#getStructMembers()
	 * @see #getStructureContent()
	 * @generated
	 */
	EReference getStructureContent_StructMembers();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.Module <em>Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Module</em>'.
	 * @see geneauto.emf.models.gacodemodel.Module
	 * @generated
	 */
	EClass getModule();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.Module#getHeaderFileName <em>Header File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header File Name</em>'.
	 * @see geneauto.emf.models.gacodemodel.Module#getHeaderFileName()
	 * @see #getModule()
	 * @generated
	 */
	EAttribute getModule_HeaderFileName();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.Module#getSourceFileName <em>Source File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source File Name</em>'.
	 * @see geneauto.emf.models.gacodemodel.Module#getSourceFileName()
	 * @see #getModule()
	 * @generated
	 */
	EAttribute getModule_SourceFileName();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.Module#getInitFunction <em>Init Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Init Function</em>'.
	 * @see geneauto.emf.models.gacodemodel.Module#getInitFunction()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_InitFunction();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.Module#getDependencies <em>Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dependencies</em>'.
	 * @see geneauto.emf.models.gacodemodel.Module#getDependencies()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_Dependencies();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.Module#getHeaderAnnotation <em>Header Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Header Annotation</em>'.
	 * @see geneauto.emf.models.gacodemodel.Module#getHeaderAnnotation()
	 * @see #getModule()
	 * @generated
	 */
	EReference getModule_HeaderAnnotation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GacodemodelFactory getGacodemodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl <em>GA Code Model Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getGACodeModelElement()
		 * @generated
		 */
		EClass GA_CODE_MODEL_ELEMENT = eINSTANCE.getGACodeModelElement();

		/**
		 * The meta object literal for the '<em><b>Source Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT = eINSTANCE.getGACodeModelElement_SourceElement();

		/**
		 * The meta object literal for the '<em><b>Source Element Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID = eINSTANCE.getGACodeModelElement_SourceElementId();

		/**
		 * The meta object literal for the '<em><b>Source Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_CODE_MODEL_ELEMENT__SOURCE_ACTION = eINSTANCE.getGACodeModelElement_SourceAction();

		/**
		 * The meta object literal for the '<em><b>Module</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_CODE_MODEL_ELEMENT__MODULE = eINSTANCE.getGACodeModelElement_Module();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.CustomTypeContentImpl <em>Custom Type Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.CustomTypeContentImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCustomTypeContent()
		 * @generated
		 */
		EClass CUSTOM_TYPE_CONTENT = eINSTANCE.getCustomTypeContent();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.HasNameSpace <em>Has Name Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.HasNameSpace
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getHasNameSpace()
		 * @generated
		 */
		EClass HAS_NAME_SPACE = eINSTANCE.getHasNameSpace();

		/**
		 * The meta object literal for the '<em><b>Name Space</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAS_NAME_SPACE__NAME_SPACE = eINSTANCE.getHasNameSpace_NameSpace();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.NameSpaceImpl <em>Name Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.NameSpaceImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getNameSpace()
		 * @generated
		 */
		EClass NAME_SPACE = eINSTANCE.getNameSpace();

		/**
		 * The meta object literal for the '<em><b>Ns Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAME_SPACE__NS_ELEMENTS = eINSTANCE.getNameSpace_NsElements();

		/**
		 * The meta object literal for the '<em><b>Implicit Argument Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAME_SPACE__IMPLICIT_ARGUMENT_VALUES = eINSTANCE.getNameSpace_ImplicitArgumentValues();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.NameSpaceElement_CMImpl <em>Name Space Element CM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.NameSpaceElement_CMImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getNameSpaceElement_CM()
		 * @generated
		 */
		EClass NAME_SPACE_ELEMENT_CM = eINSTANCE.getNameSpaceElement_CM();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAME_SPACE_ELEMENT_CM__SCOPE = eINSTANCE.getNameSpaceElement_CM_Scope();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl <em>Variable CM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.Variable_CMImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getVariable_CM()
		 * @generated
		 */
		EClass VARIABLE_CM = eINSTANCE.getVariable_CM();

		/**
		 * The meta object literal for the '<em><b>Auto Init</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_CM__AUTO_INIT = eINSTANCE.getVariable_CM_AutoInit();

		/**
		 * The meta object literal for the '<em><b>Observation Points</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_CM__OBSERVATION_POINTS = eINSTANCE.getVariable_CM_ObservationPoints();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.CodeModelVariable <em>Code Model Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.CodeModelVariable
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCodeModelVariable()
		 * @generated
		 */
		EClass CODE_MODEL_VARIABLE = eINSTANCE.getCodeModelVariable();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.SequentialComposition <em>Sequential Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.SequentialComposition
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getSequentialComposition()
		 * @generated
		 */
		EClass SEQUENTIAL_COMPOSITION = eINSTANCE.getSequentialComposition();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.LabelImpl <em>Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.LabelImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getLabel()
		 * @generated
		 */
		EClass LABEL = eINSTANCE.getLabel();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.DependencyImpl <em>Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.DependencyImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getDependency()
		 * @generated
		 */
		EClass DEPENDENCY = eINSTANCE.getDependency();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.CustomType_CMImpl <em>Custom Type CM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.CustomType_CMImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCustomType_CM()
		 * @generated
		 */
		EClass CUSTOM_TYPE_CM = eINSTANCE.getCustomType_CM();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOM_TYPE_CM__CONTENT = eINSTANCE.getCustomType_CM_Content();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.GACodeModelImpl <em>GA Code Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.GACodeModelImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getGACodeModel()
		 * @generated
		 */
		EClass GA_CODE_MODEL = eINSTANCE.getGACodeModel();

		/**
		 * The meta object literal for the '<em><b>System Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_CODE_MODEL__SYSTEM_MODEL = eINSTANCE.getGACodeModel_SystemModel();

		/**
		 * The meta object literal for the '<em><b>Temp Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_CODE_MODEL__TEMP_MODEL = eINSTANCE.getGACodeModel_TempModel();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_CODE_MODEL__ELEMENTS = eINSTANCE.getGACodeModel_Elements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.TempModelImpl <em>Temp Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.TempModelImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getTempModel()
		 * @generated
		 */
		EClass TEMP_MODEL = eINSTANCE.getTempModel();

		/**
		 * The meta object literal for the '<em><b>State Variables Struct Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE = eINSTANCE.getTempModel_StateVariablesStructType();

		/**
		 * The meta object literal for the '<em><b>Temp Name Space</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMP_MODEL__TEMP_NAME_SPACE = eINSTANCE.getTempModel_TempNameSpace();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMP_MODEL__ELEMENTS = eINSTANCE.getTempModel_Elements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.GACodeModelRoot <em>GA Code Model Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.GACodeModelRoot
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getGACodeModelRoot()
		 * @generated
		 */
		EClass GA_CODE_MODEL_ROOT = eINSTANCE.getGACodeModelRoot();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.StructureMemberImpl <em>Structure Member</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.StructureMemberImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getStructureMember()
		 * @generated
		 */
		EClass STRUCTURE_MEMBER = eINSTANCE.getStructureMember();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.ArrayContentImpl <em>Array Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.ArrayContentImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getArrayContent()
		 * @generated
		 */
		EClass ARRAY_CONTENT = eINSTANCE.getArrayContent();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_CONTENT__RANGE = eINSTANCE.getArrayContent_Range();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_CONTENT__BASE_TYPE = eINSTANCE.getArrayContent_BaseType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.DerivedTypeImpl <em>Derived Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.DerivedTypeImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getDerivedType()
		 * @generated
		 */
		EClass DERIVED_TYPE = eINSTANCE.getDerivedType();

		/**
		 * The meta object literal for the '<em><b>Parent Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DERIVED_TYPE__PARENT_TYPE = eINSTANCE.getDerivedType_ParentType();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.ExpressionFunctionBodyImpl <em>Expression Function Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.ExpressionFunctionBodyImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getExpressionFunctionBody()
		 * @generated
		 */
		EClass EXPRESSION_FUNCTION_BODY = eINSTANCE.getExpressionFunctionBody();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION_FUNCTION_BODY__EXPRESSION = eINSTANCE.getExpressionFunctionBody_Expression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.FunctionBodyImpl <em>Function Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.FunctionBodyImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getFunctionBody()
		 * @generated
		 */
		EClass FUNCTION_BODY = eINSTANCE.getFunctionBody();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.FunctionArgument_CMImpl <em>Function Argument CM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.FunctionArgument_CMImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getFunctionArgument_CM()
		 * @generated
		 */
		EClass FUNCTION_ARGUMENT_CM = eINSTANCE.getFunctionArgument_CM();

		/**
		 * The meta object literal for the '<em><b>Is Implicit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_ARGUMENT_CM__IS_IMPLICIT = eINSTANCE.getFunctionArgument_CM_IsImplicit();

		/**
		 * The meta object literal for the '<em><b>Is Redundant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_ARGUMENT_CM__IS_REDUNDANT = eINSTANCE.getFunctionArgument_CM_IsRedundant();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_ARGUMENT_CM__DIRECTION = eINSTANCE.getFunctionArgument_CM_Direction();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.StructureVariableImpl <em>Structure Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.StructureVariableImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getStructureVariable()
		 * @generated
		 */
		EClass STRUCTURE_VARIABLE = eINSTANCE.getStructureVariable();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.CodeModelDependencyImpl <em>Code Model Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.CodeModelDependencyImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCodeModelDependency()
		 * @generated
		 */
		EClass CODE_MODEL_DEPENDENCY = eINSTANCE.getCodeModelDependency();

		/**
		 * The meta object literal for the '<em><b>Dependent Module</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_MODEL_DEPENDENCY__DEPENDENT_MODULE = eINSTANCE.getCodeModelDependency_DependentModule();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.ExternalDependencyImpl <em>External Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.ExternalDependencyImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getExternalDependency()
		 * @generated
		 */
		EClass EXTERNAL_DEPENDENCY = eINSTANCE.getExternalDependency();

		/**
		 * The meta object literal for the '<em><b>Is System</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_DEPENDENCY__IS_SYSTEM = eINSTANCE.getExternalDependency_IsSystem();

		/**
		 * The meta object literal for the '<em><b>Non Standard Extension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION = eINSTANCE.getExternalDependency_NonStandardExtension();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.NamedConstantImpl <em>Named Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.NamedConstantImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getNamedConstant()
		 * @generated
		 */
		EClass NAMED_CONSTANT = eINSTANCE.getNamedConstant();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.Function_CMImpl <em>Function CM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.Function_CMImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getFunction_CM()
		 * @generated
		 */
		EClass FUNCTION_CM = eINSTANCE.getFunction_CM();

		/**
		 * The meta object literal for the '<em><b>Function Style</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_CM__FUNCTION_STYLE = eINSTANCE.getFunction_CM_FunctionStyle();

		/**
		 * The meta object literal for the '<em><b>Arguments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_CM__ARGUMENTS = eINSTANCE.getFunction_CM_Arguments();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_CM__BODY = eINSTANCE.getFunction_CM_Body();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.CodeModelFunction <em>Code Model Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.CodeModelFunction
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getCodeModelFunction()
		 * @generated
		 */
		EClass CODE_MODEL_FUNCTION = eINSTANCE.getCodeModelFunction();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.SequentialFunctionBodyImpl <em>Sequential Function Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.SequentialFunctionBodyImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getSequentialFunctionBody()
		 * @generated
		 */
		EClass SEQUENTIAL_FUNCTION_BODY = eINSTANCE.getSequentialFunctionBody();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENTIAL_FUNCTION_BODY__STATEMENTS = eINSTANCE.getSequentialFunctionBody_Statements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.LiteralContentImpl <em>Literal Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.LiteralContentImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getLiteralContent()
		 * @generated
		 */
		EClass LITERAL_CONTENT = eINSTANCE.getLiteralContent();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LITERAL_CONTENT__VALUE = eINSTANCE.getLiteralContent_Value();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.StructureContentImpl <em>Structure Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.StructureContentImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getStructureContent()
		 * @generated
		 */
		EClass STRUCTURE_CONTENT = eINSTANCE.getStructureContent();

		/**
		 * The meta object literal for the '<em><b>Struct Members</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURE_CONTENT__STRUCT_MEMBERS = eINSTANCE.getStructureContent_StructMembers();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl <em>Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.impl.ModuleImpl
		 * @see geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl#getModule()
		 * @generated
		 */
		EClass MODULE = eINSTANCE.getModule();

		/**
		 * The meta object literal for the '<em><b>Header File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE__HEADER_FILE_NAME = eINSTANCE.getModule_HeaderFileName();

		/**
		 * The meta object literal for the '<em><b>Source File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE__SOURCE_FILE_NAME = eINSTANCE.getModule_SourceFileName();

		/**
		 * The meta object literal for the '<em><b>Init Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE__INIT_FUNCTION = eINSTANCE.getModule_InitFunction();

		/**
		 * The meta object literal for the '<em><b>Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE__DEPENDENCIES = eINSTANCE.getModule_Dependencies();

		/**
		 * The meta object literal for the '<em><b>Header Annotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE__HEADER_ANNOTATION = eINSTANCE.getModule_HeaderAnnotation();

	}

} //GacodemodelPackage
