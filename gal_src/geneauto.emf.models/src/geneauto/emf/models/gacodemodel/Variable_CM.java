/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable CM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Abstract entity representing a variable.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.Variable_CM#isAutoInit <em>Auto Init</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.Variable_CM#getObservationPoints <em>Observation Points</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getVariable_CM()
 * @model
 * @generated
 */
public interface Variable_CM extends NameSpaceElement_CM, CodeModelVariable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Auto Init</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Auto Init</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auto Init</em>' attribute.
	 * @see #isSetAutoInit()
	 * @see #unsetAutoInit()
	 * @see #setAutoInit(boolean)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getVariable_CM_AutoInit()
	 * @model default="true" unsettable="true"
	 * @generated
	 */
	boolean isAutoInit();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.Variable_CM#isAutoInit <em>Auto Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auto Init</em>' attribute.
	 * @see #isSetAutoInit()
	 * @see #unsetAutoInit()
	 * @see #isAutoInit()
	 * @generated
	 */
	void setAutoInit(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Variable_CM#isAutoInit <em>Auto Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAutoInit()
	 * @see #isAutoInit()
	 * @see #setAutoInit(boolean)
	 * @generated
	 */
	void unsetAutoInit();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Variable_CM#isAutoInit <em>Auto Init</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Auto Init</em>' attribute is set.
	 * @see #unsetAutoInit()
	 * @see #isAutoInit()
	 * @see #setAutoInit(boolean)
	 * @generated
	 */
	boolean isSetAutoInit();

	/**
	 * Returns the value of the '<em><b>Observation Points</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observation Points</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observation Points</em>' attribute list.
	 * @see #isSetObservationPoints()
	 * @see #unsetObservationPoints()
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getVariable_CM_ObservationPoints()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<String> getObservationPoints();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Variable_CM#getObservationPoints <em>Observation Points</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObservationPoints()
	 * @see #getObservationPoints()
	 * @generated
	 */
	void unsetObservationPoints();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Variable_CM#getObservationPoints <em>Observation Points</em>}' attribute list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Observation Points</em>' attribute list is set.
	 * @see #unsetObservationPoints()
	 * @see #getObservationPoints()
	 * @generated
	 */
	boolean isSetObservationPoints();

} // Variable_CM
