/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.gacodemodel.expression.RangeExpression;

import geneauto.emf.models.gadatatypes.GADataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Content</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents custom type (content) that is array with defined base type and range 
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.ArrayContent#getRange <em>Range</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.ArrayContent#getBaseType <em>Base Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getArrayContent()
 * @model
 * @generated
 */
public interface ArrayContent extends CustomTypeContent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Range</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' reference.
	 * @see #isSetRange()
	 * @see #unsetRange()
	 * @see #setRange(RangeExpression)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getArrayContent_Range()
	 * @model unsettable="true"
	 * @generated
	 */
	RangeExpression getRange();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.ArrayContent#getRange <em>Range</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' reference.
	 * @see #isSetRange()
	 * @see #unsetRange()
	 * @see #getRange()
	 * @generated
	 */
	void setRange(RangeExpression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.ArrayContent#getRange <em>Range</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRange()
	 * @see #getRange()
	 * @see #setRange(RangeExpression)
	 * @generated
	 */
	void unsetRange();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.ArrayContent#getRange <em>Range</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Range</em>' reference is set.
	 * @see #unsetRange()
	 * @see #getRange()
	 * @see #setRange(RangeExpression)
	 * @generated
	 */
	boolean isSetRange();

	/**
	 * Returns the value of the '<em><b>Base Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Type</em>' containment reference.
	 * @see #isSetBaseType()
	 * @see #unsetBaseType()
	 * @see #setBaseType(GADataType)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getArrayContent_BaseType()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	GADataType getBaseType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.ArrayContent#getBaseType <em>Base Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Type</em>' containment reference.
	 * @see #isSetBaseType()
	 * @see #unsetBaseType()
	 * @see #getBaseType()
	 * @generated
	 */
	void setBaseType(GADataType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.ArrayContent#getBaseType <em>Base Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBaseType()
	 * @see #getBaseType()
	 * @see #setBaseType(GADataType)
	 * @generated
	 */
	void unsetBaseType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.ArrayContent#getBaseType <em>Base Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Base Type</em>' containment reference is set.
	 * @see #unsetBaseType()
	 * @see #getBaseType()
	 * @see #setBaseType(GADataType)
	 * @generated
	 */
	boolean isSetBaseType();

} // ArrayContent
