/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.util;

import geneauto.emf.models.common.CustomType;
import geneauto.emf.models.common.Function;
import geneauto.emf.models.common.NameSpaceElement;
import geneauto.emf.models.common.Variable;

import geneauto.emf.models.gacodemodel.*;

import geneauto.emf.models.genericmodel.GAModelElement;
import geneauto.emf.models.genericmodel.Model;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage
 * @generated
 */
public class GacodemodelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GacodemodelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GacodemodelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = GacodemodelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GacodemodelSwitch<Adapter> modelSwitch =
		new GacodemodelSwitch<Adapter>() {
			@Override
			public Adapter caseGACodeModelElement(GACodeModelElement object) {
				return createGACodeModelElementAdapter();
			}
			@Override
			public Adapter caseCustomTypeContent(CustomTypeContent object) {
				return createCustomTypeContentAdapter();
			}
			@Override
			public Adapter caseHasNameSpace(HasNameSpace object) {
				return createHasNameSpaceAdapter();
			}
			@Override
			public Adapter caseNameSpace(NameSpace object) {
				return createNameSpaceAdapter();
			}
			@Override
			public Adapter caseNameSpaceElement_CM(NameSpaceElement_CM object) {
				return createNameSpaceElement_CMAdapter();
			}
			@Override
			public Adapter caseVariable_CM(Variable_CM object) {
				return createVariable_CMAdapter();
			}
			@Override
			public Adapter caseCodeModelVariable(CodeModelVariable object) {
				return createCodeModelVariableAdapter();
			}
			@Override
			public Adapter caseSequentialComposition(SequentialComposition object) {
				return createSequentialCompositionAdapter();
			}
			@Override
			public Adapter caseLabel(Label object) {
				return createLabelAdapter();
			}
			@Override
			public Adapter caseDependency(Dependency object) {
				return createDependencyAdapter();
			}
			@Override
			public Adapter caseCustomType_CM(CustomType_CM object) {
				return createCustomType_CMAdapter();
			}
			@Override
			public Adapter caseGACodeModel(GACodeModel object) {
				return createGACodeModelAdapter();
			}
			@Override
			public Adapter caseTempModel(TempModel object) {
				return createTempModelAdapter();
			}
			@Override
			public Adapter caseGACodeModelRoot(GACodeModelRoot object) {
				return createGACodeModelRootAdapter();
			}
			@Override
			public Adapter caseStructureMember(StructureMember object) {
				return createStructureMemberAdapter();
			}
			@Override
			public Adapter caseArrayContent(ArrayContent object) {
				return createArrayContentAdapter();
			}
			@Override
			public Adapter caseDerivedType(DerivedType object) {
				return createDerivedTypeAdapter();
			}
			@Override
			public Adapter caseExpressionFunctionBody(ExpressionFunctionBody object) {
				return createExpressionFunctionBodyAdapter();
			}
			@Override
			public Adapter caseFunctionBody(FunctionBody object) {
				return createFunctionBodyAdapter();
			}
			@Override
			public Adapter caseFunctionArgument_CM(FunctionArgument_CM object) {
				return createFunctionArgument_CMAdapter();
			}
			@Override
			public Adapter caseStructureVariable(StructureVariable object) {
				return createStructureVariableAdapter();
			}
			@Override
			public Adapter caseCodeModelDependency(CodeModelDependency object) {
				return createCodeModelDependencyAdapter();
			}
			@Override
			public Adapter caseExternalDependency(ExternalDependency object) {
				return createExternalDependencyAdapter();
			}
			@Override
			public Adapter caseNamedConstant(NamedConstant object) {
				return createNamedConstantAdapter();
			}
			@Override
			public Adapter caseFunction_CM(Function_CM object) {
				return createFunction_CMAdapter();
			}
			@Override
			public Adapter caseCodeModelFunction(CodeModelFunction object) {
				return createCodeModelFunctionAdapter();
			}
			@Override
			public Adapter caseSequentialFunctionBody(SequentialFunctionBody object) {
				return createSequentialFunctionBodyAdapter();
			}
			@Override
			public Adapter caseLiteralContent(LiteralContent object) {
				return createLiteralContentAdapter();
			}
			@Override
			public Adapter caseStructureContent(StructureContent object) {
				return createStructureContentAdapter();
			}
			@Override
			public Adapter caseModule(Module object) {
				return createModuleAdapter();
			}
			@Override
			public Adapter caseGAModelElement(GAModelElement object) {
				return createGAModelElementAdapter();
			}
			@Override
			public Adapter caseNameSpaceElement(NameSpaceElement object) {
				return createNameSpaceElementAdapter();
			}
			@Override
			public Adapter caseVariable(Variable object) {
				return createVariableAdapter();
			}
			@Override
			public Adapter caseCustomType(CustomType object) {
				return createCustomTypeAdapter();
			}
			@Override
			public Adapter caseModel(Model object) {
				return createModelAdapter();
			}
			@Override
			public Adapter caseFunction(Function object) {
				return createFunctionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.GACodeModelElement <em>GA Code Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement
	 * @generated
	 */
	public Adapter createGACodeModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.CustomTypeContent <em>Custom Type Content</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.CustomTypeContent
	 * @generated
	 */
	public Adapter createCustomTypeContentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.HasNameSpace <em>Has Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.HasNameSpace
	 * @generated
	 */
	public Adapter createHasNameSpaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.NameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.NameSpace
	 * @generated
	 */
	public Adapter createNameSpaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.NameSpaceElement_CM <em>Name Space Element CM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.NameSpaceElement_CM
	 * @generated
	 */
	public Adapter createNameSpaceElement_CMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.Variable_CM <em>Variable CM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.Variable_CM
	 * @generated
	 */
	public Adapter createVariable_CMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.CodeModelVariable <em>Code Model Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.CodeModelVariable
	 * @generated
	 */
	public Adapter createCodeModelVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.SequentialComposition <em>Sequential Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.SequentialComposition
	 * @generated
	 */
	public Adapter createSequentialCompositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.Label
	 * @generated
	 */
	public Adapter createLabelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.Dependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.Dependency
	 * @generated
	 */
	public Adapter createDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.CustomType_CM <em>Custom Type CM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.CustomType_CM
	 * @generated
	 */
	public Adapter createCustomType_CMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.GACodeModel <em>GA Code Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.GACodeModel
	 * @generated
	 */
	public Adapter createGACodeModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.TempModel <em>Temp Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.TempModel
	 * @generated
	 */
	public Adapter createTempModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.GACodeModelRoot <em>GA Code Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelRoot
	 * @generated
	 */
	public Adapter createGACodeModelRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.StructureMember <em>Structure Member</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.StructureMember
	 * @generated
	 */
	public Adapter createStructureMemberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.ArrayContent <em>Array Content</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.ArrayContent
	 * @generated
	 */
	public Adapter createArrayContentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.DerivedType <em>Derived Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.DerivedType
	 * @generated
	 */
	public Adapter createDerivedTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.ExpressionFunctionBody <em>Expression Function Body</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.ExpressionFunctionBody
	 * @generated
	 */
	public Adapter createExpressionFunctionBodyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.FunctionBody <em>Function Body</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.FunctionBody
	 * @generated
	 */
	public Adapter createFunctionBodyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM <em>Function Argument CM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.FunctionArgument_CM
	 * @generated
	 */
	public Adapter createFunctionArgument_CMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.StructureVariable <em>Structure Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.StructureVariable
	 * @generated
	 */
	public Adapter createStructureVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.CodeModelDependency <em>Code Model Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.CodeModelDependency
	 * @generated
	 */
	public Adapter createCodeModelDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.ExternalDependency <em>External Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.ExternalDependency
	 * @generated
	 */
	public Adapter createExternalDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.NamedConstant <em>Named Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.NamedConstant
	 * @generated
	 */
	public Adapter createNamedConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.Function_CM <em>Function CM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.Function_CM
	 * @generated
	 */
	public Adapter createFunction_CMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.CodeModelFunction <em>Code Model Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.CodeModelFunction
	 * @generated
	 */
	public Adapter createCodeModelFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.SequentialFunctionBody <em>Sequential Function Body</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.SequentialFunctionBody
	 * @generated
	 */
	public Adapter createSequentialFunctionBodyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.LiteralContent <em>Literal Content</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.LiteralContent
	 * @generated
	 */
	public Adapter createLiteralContentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.StructureContent <em>Structure Content</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.StructureContent
	 * @generated
	 */
	public Adapter createStructureContentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.Module <em>Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.Module
	 * @generated
	 */
	public Adapter createModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.genericmodel.GAModelElement <em>GA Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.genericmodel.GAModelElement
	 * @generated
	 */
	public Adapter createGAModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.common.NameSpaceElement <em>Name Space Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.common.NameSpaceElement
	 * @generated
	 */
	public Adapter createNameSpaceElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.common.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.common.Variable
	 * @generated
	 */
	public Adapter createVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.common.CustomType <em>Custom Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.common.CustomType
	 * @generated
	 */
	public Adapter createCustomTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.genericmodel.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.genericmodel.Model
	 * @generated
	 */
	public Adapter createModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.common.Function <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.common.Function
	 * @generated
	 */
	public Adapter createFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //GacodemodelAdapterFactory
