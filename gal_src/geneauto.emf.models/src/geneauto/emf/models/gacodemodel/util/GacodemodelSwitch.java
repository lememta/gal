/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.util;

import geneauto.emf.models.common.CustomType;
import geneauto.emf.models.common.Function;
import geneauto.emf.models.common.NameSpaceElement;
import geneauto.emf.models.common.Variable;

import geneauto.emf.models.gacodemodel.*;

import geneauto.emf.models.genericmodel.GAModelElement;
import geneauto.emf.models.genericmodel.Model;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage
 * @generated
 */
public class GacodemodelSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GacodemodelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GacodemodelSwitch() {
		if (modelPackage == null) {
			modelPackage = GacodemodelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT: {
				GACodeModelElement gaCodeModelElement = (GACodeModelElement)theEObject;
				T result = caseGACodeModelElement(gaCodeModelElement);
				if (result == null) result = caseGAModelElement(gaCodeModelElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.CUSTOM_TYPE_CONTENT: {
				CustomTypeContent customTypeContent = (CustomTypeContent)theEObject;
				T result = caseCustomTypeContent(customTypeContent);
				if (result == null) result = caseGACodeModelElement(customTypeContent);
				if (result == null) result = caseGAModelElement(customTypeContent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.HAS_NAME_SPACE: {
				HasNameSpace hasNameSpace = (HasNameSpace)theEObject;
				T result = caseHasNameSpace(hasNameSpace);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.NAME_SPACE: {
				NameSpace nameSpace = (NameSpace)theEObject;
				T result = caseNameSpace(nameSpace);
				if (result == null) result = caseGACodeModelElement(nameSpace);
				if (result == null) result = caseGAModelElement(nameSpace);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.NAME_SPACE_ELEMENT_CM: {
				NameSpaceElement_CM nameSpaceElement_CM = (NameSpaceElement_CM)theEObject;
				T result = caseNameSpaceElement_CM(nameSpaceElement_CM);
				if (result == null) result = caseGACodeModelElement(nameSpaceElement_CM);
				if (result == null) result = caseGAModelElement(nameSpaceElement_CM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.VARIABLE_CM: {
				Variable_CM variable_CM = (Variable_CM)theEObject;
				T result = caseVariable_CM(variable_CM);
				if (result == null) result = caseNameSpaceElement_CM(variable_CM);
				if (result == null) result = caseCodeModelVariable(variable_CM);
				if (result == null) result = caseGACodeModelElement(variable_CM);
				if (result == null) result = caseVariable(variable_CM);
				if (result == null) result = caseGAModelElement(variable_CM);
				if (result == null) result = caseNameSpaceElement(variable_CM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.CODE_MODEL_VARIABLE: {
				CodeModelVariable codeModelVariable = (CodeModelVariable)theEObject;
				T result = caseCodeModelVariable(codeModelVariable);
				if (result == null) result = caseVariable(codeModelVariable);
				if (result == null) result = caseNameSpaceElement(codeModelVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.SEQUENTIAL_COMPOSITION: {
				SequentialComposition sequentialComposition = (SequentialComposition)theEObject;
				T result = caseSequentialComposition(sequentialComposition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.LABEL: {
				Label label = (Label)theEObject;
				T result = caseLabel(label);
				if (result == null) result = caseGACodeModelElement(label);
				if (result == null) result = caseGAModelElement(label);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.DEPENDENCY: {
				Dependency dependency = (Dependency)theEObject;
				T result = caseDependency(dependency);
				if (result == null) result = caseGACodeModelElement(dependency);
				if (result == null) result = caseGAModelElement(dependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.CUSTOM_TYPE_CM: {
				CustomType_CM customType_CM = (CustomType_CM)theEObject;
				T result = caseCustomType_CM(customType_CM);
				if (result == null) result = caseNameSpaceElement_CM(customType_CM);
				if (result == null) result = caseCustomType(customType_CM);
				if (result == null) result = caseGACodeModelElement(customType_CM);
				if (result == null) result = caseNameSpaceElement(customType_CM);
				if (result == null) result = caseGAModelElement(customType_CM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.GA_CODE_MODEL: {
				GACodeModel gaCodeModel = (GACodeModel)theEObject;
				T result = caseGACodeModel(gaCodeModel);
				if (result == null) result = caseModel(gaCodeModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.TEMP_MODEL: {
				TempModel tempModel = (TempModel)theEObject;
				T result = caseTempModel(tempModel);
				if (result == null) result = caseGACodeModelElement(tempModel);
				if (result == null) result = caseGACodeModelRoot(tempModel);
				if (result == null) result = caseGAModelElement(tempModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.GA_CODE_MODEL_ROOT: {
				GACodeModelRoot gaCodeModelRoot = (GACodeModelRoot)theEObject;
				T result = caseGACodeModelRoot(gaCodeModelRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.STRUCTURE_MEMBER: {
				StructureMember structureMember = (StructureMember)theEObject;
				T result = caseStructureMember(structureMember);
				if (result == null) result = caseVariable_CM(structureMember);
				if (result == null) result = caseNameSpaceElement_CM(structureMember);
				if (result == null) result = caseCodeModelVariable(structureMember);
				if (result == null) result = caseGACodeModelElement(structureMember);
				if (result == null) result = caseVariable(structureMember);
				if (result == null) result = caseGAModelElement(structureMember);
				if (result == null) result = caseNameSpaceElement(structureMember);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.ARRAY_CONTENT: {
				ArrayContent arrayContent = (ArrayContent)theEObject;
				T result = caseArrayContent(arrayContent);
				if (result == null) result = caseCustomTypeContent(arrayContent);
				if (result == null) result = caseGACodeModelElement(arrayContent);
				if (result == null) result = caseGAModelElement(arrayContent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.DERIVED_TYPE: {
				DerivedType derivedType = (DerivedType)theEObject;
				T result = caseDerivedType(derivedType);
				if (result == null) result = caseCustomType_CM(derivedType);
				if (result == null) result = caseNameSpaceElement_CM(derivedType);
				if (result == null) result = caseCustomType(derivedType);
				if (result == null) result = caseGACodeModelElement(derivedType);
				if (result == null) result = caseNameSpaceElement(derivedType);
				if (result == null) result = caseGAModelElement(derivedType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.EXPRESSION_FUNCTION_BODY: {
				ExpressionFunctionBody expressionFunctionBody = (ExpressionFunctionBody)theEObject;
				T result = caseExpressionFunctionBody(expressionFunctionBody);
				if (result == null) result = caseFunctionBody(expressionFunctionBody);
				if (result == null) result = caseGACodeModelElement(expressionFunctionBody);
				if (result == null) result = caseGAModelElement(expressionFunctionBody);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.FUNCTION_BODY: {
				FunctionBody functionBody = (FunctionBody)theEObject;
				T result = caseFunctionBody(functionBody);
				if (result == null) result = caseGACodeModelElement(functionBody);
				if (result == null) result = caseGAModelElement(functionBody);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.FUNCTION_ARGUMENT_CM: {
				FunctionArgument_CM functionArgument_CM = (FunctionArgument_CM)theEObject;
				T result = caseFunctionArgument_CM(functionArgument_CM);
				if (result == null) result = caseVariable_CM(functionArgument_CM);
				if (result == null) result = caseNameSpaceElement_CM(functionArgument_CM);
				if (result == null) result = caseCodeModelVariable(functionArgument_CM);
				if (result == null) result = caseGACodeModelElement(functionArgument_CM);
				if (result == null) result = caseVariable(functionArgument_CM);
				if (result == null) result = caseGAModelElement(functionArgument_CM);
				if (result == null) result = caseNameSpaceElement(functionArgument_CM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.STRUCTURE_VARIABLE: {
				StructureVariable structureVariable = (StructureVariable)theEObject;
				T result = caseStructureVariable(structureVariable);
				if (result == null) result = caseVariable_CM(structureVariable);
				if (result == null) result = caseNameSpaceElement_CM(structureVariable);
				if (result == null) result = caseCodeModelVariable(structureVariable);
				if (result == null) result = caseGACodeModelElement(structureVariable);
				if (result == null) result = caseVariable(structureVariable);
				if (result == null) result = caseGAModelElement(structureVariable);
				if (result == null) result = caseNameSpaceElement(structureVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.CODE_MODEL_DEPENDENCY: {
				CodeModelDependency codeModelDependency = (CodeModelDependency)theEObject;
				T result = caseCodeModelDependency(codeModelDependency);
				if (result == null) result = caseDependency(codeModelDependency);
				if (result == null) result = caseGACodeModelElement(codeModelDependency);
				if (result == null) result = caseGAModelElement(codeModelDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.EXTERNAL_DEPENDENCY: {
				ExternalDependency externalDependency = (ExternalDependency)theEObject;
				T result = caseExternalDependency(externalDependency);
				if (result == null) result = caseDependency(externalDependency);
				if (result == null) result = caseGACodeModelElement(externalDependency);
				if (result == null) result = caseGAModelElement(externalDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.NAMED_CONSTANT: {
				NamedConstant namedConstant = (NamedConstant)theEObject;
				T result = caseNamedConstant(namedConstant);
				if (result == null) result = caseVariable_CM(namedConstant);
				if (result == null) result = caseNameSpaceElement_CM(namedConstant);
				if (result == null) result = caseCodeModelVariable(namedConstant);
				if (result == null) result = caseGACodeModelElement(namedConstant);
				if (result == null) result = caseVariable(namedConstant);
				if (result == null) result = caseGAModelElement(namedConstant);
				if (result == null) result = caseNameSpaceElement(namedConstant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.FUNCTION_CM: {
				Function_CM function_CM = (Function_CM)theEObject;
				T result = caseFunction_CM(function_CM);
				if (result == null) result = caseNameSpaceElement_CM(function_CM);
				if (result == null) result = caseCodeModelFunction(function_CM);
				if (result == null) result = caseHasNameSpace(function_CM);
				if (result == null) result = caseGACodeModelElement(function_CM);
				if (result == null) result = caseFunction(function_CM);
				if (result == null) result = caseGAModelElement(function_CM);
				if (result == null) result = caseNameSpaceElement(function_CM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.CODE_MODEL_FUNCTION: {
				CodeModelFunction codeModelFunction = (CodeModelFunction)theEObject;
				T result = caseCodeModelFunction(codeModelFunction);
				if (result == null) result = caseFunction(codeModelFunction);
				if (result == null) result = caseNameSpaceElement(codeModelFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY: {
				SequentialFunctionBody sequentialFunctionBody = (SequentialFunctionBody)theEObject;
				T result = caseSequentialFunctionBody(sequentialFunctionBody);
				if (result == null) result = caseFunctionBody(sequentialFunctionBody);
				if (result == null) result = caseHasNameSpace(sequentialFunctionBody);
				if (result == null) result = caseSequentialComposition(sequentialFunctionBody);
				if (result == null) result = caseGACodeModelElement(sequentialFunctionBody);
				if (result == null) result = caseGAModelElement(sequentialFunctionBody);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.LITERAL_CONTENT: {
				LiteralContent literalContent = (LiteralContent)theEObject;
				T result = caseLiteralContent(literalContent);
				if (result == null) result = caseCustomTypeContent(literalContent);
				if (result == null) result = caseGACodeModelElement(literalContent);
				if (result == null) result = caseGAModelElement(literalContent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.STRUCTURE_CONTENT: {
				StructureContent structureContent = (StructureContent)theEObject;
				T result = caseStructureContent(structureContent);
				if (result == null) result = caseCustomTypeContent(structureContent);
				if (result == null) result = caseGACodeModelElement(structureContent);
				if (result == null) result = caseGAModelElement(structureContent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GacodemodelPackage.MODULE: {
				Module module = (Module)theEObject;
				T result = caseModule(module);
				if (result == null) result = caseGACodeModelElement(module);
				if (result == null) result = caseGACodeModelRoot(module);
				if (result == null) result = caseHasNameSpace(module);
				if (result == null) result = caseGAModelElement(module);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Code Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Code Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGACodeModelElement(GACodeModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Type Content</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Type Content</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomTypeContent(CustomTypeContent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Name Space</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Name Space</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNameSpace(HasNameSpace object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Space</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Space</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameSpace(NameSpace object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Space Element CM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Space Element CM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameSpaceElement_CM(NameSpaceElement_CM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable CM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable CM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable_CM(Variable_CM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Model Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Model Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeModelVariable(CodeModelVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Composition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Composition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialComposition(SequentialComposition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Label</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Label</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLabel(Label object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDependency(Dependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Type CM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Type CM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomType_CM(CustomType_CM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Code Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Code Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGACodeModel(GACodeModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Temp Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Temp Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTempModel(TempModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Code Model Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Code Model Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGACodeModelRoot(GACodeModelRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Structure Member</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Structure Member</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStructureMember(StructureMember object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Content</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Content</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayContent(ArrayContent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Derived Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Derived Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDerivedType(DerivedType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Function Body</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Function Body</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionFunctionBody(ExpressionFunctionBody object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Body</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Body</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionBody(FunctionBody object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Argument CM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Argument CM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionArgument_CM(FunctionArgument_CM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Structure Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Structure Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStructureVariable(StructureVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Model Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Model Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeModelDependency(CodeModelDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalDependency(ExternalDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedConstant(NamedConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function CM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function CM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunction_CM(Function_CM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Model Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Model Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeModelFunction(CodeModelFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Function Body</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Function Body</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialFunctionBody(SequentialFunctionBody object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Literal Content</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Literal Content</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLiteralContent(LiteralContent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Structure Content</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Structure Content</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStructureContent(StructureContent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModule(Module object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GA Model Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGAModelElement(GAModelElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Space Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Space Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameSpaceElement(NameSpaceElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomType(CustomType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunction(Function object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //GacodemodelSwitch
