/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.gacodemodel.gaenumtypes.Direction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Argument CM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Abstract entity representing a function argument. Note: Each argument requires also an entry in the CodeComponent section of the NameSpace. Its scope should be Imported, so that it won't get unnecessarily declared. Constructors RequiredArg Bool CCPtr - Argument variable and a flag stating, whether it is optimisable or not. RedundantArg - A redundant argument that shall not be printed neither in function definition nor a function call. However, (abstract) function calls must still have all arguments. The redundant ones simply won't be printed.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isImplicit <em>Is Implicit</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isRedundant <em>Is Redundant</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#getDirection <em>Direction</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunctionArgument_CM()
 * @model
 * @generated
 */
public interface FunctionArgument_CM extends Variable_CM {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Is Implicit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Implicit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Implicit</em>' attribute.
	 * @see #isSetIsImplicit()
	 * @see #unsetIsImplicit()
	 * @see #setIsImplicit(boolean)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunctionArgument_CM_IsImplicit()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isImplicit();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isImplicit <em>Is Implicit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Implicit</em>' attribute.
	 * @see #isSetIsImplicit()
	 * @see #unsetIsImplicit()
	 * @see #isImplicit()
	 * @generated
	 */
	void setIsImplicit(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isImplicit <em>Is Implicit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsImplicit()
	 * @see #isImplicit()
	 * @see #setIsImplicit(boolean)
	 * @generated
	 */
	void unsetIsImplicit();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isImplicit <em>Is Implicit</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Implicit</em>' attribute is set.
	 * @see #unsetIsImplicit()
	 * @see #isImplicit()
	 * @see #setIsImplicit(boolean)
	 * @generated
	 */
	boolean isSetIsImplicit();

	/**
	 * Returns the value of the '<em><b>Is Redundant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Redundant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Redundant</em>' attribute.
	 * @see #isSetIsRedundant()
	 * @see #unsetIsRedundant()
	 * @see #setIsRedundant(boolean)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunctionArgument_CM_IsRedundant()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isRedundant();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isRedundant <em>Is Redundant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Redundant</em>' attribute.
	 * @see #isSetIsRedundant()
	 * @see #unsetIsRedundant()
	 * @see #isRedundant()
	 * @generated
	 */
	void setIsRedundant(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isRedundant <em>Is Redundant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsRedundant()
	 * @see #isRedundant()
	 * @see #setIsRedundant(boolean)
	 * @generated
	 */
	void unsetIsRedundant();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#isRedundant <em>Is Redundant</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Redundant</em>' attribute is set.
	 * @see #unsetIsRedundant()
	 * @see #isRedundant()
	 * @see #setIsRedundant(boolean)
	 * @generated
	 */
	boolean isSetIsRedundant();

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gacodemodel.gaenumtypes.Direction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.Direction
	 * @see #isSetDirection()
	 * @see #unsetDirection()
	 * @see #setDirection(Direction)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunctionArgument_CM_Direction()
	 * @model unsettable="true"
	 * @generated
	 */
	Direction getDirection();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.Direction
	 * @see #isSetDirection()
	 * @see #unsetDirection()
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(Direction value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDirection()
	 * @see #getDirection()
	 * @see #setDirection(Direction)
	 * @generated
	 */
	void unsetDirection();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.FunctionArgument_CM#getDirection <em>Direction</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Direction</em>' attribute is set.
	 * @see #unsetDirection()
	 * @see #getDirection()
	 * @see #setDirection(Direction)
	 * @generated
	 */
	boolean isSetDirection();

} // FunctionArgument_CM
