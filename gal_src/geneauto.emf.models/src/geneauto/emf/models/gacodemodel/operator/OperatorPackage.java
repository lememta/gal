/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.operator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.operator.OperatorFactory
 * @model kind="package"
 * @generated
 */
public interface OperatorPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operator";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gacodemodel/operator.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gacodemodel.operator";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperatorPackage eINSTANCE = geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.operator.Operator <em>Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.operator.Operator
	 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getOperator()
	 * @generated
	 */
	int OPERATOR = 0;

	/**
	 * The number of structural features of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.operator.BinaryOperator <em>Binary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.operator.BinaryOperator
	 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getBinaryOperator()
	 * @generated
	 */
	int BINARY_OPERATOR = 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.operator.OperatorAppearance <em>Appearance</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.operator.OperatorAppearance
	 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getOperatorAppearance()
	 * @generated
	 */
	int OPERATOR_APPEARANCE = 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.operator.TernaryOperator <em>Ternary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.operator.TernaryOperator
	 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getTernaryOperator()
	 * @generated
	 */
	int TERNARY_OPERATOR = 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.operator.UnaryOperator <em>Unary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.operator.UnaryOperator
	 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getUnaryOperator()
	 * @generated
	 */
	int UNARY_OPERATOR = 4;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.operator.AssignOperator <em>Assign Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.operator.AssignOperator
	 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getAssignOperator()
	 * @generated
	 */
	int ASSIGN_OPERATOR = 5;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.operator.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.operator.Operator
	 * @generated
	 */
	EClass getOperator();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.operator.BinaryOperator <em>Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Binary Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.operator.BinaryOperator
	 * @generated
	 */
	EEnum getBinaryOperator();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.operator.OperatorAppearance <em>Appearance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Appearance</em>'.
	 * @see geneauto.emf.models.gacodemodel.operator.OperatorAppearance
	 * @generated
	 */
	EEnum getOperatorAppearance();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.operator.TernaryOperator <em>Ternary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Ternary Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.operator.TernaryOperator
	 * @generated
	 */
	EEnum getTernaryOperator();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.operator.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Unary Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.operator.UnaryOperator
	 * @generated
	 */
	EEnum getUnaryOperator();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.operator.AssignOperator <em>Assign Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Assign Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.operator.AssignOperator
	 * @generated
	 */
	EEnum getAssignOperator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperatorFactory getOperatorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.operator.Operator <em>Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.operator.Operator
		 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getOperator()
		 * @generated
		 */
		EClass OPERATOR = eINSTANCE.getOperator();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.operator.BinaryOperator <em>Binary Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.operator.BinaryOperator
		 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getBinaryOperator()
		 * @generated
		 */
		EEnum BINARY_OPERATOR = eINSTANCE.getBinaryOperator();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.operator.OperatorAppearance <em>Appearance</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.operator.OperatorAppearance
		 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getOperatorAppearance()
		 * @generated
		 */
		EEnum OPERATOR_APPEARANCE = eINSTANCE.getOperatorAppearance();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.operator.TernaryOperator <em>Ternary Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.operator.TernaryOperator
		 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getTernaryOperator()
		 * @generated
		 */
		EEnum TERNARY_OPERATOR = eINSTANCE.getTernaryOperator();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.operator.UnaryOperator <em>Unary Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.operator.UnaryOperator
		 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getUnaryOperator()
		 * @generated
		 */
		EEnum UNARY_OPERATOR = eINSTANCE.getUnaryOperator();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.operator.AssignOperator <em>Assign Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.operator.AssignOperator
		 * @see geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl#getAssignOperator()
		 * @generated
		 */
		EEnum ASSIGN_OPERATOR = eINSTANCE.getAssignOperator();

	}

} //OperatorPackage
