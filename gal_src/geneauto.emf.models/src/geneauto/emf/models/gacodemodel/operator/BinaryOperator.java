/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.operator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Binary Operator</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.operator.OperatorPackage#getBinaryOperator()
 * @model
 * @generated
 */
public enum BinaryOperator implements Enumerator {
	/**
	 * The '<em><b>ADD OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADD_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_OPERATOR(0, "ADD_OPERATOR", "ADD_OPERATOR"),

	/**
	 * The '<em><b>SUB OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUB_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	SUB_OPERATOR(1, "SUB_OPERATOR", "SUB_OPERATOR"),

	/**
	 * The '<em><b>MUL OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MUL_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	MUL_OPERATOR(2, "MUL_OPERATOR", "MUL_OPERATOR"),

	/**
	 * The '<em><b>DIV OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIV_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	DIV_OPERATOR(3, "DIV_OPERATOR", "DIV_OPERATOR"),

	/**
	 * The '<em><b>POWER OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #POWER_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	POWER_OPERATOR(4, "POWER_OPERATOR", "POWER_OPERATOR"),

	/**
	 * The '<em><b>MUL MAT OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MUL_MAT_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	MUL_MAT_OPERATOR(5, "MUL_MAT_OPERATOR", "MUL_MAT_OPERATOR"),

	/**
	 * The '<em><b>MOD OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOD_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	MOD_OPERATOR(6, "MOD_OPERATOR", "MOD_OPERATOR"),

	/**
	 * The '<em><b>BITWISE AND OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BITWISE_AND_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	BITWISE_AND_OPERATOR(7, "BITWISE_AND_OPERATOR", "BITWISE_AND_OPERATOR"),

	/**
	 * The '<em><b>BITWISE OR OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BITWISE_OR_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	BITWISE_OR_OPERATOR(8, "BITWISE_OR_OPERATOR", "BITWISE_OR_OPERATOR"),

	/**
	 * The '<em><b>BITWISE XOR OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BITWISE_XOR_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	BITWISE_XOR_OPERATOR(9, "BITWISE_XOR_OPERATOR", "BITWISE_XOR_OPERATOR"),

	/**
	 * The '<em><b>SHIFT LEFT OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHIFT_LEFT_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	SHIFT_LEFT_OPERATOR(10, "SHIFT_LEFT_OPERATOR", "SHIFT_LEFT_OPERATOR"),

	/**
	 * The '<em><b>SHIFT RIGHT OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHIFT_RIGHT_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	SHIFT_RIGHT_OPERATOR(11, "SHIFT_RIGHT_OPERATOR", "SHIFT_RIGHT_OPERATOR"),

	/**
	 * The '<em><b>EQ OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EQ_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	EQ_OPERATOR(12, "EQ_OPERATOR", "EQ_OPERATOR"),

	/**
	 * The '<em><b>GE OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GE_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	GE_OPERATOR(13, "GE_OPERATOR", "GE_OPERATOR"),

	/**
	 * The '<em><b>GT OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GT_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	GT_OPERATOR(14, "GT_OPERATOR", "GT_OPERATOR"),

	/**
	 * The '<em><b>LE OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LE_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	LE_OPERATOR(15, "LE_OPERATOR", "LE_OPERATOR"),

	/**
	 * The '<em><b>LT OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LT_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	LT_OPERATOR(16, "LT_OPERATOR", "LT_OPERATOR"),

	/**
	 * The '<em><b>NE OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NE_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	NE_OPERATOR(17, "NE_OPERATOR", "NE_OPERATOR"),

	/**
	 * The '<em><b>LOGICAL AND OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOGICAL_AND_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	LOGICAL_AND_OPERATOR(18, "LOGICAL_AND_OPERATOR", "LOGICAL_AND_OPERATOR"),

	/**
	 * The '<em><b>LOGICAL OR OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOGICAL_OR_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	LOGICAL_OR_OPERATOR(19, "LOGICAL_OR_OPERATOR", "LOGICAL_OR_OPERATOR");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>ADD OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ADD OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ADD_OPERATOR_VALUE = 0;

	/**
	 * The '<em><b>SUB OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SUB OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUB_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SUB_OPERATOR_VALUE = 1;

	/**
	 * The '<em><b>MUL OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MUL OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MUL_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MUL_OPERATOR_VALUE = 2;

	/**
	 * The '<em><b>DIV OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DIV OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIV_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DIV_OPERATOR_VALUE = 3;

	/**
	 * The '<em><b>POWER OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>POWER OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #POWER_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int POWER_OPERATOR_VALUE = 4;

	/**
	 * The '<em><b>MUL MAT OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MUL MAT OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MUL_MAT_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MUL_MAT_OPERATOR_VALUE = 5;

	/**
	 * The '<em><b>MOD OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MOD OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOD_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MOD_OPERATOR_VALUE = 6;

	/**
	 * The '<em><b>BITWISE AND OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BITWISE AND OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BITWISE_AND_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BITWISE_AND_OPERATOR_VALUE = 7;

	/**
	 * The '<em><b>BITWISE OR OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BITWISE OR OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BITWISE_OR_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BITWISE_OR_OPERATOR_VALUE = 8;

	/**
	 * The '<em><b>BITWISE XOR OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BITWISE XOR OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BITWISE_XOR_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BITWISE_XOR_OPERATOR_VALUE = 9;

	/**
	 * The '<em><b>SHIFT LEFT OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SHIFT LEFT OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHIFT_LEFT_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SHIFT_LEFT_OPERATOR_VALUE = 10;

	/**
	 * The '<em><b>SHIFT RIGHT OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SHIFT RIGHT OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHIFT_RIGHT_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SHIFT_RIGHT_OPERATOR_VALUE = 11;

	/**
	 * The '<em><b>EQ OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EQ OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EQ_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EQ_OPERATOR_VALUE = 12;

	/**
	 * The '<em><b>GE OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GE OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GE_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GE_OPERATOR_VALUE = 13;

	/**
	 * The '<em><b>GT OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GT OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GT_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GT_OPERATOR_VALUE = 14;

	/**
	 * The '<em><b>LE OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LE OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LE_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LE_OPERATOR_VALUE = 15;

	/**
	 * The '<em><b>LT OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LT OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LT_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LT_OPERATOR_VALUE = 16;

	/**
	 * The '<em><b>NE OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NE OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NE_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NE_OPERATOR_VALUE = 17;

	/**
	 * The '<em><b>LOGICAL AND OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOGICAL AND OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOGICAL_AND_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LOGICAL_AND_OPERATOR_VALUE = 18;

	/**
	 * The '<em><b>LOGICAL OR OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOGICAL OR OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOGICAL_OR_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LOGICAL_OR_OPERATOR_VALUE = 19;

	/**
	 * An array of all the '<em><b>Binary Operator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final BinaryOperator[] VALUES_ARRAY =
		new BinaryOperator[] {
			ADD_OPERATOR,
			SUB_OPERATOR,
			MUL_OPERATOR,
			DIV_OPERATOR,
			POWER_OPERATOR,
			MUL_MAT_OPERATOR,
			MOD_OPERATOR,
			BITWISE_AND_OPERATOR,
			BITWISE_OR_OPERATOR,
			BITWISE_XOR_OPERATOR,
			SHIFT_LEFT_OPERATOR,
			SHIFT_RIGHT_OPERATOR,
			EQ_OPERATOR,
			GE_OPERATOR,
			GT_OPERATOR,
			LE_OPERATOR,
			LT_OPERATOR,
			NE_OPERATOR,
			LOGICAL_AND_OPERATOR,
			LOGICAL_OR_OPERATOR,
		};

	/**
	 * A public read-only list of all the '<em><b>Binary Operator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<BinaryOperator> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Binary Operator</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BinaryOperator get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BinaryOperator result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Binary Operator</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BinaryOperator getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BinaryOperator result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Binary Operator</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BinaryOperator get(int value) {
		switch (value) {
			case ADD_OPERATOR_VALUE: return ADD_OPERATOR;
			case SUB_OPERATOR_VALUE: return SUB_OPERATOR;
			case MUL_OPERATOR_VALUE: return MUL_OPERATOR;
			case DIV_OPERATOR_VALUE: return DIV_OPERATOR;
			case POWER_OPERATOR_VALUE: return POWER_OPERATOR;
			case MUL_MAT_OPERATOR_VALUE: return MUL_MAT_OPERATOR;
			case MOD_OPERATOR_VALUE: return MOD_OPERATOR;
			case BITWISE_AND_OPERATOR_VALUE: return BITWISE_AND_OPERATOR;
			case BITWISE_OR_OPERATOR_VALUE: return BITWISE_OR_OPERATOR;
			case BITWISE_XOR_OPERATOR_VALUE: return BITWISE_XOR_OPERATOR;
			case SHIFT_LEFT_OPERATOR_VALUE: return SHIFT_LEFT_OPERATOR;
			case SHIFT_RIGHT_OPERATOR_VALUE: return SHIFT_RIGHT_OPERATOR;
			case EQ_OPERATOR_VALUE: return EQ_OPERATOR;
			case GE_OPERATOR_VALUE: return GE_OPERATOR;
			case GT_OPERATOR_VALUE: return GT_OPERATOR;
			case LE_OPERATOR_VALUE: return LE_OPERATOR;
			case LT_OPERATOR_VALUE: return LT_OPERATOR;
			case NE_OPERATOR_VALUE: return NE_OPERATOR;
			case LOGICAL_AND_OPERATOR_VALUE: return LOGICAL_AND_OPERATOR;
			case LOGICAL_OR_OPERATOR_VALUE: return LOGICAL_OR_OPERATOR;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private BinaryOperator(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //BinaryOperator
