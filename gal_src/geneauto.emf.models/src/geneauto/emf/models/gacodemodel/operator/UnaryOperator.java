/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.operator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Unary Operator</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.operator.OperatorPackage#getUnaryOperator()
 * @model
 * @generated
 */
public enum UnaryOperator implements Enumerator {
	/**
	 * The '<em><b>DEREF OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEREF_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	DEREF_OPERATOR(0, "DEREF_OPERATOR", "DEREF_OPERATOR"),

	/**
	 * The '<em><b>REF OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REF_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	REF_OPERATOR(1, "REF_OPERATOR", "REF_OPERATOR"),

	/**
	 * The '<em><b>NOT OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_OPERATOR(2, "NOT_OPERATOR", "NOT_OPERATOR"),

	/**
	 * The '<em><b>COMPLEMENT OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPLEMENT_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	COMPLEMENT_OPERATOR(3, "COMPLEMENT_OPERATOR", "COMPLEMENT_OPERATOR"),

	/**
	 * The '<em><b>CAST OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAST_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	CAST_OPERATOR(4, "CAST_OPERATOR", "CAST_OPERATOR"),

	/**
	 * The '<em><b>UNARY PLUS OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNARY_PLUS_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	UNARY_PLUS_OPERATOR(5, "UNARY_PLUS_OPERATOR", "UNARY_PLUS_OPERATOR"),

	/**
	 * The '<em><b>UNARY MINUS OPERATOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNARY_MINUS_OPERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	UNARY_MINUS_OPERATOR(6, "UNARY_MINUS_OPERATOR", "UNARY_MINUS_OPERATOR"),

	/**
	 * The '<em><b>Precedence</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRECEDENCE_VALUE
	 * @generated
	 * @ordered
	 */
	PRECEDENCE(7, "precedence", "precedence"),

	/**
	 * The '<em><b>Is Prefix</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PREFIX_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PREFIX(8, "isPrefix", "isPrefix");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>DEREF OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DEREF OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DEREF_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DEREF_OPERATOR_VALUE = 0;

	/**
	 * The '<em><b>REF OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>REF OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REF_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REF_OPERATOR_VALUE = 1;

	/**
	 * The '<em><b>NOT OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NOT OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOT_OPERATOR_VALUE = 2;

	/**
	 * The '<em><b>COMPLEMENT OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>COMPLEMENT OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPLEMENT_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int COMPLEMENT_OPERATOR_VALUE = 3;

	/**
	 * The '<em><b>CAST OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CAST OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CAST_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAST_OPERATOR_VALUE = 4;

	/**
	 * The '<em><b>UNARY PLUS OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNARY PLUS OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNARY_PLUS_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNARY_PLUS_OPERATOR_VALUE = 5;

	/**
	 * The '<em><b>UNARY MINUS OPERATOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNARY MINUS OPERATOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNARY_MINUS_OPERATOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNARY_MINUS_OPERATOR_VALUE = 6;

	/**
	 * The '<em><b>Precedence</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Precedence</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRECEDENCE
	 * @model name="precedence"
	 * @generated
	 * @ordered
	 */
	public static final int PRECEDENCE_VALUE = 7;

	/**
	 * The '<em><b>Is Prefix</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Prefix</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PREFIX
	 * @model name="isPrefix"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PREFIX_VALUE = 8;

	/**
	 * An array of all the '<em><b>Unary Operator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final UnaryOperator[] VALUES_ARRAY =
		new UnaryOperator[] {
			DEREF_OPERATOR,
			REF_OPERATOR,
			NOT_OPERATOR,
			COMPLEMENT_OPERATOR,
			CAST_OPERATOR,
			UNARY_PLUS_OPERATOR,
			UNARY_MINUS_OPERATOR,
			PRECEDENCE,
			IS_PREFIX,
		};

	/**
	 * A public read-only list of all the '<em><b>Unary Operator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<UnaryOperator> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Unary Operator</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UnaryOperator get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			UnaryOperator result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Unary Operator</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UnaryOperator getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			UnaryOperator result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Unary Operator</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UnaryOperator get(int value) {
		switch (value) {
			case DEREF_OPERATOR_VALUE: return DEREF_OPERATOR;
			case REF_OPERATOR_VALUE: return REF_OPERATOR;
			case NOT_OPERATOR_VALUE: return NOT_OPERATOR;
			case COMPLEMENT_OPERATOR_VALUE: return COMPLEMENT_OPERATOR;
			case CAST_OPERATOR_VALUE: return CAST_OPERATOR;
			case UNARY_PLUS_OPERATOR_VALUE: return UNARY_PLUS_OPERATOR;
			case UNARY_MINUS_OPERATOR_VALUE: return UNARY_MINUS_OPERATOR;
			case PRECEDENCE_VALUE: return PRECEDENCE;
			case IS_PREFIX_VALUE: return IS_PREFIX;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private UnaryOperator(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //UnaryOperator
