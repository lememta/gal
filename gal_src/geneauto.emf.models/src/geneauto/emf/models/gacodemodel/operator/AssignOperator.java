/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.operator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Assign Operator</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.operator.OperatorPackage#getAssignOperator()
 * @model
 * @generated
 */
public enum AssignOperator implements Enumerator {
	/**
	 * The '<em><b>SIMPLE ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIMPLE_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	SIMPLE_ASSIGN(0, "SIMPLE_ASSIGN", "SIMPLE_ASSIGN"),

	/**
	 * The '<em><b>ADD ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADD_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_ASSIGN(1, "ADD_ASSIGN", "ADD_ASSIGN"),

	/**
	 * The '<em><b>SUB ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUB_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	SUB_ASSIGN(2, "SUB_ASSIGN", "SUB_ASSIGN"),

	/**
	 * The '<em><b>MUL ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MUL_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	MUL_ASSIGN(3, "MUL_ASSIGN", "MUL_ASSIGN"),

	/**
	 * The '<em><b>DIV ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIV_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	DIV_ASSIGN(4, "DIV_ASSIGN", "DIV_ASSIGN"),

	/**
	 * The '<em><b>AND ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AND_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	AND_ASSIGN(5, "AND_ASSIGN", "AND_ASSIGN"),

	/**
	 * The '<em><b>OR ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OR_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	OR_ASSIGN(6, "OR_ASSIGN", "OR_ASSIGN"),

	/**
	 * The '<em><b>XOR ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XOR_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	XOR_ASSIGN(7, "XOR_ASSIGN", "XOR_ASSIGN"),

	/**
	 * The '<em><b>INIT ASSIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INIT_ASSIGN_VALUE
	 * @generated
	 * @ordered
	 */
	INIT_ASSIGN(8, "INIT_ASSIGN", "INIT_ASSIGN"),

	/**
	 * The '<em><b>Precedence</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRECEDENCE_VALUE
	 * @generated
	 * @ordered
	 */
	PRECEDENCE(9, "precedence", "precedence");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>SIMPLE ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIMPLE ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIMPLE_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIMPLE_ASSIGN_VALUE = 0;

	/**
	 * The '<em><b>ADD ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ADD ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ADD_ASSIGN_VALUE = 1;

	/**
	 * The '<em><b>SUB ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SUB ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUB_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SUB_ASSIGN_VALUE = 2;

	/**
	 * The '<em><b>MUL ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MUL ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MUL_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MUL_ASSIGN_VALUE = 3;

	/**
	 * The '<em><b>DIV ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DIV ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIV_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DIV_ASSIGN_VALUE = 4;

	/**
	 * The '<em><b>AND ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AND ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AND_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AND_ASSIGN_VALUE = 5;

	/**
	 * The '<em><b>OR ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OR ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OR_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OR_ASSIGN_VALUE = 6;

	/**
	 * The '<em><b>XOR ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>XOR ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XOR_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int XOR_ASSIGN_VALUE = 7;

	/**
	 * The '<em><b>INIT ASSIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INIT ASSIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INIT_ASSIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INIT_ASSIGN_VALUE = 8;

	/**
	 * The '<em><b>Precedence</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Precedence</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRECEDENCE
	 * @model name="precedence"
	 * @generated
	 * @ordered
	 */
	public static final int PRECEDENCE_VALUE = 9;

	/**
	 * An array of all the '<em><b>Assign Operator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AssignOperator[] VALUES_ARRAY =
		new AssignOperator[] {
			SIMPLE_ASSIGN,
			ADD_ASSIGN,
			SUB_ASSIGN,
			MUL_ASSIGN,
			DIV_ASSIGN,
			AND_ASSIGN,
			OR_ASSIGN,
			XOR_ASSIGN,
			INIT_ASSIGN,
			PRECEDENCE,
		};

	/**
	 * A public read-only list of all the '<em><b>Assign Operator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AssignOperator> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Assign Operator</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AssignOperator get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AssignOperator result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Assign Operator</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AssignOperator getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AssignOperator result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Assign Operator</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AssignOperator get(int value) {
		switch (value) {
			case SIMPLE_ASSIGN_VALUE: return SIMPLE_ASSIGN;
			case ADD_ASSIGN_VALUE: return ADD_ASSIGN;
			case SUB_ASSIGN_VALUE: return SUB_ASSIGN;
			case MUL_ASSIGN_VALUE: return MUL_ASSIGN;
			case DIV_ASSIGN_VALUE: return DIV_ASSIGN;
			case AND_ASSIGN_VALUE: return AND_ASSIGN;
			case OR_ASSIGN_VALUE: return OR_ASSIGN;
			case XOR_ASSIGN_VALUE: return XOR_ASSIGN;
			case INIT_ASSIGN_VALUE: return INIT_ASSIGN;
			case PRECEDENCE_VALUE: return PRECEDENCE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AssignOperator(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AssignOperator
