/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.operator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Appearance</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Enumeration type to determine how the opeartor should be processed in expression or statement. Each operator type will determine the processing method based on operator argument(s)
 * <!-- end-model-doc -->
 * @see geneauto.emf.models.gacodemodel.operator.OperatorPackage#getOperatorAppearance()
 * @model
 * @generated
 */
public enum OperatorAppearance implements Enumerator {
	/**
	 * The '<em><b>NOTACCEPTED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOTACCEPTED_VALUE
	 * @generated
	 * @ordered
	 */
	NOTACCEPTED(0, "NOTACCEPTED", "NOTACCEPTED"),

	/**
	 * The '<em><b>SCALAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCALAR_VALUE
	 * @generated
	 * @ordered
	 */
	SCALAR(1, "SCALAR", "SCALAR"),

	/**
	 * The '<em><b>SCALARTOARRAY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCALARTOARRAY_VALUE
	 * @generated
	 * @ordered
	 */
	SCALARTOARRAY(2, "SCALARTOARRAY", "SCALARTOARRAY"),

	/**
	 * The '<em><b>ARRAYTOSCALAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARRAYTOSCALAR_VALUE
	 * @generated
	 * @ordered
	 */
	ARRAYTOSCALAR(3, "ARRAYTOSCALAR", "ARRAYTOSCALAR"),

	/**
	 * The '<em><b>ELEMENTWISEARRAY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ELEMENTWISEARRAY_VALUE
	 * @generated
	 * @ordered
	 */
	ELEMENTWISEARRAY(4, "ELEMENTWISEARRAY", "ELEMENTWISEARRAY"),

	/**
	 * The '<em><b>VECTORTOMATRIX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VECTORTOMATRIX_VALUE
	 * @generated
	 * @ordered
	 */
	VECTORTOMATRIX(5, "VECTORTOMATRIX", "VECTORTOMATRIX");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>NOTACCEPTED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NOTACCEPTED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOTACCEPTED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOTACCEPTED_VALUE = 0;

	/**
	 * The '<em><b>SCALAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SCALAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SCALAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SCALAR_VALUE = 1;

	/**
	 * The '<em><b>SCALARTOARRAY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SCALARTOARRAY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SCALARTOARRAY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SCALARTOARRAY_VALUE = 2;

	/**
	 * The '<em><b>ARRAYTOSCALAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ARRAYTOSCALAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ARRAYTOSCALAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ARRAYTOSCALAR_VALUE = 3;

	/**
	 * The '<em><b>ELEMENTWISEARRAY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ELEMENTWISEARRAY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ELEMENTWISEARRAY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ELEMENTWISEARRAY_VALUE = 4;

	/**
	 * The '<em><b>VECTORTOMATRIX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VECTORTOMATRIX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VECTORTOMATRIX
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VECTORTOMATRIX_VALUE = 5;

	/**
	 * An array of all the '<em><b>Appearance</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final OperatorAppearance[] VALUES_ARRAY =
		new OperatorAppearance[] {
			NOTACCEPTED,
			SCALAR,
			SCALARTOARRAY,
			ARRAYTOSCALAR,
			ELEMENTWISEARRAY,
			VECTORTOMATRIX,
		};

	/**
	 * A public read-only list of all the '<em><b>Appearance</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<OperatorAppearance> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Appearance</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OperatorAppearance get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OperatorAppearance result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Appearance</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OperatorAppearance getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OperatorAppearance result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Appearance</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OperatorAppearance get(int value) {
		switch (value) {
			case NOTACCEPTED_VALUE: return NOTACCEPTED;
			case SCALAR_VALUE: return SCALAR;
			case SCALARTOARRAY_VALUE: return SCALARTOARRAY;
			case ARRAYTOSCALAR_VALUE: return ARRAYTOSCALAR;
			case ELEMENTWISEARRAY_VALUE: return ELEMENTWISEARRAY;
			case VECTORTOMATRIX_VALUE: return VECTORTOMATRIX;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private OperatorAppearance(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //OperatorAppearance
