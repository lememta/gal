/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Model Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is a dependency of a CodeModel Module of another CodeModel module.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.CodeModelDependency#getDependentModule <em>Dependent Module</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getCodeModelDependency()
 * @model
 * @generated
 */
public interface CodeModelDependency extends Dependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Dependent Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependent Module</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependent Module</em>' reference.
	 * @see #isSetDependentModule()
	 * @see #unsetDependentModule()
	 * @see #setDependentModule(Module)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getCodeModelDependency_DependentModule()
	 * @model unsettable="true"
	 * @generated
	 */
	Module getDependentModule();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.CodeModelDependency#getDependentModule <em>Dependent Module</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependent Module</em>' reference.
	 * @see #isSetDependentModule()
	 * @see #unsetDependentModule()
	 * @see #getDependentModule()
	 * @generated
	 */
	void setDependentModule(Module value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.CodeModelDependency#getDependentModule <em>Dependent Module</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDependentModule()
	 * @see #getDependentModule()
	 * @see #setDependentModule(Module)
	 * @generated
	 */
	void unsetDependentModule();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.CodeModelDependency#getDependentModule <em>Dependent Module</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Dependent Module</em>' reference is set.
	 * @see #unsetDependentModule()
	 * @see #getDependentModule()
	 * @see #setDependentModule(Module)
	 * @generated
	 */
	boolean isSetDependentModule();

} // CodeModelDependency
