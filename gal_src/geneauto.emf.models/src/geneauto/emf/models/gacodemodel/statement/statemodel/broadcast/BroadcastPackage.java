/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement.statemodel.broadcast;

import geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastFactory
 * @model kind="package"
 * @generated
 */
public interface BroadcastPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "broadcast";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gacodemodel/statement/statemodel/broadcast.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gacodemodel.statement.statemodel.broadcast";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BroadcastPackage eINSTANCE = geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastStatementImpl <em>Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getBroadcastStatement()
	 * @generated
	 */
	int BROADCAST_STATEMENT = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__EXTERNAL_ID = StatemodelPackage.STATE_MODEL_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__ID = StatemodelPackage.STATE_MODEL_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__IS_FIXED_NAME = StatemodelPackage.STATE_MODEL_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__NAME = StatemodelPackage.STATE_MODEL_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__ORIGINAL_FULL_NAME = StatemodelPackage.STATE_MODEL_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__ORIGINAL_NAME = StatemodelPackage.STATE_MODEL_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__PARENT = StatemodelPackage.STATE_MODEL_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__MODEL = StatemodelPackage.STATE_MODEL_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__ANNOTATIONS = StatemodelPackage.STATE_MODEL_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__SOURCE_ELEMENT = StatemodelPackage.STATE_MODEL_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__SOURCE_ELEMENT_ID = StatemodelPackage.STATE_MODEL_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__SOURCE_ACTION = StatemodelPackage.STATE_MODEL_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__MODULE = StatemodelPackage.STATE_MODEL_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__SEQUENCE_NUMBER = StatemodelPackage.STATE_MODEL_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__LABEL = StatemodelPackage.STATE_MODEL_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT__EVENT = StatemodelPackage.STATE_MODEL_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_STATEMENT_FEATURE_COUNT = StatemodelPackage.STATE_MODEL_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartGlobalBroadcastImpl <em>Chart Global Broadcast</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartGlobalBroadcastImpl
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getChartGlobalBroadcast()
	 * @generated
	 */
	int CHART_GLOBAL_BROADCAST = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__EXTERNAL_ID = BROADCAST_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__ID = BROADCAST_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__IS_FIXED_NAME = BROADCAST_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__NAME = BROADCAST_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__ORIGINAL_FULL_NAME = BROADCAST_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__ORIGINAL_NAME = BROADCAST_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__PARENT = BROADCAST_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__MODEL = BROADCAST_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__ANNOTATIONS = BROADCAST_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__SOURCE_ELEMENT = BROADCAST_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__SOURCE_ELEMENT_ID = BROADCAST_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__SOURCE_ACTION = BROADCAST_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__MODULE = BROADCAST_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__SEQUENCE_NUMBER = BROADCAST_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__LABEL = BROADCAST_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST__EVENT = BROADCAST_STATEMENT__EVENT;

	/**
	 * The number of structural features of the '<em>Chart Global Broadcast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_GLOBAL_BROADCAST_FEATURE_COUNT = BROADCAST_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartSendBroadcastImpl <em>Chart Send Broadcast</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartSendBroadcastImpl
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getChartSendBroadcast()
	 * @generated
	 */
	int CHART_SEND_BROADCAST = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__EXTERNAL_ID = BROADCAST_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__ID = BROADCAST_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__IS_FIXED_NAME = BROADCAST_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__NAME = BROADCAST_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__ORIGINAL_FULL_NAME = BROADCAST_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__ORIGINAL_NAME = BROADCAST_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__PARENT = BROADCAST_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__MODEL = BROADCAST_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__ANNOTATIONS = BROADCAST_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__SOURCE_ELEMENT = BROADCAST_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__SOURCE_ELEMENT_ID = BROADCAST_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__SOURCE_ACTION = BROADCAST_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__MODULE = BROADCAST_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__SEQUENCE_NUMBER = BROADCAST_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__LABEL = BROADCAST_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST__EVENT = BROADCAST_STATEMENT__EVENT;

	/**
	 * The number of structural features of the '<em>Chart Send Broadcast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_SEND_BROADCAST_FEATURE_COUNT = BROADCAST_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ExternalBroadcastImpl <em>External Broadcast</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ExternalBroadcastImpl
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getExternalBroadcast()
	 * @generated
	 */
	int EXTERNAL_BROADCAST = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__EXTERNAL_ID = BROADCAST_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__ID = BROADCAST_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__IS_FIXED_NAME = BROADCAST_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__NAME = BROADCAST_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__ORIGINAL_FULL_NAME = BROADCAST_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__ORIGINAL_NAME = BROADCAST_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__PARENT = BROADCAST_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__MODEL = BROADCAST_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__ANNOTATIONS = BROADCAST_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__SOURCE_ELEMENT = BROADCAST_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__SOURCE_ELEMENT_ID = BROADCAST_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__SOURCE_ACTION = BROADCAST_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__MODULE = BROADCAST_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__SEQUENCE_NUMBER = BROADCAST_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__LABEL = BROADCAST_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST__EVENT = BROADCAST_STATEMENT__EVENT;

	/**
	 * The number of structural features of the '<em>External Broadcast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_BROADCAST_FEATURE_COUNT = BROADCAST_STATEMENT_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastStatement
	 * @generated
	 */
	EClass getBroadcastStatement();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastStatement#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastStatement#getEvent()
	 * @see #getBroadcastStatement()
	 * @generated
	 */
	EReference getBroadcastStatement_Event();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.ChartGlobalBroadcast <em>Chart Global Broadcast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chart Global Broadcast</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.ChartGlobalBroadcast
	 * @generated
	 */
	EClass getChartGlobalBroadcast();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.ChartSendBroadcast <em>Chart Send Broadcast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chart Send Broadcast</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.ChartSendBroadcast
	 * @generated
	 */
	EClass getChartSendBroadcast();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.ExternalBroadcast <em>External Broadcast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Broadcast</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.ExternalBroadcast
	 * @generated
	 */
	EClass getExternalBroadcast();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BroadcastFactory getBroadcastFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastStatementImpl <em>Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getBroadcastStatement()
		 * @generated
		 */
		EClass BROADCAST_STATEMENT = eINSTANCE.getBroadcastStatement();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BROADCAST_STATEMENT__EVENT = eINSTANCE.getBroadcastStatement_Event();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartGlobalBroadcastImpl <em>Chart Global Broadcast</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartGlobalBroadcastImpl
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getChartGlobalBroadcast()
		 * @generated
		 */
		EClass CHART_GLOBAL_BROADCAST = eINSTANCE.getChartGlobalBroadcast();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartSendBroadcastImpl <em>Chart Send Broadcast</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ChartSendBroadcastImpl
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getChartSendBroadcast()
		 * @generated
		 */
		EClass CHART_SEND_BROADCAST = eINSTANCE.getChartSendBroadcast();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ExternalBroadcastImpl <em>External Broadcast</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.ExternalBroadcastImpl
		 * @see geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl#getExternalBroadcast()
		 * @generated
		 */
		EClass EXTERNAL_BROADCAST = eINSTANCE.getExternalBroadcast();

	}

} //BroadcastPackage
