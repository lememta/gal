/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement.util;

import geneauto.emf.models.common.HasVariable;

import geneauto.emf.models.gacodemodel.GACodeModelElement;
import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.SequentialComposition;

import geneauto.emf.models.gacodemodel.statement.*;

import geneauto.emf.models.genericmodel.GAModelElement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage
 * @generated
 */
public class StatementAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StatementPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = StatementPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatementSwitch<Adapter> modelSwitch =
		new StatementSwitch<Adapter>() {
			@Override
			public Adapter caseBlankStatement(BlankStatement object) {
				return createBlankStatementAdapter();
			}
			@Override
			public Adapter caseBreakStatement(BreakStatement object) {
				return createBreakStatementAdapter();
			}
			@Override
			public Adapter caseDecStatement(DecStatement object) {
				return createDecStatementAdapter();
			}
			@Override
			public Adapter caseUnaryActionStatement(UnaryActionStatement object) {
				return createUnaryActionStatementAdapter();
			}
			@Override
			public Adapter caseIncStatement(IncStatement object) {
				return createIncStatementAdapter();
			}
			@Override
			public Adapter caseLoopStatement(LoopStatement object) {
				return createLoopStatementAdapter();
			}
			@Override
			public Adapter caseRangeIterationStatement(RangeIterationStatement object) {
				return createRangeIterationStatementAdapter();
			}
			@Override
			public Adapter caseAssignStatement(AssignStatement object) {
				return createAssignStatementAdapter();
			}
			@Override
			public Adapter caseCaseStatement(CaseStatement object) {
				return createCaseStatementAdapter();
			}
			@Override
			public Adapter caseStatementWithBranches(StatementWithBranches object) {
				return createStatementWithBranchesAdapter();
			}
			@Override
			public Adapter caseWhenCase(WhenCase object) {
				return createWhenCaseAdapter();
			}
			@Override
			public Adapter caseDefaultCase(DefaultCase object) {
				return createDefaultCaseAdapter();
			}
			@Override
			public Adapter caseCompoundStatement(CompoundStatement object) {
				return createCompoundStatementAdapter();
			}
			@Override
			public Adapter caseExpressionStatement(ExpressionStatement object) {
				return createExpressionStatementAdapter();
			}
			@Override
			public Adapter caseForStatement(ForStatement object) {
				return createForStatementAdapter();
			}
			@Override
			public Adapter caseGotoStatement(GotoStatement object) {
				return createGotoStatementAdapter();
			}
			@Override
			public Adapter caseIfStatement(IfStatement object) {
				return createIfStatementAdapter();
			}
			@Override
			public Adapter caseReturnStatement(ReturnStatement object) {
				return createReturnStatementAdapter();
			}
			@Override
			public Adapter caseStatement(Statement object) {
				return createStatementAdapter();
			}
			@Override
			public Adapter caseGAModelElement(GAModelElement object) {
				return createGAModelElementAdapter();
			}
			@Override
			public Adapter caseGACodeModelElement(GACodeModelElement object) {
				return createGACodeModelElementAdapter();
			}
			@Override
			public Adapter caseHasNameSpace(HasNameSpace object) {
				return createHasNameSpaceAdapter();
			}
			@Override
			public Adapter caseHasVariable(HasVariable object) {
				return createHasVariableAdapter();
			}
			@Override
			public Adapter caseSequentialComposition(SequentialComposition object) {
				return createSequentialCompositionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.BlankStatement <em>Blank Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.BlankStatement
	 * @generated
	 */
	public Adapter createBlankStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.BreakStatement <em>Break Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.BreakStatement
	 * @generated
	 */
	public Adapter createBreakStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.DecStatement <em>Dec Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.DecStatement
	 * @generated
	 */
	public Adapter createDecStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.UnaryActionStatement <em>Unary Action Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.UnaryActionStatement
	 * @generated
	 */
	public Adapter createUnaryActionStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.IncStatement <em>Inc Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.IncStatement
	 * @generated
	 */
	public Adapter createIncStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.LoopStatement <em>Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.LoopStatement
	 * @generated
	 */
	public Adapter createLoopStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement <em>Range Iteration Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.RangeIterationStatement
	 * @generated
	 */
	public Adapter createRangeIterationStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement <em>Assign Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.AssignStatement
	 * @generated
	 */
	public Adapter createAssignStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement <em>Case Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.CaseStatement
	 * @generated
	 */
	public Adapter createCaseStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.StatementWithBranches <em>With Branches</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.StatementWithBranches
	 * @generated
	 */
	public Adapter createStatementWithBranchesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.WhenCase <em>When Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.WhenCase
	 * @generated
	 */
	public Adapter createWhenCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.DefaultCase <em>Default Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.DefaultCase
	 * @generated
	 */
	public Adapter createDefaultCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement <em>Compound Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.CompoundStatement
	 * @generated
	 */
	public Adapter createCompoundStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.ExpressionStatement <em>Expression Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.ExpressionStatement
	 * @generated
	 */
	public Adapter createExpressionStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.ForStatement <em>For Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.ForStatement
	 * @generated
	 */
	public Adapter createForStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.GotoStatement <em>Goto Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.GotoStatement
	 * @generated
	 */
	public Adapter createGotoStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.IfStatement <em>If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.IfStatement
	 * @generated
	 */
	public Adapter createIfStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.ReturnStatement <em>Return Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.ReturnStatement
	 * @generated
	 */
	public Adapter createReturnStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.statement.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.statement.Statement
	 * @generated
	 */
	public Adapter createStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.genericmodel.GAModelElement <em>GA Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.genericmodel.GAModelElement
	 * @generated
	 */
	public Adapter createGAModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.GACodeModelElement <em>GA Code Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement
	 * @generated
	 */
	public Adapter createGACodeModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.HasNameSpace <em>Has Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.HasNameSpace
	 * @generated
	 */
	public Adapter createHasNameSpaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.common.HasVariable <em>Has Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.common.HasVariable
	 * @generated
	 */
	public Adapter createHasVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.SequentialComposition <em>Sequential Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.SequentialComposition
	 * @generated
	 */
	public Adapter createSequentialCompositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //StatementAdapterFactory
