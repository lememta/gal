/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.common.HasVariable;

import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.Variable_CM;

import geneauto.emf.models.gacodemodel.expression.RangeExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Iteration Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * RangeIterationStatement expresses a loop over fixed number of steps. While the ForStament allows to define dynamic end condition evaluated on each step, the RangeIterationStatemnt is guaranteed to go through all steps in the range.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getIteratorVariable <em>Iterator Variable</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getRange <em>Range</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getRangeIterationStatement()
 * @model
 * @generated
 */
public interface RangeIterationStatement extends LoopStatement, HasNameSpace, HasVariable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Iterator Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator Variable</em>' containment reference.
	 * @see #isSetIteratorVariable()
	 * @see #unsetIteratorVariable()
	 * @see #setIteratorVariable(Variable_CM)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getRangeIterationStatement_IteratorVariable()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Variable_CM getIteratorVariable();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getIteratorVariable <em>Iterator Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator Variable</em>' containment reference.
	 * @see #isSetIteratorVariable()
	 * @see #unsetIteratorVariable()
	 * @see #getIteratorVariable()
	 * @generated
	 */
	void setIteratorVariable(Variable_CM value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getIteratorVariable <em>Iterator Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIteratorVariable()
	 * @see #getIteratorVariable()
	 * @see #setIteratorVariable(Variable_CM)
	 * @generated
	 */
	void unsetIteratorVariable();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getIteratorVariable <em>Iterator Variable</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Iterator Variable</em>' containment reference is set.
	 * @see #unsetIteratorVariable()
	 * @see #getIteratorVariable()
	 * @see #setIteratorVariable(Variable_CM)
	 * @generated
	 */
	boolean isSetIteratorVariable();

	/**
	 * Returns the value of the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' containment reference.
	 * @see #isSetRange()
	 * @see #unsetRange()
	 * @see #setRange(RangeExpression)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getRangeIterationStatement_Range()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	RangeExpression getRange();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getRange <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' containment reference.
	 * @see #isSetRange()
	 * @see #unsetRange()
	 * @see #getRange()
	 * @generated
	 */
	void setRange(RangeExpression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getRange <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRange()
	 * @see #getRange()
	 * @see #setRange(RangeExpression)
	 * @generated
	 */
	void unsetRange();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getRange <em>Range</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Range</em>' containment reference is set.
	 * @see #unsetRange()
	 * @see #getRange()
	 * @see #setRange(RangeExpression)
	 * @generated
	 */
	boolean isSetRange();

} // RangeIterationStatement
