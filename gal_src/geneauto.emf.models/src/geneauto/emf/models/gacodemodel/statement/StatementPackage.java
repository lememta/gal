/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.statement.StatementFactory
 * @model kind="package"
 * @generated
 */
public interface StatementPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "statement";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gacodemodel/statement.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gacodemodel.statement";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatementPackage eINSTANCE = geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.StatementImpl <em>Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getStatement()
	 * @generated
	 */
	int STATEMENT = 18;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__EXTERNAL_ID = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__ID = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__IS_FIXED_NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__ORIGINAL_FULL_NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__ORIGINAL_NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__PARENT = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__MODEL = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__ANNOTATIONS = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__SOURCE_ELEMENT = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__SOURCE_ELEMENT_ID = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__SOURCE_ACTION = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__MODULE = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__SEQUENCE_NUMBER = GacodemodelPackage.GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__LABEL = GacodemodelPackage.GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_FEATURE_COUNT = GacodemodelPackage.GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.BlankStatementImpl <em>Blank Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.BlankStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getBlankStatement()
	 * @generated
	 */
	int BLANK_STATEMENT = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Empty Lines Before</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT__EMPTY_LINES_BEFORE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Blank Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLANK_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.BreakStatementImpl <em>Break Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.BreakStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getBreakStatement()
	 * @generated
	 */
	int BREAK_STATEMENT = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The number of structural features of the '<em>Break Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.UnaryActionStatementImpl <em>Unary Action Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.UnaryActionStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getUnaryActionStatement()
	 * @generated
	 */
	int UNARY_ACTION_STATEMENT = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT__ARGUMENT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary Action Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_ACTION_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.DecStatementImpl <em>Dec Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.DecStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getDecStatement()
	 * @generated
	 */
	int DEC_STATEMENT = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__EXTERNAL_ID = UNARY_ACTION_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__ID = UNARY_ACTION_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__IS_FIXED_NAME = UNARY_ACTION_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__NAME = UNARY_ACTION_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__ORIGINAL_FULL_NAME = UNARY_ACTION_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__ORIGINAL_NAME = UNARY_ACTION_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__PARENT = UNARY_ACTION_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__MODEL = UNARY_ACTION_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__ANNOTATIONS = UNARY_ACTION_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__SOURCE_ELEMENT = UNARY_ACTION_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__SOURCE_ELEMENT_ID = UNARY_ACTION_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__SOURCE_ACTION = UNARY_ACTION_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__MODULE = UNARY_ACTION_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__SEQUENCE_NUMBER = UNARY_ACTION_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__LABEL = UNARY_ACTION_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT__ARGUMENT = UNARY_ACTION_STATEMENT__ARGUMENT;

	/**
	 * The number of structural features of the '<em>Dec Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_STATEMENT_FEATURE_COUNT = UNARY_ACTION_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.IncStatementImpl <em>Inc Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.IncStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getIncStatement()
	 * @generated
	 */
	int INC_STATEMENT = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__EXTERNAL_ID = UNARY_ACTION_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__ID = UNARY_ACTION_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__IS_FIXED_NAME = UNARY_ACTION_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__NAME = UNARY_ACTION_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__ORIGINAL_FULL_NAME = UNARY_ACTION_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__ORIGINAL_NAME = UNARY_ACTION_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__PARENT = UNARY_ACTION_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__MODEL = UNARY_ACTION_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__ANNOTATIONS = UNARY_ACTION_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__SOURCE_ELEMENT = UNARY_ACTION_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__SOURCE_ELEMENT_ID = UNARY_ACTION_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__SOURCE_ACTION = UNARY_ACTION_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__MODULE = UNARY_ACTION_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__SEQUENCE_NUMBER = UNARY_ACTION_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__LABEL = UNARY_ACTION_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT__ARGUMENT = UNARY_ACTION_STATEMENT__ARGUMENT;

	/**
	 * The number of structural features of the '<em>Inc Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INC_STATEMENT_FEATURE_COUNT = UNARY_ACTION_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.LoopStatementImpl <em>Loop Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.LoopStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getLoopStatement()
	 * @generated
	 */
	int LOOP_STATEMENT = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Body Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__BODY_STATEMENT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Loop Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.RangeIterationStatementImpl <em>Range Iteration Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.RangeIterationStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getRangeIterationStatement()
	 * @generated
	 */
	int RANGE_ITERATION_STATEMENT = 6;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__EXTERNAL_ID = LOOP_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__ID = LOOP_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__IS_FIXED_NAME = LOOP_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__NAME = LOOP_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__ORIGINAL_FULL_NAME = LOOP_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__ORIGINAL_NAME = LOOP_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__PARENT = LOOP_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__MODEL = LOOP_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__ANNOTATIONS = LOOP_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__SOURCE_ELEMENT = LOOP_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__SOURCE_ELEMENT_ID = LOOP_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__SOURCE_ACTION = LOOP_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__MODULE = LOOP_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__SEQUENCE_NUMBER = LOOP_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__LABEL = LOOP_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Body Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__BODY_STATEMENT = LOOP_STATEMENT__BODY_STATEMENT;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__NAME_SPACE = LOOP_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iterator Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE = LOOP_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT__RANGE = LOOP_STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Range Iteration Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ITERATION_STATEMENT_FEATURE_COUNT = LOOP_STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.AssignStatementImpl <em>Assign Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.AssignStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getAssignStatement()
	 * @generated
	 */
	int ASSIGN_STATEMENT = 7;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Right Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__RIGHT_EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__LEFT_EXPRESSION = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT__OPERATOR = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Assign Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGN_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.CaseStatementImpl <em>Case Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.CaseStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getCaseStatement()
	 * @generated
	 */
	int CASE_STATEMENT = 8;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__CONDITION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>When Cases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__WHEN_CASES = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default Case</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__DEFAULT_CASE = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Case Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.StatementWithBranches <em>With Branches</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.StatementWithBranches
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getStatementWithBranches()
	 * @generated
	 */
	int STATEMENT_WITH_BRANCHES = 9;

	/**
	 * The number of structural features of the '<em>With Branches</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_WITH_BRANCHES_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.WhenCaseImpl <em>When Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.WhenCaseImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getWhenCase()
	 * @generated
	 */
	int WHEN_CASE = 10;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__WHEN = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE__STATEMENTS = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>When Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_CASE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.DefaultCaseImpl <em>Default Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.DefaultCaseImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getDefaultCase()
	 * @generated
	 */
	int DEFAULT_CASE = 11;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE__STATEMENTS = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Default Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFAULT_CASE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.CompoundStatementImpl <em>Compound Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.CompoundStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getCompoundStatement()
	 * @generated
	 */
	int COMPOUND_STATEMENT = 12;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__NAME_SPACE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Atomic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__IS_ATOMIC = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__IS_VIRTUAL = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT__STATEMENTS = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Compound Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.ExpressionStatementImpl <em>Expression Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.ExpressionStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getExpressionStatement()
	 * @generated
	 */
	int EXPRESSION_STATEMENT = 13;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expression Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.ForStatementImpl <em>For Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.ForStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getForStatement()
	 * @generated
	 */
	int FOR_STATEMENT = 14;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__EXTERNAL_ID = LOOP_STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__ID = LOOP_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__IS_FIXED_NAME = LOOP_STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__NAME = LOOP_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__ORIGINAL_FULL_NAME = LOOP_STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__ORIGINAL_NAME = LOOP_STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__PARENT = LOOP_STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__MODEL = LOOP_STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__ANNOTATIONS = LOOP_STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__SOURCE_ELEMENT = LOOP_STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__SOURCE_ELEMENT_ID = LOOP_STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__SOURCE_ACTION = LOOP_STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__MODULE = LOOP_STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__SEQUENCE_NUMBER = LOOP_STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__LABEL = LOOP_STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Body Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__BODY_STATEMENT = LOOP_STATEMENT__BODY_STATEMENT;

	/**
	 * The feature id for the '<em><b>Condition Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__CONDITION_EXPRESSION = LOOP_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pre Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__PRE_STATEMENT = LOOP_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Post Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT__POST_STATEMENT = LOOP_STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>For Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_STATEMENT_FEATURE_COUNT = LOOP_STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.GotoStatementImpl <em>Goto Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.GotoStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getGotoStatement()
	 * @generated
	 */
	int GOTO_STATEMENT = 15;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT__TARGET = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Goto Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.IfStatementImpl <em>If Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.IfStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getIfStatement()
	 * @generated
	 */
	int IF_STATEMENT = 16;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Condition Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__CONDITION_EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Else Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__ELSE_STATEMENT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Then Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__THEN_STATEMENT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>If Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.statement.impl.ReturnStatementImpl <em>Return Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.statement.impl.ReturnStatementImpl
	 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getReturnStatement()
	 * @generated
	 */
	int RETURN_STATEMENT = 17;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__EXTERNAL_ID = STATEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__IS_FIXED_NAME = STATEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__ORIGINAL_FULL_NAME = STATEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__ORIGINAL_NAME = STATEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__PARENT = STATEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__MODEL = STATEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__ANNOTATIONS = STATEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__SOURCE_ELEMENT = STATEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__SOURCE_ELEMENT_ID = STATEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__SOURCE_ACTION = STATEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__MODULE = STATEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__SEQUENCE_NUMBER = STATEMENT__SEQUENCE_NUMBER;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__LABEL = STATEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Return Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__RETURN_EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Return Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.BlankStatement <em>Blank Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blank Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.BlankStatement
	 * @generated
	 */
	EClass getBlankStatement();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.statement.BlankStatement#getEmptyLinesBefore <em>Empty Lines Before</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Empty Lines Before</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.BlankStatement#getEmptyLinesBefore()
	 * @see #getBlankStatement()
	 * @generated
	 */
	EAttribute getBlankStatement_EmptyLinesBefore();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.BreakStatement <em>Break Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Break Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.BreakStatement
	 * @generated
	 */
	EClass getBreakStatement();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.DecStatement <em>Dec Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dec Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.DecStatement
	 * @generated
	 */
	EClass getDecStatement();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.UnaryActionStatement <em>Unary Action Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Action Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.UnaryActionStatement
	 * @generated
	 */
	EClass getUnaryActionStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.UnaryActionStatement#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Argument</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.UnaryActionStatement#getArgument()
	 * @see #getUnaryActionStatement()
	 * @generated
	 */
	EReference getUnaryActionStatement_Argument();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.IncStatement <em>Inc Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inc Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.IncStatement
	 * @generated
	 */
	EClass getIncStatement();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.LoopStatement <em>Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.LoopStatement
	 * @generated
	 */
	EClass getLoopStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.LoopStatement#getBodyStatement <em>Body Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.LoopStatement#getBodyStatement()
	 * @see #getLoopStatement()
	 * @generated
	 */
	EReference getLoopStatement_BodyStatement();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement <em>Range Iteration Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Iteration Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.RangeIterationStatement
	 * @generated
	 */
	EClass getRangeIterationStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getIteratorVariable <em>Iterator Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Iterator Variable</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getIteratorVariable()
	 * @see #getRangeIterationStatement()
	 * @generated
	 */
	EReference getRangeIterationStatement_IteratorVariable();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Range</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.RangeIterationStatement#getRange()
	 * @see #getRangeIterationStatement()
	 * @generated
	 */
	EReference getRangeIterationStatement_Range();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement <em>Assign Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assign Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.AssignStatement
	 * @generated
	 */
	EClass getAssignStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getRightExpression <em>Right Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.AssignStatement#getRightExpression()
	 * @see #getAssignStatement()
	 * @generated
	 */
	EReference getAssignStatement_RightExpression();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getLeftExpression <em>Left Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.AssignStatement#getLeftExpression()
	 * @see #getAssignStatement()
	 * @generated
	 */
	EReference getAssignStatement_LeftExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.AssignStatement#getOperator()
	 * @see #getAssignStatement()
	 * @generated
	 */
	EAttribute getAssignStatement_Operator();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement <em>Case Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CaseStatement
	 * @generated
	 */
	EClass getCaseStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CaseStatement#getCondition()
	 * @see #getCaseStatement()
	 * @generated
	 */
	EReference getCaseStatement_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getWhenCases <em>When Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>When Cases</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CaseStatement#getWhenCases()
	 * @see #getCaseStatement()
	 * @generated
	 */
	EReference getCaseStatement_WhenCases();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getDefaultCase <em>Default Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Case</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CaseStatement#getDefaultCase()
	 * @see #getCaseStatement()
	 * @generated
	 */
	EReference getCaseStatement_DefaultCase();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.StatementWithBranches <em>With Branches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>With Branches</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.StatementWithBranches
	 * @generated
	 */
	EClass getStatementWithBranches();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.WhenCase <em>When Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>When Case</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.WhenCase
	 * @generated
	 */
	EClass getWhenCase();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>When</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.WhenCase#getWhen()
	 * @see #getWhenCase()
	 * @generated
	 */
	EReference getWhenCase_When();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.WhenCase#getStatements()
	 * @see #getWhenCase()
	 * @generated
	 */
	EReference getWhenCase_Statements();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.DefaultCase <em>Default Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Default Case</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.DefaultCase
	 * @generated
	 */
	EClass getDefaultCase();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.statement.DefaultCase#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.DefaultCase#getStatements()
	 * @see #getDefaultCase()
	 * @generated
	 */
	EReference getDefaultCase_Statements();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement <em>Compound Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compound Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CompoundStatement
	 * @generated
	 */
	EClass getCompoundStatement();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isAtomic <em>Is Atomic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Atomic</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CompoundStatement#isAtomic()
	 * @see #getCompoundStatement()
	 * @generated
	 */
	EAttribute getCompoundStatement_IsAtomic();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isVirtual <em>Is Virtual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Virtual</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CompoundStatement#isVirtual()
	 * @see #getCompoundStatement()
	 * @generated
	 */
	EAttribute getCompoundStatement_IsVirtual();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.CompoundStatement#getStatements()
	 * @see #getCompoundStatement()
	 * @generated
	 */
	EReference getCompoundStatement_Statements();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.ExpressionStatement <em>Expression Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ExpressionStatement
	 * @generated
	 */
	EClass getExpressionStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.ExpressionStatement#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ExpressionStatement#getExpression()
	 * @see #getExpressionStatement()
	 * @generated
	 */
	EReference getExpressionStatement_Expression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.ForStatement <em>For Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ForStatement
	 * @generated
	 */
	EClass getForStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getConditionExpression <em>Condition Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ForStatement#getConditionExpression()
	 * @see #getForStatement()
	 * @generated
	 */
	EReference getForStatement_ConditionExpression();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPreStatement <em>Pre Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ForStatement#getPreStatement()
	 * @see #getForStatement()
	 * @generated
	 */
	EReference getForStatement_PreStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPostStatement <em>Post Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Post Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ForStatement#getPostStatement()
	 * @see #getForStatement()
	 * @generated
	 */
	EReference getForStatement_PostStatement();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.GotoStatement <em>Goto Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goto Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.GotoStatement
	 * @generated
	 */
	EClass getGotoStatement();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.statement.GotoStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.GotoStatement#getTarget()
	 * @see #getGotoStatement()
	 * @generated
	 */
	EReference getGotoStatement_Target();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.IfStatement <em>If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.IfStatement
	 * @generated
	 */
	EClass getIfStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getConditionExpression <em>Condition Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.IfStatement#getConditionExpression()
	 * @see #getIfStatement()
	 * @generated
	 */
	EReference getIfStatement_ConditionExpression();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getElseStatement <em>Else Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.IfStatement#getElseStatement()
	 * @see #getIfStatement()
	 * @generated
	 */
	EReference getIfStatement_ElseStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getThenStatement <em>Then Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.IfStatement#getThenStatement()
	 * @see #getIfStatement()
	 * @generated
	 */
	EReference getIfStatement_ThenStatement();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.ReturnStatement <em>Return Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ReturnStatement
	 * @generated
	 */
	EClass getReturnStatement();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.ReturnStatement#getReturnExpression <em>Return Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.ReturnStatement#getReturnExpression()
	 * @see #getReturnStatement()
	 * @generated
	 */
	EReference getReturnStatement_ReturnExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.statement.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.Statement
	 * @generated
	 */
	EClass getStatement();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.statement.Statement#getSequenceNumber <em>Sequence Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sequence Number</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.Statement#getSequenceNumber()
	 * @see #getStatement()
	 * @generated
	 */
	EAttribute getStatement_SequenceNumber();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.statement.Statement#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Label</em>'.
	 * @see geneauto.emf.models.gacodemodel.statement.Statement#getLabel()
	 * @see #getStatement()
	 * @generated
	 */
	EReference getStatement_Label();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatementFactory getStatementFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.BlankStatementImpl <em>Blank Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.BlankStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getBlankStatement()
		 * @generated
		 */
		EClass BLANK_STATEMENT = eINSTANCE.getBlankStatement();

		/**
		 * The meta object literal for the '<em><b>Empty Lines Before</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLANK_STATEMENT__EMPTY_LINES_BEFORE = eINSTANCE.getBlankStatement_EmptyLinesBefore();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.BreakStatementImpl <em>Break Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.BreakStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getBreakStatement()
		 * @generated
		 */
		EClass BREAK_STATEMENT = eINSTANCE.getBreakStatement();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.DecStatementImpl <em>Dec Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.DecStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getDecStatement()
		 * @generated
		 */
		EClass DEC_STATEMENT = eINSTANCE.getDecStatement();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.UnaryActionStatementImpl <em>Unary Action Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.UnaryActionStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getUnaryActionStatement()
		 * @generated
		 */
		EClass UNARY_ACTION_STATEMENT = eINSTANCE.getUnaryActionStatement();

		/**
		 * The meta object literal for the '<em><b>Argument</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_ACTION_STATEMENT__ARGUMENT = eINSTANCE.getUnaryActionStatement_Argument();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.IncStatementImpl <em>Inc Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.IncStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getIncStatement()
		 * @generated
		 */
		EClass INC_STATEMENT = eINSTANCE.getIncStatement();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.LoopStatementImpl <em>Loop Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.LoopStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getLoopStatement()
		 * @generated
		 */
		EClass LOOP_STATEMENT = eINSTANCE.getLoopStatement();

		/**
		 * The meta object literal for the '<em><b>Body Statement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOOP_STATEMENT__BODY_STATEMENT = eINSTANCE.getLoopStatement_BodyStatement();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.RangeIterationStatementImpl <em>Range Iteration Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.RangeIterationStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getRangeIterationStatement()
		 * @generated
		 */
		EClass RANGE_ITERATION_STATEMENT = eINSTANCE.getRangeIterationStatement();

		/**
		 * The meta object literal for the '<em><b>Iterator Variable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE = eINSTANCE.getRangeIterationStatement_IteratorVariable();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_ITERATION_STATEMENT__RANGE = eINSTANCE.getRangeIterationStatement_Range();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.AssignStatementImpl <em>Assign Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.AssignStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getAssignStatement()
		 * @generated
		 */
		EClass ASSIGN_STATEMENT = eINSTANCE.getAssignStatement();

		/**
		 * The meta object literal for the '<em><b>Right Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGN_STATEMENT__RIGHT_EXPRESSION = eINSTANCE.getAssignStatement_RightExpression();

		/**
		 * The meta object literal for the '<em><b>Left Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGN_STATEMENT__LEFT_EXPRESSION = eINSTANCE.getAssignStatement_LeftExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSIGN_STATEMENT__OPERATOR = eINSTANCE.getAssignStatement_Operator();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.CaseStatementImpl <em>Case Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.CaseStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getCaseStatement()
		 * @generated
		 */
		EClass CASE_STATEMENT = eINSTANCE.getCaseStatement();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_STATEMENT__CONDITION = eINSTANCE.getCaseStatement_Condition();

		/**
		 * The meta object literal for the '<em><b>When Cases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_STATEMENT__WHEN_CASES = eINSTANCE.getCaseStatement_WhenCases();

		/**
		 * The meta object literal for the '<em><b>Default Case</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_STATEMENT__DEFAULT_CASE = eINSTANCE.getCaseStatement_DefaultCase();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.StatementWithBranches <em>With Branches</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.StatementWithBranches
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getStatementWithBranches()
		 * @generated
		 */
		EClass STATEMENT_WITH_BRANCHES = eINSTANCE.getStatementWithBranches();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.WhenCaseImpl <em>When Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.WhenCaseImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getWhenCase()
		 * @generated
		 */
		EClass WHEN_CASE = eINSTANCE.getWhenCase();

		/**
		 * The meta object literal for the '<em><b>When</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHEN_CASE__WHEN = eINSTANCE.getWhenCase_When();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHEN_CASE__STATEMENTS = eINSTANCE.getWhenCase_Statements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.DefaultCaseImpl <em>Default Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.DefaultCaseImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getDefaultCase()
		 * @generated
		 */
		EClass DEFAULT_CASE = eINSTANCE.getDefaultCase();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFAULT_CASE__STATEMENTS = eINSTANCE.getDefaultCase_Statements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.CompoundStatementImpl <em>Compound Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.CompoundStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getCompoundStatement()
		 * @generated
		 */
		EClass COMPOUND_STATEMENT = eINSTANCE.getCompoundStatement();

		/**
		 * The meta object literal for the '<em><b>Is Atomic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOUND_STATEMENT__IS_ATOMIC = eINSTANCE.getCompoundStatement_IsAtomic();

		/**
		 * The meta object literal for the '<em><b>Is Virtual</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOUND_STATEMENT__IS_VIRTUAL = eINSTANCE.getCompoundStatement_IsVirtual();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOUND_STATEMENT__STATEMENTS = eINSTANCE.getCompoundStatement_Statements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.ExpressionStatementImpl <em>Expression Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.ExpressionStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getExpressionStatement()
		 * @generated
		 */
		EClass EXPRESSION_STATEMENT = eINSTANCE.getExpressionStatement();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION_STATEMENT__EXPRESSION = eINSTANCE.getExpressionStatement_Expression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.ForStatementImpl <em>For Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.ForStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getForStatement()
		 * @generated
		 */
		EClass FOR_STATEMENT = eINSTANCE.getForStatement();

		/**
		 * The meta object literal for the '<em><b>Condition Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_STATEMENT__CONDITION_EXPRESSION = eINSTANCE.getForStatement_ConditionExpression();

		/**
		 * The meta object literal for the '<em><b>Pre Statement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_STATEMENT__PRE_STATEMENT = eINSTANCE.getForStatement_PreStatement();

		/**
		 * The meta object literal for the '<em><b>Post Statement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_STATEMENT__POST_STATEMENT = eINSTANCE.getForStatement_PostStatement();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.GotoStatementImpl <em>Goto Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.GotoStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getGotoStatement()
		 * @generated
		 */
		EClass GOTO_STATEMENT = eINSTANCE.getGotoStatement();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOTO_STATEMENT__TARGET = eINSTANCE.getGotoStatement_Target();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.IfStatementImpl <em>If Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.IfStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getIfStatement()
		 * @generated
		 */
		EClass IF_STATEMENT = eINSTANCE.getIfStatement();

		/**
		 * The meta object literal for the '<em><b>Condition Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_STATEMENT__CONDITION_EXPRESSION = eINSTANCE.getIfStatement_ConditionExpression();

		/**
		 * The meta object literal for the '<em><b>Else Statement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_STATEMENT__ELSE_STATEMENT = eINSTANCE.getIfStatement_ElseStatement();

		/**
		 * The meta object literal for the '<em><b>Then Statement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_STATEMENT__THEN_STATEMENT = eINSTANCE.getIfStatement_ThenStatement();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.ReturnStatementImpl <em>Return Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.ReturnStatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getReturnStatement()
		 * @generated
		 */
		EClass RETURN_STATEMENT = eINSTANCE.getReturnStatement();

		/**
		 * The meta object literal for the '<em><b>Return Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_STATEMENT__RETURN_EXPRESSION = eINSTANCE.getReturnStatement_ReturnExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.statement.impl.StatementImpl <em>Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementImpl
		 * @see geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl#getStatement()
		 * @generated
		 */
		EClass STATEMENT = eINSTANCE.getStatement();

		/**
		 * The meta object literal for the '<em><b>Sequence Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATEMENT__SEQUENCE_NUMBER = eINSTANCE.getStatement_SequenceNumber();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATEMENT__LABEL = eINSTANCE.getStatement_Label();

	}

} //StatementPackage
