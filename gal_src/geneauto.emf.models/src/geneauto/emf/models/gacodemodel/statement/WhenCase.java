/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.gacodemodel.SequentialComposition;

import geneauto.emf.models.gacodemodel.expression.Expression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>When Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Single case of a CaseStatement. See also CaseStatement.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getWhen <em>When</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getWhenCase()
 * @model
 * @generated
 */
public interface WhenCase extends Statement, SequentialComposition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>When</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>When</em>' containment reference.
	 * @see #isSetWhen()
	 * @see #unsetWhen()
	 * @see #setWhen(Expression)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getWhenCase_When()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getWhen();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getWhen <em>When</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>When</em>' containment reference.
	 * @see #isSetWhen()
	 * @see #unsetWhen()
	 * @see #getWhen()
	 * @generated
	 */
	void setWhen(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getWhen <em>When</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetWhen()
	 * @see #getWhen()
	 * @see #setWhen(Expression)
	 * @generated
	 */
	void unsetWhen();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getWhen <em>When</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>When</em>' containment reference is set.
	 * @see #unsetWhen()
	 * @see #getWhen()
	 * @see #setWhen(Expression)
	 * @generated
	 */
	boolean isSetWhen();

	/**
	 * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.statement.Statement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statements</em>' containment reference list.
	 * @see #isSetStatements()
	 * @see #unsetStatements()
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getWhenCase_Statements()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Statement> getStatements();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getStatements <em>Statements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStatements()
	 * @see #getStatements()
	 * @generated
	 */
	void unsetStatements();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.WhenCase#getStatements <em>Statements</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Statements</em>' containment reference list is set.
	 * @see #unsetStatements()
	 * @see #getStatements()
	 * @generated
	 */
	boolean isSetStatements();

} // WhenCase
