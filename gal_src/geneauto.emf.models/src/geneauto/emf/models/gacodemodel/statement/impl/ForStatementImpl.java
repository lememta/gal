/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement.impl;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gacodemodel.statement.ForStatement;
import geneauto.emf.models.gacodemodel.statement.Statement;
import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.ForStatementImpl#getConditionExpression <em>Condition Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.ForStatementImpl#getPreStatement <em>Pre Statement</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.ForStatementImpl#getPostStatement <em>Post Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ForStatementImpl extends LoopStatementImpl implements ForStatement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getConditionExpression() <em>Condition Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression conditionExpression;

	/**
	 * This is true if the Condition Expression containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean conditionExpressionESet;

	/**
	 * The cached value of the '{@link #getPreStatement() <em>Pre Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreStatement()
	 * @generated
	 * @ordered
	 */
	protected Statement preStatement;

	/**
	 * This is true if the Pre Statement containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean preStatementESet;

	/**
	 * The cached value of the '{@link #getPostStatement() <em>Post Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostStatement()
	 * @generated
	 * @ordered
	 */
	protected Statement postStatement;

	/**
	 * This is true if the Post Statement containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean postStatementESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatementPackage.Literals.FOR_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getConditionExpression() {
		return conditionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConditionExpression(Expression newConditionExpression, NotificationChain msgs) {
		Expression oldConditionExpression = conditionExpression;
		conditionExpression = newConditionExpression;
		boolean oldConditionExpressionESet = conditionExpressionESet;
		conditionExpressionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION, oldConditionExpression, newConditionExpression, !oldConditionExpressionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionExpression(Expression newConditionExpression) {
		if (newConditionExpression != conditionExpression) {
			NotificationChain msgs = null;
			if (conditionExpression != null)
				msgs = ((InternalEObject)conditionExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION, null, msgs);
			if (newConditionExpression != null)
				msgs = ((InternalEObject)newConditionExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION, null, msgs);
			msgs = basicSetConditionExpression(newConditionExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldConditionExpressionESet = conditionExpressionESet;
			conditionExpressionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION, newConditionExpression, newConditionExpression, !oldConditionExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetConditionExpression(NotificationChain msgs) {
		Expression oldConditionExpression = conditionExpression;
		conditionExpression = null;
		boolean oldConditionExpressionESet = conditionExpressionESet;
		conditionExpressionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION, oldConditionExpression, null, oldConditionExpressionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConditionExpression() {
		if (conditionExpression != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)conditionExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION, null, msgs);
			msgs = basicUnsetConditionExpression(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldConditionExpressionESet = conditionExpressionESet;
			conditionExpressionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION, null, null, oldConditionExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConditionExpression() {
		return conditionExpressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Statement getPreStatement() {
		return preStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreStatement(Statement newPreStatement, NotificationChain msgs) {
		Statement oldPreStatement = preStatement;
		preStatement = newPreStatement;
		boolean oldPreStatementESet = preStatementESet;
		preStatementESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.FOR_STATEMENT__PRE_STATEMENT, oldPreStatement, newPreStatement, !oldPreStatementESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreStatement(Statement newPreStatement) {
		if (newPreStatement != preStatement) {
			NotificationChain msgs = null;
			if (preStatement != null)
				msgs = ((InternalEObject)preStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__PRE_STATEMENT, null, msgs);
			if (newPreStatement != null)
				msgs = ((InternalEObject)newPreStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__PRE_STATEMENT, null, msgs);
			msgs = basicSetPreStatement(newPreStatement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldPreStatementESet = preStatementESet;
			preStatementESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.FOR_STATEMENT__PRE_STATEMENT, newPreStatement, newPreStatement, !oldPreStatementESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetPreStatement(NotificationChain msgs) {
		Statement oldPreStatement = preStatement;
		preStatement = null;
		boolean oldPreStatementESet = preStatementESet;
		preStatementESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.FOR_STATEMENT__PRE_STATEMENT, oldPreStatement, null, oldPreStatementESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPreStatement() {
		if (preStatement != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)preStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__PRE_STATEMENT, null, msgs);
			msgs = basicUnsetPreStatement(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldPreStatementESet = preStatementESet;
			preStatementESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.FOR_STATEMENT__PRE_STATEMENT, null, null, oldPreStatementESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPreStatement() {
		return preStatementESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Statement getPostStatement() {
		return postStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPostStatement(Statement newPostStatement, NotificationChain msgs) {
		Statement oldPostStatement = postStatement;
		postStatement = newPostStatement;
		boolean oldPostStatementESet = postStatementESet;
		postStatementESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.FOR_STATEMENT__POST_STATEMENT, oldPostStatement, newPostStatement, !oldPostStatementESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostStatement(Statement newPostStatement) {
		if (newPostStatement != postStatement) {
			NotificationChain msgs = null;
			if (postStatement != null)
				msgs = ((InternalEObject)postStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__POST_STATEMENT, null, msgs);
			if (newPostStatement != null)
				msgs = ((InternalEObject)newPostStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__POST_STATEMENT, null, msgs);
			msgs = basicSetPostStatement(newPostStatement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldPostStatementESet = postStatementESet;
			postStatementESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.FOR_STATEMENT__POST_STATEMENT, newPostStatement, newPostStatement, !oldPostStatementESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetPostStatement(NotificationChain msgs) {
		Statement oldPostStatement = postStatement;
		postStatement = null;
		boolean oldPostStatementESet = postStatementESet;
		postStatementESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.FOR_STATEMENT__POST_STATEMENT, oldPostStatement, null, oldPostStatementESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPostStatement() {
		if (postStatement != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)postStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.FOR_STATEMENT__POST_STATEMENT, null, msgs);
			msgs = basicUnsetPostStatement(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldPostStatementESet = postStatementESet;
			postStatementESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.FOR_STATEMENT__POST_STATEMENT, null, null, oldPostStatementESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPostStatement() {
		return postStatementESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION:
				return basicUnsetConditionExpression(msgs);
			case StatementPackage.FOR_STATEMENT__PRE_STATEMENT:
				return basicUnsetPreStatement(msgs);
			case StatementPackage.FOR_STATEMENT__POST_STATEMENT:
				return basicUnsetPostStatement(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION:
				return getConditionExpression();
			case StatementPackage.FOR_STATEMENT__PRE_STATEMENT:
				return getPreStatement();
			case StatementPackage.FOR_STATEMENT__POST_STATEMENT:
				return getPostStatement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION:
				setConditionExpression((Expression)newValue);
				return;
			case StatementPackage.FOR_STATEMENT__PRE_STATEMENT:
				setPreStatement((Statement)newValue);
				return;
			case StatementPackage.FOR_STATEMENT__POST_STATEMENT:
				setPostStatement((Statement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION:
				unsetConditionExpression();
				return;
			case StatementPackage.FOR_STATEMENT__PRE_STATEMENT:
				unsetPreStatement();
				return;
			case StatementPackage.FOR_STATEMENT__POST_STATEMENT:
				unsetPostStatement();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatementPackage.FOR_STATEMENT__CONDITION_EXPRESSION:
				return isSetConditionExpression();
			case StatementPackage.FOR_STATEMENT__PRE_STATEMENT:
				return isSetPreStatement();
			case StatementPackage.FOR_STATEMENT__POST_STATEMENT:
				return isSetPostStatement();
		}
		return super.eIsSet(featureID);
	}

} //ForStatementImpl
