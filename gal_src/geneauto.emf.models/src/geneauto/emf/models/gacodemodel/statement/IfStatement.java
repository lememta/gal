/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.gacodemodel.expression.Expression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * If-Then-Else statement
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getConditionExpression <em>Condition Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getElseStatement <em>Else Statement</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getThenStatement <em>Then Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getIfStatement()
 * @model
 * @generated
 */
public interface IfStatement extends Statement, StatementWithBranches {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Condition Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Expression</em>' containment reference.
	 * @see #isSetConditionExpression()
	 * @see #unsetConditionExpression()
	 * @see #setConditionExpression(Expression)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getIfStatement_ConditionExpression()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getConditionExpression();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getConditionExpression <em>Condition Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Expression</em>' containment reference.
	 * @see #isSetConditionExpression()
	 * @see #unsetConditionExpression()
	 * @see #getConditionExpression()
	 * @generated
	 */
	void setConditionExpression(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getConditionExpression <em>Condition Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConditionExpression()
	 * @see #getConditionExpression()
	 * @see #setConditionExpression(Expression)
	 * @generated
	 */
	void unsetConditionExpression();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getConditionExpression <em>Condition Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Condition Expression</em>' containment reference is set.
	 * @see #unsetConditionExpression()
	 * @see #getConditionExpression()
	 * @see #setConditionExpression(Expression)
	 * @generated
	 */
	boolean isSetConditionExpression();

	/**
	 * Returns the value of the '<em><b>Else Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Statement</em>' containment reference.
	 * @see #isSetElseStatement()
	 * @see #unsetElseStatement()
	 * @see #setElseStatement(Statement)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getIfStatement_ElseStatement()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Statement getElseStatement();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getElseStatement <em>Else Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Statement</em>' containment reference.
	 * @see #isSetElseStatement()
	 * @see #unsetElseStatement()
	 * @see #getElseStatement()
	 * @generated
	 */
	void setElseStatement(Statement value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getElseStatement <em>Else Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElseStatement()
	 * @see #getElseStatement()
	 * @see #setElseStatement(Statement)
	 * @generated
	 */
	void unsetElseStatement();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getElseStatement <em>Else Statement</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Else Statement</em>' containment reference is set.
	 * @see #unsetElseStatement()
	 * @see #getElseStatement()
	 * @see #setElseStatement(Statement)
	 * @generated
	 */
	boolean isSetElseStatement();

	/**
	 * Returns the value of the '<em><b>Then Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Statement</em>' containment reference.
	 * @see #isSetThenStatement()
	 * @see #unsetThenStatement()
	 * @see #setThenStatement(Statement)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getIfStatement_ThenStatement()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Statement getThenStatement();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getThenStatement <em>Then Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Statement</em>' containment reference.
	 * @see #isSetThenStatement()
	 * @see #unsetThenStatement()
	 * @see #getThenStatement()
	 * @generated
	 */
	void setThenStatement(Statement value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getThenStatement <em>Then Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetThenStatement()
	 * @see #getThenStatement()
	 * @see #setThenStatement(Statement)
	 * @generated
	 */
	void unsetThenStatement();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.IfStatement#getThenStatement <em>Then Statement</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Then Statement</em>' containment reference is set.
	 * @see #unsetThenStatement()
	 * @see #getThenStatement()
	 * @see #setThenStatement(Statement)
	 * @generated
	 */
	boolean isSetThenStatement();

} // IfStatement
