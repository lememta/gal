/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.gacodemodel.expression.Expression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Case statement. Similar to the C switch, but a) has no fall through - all cases are implicitly terminated b) "when" expressions can be arbitrary expressions. Note: depending on the target language, not all expressions are legal. E.g. in C only singleton expressions are legal. However, in Ada a list expression is also legal.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getCondition <em>Condition</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getWhenCases <em>When Cases</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getDefaultCase <em>Default Case</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCaseStatement()
 * @model
 * @generated
 */
public interface CaseStatement extends Statement, StatementWithBranches {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #isSetCondition()
	 * @see #unsetCondition()
	 * @see #setCondition(Expression)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCaseStatement_Condition()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getCondition();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #isSetCondition()
	 * @see #unsetCondition()
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCondition()
	 * @see #getCondition()
	 * @see #setCondition(Expression)
	 * @generated
	 */
	void unsetCondition();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getCondition <em>Condition</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Condition</em>' containment reference is set.
	 * @see #unsetCondition()
	 * @see #getCondition()
	 * @see #setCondition(Expression)
	 * @generated
	 */
	boolean isSetCondition();

	/**
	 * Returns the value of the '<em><b>When Cases</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.statement.WhenCase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>When Cases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>When Cases</em>' containment reference list.
	 * @see #isSetWhenCases()
	 * @see #unsetWhenCases()
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCaseStatement_WhenCases()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<WhenCase> getWhenCases();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getWhenCases <em>When Cases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetWhenCases()
	 * @see #getWhenCases()
	 * @generated
	 */
	void unsetWhenCases();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getWhenCases <em>When Cases</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>When Cases</em>' containment reference list is set.
	 * @see #unsetWhenCases()
	 * @see #getWhenCases()
	 * @generated
	 */
	boolean isSetWhenCases();

	/**
	 * Returns the value of the '<em><b>Default Case</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Case</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Case</em>' containment reference.
	 * @see #isSetDefaultCase()
	 * @see #unsetDefaultCase()
	 * @see #setDefaultCase(DefaultCase)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCaseStatement_DefaultCase()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	DefaultCase getDefaultCase();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getDefaultCase <em>Default Case</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Case</em>' containment reference.
	 * @see #isSetDefaultCase()
	 * @see #unsetDefaultCase()
	 * @see #getDefaultCase()
	 * @generated
	 */
	void setDefaultCase(DefaultCase value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getDefaultCase <em>Default Case</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDefaultCase()
	 * @see #getDefaultCase()
	 * @see #setDefaultCase(DefaultCase)
	 * @generated
	 */
	void unsetDefaultCase();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.CaseStatement#getDefaultCase <em>Default Case</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Default Case</em>' containment reference is set.
	 * @see #unsetDefaultCase()
	 * @see #getDefaultCase()
	 * @see #setDefaultCase(DefaultCase)
	 * @generated
	 */
	boolean isSetDefaultCase();

} // CaseStatement
