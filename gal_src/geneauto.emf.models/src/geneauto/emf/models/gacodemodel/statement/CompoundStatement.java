/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.SequentialComposition;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compound Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Compound statement is a group of statements encapsulated in common local scope. The scope may contain local variables
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isAtomic <em>Is Atomic</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isVirtual <em>Is Virtual</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCompoundStatement()
 * @model
 * @generated
 */
public interface CompoundStatement extends Statement, HasNameSpace, SequentialComposition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Is Atomic</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Atomic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Atomic</em>' attribute.
	 * @see #isSetIsAtomic()
	 * @see #unsetIsAtomic()
	 * @see #setIsAtomic(boolean)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCompoundStatement_IsAtomic()
	 * @model default="true" unsettable="true"
	 * @generated
	 */
	boolean isAtomic();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isAtomic <em>Is Atomic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Atomic</em>' attribute.
	 * @see #isSetIsAtomic()
	 * @see #unsetIsAtomic()
	 * @see #isAtomic()
	 * @generated
	 */
	void setIsAtomic(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isAtomic <em>Is Atomic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsAtomic()
	 * @see #isAtomic()
	 * @see #setIsAtomic(boolean)
	 * @generated
	 */
	void unsetIsAtomic();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isAtomic <em>Is Atomic</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Atomic</em>' attribute is set.
	 * @see #unsetIsAtomic()
	 * @see #isAtomic()
	 * @see #setIsAtomic(boolean)
	 * @generated
	 */
	boolean isSetIsAtomic();

	/**
	 * Returns the value of the '<em><b>Is Virtual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Virtual</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Virtual</em>' attribute.
	 * @see #isSetIsVirtual()
	 * @see #unsetIsVirtual()
	 * @see #setIsVirtual(boolean)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCompoundStatement_IsVirtual()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isVirtual();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isVirtual <em>Is Virtual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Virtual</em>' attribute.
	 * @see #isSetIsVirtual()
	 * @see #unsetIsVirtual()
	 * @see #isVirtual()
	 * @generated
	 */
	void setIsVirtual(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isVirtual <em>Is Virtual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsVirtual()
	 * @see #isVirtual()
	 * @see #setIsVirtual(boolean)
	 * @generated
	 */
	void unsetIsVirtual();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#isVirtual <em>Is Virtual</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Virtual</em>' attribute is set.
	 * @see #unsetIsVirtual()
	 * @see #isVirtual()
	 * @see #setIsVirtual(boolean)
	 * @generated
	 */
	boolean isSetIsVirtual();

	/**
	 * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.statement.Statement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statements</em>' containment reference list.
	 * @see #isSetStatements()
	 * @see #unsetStatements()
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getCompoundStatement_Statements()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Statement> getStatements();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#getStatements <em>Statements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStatements()
	 * @see #getStatements()
	 * @generated
	 */
	void unsetStatements();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.CompoundStatement#getStatements <em>Statements</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Statements</em>' containment reference list is set.
	 * @see #unsetStatements()
	 * @see #getStatements()
	 * @generated
	 */
	boolean isSetStatements();

} // CompoundStatement
