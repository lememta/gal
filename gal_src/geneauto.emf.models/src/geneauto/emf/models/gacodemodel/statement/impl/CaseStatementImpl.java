/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement.impl;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gacodemodel.statement.CaseStatement;
import geneauto.emf.models.gacodemodel.statement.DefaultCase;
import geneauto.emf.models.gacodemodel.statement.StatementPackage;
import geneauto.emf.models.gacodemodel.statement.WhenCase;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Case Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.CaseStatementImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.CaseStatementImpl#getWhenCases <em>When Cases</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.CaseStatementImpl#getDefaultCase <em>Default Case</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CaseStatementImpl extends StatementImpl implements CaseStatement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected Expression condition;

	/**
	 * This is true if the Condition containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean conditionESet;

	/**
	 * The cached value of the '{@link #getWhenCases() <em>When Cases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhenCases()
	 * @generated
	 * @ordered
	 */
	protected EList<WhenCase> whenCases;

	/**
	 * The cached value of the '{@link #getDefaultCase() <em>Default Case</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultCase()
	 * @generated
	 * @ordered
	 */
	protected DefaultCase defaultCase;

	/**
	 * This is true if the Default Case containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean defaultCaseESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatementPackage.Literals.CASE_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(Expression newCondition, NotificationChain msgs) {
		Expression oldCondition = condition;
		condition = newCondition;
		boolean oldConditionESet = conditionESet;
		conditionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.CASE_STATEMENT__CONDITION, oldCondition, newCondition, !oldConditionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(Expression newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.CASE_STATEMENT__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.CASE_STATEMENT__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldConditionESet = conditionESet;
			conditionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.CASE_STATEMENT__CONDITION, newCondition, newCondition, !oldConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetCondition(NotificationChain msgs) {
		Expression oldCondition = condition;
		condition = null;
		boolean oldConditionESet = conditionESet;
		conditionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.CASE_STATEMENT__CONDITION, oldCondition, null, oldConditionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCondition() {
		if (condition != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.CASE_STATEMENT__CONDITION, null, msgs);
			msgs = basicUnsetCondition(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldConditionESet = conditionESet;
			conditionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.CASE_STATEMENT__CONDITION, null, null, oldConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCondition() {
		return conditionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WhenCase> getWhenCases() {
		if (whenCases == null) {
			whenCases = new EObjectContainmentEList.Unsettable<WhenCase>(WhenCase.class, this, StatementPackage.CASE_STATEMENT__WHEN_CASES);
		}
		return whenCases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetWhenCases() {
		if (whenCases != null) ((InternalEList.Unsettable<?>)whenCases).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetWhenCases() {
		return whenCases != null && ((InternalEList.Unsettable<?>)whenCases).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultCase getDefaultCase() {
		return defaultCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultCase(DefaultCase newDefaultCase, NotificationChain msgs) {
		DefaultCase oldDefaultCase = defaultCase;
		defaultCase = newDefaultCase;
		boolean oldDefaultCaseESet = defaultCaseESet;
		defaultCaseESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.CASE_STATEMENT__DEFAULT_CASE, oldDefaultCase, newDefaultCase, !oldDefaultCaseESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultCase(DefaultCase newDefaultCase) {
		if (newDefaultCase != defaultCase) {
			NotificationChain msgs = null;
			if (defaultCase != null)
				msgs = ((InternalEObject)defaultCase).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.CASE_STATEMENT__DEFAULT_CASE, null, msgs);
			if (newDefaultCase != null)
				msgs = ((InternalEObject)newDefaultCase).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.CASE_STATEMENT__DEFAULT_CASE, null, msgs);
			msgs = basicSetDefaultCase(newDefaultCase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDefaultCaseESet = defaultCaseESet;
			defaultCaseESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.CASE_STATEMENT__DEFAULT_CASE, newDefaultCase, newDefaultCase, !oldDefaultCaseESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDefaultCase(NotificationChain msgs) {
		DefaultCase oldDefaultCase = defaultCase;
		defaultCase = null;
		boolean oldDefaultCaseESet = defaultCaseESet;
		defaultCaseESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.CASE_STATEMENT__DEFAULT_CASE, oldDefaultCase, null, oldDefaultCaseESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDefaultCase() {
		if (defaultCase != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)defaultCase).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.CASE_STATEMENT__DEFAULT_CASE, null, msgs);
			msgs = basicUnsetDefaultCase(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDefaultCaseESet = defaultCaseESet;
			defaultCaseESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.CASE_STATEMENT__DEFAULT_CASE, null, null, oldDefaultCaseESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDefaultCase() {
		return defaultCaseESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatementPackage.CASE_STATEMENT__CONDITION:
				return basicUnsetCondition(msgs);
			case StatementPackage.CASE_STATEMENT__WHEN_CASES:
				return ((InternalEList<?>)getWhenCases()).basicRemove(otherEnd, msgs);
			case StatementPackage.CASE_STATEMENT__DEFAULT_CASE:
				return basicUnsetDefaultCase(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatementPackage.CASE_STATEMENT__CONDITION:
				return getCondition();
			case StatementPackage.CASE_STATEMENT__WHEN_CASES:
				return getWhenCases();
			case StatementPackage.CASE_STATEMENT__DEFAULT_CASE:
				return getDefaultCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatementPackage.CASE_STATEMENT__CONDITION:
				setCondition((Expression)newValue);
				return;
			case StatementPackage.CASE_STATEMENT__WHEN_CASES:
				getWhenCases().clear();
				getWhenCases().addAll((Collection<? extends WhenCase>)newValue);
				return;
			case StatementPackage.CASE_STATEMENT__DEFAULT_CASE:
				setDefaultCase((DefaultCase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatementPackage.CASE_STATEMENT__CONDITION:
				unsetCondition();
				return;
			case StatementPackage.CASE_STATEMENT__WHEN_CASES:
				unsetWhenCases();
				return;
			case StatementPackage.CASE_STATEMENT__DEFAULT_CASE:
				unsetDefaultCase();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatementPackage.CASE_STATEMENT__CONDITION:
				return isSetCondition();
			case StatementPackage.CASE_STATEMENT__WHEN_CASES:
				return isSetWhenCases();
			case StatementPackage.CASE_STATEMENT__DEFAULT_CASE:
				return isSetDefaultCase();
		}
		return super.eIsSet(featureID);
	}

} //CaseStatementImpl
