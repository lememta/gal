/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement.impl;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gacodemodel.statement.ReturnStatement;
import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Return Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.ReturnStatementImpl#getReturnExpression <em>Return Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReturnStatementImpl extends StatementImpl implements ReturnStatement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getReturnExpression() <em>Return Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression returnExpression;

	/**
	 * This is true if the Return Expression containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean returnExpressionESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReturnStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatementPackage.Literals.RETURN_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getReturnExpression() {
		return returnExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnExpression(Expression newReturnExpression, NotificationChain msgs) {
		Expression oldReturnExpression = returnExpression;
		returnExpression = newReturnExpression;
		boolean oldReturnExpressionESet = returnExpressionESet;
		returnExpressionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION, oldReturnExpression, newReturnExpression, !oldReturnExpressionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnExpression(Expression newReturnExpression) {
		if (newReturnExpression != returnExpression) {
			NotificationChain msgs = null;
			if (returnExpression != null)
				msgs = ((InternalEObject)returnExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION, null, msgs);
			if (newReturnExpression != null)
				msgs = ((InternalEObject)newReturnExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION, null, msgs);
			msgs = basicSetReturnExpression(newReturnExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldReturnExpressionESet = returnExpressionESet;
			returnExpressionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION, newReturnExpression, newReturnExpression, !oldReturnExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetReturnExpression(NotificationChain msgs) {
		Expression oldReturnExpression = returnExpression;
		returnExpression = null;
		boolean oldReturnExpressionESet = returnExpressionESet;
		returnExpressionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION, oldReturnExpression, null, oldReturnExpressionESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReturnExpression() {
		if (returnExpression != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)returnExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION, null, msgs);
			msgs = basicUnsetReturnExpression(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldReturnExpressionESet = returnExpressionESet;
			returnExpressionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION, null, null, oldReturnExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReturnExpression() {
		return returnExpressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION:
				return basicUnsetReturnExpression(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION:
				return getReturnExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION:
				setReturnExpression((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION:
				unsetReturnExpression();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatementPackage.RETURN_STATEMENT__RETURN_EXPRESSION:
				return isSetReturnExpression();
		}
		return super.eIsSet(featureID);
	}

} //ReturnStatementImpl
