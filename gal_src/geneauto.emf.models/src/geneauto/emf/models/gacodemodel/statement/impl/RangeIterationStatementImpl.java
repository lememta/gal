/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement.impl;

import geneauto.emf.models.common.HasVariable;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.NameSpace;
import geneauto.emf.models.gacodemodel.Variable_CM;

import geneauto.emf.models.gacodemodel.expression.RangeExpression;

import geneauto.emf.models.gacodemodel.statement.RangeIterationStatement;
import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Range Iteration Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.RangeIterationStatementImpl#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.RangeIterationStatementImpl#getIteratorVariable <em>Iterator Variable</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.impl.RangeIterationStatementImpl#getRange <em>Range</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RangeIterationStatementImpl extends LoopStatementImpl implements RangeIterationStatement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getNameSpace() <em>Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpace()
	 * @generated
	 * @ordered
	 */
	protected NameSpace nameSpace;

	/**
	 * This is true if the Name Space containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameSpaceESet;

	/**
	 * The cached value of the '{@link #getIteratorVariable() <em>Iterator Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIteratorVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable_CM iteratorVariable;

	/**
	 * This is true if the Iterator Variable containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iteratorVariableESet;

	/**
	 * The cached value of the '{@link #getRange() <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected RangeExpression range;

	/**
	 * This is true if the Range containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean rangeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RangeIterationStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatementPackage.Literals.RANGE_ITERATION_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace getNameSpace() {
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameSpace(NameSpace newNameSpace, NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = newNameSpace;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE, oldNameSpace, newNameSpace, !oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameSpace(NameSpace newNameSpace) {
		if (newNameSpace != nameSpace) {
			NotificationChain msgs = null;
			if (nameSpace != null)
				msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE, null, msgs);
			if (newNameSpace != null)
				msgs = ((InternalEObject)newNameSpace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE, null, msgs);
			msgs = basicSetNameSpace(newNameSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE, newNameSpace, newNameSpace, !oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetNameSpace(NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = null;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE, oldNameSpace, null, oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNameSpace() {
		if (nameSpace != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE, null, msgs);
			msgs = basicUnsetNameSpace(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE, null, null, oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNameSpace() {
		return nameSpaceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable_CM getIteratorVariable() {
		return iteratorVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIteratorVariable(Variable_CM newIteratorVariable, NotificationChain msgs) {
		Variable_CM oldIteratorVariable = iteratorVariable;
		iteratorVariable = newIteratorVariable;
		boolean oldIteratorVariableESet = iteratorVariableESet;
		iteratorVariableESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE, oldIteratorVariable, newIteratorVariable, !oldIteratorVariableESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIteratorVariable(Variable_CM newIteratorVariable) {
		if (newIteratorVariable != iteratorVariable) {
			NotificationChain msgs = null;
			if (iteratorVariable != null)
				msgs = ((InternalEObject)iteratorVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE, null, msgs);
			if (newIteratorVariable != null)
				msgs = ((InternalEObject)newIteratorVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE, null, msgs);
			msgs = basicSetIteratorVariable(newIteratorVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldIteratorVariableESet = iteratorVariableESet;
			iteratorVariableESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE, newIteratorVariable, newIteratorVariable, !oldIteratorVariableESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetIteratorVariable(NotificationChain msgs) {
		Variable_CM oldIteratorVariable = iteratorVariable;
		iteratorVariable = null;
		boolean oldIteratorVariableESet = iteratorVariableESet;
		iteratorVariableESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE, oldIteratorVariable, null, oldIteratorVariableESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIteratorVariable() {
		if (iteratorVariable != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)iteratorVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE, null, msgs);
			msgs = basicUnsetIteratorVariable(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldIteratorVariableESet = iteratorVariableESet;
			iteratorVariableESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE, null, null, oldIteratorVariableESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIteratorVariable() {
		return iteratorVariableESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeExpression getRange() {
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRange(RangeExpression newRange, NotificationChain msgs) {
		RangeExpression oldRange = range;
		range = newRange;
		boolean oldRangeESet = rangeESet;
		rangeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementPackage.RANGE_ITERATION_STATEMENT__RANGE, oldRange, newRange, !oldRangeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRange(RangeExpression newRange) {
		if (newRange != range) {
			NotificationChain msgs = null;
			if (range != null)
				msgs = ((InternalEObject)range).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__RANGE, null, msgs);
			if (newRange != null)
				msgs = ((InternalEObject)newRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__RANGE, null, msgs);
			msgs = basicSetRange(newRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldRangeESet = rangeESet;
			rangeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, StatementPackage.RANGE_ITERATION_STATEMENT__RANGE, newRange, newRange, !oldRangeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetRange(NotificationChain msgs) {
		RangeExpression oldRange = range;
		range = null;
		boolean oldRangeESet = rangeESet;
		rangeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, StatementPackage.RANGE_ITERATION_STATEMENT__RANGE, oldRange, null, oldRangeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRange() {
		if (range != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)range).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementPackage.RANGE_ITERATION_STATEMENT__RANGE, null, msgs);
			msgs = basicUnsetRange(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldRangeESet = rangeESet;
			rangeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, StatementPackage.RANGE_ITERATION_STATEMENT__RANGE, null, null, oldRangeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRange() {
		return rangeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE:
				return basicUnsetNameSpace(msgs);
			case StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE:
				return basicUnsetIteratorVariable(msgs);
			case StatementPackage.RANGE_ITERATION_STATEMENT__RANGE:
				return basicUnsetRange(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE:
				return getNameSpace();
			case StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE:
				return getIteratorVariable();
			case StatementPackage.RANGE_ITERATION_STATEMENT__RANGE:
				return getRange();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE:
				setNameSpace((NameSpace)newValue);
				return;
			case StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE:
				setIteratorVariable((Variable_CM)newValue);
				return;
			case StatementPackage.RANGE_ITERATION_STATEMENT__RANGE:
				setRange((RangeExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE:
				unsetNameSpace();
				return;
			case StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE:
				unsetIteratorVariable();
				return;
			case StatementPackage.RANGE_ITERATION_STATEMENT__RANGE:
				unsetRange();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE:
				return isSetNameSpace();
			case StatementPackage.RANGE_ITERATION_STATEMENT__ITERATOR_VARIABLE:
				return isSetIteratorVariable();
			case StatementPackage.RANGE_ITERATION_STATEMENT__RANGE:
				return isSetRange();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == HasNameSpace.class) {
			switch (derivedFeatureID) {
				case StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE: return GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE;
				default: return -1;
			}
		}
		if (baseClass == HasVariable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == HasNameSpace.class) {
			switch (baseFeatureID) {
				case GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE: return StatementPackage.RANGE_ITERATION_STATEMENT__NAME_SPACE;
				default: return -1;
			}
		}
		if (baseClass == HasVariable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //RangeIterationStatementImpl
