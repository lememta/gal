/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.gacodemodel.expression.Expression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * ForStatement implements For-loop in the program code The components of for statement are:
 * <ul>
 * <li>preStatement -- a statement executed before entering the first loop</li>
 * <li>postStatement -- a statement executed after each loop </li>
 * <li>conditionalExpression -- an expression to determine the terminate condition </li>
 * <li>bodyStatement -- a statement to execute as loop body</li>
 * </ul>
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getConditionExpression <em>Condition Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPreStatement <em>Pre Statement</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPostStatement <em>Post Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getForStatement()
 * @model
 * @generated
 */
public interface ForStatement extends LoopStatement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Condition Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Expression</em>' containment reference.
	 * @see #isSetConditionExpression()
	 * @see #unsetConditionExpression()
	 * @see #setConditionExpression(Expression)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getForStatement_ConditionExpression()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getConditionExpression();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getConditionExpression <em>Condition Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Expression</em>' containment reference.
	 * @see #isSetConditionExpression()
	 * @see #unsetConditionExpression()
	 * @see #getConditionExpression()
	 * @generated
	 */
	void setConditionExpression(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getConditionExpression <em>Condition Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConditionExpression()
	 * @see #getConditionExpression()
	 * @see #setConditionExpression(Expression)
	 * @generated
	 */
	void unsetConditionExpression();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getConditionExpression <em>Condition Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Condition Expression</em>' containment reference is set.
	 * @see #unsetConditionExpression()
	 * @see #getConditionExpression()
	 * @see #setConditionExpression(Expression)
	 * @generated
	 */
	boolean isSetConditionExpression();

	/**
	 * Returns the value of the '<em><b>Pre Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pre Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Statement</em>' containment reference.
	 * @see #isSetPreStatement()
	 * @see #unsetPreStatement()
	 * @see #setPreStatement(Statement)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getForStatement_PreStatement()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Statement getPreStatement();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPreStatement <em>Pre Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Statement</em>' containment reference.
	 * @see #isSetPreStatement()
	 * @see #unsetPreStatement()
	 * @see #getPreStatement()
	 * @generated
	 */
	void setPreStatement(Statement value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPreStatement <em>Pre Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreStatement()
	 * @see #getPreStatement()
	 * @see #setPreStatement(Statement)
	 * @generated
	 */
	void unsetPreStatement();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPreStatement <em>Pre Statement</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Pre Statement</em>' containment reference is set.
	 * @see #unsetPreStatement()
	 * @see #getPreStatement()
	 * @see #setPreStatement(Statement)
	 * @generated
	 */
	boolean isSetPreStatement();

	/**
	 * Returns the value of the '<em><b>Post Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Post Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Post Statement</em>' containment reference.
	 * @see #isSetPostStatement()
	 * @see #unsetPostStatement()
	 * @see #setPostStatement(Statement)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getForStatement_PostStatement()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Statement getPostStatement();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPostStatement <em>Post Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Post Statement</em>' containment reference.
	 * @see #isSetPostStatement()
	 * @see #unsetPostStatement()
	 * @see #getPostStatement()
	 * @generated
	 */
	void setPostStatement(Statement value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPostStatement <em>Post Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPostStatement()
	 * @see #getPostStatement()
	 * @see #setPostStatement(Statement)
	 * @generated
	 */
	void unsetPostStatement();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.ForStatement#getPostStatement <em>Post Statement</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Post Statement</em>' containment reference is set.
	 * @see #unsetPostStatement()
	 * @see #getPostStatement()
	 * @see #setPostStatement(Statement)
	 * @generated
	 */
	boolean isSetPostStatement();

} // ForStatement
