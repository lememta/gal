/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.statement;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gacodemodel.operator.AssignOperator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assign Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * AssignStatement implements all types of assignments (=, +=, *= etc.) NB! the class assumes that the operator is fixed in constructor and is not changed later. That is why the setter for operator is not provided.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getRightExpression <em>Right Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getLeftExpression <em>Left Expression</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getOperator <em>Operator</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getAssignStatement()
 * @model
 * @generated
 */
public interface AssignStatement extends Statement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Right Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Expression</em>' containment reference.
	 * @see #isSetRightExpression()
	 * @see #unsetRightExpression()
	 * @see #setRightExpression(Expression)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getAssignStatement_RightExpression()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getRightExpression();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getRightExpression <em>Right Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Expression</em>' containment reference.
	 * @see #isSetRightExpression()
	 * @see #unsetRightExpression()
	 * @see #getRightExpression()
	 * @generated
	 */
	void setRightExpression(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getRightExpression <em>Right Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRightExpression()
	 * @see #getRightExpression()
	 * @see #setRightExpression(Expression)
	 * @generated
	 */
	void unsetRightExpression();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getRightExpression <em>Right Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Right Expression</em>' containment reference is set.
	 * @see #unsetRightExpression()
	 * @see #getRightExpression()
	 * @see #setRightExpression(Expression)
	 * @generated
	 */
	boolean isSetRightExpression();

	/**
	 * Returns the value of the '<em><b>Left Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Expression</em>' containment reference.
	 * @see #isSetLeftExpression()
	 * @see #unsetLeftExpression()
	 * @see #setLeftExpression(Expression)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getAssignStatement_LeftExpression()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getLeftExpression();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getLeftExpression <em>Left Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Expression</em>' containment reference.
	 * @see #isSetLeftExpression()
	 * @see #unsetLeftExpression()
	 * @see #getLeftExpression()
	 * @generated
	 */
	void setLeftExpression(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getLeftExpression <em>Left Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLeftExpression()
	 * @see #getLeftExpression()
	 * @see #setLeftExpression(Expression)
	 * @generated
	 */
	void unsetLeftExpression();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getLeftExpression <em>Left Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Left Expression</em>' containment reference is set.
	 * @see #unsetLeftExpression()
	 * @see #getLeftExpression()
	 * @see #setLeftExpression(Expression)
	 * @generated
	 */
	boolean isSetLeftExpression();

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gacodemodel.operator.AssignOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.operator.AssignOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #setOperator(AssignOperator)
	 * @see geneauto.emf.models.gacodemodel.statement.StatementPackage#getAssignStatement_Operator()
	 * @model unsettable="true"
	 * @generated
	 */
	AssignOperator getOperator();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.operator.AssignOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(AssignOperator value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperator()
	 * @see #getOperator()
	 * @see #setOperator(AssignOperator)
	 * @generated
	 */
	void unsetOperator();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.statement.AssignStatement#getOperator <em>Operator</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operator</em>' attribute is set.
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @see #setOperator(AssignOperator)
	 * @generated
	 */
	boolean isSetOperator();

} // AssignStatement
