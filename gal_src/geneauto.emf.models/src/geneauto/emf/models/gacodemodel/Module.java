/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.genericmodel.Annotation;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Abstract entity representing a software module.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.Module#getHeaderFileName <em>Header File Name</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.Module#getSourceFileName <em>Source File Name</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.Module#getInitFunction <em>Init Function</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.Module#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.Module#getHeaderAnnotation <em>Header Annotation</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getModule()
 * @model
 * @generated
 */
public interface Module extends GACodeModelElement, GACodeModelRoot, HasNameSpace {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Header File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header File Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header File Name</em>' attribute.
	 * @see #isSetHeaderFileName()
	 * @see #unsetHeaderFileName()
	 * @see #setHeaderFileName(String)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getModule_HeaderFileName()
	 * @model unsettable="true"
	 * @generated
	 */
	String getHeaderFileName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getHeaderFileName <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header File Name</em>' attribute.
	 * @see #isSetHeaderFileName()
	 * @see #unsetHeaderFileName()
	 * @see #getHeaderFileName()
	 * @generated
	 */
	void setHeaderFileName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getHeaderFileName <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHeaderFileName()
	 * @see #getHeaderFileName()
	 * @see #setHeaderFileName(String)
	 * @generated
	 */
	void unsetHeaderFileName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Module#getHeaderFileName <em>Header File Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Header File Name</em>' attribute is set.
	 * @see #unsetHeaderFileName()
	 * @see #getHeaderFileName()
	 * @see #setHeaderFileName(String)
	 * @generated
	 */
	boolean isSetHeaderFileName();

	/**
	 * Returns the value of the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source File Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source File Name</em>' attribute.
	 * @see #isSetSourceFileName()
	 * @see #unsetSourceFileName()
	 * @see #setSourceFileName(String)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getModule_SourceFileName()
	 * @model unsettable="true"
	 * @generated
	 */
	String getSourceFileName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getSourceFileName <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source File Name</em>' attribute.
	 * @see #isSetSourceFileName()
	 * @see #unsetSourceFileName()
	 * @see #getSourceFileName()
	 * @generated
	 */
	void setSourceFileName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getSourceFileName <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSourceFileName()
	 * @see #getSourceFileName()
	 * @see #setSourceFileName(String)
	 * @generated
	 */
	void unsetSourceFileName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Module#getSourceFileName <em>Source File Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Source File Name</em>' attribute is set.
	 * @see #unsetSourceFileName()
	 * @see #getSourceFileName()
	 * @see #setSourceFileName(String)
	 * @generated
	 */
	boolean isSetSourceFileName();

	/**
	 * Returns the value of the '<em><b>Init Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Function</em>' reference.
	 * @see #isSetInitFunction()
	 * @see #unsetInitFunction()
	 * @see #setInitFunction(Function_CM)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getModule_InitFunction()
	 * @model unsettable="true"
	 * @generated
	 */
	Function_CM getInitFunction();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getInitFunction <em>Init Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Function</em>' reference.
	 * @see #isSetInitFunction()
	 * @see #unsetInitFunction()
	 * @see #getInitFunction()
	 * @generated
	 */
	void setInitFunction(Function_CM value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getInitFunction <em>Init Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInitFunction()
	 * @see #getInitFunction()
	 * @see #setInitFunction(Function_CM)
	 * @generated
	 */
	void unsetInitFunction();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Module#getInitFunction <em>Init Function</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Init Function</em>' reference is set.
	 * @see #unsetInitFunction()
	 * @see #getInitFunction()
	 * @see #setInitFunction(Function_CM)
	 * @generated
	 */
	boolean isSetInitFunction();

	/**
	 * Returns the value of the '<em><b>Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.Dependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependencies</em>' containment reference list.
	 * @see #isSetDependencies()
	 * @see #unsetDependencies()
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getModule_Dependencies()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Dependency> getDependencies();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getDependencies <em>Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDependencies()
	 * @see #getDependencies()
	 * @generated
	 */
	void unsetDependencies();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Module#getDependencies <em>Dependencies</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Dependencies</em>' containment reference list is set.
	 * @see #unsetDependencies()
	 * @see #getDependencies()
	 * @generated
	 */
	boolean isSetDependencies();

	/**
	 * Returns the value of the '<em><b>Header Annotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Annotation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Annotation</em>' containment reference.
	 * @see #isSetHeaderAnnotation()
	 * @see #unsetHeaderAnnotation()
	 * @see #setHeaderAnnotation(Annotation)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getModule_HeaderAnnotation()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Annotation getHeaderAnnotation();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getHeaderAnnotation <em>Header Annotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header Annotation</em>' containment reference.
	 * @see #isSetHeaderAnnotation()
	 * @see #unsetHeaderAnnotation()
	 * @see #getHeaderAnnotation()
	 * @generated
	 */
	void setHeaderAnnotation(Annotation value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Module#getHeaderAnnotation <em>Header Annotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHeaderAnnotation()
	 * @see #getHeaderAnnotation()
	 * @see #setHeaderAnnotation(Annotation)
	 * @generated
	 */
	void unsetHeaderAnnotation();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Module#getHeaderAnnotation <em>Header Annotation</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Header Annotation</em>' containment reference is set.
	 * @see #unsetHeaderAnnotation()
	 * @see #getHeaderAnnotation()
	 * @see #setHeaderAnnotation(Annotation)
	 * @generated
	 */
	boolean isSetHeaderAnnotation();

} // Module
