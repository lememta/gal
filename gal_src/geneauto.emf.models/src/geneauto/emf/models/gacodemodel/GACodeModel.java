/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.gasystemmodel.GASystemModel;

import geneauto.emf.models.genericmodel.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GA Code Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.GACodeModel#getSystemModel <em>System Model</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.GACodeModel#getTempModel <em>Temp Model</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.GACodeModel#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModel()
 * @model
 * @generated
 */
public interface GACodeModel extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>System Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Model</em>' reference.
	 * @see #isSetSystemModel()
	 * @see #unsetSystemModel()
	 * @see #setSystemModel(GASystemModel)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModel_SystemModel()
	 * @model unsettable="true"
	 * @generated
	 */
	GASystemModel getSystemModel();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getSystemModel <em>System Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Model</em>' reference.
	 * @see #isSetSystemModel()
	 * @see #unsetSystemModel()
	 * @see #getSystemModel()
	 * @generated
	 */
	void setSystemModel(GASystemModel value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getSystemModel <em>System Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSystemModel()
	 * @see #getSystemModel()
	 * @see #setSystemModel(GASystemModel)
	 * @generated
	 */
	void unsetSystemModel();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getSystemModel <em>System Model</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>System Model</em>' reference is set.
	 * @see #unsetSystemModel()
	 * @see #getSystemModel()
	 * @see #setSystemModel(GASystemModel)
	 * @generated
	 */
	boolean isSetSystemModel();

	/**
	 * Returns the value of the '<em><b>Temp Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temp Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temp Model</em>' reference.
	 * @see #isSetTempModel()
	 * @see #unsetTempModel()
	 * @see #setTempModel(TempModel)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModel_TempModel()
	 * @model unsettable="true"
	 * @generated
	 */
	TempModel getTempModel();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getTempModel <em>Temp Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temp Model</em>' reference.
	 * @see #isSetTempModel()
	 * @see #unsetTempModel()
	 * @see #getTempModel()
	 * @generated
	 */
	void setTempModel(TempModel value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getTempModel <em>Temp Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTempModel()
	 * @see #getTempModel()
	 * @see #setTempModel(TempModel)
	 * @generated
	 */
	void unsetTempModel();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getTempModel <em>Temp Model</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Temp Model</em>' reference is set.
	 * @see #unsetTempModel()
	 * @see #getTempModel()
	 * @see #setTempModel(TempModel)
	 * @generated
	 */
	boolean isSetTempModel();

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.GACodeModelRoot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see #isSetElements()
	 * @see #unsetElements()
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModel_Elements()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<GACodeModelRoot> getElements();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getElements <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElements()
	 * @see #getElements()
	 * @generated
	 */
	void unsetElements();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModel#getElements <em>Elements</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Elements</em>' containment reference list is set.
	 * @see #unsetElements()
	 * @see #getElements()
	 * @generated
	 */
	boolean isSetElements();

} // GACodeModel
