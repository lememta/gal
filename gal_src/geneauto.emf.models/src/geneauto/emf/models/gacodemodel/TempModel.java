/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.genericmodel.GAModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Temp Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Temporary storage for model elements that are created during the code-generation transformations and need to be stored temporarily for the next transformation steps.  The elements contained in TempModel are ignored when performing optimisation and printing output code
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.TempModel#getStateVariablesStructType <em>State Variables Struct Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.TempModel#getTempNameSpace <em>Temp Name Space</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.TempModel#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getTempModel()
 * @model
 * @generated
 */
public interface TempModel extends GACodeModelElement, GACodeModelRoot {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>State Variables Struct Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Variables Struct Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Variables Struct Type</em>' reference.
	 * @see #isSetStateVariablesStructType()
	 * @see #unsetStateVariablesStructType()
	 * @see #setStateVariablesStructType(CustomType_CM)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getTempModel_StateVariablesStructType()
	 * @model unsettable="true"
	 * @generated
	 */
	CustomType_CM getStateVariablesStructType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getStateVariablesStructType <em>State Variables Struct Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Variables Struct Type</em>' reference.
	 * @see #isSetStateVariablesStructType()
	 * @see #unsetStateVariablesStructType()
	 * @see #getStateVariablesStructType()
	 * @generated
	 */
	void setStateVariablesStructType(CustomType_CM value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getStateVariablesStructType <em>State Variables Struct Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStateVariablesStructType()
	 * @see #getStateVariablesStructType()
	 * @see #setStateVariablesStructType(CustomType_CM)
	 * @generated
	 */
	void unsetStateVariablesStructType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getStateVariablesStructType <em>State Variables Struct Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>State Variables Struct Type</em>' reference is set.
	 * @see #unsetStateVariablesStructType()
	 * @see #getStateVariablesStructType()
	 * @see #setStateVariablesStructType(CustomType_CM)
	 * @generated
	 */
	boolean isSetStateVariablesStructType();

	/**
	 * Returns the value of the '<em><b>Temp Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temp Name Space</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temp Name Space</em>' containment reference.
	 * @see #isSetTempNameSpace()
	 * @see #unsetTempNameSpace()
	 * @see #setTempNameSpace(NameSpace)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getTempModel_TempNameSpace()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	NameSpace getTempNameSpace();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getTempNameSpace <em>Temp Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temp Name Space</em>' containment reference.
	 * @see #isSetTempNameSpace()
	 * @see #unsetTempNameSpace()
	 * @see #getTempNameSpace()
	 * @generated
	 */
	void setTempNameSpace(NameSpace value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getTempNameSpace <em>Temp Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTempNameSpace()
	 * @see #getTempNameSpace()
	 * @see #setTempNameSpace(NameSpace)
	 * @generated
	 */
	void unsetTempNameSpace();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getTempNameSpace <em>Temp Name Space</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Temp Name Space</em>' containment reference is set.
	 * @see #unsetTempNameSpace()
	 * @see #getTempNameSpace()
	 * @see #setTempNameSpace(NameSpace)
	 * @generated
	 */
	boolean isSetTempNameSpace();

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.genericmodel.GAModelElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see #isSetElements()
	 * @see #unsetElements()
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getTempModel_Elements()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<GAModelElement> getElements();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getElements <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetElements()
	 * @see #getElements()
	 * @generated
	 */
	void unsetElements();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.TempModel#getElements <em>Elements</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Elements</em>' containment reference list is set.
	 * @see #unsetElements()
	 * @see #getElements()
	 * @generated
	 */
	boolean isSetElements();

} // TempModel
