/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression.impl;

import geneauto.emf.models.gacodemodel.expression.Expression;
import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;
import geneauto.emf.models.gacodemodel.expression.TernaryExpression;

import geneauto.emf.models.gacodemodel.operator.TernaryOperator;

import geneauto.emf.models.gadatatypes.GADataType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ternary Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl#getFirst <em>First</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl#getSecond <em>Second</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl#getThird <em>Third</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TernaryExpressionImpl extends ExpressionImpl implements TernaryExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final TernaryOperator OPERATOR_EDEFAULT = TernaryOperator.TERNARY_IF_OPERATOR;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected TernaryOperator operator = OPERATOR_EDEFAULT;

	/**
	 * This is true if the Operator attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean operatorESet;

	/**
	 * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected Expression first;

	/**
	 * This is true if the First containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean firstESet;

	/**
	 * The cached value of the '{@link #getSecond() <em>Second</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecond()
	 * @generated
	 * @ordered
	 */
	protected Expression second;

	/**
	 * This is true if the Second containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean secondESet;

	/**
	 * The cached value of the '{@link #getThird() <em>Third</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThird()
	 * @generated
	 * @ordered
	 */
	protected Expression third;

	/**
	 * This is true if the Third containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean thirdESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TernaryExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionPackage.Literals.TERNARY_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TernaryOperator getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperator(TernaryOperator newOperator) {
		TernaryOperator oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		boolean oldOperatorESet = operatorESet;
		operatorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionPackage.TERNARY_EXPRESSION__OPERATOR, oldOperator, operator, !oldOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOperator() {
		TernaryOperator oldOperator = operator;
		boolean oldOperatorESet = operatorESet;
		operator = OPERATOR_EDEFAULT;
		operatorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.TERNARY_EXPRESSION__OPERATOR, oldOperator, OPERATOR_EDEFAULT, oldOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperator() {
		return operatorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getFirst() {
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirst(Expression newFirst, NotificationChain msgs) {
		Expression oldFirst = first;
		first = newFirst;
		boolean oldFirstESet = firstESet;
		firstESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionPackage.TERNARY_EXPRESSION__FIRST, oldFirst, newFirst, !oldFirstESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirst(Expression newFirst) {
		if (newFirst != first) {
			NotificationChain msgs = null;
			if (first != null)
				msgs = ((InternalEObject)first).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__FIRST, null, msgs);
			if (newFirst != null)
				msgs = ((InternalEObject)newFirst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__FIRST, null, msgs);
			msgs = basicSetFirst(newFirst, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldFirstESet = firstESet;
			firstESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionPackage.TERNARY_EXPRESSION__FIRST, newFirst, newFirst, !oldFirstESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetFirst(NotificationChain msgs) {
		Expression oldFirst = first;
		first = null;
		boolean oldFirstESet = firstESet;
		firstESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.TERNARY_EXPRESSION__FIRST, oldFirst, null, oldFirstESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFirst() {
		if (first != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)first).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__FIRST, null, msgs);
			msgs = basicUnsetFirst(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldFirstESet = firstESet;
			firstESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.TERNARY_EXPRESSION__FIRST, null, null, oldFirstESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFirst() {
		return firstESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getSecond() {
		return second;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSecond(Expression newSecond, NotificationChain msgs) {
		Expression oldSecond = second;
		second = newSecond;
		boolean oldSecondESet = secondESet;
		secondESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionPackage.TERNARY_EXPRESSION__SECOND, oldSecond, newSecond, !oldSecondESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecond(Expression newSecond) {
		if (newSecond != second) {
			NotificationChain msgs = null;
			if (second != null)
				msgs = ((InternalEObject)second).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__SECOND, null, msgs);
			if (newSecond != null)
				msgs = ((InternalEObject)newSecond).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__SECOND, null, msgs);
			msgs = basicSetSecond(newSecond, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldSecondESet = secondESet;
			secondESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionPackage.TERNARY_EXPRESSION__SECOND, newSecond, newSecond, !oldSecondESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetSecond(NotificationChain msgs) {
		Expression oldSecond = second;
		second = null;
		boolean oldSecondESet = secondESet;
		secondESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.TERNARY_EXPRESSION__SECOND, oldSecond, null, oldSecondESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSecond() {
		if (second != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)second).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__SECOND, null, msgs);
			msgs = basicUnsetSecond(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldSecondESet = secondESet;
			secondESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.TERNARY_EXPRESSION__SECOND, null, null, oldSecondESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSecond() {
		return secondESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getThird() {
		return third;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThird(Expression newThird, NotificationChain msgs) {
		Expression oldThird = third;
		third = newThird;
		boolean oldThirdESet = thirdESet;
		thirdESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionPackage.TERNARY_EXPRESSION__THIRD, oldThird, newThird, !oldThirdESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThird(Expression newThird) {
		if (newThird != third) {
			NotificationChain msgs = null;
			if (third != null)
				msgs = ((InternalEObject)third).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__THIRD, null, msgs);
			if (newThird != null)
				msgs = ((InternalEObject)newThird).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__THIRD, null, msgs);
			msgs = basicSetThird(newThird, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldThirdESet = thirdESet;
			thirdESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionPackage.TERNARY_EXPRESSION__THIRD, newThird, newThird, !oldThirdESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetThird(NotificationChain msgs) {
		Expression oldThird = third;
		third = null;
		boolean oldThirdESet = thirdESet;
		thirdESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.TERNARY_EXPRESSION__THIRD, oldThird, null, oldThirdESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetThird() {
		if (third != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)third).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.TERNARY_EXPRESSION__THIRD, null, msgs);
			msgs = basicUnsetThird(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldThirdESet = thirdESet;
			thirdESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.TERNARY_EXPRESSION__THIRD, null, null, oldThirdESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetThird() {
		return thirdESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression Get_value() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType Get_derivedDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ExpressionPackage.TERNARY_EXPRESSION__FIRST:
				return basicUnsetFirst(msgs);
			case ExpressionPackage.TERNARY_EXPRESSION__SECOND:
				return basicUnsetSecond(msgs);
			case ExpressionPackage.TERNARY_EXPRESSION__THIRD:
				return basicUnsetThird(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExpressionPackage.TERNARY_EXPRESSION__OPERATOR:
				return getOperator();
			case ExpressionPackage.TERNARY_EXPRESSION__FIRST:
				return getFirst();
			case ExpressionPackage.TERNARY_EXPRESSION__SECOND:
				return getSecond();
			case ExpressionPackage.TERNARY_EXPRESSION__THIRD:
				return getThird();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExpressionPackage.TERNARY_EXPRESSION__OPERATOR:
				setOperator((TernaryOperator)newValue);
				return;
			case ExpressionPackage.TERNARY_EXPRESSION__FIRST:
				setFirst((Expression)newValue);
				return;
			case ExpressionPackage.TERNARY_EXPRESSION__SECOND:
				setSecond((Expression)newValue);
				return;
			case ExpressionPackage.TERNARY_EXPRESSION__THIRD:
				setThird((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExpressionPackage.TERNARY_EXPRESSION__OPERATOR:
				unsetOperator();
				return;
			case ExpressionPackage.TERNARY_EXPRESSION__FIRST:
				unsetFirst();
				return;
			case ExpressionPackage.TERNARY_EXPRESSION__SECOND:
				unsetSecond();
				return;
			case ExpressionPackage.TERNARY_EXPRESSION__THIRD:
				unsetThird();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExpressionPackage.TERNARY_EXPRESSION__OPERATOR:
				return isSetOperator();
			case ExpressionPackage.TERNARY_EXPRESSION__FIRST:
				return isSetFirst();
			case ExpressionPackage.TERNARY_EXPRESSION__SECOND:
				return isSetSecond();
			case ExpressionPackage.TERNARY_EXPRESSION__THIRD:
				return isSetThird();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operator: ");
		if (operatorESet) result.append(operator); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //TernaryExpressionImpl
