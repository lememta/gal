/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression.impl;

import geneauto.emf.models.gacodemodel.expression.BinaryExpression;
import geneauto.emf.models.gacodemodel.expression.Expression;
import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;

import geneauto.emf.models.gacodemodel.operator.BinaryOperator;

import geneauto.emf.models.gadatatypes.GADataType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.impl.BinaryExpressionImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.impl.BinaryExpressionImpl#getRightArgument <em>Right Argument</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.impl.BinaryExpressionImpl#getLeftArgument <em>Left Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BinaryExpressionImpl extends ExpressionImpl implements BinaryExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final BinaryOperator OPERATOR_EDEFAULT = BinaryOperator.ADD_OPERATOR;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected BinaryOperator operator = OPERATOR_EDEFAULT;

	/**
	 * This is true if the Operator attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean operatorESet;

	/**
	 * The cached value of the '{@link #getRightArgument() <em>Right Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightArgument()
	 * @generated
	 * @ordered
	 */
	protected Expression rightArgument;

	/**
	 * This is true if the Right Argument containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean rightArgumentESet;

	/**
	 * The cached value of the '{@link #getLeftArgument() <em>Left Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftArgument()
	 * @generated
	 * @ordered
	 */
	protected Expression leftArgument;

	/**
	 * This is true if the Left Argument containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean leftArgumentESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionPackage.Literals.BINARY_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOperator getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperator(BinaryOperator newOperator) {
		BinaryOperator oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		boolean oldOperatorESet = operatorESet;
		operatorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionPackage.BINARY_EXPRESSION__OPERATOR, oldOperator, operator, !oldOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOperator() {
		BinaryOperator oldOperator = operator;
		boolean oldOperatorESet = operatorESet;
		operator = OPERATOR_EDEFAULT;
		operatorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.BINARY_EXPRESSION__OPERATOR, oldOperator, OPERATOR_EDEFAULT, oldOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperator() {
		return operatorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getRightArgument() {
		return rightArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightArgument(Expression newRightArgument, NotificationChain msgs) {
		Expression oldRightArgument = rightArgument;
		rightArgument = newRightArgument;
		boolean oldRightArgumentESet = rightArgumentESet;
		rightArgumentESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT, oldRightArgument, newRightArgument, !oldRightArgumentESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightArgument(Expression newRightArgument) {
		if (newRightArgument != rightArgument) {
			NotificationChain msgs = null;
			if (rightArgument != null)
				msgs = ((InternalEObject)rightArgument).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT, null, msgs);
			if (newRightArgument != null)
				msgs = ((InternalEObject)newRightArgument).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT, null, msgs);
			msgs = basicSetRightArgument(newRightArgument, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldRightArgumentESet = rightArgumentESet;
			rightArgumentESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT, newRightArgument, newRightArgument, !oldRightArgumentESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetRightArgument(NotificationChain msgs) {
		Expression oldRightArgument = rightArgument;
		rightArgument = null;
		boolean oldRightArgumentESet = rightArgumentESet;
		rightArgumentESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT, oldRightArgument, null, oldRightArgumentESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRightArgument() {
		if (rightArgument != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)rightArgument).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT, null, msgs);
			msgs = basicUnsetRightArgument(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldRightArgumentESet = rightArgumentESet;
			rightArgumentESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT, null, null, oldRightArgumentESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRightArgument() {
		return rightArgumentESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getLeftArgument() {
		return leftArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftArgument(Expression newLeftArgument, NotificationChain msgs) {
		Expression oldLeftArgument = leftArgument;
		leftArgument = newLeftArgument;
		boolean oldLeftArgumentESet = leftArgumentESet;
		leftArgumentESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT, oldLeftArgument, newLeftArgument, !oldLeftArgumentESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftArgument(Expression newLeftArgument) {
		if (newLeftArgument != leftArgument) {
			NotificationChain msgs = null;
			if (leftArgument != null)
				msgs = ((InternalEObject)leftArgument).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT, null, msgs);
			if (newLeftArgument != null)
				msgs = ((InternalEObject)newLeftArgument).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT, null, msgs);
			msgs = basicSetLeftArgument(newLeftArgument, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldLeftArgumentESet = leftArgumentESet;
			leftArgumentESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT, newLeftArgument, newLeftArgument, !oldLeftArgumentESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetLeftArgument(NotificationChain msgs) {
		Expression oldLeftArgument = leftArgument;
		leftArgument = null;
		boolean oldLeftArgumentESet = leftArgumentESet;
		leftArgumentESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT, oldLeftArgument, null, oldLeftArgumentESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLeftArgument() {
		if (leftArgument != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)leftArgument).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT, null, msgs);
			msgs = basicUnsetLeftArgument(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldLeftArgumentESet = leftArgumentESet;
			leftArgumentESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT, null, null, oldLeftArgumentESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLeftArgument() {
		return leftArgumentESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression Get_value() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType Get_derivedDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT:
				return basicUnsetRightArgument(msgs);
			case ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT:
				return basicUnsetLeftArgument(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExpressionPackage.BINARY_EXPRESSION__OPERATOR:
				return getOperator();
			case ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT:
				return getRightArgument();
			case ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT:
				return getLeftArgument();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExpressionPackage.BINARY_EXPRESSION__OPERATOR:
				setOperator((BinaryOperator)newValue);
				return;
			case ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT:
				setRightArgument((Expression)newValue);
				return;
			case ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT:
				setLeftArgument((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExpressionPackage.BINARY_EXPRESSION__OPERATOR:
				unsetOperator();
				return;
			case ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT:
				unsetRightArgument();
				return;
			case ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT:
				unsetLeftArgument();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExpressionPackage.BINARY_EXPRESSION__OPERATOR:
				return isSetOperator();
			case ExpressionPackage.BINARY_EXPRESSION__RIGHT_ARGUMENT:
				return isSetRightArgument();
			case ExpressionPackage.BINARY_EXPRESSION__LEFT_ARGUMENT:
				return isSetLeftArgument();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operator: ");
		if (operatorESet) result.append(operator); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //BinaryExpressionImpl
