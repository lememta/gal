/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression;

import geneauto.emf.models.common.Function;
import geneauto.emf.models.common.StandardFunction;

import geneauto.emf.models.gacodemodel.Dependency;

import geneauto.emf.models.gadatatypes.GADataType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An expression representing a function call. 
 * 
 * The called function object is specified in one of the following ways:
 *   1) by 'name' -- The function is referenced indirectly. The exact function will be resolved at a later stage (possibly even by the linker after code generation)
 *   2) by the 'standardFunction' reference -- The function maps to some internally defined function
 *   3) by the 'customFunction' reference -- The function maps to some function defined in the current model
 * 
 * All these options are mutually exclusive. For example, if the by name reference ('name') is resolved to a function object 
 * in the current model and 'customFunction' is set to point to that function, the 'name' attribute in the current expression is considered invalid.
 * 
 * TODO Rename 'function' to 'customFunction'
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getFunction <em>Function</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getDependency <em>Dependency</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getArguments <em>Arguments</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getStandardFunction <em>Standard Function</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getCallExpression()
 * @model
 * @generated
 */
public interface CallExpression extends Expression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function</em>' reference.
	 * @see #isSetFunction()
	 * @see #unsetFunction()
	 * @see #setFunction(Function)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getCallExpression_Function()
	 * @model unsettable="true"
	 * @generated
	 */
	Function getFunction();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getFunction <em>Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function</em>' reference.
	 * @see #isSetFunction()
	 * @see #unsetFunction()
	 * @see #getFunction()
	 * @generated
	 */
	void setFunction(Function value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getFunction <em>Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFunction()
	 * @see #getFunction()
	 * @see #setFunction(Function)
	 * @generated
	 */
	void unsetFunction();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getFunction <em>Function</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Function</em>' reference is set.
	 * @see #unsetFunction()
	 * @see #getFunction()
	 * @see #setFunction(Function)
	 * @generated
	 */
	boolean isSetFunction();

	/**
	 * Returns the value of the '<em><b>Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency</em>' reference.
	 * @see #isSetDependency()
	 * @see #unsetDependency()
	 * @see #setDependency(Dependency)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getCallExpression_Dependency()
	 * @model unsettable="true"
	 * @generated
	 */
	Dependency getDependency();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getDependency <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependency</em>' reference.
	 * @see #isSetDependency()
	 * @see #unsetDependency()
	 * @see #getDependency()
	 * @generated
	 */
	void setDependency(Dependency value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getDependency <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDependency()
	 * @see #getDependency()
	 * @see #setDependency(Dependency)
	 * @generated
	 */
	void unsetDependency();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getDependency <em>Dependency</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Dependency</em>' reference is set.
	 * @see #unsetDependency()
	 * @see #getDependency()
	 * @see #setDependency(Dependency)
	 * @generated
	 */
	boolean isSetDependency();

	/**
	 * Returns the value of the '<em><b>Arguments</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.expression.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arguments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments</em>' containment reference list.
	 * @see #isSetArguments()
	 * @see #unsetArguments()
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getCallExpression_Arguments()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Expression> getArguments();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getArguments <em>Arguments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetArguments()
	 * @see #getArguments()
	 * @generated
	 */
	void unsetArguments();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getArguments <em>Arguments</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Arguments</em>' containment reference list is set.
	 * @see #unsetArguments()
	 * @see #getArguments()
	 * @generated
	 */
	boolean isSetArguments();

	/**
	 * Returns the value of the '<em><b>Standard Function</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.common.StandardFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Standard Function</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Standard Function</em>' attribute.
	 * @see geneauto.emf.models.common.StandardFunction
	 * @see #isSetStandardFunction()
	 * @see #unsetStandardFunction()
	 * @see #setStandardFunction(StandardFunction)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getCallExpression_StandardFunction()
	 * @model unsettable="true"
	 * @generated
	 */
	StandardFunction getStandardFunction();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getStandardFunction <em>Standard Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Standard Function</em>' attribute.
	 * @see geneauto.emf.models.common.StandardFunction
	 * @see #isSetStandardFunction()
	 * @see #unsetStandardFunction()
	 * @see #getStandardFunction()
	 * @generated
	 */
	void setStandardFunction(StandardFunction value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getStandardFunction <em>Standard Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStandardFunction()
	 * @see #getStandardFunction()
	 * @see #setStandardFunction(StandardFunction)
	 * @generated
	 */
	void unsetStandardFunction();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getStandardFunction <em>Standard Function</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Standard Function</em>' attribute is set.
	 * @see #unsetStandardFunction()
	 * @see #getStandardFunction()
	 * @see #setStandardFunction(StandardFunction)
	 * @generated
	 */
	boolean isSetStandardFunction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Expression Get_value();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GADataType Get_derivedDataType();

} // CallExpression
