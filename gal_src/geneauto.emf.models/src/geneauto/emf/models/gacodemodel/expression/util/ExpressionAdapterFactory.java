/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression.util;

import geneauto.emf.models.common.HasVariable;

import geneauto.emf.models.gacodemodel.GACodeModelElement;

import geneauto.emf.models.gacodemodel.expression.*;

import geneauto.emf.models.genericmodel.GAModelElement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage
 * @generated
 */
public class ExpressionAdapterFactory extends AdapterFactoryImpl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ExpressionPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ExpressionPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionSwitch<Adapter> modelSwitch =
		new ExpressionSwitch<Adapter>() {
			@Override
			public Adapter caseExpression(Expression object) {
				return createExpressionAdapter();
			}
			@Override
			public Adapter caseBinaryExpression(BinaryExpression object) {
				return createBinaryExpressionAdapter();
			}
			@Override
			public Adapter caseCallExpression(CallExpression object) {
				return createCallExpressionAdapter();
			}
			@Override
			public Adapter caseLiteralExpression(LiteralExpression object) {
				return createLiteralExpressionAdapter();
			}
			@Override
			public Adapter caseBooleanExpression(BooleanExpression object) {
				return createBooleanExpressionAdapter();
			}
			@Override
			public Adapter caseFalseExpression(FalseExpression object) {
				return createFalseExpressionAdapter();
			}
			@Override
			public Adapter caseTrueExpression(TrueExpression object) {
				return createTrueExpressionAdapter();
			}
			@Override
			public Adapter caseNumericExpression(NumericExpression object) {
				return createNumericExpressionAdapter();
			}
			@Override
			public Adapter caseFloatingPointExpression(FloatingPointExpression object) {
				return createFloatingPointExpressionAdapter();
			}
			@Override
			public Adapter caseDoubleExpression(DoubleExpression object) {
				return createDoubleExpressionAdapter();
			}
			@Override
			public Adapter caseSingleExpression(SingleExpression object) {
				return createSingleExpressionAdapter();
			}
			@Override
			public Adapter caseIntegerExpression(IntegerExpression object) {
				return createIntegerExpressionAdapter();
			}
			@Override
			public Adapter caseStringExpression(StringExpression object) {
				return createStringExpressionAdapter();
			}
			@Override
			public Adapter caseListExpression(ListExpression object) {
				return createListExpressionAdapter();
			}
			@Override
			public Adapter caseConstantListExpression(ConstantListExpression object) {
				return createConstantListExpressionAdapter();
			}
			@Override
			public Adapter caseGeneralListExpression(GeneralListExpression object) {
				return createGeneralListExpressionAdapter();
			}
			@Override
			public Adapter caseMemberExpression(MemberExpression object) {
				return createMemberExpressionAdapter();
			}
			@Override
			public Adapter caseParenthesisExpression(ParenthesisExpression object) {
				return createParenthesisExpressionAdapter();
			}
			@Override
			public Adapter caseRangeExpression(RangeExpression object) {
				return createRangeExpressionAdapter();
			}
			@Override
			public Adapter caseSubArrayExpression(SubArrayExpression object) {
				return createSubArrayExpressionAdapter();
			}
			@Override
			public Adapter caseSubArrayExpression1D(SubArrayExpression1D object) {
				return createSubArrayExpression1DAdapter();
			}
			@Override
			public Adapter caseTernaryExpression(TernaryExpression object) {
				return createTernaryExpressionAdapter();
			}
			@Override
			public Adapter caseUnaryExpression(UnaryExpression object) {
				return createUnaryExpressionAdapter();
			}
			@Override
			public Adapter caseVariableExpression(VariableExpression object) {
				return createVariableExpressionAdapter();
			}
			@Override
			public Adapter caseLValueExpression(LValueExpression object) {
				return createLValueExpressionAdapter();
			}
			@Override
			public Adapter caseGAModelElement(GAModelElement object) {
				return createGAModelElementAdapter();
			}
			@Override
			public Adapter caseGACodeModelElement(GACodeModelElement object) {
				return createGACodeModelElementAdapter();
			}
			@Override
			public Adapter caseHasVariable(HasVariable object) {
				return createHasVariableAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.Expression
	 * @generated
	 */
	public Adapter createExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.BinaryExpression
	 * @generated
	 */
	public Adapter createBinaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.CallExpression <em>Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.CallExpression
	 * @generated
	 */
	public Adapter createCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.LiteralExpression <em>Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.LiteralExpression
	 * @generated
	 */
	public Adapter createLiteralExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.BooleanExpression <em>Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.BooleanExpression
	 * @generated
	 */
	public Adapter createBooleanExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.FalseExpression <em>False Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.FalseExpression
	 * @generated
	 */
	public Adapter createFalseExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.TrueExpression <em>True Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.TrueExpression
	 * @generated
	 */
	public Adapter createTrueExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.NumericExpression <em>Numeric Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.NumericExpression
	 * @generated
	 */
	public Adapter createNumericExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.FloatingPointExpression <em>Floating Point Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.FloatingPointExpression
	 * @generated
	 */
	public Adapter createFloatingPointExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.DoubleExpression <em>Double Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.DoubleExpression
	 * @generated
	 */
	public Adapter createDoubleExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.SingleExpression <em>Single Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.SingleExpression
	 * @generated
	 */
	public Adapter createSingleExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.IntegerExpression <em>Integer Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.IntegerExpression
	 * @generated
	 */
	public Adapter createIntegerExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.StringExpression <em>String Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.StringExpression
	 * @generated
	 */
	public Adapter createStringExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.ListExpression <em>List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.ListExpression
	 * @generated
	 */
	public Adapter createListExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.ConstantListExpression <em>Constant List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.ConstantListExpression
	 * @generated
	 */
	public Adapter createConstantListExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.GeneralListExpression <em>General List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.GeneralListExpression
	 * @generated
	 */
	public Adapter createGeneralListExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.MemberExpression <em>Member Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.MemberExpression
	 * @generated
	 */
	public Adapter createMemberExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.ParenthesisExpression <em>Parenthesis Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.ParenthesisExpression
	 * @generated
	 */
	public Adapter createParenthesisExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression <em>Range Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.RangeExpression
	 * @generated
	 */
	public Adapter createRangeExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression <em>Sub Array Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.SubArrayExpression
	 * @generated
	 */
	public Adapter createSubArrayExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D <em>Sub Array Expression1 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D
	 * @generated
	 */
	public Adapter createSubArrayExpression1DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression <em>Ternary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.TernaryExpression
	 * @generated
	 */
	public Adapter createTernaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.UnaryExpression
	 * @generated
	 */
	public Adapter createUnaryExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.VariableExpression <em>Variable Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.VariableExpression
	 * @generated
	 */
	public Adapter createVariableExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.expression.LValueExpression <em>LValue Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.expression.LValueExpression
	 * @generated
	 */
	public Adapter createLValueExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.genericmodel.GAModelElement <em>GA Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.genericmodel.GAModelElement
	 * @generated
	 */
	public Adapter createGAModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.gacodemodel.GACodeModelElement <em>GA Code Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.gacodemodel.GACodeModelElement
	 * @generated
	 */
	public Adapter createGACodeModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link geneauto.emf.models.common.HasVariable <em>Has Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see geneauto.emf.models.common.HasVariable
	 * @generated
	 */
	public Adapter createHasVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ExpressionAdapterFactory
