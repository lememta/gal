/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression;

import geneauto.emf.models.gadatatypes.GADataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Array Expression1 D</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * SubArrayExpression for 1 dimensional expression. It is sub expression of  expression from start (inclusive) to end index (inclusive) of single dimension of expression 
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getEndIndex <em>End Index</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getStartIndex <em>Start Index</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getSubArrayExpression1D()
 * @model
 * @generated
 */
public interface SubArrayExpression1D extends SubArrayExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>End Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Index</em>' attribute.
	 * @see #isSetEndIndex()
	 * @see #unsetEndIndex()
	 * @see #setEndIndex(int)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getSubArrayExpression1D_EndIndex()
	 * @model unsettable="true"
	 * @generated
	 */
	int getEndIndex();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getEndIndex <em>End Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Index</em>' attribute.
	 * @see #isSetEndIndex()
	 * @see #unsetEndIndex()
	 * @see #getEndIndex()
	 * @generated
	 */
	void setEndIndex(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getEndIndex <em>End Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEndIndex()
	 * @see #getEndIndex()
	 * @see #setEndIndex(int)
	 * @generated
	 */
	void unsetEndIndex();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getEndIndex <em>End Index</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>End Index</em>' attribute is set.
	 * @see #unsetEndIndex()
	 * @see #getEndIndex()
	 * @see #setEndIndex(int)
	 * @generated
	 */
	boolean isSetEndIndex();

	/**
	 * Returns the value of the '<em><b>Start Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Index</em>' attribute.
	 * @see #isSetStartIndex()
	 * @see #unsetStartIndex()
	 * @see #setStartIndex(int)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getSubArrayExpression1D_StartIndex()
	 * @model unsettable="true"
	 * @generated
	 */
	int getStartIndex();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getStartIndex <em>Start Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Index</em>' attribute.
	 * @see #isSetStartIndex()
	 * @see #unsetStartIndex()
	 * @see #getStartIndex()
	 * @generated
	 */
	void setStartIndex(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getStartIndex <em>Start Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStartIndex()
	 * @see #getStartIndex()
	 * @see #setStartIndex(int)
	 * @generated
	 */
	void unsetStartIndex();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getStartIndex <em>Start Index</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Start Index</em>' attribute is set.
	 * @see #unsetStartIndex()
	 * @see #getStartIndex()
	 * @see #setStartIndex(int)
	 * @generated
	 */
	boolean isSetStartIndex();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Expression Get_value();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GADataType Get_derivedDataType();

} // SubArrayExpression1D
