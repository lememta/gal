/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression;

import geneauto.emf.models.gacodemodel.operator.BinaryOperator;

import geneauto.emf.models.gadatatypes.GADataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getOperator <em>Operator</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getRightArgument <em>Right Argument</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getLeftArgument <em>Left Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getBinaryExpression()
 * @model
 * @generated
 */
public interface BinaryExpression extends Expression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gacodemodel.operator.BinaryOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.operator.BinaryOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #setOperator(BinaryOperator)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getBinaryExpression_Operator()
	 * @model unsettable="true"
	 * @generated
	 */
	BinaryOperator getOperator();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.operator.BinaryOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(BinaryOperator value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperator()
	 * @see #getOperator()
	 * @see #setOperator(BinaryOperator)
	 * @generated
	 */
	void unsetOperator();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getOperator <em>Operator</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operator</em>' attribute is set.
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @see #setOperator(BinaryOperator)
	 * @generated
	 */
	boolean isSetOperator();

	/**
	 * Returns the value of the '<em><b>Right Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Argument</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Argument</em>' containment reference.
	 * @see #isSetRightArgument()
	 * @see #unsetRightArgument()
	 * @see #setRightArgument(Expression)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getBinaryExpression_RightArgument()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getRightArgument();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getRightArgument <em>Right Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Argument</em>' containment reference.
	 * @see #isSetRightArgument()
	 * @see #unsetRightArgument()
	 * @see #getRightArgument()
	 * @generated
	 */
	void setRightArgument(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getRightArgument <em>Right Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRightArgument()
	 * @see #getRightArgument()
	 * @see #setRightArgument(Expression)
	 * @generated
	 */
	void unsetRightArgument();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getRightArgument <em>Right Argument</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Right Argument</em>' containment reference is set.
	 * @see #unsetRightArgument()
	 * @see #getRightArgument()
	 * @see #setRightArgument(Expression)
	 * @generated
	 */
	boolean isSetRightArgument();

	/**
	 * Returns the value of the '<em><b>Left Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Argument</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Argument</em>' containment reference.
	 * @see #isSetLeftArgument()
	 * @see #unsetLeftArgument()
	 * @see #setLeftArgument(Expression)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getBinaryExpression_LeftArgument()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getLeftArgument();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getLeftArgument <em>Left Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Argument</em>' containment reference.
	 * @see #isSetLeftArgument()
	 * @see #unsetLeftArgument()
	 * @see #getLeftArgument()
	 * @generated
	 */
	void setLeftArgument(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getLeftArgument <em>Left Argument</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLeftArgument()
	 * @see #getLeftArgument()
	 * @see #setLeftArgument(Expression)
	 * @generated
	 */
	void unsetLeftArgument();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getLeftArgument <em>Left Argument</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Left Argument</em>' containment reference is set.
	 * @see #unsetLeftArgument()
	 * @see #getLeftArgument()
	 * @see #setLeftArgument(Expression)
	 * @generated
	 */
	boolean isSetLeftArgument();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Expression Get_value();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GADataType Get_derivedDataType();

} // BinaryExpression
