/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression;

import geneauto.emf.models.common.CommonPackage;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionFactory
 * @model kind="package"
 * @generated
 */
public interface ExpressionPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "expression";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gacodemodel/expression.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gacodemodel.expression";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExpressionPackage eINSTANCE = geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__EXTERNAL_ID = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__ID = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__IS_FIXED_NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__ORIGINAL_FULL_NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__ORIGINAL_NAME = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__PARENT = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__MODEL = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__ANNOTATIONS = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__SOURCE_ELEMENT = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__SOURCE_ELEMENT_ID = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__SOURCE_ACTION = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__MODULE = GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__DATA_TYPE = GacodemodelPackage.GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__INDEX_EXPRESSIONS = GacodemodelPackage.GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = GacodemodelPackage.GA_CODE_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.BinaryExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RIGHT_ARGUMENT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Left Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__LEFT_ARGUMENT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.CallExpressionImpl <em>Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.CallExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getCallExpression()
	 * @generated
	 */
	int CALL_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__FUNCTION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__DEPENDENCY = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__ARGUMENTS = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Standard Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION__STANDARD_FUNCTION = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.LiteralExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getLiteralExpression()
	 * @generated
	 */
	int LITERAL_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__LIT_VALUE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.BooleanExpressionImpl <em>Boolean Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.BooleanExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getBooleanExpression()
	 * @generated
	 */
	int BOOLEAN_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__EXTERNAL_ID = LITERAL_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__ID = LITERAL_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__IS_FIXED_NAME = LITERAL_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__NAME = LITERAL_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__ORIGINAL_FULL_NAME = LITERAL_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__ORIGINAL_NAME = LITERAL_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__PARENT = LITERAL_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__MODEL = LITERAL_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__ANNOTATIONS = LITERAL_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__SOURCE_ELEMENT = LITERAL_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__SOURCE_ELEMENT_ID = LITERAL_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__SOURCE_ACTION = LITERAL_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__MODULE = LITERAL_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__DATA_TYPE = LITERAL_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__INDEX_EXPRESSIONS = LITERAL_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__LIT_VALUE = LITERAL_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>Boolean Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.FalseExpressionImpl <em>False Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.FalseExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getFalseExpression()
	 * @generated
	 */
	int FALSE_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__EXTERNAL_ID = BOOLEAN_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__ID = BOOLEAN_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__IS_FIXED_NAME = BOOLEAN_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__NAME = BOOLEAN_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__ORIGINAL_FULL_NAME = BOOLEAN_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__ORIGINAL_NAME = BOOLEAN_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__PARENT = BOOLEAN_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__MODEL = BOOLEAN_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__ANNOTATIONS = BOOLEAN_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__SOURCE_ELEMENT = BOOLEAN_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__SOURCE_ELEMENT_ID = BOOLEAN_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__SOURCE_ACTION = BOOLEAN_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__MODULE = BOOLEAN_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__DATA_TYPE = BOOLEAN_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__INDEX_EXPRESSIONS = BOOLEAN_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION__LIT_VALUE = BOOLEAN_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>False Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_EXPRESSION_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.TrueExpressionImpl <em>True Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.TrueExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getTrueExpression()
	 * @generated
	 */
	int TRUE_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__EXTERNAL_ID = BOOLEAN_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__ID = BOOLEAN_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__IS_FIXED_NAME = BOOLEAN_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__NAME = BOOLEAN_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__ORIGINAL_FULL_NAME = BOOLEAN_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__ORIGINAL_NAME = BOOLEAN_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__PARENT = BOOLEAN_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__MODEL = BOOLEAN_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__ANNOTATIONS = BOOLEAN_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__SOURCE_ELEMENT = BOOLEAN_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__SOURCE_ELEMENT_ID = BOOLEAN_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__SOURCE_ACTION = BOOLEAN_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__MODULE = BOOLEAN_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__DATA_TYPE = BOOLEAN_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__INDEX_EXPRESSIONS = BOOLEAN_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__LIT_VALUE = BOOLEAN_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>True Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.NumericExpressionImpl <em>Numeric Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.NumericExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getNumericExpression()
	 * @generated
	 */
	int NUMERIC_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__EXTERNAL_ID = LITERAL_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__ID = LITERAL_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__IS_FIXED_NAME = LITERAL_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__NAME = LITERAL_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__ORIGINAL_FULL_NAME = LITERAL_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__ORIGINAL_NAME = LITERAL_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__PARENT = LITERAL_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__MODEL = LITERAL_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__ANNOTATIONS = LITERAL_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__SOURCE_ELEMENT = LITERAL_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__SOURCE_ELEMENT_ID = LITERAL_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__SOURCE_ACTION = LITERAL_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__MODULE = LITERAL_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__DATA_TYPE = LITERAL_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__INDEX_EXPRESSIONS = LITERAL_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION__LIT_VALUE = LITERAL_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>Numeric Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMERIC_EXPRESSION_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.FloatingPointExpressionImpl <em>Floating Point Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.FloatingPointExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getFloatingPointExpression()
	 * @generated
	 */
	int FLOATING_POINT_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__EXTERNAL_ID = NUMERIC_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__ID = NUMERIC_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__IS_FIXED_NAME = NUMERIC_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__NAME = NUMERIC_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__ORIGINAL_FULL_NAME = NUMERIC_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__ORIGINAL_NAME = NUMERIC_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__PARENT = NUMERIC_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__MODEL = NUMERIC_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__ANNOTATIONS = NUMERIC_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__SOURCE_ELEMENT = NUMERIC_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__SOURCE_ELEMENT_ID = NUMERIC_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__SOURCE_ACTION = NUMERIC_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__MODULE = NUMERIC_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__DATA_TYPE = NUMERIC_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__INDEX_EXPRESSIONS = NUMERIC_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION__LIT_VALUE = NUMERIC_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>Floating Point Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOATING_POINT_EXPRESSION_FEATURE_COUNT = NUMERIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.DoubleExpressionImpl <em>Double Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.DoubleExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getDoubleExpression()
	 * @generated
	 */
	int DOUBLE_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__EXTERNAL_ID = FLOATING_POINT_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__ID = FLOATING_POINT_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__IS_FIXED_NAME = FLOATING_POINT_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__NAME = FLOATING_POINT_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__ORIGINAL_FULL_NAME = FLOATING_POINT_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__ORIGINAL_NAME = FLOATING_POINT_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__PARENT = FLOATING_POINT_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__MODEL = FLOATING_POINT_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__ANNOTATIONS = FLOATING_POINT_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__SOURCE_ELEMENT = FLOATING_POINT_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__SOURCE_ELEMENT_ID = FLOATING_POINT_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__SOURCE_ACTION = FLOATING_POINT_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__MODULE = FLOATING_POINT_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__DATA_TYPE = FLOATING_POINT_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__INDEX_EXPRESSIONS = FLOATING_POINT_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION__LIT_VALUE = FLOATING_POINT_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>Double Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_EXPRESSION_FEATURE_COUNT = FLOATING_POINT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.SingleExpressionImpl <em>Single Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.SingleExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getSingleExpression()
	 * @generated
	 */
	int SINGLE_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__EXTERNAL_ID = FLOATING_POINT_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__ID = FLOATING_POINT_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__IS_FIXED_NAME = FLOATING_POINT_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__NAME = FLOATING_POINT_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__ORIGINAL_FULL_NAME = FLOATING_POINT_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__ORIGINAL_NAME = FLOATING_POINT_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__PARENT = FLOATING_POINT_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__MODEL = FLOATING_POINT_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__ANNOTATIONS = FLOATING_POINT_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__SOURCE_ELEMENT = FLOATING_POINT_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__SOURCE_ELEMENT_ID = FLOATING_POINT_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__SOURCE_ACTION = FLOATING_POINT_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__MODULE = FLOATING_POINT_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__DATA_TYPE = FLOATING_POINT_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__INDEX_EXPRESSIONS = FLOATING_POINT_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION__LIT_VALUE = FLOATING_POINT_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>Single Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EXPRESSION_FEATURE_COUNT = FLOATING_POINT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.IntegerExpressionImpl <em>Integer Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.IntegerExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getIntegerExpression()
	 * @generated
	 */
	int INTEGER_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__EXTERNAL_ID = NUMERIC_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__ID = NUMERIC_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__IS_FIXED_NAME = NUMERIC_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__NAME = NUMERIC_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__ORIGINAL_FULL_NAME = NUMERIC_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__ORIGINAL_NAME = NUMERIC_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__PARENT = NUMERIC_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__MODEL = NUMERIC_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__ANNOTATIONS = NUMERIC_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__SOURCE_ELEMENT = NUMERIC_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__SOURCE_ELEMENT_ID = NUMERIC_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__SOURCE_ACTION = NUMERIC_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__MODULE = NUMERIC_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__DATA_TYPE = NUMERIC_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__INDEX_EXPRESSIONS = NUMERIC_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION__LIT_VALUE = NUMERIC_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>Integer Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_EXPRESSION_FEATURE_COUNT = NUMERIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.StringExpressionImpl <em>String Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.StringExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getStringExpression()
	 * @generated
	 */
	int STRING_EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__EXTERNAL_ID = LITERAL_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__ID = LITERAL_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__IS_FIXED_NAME = LITERAL_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__NAME = LITERAL_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__ORIGINAL_FULL_NAME = LITERAL_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__ORIGINAL_NAME = LITERAL_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__PARENT = LITERAL_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__MODEL = LITERAL_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__ANNOTATIONS = LITERAL_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__SOURCE_ELEMENT = LITERAL_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__SOURCE_ELEMENT_ID = LITERAL_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__SOURCE_ACTION = LITERAL_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__MODULE = LITERAL_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__DATA_TYPE = LITERAL_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__INDEX_EXPRESSIONS = LITERAL_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Lit Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__LIT_VALUE = LITERAL_EXPRESSION__LIT_VALUE;

	/**
	 * The number of structural features of the '<em>String Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ListExpressionImpl <em>List Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ListExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getListExpression()
	 * @generated
	 */
	int LIST_EXPRESSION = 13;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The number of structural features of the '<em>List Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ConstantListExpressionImpl <em>Constant List Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ConstantListExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getConstantListExpression()
	 * @generated
	 */
	int CONSTANT_LIST_EXPRESSION = 14;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__EXTERNAL_ID = LIST_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__ID = LIST_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__IS_FIXED_NAME = LIST_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__NAME = LIST_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__ORIGINAL_FULL_NAME = LIST_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__ORIGINAL_NAME = LIST_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__PARENT = LIST_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__MODEL = LIST_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__ANNOTATIONS = LIST_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__SOURCE_ELEMENT = LIST_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__SOURCE_ELEMENT_ID = LIST_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__SOURCE_ACTION = LIST_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__MODULE = LIST_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__DATA_TYPE = LIST_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__INDEX_EXPRESSIONS = LIST_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__LENGTH = LIST_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION__EXPRESSION = LIST_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Constant List Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LIST_EXPRESSION_FEATURE_COUNT = LIST_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.GeneralListExpressionImpl <em>General List Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.GeneralListExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getGeneralListExpression()
	 * @generated
	 */
	int GENERAL_LIST_EXPRESSION = 15;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__EXTERNAL_ID = LIST_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__ID = LIST_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__IS_FIXED_NAME = LIST_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__NAME = LIST_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__ORIGINAL_FULL_NAME = LIST_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__ORIGINAL_NAME = LIST_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__PARENT = LIST_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__MODEL = LIST_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__ANNOTATIONS = LIST_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__SOURCE_ELEMENT = LIST_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__SOURCE_ELEMENT_ID = LIST_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__SOURCE_ACTION = LIST_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__MODULE = LIST_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__DATA_TYPE = LIST_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__INDEX_EXPRESSIONS = LIST_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION__EXPRESSIONS = LIST_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>General List Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_LIST_EXPRESSION_FEATURE_COUNT = LIST_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.MemberExpressionImpl <em>Member Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.MemberExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getMemberExpression()
	 * @generated
	 */
	int MEMBER_EXPRESSION = 16;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__MEMBER = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Member Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ParenthesisExpressionImpl <em>Parenthesis Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ParenthesisExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getParenthesisExpression()
	 * @generated
	 */
	int PARENTHESIS_EXPRESSION = 17;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parenthesis Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARENTHESIS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.RangeExpressionImpl <em>Range Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.RangeExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getRangeExpression()
	 * @generated
	 */
	int RANGE_EXPRESSION = 18;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__END = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION__START = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Range Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpressionImpl <em>Sub Array Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getSubArrayExpression()
	 * @generated
	 */
	int SUB_ARRAY_EXPRESSION = 19;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sub Array Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpression1DImpl <em>Sub Array Expression1 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpression1DImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getSubArrayExpression1D()
	 * @generated
	 */
	int SUB_ARRAY_EXPRESSION1_D = 20;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__EXTERNAL_ID = SUB_ARRAY_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__ID = SUB_ARRAY_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__IS_FIXED_NAME = SUB_ARRAY_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__NAME = SUB_ARRAY_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__ORIGINAL_FULL_NAME = SUB_ARRAY_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__ORIGINAL_NAME = SUB_ARRAY_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__PARENT = SUB_ARRAY_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__MODEL = SUB_ARRAY_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__ANNOTATIONS = SUB_ARRAY_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__SOURCE_ELEMENT = SUB_ARRAY_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__SOURCE_ELEMENT_ID = SUB_ARRAY_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__SOURCE_ACTION = SUB_ARRAY_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__MODULE = SUB_ARRAY_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__DATA_TYPE = SUB_ARRAY_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__INDEX_EXPRESSIONS = SUB_ARRAY_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__EXPRESSION = SUB_ARRAY_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>End Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__END_INDEX = SUB_ARRAY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D__START_INDEX = SUB_ARRAY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sub Array Expression1 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_ARRAY_EXPRESSION1_D_FEATURE_COUNT = SUB_ARRAY_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl <em>Ternary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getTernaryExpression()
	 * @generated
	 */
	int TERNARY_EXPRESSION = 21;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__FIRST = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Second</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__SECOND = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Third</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION__THIRD = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Ternary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.UnaryExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getUnaryExpression()
	 * @generated
	 */
	int UNARY_EXPRESSION = 22;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Argument</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__ARGUMENT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.impl.VariableExpressionImpl <em>Variable Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.impl.VariableExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getVariableExpression()
	 * @generated
	 */
	int VARIABLE_EXPRESSION = 23;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__EXTERNAL_ID = EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__IS_FIXED_NAME = EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__ORIGINAL_FULL_NAME = EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__ORIGINAL_NAME = EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__PARENT = EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__MODEL = EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__ANNOTATIONS = EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__SOURCE_ELEMENT = EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__SOURCE_ELEMENT_ID = EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__SOURCE_ACTION = EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__MODULE = EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__DATA_TYPE = EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__INDEX_EXPRESSIONS = EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__VARIABLE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.LValueExpression <em>LValue Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.LValueExpression
	 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getLValueExpression()
	 * @generated
	 */
	int LVALUE_EXPRESSION = 24;

	/**
	 * The number of structural features of the '<em>LValue Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LVALUE_EXPRESSION_FEATURE_COUNT = CommonPackage.HAS_VARIABLE_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.Expression#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Type</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.Expression#getDataType()
	 * @see #getExpression()
	 * @generated
	 */
	EReference getExpression_DataType();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.expression.Expression#getIndexExpressions <em>Index Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Index Expressions</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.Expression#getIndexExpressions()
	 * @see #getExpression()
	 * @generated
	 */
	EReference getExpression_IndexExpressions();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.BinaryExpression#getOperator()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EAttribute getBinaryExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getRightArgument <em>Right Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Argument</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.BinaryExpression#getRightArgument()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_RightArgument();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.BinaryExpression#getLeftArgument <em>Left Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Argument</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.BinaryExpression#getLeftArgument()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_LeftArgument();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.CallExpression <em>Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.CallExpression
	 * @generated
	 */
	EClass getCallExpression();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getFunction <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.CallExpression#getFunction()
	 * @see #getCallExpression()
	 * @generated
	 */
	EReference getCallExpression_Function();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dependency</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.CallExpression#getDependency()
	 * @see #getCallExpression()
	 * @generated
	 */
	EReference getCallExpression_Dependency();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getArguments <em>Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Arguments</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.CallExpression#getArguments()
	 * @see #getCallExpression()
	 * @generated
	 */
	EReference getCallExpression_Arguments();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.CallExpression#getStandardFunction <em>Standard Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Standard Function</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.CallExpression#getStandardFunction()
	 * @see #getCallExpression()
	 * @generated
	 */
	EAttribute getCallExpression_StandardFunction();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.LiteralExpression <em>Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.LiteralExpression
	 * @generated
	 */
	EClass getLiteralExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.LiteralExpression#getLitValue <em>Lit Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lit Value</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.LiteralExpression#getLitValue()
	 * @see #getLiteralExpression()
	 * @generated
	 */
	EAttribute getLiteralExpression_LitValue();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.BooleanExpression <em>Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.BooleanExpression
	 * @generated
	 */
	EClass getBooleanExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.FalseExpression <em>False Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>False Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.FalseExpression
	 * @generated
	 */
	EClass getFalseExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.TrueExpression <em>True Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>True Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.TrueExpression
	 * @generated
	 */
	EClass getTrueExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.NumericExpression <em>Numeric Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Numeric Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.NumericExpression
	 * @generated
	 */
	EClass getNumericExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.FloatingPointExpression <em>Floating Point Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Floating Point Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.FloatingPointExpression
	 * @generated
	 */
	EClass getFloatingPointExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.DoubleExpression <em>Double Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.DoubleExpression
	 * @generated
	 */
	EClass getDoubleExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.SingleExpression <em>Single Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.SingleExpression
	 * @generated
	 */
	EClass getSingleExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.IntegerExpression <em>Integer Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.IntegerExpression
	 * @generated
	 */
	EClass getIntegerExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.StringExpression <em>String Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.StringExpression
	 * @generated
	 */
	EClass getStringExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.ListExpression <em>List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.ListExpression
	 * @generated
	 */
	EClass getListExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.ConstantListExpression <em>Constant List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant List Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.ConstantListExpression
	 * @generated
	 */
	EClass getConstantListExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.ConstantListExpression#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.ConstantListExpression#getLength()
	 * @see #getConstantListExpression()
	 * @generated
	 */
	EAttribute getConstantListExpression_Length();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.expression.ConstantListExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.ConstantListExpression#getExpression()
	 * @see #getConstantListExpression()
	 * @generated
	 */
	EReference getConstantListExpression_Expression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.GeneralListExpression <em>General List Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>General List Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.GeneralListExpression
	 * @generated
	 */
	EClass getGeneralListExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gacodemodel.expression.GeneralListExpression#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expressions</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.GeneralListExpression#getExpressions()
	 * @see #getGeneralListExpression()
	 * @generated
	 */
	EReference getGeneralListExpression_Expressions();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.MemberExpression <em>Member Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Member Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.MemberExpression
	 * @generated
	 */
	EClass getMemberExpression();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.expression.MemberExpression#getMember <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Member</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.MemberExpression#getMember()
	 * @see #getMemberExpression()
	 * @generated
	 */
	EReference getMemberExpression_Member();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.MemberExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.MemberExpression#getExpression()
	 * @see #getMemberExpression()
	 * @generated
	 */
	EReference getMemberExpression_Expression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.ParenthesisExpression <em>Parenthesis Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parenthesis Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.ParenthesisExpression
	 * @generated
	 */
	EClass getParenthesisExpression();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.ParenthesisExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.ParenthesisExpression#getExpression()
	 * @see #getParenthesisExpression()
	 * @generated
	 */
	EReference getParenthesisExpression_Expression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression <em>Range Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.RangeExpression
	 * @generated
	 */
	EClass getRangeExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.RangeExpression#getEnd()
	 * @see #getRangeExpression()
	 * @generated
	 */
	EAttribute getRangeExpression_End();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.RangeExpression#getStart()
	 * @see #getRangeExpression()
	 * @generated
	 */
	EAttribute getRangeExpression_Start();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression <em>Sub Array Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Array Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.SubArrayExpression
	 * @generated
	 */
	EClass getSubArrayExpression();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.SubArrayExpression#getExpression()
	 * @see #getSubArrayExpression()
	 * @generated
	 */
	EReference getSubArrayExpression_Expression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D <em>Sub Array Expression1 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Array Expression1 D</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D
	 * @generated
	 */
	EClass getSubArrayExpression1D();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getEndIndex <em>End Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Index</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getEndIndex()
	 * @see #getSubArrayExpression1D()
	 * @generated
	 */
	EAttribute getSubArrayExpression1D_EndIndex();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getStartIndex <em>Start Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Index</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.SubArrayExpression1D#getStartIndex()
	 * @see #getSubArrayExpression1D()
	 * @generated
	 */
	EAttribute getSubArrayExpression1D_StartIndex();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression <em>Ternary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ternary Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.TernaryExpression
	 * @generated
	 */
	EClass getTernaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.TernaryExpression#getOperator()
	 * @see #getTernaryExpression()
	 * @generated
	 */
	EAttribute getTernaryExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.TernaryExpression#getFirst()
	 * @see #getTernaryExpression()
	 * @generated
	 */
	EReference getTernaryExpression_First();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getSecond <em>Second</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Second</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.TernaryExpression#getSecond()
	 * @see #getTernaryExpression()
	 * @generated
	 */
	EReference getTernaryExpression_Second();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getThird <em>Third</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Third</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.TernaryExpression#getThird()
	 * @see #getTernaryExpression()
	 * @generated
	 */
	EReference getTernaryExpression_Third();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.UnaryExpression
	 * @generated
	 */
	EClass getUnaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gacodemodel.expression.UnaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.UnaryExpression#getOperator()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EAttribute getUnaryExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link geneauto.emf.models.gacodemodel.expression.UnaryExpression#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Argument</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.UnaryExpression#getArgument()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Argument();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.VariableExpression <em>Variable Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.VariableExpression
	 * @generated
	 */
	EClass getVariableExpression();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.expression.VariableExpression#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.VariableExpression#getVariable()
	 * @see #getVariableExpression()
	 * @generated
	 */
	EReference getVariableExpression_Variable();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.LValueExpression <em>LValue Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LValue Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.LValueExpression
	 * @generated
	 */
	EClass getLValueExpression();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExpressionFactory getExpressionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION__DATA_TYPE = eINSTANCE.getExpression_DataType();

		/**
		 * The meta object literal for the '<em><b>Index Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION__INDEX_EXPRESSIONS = eINSTANCE.getExpression_IndexExpressions();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.BinaryExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EXPRESSION__OPERATOR = eINSTANCE.getBinaryExpression_Operator();

		/**
		 * The meta object literal for the '<em><b>Right Argument</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__RIGHT_ARGUMENT = eINSTANCE.getBinaryExpression_RightArgument();

		/**
		 * The meta object literal for the '<em><b>Left Argument</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__LEFT_ARGUMENT = eINSTANCE.getBinaryExpression_LeftArgument();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.CallExpressionImpl <em>Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.CallExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getCallExpression()
		 * @generated
		 */
		EClass CALL_EXPRESSION = eINSTANCE.getCallExpression();

		/**
		 * The meta object literal for the '<em><b>Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_EXPRESSION__FUNCTION = eINSTANCE.getCallExpression_Function();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_EXPRESSION__DEPENDENCY = eINSTANCE.getCallExpression_Dependency();

		/**
		 * The meta object literal for the '<em><b>Arguments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_EXPRESSION__ARGUMENTS = eINSTANCE.getCallExpression_Arguments();

		/**
		 * The meta object literal for the '<em><b>Standard Function</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CALL_EXPRESSION__STANDARD_FUNCTION = eINSTANCE.getCallExpression_StandardFunction();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.LiteralExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getLiteralExpression()
		 * @generated
		 */
		EClass LITERAL_EXPRESSION = eINSTANCE.getLiteralExpression();

		/**
		 * The meta object literal for the '<em><b>Lit Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LITERAL_EXPRESSION__LIT_VALUE = eINSTANCE.getLiteralExpression_LitValue();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.BooleanExpressionImpl <em>Boolean Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.BooleanExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getBooleanExpression()
		 * @generated
		 */
		EClass BOOLEAN_EXPRESSION = eINSTANCE.getBooleanExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.FalseExpressionImpl <em>False Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.FalseExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getFalseExpression()
		 * @generated
		 */
		EClass FALSE_EXPRESSION = eINSTANCE.getFalseExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.TrueExpressionImpl <em>True Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.TrueExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getTrueExpression()
		 * @generated
		 */
		EClass TRUE_EXPRESSION = eINSTANCE.getTrueExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.NumericExpressionImpl <em>Numeric Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.NumericExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getNumericExpression()
		 * @generated
		 */
		EClass NUMERIC_EXPRESSION = eINSTANCE.getNumericExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.FloatingPointExpressionImpl <em>Floating Point Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.FloatingPointExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getFloatingPointExpression()
		 * @generated
		 */
		EClass FLOATING_POINT_EXPRESSION = eINSTANCE.getFloatingPointExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.DoubleExpressionImpl <em>Double Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.DoubleExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getDoubleExpression()
		 * @generated
		 */
		EClass DOUBLE_EXPRESSION = eINSTANCE.getDoubleExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.SingleExpressionImpl <em>Single Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.SingleExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getSingleExpression()
		 * @generated
		 */
		EClass SINGLE_EXPRESSION = eINSTANCE.getSingleExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.IntegerExpressionImpl <em>Integer Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.IntegerExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getIntegerExpression()
		 * @generated
		 */
		EClass INTEGER_EXPRESSION = eINSTANCE.getIntegerExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.StringExpressionImpl <em>String Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.StringExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getStringExpression()
		 * @generated
		 */
		EClass STRING_EXPRESSION = eINSTANCE.getStringExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ListExpressionImpl <em>List Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ListExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getListExpression()
		 * @generated
		 */
		EClass LIST_EXPRESSION = eINSTANCE.getListExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ConstantListExpressionImpl <em>Constant List Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ConstantListExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getConstantListExpression()
		 * @generated
		 */
		EClass CONSTANT_LIST_EXPRESSION = eINSTANCE.getConstantListExpression();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_LIST_EXPRESSION__LENGTH = eINSTANCE.getConstantListExpression_Length();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_LIST_EXPRESSION__EXPRESSION = eINSTANCE.getConstantListExpression_Expression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.GeneralListExpressionImpl <em>General List Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.GeneralListExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getGeneralListExpression()
		 * @generated
		 */
		EClass GENERAL_LIST_EXPRESSION = eINSTANCE.getGeneralListExpression();

		/**
		 * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERAL_LIST_EXPRESSION__EXPRESSIONS = eINSTANCE.getGeneralListExpression_Expressions();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.MemberExpressionImpl <em>Member Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.MemberExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getMemberExpression()
		 * @generated
		 */
		EClass MEMBER_EXPRESSION = eINSTANCE.getMemberExpression();

		/**
		 * The meta object literal for the '<em><b>Member</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMBER_EXPRESSION__MEMBER = eINSTANCE.getMemberExpression_Member();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMBER_EXPRESSION__EXPRESSION = eINSTANCE.getMemberExpression_Expression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.ParenthesisExpressionImpl <em>Parenthesis Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ParenthesisExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getParenthesisExpression()
		 * @generated
		 */
		EClass PARENTHESIS_EXPRESSION = eINSTANCE.getParenthesisExpression();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARENTHESIS_EXPRESSION__EXPRESSION = eINSTANCE.getParenthesisExpression_Expression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.RangeExpressionImpl <em>Range Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.RangeExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getRangeExpression()
		 * @generated
		 */
		EClass RANGE_EXPRESSION = eINSTANCE.getRangeExpression();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANGE_EXPRESSION__END = eINSTANCE.getRangeExpression_End();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANGE_EXPRESSION__START = eINSTANCE.getRangeExpression_Start();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpressionImpl <em>Sub Array Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getSubArrayExpression()
		 * @generated
		 */
		EClass SUB_ARRAY_EXPRESSION = eINSTANCE.getSubArrayExpression();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_ARRAY_EXPRESSION__EXPRESSION = eINSTANCE.getSubArrayExpression_Expression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpression1DImpl <em>Sub Array Expression1 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.SubArrayExpression1DImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getSubArrayExpression1D()
		 * @generated
		 */
		EClass SUB_ARRAY_EXPRESSION1_D = eINSTANCE.getSubArrayExpression1D();

		/**
		 * The meta object literal for the '<em><b>End Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_ARRAY_EXPRESSION1_D__END_INDEX = eINSTANCE.getSubArrayExpression1D_EndIndex();

		/**
		 * The meta object literal for the '<em><b>Start Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_ARRAY_EXPRESSION1_D__START_INDEX = eINSTANCE.getSubArrayExpression1D_StartIndex();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl <em>Ternary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.TernaryExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getTernaryExpression()
		 * @generated
		 */
		EClass TERNARY_EXPRESSION = eINSTANCE.getTernaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TERNARY_EXPRESSION__OPERATOR = eINSTANCE.getTernaryExpression_Operator();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TERNARY_EXPRESSION__FIRST = eINSTANCE.getTernaryExpression_First();

		/**
		 * The meta object literal for the '<em><b>Second</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TERNARY_EXPRESSION__SECOND = eINSTANCE.getTernaryExpression_Second();

		/**
		 * The meta object literal for the '<em><b>Third</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TERNARY_EXPRESSION__THIRD = eINSTANCE.getTernaryExpression_Third();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.UnaryExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getUnaryExpression()
		 * @generated
		 */
		EClass UNARY_EXPRESSION = eINSTANCE.getUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_EXPRESSION__OPERATOR = eINSTANCE.getUnaryExpression_Operator();

		/**
		 * The meta object literal for the '<em><b>Argument</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EXPRESSION__ARGUMENT = eINSTANCE.getUnaryExpression_Argument();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.impl.VariableExpressionImpl <em>Variable Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.impl.VariableExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getVariableExpression()
		 * @generated
		 */
		EClass VARIABLE_EXPRESSION = eINSTANCE.getVariableExpression();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_EXPRESSION__VARIABLE = eINSTANCE.getVariableExpression_Variable();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.LValueExpression <em>LValue Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.LValueExpression
		 * @see geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl#getLValueExpression()
		 * @generated
		 */
		EClass LVALUE_EXPRESSION = eINSTANCE.getLValueExpression();

	}

} //ExpressionPackage
