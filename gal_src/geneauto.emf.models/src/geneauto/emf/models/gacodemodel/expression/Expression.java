/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression;

import geneauto.emf.models.gacodemodel.GACodeModelElement;

import geneauto.emf.models.gadatatypes.GADataType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An expression is a sequence of operators and operands that specifies computation of a value, or that designates an object or a function, or that generates side effects, or that performs a combination thereof. In this CodeModel expressions have a pure meaning. I.e. they always return a value and cause no side-effects. E.g.: myFunction(5) + x2.  Absence of side effects is assumed for the expression itself and is not guaranteed in case the expression contains functions.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.Expression#getDataType <em>Data Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.Expression#getIndexExpressions <em>Index Expressions</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getExpression()
 * @model abstract="true"
 * @generated
 */
public interface Expression extends GACodeModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #setDataType(GADataType)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getExpression_DataType()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	GADataType getDataType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.Expression#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(GADataType value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.Expression#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDataType()
	 * @see #getDataType()
	 * @see #setDataType(GADataType)
	 * @generated
	 */
	void unsetDataType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.Expression#getDataType <em>Data Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Data Type</em>' containment reference is set.
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @see #setDataType(GADataType)
	 * @generated
	 */
	boolean isSetDataType();

	/**
	 * Returns the value of the '<em><b>Index Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.expression.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Expressions</em>' containment reference list.
	 * @see #isSetIndexExpressions()
	 * @see #unsetIndexExpressions()
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getExpression_IndexExpressions()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Expression> getIndexExpressions();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.Expression#getIndexExpressions <em>Index Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIndexExpressions()
	 * @see #getIndexExpressions()
	 * @generated
	 */
	void unsetIndexExpressions();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.Expression#getIndexExpressions <em>Index Expressions</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Index Expressions</em>' containment reference list is set.
	 * @see #unsetIndexExpressions()
	 * @see #getIndexExpressions()
	 * @generated
	 */
	boolean isSetIndexExpressions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Expression Get_value();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GADataType Get_derivedDataType();

} // Expression
