/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression;

import geneauto.emf.models.gadatatypes.GADataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * RangeExpression represents a finite set of integer values identified by start value and end value. The range expression is used for expressing iteration steps.  The dataType of the RangeExpression is a type for a range iterator variable. Any numeric type can be used, however, the value will be always integer. If the type is not explicitly defined in the model, the smallest integer type suitable for accommodating the iterator values is assigned.  If named range expression types are used (e.g. in ADA) the data type is the Derived type expressing the range datatype.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getEnd <em>End</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getStart <em>Start</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getRangeExpression()
 * @model
 * @generated
 */
public interface RangeExpression extends Expression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' attribute.
	 * @see #isSetEnd()
	 * @see #unsetEnd()
	 * @see #setEnd(int)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getRangeExpression_End()
	 * @model unsettable="true"
	 * @generated
	 */
	int getEnd();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getEnd <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' attribute.
	 * @see #isSetEnd()
	 * @see #unsetEnd()
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getEnd <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEnd()
	 * @see #getEnd()
	 * @see #setEnd(int)
	 * @generated
	 */
	void unsetEnd();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getEnd <em>End</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>End</em>' attribute is set.
	 * @see #unsetEnd()
	 * @see #getEnd()
	 * @see #setEnd(int)
	 * @generated
	 */
	boolean isSetEnd();

	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #isSetStart()
	 * @see #unsetStart()
	 * @see #setStart(int)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getRangeExpression_Start()
	 * @model unsettable="true"
	 * @generated
	 */
	int getStart();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #isSetStart()
	 * @see #unsetStart()
	 * @see #getStart()
	 * @generated
	 */
	void setStart(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStart()
	 * @see #getStart()
	 * @see #setStart(int)
	 * @generated
	 */
	void unsetStart();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.RangeExpression#getStart <em>Start</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Start</em>' attribute is set.
	 * @see #unsetStart()
	 * @see #getStart()
	 * @see #setStart(int)
	 * @generated
	 */
	boolean isSetStart();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Expression Get_value();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GADataType Get_derivedDataType();

} // RangeExpression
