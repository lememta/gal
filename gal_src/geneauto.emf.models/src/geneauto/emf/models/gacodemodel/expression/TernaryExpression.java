/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression;

import geneauto.emf.models.gacodemodel.operator.TernaryOperator;

import geneauto.emf.models.gadatatypes.GADataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ternary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Ternary Expression - expression with a ternary operator and 3 operands
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getOperator <em>Operator</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getFirst <em>First</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getSecond <em>Second</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getThird <em>Third</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getTernaryExpression()
 * @model
 * @generated
 */
public interface TernaryExpression extends Expression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gacodemodel.operator.TernaryOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.operator.TernaryOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #setOperator(TernaryOperator)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getTernaryExpression_Operator()
	 * @model unsettable="true"
	 * @generated
	 */
	TernaryOperator getOperator();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.operator.TernaryOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(TernaryOperator value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperator()
	 * @see #getOperator()
	 * @see #setOperator(TernaryOperator)
	 * @generated
	 */
	void unsetOperator();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getOperator <em>Operator</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operator</em>' attribute is set.
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @see #setOperator(TernaryOperator)
	 * @generated
	 */
	boolean isSetOperator();

	/**
	 * Returns the value of the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' containment reference.
	 * @see #isSetFirst()
	 * @see #unsetFirst()
	 * @see #setFirst(Expression)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getTernaryExpression_First()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getFirst();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getFirst <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' containment reference.
	 * @see #isSetFirst()
	 * @see #unsetFirst()
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getFirst <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFirst()
	 * @see #getFirst()
	 * @see #setFirst(Expression)
	 * @generated
	 */
	void unsetFirst();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getFirst <em>First</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>First</em>' containment reference is set.
	 * @see #unsetFirst()
	 * @see #getFirst()
	 * @see #setFirst(Expression)
	 * @generated
	 */
	boolean isSetFirst();

	/**
	 * Returns the value of the '<em><b>Second</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second</em>' containment reference.
	 * @see #isSetSecond()
	 * @see #unsetSecond()
	 * @see #setSecond(Expression)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getTernaryExpression_Second()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getSecond();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getSecond <em>Second</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second</em>' containment reference.
	 * @see #isSetSecond()
	 * @see #unsetSecond()
	 * @see #getSecond()
	 * @generated
	 */
	void setSecond(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getSecond <em>Second</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSecond()
	 * @see #getSecond()
	 * @see #setSecond(Expression)
	 * @generated
	 */
	void unsetSecond();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getSecond <em>Second</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Second</em>' containment reference is set.
	 * @see #unsetSecond()
	 * @see #getSecond()
	 * @see #setSecond(Expression)
	 * @generated
	 */
	boolean isSetSecond();

	/**
	 * Returns the value of the '<em><b>Third</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Third</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Third</em>' containment reference.
	 * @see #isSetThird()
	 * @see #unsetThird()
	 * @see #setThird(Expression)
	 * @see geneauto.emf.models.gacodemodel.expression.ExpressionPackage#getTernaryExpression_Third()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	Expression getThird();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getThird <em>Third</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Third</em>' containment reference.
	 * @see #isSetThird()
	 * @see #unsetThird()
	 * @see #getThird()
	 * @generated
	 */
	void setThird(Expression value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getThird <em>Third</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetThird()
	 * @see #getThird()
	 * @see #setThird(Expression)
	 * @generated
	 */
	void unsetThird();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.expression.TernaryExpression#getThird <em>Third</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Third</em>' containment reference is set.
	 * @see #unsetThird()
	 * @see #getThird()
	 * @see #setThird(Expression)
	 * @generated
	 */
	boolean isSetThird();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Expression Get_value();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GADataType Get_derivedDataType();

} // TernaryExpression
