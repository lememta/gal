/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.expression.statemodel;

import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.expression.statemodel.StatemodelFactory
 * @model kind="package"
 * @generated
 */
public interface StatemodelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "statemodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gacodemodel/expression/statemodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gacodemodel.expression.statemodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatemodelPackage eINSTANCE = geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.statemodel.impl.StateModelExpressionImpl <em>State Model Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StateModelExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl#getStateModelExpression()
	 * @generated
	 */
	int STATE_MODEL_EXPRESSION = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__EXTERNAL_ID = ExpressionPackage.EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__ID = ExpressionPackage.EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__IS_FIXED_NAME = ExpressionPackage.EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__NAME = ExpressionPackage.EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__ORIGINAL_FULL_NAME = ExpressionPackage.EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__ORIGINAL_NAME = ExpressionPackage.EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__PARENT = ExpressionPackage.EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__MODEL = ExpressionPackage.EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__ANNOTATIONS = ExpressionPackage.EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__SOURCE_ELEMENT = ExpressionPackage.EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__SOURCE_ELEMENT_ID = ExpressionPackage.EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__SOURCE_ACTION = ExpressionPackage.EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__MODULE = ExpressionPackage.EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__DATA_TYPE = ExpressionPackage.EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION__INDEX_EXPRESSIONS = ExpressionPackage.EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The number of structural features of the '<em>State Model Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MODEL_EXPRESSION_FEATURE_COUNT = ExpressionPackage.EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsEventExpressionImpl <em>Is Event Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsEventExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl#getIsEventExpression()
	 * @generated
	 */
	int IS_EVENT_EXPRESSION = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__EXTERNAL_ID = STATE_MODEL_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__ID = STATE_MODEL_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__IS_FIXED_NAME = STATE_MODEL_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__NAME = STATE_MODEL_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__ORIGINAL_FULL_NAME = STATE_MODEL_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__ORIGINAL_NAME = STATE_MODEL_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__PARENT = STATE_MODEL_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__MODEL = STATE_MODEL_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__ANNOTATIONS = STATE_MODEL_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__SOURCE_ELEMENT = STATE_MODEL_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__SOURCE_ELEMENT_ID = STATE_MODEL_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__SOURCE_ACTION = STATE_MODEL_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__MODULE = STATE_MODEL_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__DATA_TYPE = STATE_MODEL_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__INDEX_EXPRESSIONS = STATE_MODEL_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION__EVENT = STATE_MODEL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Is Event Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_EVENT_EXPRESSION_FEATURE_COUNT = STATE_MODEL_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsOpenExpressionImpl <em>Is Open Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsOpenExpressionImpl
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl#getIsOpenExpression()
	 * @generated
	 */
	int IS_OPEN_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__EXTERNAL_ID = STATE_MODEL_EXPRESSION__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__ID = STATE_MODEL_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__IS_FIXED_NAME = STATE_MODEL_EXPRESSION__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__NAME = STATE_MODEL_EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__ORIGINAL_FULL_NAME = STATE_MODEL_EXPRESSION__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__ORIGINAL_NAME = STATE_MODEL_EXPRESSION__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__PARENT = STATE_MODEL_EXPRESSION__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__MODEL = STATE_MODEL_EXPRESSION__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__ANNOTATIONS = STATE_MODEL_EXPRESSION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__SOURCE_ELEMENT = STATE_MODEL_EXPRESSION__SOURCE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__SOURCE_ELEMENT_ID = STATE_MODEL_EXPRESSION__SOURCE_ELEMENT_ID;

	/**
	 * The feature id for the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__SOURCE_ACTION = STATE_MODEL_EXPRESSION__SOURCE_ACTION;

	/**
	 * The feature id for the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__MODULE = STATE_MODEL_EXPRESSION__MODULE;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__DATA_TYPE = STATE_MODEL_EXPRESSION__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Index Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__INDEX_EXPRESSIONS = STATE_MODEL_EXPRESSION__INDEX_EXPRESSIONS;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION__LOCATION = STATE_MODEL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Is Open Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_OPEN_EXPRESSION_FEATURE_COUNT = STATE_MODEL_EXPRESSION_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.statemodel.IsEventExpression <em>Is Event Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Event Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.IsEventExpression
	 * @generated
	 */
	EClass getIsEventExpression();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.expression.statemodel.IsEventExpression#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.IsEventExpression#getEvent()
	 * @see #getIsEventExpression()
	 * @generated
	 */
	EReference getIsEventExpression_Event();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.statemodel.StateModelExpression <em>State Model Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Model Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.StateModelExpression
	 * @generated
	 */
	EClass getStateModelExpression();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gacodemodel.expression.statemodel.IsOpenExpression <em>Is Open Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Open Expression</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.IsOpenExpression
	 * @generated
	 */
	EClass getIsOpenExpression();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gacodemodel.expression.statemodel.IsOpenExpression#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see geneauto.emf.models.gacodemodel.expression.statemodel.IsOpenExpression#getLocation()
	 * @see #getIsOpenExpression()
	 * @generated
	 */
	EReference getIsOpenExpression_Location();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatemodelFactory getStatemodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsEventExpressionImpl <em>Is Event Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsEventExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl#getIsEventExpression()
		 * @generated
		 */
		EClass IS_EVENT_EXPRESSION = eINSTANCE.getIsEventExpression();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IS_EVENT_EXPRESSION__EVENT = eINSTANCE.getIsEventExpression_Event();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.statemodel.impl.StateModelExpressionImpl <em>State Model Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StateModelExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl#getStateModelExpression()
		 * @generated
		 */
		EClass STATE_MODEL_EXPRESSION = eINSTANCE.getStateModelExpression();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsOpenExpressionImpl <em>Is Open Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.IsOpenExpressionImpl
		 * @see geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl#getIsOpenExpression()
		 * @generated
		 */
		EClass IS_OPEN_EXPRESSION = eINSTANCE.getIsOpenExpression();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IS_OPEN_EXPRESSION__LOCATION = eINSTANCE.getIsOpenExpression_Location();

	}

} //StatemodelPackage
