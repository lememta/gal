/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.gacodemodel.expression.Expression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Name Space</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Symbol table for code generation. Statements and expressions can refer to this table. For instance a VariableExpression may contain a reference to the specific section of the NameSpace The final code generation phase retrieves the actual piece of code (usually an identifier, but could be also a section of code) that corresponds to some part of the expression.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.NameSpace#getNsElements <em>Ns Elements</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.NameSpace#getImplicitArgumentValues <em>Implicit Argument Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getNameSpace()
 * @model
 * @generated
 */
public interface NameSpace extends GACodeModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Ns Elements</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.NameSpaceElement_CM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ns Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ns Elements</em>' containment reference list.
	 * @see #isSetNsElements()
	 * @see #unsetNsElements()
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getNameSpace_NsElements()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<NameSpaceElement_CM> getNsElements();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.NameSpace#getNsElements <em>Ns Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNsElements()
	 * @see #getNsElements()
	 * @generated
	 */
	void unsetNsElements();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.NameSpace#getNsElements <em>Ns Elements</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Ns Elements</em>' containment reference list is set.
	 * @see #unsetNsElements()
	 * @see #getNsElements()
	 * @generated
	 */
	boolean isSetNsElements();

	/**
	 * Returns the value of the '<em><b>Implicit Argument Values</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.expression.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implicit Argument Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implicit Argument Values</em>' containment reference list.
	 * @see #isSetImplicitArgumentValues()
	 * @see #unsetImplicitArgumentValues()
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getNameSpace_ImplicitArgumentValues()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Expression> getImplicitArgumentValues();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.NameSpace#getImplicitArgumentValues <em>Implicit Argument Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetImplicitArgumentValues()
	 * @see #getImplicitArgumentValues()
	 * @generated
	 */
	void unsetImplicitArgumentValues();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.NameSpace#getImplicitArgumentValues <em>Implicit Argument Values</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Implicit Argument Values</em>' containment reference list is set.
	 * @see #unsetImplicitArgumentValues()
	 * @see #getImplicitArgumentValues()
	 * @generated
	 */
	boolean isSetImplicitArgumentValues();

} // NameSpace
