/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.gaenumtypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Type Inheritance Rule</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Simulink Data Type Inheritance Rule.  Enums to be detailed according to need.
 * <!-- end-model-doc -->
 * @see geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesPackage#getTypeInheritanceRule()
 * @model
 * @generated
 */
public enum TypeInheritanceRule implements Enumerator {
	/**
	 * The '<em><b>Inherit Via Back Propagation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INHERIT_VIA_BACK_PROPAGATION_VALUE
	 * @generated
	 * @ordered
	 */
	INHERIT_VIA_BACK_PROPAGATION(0, "InheritViaBackPropagation", "InheritViaBackPropagation"),

	/**
	 * The '<em><b>Inherit From Constant Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INHERIT_FROM_CONSTANT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	INHERIT_FROM_CONSTANT_VALUE(1, "InheritFromConstantValue", "InheritFromConstantValue"),

	/**
	 * The '<em><b>Same As Input</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAME_AS_INPUT_VALUE
	 * @generated
	 * @ordered
	 */
	SAME_AS_INPUT(2, "SameAsInput", "SameAsInput"),

	/**
	 * The '<em><b>Same As First Input</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAME_AS_FIRST_INPUT_VALUE
	 * @generated
	 * @ordered
	 */
	SAME_AS_FIRST_INPUT(3, "SameAsFirstInput", "SameAsFirstInput");

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The '<em><b>Inherit Via Back Propagation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Inherit Via Back Propagation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INHERIT_VIA_BACK_PROPAGATION
	 * @model name="InheritViaBackPropagation"
	 * @generated
	 * @ordered
	 */
	public static final int INHERIT_VIA_BACK_PROPAGATION_VALUE = 0;

	/**
	 * The '<em><b>Inherit From Constant Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Inherit From Constant Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INHERIT_FROM_CONSTANT_VALUE
	 * @model name="InheritFromConstantValue"
	 * @generated
	 * @ordered
	 */
	public static final int INHERIT_FROM_CONSTANT_VALUE_VALUE = 1;

	/**
	 * The '<em><b>Same As Input</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Same As Input</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAME_AS_INPUT
	 * @model name="SameAsInput"
	 * @generated
	 * @ordered
	 */
	public static final int SAME_AS_INPUT_VALUE = 2;

	/**
	 * The '<em><b>Same As First Input</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Same As First Input</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAME_AS_FIRST_INPUT
	 * @model name="SameAsFirstInput"
	 * @generated
	 * @ordered
	 */
	public static final int SAME_AS_FIRST_INPUT_VALUE = 3;

	/**
	 * An array of all the '<em><b>Type Inheritance Rule</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final TypeInheritanceRule[] VALUES_ARRAY =
		new TypeInheritanceRule[] {
			INHERIT_VIA_BACK_PROPAGATION,
			INHERIT_FROM_CONSTANT_VALUE,
			SAME_AS_INPUT,
			SAME_AS_FIRST_INPUT,
		};

	/**
	 * A public read-only list of all the '<em><b>Type Inheritance Rule</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<TypeInheritanceRule> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Type Inheritance Rule</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TypeInheritanceRule get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TypeInheritanceRule result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Type Inheritance Rule</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TypeInheritanceRule getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TypeInheritanceRule result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Type Inheritance Rule</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TypeInheritanceRule get(int value) {
		switch (value) {
			case INHERIT_VIA_BACK_PROPAGATION_VALUE: return INHERIT_VIA_BACK_PROPAGATION;
			case INHERIT_FROM_CONSTANT_VALUE_VALUE: return INHERIT_FROM_CONSTANT_VALUE;
			case SAME_AS_INPUT_VALUE: return SAME_AS_INPUT;
			case SAME_AS_FIRST_INPUT_VALUE: return SAME_AS_FIRST_INPUT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private TypeInheritanceRule(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //TypeInheritanceRule
