/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.gaenumtypes;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesFactory
 * @model kind="package"
 * @generated
 */
public interface GaenumtypesPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gaenumtypes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gacodemodel/gaenumtypes.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gacodemodel.gaenumtypes";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GaenumtypesPackage eINSTANCE = geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.ArrayDefinitionStyle <em>Array Definition Style</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.ArrayDefinitionStyle
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getArrayDefinitionStyle()
	 * @generated
	 */
	int ARRAY_DEFINITION_STYLE = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.Direction <em>Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.Direction
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getDirection()
	 * @generated
	 */
	int DIRECTION = 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle <em>Function Style</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getFunctionStyle()
	 * @generated
	 */
	int FUNCTION_STYLE = 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule <em>Type Inheritance Rule</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getTypeInheritanceRule()
	 * @generated
	 */
	int TYPE_INHERITANCE_RULE = 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.DefinitionScope <em>Definition Scope</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.DefinitionScope
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getDefinitionScope()
	 * @generated
	 */
	int DEFINITION_SCOPE = 4;


	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.gaenumtypes.ArrayDefinitionStyle <em>Array Definition Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Array Definition Style</em>'.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.ArrayDefinitionStyle
	 * @generated
	 */
	EEnum getArrayDefinitionStyle();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.gaenumtypes.Direction <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Direction</em>'.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.Direction
	 * @generated
	 */
	EEnum getDirection();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle <em>Function Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Function Style</em>'.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle
	 * @generated
	 */
	EEnum getFunctionStyle();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule <em>Type Inheritance Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type Inheritance Rule</em>'.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule
	 * @generated
	 */
	EEnum getTypeInheritanceRule();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gacodemodel.gaenumtypes.DefinitionScope <em>Definition Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Definition Scope</em>'.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.DefinitionScope
	 * @generated
	 */
	EEnum getDefinitionScope();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GaenumtypesFactory getGaenumtypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.ArrayDefinitionStyle <em>Array Definition Style</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.ArrayDefinitionStyle
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getArrayDefinitionStyle()
		 * @generated
		 */
		EEnum ARRAY_DEFINITION_STYLE = eINSTANCE.getArrayDefinitionStyle();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.Direction <em>Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.Direction
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getDirection()
		 * @generated
		 */
		EEnum DIRECTION = eINSTANCE.getDirection();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle <em>Function Style</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getFunctionStyle()
		 * @generated
		 */
		EEnum FUNCTION_STYLE = eINSTANCE.getFunctionStyle();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule <em>Type Inheritance Rule</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getTypeInheritanceRule()
		 * @generated
		 */
		EEnum TYPE_INHERITANCE_RULE = eINSTANCE.getTypeInheritanceRule();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gacodemodel.gaenumtypes.DefinitionScope <em>Definition Scope</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.DefinitionScope
		 * @see geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl#getDefinitionScope()
		 * @generated
		 */
		EEnum DEFINITION_SCOPE = eINSTANCE.getDefinitionScope();

	}

} //GaenumtypesPackage
