/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.gaenumtypes.impl;

import geneauto.emf.models.common.CommonPackage;

import geneauto.emf.models.common.impl.CommonPackageImpl;

import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;

import geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;

import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;

import geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl;

import geneauto.emf.models.gacodemodel.expression.statemodel.StatemodelPackage;

import geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl;

import geneauto.emf.models.gacodemodel.gaenumtypes.ArrayDefinitionStyle;
import geneauto.emf.models.gacodemodel.gaenumtypes.DefinitionScope;
import geneauto.emf.models.gacodemodel.gaenumtypes.Direction;
import geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle;
import geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesFactory;
import geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesPackage;
import geneauto.emf.models.gacodemodel.gaenumtypes.TypeInheritanceRule;

import geneauto.emf.models.gacodemodel.impl.GacodemodelPackageImpl;

import geneauto.emf.models.gacodemodel.operator.OperatorPackage;

import geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl;

import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastPackage;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl;

import geneauto.emf.models.gadatatypes.GadatatypesPackage;

import geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.GafunctionalmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl;

import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;

import geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl;

import geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GaenumtypesPackageImpl extends EPackageImpl implements GaenumtypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum arrayDefinitionStyleEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum directionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum functionStyleEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum typeInheritanceRuleEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum definitionScopeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GaenumtypesPackageImpl() {
		super(eNS_URI, GaenumtypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GaenumtypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GaenumtypesPackage init() {
		if (isInited) return (GaenumtypesPackage)EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI);

		// Obtain or create and register package
		GaenumtypesPackageImpl theGaenumtypesPackage = (GaenumtypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GaenumtypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GaenumtypesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) : CommonPackage.eINSTANCE);
		GadatatypesPackageImpl theGadatatypesPackage = (GadatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) instanceof GadatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) : GadatatypesPackage.eINSTANCE);
		GenericmodelPackageImpl theGenericmodelPackage = (GenericmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) instanceof GenericmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) : GenericmodelPackage.eINSTANCE);
		GacodemodelPackageImpl theGacodemodelPackage = (GacodemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) instanceof GacodemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI) : GacodemodelPackage.eINSTANCE);
		ExpressionPackageImpl theExpressionPackage = (ExpressionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) instanceof ExpressionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) : ExpressionPackage.eINSTANCE);
		StatemodelPackageImpl theStatemodelPackage = (StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) instanceof StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) : StatemodelPackage.eINSTANCE);
		StatementPackageImpl theStatementPackage = (StatementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) instanceof StatementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) : StatementPackage.eINSTANCE);
		geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl theStatemodelPackage_1 = (geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) instanceof geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) : geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eINSTANCE);
		BroadcastPackageImpl theBroadcastPackage = (BroadcastPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) instanceof BroadcastPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) : BroadcastPackage.eINSTANCE);
		OperatorPackageImpl theOperatorPackage = (OperatorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) instanceof OperatorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) : OperatorPackage.eINSTANCE);
		GasystemmodelPackageImpl theGasystemmodelPackage = (GasystemmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) instanceof GasystemmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) : GasystemmodelPackage.eINSTANCE);
		geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl theCommonPackage_1 = (geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) instanceof geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) : geneauto.emf.models.gasystemmodel.common.CommonPackage.eINSTANCE);
		GastatemodelPackageImpl theGastatemodelPackage = (GastatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) instanceof GastatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) : GastatemodelPackage.eINSTANCE);
		GafunctionalmodelPackageImpl theGafunctionalmodelPackage = (GafunctionalmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) instanceof GafunctionalmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) : GafunctionalmodelPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);
		PortsPackageImpl thePortsPackage = (PortsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) instanceof PortsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) : PortsPackage.eINSTANCE);
		GablocklibraryPackageImpl theGablocklibraryPackage = (GablocklibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) instanceof GablocklibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) : GablocklibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theGaenumtypesPackage.createPackageContents();
		theCommonPackage.createPackageContents();
		theGadatatypesPackage.createPackageContents();
		theGenericmodelPackage.createPackageContents();
		theGacodemodelPackage.createPackageContents();
		theExpressionPackage.createPackageContents();
		theStatemodelPackage.createPackageContents();
		theStatementPackage.createPackageContents();
		theStatemodelPackage_1.createPackageContents();
		theBroadcastPackage.createPackageContents();
		theOperatorPackage.createPackageContents();
		theGasystemmodelPackage.createPackageContents();
		theCommonPackage_1.createPackageContents();
		theGastatemodelPackage.createPackageContents();
		theGafunctionalmodelPackage.createPackageContents();
		theBlocksPackage.createPackageContents();
		thePortsPackage.createPackageContents();
		theGablocklibraryPackage.createPackageContents();

		// Initialize created meta-data
		theGaenumtypesPackage.initializePackageContents();
		theCommonPackage.initializePackageContents();
		theGadatatypesPackage.initializePackageContents();
		theGenericmodelPackage.initializePackageContents();
		theGacodemodelPackage.initializePackageContents();
		theExpressionPackage.initializePackageContents();
		theStatemodelPackage.initializePackageContents();
		theStatementPackage.initializePackageContents();
		theStatemodelPackage_1.initializePackageContents();
		theBroadcastPackage.initializePackageContents();
		theOperatorPackage.initializePackageContents();
		theGasystemmodelPackage.initializePackageContents();
		theCommonPackage_1.initializePackageContents();
		theGastatemodelPackage.initializePackageContents();
		theGafunctionalmodelPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();
		thePortsPackage.initializePackageContents();
		theGablocklibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGaenumtypesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GaenumtypesPackage.eNS_URI, theGaenumtypesPackage);
		return theGaenumtypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getArrayDefinitionStyle() {
		return arrayDefinitionStyleEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDirection() {
		return directionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFunctionStyle() {
		return functionStyleEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTypeInheritanceRule() {
		return typeInheritanceRuleEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDefinitionScope() {
		return definitionScopeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GaenumtypesFactory getGaenumtypesFactory() {
		return (GaenumtypesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create enums
		arrayDefinitionStyleEEnum = createEEnum(ARRAY_DEFINITION_STYLE);
		directionEEnum = createEEnum(DIRECTION);
		functionStyleEEnum = createEEnum(FUNCTION_STYLE);
		typeInheritanceRuleEEnum = createEEnum(TYPE_INHERITANCE_RULE);
		definitionScopeEEnum = createEEnum(DEFINITION_SCOPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Initialize enums and add enum literals
		initEEnum(arrayDefinitionStyleEEnum, ArrayDefinitionStyle.class, "ArrayDefinitionStyle");
		addEEnumLiteral(arrayDefinitionStyleEEnum, ArrayDefinitionStyle.DEFAULT);
		addEEnumLiteral(arrayDefinitionStyleEEnum, ArrayDefinitionStyle.ARRAYS_AS_POINTERS);

		initEEnum(directionEEnum, Direction.class, "Direction");
		addEEnumLiteral(directionEEnum, Direction.IN);
		addEEnumLiteral(directionEEnum, Direction.OUT);
		addEEnumLiteral(directionEEnum, Direction.IN_OUT);

		initEEnum(functionStyleEEnum, FunctionStyle.class, "FunctionStyle");
		addEEnumLiteral(functionStyleEEnum, FunctionStyle.FUNCTION);
		addEEnumLiteral(functionStyleEEnum, FunctionStyle.MACRO);
		addEEnumLiteral(functionStyleEEnum, FunctionStyle.INLINE);
		addEEnumLiteral(functionStyleEEnum, FunctionStyle.INLINE_FUNCTION);
		addEEnumLiteral(functionStyleEEnum, FunctionStyle.REUSABLE_FUNCTION);

		initEEnum(typeInheritanceRuleEEnum, TypeInheritanceRule.class, "TypeInheritanceRule");
		addEEnumLiteral(typeInheritanceRuleEEnum, TypeInheritanceRule.INHERIT_VIA_BACK_PROPAGATION);
		addEEnumLiteral(typeInheritanceRuleEEnum, TypeInheritanceRule.INHERIT_FROM_CONSTANT_VALUE);
		addEEnumLiteral(typeInheritanceRuleEEnum, TypeInheritanceRule.SAME_AS_INPUT);
		addEEnumLiteral(typeInheritanceRuleEEnum, TypeInheritanceRule.SAME_AS_FIRST_INPUT);

		initEEnum(definitionScopeEEnum, DefinitionScope.class, "DefinitionScope");
		addEEnumLiteral(definitionScopeEEnum, DefinitionScope.LOCAL);
		addEEnumLiteral(definitionScopeEEnum, DefinitionScope.FUNCTION_OUTPUT);
		addEEnumLiteral(definitionScopeEEnum, DefinitionScope.IMPORTED);
		addEEnumLiteral(definitionScopeEEnum, DefinitionScope.IMPORTED_DECLARED);
		addEEnumLiteral(definitionScopeEEnum, DefinitionScope.EXPORTED);
	}

} //GaenumtypesPackageImpl
