/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.genericmodel.GAModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GA Code Model Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Abstract base class for all Gene-Auto Code Model language elements
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElement <em>Source Element</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElementId <em>Source Element Id</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceAction <em>Source Action</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getModule <em>Module</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModelElement()
 * @model abstract="true"
 * @generated
 */
public interface GACodeModelElement extends GAModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Source Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A GASystemModelElement that is the source (origin) of this GACodeModel element.
	 * 
	 * Note that 'sourceElement' is a direct object reference. Another feature 'sourceElementId' provides an indirect reference through the element's id. This can be used to simplify references, when the two models are serialized in different files.
	 * 
	 * At most one of 'sourceElement' or 'sourceElementId' should be assigned.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Source Element</em>' reference.
	 * @see #isSetSourceElement()
	 * @see #unsetSourceElement()
	 * @see #setSourceElement(GASystemModelElement)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModelElement_SourceElement()
	 * @model unsettable="true"
	 * @generated
	 */
	GASystemModelElement getSourceElement();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElement <em>Source Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Element</em>' reference.
	 * @see #isSetSourceElement()
	 * @see #unsetSourceElement()
	 * @see #getSourceElement()
	 * @generated
	 */
	void setSourceElement(GASystemModelElement value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElement <em>Source Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSourceElement()
	 * @see #getSourceElement()
	 * @see #setSourceElement(GASystemModelElement)
	 * @generated
	 */
	void unsetSourceElement();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElement <em>Source Element</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Source Element</em>' reference is set.
	 * @see #unsetSourceElement()
	 * @see #getSourceElement()
	 * @see #setSourceElement(GASystemModelElement)
	 * @generated
	 */
	boolean isSetSourceElement();

	/**
	 * Returns the value of the '<em><b>Source Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Indirect reference to the GASystemModelElement that is the source (origin) of this GACodeModel element.
	 * 
	 * Note that another feature 'sourceElement' provides a direct reference to that element.
	 * 
	 * At most one of 'sourceElement' or 'sourceElementId' should be assigned.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Source Element Id</em>' attribute.
	 * @see #setSourceElementId(int)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModelElement_SourceElementId()
	 * @model
	 * @generated
	 */
	int getSourceElementId();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceElementId <em>Source Element Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Element Id</em>' attribute.
	 * @see #getSourceElementId()
	 * @generated
	 */
	void setSourceElementId(int value);

	/**
	 * Returns the value of the '<em><b>Source Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Optional qualifier that specifies the role/action of the source GASystemModelElement that generated this GACodeModelElement.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Source Action</em>' attribute.
	 * @see #setSourceAction(String)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModelElement_SourceAction()
	 * @model
	 * @generated
	 */
	String getSourceAction();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getSourceAction <em>Source Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Action</em>' attribute.
	 * @see #getSourceAction()
	 * @generated
	 */
	void setSourceAction(String value);

	/**
	 * Returns the value of the '<em><b>Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module</em>' reference.
	 * @see #isSetModule()
	 * @see #unsetModule()
	 * @see #setModule(Module)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getGACodeModelElement_Module()
	 * @model unsettable="true" transient="true"
	 * @generated
	 */
	Module getModule();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getModule <em>Module</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Module</em>' reference.
	 * @see #isSetModule()
	 * @see #unsetModule()
	 * @see #getModule()
	 * @generated
	 */
	void setModule(Module value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getModule <em>Module</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetModule()
	 * @see #getModule()
	 * @see #setModule(Module)
	 * @generated
	 */
	void unsetModule();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.GACodeModelElement#getModule <em>Module</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Module</em>' reference is set.
	 * @see #unsetModule()
	 * @see #getModule()
	 * @see #setModule(Module)
	 * @generated
	 */
	boolean isSetModule();

} // GACodeModelElement
