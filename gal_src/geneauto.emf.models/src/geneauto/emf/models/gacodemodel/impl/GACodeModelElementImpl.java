/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.GACodeModelElement;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.Module;

import geneauto.emf.models.gasystemmodel.GASystemModelElement;

import geneauto.emf.models.genericmodel.impl.GAModelElementImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GA Code Model Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl#getSourceElement <em>Source Element</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl#getSourceElementId <em>Source Element Id</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl#getSourceAction <em>Source Action</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.GACodeModelElementImpl#getModule <em>Module</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class GACodeModelElementImpl extends GAModelElementImpl implements GACodeModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getSourceElement() <em>Source Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceElement()
	 * @generated
	 * @ordered
	 */
	protected GASystemModelElement sourceElement;

	/**
	 * This is true if the Source Element reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sourceElementESet;

	/**
	 * The default value of the '{@link #getSourceElementId() <em>Source Element Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceElementId()
	 * @generated
	 * @ordered
	 */
	protected static final int SOURCE_ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSourceElementId() <em>Source Element Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceElementId()
	 * @generated
	 * @ordered
	 */
	protected int sourceElementId = SOURCE_ELEMENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceAction() <em>Source Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceAction()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_ACTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceAction() <em>Source Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceAction()
	 * @generated
	 * @ordered
	 */
	protected String sourceAction = SOURCE_ACTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getModule() <em>Module</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModule()
	 * @generated
	 * @ordered
	 */
	protected Module module;

	/**
	 * This is true if the Module reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean moduleESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GACodeModelElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.GA_CODE_MODEL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GASystemModelElement getSourceElement() {
		if (sourceElement != null && sourceElement.eIsProxy()) {
			InternalEObject oldSourceElement = (InternalEObject)sourceElement;
			sourceElement = (GASystemModelElement)eResolveProxy(oldSourceElement);
			if (sourceElement != oldSourceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT, oldSourceElement, sourceElement));
			}
		}
		return sourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GASystemModelElement basicGetSourceElement() {
		return sourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceElement(GASystemModelElement newSourceElement) {
		GASystemModelElement oldSourceElement = sourceElement;
		sourceElement = newSourceElement;
		boolean oldSourceElementESet = sourceElementESet;
		sourceElementESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT, oldSourceElement, sourceElement, !oldSourceElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSourceElement() {
		GASystemModelElement oldSourceElement = sourceElement;
		boolean oldSourceElementESet = sourceElementESet;
		sourceElement = null;
		sourceElementESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT, oldSourceElement, null, oldSourceElementESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSourceElement() {
		return sourceElementESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSourceElementId() {
		return sourceElementId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceElementId(int newSourceElementId) {
		int oldSourceElementId = sourceElementId;
		sourceElementId = newSourceElementId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID, oldSourceElementId, sourceElementId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceAction() {
		return sourceAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceAction(String newSourceAction) {
		String oldSourceAction = sourceAction;
		sourceAction = newSourceAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ACTION, oldSourceAction, sourceAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Module getModule() {
		if (module != null && module.eIsProxy()) {
			InternalEObject oldModule = (InternalEObject)module;
			module = (Module)eResolveProxy(oldModule);
			if (module != oldModule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE, oldModule, module));
			}
		}
		return module;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Module basicGetModule() {
		return module;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModule(Module newModule) {
		Module oldModule = module;
		module = newModule;
		boolean oldModuleESet = moduleESet;
		moduleESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE, oldModule, module, !oldModuleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetModule() {
		Module oldModule = module;
		boolean oldModuleESet = moduleESet;
		module = null;
		moduleESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE, oldModule, null, oldModuleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetModule() {
		return moduleESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT:
				if (resolve) return getSourceElement();
				return basicGetSourceElement();
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID:
				return getSourceElementId();
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ACTION:
				return getSourceAction();
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE:
				if (resolve) return getModule();
				return basicGetModule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT:
				setSourceElement((GASystemModelElement)newValue);
				return;
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID:
				setSourceElementId((Integer)newValue);
				return;
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ACTION:
				setSourceAction((String)newValue);
				return;
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE:
				setModule((Module)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT:
				unsetSourceElement();
				return;
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID:
				setSourceElementId(SOURCE_ELEMENT_ID_EDEFAULT);
				return;
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ACTION:
				setSourceAction(SOURCE_ACTION_EDEFAULT);
				return;
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE:
				unsetModule();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT:
				return isSetSourceElement();
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID:
				return sourceElementId != SOURCE_ELEMENT_ID_EDEFAULT;
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__SOURCE_ACTION:
				return SOURCE_ACTION_EDEFAULT == null ? sourceAction != null : !SOURCE_ACTION_EDEFAULT.equals(sourceAction);
			case GacodemodelPackage.GA_CODE_MODEL_ELEMENT__MODULE:
				return isSetModule();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sourceElementId: ");
		result.append(sourceElementId);
		result.append(", sourceAction: ");
		result.append(sourceAction);
		result.append(')');
		return result.toString();
	}

} //GACodeModelElementImpl
