/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.Dependency;
import geneauto.emf.models.gacodemodel.Function_CM;
import geneauto.emf.models.gacodemodel.GACodeModelRoot;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.Module;
import geneauto.emf.models.gacodemodel.NameSpace;

import geneauto.emf.models.genericmodel.Annotation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Module</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl#getHeaderFileName <em>Header File Name</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl#getSourceFileName <em>Source File Name</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl#getInitFunction <em>Init Function</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ModuleImpl#getHeaderAnnotation <em>Header Annotation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModuleImpl extends GACodeModelElementImpl implements Module {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getNameSpace() <em>Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpace()
	 * @generated
	 * @ordered
	 */
	protected NameSpace nameSpace;

	/**
	 * This is true if the Name Space containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameSpaceESet;

	/**
	 * The default value of the '{@link #getHeaderFileName() <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String HEADER_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeaderFileName() <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFileName()
	 * @generated
	 * @ordered
	 */
	protected String headerFileName = HEADER_FILE_NAME_EDEFAULT;

	/**
	 * This is true if the Header File Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean headerFileNameESet;

	/**
	 * The default value of the '{@link #getSourceFileName() <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceFileName() <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFileName()
	 * @generated
	 * @ordered
	 */
	protected String sourceFileName = SOURCE_FILE_NAME_EDEFAULT;

	/**
	 * This is true if the Source File Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sourceFileNameESet;

	/**
	 * The cached value of the '{@link #getInitFunction() <em>Init Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitFunction()
	 * @generated
	 * @ordered
	 */
	protected Function_CM initFunction;

	/**
	 * This is true if the Init Function reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initFunctionESet;

	/**
	 * The cached value of the '{@link #getDependencies() <em>Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependency> dependencies;

	/**
	 * The cached value of the '{@link #getHeaderAnnotation() <em>Header Annotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderAnnotation()
	 * @generated
	 * @ordered
	 */
	protected Annotation headerAnnotation;

	/**
	 * This is true if the Header Annotation containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean headerAnnotationESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.MODULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace getNameSpace() {
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameSpace(NameSpace newNameSpace, NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = newNameSpace;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.MODULE__NAME_SPACE, oldNameSpace, newNameSpace, !oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameSpace(NameSpace newNameSpace) {
		if (newNameSpace != nameSpace) {
			NotificationChain msgs = null;
			if (nameSpace != null)
				msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.MODULE__NAME_SPACE, null, msgs);
			if (newNameSpace != null)
				msgs = ((InternalEObject)newNameSpace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.MODULE__NAME_SPACE, null, msgs);
			msgs = basicSetNameSpace(newNameSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.MODULE__NAME_SPACE, newNameSpace, newNameSpace, !oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetNameSpace(NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = null;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.MODULE__NAME_SPACE, oldNameSpace, null, oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNameSpace() {
		if (nameSpace != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.MODULE__NAME_SPACE, null, msgs);
			msgs = basicUnsetNameSpace(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.MODULE__NAME_SPACE, null, null, oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNameSpace() {
		return nameSpaceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHeaderFileName() {
		return headerFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeaderFileName(String newHeaderFileName) {
		String oldHeaderFileName = headerFileName;
		headerFileName = newHeaderFileName;
		boolean oldHeaderFileNameESet = headerFileNameESet;
		headerFileNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.MODULE__HEADER_FILE_NAME, oldHeaderFileName, headerFileName, !oldHeaderFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHeaderFileName() {
		String oldHeaderFileName = headerFileName;
		boolean oldHeaderFileNameESet = headerFileNameESet;
		headerFileName = HEADER_FILE_NAME_EDEFAULT;
		headerFileNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.MODULE__HEADER_FILE_NAME, oldHeaderFileName, HEADER_FILE_NAME_EDEFAULT, oldHeaderFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHeaderFileName() {
		return headerFileNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceFileName() {
		return sourceFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceFileName(String newSourceFileName) {
		String oldSourceFileName = sourceFileName;
		sourceFileName = newSourceFileName;
		boolean oldSourceFileNameESet = sourceFileNameESet;
		sourceFileNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.MODULE__SOURCE_FILE_NAME, oldSourceFileName, sourceFileName, !oldSourceFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSourceFileName() {
		String oldSourceFileName = sourceFileName;
		boolean oldSourceFileNameESet = sourceFileNameESet;
		sourceFileName = SOURCE_FILE_NAME_EDEFAULT;
		sourceFileNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.MODULE__SOURCE_FILE_NAME, oldSourceFileName, SOURCE_FILE_NAME_EDEFAULT, oldSourceFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSourceFileName() {
		return sourceFileNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function_CM getInitFunction() {
		if (initFunction != null && initFunction.eIsProxy()) {
			InternalEObject oldInitFunction = (InternalEObject)initFunction;
			initFunction = (Function_CM)eResolveProxy(oldInitFunction);
			if (initFunction != oldInitFunction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GacodemodelPackage.MODULE__INIT_FUNCTION, oldInitFunction, initFunction));
			}
		}
		return initFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function_CM basicGetInitFunction() {
		return initFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitFunction(Function_CM newInitFunction) {
		Function_CM oldInitFunction = initFunction;
		initFunction = newInitFunction;
		boolean oldInitFunctionESet = initFunctionESet;
		initFunctionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.MODULE__INIT_FUNCTION, oldInitFunction, initFunction, !oldInitFunctionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitFunction() {
		Function_CM oldInitFunction = initFunction;
		boolean oldInitFunctionESet = initFunctionESet;
		initFunction = null;
		initFunctionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.MODULE__INIT_FUNCTION, oldInitFunction, null, oldInitFunctionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitFunction() {
		return initFunctionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependency> getDependencies() {
		if (dependencies == null) {
			dependencies = new EObjectContainmentEList.Unsettable<Dependency>(Dependency.class, this, GacodemodelPackage.MODULE__DEPENDENCIES);
		}
		return dependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDependencies() {
		if (dependencies != null) ((InternalEList.Unsettable<?>)dependencies).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDependencies() {
		return dependencies != null && ((InternalEList.Unsettable<?>)dependencies).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation getHeaderAnnotation() {
		return headerAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHeaderAnnotation(Annotation newHeaderAnnotation, NotificationChain msgs) {
		Annotation oldHeaderAnnotation = headerAnnotation;
		headerAnnotation = newHeaderAnnotation;
		boolean oldHeaderAnnotationESet = headerAnnotationESet;
		headerAnnotationESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.MODULE__HEADER_ANNOTATION, oldHeaderAnnotation, newHeaderAnnotation, !oldHeaderAnnotationESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeaderAnnotation(Annotation newHeaderAnnotation) {
		if (newHeaderAnnotation != headerAnnotation) {
			NotificationChain msgs = null;
			if (headerAnnotation != null)
				msgs = ((InternalEObject)headerAnnotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.MODULE__HEADER_ANNOTATION, null, msgs);
			if (newHeaderAnnotation != null)
				msgs = ((InternalEObject)newHeaderAnnotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.MODULE__HEADER_ANNOTATION, null, msgs);
			msgs = basicSetHeaderAnnotation(newHeaderAnnotation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldHeaderAnnotationESet = headerAnnotationESet;
			headerAnnotationESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.MODULE__HEADER_ANNOTATION, newHeaderAnnotation, newHeaderAnnotation, !oldHeaderAnnotationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetHeaderAnnotation(NotificationChain msgs) {
		Annotation oldHeaderAnnotation = headerAnnotation;
		headerAnnotation = null;
		boolean oldHeaderAnnotationESet = headerAnnotationESet;
		headerAnnotationESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.MODULE__HEADER_ANNOTATION, oldHeaderAnnotation, null, oldHeaderAnnotationESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHeaderAnnotation() {
		if (headerAnnotation != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)headerAnnotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.MODULE__HEADER_ANNOTATION, null, msgs);
			msgs = basicUnsetHeaderAnnotation(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldHeaderAnnotationESet = headerAnnotationESet;
			headerAnnotationESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.MODULE__HEADER_ANNOTATION, null, null, oldHeaderAnnotationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHeaderAnnotation() {
		return headerAnnotationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GacodemodelPackage.MODULE__NAME_SPACE:
				return basicUnsetNameSpace(msgs);
			case GacodemodelPackage.MODULE__DEPENDENCIES:
				return ((InternalEList<?>)getDependencies()).basicRemove(otherEnd, msgs);
			case GacodemodelPackage.MODULE__HEADER_ANNOTATION:
				return basicUnsetHeaderAnnotation(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.MODULE__NAME_SPACE:
				return getNameSpace();
			case GacodemodelPackage.MODULE__HEADER_FILE_NAME:
				return getHeaderFileName();
			case GacodemodelPackage.MODULE__SOURCE_FILE_NAME:
				return getSourceFileName();
			case GacodemodelPackage.MODULE__INIT_FUNCTION:
				if (resolve) return getInitFunction();
				return basicGetInitFunction();
			case GacodemodelPackage.MODULE__DEPENDENCIES:
				return getDependencies();
			case GacodemodelPackage.MODULE__HEADER_ANNOTATION:
				return getHeaderAnnotation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.MODULE__NAME_SPACE:
				setNameSpace((NameSpace)newValue);
				return;
			case GacodemodelPackage.MODULE__HEADER_FILE_NAME:
				setHeaderFileName((String)newValue);
				return;
			case GacodemodelPackage.MODULE__SOURCE_FILE_NAME:
				setSourceFileName((String)newValue);
				return;
			case GacodemodelPackage.MODULE__INIT_FUNCTION:
				setInitFunction((Function_CM)newValue);
				return;
			case GacodemodelPackage.MODULE__DEPENDENCIES:
				getDependencies().clear();
				getDependencies().addAll((Collection<? extends Dependency>)newValue);
				return;
			case GacodemodelPackage.MODULE__HEADER_ANNOTATION:
				setHeaderAnnotation((Annotation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.MODULE__NAME_SPACE:
				unsetNameSpace();
				return;
			case GacodemodelPackage.MODULE__HEADER_FILE_NAME:
				unsetHeaderFileName();
				return;
			case GacodemodelPackage.MODULE__SOURCE_FILE_NAME:
				unsetSourceFileName();
				return;
			case GacodemodelPackage.MODULE__INIT_FUNCTION:
				unsetInitFunction();
				return;
			case GacodemodelPackage.MODULE__DEPENDENCIES:
				unsetDependencies();
				return;
			case GacodemodelPackage.MODULE__HEADER_ANNOTATION:
				unsetHeaderAnnotation();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.MODULE__NAME_SPACE:
				return isSetNameSpace();
			case GacodemodelPackage.MODULE__HEADER_FILE_NAME:
				return isSetHeaderFileName();
			case GacodemodelPackage.MODULE__SOURCE_FILE_NAME:
				return isSetSourceFileName();
			case GacodemodelPackage.MODULE__INIT_FUNCTION:
				return isSetInitFunction();
			case GacodemodelPackage.MODULE__DEPENDENCIES:
				return isSetDependencies();
			case GacodemodelPackage.MODULE__HEADER_ANNOTATION:
				return isSetHeaderAnnotation();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == GACodeModelRoot.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == HasNameSpace.class) {
			switch (derivedFeatureID) {
				case GacodemodelPackage.MODULE__NAME_SPACE: return GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == GACodeModelRoot.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == HasNameSpace.class) {
			switch (baseFeatureID) {
				case GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE: return GacodemodelPackage.MODULE__NAME_SPACE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (headerFileName: ");
		if (headerFileNameESet) result.append(headerFileName); else result.append("<unset>");
		result.append(", sourceFileName: ");
		if (sourceFileNameESet) result.append(sourceFileName); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ModuleImpl
