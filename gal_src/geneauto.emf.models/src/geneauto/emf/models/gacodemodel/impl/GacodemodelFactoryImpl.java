/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GacodemodelFactoryImpl extends EFactoryImpl implements GacodemodelFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GacodemodelFactory init() {
		try {
			GacodemodelFactory theGacodemodelFactory = (GacodemodelFactory)EPackage.Registry.INSTANCE.getEFactory("http:///geneauto/emf/models/gacodemodel.ecore"); 
			if (theGacodemodelFactory != null) {
				return theGacodemodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GacodemodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GacodemodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case GacodemodelPackage.NAME_SPACE: return createNameSpace();
			case GacodemodelPackage.VARIABLE_CM: return createVariable_CM();
			case GacodemodelPackage.LABEL: return createLabel();
			case GacodemodelPackage.CUSTOM_TYPE_CM: return createCustomType_CM();
			case GacodemodelPackage.GA_CODE_MODEL: return createGACodeModel();
			case GacodemodelPackage.TEMP_MODEL: return createTempModel();
			case GacodemodelPackage.STRUCTURE_MEMBER: return createStructureMember();
			case GacodemodelPackage.ARRAY_CONTENT: return createArrayContent();
			case GacodemodelPackage.DERIVED_TYPE: return createDerivedType();
			case GacodemodelPackage.EXPRESSION_FUNCTION_BODY: return createExpressionFunctionBody();
			case GacodemodelPackage.FUNCTION_ARGUMENT_CM: return createFunctionArgument_CM();
			case GacodemodelPackage.STRUCTURE_VARIABLE: return createStructureVariable();
			case GacodemodelPackage.CODE_MODEL_DEPENDENCY: return createCodeModelDependency();
			case GacodemodelPackage.EXTERNAL_DEPENDENCY: return createExternalDependency();
			case GacodemodelPackage.NAMED_CONSTANT: return createNamedConstant();
			case GacodemodelPackage.FUNCTION_CM: return createFunction_CM();
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY: return createSequentialFunctionBody();
			case GacodemodelPackage.LITERAL_CONTENT: return createLiteralContent();
			case GacodemodelPackage.STRUCTURE_CONTENT: return createStructureContent();
			case GacodemodelPackage.MODULE: return createModule();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace createNameSpace() {
		NameSpaceImpl nameSpace = new NameSpaceImpl();
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable_CM createVariable_CM() {
		Variable_CMImpl variable_CM = new Variable_CMImpl();
		return variable_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Label createLabel() {
		LabelImpl label = new LabelImpl();
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomType_CM createCustomType_CM() {
		CustomType_CMImpl customType_CM = new CustomType_CMImpl();
		return customType_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GACodeModel createGACodeModel() {
		GACodeModelImpl gaCodeModel = new GACodeModelImpl();
		return gaCodeModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TempModel createTempModel() {
		TempModelImpl tempModel = new TempModelImpl();
		return tempModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructureMember createStructureMember() {
		StructureMemberImpl structureMember = new StructureMemberImpl();
		return structureMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayContent createArrayContent() {
		ArrayContentImpl arrayContent = new ArrayContentImpl();
		return arrayContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DerivedType createDerivedType() {
		DerivedTypeImpl derivedType = new DerivedTypeImpl();
		return derivedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionFunctionBody createExpressionFunctionBody() {
		ExpressionFunctionBodyImpl expressionFunctionBody = new ExpressionFunctionBodyImpl();
		return expressionFunctionBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionArgument_CM createFunctionArgument_CM() {
		FunctionArgument_CMImpl functionArgument_CM = new FunctionArgument_CMImpl();
		return functionArgument_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructureVariable createStructureVariable() {
		StructureVariableImpl structureVariable = new StructureVariableImpl();
		return structureVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeModelDependency createCodeModelDependency() {
		CodeModelDependencyImpl codeModelDependency = new CodeModelDependencyImpl();
		return codeModelDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalDependency createExternalDependency() {
		ExternalDependencyImpl externalDependency = new ExternalDependencyImpl();
		return externalDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedConstant createNamedConstant() {
		NamedConstantImpl namedConstant = new NamedConstantImpl();
		return namedConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function_CM createFunction_CM() {
		Function_CMImpl function_CM = new Function_CMImpl();
		return function_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequentialFunctionBody createSequentialFunctionBody() {
		SequentialFunctionBodyImpl sequentialFunctionBody = new SequentialFunctionBodyImpl();
		return sequentialFunctionBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiteralContent createLiteralContent() {
		LiteralContentImpl literalContent = new LiteralContentImpl();
		return literalContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructureContent createStructureContent() {
		StructureContentImpl structureContent = new StructureContentImpl();
		return structureContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Module createModule() {
		ModuleImpl module = new ModuleImpl();
		return module;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GacodemodelPackage getGacodemodelPackage() {
		return (GacodemodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GacodemodelPackage getPackage() {
		return GacodemodelPackage.eINSTANCE;
	}

} //GacodemodelFactoryImpl
