/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.CustomType_CM;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.NameSpace;
import geneauto.emf.models.gacodemodel.TempModel;

import geneauto.emf.models.genericmodel.GAModelElement;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Temp Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.TempModelImpl#getStateVariablesStructType <em>State Variables Struct Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.TempModelImpl#getTempNameSpace <em>Temp Name Space</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.TempModelImpl#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TempModelImpl extends GACodeModelElementImpl implements TempModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getStateVariablesStructType() <em>State Variables Struct Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateVariablesStructType()
	 * @generated
	 * @ordered
	 */
	protected CustomType_CM stateVariablesStructType;

	/**
	 * This is true if the State Variables Struct Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean stateVariablesStructTypeESet;

	/**
	 * The cached value of the '{@link #getTempNameSpace() <em>Temp Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTempNameSpace()
	 * @generated
	 * @ordered
	 */
	protected NameSpace tempNameSpace;

	/**
	 * This is true if the Temp Name Space containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tempNameSpaceESet;

	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<GAModelElement> elements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TempModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.TEMP_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomType_CM getStateVariablesStructType() {
		if (stateVariablesStructType != null && stateVariablesStructType.eIsProxy()) {
			InternalEObject oldStateVariablesStructType = (InternalEObject)stateVariablesStructType;
			stateVariablesStructType = (CustomType_CM)eResolveProxy(oldStateVariablesStructType);
			if (stateVariablesStructType != oldStateVariablesStructType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GacodemodelPackage.TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE, oldStateVariablesStructType, stateVariablesStructType));
			}
		}
		return stateVariablesStructType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomType_CM basicGetStateVariablesStructType() {
		return stateVariablesStructType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateVariablesStructType(CustomType_CM newStateVariablesStructType) {
		CustomType_CM oldStateVariablesStructType = stateVariablesStructType;
		stateVariablesStructType = newStateVariablesStructType;
		boolean oldStateVariablesStructTypeESet = stateVariablesStructTypeESet;
		stateVariablesStructTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE, oldStateVariablesStructType, stateVariablesStructType, !oldStateVariablesStructTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStateVariablesStructType() {
		CustomType_CM oldStateVariablesStructType = stateVariablesStructType;
		boolean oldStateVariablesStructTypeESet = stateVariablesStructTypeESet;
		stateVariablesStructType = null;
		stateVariablesStructTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE, oldStateVariablesStructType, null, oldStateVariablesStructTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStateVariablesStructType() {
		return stateVariablesStructTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace getTempNameSpace() {
		return tempNameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTempNameSpace(NameSpace newTempNameSpace, NotificationChain msgs) {
		NameSpace oldTempNameSpace = tempNameSpace;
		tempNameSpace = newTempNameSpace;
		boolean oldTempNameSpaceESet = tempNameSpaceESet;
		tempNameSpaceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE, oldTempNameSpace, newTempNameSpace, !oldTempNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTempNameSpace(NameSpace newTempNameSpace) {
		if (newTempNameSpace != tempNameSpace) {
			NotificationChain msgs = null;
			if (tempNameSpace != null)
				msgs = ((InternalEObject)tempNameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE, null, msgs);
			if (newTempNameSpace != null)
				msgs = ((InternalEObject)newTempNameSpace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE, null, msgs);
			msgs = basicSetTempNameSpace(newTempNameSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldTempNameSpaceESet = tempNameSpaceESet;
			tempNameSpaceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE, newTempNameSpace, newTempNameSpace, !oldTempNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetTempNameSpace(NotificationChain msgs) {
		NameSpace oldTempNameSpace = tempNameSpace;
		tempNameSpace = null;
		boolean oldTempNameSpaceESet = tempNameSpaceESet;
		tempNameSpaceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE, oldTempNameSpace, null, oldTempNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTempNameSpace() {
		if (tempNameSpace != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)tempNameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE, null, msgs);
			msgs = basicUnsetTempNameSpace(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldTempNameSpaceESet = tempNameSpaceESet;
			tempNameSpaceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE, null, null, oldTempNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTempNameSpace() {
		return tempNameSpaceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GAModelElement> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList.Unsettable<GAModelElement>(GAModelElement.class, this, GacodemodelPackage.TEMP_MODEL__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElements() {
		if (elements != null) ((InternalEList.Unsettable<?>)elements).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElements() {
		return elements != null && ((InternalEList.Unsettable<?>)elements).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE:
				return basicUnsetTempNameSpace(msgs);
			case GacodemodelPackage.TEMP_MODEL__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE:
				if (resolve) return getStateVariablesStructType();
				return basicGetStateVariablesStructType();
			case GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE:
				return getTempNameSpace();
			case GacodemodelPackage.TEMP_MODEL__ELEMENTS:
				return getElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE:
				setStateVariablesStructType((CustomType_CM)newValue);
				return;
			case GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE:
				setTempNameSpace((NameSpace)newValue);
				return;
			case GacodemodelPackage.TEMP_MODEL__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends GAModelElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE:
				unsetStateVariablesStructType();
				return;
			case GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE:
				unsetTempNameSpace();
				return;
			case GacodemodelPackage.TEMP_MODEL__ELEMENTS:
				unsetElements();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE:
				return isSetStateVariablesStructType();
			case GacodemodelPackage.TEMP_MODEL__TEMP_NAME_SPACE:
				return isSetTempNameSpace();
			case GacodemodelPackage.TEMP_MODEL__ELEMENTS:
				return isSetElements();
		}
		return super.eIsSet(featureID);
	}

} //TempModelImpl
