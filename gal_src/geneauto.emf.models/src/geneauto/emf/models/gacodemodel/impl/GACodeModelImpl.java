/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.GACodeModel;
import geneauto.emf.models.gacodemodel.GACodeModelRoot;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.TempModel;

import geneauto.emf.models.gasystemmodel.GASystemModel;

import geneauto.emf.models.genericmodel.impl.ModelImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GA Code Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.GACodeModelImpl#getSystemModel <em>System Model</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.GACodeModelImpl#getTempModel <em>Temp Model</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.GACodeModelImpl#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GACodeModelImpl extends ModelImpl implements GACodeModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getSystemModel() <em>System Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemModel()
	 * @generated
	 * @ordered
	 */
	protected GASystemModel systemModel;

	/**
	 * This is true if the System Model reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean systemModelESet;

	/**
	 * The cached value of the '{@link #getTempModel() <em>Temp Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTempModel()
	 * @generated
	 * @ordered
	 */
	protected TempModel tempModel;

	/**
	 * This is true if the Temp Model reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tempModelESet;

	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<GACodeModelRoot> elements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GACodeModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.GA_CODE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GASystemModel getSystemModel() {
		if (systemModel != null && systemModel.eIsProxy()) {
			InternalEObject oldSystemModel = (InternalEObject)systemModel;
			systemModel = (GASystemModel)eResolveProxy(oldSystemModel);
			if (systemModel != oldSystemModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GacodemodelPackage.GA_CODE_MODEL__SYSTEM_MODEL, oldSystemModel, systemModel));
			}
		}
		return systemModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GASystemModel basicGetSystemModel() {
		return systemModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemModel(GASystemModel newSystemModel) {
		GASystemModel oldSystemModel = systemModel;
		systemModel = newSystemModel;
		boolean oldSystemModelESet = systemModelESet;
		systemModelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.GA_CODE_MODEL__SYSTEM_MODEL, oldSystemModel, systemModel, !oldSystemModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSystemModel() {
		GASystemModel oldSystemModel = systemModel;
		boolean oldSystemModelESet = systemModelESet;
		systemModel = null;
		systemModelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.GA_CODE_MODEL__SYSTEM_MODEL, oldSystemModel, null, oldSystemModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSystemModel() {
		return systemModelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TempModel getTempModel() {
		if (tempModel != null && tempModel.eIsProxy()) {
			InternalEObject oldTempModel = (InternalEObject)tempModel;
			tempModel = (TempModel)eResolveProxy(oldTempModel);
			if (tempModel != oldTempModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GacodemodelPackage.GA_CODE_MODEL__TEMP_MODEL, oldTempModel, tempModel));
			}
		}
		return tempModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TempModel basicGetTempModel() {
		return tempModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTempModel(TempModel newTempModel) {
		TempModel oldTempModel = tempModel;
		tempModel = newTempModel;
		boolean oldTempModelESet = tempModelESet;
		tempModelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.GA_CODE_MODEL__TEMP_MODEL, oldTempModel, tempModel, !oldTempModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTempModel() {
		TempModel oldTempModel = tempModel;
		boolean oldTempModelESet = tempModelESet;
		tempModel = null;
		tempModelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.GA_CODE_MODEL__TEMP_MODEL, oldTempModel, null, oldTempModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTempModel() {
		return tempModelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GACodeModelRoot> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList.Unsettable<GACodeModelRoot>(GACodeModelRoot.class, this, GacodemodelPackage.GA_CODE_MODEL__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElements() {
		if (elements != null) ((InternalEList.Unsettable<?>)elements).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElements() {
		return elements != null && ((InternalEList.Unsettable<?>)elements).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL__SYSTEM_MODEL:
				if (resolve) return getSystemModel();
				return basicGetSystemModel();
			case GacodemodelPackage.GA_CODE_MODEL__TEMP_MODEL:
				if (resolve) return getTempModel();
				return basicGetTempModel();
			case GacodemodelPackage.GA_CODE_MODEL__ELEMENTS:
				return getElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL__SYSTEM_MODEL:
				setSystemModel((GASystemModel)newValue);
				return;
			case GacodemodelPackage.GA_CODE_MODEL__TEMP_MODEL:
				setTempModel((TempModel)newValue);
				return;
			case GacodemodelPackage.GA_CODE_MODEL__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends GACodeModelRoot>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL__SYSTEM_MODEL:
				unsetSystemModel();
				return;
			case GacodemodelPackage.GA_CODE_MODEL__TEMP_MODEL:
				unsetTempModel();
				return;
			case GacodemodelPackage.GA_CODE_MODEL__ELEMENTS:
				unsetElements();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.GA_CODE_MODEL__SYSTEM_MODEL:
				return isSetSystemModel();
			case GacodemodelPackage.GA_CODE_MODEL__TEMP_MODEL:
				return isSetTempModel();
			case GacodemodelPackage.GA_CODE_MODEL__ELEMENTS:
				return isSetElements();
		}
		return super.eIsSet(featureID);
	}

} //GACodeModelImpl
