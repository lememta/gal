/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.NameSpace;
import geneauto.emf.models.gacodemodel.NameSpaceElement_CM;

import geneauto.emf.models.gacodemodel.expression.Expression;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Name Space</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.NameSpaceImpl#getNsElements <em>Ns Elements</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.NameSpaceImpl#getImplicitArgumentValues <em>Implicit Argument Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NameSpaceImpl extends GACodeModelElementImpl implements NameSpace {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getNsElements() <em>Ns Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNsElements()
	 * @generated
	 * @ordered
	 */
	protected EList<NameSpaceElement_CM> nsElements;

	/**
	 * The cached value of the '{@link #getImplicitArgumentValues() <em>Implicit Argument Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplicitArgumentValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> implicitArgumentValues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NameSpaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.NAME_SPACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NameSpaceElement_CM> getNsElements() {
		if (nsElements == null) {
			nsElements = new EObjectContainmentEList.Unsettable<NameSpaceElement_CM>(NameSpaceElement_CM.class, this, GacodemodelPackage.NAME_SPACE__NS_ELEMENTS);
		}
		return nsElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNsElements() {
		if (nsElements != null) ((InternalEList.Unsettable<?>)nsElements).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNsElements() {
		return nsElements != null && ((InternalEList.Unsettable<?>)nsElements).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getImplicitArgumentValues() {
		if (implicitArgumentValues == null) {
			implicitArgumentValues = new EObjectContainmentEList.Unsettable<Expression>(Expression.class, this, GacodemodelPackage.NAME_SPACE__IMPLICIT_ARGUMENT_VALUES);
		}
		return implicitArgumentValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetImplicitArgumentValues() {
		if (implicitArgumentValues != null) ((InternalEList.Unsettable<?>)implicitArgumentValues).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetImplicitArgumentValues() {
		return implicitArgumentValues != null && ((InternalEList.Unsettable<?>)implicitArgumentValues).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GacodemodelPackage.NAME_SPACE__NS_ELEMENTS:
				return ((InternalEList<?>)getNsElements()).basicRemove(otherEnd, msgs);
			case GacodemodelPackage.NAME_SPACE__IMPLICIT_ARGUMENT_VALUES:
				return ((InternalEList<?>)getImplicitArgumentValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.NAME_SPACE__NS_ELEMENTS:
				return getNsElements();
			case GacodemodelPackage.NAME_SPACE__IMPLICIT_ARGUMENT_VALUES:
				return getImplicitArgumentValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.NAME_SPACE__NS_ELEMENTS:
				getNsElements().clear();
				getNsElements().addAll((Collection<? extends NameSpaceElement_CM>)newValue);
				return;
			case GacodemodelPackage.NAME_SPACE__IMPLICIT_ARGUMENT_VALUES:
				getImplicitArgumentValues().clear();
				getImplicitArgumentValues().addAll((Collection<? extends Expression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.NAME_SPACE__NS_ELEMENTS:
				unsetNsElements();
				return;
			case GacodemodelPackage.NAME_SPACE__IMPLICIT_ARGUMENT_VALUES:
				unsetImplicitArgumentValues();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.NAME_SPACE__NS_ELEMENTS:
				return isSetNsElements();
			case GacodemodelPackage.NAME_SPACE__IMPLICIT_ARGUMENT_VALUES:
				return isSetImplicitArgumentValues();
		}
		return super.eIsSet(featureID);
	}

} //NameSpaceImpl
