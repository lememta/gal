/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.common.CommonPackage;
import geneauto.emf.models.common.NameSpaceElement;
import geneauto.emf.models.common.Variable;

import geneauto.emf.models.gacodemodel.CodeModelVariable;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.Variable_CM;

import geneauto.emf.models.gacodemodel.expression.Expression;

import geneauto.emf.models.gadatatypes.GADataType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable CM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#getInitialValue <em>Initial Value</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#isConst <em>Is Const</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#isOptimizable <em>Is Optimizable</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#isStatic <em>Is Static</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#isVolatile <em>Is Volatile</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#isAutoInit <em>Auto Init</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Variable_CMImpl#getObservationPoints <em>Observation Points</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Variable_CMImpl extends NameSpaceElement_CMImpl implements Variable_CM {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected GADataType dataType;

	/**
	 * This is true if the Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dataTypeESet;

	/**
	 * The cached value of the '{@link #getInitialValue() <em>Initial Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialValue()
	 * @generated
	 * @ordered
	 */
	protected Expression initialValue;

	/**
	 * This is true if the Initial Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initialValueESet;

	/**
	 * The default value of the '{@link #isConst() <em>Is Const</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConst()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CONST_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConst() <em>Is Const</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConst()
	 * @generated
	 * @ordered
	 */
	protected boolean isConst = IS_CONST_EDEFAULT;

	/**
	 * This is true if the Is Const attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isConstESet;

	/**
	 * The default value of the '{@link #isOptimizable() <em>Is Optimizable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptimizable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_OPTIMIZABLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isOptimizable() <em>Is Optimizable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptimizable()
	 * @generated
	 * @ordered
	 */
	protected boolean isOptimizable = IS_OPTIMIZABLE_EDEFAULT;

	/**
	 * This is true if the Is Optimizable attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isOptimizableESet;

	/**
	 * The default value of the '{@link #isStatic() <em>Is Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_STATIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStatic() <em>Is Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected boolean isStatic = IS_STATIC_EDEFAULT;

	/**
	 * This is true if the Is Static attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isStaticESet;

	/**
	 * The default value of the '{@link #isVolatile() <em>Is Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_VOLATILE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVolatile() <em>Is Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected boolean isVolatile = IS_VOLATILE_EDEFAULT;

	/**
	 * This is true if the Is Volatile attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isVolatileESet;

	/**
	 * The default value of the '{@link #isAutoInit() <em>Auto Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoInit()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AUTO_INIT_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isAutoInit() <em>Auto Init</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoInit()
	 * @generated
	 * @ordered
	 */
	protected boolean autoInit = AUTO_INIT_EDEFAULT;

	/**
	 * This is true if the Auto Init attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean autoInitESet;

	/**
	 * The cached value of the '{@link #getObservationPoints() <em>Observation Points</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservationPoints()
	 * @generated
	 * @ordered
	 */
	protected EList<String> observationPoints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Variable_CMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.VARIABLE_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataType(GADataType newDataType, NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = newDataType;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__DATA_TYPE, oldDataType, newDataType, !oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(GADataType newDataType) {
		if (newDataType != dataType) {
			NotificationChain msgs = null;
			if (dataType != null)
				msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.VARIABLE_CM__DATA_TYPE, null, msgs);
			if (newDataType != null)
				msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.VARIABLE_CM__DATA_TYPE, null, msgs);
			msgs = basicSetDataType(newDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__DATA_TYPE, newDataType, newDataType, !oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDataType(NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = null;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__DATA_TYPE, oldDataType, null, oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDataType() {
		if (dataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.VARIABLE_CM__DATA_TYPE, null, msgs);
			msgs = basicUnsetDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__DATA_TYPE, null, null, oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDataType() {
		return dataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInitialValue() {
		return initialValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitialValue(Expression newInitialValue, NotificationChain msgs) {
		Expression oldInitialValue = initialValue;
		initialValue = newInitialValue;
		boolean oldInitialValueESet = initialValueESet;
		initialValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE, oldInitialValue, newInitialValue, !oldInitialValueESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialValue(Expression newInitialValue) {
		if (newInitialValue != initialValue) {
			NotificationChain msgs = null;
			if (initialValue != null)
				msgs = ((InternalEObject)initialValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE, null, msgs);
			if (newInitialValue != null)
				msgs = ((InternalEObject)newInitialValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE, null, msgs);
			msgs = basicSetInitialValue(newInitialValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInitialValueESet = initialValueESet;
			initialValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE, newInitialValue, newInitialValue, !oldInitialValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInitialValue(NotificationChain msgs) {
		Expression oldInitialValue = initialValue;
		initialValue = null;
		boolean oldInitialValueESet = initialValueESet;
		initialValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE, oldInitialValue, null, oldInitialValueESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitialValue() {
		if (initialValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)initialValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE, null, msgs);
			msgs = basicUnsetInitialValue(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldInitialValueESet = initialValueESet;
			initialValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE, null, null, oldInitialValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitialValue() {
		return initialValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConst() {
		return isConst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConst(boolean newIsConst) {
		boolean oldIsConst = isConst;
		isConst = newIsConst;
		boolean oldIsConstESet = isConstESet;
		isConstESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__IS_CONST, oldIsConst, isConst, !oldIsConstESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsConst() {
		boolean oldIsConst = isConst;
		boolean oldIsConstESet = isConstESet;
		isConst = IS_CONST_EDEFAULT;
		isConstESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__IS_CONST, oldIsConst, IS_CONST_EDEFAULT, oldIsConstESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsConst() {
		return isConstESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptimizable() {
		return isOptimizable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsOptimizable(boolean newIsOptimizable) {
		boolean oldIsOptimizable = isOptimizable;
		isOptimizable = newIsOptimizable;
		boolean oldIsOptimizableESet = isOptimizableESet;
		isOptimizableESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE, oldIsOptimizable, isOptimizable, !oldIsOptimizableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsOptimizable() {
		boolean oldIsOptimizable = isOptimizable;
		boolean oldIsOptimizableESet = isOptimizableESet;
		isOptimizable = IS_OPTIMIZABLE_EDEFAULT;
		isOptimizableESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE, oldIsOptimizable, IS_OPTIMIZABLE_EDEFAULT, oldIsOptimizableESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsOptimizable() {
		return isOptimizableESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStatic() {
		return isStatic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsStatic(boolean newIsStatic) {
		boolean oldIsStatic = isStatic;
		isStatic = newIsStatic;
		boolean oldIsStaticESet = isStaticESet;
		isStaticESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__IS_STATIC, oldIsStatic, isStatic, !oldIsStaticESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsStatic() {
		boolean oldIsStatic = isStatic;
		boolean oldIsStaticESet = isStaticESet;
		isStatic = IS_STATIC_EDEFAULT;
		isStaticESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__IS_STATIC, oldIsStatic, IS_STATIC_EDEFAULT, oldIsStaticESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsStatic() {
		return isStaticESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVolatile() {
		return isVolatile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsVolatile(boolean newIsVolatile) {
		boolean oldIsVolatile = isVolatile;
		isVolatile = newIsVolatile;
		boolean oldIsVolatileESet = isVolatileESet;
		isVolatileESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__IS_VOLATILE, oldIsVolatile, isVolatile, !oldIsVolatileESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsVolatile() {
		boolean oldIsVolatile = isVolatile;
		boolean oldIsVolatileESet = isVolatileESet;
		isVolatile = IS_VOLATILE_EDEFAULT;
		isVolatileESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__IS_VOLATILE, oldIsVolatile, IS_VOLATILE_EDEFAULT, oldIsVolatileESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsVolatile() {
		return isVolatileESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAutoInit() {
		return autoInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAutoInit(boolean newAutoInit) {
		boolean oldAutoInit = autoInit;
		autoInit = newAutoInit;
		boolean oldAutoInitESet = autoInitESet;
		autoInitESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.VARIABLE_CM__AUTO_INIT, oldAutoInit, autoInit, !oldAutoInitESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAutoInit() {
		boolean oldAutoInit = autoInit;
		boolean oldAutoInitESet = autoInitESet;
		autoInit = AUTO_INIT_EDEFAULT;
		autoInitESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.VARIABLE_CM__AUTO_INIT, oldAutoInit, AUTO_INIT_EDEFAULT, oldAutoInitESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAutoInit() {
		return autoInitESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getObservationPoints() {
		if (observationPoints == null) {
			observationPoints = new EDataTypeUniqueEList.Unsettable<String>(String.class, this, GacodemodelPackage.VARIABLE_CM__OBSERVATION_POINTS);
		}
		return observationPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObservationPoints() {
		if (observationPoints != null) ((InternalEList.Unsettable<?>)observationPoints).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObservationPoints() {
		return observationPoints != null && ((InternalEList.Unsettable<?>)observationPoints).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GacodemodelPackage.VARIABLE_CM__DATA_TYPE:
				return basicUnsetDataType(msgs);
			case GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE:
				return basicUnsetInitialValue(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.VARIABLE_CM__DATA_TYPE:
				return getDataType();
			case GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE:
				return getInitialValue();
			case GacodemodelPackage.VARIABLE_CM__IS_CONST:
				return isConst();
			case GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE:
				return isOptimizable();
			case GacodemodelPackage.VARIABLE_CM__IS_STATIC:
				return isStatic();
			case GacodemodelPackage.VARIABLE_CM__IS_VOLATILE:
				return isVolatile();
			case GacodemodelPackage.VARIABLE_CM__AUTO_INIT:
				return isAutoInit();
			case GacodemodelPackage.VARIABLE_CM__OBSERVATION_POINTS:
				return getObservationPoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.VARIABLE_CM__DATA_TYPE:
				setDataType((GADataType)newValue);
				return;
			case GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE:
				setInitialValue((Expression)newValue);
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_CONST:
				setIsConst((Boolean)newValue);
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE:
				setIsOptimizable((Boolean)newValue);
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_STATIC:
				setIsStatic((Boolean)newValue);
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_VOLATILE:
				setIsVolatile((Boolean)newValue);
				return;
			case GacodemodelPackage.VARIABLE_CM__AUTO_INIT:
				setAutoInit((Boolean)newValue);
				return;
			case GacodemodelPackage.VARIABLE_CM__OBSERVATION_POINTS:
				getObservationPoints().clear();
				getObservationPoints().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.VARIABLE_CM__DATA_TYPE:
				unsetDataType();
				return;
			case GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE:
				unsetInitialValue();
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_CONST:
				unsetIsConst();
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE:
				unsetIsOptimizable();
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_STATIC:
				unsetIsStatic();
				return;
			case GacodemodelPackage.VARIABLE_CM__IS_VOLATILE:
				unsetIsVolatile();
				return;
			case GacodemodelPackage.VARIABLE_CM__AUTO_INIT:
				unsetAutoInit();
				return;
			case GacodemodelPackage.VARIABLE_CM__OBSERVATION_POINTS:
				unsetObservationPoints();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.VARIABLE_CM__DATA_TYPE:
				return isSetDataType();
			case GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE:
				return isSetInitialValue();
			case GacodemodelPackage.VARIABLE_CM__IS_CONST:
				return isSetIsConst();
			case GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE:
				return isSetIsOptimizable();
			case GacodemodelPackage.VARIABLE_CM__IS_STATIC:
				return isSetIsStatic();
			case GacodemodelPackage.VARIABLE_CM__IS_VOLATILE:
				return isSetIsVolatile();
			case GacodemodelPackage.VARIABLE_CM__AUTO_INIT:
				return isSetAutoInit();
			case GacodemodelPackage.VARIABLE_CM__OBSERVATION_POINTS:
				return isSetObservationPoints();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NameSpaceElement.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Variable.class) {
			switch (derivedFeatureID) {
				case GacodemodelPackage.VARIABLE_CM__DATA_TYPE: return CommonPackage.VARIABLE__DATA_TYPE;
				case GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE: return CommonPackage.VARIABLE__INITIAL_VALUE;
				case GacodemodelPackage.VARIABLE_CM__IS_CONST: return CommonPackage.VARIABLE__IS_CONST;
				case GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE: return CommonPackage.VARIABLE__IS_OPTIMIZABLE;
				case GacodemodelPackage.VARIABLE_CM__IS_STATIC: return CommonPackage.VARIABLE__IS_STATIC;
				case GacodemodelPackage.VARIABLE_CM__IS_VOLATILE: return CommonPackage.VARIABLE__IS_VOLATILE;
				default: return -1;
			}
		}
		if (baseClass == CodeModelVariable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NameSpaceElement.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Variable.class) {
			switch (baseFeatureID) {
				case CommonPackage.VARIABLE__DATA_TYPE: return GacodemodelPackage.VARIABLE_CM__DATA_TYPE;
				case CommonPackage.VARIABLE__INITIAL_VALUE: return GacodemodelPackage.VARIABLE_CM__INITIAL_VALUE;
				case CommonPackage.VARIABLE__IS_CONST: return GacodemodelPackage.VARIABLE_CM__IS_CONST;
				case CommonPackage.VARIABLE__IS_OPTIMIZABLE: return GacodemodelPackage.VARIABLE_CM__IS_OPTIMIZABLE;
				case CommonPackage.VARIABLE__IS_STATIC: return GacodemodelPackage.VARIABLE_CM__IS_STATIC;
				case CommonPackage.VARIABLE__IS_VOLATILE: return GacodemodelPackage.VARIABLE_CM__IS_VOLATILE;
				default: return -1;
			}
		}
		if (baseClass == CodeModelVariable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isConst: ");
		if (isConstESet) result.append(isConst); else result.append("<unset>");
		result.append(", isOptimizable: ");
		if (isOptimizableESet) result.append(isOptimizable); else result.append("<unset>");
		result.append(", isStatic: ");
		if (isStaticESet) result.append(isStatic); else result.append("<unset>");
		result.append(", isVolatile: ");
		if (isVolatileESet) result.append(isVolatile); else result.append("<unset>");
		result.append(", autoInit: ");
		if (autoInitESet) result.append(autoInit); else result.append("<unset>");
		result.append(", observationPoints: ");
		result.append(observationPoints);
		result.append(')');
		return result.toString();
	}

} //Variable_CMImpl
