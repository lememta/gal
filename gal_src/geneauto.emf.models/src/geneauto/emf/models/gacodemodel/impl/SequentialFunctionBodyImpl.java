/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.NameSpace;
import geneauto.emf.models.gacodemodel.SequentialComposition;
import geneauto.emf.models.gacodemodel.SequentialFunctionBody;

import geneauto.emf.models.gacodemodel.statement.Statement;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sequential Function Body</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.SequentialFunctionBodyImpl#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.SequentialFunctionBodyImpl#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SequentialFunctionBodyImpl extends FunctionBodyImpl implements SequentialFunctionBody {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getNameSpace() <em>Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpace()
	 * @generated
	 * @ordered
	 */
	protected NameSpace nameSpace;

	/**
	 * This is true if the Name Space containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameSpaceESet;

	/**
	 * The cached value of the '{@link #getStatements() <em>Statements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatements()
	 * @generated
	 * @ordered
	 */
	protected EList<Statement> statements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SequentialFunctionBodyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.SEQUENTIAL_FUNCTION_BODY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace getNameSpace() {
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameSpace(NameSpace newNameSpace, NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = newNameSpace;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE, oldNameSpace, newNameSpace, !oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameSpace(NameSpace newNameSpace) {
		if (newNameSpace != nameSpace) {
			NotificationChain msgs = null;
			if (nameSpace != null)
				msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE, null, msgs);
			if (newNameSpace != null)
				msgs = ((InternalEObject)newNameSpace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE, null, msgs);
			msgs = basicSetNameSpace(newNameSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE, newNameSpace, newNameSpace, !oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetNameSpace(NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = null;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE, oldNameSpace, null, oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNameSpace() {
		if (nameSpace != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE, null, msgs);
			msgs = basicUnsetNameSpace(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE, null, null, oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNameSpace() {
		return nameSpaceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Statement> getStatements() {
		if (statements == null) {
			statements = new EObjectContainmentEList.Unsettable<Statement>(Statement.class, this, GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__STATEMENTS);
		}
		return statements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStatements() {
		if (statements != null) ((InternalEList.Unsettable<?>)statements).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStatements() {
		return statements != null && ((InternalEList.Unsettable<?>)statements).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE:
				return basicUnsetNameSpace(msgs);
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__STATEMENTS:
				return ((InternalEList<?>)getStatements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE:
				return getNameSpace();
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__STATEMENTS:
				return getStatements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE:
				setNameSpace((NameSpace)newValue);
				return;
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__STATEMENTS:
				getStatements().clear();
				getStatements().addAll((Collection<? extends Statement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE:
				unsetNameSpace();
				return;
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__STATEMENTS:
				unsetStatements();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE:
				return isSetNameSpace();
			case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__STATEMENTS:
				return isSetStatements();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == HasNameSpace.class) {
			switch (derivedFeatureID) {
				case GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE: return GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE;
				default: return -1;
			}
		}
		if (baseClass == SequentialComposition.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == HasNameSpace.class) {
			switch (baseFeatureID) {
				case GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE: return GacodemodelPackage.SEQUENTIAL_FUNCTION_BODY__NAME_SPACE;
				default: return -1;
			}
		}
		if (baseClass == SequentialComposition.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //SequentialFunctionBodyImpl
