/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.common.CommonPackage;

import geneauto.emf.models.common.impl.CommonPackageImpl;

import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;

import geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl;

import geneauto.emf.models.gacodemodel.ArrayContent;
import geneauto.emf.models.gacodemodel.CodeModelDependency;
import geneauto.emf.models.gacodemodel.CodeModelFunction;
import geneauto.emf.models.gacodemodel.CodeModelVariable;
import geneauto.emf.models.gacodemodel.CustomTypeContent;
import geneauto.emf.models.gacodemodel.CustomType_CM;
import geneauto.emf.models.gacodemodel.Dependency;
import geneauto.emf.models.gacodemodel.DerivedType;
import geneauto.emf.models.gacodemodel.ExpressionFunctionBody;
import geneauto.emf.models.gacodemodel.ExternalDependency;
import geneauto.emf.models.gacodemodel.FunctionArgument_CM;
import geneauto.emf.models.gacodemodel.FunctionBody;
import geneauto.emf.models.gacodemodel.Function_CM;
import geneauto.emf.models.gacodemodel.GACodeModel;
import geneauto.emf.models.gacodemodel.GACodeModelElement;
import geneauto.emf.models.gacodemodel.GACodeModelRoot;
import geneauto.emf.models.gacodemodel.GacodemodelFactory;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.Label;
import geneauto.emf.models.gacodemodel.LiteralContent;
import geneauto.emf.models.gacodemodel.Module;
import geneauto.emf.models.gacodemodel.NameSpace;
import geneauto.emf.models.gacodemodel.NameSpaceElement_CM;
import geneauto.emf.models.gacodemodel.NamedConstant;
import geneauto.emf.models.gacodemodel.SequentialComposition;
import geneauto.emf.models.gacodemodel.SequentialFunctionBody;
import geneauto.emf.models.gacodemodel.StructureContent;
import geneauto.emf.models.gacodemodel.StructureMember;
import geneauto.emf.models.gacodemodel.StructureVariable;
import geneauto.emf.models.gacodemodel.TempModel;
import geneauto.emf.models.gacodemodel.Variable_CM;

import geneauto.emf.models.gacodemodel.expression.ExpressionPackage;

import geneauto.emf.models.gacodemodel.expression.impl.ExpressionPackageImpl;

import geneauto.emf.models.gacodemodel.expression.statemodel.StatemodelPackage;

import geneauto.emf.models.gacodemodel.expression.statemodel.impl.StatemodelPackageImpl;

import geneauto.emf.models.gacodemodel.gaenumtypes.GaenumtypesPackage;

import geneauto.emf.models.gacodemodel.gaenumtypes.impl.GaenumtypesPackageImpl;

import geneauto.emf.models.gacodemodel.operator.OperatorPackage;

import geneauto.emf.models.gacodemodel.operator.impl.OperatorPackageImpl;

import geneauto.emf.models.gacodemodel.statement.StatementPackage;

import geneauto.emf.models.gacodemodel.statement.impl.StatementPackageImpl;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.BroadcastPackage;

import geneauto.emf.models.gacodemodel.statement.statemodel.broadcast.impl.BroadcastPackageImpl;

import geneauto.emf.models.gadatatypes.GadatatypesPackage;

import geneauto.emf.models.gadatatypes.impl.GadatatypesPackageImpl;

import geneauto.emf.models.gasystemmodel.GasystemmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.GafunctionalmodelPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.BlocksPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.blocks.impl.BlocksPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.impl.GafunctionalmodelPackageImpl;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.PortsPackage;

import geneauto.emf.models.gasystemmodel.gafunctionalmodel.ports.impl.PortsPackageImpl;

import geneauto.emf.models.gasystemmodel.gastatemodel.GastatemodelPackage;

import geneauto.emf.models.gasystemmodel.gastatemodel.impl.GastatemodelPackageImpl;

import geneauto.emf.models.gasystemmodel.impl.GasystemmodelPackageImpl;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GacodemodelPackageImpl extends EPackageImpl implements GacodemodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaCodeModelElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customTypeContentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasNameSpaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameSpaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameSpaceElement_CMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variable_CMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeModelVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequentialCompositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass labelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customType_CMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaCodeModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tempModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaCodeModelRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structureMemberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arrayContentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass derivedTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionFunctionBodyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionBodyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionArgument_CMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structureVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeModelDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass function_CMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeModelFunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequentialFunctionBodyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass literalContentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structureContentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moduleEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GacodemodelPackageImpl() {
		super(eNS_URI, GacodemodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GacodemodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GacodemodelPackage init() {
		if (isInited) return (GacodemodelPackage)EPackage.Registry.INSTANCE.getEPackage(GacodemodelPackage.eNS_URI);

		// Obtain or create and register package
		GacodemodelPackageImpl theGacodemodelPackage = (GacodemodelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GacodemodelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GacodemodelPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) : CommonPackage.eINSTANCE);
		GadatatypesPackageImpl theGadatatypesPackage = (GadatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) instanceof GadatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI) : GadatatypesPackage.eINSTANCE);
		GenericmodelPackageImpl theGenericmodelPackage = (GenericmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) instanceof GenericmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI) : GenericmodelPackage.eINSTANCE);
		ExpressionPackageImpl theExpressionPackage = (ExpressionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) instanceof ExpressionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI) : ExpressionPackage.eINSTANCE);
		StatemodelPackageImpl theStatemodelPackage = (StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) instanceof StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatemodelPackage.eNS_URI) : StatemodelPackage.eINSTANCE);
		StatementPackageImpl theStatementPackage = (StatementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) instanceof StatementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI) : StatementPackage.eINSTANCE);
		geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl theStatemodelPackage_1 = (geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) instanceof geneauto.emf.models.gacodemodel.statement.statemodel.impl.StatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eNS_URI) : geneauto.emf.models.gacodemodel.statement.statemodel.StatemodelPackage.eINSTANCE);
		BroadcastPackageImpl theBroadcastPackage = (BroadcastPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) instanceof BroadcastPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BroadcastPackage.eNS_URI) : BroadcastPackage.eINSTANCE);
		GaenumtypesPackageImpl theGaenumtypesPackage = (GaenumtypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) instanceof GaenumtypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI) : GaenumtypesPackage.eINSTANCE);
		OperatorPackageImpl theOperatorPackage = (OperatorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) instanceof OperatorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI) : OperatorPackage.eINSTANCE);
		GasystemmodelPackageImpl theGasystemmodelPackage = (GasystemmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) instanceof GasystemmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI) : GasystemmodelPackage.eINSTANCE);
		geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl theCommonPackage_1 = (geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) instanceof geneauto.emf.models.gasystemmodel.common.impl.CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(geneauto.emf.models.gasystemmodel.common.CommonPackage.eNS_URI) : geneauto.emf.models.gasystemmodel.common.CommonPackage.eINSTANCE);
		GastatemodelPackageImpl theGastatemodelPackage = (GastatemodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) instanceof GastatemodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GastatemodelPackage.eNS_URI) : GastatemodelPackage.eINSTANCE);
		GafunctionalmodelPackageImpl theGafunctionalmodelPackage = (GafunctionalmodelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) instanceof GafunctionalmodelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GafunctionalmodelPackage.eNS_URI) : GafunctionalmodelPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);
		PortsPackageImpl thePortsPackage = (PortsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) instanceof PortsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortsPackage.eNS_URI) : PortsPackage.eINSTANCE);
		GablocklibraryPackageImpl theGablocklibraryPackage = (GablocklibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) instanceof GablocklibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GablocklibraryPackage.eNS_URI) : GablocklibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theGacodemodelPackage.createPackageContents();
		theCommonPackage.createPackageContents();
		theGadatatypesPackage.createPackageContents();
		theGenericmodelPackage.createPackageContents();
		theExpressionPackage.createPackageContents();
		theStatemodelPackage.createPackageContents();
		theStatementPackage.createPackageContents();
		theStatemodelPackage_1.createPackageContents();
		theBroadcastPackage.createPackageContents();
		theGaenumtypesPackage.createPackageContents();
		theOperatorPackage.createPackageContents();
		theGasystemmodelPackage.createPackageContents();
		theCommonPackage_1.createPackageContents();
		theGastatemodelPackage.createPackageContents();
		theGafunctionalmodelPackage.createPackageContents();
		theBlocksPackage.createPackageContents();
		thePortsPackage.createPackageContents();
		theGablocklibraryPackage.createPackageContents();

		// Initialize created meta-data
		theGacodemodelPackage.initializePackageContents();
		theCommonPackage.initializePackageContents();
		theGadatatypesPackage.initializePackageContents();
		theGenericmodelPackage.initializePackageContents();
		theExpressionPackage.initializePackageContents();
		theStatemodelPackage.initializePackageContents();
		theStatementPackage.initializePackageContents();
		theStatemodelPackage_1.initializePackageContents();
		theBroadcastPackage.initializePackageContents();
		theGaenumtypesPackage.initializePackageContents();
		theOperatorPackage.initializePackageContents();
		theGasystemmodelPackage.initializePackageContents();
		theCommonPackage_1.initializePackageContents();
		theGastatemodelPackage.initializePackageContents();
		theGafunctionalmodelPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();
		thePortsPackage.initializePackageContents();
		theGablocklibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGacodemodelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GacodemodelPackage.eNS_URI, theGacodemodelPackage);
		return theGacodemodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGACodeModelElement() {
		return gaCodeModelElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGACodeModelElement_SourceElement() {
		return (EReference)gaCodeModelElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGACodeModelElement_SourceElementId() {
		return (EAttribute)gaCodeModelElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGACodeModelElement_SourceAction() {
		return (EAttribute)gaCodeModelElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGACodeModelElement_Module() {
		return (EReference)gaCodeModelElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomTypeContent() {
		return customTypeContentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHasNameSpace() {
		return hasNameSpaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHasNameSpace_NameSpace() {
		return (EReference)hasNameSpaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNameSpace() {
		return nameSpaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameSpace_NsElements() {
		return (EReference)nameSpaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameSpace_ImplicitArgumentValues() {
		return (EReference)nameSpaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNameSpaceElement_CM() {
		return nameSpaceElement_CMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNameSpaceElement_CM_Scope() {
		return (EAttribute)nameSpaceElement_CMEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariable_CM() {
		return variable_CMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariable_CM_AutoInit() {
		return (EAttribute)variable_CMEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariable_CM_ObservationPoints() {
		return (EAttribute)variable_CMEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeModelVariable() {
		return codeModelVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequentialComposition() {
		return sequentialCompositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLabel() {
		return labelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDependency() {
		return dependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomType_CM() {
		return customType_CMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCustomType_CM_Content() {
		return (EReference)customType_CMEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGACodeModel() {
		return gaCodeModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGACodeModel_SystemModel() {
		return (EReference)gaCodeModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGACodeModel_TempModel() {
		return (EReference)gaCodeModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGACodeModel_Elements() {
		return (EReference)gaCodeModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTempModel() {
		return tempModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTempModel_StateVariablesStructType() {
		return (EReference)tempModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTempModel_TempNameSpace() {
		return (EReference)tempModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTempModel_Elements() {
		return (EReference)tempModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGACodeModelRoot() {
		return gaCodeModelRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStructureMember() {
		return structureMemberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArrayContent() {
		return arrayContentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayContent_Range() {
		return (EReference)arrayContentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayContent_BaseType() {
		return (EReference)arrayContentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDerivedType() {
		return derivedTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDerivedType_ParentType() {
		return (EReference)derivedTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpressionFunctionBody() {
		return expressionFunctionBodyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExpressionFunctionBody_Expression() {
		return (EReference)expressionFunctionBodyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionBody() {
		return functionBodyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionArgument_CM() {
		return functionArgument_CMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionArgument_CM_IsImplicit() {
		return (EAttribute)functionArgument_CMEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionArgument_CM_IsRedundant() {
		return (EAttribute)functionArgument_CMEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionArgument_CM_Direction() {
		return (EAttribute)functionArgument_CMEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStructureVariable() {
		return structureVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeModelDependency() {
		return codeModelDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeModelDependency_DependentModule() {
		return (EReference)codeModelDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalDependency() {
		return externalDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalDependency_IsSystem() {
		return (EAttribute)externalDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalDependency_NonStandardExtension() {
		return (EAttribute)externalDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedConstant() {
		return namedConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunction_CM() {
		return function_CMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunction_CM_FunctionStyle() {
		return (EAttribute)function_CMEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_CM_Arguments() {
		return (EReference)function_CMEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_CM_Body() {
		return (EReference)function_CMEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeModelFunction() {
		return codeModelFunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequentialFunctionBody() {
		return sequentialFunctionBodyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequentialFunctionBody_Statements() {
		return (EReference)sequentialFunctionBodyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLiteralContent() {
		return literalContentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLiteralContent_Value() {
		return (EAttribute)literalContentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStructureContent() {
		return structureContentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStructureContent_StructMembers() {
		return (EReference)structureContentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModule() {
		return moduleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModule_HeaderFileName() {
		return (EAttribute)moduleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModule_SourceFileName() {
		return (EAttribute)moduleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModule_InitFunction() {
		return (EReference)moduleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModule_Dependencies() {
		return (EReference)moduleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModule_HeaderAnnotation() {
		return (EReference)moduleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GacodemodelFactory getGacodemodelFactory() {
		return (GacodemodelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		gaCodeModelElementEClass = createEClass(GA_CODE_MODEL_ELEMENT);
		createEReference(gaCodeModelElementEClass, GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT);
		createEAttribute(gaCodeModelElementEClass, GA_CODE_MODEL_ELEMENT__SOURCE_ELEMENT_ID);
		createEAttribute(gaCodeModelElementEClass, GA_CODE_MODEL_ELEMENT__SOURCE_ACTION);
		createEReference(gaCodeModelElementEClass, GA_CODE_MODEL_ELEMENT__MODULE);

		customTypeContentEClass = createEClass(CUSTOM_TYPE_CONTENT);

		hasNameSpaceEClass = createEClass(HAS_NAME_SPACE);
		createEReference(hasNameSpaceEClass, HAS_NAME_SPACE__NAME_SPACE);

		nameSpaceEClass = createEClass(NAME_SPACE);
		createEReference(nameSpaceEClass, NAME_SPACE__NS_ELEMENTS);
		createEReference(nameSpaceEClass, NAME_SPACE__IMPLICIT_ARGUMENT_VALUES);

		nameSpaceElement_CMEClass = createEClass(NAME_SPACE_ELEMENT_CM);
		createEAttribute(nameSpaceElement_CMEClass, NAME_SPACE_ELEMENT_CM__SCOPE);

		variable_CMEClass = createEClass(VARIABLE_CM);
		createEAttribute(variable_CMEClass, VARIABLE_CM__AUTO_INIT);
		createEAttribute(variable_CMEClass, VARIABLE_CM__OBSERVATION_POINTS);

		codeModelVariableEClass = createEClass(CODE_MODEL_VARIABLE);

		sequentialCompositionEClass = createEClass(SEQUENTIAL_COMPOSITION);

		labelEClass = createEClass(LABEL);

		dependencyEClass = createEClass(DEPENDENCY);

		customType_CMEClass = createEClass(CUSTOM_TYPE_CM);
		createEReference(customType_CMEClass, CUSTOM_TYPE_CM__CONTENT);

		gaCodeModelEClass = createEClass(GA_CODE_MODEL);
		createEReference(gaCodeModelEClass, GA_CODE_MODEL__SYSTEM_MODEL);
		createEReference(gaCodeModelEClass, GA_CODE_MODEL__TEMP_MODEL);
		createEReference(gaCodeModelEClass, GA_CODE_MODEL__ELEMENTS);

		tempModelEClass = createEClass(TEMP_MODEL);
		createEReference(tempModelEClass, TEMP_MODEL__STATE_VARIABLES_STRUCT_TYPE);
		createEReference(tempModelEClass, TEMP_MODEL__TEMP_NAME_SPACE);
		createEReference(tempModelEClass, TEMP_MODEL__ELEMENTS);

		gaCodeModelRootEClass = createEClass(GA_CODE_MODEL_ROOT);

		structureMemberEClass = createEClass(STRUCTURE_MEMBER);

		arrayContentEClass = createEClass(ARRAY_CONTENT);
		createEReference(arrayContentEClass, ARRAY_CONTENT__RANGE);
		createEReference(arrayContentEClass, ARRAY_CONTENT__BASE_TYPE);

		derivedTypeEClass = createEClass(DERIVED_TYPE);
		createEReference(derivedTypeEClass, DERIVED_TYPE__PARENT_TYPE);

		expressionFunctionBodyEClass = createEClass(EXPRESSION_FUNCTION_BODY);
		createEReference(expressionFunctionBodyEClass, EXPRESSION_FUNCTION_BODY__EXPRESSION);

		functionBodyEClass = createEClass(FUNCTION_BODY);

		functionArgument_CMEClass = createEClass(FUNCTION_ARGUMENT_CM);
		createEAttribute(functionArgument_CMEClass, FUNCTION_ARGUMENT_CM__IS_IMPLICIT);
		createEAttribute(functionArgument_CMEClass, FUNCTION_ARGUMENT_CM__IS_REDUNDANT);
		createEAttribute(functionArgument_CMEClass, FUNCTION_ARGUMENT_CM__DIRECTION);

		structureVariableEClass = createEClass(STRUCTURE_VARIABLE);

		codeModelDependencyEClass = createEClass(CODE_MODEL_DEPENDENCY);
		createEReference(codeModelDependencyEClass, CODE_MODEL_DEPENDENCY__DEPENDENT_MODULE);

		externalDependencyEClass = createEClass(EXTERNAL_DEPENDENCY);
		createEAttribute(externalDependencyEClass, EXTERNAL_DEPENDENCY__IS_SYSTEM);
		createEAttribute(externalDependencyEClass, EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION);

		namedConstantEClass = createEClass(NAMED_CONSTANT);

		function_CMEClass = createEClass(FUNCTION_CM);
		createEAttribute(function_CMEClass, FUNCTION_CM__FUNCTION_STYLE);
		createEReference(function_CMEClass, FUNCTION_CM__ARGUMENTS);
		createEReference(function_CMEClass, FUNCTION_CM__BODY);

		codeModelFunctionEClass = createEClass(CODE_MODEL_FUNCTION);

		sequentialFunctionBodyEClass = createEClass(SEQUENTIAL_FUNCTION_BODY);
		createEReference(sequentialFunctionBodyEClass, SEQUENTIAL_FUNCTION_BODY__STATEMENTS);

		literalContentEClass = createEClass(LITERAL_CONTENT);
		createEAttribute(literalContentEClass, LITERAL_CONTENT__VALUE);

		structureContentEClass = createEClass(STRUCTURE_CONTENT);
		createEReference(structureContentEClass, STRUCTURE_CONTENT__STRUCT_MEMBERS);

		moduleEClass = createEClass(MODULE);
		createEAttribute(moduleEClass, MODULE__HEADER_FILE_NAME);
		createEAttribute(moduleEClass, MODULE__SOURCE_FILE_NAME);
		createEReference(moduleEClass, MODULE__INIT_FUNCTION);
		createEReference(moduleEClass, MODULE__DEPENDENCIES);
		createEReference(moduleEClass, MODULE__HEADER_ANNOTATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ExpressionPackage theExpressionPackage = (ExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionPackage.eNS_URI);
		StatementPackage theStatementPackage = (StatementPackage)EPackage.Registry.INSTANCE.getEPackage(StatementPackage.eNS_URI);
		GaenumtypesPackage theGaenumtypesPackage = (GaenumtypesPackage)EPackage.Registry.INSTANCE.getEPackage(GaenumtypesPackage.eNS_URI);
		OperatorPackage theOperatorPackage = (OperatorPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorPackage.eNS_URI);
		GenericmodelPackage theGenericmodelPackage = (GenericmodelPackage)EPackage.Registry.INSTANCE.getEPackage(GenericmodelPackage.eNS_URI);
		GasystemmodelPackage theGasystemmodelPackage = (GasystemmodelPackage)EPackage.Registry.INSTANCE.getEPackage(GasystemmodelPackage.eNS_URI);
		CommonPackage theCommonPackage = (CommonPackage)EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI);
		GadatatypesPackage theGadatatypesPackage = (GadatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(GadatatypesPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theExpressionPackage);
		getESubpackages().add(theStatementPackage);
		getESubpackages().add(theGaenumtypesPackage);
		getESubpackages().add(theOperatorPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		gaCodeModelElementEClass.getESuperTypes().add(theGenericmodelPackage.getGAModelElement());
		customTypeContentEClass.getESuperTypes().add(this.getGACodeModelElement());
		nameSpaceEClass.getESuperTypes().add(this.getGACodeModelElement());
		nameSpaceElement_CMEClass.getESuperTypes().add(this.getGACodeModelElement());
		variable_CMEClass.getESuperTypes().add(this.getNameSpaceElement_CM());
		variable_CMEClass.getESuperTypes().add(this.getCodeModelVariable());
		codeModelVariableEClass.getESuperTypes().add(theCommonPackage.getVariable());
		labelEClass.getESuperTypes().add(this.getGACodeModelElement());
		dependencyEClass.getESuperTypes().add(this.getGACodeModelElement());
		customType_CMEClass.getESuperTypes().add(this.getNameSpaceElement_CM());
		customType_CMEClass.getESuperTypes().add(theCommonPackage.getCustomType());
		gaCodeModelEClass.getESuperTypes().add(theGenericmodelPackage.getModel());
		tempModelEClass.getESuperTypes().add(this.getGACodeModelElement());
		tempModelEClass.getESuperTypes().add(this.getGACodeModelRoot());
		structureMemberEClass.getESuperTypes().add(this.getVariable_CM());
		arrayContentEClass.getESuperTypes().add(this.getCustomTypeContent());
		derivedTypeEClass.getESuperTypes().add(this.getCustomType_CM());
		expressionFunctionBodyEClass.getESuperTypes().add(this.getFunctionBody());
		functionBodyEClass.getESuperTypes().add(this.getGACodeModelElement());
		functionArgument_CMEClass.getESuperTypes().add(this.getVariable_CM());
		structureVariableEClass.getESuperTypes().add(this.getVariable_CM());
		codeModelDependencyEClass.getESuperTypes().add(this.getDependency());
		externalDependencyEClass.getESuperTypes().add(this.getDependency());
		namedConstantEClass.getESuperTypes().add(this.getVariable_CM());
		function_CMEClass.getESuperTypes().add(this.getNameSpaceElement_CM());
		function_CMEClass.getESuperTypes().add(this.getCodeModelFunction());
		function_CMEClass.getESuperTypes().add(this.getHasNameSpace());
		codeModelFunctionEClass.getESuperTypes().add(theCommonPackage.getFunction());
		sequentialFunctionBodyEClass.getESuperTypes().add(this.getFunctionBody());
		sequentialFunctionBodyEClass.getESuperTypes().add(this.getHasNameSpace());
		sequentialFunctionBodyEClass.getESuperTypes().add(this.getSequentialComposition());
		literalContentEClass.getESuperTypes().add(this.getCustomTypeContent());
		structureContentEClass.getESuperTypes().add(this.getCustomTypeContent());
		moduleEClass.getESuperTypes().add(this.getGACodeModelElement());
		moduleEClass.getESuperTypes().add(this.getGACodeModelRoot());
		moduleEClass.getESuperTypes().add(this.getHasNameSpace());

		// Initialize classes and features; add operations and parameters
		initEClass(gaCodeModelElementEClass, GACodeModelElement.class, "GACodeModelElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGACodeModelElement_SourceElement(), theGasystemmodelPackage.getGASystemModelElement(), null, "sourceElement", null, 0, 1, GACodeModelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGACodeModelElement_SourceElementId(), ecorePackage.getEInt(), "sourceElementId", null, 0, 1, GACodeModelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGACodeModelElement_SourceAction(), ecorePackage.getEString(), "sourceAction", null, 0, 1, GACodeModelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGACodeModelElement_Module(), this.getModule(), null, "module", null, 0, 1, GACodeModelElement.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(customTypeContentEClass, CustomTypeContent.class, "CustomTypeContent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hasNameSpaceEClass, HasNameSpace.class, "HasNameSpace", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHasNameSpace_NameSpace(), this.getNameSpace(), null, "nameSpace", null, 0, 1, HasNameSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nameSpaceEClass, NameSpace.class, "NameSpace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNameSpace_NsElements(), this.getNameSpaceElement_CM(), null, "nsElements", null, 0, -1, NameSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNameSpace_ImplicitArgumentValues(), theExpressionPackage.getExpression(), null, "implicitArgumentValues", null, 0, -1, NameSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nameSpaceElement_CMEClass, NameSpaceElement_CM.class, "NameSpaceElement_CM", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNameSpaceElement_CM_Scope(), theGaenumtypesPackage.getDefinitionScope(), "scope", null, 0, 1, NameSpaceElement_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variable_CMEClass, Variable_CM.class, "Variable_CM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVariable_CM_AutoInit(), ecorePackage.getEBoolean(), "autoInit", "true", 0, 1, Variable_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariable_CM_ObservationPoints(), ecorePackage.getEString(), "observationPoints", null, 0, -1, Variable_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeModelVariableEClass, CodeModelVariable.class, "CodeModelVariable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sequentialCompositionEClass, SequentialComposition.class, "SequentialComposition", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(labelEClass, Label.class, "Label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dependencyEClass, Dependency.class, "Dependency", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(customType_CMEClass, CustomType_CM.class, "CustomType_CM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCustomType_CM_Content(), this.getCustomTypeContent(), null, "content", null, 0, 1, CustomType_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gaCodeModelEClass, GACodeModel.class, "GACodeModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGACodeModel_SystemModel(), theGasystemmodelPackage.getGASystemModel(), null, "systemModel", null, 0, 1, GACodeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGACodeModel_TempModel(), this.getTempModel(), null, "tempModel", null, 0, 1, GACodeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGACodeModel_Elements(), this.getGACodeModelRoot(), null, "elements", null, 0, -1, GACodeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tempModelEClass, TempModel.class, "TempModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTempModel_StateVariablesStructType(), this.getCustomType_CM(), null, "stateVariablesStructType", null, 0, 1, TempModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTempModel_TempNameSpace(), this.getNameSpace(), null, "tempNameSpace", null, 0, 1, TempModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTempModel_Elements(), theGenericmodelPackage.getGAModelElement(), null, "elements", null, 0, -1, TempModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gaCodeModelRootEClass, GACodeModelRoot.class, "GACodeModelRoot", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(structureMemberEClass, StructureMember.class, "StructureMember", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(arrayContentEClass, ArrayContent.class, "ArrayContent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArrayContent_Range(), theExpressionPackage.getRangeExpression(), null, "range", null, 0, 1, ArrayContent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArrayContent_BaseType(), theGadatatypesPackage.getGADataType(), null, "baseType", null, 0, 1, ArrayContent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(derivedTypeEClass, DerivedType.class, "DerivedType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDerivedType_ParentType(), theGadatatypesPackage.getGADataType(), null, "parentType", null, 0, 1, DerivedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionFunctionBodyEClass, ExpressionFunctionBody.class, "ExpressionFunctionBody", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpressionFunctionBody_Expression(), theExpressionPackage.getExpression(), null, "expression", null, 0, 1, ExpressionFunctionBody.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionBodyEClass, FunctionBody.class, "FunctionBody", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(functionArgument_CMEClass, FunctionArgument_CM.class, "FunctionArgument_CM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFunctionArgument_CM_IsImplicit(), ecorePackage.getEBoolean(), "isImplicit", null, 0, 1, FunctionArgument_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionArgument_CM_IsRedundant(), ecorePackage.getEBoolean(), "isRedundant", null, 0, 1, FunctionArgument_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionArgument_CM_Direction(), theGaenumtypesPackage.getDirection(), "direction", null, 0, 1, FunctionArgument_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(structureVariableEClass, StructureVariable.class, "StructureVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(codeModelDependencyEClass, CodeModelDependency.class, "CodeModelDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCodeModelDependency_DependentModule(), this.getModule(), null, "dependentModule", null, 0, 1, CodeModelDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(externalDependencyEClass, ExternalDependency.class, "ExternalDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExternalDependency_IsSystem(), ecorePackage.getEBoolean(), "isSystem", "false", 0, 1, ExternalDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExternalDependency_NonStandardExtension(), ecorePackage.getEBoolean(), "nonStandardExtension", "false", 0, 1, ExternalDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedConstantEClass, NamedConstant.class, "NamedConstant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(function_CMEClass, Function_CM.class, "Function_CM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFunction_CM_FunctionStyle(), theGaenumtypesPackage.getFunctionStyle(), "functionStyle", null, 0, 1, Function_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_CM_Arguments(), this.getFunctionArgument_CM(), null, "arguments", null, 0, -1, Function_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_CM_Body(), this.getFunctionBody(), null, "body", null, 0, 1, Function_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeModelFunctionEClass, CodeModelFunction.class, "CodeModelFunction", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sequentialFunctionBodyEClass, SequentialFunctionBody.class, "SequentialFunctionBody", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSequentialFunctionBody_Statements(), theStatementPackage.getStatement(), null, "statements", null, 0, -1, SequentialFunctionBody.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(literalContentEClass, LiteralContent.class, "LiteralContent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLiteralContent_Value(), ecorePackage.getEString(), "value", null, 0, 1, LiteralContent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(structureContentEClass, StructureContent.class, "StructureContent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStructureContent_StructMembers(), this.getStructureMember(), null, "structMembers", null, 0, -1, StructureContent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(moduleEClass, Module.class, "Module", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModule_HeaderFileName(), ecorePackage.getEString(), "headerFileName", null, 0, 1, Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModule_SourceFileName(), ecorePackage.getEString(), "sourceFileName", null, 0, 1, Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_InitFunction(), this.getFunction_CM(), null, "initFunction", null, 0, 1, Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_Dependencies(), this.getDependency(), null, "dependencies", null, 0, -1, Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModule_HeaderAnnotation(), theGenericmodelPackage.getAnnotation(), null, "headerAnnotation", null, 0, 1, Module.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //GacodemodelPackageImpl
