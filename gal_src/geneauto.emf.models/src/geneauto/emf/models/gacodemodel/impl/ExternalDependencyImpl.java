/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.gacodemodel.ExternalDependency;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ExternalDependencyImpl#isSystem <em>Is System</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.ExternalDependencyImpl#isNonStandardExtension <em>Non Standard Extension</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExternalDependencyImpl extends DependencyImpl implements ExternalDependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #isSystem() <em>Is System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSystem()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_SYSTEM_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSystem() <em>Is System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSystem()
	 * @generated
	 * @ordered
	 */
	protected boolean isSystem = IS_SYSTEM_EDEFAULT;

	/**
	 * This is true if the Is System attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isSystemESet;

	/**
	 * The default value of the '{@link #isNonStandardExtension() <em>Non Standard Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNonStandardExtension()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NON_STANDARD_EXTENSION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNonStandardExtension() <em>Non Standard Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNonStandardExtension()
	 * @generated
	 * @ordered
	 */
	protected boolean nonStandardExtension = NON_STANDARD_EXTENSION_EDEFAULT;

	/**
	 * This is true if the Non Standard Extension attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nonStandardExtensionESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.EXTERNAL_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSystem() {
		return isSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsSystem(boolean newIsSystem) {
		boolean oldIsSystem = isSystem;
		isSystem = newIsSystem;
		boolean oldIsSystemESet = isSystemESet;
		isSystemESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.EXTERNAL_DEPENDENCY__IS_SYSTEM, oldIsSystem, isSystem, !oldIsSystemESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsSystem() {
		boolean oldIsSystem = isSystem;
		boolean oldIsSystemESet = isSystemESet;
		isSystem = IS_SYSTEM_EDEFAULT;
		isSystemESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.EXTERNAL_DEPENDENCY__IS_SYSTEM, oldIsSystem, IS_SYSTEM_EDEFAULT, oldIsSystemESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsSystem() {
		return isSystemESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNonStandardExtension() {
		return nonStandardExtension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNonStandardExtension(boolean newNonStandardExtension) {
		boolean oldNonStandardExtension = nonStandardExtension;
		nonStandardExtension = newNonStandardExtension;
		boolean oldNonStandardExtensionESet = nonStandardExtensionESet;
		nonStandardExtensionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION, oldNonStandardExtension, nonStandardExtension, !oldNonStandardExtensionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNonStandardExtension() {
		boolean oldNonStandardExtension = nonStandardExtension;
		boolean oldNonStandardExtensionESet = nonStandardExtensionESet;
		nonStandardExtension = NON_STANDARD_EXTENSION_EDEFAULT;
		nonStandardExtensionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION, oldNonStandardExtension, NON_STANDARD_EXTENSION_EDEFAULT, oldNonStandardExtensionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNonStandardExtension() {
		return nonStandardExtensionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__IS_SYSTEM:
				return isSystem();
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION:
				return isNonStandardExtension();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__IS_SYSTEM:
				setIsSystem((Boolean)newValue);
				return;
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION:
				setNonStandardExtension((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__IS_SYSTEM:
				unsetIsSystem();
				return;
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION:
				unsetNonStandardExtension();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__IS_SYSTEM:
				return isSetIsSystem();
			case GacodemodelPackage.EXTERNAL_DEPENDENCY__NON_STANDARD_EXTENSION:
				return isSetNonStandardExtension();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isSystem: ");
		if (isSystemESet) result.append(isSystem); else result.append("<unset>");
		result.append(", nonStandardExtension: ");
		if (nonStandardExtensionESet) result.append(nonStandardExtension); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ExternalDependencyImpl
