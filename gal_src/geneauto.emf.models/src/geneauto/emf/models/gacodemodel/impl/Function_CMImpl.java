/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel.impl;

import geneauto.emf.models.common.CommonPackage;
import geneauto.emf.models.common.Function;
import geneauto.emf.models.common.NameSpaceElement;

import geneauto.emf.models.gacodemodel.CodeModelFunction;
import geneauto.emf.models.gacodemodel.FunctionArgument_CM;
import geneauto.emf.models.gacodemodel.FunctionBody;
import geneauto.emf.models.gacodemodel.Function_CM;
import geneauto.emf.models.gacodemodel.GacodemodelPackage;
import geneauto.emf.models.gacodemodel.HasNameSpace;
import geneauto.emf.models.gacodemodel.NameSpace;

import geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle;

import geneauto.emf.models.gadatatypes.GADataType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function CM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Function_CMImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Function_CMImpl#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Function_CMImpl#getFunctionStyle <em>Function Style</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Function_CMImpl#getArguments <em>Arguments</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.impl.Function_CMImpl#getBody <em>Body</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Function_CMImpl extends NameSpaceElement_CMImpl implements Function_CM {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected GADataType dataType;

	/**
	 * This is true if the Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dataTypeESet;

	/**
	 * The cached value of the '{@link #getNameSpace() <em>Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpace()
	 * @generated
	 * @ordered
	 */
	protected NameSpace nameSpace;

	/**
	 * This is true if the Name Space containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameSpaceESet;

	/**
	 * The default value of the '{@link #getFunctionStyle() <em>Function Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionStyle()
	 * @generated
	 * @ordered
	 */
	protected static final FunctionStyle FUNCTION_STYLE_EDEFAULT = FunctionStyle.FUNCTION;

	/**
	 * The cached value of the '{@link #getFunctionStyle() <em>Function Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionStyle()
	 * @generated
	 * @ordered
	 */
	protected FunctionStyle functionStyle = FUNCTION_STYLE_EDEFAULT;

	/**
	 * This is true if the Function Style attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean functionStyleESet;

	/**
	 * The cached value of the '{@link #getArguments() <em>Arguments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArguments()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionArgument_CM> arguments;

	/**
	 * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBody()
	 * @generated
	 * @ordered
	 */
	protected FunctionBody body;

	/**
	 * This is true if the Body containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bodyESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Function_CMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GacodemodelPackage.Literals.FUNCTION_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GADataType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataType(GADataType newDataType, NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = newDataType;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.FUNCTION_CM__DATA_TYPE, oldDataType, newDataType, !oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(GADataType newDataType) {
		if (newDataType != dataType) {
			NotificationChain msgs = null;
			if (dataType != null)
				msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__DATA_TYPE, null, msgs);
			if (newDataType != null)
				msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__DATA_TYPE, null, msgs);
			msgs = basicSetDataType(newDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.FUNCTION_CM__DATA_TYPE, newDataType, newDataType, !oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDataType(NotificationChain msgs) {
		GADataType oldDataType = dataType;
		dataType = null;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.FUNCTION_CM__DATA_TYPE, oldDataType, null, oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDataType() {
		if (dataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__DATA_TYPE, null, msgs);
			msgs = basicUnsetDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.FUNCTION_CM__DATA_TYPE, null, null, oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDataType() {
		return dataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace getNameSpace() {
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameSpace(NameSpace newNameSpace, NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = newNameSpace;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.FUNCTION_CM__NAME_SPACE, oldNameSpace, newNameSpace, !oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameSpace(NameSpace newNameSpace) {
		if (newNameSpace != nameSpace) {
			NotificationChain msgs = null;
			if (nameSpace != null)
				msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__NAME_SPACE, null, msgs);
			if (newNameSpace != null)
				msgs = ((InternalEObject)newNameSpace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__NAME_SPACE, null, msgs);
			msgs = basicSetNameSpace(newNameSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.FUNCTION_CM__NAME_SPACE, newNameSpace, newNameSpace, !oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetNameSpace(NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = null;
		boolean oldNameSpaceESet = nameSpaceESet;
		nameSpaceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.FUNCTION_CM__NAME_SPACE, oldNameSpace, null, oldNameSpaceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNameSpace() {
		if (nameSpace != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__NAME_SPACE, null, msgs);
			msgs = basicUnsetNameSpace(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldNameSpaceESet = nameSpaceESet;
			nameSpaceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.FUNCTION_CM__NAME_SPACE, null, null, oldNameSpaceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNameSpace() {
		return nameSpaceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionStyle getFunctionStyle() {
		return functionStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionStyle(FunctionStyle newFunctionStyle) {
		FunctionStyle oldFunctionStyle = functionStyle;
		functionStyle = newFunctionStyle == null ? FUNCTION_STYLE_EDEFAULT : newFunctionStyle;
		boolean oldFunctionStyleESet = functionStyleESet;
		functionStyleESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.FUNCTION_CM__FUNCTION_STYLE, oldFunctionStyle, functionStyle, !oldFunctionStyleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFunctionStyle() {
		FunctionStyle oldFunctionStyle = functionStyle;
		boolean oldFunctionStyleESet = functionStyleESet;
		functionStyle = FUNCTION_STYLE_EDEFAULT;
		functionStyleESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.FUNCTION_CM__FUNCTION_STYLE, oldFunctionStyle, FUNCTION_STYLE_EDEFAULT, oldFunctionStyleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFunctionStyle() {
		return functionStyleESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionArgument_CM> getArguments() {
		if (arguments == null) {
			arguments = new EObjectResolvingEList.Unsettable<FunctionArgument_CM>(FunctionArgument_CM.class, this, GacodemodelPackage.FUNCTION_CM__ARGUMENTS);
		}
		return arguments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetArguments() {
		if (arguments != null) ((InternalEList.Unsettable<?>)arguments).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetArguments() {
		return arguments != null && ((InternalEList.Unsettable<?>)arguments).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBody getBody() {
		return body;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBody(FunctionBody newBody, NotificationChain msgs) {
		FunctionBody oldBody = body;
		body = newBody;
		boolean oldBodyESet = bodyESet;
		bodyESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GacodemodelPackage.FUNCTION_CM__BODY, oldBody, newBody, !oldBodyESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBody(FunctionBody newBody) {
		if (newBody != body) {
			NotificationChain msgs = null;
			if (body != null)
				msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__BODY, null, msgs);
			if (newBody != null)
				msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__BODY, null, msgs);
			msgs = basicSetBody(newBody, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldBodyESet = bodyESet;
			bodyESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, GacodemodelPackage.FUNCTION_CM__BODY, newBody, newBody, !oldBodyESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetBody(NotificationChain msgs) {
		FunctionBody oldBody = body;
		body = null;
		boolean oldBodyESet = bodyESet;
		bodyESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.FUNCTION_CM__BODY, oldBody, null, oldBodyESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBody() {
		if (body != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GacodemodelPackage.FUNCTION_CM__BODY, null, msgs);
			msgs = basicUnsetBody(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldBodyESet = bodyESet;
			bodyESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, GacodemodelPackage.FUNCTION_CM__BODY, null, null, oldBodyESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBody() {
		return bodyESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GacodemodelPackage.FUNCTION_CM__DATA_TYPE:
				return basicUnsetDataType(msgs);
			case GacodemodelPackage.FUNCTION_CM__NAME_SPACE:
				return basicUnsetNameSpace(msgs);
			case GacodemodelPackage.FUNCTION_CM__BODY:
				return basicUnsetBody(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GacodemodelPackage.FUNCTION_CM__DATA_TYPE:
				return getDataType();
			case GacodemodelPackage.FUNCTION_CM__NAME_SPACE:
				return getNameSpace();
			case GacodemodelPackage.FUNCTION_CM__FUNCTION_STYLE:
				return getFunctionStyle();
			case GacodemodelPackage.FUNCTION_CM__ARGUMENTS:
				return getArguments();
			case GacodemodelPackage.FUNCTION_CM__BODY:
				return getBody();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GacodemodelPackage.FUNCTION_CM__DATA_TYPE:
				setDataType((GADataType)newValue);
				return;
			case GacodemodelPackage.FUNCTION_CM__NAME_SPACE:
				setNameSpace((NameSpace)newValue);
				return;
			case GacodemodelPackage.FUNCTION_CM__FUNCTION_STYLE:
				setFunctionStyle((FunctionStyle)newValue);
				return;
			case GacodemodelPackage.FUNCTION_CM__ARGUMENTS:
				getArguments().clear();
				getArguments().addAll((Collection<? extends FunctionArgument_CM>)newValue);
				return;
			case GacodemodelPackage.FUNCTION_CM__BODY:
				setBody((FunctionBody)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.FUNCTION_CM__DATA_TYPE:
				unsetDataType();
				return;
			case GacodemodelPackage.FUNCTION_CM__NAME_SPACE:
				unsetNameSpace();
				return;
			case GacodemodelPackage.FUNCTION_CM__FUNCTION_STYLE:
				unsetFunctionStyle();
				return;
			case GacodemodelPackage.FUNCTION_CM__ARGUMENTS:
				unsetArguments();
				return;
			case GacodemodelPackage.FUNCTION_CM__BODY:
				unsetBody();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GacodemodelPackage.FUNCTION_CM__DATA_TYPE:
				return isSetDataType();
			case GacodemodelPackage.FUNCTION_CM__NAME_SPACE:
				return isSetNameSpace();
			case GacodemodelPackage.FUNCTION_CM__FUNCTION_STYLE:
				return isSetFunctionStyle();
			case GacodemodelPackage.FUNCTION_CM__ARGUMENTS:
				return isSetArguments();
			case GacodemodelPackage.FUNCTION_CM__BODY:
				return isSetBody();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NameSpaceElement.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Function.class) {
			switch (derivedFeatureID) {
				case GacodemodelPackage.FUNCTION_CM__DATA_TYPE: return CommonPackage.FUNCTION__DATA_TYPE;
				default: return -1;
			}
		}
		if (baseClass == CodeModelFunction.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == HasNameSpace.class) {
			switch (derivedFeatureID) {
				case GacodemodelPackage.FUNCTION_CM__NAME_SPACE: return GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NameSpaceElement.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Function.class) {
			switch (baseFeatureID) {
				case CommonPackage.FUNCTION__DATA_TYPE: return GacodemodelPackage.FUNCTION_CM__DATA_TYPE;
				default: return -1;
			}
		}
		if (baseClass == CodeModelFunction.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == HasNameSpace.class) {
			switch (baseFeatureID) {
				case GacodemodelPackage.HAS_NAME_SPACE__NAME_SPACE: return GacodemodelPackage.FUNCTION_CM__NAME_SPACE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (functionStyle: ");
		if (functionStyleESet) result.append(functionStyle); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //Function_CMImpl
