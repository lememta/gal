/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is a dependency of a CodeModel Module of an external module that is not represented in the CodeModel, e.g. "math" (referenced by including "math.h").  The external module is referenced by name attribute
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.ExternalDependency#isSystem <em>Is System</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.ExternalDependency#isNonStandardExtension <em>Non Standard Extension</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getExternalDependency()
 * @model
 * @generated
 */
public interface ExternalDependency extends Dependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Is System</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is System</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is System</em>' attribute.
	 * @see #isSetIsSystem()
	 * @see #unsetIsSystem()
	 * @see #setIsSystem(boolean)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getExternalDependency_IsSystem()
	 * @model default="false" unsettable="true"
	 * @generated
	 */
	boolean isSystem();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isSystem <em>Is System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is System</em>' attribute.
	 * @see #isSetIsSystem()
	 * @see #unsetIsSystem()
	 * @see #isSystem()
	 * @generated
	 */
	void setIsSystem(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isSystem <em>Is System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsSystem()
	 * @see #isSystem()
	 * @see #setIsSystem(boolean)
	 * @generated
	 */
	void unsetIsSystem();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isSystem <em>Is System</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is System</em>' attribute is set.
	 * @see #unsetIsSystem()
	 * @see #isSystem()
	 * @see #setIsSystem(boolean)
	 * @generated
	 */
	boolean isSetIsSystem();

	/**
	 * Returns the value of the '<em><b>Non Standard Extension</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Non Standard Extension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Non Standard Extension</em>' attribute.
	 * @see #isSetNonStandardExtension()
	 * @see #unsetNonStandardExtension()
	 * @see #setNonStandardExtension(boolean)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getExternalDependency_NonStandardExtension()
	 * @model default="false" unsettable="true"
	 * @generated
	 */
	boolean isNonStandardExtension();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isNonStandardExtension <em>Non Standard Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Non Standard Extension</em>' attribute.
	 * @see #isSetNonStandardExtension()
	 * @see #unsetNonStandardExtension()
	 * @see #isNonStandardExtension()
	 * @generated
	 */
	void setNonStandardExtension(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isNonStandardExtension <em>Non Standard Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNonStandardExtension()
	 * @see #isNonStandardExtension()
	 * @see #setNonStandardExtension(boolean)
	 * @generated
	 */
	void unsetNonStandardExtension();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.ExternalDependency#isNonStandardExtension <em>Non Standard Extension</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Non Standard Extension</em>' attribute is set.
	 * @see #unsetNonStandardExtension()
	 * @see #isNonStandardExtension()
	 * @see #setNonStandardExtension(boolean)
	 * @generated
	 */
	boolean isSetNonStandardExtension();

} // ExternalDependency
