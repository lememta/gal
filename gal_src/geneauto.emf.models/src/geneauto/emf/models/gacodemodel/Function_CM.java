/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gacodemodel;

import geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function CM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Abstract entity representing a function. Function description contains information about its signature (name, return value, arguments) and body. Three related code sections can be generated from the function object:
 * <ul>
 * <li><b>Function declaration</b> or function prototype to be placed either in . h file (in case of exported function) or in *.c file (in case of local function).</li>
 * <li><b>Function definition</b> - full function definition including the signature and body</li>
 * <li><b>Function instance </b>or function call to be used to execute the function.</li>
 * </ul> Function declaration and function definition can be compiled solely from the information contained in the Function object. To compile a function instance, a list of function arguments is required to link the function call to the rest of the code.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gacodemodel.Function_CM#getFunctionStyle <em>Function Style</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.Function_CM#getArguments <em>Arguments</em>}</li>
 *   <li>{@link geneauto.emf.models.gacodemodel.Function_CM#getBody <em>Body</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunction_CM()
 * @model
 * @generated
 */
public interface Function_CM extends NameSpaceElement_CM, CodeModelFunction, HasNameSpace {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Function Style</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Style</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Style</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle
	 * @see #isSetFunctionStyle()
	 * @see #unsetFunctionStyle()
	 * @see #setFunctionStyle(FunctionStyle)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunction_CM_FunctionStyle()
	 * @model unsettable="true"
	 * @generated
	 */
	FunctionStyle getFunctionStyle();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getFunctionStyle <em>Function Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Style</em>' attribute.
	 * @see geneauto.emf.models.gacodemodel.gaenumtypes.FunctionStyle
	 * @see #isSetFunctionStyle()
	 * @see #unsetFunctionStyle()
	 * @see #getFunctionStyle()
	 * @generated
	 */
	void setFunctionStyle(FunctionStyle value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getFunctionStyle <em>Function Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFunctionStyle()
	 * @see #getFunctionStyle()
	 * @see #setFunctionStyle(FunctionStyle)
	 * @generated
	 */
	void unsetFunctionStyle();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getFunctionStyle <em>Function Style</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Function Style</em>' attribute is set.
	 * @see #unsetFunctionStyle()
	 * @see #getFunctionStyle()
	 * @see #setFunctionStyle(FunctionStyle)
	 * @generated
	 */
	boolean isSetFunctionStyle();

	/**
	 * Returns the value of the '<em><b>Arguments</b></em>' reference list.
	 * The list contents are of type {@link geneauto.emf.models.gacodemodel.FunctionArgument_CM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arguments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments</em>' reference list.
	 * @see #isSetArguments()
	 * @see #unsetArguments()
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunction_CM_Arguments()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<FunctionArgument_CM> getArguments();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getArguments <em>Arguments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetArguments()
	 * @see #getArguments()
	 * @generated
	 */
	void unsetArguments();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getArguments <em>Arguments</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Arguments</em>' reference list is set.
	 * @see #unsetArguments()
	 * @see #getArguments()
	 * @generated
	 */
	boolean isSetArguments();

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #isSetBody()
	 * @see #unsetBody()
	 * @see #setBody(FunctionBody)
	 * @see geneauto.emf.models.gacodemodel.GacodemodelPackage#getFunction_CM_Body()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	FunctionBody getBody();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #isSetBody()
	 * @see #unsetBody()
	 * @see #getBody()
	 * @generated
	 */
	void setBody(FunctionBody value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBody()
	 * @see #getBody()
	 * @see #setBody(FunctionBody)
	 * @generated
	 */
	void unsetBody();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gacodemodel.Function_CM#getBody <em>Body</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Body</em>' containment reference is set.
	 * @see #unsetBody()
	 * @see #getBody()
	 * @see #setBody(FunctionBody)
	 * @generated
	 */
	boolean isSetBody();

} // Function_CM
