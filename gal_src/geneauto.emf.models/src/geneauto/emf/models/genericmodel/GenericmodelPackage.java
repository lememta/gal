/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.genericmodel.GenericmodelFactory
 * @model kind="package"
 * @generated
 */
public interface GenericmodelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "genericmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/genericmodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.genericmodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GenericmodelPackage eINSTANCE = geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.genericmodel.impl.AnnotationImpl
	 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 0;

	/**
	 * The feature id for the '<em><b>From Input Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__FROM_INPUT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Is Block Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__IS_BLOCK_DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Ordinal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__ORDINAL = 2;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__SOURCE = 3;

	/**
	 * The feature id for the '<em><b>To Print</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__TO_PRINT = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__VALUE = 5;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.genericmodel.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.genericmodel.impl.ModelImpl
	 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 1;

	/**
	 * The feature id for the '<em><b>Last Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__LAST_ID = 0;

	/**
	 * The feature id for the '<em><b>Last Saved By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__LAST_SAVED_BY = 1;

	/**
	 * The feature id for the '<em><b>Last Saved On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__LAST_SAVED_ON = 2;

	/**
	 * The feature id for the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__MODEL_NAME = 3;

	/**
	 * The feature id for the '<em><b>Model Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__MODEL_VERSION = 4;

	/**
	 * The feature id for the '<em><b>No New Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__NO_NEW_ID = 5;

	/**
	 * The feature id for the '<em><b>Transformations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__TRANSFORMATIONS = 6;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.genericmodel.impl.TransformationImpl <em>Transformation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.genericmodel.impl.TransformationImpl
	 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getTransformation()
	 * @generated
	 */
	int TRANSFORMATION = 2;

	/**
	 * The feature id for the '<em><b>Read Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION__READ_TIME = 0;

	/**
	 * The feature id for the '<em><b>Tool Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION__TOOL_NAME = 1;

	/**
	 * The feature id for the '<em><b>Write Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION__WRITE_TIME = 2;

	/**
	 * The number of structural features of the '<em>Transformation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.genericmodel.impl.NodeTypeMapImpl <em>Node Type Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.genericmodel.impl.NodeTypeMapImpl
	 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getNodeTypeMap()
	 * @generated
	 */
	int NODE_TYPE_MAP = 3;

	/**
	 * The number of structural features of the '<em>Node Type Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE_MAP_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.genericmodel.GANamed <em>GA Named</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.genericmodel.GANamed
	 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getGANamed()
	 * @generated
	 */
	int GA_NAMED = 4;

	/**
	 * The number of structural features of the '<em>GA Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_NAMED_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl <em>GA Model Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.genericmodel.impl.GAModelElementImpl
	 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getGAModelElement()
	 * @generated
	 */
	int GA_MODEL_ELEMENT = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__EXTERNAL_ID = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__ID = 1;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__IS_FIXED_NAME = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__NAME = 3;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME = 4;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__ORIGINAL_NAME = 5;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__PARENT = 6;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__MODEL = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT__ANNOTATIONS = 8;

	/**
	 * The number of structural features of the '<em>GA Model Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_MODEL_ELEMENT_FEATURE_COUNT = 9;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Annotation#isFromInputModel <em>From Input Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From Input Model</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation#isFromInputModel()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_FromInputModel();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Annotation#isBlockDescription <em>Is Block Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Block Description</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation#isBlockDescription()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_IsBlockDescription();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Annotation#getOrdinal <em>Ordinal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ordinal</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation#getOrdinal()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Ordinal();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Annotation#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation#getSource()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Source();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Annotation#isToPrint <em>To Print</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Print</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation#isToPrint()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_ToPrint();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Annotation#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see geneauto.emf.models.genericmodel.Annotation#getValue()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Value();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see geneauto.emf.models.genericmodel.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Model#getLastId <em>Last Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Id</em>'.
	 * @see geneauto.emf.models.genericmodel.Model#getLastId()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_LastId();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Model#getLastSavedBy <em>Last Saved By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Saved By</em>'.
	 * @see geneauto.emf.models.genericmodel.Model#getLastSavedBy()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_LastSavedBy();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Model#getLastSavedOn <em>Last Saved On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Saved On</em>'.
	 * @see geneauto.emf.models.genericmodel.Model#getLastSavedOn()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_LastSavedOn();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Model#getModelName <em>Model Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Name</em>'.
	 * @see geneauto.emf.models.genericmodel.Model#getModelName()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_ModelName();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Model#getModelVersion <em>Model Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Version</em>'.
	 * @see geneauto.emf.models.genericmodel.Model#getModelVersion()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_ModelVersion();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Model#isNoNewId <em>No New Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>No New Id</em>'.
	 * @see geneauto.emf.models.genericmodel.Model#isNoNewId()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_NoNewId();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.genericmodel.Model#getTransformations <em>Transformations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transformations</em>'.
	 * @see geneauto.emf.models.genericmodel.Model#getTransformations()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Transformations();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.Transformation <em>Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation</em>'.
	 * @see geneauto.emf.models.genericmodel.Transformation
	 * @generated
	 */
	EClass getTransformation();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Transformation#getReadTime <em>Read Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Read Time</em>'.
	 * @see geneauto.emf.models.genericmodel.Transformation#getReadTime()
	 * @see #getTransformation()
	 * @generated
	 */
	EAttribute getTransformation_ReadTime();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Transformation#getToolName <em>Tool Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tool Name</em>'.
	 * @see geneauto.emf.models.genericmodel.Transformation#getToolName()
	 * @see #getTransformation()
	 * @generated
	 */
	EAttribute getTransformation_ToolName();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.Transformation#getWriteTime <em>Write Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Write Time</em>'.
	 * @see geneauto.emf.models.genericmodel.Transformation#getWriteTime()
	 * @see #getTransformation()
	 * @generated
	 */
	EAttribute getTransformation_WriteTime();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.NodeTypeMap <em>Node Type Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Type Map</em>'.
	 * @see geneauto.emf.models.genericmodel.NodeTypeMap
	 * @generated
	 */
	EClass getNodeTypeMap();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.GANamed <em>GA Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA Named</em>'.
	 * @see geneauto.emf.models.genericmodel.GANamed
	 * @generated
	 */
	EClass getGANamed();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.genericmodel.GAModelElement <em>GA Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA Model Element</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement
	 * @generated
	 */
	EClass getGAModelElement();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.GAModelElement#getExternalID <em>External ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External ID</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getExternalID()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EAttribute getGAModelElement_ExternalID();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.GAModelElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getId()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EAttribute getGAModelElement_Id();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.GAModelElement#isFixedName <em>Is Fixed Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Fixed Name</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#isFixedName()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EAttribute getGAModelElement_IsFixedName();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.GAModelElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getName()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EAttribute getGAModelElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalFullName <em>Original Full Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Original Full Name</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getOriginalFullName()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EAttribute getGAModelElement_OriginalFullName();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalName <em>Original Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Original Name</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getOriginalName()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EAttribute getGAModelElement_OriginalName();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.genericmodel.GAModelElement#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getParent()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EReference getGAModelElement_Parent();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.genericmodel.GAModelElement#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Model</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getModel()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EReference getGAModelElement_Model();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.genericmodel.GAModelElement#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotations</em>'.
	 * @see geneauto.emf.models.genericmodel.GAModelElement#getAnnotations()
	 * @see #getGAModelElement()
	 * @generated
	 */
	EReference getGAModelElement_Annotations();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GenericmodelFactory getGenericmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl <em>Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.genericmodel.impl.AnnotationImpl
		 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getAnnotation()
		 * @generated
		 */
		EClass ANNOTATION = eINSTANCE.getAnnotation();

		/**
		 * The meta object literal for the '<em><b>From Input Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__FROM_INPUT_MODEL = eINSTANCE.getAnnotation_FromInputModel();

		/**
		 * The meta object literal for the '<em><b>Is Block Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__IS_BLOCK_DESCRIPTION = eINSTANCE.getAnnotation_IsBlockDescription();

		/**
		 * The meta object literal for the '<em><b>Ordinal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__ORDINAL = eINSTANCE.getAnnotation_Ordinal();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__SOURCE = eINSTANCE.getAnnotation_Source();

		/**
		 * The meta object literal for the '<em><b>To Print</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__TO_PRINT = eINSTANCE.getAnnotation_ToPrint();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__VALUE = eINSTANCE.getAnnotation_Value();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.genericmodel.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.genericmodel.impl.ModelImpl
		 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>Last Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__LAST_ID = eINSTANCE.getModel_LastId();

		/**
		 * The meta object literal for the '<em><b>Last Saved By</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__LAST_SAVED_BY = eINSTANCE.getModel_LastSavedBy();

		/**
		 * The meta object literal for the '<em><b>Last Saved On</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__LAST_SAVED_ON = eINSTANCE.getModel_LastSavedOn();

		/**
		 * The meta object literal for the '<em><b>Model Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__MODEL_NAME = eINSTANCE.getModel_ModelName();

		/**
		 * The meta object literal for the '<em><b>Model Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__MODEL_VERSION = eINSTANCE.getModel_ModelVersion();

		/**
		 * The meta object literal for the '<em><b>No New Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__NO_NEW_ID = eINSTANCE.getModel_NoNewId();

		/**
		 * The meta object literal for the '<em><b>Transformations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__TRANSFORMATIONS = eINSTANCE.getModel_Transformations();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.genericmodel.impl.TransformationImpl <em>Transformation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.genericmodel.impl.TransformationImpl
		 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getTransformation()
		 * @generated
		 */
		EClass TRANSFORMATION = eINSTANCE.getTransformation();

		/**
		 * The meta object literal for the '<em><b>Read Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION__READ_TIME = eINSTANCE.getTransformation_ReadTime();

		/**
		 * The meta object literal for the '<em><b>Tool Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION__TOOL_NAME = eINSTANCE.getTransformation_ToolName();

		/**
		 * The meta object literal for the '<em><b>Write Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSFORMATION__WRITE_TIME = eINSTANCE.getTransformation_WriteTime();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.genericmodel.impl.NodeTypeMapImpl <em>Node Type Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.genericmodel.impl.NodeTypeMapImpl
		 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getNodeTypeMap()
		 * @generated
		 */
		EClass NODE_TYPE_MAP = eINSTANCE.getNodeTypeMap();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.genericmodel.GANamed <em>GA Named</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.genericmodel.GANamed
		 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getGANamed()
		 * @generated
		 */
		EClass GA_NAMED = eINSTANCE.getGANamed();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl <em>GA Model Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.genericmodel.impl.GAModelElementImpl
		 * @see geneauto.emf.models.genericmodel.impl.GenericmodelPackageImpl#getGAModelElement()
		 * @generated
		 */
		EClass GA_MODEL_ELEMENT = eINSTANCE.getGAModelElement();

		/**
		 * The meta object literal for the '<em><b>External ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_MODEL_ELEMENT__EXTERNAL_ID = eINSTANCE.getGAModelElement_ExternalID();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_MODEL_ELEMENT__ID = eINSTANCE.getGAModelElement_Id();

		/**
		 * The meta object literal for the '<em><b>Is Fixed Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_MODEL_ELEMENT__IS_FIXED_NAME = eINSTANCE.getGAModelElement_IsFixedName();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_MODEL_ELEMENT__NAME = eINSTANCE.getGAModelElement_Name();

		/**
		 * The meta object literal for the '<em><b>Original Full Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME = eINSTANCE.getGAModelElement_OriginalFullName();

		/**
		 * The meta object literal for the '<em><b>Original Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GA_MODEL_ELEMENT__ORIGINAL_NAME = eINSTANCE.getGAModelElement_OriginalName();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_MODEL_ELEMENT__PARENT = eINSTANCE.getGAModelElement_Parent();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_MODEL_ELEMENT__MODEL = eINSTANCE.getGAModelElement_Model();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_MODEL_ELEMENT__ANNOTATIONS = eINSTANCE.getGAModelElement_Annotations();

	}

} //GenericmodelPackage
