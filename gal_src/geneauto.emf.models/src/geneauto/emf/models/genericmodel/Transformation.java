/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transformation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Class which is used for storing information about the transformations of the model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.Transformation#getReadTime <em>Read Time</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Transformation#getToolName <em>Tool Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Transformation#getWriteTime <em>Write Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getTransformation()
 * @model
 * @generated
 */
public interface Transformation extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Read Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Read Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Read Time</em>' attribute.
	 * @see #isSetReadTime()
	 * @see #unsetReadTime()
	 * @see #setReadTime(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getTransformation_ReadTime()
	 * @model unsettable="true"
	 * @generated
	 */
	String getReadTime();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getReadTime <em>Read Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Read Time</em>' attribute.
	 * @see #isSetReadTime()
	 * @see #unsetReadTime()
	 * @see #getReadTime()
	 * @generated
	 */
	void setReadTime(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getReadTime <em>Read Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetReadTime()
	 * @see #getReadTime()
	 * @see #setReadTime(String)
	 * @generated
	 */
	void unsetReadTime();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getReadTime <em>Read Time</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Read Time</em>' attribute is set.
	 * @see #unsetReadTime()
	 * @see #getReadTime()
	 * @see #setReadTime(String)
	 * @generated
	 */
	boolean isSetReadTime();

	/**
	 * Returns the value of the '<em><b>Tool Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tool Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tool Name</em>' attribute.
	 * @see #isSetToolName()
	 * @see #unsetToolName()
	 * @see #setToolName(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getTransformation_ToolName()
	 * @model unsettable="true"
	 * @generated
	 */
	String getToolName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getToolName <em>Tool Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tool Name</em>' attribute.
	 * @see #isSetToolName()
	 * @see #unsetToolName()
	 * @see #getToolName()
	 * @generated
	 */
	void setToolName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getToolName <em>Tool Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetToolName()
	 * @see #getToolName()
	 * @see #setToolName(String)
	 * @generated
	 */
	void unsetToolName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getToolName <em>Tool Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Tool Name</em>' attribute is set.
	 * @see #unsetToolName()
	 * @see #getToolName()
	 * @see #setToolName(String)
	 * @generated
	 */
	boolean isSetToolName();

	/**
	 * Returns the value of the '<em><b>Write Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Write Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Write Time</em>' attribute.
	 * @see #isSetWriteTime()
	 * @see #unsetWriteTime()
	 * @see #setWriteTime(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getTransformation_WriteTime()
	 * @model unsettable="true"
	 * @generated
	 */
	String getWriteTime();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getWriteTime <em>Write Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Write Time</em>' attribute.
	 * @see #isSetWriteTime()
	 * @see #unsetWriteTime()
	 * @see #getWriteTime()
	 * @generated
	 */
	void setWriteTime(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getWriteTime <em>Write Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetWriteTime()
	 * @see #getWriteTime()
	 * @see #setWriteTime(String)
	 * @generated
	 */
	void unsetWriteTime();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Transformation#getWriteTime <em>Write Time</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Write Time</em>' attribute is set.
	 * @see #unsetWriteTime()
	 * @see #getWriteTime()
	 * @see #setWriteTime(String)
	 * @generated
	 */
	boolean isSetWriteTime();

} // Transformation
