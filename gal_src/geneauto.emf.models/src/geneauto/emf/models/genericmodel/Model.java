/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Base class for representing a model. The model type can be : - GACodeModel - BlockLibrary - GASystemModel
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.Model#getLastId <em>Last Id</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Model#getLastSavedBy <em>Last Saved By</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Model#getLastSavedOn <em>Last Saved On</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Model#getModelName <em>Model Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Model#getModelVersion <em>Model Version</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Model#isNoNewId <em>No New Id</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Model#getTransformations <em>Transformations</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel()
 * @model abstract="true"
 * @generated
 */
public interface Model extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Last Id</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Id</em>' attribute.
	 * @see #isSetLastId()
	 * @see #unsetLastId()
	 * @see #setLastId(int)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel_LastId()
	 * @model default="0" unsettable="true"
	 * @generated
	 */
	int getLastId();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastId <em>Last Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Id</em>' attribute.
	 * @see #isSetLastId()
	 * @see #unsetLastId()
	 * @see #getLastId()
	 * @generated
	 */
	void setLastId(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastId <em>Last Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLastId()
	 * @see #getLastId()
	 * @see #setLastId(int)
	 * @generated
	 */
	void unsetLastId();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastId <em>Last Id</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Last Id</em>' attribute is set.
	 * @see #unsetLastId()
	 * @see #getLastId()
	 * @see #setLastId(int)
	 * @generated
	 */
	boolean isSetLastId();

	/**
	 * Returns the value of the '<em><b>Last Saved By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Saved By</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Saved By</em>' attribute.
	 * @see #isSetLastSavedBy()
	 * @see #unsetLastSavedBy()
	 * @see #setLastSavedBy(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel_LastSavedBy()
	 * @model unsettable="true"
	 * @generated
	 */
	String getLastSavedBy();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastSavedBy <em>Last Saved By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Saved By</em>' attribute.
	 * @see #isSetLastSavedBy()
	 * @see #unsetLastSavedBy()
	 * @see #getLastSavedBy()
	 * @generated
	 */
	void setLastSavedBy(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastSavedBy <em>Last Saved By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLastSavedBy()
	 * @see #getLastSavedBy()
	 * @see #setLastSavedBy(String)
	 * @generated
	 */
	void unsetLastSavedBy();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastSavedBy <em>Last Saved By</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Last Saved By</em>' attribute is set.
	 * @see #unsetLastSavedBy()
	 * @see #getLastSavedBy()
	 * @see #setLastSavedBy(String)
	 * @generated
	 */
	boolean isSetLastSavedBy();

	/**
	 * Returns the value of the '<em><b>Last Saved On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Saved On</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Saved On</em>' attribute.
	 * @see #isSetLastSavedOn()
	 * @see #unsetLastSavedOn()
	 * @see #setLastSavedOn(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel_LastSavedOn()
	 * @model unsettable="true"
	 * @generated
	 */
	String getLastSavedOn();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastSavedOn <em>Last Saved On</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Saved On</em>' attribute.
	 * @see #isSetLastSavedOn()
	 * @see #unsetLastSavedOn()
	 * @see #getLastSavedOn()
	 * @generated
	 */
	void setLastSavedOn(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastSavedOn <em>Last Saved On</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLastSavedOn()
	 * @see #getLastSavedOn()
	 * @see #setLastSavedOn(String)
	 * @generated
	 */
	void unsetLastSavedOn();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Model#getLastSavedOn <em>Last Saved On</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Last Saved On</em>' attribute is set.
	 * @see #unsetLastSavedOn()
	 * @see #getLastSavedOn()
	 * @see #setLastSavedOn(String)
	 * @generated
	 */
	boolean isSetLastSavedOn();

	/**
	 * Returns the value of the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Name</em>' attribute.
	 * @see #isSetModelName()
	 * @see #unsetModelName()
	 * @see #setModelName(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel_ModelName()
	 * @model unsettable="true"
	 * @generated
	 */
	String getModelName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Model#getModelName <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Name</em>' attribute.
	 * @see #isSetModelName()
	 * @see #unsetModelName()
	 * @see #getModelName()
	 * @generated
	 */
	void setModelName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Model#getModelName <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetModelName()
	 * @see #getModelName()
	 * @see #setModelName(String)
	 * @generated
	 */
	void unsetModelName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Model#getModelName <em>Model Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Model Name</em>' attribute is set.
	 * @see #unsetModelName()
	 * @see #getModelName()
	 * @see #setModelName(String)
	 * @generated
	 */
	boolean isSetModelName();

	/**
	 * Returns the value of the '<em><b>Model Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Version</em>' attribute.
	 * @see #isSetModelVersion()
	 * @see #unsetModelVersion()
	 * @see #setModelVersion(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel_ModelVersion()
	 * @model unsettable="true"
	 * @generated
	 */
	String getModelVersion();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Model#getModelVersion <em>Model Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Version</em>' attribute.
	 * @see #isSetModelVersion()
	 * @see #unsetModelVersion()
	 * @see #getModelVersion()
	 * @generated
	 */
	void setModelVersion(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Model#getModelVersion <em>Model Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetModelVersion()
	 * @see #getModelVersion()
	 * @see #setModelVersion(String)
	 * @generated
	 */
	void unsetModelVersion();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Model#getModelVersion <em>Model Version</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Model Version</em>' attribute is set.
	 * @see #unsetModelVersion()
	 * @see #getModelVersion()
	 * @see #setModelVersion(String)
	 * @generated
	 */
	boolean isSetModelVersion();

	/**
	 * Returns the value of the '<em><b>No New Id</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No New Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No New Id</em>' attribute.
	 * @see #isSetNoNewId()
	 * @see #unsetNoNewId()
	 * @see #setNoNewId(boolean)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel_NoNewId()
	 * @model default="false" unsettable="true"
	 * @generated
	 */
	boolean isNoNewId();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Model#isNoNewId <em>No New Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No New Id</em>' attribute.
	 * @see #isSetNoNewId()
	 * @see #unsetNoNewId()
	 * @see #isNoNewId()
	 * @generated
	 */
	void setNoNewId(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Model#isNoNewId <em>No New Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNoNewId()
	 * @see #isNoNewId()
	 * @see #setNoNewId(boolean)
	 * @generated
	 */
	void unsetNoNewId();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Model#isNoNewId <em>No New Id</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>No New Id</em>' attribute is set.
	 * @see #unsetNoNewId()
	 * @see #isNoNewId()
	 * @see #setNoNewId(boolean)
	 * @generated
	 */
	boolean isSetNoNewId();

	/**
	 * Returns the value of the '<em><b>Transformations</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.genericmodel.Transformation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transformations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transformations</em>' containment reference list.
	 * @see #isSetTransformations()
	 * @see #unsetTransformations()
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getModel_Transformations()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Transformation> getTransformations();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Model#getTransformations <em>Transformations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTransformations()
	 * @see #getTransformations()
	 * @generated
	 */
	void unsetTransformations();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Model#getTransformations <em>Transformations</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Transformations</em>' containment reference list is set.
	 * @see #unsetTransformations()
	 * @see #getTransformations()
	 * @generated
	 */
	boolean isSetTransformations();

} // Model
