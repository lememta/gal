/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GA Model Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Abstract base class for all geneauto language elements
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getExternalID <em>External ID</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getId <em>Id</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#isFixedName <em>Is Fixed Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getName <em>Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalFullName <em>Original Full Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalName <em>Original Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getParent <em>Parent</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getModel <em>Model</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.GAModelElement#getAnnotations <em>Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement()
 * @model abstract="true"
 * @generated
 */
public interface GAModelElement extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External ID</em>' attribute.
	 * @see #isSetExternalID()
	 * @see #unsetExternalID()
	 * @see #setExternalID(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_ExternalID()
	 * @model unsettable="true"
	 * @generated
	 */
	String getExternalID();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getExternalID <em>External ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External ID</em>' attribute.
	 * @see #isSetExternalID()
	 * @see #unsetExternalID()
	 * @see #getExternalID()
	 * @generated
	 */
	void setExternalID(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getExternalID <em>External ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExternalID()
	 * @see #getExternalID()
	 * @see #setExternalID(String)
	 * @generated
	 */
	void unsetExternalID();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getExternalID <em>External ID</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>External ID</em>' attribute is set.
	 * @see #unsetExternalID()
	 * @see #getExternalID()
	 * @see #setExternalID(String)
	 * @generated
	 */
	boolean isSetExternalID();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #isSetId()
	 * @see #unsetId()
	 * @see #setId(int)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_Id()
	 * @model unsettable="true" id="true"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #isSetId()
	 * @see #unsetId()
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetId()
	 * @see #getId()
	 * @see #setId(int)
	 * @generated
	 */
	void unsetId();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getId <em>Id</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id</em>' attribute is set.
	 * @see #unsetId()
	 * @see #getId()
	 * @see #setId(int)
	 * @generated
	 */
	boolean isSetId();

	/**
	 * Returns the value of the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Fixed Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Fixed Name</em>' attribute.
	 * @see #isSetIsFixedName()
	 * @see #unsetIsFixedName()
	 * @see #setIsFixedName(boolean)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_IsFixedName()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isFixedName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#isFixedName <em>Is Fixed Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Fixed Name</em>' attribute.
	 * @see #isSetIsFixedName()
	 * @see #unsetIsFixedName()
	 * @see #isFixedName()
	 * @generated
	 */
	void setIsFixedName(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#isFixedName <em>Is Fixed Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsFixedName()
	 * @see #isFixedName()
	 * @see #setIsFixedName(boolean)
	 * @generated
	 */
	void unsetIsFixedName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#isFixedName <em>Is Fixed Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Fixed Name</em>' attribute is set.
	 * @see #unsetIsFixedName()
	 * @see #isFixedName()
	 * @see #setIsFixedName(boolean)
	 * @generated
	 */
	boolean isSetIsFixedName();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #setName(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_Name()
	 * @model unsettable="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	void unsetName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #unsetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Original Full Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original Full Name</em>' attribute.
	 * @see #isSetOriginalFullName()
	 * @see #unsetOriginalFullName()
	 * @see #setOriginalFullName(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_OriginalFullName()
	 * @model unsettable="true"
	 * @generated
	 */
	String getOriginalFullName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalFullName <em>Original Full Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original Full Name</em>' attribute.
	 * @see #isSetOriginalFullName()
	 * @see #unsetOriginalFullName()
	 * @see #getOriginalFullName()
	 * @generated
	 */
	void setOriginalFullName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalFullName <em>Original Full Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOriginalFullName()
	 * @see #getOriginalFullName()
	 * @see #setOriginalFullName(String)
	 * @generated
	 */
	void unsetOriginalFullName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalFullName <em>Original Full Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Original Full Name</em>' attribute is set.
	 * @see #unsetOriginalFullName()
	 * @see #getOriginalFullName()
	 * @see #setOriginalFullName(String)
	 * @generated
	 */
	boolean isSetOriginalFullName();

	/**
	 * Returns the value of the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Original Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original Name</em>' attribute.
	 * @see #isSetOriginalName()
	 * @see #unsetOriginalName()
	 * @see #setOriginalName(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_OriginalName()
	 * @model unsettable="true"
	 * @generated
	 */
	String getOriginalName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalName <em>Original Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original Name</em>' attribute.
	 * @see #isSetOriginalName()
	 * @see #unsetOriginalName()
	 * @see #getOriginalName()
	 * @generated
	 */
	void setOriginalName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalName <em>Original Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOriginalName()
	 * @see #getOriginalName()
	 * @see #setOriginalName(String)
	 * @generated
	 */
	void unsetOriginalName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getOriginalName <em>Original Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Original Name</em>' attribute is set.
	 * @see #unsetOriginalName()
	 * @see #getOriginalName()
	 * @see #setOriginalName(String)
	 * @generated
	 */
	boolean isSetOriginalName();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #isSetParent()
	 * @see #unsetParent()
	 * @see #setParent(GAModelElement)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_Parent()
	 * @model unsettable="true" transient="true"
	 * @generated
	 */
	GAModelElement getParent();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #isSetParent()
	 * @see #unsetParent()
	 * @see #getParent()
	 * @generated
	 */
	void setParent(GAModelElement value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParent()
	 * @see #getParent()
	 * @see #setParent(GAModelElement)
	 * @generated
	 */
	void unsetParent();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getParent <em>Parent</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parent</em>' reference is set.
	 * @see #unsetParent()
	 * @see #getParent()
	 * @see #setParent(GAModelElement)
	 * @generated
	 */
	boolean isSetParent();

	/**
	 * Returns the value of the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' reference.
	 * @see #isSetModel()
	 * @see #unsetModel()
	 * @see #setModel(Model)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_Model()
	 * @model unsettable="true" transient="true"
	 * @generated
	 */
	Model getModel();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getModel <em>Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' reference.
	 * @see #isSetModel()
	 * @see #unsetModel()
	 * @see #getModel()
	 * @generated
	 */
	void setModel(Model value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getModel <em>Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetModel()
	 * @see #getModel()
	 * @see #setModel(Model)
	 * @generated
	 */
	void unsetModel();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getModel <em>Model</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Model</em>' reference is set.
	 * @see #unsetModel()
	 * @see #getModel()
	 * @see #setModel(Model)
	 * @generated
	 */
	boolean isSetModel();

	/**
	 * Returns the value of the '<em><b>Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.genericmodel.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations</em>' containment reference list.
	 * @see #isSetAnnotations()
	 * @see #unsetAnnotations()
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getGAModelElement_Annotations()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<Annotation> getAnnotations();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getAnnotations <em>Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAnnotations()
	 * @see #getAnnotations()
	 * @generated
	 */
	void unsetAnnotations();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.GAModelElement#getAnnotations <em>Annotations</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Annotations</em>' containment reference list is set.
	 * @see #unsetAnnotations()
	 * @see #getAnnotations()
	 * @generated
	 */
	boolean isSetAnnotations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String Get_infoString();

} // GAModelElement
