/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel.impl;

import geneauto.emf.models.genericmodel.Annotation;
import geneauto.emf.models.genericmodel.GAModelElement;
import geneauto.emf.models.genericmodel.GenericmodelPackage;
import geneauto.emf.models.genericmodel.Model;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GA Model Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getExternalID <em>External ID</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getId <em>Id</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#isFixedName <em>Is Fixed Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getName <em>Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getOriginalFullName <em>Original Full Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getOriginalName <em>Original Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getModel <em>Model</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.GAModelElementImpl#getAnnotations <em>Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class GAModelElementImpl extends EObjectImpl implements GAModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getExternalID() <em>External ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalID()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTERNAL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExternalID() <em>External ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalID()
	 * @generated
	 * @ordered
	 */
	protected String externalID = EXTERNAL_ID_EDEFAULT;

	/**
	 * This is true if the External ID attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean externalIDESet;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * This is true if the Id attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean idESet;

	/**
	 * The default value of the '{@link #isFixedName() <em>Is Fixed Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFixedName()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_FIXED_NAME_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFixedName() <em>Is Fixed Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFixedName()
	 * @generated
	 * @ordered
	 */
	protected boolean isFixedName = IS_FIXED_NAME_EDEFAULT;

	/**
	 * This is true if the Is Fixed Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isFixedNameESet;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The default value of the '{@link #getOriginalFullName() <em>Original Full Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalFullName()
	 * @generated
	 * @ordered
	 */
	protected static final String ORIGINAL_FULL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOriginalFullName() <em>Original Full Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalFullName()
	 * @generated
	 * @ordered
	 */
	protected String originalFullName = ORIGINAL_FULL_NAME_EDEFAULT;

	/**
	 * This is true if the Original Full Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean originalFullNameESet;

	/**
	 * The default value of the '{@link #getOriginalName() <em>Original Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalName()
	 * @generated
	 * @ordered
	 */
	protected static final String ORIGINAL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOriginalName() <em>Original Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalName()
	 * @generated
	 * @ordered
	 */
	protected String originalName = ORIGINAL_NAME_EDEFAULT;

	/**
	 * This is true if the Original Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean originalNameESet;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected GAModelElement parent;

	/**
	 * This is true if the Parent reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean parentESet;

	/**
	 * The cached value of the '{@link #getModel() <em>Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel()
	 * @generated
	 * @ordered
	 */
	protected Model model;

	/**
	 * This is true if the Model reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean modelESet;

	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GAModelElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenericmodelPackage.Literals.GA_MODEL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExternalID() {
		return externalID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalID(String newExternalID) {
		String oldExternalID = externalID;
		externalID = newExternalID;
		boolean oldExternalIDESet = externalIDESet;
		externalIDESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID, oldExternalID, externalID, !oldExternalIDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExternalID() {
		String oldExternalID = externalID;
		boolean oldExternalIDESet = externalIDESet;
		externalID = EXTERNAL_ID_EDEFAULT;
		externalIDESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID, oldExternalID, EXTERNAL_ID_EDEFAULT, oldExternalIDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExternalID() {
		return externalIDESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		boolean oldIdESet = idESet;
		idESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__ID, oldId, id, !oldIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetId() {
		int oldId = id;
		boolean oldIdESet = idESet;
		id = ID_EDEFAULT;
		idESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__ID, oldId, ID_EDEFAULT, oldIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetId() {
		return idESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFixedName() {
		return isFixedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsFixedName(boolean newIsFixedName) {
		boolean oldIsFixedName = isFixedName;
		isFixedName = newIsFixedName;
		boolean oldIsFixedNameESet = isFixedNameESet;
		isFixedNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME, oldIsFixedName, isFixedName, !oldIsFixedNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsFixedName() {
		boolean oldIsFixedName = isFixedName;
		boolean oldIsFixedNameESet = isFixedNameESet;
		isFixedName = IS_FIXED_NAME_EDEFAULT;
		isFixedNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME, oldIsFixedName, IS_FIXED_NAME_EDEFAULT, oldIsFixedNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsFixedName() {
		return isFixedNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		boolean oldNameESet = nameESet;
		nameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__NAME, oldName, name, !oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetName() {
		String oldName = name;
		boolean oldNameESet = nameESet;
		name = NAME_EDEFAULT;
		nameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__NAME, oldName, NAME_EDEFAULT, oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOriginalFullName() {
		return originalFullName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginalFullName(String newOriginalFullName) {
		String oldOriginalFullName = originalFullName;
		originalFullName = newOriginalFullName;
		boolean oldOriginalFullNameESet = originalFullNameESet;
		originalFullNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME, oldOriginalFullName, originalFullName, !oldOriginalFullNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOriginalFullName() {
		String oldOriginalFullName = originalFullName;
		boolean oldOriginalFullNameESet = originalFullNameESet;
		originalFullName = ORIGINAL_FULL_NAME_EDEFAULT;
		originalFullNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME, oldOriginalFullName, ORIGINAL_FULL_NAME_EDEFAULT, oldOriginalFullNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOriginalFullName() {
		return originalFullNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOriginalName() {
		return originalName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginalName(String newOriginalName) {
		String oldOriginalName = originalName;
		originalName = newOriginalName;
		boolean oldOriginalNameESet = originalNameESet;
		originalNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME, oldOriginalName, originalName, !oldOriginalNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOriginalName() {
		String oldOriginalName = originalName;
		boolean oldOriginalNameESet = originalNameESet;
		originalName = ORIGINAL_NAME_EDEFAULT;
		originalNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME, oldOriginalName, ORIGINAL_NAME_EDEFAULT, oldOriginalNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOriginalName() {
		return originalNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GAModelElement getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (GAModelElement)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GenericmodelPackage.GA_MODEL_ELEMENT__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GAModelElement basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(GAModelElement newParent) {
		GAModelElement oldParent = parent;
		parent = newParent;
		boolean oldParentESet = parentESet;
		parentESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__PARENT, oldParent, parent, !oldParentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParent() {
		GAModelElement oldParent = parent;
		boolean oldParentESet = parentESet;
		parent = null;
		parentESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__PARENT, oldParent, null, oldParentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParent() {
		return parentESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model getModel() {
		if (model != null && model.eIsProxy()) {
			InternalEObject oldModel = (InternalEObject)model;
			model = (Model)eResolveProxy(oldModel);
			if (model != oldModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GenericmodelPackage.GA_MODEL_ELEMENT__MODEL, oldModel, model));
			}
		}
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model basicGetModel() {
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel(Model newModel) {
		Model oldModel = model;
		model = newModel;
		boolean oldModelESet = modelESet;
		modelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.GA_MODEL_ELEMENT__MODEL, oldModel, model, !oldModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetModel() {
		Model oldModel = model;
		boolean oldModelESet = modelESet;
		model = null;
		modelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.GA_MODEL_ELEMENT__MODEL, oldModel, null, oldModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetModel() {
		return modelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectContainmentEList.Unsettable<Annotation>(Annotation.class, this, GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAnnotations() {
		if (annotations != null) ((InternalEList.Unsettable<?>)annotations).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAnnotations() {
		return annotations != null && ((InternalEList.Unsettable<?>)annotations).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String Get_infoString() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID:
				return getExternalID();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ID:
				return getId();
			case GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME:
				return isFixedName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__NAME:
				return getName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME:
				return getOriginalFullName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME:
				return getOriginalName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case GenericmodelPackage.GA_MODEL_ELEMENT__MODEL:
				if (resolve) return getModel();
				return basicGetModel();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS:
				return getAnnotations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID:
				setExternalID((String)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ID:
				setId((Integer)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME:
				setIsFixedName((Boolean)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__NAME:
				setName((String)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME:
				setOriginalFullName((String)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME:
				setOriginalName((String)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__PARENT:
				setParent((GAModelElement)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__MODEL:
				setModel((Model)newValue);
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID:
				unsetExternalID();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ID:
				unsetId();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME:
				unsetIsFixedName();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__NAME:
				unsetName();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME:
				unsetOriginalFullName();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME:
				unsetOriginalName();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__PARENT:
				unsetParent();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__MODEL:
				unsetModel();
				return;
			case GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS:
				unsetAnnotations();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID:
				return isSetExternalID();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ID:
				return isSetId();
			case GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME:
				return isSetIsFixedName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__NAME:
				return isSetName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME:
				return isSetOriginalFullName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME:
				return isSetOriginalName();
			case GenericmodelPackage.GA_MODEL_ELEMENT__PARENT:
				return isSetParent();
			case GenericmodelPackage.GA_MODEL_ELEMENT__MODEL:
				return isSetModel();
			case GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS:
				return isSetAnnotations();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (externalID: ");
		if (externalIDESet) result.append(externalID); else result.append("<unset>");
		result.append(", id: ");
		if (idESet) result.append(id); else result.append("<unset>");
		result.append(", isFixedName: ");
		if (isFixedNameESet) result.append(isFixedName); else result.append("<unset>");
		result.append(", name: ");
		if (nameESet) result.append(name); else result.append("<unset>");
		result.append(", originalFullName: ");
		if (originalFullNameESet) result.append(originalFullName); else result.append("<unset>");
		result.append(", originalName: ");
		if (originalNameESet) result.append(originalName); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //GAModelElementImpl
