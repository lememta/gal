/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel.impl;

import geneauto.emf.models.genericmodel.GenericmodelPackage;
import geneauto.emf.models.genericmodel.Model;
import geneauto.emf.models.genericmodel.Transformation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.ModelImpl#getLastId <em>Last Id</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.ModelImpl#getLastSavedBy <em>Last Saved By</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.ModelImpl#getLastSavedOn <em>Last Saved On</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.ModelImpl#getModelName <em>Model Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.ModelImpl#getModelVersion <em>Model Version</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.ModelImpl#isNoNewId <em>No New Id</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.ModelImpl#getTransformations <em>Transformations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ModelImpl extends EObjectImpl implements Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getLastId() <em>Last Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastId()
	 * @generated
	 * @ordered
	 */
	protected static final int LAST_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLastId() <em>Last Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastId()
	 * @generated
	 * @ordered
	 */
	protected int lastId = LAST_ID_EDEFAULT;

	/**
	 * This is true if the Last Id attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lastIdESet;

	/**
	 * The default value of the '{@link #getLastSavedBy() <em>Last Saved By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastSavedBy()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_SAVED_BY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastSavedBy() <em>Last Saved By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastSavedBy()
	 * @generated
	 * @ordered
	 */
	protected String lastSavedBy = LAST_SAVED_BY_EDEFAULT;

	/**
	 * This is true if the Last Saved By attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lastSavedByESet;

	/**
	 * The default value of the '{@link #getLastSavedOn() <em>Last Saved On</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastSavedOn()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_SAVED_ON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastSavedOn() <em>Last Saved On</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastSavedOn()
	 * @generated
	 * @ordered
	 */
	protected String lastSavedOn = LAST_SAVED_ON_EDEFAULT;

	/**
	 * This is true if the Last Saved On attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lastSavedOnESet;

	/**
	 * The default value of the '{@link #getModelName() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelName()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelName() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelName()
	 * @generated
	 * @ordered
	 */
	protected String modelName = MODEL_NAME_EDEFAULT;

	/**
	 * This is true if the Model Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean modelNameESet;

	/**
	 * The default value of the '{@link #getModelVersion() <em>Model Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelVersion() <em>Model Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelVersion()
	 * @generated
	 * @ordered
	 */
	protected String modelVersion = MODEL_VERSION_EDEFAULT;

	/**
	 * This is true if the Model Version attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean modelVersionESet;

	/**
	 * The default value of the '{@link #isNoNewId() <em>No New Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNoNewId()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NO_NEW_ID_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNoNewId() <em>No New Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNoNewId()
	 * @generated
	 * @ordered
	 */
	protected boolean noNewId = NO_NEW_ID_EDEFAULT;

	/**
	 * This is true if the No New Id attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean noNewIdESet;

	/**
	 * The cached value of the '{@link #getTransformations() <em>Transformations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransformations()
	 * @generated
	 * @ordered
	 */
	protected EList<Transformation> transformations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenericmodelPackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLastId() {
		return lastId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastId(int newLastId) {
		int oldLastId = lastId;
		lastId = newLastId;
		boolean oldLastIdESet = lastIdESet;
		lastIdESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.MODEL__LAST_ID, oldLastId, lastId, !oldLastIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLastId() {
		int oldLastId = lastId;
		boolean oldLastIdESet = lastIdESet;
		lastId = LAST_ID_EDEFAULT;
		lastIdESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.MODEL__LAST_ID, oldLastId, LAST_ID_EDEFAULT, oldLastIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLastId() {
		return lastIdESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastSavedBy() {
		return lastSavedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastSavedBy(String newLastSavedBy) {
		String oldLastSavedBy = lastSavedBy;
		lastSavedBy = newLastSavedBy;
		boolean oldLastSavedByESet = lastSavedByESet;
		lastSavedByESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.MODEL__LAST_SAVED_BY, oldLastSavedBy, lastSavedBy, !oldLastSavedByESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLastSavedBy() {
		String oldLastSavedBy = lastSavedBy;
		boolean oldLastSavedByESet = lastSavedByESet;
		lastSavedBy = LAST_SAVED_BY_EDEFAULT;
		lastSavedByESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.MODEL__LAST_SAVED_BY, oldLastSavedBy, LAST_SAVED_BY_EDEFAULT, oldLastSavedByESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLastSavedBy() {
		return lastSavedByESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastSavedOn() {
		return lastSavedOn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastSavedOn(String newLastSavedOn) {
		String oldLastSavedOn = lastSavedOn;
		lastSavedOn = newLastSavedOn;
		boolean oldLastSavedOnESet = lastSavedOnESet;
		lastSavedOnESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.MODEL__LAST_SAVED_ON, oldLastSavedOn, lastSavedOn, !oldLastSavedOnESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLastSavedOn() {
		String oldLastSavedOn = lastSavedOn;
		boolean oldLastSavedOnESet = lastSavedOnESet;
		lastSavedOn = LAST_SAVED_ON_EDEFAULT;
		lastSavedOnESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.MODEL__LAST_SAVED_ON, oldLastSavedOn, LAST_SAVED_ON_EDEFAULT, oldLastSavedOnESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLastSavedOn() {
		return lastSavedOnESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelName(String newModelName) {
		String oldModelName = modelName;
		modelName = newModelName;
		boolean oldModelNameESet = modelNameESet;
		modelNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.MODEL__MODEL_NAME, oldModelName, modelName, !oldModelNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetModelName() {
		String oldModelName = modelName;
		boolean oldModelNameESet = modelNameESet;
		modelName = MODEL_NAME_EDEFAULT;
		modelNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.MODEL__MODEL_NAME, oldModelName, MODEL_NAME_EDEFAULT, oldModelNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetModelName() {
		return modelNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelVersion() {
		return modelVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelVersion(String newModelVersion) {
		String oldModelVersion = modelVersion;
		modelVersion = newModelVersion;
		boolean oldModelVersionESet = modelVersionESet;
		modelVersionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.MODEL__MODEL_VERSION, oldModelVersion, modelVersion, !oldModelVersionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetModelVersion() {
		String oldModelVersion = modelVersion;
		boolean oldModelVersionESet = modelVersionESet;
		modelVersion = MODEL_VERSION_EDEFAULT;
		modelVersionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.MODEL__MODEL_VERSION, oldModelVersion, MODEL_VERSION_EDEFAULT, oldModelVersionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetModelVersion() {
		return modelVersionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNoNewId() {
		return noNewId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoNewId(boolean newNoNewId) {
		boolean oldNoNewId = noNewId;
		noNewId = newNoNewId;
		boolean oldNoNewIdESet = noNewIdESet;
		noNewIdESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.MODEL__NO_NEW_ID, oldNoNewId, noNewId, !oldNoNewIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNoNewId() {
		boolean oldNoNewId = noNewId;
		boolean oldNoNewIdESet = noNewIdESet;
		noNewId = NO_NEW_ID_EDEFAULT;
		noNewIdESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.MODEL__NO_NEW_ID, oldNoNewId, NO_NEW_ID_EDEFAULT, oldNoNewIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNoNewId() {
		return noNewIdESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transformation> getTransformations() {
		if (transformations == null) {
			transformations = new EObjectContainmentEList.Unsettable<Transformation>(Transformation.class, this, GenericmodelPackage.MODEL__TRANSFORMATIONS);
		}
		return transformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTransformations() {
		if (transformations != null) ((InternalEList.Unsettable<?>)transformations).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTransformations() {
		return transformations != null && ((InternalEList.Unsettable<?>)transformations).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GenericmodelPackage.MODEL__TRANSFORMATIONS:
				return ((InternalEList<?>)getTransformations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GenericmodelPackage.MODEL__LAST_ID:
				return getLastId();
			case GenericmodelPackage.MODEL__LAST_SAVED_BY:
				return getLastSavedBy();
			case GenericmodelPackage.MODEL__LAST_SAVED_ON:
				return getLastSavedOn();
			case GenericmodelPackage.MODEL__MODEL_NAME:
				return getModelName();
			case GenericmodelPackage.MODEL__MODEL_VERSION:
				return getModelVersion();
			case GenericmodelPackage.MODEL__NO_NEW_ID:
				return isNoNewId();
			case GenericmodelPackage.MODEL__TRANSFORMATIONS:
				return getTransformations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GenericmodelPackage.MODEL__LAST_ID:
				setLastId((Integer)newValue);
				return;
			case GenericmodelPackage.MODEL__LAST_SAVED_BY:
				setLastSavedBy((String)newValue);
				return;
			case GenericmodelPackage.MODEL__LAST_SAVED_ON:
				setLastSavedOn((String)newValue);
				return;
			case GenericmodelPackage.MODEL__MODEL_NAME:
				setModelName((String)newValue);
				return;
			case GenericmodelPackage.MODEL__MODEL_VERSION:
				setModelVersion((String)newValue);
				return;
			case GenericmodelPackage.MODEL__NO_NEW_ID:
				setNoNewId((Boolean)newValue);
				return;
			case GenericmodelPackage.MODEL__TRANSFORMATIONS:
				getTransformations().clear();
				getTransformations().addAll((Collection<? extends Transformation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.MODEL__LAST_ID:
				unsetLastId();
				return;
			case GenericmodelPackage.MODEL__LAST_SAVED_BY:
				unsetLastSavedBy();
				return;
			case GenericmodelPackage.MODEL__LAST_SAVED_ON:
				unsetLastSavedOn();
				return;
			case GenericmodelPackage.MODEL__MODEL_NAME:
				unsetModelName();
				return;
			case GenericmodelPackage.MODEL__MODEL_VERSION:
				unsetModelVersion();
				return;
			case GenericmodelPackage.MODEL__NO_NEW_ID:
				unsetNoNewId();
				return;
			case GenericmodelPackage.MODEL__TRANSFORMATIONS:
				unsetTransformations();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.MODEL__LAST_ID:
				return isSetLastId();
			case GenericmodelPackage.MODEL__LAST_SAVED_BY:
				return isSetLastSavedBy();
			case GenericmodelPackage.MODEL__LAST_SAVED_ON:
				return isSetLastSavedOn();
			case GenericmodelPackage.MODEL__MODEL_NAME:
				return isSetModelName();
			case GenericmodelPackage.MODEL__MODEL_VERSION:
				return isSetModelVersion();
			case GenericmodelPackage.MODEL__NO_NEW_ID:
				return isSetNoNewId();
			case GenericmodelPackage.MODEL__TRANSFORMATIONS:
				return isSetTransformations();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lastId: ");
		if (lastIdESet) result.append(lastId); else result.append("<unset>");
		result.append(", lastSavedBy: ");
		if (lastSavedByESet) result.append(lastSavedBy); else result.append("<unset>");
		result.append(", lastSavedOn: ");
		if (lastSavedOnESet) result.append(lastSavedOn); else result.append("<unset>");
		result.append(", modelName: ");
		if (modelNameESet) result.append(modelName); else result.append("<unset>");
		result.append(", modelVersion: ");
		if (modelVersionESet) result.append(modelVersion); else result.append("<unset>");
		result.append(", noNewId: ");
		if (noNewIdESet) result.append(noNewId); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ModelImpl
