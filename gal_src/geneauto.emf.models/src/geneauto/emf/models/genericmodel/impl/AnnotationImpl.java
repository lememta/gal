/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel.impl;

import geneauto.emf.models.genericmodel.Annotation;
import geneauto.emf.models.genericmodel.GenericmodelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl#isFromInputModel <em>From Input Model</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl#isBlockDescription <em>Is Block Description</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl#getOrdinal <em>Ordinal</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl#getSource <em>Source</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl#isToPrint <em>To Print</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.AnnotationImpl#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AnnotationImpl extends EObjectImpl implements Annotation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #isFromInputModel() <em>From Input Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFromInputModel()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FROM_INPUT_MODEL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFromInputModel() <em>From Input Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFromInputModel()
	 * @generated
	 * @ordered
	 */
	protected boolean fromInputModel = FROM_INPUT_MODEL_EDEFAULT;

	/**
	 * This is true if the From Input Model attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fromInputModelESet;

	/**
	 * The default value of the '{@link #isBlockDescription() <em>Is Block Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBlockDescription()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_BLOCK_DESCRIPTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBlockDescription() <em>Is Block Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBlockDescription()
	 * @generated
	 * @ordered
	 */
	protected boolean isBlockDescription = IS_BLOCK_DESCRIPTION_EDEFAULT;

	/**
	 * This is true if the Is Block Description attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isBlockDescriptionESet;

	/**
	 * The default value of the '{@link #getOrdinal() <em>Ordinal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdinal()
	 * @generated
	 * @ordered
	 */
	protected static final int ORDINAL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOrdinal() <em>Ordinal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdinal()
	 * @generated
	 * @ordered
	 */
	protected int ordinal = ORDINAL_EDEFAULT;

	/**
	 * This is true if the Ordinal attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean ordinalESet;

	/**
	 * The default value of the '{@link #getSource() <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected String source = SOURCE_EDEFAULT;

	/**
	 * This is true if the Source attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sourceESet;

	/**
	 * The default value of the '{@link #isToPrint() <em>To Print</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isToPrint()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TO_PRINT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isToPrint() <em>To Print</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isToPrint()
	 * @generated
	 * @ordered
	 */
	protected boolean toPrint = TO_PRINT_EDEFAULT;

	/**
	 * This is true if the To Print attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean toPrintESet;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * This is true if the Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean valueESet;

	/**
     * If true, the annotation is aimed at being used for verification
     * purposes. It must be printed according to source code type.
     */
    private boolean isVerificationAnnotation;
   
    private boolean isPostAnnotation;
    
    /**
     * If true then the annotations aimed at being printed before the 
     * code element.
     */
    private boolean isPrefixOfStatement;
    
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenericmodelPackage.Literals.ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFromInputModel() {
		return fromInputModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromInputModel(boolean newFromInputModel) {
		boolean oldFromInputModel = fromInputModel;
		fromInputModel = newFromInputModel;
		boolean oldFromInputModelESet = fromInputModelESet;
		fromInputModelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.ANNOTATION__FROM_INPUT_MODEL, oldFromInputModel, fromInputModel, !oldFromInputModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFromInputModel() {
		boolean oldFromInputModel = fromInputModel;
		boolean oldFromInputModelESet = fromInputModelESet;
		fromInputModel = FROM_INPUT_MODEL_EDEFAULT;
		fromInputModelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.ANNOTATION__FROM_INPUT_MODEL, oldFromInputModel, FROM_INPUT_MODEL_EDEFAULT, oldFromInputModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFromInputModel() {
		return fromInputModelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBlockDescription() {
		return isBlockDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsBlockDescription(boolean newIsBlockDescription) {
		boolean oldIsBlockDescription = isBlockDescription;
		isBlockDescription = newIsBlockDescription;
		boolean oldIsBlockDescriptionESet = isBlockDescriptionESet;
		isBlockDescriptionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.ANNOTATION__IS_BLOCK_DESCRIPTION, oldIsBlockDescription, isBlockDescription, !oldIsBlockDescriptionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsBlockDescription() {
		boolean oldIsBlockDescription = isBlockDescription;
		boolean oldIsBlockDescriptionESet = isBlockDescriptionESet;
		isBlockDescription = IS_BLOCK_DESCRIPTION_EDEFAULT;
		isBlockDescriptionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.ANNOTATION__IS_BLOCK_DESCRIPTION, oldIsBlockDescription, IS_BLOCK_DESCRIPTION_EDEFAULT, oldIsBlockDescriptionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsBlockDescription() {
		return isBlockDescriptionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOrdinal() {
		return ordinal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinal(int newOrdinal) {
		int oldOrdinal = ordinal;
		ordinal = newOrdinal;
		boolean oldOrdinalESet = ordinalESet;
		ordinalESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.ANNOTATION__ORDINAL, oldOrdinal, ordinal, !oldOrdinalESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOrdinal() {
		int oldOrdinal = ordinal;
		boolean oldOrdinalESet = ordinalESet;
		ordinal = ORDINAL_EDEFAULT;
		ordinalESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.ANNOTATION__ORDINAL, oldOrdinal, ORDINAL_EDEFAULT, oldOrdinalESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOrdinal() {
		return ordinalESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(String newSource) {
		String oldSource = source;
		source = newSource;
		boolean oldSourceESet = sourceESet;
		sourceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.ANNOTATION__SOURCE, oldSource, source, !oldSourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSource() {
		String oldSource = source;
		boolean oldSourceESet = sourceESet;
		source = SOURCE_EDEFAULT;
		sourceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.ANNOTATION__SOURCE, oldSource, SOURCE_EDEFAULT, oldSourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSource() {
		return sourceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isToPrint() {
		return toPrint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToPrint(boolean newToPrint) {
		boolean oldToPrint = toPrint;
		toPrint = newToPrint;
		boolean oldToPrintESet = toPrintESet;
		toPrintESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.ANNOTATION__TO_PRINT, oldToPrint, toPrint, !oldToPrintESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetToPrint() {
		boolean oldToPrint = toPrint;
		boolean oldToPrintESet = toPrintESet;
		toPrint = TO_PRINT_EDEFAULT;
		toPrintESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.ANNOTATION__TO_PRINT, oldToPrint, TO_PRINT_EDEFAULT, oldToPrintESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetToPrint() {
		return toPrintESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		boolean oldValueESet = valueESet;
		valueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.ANNOTATION__VALUE, oldValue, value, !oldValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetValue() {
		String oldValue = value;
		boolean oldValueESet = valueESet;
		value = VALUE_EDEFAULT;
		valueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.ANNOTATION__VALUE, oldValue, VALUE_EDEFAULT, oldValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetValue() {
		return valueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GenericmodelPackage.ANNOTATION__FROM_INPUT_MODEL:
				return isFromInputModel();
			case GenericmodelPackage.ANNOTATION__IS_BLOCK_DESCRIPTION:
				return isBlockDescription();
			case GenericmodelPackage.ANNOTATION__ORDINAL:
				return getOrdinal();
			case GenericmodelPackage.ANNOTATION__SOURCE:
				return getSource();
			case GenericmodelPackage.ANNOTATION__TO_PRINT:
				return isToPrint();
			case GenericmodelPackage.ANNOTATION__VALUE:
				return getValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GenericmodelPackage.ANNOTATION__FROM_INPUT_MODEL:
				setFromInputModel((Boolean)newValue);
				return;
			case GenericmodelPackage.ANNOTATION__IS_BLOCK_DESCRIPTION:
				setIsBlockDescription((Boolean)newValue);
				return;
			case GenericmodelPackage.ANNOTATION__ORDINAL:
				setOrdinal((Integer)newValue);
				return;
			case GenericmodelPackage.ANNOTATION__SOURCE:
				setSource((String)newValue);
				return;
			case GenericmodelPackage.ANNOTATION__TO_PRINT:
				setToPrint((Boolean)newValue);
				return;
			case GenericmodelPackage.ANNOTATION__VALUE:
				setValue((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.ANNOTATION__FROM_INPUT_MODEL:
				unsetFromInputModel();
				return;
			case GenericmodelPackage.ANNOTATION__IS_BLOCK_DESCRIPTION:
				unsetIsBlockDescription();
				return;
			case GenericmodelPackage.ANNOTATION__ORDINAL:
				unsetOrdinal();
				return;
			case GenericmodelPackage.ANNOTATION__SOURCE:
				unsetSource();
				return;
			case GenericmodelPackage.ANNOTATION__TO_PRINT:
				unsetToPrint();
				return;
			case GenericmodelPackage.ANNOTATION__VALUE:
				unsetValue();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.ANNOTATION__FROM_INPUT_MODEL:
				return isSetFromInputModel();
			case GenericmodelPackage.ANNOTATION__IS_BLOCK_DESCRIPTION:
				return isSetIsBlockDescription();
			case GenericmodelPackage.ANNOTATION__ORDINAL:
				return isSetOrdinal();
			case GenericmodelPackage.ANNOTATION__SOURCE:
				return isSetSource();
			case GenericmodelPackage.ANNOTATION__TO_PRINT:
				return isSetToPrint();
			case GenericmodelPackage.ANNOTATION__VALUE:
				return isSetValue();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fromInputModel: ");
		if (fromInputModelESet) result.append(fromInputModel); else result.append("<unset>");
		result.append(", isBlockDescription: ");
		if (isBlockDescriptionESet) result.append(isBlockDescription); else result.append("<unset>");
		result.append(", ordinal: ");
		if (ordinalESet) result.append(ordinal); else result.append("<unset>");
		result.append(", source: ");
		if (sourceESet) result.append(source); else result.append("<unset>");
		result.append(", toPrint: ");
		if (toPrintESet) result.append(toPrint); else result.append("<unset>");
		result.append(", value: ");
		if (valueESet) result.append(value); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	public boolean isVerificationAnnotation() {
		return isVerificationAnnotation;
	}

	public void setVerificationAnnotation(boolean isVerificationAnnotation) {
		this.isVerificationAnnotation = isVerificationAnnotation;
	}

	public boolean isPostAnnotation() {
		return isPostAnnotation;
	}

	public void setPostAnnotation(boolean isPostAnnotation) {
		this.isPostAnnotation = isPostAnnotation;
	}

	public boolean isPrefixOfStatement() {
		return isPrefixOfStatement;
	}

	public void setPrefixOfStatement(boolean isPrefixOfStatement) {
		this.isPrefixOfStatement = isPrefixOfStatement;
	}

} //AnnotationImpl
