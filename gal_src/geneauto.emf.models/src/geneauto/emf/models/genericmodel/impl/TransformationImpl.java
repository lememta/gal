/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel.impl;

import geneauto.emf.models.genericmodel.GenericmodelPackage;
import geneauto.emf.models.genericmodel.Transformation;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transformation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.TransformationImpl#getReadTime <em>Read Time</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.TransformationImpl#getToolName <em>Tool Name</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.impl.TransformationImpl#getWriteTime <em>Write Time</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TransformationImpl extends EObjectImpl implements Transformation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getReadTime() <em>Read Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReadTime()
	 * @generated
	 * @ordered
	 */
	protected static final String READ_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReadTime() <em>Read Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReadTime()
	 * @generated
	 * @ordered
	 */
	protected String readTime = READ_TIME_EDEFAULT;

	/**
	 * This is true if the Read Time attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean readTimeESet;

	/**
	 * The default value of the '{@link #getToolName() <em>Tool Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToolName()
	 * @generated
	 * @ordered
	 */
	protected static final String TOOL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToolName() <em>Tool Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToolName()
	 * @generated
	 * @ordered
	 */
	protected String toolName = TOOL_NAME_EDEFAULT;

	/**
	 * This is true if the Tool Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean toolNameESet;

	/**
	 * The default value of the '{@link #getWriteTime() <em>Write Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWriteTime()
	 * @generated
	 * @ordered
	 */
	protected static final String WRITE_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWriteTime() <em>Write Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWriteTime()
	 * @generated
	 * @ordered
	 */
	protected String writeTime = WRITE_TIME_EDEFAULT;

	/**
	 * This is true if the Write Time attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean writeTimeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenericmodelPackage.Literals.TRANSFORMATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReadTime() {
		return readTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReadTime(String newReadTime) {
		String oldReadTime = readTime;
		readTime = newReadTime;
		boolean oldReadTimeESet = readTimeESet;
		readTimeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.TRANSFORMATION__READ_TIME, oldReadTime, readTime, !oldReadTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetReadTime() {
		String oldReadTime = readTime;
		boolean oldReadTimeESet = readTimeESet;
		readTime = READ_TIME_EDEFAULT;
		readTimeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.TRANSFORMATION__READ_TIME, oldReadTime, READ_TIME_EDEFAULT, oldReadTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetReadTime() {
		return readTimeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToolName() {
		return toolName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToolName(String newToolName) {
		String oldToolName = toolName;
		toolName = newToolName;
		boolean oldToolNameESet = toolNameESet;
		toolNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.TRANSFORMATION__TOOL_NAME, oldToolName, toolName, !oldToolNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetToolName() {
		String oldToolName = toolName;
		boolean oldToolNameESet = toolNameESet;
		toolName = TOOL_NAME_EDEFAULT;
		toolNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.TRANSFORMATION__TOOL_NAME, oldToolName, TOOL_NAME_EDEFAULT, oldToolNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetToolName() {
		return toolNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getWriteTime() {
		return writeTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWriteTime(String newWriteTime) {
		String oldWriteTime = writeTime;
		writeTime = newWriteTime;
		boolean oldWriteTimeESet = writeTimeESet;
		writeTimeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericmodelPackage.TRANSFORMATION__WRITE_TIME, oldWriteTime, writeTime, !oldWriteTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetWriteTime() {
		String oldWriteTime = writeTime;
		boolean oldWriteTimeESet = writeTimeESet;
		writeTime = WRITE_TIME_EDEFAULT;
		writeTimeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GenericmodelPackage.TRANSFORMATION__WRITE_TIME, oldWriteTime, WRITE_TIME_EDEFAULT, oldWriteTimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetWriteTime() {
		return writeTimeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GenericmodelPackage.TRANSFORMATION__READ_TIME:
				return getReadTime();
			case GenericmodelPackage.TRANSFORMATION__TOOL_NAME:
				return getToolName();
			case GenericmodelPackage.TRANSFORMATION__WRITE_TIME:
				return getWriteTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GenericmodelPackage.TRANSFORMATION__READ_TIME:
				setReadTime((String)newValue);
				return;
			case GenericmodelPackage.TRANSFORMATION__TOOL_NAME:
				setToolName((String)newValue);
				return;
			case GenericmodelPackage.TRANSFORMATION__WRITE_TIME:
				setWriteTime((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.TRANSFORMATION__READ_TIME:
				unsetReadTime();
				return;
			case GenericmodelPackage.TRANSFORMATION__TOOL_NAME:
				unsetToolName();
				return;
			case GenericmodelPackage.TRANSFORMATION__WRITE_TIME:
				unsetWriteTime();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GenericmodelPackage.TRANSFORMATION__READ_TIME:
				return isSetReadTime();
			case GenericmodelPackage.TRANSFORMATION__TOOL_NAME:
				return isSetToolName();
			case GenericmodelPackage.TRANSFORMATION__WRITE_TIME:
				return isSetWriteTime();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (readTime: ");
		if (readTimeESet) result.append(readTime); else result.append("<unset>");
		result.append(", toolName: ");
		if (toolNameESet) result.append(toolName); else result.append("<unset>");
		result.append(", writeTime: ");
		if (writeTimeESet) result.append(writeTime); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //TransformationImpl
