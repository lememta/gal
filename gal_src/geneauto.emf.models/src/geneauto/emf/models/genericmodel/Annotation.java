/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.genericmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Annotation is an arbitrary text that can be attached to language element (any GAModelElement). If attribute toPrint is turue, the annotation is printed to generated code as comment. Otherwise it simply provides additinoal information about the model element, but is ignored during the final transformation to source code
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.genericmodel.Annotation#isFromInputModel <em>From Input Model</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Annotation#isBlockDescription <em>Is Block Description</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Annotation#getOrdinal <em>Ordinal</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Annotation#getSource <em>Source</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Annotation#isToPrint <em>To Print</em>}</li>
 *   <li>{@link geneauto.emf.models.genericmodel.Annotation#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getAnnotation()
 * @model
 * @generated
 */
public interface Annotation extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>From Input Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Input Model</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Input Model</em>' attribute.
	 * @see #isSetFromInputModel()
	 * @see #unsetFromInputModel()
	 * @see #setFromInputModel(boolean)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getAnnotation_FromInputModel()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isFromInputModel();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isFromInputModel <em>From Input Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Input Model</em>' attribute.
	 * @see #isSetFromInputModel()
	 * @see #unsetFromInputModel()
	 * @see #isFromInputModel()
	 * @generated
	 */
	void setFromInputModel(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isFromInputModel <em>From Input Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFromInputModel()
	 * @see #isFromInputModel()
	 * @see #setFromInputModel(boolean)
	 * @generated
	 */
	void unsetFromInputModel();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isFromInputModel <em>From Input Model</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>From Input Model</em>' attribute is set.
	 * @see #unsetFromInputModel()
	 * @see #isFromInputModel()
	 * @see #setFromInputModel(boolean)
	 * @generated
	 */
	boolean isSetFromInputModel();

	/**
	 * Returns the value of the '<em><b>Is Block Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Block Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Block Description</em>' attribute.
	 * @see #isSetIsBlockDescription()
	 * @see #unsetIsBlockDescription()
	 * @see #setIsBlockDescription(boolean)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getAnnotation_IsBlockDescription()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isBlockDescription();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isBlockDescription <em>Is Block Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Block Description</em>' attribute.
	 * @see #isSetIsBlockDescription()
	 * @see #unsetIsBlockDescription()
	 * @see #isBlockDescription()
	 * @generated
	 */
	void setIsBlockDescription(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isBlockDescription <em>Is Block Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsBlockDescription()
	 * @see #isBlockDescription()
	 * @see #setIsBlockDescription(boolean)
	 * @generated
	 */
	void unsetIsBlockDescription();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isBlockDescription <em>Is Block Description</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Block Description</em>' attribute is set.
	 * @see #unsetIsBlockDescription()
	 * @see #isBlockDescription()
	 * @see #setIsBlockDescription(boolean)
	 * @generated
	 */
	boolean isSetIsBlockDescription();

	/**
	 * Returns the value of the '<em><b>Ordinal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinal</em>' attribute.
	 * @see #isSetOrdinal()
	 * @see #unsetOrdinal()
	 * @see #setOrdinal(int)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getAnnotation_Ordinal()
	 * @model unsettable="true"
	 * @generated
	 */
	int getOrdinal();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getOrdinal <em>Ordinal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinal</em>' attribute.
	 * @see #isSetOrdinal()
	 * @see #unsetOrdinal()
	 * @see #getOrdinal()
	 * @generated
	 */
	void setOrdinal(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getOrdinal <em>Ordinal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOrdinal()
	 * @see #getOrdinal()
	 * @see #setOrdinal(int)
	 * @generated
	 */
	void unsetOrdinal();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getOrdinal <em>Ordinal</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Ordinal</em>' attribute is set.
	 * @see #unsetOrdinal()
	 * @see #getOrdinal()
	 * @see #setOrdinal(int)
	 * @generated
	 */
	boolean isSetOrdinal();

	/**
	 * Returns the value of the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' attribute.
	 * @see #isSetSource()
	 * @see #unsetSource()
	 * @see #setSource(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getAnnotation_Source()
	 * @model unsettable="true"
	 * @generated
	 */
	String getSource();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getSource <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' attribute.
	 * @see #isSetSource()
	 * @see #unsetSource()
	 * @see #getSource()
	 * @generated
	 */
	void setSource(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getSource <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSource()
	 * @see #getSource()
	 * @see #setSource(String)
	 * @generated
	 */
	void unsetSource();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getSource <em>Source</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Source</em>' attribute is set.
	 * @see #unsetSource()
	 * @see #getSource()
	 * @see #setSource(String)
	 * @generated
	 */
	boolean isSetSource();

	/**
	 * Returns the value of the '<em><b>To Print</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Print</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Print</em>' attribute.
	 * @see #isSetToPrint()
	 * @see #unsetToPrint()
	 * @see #setToPrint(boolean)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getAnnotation_ToPrint()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isToPrint();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isToPrint <em>To Print</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Print</em>' attribute.
	 * @see #isSetToPrint()
	 * @see #unsetToPrint()
	 * @see #isToPrint()
	 * @generated
	 */
	void setToPrint(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isToPrint <em>To Print</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetToPrint()
	 * @see #isToPrint()
	 * @see #setToPrint(boolean)
	 * @generated
	 */
	void unsetToPrint();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Annotation#isToPrint <em>To Print</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>To Print</em>' attribute is set.
	 * @see #unsetToPrint()
	 * @see #isToPrint()
	 * @see #setToPrint(boolean)
	 * @generated
	 */
	boolean isSetToPrint();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(String)
	 * @see geneauto.emf.models.genericmodel.GenericmodelPackage#getAnnotation_Value()
	 * @model unsettable="true"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.genericmodel.Annotation#getValue <em>Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' attribute is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	boolean isSetValue();

} // Annotation
