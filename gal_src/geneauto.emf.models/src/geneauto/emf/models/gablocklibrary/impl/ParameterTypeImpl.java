/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gablocklibrary.impl;

import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;
import geneauto.emf.models.gablocklibrary.InputLanguageMapping;
import geneauto.emf.models.gablocklibrary.ParameterType;

import geneauto.emf.models.genericmodel.impl.GAModelElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl#getEditorMapping <em>Editor Mapping</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl#isEvaluateEML <em>Evaluate EML</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl#isFullNormalization <em>Full Normalization</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl#isParseEML <em>Parse EML</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl#isSupportSpecialReferences <em>Support Special References</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl#getInputLanguageMappings <em>Input Language Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ParameterTypeImpl extends GAModelElementImpl implements ParameterType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #getEditorMapping() <em>Editor Mapping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorMapping()
	 * @generated
	 * @ordered
	 */
	protected static final String EDITOR_MAPPING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEditorMapping() <em>Editor Mapping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorMapping()
	 * @generated
	 * @ordered
	 */
	protected String editorMapping = EDITOR_MAPPING_EDEFAULT;

	/**
	 * This is true if the Editor Mapping attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean editorMappingESet;

	/**
	 * The default value of the '{@link #isEvaluateEML() <em>Evaluate EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEvaluateEML()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EVALUATE_EML_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEvaluateEML() <em>Evaluate EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEvaluateEML()
	 * @generated
	 * @ordered
	 */
	protected boolean evaluateEML = EVALUATE_EML_EDEFAULT;

	/**
	 * This is true if the Evaluate EML attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean evaluateEMLESet;

	/**
	 * The default value of the '{@link #isFullNormalization() <em>Full Normalization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFullNormalization()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FULL_NORMALIZATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFullNormalization() <em>Full Normalization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFullNormalization()
	 * @generated
	 * @ordered
	 */
	protected boolean fullNormalization = FULL_NORMALIZATION_EDEFAULT;

	/**
	 * This is true if the Full Normalization attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fullNormalizationESet;

	/**
	 * The default value of the '{@link #isParseEML() <em>Parse EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParseEML()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARSE_EML_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isParseEML() <em>Parse EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParseEML()
	 * @generated
	 * @ordered
	 */
	protected boolean parseEML = PARSE_EML_EDEFAULT;

	/**
	 * This is true if the Parse EML attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean parseEMLESet;

	/**
	 * The default value of the '{@link #isSupportSpecialReferences() <em>Support Special References</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSupportSpecialReferences()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SUPPORT_SPECIAL_REFERENCES_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSupportSpecialReferences() <em>Support Special References</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSupportSpecialReferences()
	 * @generated
	 * @ordered
	 */
	protected boolean supportSpecialReferences = SUPPORT_SPECIAL_REFERENCES_EDEFAULT;

	/**
	 * This is true if the Support Special References attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean supportSpecialReferencesESet;

	/**
	 * The cached value of the '{@link #getInputLanguageMappings() <em>Input Language Mappings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputLanguageMappings()
	 * @generated
	 * @ordered
	 */
	protected EList<InputLanguageMapping> inputLanguageMappings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GablocklibraryPackage.Literals.PARAMETER_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEditorMapping() {
		return editorMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditorMapping(String newEditorMapping) {
		String oldEditorMapping = editorMapping;
		editorMapping = newEditorMapping;
		boolean oldEditorMappingESet = editorMappingESet;
		editorMappingESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.PARAMETER_TYPE__EDITOR_MAPPING, oldEditorMapping, editorMapping, !oldEditorMappingESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEditorMapping() {
		String oldEditorMapping = editorMapping;
		boolean oldEditorMappingESet = editorMappingESet;
		editorMapping = EDITOR_MAPPING_EDEFAULT;
		editorMappingESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.PARAMETER_TYPE__EDITOR_MAPPING, oldEditorMapping, EDITOR_MAPPING_EDEFAULT, oldEditorMappingESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEditorMapping() {
		return editorMappingESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEvaluateEML() {
		return evaluateEML;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluateEML(boolean newEvaluateEML) {
		boolean oldEvaluateEML = evaluateEML;
		evaluateEML = newEvaluateEML;
		boolean oldEvaluateEMLESet = evaluateEMLESet;
		evaluateEMLESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.PARAMETER_TYPE__EVALUATE_EML, oldEvaluateEML, evaluateEML, !oldEvaluateEMLESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEvaluateEML() {
		boolean oldEvaluateEML = evaluateEML;
		boolean oldEvaluateEMLESet = evaluateEMLESet;
		evaluateEML = EVALUATE_EML_EDEFAULT;
		evaluateEMLESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.PARAMETER_TYPE__EVALUATE_EML, oldEvaluateEML, EVALUATE_EML_EDEFAULT, oldEvaluateEMLESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEvaluateEML() {
		return evaluateEMLESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFullNormalization() {
		return fullNormalization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFullNormalization(boolean newFullNormalization) {
		boolean oldFullNormalization = fullNormalization;
		fullNormalization = newFullNormalization;
		boolean oldFullNormalizationESet = fullNormalizationESet;
		fullNormalizationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.PARAMETER_TYPE__FULL_NORMALIZATION, oldFullNormalization, fullNormalization, !oldFullNormalizationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFullNormalization() {
		boolean oldFullNormalization = fullNormalization;
		boolean oldFullNormalizationESet = fullNormalizationESet;
		fullNormalization = FULL_NORMALIZATION_EDEFAULT;
		fullNormalizationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.PARAMETER_TYPE__FULL_NORMALIZATION, oldFullNormalization, FULL_NORMALIZATION_EDEFAULT, oldFullNormalizationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFullNormalization() {
		return fullNormalizationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isParseEML() {
		return parseEML;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParseEML(boolean newParseEML) {
		boolean oldParseEML = parseEML;
		parseEML = newParseEML;
		boolean oldParseEMLESet = parseEMLESet;
		parseEMLESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.PARAMETER_TYPE__PARSE_EML, oldParseEML, parseEML, !oldParseEMLESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParseEML() {
		boolean oldParseEML = parseEML;
		boolean oldParseEMLESet = parseEMLESet;
		parseEML = PARSE_EML_EDEFAULT;
		parseEMLESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.PARAMETER_TYPE__PARSE_EML, oldParseEML, PARSE_EML_EDEFAULT, oldParseEMLESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParseEML() {
		return parseEMLESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSupportSpecialReferences() {
		return supportSpecialReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSupportSpecialReferences(boolean newSupportSpecialReferences) {
		boolean oldSupportSpecialReferences = supportSpecialReferences;
		supportSpecialReferences = newSupportSpecialReferences;
		boolean oldSupportSpecialReferencesESet = supportSpecialReferencesESet;
		supportSpecialReferencesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES, oldSupportSpecialReferences, supportSpecialReferences, !oldSupportSpecialReferencesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSupportSpecialReferences() {
		boolean oldSupportSpecialReferences = supportSpecialReferences;
		boolean oldSupportSpecialReferencesESet = supportSpecialReferencesESet;
		supportSpecialReferences = SUPPORT_SPECIAL_REFERENCES_EDEFAULT;
		supportSpecialReferencesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES, oldSupportSpecialReferences, SUPPORT_SPECIAL_REFERENCES_EDEFAULT, oldSupportSpecialReferencesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSupportSpecialReferences() {
		return supportSpecialReferencesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InputLanguageMapping> getInputLanguageMappings() {
		if (inputLanguageMappings == null) {
			inputLanguageMappings = new EObjectContainmentEList.Unsettable<InputLanguageMapping>(InputLanguageMapping.class, this, GablocklibraryPackage.PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS);
		}
		return inputLanguageMappings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInputLanguageMappings() {
		if (inputLanguageMappings != null) ((InternalEList.Unsettable<?>)inputLanguageMappings).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInputLanguageMappings() {
		return inputLanguageMappings != null && ((InternalEList.Unsettable<?>)inputLanguageMappings).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GablocklibraryPackage.PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS:
				return ((InternalEList<?>)getInputLanguageMappings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GablocklibraryPackage.PARAMETER_TYPE__EDITOR_MAPPING:
				return getEditorMapping();
			case GablocklibraryPackage.PARAMETER_TYPE__EVALUATE_EML:
				return isEvaluateEML();
			case GablocklibraryPackage.PARAMETER_TYPE__FULL_NORMALIZATION:
				return isFullNormalization();
			case GablocklibraryPackage.PARAMETER_TYPE__PARSE_EML:
				return isParseEML();
			case GablocklibraryPackage.PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES:
				return isSupportSpecialReferences();
			case GablocklibraryPackage.PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS:
				return getInputLanguageMappings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GablocklibraryPackage.PARAMETER_TYPE__EDITOR_MAPPING:
				setEditorMapping((String)newValue);
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__EVALUATE_EML:
				setEvaluateEML((Boolean)newValue);
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__FULL_NORMALIZATION:
				setFullNormalization((Boolean)newValue);
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__PARSE_EML:
				setParseEML((Boolean)newValue);
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES:
				setSupportSpecialReferences((Boolean)newValue);
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS:
				getInputLanguageMappings().clear();
				getInputLanguageMappings().addAll((Collection<? extends InputLanguageMapping>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GablocklibraryPackage.PARAMETER_TYPE__EDITOR_MAPPING:
				unsetEditorMapping();
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__EVALUATE_EML:
				unsetEvaluateEML();
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__FULL_NORMALIZATION:
				unsetFullNormalization();
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__PARSE_EML:
				unsetParseEML();
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES:
				unsetSupportSpecialReferences();
				return;
			case GablocklibraryPackage.PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS:
				unsetInputLanguageMappings();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GablocklibraryPackage.PARAMETER_TYPE__EDITOR_MAPPING:
				return isSetEditorMapping();
			case GablocklibraryPackage.PARAMETER_TYPE__EVALUATE_EML:
				return isSetEvaluateEML();
			case GablocklibraryPackage.PARAMETER_TYPE__FULL_NORMALIZATION:
				return isSetFullNormalization();
			case GablocklibraryPackage.PARAMETER_TYPE__PARSE_EML:
				return isSetParseEML();
			case GablocklibraryPackage.PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES:
				return isSetSupportSpecialReferences();
			case GablocklibraryPackage.PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS:
				return isSetInputLanguageMappings();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (editorMapping: ");
		if (editorMappingESet) result.append(editorMapping); else result.append("<unset>");
		result.append(", evaluateEML: ");
		if (evaluateEMLESet) result.append(evaluateEML); else result.append("<unset>");
		result.append(", fullNormalization: ");
		if (fullNormalizationESet) result.append(fullNormalization); else result.append("<unset>");
		result.append(", parseEML: ");
		if (parseEMLESet) result.append(parseEML); else result.append("<unset>");
		result.append(", supportSpecialReferences: ");
		if (supportSpecialReferencesESet) result.append(supportSpecialReferences); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ParameterTypeImpl
