/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gablocklibrary.impl;

import geneauto.emf.models.gablocklibrary.BlockBackend;
import geneauto.emf.models.gablocklibrary.BlockTyper;
import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;
import geneauto.emf.models.gablocklibrary.LibraryHandler;

import geneauto.emf.models.genericmodel.impl.GAModelElementImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Library Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.LibraryHandlerImpl#getBackend <em>Backend</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.LibraryHandlerImpl#getTyper <em>Typer</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LibraryHandlerImpl extends GAModelElementImpl implements LibraryHandler {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The cached value of the '{@link #getBackend() <em>Backend</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackend()
	 * @generated
	 * @ordered
	 */
	protected BlockBackend backend;

	/**
	 * This is true if the Backend reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean backendESet;

	/**
	 * The cached value of the '{@link #getTyper() <em>Typer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTyper()
	 * @generated
	 * @ordered
	 */
	protected BlockTyper typer;

	/**
	 * This is true if the Typer reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typerESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LibraryHandlerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GablocklibraryPackage.Literals.LIBRARY_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockBackend getBackend() {
		if (backend != null && backend.eIsProxy()) {
			InternalEObject oldBackend = (InternalEObject)backend;
			backend = (BlockBackend)eResolveProxy(oldBackend);
			if (backend != oldBackend) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GablocklibraryPackage.LIBRARY_HANDLER__BACKEND, oldBackend, backend));
			}
		}
		return backend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockBackend basicGetBackend() {
		return backend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBackend(BlockBackend newBackend) {
		BlockBackend oldBackend = backend;
		backend = newBackend;
		boolean oldBackendESet = backendESet;
		backendESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.LIBRARY_HANDLER__BACKEND, oldBackend, backend, !oldBackendESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBackend() {
		BlockBackend oldBackend = backend;
		boolean oldBackendESet = backendESet;
		backend = null;
		backendESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.LIBRARY_HANDLER__BACKEND, oldBackend, null, oldBackendESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBackend() {
		return backendESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockTyper getTyper() {
		if (typer != null && typer.eIsProxy()) {
			InternalEObject oldTyper = (InternalEObject)typer;
			typer = (BlockTyper)eResolveProxy(oldTyper);
			if (typer != oldTyper) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GablocklibraryPackage.LIBRARY_HANDLER__TYPER, oldTyper, typer));
			}
		}
		return typer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockTyper basicGetTyper() {
		return typer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTyper(BlockTyper newTyper) {
		BlockTyper oldTyper = typer;
		typer = newTyper;
		boolean oldTyperESet = typerESet;
		typerESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.LIBRARY_HANDLER__TYPER, oldTyper, typer, !oldTyperESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTyper() {
		BlockTyper oldTyper = typer;
		boolean oldTyperESet = typerESet;
		typer = null;
		typerESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.LIBRARY_HANDLER__TYPER, oldTyper, null, oldTyperESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTyper() {
		return typerESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GablocklibraryPackage.LIBRARY_HANDLER__BACKEND:
				if (resolve) return getBackend();
				return basicGetBackend();
			case GablocklibraryPackage.LIBRARY_HANDLER__TYPER:
				if (resolve) return getTyper();
				return basicGetTyper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GablocklibraryPackage.LIBRARY_HANDLER__BACKEND:
				setBackend((BlockBackend)newValue);
				return;
			case GablocklibraryPackage.LIBRARY_HANDLER__TYPER:
				setTyper((BlockTyper)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GablocklibraryPackage.LIBRARY_HANDLER__BACKEND:
				unsetBackend();
				return;
			case GablocklibraryPackage.LIBRARY_HANDLER__TYPER:
				unsetTyper();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GablocklibraryPackage.LIBRARY_HANDLER__BACKEND:
				return isSetBackend();
			case GablocklibraryPackage.LIBRARY_HANDLER__TYPER:
				return isSetTyper();
		}
		return super.eIsSet(featureID);
	}

} //LibraryHandlerImpl
