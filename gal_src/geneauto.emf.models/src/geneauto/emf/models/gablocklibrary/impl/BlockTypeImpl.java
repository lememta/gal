/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gablocklibrary.impl;

import geneauto.emf.models.gablocklibrary.BlockClass;
import geneauto.emf.models.gablocklibrary.BlockType;
import geneauto.emf.models.gablocklibrary.GablocklibraryPackage;
import geneauto.emf.models.gablocklibrary.LibraryHandler;
import geneauto.emf.models.gablocklibrary.ParameterType;

import geneauto.emf.models.genericmodel.impl.GAModelElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#isDirectFeedThrough <em>Direct Feed Through</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#getFunctionSignature <em>Function Signature</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#getHeaderFileName <em>Header File Name</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#getPreprocessorPriority <em>Preprocessor Priority</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#isSingleInstruction <em>Single Instruction</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#getLibraryHandler <em>Library Handler</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#getBlockClass <em>Block Class</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl#getParameterTypes <em>Parameter Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BlockTypeImpl extends GAModelElementImpl implements BlockType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The default value of the '{@link #isDirectFeedThrough() <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirectFeedThrough()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DIRECT_FEED_THROUGH_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDirectFeedThrough() <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirectFeedThrough()
	 * @generated
	 * @ordered
	 */
	protected boolean directFeedThrough = DIRECT_FEED_THROUGH_EDEFAULT;

	/**
	 * This is true if the Direct Feed Through attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean directFeedThroughESet;

	/**
	 * The default value of the '{@link #getFunctionSignature() <em>Function Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionSignature()
	 * @generated
	 * @ordered
	 */
	protected static final String FUNCTION_SIGNATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFunctionSignature() <em>Function Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionSignature()
	 * @generated
	 * @ordered
	 */
	protected String functionSignature = FUNCTION_SIGNATURE_EDEFAULT;

	/**
	 * This is true if the Function Signature attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean functionSignatureESet;

	/**
	 * The default value of the '{@link #getHeaderFileName() <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String HEADER_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeaderFileName() <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFileName()
	 * @generated
	 * @ordered
	 */
	protected String headerFileName = HEADER_FILE_NAME_EDEFAULT;

	/**
	 * This is true if the Header File Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean headerFileNameESet;

	/**
	 * The default value of the '{@link #getPreprocessorPriority() <em>Preprocessor Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreprocessorPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int PREPROCESSOR_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPreprocessorPriority() <em>Preprocessor Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreprocessorPriority()
	 * @generated
	 * @ordered
	 */
	protected int preprocessorPriority = PREPROCESSOR_PRIORITY_EDEFAULT;

	/**
	 * This is true if the Preprocessor Priority attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean preprocessorPriorityESet;

	/**
	 * The default value of the '{@link #isSingleInstruction() <em>Single Instruction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSingleInstruction()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SINGLE_INSTRUCTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSingleInstruction() <em>Single Instruction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSingleInstruction()
	 * @generated
	 * @ordered
	 */
	protected boolean singleInstruction = SINGLE_INSTRUCTION_EDEFAULT;

	/**
	 * This is true if the Single Instruction attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean singleInstructionESet;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * This is true if the Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * The cached value of the '{@link #getLibraryHandler() <em>Library Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLibraryHandler()
	 * @generated
	 * @ordered
	 */
	protected LibraryHandler libraryHandler;

	/**
	 * This is true if the Library Handler reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean libraryHandlerESet;

	/**
	 * The default value of the '{@link #getBlockClass() <em>Block Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockClass()
	 * @generated
	 * @ordered
	 */
	protected static final BlockClass BLOCK_CLASS_EDEFAULT = BlockClass.SOURCE;

	/**
	 * The cached value of the '{@link #getBlockClass() <em>Block Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockClass()
	 * @generated
	 * @ordered
	 */
	protected BlockClass blockClass = BLOCK_CLASS_EDEFAULT;

	/**
	 * This is true if the Block Class attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean blockClassESet;

	/**
	 * The cached value of the '{@link #getParameterTypes() <em>Parameter Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterType> parameterTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GablocklibraryPackage.Literals.BLOCK_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDirectFeedThrough() {
		return directFeedThrough;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectFeedThrough(boolean newDirectFeedThrough) {
		boolean oldDirectFeedThrough = directFeedThrough;
		directFeedThrough = newDirectFeedThrough;
		boolean oldDirectFeedThroughESet = directFeedThroughESet;
		directFeedThroughESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__DIRECT_FEED_THROUGH, oldDirectFeedThrough, directFeedThrough, !oldDirectFeedThroughESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDirectFeedThrough() {
		boolean oldDirectFeedThrough = directFeedThrough;
		boolean oldDirectFeedThroughESet = directFeedThroughESet;
		directFeedThrough = DIRECT_FEED_THROUGH_EDEFAULT;
		directFeedThroughESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__DIRECT_FEED_THROUGH, oldDirectFeedThrough, DIRECT_FEED_THROUGH_EDEFAULT, oldDirectFeedThroughESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDirectFeedThrough() {
		return directFeedThroughESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFunctionSignature() {
		return functionSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionSignature(String newFunctionSignature) {
		String oldFunctionSignature = functionSignature;
		functionSignature = newFunctionSignature;
		boolean oldFunctionSignatureESet = functionSignatureESet;
		functionSignatureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__FUNCTION_SIGNATURE, oldFunctionSignature, functionSignature, !oldFunctionSignatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFunctionSignature() {
		String oldFunctionSignature = functionSignature;
		boolean oldFunctionSignatureESet = functionSignatureESet;
		functionSignature = FUNCTION_SIGNATURE_EDEFAULT;
		functionSignatureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__FUNCTION_SIGNATURE, oldFunctionSignature, FUNCTION_SIGNATURE_EDEFAULT, oldFunctionSignatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFunctionSignature() {
		return functionSignatureESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHeaderFileName() {
		return headerFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeaderFileName(String newHeaderFileName) {
		String oldHeaderFileName = headerFileName;
		headerFileName = newHeaderFileName;
		boolean oldHeaderFileNameESet = headerFileNameESet;
		headerFileNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__HEADER_FILE_NAME, oldHeaderFileName, headerFileName, !oldHeaderFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHeaderFileName() {
		String oldHeaderFileName = headerFileName;
		boolean oldHeaderFileNameESet = headerFileNameESet;
		headerFileName = HEADER_FILE_NAME_EDEFAULT;
		headerFileNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__HEADER_FILE_NAME, oldHeaderFileName, HEADER_FILE_NAME_EDEFAULT, oldHeaderFileNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHeaderFileName() {
		return headerFileNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPreprocessorPriority() {
		return preprocessorPriority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreprocessorPriority(int newPreprocessorPriority) {
		int oldPreprocessorPriority = preprocessorPriority;
		preprocessorPriority = newPreprocessorPriority;
		boolean oldPreprocessorPriorityESet = preprocessorPriorityESet;
		preprocessorPriorityESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__PREPROCESSOR_PRIORITY, oldPreprocessorPriority, preprocessorPriority, !oldPreprocessorPriorityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPreprocessorPriority() {
		int oldPreprocessorPriority = preprocessorPriority;
		boolean oldPreprocessorPriorityESet = preprocessorPriorityESet;
		preprocessorPriority = PREPROCESSOR_PRIORITY_EDEFAULT;
		preprocessorPriorityESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__PREPROCESSOR_PRIORITY, oldPreprocessorPriority, PREPROCESSOR_PRIORITY_EDEFAULT, oldPreprocessorPriorityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPreprocessorPriority() {
		return preprocessorPriorityESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSingleInstruction() {
		return singleInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingleInstruction(boolean newSingleInstruction) {
		boolean oldSingleInstruction = singleInstruction;
		singleInstruction = newSingleInstruction;
		boolean oldSingleInstructionESet = singleInstructionESet;
		singleInstructionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__SINGLE_INSTRUCTION, oldSingleInstruction, singleInstruction, !oldSingleInstructionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSingleInstruction() {
		boolean oldSingleInstruction = singleInstruction;
		boolean oldSingleInstructionESet = singleInstructionESet;
		singleInstruction = SINGLE_INSTRUCTION_EDEFAULT;
		singleInstructionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__SINGLE_INSTRUCTION, oldSingleInstruction, SINGLE_INSTRUCTION_EDEFAULT, oldSingleInstructionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSingleInstruction() {
		return singleInstructionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__TYPE, oldType, type, !oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetType() {
		String oldType = type;
		boolean oldTypeESet = typeESet;
		type = TYPE_EDEFAULT;
		typeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__TYPE, oldType, TYPE_EDEFAULT, oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryHandler getLibraryHandler() {
		if (libraryHandler != null && libraryHandler.eIsProxy()) {
			InternalEObject oldLibraryHandler = (InternalEObject)libraryHandler;
			libraryHandler = (LibraryHandler)eResolveProxy(oldLibraryHandler);
			if (libraryHandler != oldLibraryHandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GablocklibraryPackage.BLOCK_TYPE__LIBRARY_HANDLER, oldLibraryHandler, libraryHandler));
			}
		}
		return libraryHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryHandler basicGetLibraryHandler() {
		return libraryHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLibraryHandler(LibraryHandler newLibraryHandler) {
		LibraryHandler oldLibraryHandler = libraryHandler;
		libraryHandler = newLibraryHandler;
		boolean oldLibraryHandlerESet = libraryHandlerESet;
		libraryHandlerESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__LIBRARY_HANDLER, oldLibraryHandler, libraryHandler, !oldLibraryHandlerESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLibraryHandler() {
		LibraryHandler oldLibraryHandler = libraryHandler;
		boolean oldLibraryHandlerESet = libraryHandlerESet;
		libraryHandler = null;
		libraryHandlerESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__LIBRARY_HANDLER, oldLibraryHandler, null, oldLibraryHandlerESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLibraryHandler() {
		return libraryHandlerESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockClass getBlockClass() {
		return blockClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlockClass(BlockClass newBlockClass) {
		BlockClass oldBlockClass = blockClass;
		blockClass = newBlockClass == null ? BLOCK_CLASS_EDEFAULT : newBlockClass;
		boolean oldBlockClassESet = blockClassESet;
		blockClassESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GablocklibraryPackage.BLOCK_TYPE__BLOCK_CLASS, oldBlockClass, blockClass, !oldBlockClassESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBlockClass() {
		BlockClass oldBlockClass = blockClass;
		boolean oldBlockClassESet = blockClassESet;
		blockClass = BLOCK_CLASS_EDEFAULT;
		blockClassESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, GablocklibraryPackage.BLOCK_TYPE__BLOCK_CLASS, oldBlockClass, BLOCK_CLASS_EDEFAULT, oldBlockClassESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBlockClass() {
		return blockClassESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterType> getParameterTypes() {
		if (parameterTypes == null) {
			parameterTypes = new EObjectContainmentEList.Unsettable<ParameterType>(ParameterType.class, this, GablocklibraryPackage.BLOCK_TYPE__PARAMETER_TYPES);
		}
		return parameterTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParameterTypes() {
		if (parameterTypes != null) ((InternalEList.Unsettable<?>)parameterTypes).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParameterTypes() {
		return parameterTypes != null && ((InternalEList.Unsettable<?>)parameterTypes).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GablocklibraryPackage.BLOCK_TYPE__PARAMETER_TYPES:
				return ((InternalEList<?>)getParameterTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GablocklibraryPackage.BLOCK_TYPE__DIRECT_FEED_THROUGH:
				return isDirectFeedThrough();
			case GablocklibraryPackage.BLOCK_TYPE__FUNCTION_SIGNATURE:
				return getFunctionSignature();
			case GablocklibraryPackage.BLOCK_TYPE__HEADER_FILE_NAME:
				return getHeaderFileName();
			case GablocklibraryPackage.BLOCK_TYPE__PREPROCESSOR_PRIORITY:
				return getPreprocessorPriority();
			case GablocklibraryPackage.BLOCK_TYPE__SINGLE_INSTRUCTION:
				return isSingleInstruction();
			case GablocklibraryPackage.BLOCK_TYPE__TYPE:
				return getType();
			case GablocklibraryPackage.BLOCK_TYPE__LIBRARY_HANDLER:
				if (resolve) return getLibraryHandler();
				return basicGetLibraryHandler();
			case GablocklibraryPackage.BLOCK_TYPE__BLOCK_CLASS:
				return getBlockClass();
			case GablocklibraryPackage.BLOCK_TYPE__PARAMETER_TYPES:
				return getParameterTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GablocklibraryPackage.BLOCK_TYPE__DIRECT_FEED_THROUGH:
				setDirectFeedThrough((Boolean)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__FUNCTION_SIGNATURE:
				setFunctionSignature((String)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__HEADER_FILE_NAME:
				setHeaderFileName((String)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__PREPROCESSOR_PRIORITY:
				setPreprocessorPriority((Integer)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__SINGLE_INSTRUCTION:
				setSingleInstruction((Boolean)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__TYPE:
				setType((String)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__LIBRARY_HANDLER:
				setLibraryHandler((LibraryHandler)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__BLOCK_CLASS:
				setBlockClass((BlockClass)newValue);
				return;
			case GablocklibraryPackage.BLOCK_TYPE__PARAMETER_TYPES:
				getParameterTypes().clear();
				getParameterTypes().addAll((Collection<? extends ParameterType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GablocklibraryPackage.BLOCK_TYPE__DIRECT_FEED_THROUGH:
				unsetDirectFeedThrough();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__FUNCTION_SIGNATURE:
				unsetFunctionSignature();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__HEADER_FILE_NAME:
				unsetHeaderFileName();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__PREPROCESSOR_PRIORITY:
				unsetPreprocessorPriority();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__SINGLE_INSTRUCTION:
				unsetSingleInstruction();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__TYPE:
				unsetType();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__LIBRARY_HANDLER:
				unsetLibraryHandler();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__BLOCK_CLASS:
				unsetBlockClass();
				return;
			case GablocklibraryPackage.BLOCK_TYPE__PARAMETER_TYPES:
				unsetParameterTypes();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GablocklibraryPackage.BLOCK_TYPE__DIRECT_FEED_THROUGH:
				return isSetDirectFeedThrough();
			case GablocklibraryPackage.BLOCK_TYPE__FUNCTION_SIGNATURE:
				return isSetFunctionSignature();
			case GablocklibraryPackage.BLOCK_TYPE__HEADER_FILE_NAME:
				return isSetHeaderFileName();
			case GablocklibraryPackage.BLOCK_TYPE__PREPROCESSOR_PRIORITY:
				return isSetPreprocessorPriority();
			case GablocklibraryPackage.BLOCK_TYPE__SINGLE_INSTRUCTION:
				return isSetSingleInstruction();
			case GablocklibraryPackage.BLOCK_TYPE__TYPE:
				return isSetType();
			case GablocklibraryPackage.BLOCK_TYPE__LIBRARY_HANDLER:
				return isSetLibraryHandler();
			case GablocklibraryPackage.BLOCK_TYPE__BLOCK_CLASS:
				return isSetBlockClass();
			case GablocklibraryPackage.BLOCK_TYPE__PARAMETER_TYPES:
				return isSetParameterTypes();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (directFeedThrough: ");
		if (directFeedThroughESet) result.append(directFeedThrough); else result.append("<unset>");
		result.append(", functionSignature: ");
		if (functionSignatureESet) result.append(functionSignature); else result.append("<unset>");
		result.append(", headerFileName: ");
		if (headerFileNameESet) result.append(headerFileName); else result.append("<unset>");
		result.append(", preprocessorPriority: ");
		if (preprocessorPriorityESet) result.append(preprocessorPriority); else result.append("<unset>");
		result.append(", singleInstruction: ");
		if (singleInstructionESet) result.append(singleInstruction); else result.append("<unset>");
		result.append(", type: ");
		if (typeESet) result.append(type); else result.append("<unset>");
		result.append(", blockClass: ");
		if (blockClassESet) result.append(blockClass); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //BlockTypeImpl
