/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gablocklibrary;

import geneauto.emf.models.genericmodel.GAModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gablocklibrary.ParameterType#getEditorMapping <em>Editor Mapping</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.ParameterType#isEvaluateEML <em>Evaluate EML</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.ParameterType#isFullNormalization <em>Full Normalization</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.ParameterType#isParseEML <em>Parse EML</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.ParameterType#isSupportSpecialReferences <em>Support Special References</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.ParameterType#getInputLanguageMappings <em>Input Language Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getParameterType()
 * @model
 * @generated
 */
public interface ParameterType extends GAModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Editor Mapping</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Mapping</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Mapping</em>' attribute.
	 * @see #isSetEditorMapping()
	 * @see #unsetEditorMapping()
	 * @see #setEditorMapping(String)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getParameterType_EditorMapping()
	 * @model unsettable="true"
	 * @generated
	 */
	String getEditorMapping();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#getEditorMapping <em>Editor Mapping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Mapping</em>' attribute.
	 * @see #isSetEditorMapping()
	 * @see #unsetEditorMapping()
	 * @see #getEditorMapping()
	 * @generated
	 */
	void setEditorMapping(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#getEditorMapping <em>Editor Mapping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEditorMapping()
	 * @see #getEditorMapping()
	 * @see #setEditorMapping(String)
	 * @generated
	 */
	void unsetEditorMapping();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#getEditorMapping <em>Editor Mapping</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Editor Mapping</em>' attribute is set.
	 * @see #unsetEditorMapping()
	 * @see #getEditorMapping()
	 * @see #setEditorMapping(String)
	 * @generated
	 */
	boolean isSetEditorMapping();

	/**
	 * Returns the value of the '<em><b>Evaluate EML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluate EML</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluate EML</em>' attribute.
	 * @see #isSetEvaluateEML()
	 * @see #unsetEvaluateEML()
	 * @see #setEvaluateEML(boolean)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getParameterType_EvaluateEML()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isEvaluateEML();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isEvaluateEML <em>Evaluate EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluate EML</em>' attribute.
	 * @see #isSetEvaluateEML()
	 * @see #unsetEvaluateEML()
	 * @see #isEvaluateEML()
	 * @generated
	 */
	void setEvaluateEML(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isEvaluateEML <em>Evaluate EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEvaluateEML()
	 * @see #isEvaluateEML()
	 * @see #setEvaluateEML(boolean)
	 * @generated
	 */
	void unsetEvaluateEML();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isEvaluateEML <em>Evaluate EML</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Evaluate EML</em>' attribute is set.
	 * @see #unsetEvaluateEML()
	 * @see #isEvaluateEML()
	 * @see #setEvaluateEML(boolean)
	 * @generated
	 */
	boolean isSetEvaluateEML();

	/**
	 * Returns the value of the '<em><b>Full Normalization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Full Normalization</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Full Normalization</em>' attribute.
	 * @see #isSetFullNormalization()
	 * @see #unsetFullNormalization()
	 * @see #setFullNormalization(boolean)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getParameterType_FullNormalization()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isFullNormalization();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isFullNormalization <em>Full Normalization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Full Normalization</em>' attribute.
	 * @see #isSetFullNormalization()
	 * @see #unsetFullNormalization()
	 * @see #isFullNormalization()
	 * @generated
	 */
	void setFullNormalization(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isFullNormalization <em>Full Normalization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFullNormalization()
	 * @see #isFullNormalization()
	 * @see #setFullNormalization(boolean)
	 * @generated
	 */
	void unsetFullNormalization();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isFullNormalization <em>Full Normalization</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Full Normalization</em>' attribute is set.
	 * @see #unsetFullNormalization()
	 * @see #isFullNormalization()
	 * @see #setFullNormalization(boolean)
	 * @generated
	 */
	boolean isSetFullNormalization();

	/**
	 * Returns the value of the '<em><b>Parse EML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parse EML</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parse EML</em>' attribute.
	 * @see #isSetParseEML()
	 * @see #unsetParseEML()
	 * @see #setParseEML(boolean)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getParameterType_ParseEML()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isParseEML();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isParseEML <em>Parse EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parse EML</em>' attribute.
	 * @see #isSetParseEML()
	 * @see #unsetParseEML()
	 * @see #isParseEML()
	 * @generated
	 */
	void setParseEML(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isParseEML <em>Parse EML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParseEML()
	 * @see #isParseEML()
	 * @see #setParseEML(boolean)
	 * @generated
	 */
	void unsetParseEML();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isParseEML <em>Parse EML</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parse EML</em>' attribute is set.
	 * @see #unsetParseEML()
	 * @see #isParseEML()
	 * @see #setParseEML(boolean)
	 * @generated
	 */
	boolean isSetParseEML();

	/**
	 * Returns the value of the '<em><b>Support Special References</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Support Special References</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Support Special References</em>' attribute.
	 * @see #isSetSupportSpecialReferences()
	 * @see #unsetSupportSpecialReferences()
	 * @see #setSupportSpecialReferences(boolean)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getParameterType_SupportSpecialReferences()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isSupportSpecialReferences();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isSupportSpecialReferences <em>Support Special References</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Support Special References</em>' attribute.
	 * @see #isSetSupportSpecialReferences()
	 * @see #unsetSupportSpecialReferences()
	 * @see #isSupportSpecialReferences()
	 * @generated
	 */
	void setSupportSpecialReferences(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isSupportSpecialReferences <em>Support Special References</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSupportSpecialReferences()
	 * @see #isSupportSpecialReferences()
	 * @see #setSupportSpecialReferences(boolean)
	 * @generated
	 */
	void unsetSupportSpecialReferences();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#isSupportSpecialReferences <em>Support Special References</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Support Special References</em>' attribute is set.
	 * @see #unsetSupportSpecialReferences()
	 * @see #isSupportSpecialReferences()
	 * @see #setSupportSpecialReferences(boolean)
	 * @generated
	 */
	boolean isSetSupportSpecialReferences();

	/**
	 * Returns the value of the '<em><b>Input Language Mappings</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gablocklibrary.InputLanguageMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Language Mappings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Language Mappings</em>' containment reference list.
	 * @see #isSetInputLanguageMappings()
	 * @see #unsetInputLanguageMappings()
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getParameterType_InputLanguageMappings()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<InputLanguageMapping> getInputLanguageMappings();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#getInputLanguageMappings <em>Input Language Mappings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInputLanguageMappings()
	 * @see #getInputLanguageMappings()
	 * @generated
	 */
	void unsetInputLanguageMappings();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.ParameterType#getInputLanguageMappings <em>Input Language Mappings</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Input Language Mappings</em>' containment reference list is set.
	 * @see #unsetInputLanguageMappings()
	 * @see #getInputLanguageMappings()
	 * @generated
	 */
	boolean isSetInputLanguageMappings();

} // ParameterType
