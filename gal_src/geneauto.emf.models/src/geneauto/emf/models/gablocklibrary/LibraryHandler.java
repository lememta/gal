/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gablocklibrary;

import geneauto.emf.models.genericmodel.GAModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Library Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Definition of a typer and backend in GABlock library handling code-generation for this block type
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getBackend <em>Backend</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getTyper <em>Typer</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getLibraryHandler()
 * @model
 * @generated
 */
public interface LibraryHandler extends GAModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Backend</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Backend</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Backend</em>' reference.
	 * @see #isSetBackend()
	 * @see #unsetBackend()
	 * @see #setBackend(BlockBackend)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getLibraryHandler_Backend()
	 * @model unsettable="true"
	 * @generated
	 */
	BlockBackend getBackend();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getBackend <em>Backend</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Backend</em>' reference.
	 * @see #isSetBackend()
	 * @see #unsetBackend()
	 * @see #getBackend()
	 * @generated
	 */
	void setBackend(BlockBackend value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getBackend <em>Backend</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBackend()
	 * @see #getBackend()
	 * @see #setBackend(BlockBackend)
	 * @generated
	 */
	void unsetBackend();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getBackend <em>Backend</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Backend</em>' reference is set.
	 * @see #unsetBackend()
	 * @see #getBackend()
	 * @see #setBackend(BlockBackend)
	 * @generated
	 */
	boolean isSetBackend();

	/**
	 * Returns the value of the '<em><b>Typer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typer</em>' reference.
	 * @see #isSetTyper()
	 * @see #unsetTyper()
	 * @see #setTyper(BlockTyper)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getLibraryHandler_Typer()
	 * @model unsettable="true"
	 * @generated
	 */
	BlockTyper getTyper();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getTyper <em>Typer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Typer</em>' reference.
	 * @see #isSetTyper()
	 * @see #unsetTyper()
	 * @see #getTyper()
	 * @generated
	 */
	void setTyper(BlockTyper value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getTyper <em>Typer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTyper()
	 * @see #getTyper()
	 * @see #setTyper(BlockTyper)
	 * @generated
	 */
	void unsetTyper();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getTyper <em>Typer</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Typer</em>' reference is set.
	 * @see #unsetTyper()
	 * @see #getTyper()
	 * @see #setTyper(BlockTyper)
	 * @generated
	 */
	boolean isSetTyper();

} // LibraryHandler
