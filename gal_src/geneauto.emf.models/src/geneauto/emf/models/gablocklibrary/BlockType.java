/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gablocklibrary;

import geneauto.emf.models.genericmodel.GAModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Defines a block for GASystemModel.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#isDirectFeedThrough <em>Direct Feed Through</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#getFunctionSignature <em>Function Signature</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#getHeaderFileName <em>Header File Name</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#getPreprocessorPriority <em>Preprocessor Priority</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#isSingleInstruction <em>Single Instruction</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#getType <em>Type</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#getLibraryHandler <em>Library Handler</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#getBlockClass <em>Block Class</em>}</li>
 *   <li>{@link geneauto.emf.models.gablocklibrary.BlockType#getParameterTypes <em>Parameter Types</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType()
 * @model
 * @generated
 */
public interface BlockType extends GAModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * Returns the value of the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direct Feed Through</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direct Feed Through</em>' attribute.
	 * @see #isSetDirectFeedThrough()
	 * @see #unsetDirectFeedThrough()
	 * @see #setDirectFeedThrough(boolean)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_DirectFeedThrough()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isDirectFeedThrough();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#isDirectFeedThrough <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direct Feed Through</em>' attribute.
	 * @see #isSetDirectFeedThrough()
	 * @see #unsetDirectFeedThrough()
	 * @see #isDirectFeedThrough()
	 * @generated
	 */
	void setDirectFeedThrough(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#isDirectFeedThrough <em>Direct Feed Through</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDirectFeedThrough()
	 * @see #isDirectFeedThrough()
	 * @see #setDirectFeedThrough(boolean)
	 * @generated
	 */
	void unsetDirectFeedThrough();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#isDirectFeedThrough <em>Direct Feed Through</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Direct Feed Through</em>' attribute is set.
	 * @see #unsetDirectFeedThrough()
	 * @see #isDirectFeedThrough()
	 * @see #setDirectFeedThrough(boolean)
	 * @generated
	 */
	boolean isSetDirectFeedThrough();

	/**
	 * Returns the value of the '<em><b>Function Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Signature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Signature</em>' attribute.
	 * @see #isSetFunctionSignature()
	 * @see #unsetFunctionSignature()
	 * @see #setFunctionSignature(String)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_FunctionSignature()
	 * @model unsettable="true"
	 * @generated
	 */
	String getFunctionSignature();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getFunctionSignature <em>Function Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Signature</em>' attribute.
	 * @see #isSetFunctionSignature()
	 * @see #unsetFunctionSignature()
	 * @see #getFunctionSignature()
	 * @generated
	 */
	void setFunctionSignature(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getFunctionSignature <em>Function Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFunctionSignature()
	 * @see #getFunctionSignature()
	 * @see #setFunctionSignature(String)
	 * @generated
	 */
	void unsetFunctionSignature();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getFunctionSignature <em>Function Signature</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Function Signature</em>' attribute is set.
	 * @see #unsetFunctionSignature()
	 * @see #getFunctionSignature()
	 * @see #setFunctionSignature(String)
	 * @generated
	 */
	boolean isSetFunctionSignature();

	/**
	 * Returns the value of the '<em><b>Header File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header File Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header File Name</em>' attribute.
	 * @see #isSetHeaderFileName()
	 * @see #unsetHeaderFileName()
	 * @see #setHeaderFileName(String)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_HeaderFileName()
	 * @model unsettable="true"
	 * @generated
	 */
	String getHeaderFileName();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getHeaderFileName <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header File Name</em>' attribute.
	 * @see #isSetHeaderFileName()
	 * @see #unsetHeaderFileName()
	 * @see #getHeaderFileName()
	 * @generated
	 */
	void setHeaderFileName(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getHeaderFileName <em>Header File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHeaderFileName()
	 * @see #getHeaderFileName()
	 * @see #setHeaderFileName(String)
	 * @generated
	 */
	void unsetHeaderFileName();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getHeaderFileName <em>Header File Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Header File Name</em>' attribute is set.
	 * @see #unsetHeaderFileName()
	 * @see #getHeaderFileName()
	 * @see #setHeaderFileName(String)
	 * @generated
	 */
	boolean isSetHeaderFileName();

	/**
	 * Returns the value of the '<em><b>Preprocessor Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preprocessor Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preprocessor Priority</em>' attribute.
	 * @see #isSetPreprocessorPriority()
	 * @see #unsetPreprocessorPriority()
	 * @see #setPreprocessorPriority(int)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_PreprocessorPriority()
	 * @model unsettable="true"
	 * @generated
	 */
	int getPreprocessorPriority();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getPreprocessorPriority <em>Preprocessor Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preprocessor Priority</em>' attribute.
	 * @see #isSetPreprocessorPriority()
	 * @see #unsetPreprocessorPriority()
	 * @see #getPreprocessorPriority()
	 * @generated
	 */
	void setPreprocessorPriority(int value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getPreprocessorPriority <em>Preprocessor Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreprocessorPriority()
	 * @see #getPreprocessorPriority()
	 * @see #setPreprocessorPriority(int)
	 * @generated
	 */
	void unsetPreprocessorPriority();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getPreprocessorPriority <em>Preprocessor Priority</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Preprocessor Priority</em>' attribute is set.
	 * @see #unsetPreprocessorPriority()
	 * @see #getPreprocessorPriority()
	 * @see #setPreprocessorPriority(int)
	 * @generated
	 */
	boolean isSetPreprocessorPriority();

	/**
	 * Returns the value of the '<em><b>Single Instruction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Instruction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Instruction</em>' attribute.
	 * @see #isSetSingleInstruction()
	 * @see #unsetSingleInstruction()
	 * @see #setSingleInstruction(boolean)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_SingleInstruction()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isSingleInstruction();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#isSingleInstruction <em>Single Instruction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Single Instruction</em>' attribute.
	 * @see #isSetSingleInstruction()
	 * @see #unsetSingleInstruction()
	 * @see #isSingleInstruction()
	 * @generated
	 */
	void setSingleInstruction(boolean value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#isSingleInstruction <em>Single Instruction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSingleInstruction()
	 * @see #isSingleInstruction()
	 * @see #setSingleInstruction(boolean)
	 * @generated
	 */
	void unsetSingleInstruction();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#isSingleInstruction <em>Single Instruction</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Single Instruction</em>' attribute is set.
	 * @see #unsetSingleInstruction()
	 * @see #isSingleInstruction()
	 * @see #setSingleInstruction(boolean)
	 * @generated
	 */
	boolean isSetSingleInstruction();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(String)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_Type()
	 * @model unsettable="true"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(String)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(String)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Library Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Library Handler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Library Handler</em>' reference.
	 * @see #isSetLibraryHandler()
	 * @see #unsetLibraryHandler()
	 * @see #setLibraryHandler(LibraryHandler)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_LibraryHandler()
	 * @model unsettable="true"
	 * @generated
	 */
	LibraryHandler getLibraryHandler();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getLibraryHandler <em>Library Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Library Handler</em>' reference.
	 * @see #isSetLibraryHandler()
	 * @see #unsetLibraryHandler()
	 * @see #getLibraryHandler()
	 * @generated
	 */
	void setLibraryHandler(LibraryHandler value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getLibraryHandler <em>Library Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLibraryHandler()
	 * @see #getLibraryHandler()
	 * @see #setLibraryHandler(LibraryHandler)
	 * @generated
	 */
	void unsetLibraryHandler();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getLibraryHandler <em>Library Handler</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Library Handler</em>' reference is set.
	 * @see #unsetLibraryHandler()
	 * @see #getLibraryHandler()
	 * @see #setLibraryHandler(LibraryHandler)
	 * @generated
	 */
	boolean isSetLibraryHandler();

	/**
	 * Returns the value of the '<em><b>Block Class</b></em>' attribute.
	 * The literals are from the enumeration {@link geneauto.emf.models.gablocklibrary.BlockClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Class</em>' attribute.
	 * @see geneauto.emf.models.gablocklibrary.BlockClass
	 * @see #isSetBlockClass()
	 * @see #unsetBlockClass()
	 * @see #setBlockClass(BlockClass)
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_BlockClass()
	 * @model unsettable="true"
	 * @generated
	 */
	BlockClass getBlockClass();

	/**
	 * Sets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getBlockClass <em>Block Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block Class</em>' attribute.
	 * @see geneauto.emf.models.gablocklibrary.BlockClass
	 * @see #isSetBlockClass()
	 * @see #unsetBlockClass()
	 * @see #getBlockClass()
	 * @generated
	 */
	void setBlockClass(BlockClass value);

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getBlockClass <em>Block Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBlockClass()
	 * @see #getBlockClass()
	 * @see #setBlockClass(BlockClass)
	 * @generated
	 */
	void unsetBlockClass();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getBlockClass <em>Block Class</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Block Class</em>' attribute is set.
	 * @see #unsetBlockClass()
	 * @see #getBlockClass()
	 * @see #setBlockClass(BlockClass)
	 * @generated
	 */
	boolean isSetBlockClass();

	/**
	 * Returns the value of the '<em><b>Parameter Types</b></em>' containment reference list.
	 * The list contents are of type {@link geneauto.emf.models.gablocklibrary.ParameterType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Types</em>' containment reference list.
	 * @see #isSetParameterTypes()
	 * @see #unsetParameterTypes()
	 * @see geneauto.emf.models.gablocklibrary.GablocklibraryPackage#getBlockType_ParameterTypes()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<ParameterType> getParameterTypes();

	/**
	 * Unsets the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getParameterTypes <em>Parameter Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParameterTypes()
	 * @see #getParameterTypes()
	 * @generated
	 */
	void unsetParameterTypes();

	/**
	 * Returns whether the value of the '{@link geneauto.emf.models.gablocklibrary.BlockType#getParameterTypes <em>Parameter Types</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parameter Types</em>' containment reference list is set.
	 * @see #unsetParameterTypes()
	 * @see #getParameterTypes()
	 * @generated
	 */
	boolean isSetParameterTypes();

} // BlockType
