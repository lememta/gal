/**
 * Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)
 * 
 *  Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse
 *     http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr
 *  Copyright (c) 2009-2010 Institute of Cybernetics at 
 *     Tallinn University of Technology
 *     http://www.ioc.ee, toom@cs.ioc.ee
 *  Copyright (c) 2009-2010 IB Krates OU
 *     http://www.krates.ee, geneauto@krates.ee
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
package geneauto.emf.models.gablocklibrary;

import geneauto.emf.models.genericmodel.GenericmodelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.models.gablocklibrary.GablocklibraryFactory
 * @model kind="package"
 * @generated
 */
public interface GablocklibraryPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Gene-Auto Ecore-based metamodel and editors (see www.geneauto.org)\r\n\r\n Copyright (c) 2009-2010 FeRIA/IRIT - INPT/University of Toulouse\r\n    http://www.enseeiht.fr/~pantel, Marc.Pantel@enseeiht.fr\r\n Copyright (c) 2009-2010 Institute of Cybernetics at \r\n    Tallinn University of Technology\r\n    http://www.ioc.ee, toom@cs.ioc.ee\r\n Copyright (c) 2009-2010 IB Krates OU\r\n    http://www.krates.ee, geneauto@krates.ee\r\n \r\n This program is free software; you can redistribute it and/or modify\r\n it under the terms of the GNU General Public License as published by\r\n the Free Software Foundation, either version 3 of the License, or\r\n (at your option) any later version.\r\n \r\n This program is distributed in the hope that it will be useful,\r\n but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\n GNU General Public License for more details.\r\n \r\n You should have received a copy of the GNU General Public License\r\n along with this program. If not, see <http://www.gnu.org/licenses/>";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gablocklibrary";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///geneauto/emf/models/gablocklibrary.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "geneauto.emf.models.gablocklibrary";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GablocklibraryPackage eINSTANCE = geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl <em>Block Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockType()
	 * @generated
	 */
	int BLOCK_TYPE = 0;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Direct Feed Through</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__DIRECT_FEED_THROUGH = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Function Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__FUNCTION_SIGNATURE = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Header File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__HEADER_FILE_NAME = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Preprocessor Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__PREPROCESSOR_PRIORITY = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Single Instruction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__SINGLE_INSTRUCTION = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__TYPE = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Library Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__LIBRARY_HANDLER = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Block Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__BLOCK_CLASS = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Parameter Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__PARAMETER_TYPES = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Block Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.impl.LibraryHandlerImpl <em>Library Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.impl.LibraryHandlerImpl
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getLibraryHandler()
	 * @generated
	 */
	int LIBRARY_HANDLER = 1;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Backend</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__BACKEND = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Typer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER__TYPER = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Library Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_HANDLER_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.BlockBackend <em>Block Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.BlockBackend
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockBackend()
	 * @generated
	 */
	int BLOCK_BACKEND = 2;

	/**
	 * The number of structural features of the '<em>Block Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_BACKEND_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.impl.BlockTyperImpl <em>Block Typer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.impl.BlockTyperImpl
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockTyper()
	 * @generated
	 */
	int BLOCK_TYPER = 3;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Block Typer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPER_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl <em>Parameter Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getParameterType()
	 * @generated
	 */
	int PARAMETER_TYPE = 4;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Editor Mapping</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__EDITOR_MAPPING = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Evaluate EML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__EVALUATE_EML = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Full Normalization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__FULL_NORMALIZATION = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parse EML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__PARSE_EML = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Support Special References</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Input Language Mappings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Parameter Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TYPE_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.impl.InputLanguageMappingImpl <em>Input Language Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.impl.InputLanguageMappingImpl
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getInputLanguageMapping()
	 * @generated
	 */
	int INPUT_LANGUAGE_MAPPING = 5;

	/**
	 * The feature id for the '<em><b>External ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__EXTERNAL_ID = GenericmodelPackage.GA_MODEL_ELEMENT__EXTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__ID = GenericmodelPackage.GA_MODEL_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Is Fixed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__IS_FIXED_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__IS_FIXED_NAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__NAME = GenericmodelPackage.GA_MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Original Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__ORIGINAL_FULL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_FULL_NAME;

	/**
	 * The feature id for the '<em><b>Original Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__ORIGINAL_NAME = GenericmodelPackage.GA_MODEL_ELEMENT__ORIGINAL_NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__PARENT = GenericmodelPackage.GA_MODEL_ELEMENT__PARENT;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__MODEL = GenericmodelPackage.GA_MODEL_ELEMENT__MODEL;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__ANNOTATIONS = GenericmodelPackage.GA_MODEL_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__LANGUAGE = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING__VERSION = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Input Language Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LANGUAGE_MAPPING_FEATURE_COUNT = GenericmodelPackage.GA_MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.CombinatorialBackend <em>Combinatorial Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.CombinatorialBackend
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getCombinatorialBackend()
	 * @generated
	 */
	int COMBINATORIAL_BACKEND = 6;

	/**
	 * The number of structural features of the '<em>Combinatorial Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATORIAL_BACKEND_FEATURE_COUNT = BLOCK_BACKEND_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.SequentialBackend <em>Sequential Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.SequentialBackend
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSequentialBackend()
	 * @generated
	 */
	int SEQUENTIAL_BACKEND = 7;

	/**
	 * The number of structural features of the '<em>Sequential Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BACKEND_FEATURE_COUNT = BLOCK_BACKEND_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.SinkBackend <em>Sink Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.SinkBackend
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSinkBackend()
	 * @generated
	 */
	int SINK_BACKEND = 8;

	/**
	 * The number of structural features of the '<em>Sink Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINK_BACKEND_FEATURE_COUNT = BLOCK_BACKEND_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.SourceBackend <em>Source Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.SourceBackend
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSourceBackend()
	 * @generated
	 */
	int SOURCE_BACKEND = 9;

	/**
	 * The number of structural features of the '<em>Source Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_BACKEND_FEATURE_COUNT = BLOCK_BACKEND_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.SubSystemBackend <em>Sub System Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.SubSystemBackend
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSubSystemBackend()
	 * @generated
	 */
	int SUB_SYSTEM_BACKEND = 10;

	/**
	 * The number of structural features of the '<em>Sub System Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_SYSTEM_BACKEND_FEATURE_COUNT = BLOCK_BACKEND_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.UserDefinedBlockBackend <em>User Defined Block Backend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.UserDefinedBlockBackend
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getUserDefinedBlockBackend()
	 * @generated
	 */
	int USER_DEFINED_BLOCK_BACKEND = 11;

	/**
	 * The number of structural features of the '<em>User Defined Block Backend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_BLOCK_BACKEND_FEATURE_COUNT = BLOCK_BACKEND_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.impl.GABlockLibraryImpl <em>GA Block Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.impl.GABlockLibraryImpl
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getGABlockLibrary()
	 * @generated
	 */
	int GA_BLOCK_LIBRARY = 12;

	/**
	 * The feature id for the '<em><b>Last Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__LAST_ID = GenericmodelPackage.MODEL__LAST_ID;

	/**
	 * The feature id for the '<em><b>Last Saved By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__LAST_SAVED_BY = GenericmodelPackage.MODEL__LAST_SAVED_BY;

	/**
	 * The feature id for the '<em><b>Last Saved On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__LAST_SAVED_ON = GenericmodelPackage.MODEL__LAST_SAVED_ON;

	/**
	 * The feature id for the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__MODEL_NAME = GenericmodelPackage.MODEL__MODEL_NAME;

	/**
	 * The feature id for the '<em><b>Model Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__MODEL_VERSION = GenericmodelPackage.MODEL__MODEL_VERSION;

	/**
	 * The feature id for the '<em><b>No New Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__NO_NEW_ID = GenericmodelPackage.MODEL__NO_NEW_ID;

	/**
	 * The feature id for the '<em><b>Transformations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__TRANSFORMATIONS = GenericmodelPackage.MODEL__TRANSFORMATIONS;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY__ELEMENTS = GenericmodelPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>GA Block Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GA_BLOCK_LIBRARY_FEATURE_COUNT = GenericmodelPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.models.gablocklibrary.BlockClass <em>Block Class</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.models.gablocklibrary.BlockClass
	 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockClass()
	 * @generated
	 */
	int BLOCK_CLASS = 13;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.BlockType <em>Block Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Type</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType
	 * @generated
	 */
	EClass getBlockType();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.BlockType#isDirectFeedThrough <em>Direct Feed Through</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direct Feed Through</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#isDirectFeedThrough()
	 * @see #getBlockType()
	 * @generated
	 */
	EAttribute getBlockType_DirectFeedThrough();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.BlockType#getFunctionSignature <em>Function Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Function Signature</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#getFunctionSignature()
	 * @see #getBlockType()
	 * @generated
	 */
	EAttribute getBlockType_FunctionSignature();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.BlockType#getHeaderFileName <em>Header File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header File Name</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#getHeaderFileName()
	 * @see #getBlockType()
	 * @generated
	 */
	EAttribute getBlockType_HeaderFileName();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.BlockType#getPreprocessorPriority <em>Preprocessor Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preprocessor Priority</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#getPreprocessorPriority()
	 * @see #getBlockType()
	 * @generated
	 */
	EAttribute getBlockType_PreprocessorPriority();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.BlockType#isSingleInstruction <em>Single Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Single Instruction</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#isSingleInstruction()
	 * @see #getBlockType()
	 * @generated
	 */
	EAttribute getBlockType_SingleInstruction();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.BlockType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#getType()
	 * @see #getBlockType()
	 * @generated
	 */
	EAttribute getBlockType_Type();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gablocklibrary.BlockType#getLibraryHandler <em>Library Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Library Handler</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#getLibraryHandler()
	 * @see #getBlockType()
	 * @generated
	 */
	EReference getBlockType_LibraryHandler();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.BlockType#getBlockClass <em>Block Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Block Class</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#getBlockClass()
	 * @see #getBlockType()
	 * @generated
	 */
	EAttribute getBlockType_BlockClass();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gablocklibrary.BlockType#getParameterTypes <em>Parameter Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Types</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockType#getParameterTypes()
	 * @see #getBlockType()
	 * @generated
	 */
	EReference getBlockType_ParameterTypes();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.LibraryHandler <em>Library Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Library Handler</em>'.
	 * @see geneauto.emf.models.gablocklibrary.LibraryHandler
	 * @generated
	 */
	EClass getLibraryHandler();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getBackend <em>Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.LibraryHandler#getBackend()
	 * @see #getLibraryHandler()
	 * @generated
	 */
	EReference getLibraryHandler_Backend();

	/**
	 * Returns the meta object for the reference '{@link geneauto.emf.models.gablocklibrary.LibraryHandler#getTyper <em>Typer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Typer</em>'.
	 * @see geneauto.emf.models.gablocklibrary.LibraryHandler#getTyper()
	 * @see #getLibraryHandler()
	 * @generated
	 */
	EReference getLibraryHandler_Typer();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.BlockBackend <em>Block Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockBackend
	 * @generated
	 */
	EClass getBlockBackend();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.BlockTyper <em>Block Typer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Typer</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockTyper
	 * @generated
	 */
	EClass getBlockTyper();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.ParameterType <em>Parameter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Type</em>'.
	 * @see geneauto.emf.models.gablocklibrary.ParameterType
	 * @generated
	 */
	EClass getParameterType();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.ParameterType#getEditorMapping <em>Editor Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Mapping</em>'.
	 * @see geneauto.emf.models.gablocklibrary.ParameterType#getEditorMapping()
	 * @see #getParameterType()
	 * @generated
	 */
	EAttribute getParameterType_EditorMapping();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.ParameterType#isEvaluateEML <em>Evaluate EML</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluate EML</em>'.
	 * @see geneauto.emf.models.gablocklibrary.ParameterType#isEvaluateEML()
	 * @see #getParameterType()
	 * @generated
	 */
	EAttribute getParameterType_EvaluateEML();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.ParameterType#isFullNormalization <em>Full Normalization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Full Normalization</em>'.
	 * @see geneauto.emf.models.gablocklibrary.ParameterType#isFullNormalization()
	 * @see #getParameterType()
	 * @generated
	 */
	EAttribute getParameterType_FullNormalization();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.ParameterType#isParseEML <em>Parse EML</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parse EML</em>'.
	 * @see geneauto.emf.models.gablocklibrary.ParameterType#isParseEML()
	 * @see #getParameterType()
	 * @generated
	 */
	EAttribute getParameterType_ParseEML();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.ParameterType#isSupportSpecialReferences <em>Support Special References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Support Special References</em>'.
	 * @see geneauto.emf.models.gablocklibrary.ParameterType#isSupportSpecialReferences()
	 * @see #getParameterType()
	 * @generated
	 */
	EAttribute getParameterType_SupportSpecialReferences();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gablocklibrary.ParameterType#getInputLanguageMappings <em>Input Language Mappings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Language Mappings</em>'.
	 * @see geneauto.emf.models.gablocklibrary.ParameterType#getInputLanguageMappings()
	 * @see #getParameterType()
	 * @generated
	 */
	EReference getParameterType_InputLanguageMappings();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.InputLanguageMapping <em>Input Language Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Language Mapping</em>'.
	 * @see geneauto.emf.models.gablocklibrary.InputLanguageMapping
	 * @generated
	 */
	EClass getInputLanguageMapping();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.InputLanguageMapping#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see geneauto.emf.models.gablocklibrary.InputLanguageMapping#getLanguage()
	 * @see #getInputLanguageMapping()
	 * @generated
	 */
	EAttribute getInputLanguageMapping_Language();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.models.gablocklibrary.InputLanguageMapping#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see geneauto.emf.models.gablocklibrary.InputLanguageMapping#getVersion()
	 * @see #getInputLanguageMapping()
	 * @generated
	 */
	EAttribute getInputLanguageMapping_Version();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.CombinatorialBackend <em>Combinatorial Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Combinatorial Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.CombinatorialBackend
	 * @generated
	 */
	EClass getCombinatorialBackend();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.SequentialBackend <em>Sequential Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequential Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.SequentialBackend
	 * @generated
	 */
	EClass getSequentialBackend();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.SinkBackend <em>Sink Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sink Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.SinkBackend
	 * @generated
	 */
	EClass getSinkBackend();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.SourceBackend <em>Source Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.SourceBackend
	 * @generated
	 */
	EClass getSourceBackend();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.SubSystemBackend <em>Sub System Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub System Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.SubSystemBackend
	 * @generated
	 */
	EClass getSubSystemBackend();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.UserDefinedBlockBackend <em>User Defined Block Backend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Defined Block Backend</em>'.
	 * @see geneauto.emf.models.gablocklibrary.UserDefinedBlockBackend
	 * @generated
	 */
	EClass getUserDefinedBlockBackend();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.models.gablocklibrary.GABlockLibrary <em>GA Block Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GA Block Library</em>'.
	 * @see geneauto.emf.models.gablocklibrary.GABlockLibrary
	 * @generated
	 */
	EClass getGABlockLibrary();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.models.gablocklibrary.GABlockLibrary#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see geneauto.emf.models.gablocklibrary.GABlockLibrary#getElements()
	 * @see #getGABlockLibrary()
	 * @generated
	 */
	EReference getGABlockLibrary_Elements();

	/**
	 * Returns the meta object for enum '{@link geneauto.emf.models.gablocklibrary.BlockClass <em>Block Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Block Class</em>'.
	 * @see geneauto.emf.models.gablocklibrary.BlockClass
	 * @generated
	 */
	EEnum getBlockClass();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GablocklibraryFactory getGablocklibraryFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl <em>Block Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.impl.BlockTypeImpl
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockType()
		 * @generated
		 */
		EClass BLOCK_TYPE = eINSTANCE.getBlockType();

		/**
		 * The meta object literal for the '<em><b>Direct Feed Through</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_TYPE__DIRECT_FEED_THROUGH = eINSTANCE.getBlockType_DirectFeedThrough();

		/**
		 * The meta object literal for the '<em><b>Function Signature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_TYPE__FUNCTION_SIGNATURE = eINSTANCE.getBlockType_FunctionSignature();

		/**
		 * The meta object literal for the '<em><b>Header File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_TYPE__HEADER_FILE_NAME = eINSTANCE.getBlockType_HeaderFileName();

		/**
		 * The meta object literal for the '<em><b>Preprocessor Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_TYPE__PREPROCESSOR_PRIORITY = eINSTANCE.getBlockType_PreprocessorPriority();

		/**
		 * The meta object literal for the '<em><b>Single Instruction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_TYPE__SINGLE_INSTRUCTION = eINSTANCE.getBlockType_SingleInstruction();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_TYPE__TYPE = eINSTANCE.getBlockType_Type();

		/**
		 * The meta object literal for the '<em><b>Library Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TYPE__LIBRARY_HANDLER = eINSTANCE.getBlockType_LibraryHandler();

		/**
		 * The meta object literal for the '<em><b>Block Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_TYPE__BLOCK_CLASS = eINSTANCE.getBlockType_BlockClass();

		/**
		 * The meta object literal for the '<em><b>Parameter Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TYPE__PARAMETER_TYPES = eINSTANCE.getBlockType_ParameterTypes();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.impl.LibraryHandlerImpl <em>Library Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.impl.LibraryHandlerImpl
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getLibraryHandler()
		 * @generated
		 */
		EClass LIBRARY_HANDLER = eINSTANCE.getLibraryHandler();

		/**
		 * The meta object literal for the '<em><b>Backend</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIBRARY_HANDLER__BACKEND = eINSTANCE.getLibraryHandler_Backend();

		/**
		 * The meta object literal for the '<em><b>Typer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIBRARY_HANDLER__TYPER = eINSTANCE.getLibraryHandler_Typer();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.BlockBackend <em>Block Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.BlockBackend
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockBackend()
		 * @generated
		 */
		EClass BLOCK_BACKEND = eINSTANCE.getBlockBackend();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.impl.BlockTyperImpl <em>Block Typer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.impl.BlockTyperImpl
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockTyper()
		 * @generated
		 */
		EClass BLOCK_TYPER = eINSTANCE.getBlockTyper();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl <em>Parameter Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.impl.ParameterTypeImpl
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getParameterType()
		 * @generated
		 */
		EClass PARAMETER_TYPE = eINSTANCE.getParameterType();

		/**
		 * The meta object literal for the '<em><b>Editor Mapping</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_TYPE__EDITOR_MAPPING = eINSTANCE.getParameterType_EditorMapping();

		/**
		 * The meta object literal for the '<em><b>Evaluate EML</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_TYPE__EVALUATE_EML = eINSTANCE.getParameterType_EvaluateEML();

		/**
		 * The meta object literal for the '<em><b>Full Normalization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_TYPE__FULL_NORMALIZATION = eINSTANCE.getParameterType_FullNormalization();

		/**
		 * The meta object literal for the '<em><b>Parse EML</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_TYPE__PARSE_EML = eINSTANCE.getParameterType_ParseEML();

		/**
		 * The meta object literal for the '<em><b>Support Special References</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_TYPE__SUPPORT_SPECIAL_REFERENCES = eINSTANCE.getParameterType_SupportSpecialReferences();

		/**
		 * The meta object literal for the '<em><b>Input Language Mappings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_TYPE__INPUT_LANGUAGE_MAPPINGS = eINSTANCE.getParameterType_InputLanguageMappings();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.impl.InputLanguageMappingImpl <em>Input Language Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.impl.InputLanguageMappingImpl
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getInputLanguageMapping()
		 * @generated
		 */
		EClass INPUT_LANGUAGE_MAPPING = eINSTANCE.getInputLanguageMapping();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INPUT_LANGUAGE_MAPPING__LANGUAGE = eINSTANCE.getInputLanguageMapping_Language();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INPUT_LANGUAGE_MAPPING__VERSION = eINSTANCE.getInputLanguageMapping_Version();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.CombinatorialBackend <em>Combinatorial Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.CombinatorialBackend
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getCombinatorialBackend()
		 * @generated
		 */
		EClass COMBINATORIAL_BACKEND = eINSTANCE.getCombinatorialBackend();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.SequentialBackend <em>Sequential Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.SequentialBackend
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSequentialBackend()
		 * @generated
		 */
		EClass SEQUENTIAL_BACKEND = eINSTANCE.getSequentialBackend();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.SinkBackend <em>Sink Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.SinkBackend
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSinkBackend()
		 * @generated
		 */
		EClass SINK_BACKEND = eINSTANCE.getSinkBackend();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.SourceBackend <em>Source Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.SourceBackend
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSourceBackend()
		 * @generated
		 */
		EClass SOURCE_BACKEND = eINSTANCE.getSourceBackend();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.SubSystemBackend <em>Sub System Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.SubSystemBackend
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getSubSystemBackend()
		 * @generated
		 */
		EClass SUB_SYSTEM_BACKEND = eINSTANCE.getSubSystemBackend();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.UserDefinedBlockBackend <em>User Defined Block Backend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.UserDefinedBlockBackend
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getUserDefinedBlockBackend()
		 * @generated
		 */
		EClass USER_DEFINED_BLOCK_BACKEND = eINSTANCE.getUserDefinedBlockBackend();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.impl.GABlockLibraryImpl <em>GA Block Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.impl.GABlockLibraryImpl
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getGABlockLibrary()
		 * @generated
		 */
		EClass GA_BLOCK_LIBRARY = eINSTANCE.getGABlockLibrary();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GA_BLOCK_LIBRARY__ELEMENTS = eINSTANCE.getGABlockLibrary_Elements();

		/**
		 * The meta object literal for the '{@link geneauto.emf.models.gablocklibrary.BlockClass <em>Block Class</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.models.gablocklibrary.BlockClass
		 * @see geneauto.emf.models.gablocklibrary.impl.GablocklibraryPackageImpl#getBlockClass()
		 * @generated
		 */
		EEnum BLOCK_CLASS = eINSTANCE.getBlockClass();

	}

} //GablocklibraryPackage
