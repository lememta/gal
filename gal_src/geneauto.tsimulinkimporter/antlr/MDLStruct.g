//* Gene-Auto code generator
//* 
//*	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/antlr/MDLStruct.g,v $
//*  @version	$Revision: 1.12 $
//*	@date		$Date: 2010-07-28 11:27:53 $
//*
//*  Copyright (c) 2006-2009 IB Krates OU
//*  	http://www.krates.ee, geneauto@krates.ee
//*  Copyright (c) 2006-2009 Alyotech
//*  	http://www.alyotech.fr, geneauto@alyotech.fr
//*
//*  
//*  This program is free software; you can redistribute it and/or modify
//*  it under the terms of the GNU General Public License as published by
//*  the Free Software Foundation, either version 3 of the License, or
//*  (at your option) any later version.
//*
//*  This program is distributed in the hope that it will be useful,
//*  but WITHOUT ANY WARRANTY; without even the implied warranty of
//*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//*  GNU General Public License for more details.
//*
//*  You should have received a copy of the GNU General Public License
//*  along with this program. If not, see <http://www.gnu.org/licenses/>
//*  
//*  Gene-Auto was originally developed by Gene-Auto consortium with support 
//*  of ITEA (www.itea2-org) and national public funding bodies in France, 
//*  Estonia, Belgium and Israel.  
//*  
//*  The members of Gene-Auto consortium are
//*  	Continental SAS
//*  	Airbus France SAS
//*  	Thales Alenia Space France SAS
//*  	ASTRIUM SAS
//*  	IB KRATES OU
//*  	Barco NV
//*  	Israel Aircraft Industries
//*  	Alyotech
//*  	Tallinn University of Technology
//*  	FERIA - Institut National Polytechnique de Toulouse
//*  	INRIA - Institut National de Recherche en Informatique et en Automatique

// base name for the lexer and parser
grammar MDLStruct;

// generation options
options {
	// the result is an Abstract syntax tree
    	output=AST;
    	// the java type of the result Tree 
    	ASTLabelType=CommonTree; 
}

@header {
package geneauto.tsimulinkimporter.antlr;
}

@lexer::header {
package geneauto.tsimulinkimporter.antlr;
}

@parser::members {
protected void mismatch(IntStream input, int ttype, BitSet follow)
  throws RecognitionException
{
  throw new MismatchedTokenException(ttype, input);
}
}

@rulecatch {
catch (RecognitionException re) {
reportError(re);
throw re;
}
}

// name of the generated function used to generate the tree
parse       :	  	( mdl ) ;

// High level description of the input model file :
// - A simulink file could be a model or a library
// - It consists in :
//	* "Model" or "Library" keyword
//	* "{" description beginning character
//	* general descriptions and/or parameter fields (optional)
// 	* graphical interface description (optional)
//	* one system description (optional)
//	* stateflow descriptions (optional)
//	* "}" description ending character
mdl         :   	(MODEL^|LIB^) OPEN! (desc|field|gidesc|defaultdesc)* sysdesc? CLOSE! (ID!)*;

// System description :
// - A system description consists in :
//	* "System" keyword
//	* "{" description beginning character
//	* system parameter fields (optional)
//	* block descriptions (optional)
//	* line descriptions (optional)
//	* annotation descriptions (optional)
//	* "}" description ending character
sysdesc	    :		SYSTEM^ OPEN! (desc|field)+ (blodesc|linedesc)* annodesc* CLOSE!;

// Default block parameters description :
// - A stateflow description consists in :
//	* "Stateflow" keyword
//	* "{" description beginning character
//	* descriptions and/or parameter fields (optional)
//	* "}" description ending character
defaultdesc	:	DEFAULT^ OPEN! blodesc* CLOSE!;

// Stateflow description :
// - A stateflow description consists of :
//	* "Stateflow" keyword
//	* "{" description beginning character
//	* descriptions and/or parameter fields (optional)
//	* "}" description ending character
sfdesc	    :		STATEF^ OPEN! (desc)+ CLOSE!;

// Annotation description :
// - An annotation description consists in :
//	* "Annotation" keyword
//	* "{" description beginning character
//	* descriptions and/or parameter fields (optional)
//	* "}" description ending character
annodesc    :		ANNOT^ OPEN! (desc|field)+ CLOSE!; 

// Block description :
// - A block description consists in :
//	* "Block" keyword
//	* "{" description beginning character
//	* block parameter fields (optional)
//	* descriptions (optional)
//	* one system description (optional)
//	* other block parameter fields (optional)
//	* "}" description ending character
blodesc	    :		BLOCK^ OPEN! (desc|field)*  sysdesc?   CLOSE!;

// Line description :
// - A line description consists in :
//	* "Line" keyword
//	* "{" description begining character
//	* parameter fields and/or branch descriptions(optionnal)
//	* "}" description ending character
linedesc    :	 	LINE^ OPEN! (field|branchdesc)* CLOSE!;

// Branch description :
// - A branch description consists in :
//	* "Branch" keyword
//	* "{" description begining character
//	* parameter fields and/or branch descriptions(optionnal)
//	* "}" description ending character
branchdesc  :		BRANCH^ OPEN! (field|branchdesc)* CLOSE!; 

// Description :
// - A description :
//	+ could be a block description
//	+ could be a stateflow description
//	+ could be a annotation description
//	+ could be a line description
// 	+ could be a branch description
//	+ could consist in :
//		* ID
//		* "{" description begining character
//		* parameter fields and/or descriptions(optionnal)
//		* * "}" description ending character
desc        :		/*blodesc!
			|sfdesc!
			|annodesc!	
			|linedesc!
			|branchdesc!
			|defaultdesc!
			|*/(ID^ OPEN! (desc|field)* CLOSE!);

// Graphical interface description: 
// - Graphical interface descritpion consists in: 
// 	* "GraphicalInterface" keyword
//	* "{" description begining character
//	* parameter fields and/or descriptions(optionnal)
//	* "}" description ending character			
gidesc		: 	GI^ OPEN! (desc|field)+ (testpointdesc)* CLOSE!; 

// Test pointed signal description: 
// - Test pointed signal description consists in: 
//	* "TestPointedSignal" keyword
//	* "{" description begining character
//	* parameter fields and/or descriptions(optionnal)
//	* "}" description ending character
testpointdesc	: 	TESTPOINT^ OPEN! (desc|field)* CLOSE!;	 					

// Field :
// - A field consists in two ID
field       :		(ID^ ID);

// Keywords
ANNOT	  :	'Annotation';
STATEF	  :	'Stateflow';
MODEL	  :	'Model';
DEFAULT   :	'BlockParameterDefaults';		
LIB	  :	'Library';
SYSTEM	  :	'System';
BLOCK	  :	'Block';
LINE	  :	'Line';
BRANCH	  :	'Branch';
GI        :	'GraphicalInterface'; 
TESTPOINT :	'TestPointedSignal';

// description beginnig and ending character
CLOSE	:	'}';
OPEN	:	'{';	

// ID :
// - An ID :
//	+ could be a STRING
//	+ could be a KEY
//	+ could be an INTER
ID 	:	STRING|KEY|INTER;

// STRING :
// - A STRING is a sequence character within double quotes
STRING  :	('"'('\u0020'..'\u0021'|'\u0023'..'\u2122'|'\t'|'\\"')+ '"')
		(('\u000A'|'\u000D''\u000A')+ ('\t'|'\u0020')* ('"'('\u0020'..'\u2122'|'\t'|'\\"')* '"')?)*;
// INTER :
// - A INTER (interval) is a sequence of numbers 
//	and some special character within '[' and ']'
INTER	:	('[' ('0'..'9'|' '|','..'.'|';')+ ']');

// KEY :
// - A KEY is a sequence character 
KEY	:	('0'..'9'|','|'"'|'%'|'('..')'|'+'..':'|'<'|'>'|'A'..'['|']'|'_'|'a'..'z'|'|'|'~'|'$')+;	 

// NEWLINE :
// - A NEWLINE is a caret return character and is skipped when found (ignored)
NEWLINE	:	(('\u000A'|'\u000D''\u000A')+ {skip();})
		/*|('""' '\r'? '\n'+ {skip();})*/;

// WS :
// - A WS is a space character or a tabulation and is skipped when found (ignored)
WS	:	(' '|'\t')+ {skip();};
// COMMENT :
// - A COMMENT consists in :
//	* a "#" character
//	* a sequence of ID (optional)
// (In this grammar version, it is skipped (ignored) when found)
COMMENT :	'#' (' '|'\t'|KEY)* {skip();};


