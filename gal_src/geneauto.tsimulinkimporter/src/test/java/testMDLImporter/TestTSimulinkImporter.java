/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/test/java/testMDLImporter/TestTSimulinkImporter.java,v $
 *  @version	$Revision: 1.54 $
 *	@date		$Date: 2010-06-04 13:05:02 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package testMDLImporter;

import static org.junit.Assert.assertEquals;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.test.utilities.CompareXml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

public class TestTSimulinkImporter {

    String[] args = new String[5];
    String outputfile;
    private String testFilesRoot = "src/test/resources/";

    /* Check that only unique parameters are added and that in case of 
     * overlapping names, the first one stays in the list
     */
    @Test
    public void paramMerge1() {
    	BlockParameter p1 = new BlockParameter("P1", "V1_A");
    	BlockParameter p2 = new BlockParameter("P2", "V2_A");
    	BlockParameter p3 = new BlockParameter("P1", "V1_B");
    	BlockParameter p4 = new BlockParameter("P2", "V2_B");
    	BlockParameter p5 = new BlockParameter("P3", "V3_B");

        Block block = new Block();
        block.addParameter(p1);
        block.addParameter(p2);

        block.addUniqueParameter(p3);
        block.addUniqueParameter(p4);
        block.addUniqueParameter(p5);

        List<Parameter> ref = new ArrayList<Parameter>();
        ref.add(p1);
        ref.add(p2);
        ref.add(p5);

        assertEquals("Parameter merging test 1: incorrect parameter count", 
        					3, block.getParameters().size());
        // NB! the following checks must use "==" not equals!
        assertEquals("Parameter merging test 1: incorrect first element", 
				true, p1 == block.getParameterByName(p1.getName()));
        assertEquals("Parameter merging test 1: incorrect second element", 
				true, p2 == block.getParameterByName(p2.getName()));
        assertEquals("Parameter merging test 1: incorrect third element", 
				true, p5 == block.getParameterByName(p5.getName()));
    }

/*
 * ToNa 05/03/10 To be moved to TestConstDefImport 
 * 
    // Unit test 2 for M-File parsing
    @Test
    @Ignore("TF:509 Decide how to handle non-standard matrix expressions")
    public void mFileImport2() {
        MConstantDefParser mfileParser = null;
        boolean ok = true;

        List<Variable_SM> refList = new ArrayList<Variable_SM>();

        Variable_SM var1 = new Variable_SM();
        var1.setName("geneauto2");

        IntegerExpression type = new IntegerExpression(3);
        List<Expression> tList = new ArrayList<Expression>();
        tList.add(type);
        var1.setDataType(new TArray(tList, new TRealDouble()));

        RealExpression e1 = new RealExpression("777.222");
        IntegerExpression e2 = new IntegerExpression(989898);
        IntegerExpression e3 = new IntegerExpression(123);
        List<Expression> eList = new ArrayList<Expression>();
        eList.add(e1);
        eList.add(e2);
        eList.add(e3);
        var1.setInitialValue(new ExpressionValue(new ListExpression(eList)));

        Variable_SM var2 = new Variable_SM();
        var2.setName("gene2auto");
        IntegerExpression t1 = new IntegerExpression(3);
        IntegerExpression t2 = new IntegerExpression(2);
        List<Expression> tList2 = new ArrayList<Expression>();
        tList2.add(t1);
        tList2.add(t2);
        var2.setDataType(new TArray(tList2, new TRealInteger(16, false)));
        IntegerExpression e11 = new IntegerExpression(-3);
        IntegerExpression e12 = new IntegerExpression(-2);
        IntegerExpression e21 = new IntegerExpression(3);
        RealExpression e22 = new RealExpression("-2.33333");
        RealExpression e31 = new RealExpression("-1.5");
        RealExpression e32 = new RealExpression("0.2222222222");
        List<Expression> eList1 = new ArrayList<Expression>();
        eList1.add(e11);
        eList1.add(e12);
        List<Expression> eList2 = new ArrayList<Expression>();
        eList2.add(e21);
        eList2.add(e22);
        List<Expression> eList3 = new ArrayList<Expression>();
        eList3.add(e31);
        eList3.add(e32);
        List<Expression> eList123 = new ArrayList<Expression>();
        eList123.add(new ListExpression(eList1));
        eList123.add(new ListExpression(eList2));
        eList123.add(new ListExpression(eList3));
        var2.setInitialValue(new ExpressionValue(new ListExpression(eList123)));

        refList.add(var1);
        refList.add(var2);

        File mFile = new File(testFilesRoot + "tests/test_mFile2.m");
        List<Variable_SM> constVarList = new ArrayList<Variable_SM>();
        try {
            mfileParser = TImporterUtilities.getParserMFile(mFile);
        } catch (Exception e) {
            ok = false;
        }
        if (mfileParser != null) {
            try {
                CommonTree t = (CommonTree) mfileParser.parse().getTree();
                constVarList = TImporterUtilities.getConstVar_List(mFile
                        .getPath(), t, null,
                        new HashMap<String, GAModelElement>());
            } catch (RecognitionException re) {
                ok = false;
            } catch (Exception e) {
                ok = false;
            }
        }
        if (ok) {
            ok = checkSameVarList(refList, constVarList);
        }
        assertEquals("M-File parsing : test 2", true, ok);
    }

    // Unit test 3 for M-File parsing
    @Test
    @Ignore("TF:509 Decide how to handle non-standard matrix expressions")
    public void mFileImport3() {
        MConstantDefParser mfileParser = null;
        boolean ok = true;

        List<Variable_SM> refList = new ArrayList<Variable_SM>();

        Annotation a1 = new Annotation();
        a1.setValue(" First comment");
        Annotation a2 = new Annotation();
        a2.setValue(" Second comment");
        Annotation a2bis = new Annotation();
        a2bis.setValue(" Second-bis comment");

        Variable_SM var1 = new Variable_SM();
        var1.setName("one");
        var1.setDataType(new TRealSingle());
        var1.setInitialValue(new ExpressionValue(new IntegerExpression(1)));
        var1.addAnnotation(a1);
        var1.addAnnotation(a2);
        var1.addAnnotation(a2bis);

        Annotation a3 = new Annotation();
        a3.setValue(" Third comment");

        Variable_SM var2 = new Variable_SM();
        var2.setName("minus2dotzero");
        var2.setDataType(new TRealDouble());
        var2.setInitialValue(new ExpressionValue(new IntegerExpression(-2)));
        var2.addAnnotation(a3);

        Annotation a4 = new Annotation();
        a4.setValue(" Fourth comment");
        Annotation a5 = new Annotation();
        a5.setValue(" Fifth comment");

        Variable_SM var3 = new Variable_SM();
        var3.setName("table");
        IntegerExpression t1 = new IntegerExpression(3);
        IntegerExpression t2 = new IntegerExpression(3);
        List<Expression> tList2 = new ArrayList<Expression>();
        tList2.add(t1);
        tList2.add(t2);
        var3.setDataType(new TArray(tList2, new TRealInteger(16, false)));
        IntegerExpression e11 = new IntegerExpression(1);
        IntegerExpression e12 = new IntegerExpression(1);
        IntegerExpression e13 = new IntegerExpression(1);
        RealExpression e21 = new RealExpression("-0.1");
        RealExpression e22 = new RealExpression("-0.1");
        RealExpression e23 = new RealExpression("-0.1");
        RealExpression e31 = new RealExpression("1.1");
        RealExpression e32 = new RealExpression("1.1");
        RealExpression e33 = new RealExpression("1.1");
        List<Expression> eList1 = new ArrayList<Expression>();
        eList1.add(e11);
        eList1.add(e12);
        eList1.add(e13);
        List<Expression> eList2 = new ArrayList<Expression>();
        eList2.add(e21);
        eList2.add(e22);
        eList2.add(e23);
        List<Expression> eList3 = new ArrayList<Expression>();
        eList3.add(e31);
        eList3.add(e32);
        eList3.add(e33);
        List<Expression> eList123 = new ArrayList<Expression>();
        eList123.add(new ListExpression(eList1));
        eList123.add(new ListExpression(eList2));
        eList123.add(new ListExpression(eList3));
        var3.setInitialValue(new ExpressionValue(new ListExpression(eList123)));
        var3.addAnnotation(a4);
        var3.addAnnotation(a5);

        refList.add(var1);
        refList.add(var2);
        refList.add(var3);

        File mFile = new File(testFilesRoot + "tests/test_mFile3.m");
        List<Variable_SM> constVarList = new ArrayList<Variable_SM>();
        try {
            mfileParser = TImporterUtilities.getParserMFile(mFile);
        } catch (Exception e) {
            ok = false;
        }
        if (mfileParser != null) {
            try {
                CommonTree t = (CommonTree) mfileParser.parse().getTree();
                constVarList = TImporterUtilities.getConstVar_List(mFile
                        .getPath(), t, null,
                        new HashMap<String, GAModelElement>());
            } catch (RecognitionException re) {
                ok = false;
            } catch (Exception e) {
                ok = false;
            }
        }
        if (ok) {
            ok = checkSameVarList(refList, constVarList);
        }
        assertEquals("M-File parsing : test 3", true, ok);
    }
*/
    // Sample with a high amount of block and signals
    // Product blocks with "/" operator and several inputs
    @Test
    @Ignore("Temporarily ignored")
    // TODO Check the reason.
    // Appeared after a workaround for TF:508
    public void funcGlobal() {
        initArgs(testFilesRoot + "tests/test_global.mdl");
        String reference = testFilesRoot + "refs/ref_test_global.gsm.xml";
        geneauto.tsimulinkimporter.main.TSimulinkImporter.main(args);
        CompareXml comparator = new CompareXml(outputfile, reference);
        boolean result = comparator.compareFiles();
        assertEquals("Sample with product blocks", true, result);
        if (result) {
            new File(outputfile).delete();
            new File(testFilesRoot + "tests/test_global.mdl.cpx").delete();
        }
    }

    // Sample with Masked SubSystem
    @Test
    @Ignore("BZ:796 Creation order of block parameters varies")
    public void funcMaskedSys() {
        initArgs(testFilesRoot + "tests/test_mask.mdl");
        String reference = testFilesRoot + "refs/ref_test_mask.gsm.xml";
        geneauto.tsimulinkimporter.main.TSimulinkImporter.main(args);
        CompareXml comparator = new CompareXml(outputfile, reference);
        boolean result = comparator.compareFiles();
        assertEquals("Sample with masked system", true, result);
        if (result) {
            new File(outputfile).delete();
            new File(testFilesRoot + "tests/test_mask.mdl.cpx").delete();
        }
    }

    // Sample with Reference block
    @Test
    @Ignore("BZ:796 Creation order of block parameters varies")
    public void funcReference() {
        initArgs(testFilesRoot + "tests/test_ref.mdl");
        String reference = testFilesRoot + "refs/ref_test_ref.gsm.xml";
        geneauto.tsimulinkimporter.main.TSimulinkImporter.main(args);
        CompareXml comparator = new CompareXml(outputfile, reference);
        boolean result = comparator.compareFiles();
        assertEquals("Sample with referenced block", true, result);
        if (result) {
            new File(outputfile).delete();
            new File(testFilesRoot + "tests/test_ref.mdl.cpx").delete();
        }
    }

    // Sample with Triggered SubSystem
    @Test
    @Ignore("BZ:796 Creation order of block parameters varies")
    public void funcTriggeredSys() {
        initArgs(testFilesRoot + "tests/test_trig.mdl");
        String reference = testFilesRoot + "refs/ref_test_trig.gsm.xml";
        geneauto.tsimulinkimporter.main.TSimulinkImporter.main(args);
        CompareXml comparator = new CompareXml(outputfile, reference);
        boolean result = comparator.compareFiles();
        assertEquals("Sample with triggered system", true, result);
        if (result) {
            new File(outputfile).delete();
            new File(testFilesRoot + "tests/test_trig.mdl.cpx").delete();
        }
    }

    // Sample with Enabled SubSystem
    @Test
    @Ignore("Temporarily ignored")
    // TODO Check the reason. TF:509?
    public void funcEnabledSys() {
        initArgs(testFilesRoot + "tests/test_ena.mdl");
        String reference = testFilesRoot + "refs/ref_test_ena.gsm.xml";
        geneauto.tsimulinkimporter.main.TSimulinkImporter.main(args);
        CompareXml comparator = new CompareXml(outputfile, reference);
        boolean result = comparator.compareFiles();
        assertEquals("Sample with enabled system", true, result);
        if (result) {
            new File(outputfile).delete();
            new File(testFilesRoot + "tests/test_ena.mdl.cpx").delete();
        }
    }

    // Sample with IfAction SubSystem
    @Test
    @Ignore("Temporarily ignored")
    // TODO Check the reason. TF:509?
    public void funcIfThelA() {
        initArgs(testFilesRoot + "tests/ifthela.mdl");
        String reference = testFilesRoot + "refs/ref_ifthela.gsm.xml";
        geneauto.tsimulinkimporter.main.TSimulinkImporter.main(args);
        CompareXml comparator = new CompareXml(outputfile, reference);
        boolean result = comparator.compareFiles();
        assertEquals("Sample with IfAction system", true, result);
        if (result) {
            new File(outputfile).delete();
            new File(testFilesRoot + "tests/ifthela.mdl.cpx").delete();
        }
    }

    public void initArgs(String inputFile) {
        args[0] = inputFile;
        args[1] = "--mLib";
        args[2] = testFilesRoot + "lib";
        args[3] = "-o";
        args[4] = inputFile.substring(0, args[0].length() - 4)
                + args[0].substring(args[0].length() - 4, args[0].length())
                        .replace(".mdl", ".gsm.xml");
        outputfile = args[4];
    }
}
