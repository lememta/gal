/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/test/java/testMDLImporter/TestConstDefImport.java,v $
 *  @version	$Revision: 1.3 $
 *	@date		$Date: 2011-11-28 22:44:12 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package testMDLImporter;

import static org.junit.Assert.assertEquals;
import geneauto.models.gacodemodel.expression.BinaryExpression;
import geneauto.models.gacodemodel.expression.DoubleExpression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.IntegerExpression;
import geneauto.models.gacodemodel.expression.UnaryExpression;
import geneauto.models.gacodemodel.operator.BinaryOperator;
import geneauto.models.gacodemodel.operator.UnaryOperator;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TRealDouble;
import geneauto.models.gadatatypes.TRealInteger;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.tsimulinkimporter.utils.MConstantDefImporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

public class TestConstDefImport {

    private String testFilesRoot = "src/test/resources/";

    @Test
    public void mFileImportSimple() {
        boolean ok = true;
        MConstantDefImporter parser = null;

        List<Variable_SM> refList = new ArrayList<Variable_SM>();
        Variable_SM var1 = new Variable_SM();
        var1.setName("A");
        var1.setDataType(new TRealInteger());
        var1.setInitialValue(new IntegerExpression("8"));
        Variable_SM var2 = new Variable_SM();
        var2.setName("var_geneauto2");
        var2.setDataType(new TRealDouble());
        DoubleExpression realExp = new DoubleExpression("-2", new TRealDouble());
        var2.setInitialValue(realExp);
        Variable_SM var3 = new Variable_SM();
        var3.setName("posDouble");
        var3.setDataType(new TRealDouble());
        var3.setInitialValue(new DoubleExpression(7.2));
        Variable_SM var4 = new Variable_SM();
        var4.setName("negDouble");
        var4.setDataType(new TRealDouble());
        var4.setInitialValue(new DoubleExpression(-7.2));
        Variable_SM var5 = new Variable_SM();
        var5.setName("posDouble2");
        var5.setDataType(new TRealDouble());
        var5.setInitialValue(new DoubleExpression(0.2));
        Variable_SM var6 = new Variable_SM();
        var6.setName("negDouble2");
        var6.setDataType(new TRealDouble());
        var6.setInitialValue(new DoubleExpression(-0.2));
        refList.add(var1);
        refList.add(var2);
        refList.add(var3);
        refList.add(var4);
        refList.add(var5);
        refList.add(var6);
        
        List<Variable_SM> constVarList = new ArrayList<Variable_SM>();
        try {
        	parser = new MConstantDefImporter();
        } catch (Exception e) {
            ok = false;
        }
        if (ok) {
            try {
                constVarList = parser.parseFile(
                			testFilesRoot + "tests/test_mFileSimple.m", 
                			null, 
                			new HashMap<String, GAModelElement>()); 
            } catch (Exception e) {
                ok = false;
            }
        }
        if (ok) {
            ok = checkSameVarList(refList, constVarList);
        }
        assertEquals("M-File parsing : test 1", true, ok);
    }

    @Test
    public void mFileImportExpr() {
        boolean ok = true;
        MConstantDefImporter parser = null;

        List<Variable_SM> refList = new ArrayList<Variable_SM>();
        Variable_SM var1 = new Variable_SM();
        var1.setName("binExpr");
        var1.setDataType(new TRealInteger());
        var1.setInitialValue(new UnaryExpression(
        		new BinaryExpression(
        			new IntegerExpression(1),
        			new IntegerExpression(2),
        			BinaryOperator.ADD_OPERATOR),
        		UnaryOperator.CAST_OPERATOR,
        		new TRealInteger()));
/*
        Variable_SM var2 = new Variable_SM();
        var2.setName("posDouble");
        var2.setDataType(new TRealDouble());
        var2.setInitialValue(new ExpressionValue(new RealExpression(7.2)));
        
        Variable_SM var3 = new Variable_SM();
        var3.setName("varExpr");
        var3.setDataType(new TRealDouble());
        var3.setInitialValue(new ExpressionValue(new UnaryExpression(
        		new UnaryExpression(
                		new VariableExpression("posDouble"),
                		UnaryOperator.UNARY_MINUS_OPERATOR),
                		UnaryOperator.CAST_OPERATOR,
                		new TRealInteger())));
        Variable_SM var4 = new Variable_SM();
        var4.setName("callExpr");
        var4.setDataType(new TRealDouble());
        List<Expression> argList = new ArrayList<Expression>();
        argList.add(new RealExpression(0.2));
        var4.setInitialValue(new ExpressionValue(new UnaryExpression(
        		new CallExpression("sin", argList, new TRealDouble()),
                		UnaryOperator.CAST_OPERATOR,
                		new TRealInteger())));
*/        
                		
        Variable_SM var5 = new Variable_SM();
        var5.setName("matrix");
        
        var5.setDataType(new TArray(
        		new IntegerExpression(2),
        		new IntegerExpression(2), 
        		new TRealDouble()));
        GeneralListExpression row1 = new GeneralListExpression();
        row1.addExpression(new DoubleExpression(1.0));
        row1.addExpression(new DoubleExpression(2.0));
        GeneralListExpression row2 = new GeneralListExpression();
        row2.addExpression(new DoubleExpression(3.0));
        row2.addExpression(new DoubleExpression(4.0));
        GeneralListExpression matrix = new GeneralListExpression();
        row1.addExpression(row1);
        row1.addExpression(row2);
        var5.setInitialValue(matrix);

        refList.add(var1);
//        refList.add(var2);
//        refList.add(var3);
//        refList.add(var4);
        refList.add(var5);
        
        List<Variable_SM> constVarList = new ArrayList<Variable_SM>();
        try {
        	parser = new MConstantDefImporter();
        } catch (Exception e) {
            ok = false;
        }
        if (ok) {
            try {
                constVarList = parser.parseFile(
                			testFilesRoot + "tests/test_mFileExpr.m", 
                			null, 
                			new HashMap<String, GAModelElement>()); 
            } catch (Exception e) {
                ok = false;
            }
        }
        if (ok) {
            ok = checkSameVarList(refList, constVarList);
        }
        assertEquals("M-File parsing : test 1", true, ok);
    }

    private boolean checkSameVarList(List<Variable_SM> refList,
            List<Variable_SM> constVarList) {
        boolean flag;
        if (!(refList.size() == constVarList.size())) {
            return false;
        }
        for (Variable_SM currentVar : constVarList) {
            flag = false;
            for (Variable_SM currentCheck : refList) {
                if (currentVar.getName().equals(currentCheck.getName())) {

                    // check data types
                    if (!DataTypeAccessor.compare(currentVar.getDataType(),
                            currentCheck.getDataType())) {
                        // data types do not match
                        return false;
                    }

                    // check values
                    String val1 = currentVar.getInitialValue()
                    							.printElement();
                    String val2 = currentVar.getInitialValue()
												.printElement();
                    if (!val1.equals(val2)) {
                        // values do not match
                        return false;
                    }

                    if (!currentVar.getAnnotations().isEmpty()) {
                        for (Annotation currentAnno : currentVar
                                .getAnnotations()) {
                            flag = false;
                            for (Annotation currentAnnoCheck : currentCheck
                                    .getAnnotations()) {
                                if (currentAnno.getValue().equals(
                                        currentAnnoCheck.getValue())) {
                                    flag = true;
                                }
                            }
                        }
                    } else if (currentCheck.getAnnotations().isEmpty()) {
                        flag = true;
                    }

                    break;
                }
            }

            if (!flag) {
                return false;
            }
        }

        return true;
    }

}
