// $ANTLR 3.0.1 T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g 2010-07-28 14:08:16

package geneauto.tsimulinkimporter.antlr;


import org.antlr.runtime.BitSet;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.IntStream;
import org.antlr.runtime.MismatchedTokenException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.ParserRuleReturnScope;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.TreeAdaptor;

@SuppressWarnings("unused")
public class MDLStructParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "MODEL", "LIB", "OPEN", "CLOSE", "ID", "SYSTEM", "DEFAULT", "STATEF", "ANNOT", "BLOCK", "LINE", "BRANCH", "GI", "TESTPOINT", "STRING", "KEY", "INTER", "NEWLINE", "WS", "COMMENT"
    };
    public static final int KEY=19;
    public static final int DEFAULT=10;
    public static final int INTER=20;
    public static final int ID=8;
    public static final int ANNOT=12;
    public static final int EOF=-1;
    public static final int LINE=14;
    public static final int LIB=5;
    public static final int OPEN=6;
    public static final int WS=22;
    public static final int NEWLINE=21;
    public static final int CLOSE=7;
    public static final int SYSTEM=9;
    public static final int BLOCK=13;
    public static final int STATEF=11;
    public static final int BRANCH=15;
    public static final int MODEL=4;
    public static final int COMMENT=23;
    public static final int TESTPOINT=17;
    public static final int GI=16;
    public static final int STRING=18;

        public MDLStructParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g"; }

    
    protected void mismatch(IntStream input, int ttype, BitSet follow)
      throws RecognitionException
    {
      throw new MismatchedTokenException(ttype, input);
    }


    public static class parse_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parse
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:78:1: parse : ( mdl ) ;
    public final parse_return parse() throws RecognitionException {
        parse_return retval = new parse_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        mdl_return mdl1 = null;



        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:78:13: ( ( mdl ) )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:78:18: ( mdl )
            {
            root_0 = (CommonTree)adaptor.nil();

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:78:18: ( mdl )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:78:20: mdl
            {
            pushFollow(FOLLOW_mdl_in_parse130);
            mdl1=mdl();
            _fsp--;

            adaptor.addChild(root_0, mdl1.getTree());

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parse

    public static class mdl_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start mdl
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:1: mdl : ( MODEL | LIB ) OPEN ( desc | field | gidesc | defaultdesc )* ( sysdesc )? CLOSE ( ID )* ;
    public final mdl_return mdl() throws RecognitionException {
        mdl_return retval = new mdl_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token MODEL2=null;
        Token LIB3=null;
        Token OPEN4=null;
        Token CLOSE10=null;
        Token ID11=null;
        desc_return desc5 = null;

        field_return field6 = null;

        gidesc_return gidesc7 = null;

        defaultdesc_return defaultdesc8 = null;

        sysdesc_return sysdesc9 = null;


        CommonTree MODEL2_tree=null;
        CommonTree LIB3_tree=null;
        CommonTree OPEN4_tree=null;
        CommonTree CLOSE10_tree=null;
        CommonTree ID11_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:13: ( ( MODEL | LIB ) OPEN ( desc | field | gidesc | defaultdesc )* ( sysdesc )? CLOSE ( ID )* )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:18: ( MODEL | LIB ) OPEN ( desc | field | gidesc | defaultdesc )* ( sysdesc )? CLOSE ( ID )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:18: ( MODEL | LIB )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==MODEL) ) {
                alt1=1;
            }
            else if ( (LA1_0==LIB) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("90:18: ( MODEL | LIB )", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:19: MODEL
                    {
                    MODEL2=(Token)input.LT(1);
                    match(input,MODEL,FOLLOW_MODEL_in_mdl163); 
                    MODEL2_tree = (CommonTree)adaptor.create(MODEL2);
                    root_0 = (CommonTree)adaptor.becomeRoot(MODEL2_tree, root_0);


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:26: LIB
                    {
                    LIB3=(Token)input.LT(1);
                    match(input,LIB,FOLLOW_LIB_in_mdl166); 
                    LIB3_tree = (CommonTree)adaptor.create(LIB3);
                    root_0 = (CommonTree)adaptor.becomeRoot(LIB3_tree, root_0);


                    }
                    break;

            }

            OPEN4=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_mdl170); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:38: ( desc | field | gidesc | defaultdesc )*
            loop2:
            do {
                int alt2=5;
                switch ( input.LA(1) ) {
                case ID:
                    {
                    int LA2_2 = input.LA(2);

                    if ( (LA2_2==ID) ) {
                        alt2=2;
                    }
                    else if ( (LA2_2==OPEN) ) {
                        alt2=1;
                    }


                    }
                    break;
                case GI:
                    {
                    alt2=3;
                    }
                    break;
                case DEFAULT:
                    {
                    alt2=4;
                    }
                    break;

                }

                switch (alt2) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:39: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_mdl174);
            	    desc5=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc5.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:44: field
            	    {
            	    pushFollow(FOLLOW_field_in_mdl176);
            	    field6=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field6.getTree());

            	    }
            	    break;
            	case 3 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:50: gidesc
            	    {
            	    pushFollow(FOLLOW_gidesc_in_mdl178);
            	    gidesc7=gidesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, gidesc7.getTree());

            	    }
            	    break;
            	case 4 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:57: defaultdesc
            	    {
            	    pushFollow(FOLLOW_defaultdesc_in_mdl180);
            	    defaultdesc8=defaultdesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, defaultdesc8.getTree());

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:71: ( sysdesc )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==SYSTEM) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:71: sysdesc
                    {
                    pushFollow(FOLLOW_sysdesc_in_mdl184);
                    sysdesc9=sysdesc();
                    _fsp--;

                    adaptor.addChild(root_0, sysdesc9.getTree());

                    }
                    break;

            }

            CLOSE10=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_mdl187); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:87: ( ID )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:90:88: ID
            	    {
            	    ID11=(Token)input.LT(1);
            	    match(input,ID,FOLLOW_ID_in_mdl191); 

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end mdl

    public static class sysdesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start sysdesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:1: sysdesc : SYSTEM OPEN ( desc | field )+ ( blodesc | linedesc )* ( annodesc )* CLOSE ;
    public final sysdesc_return sysdesc() throws RecognitionException {
        sysdesc_return retval = new sysdesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token SYSTEM12=null;
        Token OPEN13=null;
        Token CLOSE19=null;
        desc_return desc14 = null;

        field_return field15 = null;

        blodesc_return blodesc16 = null;

        linedesc_return linedesc17 = null;

        annodesc_return annodesc18 = null;


        CommonTree SYSTEM12_tree=null;
        CommonTree OPEN13_tree=null;
        CommonTree CLOSE19_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:13: ( SYSTEM OPEN ( desc | field )+ ( blodesc | linedesc )* ( annodesc )* CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:16: SYSTEM OPEN ( desc | field )+ ( blodesc | linedesc )* ( annodesc )* CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            SYSTEM12=(Token)input.LT(1);
            match(input,SYSTEM,FOLLOW_SYSTEM_in_sysdesc216); 
            SYSTEM12_tree = (CommonTree)adaptor.create(SYSTEM12);
            root_0 = (CommonTree)adaptor.becomeRoot(SYSTEM12_tree, root_0);

            OPEN13=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_sysdesc219); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:30: ( desc | field )+
            int cnt5=0;
            loop5:
            do {
                int alt5=3;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==ID) ) {
                    int LA5_2 = input.LA(2);

                    if ( (LA5_2==ID) ) {
                        alt5=2;
                    }
                    else if ( (LA5_2==OPEN) ) {
                        alt5=1;
                    }


                }


                switch (alt5) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:31: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_sysdesc223);
            	    desc14=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc14.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:36: field
            	    {
            	    pushFollow(FOLLOW_field_in_sysdesc225);
            	    field15=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field15.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:44: ( blodesc | linedesc )*
            loop6:
            do {
                int alt6=3;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==BLOCK) ) {
                    alt6=1;
                }
                else if ( (LA6_0==LINE) ) {
                    alt6=2;
                }


                switch (alt6) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:45: blodesc
            	    {
            	    pushFollow(FOLLOW_blodesc_in_sysdesc230);
            	    blodesc16=blodesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, blodesc16.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:53: linedesc
            	    {
            	    pushFollow(FOLLOW_linedesc_in_sysdesc232);
            	    linedesc17=linedesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, linedesc17.getTree());

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:64: ( annodesc )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==ANNOT) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:101:64: annodesc
            	    {
            	    pushFollow(FOLLOW_annodesc_in_sysdesc236);
            	    annodesc18=annodesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, annodesc18.getTree());

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            CLOSE19=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_sysdesc239); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end sysdesc

    public static class defaultdesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start defaultdesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:109:1: defaultdesc : DEFAULT OPEN ( blodesc )* CLOSE ;
    public final defaultdesc_return defaultdesc() throws RecognitionException {
        defaultdesc_return retval = new defaultdesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token DEFAULT20=null;
        Token OPEN21=null;
        Token CLOSE23=null;
        blodesc_return blodesc22 = null;


        CommonTree DEFAULT20_tree=null;
        CommonTree OPEN21_tree=null;
        CommonTree CLOSE23_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:109:13: ( DEFAULT OPEN ( blodesc )* CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:109:15: DEFAULT OPEN ( blodesc )* CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            DEFAULT20=(Token)input.LT(1);
            match(input,DEFAULT,FOLLOW_DEFAULT_in_defaultdesc254); 
            DEFAULT20_tree = (CommonTree)adaptor.create(DEFAULT20);
            root_0 = (CommonTree)adaptor.becomeRoot(DEFAULT20_tree, root_0);

            OPEN21=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_defaultdesc257); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:109:30: ( blodesc )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==BLOCK) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:109:30: blodesc
            	    {
            	    pushFollow(FOLLOW_blodesc_in_defaultdesc260);
            	    blodesc22=blodesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, blodesc22.getTree());

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            CLOSE23=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_defaultdesc263); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end defaultdesc

    public static class sfdesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start sfdesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:117:1: sfdesc : STATEF OPEN ( desc )+ CLOSE ;
    public final sfdesc_return sfdesc() throws RecognitionException {
        sfdesc_return retval = new sfdesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token STATEF24=null;
        Token OPEN25=null;
        Token CLOSE27=null;
        desc_return desc26 = null;


        CommonTree STATEF24_tree=null;
        CommonTree OPEN25_tree=null;
        CommonTree CLOSE27_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:117:12: ( STATEF OPEN ( desc )+ CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:117:15: STATEF OPEN ( desc )+ CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            STATEF24=(Token)input.LT(1);
            match(input,STATEF,FOLLOW_STATEF_in_sfdesc283); 
            STATEF24_tree = (CommonTree)adaptor.create(STATEF24);
            root_0 = (CommonTree)adaptor.becomeRoot(STATEF24_tree, root_0);

            OPEN25=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_sfdesc286); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:117:29: ( desc )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==ID) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:117:30: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_sfdesc290);
            	    desc26=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc26.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);

            CLOSE27=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_sfdesc294); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end sfdesc

    public static class annodesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start annodesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:125:1: annodesc : ANNOT OPEN ( desc | field )+ CLOSE ;
    public final annodesc_return annodesc() throws RecognitionException {
        annodesc_return retval = new annodesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ANNOT28=null;
        Token OPEN29=null;
        Token CLOSE32=null;
        desc_return desc30 = null;

        field_return field31 = null;


        CommonTree ANNOT28_tree=null;
        CommonTree OPEN29_tree=null;
        CommonTree CLOSE32_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:125:13: ( ANNOT OPEN ( desc | field )+ CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:125:16: ANNOT OPEN ( desc | field )+ CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            ANNOT28=(Token)input.LT(1);
            match(input,ANNOT,FOLLOW_ANNOT_in_annodesc313); 
            ANNOT28_tree = (CommonTree)adaptor.create(ANNOT28);
            root_0 = (CommonTree)adaptor.becomeRoot(ANNOT28_tree, root_0);

            OPEN29=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_annodesc316); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:125:29: ( desc | field )+
            int cnt10=0;
            loop10:
            do {
                int alt10=3;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==ID) ) {
                    int LA10_2 = input.LA(2);

                    if ( (LA10_2==ID) ) {
                        alt10=2;
                    }
                    else if ( (LA10_2==OPEN) ) {
                        alt10=1;
                    }


                }


                switch (alt10) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:125:30: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_annodesc320);
            	    desc30=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc30.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:125:35: field
            	    {
            	    pushFollow(FOLLOW_field_in_annodesc322);
            	    field31=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field31.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);

            CLOSE32=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_annodesc326); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end annodesc

    public static class blodesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start blodesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:1: blodesc : BLOCK OPEN ( desc | field )* ( sysdesc )? CLOSE ;
    public final blodesc_return blodesc() throws RecognitionException {
        blodesc_return retval = new blodesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token BLOCK33=null;
        Token OPEN34=null;
        Token CLOSE38=null;
        desc_return desc35 = null;

        field_return field36 = null;

        sysdesc_return sysdesc37 = null;


        CommonTree BLOCK33_tree=null;
        CommonTree OPEN34_tree=null;
        CommonTree CLOSE38_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:13: ( BLOCK OPEN ( desc | field )* ( sysdesc )? CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:16: BLOCK OPEN ( desc | field )* ( sysdesc )? CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            BLOCK33=(Token)input.LT(1);
            match(input,BLOCK,FOLLOW_BLOCK_in_blodesc350); 
            BLOCK33_tree = (CommonTree)adaptor.create(BLOCK33);
            root_0 = (CommonTree)adaptor.becomeRoot(BLOCK33_tree, root_0);

            OPEN34=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_blodesc353); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:29: ( desc | field )*
            loop11:
            do {
                int alt11=3;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==ID) ) {
                    int LA11_2 = input.LA(2);

                    if ( (LA11_2==OPEN) ) {
                        alt11=1;
                    }
                    else if ( (LA11_2==ID) ) {
                        alt11=2;
                    }


                }


                switch (alt11) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:30: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_blodesc357);
            	    desc35=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc35.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:35: field
            	    {
            	    pushFollow(FOLLOW_field_in_blodesc359);
            	    field36=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field36.getTree());

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:44: ( sysdesc )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==SYSTEM) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:136:44: sysdesc
                    {
                    pushFollow(FOLLOW_sysdesc_in_blodesc364);
                    sysdesc37=sysdesc();
                    _fsp--;

                    adaptor.addChild(root_0, sysdesc37.getTree());

                    }
                    break;

            }

            CLOSE38=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_blodesc369); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end blodesc

    public static class linedesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start linedesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:144:1: linedesc : LINE OPEN ( field | branchdesc )* CLOSE ;
    public final linedesc_return linedesc() throws RecognitionException {
        linedesc_return retval = new linedesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LINE39=null;
        Token OPEN40=null;
        Token CLOSE43=null;
        field_return field41 = null;

        branchdesc_return branchdesc42 = null;


        CommonTree LINE39_tree=null;
        CommonTree OPEN40_tree=null;
        CommonTree CLOSE43_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:144:13: ( LINE OPEN ( field | branchdesc )* CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:144:17: LINE OPEN ( field | branchdesc )* CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            LINE39=(Token)input.LT(1);
            match(input,LINE,FOLLOW_LINE_in_linedesc389); 
            LINE39_tree = (CommonTree)adaptor.create(LINE39);
            root_0 = (CommonTree)adaptor.becomeRoot(LINE39_tree, root_0);

            OPEN40=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_linedesc392); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:144:29: ( field | branchdesc )*
            loop13:
            do {
                int alt13=3;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==ID) ) {
                    alt13=1;
                }
                else if ( (LA13_0==BRANCH) ) {
                    alt13=2;
                }


                switch (alt13) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:144:30: field
            	    {
            	    pushFollow(FOLLOW_field_in_linedesc396);
            	    field41=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field41.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:144:36: branchdesc
            	    {
            	    pushFollow(FOLLOW_branchdesc_in_linedesc398);
            	    branchdesc42=branchdesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, branchdesc42.getTree());

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            CLOSE43=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_linedesc402); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end linedesc

    public static class branchdesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start branchdesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:152:1: branchdesc : BRANCH OPEN ( field | branchdesc )* CLOSE ;
    public final branchdesc_return branchdesc() throws RecognitionException {
        branchdesc_return retval = new branchdesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token BRANCH44=null;
        Token OPEN45=null;
        Token CLOSE48=null;
        field_return field46 = null;

        branchdesc_return branchdesc47 = null;


        CommonTree BRANCH44_tree=null;
        CommonTree OPEN45_tree=null;
        CommonTree CLOSE48_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:152:13: ( BRANCH OPEN ( field | branchdesc )* CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:152:16: BRANCH OPEN ( field | branchdesc )* CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            BRANCH44=(Token)input.LT(1);
            match(input,BRANCH,FOLLOW_BRANCH_in_branchdesc419); 
            BRANCH44_tree = (CommonTree)adaptor.create(BRANCH44);
            root_0 = (CommonTree)adaptor.becomeRoot(BRANCH44_tree, root_0);

            OPEN45=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_branchdesc422); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:152:30: ( field | branchdesc )*
            loop14:
            do {
                int alt14=3;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==ID) ) {
                    alt14=1;
                }
                else if ( (LA14_0==BRANCH) ) {
                    alt14=2;
                }


                switch (alt14) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:152:31: field
            	    {
            	    pushFollow(FOLLOW_field_in_branchdesc426);
            	    field46=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field46.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:152:37: branchdesc
            	    {
            	    pushFollow(FOLLOW_branchdesc_in_branchdesc428);
            	    branchdesc47=branchdesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, branchdesc47.getTree());

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            CLOSE48=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_branchdesc432); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end branchdesc

    public static class desc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start desc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:166:1: desc : ( ID OPEN ( desc | field )* CLOSE ) ;
    public final desc_return desc() throws RecognitionException {
        desc_return retval = new desc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ID49=null;
        Token OPEN50=null;
        Token CLOSE53=null;
        desc_return desc51 = null;

        field_return field52 = null;


        CommonTree ID49_tree=null;
        CommonTree OPEN50_tree=null;
        CommonTree CLOSE53_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:166:13: ( ( ID OPEN ( desc | field )* CLOSE ) )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:172:7: ( ID OPEN ( desc | field )* CLOSE )
            {
            root_0 = (CommonTree)adaptor.nil();

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:172:7: ( ID OPEN ( desc | field )* CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:172:8: ID OPEN ( desc | field )* CLOSE
            {
            ID49=(Token)input.LT(1);
            match(input,ID,FOLLOW_ID_in_desc464); 
            ID49_tree = (CommonTree)adaptor.create(ID49);
            root_0 = (CommonTree)adaptor.becomeRoot(ID49_tree, root_0);

            OPEN50=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_desc467); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:172:18: ( desc | field )*
            loop15:
            do {
                int alt15=3;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==ID) ) {
                    int LA15_2 = input.LA(2);

                    if ( (LA15_2==OPEN) ) {
                        alt15=1;
                    }
                    else if ( (LA15_2==ID) ) {
                        alt15=2;
                    }


                }


                switch (alt15) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:172:19: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_desc471);
            	    desc51=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc51.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:172:24: field
            	    {
            	    pushFollow(FOLLOW_field_in_desc473);
            	    field52=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field52.getTree());

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            CLOSE53=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_desc477); 

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end desc

    public static class gidesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start gidesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:1: gidesc : GI OPEN ( desc | field )+ ( testpointdesc )* CLOSE ;
    public final gidesc_return gidesc() throws RecognitionException {
        gidesc_return retval = new gidesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token GI54=null;
        Token OPEN55=null;
        Token CLOSE59=null;
        desc_return desc56 = null;

        field_return field57 = null;

        testpointdesc_return testpointdesc58 = null;


        CommonTree GI54_tree=null;
        CommonTree OPEN55_tree=null;
        CommonTree CLOSE59_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:9: ( GI OPEN ( desc | field )+ ( testpointdesc )* CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:12: GI OPEN ( desc | field )+ ( testpointdesc )* CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            GI54=(Token)input.LT(1);
            match(input,GI,FOLLOW_GI_in_gidesc495); 
            GI54_tree = (CommonTree)adaptor.create(GI54);
            root_0 = (CommonTree)adaptor.becomeRoot(GI54_tree, root_0);

            OPEN55=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_gidesc498); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:22: ( desc | field )+
            int cnt16=0;
            loop16:
            do {
                int alt16=3;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==ID) ) {
                    int LA16_2 = input.LA(2);

                    if ( (LA16_2==ID) ) {
                        alt16=2;
                    }
                    else if ( (LA16_2==OPEN) ) {
                        alt16=1;
                    }


                }


                switch (alt16) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:23: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_gidesc502);
            	    desc56=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc56.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:28: field
            	    {
            	    pushFollow(FOLLOW_field_in_gidesc504);
            	    field57=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field57.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:36: ( testpointdesc )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==TESTPOINT) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:180:37: testpointdesc
            	    {
            	    pushFollow(FOLLOW_testpointdesc_in_gidesc509);
            	    testpointdesc58=testpointdesc();
            	    _fsp--;

            	    adaptor.addChild(root_0, testpointdesc58.getTree());

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            CLOSE59=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_gidesc513); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end gidesc

    public static class testpointdesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start testpointdesc
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:188:1: testpointdesc : TESTPOINT OPEN ( desc | field )* CLOSE ;
    public final testpointdesc_return testpointdesc() throws RecognitionException {
        testpointdesc_return retval = new testpointdesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token TESTPOINT60=null;
        Token OPEN61=null;
        Token CLOSE64=null;
        desc_return desc62 = null;

        field_return field63 = null;


        CommonTree TESTPOINT60_tree=null;
        CommonTree OPEN61_tree=null;
        CommonTree CLOSE64_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:188:15: ( TESTPOINT OPEN ( desc | field )* CLOSE )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:188:18: TESTPOINT OPEN ( desc | field )* CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            TESTPOINT60=(Token)input.LT(1);
            match(input,TESTPOINT,FOLLOW_TESTPOINT_in_testpointdesc530); 
            TESTPOINT60_tree = (CommonTree)adaptor.create(TESTPOINT60);
            root_0 = (CommonTree)adaptor.becomeRoot(TESTPOINT60_tree, root_0);

            OPEN61=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_testpointdesc533); 
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:188:35: ( desc | field )*
            loop18:
            do {
                int alt18=3;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==ID) ) {
                    int LA18_2 = input.LA(2);

                    if ( (LA18_2==ID) ) {
                        alt18=2;
                    }
                    else if ( (LA18_2==OPEN) ) {
                        alt18=1;
                    }


                }


                switch (alt18) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:188:36: desc
            	    {
            	    pushFollow(FOLLOW_desc_in_testpointdesc537);
            	    desc62=desc();
            	    _fsp--;

            	    adaptor.addChild(root_0, desc62.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:188:41: field
            	    {
            	    pushFollow(FOLLOW_field_in_testpointdesc539);
            	    field63=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field63.getTree());

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            CLOSE64=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_testpointdesc543); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end testpointdesc

    public static class field_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start field
    // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:192:1: field : ( ID ID ) ;
    public final field_return field() throws RecognitionException {
        field_return retval = new field_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ID65=null;
        Token ID66=null;

        CommonTree ID65_tree=null;
        CommonTree ID66_tree=null;

        try {
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:192:13: ( ( ID ID ) )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:192:16: ( ID ID )
            {
            root_0 = (CommonTree)adaptor.nil();

            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:192:16: ( ID ID )
            // T:\\geneauto-dev-ibk_ada\\Eclipse\\geneauto.tsimulinkimporter\\antlr\\MDLStruct.g:192:17: ID ID
            {
            ID65=(Token)input.LT(1);
            match(input,ID,FOLLOW_ID_in_field569); 
            ID65_tree = (CommonTree)adaptor.create(ID65);
            root_0 = (CommonTree)adaptor.becomeRoot(ID65_tree, root_0);

            ID66=(Token)input.LT(1);
            match(input,ID,FOLLOW_ID_in_field572); 
            ID66_tree = (CommonTree)adaptor.create(ID66);
            adaptor.addChild(root_0, ID66_tree);


            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        catch (RecognitionException re) {
        reportError(re);
        throw re;
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end field


 

    public static final BitSet FOLLOW_mdl_in_parse130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MODEL_in_mdl163 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_LIB_in_mdl166 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_mdl170 = new BitSet(new long[]{0x0000000000010780L});
    public static final BitSet FOLLOW_desc_in_mdl174 = new BitSet(new long[]{0x0000000000010780L});
    public static final BitSet FOLLOW_field_in_mdl176 = new BitSet(new long[]{0x0000000000010780L});
    public static final BitSet FOLLOW_gidesc_in_mdl178 = new BitSet(new long[]{0x0000000000010780L});
    public static final BitSet FOLLOW_defaultdesc_in_mdl180 = new BitSet(new long[]{0x0000000000010780L});
    public static final BitSet FOLLOW_sysdesc_in_mdl184 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_CLOSE_in_mdl187 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_ID_in_mdl191 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_SYSTEM_in_sysdesc216 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_sysdesc219 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_desc_in_sysdesc223 = new BitSet(new long[]{0x0000000000007180L});
    public static final BitSet FOLLOW_field_in_sysdesc225 = new BitSet(new long[]{0x0000000000007180L});
    public static final BitSet FOLLOW_blodesc_in_sysdesc230 = new BitSet(new long[]{0x0000000000007080L});
    public static final BitSet FOLLOW_linedesc_in_sysdesc232 = new BitSet(new long[]{0x0000000000007080L});
    public static final BitSet FOLLOW_annodesc_in_sysdesc236 = new BitSet(new long[]{0x0000000000001080L});
    public static final BitSet FOLLOW_CLOSE_in_sysdesc239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DEFAULT_in_defaultdesc254 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_defaultdesc257 = new BitSet(new long[]{0x0000000000002080L});
    public static final BitSet FOLLOW_blodesc_in_defaultdesc260 = new BitSet(new long[]{0x0000000000002080L});
    public static final BitSet FOLLOW_CLOSE_in_defaultdesc263 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STATEF_in_sfdesc283 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_sfdesc286 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_desc_in_sfdesc290 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_CLOSE_in_sfdesc294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ANNOT_in_annodesc313 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_annodesc316 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_desc_in_annodesc320 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_field_in_annodesc322 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_CLOSE_in_annodesc326 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BLOCK_in_blodesc350 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_blodesc353 = new BitSet(new long[]{0x0000000000000380L});
    public static final BitSet FOLLOW_desc_in_blodesc357 = new BitSet(new long[]{0x0000000000000380L});
    public static final BitSet FOLLOW_field_in_blodesc359 = new BitSet(new long[]{0x0000000000000380L});
    public static final BitSet FOLLOW_sysdesc_in_blodesc364 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_CLOSE_in_blodesc369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LINE_in_linedesc389 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_linedesc392 = new BitSet(new long[]{0x0000000000008180L});
    public static final BitSet FOLLOW_field_in_linedesc396 = new BitSet(new long[]{0x0000000000008180L});
    public static final BitSet FOLLOW_branchdesc_in_linedesc398 = new BitSet(new long[]{0x0000000000008180L});
    public static final BitSet FOLLOW_CLOSE_in_linedesc402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BRANCH_in_branchdesc419 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_branchdesc422 = new BitSet(new long[]{0x0000000000008180L});
    public static final BitSet FOLLOW_field_in_branchdesc426 = new BitSet(new long[]{0x0000000000008180L});
    public static final BitSet FOLLOW_branchdesc_in_branchdesc428 = new BitSet(new long[]{0x0000000000008180L});
    public static final BitSet FOLLOW_CLOSE_in_branchdesc432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_desc464 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_desc467 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_desc_in_desc471 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_field_in_desc473 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_CLOSE_in_desc477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GI_in_gidesc495 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_gidesc498 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_desc_in_gidesc502 = new BitSet(new long[]{0x0000000000020180L});
    public static final BitSet FOLLOW_field_in_gidesc504 = new BitSet(new long[]{0x0000000000020180L});
    public static final BitSet FOLLOW_testpointdesc_in_gidesc509 = new BitSet(new long[]{0x0000000000020080L});
    public static final BitSet FOLLOW_CLOSE_in_gidesc513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TESTPOINT_in_testpointdesc530 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_testpointdesc533 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_desc_in_testpointdesc537 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_field_in_testpointdesc539 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_CLOSE_in_testpointdesc543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_field569 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_ID_in_field572 = new BitSet(new long[]{0x0000000000000002L});

}