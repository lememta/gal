/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/states/TSICheckingMultiRateState.java,v $
 *  @version	$Revision: 1.10 $
 *	@date		$Date: 2011-07-07 12:24:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.gaenumtypes.FunctionStyle;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.tsimulinkimporter.main.TSimulinkImporterTool;

import java.util.HashMap;
import java.util.Map;

/**
 * This class offers methods for checking if input model has a valid multi-rate
 * configuration, i.e sample time values compatibility (systems have lower
 * sample time values than subsystems, all sample time values are multiple of
 * minimum sample time value)
 * 
 * 
 */
public class TSICheckingMultiRateState extends TSIState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public TSICheckingMultiRateState(TSimulinkImporterTool machine) {
        super(machine, "Checking multi-rate configuration");
    }

    public void stateExecute() {
        // Exports GASystemModel to XML file (.gsm)
        checkMultiRateValidity();
        stateMachine.setState(stateMachine.getParsingEmlState());
    }

    /**
     * Check multi-rating layout and validity
     */
    private void checkMultiRateValidity() {

        // Retrieves the Top-level SystemBlock
        SystemBlock topLvl = (SystemBlock) stateMachine.getSystemModelFactory()
                .getModel().getElements().get(0);
        // Gets its sample time attribute
        double topST = topLvl.getSampleTime();

        // If it has a sample time,
        if ((topST != 0.0) && (topST != -1.0)) {
            topLvl.setStyle(FunctionStyle.FUNCTION);
            // Checks the validity of subsystems
            checkSubSystemMR(topLvl, topST);
        } else {
            // Checks the validity of subsystems
            checkSubSystemMR(topLvl, (float) -1.0);
        }
    }

    /**
     * Check multi-rating layout and validity for subsystem children of the
     * given system
     * 
     * @param sys ,
     *            the parent system
     * @param sysST ,
     *            the sample time of the parent system
     */
    private void checkSubSystemMR(SystemBlock sys, double sysST) {
        Map<Integer, Double> sampleTimes = new HashMap<Integer, Double>();
        // If parent system has a sample time
        if (sysST != -1.0) {
            // Then, saves a match between system Id and system SampleTime
            sampleTimes.put(sys.getId(), sysST);
        }

        double sT = 0;

        // For each block of the parent system
        for (Block currentBlock : sys.getBlocks()) {
            // // If the block is a SystemBlock
            // if (currentBlock instanceof SystemBlock) {
            // Retrieves its sample time
            sT = currentBlock.getSampleTime();
            // If it has one,
            if (sT != 0 && sT != -1.0) {
                if (currentBlock instanceof SystemBlock) {
                    ((SystemBlock) currentBlock).setStyle(FunctionStyle.FUNCTION);
                    // Checks the validity of subsystems
                    checkSubSystemMR((SystemBlock) currentBlock, sT);
                }
                // Saves a match between id and sample time
                sampleTimes.put(currentBlock.getId(), sT);
            } else if (currentBlock instanceof SystemBlock) {
                // Checks the validity of subsystems
                checkSubSystemMR((SystemBlock) currentBlock, (float) -1.0);
            }
            // }
            // If the block is a SequentialBlock
            // if (currentBlock instanceof SequentialBlock) {
            // sT = currentBlock.getSampleTime();
            // if (sT != -1) {
            // // Saves a match between id and sample time
            // sampleTimes.put(currentBlock.getId(), sT);
            // }
            // }
        }

        // Computes the min sample time
        double minST = Float.MAX_VALUE;
        // Integer minSTSysId = null;
        for (Integer currentSysId : sampleTimes.keySet()) {
            double currentST = sampleTimes.get(currentSysId);
            if (currentST < minST) {
                minST = currentST;
                // minSTSysId = currentSysId;
            }
        }

        // If current system has not the minimum sample time
        if (sysST != -1.0) {
            if (sysST != minST) {
                EventHandler.handle(EventLevel.CRITICAL_ERROR, this.getClass()
                        .getCanonicalName(), "", "System has not the minimal sample time.");
            }
        }

        // If at least one sample time is not a multiple of the minimum sample
        // time
        if (!multipleOf(sampleTimes, minST)) {
            EventHandler
                    .handle(EventLevel.CRITICAL_ERROR, this.getClass()
                            .getCanonicalName(), "",
                            "Sample times are not all multiple of the lowest sample time.");
        } else {
            sampleTimes.remove(sys.getId());
            // Assigns multiRate values to the SystemBlocks
            setMultiRateCoeff(sampleTimes, minST);
        }

    }

    /**
     * Browses the given map containing matches between system ids and system
     * sample times. For each system, creates a multiRateCoeff parameter which
     * is computed from the system sample time value and the minimum value of
     * all system sample times: sysMultiRateCoeff = sysSampleTime /
     * minSampleTime
     * 
     * @param sampleTimes ,
     *            the map containing matches between system ids and system
     *            sample times.
     * @param minST ,
     *            the min value of all system sample times.
     */
    private void setMultiRateCoeff(Map<Integer, Double> sampleTimes,
            double minST) {
        double factor = (1 / minST);
        for (Integer currentId : sampleTimes.keySet()) {
            Block block = (Block) stateMachine.getSystemModelFactory()
                    .getModel().getElementById(currentId);
            int coeffValue = (int) (sampleTimes.get(currentId) * factor);
            if (coeffValue != sampleTimes.get(currentId) * factor) {
                EventHandler
                        .handle(EventLevel.CRITICAL_ERROR, this.getClass()
                                .getCanonicalName(), "",
                                "Problem while trying to assign a non-integer multi-rate coefficient");
            }
            block.setSampleTime(coeffValue);
        }

    }

    /**
     * Checks if each value of the given Map is a multiple of sysST, returns
     * true in this case
     * 
     * @param sampleTimes ,
     *            the Map containing the values to check
     * @param sysST ,
     *            the factor used for the check
     * @return the result of the check
     */
    private boolean multipleOf(Map<Integer, Double> sampleTimes, double sysST) {
        boolean result = true;

        for (Integer currentId : sampleTimes.keySet()) {
            double v = sampleTimes.get(currentId);
            double invMinValue = 1 / sysST;

            double a = v * invMinValue;
            if ((int) a != (sampleTimes.get(currentId) * (1 / sysST))) {
                result = false;
            }
        }

        return result;
    }
}
