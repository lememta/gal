/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/states/TSIInitializingState.java,v $
 *  @version	$Revision: 1.23 $
 *	@date		$Date: 2011-09-27 14:30:37 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.tsimulinkimporter.main.TSimulinkImporterTool;
import geneauto.tsimulinkimporter.utils.TImporterConstant;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TSIInitializingState extends TSIState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public TSIInitializingState(TSimulinkImporterTool machine) {
        super(machine, "Initialising");
    }

    /**
     * Initialises the simulink importer members and read the main arguments
     */
    public void stateExecute() {

        // get the tool parameters.
        String inputFile = ArgumentReader.getParameter("inputFilePath");
        String outputFile = ArgumentReader.getParameter("outputFilePath");
        String blockLibrary = ArgumentReader
                .getParameter("blockLibraryFilePath");
        String constantsFile = ArgumentReader.getParameter("constantFilePath");
        String modelLibrary = ArgumentReader.getParameter("externalLibFolder");
        
        String tempPath = ArgumentReader.getParameter(GAConst.ARG_TMPFOLDER);

        boolean debug = ArgumentReader.isFlagSet(GAConst.ARG_DEBUG);
        // check for read/write permissions
        FileUtil.assertCanRead(inputFile);
        FileUtil.assertCanWrite(outputFile);
        FileUtil.assertCanWrite(tempPath);

        // Puts the output folder, library folder and definition folder paths
        // and library file in the system properties for TImporterUtilities to
        // get it
        System.setProperty(TImporterConstant.PROP_TMPFOLDER, tempPath);

        if (modelLibrary == null) {
            modelLibrary = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE_DIR); 
        }
        if (modelLibrary != null && !modelLibrary.endsWith(File.separator)) {
            modelLibrary += File.separator;
        }
        System.setProperty(TImporterConstant.PROP_LIBFOLDER, modelLibrary);
        
        if (blockLibrary != null) {
            System.setProperty(TImporterConstant.PROP_LIBFILE, blockLibrary);
        }
        if (constantsFile != null) {
            System.setProperty(TImporterConstant.PROP_CONSTFILE, constantsFile);
        }

        // Init input and output
        initTSI(inputFile, outputFile, debug);

        if (!stateMachine.getInputFile().exists()) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_INPUTNOTFOUND,
                    TImporterConstant.EMPTY);
        } else {
            stateMachine.setState(stateMachine.getParsingState());
        }

    }

    /**
     * Initialises TSI members
     * 
     * @param inputPath
     *            the input file path
     * @param outputPath
     *            the output file path
     */
    private void initTSI(String inputPath, String outputPath, boolean debugMode) {
        // Set the inputFile member to the file given as first argument of main
        stateMachine.setInputFile(new File(inputPath));
        // Set the outputFile member to the file given as second argument of
        // main
        stateMachine.setOutputFile(new File(outputPath));
        // Reset the map
        stateMachine.resetCurrentEltDesc();
        // Reset the portMatches matrice
        stateMachine.resetPortMatches();
        
        stateMachine.setDebugMode(debugMode);
        // Initializes the model factory
        initMF();
    }

    /**
     * Initializes a model factory
     */
    private void initMF() {
        // Adds the outputFile name to an output array
        List<String> outputFiles = new ArrayList<String>();
        outputFiles.add(stateMachine.getOutputFile().getName());

        // Adds the blocklibrary file path to an input array
        List<String> inputFiles = new ArrayList<String>();
        if (ArgumentReader.getParameter(GAConst.ARG_DEFLIBFILE) != null) {
            inputFiles.add(ArgumentReader.getParameter(GAConst.ARG_DEFLIBFILE));
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "",
                    "Default block library is missing");
        }
        if (System.getProperty(TImporterConstant.PROP_LIBFILE) != null) {
            inputFiles.add(System.getProperty(TImporterConstant.PROP_LIBFILE));
        }
        // Creates a new Block library factory using the input array
        // (containing blocklibrary file path)
        this.stateMachine.setBlockLibraryFactory(new BlockLibraryFactory(
                new GABlockLibrary(), TImporterConstant.TOOL_NAME, inputFiles, null));

        // Creates a new System model factory using the output array and
        // the Block library factory
		stateMachine.setSystemModelFactory(new SystemModelFactory(
				new GASystemModel(), TImporterConstant.TOOL_NAME, null,
				stateMachine.getOutputFile().getPath(), this.stateMachine
						.getBlockLibraryFactory(), null));

    }

}
