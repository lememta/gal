/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/states/TSIBuildingSMState.java,v $
 *  @version	$Revision: 1.70 $
 *	@date		$Date: 2011-09-27 14:30:37 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.ModelFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gasystemmodel.common.BlockParameter;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Outport;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.Port;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tsimulinkimporter.main.TSimulinkImporterTool;
import geneauto.tsimulinkimporter.utils.DefaultParametersMapping;
import geneauto.tsimulinkimporter.utils.MConstantDefImporter;
import geneauto.tsimulinkimporter.utils.TImporterConstant;
import geneauto.tsimulinkimporter.utils.TImporterUtilities;
import geneauto.tsimulinkimporter.utils.TestSignalUtilities;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;
import geneauto.utils.general.NameFormatter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

public class TSIBuildingSMState extends TSIState {
    
    /** 
     * Set with paths of signals that are marked as test points.
     * Every signal path is stored as follows; 
     * <block's full path>//<port's number>
     * where: 
     * <block's full path> - full path of source block
     * <port's number> - number of source port
     * 
     */
    private Set<String> testSignals = new HashSet<String>();
    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public TSIBuildingSMState(TSimulinkImporterTool machine) {
        super(machine, "BuildingSM");
    }

    public void stateExecute() {
        
        try {
            // Builds the GASystemModel and exports it in an output file
            buildGASystemModel();
        } catch (Exception e) {
            geneauto.eventhandler.EventHandler.handle(
                    EventLevel.CRITICAL_ERROR, "TSIBuildingSMState", "",
                    "Unhandled exception", e);
        }

        for (GAModelElement el : stateMachine.getSystemModelFactory()
                .getModel().getAllElements()) {
            if (el instanceof Block || el instanceof Signal) {
                el.setOriginalFullName(el.getReferenceString(!stateMachine.isDebugMode()));
            }
        }

        stateMachine.setState(stateMachine.getCheckingMultiRateState());
    }

    /**
     * Builds a GASystemModel from the model tree (AST) obtained after parsing
     */
    private void buildGASystemModel() throws Exception {
        TestSignalUtilities.init();
        
        // Retrieves the model tree
        CommonTree modelTree = stateMachine.getModelTree();

        // Retrieves the system model factory
        SystemModelFactory factory = stateMachine.getSystemModelFactory();

        /** Model field processing * */
        // Retrieves the model version
        String mversion = TImporterUtilities.getAttFromTree(modelTree,
                TImporterConstant.SK_VERSION);
        // Retrieve the model name
		// For model name we do not use the name stored in the file, but use the
		// file name instead, because the name in the .mdl is not directly
		// visible or modifiable by the user. AnTo 23.04.2011
        String fileName = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE_NAME);
        String mname    = FileUtil.getFileNameNoExt(fileName);        
        
        // Retrieves the last modifier name
        String mlastsavedby = TImporterUtilities.getAttFromTree(modelTree,
                TImporterConstant.SK_LASTMODBY);
        // Retrieves the last modification date
        String mlastsavedon = TImporterUtilities.getAttFromTree(modelTree,
                TImporterConstant.SK_LASTMODON);

        // Sets GASystemModel version attribute to the retrieved tool modelling
        // version
        if (mversion != null) {
            factory.getModel().setModelVersion(mversion);
        }
        // If model version is missing in the .mdl file
        else {
            // Adds a new log ERROR
            EventHandler.handle(EventLevel.ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_MODELVERSIONMISSING,
                    TImporterConstant.EMPTY);
        }
        // Sets GASystemModel name attribute to the retrieved model name
        if (mname != null) {
            factory.getModel().setModelName(mname);
        }
        // If model name is missing in the .mdl file
        else {
            // Adds a new log ERROR
            EventHandler.handle(EventLevel.ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_MODELNAMEMISSING,
                    TImporterConstant.EMPTY);
        }
        // Sets GASystemModel lastSavedBy attribute to the retrieved model
        // lastSavedBy
        if (mlastsavedby != null) {
            factory.getModel().setLastSavedBy(mlastsavedby);
        }
        // Sets GASystemModel lastSavedDate attribute to the retrieved model
        // lastSavedDate
        if (mlastsavedon != null) {
            factory.getModel().setLastSavedOn(mlastsavedon);
        }
        /*
         * if (mdesc != null) { factory.getModel().setModelDescription(mdesc); }
         */

        // Retrieves the graphical interface tree
        Tree graphInterfaceTree = getGraphInterfaceTree();
        
        if (graphInterfaceTree != null) {

            // Retrieves the test pointed signals subtrees
            Vector<Tree> testPointSignalTrees = TImporterUtilities.subTrees(
                    graphInterfaceTree, 
                        TImporterConstant.SK_TEST_POINTED_SIGNAL);
            for (Tree testPointSignalTree : testPointSignalTrees) {
                
                // Retrieves related block's full type
                String fullBlockPath = TImporterUtilities.getAttFromTree(
                        testPointSignalTree, 
                        TImporterConstant.SK_FULL_BLOCK_PATH);
                
                // Retrieves port's index
                String portIndexString = TImporterUtilities.getAttFromTree(
                        testPointSignalTree, 
                        TImporterConstant.SK_PORT_INDEX);
                
                // in case port index is not set it is assumed that it is 1 by default
                int portIndex = 1;
                if (portIndexString != null) {
                    try {
                        portIndex = Integer.parseInt(portIndexString.trim());
                    } catch (NumberFormatException nfe) {
                        return;
                    }
                }
                
                testSignals.add(fullBlockPath + "//" + portIndex);
            }
        }
        
        // Retrieves the block default parameters tree
        Tree defParamTree = getBlockDefParamTree();

        /** Block default parameters processes * */
        if (defParamTree != null) {

            // Retrieves the block parameters subtrees
            Vector<Tree> blockParamTrees = TImporterUtilities.subTrees(
                    defParamTree, TImporterConstant.SK_BLOCK);
            for (Tree currentBloParamTree : blockParamTrees) {
                // Retrieves the block type
                String bloType = TImporterUtilities.getAttFromTree(
                        currentBloParamTree, TImporterConstant.SK_BLOCKTYPE);
                DefaultParametersMapping.getInstance().addBlockParam(bloType,
                        TImporterUtilities.getAttMap(currentBloParamTree));
            }
        }

        // Retrieves the main system tree from the whole model tree
        Tree systemTree = stateMachine.getSystemTree();

        /** System field processing * */
        // Reset the map
        stateMachine.resetCurrentEltDesc();
        // Set root level system name equal to model name
        // the root-level subsystem name is set automatically by simulink and 
        // in many cases it is not suitable for a readable identifier name
        stateMachine.getCurrentEltDesc().put(TImporterConstant.GA_NAME, mname);
        // Adding of the System GAModelElement in GASystemModel
        SystemBlock sysElt = (SystemBlock) factory.createModelElement(
                TImporterConstant.GA_BLOCK, TImporterConstant.GA_SUBSYS, null,
                stateMachine.getCurrentEltDesc());

        // root level element must have the same name as 
        // Processes the constant definitions (if there is a mfile)
        String mFilePath = System.getProperty(TImporterConstant.PROP_CONSTFILE);
        if (mFilePath != null) {
        	MConstantDefImporter constImporter = new MConstantDefImporter();
        	List<Variable_SM> constVarList = constImporter.parseFile(
        										mFilePath, 
        										sysElt, 
        										stateMachine.getNamesMap()); 
        	sysElt.addVariables(constVarList);
        }
        factory.getModel().addElement(sysElt);

        // Adds all system elements to GASystemModel
        addSystemToGASM(systemTree, sysElt);

        // Retrieves all annotation trees
        Vector<Tree> annoTrees = TImporterUtilities.subTrees(systemTree,
                TImporterConstant.SK_ANNOTATION);
        // Adds all annotations to GASystemModel
        addAnnotationsToGASM(0, annoTrees, sysElt);
    }

    /**
     * Adds all annotations given as argument to the Annotation list of a
     * GAModelElement (also given as argument)
     * 
     * @param numberOfAnno
     *            the number of annotations already added for the given element
     * @param annoTrees
     *            the list of annotation trees to parse and add to the element
     * @param sysElt
     *            the element (mostly a system block)
     */
    private void addAnnotationsToGASM(int numberOfAnno, Vector<Tree> annoTrees,
            GAModelElement sysElt) {
        // Browses the annotation trees, for each...
        for (Tree currentTree : annoTrees) {
            // Retrieves the annotation text field
            String annoText = TImporterUtilities.getAttFromTree(currentTree,
                    TImporterConstant.SK_TEXT);

            // TODO check that the flag fromInputFile was set correctly (AnRo)
            if (annoText != null) {
                sysElt.addAnnotation(annoText, true);
            }
        }
    }

    /**
     * Creates all signal GAModelElements from a branch description
     * 
     * @param parent
     *            the parent element for GAModelElement creation
     * @param brTree
     *            the input branch tree (branch description)
     * @param gloSrcPort
     *            the global id of the signal source port
     */
    private void addBranchToGASM(GAModelElement parent, Tree brTree,
            String gloSrcPort) {
        // Retrieves the branch subtrees from the input branch tree
        Vector<Tree> subBrTrees = TImporterUtilities.subTrees(brTree,
                TImporterConstant.SK_BRANCH);
        
        // If there are branch subtrees
        if (subBrTrees.size() > 0) {
            // For each branch subtree ...
            for (int i = 0; i < subBrTrees.size(); i++) {
                // Recursive call
                addBranchToGASM(parent, subBrTrees.get(i), gloSrcPort);
            }
        }
        
        // If there is no branch subtree
        else {
            // Retrieves the "DstBlock" value
            String dstBlock = TImporterUtilities.getAttFromTree(brTree,
                    TImporterConstant.SK_DSTBLOCK);
            // Retrieves the "DstPort" value
            String dstPort = TImporterUtilities.getAttFromTree(brTree,
                    TImporterConstant.SK_DSTPORT);

            // If DstBlock is missing
            if (dstBlock == null) {
                // Adds a new log ERROR
                EventHandler.handle(EventLevel.ERROR,
                        TImporterConstant.TOOL_EVENT_ID,
                        TImporterConstant.EMPTY,
                        TImporterConstant.EVENT_ERROR_DSTBLOCKMISSING
                        	+ getBranchRefStr(brTree, parent));
                return;
            }
            // If DstPort is missing
            if (dstPort == null) {
                // Adds a new log ERROR
                EventHandler.handle(EventLevel.ERROR,
                        TImporterConstant.TOOL_EVENT_ID,
                        TImporterConstant.EMPTY,
                        TImporterConstant.EVENT_ERROR_DSTPORTMISSING
                        	+ getBranchRefStr(brTree, parent));
                return;
            }

            // Declares a String variable for storing global id
            String gloDstPort;
            if (dstBlock != null && dstPort != null) {
                // If "DstPort" value is "trigger"
                if (dstPort.equals(TImporterConstant.SK_TRIGGER)) {
                    // The port is an InputControl port (fourth parameter)
                    gloDstPort = getGlobalId(parent.getId(), dstBlock,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED, dstPort,
                            TImporterConstant.NOT_USED);
                } else if (dstPort.equals(TImporterConstant.SK_ENABLE)) {
                    // The port is an Enable port (third parameter)
                    gloDstPort = getGlobalId(parent.getId(), dstBlock,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED, dstPort,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED);
                } else {
                    // The port is an InputData port (first parameter)
                    gloDstPort = getGlobalId(parent.getId(), dstBlock, dstPort,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED);
                }
                // Reset the map
                stateMachine.resetCurrentEltDesc();
                // Reset the map
                stateMachine.getCurrentEltDesc().putAll(
                        TImporterUtilities.getAttMap(brTree));
                // Adds the source identifier to the map
                stateMachine.getCurrentEltDesc().put(
                        TImporterConstant.GA_SRCPORT, gloSrcPort);
                // Adds the destination identifier to the map
                stateMachine.getCurrentEltDesc().put(
                        TImporterConstant.GA_DSTPORT, gloDstPort);

                // Adding of the Signal GAModelElement
                GAModelElement elt = stateMachine.getSystemModelFactory().createModelElement(
                        TImporterConstant.GA_SIGNAL,
                        TImporterConstant.GA_SIGNAL_LIST, parent,
                        stateMachine.getCurrentEltDesc());
                checkSignalIsObservable(elt);
                
            } else {
                EventHandler
                        .handle(
                                EventLevel.ERROR,
                                TImporterConstant.TOOL_EVENT_ID,
                                TImporterConstant.EMPTY,
                                "There is an invalid line description in system : "
                                        + getBranchRefStr(brTree, parent)
                                        + "\n You should check your model that there are no unused branch points.");
            }
        }
    }
    
    /**
     * Checks if signal is observable or not: 
     * - In case full path of the source block and source port number are 
     * the same as one from testSignals collection flag isObservable is set to true
     * 
     * @param elt Signal to be checked
     */
    private void checkSignalIsObservable(GAModelElement elt) {
        if (elt instanceof Signal) {
            Signal signal = (Signal) elt;
            Port srcPort = signal.getSrcPort();
            Block sourceBlock = (Block) srcPort.getParent();
            String signalString = TestSignalUtilities
                .getBlockFullPath(sourceBlock) + "//" + srcPort.getPortNumber();
            if (testSignals.contains(signalString)) {
                signal.addObservationPoint(mkObsPointName(signal));
            }
        }
    }

    private String mkObsPointName(Signal signal) {
    	// Similar approach, as creating a variable name from Signal, 
    	// but repeated here to have more flexibility.
    	
        // Retrieves the signal name or if there is not creates a new name
        String varName;
        if (signal.getName() != null) {
        	// Use the signal name
            varName = signal.getName();
        } else {
            // signal does not have a name -- use the name of source block
            varName = signal.getSrcPort().getParent().getName();

            /*
             * If source port has a name, append it to the variable name NB!
             * here we assume that the tool takes care of assigning unique names
             * to all ports before this point
             */
            Outport srcPort = signal.getSrcPort();
			if (srcPort.getName() != null
                    && !srcPort.getName().isEmpty()) {
                varName += "_" + srcPort.getName();
            } else {
            	if (((Block) srcPort.getParent()).getOutDataPorts().size() > 1) {
            		varName += "_" + srcPort.getPortNumber();
            	}
            }
        }
        
		// Prefix it with the subsystem name to reduce possibility of name conflicts
    	varName = signal.getParent().getName() + "_" + varName;        	

        // make sure the variable is a legal identifier
        // NB! even if variable normalisation is done once more in the
        // printer, this step here is important because of possible
        // periods (.) in the name. Generic normalising process in the printer
        // does not know whether the period means reference to a structure 
        // element is is just a replaceable symbol. Here we know, that the
        // signal variable shall not contain any periods.
        varName = NameFormatter.normaliseVariableName(varName, 
        		"CMGConvertingModelState.addVarFromSignal", true);
        
        return varName;
	}

	/**
     * Parses a system tree and adds all corresponding GAModelElement to GASM
     * 
     * @param t
     *            the system tree to process
     */
    private void addSystemToGASM(Tree t, GAModelElement parent)
            throws Exception {
        // Retrieves the system model factory
        ModelFactory factory = stateMachine.getSystemModelFactory();

        /** Block field processing* */
        // Retrieves the block subtrees in the system
        Vector<Tree> blockTrees = TImporterUtilities.subTrees(t,
                TImporterConstant.SK_BLOCK);
        
        // If System has no block
        if (blockTrees.size() == 0) {
            // Adds a new log ERROR
            EventHandler.handle(EventLevel.INFO,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    "System " + parent.getReferenceString() + " has no block");
        }
        
        // Browse all the block subtrees, for each ...
        for (int i = 0; i < blockTrees.size(); i++) {
            // Retrieves the "BlockType" value, here it is already
            // added to the map but this value will be used next
            String bloType = TImporterUtilities.getAttFromTree(blockTrees
                    .get(i), TImporterConstant.SK_BLOCKTYPE);
            // Retrieves the "Name" value, here it is already
            // added to the map but this value will be used next
            String bloName = TImporterUtilities.getAttFromTree(blockTrees
                    .get(i), TImporterConstant.SK_NAME);

            // Retrieves the "Position" value,
            String bloPos = TImporterUtilities.getAttFromTree(
                    blockTrees.get(i), TImporterConstant.SK_POSITION);

            // Retrieves the "Description" value
            String bloDesc = TImporterUtilities.getAttFromTree(blockTrees
                    .get(i), TImporterConstant.SK_DESCRIPTION);

            // Retrieves the "MaskType" value
            String bloMaskType = TImporterUtilities.getAttFromTree(blockTrees
                    .get(i), TImporterConstant.SK_MASK_TYPE);

            addBlockToGASM(factory, parent, blockTrees.get(i), bloType,
                    bloName, bloPos, bloDesc, bloMaskType);
        }

        /** Line field processing * */
        // Retrieve the line subtrees in the system
        Vector<Tree> lineTrees = TImporterUtilities.subTrees(t,
                TImporterConstant.SK_LINE);
        
        // Browse all the line subtrees, for each...
        for (int i = 0; i < lineTrees.size(); i++) {
            Tree line = lineTrees.get(i);
            // Retrieves the "SrcBlock" value
            String srcBlock = TImporterUtilities.getAttFromTree(
            		line, TImporterConstant.SK_SRCBLOCK);
			// Retrieves the "SrcPort" value
            String srcPort = TImporterUtilities.getAttFromTree(
            		line, TImporterConstant.SK_SRCPORT);

            // If SrcPort is missing
            if (srcPort == null) {
                // Adds a new log ERROR
            	
                EventHandler.handle(EventLevel.ERROR,
                        TImporterConstant.TOOL_EVENT_ID,
                        TImporterConstant.EMPTY,
                        TImporterConstant.EVENT_ERROR_SRCPORTMISSING + getLineRefStr(line, parent));
                continue; // Skip and continue with processing the next Line object
            }
            // If SrcBlock is missing
            if (srcBlock == null) {
                // Adds a new log ERROR
                EventHandler.handle(EventLevel.ERROR,
                        TImporterConstant.TOOL_EVENT_ID,
                        TImporterConstant.EMPTY,
                        TImporterConstant.EVENT_ERROR_SRCBLOCKMISSING + getLineRefStr(line, parent));
                continue; // Skip and continue with processing the next Line object
            }

            // Retrieves with the "SrcBlock" value and the "SrcPort value"
            // the global id of the port from the portMatches matrix
            String gloSrcPort = null;
            gloSrcPort = getGlobalId(parent.getId(), srcBlock,
                    TImporterConstant.NOT_USED, srcPort,
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED);
            if (gloSrcPort == null) {
                gloSrcPort = getGlobalId(parent.getId(), srcBlock,
                        TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                        TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                        srcPort);
            }

            /** Branch field processing * */
            // Retrieves all the branch subtrees in the system
            Vector<Tree> branchTrees = TImporterUtilities.subTrees(
            		line, TImporterConstant.SK_BRANCH);

            // pass the signal name to the branch
            Tree lineName = line.getChild(0);
            for (Tree tree : branchTrees) {
                tree.addChild(lineName);
            }

            // If there is at least one branch subtree
            if (branchTrees.size() > 0) {
                // Browse all the branch subtrees, for each...
                for (int j = 0; j < branchTrees.size(); j++) {
                    addBranchToGASM(parent, branchTrees.get(j), gloSrcPort);
                }
            }
            // If there is no branch in this line description
            else {
                // Retrieves the "DstBlock" value
                String dstBlock = TImporterUtilities.getAttFromTree(
                		line, TImporterConstant.SK_DSTBLOCK);
                // Retrieves the "DstPort" value
                String dstPort = TImporterUtilities.getAttFromTree(
                		line, TImporterConstant.SK_DSTPORT);

                // If DstBlock is missing
                if (dstBlock == null) {
                    // Adds a new log ERROR
                    EventHandler.handle(EventLevel.ERROR,
                            TImporterConstant.TOOL_EVENT_ID,
                            TImporterConstant.EMPTY,
                            TImporterConstant.EVENT_ERROR_DSTBLOCKMISSING + getLineRefStr(line, parent));
                    continue; // Skip and continue with processing the next Line object
                }
                // If DstPort is missing
                if (dstPort == null) {
                    // Adds a new log ERROR
                    EventHandler.handle(EventLevel.ERROR,
                            TImporterConstant.TOOL_EVENT_ID,
                            TImporterConstant.EMPTY,
                            TImporterConstant.EVENT_ERROR_DSTPORTMISSING + getLineRefStr(line, parent));
                    continue; // Skip and continue with processing the next Line object
                }

                // Declares a String variable for storing global id
                String gloDstPort;

                // Retrieves with the "DstBlock" value and the "DstPort value"
                // the global id of the port from the portMatches matrix
                // If "DstPort" value is "trigger"
                if (dstPort.equals(TImporterConstant.SK_TRIGGER)
                        || (dstPort.equals(TImporterConstant.SK_IFACTION))) {
                    // The port is an InputControl port (fourth parameter)
                    gloDstPort = getGlobalId(parent.getId(), dstBlock,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED, dstPort,
                            TImporterConstant.NOT_USED);
                } else if (dstPort.equals(TImporterConstant.SK_ENABLE)) {
                    // The port is an Enable port (third parameter)
                    gloDstPort = getGlobalId(parent.getId(), dstBlock,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED, dstPort,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED);
                } else {
                    // The port is an InputData port (first parameter)
                    gloDstPort = getGlobalId(parent.getId(), dstBlock, dstPort,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED,
                            TImporterConstant.NOT_USED);
                }
                // Reset the map
                stateMachine.resetCurrentEltDesc();
                // Reset the map and initializes it with all the branch fields
                // <FieldName,Value>
                stateMachine.getCurrentEltDesc().putAll(
                        TImporterUtilities.getAttMap(line));
                // Adds the source identifier to the map
                stateMachine.getCurrentEltDesc().put(
                        TImporterConstant.GA_SRCPORT, gloSrcPort);
                // Adds the destination identifier to the map
                stateMachine.getCurrentEltDesc().put(
                        TImporterConstant.GA_DSTPORT, gloDstPort);

                // Adding of the Signal GAModelElement
                GAModelElement elt = factory.createModelElement(TImporterConstant.GA_SIGNAL,
                        TImporterConstant.GA_SIGNAL_LIST, parent, stateMachine
                                .getCurrentEltDesc());
                checkSignalIsObservable(elt);
            }
        }
        // checkPorts(parent);
    }

    private void addBlockToGASM(ModelFactory factory, GAModelElement parent,
            Tree blockTree, String bloType, String bloName, String bloPos,
            String bloDesc, String bloMaskType) throws Exception {
        // Reset the map
        stateMachine.resetCurrentEltDesc();
        // Reset the map and initialises it with all the default block
        // parameters
        stateMachine.getCurrentEltDesc().putAll(
                DefaultParametersMapping.getInstance().getBlockParam(bloType));

        // Updates the map with all the block fields
        // <FieldName,Value>
        stateMachine.getCurrentEltDesc().putAll(
                TImporterUtilities.getAttMap(blockTree));

        // If BlockType is missing
        if (bloType == null) {
            // Adds a new log ERROR
            EventHandler.handle(EventLevel.ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_BLOCKTYPEMISSING,
                    TImporterConstant.EMPTY);
        }
        // If Name is missing
        if (bloName == null) {
            // Adds a new log ERROR
            EventHandler.handle(EventLevel.ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_BLOCKNAMEMISSING,
                    TImporterConstant.EMPTY);
        }
        // If Position is missing
        if (bloPos == null) {
            // Adds a new log ERROR
            EventHandler.handle(EventLevel.ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_BLOCKPOSITIONMISSING,
                    TImporterConstant.EMPTY);
        }

        // Initialises a two-String table
        String[] bloPosXY = new String[2];
        // Parses the "Position" value into 2 String : posX,posY
        bloPosXY = TImporterUtilities.parsePos(bloPos);
        // Computes the sizeX and sizeY values
        int[] sizeXY = new int[2];
        sizeXY = TImporterUtilities.computeSize(bloPos);

        // Adds the parsed position values in the map
        stateMachine.getCurrentEltDesc().put(TImporterConstant.GA_POSX,
                bloPosXY[TImporterConstant.POS_X]);
        stateMachine.getCurrentEltDesc().put(TImporterConstant.GA_POSY,
                bloPosXY[TImporterConstant.POS_Y]);

        // Adds the size values in the map
        stateMachine.getCurrentEltDesc().put(TImporterConstant.GA_SIZEX,
                Integer.toString(sizeXY[TImporterConstant.POS_X]));
        stateMachine.getCurrentEltDesc().put(TImporterConstant.GA_SIZEY,
                Integer.toString(sizeXY[TImporterConstant.POS_Y]));

        // check if the block type shall be changed according to mask'
        if (bloMaskType != null) {
        	bloType = getBlockTypeByMask(bloType, bloMaskType);
        }
        
        // The block type given as argument for GAModelElement creation
        // must be Subsystem if it is a Reference block
        String eltBloType = bloType;
        // memorises that the block is reference
        Boolean isRef = false;
        // memorises that the block is model reference
        Boolean isModelRef = false;
        if (bloType.equals(TImporterConstant.SK_REFERENCE)) {
            eltBloType = "SubSystem";
            isRef = true;
        } else if (bloType.equals("ModelReference")) {
        	isRef = true;
            isModelRef = true;
            eltBloType = "SubSystem";
        }

        // tree for storing reference contents
        Tree referenceContentsTree = null;
        if (isRef) {
        	// retrieve the tree corresponding to block contents
        	referenceContentsTree = TImporterUtilities.resolveRef(
        												blockTree, bloType);
        	// when no tree was retrieved, mark block as generic block
        	// FMPreProcessor will try later to match it with a library block 
        	if (referenceContentsTree == null) {
        		isRef = false;
        		isModelRef = false;
        		eltBloType = TImporterUtilities.getRefSourceName(blockTree, 
        																bloType);
        	} else {
        		// check if the references subsystem has mask
        		bloMaskType = TImporterUtilities.getAttFromTree(
        										referenceContentsTree, 
        										TImporterConstant.SK_MASK_TYPE);
        		// transfer the unique parameters from the reference block 
        		// to the block in this model
        		TImporterUtilities.updateAttMap(referenceContentsTree, 
        									stateMachine.getCurrentEltDesc());
        		
        		// check if the mask type changes the block type
                if (bloMaskType != null) {
                	eltBloType = getBlockTypeByMask(eltBloType, bloMaskType);
                	// when block element type is changed, change also the 
                	// blockType variable and set isReference to false
                	if (!(eltBloType.equals("SubSystem"))) {
                		bloType = eltBloType;
                		isRef = false;
                	}
                }
        	}
        }
        
        // Adding of the Block GAModelElement in GASystemModel
        Block bloElt = (Block) factory.createModelElement(
                TImporterConstant.GA_BLOCK, eltBloType, parent, stateMachine
                        .getCurrentEltDesc());

        // add mask variables from the block definition as block parameters 
        processMaskedSubSystem(null, blockTree, bloElt);
        
        // If the block is a resolved reference (has referenceContentsTree)
        // and is not model reference, append mask variables from 
        // the referenced subsystem
        // We do not check isRef here in purpose -- when mask changes block
        // type, the isRef flag is taken off already, but we still need mask 
        // parameters from the block
        if (!isModelRef && referenceContentsTree != null) {
            processMaskedSubSystem(blockTree, referenceContentsTree, bloElt);        	            
        }

        // imported system via ModelReference is set a non-virtual
        if (isModelRef) {
            bloElt.addParameter("AtomicSubSystem", "on");
        }

        // set the mask type -- this will be used for determining the block
        // final type
        bloElt.setMaskType(bloMaskType);

        /** Description field processing * */
        // If a description field is found, ...
        if (bloDesc != null) {
            // Creates a new Annotation
            Annotation anno = new Annotation(0, bloDesc, null, true);
            bloElt.addAnnotation(anno);
        }

        /** Port field processing * */
        // Retrieves the "Ports" value
        String bloPorts = TImporterUtilities.getAttFromTree(blockTree,
                TImporterConstant.SK_PORTS);
        // If there a "Ports" field and if "Ports" value is not "[]"
        if (bloPorts != null
                && !bloPorts.equals(TImporterConstant.SK_EMPTY_PORTS)) {

            // Creates and adds port elements in the GASystemModel
            addPortsToGASM(bloType, bloElt, bloName, bloPorts);
        }
        // If there is no "Ports" field or if "Ports" value is "[]"
        // then the "Ports" field is implicit but must be explicited
        else {
            // If the block type is "Inport" or "TriggerPort"
            if (bloType.equals(TImporterConstant.SK_INPORT)
                    || bloType.equals(TImporterConstant.SK_DATASTOREREAD)
                    || bloType.equals(TImporterConstant.SK_FROM)
                    || bloType.equals(TImporterConstant.SK_TRIGGERPORT)
                    || bloType.equals(TImporterConstant.SK_CONSTANT)
                    || bloType.equals(TImporterConstant.SK_FROMWORKSPACE)
                    || bloType.equals(TImporterConstant.SK_GROUND)) {
                // Creates a "Ports" value for
                // 0 input port, 1 output port
                String inportBloPorts = TImporterConstant.SK_SOURCE_PORTS;
                // Creates and adds port elements in the GASystemModel
                addPortsToGASM(bloType, bloElt, bloName, inportBloPorts);
            }
            // If the block type is "Outport"
            else if (bloType.equals(TImporterConstant.SK_OUTPORT)
                    || bloType.equals(TImporterConstant.SK_GOTO)
                    || bloType.equals(TImporterConstant.SK_SCOPE)
                    || bloType.equals(TImporterConstant.SK_TERMINATOR)
                    || bloType.equals(TImporterConstant.SK_TOWORKSPACE)) {
                // Creates a "Ports" value for
                // 1 input port, 0 output port
                String outportBloPorts = TImporterConstant.SK_SINK_PORTS;
                // Creates and adds port elements in the GASystemModel
                addPortsToGASM(bloType, bloElt, bloName, outportBloPorts);
            }
            // If the block type is "RelationalOperator"
            else if (bloType.equals(TImporterConstant.SK_RELATIONAL_OP)) {
                // Creates a "Ports" value for
                // 2 input ports, 1 output port
                String outportBloPorts = TImporterConstant.SK_RELATIONNAL_OP_PORTS;
                // Creates and adds port elements in the GASystemModel
                addPortsToGASM(bloType, bloElt, bloName, outportBloPorts);
            }
            // If the block type is "ActionPort"
            else if (bloType.equals(TImporterConstant.SK_ACTIONPORT)
                    || bloType.equals(TImporterConstant.SK_ENABLEPORT)
                    || bloType.equals(TImporterConstant.SK_DATASTOREMEMORY)) {
                // Creates a "Ports" value for
                // input ports, 0 output port
                String outportBloPorts = TImporterConstant.SK_ACTIONPORT_PORTS;
                // Creates and adds port elements in the GASystemModel
                addPortsToGASM(bloType, bloElt, bloName, outportBloPorts);
            }
            // If the block type is "ActionPort"
            else if (bloType.equals(TImporterConstant.SK_SWITCH)) {
                // Creates a "Ports" value for
                // 2 input ports, 1 output port
                String outportBloPorts = TImporterConstant.SK_SWITCH_PORTS;
                // Creates and adds port elements in the GASystemModel
                addPortsToGASM(bloType, bloElt, bloName, outportBloPorts);
            }
            // Else the block has default ports
            else if (bloPorts == null) {
                // Creates a "Ports" value for
                // 1 input port, 1 output port
                String defaultBloPorts = TImporterConstant.SK_DEFAULT_PORTS;
                // Creates and adds port elements in the GASystemModel
                addPortsToGASM(bloType, bloElt, bloName, defaultBloPorts);
            }
        }

        /** Subsystem processing * */
        if (isRef || bloType.equals(TImporterConstant.SK_SUBSYS)) {
            Tree tree = null;
            // If the block type is "SubSystem" --> recursive system
            // processing
            if (bloType.equals(TImporterConstant.SK_SUBSYS)) {
                // Retrieves the System description
                tree = TImporterUtilities.subTrees(blockTree,
                        TImporterConstant.SK_SYSTEM).firstElement();
            }
            // If the block type is "Reference" -->
            // --> recursive system processing
            else {
                // when the referenced element contains subtrees, take the 
            	// subtree, otherwise the element itself
            	// TODO: clarify why this is necessary (ToNa 10/12/09)
                if (!TImporterUtilities.subTrees(referenceContentsTree,
                        TImporterConstant.SK_SYSTEM).isEmpty()) {
                    tree = TImporterUtilities.subTrees(referenceContentsTree,
                            TImporterConstant.SK_SYSTEM).firstElement();
                } else {
                    tree = referenceContentsTree;                	
                }
            }

            // Adds system elements to the GASystemModel
        	if (tree.getText().equals(TImporterConstant.SK_SYSTEM)) {
        		addSystemToGASM(tree, bloElt);
        	}

        	// Retrieves all annotation trees
        	Vector<Tree> annoTrees = TImporterUtilities.subTrees(tree,
                TImporterConstant.SK_ANNOTATION);
        	// Adds all annotations to GASystemModel
        	addAnnotationsToGASM(((Block) bloElt).getAnnotations().size(),
        			annoTrees, bloElt);
        }
    }

    /**
     * Reads mask parameters from the tree and adds as block parameter
     * 
     * @param refTree	AST of the referring block (in case of a Reference 
     * 					block only)
     * @param blockTree	AST of the parsed block
     * @param bloElt	the block where attributes shall be added
     */
    private void processMaskedSubSystem(Tree refTree, Tree blockTree,
            Block bloElt) {
        // Retrieves the Mask variable and value fields (if there are)
        String maskVars = TImporterUtilities.getAttFromTree(blockTree,
                TImporterConstant.SK_MASK_VARIABLES);
        String maskValues = TImporterUtilities.getAttFromTree(blockTree,
                TImporterConstant.SK_MASK_VALUES);

        if (maskVars != null && maskValues != null) {
            Map<String, BlockParameter> maskMap = null;
            if (refTree != null) {
                // Parses the mask variables to get a map of variable/value
            	// check for existence of similarly named parameter in  
            	// the referring block
                maskMap = getMaskMap(maskVars, maskValues, bloElt, refTree);
            } else {
                // Parses the mask variables to get a map of variable/value
            	// check for existence of similarly named parameter in  
            	// the parsed block
                maskMap = getMaskMap(maskVars, maskValues, bloElt, refTree);
            }
            
            // attach the parameters to block
            for (BlockParameter param : maskMap.values()) {
                // attach parameter to block
            	bloElt.addParameter(param);
            }
        }
    }

    /**
     * Parses the MaskVariables and MaskValueString fields and creates a Map.
     * key of the map is the parameter name, value is BlockParameter with 
     * corresponding value and parameter index in MaskValue string as parameter 
     * index
     * 
     * Example :
     * 
     * MaskVariables "UP=@1;den=@2;" MaskValueString "3|8"
     * 
     * Result map : {UP = 3 ; den = 8}
     * 
     * @param maskedVars ,
     *            the MaskVariables field value
     * @param maskValues ,
     *            the MaskValueString field value
     * @return the created Map
     */
    private Map<String, BlockParameter> getMaskMap(String maskVars, 
            String maskValues, Block block, Tree bloTree) {
        Map<String, BlockParameter> resultMap = 
        								new HashMap<String, BlockParameter>();
        
        if (maskVars == null || maskValues == null) {
        	return resultMap;
        }
        
        String[] maskVarArray = maskVars.split(";");
        String[] maskValueArray = maskValues.split("\\|");
        String maskValue;
        String maskVarName;
        int maskVarIndex;
        BlockParameter param = null;
        
        for (String maskVar: maskVarArray) {
            maskValue = null;
            maskVarName = null;
            param = null;
            /*
             * Mask variable string must match one 
             * of the following patterns:
             * <varName>=&<index> or <varName>=@<index>
             * If not - print an error.
             */
            if (!maskVar.matches(".+=[\\@|\\&]\\d+")) {
                EventHandler.handle(
                        EventLevel.ERROR, 
                        "getMaskMap", 
                        "", 
                        "mask variable string must match one of the following patterns: "
                            + "<varName>=&<index> or <varName=@<index>"
                            + "\nMask variable string: " + maskVar);
                continue;
            }
            maskVarName = maskVar.substring(0, maskVar.indexOf("="));
            maskVarIndex = Integer.parseInt(
                    maskVar.substring(maskVar.indexOf("=") + 2));
            /*
             * Print an error if index > length of 
             * maskValueArray
             */
            if (maskValueArray.length < maskVarIndex) {
                EventHandler.handle(
                        EventLevel.ERROR, 
                        "getMaskMap", 
                        "", 
                        "mask variable's index (" + maskVarIndex
                            + ") > length of mask value array (" 
                            + maskValueArray.length + ")");
                continue;
            }
            
            /*
             * if there is attribute in the reference tree 
             * with name = maskVarName -> use it as maskValue
             * otherwise use corresponding element of 
             * maskValueIndex
             */
            if (bloTree != null && maskVarName != null) {
                maskValue = TImporterUtilities.getAttFromTree(bloTree, maskVarName);
            }
            if (maskValue == null) {
                maskValue = maskValueArray[maskVarIndex - 1];
            }
            
            // Remove starting and ending quotes, if present. 
			// NOTE: This assumes that we must have other means to identify that
			// the parameter value is indeed a string and should be treated so 
            if (maskValue != null && maskValue.startsWith("\"") && maskValue.endsWith("\"")) {
            	maskValue = maskValue.substring(1, maskValue.length()-1);
            }            
            
            param = new BlockParameter(new StringExpression(maskValue), maskVarName);
            param.setIndex(maskVarIndex);
            resultMap.put(maskVarName, param);
        }
        
        return resultMap;
    }

    /**
     * Browses the portMatches matrix and seek for the port global id with the
     * specified block name, and a specified local id
     * 
     * @param subsName ,
     *            the subsystem name
     * @param bloName ,
     *            the block name of the port
     * @param locInId ,
     *            the local id of the port in the block, if is a InDataPort (0
     *            else)
     * @param locOutId ,
     *            the local id of the port in the block, if is a OutDataPort (0
     *            else)
     * @param locEnId ,
     *            the local id of the port in the block, if is a EnablePort (0
     *            else)
     * @param locInCtrId ,
     *            the local id of the port in the block, if is a InControlPort
     *            (0 else)
     * @param locOutCtrId ,
     *            the local id of the port in the block, if is a OutControlPort
     *            (0 else)
     * @return the global Id
     */
    private String getGlobalId(Integer subId, String bloName, String locInId,
            String locOutId, String locEnId, String locInCtrId,
            String locOutCtrId) {
        String gloId = null;
        Vector<String[]> ports = stateMachine.getPortMatches();
        // Browse the port matches, for each...
        for (int i = 0; i < ports.size(); i++) {
            // If local ids and block name match
            if (ports.get(i)[TImporterConstant.BLOCK_NAME].equals(bloName)
                    && ports.get(i)[TImporterConstant.LOCAL_IN_ID]
                            .equals(locInId)
                    && ports.get(i)[TImporterConstant.LOCAL_OUT_ID]
                            .equals(locOutId)
                    && ports.get(i)[TImporterConstant.LOCAL_EN_ID]
                            .equals(locEnId)
                    && ports.get(i)[TImporterConstant.LOCAL_INCTRL_ID]
                            .equals(locInCtrId)
                    && ports.get(i)[TImporterConstant.LOCAL_OUTCTRL_ID]
                            .equals(locOutCtrId)
                    && ports.get(i)[TImporterConstant.SUBS_ID].equals(String
                            .valueOf(subId)))
                // Retrieves the port global id
                gloId = ports.get(i)[TImporterConstant.GLOBAL_ID];
        }
        return gloId;
    }

    /**
     * Parses the bloPorts String parameter and creates port elements in GASM
     * 
     * @param bloPorts
     *            the bloPorts String to parse and use to fill GASM
     * @param parent
     *            the parent Block
     * @param bloName
     *            the name of the Block
     */
    private void addPortsToGASM(String bloType, GAModelElement parent,
            String bloName, String bloPorts) {
        // Creates a five-String table
        String[] bloPortsTab = new String[8];
        // Parses the "Ports" field
        // and fill the table with the number of each kind of port
        bloPortsTab = TImporterUtilities.parsePorts(bloPorts);

        // Retrieves the factory
        ModelFactory factory = stateMachine.getSystemModelFactory();

        // Converts all Strings to integers
        int nbID = Integer.parseInt(bloPortsTab[TImporterConstant.INPORT_DATA]);
        int nbOD = Integer
                .parseInt(bloPortsTab[TImporterConstant.OUTPORT_DATA]);
        int nbIE = Integer
                .parseInt(bloPortsTab[TImporterConstant.INPORT_ENABLE]);
        int nbIC = Integer
                .parseInt(bloPortsTab[TImporterConstant.INPORT_CONTROL]);
        int nbOC = Integer
                .parseInt(bloPortsTab[TImporterConstant.OUTPORT_CONTROL]);
        int nbIA = Integer
                .parseInt(bloPortsTab[TImporterConstant.INPORT_ACTION]);

        if (bloType.equals(TImporterConstant.SK_IF)) {
            nbOC = nbOD;
            nbOD = 0;
        }
        // Loop for Input data ports
        for (int j = 0; j < nbID; j++) {
            // Reset the map
            stateMachine.resetCurrentEltDesc();
            // Adds the input data port id to the map
            stateMachine.getCurrentEltDesc().put(
                    TImporterConstant.GA_PORTNUMBER, Integer.toString(j + 1));

            // Adds the Port GAModelElement to the GASystemModel
            GAModelElement portElt = factory.createModelElement(
                    TImporterConstant.GA_INDATAPORT,
                    TImporterConstant.GA_INDATAPORT_LIST, parent, stateMachine
                            .getCurrentEltDesc());

            // Adds the block name, local port and global port to portMatches
            // matrice
            addPortMatches(((GAModelElement) parent.getParent()).getId(),
                    bloName, Integer.toString(j + 1),
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    Integer.toString(portElt.getId()));
        }

        // Loop for Output data ports
        for (int j = 0; j < nbOD; j++) {
            // Reset the map
            stateMachine.resetCurrentEltDesc();
            // Adds the input data port id to the map
            stateMachine.getCurrentEltDesc().put(
                    TImporterConstant.GA_PORTNUMBER, Integer.toString(j + 1));

            // Adding of the Port GAModelElement
            GAModelElement portElt = factory.createModelElement(
                    TImporterConstant.GA_OUTDATAPORT,
                    TImporterConstant.GA_OUTDATAPORT_LIST, parent, stateMachine
                            .getCurrentEltDesc());

            // Adding block name, local port and global port to portMatches
            // matrice
            addPortMatches(((GAModelElement) parent.getParent()).getId(),
                    bloName, TImporterConstant.NOT_USED, Integer
                            .toString(j + 1), TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    Integer.toString(portElt.getId()));
        }

        // Loop for Input Enable ports
        for (int j = 0; j < nbIE; j++) {
            // Reset the map
            stateMachine.resetCurrentEltDesc();
            // Adds the input data port id to the map
            stateMachine.getCurrentEltDesc().put(
                    TImporterConstant.GA_PORTNUMBER, Integer.toString(j + 1));

            // Adding of the Port GAModelElement
            GAModelElement portElt = factory.createModelElement(
                    TImporterConstant.GA_ENABLEPORT,
                    TImporterConstant.GA_ENABLEPORT_LIST, parent, stateMachine
                            .getCurrentEltDesc());

            // Adding block name, local port and global port to portMatches
            // matrice
            addPortMatches(((GAModelElement) parent.getParent()).getId(),
                    bloName, TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED, TImporterConstant.SK_ENABLE,
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    Integer.toString(portElt.getId()));
        }

        // Loop for Input Control ports
        for (int j = 0; j < nbIC; j++) {
            // Reset the map
            stateMachine.resetCurrentEltDesc();
            // Adds the input data port id to the map
            stateMachine.getCurrentEltDesc().put(
                    TImporterConstant.GA_PORTNUMBER, Integer.toString(j + 1));

            // Adding of the Port GAModelElement
            GAModelElement portElt = factory.createModelElement(
                    TImporterConstant.GA_EDGEENABLEPORT,
                    TImporterConstant.GA_EDGEENABLEPORT_LIST, parent,
                    stateMachine.getCurrentEltDesc());

            // Adding block name, local port and global port to portMatches
            // matrice
            addPortMatches(((GAModelElement) parent.getParent()).getId(),
                    bloName, TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    TImporterConstant.SK_TRIGGER, TImporterConstant.NOT_USED,
                    Integer.toString(portElt.getId()));
        }

        // Loop for Output Control ports
        for (int j = 0; j < nbOC; j++) {
            // Reset the map
            stateMachine.resetCurrentEltDesc();
            // Adds the input data port id to the map
            stateMachine.getCurrentEltDesc().put(
                    TImporterConstant.GA_PORTNUMBER, Integer.toString(j + 1));

            // Adding of the Port GAModelElement
            GAModelElement portElt = factory.createModelElement(
                    TImporterConstant.GA_OUTCONTROLPORT,
                    TImporterConstant.GA_OUTCONTROLPORT_LIST, parent,
                    stateMachine.getCurrentEltDesc());

            // Adding block name, local port and global port to portMatches
            // matrice
            addPortMatches(((GAModelElement) parent.getParent()).getId(),
                    bloName, TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED, Integer.toString(j + 1),
                    Integer.toString(portElt.getId()));

        }

        // Loop for input Action ports
        for (int j = 0; j < nbIA; j++) {
            // Reset the map
            stateMachine.resetCurrentEltDesc();
            // Adds the input data port id to the map
            stateMachine.getCurrentEltDesc().put(
                    TImporterConstant.GA_PORTNUMBER, Integer.toString(j + 1));

            // Adding of the Port GAModelElement
            // Adding of the Port GAModelElement
            GAModelElement portElt = factory.createModelElement(
                    TImporterConstant.GA_INCONTROLPORT,
                    TImporterConstant.GA_INCONTROLPORT_LIST, parent,
                    stateMachine.getCurrentEltDesc());

            // Adding block name, local port and global port to portMatches
            // matrice
            addPortMatches(((GAModelElement) parent.getParent()).getId(),
                    bloName, TImporterConstant.NOT_USED,
                    TImporterConstant.NOT_USED, TImporterConstant.NOT_USED,
                    TImporterConstant.SK_IFACTION, TImporterConstant.NOT_USED,
                    Integer.toString(portElt.getId()));

        }
    }

    private CommonTree getBlockDefParamTree() {
        return getParamTree(TImporterConstant.SK_PARAM_DEFAULTS);
    }
    
    private CommonTree getGraphInterfaceTree(){
        return getParamTree(TImporterConstant.SK_GRAPH_INTERFACE);
    }
    
    /**
     * 
     * @param param predefined parameter
     * 
     * @return tree for predefined parameter
     */
    private CommonTree getParamTree(String param) {
     // If there is no param tree
        if (TImporterUtilities.subTrees(stateMachine.getModelTree(),
                param).isEmpty()) {
            // Returns null
            return null;
        }
        // If there is a param tree
        else {
            // Returns the param tree
            return (CommonTree) TImporterUtilities.subTrees(
                    stateMachine.getModelTree(),
                    param).firstElement();
        }
    }

    /**
     * Adds matches between a local port id, the name of his block, and his
     * global id into the portMatches matrix
     * 
     * @param bloName
     *            the name of the block
     * @param inid
     *            the local in id of the port in the block
     * @param outid
     *            the local out id of the port in the block
     * @param id
     *            the global id of the port
     */
    private void addPortMatches(Integer subId, String bloName, String inid,
            String outid, String eid, String inctrid, String outctrid, String id) {
        // Declares a seven-String table
        String[] portMatch = new String[8];
        // 1rst element is filled with Block name
        portMatch[TImporterConstant.BLOCK_NAME] = bloName;
        // 2nd element is filled with local InData id
        portMatch[TImporterConstant.LOCAL_IN_ID] = inid;
        // 3rd element is filled with local OutData id
        portMatch[TImporterConstant.LOCAL_OUT_ID] = outid;
        // 4th element is filled with local Enable id
        portMatch[TImporterConstant.LOCAL_EN_ID] = eid;
        // 5th element is filled with local InControl id
        portMatch[TImporterConstant.LOCAL_INCTRL_ID] = inctrid;
        // 6th element is filled with local OutControl id
        portMatch[TImporterConstant.LOCAL_OUTCTRL_ID] = outctrid;
        // 7th element is filled with the global id
        portMatch[TImporterConstant.GLOBAL_ID] = id;
        // 8th element is filled with the subsystem name
        portMatch[TImporterConstant.SUBS_ID] = String.valueOf(subId);

        // The String table is added to the portMatches matrice
        stateMachine.getPortMatches().add(portMatch);
    }

    /**
     * Determines the block type name by comparing the type name, maskType and
     * block library. If the blockType does not equal to maskType and there is a
     * block type corresponding to maskType in block library then maskType is 
     * returned as block type name. Otherwise the blockType name is returned.
     * 
     * As an exception, the processing of blocks with mask "Stateflow" is 
     * deferred to FMPreprocessor
     *   
     * @param blockType		block type name from mdl file
     * @param maskType		mask type name from mdl file	
     * @return				block type name
     */
    private String getBlockTypeByMask(String blockType, String maskType) {
    	if (maskType != null						// there is masktype 
    			&& !(maskType.equals(blockType))	// masktype does not equal 
    												// to block type
    			&& !(maskType.equals("Stateflow"))	// chart block processing 
    												// shall be deferred to FMPreprocessor
    			) {
    		if (stateMachine.getBlockLibraryFactory().getBlockType(maskType) 
    																!= null) {
    			return maskType;
    		}
    	}
    	
    	return blockType;
    }
    
	/**
	 * @param line Tree representing the mdl Line object
	 * @param parent GAModelElement (parenting system)
	 * @return a string to identify a Line object for reporting purposes
	 */
	private String getLineRefStr(Tree line, GAModelElement parent) {
		// Retrieves the "SrcBlock" value
        String srcBlock = TImporterUtilities.getAttFromTree(
        		line, TImporterConstant.SK_SRCBLOCK);
		// Retrieves the "SrcPort" value
        String srcPort = TImporterUtilities.getAttFromTree(
        		line, TImporterConstant.SK_SRCPORT);
        // Retrieves the "DstBlock" value
        String dstBlock = TImporterUtilities.getAttFromTree(
        		line, TImporterConstant.SK_DSTBLOCK);
        // Retrieves the "DstPort" value
        String dstPort = TImporterUtilities.getAttFromTree(
        		line, TImporterConstant.SK_DSTPORT);
		String refStr = "\n System: " + parent.getReferenceString()
				+ "\n Signal (line):" + " SrcBlock=" + srcBlock + ", SrcPort=" + srcPort
				+ ", DstBlock=" + dstBlock + ", DstPort=" + dstPort;
		return refStr;
	}

	/**
	 * @param branch Tree representing the mdl Branch object
	 * @param parent GAModelElement (parenting system)
	 * @return a string to identify a Branch object for reporting purposes
	 */
	private String getBranchRefStr(Tree branch, GAModelElement parent) {
        // Retrieves the "DstBlock" value
        String dstBlock = TImporterUtilities.getAttFromTree(
        		branch, TImporterConstant.SK_DSTBLOCK);
        // Retrieves the "DstPort" value
        String dstPort = TImporterUtilities.getAttFromTree(
        		branch, TImporterConstant.SK_DSTPORT);
		String refStr = "\n System: " + parent.getReferenceString()
				+ "\n Signal branch:" + ", DstBlock=" + dstBlock + ", DstPort=" + dstPort;
		return refStr;
	}

}
