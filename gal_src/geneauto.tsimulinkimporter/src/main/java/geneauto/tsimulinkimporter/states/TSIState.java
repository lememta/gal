/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/states/TSIState.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2010-06-04 16:58:51 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.states;

import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.statemachine.State;
import geneauto.statemachine.StateMachine;
import geneauto.tsimulinkimporter.main.TSimulinkImporterTool;

public abstract class TSIState extends State{
	/**
     * The state machine to which this state is linked.
     */
    public TSimulinkImporterTool stateMachine;

    /**
     * Model on which each state will work.
     */
    public GASystemModel gaSystemModel;

    /**
     * Constructor of this class.
     * 
     * @param machine
     *            The state machine to which this state is linked.
     */
    public TSIState(TSimulinkImporterTool machine, String stateName) {
    	super(stateName);
        stateMachine = machine;
    }
    
    /** 
     * returns NULL until the machine of this tool is also refactored to inherit from StateMachine
     */
	public StateMachine getMachine() {
		return null;
	}
}
