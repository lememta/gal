/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/states/TSIResolvingNameReferenceState.java,v $
 *  @version	$Revision: 1.5 $
 *	@date		$Date: 2010-06-04 16:58:51 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.states;

import geneauto.models.gasystemmodel.GASystemModelRoot;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tsimulinkimporter.main.TSimulinkImporterTool;
import geneauto.utils.map.ConstantMap;

public class TSIResolvingNameReferenceState extends TSIState {

    public TSIResolvingNameReferenceState(TSimulinkImporterTool machine) {
        super(machine, "TSIResolvingNameReference");
    }

    @Override
    public void stateEntry() {
        // TODO Auto-generated method stub
     
        this.gaSystemModel = this.stateMachine.getSystemModelFactory().getModel();
        
    }
    
    
    @Override
    public void stateExecute() {

        //convert names map to a constant map
        ConstantMap<String, GAModelElement> nameMapConst = new ConstantMap<String, GAModelElement>(
                this.stateMachine.getNamesMap(), false);

        //update references in all memberExpressions
        for (GASystemModelRoot e : this.gaSystemModel.getElements()) {
            e.resolveReferences(nameMapConst, this.gaSystemModel);
        }

        stateMachine.setState(stateMachine.getSavingState());
    }
}
