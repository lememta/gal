/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/states/TSIParsingEmlState.java,v $
 *  @version	$Revision: 1.24 $
 *	@date		$Date: 2012-02-20 09:16:50 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gablocklibrary.BlockType;
import geneauto.models.gablocklibrary.ParameterType;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.emlconverter.EMLAccessor;
import geneauto.tsimulinkimporter.main.TSimulinkImporterTool;

import java.util.ArrayList;

/**
 * 
 * Class used to parse EML expressions in Simulink parameters. CodeModel
 * instance are created
 * 
 * 
 */
public class TSIParsingEmlState extends TSIState {

    public TSIParsingEmlState(TSimulinkImporterTool machine) {
        super(machine, "ParsingEml");
    }

    @Override
    public void stateExecute() {

        for (GAModelElement elem : this.stateMachine.getSystemModelFactory()
                .getModel().getAllElements(Block.class)) {

            Block block = (Block) elem;

            BlockType blockType = block.getBlockType(); 
            if (blockType == null) {
            	/** generic blocks are allowed in this stage.
            	 * if no type can be retrieved just continue
            	 * we have no information on whether any of the 
            	 * parameters needs to be parsed.
            	 * 
            	 *  TODO: (TF:653) revisit this -- here we implicitly assume that
            	 *  a masked block can not have any other parameter values than
            	 *  strings. Is this correct (ToNa 29.07.09)
            	 */
            	continue;
            }

            for (Parameter param : block.getParameters()) {
            	ParameterType pType = blockType
            							.getParameterTypeByName(param
                        				.getName());

                if (pType != null && pType.isEvaluateEML()) {

                    String parameterValue = param.getStringValue();
                    Expression expression = null;
                    
                    // Parse parameter value
                    if (param.getName().equals("CaseConditions")) {
                        // TODO BZ:219 
                        // we should not have hardcoded parameter names in 
                        // this part of the code 
                        // Updated and added BugZilla task (AnTo 16/02/12)
                        expression = EMLAccessor.parseCellExpression1D(parameterValue);
                        if (expression == null) {
                            EventHandler.handle(
                                EventLevel.CRITICAL_ERROR,
                                "EMLParser", "",
                                "Error parsing value of parameter "
                                        + param.getName()
                                        + " in block "
                                        + block.getReferenceString()
                                        + "\nValue : "
                                        + parameterValue, "");
                        }
                        
                    } else {
                    	// General case
                        expression = EMLAccessor.convertEMLToCodeModel(
                               parameterValue, pType.isFullNormalization());
                        if (expression == null) {
                            EventHandler.handle(
                                EventLevel.ERROR,
                                "EMLParser", "",
                                "Error parsing value of parameter "
                                        + param.getName()
                                        + " in block "
                                        + block.getReferenceString()
                                        + "\nValue : "
                                        + parameterValue, "");
                        } else if (expression instanceof GeneralListExpression) {
                            if (((GeneralListExpression) expression).getExpressions().isEmpty()) {
                                // This value is returned for input "[]". Ignore it
                                expression = null;
                            }
                        }
                    }
                    
                    /*
                     * ifvalue of parameter is a variableExpression, link this
                     * expression to the corresponding variable (parsed from .m
                     * files)
                     */
                    ((GASystemModel) this.stateMachine.getSystemModelFactory()
                            .getModel()).getRootSystemBlock().linkVartoVarExpr(
                            new ArrayList<Variable_SM>(), expression);

                    param.setValue(expression);
                }
            }
        }

        stateMachine.setState(stateMachine.getResolvingNameReferenceState());
    }
}
