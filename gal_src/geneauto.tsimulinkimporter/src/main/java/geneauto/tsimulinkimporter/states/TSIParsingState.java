/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/states/TSIParsingState.java,v $
 *  @version	$Revision: 1.15 $
 *	@date		$Date: 2011-07-07 12:24:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.tsimulinkimporter.antlr.MDLStructParser;
import geneauto.tsimulinkimporter.main.TSimulinkImporterTool;
import geneauto.tsimulinkimporter.utils.TImporterConstant;
import geneauto.tsimulinkimporter.utils.TImporterUtilities;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;

public class TSIParsingState extends TSIState {

    /**
     * Constructor of this class.
     * 
     * @param machine
     */
    public TSIParsingState(TSimulinkImporterTool machine) {
        super(machine, "Parsing MDL");
    }

    public void stateExecute() {

        MDLStructParser mdlParser = null;
        try {
            // Parses the mdl input file and put the result tree
            // in the modelTree attribute
            mdlParser = TImporterUtilities.getParserMDL(stateMachine
                    .getInputFile());
        } catch (Exception e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_INPUTNOTFOUND,
                    TImporterConstant.EMPTY);
        }
        try {
            stateMachine.setModelTree((CommonTree) mdlParser.parse().getTree());
        } catch (RecognitionException re) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_ERROR_UNABLETOPARSEMDL + " : "
                            + stateMachine.getInputFile().getName(),
                    TImporterConstant.EMPTY);
        }

        // checking that input model file version is supported
        checkVersionValidity(mdlParser.getTokenStream());

        stateMachine.setState(stateMachine.getCpxComputingState());
    }

    /**
     * Check that input model file version is compatible with current version of
     * the tool
     * 
     * @param tokenStream
     *            data stream extracted from input model file
     */
    private void checkVersionValidity(TokenStream tokenStream) {

        for (int i = 0; i < tokenStream.size(); i++) {
            if (tokenStream.get(i).getText().equals("Version")) {
                String version = tokenStream.get(i + 1).getText();

                List<String> simulink_version_supported = new ArrayList<String>();
//                simulink_version_supported.add("6.6");
                simulink_version_supported.add("6.5");  // r2006b
                simulink_version_supported.add("6.3");	// r14 sp3
//                simulink_version_supported.add("6.2");// r12

                if (!simulink_version_supported.contains(version)) {
                    EventHandler.handle(EventLevel.WARNING,
                            TImporterConstant.TOOL_EVENT_ID,
                            TImporterConstant.EMPTY,
                            TImporterConstant.EVENT_ERROR_BADFILEVERSION
                                    + version, TImporterConstant.EMPTY);
                }

                // only 1st occurrence of "version" has to be checked, Simulink
                // file contains others occurrences that doesn't matter here.
                break;
            }
        }

    }

}
