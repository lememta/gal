/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/main/TSimulinkImporter.java,v $
 *  @version	$Revision: 1.12 $
 *	@date		$Date: 2011-09-27 14:30:38 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.main;

import geneauto.eventhandler.EventHandler;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

public class TSimulinkImporter {

    /**
     * Main method of the simulink importer tool. It reads the program
     * parameters, launches the mdl importing process, then close the program.
     * 
     * @param args
     *            List of the arguments of the simulink importer.
     */
    public static void main(String[] args) {

        // set root of all location strings in messages
        EventHandler.setToolName("TSimulinkImporter");

        // initialise ArgumentReader
        ArgumentReader.clear();

        // -l and -O options are set by ArgumentsReader.addCommonArguments
        ArgumentReader
                .addArgument("", "inputFilePath", true, true, true, "inputFile");
        ArgumentReader.addArgument("-o", "outputFilePath", true, true,
                true, "outputFile");
        ArgumentReader.addArgument("-b", "blockLibraryFilePath", false, true,
                true, "blockLibraryFile");
        ArgumentReader.addArgument("-m", "constantFilePath", false, true,
                true, "constantFile");
        ArgumentReader.addArgument("--mLib", "externalLibFolder", false, true,
                true, "");
        ArgumentReader.addArgument("-d", GAConst.ARG_DEBUG, false, 
        		false, false, "");

        // set arguments list to ArgumentsReader and parse the list
        TSimulinkImporterTool.getInstance().initTool(args, false);

        runTSI();
    }

    /**
     * Runs the simulink importer according to the given parameters.
     */
    private static void runTSI() {
        TSimulinkImporterTool.getInstance().init();
    }
}
