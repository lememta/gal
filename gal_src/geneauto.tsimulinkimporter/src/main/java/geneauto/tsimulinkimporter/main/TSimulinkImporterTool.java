/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/main/TSimulinkImporterTool.java,v $
 *  @version	$Revision: 1.21 $
 *	@date		$Date: 2011-09-27 14:30:38 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.main;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.genericmodel.Model;
import geneauto.statemachine.StateMachine;
import geneauto.tsimulinkimporter.states.TSIBuildingSMState;
import geneauto.tsimulinkimporter.states.TSICheckingMultiRateState;
import geneauto.tsimulinkimporter.states.TSICpxComputingState;
import geneauto.tsimulinkimporter.states.TSIInitializingState;
import geneauto.tsimulinkimporter.states.TSIParsingEmlState;
import geneauto.tsimulinkimporter.states.TSIParsingState;
import geneauto.tsimulinkimporter.states.TSIResolvingNameReferenceState;
import geneauto.tsimulinkimporter.states.TSISavingSMState;
import geneauto.tsimulinkimporter.utils.TImporterUtilities;
import geneauto.utils.FileUtil;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.antlr.runtime.tree.CommonTree;

public class TSimulinkImporterTool extends StateMachine {

    /**
     * The TSimulinkImporter input file (.mdl).
     */
    private File inputFile;

    /**
     * The TSimulinkImporter output file (.gsm).
     */
    private File outputFile;

    /**
     * Model tree of the input file.
     */
    private CommonTree modelTree;

    /**
     * The ModelFactory instance used to generate and write GASystemModel
     */
    private SystemModelFactory sysModelFactory;

    private BlockLibraryFactory blockLibraryFactory;

    /**
     * The current element description is a Map of String couple
     */
    private Map<String, String> currentEltDescription;

    /**
     * The matrix which matches port subsystem name, block name, block port
     * ID,global port ID
     */
    private Vector<String[]> portMatches;

    /**
     * Map for resolving name references in CodeModel expressions
     */
    private Map<String, GAModelElement> namesMap = new HashMap<String, GAModelElement>();

    /**
     * Unique instance of this class.
     */
    private static TSimulinkImporterTool instance;
    
    /** In case this flag is true, no IDs are added to annotations  */
    private boolean debugMode;

    /**
     * Unique way to access the code model generator.
     * 
     * @return the unique instance of this class.
     */
    public static TSimulinkImporterTool getInstance() {
        if (instance == null) {
            instance = new TSimulinkImporterTool();
        }
        return instance;
    }

    /**
     * First state of the simulink importer. Reads the input parameters,
     * initialises the simulink importer with the given values.
     */
    private TSIInitializingState initializingState;

    /**
     * Parses the mdl file into an AST.
     */
    private TSIParsingState parsingState;

    /**
     * Makes some checks on the AST.
     */
    private TSICpxComputingState cpxComputingState;

    /**
     * Makes some checks on the AST.
     */
    private TSIBuildingSMState buildingState;

    /**
     * Makes some checks on the AST.
     */
    private TSICheckingMultiRateState checkingMultiRateState;

    /**
     * Parse EML expression on Simulink parameters
     */
    private TSIParsingEmlState parsingEmlState;
    
    /**
     * Resolve name references
     */
    private TSIResolvingNameReferenceState resolvingNameReferenceState;

    /**
     * Makes some checks on the AST.
     */
    private TSISavingSMState savingState;

    /**
     * Model created by the SystemModelFactory. It will contain all the
     * GAModelElements that describe the system in GASystemModel language
     */
    private Model gaSystemModel;

    /**
     * Private constructor of this class. Instantiates all of the states of the
     * state machine. Sets the current State to initializingState.
     */
    private TSimulinkImporterTool() {
        super("DEV", ""); // TODO: set version number here *
        String toolClassPath = "geneauto/tsimulinkimporter/main/TSimulinkImporterTool.class";
        URL url = this.getClass().getClassLoader().getResource(toolClassPath);

        String version = FileUtil.getVersionFromManifest(url, toolClassPath);
        String releaseDate = FileUtil.getDateFromManifest(url, toolClassPath);

        if (version != null) {
            setVerString(version);
        }
        if (releaseDate != null) {
            setReleaseDate(releaseDate);
        }
        initializingState = new TSIInitializingState(this);
        parsingState = new TSIParsingState(this);
        cpxComputingState = new TSICpxComputingState(this);
        buildingState = new TSIBuildingSMState(this);
        checkingMultiRateState = new TSICheckingMultiRateState(this);
        parsingEmlState = new TSIParsingEmlState(this);
        resolvingNameReferenceState = new TSIResolvingNameReferenceState(this);
        savingState = new TSISavingSMState(this);
    }

    /**
     * @return the initializingState
     */
    public TSIInitializingState getInitializingState() {
        return initializingState;
    }

    /**
     * @return the parsingState
     */
    public TSIParsingState getParsingState() {
        return parsingState;
    }

    /**
     * @return the checkingState
     */
    public TSICpxComputingState getCpxComputingState() {
        return cpxComputingState;
    }

    /**
     * @return the buildingState
     */
    public TSIBuildingSMState getBuildingState() {
        return buildingState;
    }

    /**
     * @return the checkingMultiRateState
     */
    public TSICheckingMultiRateState getCheckingMultiRateState() {
        return checkingMultiRateState;
    }

    /**
     * @return the parsingEmlState
     */
    public TSIParsingEmlState getParsingEmlState() {
        return parsingEmlState;
    }

    /**
     * @return the savingState
     */
    public TSISavingSMState getSavingState() {
        return savingState;
    }

    /**
     * @return the systemModel
     */
    public Model getSystemModel() {
        return gaSystemModel;
    }

    /**
     * @return the sysModelFactory
     */
    public SystemModelFactory getSystemModelFactory() {
        return sysModelFactory;
    }

    /**
     * Initializes the simulink importer
     * 
     * @param arguments
     */
    public void init() {
        setState(initializingState);
    }

    /**
     * @param systemModel
     *            the systemModel to set
     */
    public void setSystemModel(Model systemModel) {
        this.gaSystemModel = systemModel;
    }

    /**
     * @param systemModelFactory
     *            the systemModelFactory to set
     */
    public void setSystemModelFactory(SystemModelFactory systemModelFactory) {
        this.sysModelFactory = systemModelFactory;
    }

    /**
     * Set the outputFile member
     * 
     * @param file
     *            the outputFile to set
     */
    public void setOutputFile(File file) {
        outputFile = file;
    }

    /**
     * Set the inputFile member
     * 
     * @param file
     *            the inputFile to set
     */
    public void setInputFile(File file) {
        inputFile = file;
    }

    /**
     * @return the inputFile member
     */
    public File getInputFile() {
        return inputFile;
    }

    /**
     * @return the outputFile member
     */
    public File getOutputFile() {
        return outputFile;
    }

    /**
     * Returns the current element description
     * 
     * @return the current element description
     */
    public Map<String, String> getCurrentEltDesc() {
        return currentEltDescription;
    }

    /**
     * Reset the current element description
     */
    public void resetCurrentEltDesc() {
        currentEltDescription = new HashMap<String, String>();
    }

    /**
     * Returns the port matches matrix
     * 
     * @return the port matches matrix
     */
    public Vector<String[]> getPortMatches() {
        return portMatches;
    }

    /**
     * Reset the port matches matrix
     */
    public void resetPortMatches() {
        portMatches = new Vector<String[]>();
    }

    /**
     * Set the model tree
     * 
     * @param t
     *            the model tree to set
     */
    public void setModelTree(CommonTree t) {
        modelTree = t;
    }

    /**
     * Returns the model tree
     * 
     * @return the model tree
     */
    public CommonTree getModelTree() {
        return modelTree;
    }

    public BlockLibraryFactory getBlockLibraryFactory() {
        return blockLibraryFactory;
    }

    public void setBlockLibraryFactory(BlockLibraryFactory blockLibraryFactory) {
        this.blockLibraryFactory = blockLibraryFactory;
    }

    /**
     * Provides the model tree instance
     * 
     * @return the model tree
     */
    public CommonTree getSystemTree() {
        // If there is no system tree
        if (TImporterUtilities.subTrees(modelTree, "System").isEmpty()) {
            // Adds a new log ERROR
            EventHandler.handle(EventLevel.ERROR, "MDLImporter", "",
                    "MSystem description is missing in model", "");
            // Returns null
            return null;
        }
        // If there is a system tree
        else {
            // Returns the system tree
            return (CommonTree) TImporterUtilities
                    .subTrees(modelTree, "System").firstElement();
        }
    }

    public Map<String, GAModelElement> getNamesMap() {
        return namesMap;
    }

    public TSIResolvingNameReferenceState getResolvingNameReferenceState() {
        return resolvingNameReferenceState;
    }

    public void setResolvingNameReferenceState(
            TSIResolvingNameReferenceState resolvingNameReferenceState) {
        this.resolvingNameReferenceState = resolvingNameReferenceState;
    }

	public boolean isDebugMode() {
		return debugMode;
	}

	public void setDebugMode(boolean debugMode) {
		this.debugMode = debugMode;
	}
}
