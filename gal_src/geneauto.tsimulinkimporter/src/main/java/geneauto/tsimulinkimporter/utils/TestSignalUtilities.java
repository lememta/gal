package geneauto.tsimulinkimporter.utils;

import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.genericmodel.GAModelElement;

import java.util.HashMap;
import java.util.Map;

public class TestSignalUtilities {
    
    /**
     * Map where block full paths are stored: 
     * key - reference to the block
     * value - block's evaluated path
     */
    public static Map<Block, String> blockFullPathes = 
        new HashMap<Block, String>();
    
    public static void init() {
        blockFullPathes.clear();
    }
    
    /**
     * 
     * @param block Block
     * @return block's name (if block has no parent) or 
     * <block's parrent's full path>/<block's name>
     */
    public static String getBlockFullPath(Block block) {
        String result = null;
        if (blockFullPathes.containsKey(block)) {
            
            // current block is in a list of blockFullPathes 
            // keys -> return corresponding value
            result = blockFullPathes.get(block);
        } else {
            GAModelElement parent = block.getParent();
            
            // replace all "\n" with " " in block's name
            String blockName = block.getName().replace("\\n", " ");
            if (parent == null) {
                result = blockName;
            } else if (parent instanceof Block) {
                Block parentBlock = (Block) block.getParent();
                result = getBlockFullPath(parentBlock) + "/" + blockName;
            }
            
            // add evaluated block full path to the map
            blockFullPathes.put(block, result);
        }
        return result;
    }
}
