/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/utils/DefaultParametersMapping.java,v $
 *  @version	$Revision: 1.4 $
 *	@date		$Date: 2009-11-10 09:59:09 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.utils;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class DefaultParametersMapping {

    public static Map<String, HashMap<String, String>> defaultParamMap = new HashMap<String, HashMap<String, String>>();

    private static DefaultParametersMapping instance;

    private DefaultParametersMapping() {
        createPropertiesMap();
    }

    public static DefaultParametersMapping getInstance() {
        if (instance == null) {
            instance = new DefaultParametersMapping();
        }
        return instance;
    }

    /**
     * Map which allows to associate to each mdl tag the geneauto attributes
     * which must be instantiated.
     */
    private static void createPropertiesMap() {
        defaultParamMap = new HashMap<String, HashMap<String, String>>();

    }

    /**
     * 
     */
    public void addBlockParam(String blockName, Map<String, String> params) {
        defaultParamMap.put(blockName, (HashMap<String, String>) params);
    }

    /**
     * 
     */
    public Map<String, String> getBlockParam(String blockName) {
        if (defaultParamMap.get(blockName) != null) {
            return defaultParamMap.get(blockName);
        } else {
            return new HashMap<String, String>();
        }
    }
}
