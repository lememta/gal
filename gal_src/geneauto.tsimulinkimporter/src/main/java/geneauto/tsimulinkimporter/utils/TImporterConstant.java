/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/utils/TImporterConstant.java,v $
 *  @version	$Revision: 1.18 $
 *	@date		$Date: 2010-07-28 11:27:53 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.utils;

/**
 * This interface contains all the constant declarations of the importer.
 * 
 */
public interface TImporterConstant {

    /**
     * Constant value for index of X coordinate in position table
     */
    public final static int POS_X = 0;

    /**
     * Constant value for index of X coordinate in position table
     */
    public final static int POS_Y = 1;

    /**
     * Constant value for index of Input data ports number in port table
     */
    public final static int INPORT_DATA = 0;

    /**
     * Constant value for index of Output data ports number in port table
     */
    public final static int OUTPORT_DATA = 1;

    /**
     * Constant value for index of Input enable ports number in port table
     */
    public final static int INPORT_ENABLE = 2;

    /**
     * Constant value for index of Input control ports number in port table
     */
    public final static int INPORT_CONTROL = 3;

    /**
     * Constant value for index of Output control ports number in port table
     */
    public final static int OUTPORT_CONTROL = 4;

    /**
     * Constant value for index of Output control ports number in port table
     */
    public final static int INPORT_ACTION = 7;

    /**
     * Constant value for index of Block name in portMatches table
     */
    public final static int BLOCK_NAME = 0;

    /**
     * Constant value for index of local input data port ID in portMatches table
     */
    public final static int LOCAL_IN_ID = 1;

    /**
     * Constant value for index of local output data port ID in portMatches
     * table
     */
    public final static int LOCAL_OUT_ID = 2;

    /**
     * Constant value for index of local enable port ID in portMatches table
     */
    public final static int LOCAL_EN_ID = 3;

    /**
     * Constant value for index of local input control port ID in portMatches
     * table
     */
    public final static int LOCAL_INCTRL_ID = 4;

    /**
     * Constant value for index of local output control port ID in portMatches
     * table
     */
    public final static int LOCAL_OUTCTRL_ID = 5;

    /**
     * Constant value for index of global port ID in portMatches table
     */
    public final static int GLOBAL_ID = 6;

    /**
     * Constant value for parent Subsystem name in portMatches table
     */
    public final static int SUBS_ID = 7;

    /** Simulink Constant Strings * */
    public final static String SK_SOURCETYPE = "SourceType";
    public final static String SK_NAME = "Name";
    public final static String SK_VERSION = "Version";
    public final static String SK_LASTMODBY = "LastModifiedBy";
    public final static String SK_LASTMODON = "LastModifiedDate";
    public final static String SK_DESCRIPTION = "Description";
    public final static String SK_BLOCK = "Block";
    public final static String SK_BLOCKTYPE = "BlockType";
    public final static String SK_PARAM_DEFAULTS = "BlockParameterDefaults";
    public final static String SK_SUBSYS = "SubSystem";
    public final static String SK_ANNOTATION = "Annotation";
    public final static String SK_BRANCH = "Branch";
    public final static String SK_DSTPORT = "DstPort";
    public final static String SK_DSTBLOCK = "DstBlock";
    public final static String SK_TRIGGER = "trigger";
    public final static String SK_ENABLE = "enable";
    public final static String SK_TEXT = "Text";
    public final static String SK_SYSTEM = "System";
    public final static String SK_POSITION = "Position";
    public final static String SK_REFERENCE = "Reference";
    public final static String SK_PORTS = "Ports";
    public final static String SK_EMPTY_PORTS = "[]";
    public final static String SK_TRIGGERPORT = "TriggerPort";
    public final static String SK_INPORT = "Inport";
    public final static String SK_GROUND = "Ground";
    public final static String SK_FROMWORKSPACE = "FromWorkspace";
    public final static String SK_OUTPORT = "Outport";
    public final static String SK_TOWORKSPACE = "ToWorkspace";
    public final static String SK_TERMINATOR = "Terminator";
    public final static String SK_ENABLEPORT = "EnablePort";
    public final static String SK_CONSTANT = "Constant";
    public final static String SK_SCOPE = "Scope";
    public final static String SK_RELATIONAL_OP = "RelationalOperator";
    public final static String SK_ACTIONPORT = "ActionPort";
    public final static String SK_SWITCH = "Switch";
    public final static String SK_SINK_PORTS = "[1, 0]";
    public final static String SK_SOURCE_PORTS = "[0, 1]";
    public final static String SK_RELATIONNAL_OP_PORTS = "[2, 1]";
    public final static String SK_ACTIONPORT_PORTS = "[0, 0]";
    public final static String SK_SWITCH_PORTS = "[3, 1]";
    public final static String SK_DEFAULT_PORTS = "[1, 1]";
    public final static String SK_MASK_VARIABLES = "MaskVariables";
    public final static String SK_MASK_PROMPT = "MaskPromptString";
    public final static String SK_MASK_VALUES = "MaskValueString";
    public final static String SK_MASK_TYPE = "MaskType";
    public final static String SK_SRCPORT = "SrcPort";
    public final static String SK_SRCBLOCK = "SrcBlock";
    public final static String SK_IFACTION = "ifaction";
    public final static String SK_IF = "If";
    public final static String SK_LINE = "Line";
    public final static String SK_SOURCEBLOCK = "SourceBlock";
    public final static String SK_DATASTOREMEMORY = "DataStoreMemory";
    public final static String SK_DATASTOREREAD = "DataStoreRead";
    public final static String SK_GOTO = "Goto";
    public final static String SK_FROM = "From";
    public final static String SK_GRAPH_INTERFACE = "GraphicalInterface";
    public final static String SK_TEST_POINTED_SIGNAL = "TestPointedSignal";
    public final static String SK_FULL_BLOCK_PATH = "FullBlockPath";
    public final static String SK_PORT_INDEX = "PortIndex";

    /** GeneAuto Constant Strings * */
    public final static String GA_SUBSYS = "SubSystem";
    public final static String GA_BLOCK = "Block";
    public final static String GA_POSX = "positionX";
    public final static String GA_POSY = "positionY";
    public final static String GA_SIZEX = "sizeX";
    public final static String GA_SIZEY = "sizeY";
    public final static String GA_INDATAPORT = "InDataPort";
    public final static String GA_INDATAPORT_LIST = "inDataPorts";
    public final static String GA_PORTNUMBER = "portNumber";
    public final static String GA_OUTDATAPORT = "OutDataPort";
    public final static String GA_OUTDATAPORT_LIST = "outDataPorts";
    public final static String GA_ENABLEPORT = "InEnablePort";
    public final static String GA_EDGEENABLEPORT = "InEdgeEnablePort";
    public final static String GA_EDGEENABLEPORT_LIST = "inEdgeEnablePort";
    public final static String GA_ENABLEPORT_LIST = "inEnablePort";
    public final static String GA_INCONTROLPORT = "InControlPort";
    public final static String GA_INCONTROLPORT_LIST = "inControlPorts";
    public final static String GA_OUTCONTROLPORT = "OutControlPort";
    public final static String GA_OUTCONTROLPORT_LIST = "outControlPorts";
    public final static String GA_SRCPORT = "srcPort";
    public final static String GA_DSTPORT = "dstPort";
    public final static String GA_SIGNAL = "Signal";
    public final static String GA_SIGNAL_LIST = "signals";
    public final static String GA_ID = "id";
    public final static String GA_NAME = "name";

    /** Special Constant Strings * */
    public final static String NOT_USED = "0";
    public final static String EMPTY = "";
    public final static String EXT_MDL = "mdl";
    public final static String TOOL_NAME = "TSimulinkImporter";
    public final static String LOGLVL_NOLOG = "NO_LOG";
    public final static String LOGLVL_DEBUG = "DEBUG";
    public final static String TOOL_LOGSUFFIX = "/mdlImporter.log.txt";
    public final static String TOOL_EVENT_ID = "TSimulinkImporter";

    /** Property Constant Strings * */
    public final static String PROP_TMPFOLDER = "tmpfolder";
    public final static String PROP_LIBFOLDER = "libfolder";
    public final static String PROP_LIBFILE = "libFile";
    public final static String PROP_CONSTFILE = "constFile";

    /** Main Argument Constant Strings * */
    public final static String ARG_OUTPUTFOLDER = "outputPath";
    public final static String ARG_LIBFOLDER = "externalLibFolder";
    public final static String ARG_LIBFILE = "libraryFilePath";
    public final static String ARG_INPUTFILE = "inputFilePath";
    public final static String ARG_INPUTFILENAME = "inputFileName";
    public final static String ARG_INPUTFILEPATH = "inputFileDirectory";
    public final static String ARG_LOGLVL = "logLevel";
    public final static String ARG_TMPFOLDER = "tmpPath";

    /** Event message Constant Strings * */
    public final static String EVENT_END_CPXCOMPUTING = "Ending complexity computing.";
    public final static String EVENT_START_CPXCOMPUTING = "Starting complexity computing.";
    public final static String EVENT_END_INITIALIZING = "Ending initialization.";
    public final static String EVENT_START_INITIALIZING = "Starting initialization.";
    public final static String EVENT_END_PARSING = "Model has been parsed into an Abstract Syntax Tree";
    public final static String EVENT_START_PARSING = "Starting Parsing.";
    public final static String EVENT_END_BUILDINGSM = "Ending building GASystemModel.";
    public final static String EVENT_START_BUILDINGSM = "Starting building GASystemModel.";
    public final static String EVENT_START_SAVINGSM = "Starting saving GASystemModel.";
    public final static String EVENT_END_SAVINGSM = "GASystemModel has been exported.";
    public final static String EVENT_INFO_CONSTANTDEFNOTFOUND = "No constant definition file found.";
    public final static String EVENT_INFO_CPXCOMPUTING = "Model complexity has been analysed";
    public final static String EVENT_ERROR_INPUTNOTFOUND = "Input file not found.";
    public final static String EVENT_ERROR_UNABLETOPARSEMDL = "Unable to parse input MDL-file.";
    public final static String EVENT_ERROR_UNABLETOPARSEMFILE = "Unable to parse input constant M-file.";
    public final static String EVENT_ERROR_MODELVERSIONMISSING = "Modelling tool version is missing or empty.";
    public final static String EVENT_ERROR_MODELNAMEMISSING = "General model name is missing or empty.";
    public final static String EVENT_ERROR_SYSTEMNAMEMISSING = "System name is missing or empty.";
    public final static String EVENT_ERROR_DSTPORTMISSING = "DstPort field is missing in a line description.";
    public final static String EVENT_ERROR_DSTBLOCKMISSING = "DstBlock field is missing in a line description.";
    public final static String EVENT_ERROR_SRCPORTMISSING = "SrcPort field is missing in a line description.";
    public final static String EVENT_ERROR_SRCBLOCKMISSING = "SrcBlock field is missing in a line description.";
    public final static String EVENT_ERROR_BLOCKTYPEMISSING = "BlockType field is missing in a block description.";
    public final static String EVENT_ERROR_BLOCKNAMEMISSING = "Name field is missing in a block description.";
    public final static String EVENT_ERROR_BLOCKPOSITIONMISSING = "Position field is missing in a block description.";
    public final static String EVENT_ERROR_UNRESOLVEDREFERENCE = "Unresolved reference block : ";
    public final static String EVENT_ERROR_BADFILEVERSION = "Input model file has an unsupported version : ";

    /** Complexity file Constant Strings * */
    public final static String CPX_MODELNAME = "Input model : ";
    public final static String CPX_NBBLOCKS = "Number of blocks : ";
    public final static String CPX_NBSIGNALS = "Number of signals : ";
    public final static String CPX_NBLEVELS = "Number of hierarchical levels : ";
    public final static String CPX_GENERALHEADER = "--- Gene-Auto input model metrics --- \n\n";
    public final static String CPX_INPUTMODELHEADER = "* Input Model * \n";
}
