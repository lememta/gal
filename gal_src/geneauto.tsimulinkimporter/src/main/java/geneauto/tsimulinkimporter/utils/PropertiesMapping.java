/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/utils/PropertiesMapping.java,v $
 *  @version	$Revision: 1.6 $
 *	@date		$Date: 2012-03-08 10:02:20 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.utils;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class PropertiesMapping {

    private static Map<String, String> propertiesMap = new HashMap<String, String>();

    private static String DEFAULT = "Default.";

    private static PropertiesMapping instance;

    private PropertiesMapping() {
        createPropertiesMap();
    }

    public static PropertiesMapping getInstance() {
        if (instance == null) {
            instance = new PropertiesMapping();
        }
        return instance;
    }

    /**
     * Map which allows to associate to each mdl tag the geneauto attributes
     * which must be instatiated.
     */
    private static void createPropertiesMap() {
        propertiesMap = new HashMap<String, String>();

        propertiesMap.put(DEFAULT + "Type", "type");
        propertiesMap.put(DEFAULT + "Name", "name");
        propertiesMap.put(DEFAULT + "IsVirtual", "isVirtual");
        propertiesMap.put(DEFAULT + "SampleTime", "sampleTime");
        propertiesMap.put(DEFAULT + "SystemSampleTime", "sampleTime");
        propertiesMap.put(DEFAULT + "DirectFeedThrough", "directFeedThrough");
        propertiesMap.put(DEFAULT + "Priority", "userDefinedPriority");
        propertiesMap.put(DEFAULT + "AssignedPriority", "assignedPriority");
        propertiesMap.put(DEFAULT + "ExecutionOrder", "executionOrder");
        propertiesMap.put(DEFAULT + "TreatAsAtomicUnit", "AtomicSubSystem");
        propertiesMap.put(DEFAULT + "X0", "InitialValue");
        propertiesMap.put("Math.Operator", "Function");
    }

    /**
     * Method which returns a geneauto attribute name according to the simulink
     * tag name.
     * 
     * @param simulinkPropertyKey
     *            the simulink tag name
     * @return the GASM tag name
     */
    public String getGeneAutoPropertyName(String simulinkPropertyKey) {
        String res = null;
        res = propertiesMap.get(simulinkPropertyKey);

        String defaultKey = simulinkPropertyKey.substring((simulinkPropertyKey
                .indexOf('.') + 1));
        if (res == null) {
            String tmpString = DEFAULT + defaultKey;
            res = propertiesMap.get(tmpString);
        }
        if (res == null) {
            return defaultKey;
        }
        return res;
    }
}
