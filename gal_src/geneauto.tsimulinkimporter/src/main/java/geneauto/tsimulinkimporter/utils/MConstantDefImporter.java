package geneauto.tsimulinkimporter.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.StructureContent;
import geneauto.models.gacodemodel.StructureMember;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gadatatypes.TCustom;
import geneauto.models.gasystemmodel.common.CustomType_SM;
import geneauto.models.gasystemmodel.common.VariableScope;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.emlconverter.EMLAccessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MConstantDefImporter {

	// file name of the constant file
	private String constFileName;
	
	// file handler for the constants file 
	private File constFile;
	
	// list of annotations that are not connected to an element yet
	List<Annotation> pendingAnnotations = new LinkedList<Annotation>();

	// system block for created variables
	SystemBlock parent = null;
	
	// reference to the name map
	Map<String, GAModelElement> nameMap = null;

	// list of parsed variables
	List<Variable_SM> varList = null;
	
	// allowed symbols for starting a comment
	private static String commentStartRegex = "\\s*([%#]|//)";
	// regex to match comment in the beginning of line
	private static Pattern fullLineComment = Pattern.compile(commentStartRegex+".*");
	/* pattern to check the open brace in the end of statement string
	 * true when the string ends with open "["
	 * 					"xxxxx [ sss" returns true
	 * 					"xxxxx [ sss ]" returns false
	 * NB! does not detect unpaired braces:
	 * 					"xxxxx [[ sss ]" returns false
	 */
	private static Pattern openBrace = Pattern.compile(".*\\[[^\\]]*");
	// matches semicolon in the end of statement ignoring trailing whitespace
	private static Pattern endsWithSColon = Pattern.compile("(.*)(;(\\s)*)");
	// valid constant definition expression
	private static Pattern validConstStatement = 
		Pattern.compile("(\\s)*([a-zA-Z_\\$])([a-zA-Z_\\$\\.0-9])*(\\s)*=([a-zA-Z0-9_\\$\\.;+-/\\*\\(\\)\\[\\]\\s])+;(\\s)*");
	
	// a buffer to store components of currently processed statement
	private String pendingStmt = "";
	
	/**
	 * Reads a file specified by filename and returns list of 
	 * variables constructed from the constant definitions found in the file
	 * 
	 *  All constants are expected to be in form 
	 *  <varname> = <dataype>(<value expression>) ; [ '%'|'#'|'//' <annotation>
	 *  
	 *  where 
	 *  	- <varname> is the name of variable, in case of hierarchical name 
	 *  a structure is constructed
	 *  	- <dataype> is valid Simulink data type
	 *    	- <value expression> is any expression supported by EMLAccessor
	 *  
	 *  Annotations are parsed as follows:
	 *  	- all annotations before the first constant definition are bound 
	 *  to the first constant
	 *  	- annotations after a constant definition are bound to that 
	 *  definition until next constant definition starts  
	 * 
	 * @param filename
	 * @return
	 */
	public List<Variable_SM> parseFile(String filename,
			SystemBlock parent,
			Map<String, GAModelElement> nameMap) {
		varList = new LinkedList<Variable_SM>();
		constFileName = filename;
		constFile = new File(constFileName);
		this.nameMap = nameMap;
		this.parent = parent;
		
		// create scanner to retrieve data from file statement-by-statement
		Scanner fileScanner;
		try {
			fileScanner = new Scanner(constFile);
		} catch (FileNotFoundException e1) {
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), 
					"", "File not found:\n  " + constFileName);
			return null;
		}

		Variable_SM lastConst = null;		
		try {
			while(fileScanner.hasNextLine()) {
				lastConst = parseStatement(fileScanner.nextLine(), lastConst);
			}
		} catch (Exception e){
			if (fileScanner != null) {
				fileScanner.close();
			}
			EventHandler.handle(EventLevel.CRITICAL_ERROR, getClass().getName(), 
					"", "Error scanning constants file:\n  " + constFileName, e);
			return null;
		} finally {
			fileScanner.close();
		}
		
		// all statements should be converted to constant definition now
		// if there is still anything in pendingStmt then it should have been
		// unterminated statement fragment.
		if (!(pendingStmt.isEmpty())) {
            EventHandler.handle(EventLevel.ERROR,
                    "MCostantDefImporter.parseFile",
                    "",
                    "Unterminated statement \"" + pendingStmt 
                    + "\" in file \"" + filename + "\"!",
                    "");			
		}

		// in case of structures copy the initial value from structure 
		// elements to the variable. 
		for (Variable_SM constant : varList) {
			if (constant.getDataType() instanceof TCustom) {
				constant.setInitialValueExpression(
						constant.getInitialOrDefaultValue());
			}
		}
		
		return varList;
	}
	
	private Variable_SM parseStatement(String line, 
											Variable_SM currentConst) {
		
		// empty line was given
		if (line.isEmpty()) {
			return currentConst;
		}
		
		Variable_SM newConst = null;
		
		if (fullLineComment.matcher(line).matches()) {
			// the line contains only an annotation
			
			// remove the annotation symbol
			line = line.replaceFirst(commentStartRegex,"");
			
			createAnnotation(line, currentConst);
			
			return currentConst;
		} else {
			// split the line using the annotation delimiter
			// the string did not start with annotation, so
			// the first piece is statement and all others the 
			// annotation
			String[] exprArray = line.split(commentStartRegex);
			// memorise the fragment of statement
			pendingStmt += exprArray[0];
			// attach annotations to the previous statement
			for (int i = 1; i < exprArray.length; i++) {
				createAnnotation(exprArray[1], currentConst);						
			}
		}				
		
		// check if the pending statement contains fill statement string 
		if (!(pendingStmt.isEmpty())	
				// string ends with semicolon
				&& endsWithSColon.matcher(pendingStmt).matches()
				// the semicolon is not inside of braces
				&& !(openBrace.matcher(pendingStmt).matches())){
			// check if the string is valid statement
			if (validConstStatement.matcher(pendingStmt).matches()) {
				String[] constArray = pendingStmt.trim().split("=");
				newConst = createConst(
						// array name with leading and trailing spaces removed
						constArray[0].trim(), 
						// value expression with semicolon and whitespace 
						// removed from the end
						endsWithSColon.matcher(constArray[1]).replaceFirst("$1"));
			} else {
	            EventHandler.handle(EventLevel.ERROR,
	                    "MCostantDefImporter.parseStatementString",
	                    "",
	                    "The statement \"" + pendingStmt 
	                    + "\" is not a valid constant definition!",
	                    "");
			}
			pendingStmt = "";
		}
		
		return newConst;
	}
	
	private void createAnnotation(String annotStr,
								Variable_SM currentConst) {
		
		// create new annotation
		Annotation newAnnot = new Annotation(annotStr);
		
		if (currentConst == null) {
			// if there is no current constant definition memorise the annotation
			pendingAnnotations.add(newAnnot);
		} else {
			currentConst.addAnnotation(newAnnot);
		}
	}
	
	private Variable_SM createConst(String constName, String constValue) {
	    Variable_SM constDef = new Variable_SM(
		        constName,
                VariableScope.EXPORTED_VARIABLE);
		// TODO: create method with data type check
		Expression initExpr = EMLAccessor.convertEMLToCodeModel(constValue, false); 
		
		if(initExpr == null) {
            EventHandler.handle(EventLevel.ERROR,
                    "MCostantDefImporter.createConst",
                    "",
                    "Could not convert string \"" + constValue 
                    + "\" to an expression!",
                    "");			
            return null;
		}
		
        constDef.setConst(true);
        constDef.setVolatile(true);
        constDef.setStatic(false);
        constDef.setDataType(initExpr.getDataType());
        if (constName.contains(".")) {
            createStructure(parent, nameMap, varList, initExpr, constDef);
        } else {
        	constDef.setInitialValue(initExpr);
            varList.add(constDef);
        }
        
        return constDef;
	}	

    private void createStructure(GAModelElement parent,
            Map<String, GAModelElement> nameMap, 
            List<Variable_SM> listVar,
            Expression expr,
            Variable_SM currentVar) {

        CustomType_SM createdCustom = null;

        createdCustom = processName(currentVar.getName(), currentVar.getName(),
                parent, expr, nameMap);

        String varName = currentVar.getName().split("\\.")[0];

        boolean addToList = true;
        for (Variable_SM v : listVar) {
            if (v.getName().equals(varName)) {
                addToList = false;
                break;
            }
        }

        if (addToList) {
            currentVar.setName(varName);
            currentVar.setDataType(new TCustom(createdCustom));
            currentVar.setScope(VariableScope.EXPORTED_VARIABLE);

            // update map for name references
            nameMap.put(varName, currentVar);

            listVar.add(currentVar);
        }

    }

    private CustomType_SM processName(String entireName,
            String globalName, GAModelElement parent, Expression expr,
            Map<String, GAModelElement> nameMap) {

        int pointLocation = entireName.indexOf(".");

        String varName = entireName.substring(0, pointLocation);
        String memberName = entireName.substring(pointLocation + 1);

        CustomType_SM createdCustom = null;

        for (CustomType_SM ct : ((SystemBlock) parent).getCustomTypes()) {
            if (ct.getName().equals(varName + "_custom_type")) {
                createdCustom = ct;
                break;
            }
        }

        if (createdCustom == null) {

            createdCustom = new CustomType_SM();
            createdCustom.setName(varName + "_custom_type");
            createdCustom.setContent(new StructureContent());

            ((SystemBlock) parent).addCustomType(createdCustom);
        }

        StructureMember structMemb = new StructureMember();

        if (memberName.contains(".")) {
        	// TODO: not clear what this branch does, hence not sure
        	// whether the member name is set correctly (ToNa 26/04/10)
            structMemb.setDataType(new TCustom(processName(memberName,
                    globalName, parent, expr, nameMap)));

            // structMemb.setName(varName + "." + memberName.split("\\.")[0]);
            structMemb.setName(memberName.split("\\.")[0]);

            // processName(memberName, parent, expr, nameMap);
        } else {
            structMemb.setName(memberName);
            structMemb.setDataType(expr.getDataType());
            structMemb.setInitialValue(expr);
        }

        // update map for name references
        // nameMap.put(structMemb.getName(), structMemb);

        if (createdCustom.getMemberByName(structMemb.getName()) == null) {
            ((StructureContent) createdCustom.getContent())
                    .addStructureMember(structMemb);
        }

        return createdCustom;
    }
    
}
