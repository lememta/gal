/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tsimulinkimporter/src/main/java/geneauto/tsimulinkimporter/utils/TImporterUtilities.java,v $
 *  @version	$Revision: 1.54 $
 *	@date		$Date: 2011-07-07 12:24:03 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *  Copyright (c) 2006-2009 Alyotech
 *  	http://www.alyotech.fr, geneauto@alyotech.fr
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tsimulinkimporter.utils;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.tsimulinkimporter.antlr.MDLStructLexer;
import geneauto.tsimulinkimporter.antlr.MDLStructParser;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;
import geneauto.utils.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * <b>TImporterUtilities</b>
 * <p>
 * This class offers methods for re-encoding input model files, resolving
 * references to block libraries in input model files, computing input model
 * complexity.
 * </p>
 * 
 */
public final class TImporterUtilities {

    /**
     * Extract the X1,X2,Y1 and Y2 coordinates from the original mdl position
     * format : [X1,Y1,X2,Y2] and computes sizeX = X2-X1 and sizeY = Y2-Y1
     * 
     * @param posToParse
     *            the position String to parse and process
     * @return the sizeX and sizeY values
     */
    public final static int[] computeSize(String posToParse) {
        // Declares a four-String table
        String[] parsedPos = new String[4];
        // Saves the index of the first ' character in the input String
        int i = posToParse.indexOf(',');
        // Puts the substring before this index in the table (1rst element)
        parsedPos[0] = posToParse.substring(1, i);
        // Removes the extracted substring from the input String
        String substrPosToParse = posToParse.substring(i + 1, posToParse
                .length());
        // Saves the index of the first ' character in the new String
        i = substrPosToParse.indexOf(',');
        // Puts the substring before this index in the table (2nd element)
        parsedPos[1] = substrPosToParse.substring(1, i);
        // Removes the extracted substring from the input String
        substrPosToParse = substrPosToParse.substring(i + 1, substrPosToParse
                .length());
        // Saves the index of the first ' character in the new String
        i = substrPosToParse.indexOf(',');
        // Puts the substring before this index in the table (3rd element)
        parsedPos[2] = substrPosToParse.substring(1, i);
        // Removes the extracted substring from the input String
        substrPosToParse = substrPosToParse.substring(i + 1, substrPosToParse
                .length());
        // Puts the substring before this index in the table (4th element)
        parsedPos[3] = substrPosToParse.substring(1,
                substrPosToParse.length() - 1);
        // Declares a two-int table
        int[] size = new int[2];
        // Computes the sizeX
        size[0] = Integer.parseInt(parsedPos[2])
                - Integer.parseInt(parsedPos[0]);
        // Computes the sizeY
        size[1] = Integer.parseInt(parsedPos[3])
                - Integer.parseInt(parsedPos[1]);
        // Returns the int table
        return size;
    }

    /**
     * Extract the X1 and Y1 coordinates from the original mdl position format :
     * [X1,Y1,X2,Y2]
     * 
     * @param posToParse
     *            the position String to parse
     * @return the X and Y coordinates in a two-elements-String table
     */
    public final static String[] parsePos(String posToParse) {
        // Declares a two-String table
        String[] parsedPos = new String[2];
        // Saves the index of the first ' character in the input String
        int i = posToParse.indexOf(',');
        // Puts the substring before this index in the table (1rst element)
        parsedPos[0] = posToParse.substring(1, i);
        // Removes the extracted substring from the input String
        String substrPosToParse = posToParse.substring(i + 1, posToParse
                .length());
        // Saves the index of the first ' character in the new String
        i = substrPosToParse.indexOf(',');
        // Puts the substring before this index in the table (2nd element)
        parsedPos[1] = substrPosToParse.substring(1, i);
        // Returns the table
        return parsedPos;
    }

    /**
     * Extract the number of InData,OutData,InEnable,InControl,OutControl ports
     * from the original mdl ports format
     * 
     * @param portsToParse
     *            the ports String to parse
     * @return number of each port in a five-elements-String table
     */
    public final static String[] parsePorts(String portsToParse) {
        // Declares a five-String table
        String[] parsedPorts = new String[8];
        // For each String element of the table
        for (int i = 0; i < parsedPorts.length; i++) {
            // Initialises with "0" value
            parsedPorts[i] = TImporterConstant.NOT_USED;
        }
        // Declares a String for storing the current String,
        // initialises it with the input String
        String substrPosToParse = portsToParse;
        // Saves the index of the first ',' character
        int i = substrPosToParse.indexOf(',');
        // Declares an index for the String table filling
        int n = 0;
        // While there is at least one ' character
        while (i != -1) {
            // Puts the substring before the ',' index in the table
            parsedPorts[n] = substrPosToParse.substring(1, i);
            // Removes the extracted substring from the current String
            substrPosToParse = substrPosToParse.substring(i + 1,
                    substrPosToParse.length());
            // Increments the table index
            n++;
            // Saves the index of the first ',' character in the new current
            // String
            i = substrPosToParse.indexOf(',');
        }
        // Puts the substring before the ',' index in the table
        parsedPorts[n] = substrPosToParse.substring(1, substrPosToParse
                .length() - 1);
        // Returns the table
        return parsedPorts;
    }

    /**
     * Retrieves a specified attribute value from a specified tree
     * 
     * @param t
     *            the tree
     * @param att
     *            the attribute name
     * @return the attribute value
     */
    public final static String getAttFromTree(Tree t, String att) {
        // Initialises a String to null
        String attValue = null;
        // For each child of the input tree...
        for (int i = 0; i < t.getChildCount(); i++) {
            // If the child name is the same as the input attribute name
            if (t.getChild(i).getText().equals(att)) {
                // Retrieves the child's child name and stores it into the
                // String
                attValue = t.getChild(i).getChild(0).getText();
                break;
            }
        }
        
        // Returns the stored String
        return StringUtils.clean(attValue);
    }

    /**
     * Retrieves the list of String couples (attribute name, attribute value)
     * from the root of a tree
     * 
     * @param t
     *            the tree
     * @return the attribute value
     */
    public final static Map<String, String> getAttMap(Tree t) {
        // Declares a map
        Map<String, String> map = new HashMap<String, String>();
        // For each child in the input tree
        for (int i = 0; i < t.getChildCount(); i++) {
            // If child has only one child
            if (t.getChild(i).getChildCount() == 1)
                // Puts the child name and the child's child name in the map
                map.put(PropertiesMapping.getInstance()
                        .getGeneAutoPropertyName(
                                getAttFromTree(t,
                                        TImporterConstant.SK_BLOCKTYPE)
                                        + "."
                                        + StringUtils.clean(t.getChild(i)
                                                .getText())), StringUtils
                        .clean(t.getChild(i).getChild(0).getText()));
        }
        return map;
    }

    /**
     * Retrieves the list of String pairs (attribute name, attribute value)
     * and adds those where name is not contained in the map there
     * 
     * @param t			AST with block attributes
     * @param map		map to be updated
     * @return the attribute value
     */
    public final static Map<String, String> updateAttMap(Tree t, 
    											Map<String, String> map) {
    	// create map of attributes
    	Map<String, String> tmpMap = getAttMap(t);
    	
    	for (String attr : tmpMap.keySet()) {
    		if (!(map.containsKey(attr))) {
    			map.put(attr, tmpMap.get(attr));
    		}
    	}
    	
        return map;
    }

    /**
     * Extracts the direct subtrees which have a specified root name from a
     * specified tree
     * 
     * @param t
     *            the tree
     * @param rootName
     *            the root name
     * @return the extracted subtrees
     */
    public final static Vector<Tree> subTrees(Tree t, String rootName) {
        // Declares a list of trees
        Vector<Tree> subtrees = new Vector<Tree>();
        // For each child in the input tree...
        for (int i = 0; i < t.getChildCount(); i++) {
            // If the child name in the same as the input String
            if (t.getChild(i).getText().equals(rootName)) {
                // Adds the child (which is a subtree) to the list of trees
                subtrees.add(t.getChild(i));
            }
        }
        // Returns the list of trees
        return subtrees;
    }

    /**
     * Extracts all the subtrees which have a specified root name from a
     * specified tree
     * 
     * @param t
     *            the tree
     * @param rootName
     *            the root name
     * @return the extracted subtrees
     */
    public final static Vector<Tree> allSubTrees(Tree t, String rootName) {
        // Declares a list of trees
        Vector<Tree> subtrees = new Vector<Tree>();
        // For each child in the input tree...
        for (int i = 0; i < t.getChildCount(); i++) {
            // If the child name in the same as the input String
            if (t.getChild(i).getText().equals(rootName)) {
                // Adds the child (which is a subtree) to the list of trees
                subtrees.add(t.getChild(i));
                subtrees.addAll(allSubTrees(t.getChild(i), rootName));
            }
            // Else
            else {
                // Recursive call
                subtrees.addAll(allSubTrees(t.getChild(i), rootName));
            }
        }
        // Returns the list of trees
        return subtrees;
    }

    /**
     * Convert the first character of a String to the lower-case corresponding
     * character, then returns the whole transformed String
     * 
     * @param s
     *            the String to transform
     * @return the transformed String
     */
    public final static String fstCharLowerCase(String s) {
        // if the input is null, returns null
        if (s == null)
            return null;
        // else
        else {
            // converts the first character to lower-case and concatenates it
            // with the rest of the String, returns the whole String
            return (s.substring(0, 1).toLowerCase() + s
                    .substring(1, s.length()));
        }
    }

    /**
     * Parses the input String which has the following format :
     * "fileName/s0/s1/s2/.../sn" into a String Vector : 1st element 2nd element
     * 3rd element 4th element ... Last element { fileName ; s0 ; s1 ; s2 ; ... ;
     * sN }
     * 
     * @param bloSource
     *            the String to parse (i.e a "BlockSource" field value from .mdl
     *            file)
     * @return the String Vector created from the input String
     */
    public static final Vector<String> parseBlockSource(String bloSource) {
        // Creates a new String Vector
        Vector<String> pathSteps = new Vector<String>();
        // Puts the input String in a local variable for modifying it
        String currentSubString = bloSource;
        // Saves the index of the first '/' character
        int index = currentSubString.indexOf('/');
        // Loops while there is still at least one '/' character
        while (index != -1) {
            // Extracts the substring before the saved index and adds it to the
            // Vector
            pathSteps.add(currentSubString.substring(0, index));
            // Remove the extracted substring from the currentString
            currentSubString = currentSubString.substring(index + 1,
                    currentSubString.length());
            // Saves the index of the first '/' character
            index = currentSubString.indexOf('/');
        }
        // Extracts the substring before the saved index and adds it to the
        // Vector
        pathSteps.add(currentSubString.substring(0, currentSubString.length()));
        // Returns the String Vector
        return pathSteps;
    }

    /**
     * Retrieves the block source path from a tree corresponding to reference
     * block
     * 
     * @param bloTree	-- AST of the block
     * @param bloType	-- block type
     * 
     * @return String with relative path to the referenced model and block
     */
    public static final String getRefSource(Tree bloTree, String bloType) {
        String bloSource = "";
        if (bloType.equals(TImporterConstant.SK_REFERENCE)) {
            // Retrieves the "BlockSource" field from the input block tree
            bloSource = getAttFromTree(bloTree, 
            		TImporterConstant.SK_SOURCEBLOCK);
        } else if (bloType.equals("ModelReference")) {
            bloSource = getAttFromTree(bloTree, "ModelName");
        }    	
        
        return bloSource;
    }
    
    /**
     * Retrieves the block source path from a tree corresponding to reference
     * block and returns last element of this path (block name in case of 
     * Reference, model name in case of ModelReferece
     * 
     * @param bloTree	-- AST of the block
     * @param bloType	-- block type
     * 
     * @return String with last element of the reference path
     */
    public static final String getRefSourceName(Tree bloTree, String bloType) {
    	// retrieve the block source from tree
        String bloSource = getRefSource(bloTree, bloType);
        // Parses the "BlockSource" field value into a String Vector,
        // the first element is the library file name
        Vector<String> pathSteps = parseBlockSource(bloSource);

        if (pathSteps.size() > 0) {
        	return pathSteps.get(pathSteps.size()-1);
        } else {
        	return "";
        }
    }
    
    /**
     * Resolves references to systems described in library model files
     * 
     * @param bloTree
     *            the block description (block tree) of a the "Reference" block
     * @param bloType
     * @return the system description (system tree) of the "Reference" block
     * @throws Exception
     */
    public static final Tree resolveRef(Tree bloTree, String bloType)
            throws Exception {
    	// retrieve the block source from tree
        String bloSource = getRefSource(bloTree, bloType);
        // Parses the "BlockSource" field value into a String Vector,
        // the first element is the library file name
        Vector<String> pathSteps = parseBlockSource(bloSource);
        // if library file exists
        if (new File(System.getProperty(TImporterConstant.PROP_LIBFOLDER)
                + pathSteps.firstElement() + "." + TImporterConstant.EXT_MDL)
                .exists()) {
            // Parses the library file into an AST
            MDLStructParser parser = getParserMDL(new File(System
                    .getProperty(TImporterConstant.PROP_LIBFOLDER)
                    + pathSteps.firstElement() + "." + TImporterConstant.EXT_MDL));

            // Removes the useless first element of Vector
            // (library file name is not useful anymore)
            pathSteps.removeElementAt(0);
            // Extracts the System description (System tree)
            // from the whole library model tree
            Tree libTree = (CommonTree) parser.parse().getTree();
            Tree sysTree = subTrees(libTree, TImporterConstant.SK_SYSTEM)
                    .firstElement();

            // If the referenced block is the whole library system
            // i.e if the Vector has now only a single element
            if (pathSteps.size() == 0) {
                // returns the whole system tree for processing
                return sysTree;
            }
            // Puts the system tree in a local tree variable
            Tree tmpTree = sysTree;
            // Loops while there is more then one element in the vector
            // i.e while the referenced block has not been reached
            while (pathSteps.size() > 0) {
                boolean found = false;
                if (!subTrees(tmpTree, TImporterConstant.SK_SYSTEM).isEmpty()) {
                    tmpTree = subTrees(tmpTree, TImporterConstant.SK_SYSTEM)
                            .firstElement();
                }
                // Retrieves the block subtrees from the current tmpTree
                Vector<Tree> blockTrees = subTrees(tmpTree,
                        TImporterConstant.SK_BLOCK);
                // For each block subtree ...
                for (int i = 0; i < blockTrees.size(); i++) {
                    // Retrieves the block name
                    String bloName = getAttFromTree(blockTrees.get(i),
                            TImporterConstant.SK_NAME);
                    // If the referenced block is the current block or within it
                    if (pathSteps.size() > 0) {
                        if (bloName.equals(pathSteps.firstElement())) {
                            found = true;
                            // The new current tmpTree becomes the system
                            // description
                            // retrieved from the current block tree
                            tmpTree = blockTrees.get(i);

                            // Removes the first element of the Vector
                            pathSteps.removeElementAt(0);
                        }
                    }
                }
                if (!found) {
                    // Adds a new log ERROR
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            TImporterConstant.TOOL_EVENT_ID,
                            TImporterConstant.EMPTY, "Unable to find block : "
                                    + pathSteps.firstElement() + ".",
                            TImporterConstant.EMPTY);
                }
            }
            // Retrieves all default parameters from library
            Vector<Tree> allDefParamTrees = subTrees(libTree,
                    TImporterConstant.SK_PARAM_DEFAULTS);
            Tree allDefParamTree = null;
            if (!allDefParamTrees.isEmpty()) {
                allDefParamTree = allDefParamTrees.firstElement();
            }

            for (Tree currentBlockTree : allSubTrees(tmpTree,
                    TImporterConstant.SK_BLOCK)) {
                String blockType = getAttFromTree(currentBlockTree,
                        TImporterConstant.SK_BLOCKTYPE);
                Tree defParamTree = null;
                if (allDefParamTree != null) {
                    for (int i = 0; i < allDefParamTree.getChildCount(); i++) {
                        if (blockType.equals((getAttFromTree(allDefParamTree
                                .getChild(i), TImporterConstant.SK_BLOCKTYPE)))) {
                            defParamTree = allDefParamTree.getChild(i);
                        }
                    }
                }
                // Adds block default parameters from library
                if (defParamTree != null) {
                    for (int i = 0; i < defParamTree.getChildCount(); i++) {
                        if (subTrees(currentBlockTree,
                                defParamTree.getChild(i).getText()).isEmpty()) {
                            currentBlockTree.addChild(defParamTree.getChild(i));
                        }
                    }
                }
            }
            for (Tree currentSysTree : allSubTrees(tmpTree,
                    TImporterConstant.SK_SUBSYS)) {
                String blockType = getAttFromTree(tmpTree,
                        TImporterConstant.SK_BLOCKTYPE);
                Tree defParamTree = null;
                if (allDefParamTree != null) {
                    for (int i = 0; i < allDefParamTree.getChildCount(); i++) {
                        if (blockType.equals((getAttFromTree(allDefParamTree
                                .getChild(i), TImporterConstant.SK_BLOCKTYPE)))) {
                            defParamTree = allDefParamTree.getChild(i);
                        }
                    }
                }
                // Adds subsystem default parameters from library
                if (defParamTree != null) {
                    for (int i = 0; i < defParamTree.getChildCount(); i++) {
                        if (subTrees(currentSysTree,
                                defParamTree.getChild(i).getText()).isEmpty()) {
                            currentSysTree.addChild(defParamTree.getChild(i));
                        }
                    }
                }
            }
            // Returns the current tmpTree
            return tmpTree;
        }
        // If library file does not exist
        else {
        	// reference not found, return null to indicate 
        	// that the block should be handled differently
			return null;
        }

    }

    /**
     * Computes the number of blocks in a system description
     * 
     * @param sysTree
     *            the system description to browse
     * @return the number of blocks
     * @throws Exception
     *             a reference block could not been resolved
     */
    public final static int computeNbBlocks(Tree sysTree) throws Exception {
        // Retrieves all the block subTrees
        Vector<Tree> bloTrees = allSubTrees(sysTree, TImporterConstant.SK_BLOCK);
        // Returns the total number of block subtrees
        return bloTrees.size();
    }

    /**
     * Computes the number of signals in a system description
     * 
     * @param sysTree
     *            the system description to browse
     * @return the number of signals
     * @throws Exception
     *             a reference block could not been resolved
     */
    public final static int computeNbSignals(Tree sysTree) throws Exception {
        // The number of signals is initialized to 0
        int nbSignals = 0;
        // Retrieves all line subtrees
        Vector<Tree> linTrees = allSubTrees(sysTree, TImporterConstant.SK_LINE);
        // Browses the list of line subtrees, for each...
        for (int i = 0; i < linTrees.size(); i++) {
            // Computes the number of signals in the current line subtree
            nbSignals += computeNbSubSignals(linTrees.get(i));
        }
        // Retrieves all block subtrees
        Vector<Tree> bloTrees = allSubTrees(sysTree, TImporterConstant.SK_BLOCK);
        // Browses the list of block subtrees, for each...
        for (int i = 0; i < bloTrees.size(); i++) {
            // If the block is a reference block
            String refBloType = getAttFromTree(bloTrees.get(i),
                    TImporterConstant.SK_BLOCKTYPE);
            if (refBloType.equals(TImporterConstant.SK_REFERENCE)) {
                Tree tree = resolveRef(bloTrees.get(i), refBloType);
                if (tree != null
                		&& tree.getText().equals(TImporterConstant.SK_SYSTEM)) {
                    // Recursive call to the method for the resolved block
                    // description
                	// TODO: here we do not count contents of model references
                    nbSignals += computeNbSignals(tree);
                }
            }
        }
        // Returns the total number of signals
        return nbSignals;
    }

    /**
     * Computes the number of signals in a line description
     * 
     * @param t
     *            the line description to browse
     * @return the number of signals
     */
    public final static int computeNbSubSignals(Tree t) {
        // The number of signals is initialized to 0
        int nbSubSignals = 0;
        // Retrieves the direct branch descriptions
        Vector<Tree> branchTrees = subTrees(t, TImporterConstant.SK_BRANCH);
        // If the list of direct branch subtrees is not empty
        if (branchTrees.size() > 1) {
            // Browses the list of direct branch subtrees, for each...
            for (int i = 0; i < branchTrees.size(); i++) {
                // Recursive call to the method for the current branch subtree
                nbSubSignals += computeNbSubSignals(branchTrees.get(i));
            }
        }
        // If empty
        else {
            // It is a signal end so increments the number of signal
            nbSubSignals++;
        }
        // Returns the number of signals
        return nbSubSignals;
    }

    /**
     * Computes the number of hierarchical levels in a system description
     * 
     * @param sysTree
     *            the system description to browse
     * @return the number of hierarchical levels
     * @throws Exception
     *             a reference block could not been resolved
     */
    public final static int computeNbHierLvls(Tree sysTree) throws Exception {
        // The number of hierarchical levels is initialised to 0
        int nbHierLvls = 0;
        // Retrieves the list of direct block subtrees
        Vector<Tree> bloTrees = subTrees(sysTree, TImporterConstant.SK_BLOCK);
        // Declares a counter for the current subtree
        int tmpCounter;
        // Browses the list of direct block subtrees, for each...
        for (int i = 0; i < bloTrees.size(); i++) {
            tmpCounter = 0;
            // If the block is a subsystem
            if (getAttFromTree(bloTrees.get(i), TImporterConstant.SK_BLOCKTYPE)
                    .equals(TImporterConstant.SK_SUBSYS)) {
                // Recursive call to the method for the current subtree
                // and increments the tmpCounter
                tmpCounter += computeNbHierLvls(subTrees(bloTrees.get(i),
                        TImporterConstant.SK_SYSTEM).firstElement());
            }
            // If the block is a reference
            else {
                String refBloType = getAttFromTree(bloTrees.get(i),
                        TImporterConstant.SK_BLOCKTYPE);
                if (refBloType.equals(TImporterConstant.SK_REFERENCE)) {
                    Tree tree = resolveRef(bloTrees.get(i), refBloType);
                    if (tree != null 
                    		&& tree.getText().equals(TImporterConstant.SK_SYSTEM)) {
                        // Recursive call to the method for the resolved block
                        // subtree and increments the tmpCounter
                    	// TODO: here we do not count contents of model references
                        tmpCounter += computeNbHierLvls(tree);
                    }
                }
            }
            // If the tmpCounter is higher than the max counter
            if (tmpCounter > nbHierLvls) {
                // The max counter becomes the tmpCounter
                nbHierLvls = tmpCounter;
            }
        }
        // Increments the counter to take in account the global System
        nbHierLvls++;
        // Returns the counter
        return nbHierLvls;
    }

    /**
     * Computes the complexity of a system description, it means computes the
     * number of blocks, signals, and hierarchical levels. Writes a summary
     * containing these 3 values in a metrics file
     * 
     * @param sysTree
     *            the system description to browse
     * @throws Exception
     *             a reference block could not been resolved
     */
    public final static void computeCpx(Tree sysTree) throws Exception {
        try {
            // Creates a File for the metrics report
            FileWriter f = FileUtil.openFileWriter(getMetricsFilePath(), false);
            // Prints the general header
            f.write(TImporterConstant.CPX_GENERALHEADER + "\n");
            // Prints the input model header
            f.write(TImporterConstant.CPX_INPUTMODELHEADER + "\n");
            // Prints the complexity summary in a file
            f.write(TImporterConstant.CPX_MODELNAME
                    + ArgumentReader.getParameter(GAConst.ARG_INPUTFILE_NAME)
                    + "\n");
            f.write(TImporterConstant.CPX_NBBLOCKS + computeNbBlocks(sysTree)
                    + "\n");
            f.write(TImporterConstant.CPX_NBSIGNALS + computeNbSignals(sysTree)
                    + "\n");
            f.write(TImporterConstant.CPX_NBLEVELS + computeNbHierLvls(sysTree)
                    + "\n");
            f.close();
            // Adds a new log INFO
            EventHandler.handle(EventLevel.INFO,
                    TImporterConstant.TOOL_EVENT_ID, TImporterConstant.EMPTY,
                    TImporterConstant.EVENT_INFO_CPXCOMPUTING,
                    TImporterConstant.EMPTY);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * @return path of the model metrics file
     */
    public static String getMetricsFilePath() {
        return FileUtil.appendPath(ArgumentReader
                .getParameter(GAConst.ARG_TMPFOLDER),
                GAConst.MODEL_METRICS_FILE);
    }

    /**
     * Parses the input file into a tree (AST)
     * 
     * @param inputFile
     *            the inputFile to parse
     * @throws Exception
     *             input file is missing
     */
    public final static MDLStructParser getParserMDL(File inputFile)
            throws Exception {
        FileInputStream inputStream = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        MDLStructLexer lexer = new MDLStructLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MDLStructParser parser = new MDLStructParser(tokens);
        return parser;
    }
}
