//* Gene-Auto code generator
//* 
//*	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/components/PropertyParser.java,v $
//*  @version	$Revision: 1.6 $
//*	@date		$Date: 2009-11-10 08:16:08 $
//*
//*  Copyright (c) 2006-2009 IB Krates OU
//*  	http://www.krates.ee, geneauto@krates.ee
//*
//*  
//*  This program is free software; you can redistribute it and/or modify
//*  it under the terms of the GNU General Public License as published by
//*  the Free Software Foundation, either version 3 of the License, or
//*  (at your option) any later version.
//*
//*  This program is distributed in the hope that it will be useful,
//*  but WITHOUT ANY WARRANTY; without even the implied warranty of
//*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//*  GNU General Public License for more details.
//*
//*  You should have received a copy of the GNU General Public License
//*  along with this program. If not, see <http://www.gnu.org/licenses/>
//*  
//*  Gene-Auto was originally developed by Gene-Auto consortium with support 
//*  of ITEA (www.itea2-org) and national public funding bodies in France, 
//*  Estonia, Belgium and Israel.  
//*  
//*  The members of Gene-Auto consortium are
//*  	Continental SAS
//*  	Airbus France SAS
//*  	Thales Alenia Space France SAS
//*  	ASTRIUM SAS
//*  	IB KRATES OU
//*  	Barco NV
//*  	Israel Aircraft Industries
//*  	Alyotech
//*  	Tallinn University of Technology
//*  	FERIA - Institut National Polytechnique de Toulouse
//*  	INRIA - Institut National de Recherche en Informatique et en Automatique
 
 // base name for the lexer and parser
grammar MdlStateflow;

// generation options
options {
	// the result is an Abstract syntax tree
    	output=AST;
    	// the java type of the result Tree 
    	ASTLabelType=CommonTree; 
}

@header {
package geneauto.tstateflowimporter.parser;
}

@lexer::header {
package geneauto.tstateflowimporter.parser;
}

@rulecatch {
	catch (RecognitionException re) {
		reportError(re);
		throw re;
	}
}

// name of the generated function used to generate the tree
parse       :	mdl;

// High level description of the input model file :
// Simulink part is ignored
mdl         :    sldesc mddesc? sfdesc? -> sfdesc?;

// Simulink description :
sldesc	    :	(MODEL|LIB)^ OPEN! (struct|field)+ CLOSE!;

// Matdata description :
mddesc	    :	MATDATA^ OPEN! (struct|field)+ CLOSE!;

// Stateflow description :
sfdesc	    :	STATEF^ OPEN! (struct|field)+ CLOSE!;

// Description :
struct        :	key=KEY^ OPEN! content=(struct|field)* CLOSE! ;
// for debugging  {System.err.println("Struct: "+$k.getText());};

// Field :
field       :	key=KEY^ value=(INTER|KEY|STRING) ;
// for debugging  {System.err.println("Field: "+$k.getText()+"->"+$v.getText());};

// Keywords
MODEL	:	'Model';
LIB		:	'Library';
STATEF	:	'Stateflow';
MATDATA	:	'MatData';

// description begining and ending character
CLOSE	:	'}';
OPEN	:	'{';	

// COMMENT :
// - A comment is somthing that starts with # character
COMMENT :	'#' (' '|'\t'|KEY)* {skip();};

// STRING :
// - A STRING is a sequence character whithin double quotes
//STRING  :	('"'('\u0020'..'\u2122')+ '"')
//		('\r'?'\n' ('\t')*('"'('\u0020'..'\u2122')+ '"')?)*;
STRING  :	'"' (~('"') | '\\' '"')* '"' ('\r'? '\n' ('\t'|' ')* ('"' (~('"') | '\\' '"')* '"')? )*;
//STRING  :	('"'(('\u0020'..'\u2122')|'\\"')+ '"')
//		('\r'? '\n' ('\t')* ('"'(('\u0020'..'\u2122')|'\\"')* '"')?)*;

// INTER :
// - A INTER (interval) is a sequence of numbers 
//	and some special character within '[' and ']'
INTER	:	('[' ('0'..'9'|' '|','..'.'|';')+ ']');

// KEY :
// - A KEY is a unquoted sequence of characters
KEY	:	('0'..'9'|','|'"'|'%'|'('..')'|'+'..':'|';'|'<'|'>'|'A'..'Z'|'['|']'|'_'|'a'..'z'|'|'|'~'|'$')+;	 

// WS :
// - A WS is a space character or a tabulation and is skipped when found (ignored)
WS	:	(' '|'\t')+ {skip();};

// NEWLINE :
// - A NEWLINE is a carriot return character and is skipped when found (ignored)
NEWLINE	:	'\r'? '\n' {skip();};
