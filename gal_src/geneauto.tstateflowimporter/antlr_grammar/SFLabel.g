//* Gene-Auto code generator
//* 
//*	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/antlr_grammar/SFLabel.g,v $
//*  @version	$Revision: 1.32 $
//*	@date		$Date: 2011-09-15 10:22:33 $
//*
//*  Copyright (c) 2006-2009 IB Krates OU
//*  	http://www.krates.ee, geneauto@krates.ee
//*
//*  
//*  This program is free software; you can redistribute it and/or modify
//*  it under the terms of the GNU General Public License as published by
//*  the Free Software Foundation, either version 3 of the License, or
//*  (at your option) any later version.
//*
//*  This program is distributed in the hope that it will be useful,
//*  but WITHOUT ANY WARRANTY; without even the implied warranty of
//*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//*  GNU General Public License for more details.
//*
//*  You should have received a copy of the GNU General Public License
//*  along with this program. If not, see <http://www.gnu.org/licenses/>
//*  
//*  Gene-Auto was originally developed by Gene-Auto consortium with support 
//*  of ITEA (www.itea2-org) and national public funding bodies in France, 
//*  Estonia, Belgium and Israel.  
//*  
//*  The members of Gene-Auto consortium are
//*  	Continental SAS
//*  	Airbus France SAS
//*  	Thales Alenia Space France SAS
//*  	ASTRIUM SAS
//*  	IB KRATES OU
//*  	Barco NV
//*  	Israel Aircraft Industries
//*  	Alyotech
//*  	Tallinn University of Technology
//*  	FERIA - Institut National Polytechnique de Toulouse
//*  	INRIA - Institut National de Recherche en Informatique et en Automatique

//HINTS
// local and rulebased rewriting rules together cause an exception when generating code

// Grammar for Stateflow state and transition label parsing
grammar SFLabel;

// generation options
options {
	// the result is an Abstract syntax tree
   	output=AST;
   	// the java type of the result Tree 
   	ASTLabelType=CommonTree;
   	
//  backtrack=true;
//	filter=true;
}


tokens {
	Transition;
	Events;
	Guard;
	ConditionStatements;
	TransitionStatements;
	
	EntryActions;
	DuringActions;
	ExitActions;
	OnEventActions;
	BindDecls;
	UnLabelledActions;
	
	CompoundStatement;
	AssignStatement;
	IncStatement;
	DecStatement;
	ExpressionStatement;
	
	BinaryExpression;
	CallExpression;
	IntegerExpression;
    TrueExpression;
    FalseExpression;
	ListExpression;
	RealExpression;
	MemberExpression;
	ParenthesisExpression;
	TernaryExpression;
	UnaryExpression;
	VariableExpression;
	IndexExpressions;
	
	Annotations;
	DotIdent;
}

@header {
package geneauto.tstateflowimporter.parser;

/* move manually next to the class definition after geenrating the parser and
remove this commetnafter that */
@SuppressWarnings("unused")
}

@lexer::header {
package geneauto.tstateflowimporter.parser;

// Required for lexer::members netToken()
/*
*/
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventHandler.EventLevel;
}

@members {
// Semantic predicate - indicates that the C-bitwise operations flag was set in the source language
public boolean cBitOptsOn = false;

protected void mismatch(IntStream input, int ttype, BitSet follow) throws RecognitionException {
	throw new MismatchedTokenException(ttype, input);
}
}

@lexer::members {
// Semantic predicate - indicates that the C-bitwise operations flag was set in the source language
public boolean cBitOptsOn = false;

/* Comment to run AntLRWorks debugger
   Uncomment before release
*/
public Token nextToken() {
  while (true) {
   this.token = null;
   this.channel = Token.DEFAULT_CHANNEL;
   this.tokenStartCharIndex = input.index();
   this.tokenStartCharPositionInLine = input.getCharPositionInLine();
   this.tokenStartLine = input.getLine();
   this.text = null;
   if ( input.LA(1)==CharStream.EOF ) {
    return Token.EOF_TOKEN;
   }
   try {
    mTokens();
    if ( this.token==null ) {
     emit();
    }
    else if ( this.token==Token.SKIP_TOKEN ) {
     continue;
    }
    return this.token;
   }
   catch (RecognitionException re) {
    reportError(re);
    EventHandler.handle( EventLevel.ERROR, "", "", "Errors while parsing - unable to continue.", "");
   }
  }
 }
}

@rulecatch {
	catch (RecognitionException re) {
		reportError(re);
		throw re;
	}
}

// names of the generated functions used to generate the tree
parseTransitionLabel	:	tlabel ;
parseStateLabel 	:	slabel ;

// High level description of the transition label
// Resulting AST structure Transition { Annotations {comment*}, Events {...}?, Guard {...}?, ... }
tlabel
	:	events? 
		(nlComments? guardExpression)? 
		(nlComments? conditionActions)? 
		((nlComments? transitionActions) | nlComments)?
		 -> ^(Transition ^(Annotations nlComments? nlComments? nlComments? nlComments? nlComments?) 
		 		events? guardExpression? conditionActions? transitionActions?)
	;

// Transition label elements
events
	:	cmtIdent
		(	/* empty */ -> ^(Events cmtIdent)
		|	nlComments? ((OR OR? cmtIdent nlComments?)* OR OR? cmtIdent)
/* It turn out to work differently in AntLRWorks and in generated code
				 -> ^(Events cmtIdent cmtIdent* cmtIdent)
*/				 -> ^(Events cmtIdent cmtIdent*)
		)
	;
guardExpression 	:	LSQUB nlComments? expr nlComments? RSQUB -> ^(Guard ^(expr ^(Annotations nlComments? nlComments?))) ;
conditionActions	:	LCURLB stmts? nlComments? RCURLB -> ^(ConditionStatements stmts?) ;
transitionActions	:	SLASH 
	(	nlComments? LCURLB stmts? nlComments? RCURLB -> ^(TransitionStatements ^(Annotations nlComments?) stmts?)
	|	stmts? nlComments? -> ^(TransitionStatements stmts?)
	);

// High level description of the state label
// Resulting AST structure: ID { Annotations {comment*}, Action* }
slabel
	: NL* ident
		(	nlComments? SLASH nlComments? stateActions?
				-> ^(ident ^(Annotations nlComments? nlComments?) stateActions?)
		|	(NL* comments)? (NL+ stateActions)?
				-> ^(ident ^(Annotations comments?) stateActions?)
		|	stateActions -> ^(ident ^(Annotations) stateActions)
		|	unLabelActions -> ^(ident ^(Annotations) unLabelActions)
		)
	;

// State label elements
stateActions	:	stateAction (stateAction)*;
stateAction 	:	entryActions | duringActions | exitActions | onEventActions | bindDecls ;
entryActions	:	EN stmts? nlComments? -> ^(EntryActions stmts?) ;
duringActions	:	DU stmts? nlComments? -> ^(DuringActions stmts?) ;
exitActions 	:	EX stmts? nlComments? -> ^(ExitActions stmts?) ;
onEventActions	:	ON ident COLON stmts? nlComments? -> ^(OnEventActions ident stmts?) ;
bindDecls		:	BIND dotIdents? -> ^(BindDecls dotIdents?) ;
unLabelActions	:	stmts nlComments? -> ^(UnLabelledActions stmts) ;


// Basic SL PARSER RULES

// Statements
stmts	: comtStmt+ ;

// Statement with comments - a statement ends with comma, semicolon or newline
comtStmt
	: nlComments? stmt
			// Statement ends with visible separator
		(	(nlComments? (COMMA | SCOLON))=> nlComments? (COMMA | SCOLON)
				// Tie comments before next newline to this statement
			(	(COMMENT* RCOMMENT? NL)=> COMMENT* RCOMMENT? NL
					 -> ^(stmt ^(Annotations nlComments? nlComments? COMMENT* RCOMMENT?))
				// No comments after separator
			|	/*empty*/ -> ^(stmt ^(Annotations nlComments? nlComments?))
			)
			// Statement ends with newline separator
		|	(COMMENT* RCOMMENT? NL)=> COMMENT* RCOMMENT? NL
				 -> ^(stmt ^(Annotations nlComments? COMMENT* RCOMMENT?))
			// Statement without ending separator
		|	/*empty*/ -> ^(stmt ^(Annotations nlComments?))
		)
	;

nlComments
	:	NL!* comments NL!* 
	|	NL!+ 
	;
comments	:	comment (NL!* comment)* ;
comment		:	RCOMMENT | COMMENT ;

// Statement
stmt:	
	(	varExpr
			(	/* empty */ -> ^(ExpressionStatement varExpr)
			|	PLUS PLUS   -> ^(IncStatement varExpr)
			|	MINUS MINUS -> ^(DecStatement varExpr)
			|	ASSIGN expr -> ^(AssignStatement ASSIGN varExpr expr)
			) 
	)
	|	callExpr -> ^(ExpressionStatement callExpr)
	;

// Expression
expr    	:	lOrExpr^;
lOrExpr 	:	lAndExpr (LOR^ lAndExpr)* ;
lAndExpr 	:	orExpr (LAND^ orExpr)* ;
orExpr 		:	xorExpr (OR^ xorExpr)* ;
xorExpr		:	andExpr (XOR^ andExpr)* ;
andExpr 	:	eqExpr (AND^ eqExpr)* ;
eqExpr		:	compExpr (EQ_OP^ compExpr)?;
compExpr	:	shiftExpr (COMP_OP^ shiftExpr)?;
shiftExpr 	:	sumExpr (SHIFT^ sumExpr)* ;
sumExpr 	:	prodExpr ((PLUS|MINUS)^ prodExpr)* ;
prodExpr	:	prefExpr ((STAR|SLASH|MOD|POWER)^ prefExpr)* ;
prefExpr
	:	MINUS primExpr -> ^(UnaryExpression primExpr MINUS)
	|	PLUS primExpr -> ^(UnaryExpression primExpr PLUS)
	|	NOT primExpr -> ^(UnaryExpression primExpr NOT)
	|	TILDE primExpr -> ^(UnaryExpression primExpr TILDE)
	|	STAR primExpr -> ^(UnaryExpression primExpr STAR)
	|	AND primExpr -> ^(UnaryExpression primExpr AND)
	|	/*Empty*/ primExpr ;
primExpr	:	varExpr | litExpr | intExpr | trueExpr | falseExpr | callExpr | parenExpr ;

varExpr 	:	dotIdent indexExpr* -> ^(VariableExpression dotIdent indexExpr*) ;
indexExpr	:	LSQUB! expr RSQUB! ;
litExpr 	:	NUMBER -> ^(RealExpression NUMBER) ;
intExpr		:	INTEGER -> ^(IntegerExpression INTEGER) ;
trueExpr    :   TRUE -> ^(TrueExpression) ;
falseExpr   :   FALSE -> ^(FalseExpression) ;
parenExpr	:	LPAREN expr RPAREN -> ^(ParenthesisExpression expr) ;
callExpr 	:	dotIdent LPAREN exprList? RPAREN -> ^(CallExpression dotIdent exprList?) ;
exprList	:	expr (COMMA! expr)* ;

// List of identifiers: "a.b.c, e.f"
dotIdents	:	dotIdent (COMMA! dotIdent)* ;
dotIdent	:	dotIdent_  -> ^(DotIdent dotIdent_) ;
dotIdent_	:	ident (DOT ident)* ;
// Identifier with comments
cmtIdent	:	nlComments? ident -> ^(ident ^(Annotations nlComments?)) ;
ident	:	IDENT | ON ;


//LEXER RULES

// Tokens
EN	:	('entry'|'en') (' '|'\t')* ':' ;
DU	:	('during'|'du') (' '|'\t')* ':' ;
EX	:	('exit'|'ex') (' '|'\t')* ':' ;
ON	:	'on' ;
//ON_EVENT	:	('on')  (' '|'\t')+  IDENT (' '|'\t')* ':' ;
BIND:	'bind' (' '|'\t')* ':' ;
WS	:	(' '|'\t'|'...\\n') {skip();} ;
NL 	:	'\\n' ;

// Comment is everything between "/*" and "*/"
COMMENT	: '/*'! .* '*/'! ;
// Right comment is everything but "\n" after "//"
//RCOMMENT: '//' (~'\\' | '\\' ~'n' )* ;
RCOMMENT: '//'! (~'\\')*;


// Basic SL LEXER RULES

// Tokens
LPAREN	:	'(';
RPAREN	:	')';
LCURLB	:	'{';
RCURLB	:	'}';
LSQUB	:	'[';
RSQUB	:	']';
DOT 	:	'.';
COMMA	:	',';
COLON	:	':';
SCOLON	:	';';
PLUS	:	'+';
MINUS	:	'-';
STAR	:	'*';
SLASH	:	'/';
MOD 	:	'%%';
LAND 	:	'&&';
LOR  	:	'||';
AND 	:	'&';
OR  	:	'|';
POWER 	:	{!cBitOptsOn }?=> '^';
XOR 	:	{ cBitOptsOn }?=> '^';
SHIFT	:	('<<'|'>>');
EQ_OP	:	('=='|'!='|'~='|'<>');
COMP_OP	:	('>='|'<='|'>'|'<');
ASSIGN 	:	('='|'+='|'-='|'*='|'/='|'&='|'|='|'^=');
NOT  	:	'!';
TILDE	:	'~';
TRUE    :   'true';
FALSE   :   'false';

//SL_DATA_TYPE	:	('double'|'single'|'int8'|'int16'|'int32'|'uint8'|'uint16'|'uint32'|'boolean');


// an identifier. 
IDENT	:	(LETTER|SYMBOL) (LETTER|SYMBOL|DIGIT)*;
// a number of the form 123, 0.12, 123e4, 123.45e-6, 123.
NUMBER	:	INTEGER	( DOT | DOTNUMBER | EXPONENT )
		| DOTNUMBER;
// a number of the form 1, 123, 0043
INTEGER	:	 (DIGIT+ | '0' ('x'|'X') (DIGIT | 'A'..'F' | 'a'..'f' )+) ;


// Fragments - do not become tokens 
// a number of the form .12, .123e-4,
fragment
DOTNUMBER	:	DOT INTEGER (EXPONENT)? ;
fragment
EXPONENT  :	('e'|'E') PLUS? INTEGER;
fragment
LETTER	: 'a'..'z'|'A'..'Z';
fragment
DIGIT	: '0'..'9';
fragment
SYMBOL	:	'_'|'$' ;

//WS		:	(' '|'\t') {skip();};

/* Comment to run AntLRWorks debugger
   Uncomment before release
*/
ANY :  . {
EventHandler.handle( EventLevel.ERROR, "labelParser", "",
	"Unsupported character \'" + $text + "\' - skipped ", "" );
skip();
};
