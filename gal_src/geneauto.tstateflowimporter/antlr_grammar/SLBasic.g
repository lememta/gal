//* Gene-Auto code generator
//* 
//*	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/antlr_grammar/SLBasic.g,v $
//*  @version	$Revision: 1.2 $
//*	@date		$Date: 2009-11-10 08:19:27 $
//*
//*  Copyright (c) 2006-2009 IB Krates OU
//*  	http://www.krates.ee, geneauto@krates.ee
//*
//*  
//*  This program is free software; you can redistribute it and/or modify
//*  it under the terms of the GNU General Public License as published by
//*  the Free Software Foundation, either version 3 of the License, or
//*  (at your option) any later version.
//*
//*  This program is distributed in the hope that it will be useful,
//*  but WITHOUT ANY WARRANTY; without even the implied warranty of
//*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//*  GNU General Public License for more details.
//*
//*  You should have received a copy of the GNU General Public License
//*  along with this program. If not, see <http://www.gnu.org/licenses/>
//*  
//*  Gene-Auto was originally developed by Gene-Auto consortium with support 
//*  of ITEA (www.itea2-org) and national public funding bodies in France, 
//*  Estonia, Belgium and Israel.  
//*  
//*  The members of Gene-Auto consortium are
//*  	Continental SAS
//*  	Airbus France SAS
//*  	Thales Alenia Space France SAS
//*  	ASTRIUM SAS
//*  	IB KRATES OU
//*  	Barco NV
//*  	Israel Aircraft Industries
//*  	Alyotech
//*  	Tallinn University of Technology
//*  	FERIA - Institut National Polytechnique de Toulouse
//*  	INRIA - Institut National de Recherche en Informatique et en Automatique

// basic Simulink lexer and parser rules - to be imported by specialized parsers
grammar SLBasic;

// Basic SL PARSER RULES

// Statements
stmts	:	stmt ( stmtSepar! stmt )*;
stmtSepar	:	SCOLON;
/*
stmt	:	assignStmt | incdecStmt;
assignStmt	:	varExpr EQ expr;
incdecStmt	:
	(	varExpr (PLUS PLUS|MINUS MINUS)
	|	(PLUS PLUS|MINUS MINUS) varExpr
	);
*/	
stmt
	:	varExpr^
	( (PLUS PLUS | MINUS MINUS)	// Unary inc/dec StatementExpression
	| (EQ expr)	// AssignExpression
	);

// Expressions
typedExpr:	SL_DATA_TYPE expr;
listExpr	:	expr (COMMA^ expr)*;
expr     :	condExpr;
condExpr :  sumExpr (COND_OPER^ sumExpr)?;
sumExpr  :	prodExpr (operator=(PLUS|MINUS)^ prodExpr)*;
prodExpr :	signExpr (operator=(STAR|SLASH)^ signExpr)*;
signExpr :	(MINUS|PLUS)? primExpr ;
primExpr :	litExpr | funExpr |  parenExpr;

parenExpr	:	LPAREN^ expr RPAREN!;
funExpr  :	IDENT^ LPAREN! listExpr RPAREN! ;
litExpr	 :  varExpr | NUMBER;

varExpr   :	IDENT (LSQUB! expr RSQUB!)* ;
// List of identifiers: "a.b.c, e.f"
dotIdents	: dotIdent (COMMA! dotIdent)*;
dotIdent	: IDENT (DOT IDENT)*;


// Basic SL LEXER RULES

// Tokens
LPAREN	:	'(';
RPAREN	:	')';
LCURLB	:	'{';
RCURLB	:	'}';
LSQUB	:	'[';
RSQUB	:	']';
DOT 	:	'.';
COMMA	:	',';
COLON	:	':';
SCOLON	:	';';
PLUS	:	'+';
MINUS	:	'-';
STAR	:	'*';
SLASH	:	'/';
OR  	:	'|';
AND 	:	'&';
EQ  	:	'=';
NOT  	:	'!';
LSH  	:	'<<';
RSH  	:	'>>';
COND_OPER	:	('>'|'<'|'>='|'<='|'=='|'!=');
SL_DATA_TYPE	:	('double'|'single'|'int8'|'int16'|'int32'|'uint8'|'uint16'|'uint32'|'boolean');

// an identifier. 
IDENT	: (LETTER|SYMBOL) (LETTER|SYMBOL|DIGIT)*;
// a number of the form 123, 0.12, 123e4, 123.45e-6
NUMBER	:	INTEGER	( /* empty */ | DOTNUMBER | EXPONENT );
// a number of the form .12, .123e-4
DOTNUMBER	:	DOT INTEGER (EXPONENT)? ;
// a number of the form 1, 123, 0043
INTEGER	: DIGIT+;

// Fragments - do not become tokens 
fragment
EXPONENT  :	('e'|'E') (PLUS|MINUS)? INTEGER;
fragment
LETTER	: 'a'..'z'|'A'..'Z';
fragment
DIGIT	: '0'..'9';
fragment
SYMBOL	:	'_'|'$';

WS		:	(' '|'\t') {skip();};
