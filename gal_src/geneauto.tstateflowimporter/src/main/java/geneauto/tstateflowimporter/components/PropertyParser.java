/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/components/PropertyParser.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2010-02-02 12:26:10 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.components;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.utilities.emlconverter.EMLAccessor;

/**
 * This class is intended for parsing property fields.
 * 
 * TODO Move this class to a common location for both Simulink and Stateflow
 * importers
 */
public class PropertyParser {

    /**
     * @param s
     *            String to parse
     * @return Parsed Expression object. Null, when s is empty
     * 
     *         NOTE: Currently only null, InheritedValue or ExpressionValue are
     *         returned.
     */
    public static Expression parseProperty(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        } else if (s.equals("-1")) {
        	EventHandler.handle(EventLevel.ERROR, "PropertyParser.parseProperty(..)", "", 
        			"Inherited values not supported");
            return null;
        }
        else {
            Expression exp = null;
            exp = EMLAccessor.convertEMLToCodeModel(s, false);
            return exp;
        }
    }   

}
