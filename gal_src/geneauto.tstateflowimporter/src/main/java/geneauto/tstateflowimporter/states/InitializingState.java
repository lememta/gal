/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/states/InitializingState.java,v $
 *  @version	$Revision: 1.25 $
 *	@date		$Date: 2011-07-07 12:24:00 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.modelfactory.factories.BlockLibraryFactory;
import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gablocklibrary.GABlockLibrary;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.tstateflowimporter.main.StateflowImporter;
import geneauto.utils.ArgumentReader;
import geneauto.utils.FileUtil;
import geneauto.utils.GAConst;

import java.util.ArrayList;
import java.util.List;

/**
 * First state which is executed by the StateflowImporter tool. It checks the
 * command-line parameters, imports the model from the xml file and initializes
 * the whole tool.
 */
public class InitializingState extends StateflowImporterState {

    private static InitializingState instance;

    public InitializingState() {
        // set state name for messages
        super("Initializing");
    }

    public static InitializingState getInstance() {
        if (instance == null) {
            instance = new InitializingState();
        }

        return (InitializingState) instance;
    }

    /**
     * Reads the systemModel. Doing this, the library file is read too.
     */
    public void stateExecute() {
        // get the tool parameters.
        // These parameter names should match the ones defined in the machine
        // constructor
        String mdlFile = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);
        String inputSMFile = ArgumentReader
                .getParameter(GAConst.ARG_INPUTSMFILE);
        String outputSMFile = ArgumentReader
                .getParameter(GAConst.ARG_OUTPUTFILE);
        String blockLibFile = ArgumentReader.getParameter(GAConst.ARG_LIBFILE);

        // Check if we can read input
        FileUtil.assertCanRead(mdlFile);
        // Check if we can read input
        FileUtil.assertCanRead(inputSMFile);
        // Check if we can write output
        FileUtil.assertCanWrite(outputSMFile);

        // Form list of the input files of the Block Library Factory
        List<String> blockLibFiles = new ArrayList<String>();
        if (ArgumentReader.getParameter(GAConst.ARG_DEFLIBFILE) != null) {
            blockLibFiles.add(ArgumentReader
                    .getParameter(GAConst.ARG_DEFLIBFILE));
        } else {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
                    "Default block library is missing");
        }
        if (blockLibFile != null) {
            blockLibFiles.add(blockLibFile);
        }

        // Create the blockLibraryFactory
        BlockLibraryFactory blockLibraryFactory = new BlockLibraryFactory(
        		new GABlockLibrary(), StateflowImporter.TOOL_NAME, blockLibFiles, null);

        // Create the systemModelFactory
		SystemModelFactory systemModelFactory = new SystemModelFactory(
				new GASystemModel(), StateflowImporter.TOOL_NAME, inputSMFile,
				outputSMFile, blockLibraryFactory, null);
        getMachine().setSystemModelFactory(systemModelFactory);

        // Read the model and store it to the machine
        getMachine().getSystemModelFactory().readModel();
        getMachine().setSystemModel(
                getMachine().getSystemModelFactory().getModel());

        GASystemModel systemModel = getMachine().getSystemModel();
        // Collect all chart blocks into a list and store to the machine
        getMachine().setChartBlocks(
                systemModel.getAllElements(ChartBlock.class));

        // If this is no charts block
        if(getMachine().getChartBlocks().size()== 0)
        {
        	getMachine().stop();
        }
        else
        {
        	// Go to next state - Parsing
            getMachine().setState(ParsingState.getInstance());
        }
    }

}
