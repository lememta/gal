/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/states/HandlePortsState.java,v $
 *  @version	$Revision: 1.1 $
 *	@date		$Date: 2010-02-13 17:35:55 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.states;

import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.utilities.PortResolver;

/**
 */
public class HandlePortsState extends StateflowImporterState {

	private GASystemModel systemModel;
    private static HandlePortsState instance;
	
    public HandlePortsState() {
        // set state name for messages
        super("HandlePorts");
    }


    public static HandlePortsState getInstance() {
        if (instance == null) {
            instance = new HandlePortsState();
        }

        return (HandlePortsState) instance;
    }

    @Override
    public void stateEntry() {
        super.stateEntry();

        systemModel = getMachine().getSystemModel();
    }

    @Override
    public void stateExecute() {

        /*
         * Remove mux and demux occurrences on control signal path.
         */
        PortResolver.cleanControlPath(systemModel);

    	/*
         * Check if StateFlow introduced new OutControlPorts and
         * replace ports in the end of signals starting from those
         * ports with InControlPorts 
         */
        PortResolver.feedForwardControlPorts(systemModel);

        /*
         * Check once more for Mux occurrences (just in case there was 
         * control signals before that did not reach Mux block
         */
        PortResolver.cleanControlPath(systemModel);
        
        getMachine().setState(NameResolvingState.getInstance());
    }
}
