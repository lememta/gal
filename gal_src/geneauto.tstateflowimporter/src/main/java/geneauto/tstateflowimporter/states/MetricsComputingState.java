/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/states/MetricsComputingState.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2011-07-07 12:24:00 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.common.Function_SM;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gastatemodel.Box;
import geneauto.models.gasystemmodel.gastatemodel.State;
import geneauto.models.gasystemmodel.gastatemodel.Transition;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.tsimulinkimporter.utils.TImporterUtilities;
import geneauto.utils.FileUtil;

import java.io.FileWriter;
import java.io.IOException;



public class MetricsComputingState extends StateflowImporterState {

	private static MetricsComputingState instance;
	
	public MetricsComputingState() {
		// set state name for messages
		super("Computing metrics of charts");
	}

	public static MetricsComputingState getInstance(){
		if (instance == null){
			instance = new MetricsComputingState();
		}
		
		return (MetricsComputingState) instance;
	}

    public void stateExecute() {
        
        try {
            computeChartMetrics();
        } catch (IOException e) {
            EventHandler.handle(EventLevel.ERROR, "", "", "Error computing chart metrics.", e);
        }        

        getMachine().setState(SavingState.getInstance());
    }

    /**
     * @throws IOException
     */
    private void computeChartMetrics() throws IOException {
        
        GASystemModel sm = getMachine().getSystemModel();
        int nCharts=0, nStates=0, nTransitions=0, nFuns=0, nVars=0;
        int maxChartDepth=1; // Flat chart has depth 1
        
        for (GAModelElement e : sm.getAllElements()) {
            int depth=0;
            if (e instanceof ChartBlock) {                
                nCharts++;
            } else if (e instanceof Box) {
                depth = ((Box) e).getPath(PathOption.STATEMODEL).size();
            } else if (e instanceof State) {
                nStates++;
                depth = ((State) e).getPath().size();
            } else if (e instanceof Transition) {
                nTransitions++;
            } else if (e instanceof Function_SM) {
                nFuns++;
                depth = ((Function_SM) e).getPath(PathOption.STATEMODEL).size();
            } else if (e instanceof Variable_SM) {
                nVars++;
            }
            if (depth > maxChartDepth) {
                maxChartDepth = depth;
            }
        }
        
        FileWriter f = FileUtil.openFileWriter(TImporterUtilities.getMetricsFilePath(), true);        
        
        f.write("Number of charts: " + nCharts + "\n");
        if (nCharts > 0) {
            f.write("Number of states in charts: " + nStates + "\n");
            f.write("Number of transitions in charts: " + nTransitions + "\n");
            f.write("Number of functions in charts: " + nFuns + "\n");
            f.write("Number of variables in charts: " + nVars + "\n");            
            f.write("Maximum chart depth: " + maxChartDepth + "\n");            
        }
        
        f.close();
    }
}
