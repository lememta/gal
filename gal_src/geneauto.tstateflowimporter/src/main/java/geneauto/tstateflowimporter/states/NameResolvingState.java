/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/states/NameResolvingState.java,v $
 *  @version	$Revision: 1.2 $
 *	@date		$Date: 2011-07-07 12:24:00 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.states;

import static geneauto.models.utilities.assertions.Assert.assertNameNotEmpty;
import static geneauto.utils.assertions.Assert.assertNotEmpty;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.common.NameSpaceElement;
import geneauto.models.gacodemodel.expression.CallExpression;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.expression.statemodel.IsOpenExpression;
import geneauto.models.gasystemmodel.common.ContainerNode;
import geneauto.models.gasystemmodel.common.Function_SM;
import geneauto.models.gasystemmodel.common.Node;
import geneauto.models.gasystemmodel.common.PathOption;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gastatemodel.State;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.NameResolvingContext;
import geneauto.utils.StringUtils;
import geneauto.utils.map.ConstantMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class NameResolvingState extends StateflowImporterState {

    private static NameResolvingState instance;

    public NameResolvingState() {
        // set state name for messages
        super("Resolving by-name references");
    }

    public static NameResolvingState getInstance() {
        if (instance == null) {
            instance = new NameResolvingState();
        }

        return (NameResolvingState) instance;
    }

    /**
     * 
     */
    public void stateExecute() {

        // Resolve names
        resolveNames();

        // Go to next state
        getMachine().setState(MetricsComputingState.getInstance());
    }

    /**
     * Resolves by-name references.
     */
    private void resolveNames() {

        // Resolve StateModel specific function calls.
        resolveSMCalls();

        // Resolve named references in Charts or Systems that contain Charts.
        resolveHierarchicalReferences();
    }

    /**
     * Resolve StateModel specific function calls.
     */
    private void resolveSMCalls() {
        for (ChartBlock cb : getMachine().getChartBlocks()) {
            
            // Map of ContainerNodes in one Chart for fast access, indexed
            // by name
            Map<String, List<ContainerNode>> containerNodeMap = new HashMap<String, List<ContainerNode>>();

            // Collect named ContainerNodes to a map
            namedContainerNodesToMap(cb, containerNodeMap);

            // resolve SM calls
            for (GAModelElement el : cb
                    .getAllChildren(CallExpression.class)) {
                resolveSMCall(cb, containerNodeMap, (CallExpression) el);
            }
        }
    }

    /**
     * Resolve named references in Charts or Systems that contain Charts. For
     * efficiency, we do not look into other Blocks.
     * 
     * NOTE: The process could be made even more efficient, when only the
     * path to the Charts would be processed and not the alternative
     * branches.
     */
    private void resolveHierarchicalReferences() {
        List<Block> blocksToProcess = new LinkedList<Block>();
        for (ChartBlock cb : getMachine().getChartBlocks()) {
            List<Node> path = cb.getPath(PathOption.COMPLETE);
            blocksToProcess.add((Block) path.get(0));
        }

        /*
         * Collect all exported names and store them in a temporary map.
         * 
         * Currently, we assume that such names are created only by ChartBlocks.
         * Hence, we scan all ChartBlocks and ask for the exported elements from
         * them.
         * 
         * Example, why these names must be handled separately:
         * 
         * If a system contains ChartA and ChartB, and ChartA contains exported
         * function fun1, then this should be visible from ChartB, although
         * hierarchically it is in a parallel branch.
         * 
         * If, in the future, there will be other sources of exported names that
         * aren't available via a normal hierarchical search, then these must be
         * collected also to this map, before the name resolving!
         * 
         * Note! The general CodeModel structure does not exist yet at this
         * point. Hence, we can't just look up all Modules elements from the
         * CodeModel.
         */
        HashMap<String, GAModelElement> nameMapVar = new HashMap<String, GAModelElement>();
        for (ChartBlock cb : getMachine().getChartBlocks()) {
            exportedNamesToMap(cb, nameMapVar);
        }

        // Create a constant map with top-level names as a base for name
        // resolving in Charts
        ConstantMap<String, GAModelElement> nameMapConst = new ConstantMap<String, GAModelElement>(
                nameMapVar, false);

        // Generic hierarchical named reference resolving
        NameResolvingContext.init();
        for (Block b : blocksToProcess) {
            b.resolveReferences(nameMapConst, b.getModel());

        }
    }

    /**
     * Collect top level named elements in a Chart to a map
     */
    private void exportedNamesToMap(ChartBlock cb,
            HashMap<String, GAModelElement> nameMapVar) {
        for (NameSpaceElement nsEl : cb.getExportedElements()) {
            if (!nameMapVar.containsKey(nsEl.getName())) {
                nameMapVar.put(nsEl.getName(), (GAModelElement) nsEl);
            } else {
                EventHandler.handle(EventLevel.ERROR, "resolveNames", "",
                        "Duplicate top-level name in the CodeModel: "
                                + nsEl.getReferenceString(), "");
            }
        }
    }

    /**
     * Collect named ContainerNodes in a Block to a map
     */
    private void namedContainerNodesToMap(Block b,
            Map<String, List<ContainerNode>> nodeMap) {
        for (GAModelElement el : b.getAllChildren(ContainerNode.class)) {
            assertNameNotEmpty(el);
            ContainerNode n = (ContainerNode) el;
            List<ContainerNode> contNodes = nodeMap.get(n.getName());
            if (contNodes == null) {
                contNodes = new LinkedList<ContainerNode>();
                contNodes.add(n);
                nodeMap.put(n.getName(), contNodes);
            } else {
                contNodes.add(n);
            }
        }
    }

    /**
     * Checks, if the given expression is a StateModel specific construct. If
     * so, then tries to resolve the named references in it and convert the
     * expression, if necessary.
     * 
     * Handled StateModel specific cases are:
     * <ul>
     * <li>function calls with qualified names, like s1.s2.f() - the qualified
     * function name is resolved and the function reference in the expression is
     * updated.</li>
     * 
     * <li>in(state_name) calls - the state_name is resolved and the expression
     * is substituted with an IsOpenExpression StateModel expression.</li>
     * </ul>
     * 
     * @param cb
     *            ChartBlock for error reporting
     * @param containerNodeMap
     *            Map with named ContainerNodes in the Chart
     * @param callExp
     *            CallExpression to process
     */
    private void resolveSMCall(ChartBlock cb,
            Map<String, List<ContainerNode>> containerNodeMap,
            CallExpression callExp) {

        // Check function call
        resolveQualifiedFunctionCall(cb, containerNodeMap, callExp);

        // Check in(state_name) queries
        resolveInStateQuery(cb, containerNodeMap, callExp);

    }

    /**
     * Check and resolve function calls with qualified names.
     * 
     * @param cb
     *            ChartBlock for error reporting
     * @param containerNodeMap
     *            Map with named ContainerNodes in the Chart
     * @param callExp
     *            CallExpression to process
     */
    private void resolveQualifiedFunctionCall(ChartBlock cb,
            Map<String, List<ContainerNode>> containerNodeMap,
            CallExpression callExp) {

        if (callExp.getFunction() == null) {

            assertNameNotEmpty(callExp);
            String name = callExp.getName();

            if (name.contains(".")) {

                // Call via qualified name
                List<Node> matches = findStateModelNodeByGlobalName(
                        containerNodeMap, name);
                if (matches == null || matches.isEmpty()) {
                    EventHandler.handle(EventLevel.ERROR, "", "",
                            "Unresolved qualified name: " + name + "\nChart: "
                                    + cb.getReferenceString(), "");

                } else if (matches.size() == 1) {

                    Node n = matches.get(0);

                    if (n instanceof Function_SM) {
                        callExp.setFunction((Function_SM) n);
                    } else {
                        EventHandler
                                .handle(
                                        EventLevel.ERROR,
                                        "",
                                        "",
                                        "Error resolving function call: "
                                                + name
                                                + "\nMatched object is not a function: "
                                                + n.getReferenceString()
                                                + "\nElement: "
                                                + callExp.getReferenceString()
                                                + "\nChart: "
                                                + cb.getReferenceString(), "");
                    }

                } else {
                    String msg = "Ambiguous qualified name: " + name
                            + ". Matches: ";
                    for (Node m : matches) {
                        msg += "\n" + m.getReferenceString();
                    }
                    msg += "\nChart: " + cb.getReferenceString();
                    EventHandler.handle(EventLevel.ERROR, "", "", msg, "");
                }
            }
        }
    }

    /**
     * Check and resolve in(state_name) queries.
     * 
     * @param cb
     *            ChartBlock for error reporting
     * @param containerNodeMap
     *            Map with named ContainerNodes in the Chart
     * @param callExp
     *            CallExpression to process
     */
    private void resolveInStateQuery(ChartBlock cb,
            Map<String, List<ContainerNode>> containerNodeMap,
            CallExpression callExp) {

        if ("in".equals(callExp.getName())) {

            // Resolve the state reference in the argument part
            List<Expression> args = callExp.getArguments();

            if (args == null || args.size() != 1) {
                EventHandler.handle(EventLevel.ERROR, "", "",
                        "in(<state_name>) expression should have exactly 1 argument. Expression: "
                                + callExp.getReferenceString(), "");
                return;
            }

            Expression tmpExp = args.get(0);
            if (!(tmpExp instanceof VariableExpression)) {
                EventHandler.handle(EventLevel.ERROR, "", "",
                        "Expecting a VariableExpression in an in(<state_name>) expression, but got: "
                                + tmpExp.getReferenceString(), "");
                return;
            }

            VariableExpression stateRefExp = (VariableExpression) tmpExp;

            assertNameNotEmpty(stateRefExp);
            String stateName = stateRefExp.getName();
            State stateRef = null;

            List<Node> matches = findStateModelNodeByGlobalName(
                    containerNodeMap, stateName);

            if (matches == null || matches.isEmpty()) {
                EventHandler.handle(EventLevel.ERROR, "", "",
                        "Unresolved state reference " + stateName
                                + " in an in(<state_name>) expression: "
                                + stateRefExp.getReferenceString(), "");
                return;

            } else if (matches.size() == 1) {

                Node n = matches.get(0);
                if (n instanceof State) {
                    stateRef = ((State) n);
                } else {
                    EventHandler.handle(EventLevel.ERROR, "", "",
                            "Error resolving state reference " + stateName
                                    + " in an in(<state_name>) expression: "
                                    + callExp.getReferenceString()
                                    + "\nMatched object is not a State: "
                                    + n.getReferenceString(), "");
                    return;
                }

            } else {
                String msg = "Ambiguous state reference " + stateName
                        + " in an in(<state_name>) expression: "
                        + stateRefExp.getReferenceString() + "\nMatches: ";
                for (Node m : matches) {
                    msg += "\n" + m.getReferenceString();
                }
                EventHandler.handle(EventLevel.ERROR, "", "", msg, "");
                return;
            }

            // The State reference stateRef is assumed to be initialised now

            // Replace the old function call by an appropriate expression in the
            // caller
            Expression oldExp = callExp;
            Expression newExp = new IsOpenExpression(stateRef, cb.getModel());

            // Copy any required additional fields
            for (Annotation a : oldExp.getAnnotations()) {
                newExp.addAnnotation(a.getCopy());
            }

            // Replace the old object with a new one in the parent's fields
            oldExp.replaceMe(newExp);
        }
    }

    /**
     * There are two classes of named ContainerNodes in the StateModel: States
     * and Boxes. These ContainerNodes can be referred globally by name either
     * via a simple or qualified name (i.e. with a path that consists of other
     * named ContainerNodes). Additionally, named children of ContainerNodes can
     * be referred globally via a qualified name, e.g. StateA.fun1() or
     * StateA.myVar.
     * 
     * The following named Node referencing principle is used (based on the
     * apparent approach in Stateflow):
     * 
     * <ul>
     * <li>State or Box references go across the whole Chart, i.e. the whole
     * Chart is always searched for a given name</li>.
     * 
     * <li>If no match is found, then it is an error.</li>
     * 
     * <li>If more than one are found, then it is also an error (ambiguity).</li>
     * 
     * <li>Additionally, the name can be qualified, "s1.s2.s3" instead of "s3".
     * The starting point of the path can be anywhere, the path is used for
     * disambiguation.</li>
     * 
     * <li>If anywhere in the chart there are two matching path sections, then
     * it is an error.</li>
     * </ul>
     * 
     * @param containerNodeMap
     *            Map with named ContainerNodes in the Chart
     * @param name
     *            Name of the Node to find. Can be qualified
     * @return list of Nodes found (might be empty).
     */
    private List<Node> findStateModelNodeByGlobalName(
            Map<String, List<ContainerNode>> containerNodeMap, String name) {

        // Split the name into parts
        LinkedList<String> nameParts = new LinkedList<String>(StringUtils
                .splitString(name, '.'));
        assertNotEmpty(nameParts, "nameParts");

        // Resolve the first element in the supplied name
        // It must be a ContainerNode.
        List<ContainerNode> headNodes = containerNodeMap.get(nameParts.pop());

        // Unqualified name can be matched only by a ContainerNode
        // nameParts is now one element less
        if (nameParts.isEmpty()) {
            return new ArrayList<Node>(headNodes);
        }

        // Try to match the rest of the name starting from found (root) nodes
        List<Node> matches = new LinkedList<Node>();
        for (ContainerNode n : headNodes) {
            Node cn = getChildNodeByName(n, new LinkedList<String>(nameParts));
            if (cn != null) {
                matches.add(cn);
            }
        }

        return matches;
    }

    /**
     * @param node
     *            -- ContainerNode to start from
     * @param nameParts
     *            -- parts of the name leading to the searched child. Note: This
     *            must be a private copy list for this method and must not be
     *            empty.
     * @return a child with given name or null, if not found
     */
    private Node getChildNodeByName(ContainerNode node,
            LinkedList<String> nameParts) {
        String name = nameParts.pop();
        for (Node childNode : node.getChildNodes()) {
            if (name.equals(childNode.getName())) {
                if (nameParts.isEmpty()) {
                    // Done with the name
                    return childNode;

                } else {
                    // More parts of the name to process
                    if (childNode instanceof ContainerNode) {
                        return getChildNodeByName((ContainerNode) childNode,
                                nameParts);

                    } else {
                        return null;
                    }
                }
            }
        }
        return null;
    }

}
