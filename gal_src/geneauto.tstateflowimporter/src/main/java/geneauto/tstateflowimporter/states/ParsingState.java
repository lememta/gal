/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/states/ParsingState.java,v $
 *  @version	$Revision: 1.8 $
 *	@date		$Date: 2011-07-07 12:24:00 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.states;

import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.tstateflowimporter.parser.MdlStateflowLexer;
import geneauto.tstateflowimporter.parser.MdlStateflowParser;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

import java.io.FileInputStream;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;

public class ParsingState extends StateflowImporterState {

	private static ParsingState instance;
	
	public ParsingState(){
		// set state name for messages
		super("Parsing");
	}

	public static ParsingState getInstance(){
		if (instance == null){
			instance = new ParsingState();
		}
		
		return (ParsingState) instance;
	}

    public void stateExecute() {

        String mdlFile = ArgumentReader.getParameter(GAConst.ARG_INPUTFILE);

        try {	// Parse the mdl input file and put the result into an ElementList

        	FileInputStream inputStream = new FileInputStream(mdlFile);

        	ANTLRInputStream input = new ANTLRInputStream(inputStream);
        
        	MdlStateflowLexer lexer = new MdlStateflowLexer(input);
        
        	CommonTokenStream tokens = new CommonTokenStream(lexer);
        
        	MdlStateflowParser parser = new MdlStateflowParser(tokens);

        	CommonTree tree = (CommonTree) parser.parse().getTree();
        	
        	getMachine().setParserTree(tree);
        	
        } catch (Exception e) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "", "", 
            		"Failed to parse the input model", e);
        }

        getMachine().setState(TransformingState.getInstance());
    }
}
