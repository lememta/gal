/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/states/StateflowImporterState.java,v $
 *  @version	$Revision: 1.7 $
 *	@date		$Date: 2009-11-10 08:16:09 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.states;

import geneauto.statemachine.State;
import geneauto.tstateflowimporter.main.StateflowImporterMachine;


/**
 * Abstract base class for all Stateflow Importer states
 */
public abstract class StateflowImporterState extends State {
		
	    public StateflowImporterState(String stateName) {
	    	super(stateName);
	    }

		/**
	     * returns reference to the instance of stateMachine
	     * performs type cast to the correct machine type
	     * 
	     * @return StateflowImporter stateMachine
	     */
	    public StateflowImporterMachine getMachine(){
	    	return StateflowImporterMachine.getInstance();
	    }
}
