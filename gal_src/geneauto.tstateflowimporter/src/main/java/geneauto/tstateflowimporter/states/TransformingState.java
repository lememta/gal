/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/states/TransformingState.java,v $
 *  @version	$Revision: 1.102 $
 *	@date		$Date: 2011-09-16 09:22:24 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.states;

import static geneauto.utilities.parser.AntLRUtils.getFieldValue;
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;
import geneauto.models.gacodemodel.Dependency;
import geneauto.models.gacodemodel.ExternalDependency;
import geneauto.models.gacodemodel.expression.Expression;
import geneauto.models.gacodemodel.expression.GeneralListExpression;
import geneauto.models.gacodemodel.expression.StringExpression;
import geneauto.models.gacodemodel.expression.VariableExpression;
import geneauto.models.gacodemodel.operator.AssignOperator;
import geneauto.models.gacodemodel.statement.AssignStatement;
import geneauto.models.gacodemodel.statement.DecStatement;
import geneauto.models.gacodemodel.statement.ExpressionStatement;
import geneauto.models.gacodemodel.statement.IncStatement;
import geneauto.models.gacodemodel.statement.Statement;
import geneauto.models.gacodemodel.statement.statemodel.broadcast.ExternalBroadcast;
import geneauto.models.gadatatypes.TArray;
import geneauto.models.gadatatypes.TPrimitive;
import geneauto.models.gadatatypes.TVoid;
import geneauto.models.gasystemmodel.common.FunctionArgument_SM;
import geneauto.models.gasystemmodel.common.FunctionScope;
import geneauto.models.gasystemmodel.common.Function_SM;
import geneauto.models.gasystemmodel.common.Parameter;
import geneauto.models.gasystemmodel.common.VariableScope;
import geneauto.models.gasystemmodel.common.Variable_SM;
import geneauto.models.gasystemmodel.gafunctionalmodel.Signal;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.Block;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.SystemBlock;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.InDataPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutControlPort;
import geneauto.models.gasystemmodel.gafunctionalmodel.ports.OutDataPort;
import geneauto.models.gasystemmodel.gastatemodel.AndComposition;
import geneauto.models.gasystemmodel.gastatemodel.Box;
import geneauto.models.gasystemmodel.gastatemodel.Composition;
import geneauto.models.gasystemmodel.gastatemodel.Event;
import geneauto.models.gasystemmodel.gastatemodel.EventScope;
import geneauto.models.gasystemmodel.gastatemodel.FlowGraphComposition;
import geneauto.models.gasystemmodel.gastatemodel.GraphicalFunction;
import geneauto.models.gasystemmodel.gastatemodel.Junction;
import geneauto.models.gasystemmodel.gastatemodel.JunctionType;
import geneauto.models.gasystemmodel.gastatemodel.Location;
import geneauto.models.gasystemmodel.gastatemodel.OnEventAction;
import geneauto.models.gasystemmodel.gastatemodel.OrComposition;
import geneauto.models.gasystemmodel.gastatemodel.SimpleAction;
import geneauto.models.gasystemmodel.gastatemodel.State;
import geneauto.models.gasystemmodel.gastatemodel.StateMachineType;
import geneauto.models.gasystemmodel.gastatemodel.StateType;
import geneauto.models.gasystemmodel.gastatemodel.Transition;
import geneauto.models.gasystemmodel.gastatemodel.TransitionType;
import geneauto.models.genericmodel.Annotation;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.models.utilities.DataTypeAccessor;
import geneauto.models.utilities.ModelUtilities;
import geneauto.models.utilities.emlconverter.EMLAccessor;
import geneauto.models.utilities.emlconverter.EMLConverter;
import geneauto.tstateflowimporter.components.PropertyParser;
import geneauto.tstateflowimporter.parser.SFLabelLexer;
import geneauto.tstateflowimporter.parser.SFLabelParser;
import geneauto.utilities.parser.AntLRUtils;
import geneauto.utilities.parser.FunctionSignatureParser;
import geneauto.utils.FileUtil;
import geneauto.utils.StringUtils;
import geneauto.utils.tuple.Pair;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * State where the AST from parser is transformed into state model.
 */
public class TransformingState extends StateflowImporterState {

    private static TransformingState instance;

    /**
     * Elements transformed from AST indexed with their externalID
     */
    private Map<String, GAModelElement> modelElements = 
    	new HashMap<String, GAModelElement>();

    /**
     * Elements transformed from AST whose parents were not available during
     * their transformation together with their parent's externalID.
     */
    private Map<GAModelElement, String> foundlings = 
    	new HashMap<GAModelElement, String>();

    private int eventCounter;

    /**
     * Map of container element - boolean value pairs to trigger whether &, |
     * and ^ are bitwise or logical operations in that container
     */
    private Map<GAModelElement, Boolean> bitwiseMap = 
    	new HashMap<GAModelElement, Boolean>();

    /**
     * Map to hold annotations for containers before their transformation
     */
    private Map<String, List<Annotation>> annotations = 
    	new HashMap<String, List<Annotation>>();

    /**
     * A list to keep all seen AND States. It is used after the children and
     * parents have been connected to refine the type of Compositions, where
     * required.
     */
    private List<State> andStates = new LinkedList<State>();

    /**
     * A list to keep all seen transitions.
     */
    private List<Transition> transitions = new LinkedList<Transition>();

    /**
     * A list to keep all seen Charts.
     */
    private List<ChartBlock> charts = new LinkedList<ChartBlock>();

    public TransformingState() {
        // set state name for messages
        super("Transforming");
    }

    public static TransformingState getInstance() {
        if (instance == null) {
            instance = new TransformingState();
        }
        return (TransformingState) instance;
    }

    public void stateExecute() {
    	// Init the Event object counter.
    	eventCounter = Event.getFirstUserEventCode();
    	// Init AND State list.
    	andStates.clear();
    	// 1'st transformation round - Transform parse tree into State Model
    	transformToSystemModel();
    	// 2'nd transformation round - Add children to their parents
    	// (treat elements that remained without parent during the 1'st
    	// transformation round)
    	connectParentChildren();
        // 3'rd transformation round - Refine types of Compositions, where
        // required.
    	transformAndCompositions();
    	// 4'th transformation round - Parse state and transition labels
    	// (add actions)
    	transformLabelsAndGrFuns();
        // Check, if transitions are well-formed 
        checkTransitions();     
        // 5'th transformation round - Sort the States and transitions 
        // according to their executionOrder
    	sortCharts();
    	EventHandler.handle(EventLevel.INFO, "", "",
    			"Imported " + getMachine().getChartBlocks().size() + " charts", 
    			"");
        // Go to next state
        getMachine().setState(HandlePortsState.getInstance());
    }

    /**
     * Check, if all kept transitions are well-formed.
     * We postponed some errors up to here to get a more informative error message.
     */
    private void checkTransitions() {
        // Check, if the destination of the transition 
        // has been found
        for (Transition t : transitions) {
            if (t.getDestination() == null) {
                EventHandler.handle(EventLevel.ERROR, "", "",
                    "Destination of a transition not found."
                        + "\nTransition: " + t.getReferenceString()
                        + ", externalID: " + t.getExternalID());
            }            
        }        
    }

    /**
     * During the initial parsing/transforming phase we cannot deduce the
     * Composition's type. This can be only deduced by the children of the
     * Composition, i.e. States. States of type AND_State must belong to an AND
     * Composition.
     */
    private void transformAndCompositions() {
        for (State s : andStates) {
            Composition c = (Composition) s.getParent();
            if (!(c instanceof AndComposition)) {
                AndComposition ac = c.toAndComposition();                
                c.replaceMe(ac);
            }
        }

    }

    /**
     * Builds a GASystemModel from the parser generated model tree (AST)
     */
    private void transformToSystemModel() {
        // Stateflow specification AST composed by mdl parser
        Tree t = getMachine().getParserTree();
        // Root level system block
        SystemBlock rsb = getMachine().getSystemModel().getRootSystemBlock();
        // Current Chart Block - here we store chart elements
        ChartBlock cb;
        // Loop the sections of Stateflow AST and take action according to their
        // type.
        // We call sections the top-level branches of the AST and
        // elements the sub-branches.
        for (int i = 0; i < t.getChildCount(); i++) {
            Tree source = t.getChild(i);
            if ("machine".equals(source.getText())) {
                // Ignore this - the stateflow AST starts with this section
            } else if ("chart".equals(source.getText())) {
                // Start of a new chart. Read the chart section and
                // store the attributes into the corresponding ChartBlock
                String chartFileNum = AntLRUtils.getFieldValue(source,
                        "chartFileNumber");
                cb = locateChartBlock(Integer.parseInt(chartFileNum));
                // Process sections starting from chart until instance section
                i = transformChartContent(t, i, cb);
            } else if ("junction".equals(source.getText())) {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformToSystemModel", "dev-xx",
                        "Inconsistency in mdl file - junction section "
                                + "detected outside chart-instance scope", "");
            } else if ("transition".equals(source.getText())) {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformToSystemModel", "dev-xx",
                        "Inconsistency in mdl file - transition section "
                                + "detected outside chart-instance scope", "");
            } else if ("state".equals(source.getText())) {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformToSystemModel", "dev-xx",
                        "Inconsistency in mdl file - state section "
                                + "detected outside chart-instance scope", "");
            } else if ("instance".equals(source.getText())) {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformToSystemModel", "dev-xx",
                        "Inconsistency in mdl file - instance section "
                                + "detected outside chart-instance scope", "");
            } else if ("data".equals(source.getText())) {
                rsb.addVariable(transformDataSection(source, rsb));
            } else if ("event".equals(source.getText())) {
                rsb.addEvent(transformEventSection(source, rsb));
            } else if ("target".equals(source.getText())) {
            	/*
            	 * Get the include statements from the customCode field and 
            	 * store to all ChartBlock's
            	 */
            	String value = AntLRUtils.getFieldValue(source, "customCode", false);
            	if (value.equals("")) {
            		continue;
            	}
            	List<Dependency> dependencies = new ArrayList<Dependency>();
            	String customCode = StringUtils.cleanMultilineString(value);
            	String[] lines = customCode.split("\\\\n");

                // Parse the lines and extract the header files
                String strPattern = "#include\\s*[<\"](.+)[>\"]\\s*";
                Pattern p = Pattern.compile(strPattern);            	
            	
            	for (String line : lines) {
            	    
                    Matcher m = p.matcher(line);
                    if (m.matches()) {
                        boolean isSystem;
                        boolean nonStdExt;
                        String fullName = m.group(1);
                        
                        if (line.contains("<")) {
                            isSystem = true;
                        } else {
                            isSystem = false;
                        }
                        
                        Pair<String, String> nameParts = FileUtil.splitFileByExtension(fullName);
                        String name;
                        if (!"h".equals(nameParts.getRight())) {                        
                            nonStdExt = true;
                            name = fullName;
                        } else {
                            nonStdExt = false;
                            name = nameParts.getLeft();
                        }
                        dependencies.add(new ExternalDependency(name, isSystem, nonStdExt));                        
                        
                    } else {
                        EventHandler.handle(EventLevel.ERROR,
                                "transformToSystemModel", "dev-xx", 
                                "Failed to parse included header name from the CustomCode field. Line: "
                                    + line , "");                        
                    }
            	}
            	// Store the dependencies to all charts
            	if (dependencies.size() > 0) {
                    for (ChartBlock c : getMachine().getChartBlocks()) {
                    	c.setExternalDependencies(dependencies);
                    }
            	}
                // Store the customCode as annotations of the root SystemBlock
            	/*
                if (!s.equals("")) { 
                	rsb.addAnnotation(
                			"Custom code included from the statemodel\n" + s +
                			"\nEnd of Custom code included from the statemodel");
                }
                 */
            } else {
                // For robustness - code that should be never visited
                /*
                EventsHandler.handle(EventLevel.WARNING,
                        "transformToSystemModel", "dev-xx", "Unknown section "
                                + source.getText() + " - left unhandled", "");
                */
            }
        }
    }

    /**
     * Process sections of the given AST starting from the given location and
     * store the content to the given chart block. We assume that instance
     * section ends the chart content definition
     */
    private int transformChartContent(Tree t, int location, ChartBlock cb) {
        int i;
        for (i = location; i < t.getChildCount(); i++) {
            Tree source = t.getChild(i);
            if ("chart".equals(source.getText())) {
                transformChartSection(source, cb);
            } else if ("junction".equals(source.getText())) {
                transformJunctionSection(source, cb);
            } else if ("transition".equals(source.getText())) {
                transformTransitionSection(source, cb);
            } else if ("state".equals(source.getText())) {
                transformStateSection(source, cb);
            } else if ("data".equals(source.getText())) {
                transformDataSection(source, cb);
            } else if ("event".equals(source.getText())) {
                transformEventSection(source, cb);
            } else if ("instance".equals(source.getText())) {
                // This element ends the chart definition list of section
                // Return to the main AST transformation method
                break;
            } else {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR, "transformChartContent",
                        "dev-xx", "Unknown section " + source.getText(), "");
                break;
            }
        }
        return i;
    }

    /**
     * Reads the elements of the chart section and fills the corresponding
     * attributes of ChartBlock.
     */
    private void transformChartSection(Tree chartSection, ChartBlock cb) {
        // Register chart
        charts.add(cb);
        
        // First let's create a composition - dummy to start with
        cb.setComposition(new OrComposition());

        handleCommonElements(chartSection, cb);

        // Triggers whether the chart is executed during the initialisation
        if (AntLRUtils.getFieldValue(chartSection, "executeAtInitialization")
                .equals("1")) {
            cb.setExecuteAtInit(true);
        } else {
            cb.setExecuteAtInit(false);
        }

        // Triggers whether the chart's functions are exported
        if (AntLRUtils.getFieldValue(chartSection, "exportChartFunctions")
                .equals("1")) {
            cb.setExportFunctions(true);
        } else {
            cb.setExportFunctions(false);
        }

        // Remember if in this ChartBlock the single symbol operations
        // are bitwise or logical
        if ("1".equals(getFieldValue(chartSection, "actionLanguage"))) {
            bitwiseMap.put(cb, true);
        } else {
            bitwiseMap.put(cb, false);
        }

        // Rise a WARNING if the model contains elements we do not know
        // List of known elements
        List<String> knownElements = Arrays.asList("id", "name",
                "chartFileNumber", "executeAtInitialization",
                "exportChartFunctions", "", "windowPosition", "viewLimits",
                "screen", "firstTransition", "firstJunction", "viewObj",
                "visible", "machine", "subviewS", "decomposition",
                "firstEvent", "firstData", "updateMethod", "sampleTime",
                "disableImplicitCasting", "treeNode", "zoomFactor");
        // Check whether all elements of the section are known
        leftoverCheck(chartSection, knownElements, "chart", cb);
    }

    /**
     * Reads the elements of the junction section and fills the corresponding
     * attributes of ChartBlock.
     */
    private void transformJunctionSection(Tree junctionSection, ChartBlock cb) {
        // First let's create the junction and handle it's common elements
        Junction j = new Junction();
        handleCommonElements(junctionSection, j);

        // Check the junction type - currently only CONNECTIVE_JUNCTION is
        // supported
        if (AntLRUtils.getFieldValue(junctionSection, "type").equals(
                "CONNECTIVE_JUNCTION")) {
            j.setJunctionType(JunctionType.CONNECTIVE_JUNCTION);
        } else {
            EventHandler.handle(EventLevel.ERROR, "transformJunction",
                    "dev-xx", "Unsupported junction type detected in block "
                            + cb.getReferenceString(), "");
        }

        // Rise a WARNING if the model contains elements we do not know
        // List of known elements
        List<String> knownElements = Arrays.asList("id", "linkNode", "type",
                "", 
                // Ignored known fields
                "autogen", "visible", "position", "chart", "subviewer",
                "arrowSize", "");
        // Check whether all elements of the section are known
        leftoverCheck(junctionSection, knownElements, "junction", cb);
    }

    /**
     * Reads the elements of the state section and fills the corresponding
     * attributes of ChartBlock.
     */
    private void transformStateSection(Tree stateSection, ChartBlock cb) {
        String value;
        // Handle note box (annotation)
        value = getFieldValue(stateSection, "isNoteBox");
        if (value.equals("1")) {

            // Determine the parent of the annotation
            String triple = AntLRUtils.getFieldValue(stateSection, "linkNode");
            if (triple.equals("")) {
                triple = AntLRUtils.getFieldValue(stateSection, "treeNode");
            }
            String parentExtID = parseParentTriple(triple);

            // Store the annotation in the map.
            List<Annotation> as = annotations.get(parentExtID);
            if (as == null) {
                as = new LinkedList<Annotation>();
                annotations.put(parentExtID, as);
            }
            // TODO check that the value of flag fromInputModel was set correctly (AnRo)
            as.add(new Annotation(getFieldValue(stateSection, "labelString"), false, true));
            return;
        }

        // Handle box
        value = getFieldValue(stateSection, "type");
        if (value.equals("GROUP_STATE")) {
            Box b = new Box();
            handleCommonElements(stateSection, b);
            b.setName(StringUtils.clean(getFieldValue(stateSection,
                    "labelString")));
            return;
        }

        // Handle Truth Table
        if (getFieldValue(stateSection, "truthTable.isTruthTable").equals("1")) {
        	if (getFieldValue(stateSection, "truthTable.useEML").equals("1")) {
                EventHandler.handle(EventLevel.ERROR, "transformStateSection",
                        "??????", 
                        "EML stype truthtables are not currently supported. " +
                        "Detected in block " + cb.getReferenceString(), "");
            	}
/*        	TruthTableFunction f = new TruthTableFunction();
            handleCommonElements(stateSection, f);
        	String ttPredArray = 
        		getFieldValue(stateSection, "truthTable.predicateArray");
        	String ttActArray = 
        		getFieldValue(stateSection, "truthTable.actionArray");
        	int predTrows, predTcols, actTrows, actTcols;
        	predTrows = Integer.parseInt(ttPredArray.substring(
        			ttPredArray.indexOf("C")+1, ttPredArray.indexOf("x")));
        	predTcols = Integer.parseInt(ttPredArray.substring(
        			ttPredArray.indexOf("x")+1, ttPredArray.indexOf("{")));
        	actTrows = Integer.parseInt(ttActArray.substring(
        			ttActArray.indexOf("C")+1, ttActArray.indexOf("x")));
        	actTcols = Integer.parseInt(ttActArray.substring(
        			ttActArray.indexOf("x")+1, ttActArray.indexOf("{")));
        	String predElems[] = ttPredArray.substring(
        			ttPredArray.indexOf("{")+1, ttPredArray.indexOf("}")+1).split(",");
        	String actElems[] = ttActArray.substring(
        			ttActArray.indexOf("{")+1, ttActArray.indexOf("}")+1).split(",");
        	// Clean the strings in arrays
        	for (int i=0; i<predElems.length; i++) {
        		predElems[i] = predElems[i].substring(1).replace("\\", "");
        	}
        	for (int i=0; i<actElems.length; i++) {
        		actElems[i] = actElems[i].substring(1).replace("\\", "");
        	}
        	// Process the array elements
        	
        	return;
*/        }
        
        // Handle normal state
        // First let's create the transition and handle the common elements
        State s = new State();
        handleCommonElements(stateSection, s);

        // Create a dummy composition to start with
        s.setComposition(new OrComposition());

        // Set the type of the state
        value = getFieldValue(stateSection, "type");
        if ("OR_STATE".equals(value)) {
            s.setType(StateType.OR_STATE);
        } else if ("AND_STATE".equals(value)) {
            s.setType(StateType.AND_STATE);
            andStates.add(s);
        } else if ("GROUP_STATE".equals(value)) {
            s.setType(StateType.OR_STATE);
        } else if ("FUNC_STATE".equals(value)) {
            s.setType(StateType.FUNCTION_STATE);
            // Store the "labelString" property as "name" of the State object.
            // Note: at this point the "labelString" is in a form "z = fun(x,
            // y).
            // and hence also the "name" will contain excess information that
            // will be handled later.
            s.setName(StringUtils.clean(getFieldValue(stateSection,
                    "labelString")));
        } else {
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.ERROR, "transformStateSection",
                    "dev-xx", "Unexpected state type \"" + value
                            + "\" detected in block " + cb.getReferenceString()
                            + " section " + s.getExternalID(), "");
        }

        // Set the execution order
        if (s.getType().equals(StateType.AND_STATE)) {
            value = getFieldValue(stateSection, "executionOrder");
            s.setExecutionOrder(Integer.parseInt(value));
        }

        // Propagate bitwise from parent to children
        bitwiseMap.put(s, bitwiseMap.get(cb));

        // TODO Handle state eml
        value = getFieldValue(stateSection, "eml");
        
        // 2006b extensions: StateMachineType
        value = getFieldValue(stateSection, "stateMachineType");
        if (value.equals("MEALYE_MACHINE")) {
        	s.setMachineType(StateMachineType.MEALEY);
        } else if (value.equals("MOORE_MACHINE")) {
        	s.setMachineType(StateMachineType.MOORE);
        } else {
        	s.setMachineType(StateMachineType.CLASSIC);  	
        }

        // Rise a WARNING if the model contains elements we do not know
        // List of known elements
        List<String> knownElements = Arrays.asList("id", "labelString", "type",
                "treeNode", "executionOrder", "eml", "",
                // Ignored known fields
                "position", "fontSize", "chart", "firstTransition", 
                "firstJunction", "firstData", "subviewer", "decomposition", 
                "visible", "isGrouped", "superState", "arrowSize", "", "", "", "", "", "");
        leftoverCheck(stateSection, knownElements, "state", cb);
    }

    /**
     * Reads the elements of the transition section and fills the corresponding
     * attributes of ChartBlock.
     */
    private void transformTransitionSection(Tree transitionSection,
            ChartBlock cb) {
        
        // Skip sub-transitions        
        if ("SUB".equals(getFieldValue(transitionSection, "type"))) {
            return;
        }

        // First let's create the transition and handle it's common elements
        Transition t = new Transition();
        transitions.add(t);
        handleCommonElements(transitionSection, t); // Also adds the element to 
                                                    // the modelElements map

        String dstId;
        // Set the destination - destination is detected from dst.id field
        // Assume that the locations (junctions and states) are converted before
        dstId = AntLRUtils.getFieldValue(transitionSection, "dst.id");
        if (modelElements.containsKey(dstId)) {
            Location destination = (Location) modelElements.get(dstId);
            t.setDestination(destination);
        } else {
            // Destination not found. Postpone the error message 
            // after the children have been connected to the parents
            // so that we get a better formed message. 
        }
        // Set the source and type of the transition.
        // In GA state model the source of a transition is its parent
        // unless src.id field is not specified - in this case we are dealing
        // with a default transition (no source event or junction) and its
        // parent is set by handleCommonElements method.
        // Assume that the locations (junctions and states) are converted before
        String srcId;
        srcId = AntLRUtils.getFieldValue(transitionSection, "src.id");
        if (srcId.equals("")) {
            t.setType(TransitionType.DEFAULT_TRANSITION);
            GAModelElement parent = t.getParent();
            // Only OrCompositions can contain default transitions
            if (parent instanceof ChartBlock) {
                ((OrComposition) ((ChartBlock) parent).getComposition())
                        .addDefaultTransition(t);
            } else if (parent instanceof State) {
                ((OrComposition) ((State) parent).getComposition())
                        .addDefaultTransition(t);
            }
        } else if (modelElements.containsKey(srcId)) {
            GAModelElement parent = modelElements.get(srcId);
            if (parent instanceof Junction) {
                t.setType(TransitionType.FLOWGRAPH_TRANSITION);
                ((Junction) parent).addTransition(t);
            } else if (parent instanceof State) {
                // Detect whether we have an inner or outer transition
                // based on the values of src.intersection element
                String srcIntersect = AntLRUtils.getFieldValue(
                        transitionSection, "src.intersection");
                setTransitionTypeByIntersect(t, srcIntersect, cb);
                if (t.getType().equals(TransitionType.INNER_TRANSITION)) {
                    ((State) parent).addInnerTransition(t);
                } else if (t.getType().equals(TransitionType.OUTER_TRANSITION)) {
                    ((State) parent).addOuterTransition(t);
                } else {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR,
                            "transformTransitionSection", "dev-xx",
                            "Illegal transition type \"" + t.getType()
                                    + "\" in block " + cb.getReferenceString()
                                    + " section " + t.getExternalID(), "");
                }
            } else {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformTransitionSection", "dev-xx",
                        "Illegal transition source type in block "
                                + cb.getReferenceString() + " section "
                                + t.getExternalID(), "");
            }
        } else {
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.ERROR,
                    "transformTransitionSection", "dev-xx",
                    "Unable to locate transition source \"" + srcId
                            + "\" in block " + cb.getReferenceString()
                            + " section " + t.getExternalID(), "");
        }
        
        // Set the execution order
        srcId = AntLRUtils.getFieldValue(transitionSection, "executionOrder");
        t.setExecutionOrder(Integer.parseInt(srcId));

        // Propagate bitwise from parent to children
        bitwiseMap.put(t, bitwiseMap.get(cb));

        // The actions taken during the transition are described in the
        // "labelString". These are parsed separately after the initial
        // transformation of ASt is done.

        // Rise a WARNING if the model contains elements we do not know
        // List of known elements
        List<String> knownElements = Arrays.asList("id", "labelString", "src",
                "dst", "linkNode", "executionOrder", "", "chart",
                "labelPosition", "fontSize", "midPoint", "dataLimits",
                "subviewer", "drawStyle", "slide", "stampAngle", "autogen",
                "visible", "arrowSize", "", "");
        // Check whether all elements of the section are known
        leftoverCheck(transitionSection, knownElements, "transition", cb);
    }

    /**
     * Reads the elements of the data section and fills the corresponding
     * attributes of ChartBlock.
     */
    private Variable_SM transformDataSection(Tree dataSection, GAModelElement root) {
        // First let's create the variable corresponding to the data section
        Variable_SM v = new Variable_SM();
        handleCommonElements(dataSection, v);

        // Handle dataType
        String slType = AntLRUtils.getFieldValue(dataSection, "dataType");
        TPrimitive dt = DataTypeAccessor.parsePrimitiveType(slType);

        // Get the array dimension from props.array
        String arraySize = AntLRUtils.getFieldValue(dataSection,
                "props.array.size");
        if (arraySize.equals("")) {
            // Scalar - array size property not given
            v.setDataType(dt);
        } else {
            if (arraySize.equals("-1")) {
                // TODO Add support for inheriting data size. TF138
                EventHandler.handle(EventLevel.ERROR, "transformDataSection", "",
                        "Inheriting array length is currently not supported. Element: "
                                + v.getReferenceString(), "");
                // The data type remains null
            } else if (arraySize.equals("1")) {
                // Scalar - array size property contains "1".
                v.setDataType(dt);
            } else {
                Expression exp = null;
                
                exp = EMLAccessor.convertEMLToCodeModel(arraySize, true);
                
                if (exp instanceof GeneralListExpression) {
                    // Possibly multi-dimensional array - array size property is
                    // like "[a, b, c]".
                    List<Expression> dimExps = ((GeneralListExpression) exp)
                            .getExpressions();
                    if (dimExps == null || dimExps.isEmpty()) {
                        EventHandler.handle(EventLevel.ERROR,
                                "transformDataSection", "",
                                "Bad value in data size field. Dimensions are null. Element: "
                                        + v.getReferenceString(), "");
                    } else {
                        v.setDataType(new TArray(dimExps, dt));
                    }
                } else {
                    // Single-dimensional array - array size property is like
                    // "3" or "N".
                    v.setDataType(new TArray(exp, dt));
                }
            }
        }

        // Handle data scope
        String scope = AntLRUtils.getFieldValue(dataSection, "scope");
        if ("LOCAL_DATA".equals(scope)) {
            v.setScope(VariableScope.LOCAL_VARIABLE);
        } else if ("INPUT_DATA".equals(scope)) {
            // The root can be only Chart Block
            ChartBlock cb = (ChartBlock) root;
            v.setScope(VariableScope.INPUT_VARIABLE);
            // Bind the variable to the corresponding in data port of the
            // ChartBlock by name.
            // NB! Variable and port data types to be synchronised by ChartTyper
            for (InDataPort p : cb.getInDataPorts()) {
                if (v.getName().equals(p.getName())) {
                    p.setVariable(v);
                }
            }
        } else if ("OUTPUT_DATA".equals(scope)) {
            // The root can be only Chart Block
            ChartBlock cb = (ChartBlock) root;
            v.setScope(VariableScope.OUTPUT_VARIABLE);
            // Bind the variable to the corresponding out data port of the
            // ChartBlock by name.
            // NB! Variable and port data types to be synchronised by ChartTyper
            for (OutDataPort p : cb.getOutDataPorts()) {
                if (v.getName().equals(p.getName())) {
                    p.setVariable(v);
                }
            }
        } else if ("FUNCTION_INPUT_DATA".equals(scope)) {
            v.setScope(VariableScope.FUNCTION_INPUT_VARIABLE);
        } else if ("FUNCTION_OUTPUT_DATA".equals(scope)) {
            v.setScope(VariableScope.FUNCTION_OUTPUT_VARIABLE);
        } else if ("TEMPORARY_DATA".equals(scope)) {
            v.setScope(VariableScope.LOCAL_VARIABLE);
        } else if ("CONSTANT_DATA".equals(scope)) {
            v.setScope(VariableScope.LOCAL_VARIABLE);
            v.setConst(true);
        } else if ("PARAMETER_DATA".equals(scope)) {
            v.setScope(VariableScope.IMPORTED_VARIABLE);
            // TODO (to AnTo) Do not create an object in this case. 
            // It is created on the FM side
            
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.WARNING, "transformDataSection",
                    "dev-xx", "Unsupported data scope \"" + scope + "\" in "
                            + root.getReferenceString() + " section "
                            + v.getExternalID(), "");
        } else if ("EXPORTED_DATA".equals(scope)) {
            v.setScope(VariableScope.EXPORTED_VARIABLE);
        } else if ("IMPORTED_DATA".equals(scope)) {
            v.setScope(VariableScope.IMPORTED_VARIABLE);
        } else if ("DATA_STORE_MEMORY_DATA".equals(scope)) {
            v.setScope(VariableScope.EXPORTED_VARIABLE);
            // TODO (to AnTo) Do not create an object in this case. 
            // It is created on the FM side
            
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.WARNING, "transformDataSection",
                    "dev-xx", "Unsupported data scope \"" + scope + "\" in "
                            + root.getReferenceString() + " section "
                            + v.getExternalID(), "");
        } else {
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.ERROR, "transformDataSection",
                    "dev-xx", "Unknown data scope \"" + scope + "\" in "
                            + root.getReferenceString() + " section "
                            + v.getExternalID(), "");
        }

        // Process initial value
        String initValStr = AntLRUtils.getFieldValue(dataSection,
                "props.initialValue");
        if (initValStr.equals("")) {
            // Do nothing
        } else {
        	v.setInitialValue(PropertyParser.parseProperty(initValStr));
        }

        // Rise a WARNING if the model contains elements we do not know
        // List of known elements
        List<String> knownElements = Arrays.asList("id", "name", "linkNode",
                "scope", "props", "dataType", "", "machine", "");
        // Check whether all elements of the section are known
        leftoverCheck(dataSection, knownElements, "data", root);

        return v;
    }

    /**
     * Reads the elements of the event section and fills the corresponding
     * attributes of ChartBlock.
     */
    private Event transformEventSection(Tree eventSection, GAModelElement root) {
        // First let's create the event and handle the common elements
        Event e = new Event();
        e.setEventCode(eventCounter++);
        handleCommonElements(eventSection, e);

        String value;
        // Set the event scope
        value = AntLRUtils.getFieldValue(eventSection, "scope");
        if (value.equals("LOCAL_EVENT")) {
            e.setScope(EventScope.LOCAL_EVENT);
        } else if (value.equals("INPUT_EVENT")) {
            // The root can be only Chart Block
            ChartBlock cb = (ChartBlock) root;
            e.setScope(EventScope.INPUT_EVENT);
            // Handle events by their trigger type
            String sfTrig = AntLRUtils.getFieldValue(eventSection, "trigger");
            // Refine the trigger type
            if (sfTrig.equals("RISING_EDGE_EVENT")) {
            	e.setTriggerType("rising");
            	// Assume that the events appear in the index order
            	cb.getInEdgeEnablePort().addEvent(e);
            } else if (sfTrig.equals("FALLING_EDGE_EVENT")) {
            	e.setTriggerType("falling");
            	// Assume that the events appear in the index order
            	cb.getInEdgeEnablePort().addEvent(e);
            } else if (sfTrig.equals("EITHER_EDGE_EVENT")) {
            	e.setTriggerType("either");
            	// Assume that the events appear in the index order
            	cb.getInEdgeEnablePort().addEvent(e);
            } else if (sfTrig.equals("FUNCTION_CALL_EVENT")) {
            	e.setTriggerType("function-call");
            	// Apply the event to the right inControlPort
            	// Assume that the events appear in the index order - so the 
            	// event belongs to the first inControlPort that does not have 
            	// an event yet
            	for (InControlPort icp : cb.getInControlPorts()) {
            		if (icp.getEvent() == null) {
            			icp.setEvent(e);
            			break;
            		}
            	}
            } else {
            	EventHandler.handle(
            			EventLevel.ERROR,
            			"transformEventSection",
            			"dev-xx",
            			"Unsupported input event trigger type \""+sfTrig+
            			"\" detected in block " + cb.getReferenceString(),
            	"");
            }
        } else if (value.equals("OUTPUT_EVENT")) {
            // The root can be only Chart Block
            ChartBlock cb = (ChartBlock) root;
            e.setScope(EventScope.OUTPUT_EVENT);

            // Check that the corresponding port is of proper type
            // (should be OutControlPort) and if not change to it
            // (case when the control signal ends in Terminator block)
            for (OutDataPort p : cb.getOutDataPorts()) {
                if (e.getName().equals(p.getName())) {
                    // Convert the OutDataPort to OutControlPort
                    OutControlPort ocp = new OutControlPort();
                    ModelUtilities.transferContent(p, ocp);
                    cb.getOutDataPorts().remove(p);
                    cb.addOutControlPort(ocp);
                    for (Signal sig : ((SystemBlock) cb.getParent())
                            .getSignals()) {
                        if (sig.getSrcPort().getId() == p.getId()) {
                            sig.setSrcPort(ocp);
                            // Change also the type of destination port
                            Block dstBlock = ((Block) sig.getDstPort()
                                    .getParent());
                            if ("Terminator".equals(dstBlock.getType())) {
                                InControlPort icp = new InControlPort();
                                ModelUtilities.transferContent(
                                        sig.getDstPort(), icp);
                                dstBlock.getInDataPorts().remove(
                                        sig.getDstPort());
                                dstBlock.addInControlPort(icp);
                                sig.setDstPort(icp);
                            } else {
                                EventHandler.handle(
                                		EventLevel.WARNING,
                                		"transformEventSection",
                                		"dev-xx",
                                		"OutControlPort of "
                                			+ cb.getReferenceString()
                                			+ " connected to InDataPort of "
                                			+ sig.getDstPort().getParent()
                                					.getReferenceString(),
                                		"");
                            }
                        }
                    }

                    // Terminate the loop when the target is found
                    break;
                }
            }

            // Set link to the OutControlPort
            for (OutControlPort p : cb.getOutControlPorts()) {
                if (e.getName().equals(p.getName())) {
                    e.setOutControlPort(p);
                    break;
                }
            }

            // Output events must only be of function-call type
            value = getFieldValue(eventSection, "trigger");
            if (!value.equals("FUNCTION_CALL_EVENT")) {
                // TODO (to AnTo) Refine error message and reference to the
                // modelling rule
                EventHandler.handle(EventLevel.ERROR, "", "",
                		"Modelling rule violation. The trigger type of "
                			+ "an output event must only be \"function-call\""
                			+ "\nElement: "
                			+ e.getReferenceString(), "");
            }
        } else {
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.ERROR, "transformEventSection",
                    "dev-xx", "Unsupported event scope \"" + value
                            + "\" detected in block "
                            + root.getReferenceString() + " section "
                            + e.getExternalID(), "");
        }

        // Handle the trigger type
        // The actual triggering is done in the functional model. Here we just
        // need to set the correct port type of the ChartBlock in case of output
        // function-call.
        value = getFieldValue(eventSection, "trigger");
        // TODO Add support for vector trigger port
        if (value.equals("RISING_EDGE_EVENT")) {
            // Do nothing
        } else if (value.equals("FALLING_EDGE_EVENT")) {
            // Do nothing
        } else if (value.equals("EITHER_EDGE_EVENT")) {
            // Do nothing
        } else if (value.equals("FUNCTION_CALL_EVENT")) {
            // Do nothing
            // The correct port type have to be set by the FMPreProcessor.
        } else {
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.ERROR, "transformEventSection",
                    "dev-xx", "Unsupported trigger type \"" + value
                            + "\" detected in block "
                            + root.getReferenceString() + " section "
                            + e.getExternalID(), "");
        }

        // Rise a WARNING if the model contains elements we do not know
        // List of known elements
        List<String> knownElements = Arrays.asList("id", "name", "linkNode",
                "scope", "trigger", "", 
                // Known but unused section elements
                "machine", "", "");
        // Check whether all elements of the section are known
        leftoverCheck(eventSection, knownElements, "event", root);

        return e;
    }

    /**
     * Returns reference to a ChartBlock based on its chart file number
     */
    private ChartBlock locateChartBlock(int chartFileNumber) {
        for (ChartBlock cb : getMachine().getChartBlocks()) {
            // There should be a ChartFileNumber propery present in a ChartBlock
            // after FMPreprocessing
            for (Parameter p : cb.getParameters()) {
                if (p.getName().equals("ChartFileNumber")
                        && p.getValue() instanceof StringExpression
                        && ((StringExpression) p.getValue()).getLitValue().equals(
                                Integer.toString(chartFileNumber))) {
                    return cb;
                }
            }
        }
        // For robustness - code that should be never visited
        EventHandler.handle(EventLevel.CRITICAL_ERROR, "locateChartBlock",
                "dev-xx", "Unable to locate ChartBlock No \"" + chartFileNumber
                        + "\" from the system model", "");
        return null;
    }

    /**
     * Handles the common fields "id", "name", "linkNode" and "treeNode". Adds
     * the GAModelElement into elements Map
     * 
     * @param section
     *            - section to be handled
     * @param e
     *            - GAModelElement where the values of common elements are
     *            stored
     */
    private void handleCommonElements(Tree section, GAModelElement e) {
        // Set the id
        e.setExternalID(AntLRUtils.getFieldValue(section, "id"));
        // Set the name
        e.setName(AntLRUtils.getFieldValue(section, "name"));
        // Set reference to the parent GAModelElement
        if (e.getParent() == null) {
            String triple = AntLRUtils.getFieldValue(section, "linkNode");
            if (triple.equals("")) {
                triple = AntLRUtils.getFieldValue(section, "treeNode");
            }
            String parentExtID = parseParentTriple(triple);
            GAModelElement parent = modelElements.get(parentExtID);
            if (parent != null) {
                e.setParent(parent);
            } else {
                // Remember element whose parent was not found
                foundlings.put(e, parentExtID);
            }
        } else {
            // Ignore this case - parent is already set.
        }
        // Add description
        String desc = AntLRUtils.getFieldValue(section, "description");
        if (!desc.isEmpty()) {
            e.addAnnotation(desc);
        }
        
        // Add the GAModelElement corresponding to the section to the map
        modelElements.put(e.getExternalID(), e);
    }

    /**
     * Return the first element of the triple
     * @param parentTriple
     * @return
     */
    private String parseParentTriple(String parentTriple) {
        // We take the first element of the triple assuming that
        // the 1'st char is an opening "[" and the separator is " ".
        String result = parentTriple.substring(1, parentTriple.indexOf(" "));
        return result;
    }

    /**
     * Set transition type based on graphical information provided in
     * dst.intersection
     * 
     * @param t
     *            - the transition
     * @param intersect
     *            - src.intersection value
     * @param cb
     *            - block to refer when rising error events
     */
    private void setTransitionTypeByIntersect(Transition t, String intersect,
            ChartBlock cb) {
        // It is assumed, that the first elements of the "intersection"
        // attribute have following meaning:
        // - Junction's or State's side : "0" junction, "1".."4"
        // (state's entry side: top, right, bottom, left)
        // - Horisontal entry direction : "-1" from left, "1" from right
        // - Vertical entry direction : "-1" from top , "1" from under
        String[] grInf = intersect.split(" ");
        if (grInf[0].equals("[1")) { // top side
            if (grInf[2].equals("-1")) { // vertical entry from top
                t.setType(TransitionType.OUTER_TRANSITION);
            } else if (grInf[2].equals("1")) { // vertical entry from bottom
                t.setType(TransitionType.INNER_TRANSITION);
            } else {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformTransitionSection", "dev-xx",
                        "Inconsistency in transition intersection in block "
                                + cb.getReferenceString() + " section "
                                + t.getExternalID(), "");
            }
        } else if (grInf[0].equals("[2")) { // right side
            if (grInf[1].equals("1")) { // horizontal entry from right
                t.setType(TransitionType.OUTER_TRANSITION);
            } else if (grInf[1].equals("-1")) { // horizontal entry from left
                t.setType(TransitionType.INNER_TRANSITION);
            } else {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformTransitionSection", "dev-xx",
                        "Inconsistency in transition intersection in block "
                                + cb.getReferenceString() + " section "
                                + t.getExternalID(), "");
            }
        } else if (grInf[0].equals("[3")) { // bottom side
            if (grInf[2].equals("1")) { // vertical entry from bottom
                t.setType(TransitionType.OUTER_TRANSITION);
            } else if (grInf[2].equals("-1")) { // vertical entry from top
                t.setType(TransitionType.INNER_TRANSITION);
            } else {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformTransitionSection", "dev-xx",
                        "Inconsistency in transition intersection in block "
                                + cb.getReferenceString() + " section "
                                + t.getExternalID(), "");
            }
        } else if (grInf[0].equals("[4")) { // left side
            if (grInf[1].equals("-1")) { // horizontal entry from left
                t.setType(TransitionType.OUTER_TRANSITION);
            } else if (grInf[1].equals("1")) { // horizontal entry from right
                t.setType(TransitionType.INNER_TRANSITION);
            } else {
                // For robustness - code that should be never visited
                EventHandler.handle(EventLevel.ERROR,
                        "transformTransitionSection", "dev-xx",
                        "Inconsistency in transition intersection in block "
                                + cb.getReferenceString() + " section "
                                + t.getExternalID(), "");
            }
        } else {
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.ERROR,
                    "transformTransitionSection", "dev-xx",
                    "Illegal transition intersection value in block "
                            + cb.getReferenceString() + " section "
                            + t.getExternalID(), "");
        }
    }

    /**
     * Loop over the remaining elements in the section AST and rise a WARNING if
     * any of them is not known.
     * 
     * @param section
     *            - AST to be scanned
     * @param knownElements
     *            - list of elements to being known
     * @param sectionString
     *            - string to be used in the warning message
     * @param owner
     *            - reference to the chart block related to the section
     */
    private void leftoverCheck(Tree section, List<String> knownElements,
            String sectionType, GAModelElement owner) {
        // Loop over the elements of the given section
        for (int i = 0; i < section.getChildCount(); i++) {
            String key = section.getChild(i).getText();
            if (knownElements.contains(key)) {
                continue;
            } else {
                // For robustness - code that should not being visited
                // We assume that all of the elements are known.
                /*
                EventsHandler.handle(EventLevel.WARNING, "", "dev-xx",
                        "Unknown field \"" + key + "\" in section \""
                                + sectionType + "\" of "
                                + owner.getReferenceString()
                                + " - left unhandled.", "");
                */
            }
        }
    }

    /**
     * Connect children to their parents (add children to their parents) We can
     * do this when all of the stateflow sections are transformed into GA System
     * Model.
     */
    private void connectParentChildren() {
        GAModelElement parent;
        // Loop over the processed sections and link elements to their parents
        // when they are not already linked
        // TODO create proper interfaces to reduce the amount of if-else
        // statements here
        
        /*
         * Sort modelElements by id before iterating them
         */
        Comparator<GAModelElement> comparator = new Comparator<GAModelElement>() {

			@Override
			public int compare(GAModelElement elt1, GAModelElement elt2) {
				if (elt1.getId() == elt2.getId()) {
					return 0;
				} else if (elt1.getId() > elt2.getId()) {
					return 1;
				} else {
					return -1;
				}
			}
        	
        };
        List<GAModelElement> elts = new ArrayList<GAModelElement>(modelElements.values());
        Collections.sort(elts, comparator);
        for (GAModelElement e : elts) {
            // Add annotations (separate annotation objects in the mdl) if any pending. 
            if (e instanceof ChartBlock) {
                // Different handling for Chart  
                ((ChartBlock) e).getRootLocation().addAnnotations(
                        annotations.get(e.getExternalID()));
            } else {
                e.addAnnotations(annotations.get(e.getExternalID()));
            }

            // Find parent for the element and assign it to parent's proper
            // attribute
            parent = e.getParent();
            if (parent == null) {
                parent = modelElements.get(foundlings.get(e));
            }
            if (e instanceof ChartBlock) {
                // Do nothing - this is the root for a state chart
            } else if (e instanceof Junction) {
                Junction j = (Junction) e;
                // Locate the junction's parent's composition attribute and
                // add the junction to the composition's list of junctions
                if (parent instanceof ChartBlock) {
                    // Only OrComposition can contain junctions in ChartBlock
                    OrComposition c = ((OrComposition) 
                    		((ChartBlock) parent).getComposition());
                    if (!c.getJunctions().contains(j)) {
                        c.addJunction(j);
                    }
                } else if (parent instanceof State) {
                    // Only OrComposition can contain junctions in State
                    OrComposition c = ((OrComposition) 
                    		((State) parent).getComposition());
                    if (!c.getJunctions().contains(j)) {
                        c.addJunction(j);
                    }
                } else if (parent instanceof GraphicalFunction) {
                    // Only FlowGraphComposition can contain junctions in
                    // GraphicalFunctions
                    FlowGraphComposition c = ((FlowGraphComposition)
                    		((GraphicalFunction) parent).getComposition());
                    if (!c.getJunctions().contains(j)) {
                        c.addJunction(j);
                    }
                } else if (parent instanceof Box) {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR, "", "dev-xx",
                            "Junctions in boxes are not supported by modelling " +
                            	"rules" + e.getReferenceString(), "");
                } else {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR,
                            "connectParentChildren", "dev-xx",
                            "Illegal junction's parent type \""
                                    + parent.getClass().getSimpleName()
                                    + "\" of " + e.getReferenceString()
                                    + "extID=" + e.getExternalID(), "");
                }
            } else if (e instanceof State) {
                State s = (State) e;
                if (s.getType() == StateType.FUNCTION_STATE) {
                    // Graphical functions encoded as States
                    // Note: The upward link from child to parent is added, but
                    // as
                    // these objects will be converted into Function objects
                    // later,
                    // and added to the "functions" collection of the parent
                    // object,
                    // there is no downward link at this point.
                    e.setParent(parent);
                    continue;
                }
                if (parent instanceof ChartBlock) {
                    Composition c = ((ChartBlock) parent).getComposition();
                    if (c instanceof AndComposition
                            && !((AndComposition) c).getStates().contains(s)) {
                        ((AndComposition) c).addState(s);
                    } else if (c instanceof OrComposition
                            && !((OrComposition) c).getStates().contains(s)) {
                        ((OrComposition) c).addState(s);
                    } else {
                        // For robustness - code that should be never visited
                        EventHandler.handle(EventLevel.ERROR,
                                "connectParentChildren", "dev-xx",
                                "Illegal state's composition type of "
                                        + e.getReferenceString(), "");
                    }
                } else if (parent instanceof State) {
                    Composition c = ((State) parent).getComposition();
                    if (c instanceof AndComposition
                            && !((AndComposition) c).getStates().contains(s)) {
                        ((AndComposition) c).addState(s);
                    } else if (c instanceof OrComposition
                            && !((OrComposition) c).getStates().contains(s)) {
                        ((OrComposition) c).addState(s);
                    } else {
                        // For robustness - code that should be never visited
                        EventHandler.handle(EventLevel.ERROR,
                                "connectParentChildren", "dev-xx",
                                "Illegal state's composition type of "
                                        + e.getReferenceString(), "");
                    }
                } else if (parent instanceof Box) {
                    if (s.getType() == StateType.FUNCTION_STATE) {
                        // Function objects are allowed in Boxes.
                        // At this point they are still encoded as States
                        // Do nothing here.
                    } else {
                        // For robustness - code that should be never visited
                        EventHandler.handle(EventLevel.ERROR,
                                "", "dev-xx",
                                "States in boxes are not supported  by modelling rules"
                                        + e.getReferenceString(), "");
                    }
                } else {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR,
                            "connectParentChildren", "dev-xx",
                            "Illegal state's parent type \""
                                    + parent.getClass().getSimpleName()
                                    + "\" of " + e.getReferenceString()
                                    + "extID=" + e.getExternalID(), "");
                }
            } else if (e instanceof Transition) {
                // Parent-children connection is established during the
                // 1'st transformation round.                
                
            } else if (e instanceof Variable_SM) {
                Variable_SM v = (Variable_SM) e;
                if (parent instanceof ChartBlock) {
                    if (!((ChartBlock) parent).getVariables().contains(v)) {
                        ((ChartBlock) parent).addVariable(v);
                    }
                } else if (parent instanceof State) {
                    if (!((State) parent).getVariables().contains(v)) {
                        ((State) parent).addVariable(v);
                    }
                } else if (parent instanceof Function_SM) {
                    if (!((Function_SM) parent).getVariables().contains(v)) {
                        ((Function_SM) parent).addVariable(v);
                    }
                } else if (parent instanceof InDataPort) {
                    ((InDataPort) parent).setVariable(v);
                } else if (parent instanceof OutDataPort) {
                    ((OutDataPort) parent).setVariable(v);
                } else if (parent instanceof SystemBlock) {
                    if (!((SystemBlock) parent).getVariables().contains(v)) {
                        ((State) parent).addVariable(v);
                    }
                } else {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR,
                            "connectParentChildren", "dev-xx",
                            "Illegal variable's parent type \""
                                    + parent.getClass().getSimpleName()
                                    + "\" of " + e.getReferenceString()
                                    + "extID=" + e.getExternalID(), "");
                }
            } else if (e instanceof Event) {
                Event evt = (Event) e;
                if (parent instanceof ChartBlock) {
                    if (!((ChartBlock) parent).getEvents().contains(e)) {
                        ((ChartBlock) parent).addEvent(evt);
                    }
                } else if (parent instanceof State) {
                    if (!((State) parent).getEvents().contains(evt)) {
                        ((State) parent).addEvent(evt);
                    }
                } else {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR,
                            "connectParentChildren", "dev-xx",
                            "Illegal event's parent type \""
                                    + parent.getClass().getSimpleName()
                                    + "\" of " + e.getReferenceString()
                                    + "extID=" + e.getExternalID(), "");
                }
                // Check the event's scope. We don't want to do it earlier, as
                // at this point the reference string is more useful
                if (evt.getScope() == EventScope.LOCAL_EVENT) {
                    EventHandler.handle(
                    		EventLevel.ERROR,
                    		"",
                    		"",
                    		"Event: "
                    			+ e.getReferenceString() + " has local scope."
                    			+ " Local events are not allowed according to"
                    			+ " the Gene-Auto Stateflow modelling rules.",
                    "");
                }

            } else if (e instanceof Box) {
                if (parent instanceof ChartBlock) {
                    Composition c = ((ChartBlock) parent).getComposition();
                    if (!c.getBoxes().contains((Box) e)) {
                        c.addBox((Box) e);
                    }
                } else if (parent instanceof State) {
                    Composition c = ((State) parent).getComposition();
                    if (!c.getBoxes().contains((Box) e)) {
                        c.addBox((Box) e);
                    }
                } else if (parent instanceof Box) {
                    Box b = ((Box) parent);
                    if (!b.getBoxes().contains((Box) e)) {
                        b.addBox((Box) e);
                    }
                } else {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR,
                            "connectParentChildren", "dev-xx",
                            "Illegal box's parent type \""
                                    + parent.getClass().getSimpleName()
                                    + "\" of " + e.getReferenceString()
                                    + "extID=" + e.getExternalID(), "");
                }
            }
        }
    }

    private void transformLabelsAndGrFuns() {
        // Stateflow specification AST composed by mdl parser
        Tree sectionTree = getMachine().getParserTree();
        // Loop the sections of Stateflow AST and parse labelString elements of
        // transition and state sections
        // "sections" are the top-level branches of the AST and
        // "elements" are the sub-branches.
        CommonTree labelAST;
        String extId;
        // Loop over the sections (first level elements) of stateflow parser
        // tree
        for (int i = 0; i < sectionTree.getChildCount(); i++) {
            Tree section = sectionTree.getChild(i);
            if ("state".equals(section.getText())) {
                if (AntLRUtils.getFieldValue(section, "isNoteBox").equals("1")) {
                    // Skip note boxes here
                    continue;
                }
                extId = AntLRUtils.getFieldValue(section, "id");
                GAModelElement el = modelElements.get(extId);
                if (el instanceof Box) {
                    // Skip boxes here
                    continue;
                }
                State s = (State) el;
                if (AntLRUtils.getFieldValue(section, "type").equals(
                        "FUNC_STATE")) {
                    // Function states (alias graphical functions)

                    // Transform the State to a Function
                    Function_SM f = transformGrFun(s);

                    // Register the function by the logical parent.
                    // Note: The expected parent of such a State object is
                    // here either another State or the Chart.
                    GAModelElement parent = s.getParent();
                    if (parent instanceof State) {
                        ((State) parent).addFunction(f);
                    } else if (parent instanceof Box) {
                        ((Box) parent).addFunction(f);
                    } else if (parent instanceof ChartBlock) {
                        ((ChartBlock) parent).addFunction(f);
                        // Update the function's scope, if required. Default is
                        // local.
                        if (((ChartBlock) parent).isExportFunctions()) {
                            f.setScope(FunctionScope.EXPORTED_FUNCTION);
                        }
                    } else {
                        // For robustness - code that should be never
                        // visited
                        String msg = "Unsupported parent type of a function state.\nState: "
                                + s.getReferenceString();
                        msg += "\nParent: ";
                        if (parent != null) {
                            msg += parent.getReferenceString();
                        } else {
                            msg += "null";
                        }
                        EventHandler.handle(EventLevel.ERROR,
                                "transformLabels", "", msg, "");
                    }
                    continue;
                }
                Tree element = AntLRUtils
                        .getFieldByName(section, "labelString");
                String labelString = element.getChild(0).getText();
                labelString = StringUtils.baseClean(labelString);
                boolean bitwise = bitwiseMap.get(s);
/*                EventsHandler.handle(EventLevel.DEBUG,
                        "transformLabelsAndGrFuns", "", 
                        "state "+ s.getReferenceString()+"\n\t"+labelString, "");
*/                try {
                    // Convert String to Stream
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(
                            labelString.getBytes());
                    // Parse the stream
                    ANTLRInputStream input = new ANTLRInputStream(inputStream);
                    SFLabelLexer lexer = new SFLabelLexer(input);
                    lexer.cBitOptsOn = bitwise;
                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    SFLabelParser parser = new SFLabelParser(tokens);
                    labelAST = (CommonTree) parser.parseStateLabel().getTree();
                    // Top node should be the name of the state
                    s.setName(labelAST.getText());
                    // Its children is the content
                    String segmentType = "";
                    for (int j = 0; j < labelAST.getChildCount(); j++) {
                        Tree segment = labelAST.getChild(j);
                        segmentType = segment.getText();
                        if ("EntryActions".equals(segmentType)) {
                            for (SimpleAction a : 
                            		transformActions(segment, bitwise, s)) {
                                s.addEntryAction(a);
                            }
                        } else if ("DuringActions".equals(segmentType)) {
                            for (SimpleAction a : transformActions(segment,
                                    bitwise, s)) {
                                s.addDuringAction(a);
                            }
                        } else if ("ExitActions".equals(segmentType)) {
                            for (SimpleAction a : transformActions(segment,
                                    bitwise, s)) {
                                s.addExitAction(a);
                            }
                        } else if ("OnEventActions".equals(segmentType)) {
                            for (OnEventAction a : transformOnEventActions(
                                    segment, bitwise, s)) {
                                s.addDuringAction(a);
                            }
                            // } else if ("BindDecls".equals(segmentType)) {
                            // TODO AnTo: A different approach is needed here -
                            // we need to consider also hierarchy.
                            // It is possible that the same name is used on
                            // different levels and means different
                            // objects. Proper name resolving has been
                            // implemented in SMPrePorcessor,
                            // but then these declarations must be also kept in
                            // the State objects until that point.
                            // Low priority task.
                            // String boundName;
                            // for (int k = 0; k < segment.getChildCount(); k++)
                            // {
                            // boundName = segment.getChild(k).getText();
                            // Event e = locateEventByName(boundName);
                            // if (e != null) {
                            // e.setBinder(s);
                            // } else {
                            // Variable v = locateVariableByName(boundName);
                            // if (v != null) {
                            // v.setBinder(s);
                            // } else {
                            // // For robustness - code that should not
                            // // being visited
                            // EventsHandler.handle(EventLevel.ERROR,
                            // "transformLabels", "",
                            // "Unable to locate binder \""+
                            // boundName+"\" while " +
                            // "parsing label of "+
                            // s.getReferenceString(), "");
                            // }
                            // }
                            // }
                        } else if ("UnLabelledActions".equals(segmentType)) {
                            for (SimpleAction a : transformActions(segment,
                                    bitwise, s)) {
                                s.addUnLabelledAction(a);
                            }
                        } else if ("Annotations".equals(segmentType)) {
                                 addAnnotations(s, segment);
                        } else {
                            // For robustness - code that should be never
                            // visited
                            EventHandler.handle(
                            		EventLevel.ERROR,
                            		"transformLabels",
                            		"",
                            		"Unsupported AST segment type \""
                            			+ segmentType
                            			+ "\" detected while parsing label of "
                            			+ s.getReferenceString());
                        }
                    }
                } catch (Exception e) {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.CRITICAL_ERROR,
                            "transformLabels", "", "Failed to parse label \""
                                    + labelString + "\" of "
                                    + section.getText() + "-" + extId, e);
                }

            } else if ("transition".equals(section.getText())) {
                extId = AntLRUtils.getFieldValue(section, "id");
                if (!modelElements.containsKey(extId)) {
                    // Skips redundant sub-transitions.
                    continue;
                }
                Transition t = (Transition) modelElements.get(extId);
                String labelString = AntLRUtils.getFieldValue(section,
                        "labelString");
                boolean bitwise = bitwiseMap.get(t);
                try {
                    // Convert String to Stream
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(
                            labelString.getBytes());
                    // Parse the stream
                    ANTLRInputStream input = new ANTLRInputStream(inputStream);
                    SFLabelLexer lexer = new SFLabelLexer(input);
                    lexer.cBitOptsOn = bitwise;
                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    SFLabelParser parser = new SFLabelParser(tokens);
                    labelAST = (CommonTree) parser.parseTransitionLabel()
                            .getTree();
                    // Transform the label segments
                    // Resulting AST structure Transition 
                    //	{ Annotations {comment*}, 
                    //	  Events {...}?, Guard {...}?, ...
                    //	}
                    String segmentType = "";
                    for (int j = 0; j < labelAST.getChildCount(); j++) {
                        Tree segment = labelAST.getChild(j);
                        segmentType = segment.getText();
                        if ("Events".equals(segmentType)) {
                            // for (Event e :
                            // locateEvents(labelAST.getChild(j))) {
                            // t.addEvent(e);
                            // }
                            for (int k = 0; k < segment.getChildCount(); k++) {
                                String eventName = segment.getChild(k).getText();
                                // Create a new temporary event and store it in the tempEvents pool
                                Event e = new Event();
                                e.setName(eventName);
                                getMachine().getTempEvents().add(e);
                                t.addEvent(e);
                            }
                        } else if ("Guard".equals(segmentType)) {
                            t.setGuard(transformExpression(
                            		segment.getChild(0), bitwise, t));
                        } else if ("ConditionStatements".equals(segmentType)) {
                            for (SimpleAction a : transformActions(
                            		segment, bitwise, t)) {
                                t.addConditionAction(a);
                            }
                        } else if ("TransitionStatements".equals(segmentType)) {
                            for (SimpleAction a : transformActions(
                            		segment, bitwise, t)) {
                                t.addTransitionAction(a);
                            }
                        } else if ("Annotations".equals(segmentType)) {
                            addAnnotations(t, segment);
                        } else {
                            // For robustness - code that should be never
                            // visited
                            EventHandler.handle(
                            		EventLevel.ERROR,
                            		"transformLabels",
                            		"",
                            		"Unknown AST segment type \""
                            			+ segmentType
                            			+ "\" detected while parsing label of "
                            			+ t.getReferenceString(),
                            "");
                        }
                    }
                } catch (Exception e) {
                    // For robustness - code that should be never visited
                    EventHandler.handle(EventLevel.ERROR, "", "",
                            "Failed to parse transition label: \"" + labelString
                                    + "\"\n Transition: "
                                    + t.getReferenceString(), e);
                }
            }
        }
    }

    private Statement transformStatement (
    		Tree stmtAST, boolean bitwise, GAModelElement element) {
        Statement stmt = null;
        String text = stmtAST.getText();
        if ("AssignStatement".equals(text)) {
            // AssignStatement
            AssignOperator op = AssignOperator.fromEML(stmtAST.getChild(0)
                    .getText(), bitwise, element);
            Expression lExpr = transformExpression(stmtAST.getChild(1),
                    bitwise, element);
            Expression rExpr = transformExpression(stmtAST.getChild(2),
                    bitwise, element);
            stmt = new AssignStatement(op, lExpr, rExpr);
        } else if ("IncStatement".equals(text)) {
            Expression expr = transformExpression(stmtAST.getChild(0), bitwise,
                    element);
            stmt = new IncStatement(expr);
        } else if ("DecStatement".equals(text)) {
            Expression expr = transformExpression(stmtAST.getChild(0), bitwise,
                    element);
            stmt = new DecStatement(expr);
        } else if ("ExpressionStatement".equals(text)) {
            Expression expr = transformExpression(stmtAST.getChild(0), bitwise,
                    element);
            /*
             * An ExpressionStatement consisting of just one variable reference
             * is considered a broadcast. It could be either a local broadcast
             * within the chart or a broadcast to the output port - an
             * ExternalBroadcast. Currently only the last kinds of broadcasts
             * are supported.
             */
            if (expr instanceof VariableExpression) {
                // Create a new temporary event and store it in the tempEvents pool
                Event e = new Event();
                e.setName(((VariableExpression) expr).getVariableName());
                getMachine().getTempEvents().add(e);
                stmt = new ExternalBroadcast(e);
            } else {
                stmt = new ExpressionStatement(expr);
            }
        } else if ("Annotations".equals(text)) {
        	// add these annotations to master element
            addAnnotations(element, stmtAST);
        } else {
            // For robustness - code that should be never visited
            EventHandler.handle(EventLevel.ERROR, "transformStatement",
                    "dev-xx", "Illegal statement type \"" + text + "\". "
                            + "labelParser and this method are out of sync!?",
                    "");
        }
        return stmt;
    }

    private Expression transformExpression(
    		Tree exprAST, boolean bitwise, GAModelElement element) {
    	return EMLConverter.transformExpression(exprAST, bitwise, element);
    }

    /**
     * @param a
     *            State that encodes a graphical function (type=FUNC_STATE in
     *            the mdl)
     * @return a graphical function object
     */
    private Function_SM transformGrFun(State s) {
        GraphicalFunction f = new GraphicalFunction();
        // Transfer properties from State s to the function object
        s.moveCommonAttributes(f);
        f.setDataType(new TVoid()); // Initialise output type to void
        f.setScope(FunctionScope.LOCAL_FUNCTION); // Initialise the scope to
        // local

        f.setComposition(s.getComposition().toFlowGraphComposition());
        for (Variable_SM v : s.getVariables()) {
            if (v.getScope() == VariableScope.FUNCTION_OUTPUT_VARIABLE) {
                f.setDataType(v.getDataType()); // Link as a pointer, not a
                // copy.
                f.addVariable(v);
            } else if (v.getScope() == VariableScope.FUNCTION_INPUT_VARIABLE) {
                f.addArgument(new FunctionArgument_SM(v));
            } else {
                f.addVariable(v);
            }
        }
        /*
         * Process the function signature (stored at this point in the name
         * field) to extract the function's name and arguments.
         */
        String signature = f.getName();
        FunctionSignatureParser sp = new FunctionSignatureParser(signature);
        f.setName(sp.functionName);

        // Check and sort arguments
        // Check argument count
        int sArgCount = 0;
        int fArgCount = 0;
        if (sp.arguments != null) {
            sArgCount = sp.arguments.length;
        }
        if (f.getArguments() != null) {
            fArgCount = f.getArguments().size();
        }
        if (fArgCount != sArgCount) {
            EventHandler.handle(EventLevel.CRITICAL_ERROR, "transformGrFun",
                    "",
                    "Argument count doesn't match the function's signature. Function: "
                            + f.getReferenceString() + ".\nArgument count: "
                            + fArgCount + "\nSignature: " + signature, "");
            return null;
        }

        // Sort arguments according to the signature.
        List<FunctionArgument_SM> tmpLst = new LinkedList<FunctionArgument_SM>();
        if (sp.arguments != null) {
            for (String argStr : sp.arguments) {
                boolean found = false;
                for (FunctionArgument_SM fa : f.getArguments()) {
                    if (fa.getName().equals(argStr)) {
                        tmpLst.add(fa);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    EventHandler
                            .handle(
                                    EventLevel.CRITICAL_ERROR,
                                    "transformGrFun",
                                    "",
                                    "Argument defined in the signature not present in the arguments collection. Function: "
                                            + f.getReferenceString()
                                            + "\nSignature: "
                                            + signature
                                            + "\nArgument: " + argStr, "");
                    return null;
                }
            }
        }
        f.setArguments(tmpLst);

        return f;
    }

    private List<SimpleAction> transformActions(Tree stmtsAST, boolean bitwise,
            GAModelElement element) {
        List<SimpleAction> actions = new ArrayList<SimpleAction>();
        for (int i = 0; i < stmtsAST.getChildCount(); i++) {
            SimpleAction action = new SimpleAction();
            Statement stmt = transformStatement(
            		stmtsAST.getChild(i), bitwise, element);
            if (stmt!=null) {	// the child was an annotation
            	action.setOperation(stmt);
            	actions.add(action);
            }
        }
        return actions;
    }

    private List<OnEventAction> transformOnEventActions(Tree stmtsAST,
            boolean bitwise, GAModelElement element) {
        // First element of OnEventActions in AST is the event and
        // second, third etc. is the list of actions
        String eventName = stmtsAST.getChild(0).getText();
        List<OnEventAction> actions = new ArrayList<OnEventAction>();
        for (int i = 1; i < stmtsAST.getChildCount(); i++) {
            OnEventAction action = new OnEventAction();
            // Create a new temporary event and store it in the tempEvents pool
            Event e = new Event();
            e.setName(eventName);
            getMachine().getTempEvents().add(e);
            action.setEvent(e);
            // Convert the action's body
            Statement stmt = transformStatement(
            		stmtsAST.getChild(i), bitwise, element);
            if (stmt!=null) {	// the child was an annotation
            	action.setOperation(stmt);
            	actions.add(action);
            }
        }
        return actions;
    }

    private void addAnnotations(GAModelElement elem, Tree annotationsAST) {
    	EMLConverter.addAnnotations(elem, annotationsAST);
    }
    
    private void sortCharts() {
        for (ChartBlock c : charts) {
            // Sort the chart itself
            if (c.getComposition() != null) {
                c.getComposition().sort();
            }
            Collections.sort(c.getEvents());
            if (c.getInEdgeEnablePort() != null) {
                c.getInEdgeEnablePort().sort();
            }
            // Sort any graphical functions in it
            for (GAModelElement e : c.getAllChildren(GraphicalFunction.class)) {
                GraphicalFunction gf = (GraphicalFunction) e; 
                if (gf.getComposition() != null) {
                    gf.getComposition().sort();
                }
            }
        }        
    }
    
}