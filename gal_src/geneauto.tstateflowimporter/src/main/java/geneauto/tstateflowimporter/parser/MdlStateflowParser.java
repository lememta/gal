// $ANTLR 3.0.1 T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g 2009-12-01 07:13:25

package geneauto.tstateflowimporter.parser;


import org.antlr.runtime.BitSet;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.ParserRuleReturnScope;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.RewriteRuleSubtreeStream;
import org.antlr.runtime.tree.TreeAdaptor;

@SuppressWarnings("unused")
public class MdlStateflowParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "MODEL", "LIB", "OPEN", "CLOSE", "MATDATA", "STATEF", "KEY", "INTER", "STRING", "COMMENT", "WS", "NEWLINE"
    };
    public static final int OPEN=6;
    public static final int WS=14;
    public static final int NEWLINE=15;
    public static final int CLOSE=7;
    public static final int KEY=10;
    public static final int MATDATA=8;
    public static final int STATEF=9;
    public static final int INTER=11;
    public static final int MODEL=4;
    public static final int COMMENT=13;
    public static final int EOF=-1;
    public static final int LIB=5;
    public static final int STRING=12;

        public MdlStateflowParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g"; }


    public static class parse_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parse
    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:68:1: parse : mdl ;
    public final parse_return parse() throws RecognitionException {
        parse_return retval = new parse_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        mdl_return mdl1 = null;



        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:68:13: ( mdl )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:68:15: mdl
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_mdl_in_parse116);
            mdl1=mdl();
            _fsp--;

            adaptor.addChild(root_0, mdl1.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end parse

    public static class mdl_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start mdl
    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:1: mdl : sldesc ( mddesc )? ( sfdesc )? -> ( sfdesc )? ;
    public final mdl_return mdl() throws RecognitionException {
        mdl_return retval = new mdl_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        sldesc_return sldesc2 = null;

        mddesc_return mddesc3 = null;

        sfdesc_return sfdesc4 = null;


        RewriteRuleSubtreeStream stream_mddesc=new RewriteRuleSubtreeStream(adaptor,"rule mddesc");
        RewriteRuleSubtreeStream stream_sfdesc=new RewriteRuleSubtreeStream(adaptor,"rule sfdesc");
        RewriteRuleSubtreeStream stream_sldesc=new RewriteRuleSubtreeStream(adaptor,"rule sldesc");
        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:13: ( sldesc ( mddesc )? ( sfdesc )? -> ( sfdesc )? )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:18: sldesc ( mddesc )? ( sfdesc )?
            {
            pushFollow(FOLLOW_sldesc_in_mdl137);
            sldesc2=sldesc();
            _fsp--;

            stream_sldesc.add(sldesc2.getTree());
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:25: ( mddesc )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==MATDATA) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:25: mddesc
                    {
                    pushFollow(FOLLOW_mddesc_in_mdl139);
                    mddesc3=mddesc();
                    _fsp--;

                    stream_mddesc.add(mddesc3.getTree());

                    }
                    break;

            }

            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:33: ( sfdesc )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==STATEF) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:33: sfdesc
                    {
                    pushFollow(FOLLOW_sfdesc_in_mdl142);
                    sfdesc4=sfdesc();
                    _fsp--;

                    stream_sfdesc.add(sfdesc4.getTree());

                    }
                    break;

            }


            // AST REWRITE
            // elements: sfdesc
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 72:41: -> ( sfdesc )?
            {
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:72:44: ( sfdesc )?
                if ( stream_sfdesc.hasNext() ) {
                    adaptor.addChild(root_0, stream_sfdesc.next());

                }
                stream_sfdesc.reset();

            }



            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end mdl

    public static class sldesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start sldesc
    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:75:1: sldesc : ( MODEL | LIB ) OPEN ( struct | field )+ CLOSE ;
    public final sldesc_return sldesc() throws RecognitionException {
        sldesc_return retval = new sldesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set5=null;
        Token OPEN6=null;
        Token CLOSE9=null;
        struct_return struct7 = null;

        field_return field8 = null;


        CommonTree set5_tree=null;
        CommonTree OPEN6_tree=null;
        CommonTree CLOSE9_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:75:12: ( ( MODEL | LIB ) OPEN ( struct | field )+ CLOSE )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:75:14: ( MODEL | LIB ) OPEN ( struct | field )+ CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            set5=(Token)input.LT(1);
            if ( (input.LA(1)>=MODEL && input.LA(1)<=LIB) ) {
                input.consume();
                root_0 = (CommonTree)adaptor.becomeRoot(adaptor.create(set5), root_0);
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_sldesc161);    throw mse;
            }

            OPEN6=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_sldesc168); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:75:33: ( struct | field )+
            int cnt3=0;
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==KEY) ) {
                    int LA3_2 = input.LA(2);

                    if ( (LA3_2==OPEN) ) {
                        alt3=1;
                    }
                    else if ( ((LA3_2>=KEY && LA3_2<=STRING)) ) {
                        alt3=2;
                    }


                }


                switch (alt3) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:75:34: struct
            	    {
            	    pushFollow(FOLLOW_struct_in_sldesc172);
            	    struct7=struct();
            	    _fsp--;

            	    adaptor.addChild(root_0, struct7.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:75:41: field
            	    {
            	    pushFollow(FOLLOW_field_in_sldesc174);
            	    field8=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field8.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            CLOSE9=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_sldesc178); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end sldesc

    public static class mddesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start mddesc
    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:78:1: mddesc : MATDATA OPEN ( struct | field )+ CLOSE ;
    public final mddesc_return mddesc() throws RecognitionException {
        mddesc_return retval = new mddesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token MATDATA10=null;
        Token OPEN11=null;
        Token CLOSE14=null;
        struct_return struct12 = null;

        field_return field13 = null;


        CommonTree MATDATA10_tree=null;
        CommonTree OPEN11_tree=null;
        CommonTree CLOSE14_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:78:12: ( MATDATA OPEN ( struct | field )+ CLOSE )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:78:14: MATDATA OPEN ( struct | field )+ CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            MATDATA10=(Token)input.LT(1);
            match(input,MATDATA,FOLLOW_MATDATA_in_mddesc192); 
            MATDATA10_tree = (CommonTree)adaptor.create(MATDATA10);
            root_0 = (CommonTree)adaptor.becomeRoot(MATDATA10_tree, root_0);

            OPEN11=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_mddesc195); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:78:29: ( struct | field )+
            int cnt4=0;
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==KEY) ) {
                    int LA4_2 = input.LA(2);

                    if ( ((LA4_2>=KEY && LA4_2<=STRING)) ) {
                        alt4=2;
                    }
                    else if ( (LA4_2==OPEN) ) {
                        alt4=1;
                    }


                }


                switch (alt4) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:78:30: struct
            	    {
            	    pushFollow(FOLLOW_struct_in_mddesc199);
            	    struct12=struct();
            	    _fsp--;

            	    adaptor.addChild(root_0, struct12.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:78:37: field
            	    {
            	    pushFollow(FOLLOW_field_in_mddesc201);
            	    field13=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field13.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            CLOSE14=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_mddesc205); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end mddesc

    public static class sfdesc_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start sfdesc
    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:81:1: sfdesc : STATEF OPEN ( struct | field )+ CLOSE ;
    public final sfdesc_return sfdesc() throws RecognitionException {
        sfdesc_return retval = new sfdesc_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token STATEF15=null;
        Token OPEN16=null;
        Token CLOSE19=null;
        struct_return struct17 = null;

        field_return field18 = null;


        CommonTree STATEF15_tree=null;
        CommonTree OPEN16_tree=null;
        CommonTree CLOSE19_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:81:12: ( STATEF OPEN ( struct | field )+ CLOSE )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:81:14: STATEF OPEN ( struct | field )+ CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            STATEF15=(Token)input.LT(1);
            match(input,STATEF,FOLLOW_STATEF_in_sfdesc219); 
            STATEF15_tree = (CommonTree)adaptor.create(STATEF15);
            root_0 = (CommonTree)adaptor.becomeRoot(STATEF15_tree, root_0);

            OPEN16=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_sfdesc222); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:81:28: ( struct | field )+
            int cnt5=0;
            loop5:
            do {
                int alt5=3;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==KEY) ) {
                    int LA5_2 = input.LA(2);

                    if ( ((LA5_2>=KEY && LA5_2<=STRING)) ) {
                        alt5=2;
                    }
                    else if ( (LA5_2==OPEN) ) {
                        alt5=1;
                    }


                }


                switch (alt5) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:81:29: struct
            	    {
            	    pushFollow(FOLLOW_struct_in_sfdesc226);
            	    struct17=struct();
            	    _fsp--;

            	    adaptor.addChild(root_0, struct17.getTree());

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:81:36: field
            	    {
            	    pushFollow(FOLLOW_field_in_sfdesc228);
            	    field18=field();
            	    _fsp--;

            	    adaptor.addChild(root_0, field18.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            CLOSE19=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_sfdesc232); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end sfdesc

    public static class struct_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start struct
    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:1: struct : key= KEY OPEN (content= ( struct | field ) )* CLOSE ;
    public final struct_return struct() throws RecognitionException {
        struct_return retval = new struct_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token key=null;
        Token content=null;
        Token OPEN20=null;
        Token CLOSE23=null;
        struct_return struct21 = null;

        field_return field22 = null;


        CommonTree key_tree=null;
        CommonTree content_tree=null;
        CommonTree OPEN20_tree=null;
        CommonTree CLOSE23_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:15: (key= KEY OPEN (content= ( struct | field ) )* CLOSE )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:17: key= KEY OPEN (content= ( struct | field ) )* CLOSE
            {
            root_0 = (CommonTree)adaptor.nil();

            key=(Token)input.LT(1);
            match(input,KEY,FOLLOW_KEY_in_struct251); 
            key_tree = (CommonTree)adaptor.create(key);
            root_0 = (CommonTree)adaptor.becomeRoot(key_tree, root_0);

            OPEN20=(Token)input.LT(1);
            match(input,OPEN,FOLLOW_OPEN_in_struct254); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:39: (content= ( struct | field ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==KEY) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:39: content= ( struct | field )
            	    {
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:40: ( struct | field )
            	    int alt6=2;
            	    int LA6_0 = input.LA(1);

            	    if ( (LA6_0==KEY) ) {
            	        int LA6_1 = input.LA(2);

            	        if ( (LA6_1==OPEN) ) {
            	            alt6=1;
            	        }
            	        else if ( ((LA6_1>=KEY && LA6_1<=STRING)) ) {
            	            alt6=2;
            	        }
            	        else {
            	            NoViableAltException nvae =
            	                new NoViableAltException("84:40: ( struct | field )", 6, 1, input);

            	            throw nvae;
            	        }
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("84:40: ( struct | field )", 6, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt6) {
            	        case 1 :
            	            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:41: struct
            	            {
            	            pushFollow(FOLLOW_struct_in_struct260);
            	            struct21=struct();
            	            _fsp--;

            	            adaptor.addChild(root_0, struct21.getTree());

            	            }
            	            break;
            	        case 2 :
            	            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:84:48: field
            	            {
            	            pushFollow(FOLLOW_field_in_struct262);
            	            field22=field();
            	            _fsp--;

            	            adaptor.addChild(root_0, field22.getTree());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            CLOSE23=(Token)input.LT(1);
            match(input,CLOSE,FOLLOW_CLOSE_in_struct266); 

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end struct

    public static class field_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start field
    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:88:1: field : key= KEY value= ( INTER | KEY | STRING ) ;
    public final field_return field() throws RecognitionException {
        field_return retval = new field_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token key=null;
        Token value=null;

        CommonTree key_tree=null;
        CommonTree value_tree=null;

        try {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:88:13: (key= KEY value= ( INTER | KEY | STRING ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:88:15: key= KEY value= ( INTER | KEY | STRING )
            {
            root_0 = (CommonTree)adaptor.nil();

            key=(Token)input.LT(1);
            match(input,KEY,FOLLOW_KEY_in_field286); 
            key_tree = (CommonTree)adaptor.create(key);
            root_0 = (CommonTree)adaptor.becomeRoot(key_tree, root_0);

            value=(Token)input.LT(1);
            if ( (input.LA(1)>=KEY && input.LA(1)<=STRING) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(value));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_field291);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        
        	catch (RecognitionException re) {
        		reportError(re);
        		throw re;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end field


 

    public static final BitSet FOLLOW_mdl_in_parse116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_sldesc_in_mdl137 = new BitSet(new long[]{0x0000000000000302L});
    public static final BitSet FOLLOW_mddesc_in_mdl139 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_sfdesc_in_mdl142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_sldesc161 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_sldesc168 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_struct_in_sldesc172 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_field_in_sldesc174 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_CLOSE_in_sldesc178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MATDATA_in_mddesc192 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_mddesc195 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_struct_in_mddesc199 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_field_in_mddesc201 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_CLOSE_in_mddesc205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STATEF_in_sfdesc219 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_sfdesc222 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_struct_in_sfdesc226 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_field_in_sfdesc228 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_CLOSE_in_sfdesc232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_KEY_in_struct251 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_OPEN_in_struct254 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_struct_in_struct260 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_field_in_struct262 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_CLOSE_in_struct266 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_KEY_in_field286 = new BitSet(new long[]{0x0000000000001C00L});
    public static final BitSet FOLLOW_set_in_field291 = new BitSet(new long[]{0x0000000000000002L});

}