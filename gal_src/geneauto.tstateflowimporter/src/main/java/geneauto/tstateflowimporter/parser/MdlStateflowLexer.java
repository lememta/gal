// $ANTLR 3.0.1 T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g 2009-12-01 07:13:25

package geneauto.tstateflowimporter.parser;


import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.RecognitionException;

public class MdlStateflowLexer extends Lexer {
    public static final int OPEN=6;
    public static final int WS=14;
    public static final int NEWLINE=15;
    public static final int CLOSE=7;
    public static final int KEY=10;
    public static final int MATDATA=8;
    public static final int STATEF=9;
    public static final int INTER=11;
    public static final int MODEL=4;
    public static final int COMMENT=13;
    public static final int Tokens=16;
    public static final int EOF=-1;
    public static final int LIB=5;
    public static final int STRING=12;
    public MdlStateflowLexer() {;} 
    public MdlStateflowLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g"; }

    // $ANTLR start MODEL
    public final void mMODEL() throws RecognitionException {
        try {
            int _type = MODEL;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:92:7: ( 'Model' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:92:9: 'Model'
            {
            match("Model"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end MODEL

    // $ANTLR start LIB
    public final void mLIB() throws RecognitionException {
        try {
            int _type = LIB;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:93:6: ( 'Library' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:93:8: 'Library'
            {
            match("Library"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LIB

    // $ANTLR start STATEF
    public final void mSTATEF() throws RecognitionException {
        try {
            int _type = STATEF;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:94:8: ( 'Stateflow' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:94:10: 'Stateflow'
            {
            match("Stateflow"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STATEF

    // $ANTLR start MATDATA
    public final void mMATDATA() throws RecognitionException {
        try {
            int _type = MATDATA;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:95:9: ( 'MatData' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:95:11: 'MatData'
            {
            match("MatData"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end MATDATA

    // $ANTLR start CLOSE
    public final void mCLOSE() throws RecognitionException {
        try {
            int _type = CLOSE;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:98:7: ( '}' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:98:9: '}'
            {
            match('}'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end CLOSE

    // $ANTLR start OPEN
    public final void mOPEN() throws RecognitionException {
        try {
            int _type = OPEN;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:99:6: ( '{' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:99:8: '{'
            {
            match('{'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end OPEN

    // $ANTLR start COMMENT
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:103:9: ( '#' ( ' ' | '\\t' | KEY )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:103:11: '#' ( ' ' | '\\t' | KEY )*
            {
            match('#'); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:103:15: ( ' ' | '\\t' | KEY )*
            loop1:
            do {
                int alt1=4;
                switch ( input.LA(1) ) {
                case ' ':
                    {
                    alt1=1;
                    }
                    break;
                case '\t':
                    {
                    alt1=2;
                    }
                    break;
                case '\"':
                case '$':
                case '%':
                case '(':
                case ')':
                case '+':
                case ',':
                case '-':
                case '.':
                case '/':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case ':':
                case ';':
                case '<':
                case '>':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '[':
                case ']':
                case '_':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                case '|':
                case '~':
                    {
                    alt1=3;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:103:16: ' '
            	    {
            	    match(' '); 

            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:103:20: '\\t'
            	    {
            	    match('\t'); 

            	    }
            	    break;
            	case 3 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:103:25: KEY
            	    {
            	    mKEY(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMMENT

    // $ANTLR start STRING
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:9: ( '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"' ( ( '\\r' )? '\\n' ( '\\t' | ' ' )* ( '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"' )? )* )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:11: '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"' ( ( '\\r' )? '\\n' ( '\\t' | ' ' )* ( '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"' )? )*
            {
            match('\"'); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:15: (~ ( '\"' ) | '\\\\' '\"' )*
            loop2:
            do {
                int alt2=3;
                alt2 = dfa2.predict(input);
                switch (alt2) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:16: ~ ( '\"' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;
            	case 2 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:25: '\\\\' '\"'
            	    {
            	    match('\\'); 
            	    match('\"'); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            match('\"'); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:40: ( ( '\\r' )? '\\n' ( '\\t' | ' ' )* ( '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"' )? )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0=='\n'||LA7_0=='\r') ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:41: ( '\\r' )? '\\n' ( '\\t' | ' ' )* ( '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"' )?
            	    {
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:41: ( '\\r' )?
            	    int alt3=2;
            	    int LA3_0 = input.LA(1);

            	    if ( (LA3_0=='\r') ) {
            	        alt3=1;
            	    }
            	    switch (alt3) {
            	        case 1 :
            	            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:41: '\\r'
            	            {
            	            match('\r'); 

            	            }
            	            break;

            	    }

            	    match('\n'); 
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:52: ( '\\t' | ' ' )*
            	    loop4:
            	    do {
            	        int alt4=2;
            	        int LA4_0 = input.LA(1);

            	        if ( (LA4_0=='\t'||LA4_0==' ') ) {
            	            alt4=1;
            	        }


            	        switch (alt4) {
            	    	case 1 :
            	    	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:
            	    	    {
            	    	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	    	        input.consume();

            	    	    }
            	    	    else {
            	    	        MismatchedSetException mse =
            	    	            new MismatchedSetException(null,input);
            	    	        recover(mse);    throw mse;
            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop4;
            	        }
            	    } while (true);

            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:64: ( '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"' )?
            	    int alt6=2;
            	    int LA6_0 = input.LA(1);

            	    if ( (LA6_0=='\"') ) {
            	        alt6=1;
            	    }
            	    switch (alt6) {
            	        case 1 :
            	            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:65: '\"' (~ ( '\"' ) | '\\\\' '\"' )* '\"'
            	            {
            	            match('\"'); 
            	            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:69: (~ ( '\"' ) | '\\\\' '\"' )*
            	            loop5:
            	            do {
            	                int alt5=3;
            	                alt5 = dfa5.predict(input);
            	                switch (alt5) {
            	            	case 1 :
            	            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:70: ~ ( '\"' )
            	            	    {
            	            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='\uFFFE') ) {
            	            	        input.consume();

            	            	    }
            	            	    else {
            	            	        MismatchedSetException mse =
            	            	            new MismatchedSetException(null,input);
            	            	        recover(mse);    throw mse;
            	            	    }


            	            	    }
            	            	    break;
            	            	case 2 :
            	            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:109:79: '\\\\' '\"'
            	            	    {
            	            	    match('\\'); 
            	            	    match('\"'); 

            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop5;
            	                }
            	            } while (true);

            	            match('\"'); 

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING

    // $ANTLR start INTER
    public final void mINTER() throws RecognitionException {
        try {
            int _type = INTER;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:116:7: ( ( '[' ( '0' .. '9' | ' ' | ',' .. '.' | ';' )+ ']' ) )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:116:9: ( '[' ( '0' .. '9' | ' ' | ',' .. '.' | ';' )+ ']' )
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:116:9: ( '[' ( '0' .. '9' | ' ' | ',' .. '.' | ';' )+ ']' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:116:10: '[' ( '0' .. '9' | ' ' | ',' .. '.' | ';' )+ ']'
            {
            match('['); 
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:116:14: ( '0' .. '9' | ' ' | ',' .. '.' | ';' )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==' '||(LA8_0>=',' && LA8_0<='.')||(LA8_0>='0' && LA8_0<='9')||LA8_0==';') ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:
            	    {
            	    if ( input.LA(1)==' '||(input.LA(1)>=',' && input.LA(1)<='.')||(input.LA(1)>='0' && input.LA(1)<='9')||input.LA(1)==';' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            match(']'); 

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end INTER

    // $ANTLR start KEY
    public final void mKEY() throws RecognitionException {
        try {
            int _type = KEY;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:120:5: ( ( '0' .. '9' | ',' | '\"' | '%' | '(' .. ')' | '+' .. ':' | ';' | '<' | '>' | 'A' .. 'Z' | '[' | ']' | '_' | 'a' .. 'z' | '|' | '~' | '$' )+ )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:120:7: ( '0' .. '9' | ',' | '\"' | '%' | '(' .. ')' | '+' .. ':' | ';' | '<' | '>' | 'A' .. 'Z' | '[' | ']' | '_' | 'a' .. 'z' | '|' | '~' | '$' )+
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:120:7: ( '0' .. '9' | ',' | '\"' | '%' | '(' .. ')' | '+' .. ':' | ';' | '<' | '>' | 'A' .. 'Z' | '[' | ']' | '_' | 'a' .. 'z' | '|' | '~' | '$' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='\"'||(LA9_0>='$' && LA9_0<='%')||(LA9_0>='(' && LA9_0<=')')||(LA9_0>='+' && LA9_0<='<')||LA9_0=='>'||(LA9_0>='A' && LA9_0<='[')||LA9_0==']'||LA9_0=='_'||(LA9_0>='a' && LA9_0<='z')||LA9_0=='|'||LA9_0=='~') ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:
            	    {
            	    if ( input.LA(1)=='\"'||(input.LA(1)>='$' && input.LA(1)<='%')||(input.LA(1)>='(' && input.LA(1)<=')')||(input.LA(1)>='+' && input.LA(1)<='<')||input.LA(1)=='>'||(input.LA(1)>='A' && input.LA(1)<='[')||input.LA(1)==']'||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||input.LA(1)=='|'||input.LA(1)=='~' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end KEY

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:124:4: ( ( ' ' | '\\t' )+ )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:124:6: ( ' ' | '\\t' )+
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:124:6: ( ' ' | '\\t' )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0=='\t'||LA10_0==' ') ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    // $ANTLR start NEWLINE
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:128:9: ( ( '\\r' )? '\\n' )
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:128:11: ( '\\r' )? '\\n'
            {
            // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:128:11: ( '\\r' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='\r') ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:128:11: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 
            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NEWLINE

    public void mTokens() throws RecognitionException {
        // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:8: ( MODEL | LIB | STATEF | MATDATA | CLOSE | OPEN | COMMENT | STRING | INTER | KEY | WS | NEWLINE )
        int alt12=12;
        alt12 = dfa12.predict(input);
        switch (alt12) {
            case 1 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:10: MODEL
                {
                mMODEL(); 

                }
                break;
            case 2 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:16: LIB
                {
                mLIB(); 

                }
                break;
            case 3 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:20: STATEF
                {
                mSTATEF(); 

                }
                break;
            case 4 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:27: MATDATA
                {
                mMATDATA(); 

                }
                break;
            case 5 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:35: CLOSE
                {
                mCLOSE(); 

                }
                break;
            case 6 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:41: OPEN
                {
                mOPEN(); 

                }
                break;
            case 7 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:46: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 8 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:54: STRING
                {
                mSTRING(); 

                }
                break;
            case 9 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:61: INTER
                {
                mINTER(); 

                }
                break;
            case 10 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:67: KEY
                {
                mKEY(); 

                }
                break;
            case 11 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:71: WS
                {
                mWS(); 

                }
                break;
            case 12 :
                // T:\\geneauto-dev\\Eclipse\\geneauto.tstateflowimporter\\antlr_grammar\\MdlStateflow.g:1:74: NEWLINE
                {
                mNEWLINE(); 

                }
                break;

        }

    }


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA5 dfa5 = new DFA5(this);
    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA2_eotS =
        "\4\uffff\1\3\1\uffff\1\3\1\uffff\1\3\1\7\1\uffff\2\7\1\3\1\uffff"+
        "\2\3\1\7";
    static final String DFA2_eofS =
        "\22\uffff";
    static final String DFA2_minS =
        "\1\0\1\uffff\1\0\1\uffff\3\0\1\uffff\12\0";
    static final String DFA2_maxS =
        "\1\ufffe\1\uffff\1\ufffe\1\uffff\3\ufffe\1\uffff\12\ufffe";
    static final String DFA2_acceptS =
        "\1\uffff\1\3\1\uffff\1\1\3\uffff\1\2\12\uffff";
    static final String DFA2_specialS =
        "\22\uffff}>";
    static final String[] DFA2_transitionS = {
            "\42\3\1\1\71\3\1\2\uffa2\3",
            "",
            "\42\3\1\4\uffdc\3",
            "",
            "\12\7\1\6\2\7\1\5\ufff1\7",
            "\12\7\1\6\ufff4\7",
            "\11\7\1\10\1\6\2\7\1\5\22\7\1\10\1\7\1\11\uffdc\7",
            "",
            "\11\7\1\10\1\6\2\7\1\5\22\7\1\10\1\7\1\11\uffdc\7",
            "\12\3\1\13\2\3\1\12\ufff1\3",
            "\12\3\1\13\ufff4\3",
            "\11\3\1\14\1\13\2\3\1\12\22\3\1\14\1\3\1\15\uffdc\3",
            "\11\3\1\14\1\13\2\3\1\12\22\3\1\14\1\3\1\15\uffdc\3",
            "\12\7\1\17\2\7\1\16\ufff1\7",
            "\12\7\1\17\ufff4\7",
            "\11\7\1\20\1\17\2\7\1\16\22\7\1\20\1\7\1\21\uffdc\7",
            "\11\7\1\20\1\17\2\7\1\16\22\7\1\20\1\7\1\21\uffdc\7",
            "\12\3\1\13\2\3\1\12\ufff1\3"
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "()* loopback of 109:15: (~ ( '\"' ) | '\\\\' '\"' )*";
        }
    }
    static final String DFA5_eotS =
        "\4\uffff\1\3\2\uffff\2\3\1\5\1\uffff\2\5\1\3";
    static final String DFA5_eofS =
        "\16\uffff";
    static final String DFA5_minS =
        "\1\0\1\uffff\1\0\1\uffff\1\0\1\uffff\10\0";
    static final String DFA5_maxS =
        "\1\ufffe\1\uffff\1\ufffe\1\uffff\1\ufffe\1\uffff\10\ufffe";
    static final String DFA5_acceptS =
        "\1\uffff\1\3\1\uffff\1\1\1\uffff\1\2\10\uffff";
    static final String DFA5_specialS =
        "\16\uffff}>";
    static final String[] DFA5_transitionS = {
            "\42\3\1\1\71\3\1\2\uffa2\3",
            "",
            "\42\3\1\4\uffdc\3",
            "",
            "\12\5\1\7\2\5\1\6\ufff1\5",
            "",
            "\12\5\1\7\ufff4\5",
            "\11\5\1\10\1\7\2\5\1\6\22\5\1\10\1\5\1\11\uffdc\5",
            "\11\5\1\10\1\7\2\5\1\6\22\5\1\10\1\5\1\11\uffdc\5",
            "\12\3\1\13\2\3\1\12\ufff1\3",
            "\12\3\1\13\ufff4\3",
            "\11\3\1\14\1\13\2\3\1\12\22\3\1\14\1\3\1\15\uffdc\3",
            "\11\3\1\14\1\13\2\3\1\12\22\3\1\14\1\3\1\15\uffdc\3",
            "\12\5\1\7\2\5\1\6\ufff1\5"
    };

    static final short[] DFA5_eot = DFA.unpackEncodedString(DFA5_eotS);
    static final short[] DFA5_eof = DFA.unpackEncodedString(DFA5_eofS);
    static final char[] DFA5_min = DFA.unpackEncodedStringToUnsignedChars(DFA5_minS);
    static final char[] DFA5_max = DFA.unpackEncodedStringToUnsignedChars(DFA5_maxS);
    static final short[] DFA5_accept = DFA.unpackEncodedString(DFA5_acceptS);
    static final short[] DFA5_special = DFA.unpackEncodedString(DFA5_specialS);
    static final short[][] DFA5_transition;

    static {
        int numStates = DFA5_transitionS.length;
        DFA5_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA5_transition[i] = DFA.unpackEncodedString(DFA5_transitionS[i]);
        }
    }

    class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = DFA5_eot;
            this.eof = DFA5_eof;
            this.min = DFA5_min;
            this.max = DFA5_max;
            this.accept = DFA5_accept;
            this.special = DFA5_special;
            this.transition = DFA5_transition;
        }
        public String getDescription() {
            return "()* loopback of 109:69: (~ ( '\"' ) | '\\\\' '\"' )*";
        }
    }
    static final String DFA12_eotS =
        "\1\uffff\3\11\3\uffff\2\11\3\uffff\5\11\1\22\1\uffff\1\11\1\uffff"+
        "\4\11\1\24\4\11\1\42\3\11\1\uffff\3\11\1\51\1\52\1\11\2\uffff\1"+
        "\11\1\55\1\uffff";
    static final String DFA12_eofS =
        "\56\uffff";
    static final String DFA12_minS =
        "\1\11\1\141\1\151\1\164\3\uffff\1\0\1\40\3\uffff\1\144\1\164\1\142"+
        "\1\141\1\0\1\42\1\uffff\1\40\1\uffff\1\145\1\104\1\162\1\164\1\42"+
        "\1\154\2\141\1\145\1\42\1\164\1\162\1\146\1\uffff\1\141\1\171\1"+
        "\154\2\42\1\157\2\uffff\1\167\1\42\1\uffff";
    static final String DFA12_maxS =
        "\1\176\1\157\1\151\1\164\3\uffff\1\ufffe\1\73\3\uffff\1\144\1\164"+
        "\1\142\1\141\1\ufffe\1\176\1\uffff\1\135\1\uffff\1\145\1\104\1\162"+
        "\1\164\1\176\1\154\2\141\1\145\1\176\1\164\1\162\1\146\1\uffff\1"+
        "\141\1\171\1\154\2\176\1\157\2\uffff\1\167\1\176\1\uffff";
    static final String DFA12_acceptS =
        "\4\uffff\1\5\1\6\1\7\2\uffff\1\12\1\13\1\14\6\uffff\1\10\1\uffff"+
        "\1\11\15\uffff\1\1\6\uffff\1\4\1\2\2\uffff\1\3";
    static final String DFA12_specialS =
        "\56\uffff}>";
    static final String[] DFA12_transitionS = {
            "\1\12\1\13\2\uffff\1\13\22\uffff\1\12\1\uffff\1\7\1\6\2\11\2"+
            "\uffff\2\11\1\uffff\22\11\1\uffff\1\11\2\uffff\13\11\1\2\1\1"+
            "\5\11\1\3\7\11\1\10\1\uffff\1\11\1\uffff\1\11\1\uffff\32\11"+
            "\1\5\1\11\1\4\1\11",
            "\1\15\15\uffff\1\14",
            "\1\16",
            "\1\17",
            "",
            "",
            "",
            "\42\22\1\21\1\22\2\20\2\22\2\20\1\22\22\20\1\22\1\20\2\22\33"+
            "\20\1\22\1\20\1\22\1\20\1\22\32\20\1\22\1\20\1\22\1\20\uff80"+
            "\22",
            "\1\24\13\uffff\3\23\1\uffff\12\23\1\uffff\1\23",
            "",
            "",
            "",
            "\1\25",
            "\1\26",
            "\1\27",
            "\1\30",
            "\42\22\1\21\1\22\2\20\2\22\2\20\1\22\22\20\1\22\1\20\2\22\33"+
            "\20\1\22\1\20\1\22\1\20\1\22\32\20\1\22\1\20\1\22\1\20\uff80"+
            "\22",
            "\1\11\1\uffff\2\11\2\uffff\2\11\1\uffff\22\11\1\uffff\1\11\2"+
            "\uffff\33\11\1\uffff\1\11\1\uffff\1\11\1\uffff\32\11\1\uffff"+
            "\1\11\1\uffff\1\11",
            "",
            "\1\24\13\uffff\3\23\1\uffff\12\23\1\uffff\1\23\41\uffff\1\31",
            "",
            "\1\32",
            "\1\33",
            "\1\34",
            "\1\35",
            "\1\11\1\uffff\2\11\2\uffff\2\11\1\uffff\22\11\1\uffff\1\11\2"+
            "\uffff\33\11\1\uffff\1\11\1\uffff\1\11\1\uffff\32\11\1\uffff"+
            "\1\11\1\uffff\1\11",
            "\1\36",
            "\1\37",
            "\1\40",
            "\1\41",
            "\1\11\1\uffff\2\11\2\uffff\2\11\1\uffff\22\11\1\uffff\1\11\2"+
            "\uffff\33\11\1\uffff\1\11\1\uffff\1\11\1\uffff\32\11\1\uffff"+
            "\1\11\1\uffff\1\11",
            "\1\43",
            "\1\44",
            "\1\45",
            "",
            "\1\46",
            "\1\47",
            "\1\50",
            "\1\11\1\uffff\2\11\2\uffff\2\11\1\uffff\22\11\1\uffff\1\11\2"+
            "\uffff\33\11\1\uffff\1\11\1\uffff\1\11\1\uffff\32\11\1\uffff"+
            "\1\11\1\uffff\1\11",
            "\1\11\1\uffff\2\11\2\uffff\2\11\1\uffff\22\11\1\uffff\1\11\2"+
            "\uffff\33\11\1\uffff\1\11\1\uffff\1\11\1\uffff\32\11\1\uffff"+
            "\1\11\1\uffff\1\11",
            "\1\53",
            "",
            "",
            "\1\54",
            "\1\11\1\uffff\2\11\2\uffff\2\11\1\uffff\22\11\1\uffff\1\11\2"+
            "\uffff\33\11\1\uffff\1\11\1\uffff\1\11\1\uffff\32\11\1\uffff"+
            "\1\11\1\uffff\1\11",
            ""
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( MODEL | LIB | STATEF | MATDATA | CLOSE | OPEN | COMMENT | STRING | INTER | KEY | WS | NEWLINE );";
        }
    }
 

}