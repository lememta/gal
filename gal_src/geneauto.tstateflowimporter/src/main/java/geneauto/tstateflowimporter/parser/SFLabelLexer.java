// $ANTLR 3.0.1 T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g 2011-09-14 13:46:39

package geneauto.tstateflowimporter.parser;

// Required for lexer::members netToken()
/*
*/
import geneauto.eventhandler.EventHandler;
import geneauto.eventhandler.EventLevel;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.FailedPredicateException;
import org.antlr.runtime.IntStream;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;

public class SFLabelLexer extends Lexer {
    public static final int DOTNUMBER=79;
    public static final int LOR=55;
    public static final int TrueExpression=23;
    public static final int ExpressionStatement=19;
    public static final int OnEventActions=12;
    public static final int EXPONENT=80;
    public static final int EQ_OP=59;
    public static final int COMP_OP=60;
    public static final int STAR=62;
    public static final int DU=43;
    public static final int MOD=63;
    public static final int LETTER=76;
    public static final int BindDecls=13;
    public static final int SHIFT=61;
    public static final int AssignStatement=16;
    public static final int ParenthesisExpression=28;
    public static final int NOT=65;
    public static final int AND=58;
    public static final int EOF=-1;
    public static final int Guard=6;
    public static final int LPAREN=71;
    public static final int EntryActions=9;
    public static final int RCOMMENT=51;
    public static final int RPAREN=72;
    public static final int SLASH=40;
    public static final int COMMA=48;
    public static final int EX=44;
    public static final int BIND=47;
    public static final int CompoundStatement=15;
    public static final int TILDE=66;
    public static final int IndexExpressions=32;
    public static final int IDENT=74;
    public static final int CallExpression=21;
    public static final int EN=42;
    public static final int PLUS=52;
    public static final int DIGIT=78;
    public static final int NL=41;
    public static final int DOT=73;
    public static final int COMMENT=50;
    public static final int Transition=4;
    public static final int UnaryExpression=30;
    public static final int SCOLON=49;
    public static final int INTEGER=68;
    public static final int TernaryExpression=29;
    public static final int XOR=57;
    public static final int UnLabelledActions=14;
    public static final int TransitionStatements=8;
    public static final int SYMBOL=77;
    public static final int FalseExpression=24;
    public static final int RealExpression=26;
    public static final int ExitActions=11;
    public static final int NUMBER=67;
    public static final int ON=45;
    public static final int POWER=64;
    public static final int LSQUB=36;
    public static final int DecStatement=18;
    public static final int MINUS=53;
    public static final int IntegerExpression=22;
    public static final int Tokens=82;
    public static final int RSQUB=37;
    public static final int TRUE=69;
    public static final int Annotations=33;
    public static final int VariableExpression=31;
    public static final int ConditionStatements=7;
    public static final int COLON=46;
    public static final int BinaryExpression=20;
    public static final int WS=75;
    public static final int ANY=81;
    public static final int Events=5;
    public static final int OR=35;
    public static final int DotIdent=34;
    public static final int ASSIGN=54;
    public static final int RCURLB=39;
    public static final int ListExpression=25;
    public static final int DuringActions=10;
    public static final int FALSE=70;
    public static final int LCURLB=38;
    public static final int MemberExpression=27;
    public static final int LAND=56;
    public static final int IncStatement=17;
    
    // Semantic predicate - indicates that the C-bitwise operations flag was set in the source language
    public boolean cBitOptsOn = false;
    
    /* Comment to run AntLRWorks debugger
       Uncomment before release
    */
    public Token nextToken() {
      while (true) {
       this.token = null;
       this.channel = Token.DEFAULT_CHANNEL;
       this.tokenStartCharIndex = input.index();
       this.tokenStartCharPositionInLine = input.getCharPositionInLine();
       this.tokenStartLine = input.getLine();
       this.text = null;
       if ( input.LA(1)==CharStream.EOF ) {
        return Token.EOF_TOKEN;
       }
       try {
        mTokens();
        if ( this.token==null ) {
         emit();
        }
        else if ( this.token==Token.SKIP_TOKEN ) {
         continue;
        }
        return this.token;
       }
       catch (RecognitionException re) {
        reportError(re);
        EventHandler.handle( EventLevel.ERROR, "", "", "Errors while parsing - unable to continue.", "");
       }
      }
     }

    public SFLabelLexer() {;} 
    public SFLabelLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g"; }

    // $ANTLR start EN
    public final void mEN() throws RecognitionException {
        try {
            int _type = EN;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:310:4: ( ( 'entry' | 'en' ) ( ' ' | '\\t' )* ':' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:310:6: ( 'entry' | 'en' ) ( ' ' | '\\t' )* ':'
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:310:6: ( 'entry' | 'en' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='e') ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1=='n') ) {
                    int LA1_2 = input.LA(3);

                    if ( (LA1_2=='t') ) {
                        alt1=1;
                    }
                    else if ( (LA1_2=='\t'||LA1_2==' '||LA1_2==':') ) {
                        alt1=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("310:6: ( 'entry' | 'en' )", 1, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("310:6: ( 'entry' | 'en' )", 1, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("310:6: ( 'entry' | 'en' )", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:310:7: 'entry'
                    {
                    match("entry"); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:310:15: 'en'
                    {
                    match("en"); 


                    }
                    break;

            }

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:310:21: ( ' ' | '\\t' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='\t'||LA2_0==' ') ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            match(':'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end EN

    // $ANTLR start DU
    public final void mDU() throws RecognitionException {
        try {
            int _type = DU;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:311:4: ( ( 'during' | 'du' ) ( ' ' | '\\t' )* ':' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:311:6: ( 'during' | 'du' ) ( ' ' | '\\t' )* ':'
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:311:6: ( 'during' | 'du' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='d') ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1=='u') ) {
                    int LA3_2 = input.LA(3);

                    if ( (LA3_2=='r') ) {
                        alt3=1;
                    }
                    else if ( (LA3_2=='\t'||LA3_2==' '||LA3_2==':') ) {
                        alt3=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("311:6: ( 'during' | 'du' )", 3, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("311:6: ( 'during' | 'du' )", 3, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("311:6: ( 'during' | 'du' )", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:311:7: 'during'
                    {
                    match("during"); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:311:16: 'du'
                    {
                    match("du"); 


                    }
                    break;

            }

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:311:22: ( ' ' | '\\t' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='\t'||LA4_0==' ') ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            match(':'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DU

    // $ANTLR start EX
    public final void mEX() throws RecognitionException {
        try {
            int _type = EX;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:312:4: ( ( 'exit' | 'ex' ) ( ' ' | '\\t' )* ':' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:312:6: ( 'exit' | 'ex' ) ( ' ' | '\\t' )* ':'
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:312:6: ( 'exit' | 'ex' )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='e') ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1=='x') ) {
                    int LA5_2 = input.LA(3);

                    if ( (LA5_2=='i') ) {
                        alt5=1;
                    }
                    else if ( (LA5_2=='\t'||LA5_2==' '||LA5_2==':') ) {
                        alt5=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("312:6: ( 'exit' | 'ex' )", 5, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("312:6: ( 'exit' | 'ex' )", 5, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("312:6: ( 'exit' | 'ex' )", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:312:7: 'exit'
                    {
                    match("exit"); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:312:14: 'ex'
                    {
                    match("ex"); 


                    }
                    break;

            }

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:312:20: ( ' ' | '\\t' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0=='\t'||LA6_0==' ') ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            match(':'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end EX

    // $ANTLR start ON
    public final void mON() throws RecognitionException {
        try {
            int _type = ON;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:313:4: ( 'on' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:313:6: 'on'
            {
            match("on"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ON

    // $ANTLR start BIND
    public final void mBIND() throws RecognitionException {
        try {
            int _type = BIND;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:315:5: ( 'bind' ( ' ' | '\\t' )* ':' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:315:7: 'bind' ( ' ' | '\\t' )* ':'
            {
            match("bind"); 

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:315:14: ( ' ' | '\\t' )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0=='\t'||LA7_0==' ') ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            match(':'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end BIND

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:316:4: ( ( ' ' | '\\t' | '...\\\\n' ) )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:316:6: ( ' ' | '\\t' | '...\\\\n' )
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:316:6: ( ' ' | '\\t' | '...\\\\n' )
            int alt8=3;
            switch ( input.LA(1) ) {
            case ' ':
                {
                alt8=1;
                }
                break;
            case '\t':
                {
                alt8=2;
                }
                break;
            case '.':
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("316:6: ( ' ' | '\\t' | '...\\\\n' )", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:316:7: ' '
                    {
                    match(' '); 

                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:316:11: '\\t'
                    {
                    match('\t'); 

                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:316:16: '...\\\\n'
                    {
                    match("...\\n"); 


                    }
                    break;

            }

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    // $ANTLR start NL
    public final void mNL() throws RecognitionException {
        try {
            int _type = NL;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:317:5: ( '\\\\n' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:317:7: '\\\\n'
            {
            match("\\n"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NL

    // $ANTLR start COMMENT
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:320:9: ( '/*' ( . )* '*/' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:320:11: '/*' ( . )* '*/'
            {
            match("/*"); 

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:320:17: ( . )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='*') ) {
                    int LA9_1 = input.LA(2);

                    if ( (LA9_1=='/') ) {
                        alt9=2;
                    }
                    else if ( ((LA9_1>='\u0000' && LA9_1<='.')||(LA9_1>='0' && LA9_1<='\uFFFE')) ) {
                        alt9=1;
                    }


                }
                else if ( ((LA9_0>='\u0000' && LA9_0<=')')||(LA9_0>='+' && LA9_0<='\uFFFE')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:320:17: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            match("*/"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMMENT

    // $ANTLR start RCOMMENT
    public final void mRCOMMENT() throws RecognitionException {
        try {
            int _type = RCOMMENT;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:323:9: ( '//' (~ '\\\\' )* )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:323:11: '//' (~ '\\\\' )*
            {
            match("//"); 

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:323:17: (~ '\\\\' )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='\u0000' && LA10_0<='[')||(LA10_0>=']' && LA10_0<='\uFFFE')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:323:18: ~ '\\\\'
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RCOMMENT

    // $ANTLR start LPAREN
    public final void mLPAREN() throws RecognitionException {
        try {
            int _type = LPAREN;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:329:8: ( '(' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:329:10: '('
            {
            match('('); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LPAREN

    // $ANTLR start RPAREN
    public final void mRPAREN() throws RecognitionException {
        try {
            int _type = RPAREN;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:330:8: ( ')' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:330:10: ')'
            {
            match(')'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RPAREN

    // $ANTLR start LCURLB
    public final void mLCURLB() throws RecognitionException {
        try {
            int _type = LCURLB;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:331:8: ( '{' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:331:10: '{'
            {
            match('{'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LCURLB

    // $ANTLR start RCURLB
    public final void mRCURLB() throws RecognitionException {
        try {
            int _type = RCURLB;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:332:8: ( '}' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:332:10: '}'
            {
            match('}'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RCURLB

    // $ANTLR start LSQUB
    public final void mLSQUB() throws RecognitionException {
        try {
            int _type = LSQUB;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:333:7: ( '[' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:333:9: '['
            {
            match('['); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LSQUB

    // $ANTLR start RSQUB
    public final void mRSQUB() throws RecognitionException {
        try {
            int _type = RSQUB;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:334:7: ( ']' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:334:9: ']'
            {
            match(']'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RSQUB

    // $ANTLR start DOT
    public final void mDOT() throws RecognitionException {
        try {
            int _type = DOT;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:335:6: ( '.' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:335:8: '.'
            {
            match('.'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DOT

    // $ANTLR start COMMA
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:336:7: ( ',' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:336:9: ','
            {
            match(','); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMMA

    // $ANTLR start COLON
    public final void mCOLON() throws RecognitionException {
        try {
            int _type = COLON;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:337:7: ( ':' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:337:9: ':'
            {
            match(':'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COLON

    // $ANTLR start SCOLON
    public final void mSCOLON() throws RecognitionException {
        try {
            int _type = SCOLON;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:338:8: ( ';' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:338:10: ';'
            {
            match(';'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SCOLON

    // $ANTLR start PLUS
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:339:6: ( '+' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:339:8: '+'
            {
            match('+'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end PLUS

    // $ANTLR start MINUS
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:340:7: ( '-' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:340:9: '-'
            {
            match('-'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end MINUS

    // $ANTLR start STAR
    public final void mSTAR() throws RecognitionException {
        try {
            int _type = STAR;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:341:6: ( '*' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:341:8: '*'
            {
            match('*'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STAR

    // $ANTLR start SLASH
    public final void mSLASH() throws RecognitionException {
        try {
            int _type = SLASH;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:342:7: ( '/' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:342:9: '/'
            {
            match('/'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SLASH

    // $ANTLR start MOD
    public final void mMOD() throws RecognitionException {
        try {
            int _type = MOD;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:343:6: ( '%%' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:343:8: '%%'
            {
            match("%%"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end MOD

    // $ANTLR start LAND
    public final void mLAND() throws RecognitionException {
        try {
            int _type = LAND;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:344:7: ( '&&' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:344:9: '&&'
            {
            match("&&"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LAND

    // $ANTLR start LOR
    public final void mLOR() throws RecognitionException {
        try {
            int _type = LOR;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:345:7: ( '||' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:345:9: '||'
            {
            match("||"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LOR

    // $ANTLR start AND
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:346:6: ( '&' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:346:8: '&'
            {
            match('&'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end AND

    // $ANTLR start OR
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:347:6: ( '|' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:347:8: '|'
            {
            match('|'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end OR

    // $ANTLR start POWER
    public final void mPOWER() throws RecognitionException {
        try {
            int _type = POWER;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:348:8: ({...}? => '^' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:348:10: {...}? => '^'
            {
            if ( !(!cBitOptsOn ) ) {
                throw new FailedPredicateException(input, "POWER", "!cBitOptsOn ");
            }
            match('^'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end POWER

    // $ANTLR start XOR
    public final void mXOR() throws RecognitionException {
        try {
            int _type = XOR;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:349:6: ({...}? => '^' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:349:8: {...}? => '^'
            {
            if ( !( cBitOptsOn ) ) {
                throw new FailedPredicateException(input, "XOR", " cBitOptsOn ");
            }
            match('^'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end XOR

    // $ANTLR start SHIFT
    public final void mSHIFT() throws RecognitionException {
        try {
            int _type = SHIFT;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:350:7: ( ( '<<' | '>>' ) )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:350:9: ( '<<' | '>>' )
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:350:9: ( '<<' | '>>' )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='<') ) {
                alt11=1;
            }
            else if ( (LA11_0=='>') ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("350:9: ( '<<' | '>>' )", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:350:10: '<<'
                    {
                    match("<<"); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:350:15: '>>'
                    {
                    match(">>"); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SHIFT

    // $ANTLR start EQ_OP
    public final void mEQ_OP() throws RecognitionException {
        try {
            int _type = EQ_OP;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:351:7: ( ( '==' | '!=' | '~=' | '<>' ) )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:351:9: ( '==' | '!=' | '~=' | '<>' )
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:351:9: ( '==' | '!=' | '~=' | '<>' )
            int alt12=4;
            switch ( input.LA(1) ) {
            case '=':
                {
                alt12=1;
                }
                break;
            case '!':
                {
                alt12=2;
                }
                break;
            case '~':
                {
                alt12=3;
                }
                break;
            case '<':
                {
                alt12=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("351:9: ( '==' | '!=' | '~=' | '<>' )", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:351:10: '=='
                    {
                    match("=="); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:351:15: '!='
                    {
                    match("!="); 


                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:351:20: '~='
                    {
                    match("~="); 


                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:351:25: '<>'
                    {
                    match("<>"); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end EQ_OP

    // $ANTLR start COMP_OP
    public final void mCOMP_OP() throws RecognitionException {
        try {
            int _type = COMP_OP;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:352:9: ( ( '>=' | '<=' | '>' | '<' ) )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:352:11: ( '>=' | '<=' | '>' | '<' )
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:352:11: ( '>=' | '<=' | '>' | '<' )
            int alt13=4;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='>') ) {
                int LA13_1 = input.LA(2);

                if ( (LA13_1=='=') ) {
                    alt13=1;
                }
                else {
                    alt13=3;}
            }
            else if ( (LA13_0=='<') ) {
                int LA13_2 = input.LA(2);

                if ( (LA13_2=='=') ) {
                    alt13=2;
                }
                else {
                    alt13=4;}
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("352:11: ( '>=' | '<=' | '>' | '<' )", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:352:12: '>='
                    {
                    match(">="); 


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:352:17: '<='
                    {
                    match("<="); 


                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:352:22: '>'
                    {
                    match('>'); 

                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:352:26: '<'
                    {
                    match('<'); 

                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMP_OP

    // $ANTLR start ASSIGN
    public final void mASSIGN() throws RecognitionException {
        try {
            int _type = ASSIGN;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:9: ( ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' ) )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:11: ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' )
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:11: ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' )
            int alt14=8;
            switch ( input.LA(1) ) {
            case '=':
                {
                alt14=1;
                }
                break;
            case '+':
                {
                alt14=2;
                }
                break;
            case '-':
                {
                alt14=3;
                }
                break;
            case '*':
                {
                alt14=4;
                }
                break;
            case '/':
                {
                alt14=5;
                }
                break;
            case '&':
                {
                alt14=6;
                }
                break;
            case '|':
                {
                alt14=7;
                }
                break;
            case '^':
                {
                alt14=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("353:11: ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '|=' | '^=' )", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:12: '='
                    {
                    match('='); 

                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:16: '+='
                    {
                    match("+="); 


                    }
                    break;
                case 3 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:21: '-='
                    {
                    match("-="); 


                    }
                    break;
                case 4 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:26: '*='
                    {
                    match("*="); 


                    }
                    break;
                case 5 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:31: '/='
                    {
                    match("/="); 


                    }
                    break;
                case 6 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:36: '&='
                    {
                    match("&="); 


                    }
                    break;
                case 7 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:41: '|='
                    {
                    match("|="); 


                    }
                    break;
                case 8 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:353:46: '^='
                    {
                    match("^="); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ASSIGN

    // $ANTLR start NOT
    public final void mNOT() throws RecognitionException {
        try {
            int _type = NOT;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:354:7: ( '!' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:354:9: '!'
            {
            match('!'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NOT

    // $ANTLR start TILDE
    public final void mTILDE() throws RecognitionException {
        try {
            int _type = TILDE;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:355:7: ( '~' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:355:9: '~'
            {
            match('~'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end TILDE

    // $ANTLR start TRUE
    public final void mTRUE() throws RecognitionException {
        try {
            int _type = TRUE;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:356:9: ( 'true' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:356:13: 'true'
            {
            match("true"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end TRUE

    // $ANTLR start FALSE
    public final void mFALSE() throws RecognitionException {
        try {
            int _type = FALSE;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:357:9: ( 'false' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:357:13: 'false'
            {
            match("false"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end FALSE

    // $ANTLR start IDENT
    public final void mIDENT() throws RecognitionException {
        try {
            int _type = IDENT;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:363:7: ( ( LETTER | SYMBOL ) ( LETTER | SYMBOL | DIGIT )* )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:363:9: ( LETTER | SYMBOL ) ( LETTER | SYMBOL | DIGIT )*
            {
            if ( input.LA(1)=='$'||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:363:25: ( LETTER | SYMBOL | DIGIT )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0=='$'||(LA15_0>='0' && LA15_0<='9')||(LA15_0>='A' && LA15_0<='Z')||LA15_0=='_'||(LA15_0>='a' && LA15_0<='z')) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
            	    {
            	    if ( input.LA(1)=='$'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end IDENT

    // $ANTLR start NUMBER
    public final void mNUMBER() throws RecognitionException {
        try {
            int _type = NUMBER;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:365:8: ( INTEGER ( DOT | DOTNUMBER | EXPONENT ) | DOTNUMBER )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>='0' && LA17_0<='9')) ) {
                alt17=1;
            }
            else if ( (LA17_0=='.') ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("365:1: NUMBER : ( INTEGER ( DOT | DOTNUMBER | EXPONENT ) | DOTNUMBER );", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:365:10: INTEGER ( DOT | DOTNUMBER | EXPONENT )
                    {
                    mINTEGER(); 
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:365:18: ( DOT | DOTNUMBER | EXPONENT )
                    int alt16=3;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0=='.') ) {
                        int LA16_1 = input.LA(2);

                        if ( ((LA16_1>='0' && LA16_1<='9')) ) {
                            alt16=2;
                        }
                        else {
                            alt16=1;}
                    }
                    else if ( (LA16_0=='E'||LA16_0=='e') ) {
                        alt16=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("365:18: ( DOT | DOTNUMBER | EXPONENT )", 16, 0, input);

                        throw nvae;
                    }
                    switch (alt16) {
                        case 1 :
                            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:365:20: DOT
                            {
                            mDOT(); 

                            }
                            break;
                        case 2 :
                            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:365:26: DOTNUMBER
                            {
                            mDOTNUMBER(); 

                            }
                            break;
                        case 3 :
                            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:365:38: EXPONENT
                            {
                            mEXPONENT(); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:366:5: DOTNUMBER
                    {
                    mDOTNUMBER(); 

                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NUMBER

    // $ANTLR start INTEGER
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:9: ( ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ ) )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:12: ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ )
            {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:12: ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0=='0') ) {
                int LA20_1 = input.LA(2);

                if ( (LA20_1=='X'||LA20_1=='x') ) {
                    alt20=2;
                }
                else {
                    alt20=1;}
            }
            else if ( ((LA20_0>='1' && LA20_0<='9')) ) {
                alt20=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("368:12: ( ( DIGIT )+ | '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+ )", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:13: ( DIGIT )+
                    {
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:13: ( DIGIT )+
                    int cnt18=0;
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( ((LA18_0>='0' && LA18_0<='9')) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:13: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt18 >= 1 ) break loop18;
                                EarlyExitException eee =
                                    new EarlyExitException(18, input);
                                throw eee;
                        }
                        cnt18++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:22: '0' ( 'x' | 'X' ) ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+
                    {
                    match('0'); 
                    if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse =
                            new MismatchedSetException(null,input);
                        recover(mse);    throw mse;
                    }

                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:368:36: ( DIGIT | 'A' .. 'F' | 'a' .. 'f' )+
                    int cnt19=0;
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( ((LA19_0>='0' && LA19_0<='9')||(LA19_0>='A' && LA19_0<='F')||(LA19_0>='a' && LA19_0<='f')) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
                    	    {
                    	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse =
                    	            new MismatchedSetException(null,input);
                    	        recover(mse);    throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt19 >= 1 ) break loop19;
                                EarlyExitException eee =
                                    new EarlyExitException(19, input);
                                throw eee;
                        }
                        cnt19++;
                    } while (true);


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end INTEGER

    // $ANTLR start DOTNUMBER
    public final void mDOTNUMBER() throws RecognitionException {
        try {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:374:11: ( DOT INTEGER ( EXPONENT )? )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:374:13: DOT INTEGER ( EXPONENT )?
            {
            mDOT(); 
            mINTEGER(); 
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:374:25: ( EXPONENT )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0=='E'||LA21_0=='e') ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:374:26: EXPONENT
                    {
                    mEXPONENT(); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end DOTNUMBER

    // $ANTLR start EXPONENT
    public final void mEXPONENT() throws RecognitionException {
        try {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:376:11: ( ( 'e' | 'E' ) ( PLUS )? INTEGER )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:376:13: ( 'e' | 'E' ) ( PLUS )? INTEGER
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:376:23: ( PLUS )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0=='+') ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:376:23: PLUS
                    {
                    mPLUS(); 

                    }
                    break;

            }

            mINTEGER(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end EXPONENT

    // $ANTLR start LETTER
    public final void mLETTER() throws RecognitionException {
        try {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:378:8: ( 'a' .. 'z' | 'A' .. 'Z' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

        }
        finally {
        }
    }
    // $ANTLR end LETTER

    // $ANTLR start DIGIT
    public final void mDIGIT() throws RecognitionException {
        try {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:380:7: ( '0' .. '9' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:380:9: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end DIGIT

    // $ANTLR start SYMBOL
    public final void mSYMBOL() throws RecognitionException {
        try {
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:382:8: ( '_' | '$' )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:
            {
            if ( input.LA(1)=='$'||input.LA(1)=='_' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

        }
        finally {
        }
    }
    // $ANTLR end SYMBOL

    // $ANTLR start ANY
    public final void mANY() throws RecognitionException {
        try {
            int _type = ANY;
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:389:5: ( . )
            // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:389:8: .
            {
            matchAny(); 
            
            EventHandler.handle( EventLevel.ERROR, "labelParser", "",
            	"Unsupported character \'" + getText() + "\' - skipped ", "" );
            skip();


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ANY

    public void mTokens() throws RecognitionException {
        // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:8: ( EN | DU | EX | ON | BIND | WS | NL | COMMENT | RCOMMENT | LPAREN | RPAREN | LCURLB | RCURLB | LSQUB | RSQUB | DOT | COMMA | COLON | SCOLON | PLUS | MINUS | STAR | SLASH | MOD | LAND | LOR | AND | OR | POWER | XOR | SHIFT | EQ_OP | COMP_OP | ASSIGN | NOT | TILDE | TRUE | FALSE | IDENT | NUMBER | INTEGER | ANY )
        int alt23=42;
        alt23 = dfa23.predict(input);
        switch (alt23) {
            case 1 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:10: EN
                {
                mEN(); 

                }
                break;
            case 2 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:13: DU
                {
                mDU(); 

                }
                break;
            case 3 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:16: EX
                {
                mEX(); 

                }
                break;
            case 4 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:19: ON
                {
                mON(); 

                }
                break;
            case 5 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:22: BIND
                {
                mBIND(); 

                }
                break;
            case 6 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:27: WS
                {
                mWS(); 

                }
                break;
            case 7 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:30: NL
                {
                mNL(); 

                }
                break;
            case 8 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:33: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 9 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:41: RCOMMENT
                {
                mRCOMMENT(); 

                }
                break;
            case 10 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:50: LPAREN
                {
                mLPAREN(); 

                }
                break;
            case 11 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:57: RPAREN
                {
                mRPAREN(); 

                }
                break;
            case 12 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:64: LCURLB
                {
                mLCURLB(); 

                }
                break;
            case 13 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:71: RCURLB
                {
                mRCURLB(); 

                }
                break;
            case 14 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:78: LSQUB
                {
                mLSQUB(); 

                }
                break;
            case 15 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:84: RSQUB
                {
                mRSQUB(); 

                }
                break;
            case 16 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:90: DOT
                {
                mDOT(); 

                }
                break;
            case 17 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:94: COMMA
                {
                mCOMMA(); 

                }
                break;
            case 18 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:100: COLON
                {
                mCOLON(); 

                }
                break;
            case 19 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:106: SCOLON
                {
                mSCOLON(); 

                }
                break;
            case 20 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:113: PLUS
                {
                mPLUS(); 

                }
                break;
            case 21 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:118: MINUS
                {
                mMINUS(); 

                }
                break;
            case 22 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:124: STAR
                {
                mSTAR(); 

                }
                break;
            case 23 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:129: SLASH
                {
                mSLASH(); 

                }
                break;
            case 24 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:135: MOD
                {
                mMOD(); 

                }
                break;
            case 25 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:139: LAND
                {
                mLAND(); 

                }
                break;
            case 26 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:144: LOR
                {
                mLOR(); 

                }
                break;
            case 27 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:148: AND
                {
                mAND(); 

                }
                break;
            case 28 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:152: OR
                {
                mOR(); 

                }
                break;
            case 29 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:155: POWER
                {
                mPOWER(); 

                }
                break;
            case 30 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:161: XOR
                {
                mXOR(); 

                }
                break;
            case 31 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:165: SHIFT
                {
                mSHIFT(); 

                }
                break;
            case 32 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:171: EQ_OP
                {
                mEQ_OP(); 

                }
                break;
            case 33 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:177: COMP_OP
                {
                mCOMP_OP(); 

                }
                break;
            case 34 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:185: ASSIGN
                {
                mASSIGN(); 

                }
                break;
            case 35 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:192: NOT
                {
                mNOT(); 

                }
                break;
            case 36 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:196: TILDE
                {
                mTILDE(); 

                }
                break;
            case 37 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:202: TRUE
                {
                mTRUE(); 

                }
                break;
            case 38 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:207: FALSE
                {
                mFALSE(); 

                }
                break;
            case 39 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:213: IDENT
                {
                mIDENT(); 

                }
                break;
            case 40 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:219: NUMBER
                {
                mNUMBER(); 

                }
                break;
            case 41 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:226: INTEGER
                {
                mINTEGER(); 

                }
                break;
            case 42 :
                // T:\\geneauto-dev-ibk_ada_2\\geneauto.tstateflowimporter\\antlr_grammar\\SFLabel.g:1:234: ANY
                {
                mANY(); 

                }
                break;

        }

    }


    protected DFA23 dfa23 = new DFA23(this);
    static final String DFA23_eotS =
        "\1\uffff\4\47\2\uffff\1\54\1\44\1\62\11\uffff\1\74\1\75\1\76\1\44"+
        "\1\101\1\103\1\104\2\105\1\61\1\110\1\111\2\47\1\uffff\2\115\1\uffff"+
        "\2\47\1\uffff\1\47\1\125\1\47\37\uffff\2\47\2\uffff\1\115\1\47\1"+
        "\uffff\1\47\1\uffff\1\47\2\uffff\1\47\2\uffff\2\47\1\115\4\47\1"+
        "\146\1\47\1\115\2\47\2\uffff\1\153\2\55\1\47\1\uffff";
    static final String DFA23_eofS =
        "\154\uffff";
    static final String DFA23_minS =
        "\1\0\1\156\1\165\1\156\1\151\2\uffff\1\56\1\156\1\52\11\uffff\3"+
        "\75\1\45\1\46\2\75\1\74\1\76\3\75\1\162\1\141\1\uffff\2\56\1\uffff"+
        "\2\11\1\uffff\1\11\1\44\1\156\31\uffff\1\0\5\uffff\1\165\1\154\1"+
        "\60\1\uffff\1\56\1\162\1\uffff\1\164\1\uffff\1\151\2\uffff\1\144"+
        "\2\uffff\1\145\1\163\1\56\1\171\1\11\1\156\1\11\1\44\1\145\1\53"+
        "\1\11\1\147\2\uffff\1\44\2\60\1\11\1\uffff";
    static final String DFA23_maxS =
        "\1\ufffe\1\170\1\165\1\156\1\151\2\uffff\1\71\1\156\1\75\11\uffff"+
        "\3\75\1\45\1\75\1\174\1\75\2\76\3\75\1\162\1\141\1\uffff\1\170\1"+
        "\145\1\uffff\1\164\1\151\1\uffff\1\162\1\172\1\156\31\uffff\1\0"+
        "\5\uffff\1\165\1\154\1\146\1\uffff\1\145\1\162\1\uffff\1\164\1\uffff"+
        "\1\151\2\uffff\1\144\2\uffff\1\145\1\163\1\146\1\171\1\72\1\156"+
        "\1\72\1\172\1\145\1\146\1\72\1\147\2\uffff\1\172\2\146\1\72\1\uffff";
    static final String DFA23_acceptS =
        "\5\uffff\2\6\3\uffff\1\12\1\13\1\14\1\15\1\16\1\17\1\21\1\22\1\23"+
        "\16\uffff\1\47\2\uffff\1\52\2\uffff\1\47\3\uffff\1\6\1\20\1\50\1"+
        "\7\1\10\1\11\1\42\1\27\1\12\1\13\1\14\1\15\1\16\1\17\1\21\1\22\1"+
        "\23\1\24\1\25\1\26\1\30\1\31\1\33\1\32\1\34\1\uffff\1\41\1\40\1"+
        "\37\1\43\1\44\3\uffff\1\51\2\uffff\1\1\1\uffff\1\3\1\uffff\1\2\1"+
        "\4\1\uffff\1\35\1\36\14\uffff\1\5\1\45\4\uffff\1\46";
    static final String DFA23_specialS =
        "\104\uffff\1\0\47\uffff}>";
    static final String[] DFA23_transitionS = {
            "\11\44\1\6\26\44\1\5\1\35\2\44\1\41\1\26\1\27\1\44\1\12\1\13"+
            "\1\25\1\23\1\20\1\24\1\7\1\11\1\42\11\43\1\21\1\22\1\32\1\34"+
            "\1\33\2\44\32\41\1\16\1\10\1\17\1\31\1\41\1\44\1\41\1\4\1\41"+
            "\1\2\1\1\1\40\10\41\1\3\4\41\1\37\6\41\1\14\1\30\1\15\1\36\uff80"+
            "\44",
            "\1\45\11\uffff\1\46",
            "\1\50",
            "\1\51",
            "\1\52",
            "",
            "",
            "\1\53\1\uffff\12\55",
            "\1\56",
            "\1\57\4\uffff\1\60\15\uffff\1\61",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\61",
            "\1\61",
            "\1\61",
            "\1\77",
            "\1\100\26\uffff\1\61",
            "\1\61\76\uffff\1\102",
            "\1\61",
            "\1\107\1\uffff\1\106",
            "\1\107",
            "\1\106",
            "\1\106",
            "\1\106",
            "\1\112",
            "\1\113",
            "",
            "\1\55\1\uffff\12\116\13\uffff\1\55\22\uffff\1\114\14\uffff\1"+
            "\55\22\uffff\1\114",
            "\1\55\1\uffff\12\116\13\uffff\1\55\37\uffff\1\55",
            "",
            "\1\120\26\uffff\1\120\31\uffff\1\120\71\uffff\1\117",
            "\1\122\26\uffff\1\122\31\uffff\1\122\56\uffff\1\121",
            "",
            "\1\124\26\uffff\1\124\31\uffff\1\124\67\uffff\1\123",
            "\1\47\13\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\126",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "\1\131",
            "\1\132",
            "\12\133\7\uffff\6\133\32\uffff\6\133",
            "",
            "\1\55\1\uffff\12\116\13\uffff\1\55\37\uffff\1\55",
            "\1\134",
            "",
            "\1\135",
            "",
            "\1\136",
            "",
            "",
            "\1\137",
            "",
            "",
            "\1\140",
            "\1\141",
            "\1\55\1\uffff\12\133\7\uffff\4\133\1\142\1\133\32\uffff\4\133"+
            "\1\142\1\133",
            "\1\143",
            "\1\122\26\uffff\1\122\31\uffff\1\122",
            "\1\144",
            "\1\145\26\uffff\1\145\31\uffff\1\145",
            "\1\47\13\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\147",
            "\1\55\2\uffff\1\55\1\uffff\1\150\11\151\7\uffff\4\133\1\142"+
            "\1\133\32\uffff\4\133\1\142\1\133",
            "\1\120\26\uffff\1\120\31\uffff\1\120",
            "\1\152",
            "",
            "",
            "\1\47\13\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\151\7\uffff\4\133\1\142\1\133\32\uffff\4\133\1\142\1\133",
            "\12\151\7\uffff\4\133\1\142\1\133\32\uffff\4\133\1\142\1\133",
            "\1\124\26\uffff\1\124\31\uffff\1\124",
            ""
    };

    static final short[] DFA23_eot = DFA.unpackEncodedString(DFA23_eotS);
    static final short[] DFA23_eof = DFA.unpackEncodedString(DFA23_eofS);
    static final char[] DFA23_min = DFA.unpackEncodedStringToUnsignedChars(DFA23_minS);
    static final char[] DFA23_max = DFA.unpackEncodedStringToUnsignedChars(DFA23_maxS);
    static final short[] DFA23_accept = DFA.unpackEncodedString(DFA23_acceptS);
    static final short[] DFA23_special = DFA.unpackEncodedString(DFA23_specialS);
    static final short[][] DFA23_transition;

    static {
        int numStates = DFA23_transitionS.length;
        DFA23_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA23_transition[i] = DFA.unpackEncodedString(DFA23_transitionS[i]);
        }
    }

    class DFA23 extends DFA {

        public DFA23(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 23;
            this.eot = DFA23_eot;
            this.eof = DFA23_eof;
            this.min = DFA23_min;
            this.max = DFA23_max;
            this.accept = DFA23_accept;
            this.special = DFA23_special;
            this.transition = DFA23_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( EN | DU | EX | ON | BIND | WS | NL | COMMENT | RCOMMENT | LPAREN | RPAREN | LCURLB | RCURLB | LSQUB | RSQUB | DOT | COMMA | COLON | SCOLON | PLUS | MINUS | STAR | SLASH | MOD | LAND | LOR | AND | OR | POWER | XOR | SHIFT | EQ_OP | COMP_OP | ASSIGN | NOT | TILDE | TRUE | FALSE | IDENT | NUMBER | INTEGER | ANY );";
        }
        public int specialStateTransition(int s, IntStream input) throws NoViableAltException {
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA23_68 = input.LA(1);

                         
                        int index23_68 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (!cBitOptsOn ) ) {s = 87;}

                        else if ( ( cBitOptsOn ) ) {s = 88;}

                        else if ( (true) ) {s = 36;}

                         
                        input.seek(index23_68);
                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 23, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}