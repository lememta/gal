/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/main/StateflowImporterMachine.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2010-03-02 22:54:20 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.main;

import geneauto.modelfactory.factories.SystemModelFactory;
import geneauto.models.gasystemmodel.GASystemModel;
import geneauto.models.gasystemmodel.gafunctionalmodel.blocks.ChartBlock;
import geneauto.models.gasystemmodel.gastatemodel.Event;
import geneauto.models.genericmodel.GAModelElement;
import geneauto.statemachine.StateMachine;
import geneauto.tstateflowimporter.states.InitializingState;
import geneauto.utils.FileUtil;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.antlr.runtime.tree.CommonTree;

/**
 * State machine of the Stateflow Importer Tool. It takes as an input the source model, 
 * a GASystemModel and outputs a GASystemModel where the content of Stateflow part
 * from the source model is added to the input GASystemModel. 
 * This class is a singleton as only one StateflowImporter is
 * allowed to run at the same time.
 * This machine has the following main states:
 * - Initialising
 * - Parsing
 * - Transforming
 * - Saving
 */
public class StateflowImporterMachine extends StateMachine {

	/**
	 * Unique instance of this class.
	 */
	protected static StateMachine instance;
	
    /**
     * Model read by the SystemModelFactory.
     */
    private GASystemModel systemModel;

    /**
     * Factory which is used to read the model from the xml file.
     */
    private SystemModelFactory systemModelFactory;

    /**
     * List of stateflow elements (stateflow is described as a flat structure)
     * that we get from parsing the mdl file.
     */
    private CommonTree parserTree;

    /**
     * List of references to the chart blocks present in the model.
     */
    private List<ChartBlock> chartBlocks = new LinkedList<ChartBlock>();

	/**
	 * Pool of temporary Event objects. After the NameResolvingState there
	 * should be no more references to this pool.
	 */
    private List<Event> tempEvents = new LinkedList<Event>();

    /**
     * Private constructor of this class. Initialises the state machine
     */
    private StateflowImporterMachine() {
    	
    	// set the version number of the elementary tool machine
    	super(StateflowImporter.VERSION,"");
    	
        String toolClassPath = "geneauto/tstateflowimporter/main/StateflowImporterMachine.class";
        URL url = this.getClass().getClassLoader().getResource(
                toolClassPath);
        
        String version = FileUtil.getVersionFromManifest(url,toolClassPath);
        String releaseDate = FileUtil.getDateFromManifest(url, toolClassPath);
        
        if (version!=null) {
            setVerString(version);
        }
        if (releaseDate!=null) {
            setReleaseDate(releaseDate);
        }
    	
		// set initial state
        initialState = InitializingState.getInstance();
    }

    public static StateflowImporterMachine getInstance(){
    	if (instance == null){
    		instance = new StateflowImporterMachine();
    	}
    	
    	return (StateflowImporterMachine) instance;
    }
    
    /**
     * @param systemModel
     *            the systemModel to set
     */
    public void setSystemModel(GASystemModel systemModel) {
        this.systemModel = systemModel;
    }

    /**
     * @return the systemModel
     */
    public GASystemModel getSystemModel() {
        return systemModel;
    }

    /**
     * @param systemModelFactory
     *            the systemModelFactory to set
     */
    public void setSystemModelFactory(SystemModelFactory systemModelFactory) {
        this.systemModelFactory = systemModelFactory;
    }

    /**
     * @return the systemModelFactory
     */
    public SystemModelFactory getSystemModelFactory() {
        return systemModelFactory;
    }

    public void setParserTree(CommonTree parserTree) {
		this.parserTree = parserTree;
	}

    public CommonTree getParserTree() {
		return parserTree;
	}

	public void setChartBlocks(List<GAModelElement> chartBlocks) {
		this.chartBlocks.clear();
		
		for (GAModelElement el : chartBlocks){
			if (el instanceof ChartBlock){
			    this.chartBlocks.add((ChartBlock) el);
			}
		}
	}

	public List<ChartBlock> getChartBlocks() {
		return chartBlocks;
	}

	public List<Event> getTempEvents() {
		return tempEvents;
	}
}
