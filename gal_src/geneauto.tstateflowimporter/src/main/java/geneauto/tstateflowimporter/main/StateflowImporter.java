/**
 * Gene-Auto code generator
 * 
 *	$Source: /cvsroot/geneauto/geneauto.tstateflowimporter/src/main/java/geneauto/tstateflowimporter/main/StateflowImporter.java,v $
 *  @version	$Revision: 1.13 $
 *	@date		$Date: 2011-11-29 17:40:54 $
 *
 *  Copyright (c) 2006-2009 IB Krates OU
 *  	http://www.krates.ee, geneauto@krates.ee
 *
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *  
 *  Gene-Auto was originally developed by Gene-Auto consortium with support 
 *  of ITEA (www.itea2-org) and national public funding bodies in France, 
 *  Estonia, Belgium and Israel.  
 *  
 *  The members of Gene-Auto consortium are
 *  	Continental SAS
 *  	Airbus France SAS
 *  	Thales Alenia Space France SAS
 *  	ASTRIUM SAS
 *  	IB KRATES OU
 *  	Barco NV
 *  	Israel Aircraft Industries
 *  	Alyotech
 *  	Tallinn University of Technology
 *  	FERIA - Institut National Polytechnique de Toulouse
 *  	INRIA - Institut National de Recherche en Informatique et en Automatique
 *  
 */
package geneauto.tstateflowimporter.main;

import geneauto.eventhandler.EventHandler;
import geneauto.utils.ArgumentReader;
import geneauto.utils.GAConst;

/**
 * Main class of the Stateflow Importer Tool. Sets the version and date of the
 * tool. Executes the statemachine.
 * 
 */
public class StateflowImporter {

    public static String VERSION = "DEV";
    public static String TOOL_NAME = "TStateflowImporter";

    /**
     * Reference to the tool statemachine
     */
    private static StateflowImporterMachine stateMachine;

    /**
     * Main method of the Stateflow Importer Tool. Executes the statemachine.
     * 
     * @param args
     *            List of the arguments of the tool.
     */
    public static void main(String[] args) {
        // set root of all location strings in messages
        EventHandler.setToolName(TOOL_NAME);

        // initialise ArgumentReader
        ArgumentReader.clear();

        // -l and -O options are set by ArgumentReader.addCommonArguments
        ArgumentReader.addArgument("", GAConst.ARG_INPUTFILE, true, true,

        true, GAConst.ARG_INPUTFILE_NAME_ROOT);
        // Based on the "inputFile" argument a tempFolder is detected -
        // system model files are stored in tempFolder
        ArgumentReader.addArgument("--iSM", GAConst.ARG_INPUTSMFILE, true,
                true, true, GAConst.ARG_INPUTSMFILE_NAME_ROOT);
        ArgumentReader.addArgument("-o", GAConst.ARG_OUTPUTFILE, true, true,

        true, GAConst.ARG_OUTPUTFILE_NAME_ROOT);
        ArgumentReader.addArgument("-b", GAConst.ARG_LIBFILE, false, 
        		true, true, "");
        ArgumentReader.addArgument("-d", GAConst.ARG_DEBUG, false, 
        		false, false, "");

        stateMachine = StateflowImporterMachine.getInstance();

        // set arguments list to ArgumentReader and parse the list
        stateMachine.initTool(args, false);

        // Run the state machine
        stateMachine.run();
    }
}
