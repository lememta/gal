/**
 */
package geneauto.emf.transformationconfiguration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see geneauto.emf.transformationconfiguration.TransformationconfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface TransformationconfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "transformationconfiguration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://geneauto.gaesm2lustre.configuration/geneauto/emf/transformationconfiguration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "transformationconfiguration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TransformationconfigurationPackage eINSTANCE = geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link geneauto.emf.transformationconfiguration.impl.TransformationConfigurationImpl <em>Transformation Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.transformationconfiguration.impl.TransformationConfigurationImpl
	 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getTransformationConfiguration()
	 * @generated
	 */
	int TRANSFORMATION_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_CONFIGURATION__PARAMETERS = 0;

	/**
	 * The number of structural features of the '<em>Transformation Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_CONFIGURATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.transformationconfiguration.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.transformationconfiguration.impl.ParameterImpl
	 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = 0;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.transformationconfiguration.impl.BooleanParameterImpl <em>Boolean Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.transformationconfiguration.impl.BooleanParameterImpl
	 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getBooleanParameter()
	 * @generated
	 */
	int BOOLEAN_PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER__NAME = PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER__VALUE = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.transformationconfiguration.impl.StringParameterImpl <em>String Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.transformationconfiguration.impl.StringParameterImpl
	 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getStringParameter()
	 * @generated
	 */
	int STRING_PARAMETER = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER__NAME = PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER__VALUE = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link geneauto.emf.transformationconfiguration.impl.IntegerParameterImpl <em>Integer Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see geneauto.emf.transformationconfiguration.impl.IntegerParameterImpl
	 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getIntegerParameter()
	 * @generated
	 */
	int INTEGER_PARAMETER = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER__NAME = PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER__VALUE = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link geneauto.emf.transformationconfiguration.TransformationConfiguration <em>Transformation Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation Configuration</em>'.
	 * @see geneauto.emf.transformationconfiguration.TransformationConfiguration
	 * @generated
	 */
	EClass getTransformationConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link geneauto.emf.transformationconfiguration.TransformationConfiguration#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see geneauto.emf.transformationconfiguration.TransformationConfiguration#getParameters()
	 * @see #getTransformationConfiguration()
	 * @generated
	 */
	EReference getTransformationConfiguration_Parameters();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.transformationconfiguration.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see geneauto.emf.transformationconfiguration.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.transformationconfiguration.Parameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see geneauto.emf.transformationconfiguration.Parameter#getName()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Name();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.transformationconfiguration.BooleanParameter <em>Boolean Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Parameter</em>'.
	 * @see geneauto.emf.transformationconfiguration.BooleanParameter
	 * @generated
	 */
	EClass getBooleanParameter();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.transformationconfiguration.BooleanParameter#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see geneauto.emf.transformationconfiguration.BooleanParameter#isValue()
	 * @see #getBooleanParameter()
	 * @generated
	 */
	EAttribute getBooleanParameter_Value();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.transformationconfiguration.StringParameter <em>String Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Parameter</em>'.
	 * @see geneauto.emf.transformationconfiguration.StringParameter
	 * @generated
	 */
	EClass getStringParameter();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.transformationconfiguration.StringParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see geneauto.emf.transformationconfiguration.StringParameter#getValue()
	 * @see #getStringParameter()
	 * @generated
	 */
	EAttribute getStringParameter_Value();

	/**
	 * Returns the meta object for class '{@link geneauto.emf.transformationconfiguration.IntegerParameter <em>Integer Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Parameter</em>'.
	 * @see geneauto.emf.transformationconfiguration.IntegerParameter
	 * @generated
	 */
	EClass getIntegerParameter();

	/**
	 * Returns the meta object for the attribute '{@link geneauto.emf.transformationconfiguration.IntegerParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see geneauto.emf.transformationconfiguration.IntegerParameter#getValue()
	 * @see #getIntegerParameter()
	 * @generated
	 */
	EAttribute getIntegerParameter_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TransformationconfigurationFactory getTransformationconfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link geneauto.emf.transformationconfiguration.impl.TransformationConfigurationImpl <em>Transformation Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.transformationconfiguration.impl.TransformationConfigurationImpl
		 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getTransformationConfiguration()
		 * @generated
		 */
		EClass TRANSFORMATION_CONFIGURATION = eINSTANCE.getTransformationConfiguration();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_CONFIGURATION__PARAMETERS = eINSTANCE.getTransformationConfiguration_Parameters();

		/**
		 * The meta object literal for the '{@link geneauto.emf.transformationconfiguration.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.transformationconfiguration.impl.ParameterImpl
		 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__NAME = eINSTANCE.getParameter_Name();

		/**
		 * The meta object literal for the '{@link geneauto.emf.transformationconfiguration.impl.BooleanParameterImpl <em>Boolean Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.transformationconfiguration.impl.BooleanParameterImpl
		 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getBooleanParameter()
		 * @generated
		 */
		EClass BOOLEAN_PARAMETER = eINSTANCE.getBooleanParameter();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_PARAMETER__VALUE = eINSTANCE.getBooleanParameter_Value();

		/**
		 * The meta object literal for the '{@link geneauto.emf.transformationconfiguration.impl.StringParameterImpl <em>String Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.transformationconfiguration.impl.StringParameterImpl
		 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getStringParameter()
		 * @generated
		 */
		EClass STRING_PARAMETER = eINSTANCE.getStringParameter();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_PARAMETER__VALUE = eINSTANCE.getStringParameter_Value();

		/**
		 * The meta object literal for the '{@link geneauto.emf.transformationconfiguration.impl.IntegerParameterImpl <em>Integer Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see geneauto.emf.transformationconfiguration.impl.IntegerParameterImpl
		 * @see geneauto.emf.transformationconfiguration.impl.TransformationconfigurationPackageImpl#getIntegerParameter()
		 * @generated
		 */
		EClass INTEGER_PARAMETER = eINSTANCE.getIntegerParameter();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_PARAMETER__VALUE = eINSTANCE.getIntegerParameter_Value();

	}

} //TransformationconfigurationPackage
