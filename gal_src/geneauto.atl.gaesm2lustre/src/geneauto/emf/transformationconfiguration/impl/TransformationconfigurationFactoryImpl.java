/**
 */
package geneauto.emf.transformationconfiguration.impl;

import geneauto.emf.transformationconfiguration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TransformationconfigurationFactoryImpl extends EFactoryImpl implements TransformationconfigurationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TransformationconfigurationFactory init() {
		try {
			TransformationconfigurationFactory theTransformationconfigurationFactory = (TransformationconfigurationFactory)EPackage.Registry.INSTANCE.getEFactory("http://geneauto.gaesm2lustre.configuration/geneauto/emf/transformationconfiguration"); 
			if (theTransformationconfigurationFactory != null) {
				return theTransformationconfigurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TransformationconfigurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationconfigurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TransformationconfigurationPackage.TRANSFORMATION_CONFIGURATION: return createTransformationConfiguration();
			case TransformationconfigurationPackage.PARAMETER: return createParameter();
			case TransformationconfigurationPackage.BOOLEAN_PARAMETER: return createBooleanParameter();
			case TransformationconfigurationPackage.STRING_PARAMETER: return createStringParameter();
			case TransformationconfigurationPackage.INTEGER_PARAMETER: return createIntegerParameter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationConfiguration createTransformationConfiguration() {
		TransformationConfigurationImpl transformationConfiguration = new TransformationConfigurationImpl();
		return transformationConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter createBooleanParameter() {
		BooleanParameterImpl booleanParameter = new BooleanParameterImpl();
		return booleanParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringParameter createStringParameter() {
		StringParameterImpl stringParameter = new StringParameterImpl();
		return stringParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerParameter createIntegerParameter() {
		IntegerParameterImpl integerParameter = new IntegerParameterImpl();
		return integerParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationconfigurationPackage getTransformationconfigurationPackage() {
		return (TransformationconfigurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TransformationconfigurationPackage getPackage() {
		return TransformationconfigurationPackage.eINSTANCE;
	}

} //TransformationconfigurationFactoryImpl
