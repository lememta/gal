/**
 */
package geneauto.emf.transformationconfiguration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link geneauto.emf.transformationconfiguration.BooleanParameter#isValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see geneauto.emf.transformationconfiguration.TransformationconfigurationPackage#getBooleanParameter()
 * @model
 * @generated
 */
public interface BooleanParameter extends Parameter {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(boolean)
	 * @see geneauto.emf.transformationconfiguration.TransformationconfigurationPackage#getBooleanParameter_Value()
	 * @model required="true"
	 * @generated
	 */
	boolean isValue();

	/**
	 * Sets the value of the '{@link geneauto.emf.transformationconfiguration.BooleanParameter#isValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isValue()
	 * @generated
	 */
	void setValue(boolean value);

} // BooleanParameter
