# GAL -- GeneAuto for Lustre #

GAL is a Lustre code generator from Simulink models, and its based on the [GeneAuto tool set](http://geneauto.gforge.enseeiht.fr/).


### People ###

* [Arnaud Dieumegard](http://dieumegard.perso.enseeiht.fr/)
* [Pierre-Loic Garoche](http://www.onera.fr/fr/staff/pierre-loic-garoche)
* [Temesghen Kahsai](http://lememta.info)